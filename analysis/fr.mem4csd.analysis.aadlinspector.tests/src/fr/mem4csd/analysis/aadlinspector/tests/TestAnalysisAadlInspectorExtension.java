package fr.mem4csd.analysis.aadlinspector.tests;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.osate.annexsupport.AnnexRegistry;
import org.osate.ba.AadlBaParserAction;
import org.osate.ba.AadlBaResolver;
import org.osate.ba.AadlBaUnParserAction;
import org.osate.ba.texteditor.AadlBaSemanticHighlighter;
import org.osate.ba.texteditor.AadlBaTextPositionResolver;

import de.mdelab.workflow.Workflow;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.WorkflowPackage;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage;
import fr.mem4csd.analysis.workflowaadlinspector.WorkflowaadlinspectorPackage;
import fr.mem4csd.ramses.core.util.RamsesStandaloneSetup;
import fr.mem4csd.ramses.tests.core.utils.ReportComparator;
import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage;
import fr.tpt.mem4csd.utils.osate.standalone.StandaloneAnnexRegistry.AnnexExtensionRegistration;


public class TestAnalysisAadlInspectorExtension {	
	
	protected final ResourceSet resourceSet;
	protected final URI baseWorkflowDirUri;
	
	private static String PROJECT_NAME = "fr.mem4csd.analysis.aadlinspector.tests.sources";
	private static String MODEL_DIR = "model";
	private static String OUTPUT_DIR= "output";
	private static String INPUT_DIR = "input";
	private static String OUTPUTREF_DIR= "output_ref";
	private static String PLUGIN_SCHEME = "../../../";
	
	private static final String SOURCE_AADL_FILE_KEY = "source_aadl_file";
	private static final String SOURCE_FILE_NAME_KEY = "source_file_name";
	private static final String SYSTEM_IMPLEMENTATION_NAME_KEY = "system_implementation_name";
	private static final String OUTPUT_DIR_KEY = "output_dir";
	
	private static final String EXECUTIONTIMEBOUNDS_TO_AADL_WORKFLOW = "executiontimebounds_to_AADL";
	private static final String EXECUTIONTIMEBOUNDS_MODEL_FILE = "executiontimebounds_model_file";
	private static final String OUTPUT_MODEL_FILE = "output_model_file";
	private static final String INSTANCE_MODEL_FILE = "instance_model_file";
	private static final String AIC_FILE_KEY = "aic_file_name";

	
	public TestAnalysisAadlInspectorExtension()
	{
		if ( Platform.isRunning() ) {
			baseWorkflowDirUri = URI.createPlatformPluginURI( getWorkflowProjectDir() + File.separatorChar, true );
			
			
			
			resourceSet= new ResourceSetImpl();
		}
		else {
			baseWorkflowDirUri = URI.createFileURI( new File( "." ).getAbsoluteFile().getParentFile().getParent() + File.separatorChar + getWorkflowProjectDir() );
//			baseWorkflowDirUri = URI.createURI( "classpath:" +  File.separatorChar + getWorkflowProjectDir());
			
			List<AnnexExtensionRegistration> annexExtensions = new ArrayList<AnnexExtensionRegistration>();
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_PARSER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaParserAction.class ));
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_UNPARSER_EXT_ID, AadlBaUnParserAction.ANNEX_NAME, AadlBaUnParserAction.class ));
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_RESOLVER_EXT_ID, AadlBaResolver.ANNEX_NAME, AadlBaResolver.class ));
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_TEXTPOSITIONRESOLVER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaTextPositionResolver.class ));
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_HIGHLIGHTER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaSemanticHighlighter.class ));
			resourceSet = new RamsesStandaloneSetup(annexExtensions).createResourceSet(baseWorkflowDirUri, (String[]) null);
		
		}
		
	}

	protected String getWorkflowProjectDir() {
		return "fr.mem4csd.analysis.aadlinspector/resources/workflows";
	}

	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException, IOException {
		if (Platform.isRunning()) {
			final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject( PROJECT_NAME );
			final IFolder folder = project.getFolder( MODEL_DIR );
			final IFolder folderoutput = folder.getFolder( OUTPUT_DIR );
			folderoutput.delete(true, new NullProgressMonitor());
			folderoutput.create(true, true, new NullProgressMonitor());
			project.build(IncrementalProjectBuilder.CLEAN_BUILD, new NullProgressMonitor());

			// Building is performed in another thread so we need to ensure it jas 
			Thread.sleep( 8000 );
		}
		else {
			File outputDir = new File( "../" + PROJECT_NAME + "/" + MODEL_DIR + "/" + OUTPUT_DIR ) ;
			if(outputDir.exists())
				FileUtils.cleanDirectory(outputDir);
			WorkflowanalysisexecutiontimePackage.eINSTANCE.eClass();
			WorkflowaadlinspectorPackage.eINSTANCE.eClass();
			ExecutiontimeboundsPackage.eINSTANCE.eClass();
			
		}
	}
	
	@Test
	public void testExecutionTimeBoundsToComputeExecutionTime() 
	throws WorkflowExecutionException, IOException {
		
		final Map<String, String> props = new HashMap<String, String>();
		
		final String INPUT_FILE_NAME = "test-call-sequences.aadl";
		final String OUTPUT_FILE_NAME = "call-sequences-executiontimebounds.aadl";
		final String AADL_FILE_REFINED_URI = PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+OUTPUT_FILE_NAME;
		final String AADL_FILE_REFINED_URI_REF = PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+OUTPUT_FILE_NAME;
		
		
		props.put(OUTPUT_DIR_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR);
		props.put(SYSTEM_IMPLEMENTATION_NAME_KEY, "root.impl");
		props.put(SOURCE_AADL_FILE_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+INPUT_DIR+File.separator+INPUT_FILE_NAME);
		props.put(EXECUTIONTIMEBOUNDS_MODEL_FILE, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+INPUT_DIR+File.separator+"test-call-sequences.executiontimebounds");
		props.put(OUTPUT_MODEL_FILE, "call-sequences-executiontimebounds.aadl");
		props.put(INSTANCE_MODEL_FILE, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+INPUT_DIR+File.separator+"test-call-sequences.aaxl2");
		
		
		executeWithContext(EXECUTIONTIMEBOUNDS_TO_AADL_WORKFLOW, props);
		
		final String aadlFileComp = "../" + AADL_FILE_REFINED_URI;
		final String aadlFileRef = "../" + AADL_FILE_REFINED_URI_REF;

		final ReportComparator cmpAadl = new ReportComparator(aadlFileComp, aadlFileRef);
		assertTrue( cmpAadl.checkResults() );
		
	}
	
	@Test
	public void testExecutionTimeBoundsToCriticalSectionTime() 
	throws WorkflowExecutionException, IOException {
		
		final Map<String, String> props = new HashMap<String, String>();
		
		final String INPUT_FILE_NAME = "test-lock-actions.aadl";
		final String OUTPUT_FILE_NAME = "lock-actions-executiontimebounds.aadl";
		final String AADL_FILE_REFINED_URI = PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+OUTPUT_FILE_NAME;
		final String AADL_FILE_REFINED_URI_REF = PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+OUTPUT_FILE_NAME;
		
		
		props.put(OUTPUT_DIR_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR);
		props.put(SYSTEM_IMPLEMENTATION_NAME_KEY, "root.impl");
		props.put(SOURCE_AADL_FILE_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+INPUT_DIR+File.separator+INPUT_FILE_NAME);
		props.put(EXECUTIONTIMEBOUNDS_MODEL_FILE, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+INPUT_DIR+File.separator+"test-lock-actions.executiontimebounds");
		props.put(OUTPUT_MODEL_FILE, "lock-actions-executiontimebounds.aadl");
		props.put(INSTANCE_MODEL_FILE, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+INPUT_DIR+File.separator+"test-lock-actions.aaxl2");
		
		
		executeWithContext(EXECUTIONTIMEBOUNDS_TO_AADL_WORKFLOW, props);
		
		final String aadlFileComp = "../" + AADL_FILE_REFINED_URI;
		final String aadlFileRef = "../" + AADL_FILE_REFINED_URI_REF;

		final ReportComparator cmpAadl = new ReportComparator(aadlFileComp, aadlFileRef);
		assertTrue( cmpAadl.checkResults() );
		
	}
	
	@Test
	public void testResponseTimeAnalysisLaunch() 
	throws WorkflowExecutionException, IOException {
		
		final Map<String, String> props = new HashMap<String, String>();
		
		final String INPUT_FILE_NAME = "test-response-time.aadl";
		final String OUTPUT_FILE_NAME = "resonse-time";
		final String AIC_FILE_NAME = "response-time";
		
		final String OUTPUT_DIR_PATH = PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR;
		final String OUTPUTREF_DIR_PATH = PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR;
		
		props.put(OUTPUT_DIR_KEY, PLUGIN_SCHEME+OUTPUT_DIR_PATH);
		props.put(SOURCE_AADL_FILE_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+INPUT_DIR+File.separator+INPUT_FILE_NAME);
		
		props.put(AIC_FILE_KEY, AIC_FILE_NAME);
		
		props.put("aadlinspector_exec_file", System.getProperty("AADL_INSPECTOR_EXEC_FILE"));
		
		props.put("output_file_name", OUTPUT_FILE_NAME);
		
		props.put("is_gui_mode", "false");
		
		executeWithContext("execute_responsetimeanalysis", props);
		
		String outputXMLFile = "../"+OUTPUT_DIR_PATH+File.separator+OUTPUT_FILE_NAME+".xml";
		String outputXMLRefFile = "../"+OUTPUTREF_DIR_PATH+File.separator+OUTPUT_FILE_NAME+".xml";
		
		File f = new File(outputXMLFile);
		assertTrue(f.exists());
		
		boolean sameResponseTimeFiles = FileUtils.contentEquals(f, new File(outputXMLRefFile));
		assertTrue(sameResponseTimeFiles);
	}
	
	
	@Test
	public void testChainingExecTimeAndResponseTimeAnalysisLaunch() 
	throws WorkflowExecutionException, IOException {
		
		final Map<String, String> props = new HashMap<String, String>();
		
		final String INPUT_FILE_NAME = "test-branching";
		
		final String OUTPUT_DIR_PATH = PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR;
		final String OUTPUTREF_DIR_PATH = PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR;
		
		props.put(OUTPUT_DIR_KEY, PLUGIN_SCHEME+OUTPUT_DIR_PATH);
		props.put("intermediate_file_name", INPUT_FILE_NAME);
		props.put(SOURCE_AADL_FILE_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+INPUT_DIR+File.separator+INPUT_FILE_NAME+".aadl");
		
		props.put(AIC_FILE_KEY, INPUT_FILE_NAME);
		
		props.put("aadlinspector_exec_file", System.getProperty("AADL_INSPECTOR_EXEC_FILE"));
		
		props.put("output_file_name", INPUT_FILE_NAME);
		
		props.put("system_implementation_name", "root.impl");
		
		props.put("is_gui_mode", "false");
		
		executeWithContext("chain_executiontimebounds_extraction_responsetime_analysis", props);
		
		String outputXMLFile = "../"+OUTPUT_DIR_PATH+File.separator+INPUT_FILE_NAME+".xml";
		String outputXMLRefFile = "../"+OUTPUTREF_DIR_PATH+File.separator+INPUT_FILE_NAME+".xml";
		File f = new File(outputXMLFile);
		assertTrue(f.exists());
		boolean sameResponseTimeFiles = FileUtils.contentEquals(f, new File(outputXMLRefFile));
		assertTrue(sameResponseTimeFiles);
	}
	
	
	protected boolean isPacked() {
		return Boolean.valueOf( System.getProperty("PACKED", Boolean.TRUE.toString() ) );
	}
	
	
	protected WorkflowExecutionContext executeWithContext(final String workflowUriStr,
			final Map<String, String> propertyValues )
					throws WorkflowExecutionException, IOException {
		final URI workflowUri = baseWorkflowDirUri.appendSegment( workflowUriStr ).appendFileExtension( WorkflowPackage.eNS_PREFIX );

		final Resource workflowResource = resourceSet.getResource(workflowUri, true);

		if ( workflowResource.getContents().isEmpty() ) {
			throw new WorkflowExecutionException( "Resource '" + workflowResource.getURI() + "' is empty." );
		}

		final Workflow workflow = (Workflow) workflowResource.getContents().get(0);

		final WorkflowExecutionContext context = workflow.execute(new NullProgressMonitor(), System.out, propertyValues, null, resourceSet);

		return context;
	}
}
