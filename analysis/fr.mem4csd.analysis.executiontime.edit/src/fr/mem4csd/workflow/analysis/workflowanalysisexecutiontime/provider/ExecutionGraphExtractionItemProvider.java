/**
 */
package fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.provider;


import de.mdelab.workflow.components.provider.WorkflowComponentItemProvider;

import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction;
import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ExecutionGraphExtractionItemProvider extends WorkflowComponentItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionGraphExtractionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSrcInstanceModelSlotPropertyDescriptor(object);
			addOutputDirPropertyDescriptor(object);
			addSaveExecutionGraphImagePropertyDescriptor(object);
			addExecutionGraphListPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Src Instance Model Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSrcInstanceModelSlotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ExecutionGraphExtraction_srcInstanceModelSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ExecutionGraphExtraction_srcInstanceModelSlot_feature", "_UI_ExecutionGraphExtraction_type"),
				 WorkflowanalysisexecutiontimePackage.Literals.EXECUTION_GRAPH_EXTRACTION__SRC_INSTANCE_MODEL_SLOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Output Dir feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOutputDirPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ExecutionGraphExtraction_outputDir_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ExecutionGraphExtraction_outputDir_feature", "_UI_ExecutionGraphExtraction_type"),
				 WorkflowanalysisexecutiontimePackage.Literals.EXECUTION_GRAPH_EXTRACTION__OUTPUT_DIR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Save Execution Graph Image feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSaveExecutionGraphImagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ExecutionGraphExtraction_saveExecutionGraphImage_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ExecutionGraphExtraction_saveExecutionGraphImage_feature", "_UI_ExecutionGraphExtraction_type"),
				 WorkflowanalysisexecutiontimePackage.Literals.EXECUTION_GRAPH_EXTRACTION__SAVE_EXECUTION_GRAPH_IMAGE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Execution Graph List feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExecutionGraphListPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ExecutionGraphExtraction_executionGraphList_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ExecutionGraphExtraction_executionGraphList_feature", "_UI_ExecutionGraphExtraction_type"),
				 WorkflowanalysisexecutiontimePackage.Literals.EXECUTION_GRAPH_EXTRACTION__EXECUTION_GRAPH_LIST,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns ExecutionGraphExtraction.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ExecutionGraphExtraction"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ExecutionGraphExtraction)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ExecutionGraphExtraction_type") :
			getString("_UI_ExecutionGraphExtraction_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ExecutionGraphExtraction.class)) {
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__SRC_INSTANCE_MODEL_SLOT:
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__OUTPUT_DIR:
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__SAVE_EXECUTION_GRAPH_IMAGE:
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__EXECUTION_GRAPH_LIST:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

}
