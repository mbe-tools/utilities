package fr.mem4csd.tests.analysis.executiontime;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.osate.annexsupport.AnnexRegistry;
import org.osate.ba.AadlBaParserAction;
import org.osate.ba.AadlBaResolver;
import org.osate.ba.AadlBaUnParserAction;
import org.osate.ba.texteditor.AadlBaSemanticHighlighter;
import org.osate.ba.texteditor.AadlBaTextPositionResolver;

import de.mdelab.workflow.Workflow;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.WorkflowPackage;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.util.RamsesStandaloneSetup;
import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage;
import fr.tpt.mem4csd.utils.osate.standalone.StandaloneAnnexRegistry.AnnexExtensionRegistration;


public class TestAnalysisExecutionTimeExtension {	
	
	// TODO: compare simple time model files
	
	protected final ResourceSet resourceSet;
	protected final URI baseWorkflowDirUri;
	
	private static String PROJECT_NAME = "fr.mem4csd.analysis.executiontime.tests.sources";
	private static String MODEL_DIR = "model";
	private static String OUTPUT_DIR= "output";
	private static String OUTPUTREF_DIR= "output_ref";
	private static String PLUGIN_SCHEME = "../../../";
	
	private static final String SOURCE_AADL_FILE_KEY = "source_aadl_file";
	private static final String SYSTEM_IMPLEMENTATION_NAME_KEY = "system_implementation_name";
	private static final String OUTPUT_DIR_KEY = "output_dir";
	
	public static final String T1_INSTANCE_FILE_NAME = "root_impl_Instance__p1__t1";
	public static final String T2_INSTANCE_FILE_NAME = "root_impl_Instance__p1__t2";
	public static final String T3_INSTANCE_FILE_NAME = "root_impl_Instance__p1__t3";
	private static final String EXECUTION_TIME_ANALYSIS_WORKFLOW = "executiontime_analysis";

	public TestAnalysisExecutionTimeExtension()
	{
		if ( Platform.isRunning() ) {
			baseWorkflowDirUri = URI.createPlatformPluginURI( getWorkflowProjectDir() + File.separatorChar, true );
			
			
			
			resourceSet= new ResourceSetImpl();
		}
		else {
			baseWorkflowDirUri = URI.createFileURI( new File( "." ).getAbsoluteFile().getParentFile().getParent() + File.separatorChar + getWorkflowProjectDir() );
//			baseWorkflowDirUri = URI.createURI( "classpath:" +  File.separatorChar + getWorkflowProjectDir());
			
			List<AnnexExtensionRegistration> annexExtensions = new ArrayList<AnnexExtensionRegistration>();
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_PARSER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaParserAction.class ));
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_UNPARSER_EXT_ID, AadlBaUnParserAction.ANNEX_NAME, AadlBaUnParserAction.class ));
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_RESOLVER_EXT_ID, AadlBaResolver.ANNEX_NAME, AadlBaResolver.class ));
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_TEXTPOSITIONRESOLVER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaTextPositionResolver.class ));
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_HIGHLIGHTER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaSemanticHighlighter.class ));
			resourceSet = new RamsesStandaloneSetup(annexExtensions).createResourceSet(baseWorkflowDirUri, (String[]) null);
		
		}
		
	}

	protected String getWorkflowProjectDir() {
		return "fr.mem4csd.analysis.executiontime/execution_time/workflows";
	}

	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException, IOException {
		if (Platform.isRunning()) {
			final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject( PROJECT_NAME );
			final IFolder folder = project.getFolder( MODEL_DIR );
			final IFolder folderoutput = folder.getFolder( OUTPUT_DIR );
			folderoutput.delete(true, new NullProgressMonitor());
			folderoutput.create(true, true, new NullProgressMonitor());
			project.build(IncrementalProjectBuilder.CLEAN_BUILD, new NullProgressMonitor());

			// Building is performed in another thread so we need to ensure it jas 
			Thread.sleep( 8000 );
		}
		else {
			File outputDir = new File( "../" + PROJECT_NAME + "/" + MODEL_DIR + "/" + OUTPUT_DIR ) ;
			if(outputDir.exists())
				FileUtils.cleanDirectory(outputDir);
			WorkflowanalysisexecutiontimePackage.eINSTANCE.eClass();
		}
	}
	
	@Test
	public void testSubprogramCallSequenceExecutionGraphExtraction() 
	throws WorkflowExecutionException, IOException {
		
		final Map<String, String> props = new HashMap<String, String>();
		
		props.put(OUTPUT_DIR_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"call-sequences");
		props.put(SYSTEM_IMPLEMENTATION_NAME_KEY, "root.impl");
		props.put(SOURCE_AADL_FILE_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+"input"+File.separator+"test-call-sequences.aadl");
		
		executeWithContext(EXECUTION_TIME_ANALYSIS_WORKFLOW, props);
		
		URI t1RefGraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+"call-sequences" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
		URI t1GraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"call-sequences" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
	
		File t1RefGraphImage = new File(t1RefGraphImageURI.toFileString());
		File t1GraphImage = new File(t1GraphImageURI.toFileString());
		
		boolean sameGraphImages = FileUtils.contentEquals(t1GraphImage, t1RefGraphImage);
		assertTrue(sameGraphImages);
		
		URI t2RefGraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+"call-sequences" )
				.appendSegment(T2_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
		URI t2GraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"call-sequences" )
				.appendSegment(T2_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
	
		File t2RefGraphImage = new File(t2RefGraphImageURI.toFileString());
		File t2GraphImage = new File(t2GraphImageURI.toFileString());
		
		sameGraphImages = FileUtils.contentEquals(t2GraphImage, t2RefGraphImage);
		assertTrue(sameGraphImages);
		
		URI t3RefGraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+"call-sequences" )
				.appendSegment(T3_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
		URI t3GraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"call-sequences" )
				.appendSegment(T3_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
	
		File t3RefGraphImage = new File(t3RefGraphImageURI.toFileString());
		File t3GraphImage = new File(t3GraphImageURI.toFileString());
		
		sameGraphImages = FileUtils.contentEquals(t3GraphImage, t3RefGraphImage);
		assertTrue(sameGraphImages);
	}
	
	@Test
	public void testSubprogramCallActionExecutionGraphExtraction() 
	throws WorkflowExecutionException, IOException {
		
		final Map<String, String> props = new HashMap<String, String>();
		
		props.put(OUTPUT_DIR_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"call-actions");
		props.put(SYSTEM_IMPLEMENTATION_NAME_KEY, "root.impl");
		props.put(SOURCE_AADL_FILE_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+"input"+File.separator+"test-call-actions.aadl");
		
		executeWithContext(EXECUTION_TIME_ANALYSIS_WORKFLOW, props);
		
		URI t1RefGraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+"call-actions" )
		.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
		URI t1GraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"call-actions" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
	
		File t1RefGraphImage = new File(t1RefGraphImageURI.toFileString());
		File t1GraphImage = new File(t1GraphImageURI.toFileString());
		
		boolean sameGraphImages = FileUtils.contentEquals(t1GraphImage, t1RefGraphImage);
		assertTrue(sameGraphImages);
	}

	@Test
	public void testSubprogramTimedActionExecutionGraphExtraction() 
	throws WorkflowExecutionException, IOException {
		
		final Map<String, String> props = new HashMap<String, String>();
		
		props.put(OUTPUT_DIR_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"timed-actions");
		props.put(SYSTEM_IMPLEMENTATION_NAME_KEY, "root.impl");
		props.put(SOURCE_AADL_FILE_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+"input"+File.separator+"test-timed-actions.aadl");
		
		executeWithContext(EXECUTION_TIME_ANALYSIS_WORKFLOW, props);
		

		URI t1RefGraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+"timed-actions" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
		URI t1GraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"timed-actions" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
	
		File t1RefGraphImage = new File(t1RefGraphImageURI.toFileString());
		File t1GraphImage = new File(t1GraphImageURI.toFileString());
		
		boolean sameGraphImages = FileUtils.contentEquals(t1GraphImage, t1RefGraphImage);
		assertTrue(sameGraphImages);
	}
	
	@Test
	public void testSubprogramLockActionExecutionGraphExtraction() 
	throws WorkflowExecutionException, IOException {
		
		final Map<String, String> props = new HashMap<String, String>();
		
		props.put(OUTPUT_DIR_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"lock-actions");
		props.put(SYSTEM_IMPLEMENTATION_NAME_KEY, "root.impl");
		props.put(SOURCE_AADL_FILE_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+"input"+File.separator+"test-lock-actions.aadl");
		
		executeWithContext(EXECUTION_TIME_ANALYSIS_WORKFLOW, props);
		

		URI t1RefGraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+"lock-actions" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
		URI t1GraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"lock-actions" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
	
		File t1RefGraphImage = new File(t1RefGraphImageURI.toFileString());
		File t1GraphImage = new File(t1GraphImageURI.toFileString());
		
		boolean sameGraphImages = FileUtils.contentEquals(t1GraphImage, t1RefGraphImage);
		assertTrue(sameGraphImages);
	}
	
	
	@Test
	public void testLockActionInSubprogramCallExecutionGraphExtraction() 
	throws WorkflowExecutionException, IOException {
		
		final Map<String, String> props = new HashMap<String, String>();
		
		props.put(OUTPUT_DIR_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"lock-in-subprogramcall");
		props.put(SYSTEM_IMPLEMENTATION_NAME_KEY, "root.impl");
		props.put(SOURCE_AADL_FILE_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+"input"+File.separator+"test-lock-in-subprogramcall.aadl");
		
		executeWithContext(EXECUTION_TIME_ANALYSIS_WORKFLOW, props);
		

		URI t1RefGraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+"lock-in-subprogramcall" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
		URI t1GraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"lock-in-subprogramcall" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
	
		File t1RefGraphImage = new File(t1RefGraphImageURI.toFileString());
		File t1GraphImage = new File(t1GraphImageURI.toFileString());
		
		boolean sameGraphImages = FileUtils.contentEquals(t1GraphImage, t1RefGraphImage);
		assertTrue(sameGraphImages);
	}
	
	
	@Test
	public void testBranchingExecutionGraphExtraction() 
	throws WorkflowExecutionException, IOException {
		
		final Map<String, String> props = new HashMap<String, String>();
		
		props.put(OUTPUT_DIR_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"branching");
		props.put(SYSTEM_IMPLEMENTATION_NAME_KEY, "root.impl");
		props.put(SOURCE_AADL_FILE_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+"input"+File.separator+"test-branching.aadl");
		
		executeWithContext(EXECUTION_TIME_ANALYSIS_WORKFLOW, props);
		

		URI t1RefGraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+"branching" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
		URI t1GraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"branching" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
	
		File t1RefGraphImage = new File(t1RefGraphImageURI.toFileString());
		File t1GraphImage = new File(t1GraphImageURI.toFileString());
		
		boolean sameGraphImages = FileUtils.contentEquals(t1GraphImage, t1RefGraphImage);
		assertTrue(sameGraphImages);
	}
	
	@Test
	public void testBindingExecutionGraphExtraction() 
	throws WorkflowExecutionException, IOException {
		
		final Map<String, String> props = new HashMap<String, String>();
		
		props.put(OUTPUT_DIR_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"binding");
		props.put(SYSTEM_IMPLEMENTATION_NAME_KEY, "root.impl");
		props.put(SOURCE_AADL_FILE_KEY, PLUGIN_SCHEME+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+"input"+File.separator+"test-binding.aadl");
		
		executeWithContext(EXECUTION_TIME_ANALYSIS_WORKFLOW, props);
		

		URI t1RefGraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+"binding" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
		URI t1GraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUT_DIR+File.separator+"binding" )
				.appendSegment(T1_INSTANCE_FILE_NAME)
				.appendFileExtension("png");
	
		File t1RefGraphImage = new File(t1RefGraphImageURI.toFileString());
		File t1GraphImage = new File(t1GraphImageURI.toFileString());
		
		boolean sameGraphImages = FileUtils.contentEquals(t1GraphImage, t1RefGraphImage);
		assertTrue(sameGraphImages);
		
		URI p2_t1RefGraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+"binding" )
				.appendSegment("root_impl_Instance__p2__t1")
				.appendFileExtension("png");
		URI p2_t1GraphImageURI = URI.createFileURI( "../../"+PROJECT_NAME+File.separator+MODEL_DIR+File.separator+OUTPUTREF_DIR+File.separator+"binding" )
				.appendSegment("root_impl_Instance__p2__t1")
				.appendFileExtension("png");
	
		File p2_t1RefGraphImage = new File(p2_t1RefGraphImageURI.toFileString());
		File p2_t1GraphImage = new File(p2_t1GraphImageURI.toFileString());
		
		sameGraphImages = FileUtils.contentEquals(p2_t1GraphImage, p2_t1RefGraphImage);
		assertTrue(sameGraphImages);
	}
	
	protected boolean isPacked() {
		return Boolean.valueOf( System.getProperty("PACKED", Boolean.TRUE.toString() ) );
	}
	
	protected WorkflowExecutionContext executeWithContext(final String workflowUriStr,
			final Map<String, String> propertyValues )
					throws WorkflowExecutionException, IOException {
		final URI workflowUri = baseWorkflowDirUri.appendSegment( workflowUriStr ).appendFileExtension( WorkflowPackage.eNS_PREFIX );

		final Resource workflowResource = resourceSet.getResource(workflowUri, true);

		if ( workflowResource.getContents().isEmpty() ) {
			throw new WorkflowExecutionException( "Resource '" + workflowResource.getURI() + "' is empty." );
		}

		final Workflow workflow = (Workflow) workflowResource.getContents().get(0);

		final WorkflowExecutionContext context = workflow.execute(new NullProgressMonitor(), System.out, propertyValues, null, resourceSet);

		return context;
	}
}
