/**
 */
package fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Execution Time Bounds Production</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction#getSrcExecutionGraphListSlot <em>Src Execution Graph List Slot</em>}</li>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction#getOutputDir <em>Output Dir</em>}</li>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction#getExecutionTimeBoundsModelSlot <em>Execution Time Bounds Model Slot</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage#getExecutionTimeBoundsProduction()
 * @model
 * @generated
 */
public interface ExecutionTimeBoundsProduction extends WorkflowComponent {
	/**
	 * Returns the value of the '<em><b>Src Execution Graph List Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src Execution Graph List Slot</em>' attribute.
	 * @see #setSrcExecutionGraphListSlot(String)
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage#getExecutionTimeBoundsProduction_SrcExecutionGraphListSlot()
	 * @model required="true"
	 * @generated
	 */
	String getSrcExecutionGraphListSlot();

	/**
	 * Sets the value of the '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction#getSrcExecutionGraphListSlot <em>Src Execution Graph List Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Src Execution Graph List Slot</em>' attribute.
	 * @see #getSrcExecutionGraphListSlot()
	 * @generated
	 */
	void setSrcExecutionGraphListSlot(String value);

	/**
	 * Returns the value of the '<em><b>Output Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Dir</em>' attribute.
	 * @see #setOutputDir(String)
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage#getExecutionTimeBoundsProduction_OutputDir()
	 * @model
	 * @generated
	 */
	String getOutputDir();

	/**
	 * Sets the value of the '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction#getOutputDir <em>Output Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Dir</em>' attribute.
	 * @see #getOutputDir()
	 * @generated
	 */
	void setOutputDir(String value);

	/**
	 * Returns the value of the '<em><b>Execution Time Bounds Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Time Bounds Model Slot</em>' attribute.
	 * @see #setExecutionTimeBoundsModelSlot(String)
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage#getExecutionTimeBoundsProduction_ExecutionTimeBoundsModelSlot()
	 * @model
	 * @generated
	 */
	String getExecutionTimeBoundsModelSlot();

	/**
	 * Sets the value of the '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction#getExecutionTimeBoundsModelSlot <em>Execution Time Bounds Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Time Bounds Model Slot</em>' attribute.
	 * @see #getExecutionTimeBoundsModelSlot()
	 * @generated
	 */
	void setExecutionTimeBoundsModelSlot(String value);

} // ExecutionTimeBoundsProduction
