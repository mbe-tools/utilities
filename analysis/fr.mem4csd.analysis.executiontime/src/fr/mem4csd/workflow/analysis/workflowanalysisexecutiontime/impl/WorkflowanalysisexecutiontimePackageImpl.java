/**
 */
package fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl;

import de.mdelab.workflow.WorkflowPackage;

import de.mdelab.workflow.components.ComponentsPackage;

import de.mdelab.workflow.helpers.HelpersPackage;
import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction;
import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction;
import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimeFactory;
import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowanalysisexecutiontimePackageImpl extends EPackageImpl implements WorkflowanalysisexecutiontimePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass executionTimeBoundsProductionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass executionGraphExtractionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorkflowanalysisexecutiontimePackageImpl() {
		super(eNS_URI, WorkflowanalysisexecutiontimeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link WorkflowanalysisexecutiontimePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorkflowanalysisexecutiontimePackage init() {
		if (isInited) return (WorkflowanalysisexecutiontimePackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowanalysisexecutiontimePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWorkflowanalysisexecutiontimePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		WorkflowanalysisexecutiontimePackageImpl theWorkflowanalysisexecutiontimePackage = registeredWorkflowanalysisexecutiontimePackage instanceof WorkflowanalysisexecutiontimePackageImpl ? (WorkflowanalysisexecutiontimePackageImpl)registeredWorkflowanalysisexecutiontimePackage : new WorkflowanalysisexecutiontimePackageImpl();

		isInited = true;

		// Initialize simple dependencies
		WorkflowPackage.eINSTANCE.eClass();
		ComponentsPackage.eINSTANCE.eClass();
		HelpersPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorkflowanalysisexecutiontimePackage.createPackageContents();

		// Initialize created meta-data
		theWorkflowanalysisexecutiontimePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWorkflowanalysisexecutiontimePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorkflowanalysisexecutiontimePackage.eNS_URI, theWorkflowanalysisexecutiontimePackage);
		return theWorkflowanalysisexecutiontimePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExecutionTimeBoundsProduction() {
		return executionTimeBoundsProductionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionTimeBoundsProduction_SrcExecutionGraphListSlot() {
		return (EAttribute)executionTimeBoundsProductionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionTimeBoundsProduction_OutputDir() {
		return (EAttribute)executionTimeBoundsProductionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionTimeBoundsProduction_ExecutionTimeBoundsModelSlot() {
		return (EAttribute)executionTimeBoundsProductionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExecutionGraphExtraction() {
		return executionGraphExtractionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionGraphExtraction_SrcInstanceModelSlot() {
		return (EAttribute)executionGraphExtractionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionGraphExtraction_OutputDir() {
		return (EAttribute)executionGraphExtractionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionGraphExtraction_SaveExecutionGraphImage() {
		return (EAttribute)executionGraphExtractionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionGraphExtraction_ExecutionGraphList() {
		return (EAttribute)executionGraphExtractionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowanalysisexecutiontimeFactory getWorkflowanalysisexecutiontimeFactory() {
		return (WorkflowanalysisexecutiontimeFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		executionTimeBoundsProductionEClass = createEClass(EXECUTION_TIME_BOUNDS_PRODUCTION);
		createEAttribute(executionTimeBoundsProductionEClass, EXECUTION_TIME_BOUNDS_PRODUCTION__SRC_EXECUTION_GRAPH_LIST_SLOT);
		createEAttribute(executionTimeBoundsProductionEClass, EXECUTION_TIME_BOUNDS_PRODUCTION__OUTPUT_DIR);
		createEAttribute(executionTimeBoundsProductionEClass, EXECUTION_TIME_BOUNDS_PRODUCTION__EXECUTION_TIME_BOUNDS_MODEL_SLOT);

		executionGraphExtractionEClass = createEClass(EXECUTION_GRAPH_EXTRACTION);
		createEAttribute(executionGraphExtractionEClass, EXECUTION_GRAPH_EXTRACTION__SRC_INSTANCE_MODEL_SLOT);
		createEAttribute(executionGraphExtractionEClass, EXECUTION_GRAPH_EXTRACTION__OUTPUT_DIR);
		createEAttribute(executionGraphExtractionEClass, EXECUTION_GRAPH_EXTRACTION__SAVE_EXECUTION_GRAPH_IMAGE);
		createEAttribute(executionGraphExtractionEClass, EXECUTION_GRAPH_EXTRACTION__EXECUTION_GRAPH_LIST);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ComponentsPackage theComponentsPackage = (ComponentsPackage)EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		executionTimeBoundsProductionEClass.getESuperTypes().add(theComponentsPackage.getWorkflowComponent());
		executionGraphExtractionEClass.getESuperTypes().add(theComponentsPackage.getWorkflowComponent());

		// Initialize classes, features, and operations; add parameters
		initEClass(executionTimeBoundsProductionEClass, ExecutionTimeBoundsProduction.class, "ExecutionTimeBoundsProduction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExecutionTimeBoundsProduction_SrcExecutionGraphListSlot(), ecorePackage.getEString(), "srcExecutionGraphListSlot", null, 1, 1, ExecutionTimeBoundsProduction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionTimeBoundsProduction_OutputDir(), ecorePackage.getEString(), "outputDir", null, 0, 1, ExecutionTimeBoundsProduction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionTimeBoundsProduction_ExecutionTimeBoundsModelSlot(), ecorePackage.getEString(), "executionTimeBoundsModelSlot", null, 0, 1, ExecutionTimeBoundsProduction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(executionGraphExtractionEClass, ExecutionGraphExtraction.class, "ExecutionGraphExtraction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExecutionGraphExtraction_SrcInstanceModelSlot(), ecorePackage.getEString(), "srcInstanceModelSlot", null, 1, 1, ExecutionGraphExtraction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionGraphExtraction_OutputDir(), ecorePackage.getEString(), "outputDir", null, 0, 1, ExecutionGraphExtraction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionGraphExtraction_SaveExecutionGraphImage(), ecorePackage.getEBoolean(), "saveExecutionGraphImage", null, 0, 1, ExecutionGraphExtraction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionGraphExtraction_ExecutionGraphList(), ecorePackage.getEString(), "executionGraphList", null, 1, 1, ExecutionGraphExtraction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //WorkflowanalysisexecutiontimePackageImpl
