/**
 */
package fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl;

import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowanalysisexecutiontimeFactoryImpl extends EFactoryImpl implements WorkflowanalysisexecutiontimeFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WorkflowanalysisexecutiontimeFactory init() {
		try {
			WorkflowanalysisexecutiontimeFactory theWorkflowanalysisexecutiontimeFactory = (WorkflowanalysisexecutiontimeFactory)EPackage.Registry.INSTANCE.getEFactory(WorkflowanalysisexecutiontimePackage.eNS_URI);
			if (theWorkflowanalysisexecutiontimeFactory != null) {
				return theWorkflowanalysisexecutiontimeFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WorkflowanalysisexecutiontimeFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowanalysisexecutiontimeFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION: return createExecutionTimeBoundsProduction();
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION: return createExecutionGraphExtraction();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionTimeBoundsProduction createExecutionTimeBoundsProduction() {
		ExecutionTimeBoundsProductionImpl executionTimeBoundsProduction = new ExecutionTimeBoundsProductionImpl();
		return executionTimeBoundsProduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionGraphExtraction createExecutionGraphExtraction() {
		ExecutionGraphExtractionImpl executionGraphExtraction = new ExecutionGraphExtractionImpl();
		return executionGraphExtraction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowanalysisexecutiontimePackage getWorkflowanalysisexecutiontimePackage() {
		return (WorkflowanalysisexecutiontimePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WorkflowanalysisexecutiontimePackage getPackage() {
		return WorkflowanalysisexecutiontimePackage.eINSTANCE;
	}

} //WorkflowanalysisexecutiontimeFactoryImpl
