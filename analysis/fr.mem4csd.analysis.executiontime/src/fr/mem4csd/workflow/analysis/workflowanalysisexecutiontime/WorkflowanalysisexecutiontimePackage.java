/**
 */
package fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime;

import de.mdelab.workflow.components.ComponentsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimeFactory
 * @model kind="package"
 * @generated
 */
public interface WorkflowanalysisexecutiontimePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workflowanalysisexecutiontime";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/analysis/workflowanalysisexecutiontime";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workflowanalysisexecutiontime";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowanalysisexecutiontimePackage eINSTANCE = fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.WorkflowanalysisexecutiontimePackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionTimeBoundsProductionImpl <em>Execution Time Bounds Production</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionTimeBoundsProductionImpl
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.WorkflowanalysisexecutiontimePackageImpl#getExecutionTimeBoundsProduction()
	 * @generated
	 */
	int EXECUTION_TIME_BOUNDS_PRODUCTION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_TIME_BOUNDS_PRODUCTION__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_TIME_BOUNDS_PRODUCTION__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_TIME_BOUNDS_PRODUCTION__ENABLED = ComponentsPackage.WORKFLOW_COMPONENT__ENABLED;

	/**
	 * The feature id for the '<em><b>Src Execution Graph List Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_TIME_BOUNDS_PRODUCTION__SRC_EXECUTION_GRAPH_LIST_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_TIME_BOUNDS_PRODUCTION__OUTPUT_DIR = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Execution Time Bounds Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_TIME_BOUNDS_PRODUCTION__EXECUTION_TIME_BOUNDS_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Execution Time Bounds Production</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_TIME_BOUNDS_PRODUCTION_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_TIME_BOUNDS_PRODUCTION___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int EXECUTION_TIME_BOUNDS_PRODUCTION___EXECUTE__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_TIME_BOUNDS_PRODUCTION___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_TIME_BOUNDS_PRODUCTION___CHECK_CANCELED__IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Execution Time Bounds Production</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_TIME_BOUNDS_PRODUCTION_OPERATION_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionGraphExtractionImpl <em>Execution Graph Extraction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionGraphExtractionImpl
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.WorkflowanalysisexecutiontimePackageImpl#getExecutionGraphExtraction()
	 * @generated
	 */
	int EXECUTION_GRAPH_EXTRACTION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_GRAPH_EXTRACTION__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_GRAPH_EXTRACTION__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_GRAPH_EXTRACTION__ENABLED = ComponentsPackage.WORKFLOW_COMPONENT__ENABLED;

	/**
	 * The feature id for the '<em><b>Src Instance Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_GRAPH_EXTRACTION__SRC_INSTANCE_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_GRAPH_EXTRACTION__OUTPUT_DIR = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Save Execution Graph Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_GRAPH_EXTRACTION__SAVE_EXECUTION_GRAPH_IMAGE = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Execution Graph List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_GRAPH_EXTRACTION__EXECUTION_GRAPH_LIST = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Execution Graph Extraction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_GRAPH_EXTRACTION_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_GRAPH_EXTRACTION___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int EXECUTION_GRAPH_EXTRACTION___EXECUTE__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_GRAPH_EXTRACTION___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_GRAPH_EXTRACTION___CHECK_CANCELED__IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Execution Graph Extraction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_GRAPH_EXTRACTION_OPERATION_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction <em>Execution Time Bounds Production</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Execution Time Bounds Production</em>'.
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction
	 * @generated
	 */
	EClass getExecutionTimeBoundsProduction();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction#getSrcExecutionGraphListSlot <em>Src Execution Graph List Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Src Execution Graph List Slot</em>'.
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction#getSrcExecutionGraphListSlot()
	 * @see #getExecutionTimeBoundsProduction()
	 * @generated
	 */
	EAttribute getExecutionTimeBoundsProduction_SrcExecutionGraphListSlot();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction#getOutputDir <em>Output Dir</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Dir</em>'.
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction#getOutputDir()
	 * @see #getExecutionTimeBoundsProduction()
	 * @generated
	 */
	EAttribute getExecutionTimeBoundsProduction_OutputDir();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction#getExecutionTimeBoundsModelSlot <em>Execution Time Bounds Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Time Bounds Model Slot</em>'.
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction#getExecutionTimeBoundsModelSlot()
	 * @see #getExecutionTimeBoundsProduction()
	 * @generated
	 */
	EAttribute getExecutionTimeBoundsProduction_ExecutionTimeBoundsModelSlot();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction <em>Execution Graph Extraction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Execution Graph Extraction</em>'.
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction
	 * @generated
	 */
	EClass getExecutionGraphExtraction();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#getSrcInstanceModelSlot <em>Src Instance Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Src Instance Model Slot</em>'.
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#getSrcInstanceModelSlot()
	 * @see #getExecutionGraphExtraction()
	 * @generated
	 */
	EAttribute getExecutionGraphExtraction_SrcInstanceModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#getOutputDir <em>Output Dir</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Dir</em>'.
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#getOutputDir()
	 * @see #getExecutionGraphExtraction()
	 * @generated
	 */
	EAttribute getExecutionGraphExtraction_OutputDir();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#isSaveExecutionGraphImage <em>Save Execution Graph Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Save Execution Graph Image</em>'.
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#isSaveExecutionGraphImage()
	 * @see #getExecutionGraphExtraction()
	 * @generated
	 */
	EAttribute getExecutionGraphExtraction_SaveExecutionGraphImage();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#getExecutionGraphList <em>Execution Graph List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Graph List</em>'.
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#getExecutionGraphList()
	 * @see #getExecutionGraphExtraction()
	 * @generated
	 */
	EAttribute getExecutionGraphExtraction_ExecutionGraphList();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkflowanalysisexecutiontimeFactory getWorkflowanalysisexecutiontimeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionTimeBoundsProductionImpl <em>Execution Time Bounds Production</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionTimeBoundsProductionImpl
		 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.WorkflowanalysisexecutiontimePackageImpl#getExecutionTimeBoundsProduction()
		 * @generated
		 */
		EClass EXECUTION_TIME_BOUNDS_PRODUCTION = eINSTANCE.getExecutionTimeBoundsProduction();

		/**
		 * The meta object literal for the '<em><b>Src Execution Graph List Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_TIME_BOUNDS_PRODUCTION__SRC_EXECUTION_GRAPH_LIST_SLOT = eINSTANCE.getExecutionTimeBoundsProduction_SrcExecutionGraphListSlot();

		/**
		 * The meta object literal for the '<em><b>Output Dir</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_TIME_BOUNDS_PRODUCTION__OUTPUT_DIR = eINSTANCE.getExecutionTimeBoundsProduction_OutputDir();

		/**
		 * The meta object literal for the '<em><b>Execution Time Bounds Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_TIME_BOUNDS_PRODUCTION__EXECUTION_TIME_BOUNDS_MODEL_SLOT = eINSTANCE.getExecutionTimeBoundsProduction_ExecutionTimeBoundsModelSlot();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionGraphExtractionImpl <em>Execution Graph Extraction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionGraphExtractionImpl
		 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.WorkflowanalysisexecutiontimePackageImpl#getExecutionGraphExtraction()
		 * @generated
		 */
		EClass EXECUTION_GRAPH_EXTRACTION = eINSTANCE.getExecutionGraphExtraction();

		/**
		 * The meta object literal for the '<em><b>Src Instance Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_GRAPH_EXTRACTION__SRC_INSTANCE_MODEL_SLOT = eINSTANCE.getExecutionGraphExtraction_SrcInstanceModelSlot();

		/**
		 * The meta object literal for the '<em><b>Output Dir</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_GRAPH_EXTRACTION__OUTPUT_DIR = eINSTANCE.getExecutionGraphExtraction_OutputDir();

		/**
		 * The meta object literal for the '<em><b>Save Execution Graph Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_GRAPH_EXTRACTION__SAVE_EXECUTION_GRAPH_IMAGE = eINSTANCE.getExecutionGraphExtraction_SaveExecutionGraphImage();

		/**
		 * The meta object literal for the '<em><b>Execution Graph List</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_GRAPH_EXTRACTION__EXECUTION_GRAPH_LIST = eINSTANCE.getExecutionGraphExtraction_ExecutionGraphList();

	}

} //WorkflowanalysisexecutiontimePackage
