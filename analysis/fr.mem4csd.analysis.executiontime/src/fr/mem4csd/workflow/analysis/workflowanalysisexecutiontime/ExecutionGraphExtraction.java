/**
 */
package fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Execution Graph Extraction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#getSrcInstanceModelSlot <em>Src Instance Model Slot</em>}</li>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#getOutputDir <em>Output Dir</em>}</li>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#isSaveExecutionGraphImage <em>Save Execution Graph Image</em>}</li>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#getExecutionGraphList <em>Execution Graph List</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage#getExecutionGraphExtraction()
 * @model
 * @generated
 */
public interface ExecutionGraphExtraction extends WorkflowComponent {
	/**
	 * Returns the value of the '<em><b>Src Instance Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src Instance Model Slot</em>' attribute.
	 * @see #setSrcInstanceModelSlot(String)
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage#getExecutionGraphExtraction_SrcInstanceModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getSrcInstanceModelSlot();

	/**
	 * Sets the value of the '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#getSrcInstanceModelSlot <em>Src Instance Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Src Instance Model Slot</em>' attribute.
	 * @see #getSrcInstanceModelSlot()
	 * @generated
	 */
	void setSrcInstanceModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Output Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Dir</em>' attribute.
	 * @see #setOutputDir(String)
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage#getExecutionGraphExtraction_OutputDir()
	 * @model
	 * @generated
	 */
	String getOutputDir();

	/**
	 * Sets the value of the '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#getOutputDir <em>Output Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Dir</em>' attribute.
	 * @see #getOutputDir()
	 * @generated
	 */
	void setOutputDir(String value);

	/**
	 * Returns the value of the '<em><b>Save Execution Graph Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Save Execution Graph Image</em>' attribute.
	 * @see #setSaveExecutionGraphImage(boolean)
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage#getExecutionGraphExtraction_SaveExecutionGraphImage()
	 * @model
	 * @generated
	 */
	boolean isSaveExecutionGraphImage();

	/**
	 * Sets the value of the '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#isSaveExecutionGraphImage <em>Save Execution Graph Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Save Execution Graph Image</em>' attribute.
	 * @see #isSaveExecutionGraphImage()
	 * @generated
	 */
	void setSaveExecutionGraphImage(boolean value);

	/**
	 * Returns the value of the '<em><b>Execution Graph List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Graph List</em>' attribute.
	 * @see #setExecutionGraphList(String)
	 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage#getExecutionGraphExtraction_ExecutionGraphList()
	 * @model required="true"
	 * @generated
	 */
	String getExecutionGraphList();

	/**
	 * Sets the value of the '{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction#getExecutionGraphList <em>Execution Graph List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Graph List</em>' attribute.
	 * @see #getExecutionGraphList()
	 * @generated
	 */
	void setExecutionGraphList(String value);

} // ExecutionGraphExtraction
