/**
 */
package fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.analysis.executiontimebounds.util.ExecGraphToExecutionTimeBounds;
import fr.mem4csd.analysis.executiontimegraph.model.ExecutionGraph;
import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionTimeBoundsProduction;
import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage;

import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Execution Time Bounds Production</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionTimeBoundsProductionImpl#getSrcExecutionGraphListSlot <em>Src Execution Graph List Slot</em>}</li>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionTimeBoundsProductionImpl#getOutputDir <em>Output Dir</em>}</li>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionTimeBoundsProductionImpl#getExecutionTimeBoundsModelSlot <em>Execution Time Bounds Model Slot</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExecutionTimeBoundsProductionImpl extends WorkflowComponentImpl implements ExecutionTimeBoundsProduction {
	/**
	 * The default value of the '{@link #getSrcExecutionGraphListSlot() <em>Src Execution Graph List Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcExecutionGraphListSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String SRC_EXECUTION_GRAPH_LIST_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSrcExecutionGraphListSlot() <em>Src Execution Graph List Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcExecutionGraphListSlot()
	 * @generated
	 * @ordered
	 */
	protected String srcExecutionGraphListSlot = SRC_EXECUTION_GRAPH_LIST_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutputDir() <em>Output Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDir()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTPUT_DIR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutputDir() <em>Output Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDir()
	 * @generated
	 * @ordered
	 */
	protected String outputDir = OUTPUT_DIR_EDEFAULT;

	/**
	 * The default value of the '{@link #getExecutionTimeBoundsModelSlot() <em>Execution Time Bounds Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionTimeBoundsModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String EXECUTION_TIME_BOUNDS_MODEL_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExecutionTimeBoundsModelSlot() <em>Execution Time Bounds Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionTimeBoundsModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String executionTimeBoundsModelSlot = EXECUTION_TIME_BOUNDS_MODEL_SLOT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecutionTimeBoundsProductionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowanalysisexecutiontimePackage.Literals.EXECUTION_TIME_BOUNDS_PRODUCTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSrcExecutionGraphListSlot() {
		return srcExecutionGraphListSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSrcExecutionGraphListSlot(String newSrcExecutionGraphListSlot) {
		String oldSrcExecutionGraphListSlot = srcExecutionGraphListSlot;
		srcExecutionGraphListSlot = newSrcExecutionGraphListSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__SRC_EXECUTION_GRAPH_LIST_SLOT, oldSrcExecutionGraphListSlot, srcExecutionGraphListSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOutputDir() {
		return outputDir;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputDir(String newOutputDir) {
		String oldOutputDir = outputDir;
		outputDir = newOutputDir;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__OUTPUT_DIR, oldOutputDir, outputDir));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExecutionTimeBoundsModelSlot() {
		return executionTimeBoundsModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionTimeBoundsModelSlot(String newExecutionTimeBoundsModelSlot) {
		String oldExecutionTimeBoundsModelSlot = executionTimeBoundsModelSlot;
		executionTimeBoundsModelSlot = newExecutionTimeBoundsModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__EXECUTION_TIME_BOUNDS_MODEL_SLOT, oldExecutionTimeBoundsModelSlot, executionTimeBoundsModelSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__SRC_EXECUTION_GRAPH_LIST_SLOT:
				return getSrcExecutionGraphListSlot();
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__OUTPUT_DIR:
				return getOutputDir();
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__EXECUTION_TIME_BOUNDS_MODEL_SLOT:
				return getExecutionTimeBoundsModelSlot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__SRC_EXECUTION_GRAPH_LIST_SLOT:
				setSrcExecutionGraphListSlot((String)newValue);
				return;
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__OUTPUT_DIR:
				setOutputDir((String)newValue);
				return;
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__EXECUTION_TIME_BOUNDS_MODEL_SLOT:
				setExecutionTimeBoundsModelSlot((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__SRC_EXECUTION_GRAPH_LIST_SLOT:
				setSrcExecutionGraphListSlot(SRC_EXECUTION_GRAPH_LIST_SLOT_EDEFAULT);
				return;
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__OUTPUT_DIR:
				setOutputDir(OUTPUT_DIR_EDEFAULT);
				return;
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__EXECUTION_TIME_BOUNDS_MODEL_SLOT:
				setExecutionTimeBoundsModelSlot(EXECUTION_TIME_BOUNDS_MODEL_SLOT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__SRC_EXECUTION_GRAPH_LIST_SLOT:
				return SRC_EXECUTION_GRAPH_LIST_SLOT_EDEFAULT == null ? srcExecutionGraphListSlot != null : !SRC_EXECUTION_GRAPH_LIST_SLOT_EDEFAULT.equals(srcExecutionGraphListSlot);
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__OUTPUT_DIR:
				return OUTPUT_DIR_EDEFAULT == null ? outputDir != null : !OUTPUT_DIR_EDEFAULT.equals(outputDir);
			case WorkflowanalysisexecutiontimePackage.EXECUTION_TIME_BOUNDS_PRODUCTION__EXECUTION_TIME_BOUNDS_MODEL_SLOT:
				return EXECUTION_TIME_BOUNDS_MODEL_SLOT_EDEFAULT == null ? executionTimeBoundsModelSlot != null : !EXECUTION_TIME_BOUNDS_MODEL_SLOT_EDEFAULT.equals(executionTimeBoundsModelSlot);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (srcExecutionGraphListSlot: ");
		result.append(srcExecutionGraphListSlot);
		result.append(", outputDir: ");
		result.append(outputDir);
		result.append(", executionTimeBoundsModelSlot: ");
		result.append(executionTimeBoundsModelSlot);
		result.append(')');
		return result.toString();
	}

	@Override
	public void execute(final WorkflowExecutionContext context, final IProgressMonitor monitor)
			throws WorkflowExecutionException, IOException {

		EMap<String, Object> modelSlots = context.getModelSlots();
		
		@SuppressWarnings("unchecked")
		List<ExecutionGraph> execGraphList = (List<ExecutionGraph>) modelSlots.get(srcExecutionGraphListSlot);
		
		fr.mem4csd.analysis.executiontimebounds.TaskSet outputModel = ExecGraphToExecutionTimeBounds.createModelElement(execGraphList);
		
		modelSlots.put(executionTimeBoundsModelSlot, outputModel);
		
	}
	
} //ExecutionTimeBoundsProductionImpl
