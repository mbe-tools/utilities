/**
 */
package fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import de.mdelab.workflow.util.WorkflowUtil;
import fr.mem4csd.analysis.executiontimegraph.model.ExecutionEdge;
import fr.mem4csd.analysis.executiontimegraph.model.ExecutionGraph;
import fr.mem4csd.analysis.executiontimegraph.model.ExecutionVertex;
import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.ExecutionGraphExtraction;
import fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.EcoreUtil2;
import org.jgrapht.ext.JGraphXAdapter;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.instance.ComponentInstance;

import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.util.mxCellRenderer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Execution Graph Extraction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionGraphExtractionImpl#getSrcInstanceModelSlot <em>Src Instance Model Slot</em>}</li>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionGraphExtractionImpl#getOutputDir <em>Output Dir</em>}</li>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionGraphExtractionImpl#isSaveExecutionGraphImage <em>Save Execution Graph Image</em>}</li>
 *   <li>{@link fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.ExecutionGraphExtractionImpl#getExecutionGraphList <em>Execution Graph List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExecutionGraphExtractionImpl extends WorkflowComponentImpl implements ExecutionGraphExtraction {
	/**
	 * The default value of the '{@link #getSrcInstanceModelSlot() <em>Src Instance Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcInstanceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String SRC_INSTANCE_MODEL_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSrcInstanceModelSlot() <em>Src Instance Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcInstanceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String srcInstanceModelSlot = SRC_INSTANCE_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutputDir() <em>Output Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDir()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTPUT_DIR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutputDir() <em>Output Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDir()
	 * @generated
	 * @ordered
	 */
	protected String outputDir = OUTPUT_DIR_EDEFAULT;

	/**
	 * The default value of the '{@link #isSaveExecutionGraphImage() <em>Save Execution Graph Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSaveExecutionGraphImage()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SAVE_EXECUTION_GRAPH_IMAGE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSaveExecutionGraphImage() <em>Save Execution Graph Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSaveExecutionGraphImage()
	 * @generated
	 * @ordered
	 */
	protected boolean saveExecutionGraphImage = SAVE_EXECUTION_GRAPH_IMAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getExecutionGraphList() <em>Execution Graph List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionGraphList()
	 * @generated
	 * @ordered
	 */
	protected static final String EXECUTION_GRAPH_LIST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExecutionGraphList() <em>Execution Graph List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionGraphList()
	 * @generated
	 * @ordered
	 */
	protected String executionGraphList = EXECUTION_GRAPH_LIST_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecutionGraphExtractionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowanalysisexecutiontimePackage.Literals.EXECUTION_GRAPH_EXTRACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSrcInstanceModelSlot() {
		return srcInstanceModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSrcInstanceModelSlot(String newSrcInstanceModelSlot) {
		String oldSrcInstanceModelSlot = srcInstanceModelSlot;
		srcInstanceModelSlot = newSrcInstanceModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__SRC_INSTANCE_MODEL_SLOT, oldSrcInstanceModelSlot, srcInstanceModelSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOutputDir() {
		return outputDir;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputDir(String newOutputDir) {
		String oldOutputDir = outputDir;
		outputDir = newOutputDir;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__OUTPUT_DIR, oldOutputDir, outputDir));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSaveExecutionGraphImage() {
		return saveExecutionGraphImage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSaveExecutionGraphImage(boolean newSaveExecutionGraphImage) {
		boolean oldSaveExecutionGraphImage = saveExecutionGraphImage;
		saveExecutionGraphImage = newSaveExecutionGraphImage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__SAVE_EXECUTION_GRAPH_IMAGE, oldSaveExecutionGraphImage, saveExecutionGraphImage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExecutionGraphList() {
		return executionGraphList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionGraphList(String newExecutionGraphList) {
		String oldExecutionGraphList = executionGraphList;
		executionGraphList = newExecutionGraphList;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__EXECUTION_GRAPH_LIST, oldExecutionGraphList, executionGraphList));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__SRC_INSTANCE_MODEL_SLOT:
				return getSrcInstanceModelSlot();
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__OUTPUT_DIR:
				return getOutputDir();
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__SAVE_EXECUTION_GRAPH_IMAGE:
				return isSaveExecutionGraphImage();
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__EXECUTION_GRAPH_LIST:
				return getExecutionGraphList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__SRC_INSTANCE_MODEL_SLOT:
				setSrcInstanceModelSlot((String)newValue);
				return;
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__OUTPUT_DIR:
				setOutputDir((String)newValue);
				return;
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__SAVE_EXECUTION_GRAPH_IMAGE:
				setSaveExecutionGraphImage((Boolean)newValue);
				return;
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__EXECUTION_GRAPH_LIST:
				setExecutionGraphList((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__SRC_INSTANCE_MODEL_SLOT:
				setSrcInstanceModelSlot(SRC_INSTANCE_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__OUTPUT_DIR:
				setOutputDir(OUTPUT_DIR_EDEFAULT);
				return;
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__SAVE_EXECUTION_GRAPH_IMAGE:
				setSaveExecutionGraphImage(SAVE_EXECUTION_GRAPH_IMAGE_EDEFAULT);
				return;
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__EXECUTION_GRAPH_LIST:
				setExecutionGraphList(EXECUTION_GRAPH_LIST_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__SRC_INSTANCE_MODEL_SLOT:
				return SRC_INSTANCE_MODEL_SLOT_EDEFAULT == null ? srcInstanceModelSlot != null : !SRC_INSTANCE_MODEL_SLOT_EDEFAULT.equals(srcInstanceModelSlot);
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__OUTPUT_DIR:
				return OUTPUT_DIR_EDEFAULT == null ? outputDir != null : !OUTPUT_DIR_EDEFAULT.equals(outputDir);
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__SAVE_EXECUTION_GRAPH_IMAGE:
				return saveExecutionGraphImage != SAVE_EXECUTION_GRAPH_IMAGE_EDEFAULT;
			case WorkflowanalysisexecutiontimePackage.EXECUTION_GRAPH_EXTRACTION__EXECUTION_GRAPH_LIST:
				return EXECUTION_GRAPH_LIST_EDEFAULT == null ? executionGraphList != null : !EXECUTION_GRAPH_LIST_EDEFAULT.equals(executionGraphList);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (srcInstanceModelSlot: ");
		result.append(srcInstanceModelSlot);
		result.append(", outputDir: ");
		result.append(outputDir);
		result.append(", saveExecutionGraphImage: ");
		result.append(saveExecutionGraphImage);
		result.append(", executionGraphList: ");
		result.append(executionGraphList);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @throws IOException 
	 * 
	 * @generated not
	 */
	@Override
	public void execute(final WorkflowExecutionContext context, final IProgressMonitor monitor)
			throws WorkflowExecutionException, IOException {

		EMap<String, Object> modelSlots = context.getModelSlots();
		
		Resource srcInstanceModel = ((EObject) modelSlots.get(srcInstanceModelSlot)).eResource();
		
		
		EObject si = srcInstanceModel.getContents().get(0);
		
		List<ExecutionGraph> outputExecutionGraphList = new ArrayList<ExecutionGraph>();
		
		for(ComponentInstance ci: EcoreUtil2.getAllContentsOfType(si, ComponentInstance.class))
		{
			if(ci.getCategory().equals(ComponentCategory.THREAD))
			{	
				ExecutionGraph eg = new ExecutionGraph(ci, ci, null);
				outputExecutionGraphList.add(eg);
				if(isSaveExecutionGraphImage())
				{
					JGraphXAdapter<ExecutionVertex, ExecutionEdge> graphAdapter = 
							new JGraphXAdapter<ExecutionVertex, ExecutionEdge>(eg);
					mxIGraphLayout layout = new mxHierarchicalLayout(graphAdapter);
					layout.execute(graphAdapter.getDefaultParent());

					BufferedImage image = 
							mxCellRenderer.createBufferedImage(graphAdapter, null, 2, Color.WHITE, true, null);
					String fileName = getFileName(ci);
					
					URI uri = WorkflowUtil.getResolvedURI(URI.createURI(outputDir+File.separator+fileName+".png"), context.getWorkflowFileURI());
					
					final String filePath;
					if(false==Platform.isRunning())
						filePath = uri.toFileString();
					else
						filePath = uri.toPlatformString(true);
					
					File imgFile = new File (filePath);
					if(!imgFile.exists())
					{
						if(!imgFile.getParentFile().exists())
							imgFile.getParentFile().mkdirs();
						imgFile.createNewFile();
					}
					if(image!=null)
						ImageIO.write(image, "PNG", imgFile);
					
				}
				
			}
		}
		
		modelSlots.put(executionGraphList, outputExecutionGraphList);
		
	}
	
	
	private String getFileName(ComponentInstance ci)
	{
		if(ci.eContainer()==null)
			return ci.getName();
		return getFileName((ComponentInstance) ci.eContainer())+"__"+ci.getName();
	}

} //ExecutionGraphExtractionImpl
