/**
 */
package fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.WorkflowanalysisexecutiontimePackage
 * @generated
 */
public interface WorkflowanalysisexecutiontimeFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowanalysisexecutiontimeFactory eINSTANCE = fr.mem4csd.workflow.analysis.workflowanalysisexecutiontime.impl.WorkflowanalysisexecutiontimeFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Execution Time Bounds Production</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Execution Time Bounds Production</em>'.
	 * @generated
	 */
	ExecutionTimeBoundsProduction createExecutionTimeBoundsProduction();

	/**
	 * Returns a new object of class '<em>Execution Graph Extraction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Execution Graph Extraction</em>'.
	 * @generated
	 */
	ExecutionGraphExtraction createExecutionGraphExtraction();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WorkflowanalysisexecutiontimePackage getWorkflowanalysisexecutiontimePackage();

} //WorkflowanalysisexecutiontimeFactory
