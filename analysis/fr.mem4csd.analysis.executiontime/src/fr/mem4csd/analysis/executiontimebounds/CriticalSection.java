/**
 */
package fr.mem4csd.analysis.executiontimebounds;

import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.instance.FeatureInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Critical Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.CriticalSection#getBestCaseCriticalSectionTime <em>Best Case Critical Section Time</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.CriticalSection#getWorstCaseCriticalSectionTime <em>Worst Case Critical Section Time</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.CriticalSection#getAadlDataAccess <em>Aadl Data Access</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage#getCriticalSection()
 * @model
 * @generated
 */
public interface CriticalSection extends EObject {
	/**
	 * Returns the value of the '<em><b>Best Case Critical Section Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Best Case Critical Section Time</em>' attribute.
	 * @see #setBestCaseCriticalSectionTime(double)
	 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage#getCriticalSection_BestCaseCriticalSectionTime()
	 * @model
	 * @generated
	 */
	double getBestCaseCriticalSectionTime();

	/**
	 * Sets the value of the '{@link fr.mem4csd.analysis.executiontimebounds.CriticalSection#getBestCaseCriticalSectionTime <em>Best Case Critical Section Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Best Case Critical Section Time</em>' attribute.
	 * @see #getBestCaseCriticalSectionTime()
	 * @generated
	 */
	void setBestCaseCriticalSectionTime(double value);

	/**
	 * Returns the value of the '<em><b>Worst Case Critical Section Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Worst Case Critical Section Time</em>' attribute.
	 * @see #setWorstCaseCriticalSectionTime(double)
	 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage#getCriticalSection_WorstCaseCriticalSectionTime()
	 * @model required="true"
	 * @generated
	 */
	double getWorstCaseCriticalSectionTime();

	/**
	 * Sets the value of the '{@link fr.mem4csd.analysis.executiontimebounds.CriticalSection#getWorstCaseCriticalSectionTime <em>Worst Case Critical Section Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Worst Case Critical Section Time</em>' attribute.
	 * @see #getWorstCaseCriticalSectionTime()
	 * @generated
	 */
	void setWorstCaseCriticalSectionTime(double value);

	/**
	 * Returns the value of the '<em><b>Aadl Data Access</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aadl Data Access</em>' reference.
	 * @see #setAadlDataAccess(FeatureInstance)
	 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage#getCriticalSection_AadlDataAccess()
	 * @model
	 * @generated
	 */
	FeatureInstance getAadlDataAccess();

	/**
	 * Sets the value of the '{@link fr.mem4csd.analysis.executiontimebounds.CriticalSection#getAadlDataAccess <em>Aadl Data Access</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aadl Data Access</em>' reference.
	 * @see #getAadlDataAccess()
	 * @generated
	 */
	void setAadlDataAccess(FeatureInstance value);

} // CriticalSection
