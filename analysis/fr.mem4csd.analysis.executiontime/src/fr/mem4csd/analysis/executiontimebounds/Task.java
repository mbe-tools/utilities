/**
 */
package fr.mem4csd.analysis.executiontimebounds;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.instance.ComponentInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.Task#getCriticalSections <em>Critical Sections</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.Task#getBestCaseExecutionTime <em>Best Case Execution Time</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.Task#getWorstCaseExecutionTime <em>Worst Case Execution Time</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.Task#getAadlThreadInstance <em>Aadl Thread Instance</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage#getTask()
 * @model
 * @generated
 */
public interface Task extends EObject {
	/**
	 * Returns the value of the '<em><b>Critical Sections</b></em>' containment reference list.
	 * The list contents are of type {@link fr.mem4csd.analysis.executiontimebounds.CriticalSection}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Critical Sections</em>' containment reference list.
	 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage#getTask_CriticalSections()
	 * @model containment="true"
	 * @generated
	 */
	EList<CriticalSection> getCriticalSections();

	/**
	 * Returns the value of the '<em><b>Best Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Best Case Execution Time</em>' attribute.
	 * @see #setBestCaseExecutionTime(double)
	 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage#getTask_BestCaseExecutionTime()
	 * @model
	 * @generated
	 */
	double getBestCaseExecutionTime();

	/**
	 * Sets the value of the '{@link fr.mem4csd.analysis.executiontimebounds.Task#getBestCaseExecutionTime <em>Best Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Best Case Execution Time</em>' attribute.
	 * @see #getBestCaseExecutionTime()
	 * @generated
	 */
	void setBestCaseExecutionTime(double value);

	/**
	 * Returns the value of the '<em><b>Worst Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Worst Case Execution Time</em>' attribute.
	 * @see #setWorstCaseExecutionTime(double)
	 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage#getTask_WorstCaseExecutionTime()
	 * @model required="true"
	 * @generated
	 */
	double getWorstCaseExecutionTime();

	/**
	 * Sets the value of the '{@link fr.mem4csd.analysis.executiontimebounds.Task#getWorstCaseExecutionTime <em>Worst Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Worst Case Execution Time</em>' attribute.
	 * @see #getWorstCaseExecutionTime()
	 * @generated
	 */
	void setWorstCaseExecutionTime(double value);

	/**
	 * Returns the value of the '<em><b>Aadl Thread Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aadl Thread Instance</em>' reference.
	 * @see #setAadlThreadInstance(ComponentInstance)
	 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage#getTask_AadlThreadInstance()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getAadlThreadInstance();

	/**
	 * Sets the value of the '{@link fr.mem4csd.analysis.executiontimebounds.Task#getAadlThreadInstance <em>Aadl Thread Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aadl Thread Instance</em>' reference.
	 * @see #getAadlThreadInstance()
	 * @generated
	 */
	void setAadlThreadInstance(ComponentInstance value);

} // Task
