/**
 */
package fr.mem4csd.analysis.executiontimebounds;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage
 * @generated
 */
public interface ExecutiontimeboundsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExecutiontimeboundsFactory eINSTANCE = fr.mem4csd.analysis.executiontimebounds.impl.ExecutiontimeboundsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task</em>'.
	 * @generated
	 */
	Task createTask();

	/**
	 * Returns a new object of class '<em>Critical Section</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Critical Section</em>'.
	 * @generated
	 */
	CriticalSection createCriticalSection();

	/**
	 * Returns a new object of class '<em>Task Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Set</em>'.
	 * @generated
	 */
	TaskSet createTaskSet();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ExecutiontimeboundsPackage getExecutiontimeboundsPackage();

} //ExecutiontimeboundsFactory
