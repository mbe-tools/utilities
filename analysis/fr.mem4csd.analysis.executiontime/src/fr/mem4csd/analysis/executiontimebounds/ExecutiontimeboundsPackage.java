/**
 */
package fr.mem4csd.analysis.executiontimebounds;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsFactory
 * @model kind="package"
 * @generated
 */
public interface ExecutiontimeboundsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "executiontimebounds";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mem4csd.telecom-paris.fr/analysis/executiontimebounds";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fr.mem4csd.analysis";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExecutiontimeboundsPackage eINSTANCE = fr.mem4csd.analysis.executiontimebounds.impl.ExecutiontimeboundsPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.analysis.executiontimebounds.impl.TaskImpl <em>Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.analysis.executiontimebounds.impl.TaskImpl
	 * @see fr.mem4csd.analysis.executiontimebounds.impl.ExecutiontimeboundsPackageImpl#getTask()
	 * @generated
	 */
	int TASK = 0;

	/**
	 * The feature id for the '<em><b>Critical Sections</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__CRITICAL_SECTIONS = 0;

	/**
	 * The feature id for the '<em><b>Best Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__BEST_CASE_EXECUTION_TIME = 1;

	/**
	 * The feature id for the '<em><b>Worst Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__WORST_CASE_EXECUTION_TIME = 2;

	/**
	 * The feature id for the '<em><b>Aadl Thread Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__AADL_THREAD_INSTANCE = 3;

	/**
	 * The number of structural features of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.analysis.executiontimebounds.impl.CriticalSectionImpl <em>Critical Section</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.analysis.executiontimebounds.impl.CriticalSectionImpl
	 * @see fr.mem4csd.analysis.executiontimebounds.impl.ExecutiontimeboundsPackageImpl#getCriticalSection()
	 * @generated
	 */
	int CRITICAL_SECTION = 1;

	/**
	 * The feature id for the '<em><b>Best Case Critical Section Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CRITICAL_SECTION__BEST_CASE_CRITICAL_SECTION_TIME = 0;

	/**
	 * The feature id for the '<em><b>Worst Case Critical Section Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CRITICAL_SECTION__WORST_CASE_CRITICAL_SECTION_TIME = 1;

	/**
	 * The feature id for the '<em><b>Aadl Data Access</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CRITICAL_SECTION__AADL_DATA_ACCESS = 2;

	/**
	 * The number of structural features of the '<em>Critical Section</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CRITICAL_SECTION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Critical Section</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CRITICAL_SECTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.analysis.executiontimebounds.impl.TaskSetImpl <em>Task Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.analysis.executiontimebounds.impl.TaskSetImpl
	 * @see fr.mem4csd.analysis.executiontimebounds.impl.ExecutiontimeboundsPackageImpl#getTaskSet()
	 * @generated
	 */
	int TASK_SET = 2;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_SET__TASKS = 0;

	/**
	 * The number of structural features of the '<em>Task Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_SET_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Task Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_SET_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.analysis.executiontimebounds.Task <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task</em>'.
	 * @see fr.mem4csd.analysis.executiontimebounds.Task
	 * @generated
	 */
	EClass getTask();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.mem4csd.analysis.executiontimebounds.Task#getCriticalSections <em>Critical Sections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Critical Sections</em>'.
	 * @see fr.mem4csd.analysis.executiontimebounds.Task#getCriticalSections()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_CriticalSections();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.analysis.executiontimebounds.Task#getBestCaseExecutionTime <em>Best Case Execution Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Best Case Execution Time</em>'.
	 * @see fr.mem4csd.analysis.executiontimebounds.Task#getBestCaseExecutionTime()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_BestCaseExecutionTime();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.analysis.executiontimebounds.Task#getWorstCaseExecutionTime <em>Worst Case Execution Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst Case Execution Time</em>'.
	 * @see fr.mem4csd.analysis.executiontimebounds.Task#getWorstCaseExecutionTime()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_WorstCaseExecutionTime();

	/**
	 * Returns the meta object for the reference '{@link fr.mem4csd.analysis.executiontimebounds.Task#getAadlThreadInstance <em>Aadl Thread Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Aadl Thread Instance</em>'.
	 * @see fr.mem4csd.analysis.executiontimebounds.Task#getAadlThreadInstance()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_AadlThreadInstance();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.analysis.executiontimebounds.CriticalSection <em>Critical Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Critical Section</em>'.
	 * @see fr.mem4csd.analysis.executiontimebounds.CriticalSection
	 * @generated
	 */
	EClass getCriticalSection();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.analysis.executiontimebounds.CriticalSection#getBestCaseCriticalSectionTime <em>Best Case Critical Section Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Best Case Critical Section Time</em>'.
	 * @see fr.mem4csd.analysis.executiontimebounds.CriticalSection#getBestCaseCriticalSectionTime()
	 * @see #getCriticalSection()
	 * @generated
	 */
	EAttribute getCriticalSection_BestCaseCriticalSectionTime();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.analysis.executiontimebounds.CriticalSection#getWorstCaseCriticalSectionTime <em>Worst Case Critical Section Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst Case Critical Section Time</em>'.
	 * @see fr.mem4csd.analysis.executiontimebounds.CriticalSection#getWorstCaseCriticalSectionTime()
	 * @see #getCriticalSection()
	 * @generated
	 */
	EAttribute getCriticalSection_WorstCaseCriticalSectionTime();

	/**
	 * Returns the meta object for the reference '{@link fr.mem4csd.analysis.executiontimebounds.CriticalSection#getAadlDataAccess <em>Aadl Data Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Aadl Data Access</em>'.
	 * @see fr.mem4csd.analysis.executiontimebounds.CriticalSection#getAadlDataAccess()
	 * @see #getCriticalSection()
	 * @generated
	 */
	EReference getCriticalSection_AadlDataAccess();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.analysis.executiontimebounds.TaskSet <em>Task Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task Set</em>'.
	 * @see fr.mem4csd.analysis.executiontimebounds.TaskSet
	 * @generated
	 */
	EClass getTaskSet();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.mem4csd.analysis.executiontimebounds.TaskSet#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tasks</em>'.
	 * @see fr.mem4csd.analysis.executiontimebounds.TaskSet#getTasks()
	 * @see #getTaskSet()
	 * @generated
	 */
	EReference getTaskSet_Tasks();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ExecutiontimeboundsFactory getExecutiontimeboundsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.analysis.executiontimebounds.impl.TaskImpl <em>Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.analysis.executiontimebounds.impl.TaskImpl
		 * @see fr.mem4csd.analysis.executiontimebounds.impl.ExecutiontimeboundsPackageImpl#getTask()
		 * @generated
		 */
		EClass TASK = eINSTANCE.getTask();

		/**
		 * The meta object literal for the '<em><b>Critical Sections</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__CRITICAL_SECTIONS = eINSTANCE.getTask_CriticalSections();

		/**
		 * The meta object literal for the '<em><b>Best Case Execution Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__BEST_CASE_EXECUTION_TIME = eINSTANCE.getTask_BestCaseExecutionTime();

		/**
		 * The meta object literal for the '<em><b>Worst Case Execution Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__WORST_CASE_EXECUTION_TIME = eINSTANCE.getTask_WorstCaseExecutionTime();

		/**
		 * The meta object literal for the '<em><b>Aadl Thread Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__AADL_THREAD_INSTANCE = eINSTANCE.getTask_AadlThreadInstance();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.analysis.executiontimebounds.impl.CriticalSectionImpl <em>Critical Section</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.analysis.executiontimebounds.impl.CriticalSectionImpl
		 * @see fr.mem4csd.analysis.executiontimebounds.impl.ExecutiontimeboundsPackageImpl#getCriticalSection()
		 * @generated
		 */
		EClass CRITICAL_SECTION = eINSTANCE.getCriticalSection();

		/**
		 * The meta object literal for the '<em><b>Best Case Critical Section Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CRITICAL_SECTION__BEST_CASE_CRITICAL_SECTION_TIME = eINSTANCE.getCriticalSection_BestCaseCriticalSectionTime();

		/**
		 * The meta object literal for the '<em><b>Worst Case Critical Section Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CRITICAL_SECTION__WORST_CASE_CRITICAL_SECTION_TIME = eINSTANCE.getCriticalSection_WorstCaseCriticalSectionTime();

		/**
		 * The meta object literal for the '<em><b>Aadl Data Access</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CRITICAL_SECTION__AADL_DATA_ACCESS = eINSTANCE.getCriticalSection_AadlDataAccess();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.analysis.executiontimebounds.impl.TaskSetImpl <em>Task Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.analysis.executiontimebounds.impl.TaskSetImpl
		 * @see fr.mem4csd.analysis.executiontimebounds.impl.ExecutiontimeboundsPackageImpl#getTaskSet()
		 * @generated
		 */
		EClass TASK_SET = eINSTANCE.getTaskSet();

		/**
		 * The meta object literal for the '<em><b>Tasks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_SET__TASKS = eINSTANCE.getTaskSet_Tasks();

	}

} //ExecutiontimeboundsPackage
