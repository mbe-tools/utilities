/**
 */
package fr.mem4csd.analysis.executiontimebounds.impl;

import fr.mem4csd.analysis.executiontimebounds.CriticalSection;
import fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage;
import fr.mem4csd.analysis.executiontimebounds.Task;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.osate.aadl2.instance.ComponentInstance;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.impl.TaskImpl#getCriticalSections <em>Critical Sections</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.impl.TaskImpl#getBestCaseExecutionTime <em>Best Case Execution Time</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.impl.TaskImpl#getWorstCaseExecutionTime <em>Worst Case Execution Time</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.impl.TaskImpl#getAadlThreadInstance <em>Aadl Thread Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TaskImpl extends MinimalEObjectImpl.Container implements Task {
	/**
	 * The cached value of the '{@link #getCriticalSections() <em>Critical Sections</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCriticalSections()
	 * @generated
	 * @ordered
	 */
	protected EList<CriticalSection> criticalSections;

	/**
	 * The default value of the '{@link #getBestCaseExecutionTime() <em>Best Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBestCaseExecutionTime()
	 * @generated
	 * @ordered
	 */
	protected static final double BEST_CASE_EXECUTION_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getBestCaseExecutionTime() <em>Best Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBestCaseExecutionTime()
	 * @generated
	 * @ordered
	 */
	protected double bestCaseExecutionTime = BEST_CASE_EXECUTION_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorstCaseExecutionTime() <em>Worst Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorstCaseExecutionTime()
	 * @generated
	 * @ordered
	 */
	protected static final double WORST_CASE_EXECUTION_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getWorstCaseExecutionTime() <em>Worst Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorstCaseExecutionTime()
	 * @generated
	 * @ordered
	 */
	protected double worstCaseExecutionTime = WORST_CASE_EXECUTION_TIME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAadlThreadInstance() <em>Aadl Thread Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAadlThreadInstance()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance aadlThreadInstance;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutiontimeboundsPackage.Literals.TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CriticalSection> getCriticalSections() {
		if (criticalSections == null) {
			criticalSections = new EObjectContainmentEList<CriticalSection>(CriticalSection.class, this, ExecutiontimeboundsPackage.TASK__CRITICAL_SECTIONS);
		}
		return criticalSections;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getBestCaseExecutionTime() {
		return bestCaseExecutionTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBestCaseExecutionTime(double newBestCaseExecutionTime) {
		double oldBestCaseExecutionTime = bestCaseExecutionTime;
		bestCaseExecutionTime = newBestCaseExecutionTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExecutiontimeboundsPackage.TASK__BEST_CASE_EXECUTION_TIME, oldBestCaseExecutionTime, bestCaseExecutionTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getWorstCaseExecutionTime() {
		return worstCaseExecutionTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorstCaseExecutionTime(double newWorstCaseExecutionTime) {
		double oldWorstCaseExecutionTime = worstCaseExecutionTime;
		worstCaseExecutionTime = newWorstCaseExecutionTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExecutiontimeboundsPackage.TASK__WORST_CASE_EXECUTION_TIME, oldWorstCaseExecutionTime, worstCaseExecutionTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getAadlThreadInstance() {
		if (aadlThreadInstance != null && aadlThreadInstance.eIsProxy()) {
			InternalEObject oldAadlThreadInstance = (InternalEObject)aadlThreadInstance;
			aadlThreadInstance = (ComponentInstance)eResolveProxy(oldAadlThreadInstance);
			if (aadlThreadInstance != oldAadlThreadInstance) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExecutiontimeboundsPackage.TASK__AADL_THREAD_INSTANCE, oldAadlThreadInstance, aadlThreadInstance));
			}
		}
		return aadlThreadInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetAadlThreadInstance() {
		return aadlThreadInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAadlThreadInstance(ComponentInstance newAadlThreadInstance) {
		ComponentInstance oldAadlThreadInstance = aadlThreadInstance;
		aadlThreadInstance = newAadlThreadInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExecutiontimeboundsPackage.TASK__AADL_THREAD_INSTANCE, oldAadlThreadInstance, aadlThreadInstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ExecutiontimeboundsPackage.TASK__CRITICAL_SECTIONS:
				return ((InternalEList<?>)getCriticalSections()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ExecutiontimeboundsPackage.TASK__CRITICAL_SECTIONS:
				return getCriticalSections();
			case ExecutiontimeboundsPackage.TASK__BEST_CASE_EXECUTION_TIME:
				return getBestCaseExecutionTime();
			case ExecutiontimeboundsPackage.TASK__WORST_CASE_EXECUTION_TIME:
				return getWorstCaseExecutionTime();
			case ExecutiontimeboundsPackage.TASK__AADL_THREAD_INSTANCE:
				if (resolve) return getAadlThreadInstance();
				return basicGetAadlThreadInstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ExecutiontimeboundsPackage.TASK__CRITICAL_SECTIONS:
				getCriticalSections().clear();
				getCriticalSections().addAll((Collection<? extends CriticalSection>)newValue);
				return;
			case ExecutiontimeboundsPackage.TASK__BEST_CASE_EXECUTION_TIME:
				setBestCaseExecutionTime((Double)newValue);
				return;
			case ExecutiontimeboundsPackage.TASK__WORST_CASE_EXECUTION_TIME:
				setWorstCaseExecutionTime((Double)newValue);
				return;
			case ExecutiontimeboundsPackage.TASK__AADL_THREAD_INSTANCE:
				setAadlThreadInstance((ComponentInstance)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ExecutiontimeboundsPackage.TASK__CRITICAL_SECTIONS:
				getCriticalSections().clear();
				return;
			case ExecutiontimeboundsPackage.TASK__BEST_CASE_EXECUTION_TIME:
				setBestCaseExecutionTime(BEST_CASE_EXECUTION_TIME_EDEFAULT);
				return;
			case ExecutiontimeboundsPackage.TASK__WORST_CASE_EXECUTION_TIME:
				setWorstCaseExecutionTime(WORST_CASE_EXECUTION_TIME_EDEFAULT);
				return;
			case ExecutiontimeboundsPackage.TASK__AADL_THREAD_INSTANCE:
				setAadlThreadInstance((ComponentInstance)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ExecutiontimeboundsPackage.TASK__CRITICAL_SECTIONS:
				return criticalSections != null && !criticalSections.isEmpty();
			case ExecutiontimeboundsPackage.TASK__BEST_CASE_EXECUTION_TIME:
				return bestCaseExecutionTime != BEST_CASE_EXECUTION_TIME_EDEFAULT;
			case ExecutiontimeboundsPackage.TASK__WORST_CASE_EXECUTION_TIME:
				return worstCaseExecutionTime != WORST_CASE_EXECUTION_TIME_EDEFAULT;
			case ExecutiontimeboundsPackage.TASK__AADL_THREAD_INSTANCE:
				return aadlThreadInstance != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (bestCaseExecutionTime: ");
		result.append(bestCaseExecutionTime);
		result.append(", worstCaseExecutionTime: ");
		result.append(worstCaseExecutionTime);
		result.append(')');
		return result.toString();
	}

} //TaskImpl
