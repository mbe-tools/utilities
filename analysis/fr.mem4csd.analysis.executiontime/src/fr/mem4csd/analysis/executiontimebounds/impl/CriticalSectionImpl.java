/**
 */
package fr.mem4csd.analysis.executiontimebounds.impl;

import fr.mem4csd.analysis.executiontimebounds.CriticalSection;
import fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.osate.aadl2.instance.FeatureInstance;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Critical Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.impl.CriticalSectionImpl#getBestCaseCriticalSectionTime <em>Best Case Critical Section Time</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.impl.CriticalSectionImpl#getWorstCaseCriticalSectionTime <em>Worst Case Critical Section Time</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.impl.CriticalSectionImpl#getAadlDataAccess <em>Aadl Data Access</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CriticalSectionImpl extends MinimalEObjectImpl.Container implements CriticalSection {
	/**
	 * The default value of the '{@link #getBestCaseCriticalSectionTime() <em>Best Case Critical Section Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBestCaseCriticalSectionTime()
	 * @generated
	 * @ordered
	 */
	protected static final double BEST_CASE_CRITICAL_SECTION_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getBestCaseCriticalSectionTime() <em>Best Case Critical Section Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBestCaseCriticalSectionTime()
	 * @generated
	 * @ordered
	 */
	protected double bestCaseCriticalSectionTime = BEST_CASE_CRITICAL_SECTION_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorstCaseCriticalSectionTime() <em>Worst Case Critical Section Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorstCaseCriticalSectionTime()
	 * @generated
	 * @ordered
	 */
	protected static final double WORST_CASE_CRITICAL_SECTION_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getWorstCaseCriticalSectionTime() <em>Worst Case Critical Section Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorstCaseCriticalSectionTime()
	 * @generated
	 * @ordered
	 */
	protected double worstCaseCriticalSectionTime = WORST_CASE_CRITICAL_SECTION_TIME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAadlDataAccess() <em>Aadl Data Access</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAadlDataAccess()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance aadlDataAccess;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CriticalSectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutiontimeboundsPackage.Literals.CRITICAL_SECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getBestCaseCriticalSectionTime() {
		return bestCaseCriticalSectionTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBestCaseCriticalSectionTime(double newBestCaseCriticalSectionTime) {
		double oldBestCaseCriticalSectionTime = bestCaseCriticalSectionTime;
		bestCaseCriticalSectionTime = newBestCaseCriticalSectionTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExecutiontimeboundsPackage.CRITICAL_SECTION__BEST_CASE_CRITICAL_SECTION_TIME, oldBestCaseCriticalSectionTime, bestCaseCriticalSectionTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getWorstCaseCriticalSectionTime() {
		return worstCaseCriticalSectionTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorstCaseCriticalSectionTime(double newWorstCaseCriticalSectionTime) {
		double oldWorstCaseCriticalSectionTime = worstCaseCriticalSectionTime;
		worstCaseCriticalSectionTime = newWorstCaseCriticalSectionTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExecutiontimeboundsPackage.CRITICAL_SECTION__WORST_CASE_CRITICAL_SECTION_TIME, oldWorstCaseCriticalSectionTime, worstCaseCriticalSectionTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getAadlDataAccess() {
		if (aadlDataAccess != null && aadlDataAccess.eIsProxy()) {
			InternalEObject oldAadlDataAccess = (InternalEObject)aadlDataAccess;
			aadlDataAccess = (FeatureInstance)eResolveProxy(oldAadlDataAccess);
			if (aadlDataAccess != oldAadlDataAccess) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExecutiontimeboundsPackage.CRITICAL_SECTION__AADL_DATA_ACCESS, oldAadlDataAccess, aadlDataAccess));
			}
		}
		return aadlDataAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetAadlDataAccess() {
		return aadlDataAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAadlDataAccess(FeatureInstance newAadlDataAccess) {
		FeatureInstance oldAadlDataAccess = aadlDataAccess;
		aadlDataAccess = newAadlDataAccess;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExecutiontimeboundsPackage.CRITICAL_SECTION__AADL_DATA_ACCESS, oldAadlDataAccess, aadlDataAccess));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ExecutiontimeboundsPackage.CRITICAL_SECTION__BEST_CASE_CRITICAL_SECTION_TIME:
				return getBestCaseCriticalSectionTime();
			case ExecutiontimeboundsPackage.CRITICAL_SECTION__WORST_CASE_CRITICAL_SECTION_TIME:
				return getWorstCaseCriticalSectionTime();
			case ExecutiontimeboundsPackage.CRITICAL_SECTION__AADL_DATA_ACCESS:
				if (resolve) return getAadlDataAccess();
				return basicGetAadlDataAccess();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ExecutiontimeboundsPackage.CRITICAL_SECTION__BEST_CASE_CRITICAL_SECTION_TIME:
				setBestCaseCriticalSectionTime((Double)newValue);
				return;
			case ExecutiontimeboundsPackage.CRITICAL_SECTION__WORST_CASE_CRITICAL_SECTION_TIME:
				setWorstCaseCriticalSectionTime((Double)newValue);
				return;
			case ExecutiontimeboundsPackage.CRITICAL_SECTION__AADL_DATA_ACCESS:
				setAadlDataAccess((FeatureInstance)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ExecutiontimeboundsPackage.CRITICAL_SECTION__BEST_CASE_CRITICAL_SECTION_TIME:
				setBestCaseCriticalSectionTime(BEST_CASE_CRITICAL_SECTION_TIME_EDEFAULT);
				return;
			case ExecutiontimeboundsPackage.CRITICAL_SECTION__WORST_CASE_CRITICAL_SECTION_TIME:
				setWorstCaseCriticalSectionTime(WORST_CASE_CRITICAL_SECTION_TIME_EDEFAULT);
				return;
			case ExecutiontimeboundsPackage.CRITICAL_SECTION__AADL_DATA_ACCESS:
				setAadlDataAccess((FeatureInstance)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ExecutiontimeboundsPackage.CRITICAL_SECTION__BEST_CASE_CRITICAL_SECTION_TIME:
				return bestCaseCriticalSectionTime != BEST_CASE_CRITICAL_SECTION_TIME_EDEFAULT;
			case ExecutiontimeboundsPackage.CRITICAL_SECTION__WORST_CASE_CRITICAL_SECTION_TIME:
				return worstCaseCriticalSectionTime != WORST_CASE_CRITICAL_SECTION_TIME_EDEFAULT;
			case ExecutiontimeboundsPackage.CRITICAL_SECTION__AADL_DATA_ACCESS:
				return aadlDataAccess != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (bestCaseCriticalSectionTime: ");
		result.append(bestCaseCriticalSectionTime);
		result.append(", worstCaseCriticalSectionTime: ");
		result.append(worstCaseCriticalSectionTime);
		result.append(')');
		return result.toString();
	}

} //CriticalSectionImpl
