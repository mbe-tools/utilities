/**
 */
package fr.mem4csd.analysis.executiontimebounds.impl;

import fr.mem4csd.analysis.executiontimebounds.*;
import fr.mem4csd.analysis.executiontimebounds.CriticalSection;
import fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsFactory;
import fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage;
import fr.mem4csd.analysis.executiontimebounds.Task;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExecutiontimeboundsFactoryImpl extends EFactoryImpl implements ExecutiontimeboundsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ExecutiontimeboundsFactory init() {
		try {
			ExecutiontimeboundsFactory theExecutiontimeboundsFactory = (ExecutiontimeboundsFactory)EPackage.Registry.INSTANCE.getEFactory(ExecutiontimeboundsPackage.eNS_URI);
			if (theExecutiontimeboundsFactory != null) {
				return theExecutiontimeboundsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ExecutiontimeboundsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutiontimeboundsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ExecutiontimeboundsPackage.TASK: return createTask();
			case ExecutiontimeboundsPackage.CRITICAL_SECTION: return createCriticalSection();
			case ExecutiontimeboundsPackage.TASK_SET: return createTaskSet();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Task createTask() {
		TaskImpl task = new TaskImpl();
		return task;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CriticalSection createCriticalSection() {
		CriticalSectionImpl criticalSection = new CriticalSectionImpl();
		return criticalSection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskSet createTaskSet() {
		TaskSetImpl taskSet = new TaskSetImpl();
		return taskSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutiontimeboundsPackage getExecutiontimeboundsPackage() {
		return (ExecutiontimeboundsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ExecutiontimeboundsPackage getPackage() {
		return ExecutiontimeboundsPackage.eINSTANCE;
	}

} //ExecutiontimeboundsFactoryImpl
