package fr.mem4csd.analysis.executiontimebounds.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jgrapht.GraphPath;
import org.jgrapht.Graphs;
import org.jgrapht.alg.cycle.JohnsonSimpleCycles;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.ba.aadlba.LockAction;
import org.osate.ba.aadlba.SharedDataAction;

import fr.mem4csd.analysis.executiontimebounds.CriticalSection;
import fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsFactory;
import fr.mem4csd.analysis.executiontimebounds.Task;
import fr.mem4csd.analysis.executiontimebounds.TaskSet;
import fr.mem4csd.analysis.executiontimebounds.util.LongestPathWeightAdaptor.E_MODE;
import fr.mem4csd.analysis.executiontimegraph.model.ExecutionEdge;
import fr.mem4csd.analysis.executiontimegraph.model.ExecutionGraph;
import fr.mem4csd.analysis.executiontimegraph.model.ExecutionVertex;
import fr.mem4csd.analysis.executiontimegraph.utils.DoubleRange;

public class ExecGraphToExecutionTimeBounds {
	
	private static final ExecutiontimeboundsFactory _fact = ExecutiontimeboundsFactory.eINSTANCE;
	
	public static TaskSet createModelElement(List<ExecutionGraph> execGraphList)
	{
		TaskSet result = _fact.createTaskSet();
		for(ExecutionGraph execGraph: execGraphList)
		{
			
			// Check for circuits of positive weight
			
			// Adaptor configuration
			// compute maxWeight for longest path computation ("rescaling");
			// set adaptor
			Double maxWeight = 0.0;
			LongestPathWeightAdaptor adaptor = new LongestPathWeightAdaptor(E_MODE.COMPUTE_SHORTEST);
			Iterator<ExecutionEdge> itEdge = execGraph.edgeSet().iterator();
			while(itEdge.hasNext()){
				ExecutionEdge edge = itEdge.next();
				edge.setAdaptor(adaptor);
				if(edge.getExecTime()!=null)
					maxWeight = Math.max(maxWeight, edge.getExecTime().getMax());
			}
			adaptor.setMaxWeight(maxWeight);
			// End of Adaptor configuration

			adaptor.setMode(E_MODE.RETREIVE_LONGEST);
			JohnsonSimpleCycles cyclesFinder = new JohnsonSimpleCycles(execGraph);
			List<List<ExecutionVertex>> cycleList = cyclesFinder.findSimpleCycles();
			
			List<List<ExecutionVertex>> unboundedCycles = new ArrayList<List<ExecutionVertex>>();
			
			for(List<ExecutionVertex> vertexList: cycleList)
			{
				boolean positive = false;
				// if >= 1 "complete state", it's ok
				boolean hasCompleteState = false;
				for(int i=0;i<vertexList.size();i++)
				{
					ExecutionVertex source = vertexList.get(i);
					ExecutionVertex target = vertexList.get((i+1)%vertexList.size());
					ExecutionEdge e = execGraph.getEdge(source, target);
					if(e.getWeight()>0.0)
						positive = true;
					if(execGraph.getEntryNodes().contains(source) || execGraph.getEntryNodes().contains(target))
					{
						hasCompleteState = true;
						break;
					}
				}
				if(positive && !hasCompleteState)
					unboundedCycles.add(vertexList);
			}
			
			if(unboundedCycles.size() > 0)
			{
				java.lang.System.out.println("Found elementary cycles with positive execution time:");
				for(List<ExecutionVertex> vertexList: unboundedCycles)
				{
					String cycle = "\t";
					for(ExecutionVertex v: vertexList)
						cycle+=" "+v.toString();
					java.lang.System.out.println(cycle);
				}
				throw new UnsupportedOperationException("Found a cycle of positive time, execution time cannot be bound");
			}
				
			
			
			Task threadModel = createModelElement(execGraph, adaptor);
			threadModel.setAadlThreadInstance((ComponentInstance) execGraph.getRootElement());
			result.getTasks().add(threadModel);
		}
		return result;
	}
	
	private static DoubleRange getExecutionRange(ExecutionGraph execGraph,
			ExecutionVertex source,
			ExecutionVertex target,
			LongestPathWeightAdaptor adaptor)
	{
		
		adaptor.setMode(E_MODE.COMPUTE_SHORTEST);
		
		GraphPath<ExecutionVertex, ExecutionEdge> path = 
				computeShortestPath(execGraph, source, target);
		
		if(path == null)
			return null;
		
		double executionTimeLowerBound = path.getWeight();
		
		adaptor.setMode(E_MODE.COMPUTE_LONGEST);
		
		path = 
				computeShortestPath(execGraph, source, target);
		
		if(path==null)
			return null;
		
		adaptor.setMode(E_MODE.RETREIVE_LONGEST);
		double executionTimeUpperBound = 0.0;
		for(ExecutionEdge e: path.getEdgeList())
		{
			executionTimeUpperBound += e.getWeight();
		}
				
		return new DoubleRange(executionTimeLowerBound, executionTimeUpperBound);
	}
	
	private static Task createModelElement(ExecutionGraph execGraph, LongestPathWeightAdaptor adaptor)
	{
		Task result = _fact.createTask();		
		// get BCET and WCET
		double executionTimeLowerBound = Double.POSITIVE_INFINITY;
		double executionTimeUpperBound = Double.NEGATIVE_INFINITY;
		
		Iterator<ExecutionVertex> itEntry = execGraph.getEntryNodes().iterator();
		while(itEntry.hasNext()){
			ExecutionVertex entry = itEntry.next();
			Iterator<ExecutionVertex> itExit = execGraph.getExitNodes().iterator();
			while(itExit.hasNext()){
				ExecutionVertex exit = itExit.next();
				
				DoubleRange execRange = getExecutionRange(execGraph, entry, exit, adaptor);
				if(execRange==null)
					continue;
				executionTimeLowerBound = Math.min(execRange.getMin(), executionTimeLowerBound);
				executionTimeUpperBound = Math.max(execRange.getMax(), executionTimeUpperBound);
			}
		}
		
		result.setBestCaseExecutionTime(executionTimeLowerBound);
		result.setWorstCaseExecutionTime(executionTimeUpperBound);
		
		
		// Get shortest and longest paths to lock/unlock actions
		CriticalSectionsTimeBounds elementBounds = new CriticalSectionsTimeBounds();
		Iterator<ExecutionVertex> itLock = execGraph.getLockNodes().iterator();
		while(itLock.hasNext()){
			ExecutionVertex lock = itLock.next();
			Iterator<ExecutionVertex> itUnlock = execGraph.getUnlockNodes().iterator();
			while(itUnlock.hasNext()){
				ExecutionVertex unlock = itUnlock.next();
				
				DoubleRange execRangeToLock = getExecutionRange(execGraph, lock, unlock, adaptor);
				if(elementBounds.criticalSectionMaxDuration.containsKey(lock))
				{
					double currentMaxDurationValue = elementBounds.criticalSectionMaxDuration.get(lock);
					double currentMinDurationValue = elementBounds.criticalSectionMinDuration.get(lock);
					
					currentMinDurationValue = Math.min(currentMinDurationValue, execRangeToLock.getMin());
					currentMaxDurationValue = Math.max(currentMaxDurationValue, execRangeToLock.getMax());
					
					elementBounds.criticalSectionMinDuration.put(lock,currentMinDurationValue);
					elementBounds.criticalSectionMaxDuration.put(lock,currentMaxDurationValue);
				}
				else
				{
					elementBounds.criticalSectionMinDuration.put(lock,execRangeToLock.getMin());
					elementBounds.criticalSectionMaxDuration.put(lock,execRangeToLock.getMax());
				}
			}
		}
		
		for(ExecutionVertex cs: elementBounds.criticalSectionMaxDuration.keySet())
		{
			CriticalSection critSection = _fact.createCriticalSection();
			FeatureInstance fi = (FeatureInstance) cs.getFeatureInstance();
			critSection.setAadlDataAccess(fi);
			
			critSection.setBestCaseCriticalSectionTime(elementBounds.criticalSectionMinDuration.get(cs));
			critSection.setWorstCaseCriticalSectionTime(elementBounds.criticalSectionMaxDuration.get(cs));
			result.getCriticalSections().add(critSection);
		}
		
		return result;
	}

	private static GraphPath<ExecutionVertex, ExecutionEdge> computeShortestPath(ExecutionGraph execGraph,
			ExecutionVertex source, ExecutionVertex target) {
		
		GraphPath<ExecutionVertex, ExecutionEdge> shortestPath=null;
		double shortestPathLength = Double.POSITIVE_INFINITY;
		if(source==target)
		{
			
			// get predecessors
			List<ExecutionVertex> predecessorList = Graphs.predecessorListOf(execGraph, target);
			for(ExecutionVertex pred: predecessorList)
			{
				ExecutionEdge incomingEdge = execGraph.getEdge(pred, target);
				
				GraphPath<ExecutionVertex, ExecutionEdge> path = 
						computeShortestPath(execGraph, source, pred);
				
				if(path==null)
					continue;
				
				double pathLength = path.getWeight()+incomingEdge.getWeight();
				if(pathLength<shortestPathLength)
				{
					shortestPathLength = pathLength;
					path.getEdgeList().add(incomingEdge);
					path.getVertexList().add(target);
					shortestPath = path;
				}
			}
		}
		else
		{
			// update weight of other edges in entry/exit nodes to avoid them in paths computation.
			// return original weight for recovery
			Map<ExecutionEdge, DoubleRange> originalExecTimeMap = discardOtherEntryOrExitVertices(execGraph, source, target);

			Map<ExecutionEdge, DoubleRange> originalExecTimeMap2 = null;
			if(source.getAadlElement() instanceof LockAction)
				originalExecTimeMap2 = discardIntermediateLockVertices(execGraph, source);
			
			
			shortestPath = 
					DijkstraShortestPath.findPathBetween(execGraph, source, target);

			// reset original weights
			
			for(ExecutionEdge edge: originalExecTimeMap.keySet())
			{
				edge.setExecTime(originalExecTimeMap.get(edge));
			}
			if(originalExecTimeMap2!=null)
			{
				for(ExecutionEdge edge: originalExecTimeMap2.keySet())
				{
					edge.setExecTime(originalExecTimeMap2.get(edge));
				}
			}
			if(shortestPath ==null)
			{
				java.lang.System.out.println("No path found from "+source.toString()+" to "+target.toString());
				return null;
			}
		}
		if(shortestPath!=null)
			java.lang.System.out.println("Shortest path length: "+shortestPath.getWeight());
		
		return shortestPath;
	}

	private static Map<ExecutionEdge, DoubleRange> discardIntermediateLockVertices(ExecutionGraph execGraph,
			ExecutionVertex source) {
		Map<ExecutionEdge, DoubleRange> originalWeightMap = new HashMap<ExecutionEdge, DoubleRange>();
		
		// select lock vertices that are not source
		for(ExecutionVertex lockVertex: execGraph.getLockNodes())
		{
			SharedDataAction la = (SharedDataAction) lockVertex.getAadlElement();
			SharedDataAction la2 = (SharedDataAction) source.getAadlElement();
			if(la.getDataAccess().getDataAccess().equals(la2.getDataAccess().getDataAccess()))
			{
				List<ExecutionVertex> predecessorList = Graphs.predecessorListOf(execGraph, lockVertex);
				for(ExecutionVertex pred: predecessorList)
				{
					// register incoming edges original weight
					ExecutionEdge incomingEdge = execGraph.getEdge(pred, lockVertex);
					originalWeightMap.put(incomingEdge, incomingEdge.getExecTime());
					
					// change weight of incoming edges to positive infinity
					DoubleRange infinitPositiveExecTime = new DoubleRange(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY); 
					incomingEdge.setExecTime(infinitPositiveExecTime);
				}
			}
		}
		
		return originalWeightMap;
	}

	private static Map<ExecutionEdge, DoubleRange> discardOtherEntryOrExitVertices(ExecutionGraph execGraph,
			ExecutionVertex source, ExecutionVertex target) {
		
		Map<ExecutionEdge, DoubleRange> originalWeightMap = new HashMap<ExecutionEdge, DoubleRange>();
		
		// select entry/exit vertices that are not source or target
		
		Set<ExecutionVertex> entryOrExitVertices = new HashSet<ExecutionVertex>();
		entryOrExitVertices.addAll(execGraph.getEntryNodes());
		entryOrExitVertices.addAll(execGraph.getExitNodes());
		
		for(ExecutionVertex v: entryOrExitVertices)
		{
			if(v.equals(source) || v.equals(target))
				continue;
			// Take their incoming edges
			// get predecessors
			List<ExecutionVertex> predecessorList = Graphs.predecessorListOf(execGraph, v);
			for(ExecutionVertex pred: predecessorList)
			{
				// register incoming edges original weight
				ExecutionEdge incomingEdge = execGraph.getEdge(pred, v);
				originalWeightMap.put(incomingEdge, incomingEdge.getExecTime());
				
				// change weight of incoming edges to positive infinity
				DoubleRange infinitPositiveExecTime = new DoubleRange(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY); 
				incomingEdge.setExecTime(infinitPositiveExecTime);
			}
		}
		
		return originalWeightMap;
	}
	
	private static class CriticalSectionsTimeBounds
	{
		Map<ExecutionVertex, Double> criticalSectionMinDuration = new HashMap<ExecutionVertex, Double>();
		Map<ExecutionVertex, Double> criticalSectionMaxDuration = new HashMap<ExecutionVertex, Double>();
	}
	
}
