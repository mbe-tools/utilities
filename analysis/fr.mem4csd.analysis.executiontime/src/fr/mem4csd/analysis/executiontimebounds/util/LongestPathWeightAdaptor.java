package fr.mem4csd.analysis.executiontimebounds.util;

import fr.mem4csd.analysis.executiontimegraph.model.WeightAdaptor;
import fr.mem4csd.analysis.executiontimegraph.utils.DoubleRange;

public class LongestPathWeightAdaptor implements WeightAdaptor {

	public LongestPathWeightAdaptor(E_MODE mode) {
		this.mode = mode;
	}

	public enum E_MODE
	{
		COMPUTE_SHORTEST,
		COMPUTE_LONGEST, 
		RETREIVE_LONGEST
	}
	
	private E_MODE mode;

	public void setMode(E_MODE mode) {
		this.mode = mode;
	}

	private double maxWeight;
	
	public void setMaxWeight(double maxWeight) {
		this.maxWeight = maxWeight;
	}

	@Override
	public double getWeight(DoubleRange execTime)
	{
		if(execTime.getMax()==0.0)
			return 0.0;
		if(E_MODE.COMPUTE_LONGEST.equals(mode))
		{
			if(execTime.getMax()==Double.POSITIVE_INFINITY)
				return Double.POSITIVE_INFINITY;
			return maxWeight - execTime.getMax() + 1.0;
		}
		if(E_MODE.RETREIVE_LONGEST.equals(mode))
			return execTime.getMax();
		return execTime.getMin();
	}
	
}
