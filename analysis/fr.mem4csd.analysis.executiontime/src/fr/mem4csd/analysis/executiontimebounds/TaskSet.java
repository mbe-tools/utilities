/**
 */
package fr.mem4csd.analysis.executiontimebounds;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.analysis.executiontimebounds.TaskSet#getTasks <em>Tasks</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage#getTaskSet()
 * @model
 * @generated
 */
public interface TaskSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Tasks</b></em>' containment reference list.
	 * The list contents are of type {@link fr.mem4csd.analysis.executiontimebounds.Task}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tasks</em>' containment reference list.
	 * @see fr.mem4csd.analysis.executiontimebounds.ExecutiontimeboundsPackage#getTaskSet_Tasks()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Task> getTasks();

} // TaskSet
