package fr.mem4csd.analysis.executiontimegraph.utils.ba;

import org.osate.aadl2.Aadl2Factory;
import org.osate.aadl2.IntegerLiteral;
import org.osate.ba.aadlba.BehaviorTime;

public class BehaviorTimeUtils {

	public static double getScaledValue(BehaviorTime bt, String targetUnit)
	{
		IntegerLiteral il = Aadl2Factory.eINSTANCE.createIntegerLiteral();
		if(bt.getIntegerValue() instanceof IntegerLiteral)
		{
			IntegerLiteral btIntegerLiteral = (IntegerLiteral) bt.getIntegerValue();
			il.setValue(btIntegerLiteral.getValue());
		}
		il.setUnit(bt.getUnit());
		return il.getScaledValue(targetUnit);
	}
	
}
