/**
 * MEM4CSD-Utilities
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.analysis.executiontimegraph.utils;

import java.util.ArrayList ;
import java.util.Collections ;
import java.util.List ;

import org.apache.log4j.Logger ;
import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.AnnexSubclause ;
import org.osate.aadl2.BehavioredImplementation ;
import org.osate.aadl2.CalledSubprogram ;
import org.osate.aadl2.Classifier ;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.ContainedNamedElement;
import org.osate.aadl2.ContainmentPathElement;
import org.osate.aadl2.DefaultAnnexSubclause;
import org.osate.aadl2.IntegerLiteral ;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.NamedElement ;
import org.osate.aadl2.ProcessorClassifier;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.RangeValue ;
import org.osate.aadl2.Subcomponent ;
import org.osate.aadl2.SubprogramCall ;
import org.osate.aadl2.SubprogramCallSequence ;
import org.osate.aadl2.SubprogramClassifier ;
import org.osate.aadl2.instance.ComponentInstance ;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.ba.aadlba.BehaviorAction ;
import org.osate.ba.aadlba.BehaviorActionSequence ;
import org.osate.ba.aadlba.BehaviorActions ;
import org.osate.ba.aadlba.BehaviorAnnex ;
import org.osate.ba.aadlba.SubprogramCallAction ;
import org.osate.utils.internal.PropertyUtils ;
import org.osate.xtext.aadl2.properties.util.AadlProject;

public class ExecutionGraphUtils {

	private static Logger _LOGGER = Logger.getLogger(ExecutionGraphUtils.class);

	private static final String CET_PROPERTY = "Compute_Execution_Time";

	
	// TODO NEXT is to be contributed to PropertyUtils 
	private static List<PropertyAssociation> isInAppliesTo(NamedElement owner,
			String propertyName)
	{
		List<PropertyAssociation> result = new ArrayList<PropertyAssociation>();
		
		EObject parent = owner.eContainer() ;
		String ownerName = owner.getName() ;

		while(parent != null)
		{
			if(parent instanceof NamedElement)
			{
				List<PropertyAssociation> pas = ((NamedElement)parent).
						getOwnedPropertyAssociations() ;

				for(PropertyAssociation pa: pas)
				{
					String propName = pa.getProperty().getName() ;

					if(propName != null && propName.equalsIgnoreCase(propertyName))
					{
						for (ContainedNamedElement cne: pa.getAppliesTos())
						{
							List<ContainmentPathElement> paths = cne.getContainmentPathElements() ;
							ContainmentPathElement lastEl = paths.get(paths.size()-1) ;
							String lastElName = lastEl.getNamedElement().getName() ;
							if(lastElName.equalsIgnoreCase(ownerName))
							{
								result.add(pa) ;
							}
						}
					}
				}
			}

			parent = parent.eContainer() ;
		}

		return result ;
	}

	// TODO NEXT is to be contributed to PropertyUtils 
	private static List<PropertyAssociation> findPropertyAssociation(String propertyName,
			NamedElement owner)
	{
		List<PropertyAssociation> result = new ArrayList<PropertyAssociation>();
		
		// 1. Look within the owner.    
		for (PropertyAssociation pa : owner.getOwnedPropertyAssociations())
		{
			// Sometime property doesn't have name.
			if (pa.getProperty().getName() == null)
			{
				continue;
			}

			if (pa.getProperty().getName().equalsIgnoreCase(propertyName))
			{
				result.add(pa);
			}
		}

		// 2. Look within parent containers if they defined an property that applies
		// the given property.
		if(result.isEmpty())
			result.addAll(isInAppliesTo(owner, propertyName)) ;

		if(result.isEmpty())
		{
			List<PropertyAssociation> pas = new ArrayList<PropertyAssociation>() ;

			// 3. If the named element is a component instance, look within 
			// its associated component implementation.

			if (owner instanceof ComponentInstance)
			{
				ComponentInstance inst = (ComponentInstance) owner;
				ComponentImplementation ci = inst.getContainingComponentImpl();
				if(ci != null)
				{
					owner = ci ;
				}
			}

			// 4. If the named element is a component implementation, look within it.
			//    and its interface. 

			if(owner instanceof ComponentImplementation)
			{
				ComponentImplementation ci = (ComponentImplementation) owner ;
				pas.addAll(ci.getAllPropertyAssociations()) ;
				owner = ci.getType() ;
			}

			// 5. If the named element is a component type, look within it. 

			if(owner instanceof ComponentType)
			{
				ComponentType type = (ComponentType) owner;
				pas.addAll(type.getAllPropertyAssociations()) ;
			}

			if(false == pas.isEmpty())
			{
				// The first property association found represents the latest definition
				// of the given property.
				for(PropertyAssociation pa : pas)
				{
					Property p = pa.getProperty() ;
					// Sometime, properties don't have name.
					if(p.getName() != null && p.getName().equalsIgnoreCase(propertyName))
					{
						result.add(pa) ;
					}
				}
			}
		}

		return result ;
	}

	private static RangeValue getRangeValue(NamedElement i, String propertyName, List<ProcessorClassifier> boundToList) {
		List<PropertyAssociation> paList = findPropertyAssociation(propertyName, i);
		paList:
		for(PropertyAssociation pa:paList)
		{
			if (pa != null && pa.getAppliesTos().isEmpty()) {
				Property p = pa.getProperty();

				List<Classifier> inBindingList = pa.getInBindings();

				if(!inBindingList.isEmpty())
				{
					// make sure the timed action is valid for all the processors we are bound to!
					for(ProcessorClassifier bound: boundToList)
					{
						if(!inBindingList.contains(bound))
							continue paList;
					}
				}

				if (p.getName().equalsIgnoreCase(propertyName)) {
					List<ModalPropertyValue> values = pa.getOwnedValues();

					if (values.size() == 1) {
						ModalPropertyValue v = values.get(0);
						PropertyExpression expr = v.getOwnedValue();

						if (expr instanceof RangeValue) {
							return (RangeValue) expr;
						}
					}
				}
			}
		}
		return null;
	}

	public static DoubleRange getComputeExecutionTimeInMs(NamedElement e, ComponentInstance context)
	{
		List<ComponentInstance> boundToInstanceList = getBinding(context, ComponentCategory.PROCESSOR);
		List<ProcessorClassifier> boundToCategoryList = new ArrayList<ProcessorClassifier>();
		for(ComponentInstance boundToInstance: boundToInstanceList)
		{
			boundToCategoryList.add((ProcessorClassifier) boundToInstance.getSubcomponent().getSubcomponentType());
		}
		RangeValue rv = getRangeValue(e, CET_PROPERTY, boundToCategoryList);
		if (rv != null)
		{
			IntegerLiteral min = (IntegerLiteral) rv.getMinimum();
			IntegerLiteral max = (IntegerLiteral) rv.getMaximum();
			double bcet = min.getScaledValue(AadlProject.MS_LITERAL);
			double wcet = max.getScaledValue(AadlProject.MS_LITERAL);
			DoubleRange r = new DoubleRange (bcet, wcet);
			return r;
		}
		else
		{
			if(e instanceof Classifier)
			{
				Classifier c = (Classifier) e;
				if(c.getExtended() != null)
					return getComputeExecutionTimeInMs(c.getExtended(), context);
			}
			else if(e instanceof SubprogramCall)
			{
				SubprogramCall sCall = (SubprogramCall) e;
				if(sCall.getCalledSubprogram() instanceof NamedElement)
					return getComputeExecutionTimeInMs((NamedElement) sCall.getCalledSubprogram(), context);
			}
		}
		return null;
	}

	public static SubprogramClassifier getSubprogramClassifier (
			SubprogramCallAction action)
	{
		NamedElement spg = action.getSubprogram().getElement(); 
		if (spg instanceof SubprogramClassifier)
		{
			return (SubprogramClassifier) spg;
		}
		else
		{
			String msg = '\'' + spg.getClass().getSimpleName() +
					"\' is not supported" ; 
			_LOGGER.fatal(msg);
			throw new UnsupportedOperationException(msg);
		}
	}

	public static BehaviorAnnex getBehaviorAnnex(NamedElement e)
	{
		if(e instanceof ComponentInstance)
		{
			return getBehaviorAnnex(((ComponentInstance) e).getSubcomponent()) ;
		}
		else if(e instanceof Subcomponent)
		{
			return getBehaviorAnnex(((Subcomponent) e).getClassifier()) ;
		}
		else if (e instanceof SubprogramCall)
		{
			CalledSubprogram called = ((SubprogramCall) e).getCalledSubprogram();
			if (called instanceof SubprogramClassifier)
			{
				return getBehaviorAnnex ((SubprogramClassifier) called);
			}
			else
			{
				String msg = '\'' + called.getClass().getSimpleName() +
						"\' is not supported" ; 
				_LOGGER.fatal(msg);
				throw new UnsupportedOperationException(msg);
			}
		}
		else if(e instanceof Classifier)
		{
			Classifier c = (Classifier) e ;

			while (c != null)
			{
				for(AnnexSubclause annex : c.getOwnedAnnexSubclauses())
				{
					if(annex instanceof BehaviorAnnex)
					{
						return (BehaviorAnnex) annex ;
					}
					else if(annex instanceof DefaultAnnexSubclause)
					{
						DefaultAnnexSubclause das = (DefaultAnnexSubclause) annex;
						return (BehaviorAnnex) das.getParsedAnnexSubclause();
					}
				}

				c = c.getExtended();
			}

			if (e instanceof BehavioredImplementation)
			{
				BehavioredImplementation bi = (BehavioredImplementation) e;
				return getBehaviorAnnex (bi.getType());
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null ;
		}
	}

	public static List<BehaviorAction> getBehaviorActions(BehaviorActions actions)
	{
		if (actions == null)
		{
			return Collections.emptyList();
		}
		if (actions instanceof BehaviorActionSequence)
		{
			return ((BehaviorActionSequence) actions).getActions();
		}
		else if (actions instanceof BehaviorAction)
		{
			List<BehaviorAction> l = new ArrayList<BehaviorAction>();
			l.add((BehaviorAction) actions);
			return l;
		}
		else
		{
			String msg = 
					"not supported BehaviorActions kind \'" +
							actions.getClass().getSimpleName() + '\'' ;
			_LOGGER.error(msg) ;
			return Collections.emptyList();
		}
	}

	public static List<SubprogramCall> getCallSequence(NamedElement e)
	{
		if (e instanceof ComponentInstance)
		{
			ComponentInstance c = (ComponentInstance) e;
			return getCallSequence(c.getComponentClassifier());
		}
		else if (e instanceof BehavioredImplementation)
		{
			BehavioredImplementation bi = (BehavioredImplementation) e;
			SubprogramCallSequence seq = getMainCallSequence (bi);
			List<SubprogramCall> specs = (seq==null?null:seq.getOwnedSubprogramCalls());

			if (specs != null)
			{
				List<SubprogramCall> list = new ArrayList<SubprogramCall>();
				for(SubprogramCall cs : specs)
				{
					if (cs instanceof SubprogramCall)
					{
						list.add((SubprogramCall) cs);
					}
				}
				return list;
			}
		}
		return null;
	}

	private static SubprogramCallSequence getMainCallSequence(BehavioredImplementation bi)
	{
		List<SubprogramCallSequence> sequences = bi.getOwnedSubprogramCallSequences();
		for(SubprogramCallSequence s : sequences)
		{
			if (!s.getName().toLowerCase().contains("init"))
			{
				return s;
			}
		}
		return (sequences.isEmpty() ? null : sequences.get(0));

	}

	private static List<ComponentInstance> getBindingPropertyValue(ComponentInstance ci, String property)
	{
		List<ComponentInstance> result = new ArrayList<ComponentInstance>();
		PropertyExpression pex = PropertyUtils.getPropertyValue(property,
				ci) ;
		if(pex != null)
		{
			ListValue lv = (ListValue) pex ;
			for(PropertyExpression pe : lv.getOwnedListElements())
			{
				if(pe instanceof InstanceReferenceValue)
				{
					InstanceReferenceValue irv = (InstanceReferenceValue) pe ;
					result.add((ComponentInstance) irv.getReferencedInstanceObject()) ;
				}
			}
		}
		return result;
	}

	public static List<ComponentInstance> getBinding(ComponentInstance ci, ComponentCategory cat)
	{
		List<ComponentInstance> result = new ArrayList<ComponentInstance>();
		String property;
		if(cat.equals(ComponentCategory.PROCESSOR))
			property = "Actual_Processor_Binding";
		else if(cat.equals(ComponentCategory.MEMORY))
			property = "Actual_Memory_Binding";
		else
			return result;
		List<ComponentInstance> boundFromPropertyValue = getBindingPropertyValue(ci, property);
		for(ComponentInstance boundResource: boundFromPropertyValue)
		{
			if(boundResource.getCategory().equals(cat))
				result.add(boundResource);
			else
			{
				List<ComponentInstance> nextBinding = getBinding(boundResource, cat);
				if(!nextBinding.isEmpty())
					result.addAll(nextBinding);
				else
				{
					if(boundResource.eContainer() instanceof ComponentInstance)
					{
						ComponentInstance container = (ComponentInstance) boundResource.eContainer();
						if(container.getCategory().equals(cat))
							result.add(container);
						else
							result.addAll(getBinding(container, cat));
					}

				}
			}
		}

		return result;
	}

}