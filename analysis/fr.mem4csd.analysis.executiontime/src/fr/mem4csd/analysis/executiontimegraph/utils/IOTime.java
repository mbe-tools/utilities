package fr.mem4csd.analysis.executiontimegraph.utils;

import java.util.List;

import org.apache.log4j.Logger;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.IntegerLiteral;
import org.osate.aadl2.NumberValue;
import org.osate.aadl2.RangeValue;
import org.osate.aadl2.RecordValue;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.utils.internal.PropertyUtils;
import org.osate.xtext.aadl2.properties.util.AadlProject;

public class IOTime {


	private static Logger _LOGGER = Logger.getLogger(IOTime.class) ;

	// TODO : set time unit dynamically
	private static String timeBaseUnit = AadlProject.MS_LITERAL;

	public final DoubleRange Fixed;
	public final DoubleRange PerByte;

	public IOTime(DoubleRange Fixed, DoubleRange PerByte)
	{
		this.Fixed = Fixed;
		this.PerByte = PerByte;
	}

	public IOTime()
	{
		this(new DoubleRange(0D,0D),new DoubleRange(0D,0D));
	}

	public boolean isNull()
	{
		return Fixed.getMax()==0d && PerByte.getMax()==0d;
	}

	public DoubleRange getTime(long bytes)
	{
		double min = Fixed.getMin() + (PerByte.getMin() * bytes);
		double max = Fixed.getMax() + (PerByte.getMax() * bytes);
		return new DoubleRange(min, max);
	}

	private static IOTime getAssignTime(ComponentInstance c)
	{
		return getIOTime(c,"Assign_Time");
	}

	private static IOTime getReadTime(ComponentInstance c)
	{
		return getIOTime(c,"Read_Time");
	}

	private static IOTime getWriteTime(ComponentInstance c)
	{
		return getIOTime(c,"Write_Time");
	}

	public static DoubleRange getAssignTime(ComponentInstance c, Long dataSize) {
		IOTime assignTime = IOTime.getAssignTime(c);
		return assignTime.getTime(dataSize);
	}

	public static DoubleRange getWriteTime(ComponentInstance c, Long dataSize) {
		IOTime assignTime = IOTime.getWriteTime(c);
		return assignTime.getTime(dataSize);
	}

	public static DoubleRange getReadTime(ComponentInstance c, Long dataSize) {
		IOTime readTime = IOTime.getReadTime(c);
		return readTime.getTime(dataSize);
	}

	private static IOTime getIOTime(ComponentInstance c, String property)
	{
		switch (c.getCategory())
		{
		case THREAD: return getIOTimeThread(c, property);
		case PROCESS: return getIOTimeProcess(c, property);
		default:
		{
			IOTime result =  getIOTimeFromProperty(c,property);
			if(result != null)
			{
				return result ;
			}
			else
			{
				String msg = "cannot getIOTime for\'"+ c.getName() + '\'' ;
				_LOGGER.warn(msg) ;
				return new IOTime();
			}
		} 
		}
	}

	private static IOTime getIOTimeThread(ComponentInstance thread, String property)
	{
		return getIOTimeProcess((ComponentInstance) thread.eContainer(), property);
	}

	private static IOTime getIOTimeProcessFromListOfComponents(List<ComponentInstance> ciList, String property)
	{
		double maxFixed = 0D;
		double minFixed = 0D;
		double maxPerByte = 0D;
		double minPerByte = 0D;
		boolean first = true;
		for(ComponentInstance processor: ciList)
		{
			// If several potential processors, get the largest possible interval.
			IOTime cur = getIOTimeFromProperty(processor, property) ;
			if(first)
			{
				minFixed = cur.Fixed.getMin();
				minPerByte = cur.PerByte.getMin();
				first = false;
			}
			else 
			{
				if(cur.Fixed.getMin() <  minFixed)
				{
					minFixed = cur.Fixed.getMin();
				}
				if(cur.PerByte.getMin() <  minPerByte)
				{
					minPerByte = cur.PerByte.getMin();
				}
			}
			
			if(cur.Fixed.getMax() >  maxFixed)
			{
				maxFixed = cur.Fixed.getMax();
			}
			if(cur.PerByte.getMax() >  maxPerByte)
			{
				maxPerByte = cur.PerByte.getMax();
			}
		}
		DoubleRange fixed = new DoubleRange(minFixed, maxFixed);
		DoubleRange perByte = new DoubleRange(minPerByte, maxPerByte);
		return new IOTime(fixed, perByte);
	}
	
	/** Find property on Processor and Memory bound to the Process */
	private static IOTime getIOTimeProcess(ComponentInstance process, String property)
	{
		List<ComponentInstance> processors = ExecutionGraphUtils.getBinding(process, ComponentCategory.PROCESSOR);
		if(!processors.isEmpty())
			return getIOTimeProcessFromListOfComponents(processors, property);
		else
		{
			List<ComponentInstance> memories = ExecutionGraphUtils.getBinding(process, ComponentCategory.MEMORY);
			if(!memories.isEmpty())
				return getIOTimeProcessFromListOfComponents(memories, property);
			else
			{
				String msg = "cannot get actual memory binding for \'" + process.getName() +
						'\'' ;
				_LOGGER.warn(msg) ;
			}
		}
		return new IOTime();
	}

	
	
	private static IOTime getIOTimeFromProperty (ComponentInstance cpuOrMemory, String property)
	{
		RecordValue rv = PropertyUtils.getRecordValue(cpuOrMemory, property);

		if(rv != null)
		{	
			double fixedMin=0D,fixedMax=0D,perByteMin=0D,perByteMax=0D;

			for(BasicPropertyAssociation value : rv.getOwnedFieldValues())
			{
				String propertyName = value.getProperty().getName();
				if (propertyName.equalsIgnoreCase("Fixed"))
				{
					RangeValue range = (RangeValue) value.getOwnedValue();
					NumberValue nvMax = range.getMaximumValue();
					if(nvMax instanceof IntegerLiteral)
					{
						IntegerLiteral il = (IntegerLiteral) nvMax;
						fixedMax = il.getScaledValue(timeBaseUnit );
					}

					NumberValue nvMin = range.getMinimumValue();
					if(nvMin instanceof IntegerLiteral)
					{
						IntegerLiteral il = (IntegerLiteral) nvMin;
						fixedMin = il.getScaledValue(timeBaseUnit );
					}

				}
				else if (propertyName.equalsIgnoreCase("PerByte"))
				{
					RangeValue range = (RangeValue) value.getOwnedValue();
					NumberValue nvMax = range.getMaximumValue();
					if(nvMax instanceof IntegerLiteral)
					{
						IntegerLiteral il = (IntegerLiteral) nvMax;
						perByteMax = il.getScaledValue(timeBaseUnit);
					}
					NumberValue nvMin = range.getMinimumValue();
					if(nvMin instanceof IntegerLiteral)
					{
						IntegerLiteral il = (IntegerLiteral) nvMin;
						perByteMin = il.getScaledValue(timeBaseUnit);
					}
				}
			}

			DoubleRange fixed = new DoubleRange(fixedMin, fixedMax);
			DoubleRange perByte = new DoubleRange(perByteMin, perByteMax);

			return new IOTime(fixed,perByte);
		}
		else
		{
			return null ;
		}
	}

}
