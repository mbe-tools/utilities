package fr.mem4csd.analysis.executiontimegraph.utils;

import org.apache.log4j.Logger;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.ClassifierValue;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentPrototypeActual;
import org.osate.aadl2.ComponentPrototypeBinding;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.DataClassifier;
import org.osate.aadl2.DataImplementation;
import org.osate.aadl2.DataPrototype;
import org.osate.aadl2.DataSubcomponent;
import org.osate.aadl2.DataSubcomponentType;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.PrototypeBinding;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.SubcomponentType;
import org.osate.utils.internal.PropertyUtils;
import org.osate.xtext.aadl2.properties.util.AadlProject;

public class DataSizeUtils {

	private static final String DATA_SIZE_PROPERTY_NAME = "Data_Size";

	private static final String DATA_REPRESENTATION_PROPERTY_NAME = "Data_Representation";
	
	private static Logger _LOGGER = Logger.getLogger(DataSizeUtils.class);
	
	private static long getDataSizeInBytes(NamedElement e)
	{
		long size = getDataSizeInBytesFromProperty(e);
		if (size == 0)
		{
			if (e instanceof ComponentClassifier)
			{
				ComponentClassifier cc = (ComponentClassifier) e;
				if (cc instanceof ComponentImplementation)
				{
					ComponentImplementation ci = (ComponentImplementation) cc;
					ComponentImplementation extended = ci.getExtended();

					if (extended != null)
					{
						size = getDataSizeInBytes (extended);
						if (size == 0)
						{
							if (ci.getType() != null)
							{
								cc = ci.getType();
							}
						}
					}
				}
				if (cc instanceof ComponentType)
				{
					ComponentType extended = (ComponentType) cc.getExtended();
					if(extended!=null)
						size = getDataSizeInBytes (extended);
				}
			}
		}
		return size;
	}
	
	public static long getOrComputeDataSizeInBytes (NamedElement e)
	{
		long size = getDataSizeInBytes (e);
		if (size == 0)
		{
			size = computeDataSizeInBytes (e);
		}
		return size;
	}

	private static long computeDataSizeInBytes(NamedElement e)
	{

		String rep = getDataRepresentation(e);
		if ((e == null) || (rep == null))
		{
			return 0;
		}
		else if ((rep.equalsIgnoreCase("Struct")) && (e instanceof DataImplementation))
		{
			DataImplementation di = (DataImplementation) e;
			long size = 0;
			for(Subcomponent sub : di.getAllSubcomponents())
			{
				if (sub instanceof DataSubcomponent)
				{
					DataSubcomponent dsub = (DataSubcomponent) sub;
					DataSubcomponentType dst = dsub.getDataSubcomponentType();
					DataClassifier subdc = resolveClassifier (dst, di);
					size += getOrComputeDataSizeInBytes (subdc); 
				}
			}
			return size;
		}
		else if (rep.equalsIgnoreCase("Array"))
		{
			DataClassifier dc = getBaseType (e);
			long elementSize = getOrComputeDataSizeInBytes (dc);
			return elementSize;
		}
		else
		{
			return getDataSizeInBytes (e);
		}
	}


	private static DataClassifier getBaseType (NamedElement e)
	{
		DataClassifier dc = null;

		PropertyAssociation pa = PropertyUtils.findPropertyAssociation("Base_Type", e);

		if(pa != null)
		{
			ModalPropertyValue mpv = pa.getOwnedValues().get(0);
			PropertyExpression pex = mpv.getOwnedValue();
			if (pex != null && pex instanceof ListValue)
			{
				PropertyExpression pe = ((ListValue) pex).getOwnedListElements().get(0);
				if (pe instanceof ClassifierValue)
				{
					ClassifierValue cv = (ClassifierValue) pe;
					Classifier c = cv.getClassifier();
					if (c instanceof DataClassifier)
					{
						return (DataClassifier) c;
					}
				}
			}
		}
		else
		{
			String msg = "cannot find Base_Type for \'" + e.getName() + '\'' ;
			_LOGGER.warn(msg) ;
		}

		return dc;
	}
	private static DataClassifier resolveClassifier (DataSubcomponentType dst, DataClassifier dc)
	{
		if (dst instanceof DataClassifier)
		{
			return (DataClassifier) dst;
		}
		else if (dst instanceof DataPrototype)
		{
			//DataPrototype dp = (DataPrototype) dst;
			for(PrototypeBinding b : dc.getOwnedPrototypeBindings())
			{
				if (b.getFormal().getName().equals(dst.getName())
						&& b instanceof ComponentPrototypeBinding)
				{
					ComponentPrototypeBinding cpb = (ComponentPrototypeBinding) b;
					ComponentPrototypeActual a = cpb.getActuals().get(0);
					SubcomponentType t = a.getSubcomponentType();
					if (t instanceof DataClassifier)
					{
						return (DataClassifier) t;
					}
				}
			}
		}
		return null;
	}

	private static String getDataRepresentation(NamedElement e)
	{
		String rep = getDataRepresentationImpl(e);
		if (rep == null)
		{
			if (e instanceof ComponentClassifier)
			{
				ComponentClassifier cc = (ComponentClassifier) e;
				if (cc instanceof ComponentImplementation)
				{
					ComponentImplementation ci = (ComponentImplementation) cc;
					ComponentImplementation extended = ci.getExtended();

					if (extended != null)
					{
						rep = getDataRepresentation (extended);
						if (rep == null)
						{
							if (ci.getType() != null)
							{
								cc = ci.getType();
							}
						}
					}
				}
				if (cc instanceof ComponentType)
				{
					ComponentType ct = (ComponentType) cc;
					ComponentType extended = ct.getExtended();
					if(extended!=null)
						rep = getDataRepresentationImpl (ct.getExtended());
				}
			}
		}
		return rep;
	}


	private static String getDataRepresentationImpl(NamedElement e)
	{
		String rep = PropertyUtils.getEnumValue(e, DATA_REPRESENTATION_PROPERTY_NAME);
		if(rep == null)
		{
			String msg = "cannot find Data_Representation for \'" + e.getName() + '\'' ;
			_LOGGER.warn(msg) ;
		}
		return rep;
	}


	private static long getDataSizeInBytesFromProperty(NamedElement e)
	{
		Long size = PropertyUtils.getIntValue(e, DATA_SIZE_PROPERTY_NAME, AadlProject.B_LITERAL);

		if(size == null)
		{
			size = 0l ;
		}

		return size;
	}

	
}
