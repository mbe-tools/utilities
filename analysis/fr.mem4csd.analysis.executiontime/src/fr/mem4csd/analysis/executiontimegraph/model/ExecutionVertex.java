package fr.mem4csd.analysis.executiontimegraph.model;

import org.osate.aadl2.Element;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.instance.FeatureInstance;

public class ExecutionVertex {

	private String _identifier;
	private Element _aadlElement;
	private FeatureInstance _featureInstance;
	
	public ExecutionVertex(String identifier, Element aadlElement)
	{
		_identifier = identifier;
		_aadlElement = aadlElement;
	}
	
	public ExecutionVertex(String identifier, Element aadlElement, FeatureInstance featureInstance)
	{
		_identifier = identifier;
		_aadlElement = aadlElement;
		setFeatureInstance(featureInstance);
	}
	
	public String getIdentifier() {
		return _identifier;
	}
	
	public void setIdentifier(String _identifier) {
		this._identifier = _identifier;
	}
	
	public Element getAadlElement() {
		return _aadlElement;
	}
	
	public void setAadlElement(Element _element) {
		this._aadlElement = _element;
	}
	
	@Override
	public String toString()
	{
		if(this.getAadlElement() instanceof NamedElement)
		{
			NamedElement ne = (NamedElement) this.getAadlElement();
			if(ne.getName()!=null)
				return this.getIdentifier()+"_"+ne.getName();
		}
		return this.getIdentifier();
	}

	public Element getFeatureInstance() {
		return _featureInstance;
	}

	public void setFeatureInstance(FeatureInstance featureInstance) {
		this._featureInstance = featureInstance;
	}
}
