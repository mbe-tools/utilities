package fr.mem4csd.analysis.executiontimegraph.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;
import org.osate.aadl2.BehavioredImplementation;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.Connection;
import org.osate.aadl2.Element;
import org.osate.aadl2.Feature;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.ProcessorClassifier;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.SubprogramCall;
import org.osate.aadl2.SubprogramClassifier;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.ba.aadlba.BasicAction;
import org.osate.ba.aadlba.BehaviorAction;
import org.osate.ba.aadlba.BehaviorAnnex;
import org.osate.ba.aadlba.BehaviorState;
import org.osate.ba.aadlba.BehaviorTime;
import org.osate.ba.aadlba.BehaviorTransition;
import org.osate.ba.aadlba.CalledSubprogramHolder;
import org.osate.ba.aadlba.CondStatement;
import org.osate.ba.aadlba.DataAccessHolder;
import org.osate.ba.aadlba.ElseStatement;
import org.osate.ba.aadlba.ForOrForAllStatement;
import org.osate.ba.aadlba.IfStatement;
import org.osate.ba.aadlba.LockAction;
import org.osate.ba.aadlba.ParameterLabel;
import org.osate.ba.aadlba.SubprogramCallAction;
import org.osate.ba.aadlba.TimedAction;
import org.osate.ba.aadlba.UnlockAction;
import org.osate.ba.aadlba.WhileOrDoUntilStatement;
import org.osate.xtext.aadl2.properties.util.AadlProject;

import fr.mem4csd.analysis.executiontimegraph.utils.DoubleRange;
import fr.mem4csd.analysis.executiontimegraph.utils.ExecutionGraphUtils;
import fr.mem4csd.analysis.executiontimegraph.utils.ba.BehaviorTimeUtils;

@SuppressWarnings("serial")
public class ExecutionGraph extends DefaultDirectedWeightedGraph<ExecutionVertex, ExecutionEdge>{

	private Set<ExecutionVertex> _entryNodes = new HashSet<ExecutionVertex>(); 
	private Set<ExecutionVertex> _exitNodes = new HashSet<ExecutionVertex>();
	private Set<ExecutionVertex> _lockNodes = new HashSet<ExecutionVertex>();
	private Set<ExecutionVertex> _unlockNodes = new HashSet<ExecutionVertex>();
		
	private Map<Feature, FeatureInstance> featureToFeatureInstanceMap;

	
	private boolean _hasWCET = false;

	private Element rootElement;
	
	public ExecutionGraph(Element elt, Element root, Map<Feature, FeatureInstance> featureToFeatureInstanceMap)
	{
		super(ExecutionEdge.class);
		rootElement = root;
		if(featureToFeatureInstanceMap==null)
			this.featureToFeatureInstanceMap = new HashMap<Feature, FeatureInstance>();
		else
			this.featureToFeatureInstanceMap = featureToFeatureInstanceMap;
		initExecutionGraph(elt,root);
	}

	private void initExecutionGraph(Element elt, Element root)
	{
		_hasWCET=false;
		if(elt instanceof NamedElement)
		{
			DoubleRange execTimeRange = ExecutionGraphUtils.getComputeExecutionTimeInMs((NamedElement) elt, (ComponentInstance) rootElement);
			if(execTimeRange!=null && execTimeRange.getMax() >= 0D)
			{
				ExecutionVertex entryWCET = new ExecutionVertex("START_WCET", elt);
				_entryNodes.add(entryWCET);
				addVertex(entryWCET);

				ExecutionVertex exitWCET = new ExecutionVertex("STOP_WCET", elt);
				_exitNodes.add(exitWCET);
				addVertex(exitWCET);
				
				addEdge(this, entryWCET, exitWCET, execTimeRange);
				
				_hasWCET = true;
			}
		}
		if(!_hasWCET && elt instanceof ComponentInstance)
		{
			ComponentInstance ci = (ComponentInstance) elt;
			initExecutionGraph((Element)ci.getSubcomponent(), root);
		}
		if(!_hasWCET && elt instanceof Subcomponent)
		{
			Subcomponent subcpt = (Subcomponent) elt;
			ComponentImplementation cImpl = subcpt.getComponentImplementation();
			if(cImpl != null)
				initExecutionGraph((Element)cImpl, root);
			else
			{
				ComponentType cType = subcpt.getComponentType();
				if(cType != null)
					initExecutionGraph((Element)cType, root);
			}
		}
		if(!_hasWCET && elt instanceof BehavioredImplementation)
		{
			List<SubprogramCall> scList = ExecutionGraphUtils.getCallSequence((NamedElement) elt);
			ExecutionGraph eg = initExecutionSubGraph(scList, root);
			if(eg.hasWCET())
			{
				// search WCET contributions in call sequence
				ExecutionVertex entryBehavioredImplementation = new ExecutionVertex("START_BEHAVIORED_IMPLEMENTATION", elt);
				_entryNodes.add(entryBehavioredImplementation);
				addVertex(entryBehavioredImplementation);

				_hasWCET = true;
				merge(eg);
				connectVertexSets(_entryNodes, eg.getEntryNodes());

				ExecutionVertex exitBehavioredImplementation = new ExecutionVertex("STOP_BEHAVIORED_IMPLEMENTATION", elt);
				_exitNodes.add(exitBehavioredImplementation);
				addVertex(exitBehavioredImplementation);

				connectVertexSets(eg.getExitNodes(), _exitNodes);
			}
			
		}
		if(!_hasWCET && elt instanceof BehaviorTransition)
		{
			// search WCET contributions in behavior transition
			BehaviorTransition bt = (BehaviorTransition) elt;
			ExecutionGraph eg = initExecutionSubGraph(bt, root);
			if(eg.hasWCET())
			{
				ExecutionVertex entryBehavioredTransition = new ExecutionVertex("START_BA_TRANSITION", elt);
				_entryNodes.add(entryBehavioredTransition);
				addVertex(entryBehavioredTransition);

				_hasWCET = true;
				merge(eg);
				connectVertexSets(_entryNodes, eg.getEntryNodes());

				ExecutionVertex exitBehavioredTransition = new ExecutionVertex("STOP_BA_TRANSITION", elt);
				addVertex(exitBehavioredTransition);
				_exitNodes.add(exitBehavioredTransition);
				if(false == bt.getDestinationState().isFinal())
					connectVertexSets(eg.getExitNodes(), _exitNodes);
				else
				{
					for(ExecutionVertex t: eg.getExitNodes())
					{
						addEdge(this, t, exitBehavioredTransition, null);
					}
				}
			}
		}
		if(!_hasWCET && elt instanceof BehaviorAction)
		{
			// search WCET contributions in behavior action
			BehaviorAction act = (BehaviorAction) elt;
			ExecutionGraph eg = initExecutionSubGraph(act, root);
			if(eg==null)
				return;
			if(eg.hasWCET())
			{
				ExecutionVertex entryBehaviorAction = new ExecutionVertex("START_TRANSITION_ACTION", elt);
				_entryNodes.add(entryBehaviorAction);
				addVertex(entryBehaviorAction);

				_hasWCET = true;
				merge(eg);
				connectVertexSets(_entryNodes, eg.getEntryNodes());

				ExecutionVertex exitBehaviorAction = new ExecutionVertex("STOP_TRANSITION_ACTION", elt);
				_exitNodes.add(exitBehaviorAction);
				addVertex(exitBehaviorAction);

				connectVertexSets(eg.getExitNodes(), _exitNodes);

			}
		}
		if(!_hasWCET && elt instanceof NamedElement)
		{
			// search WCET contributions in BA
			BehaviorAnnex ba = ExecutionGraphUtils.getBehaviorAnnex((NamedElement) elt);
			if(ba != null)
			{
				ExecutionGraph eg = initExecutionSubGraph(ba, root);
				if(eg.hasWCET())
				{
//					ExecutionVertex entryBehaviorAnnex = new ExecutionVertex("START_BEHAVIOR_ANNEX", elt);
//					_entryNodes.add(entryBehaviorAnnex);
//					addVertex(entryBehaviorAnnex);


					_hasWCET = true;
					merge(eg);

					_entryNodes = eg._entryNodes;
					_exitNodes = eg._exitNodes;
					
//					connectVertexSets(_entryNodes, eg.getEntryNodes());

//					ExecutionVertex exitBehaviorAnnex = new ExecutionVertex("STOP_BEHAVIOR_ANNEX", elt);
//					_exitNodes.add(exitBehaviorAnnex);
//					addVertex(exitBehaviorAnnex);

//					connectVertexSets(eg.getExitNodes(), _exitNodes);
				}
			}
		}
	}


	private void addEdge(Graph<ExecutionVertex, ExecutionEdge> graph,
			ExecutionVertex source,
			ExecutionVertex target, 
			DoubleRange execTimeRange) {
		if(execTimeRange!=null)
		{
			ExecutionEdge edge = graph.addEdge(source, target);
			edge.setExecTime(execTimeRange);
		}
		else
			Graphs.addEdge(graph, source, target, 0D);
	}

	private ExecutionGraph initExecutionSubGraph(SubprogramCallAction action, Element root)
	{
		ExecutionGraph eg = new ExecutionGraph(root);

		ExecutionVertex entryCallAction = new ExecutionVertex("START_CALLACTION", action);
		eg._entryNodes.add(entryCallAction);
		eg.addVertex(entryCallAction);

		CalledSubprogramHolder spgHolder = action.getSubprogram();
		NamedElement spg = spgHolder.getElement();
		
		addParametersMapping(action, root);
				
		ExecutionGraph calledSpgGraph = new ExecutionGraph(spg, root, this.featureToFeatureInstanceMap);
		eg._hasWCET = calledSpgGraph._hasWCET;
		
		merge(eg, calledSpgGraph);
		connectVertexSets(eg, entryCallAction, calledSpgGraph._entryNodes);
		
		ExecutionVertex exitCallAction = new ExecutionVertex("STOP_CALLACTION", action);
		eg._exitNodes.add(exitCallAction);
		eg.addVertex(exitCallAction);

		connectVertexSets(eg, calledSpgGraph._exitNodes, exitCallAction);

		removeParametersMapping(action, root);
		
		
		return eg;
	}

	private void removeParametersMapping(SubprogramCallAction action, Element root) {
		updateParametersMapping(action, root, false);
	}

	private void addParametersMapping(SubprogramCallAction action, Element root) {
		updateParametersMapping(action, root, true);
	}

	private void updateParametersMapping(SubprogramCallAction action, Element root, boolean add) {
		
		ComponentInstance ci = (ComponentInstance) root;
		List<FeatureInstance> rootFeatureInstanceList = 
				ci.getAllFeatureInstances();
		
		int idx=-1;
		for(ParameterLabel pl: action.getParameterLabels())
		{
			idx++;
			if(pl instanceof DataAccessHolder)
			{
				for(FeatureInstance rootFeatureInstance: rootFeatureInstanceList)
				{
					Feature toUpdate = null;

					CalledSubprogramHolder spgHolder = action.getSubprogram();
					NamedElement ne = spgHolder.getElement();
					if(ne instanceof SubprogramClassifier)
					{
						SubprogramClassifier spg = (SubprogramClassifier) ne;
						toUpdate = spg.getAllFeatures().get(idx);
					}
					
					if(toUpdate==null)
						continue;
					
					if(rootFeatureInstance.getFeature().equals(((DataAccessHolder) pl).getDataAccess()))
					{
						if(add)
							featureToFeatureInstanceMap.put(toUpdate, 
									rootFeatureInstance);
						else
							featureToFeatureInstanceMap.remove(toUpdate);
					}
					else if(featureToFeatureInstanceMap.containsKey(((DataAccessHolder) pl).getDataAccess()))
					{
						FeatureInstance mapped = featureToFeatureInstanceMap.get(((DataAccessHolder) pl).getDataAccess());
						if(add)
							featureToFeatureInstanceMap.put(toUpdate, mapped);
						else
							featureToFeatureInstanceMap.remove(toUpdate);
					}
				}
			}
		}
	}

	private ExecutionGraph initExecutionSubGraph(TimedAction action, Element root)
	{
		ExecutionGraph eg = new ExecutionGraph(root);

		ExecutionVertex entryTimedAction = new ExecutionVertex("START_TIMEDACTION", action);
		eg._entryNodes.add(entryTimedAction);
		eg.addVertex(entryTimedAction);

		ExecutionVertex exitTimedAction = new ExecutionVertex("STOP_TIMEDACTION", action);
		eg._exitNodes.add(exitTimedAction);
		eg.addVertex(exitTimedAction);

		BehaviorTime btLow = action.getLowerTime();
		BehaviorTime btUp = action.getUpperTime();

		List<ProcessorClassifier> inBindings = action.getProcessorClassifier();
		
		List<ProcessorClassifier> boundTo = getProcessorClassifier();
		if(!inBindings.isEmpty())
		{
			// make sure the timed action is valid for all the processors we are bound to!
			for(ProcessorClassifier bound: boundTo)
			{
				if(!inBindings.contains(bound))
					return eg;
			}
		}
		double scaledUpperBound = BehaviorTimeUtils.getScaledValue(btUp, AadlProject.MS_LITERAL);
		double scaledLowerBound = BehaviorTimeUtils.getScaledValue(btLow, AadlProject.MS_LITERAL);
		
		if(scaledUpperBound>=0D)
			eg._hasWCET = true;
		addEdge(eg, entryTimedAction, exitTimedAction, new DoubleRange(scaledLowerBound, scaledUpperBound));
		return eg;

	}

	private List<ProcessorClassifier> getProcessorClassifier() {
		List<ProcessorClassifier> result = new ArrayList<ProcessorClassifier>();
		if(rootElement instanceof ComponentInstance)
		{
			ComponentInstance ci = (ComponentInstance) rootElement;
			List<ComponentInstance> processorInstances = ExecutionGraphUtils.getBinding(ci, ComponentCategory.PROCESSOR);
			for(ComponentInstance processorInstance : processorInstances)
			{
				result.add((ProcessorClassifier) processorInstance.getSubcomponent().getSubcomponentType());
			}
		}
		return result;
	}

	private ExecutionGraph initExecutionSubGraph(LockAction action, Element root)
	{
		ExecutionGraph eg = new ExecutionGraph(root);
		
		ExecutionVertex lockAction = new ExecutionVertex("LOCK_ACTION", action, featureToFeatureInstanceMap.get(action.getDataAccess().getDataAccess()));
		eg._entryNodes.add(lockAction);
		eg._exitNodes.add(lockAction);
		
		eg._lockNodes.add(lockAction);
		
		eg.addVertex(lockAction);
		eg._hasWCET = true;
		return eg;
	}

	private ExecutionGraph initExecutionSubGraph(UnlockAction action, Element root)
	{
		ExecutionGraph eg = new ExecutionGraph(root);
		ExecutionVertex unlockAction = new ExecutionVertex("UNLOCK_ACTION", action, featureToFeatureInstanceMap.get(action.getDataAccess().getDataAccess()));
		eg._entryNodes.add(unlockAction);
		eg._exitNodes.add(unlockAction);
		
		eg._unlockNodes.add(unlockAction);

		
		eg.addVertex(unlockAction);
		eg._hasWCET = true;
		return eg;
	}

	private void populateListOfBranches(ElseStatement action, List<ElseStatement> result)
	{
		result.add(action);
		if(action instanceof IfStatement)
		{
			IfStatement ifStatement = (IfStatement) action;  
			populateListOfBranches(ifStatement.getElseStatement(), result);
		}
	}
	
	private ExecutionGraph initExecutionSubGraph(IfStatement action, Element root)
	{
		ExecutionGraph eg = new ExecutionGraph(root);
		List<ExecutionGraph> branchGraphs = new ArrayList<ExecutionGraph>();
		
		ExecutionVertex entryIfAction = new ExecutionVertex("START_IFACTION", action);
		eg._entryNodes.add(entryIfAction);
		eg.addVertex(entryIfAction);
		
		List<ElseStatement> branches = new ArrayList<ElseStatement>();
		populateListOfBranches(action, branches);

		for(ElseStatement branch: branches)
		{
			ExecutionGraph branchActionsGraph = new ExecutionGraph(branch.getBehaviorActions(), root, this.featureToFeatureInstanceMap);
			
			merge(eg, branchActionsGraph);
			connectVertexSets(eg, eg._entryNodes, branchActionsGraph._entryNodes);
		
			eg._hasWCET |= branchActionsGraph._hasWCET;
		
			branchGraphs.add(branchActionsGraph);
		}		
		
		
		ExecutionVertex exitIfAction = new ExecutionVertex("STOP_IFACTION", action);
		eg._exitNodes.add(exitIfAction);
		eg.addVertex(exitIfAction);
		
		for(ExecutionGraph intermediateGraph: branchGraphs)
		{
			connectVertexSets(eg, intermediateGraph._exitNodes, eg._exitNodes);
		}		
		return eg;
	}

	private ExecutionGraph initExecutionSubGraph(ForOrForAllStatement action, Element root)
	{
		for(BehaviorAction act: ExecutionGraphUtils.getBehaviorActions(action.getBehaviorActions()))
		{
			ExecutionGraph actionExecGraph = new ExecutionGraph(act, root, this.featureToFeatureInstanceMap);
			if(actionExecGraph._hasWCET==false)
				continue;
			throw new UnsupportedOperationException("Execution subgraph init for "+action.getClass().getSimpleName()
					+": cannot bound the number of loop iterations (use timed actions outside the loop)");
		}
		return null;
	}

	private ExecutionGraph initExecutionSubGraph(WhileOrDoUntilStatement action, Element root)
	{
		for(BehaviorAction act: ExecutionGraphUtils.getBehaviorActions(action.getBehaviorActions()))
		{
			ExecutionGraph actionExecGraph = new ExecutionGraph(act, root, this.featureToFeatureInstanceMap);
			if(actionExecGraph._hasWCET==false)
				continue;
			throw new UnsupportedOperationException("Execution subgraph init for "+action.getClass().getSimpleName()
					+": cannot bound the number of loop iterations (use timed actions outside the loop)");
		}
		return null;
	}

	private ExecutionGraph initExecutionSubGraph(BehaviorAction action, Element root) {
		if (action instanceof BasicAction)
		{
			if (action instanceof TimedAction)
			{
				return initExecutionSubGraph((TimedAction) action, root);
			}
			else if (action instanceof SubprogramCallAction)
			{
				return initExecutionSubGraph((SubprogramCallAction) action, root);
			}
			else if (action instanceof LockAction)
			{
				return initExecutionSubGraph((LockAction) action, root);
			}
			else if (action instanceof UnlockAction)
			{
				return initExecutionSubGraph ((UnlockAction) action, root);
			}
			else
			{
				String msg = '\'' +
						action.getClass().getSimpleName() + "\' initExecutionSubgraph" ;
				throw new UnsupportedOperationException(msg);
			}
		}
		else if (action instanceof CondStatement)
		{
			if (action instanceof IfStatement)
			{
				return initExecutionSubGraph((IfStatement) action, root);
			}
			else if (action instanceof ForOrForAllStatement)
			{
				return initExecutionSubGraph((ForOrForAllStatement) action, root);
			}
			else if (action instanceof WhileOrDoUntilStatement)
			{
				return initExecutionSubGraph((WhileOrDoUntilStatement) action, root);
			}
			else
			{
				String msg = '\'' +
						action.getClass().getSimpleName() + "\' initExecutionSubgraph" ;
				throw new UnsupportedOperationException(msg);
			}
		}
		return null;
	}

	private ExecutionGraph initExecutionSubGraph(BehaviorTransition bt, Element root) {
		//Behavior Actions
		ExecutionGraph actionBlockExecGraph= new ExecutionGraph(root);
		
		
		
		boolean first = true;
		ExecutionGraph actionExecGraph = null;
		for(BehaviorAction act: ExecutionGraphUtils.getBehaviorActions(bt.getActionBlock().getContent()))
		{
			actionExecGraph = new ExecutionGraph(act, root, this.featureToFeatureInstanceMap);
			if(actionExecGraph._hasWCET==false)
				continue;
			if(first)
				actionBlockExecGraph._entryNodes = actionExecGraph._entryNodes;
			first = false;
			merge(actionBlockExecGraph, actionExecGraph);
			
			connectVertexSets(actionBlockExecGraph,actionBlockExecGraph._exitNodes, actionExecGraph._entryNodes);
			actionBlockExecGraph._exitNodes = actionExecGraph._exitNodes;
			
			actionBlockExecGraph._hasWCET = actionBlockExecGraph._hasWCET || actionExecGraph._hasWCET;
		}
		
		return actionBlockExecGraph;
	}

	private ExecutionGraph(Element root) {
		super(ExecutionEdge.class);
		rootElement = root;
	}

	private ExecutionGraph initExecutionSubGraph(BehaviorAnnex ba, Element root) {

		ExecutionGraph result= new ExecutionGraph(root);

		Set<ExecutionVertex> behaviorAnnexEntryNodes = new HashSet<ExecutionVertex>();
		Set<ExecutionVertex> behaviorAnnexExitNodes = new HashSet<ExecutionVertex>();

		Map<BehaviorState, ExecutionVertex> baStateToVertex = new HashMap<BehaviorState, ExecutionVertex>();

		// get complete, initial and final states
		for(BehaviorState bs: ba.getStates())
		{
			ExecutionVertex bsVertex = new ExecutionVertex("BA_STATE", bs);
			baStateToVertex.put(bs, bsVertex);

			if(bs.isInitial() || bs.isComplete())
			{
				behaviorAnnexEntryNodes.add(bsVertex);
			}
			if((!bs.isInitial() && bs.isFinal()) || bs.isComplete())
			{
				behaviorAnnexExitNodes.add(bsVertex);
			}
		}

		for(BehaviorTransition bt: ba.getTransitions())
		{
			result.addVertex(baStateToVertex.get(bt.getSourceState()));
			ExecutionGraph egTrans = new ExecutionGraph(bt, root, this.featureToFeatureInstanceMap);
			result._hasWCET = result._hasWCET || egTrans._hasWCET;
			merge(result, egTrans);
			result.addVertex(baStateToVertex.get(bt.getDestinationState()));
			connectVertexSets(result, baStateToVertex.get(bt.getSourceState()), egTrans.getEntryNodes());
			if(false == bt.getDestinationState().isFinal())
				connectVertexSets(result, egTrans.getExitNodes(), baStateToVertex.get(bt.getDestinationState()));
			else
				behaviorAnnexExitNodes.addAll(egTrans.getExitNodes());
		}

		result.setEntryNodes(behaviorAnnexEntryNodes);
		result.setExitNodes(behaviorAnnexExitNodes);

		return result;
	}

	private ExecutionGraph initExecutionSubGraph(List<SubprogramCall> scList, Element root)
	{
		Set<ExecutionVertex> currentVertexSet = new HashSet<ExecutionVertex>();
		ExecutionGraph result = new ExecutionGraph(root);
		ExecutionGraph subprogramCallExecGraph = null;
		boolean first = true;
		for(SubprogramCall sc : scList)
		{
			addParametersMapping(sc, root);
			
			subprogramCallExecGraph = new ExecutionGraph((Element) sc.getCalledSubprogram(), root, this.featureToFeatureInstanceMap);
			if(subprogramCallExecGraph._hasWCET==false)
				continue;
			if(first)
			{
				first = false;
				result._entryNodes = subprogramCallExecGraph._entryNodes;
			}

			if(subprogramCallExecGraph.hasWCET())
			{
				result._hasWCET = true;
			}
			merge(result, subprogramCallExecGraph);

			connectVertexSets(result, currentVertexSet, subprogramCallExecGraph.getEntryNodes());
			currentVertexSet = subprogramCallExecGraph.getExitNodes();

			removeParametersMapping(sc, root);

		}
		if(result._hasWCET)
		{
			result._exitNodes = subprogramCallExecGraph._exitNodes;
		}
		return result;
	}

	private void updateParametersMapping(SubprogramCall sc, Element root, boolean add) {
		
		ComponentInstance ci = (ComponentInstance) root;
		List<FeatureInstance> rootFeatureInstanceList = 
				ci.getAllFeatureInstances();
		
		List<Connection> connectionList = getConnectionList(sc);
		for(Connection cnx: connectionList)
		{
			for(FeatureInstance rootFeatureInstance: rootFeatureInstanceList)
			{
				if(rootFeatureInstance.getFeature().equals(cnx.getSource().getConnectionEnd()))
				{
					if(add)
						featureToFeatureInstanceMap.put((Feature) cnx.getDestination().getConnectionEnd(), 
								rootFeatureInstance);
					else
						featureToFeatureInstanceMap.remove((Feature) cnx.getDestination().getConnectionEnd());
				}
				else if(rootFeatureInstance.getFeature().equals(cnx.getDestination().getConnectionEnd()))
				{
					if(add)
						featureToFeatureInstanceMap.put((Feature) cnx.getSource().getConnectionEnd(), 
								rootFeatureInstance);
					else
						featureToFeatureInstanceMap.remove((Feature) cnx.getSource().getConnectionEnd());
				}
				else if(featureToFeatureInstanceMap.containsKey(cnx.getSource().getConnectionEnd()))
				{
					FeatureInstance mapped = featureToFeatureInstanceMap.get(cnx.getSource().getConnectionEnd());
					if(add)
						featureToFeatureInstanceMap.put((Feature) cnx.getDestination().getConnectionEnd(), mapped);
					else
						featureToFeatureInstanceMap.remove((Feature) cnx.getDestination().getConnectionEnd());
				}
				else if(featureToFeatureInstanceMap.containsKey(cnx.getDestination().getConnectionEnd()))
				{
					FeatureInstance mapped = featureToFeatureInstanceMap.get(cnx.getDestination().getConnectionEnd());
					if(add)
						featureToFeatureInstanceMap.put((Feature) cnx.getSource().getConnectionEnd(), mapped);
					else
						featureToFeatureInstanceMap.remove((Feature) cnx.getSource().getConnectionEnd());
				}
			}
		}
		
	}
	
	private void addParametersMapping(SubprogramCall sc, Element root) {
		updateParametersMapping(sc, root, true);
	}

	private void removeParametersMapping(SubprogramCall sc, Element root) {
		updateParametersMapping(sc, root, false);
	}
	
	private List<Connection> getConnectionList(SubprogramCall sc) {
		BehavioredImplementation bi = (BehavioredImplementation) sc.eContainer().eContainer();
		return bi.getAllConnections();
	}

	private void connectVertexSets(Collection<ExecutionVertex> sources, Collection<ExecutionVertex> targets)
	{
		connectVertexSets(sources, targets, null);
	}

	private void connectVertexSets(Collection<ExecutionVertex> sources, Collection<ExecutionVertex> targets,
			DoubleRange execTime) {
		for(ExecutionVertex s: sources)
		{
			for(ExecutionVertex t: targets)
			{
				addEdge(this, s, t, execTime);
			}
		}
	}


	private void connectVertexSets(Graph<ExecutionVertex, ExecutionEdge> g, Collection<ExecutionVertex> sources, Collection<ExecutionVertex> targets)
	{
		for(ExecutionVertex s: sources)
		{
			for(ExecutionVertex t: targets)
			{
				Graphs.addEdge(g, s, t, 0D);
			}
		}
	}

	private void connectVertexSets(Graph<ExecutionVertex, ExecutionEdge> g,
			ExecutionVertex source, Set<ExecutionVertex> targets, DoubleRange execTime) {
		for(ExecutionVertex t: targets)
		{
			if(execTime!=null)
			{
				ExecutionEdge edge = g.addEdge(source, t);
				edge.setExecTime(execTime);
			}
			else
				Graphs.addEdge(g, source, t, 0D);
				
		}
	}

	private void connectVertexSets(Graph<ExecutionVertex, ExecutionEdge> g,
			Set<ExecutionVertex> sources, ExecutionVertex target, DoubleRange execTime) {
		for(ExecutionVertex s: sources)
		{
			if(execTime!=null)
			{
				ExecutionEdge edge = g.addEdge(s, target);
				edge.setExecTime(execTime);
			}
			else
				Graphs.addEdge(g, s, target, 0D);
		}
	}
	
	private void connectVertexSets(Graph<ExecutionVertex, ExecutionEdge> g,
			ExecutionVertex source, Set<ExecutionVertex> targets) {
		connectVertexSets(g, source, targets, null);
	}

	private void connectVertexSets(Graph<ExecutionVertex, ExecutionEdge> g,
			Set<ExecutionVertex> sources, ExecutionVertex target) {
		connectVertexSets(g, sources, target, null);
	}

	private void merge(ExecutionGraph eg)
	{
		this._lockNodes.addAll(eg.getLockNodes());
		this._unlockNodes.addAll(eg.getUnlockNodes());
		Graphs.addGraph(this, eg);
	}

	private void merge(ExecutionGraph eg1, ExecutionGraph eg2)
	{
		Graphs.addGraph(eg1, eg2);
		eg1._lockNodes.addAll(eg2.getLockNodes());
		eg1._unlockNodes.addAll(eg2.getUnlockNodes());
	}

	public Set<ExecutionVertex> getEntryNodes() {
		return _entryNodes;
	}

	public void setEntryNodes(Set<ExecutionVertex> entryNodes) {
		this._entryNodes = entryNodes;
	}

	public Set<ExecutionVertex> getExitNodes() {
		return _exitNodes;
	}

	public void setExitNodes(Set<ExecutionVertex> exitNodes) {
		this._exitNodes = exitNodes;
	}

	public boolean hasWCET() {
		return _hasWCET;
	}

	public Element getRootElement() {
		return rootElement;
	}
	
	@Override
	public double getEdgeWeight(ExecutionEdge e)
	{
		return e.getWeight();
	}

	public Set<ExecutionVertex> getLockNodes() {
		return _lockNodes;
	}

	public Set<ExecutionVertex> getUnlockNodes() {
		return _unlockNodes;
	}
}
