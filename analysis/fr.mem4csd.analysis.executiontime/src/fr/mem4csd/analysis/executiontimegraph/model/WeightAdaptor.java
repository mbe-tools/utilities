package fr.mem4csd.analysis.executiontimegraph.model;

import fr.mem4csd.analysis.executiontimegraph.utils.DoubleRange;

public interface WeightAdaptor {

	double getWeight(DoubleRange execTime);
	
}
