package fr.mem4csd.analysis.executiontimegraph.model;

import org.jgrapht.graph.DefaultWeightedEdge;

import fr.mem4csd.analysis.executiontimegraph.utils.DoubleRange;

@SuppressWarnings("serial")
public class ExecutionEdge  extends DefaultWeightedEdge {
	
	private DoubleRange execTime;
	
	public DoubleRange getExecTime() {
		return execTime;
	}

	public void setExecTime(DoubleRange execTime) {
		this.execTime = execTime;
	}

	private WeightAdaptor adaptor;
	
	public WeightAdaptor getAdaptor() {
		return adaptor;
	}

	public void setAdaptor(WeightAdaptor adaptor) {
		this.adaptor = adaptor;
	}

	@Override
	public double getWeight() {
		if(adaptor==null)
		{
			if(execTime==null)
				return 0D;
			return execTime.getMax();
		}
		else if(execTime==null)
			return 0D;
		else 
			return adaptor.getWeight(execTime);
	}
	
	@Override
	public String toString() {
		if(execTime!=null)
			return "["+execTime.getMin()+".."+execTime.getMax()+"]";
		if(getWeight()>0.0)
			return Double.toString(getWeight());
		else
			return "";
	}
}
