<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowanalysisexecutiontime="https://mem4csd.telecom-paris.fr/analysis/workflowanalysisexecutiontime" xmlns:workflowosate="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowosate" xmi:id="_dXWJgLImEeqBrOJiHe_Vxw" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_8hHDkLI2EeqBrOJiHe_Vxw" name="workflowDelegation" workflowURI="${scheme}${executiontime_plugin}workflows/load_osate_aadl_contributions.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_KBQCELInEeqBrOJiHe_Vxw" name="modelReader" modelSlot="executionTimeAadlSrcModel" modelURI="${source_aadl_file}"/>
  <components xsi:type="workflowosate:AadlInstanceModelCreator" xmi:id="_vypd0LI_Eeq9c-LlaAt5RA" name="aadlInstanceModelCreator" systemImplementationName="${system_implementation_name}" packageModelSlot="executionTimeAadlSrcModel" systemInstanceModelSlot="srcInstanceModel"/>
  <components xsi:type="workflowanalysisexecutiontime:ExecutionGraphExtraction" xmi:id="_nvvuQLR3EeqcM5l-N_z0Og" name="executionGraphExtraction" srcInstanceModelSlot="srcInstanceModel" outputDir="${output_dir}" saveExecutionGraphImage="true" executionGraphList="executionTimeExecutionGraphList"/>
  <components xsi:type="workflowanalysisexecutiontime:ExecutionTimeBoundsProduction" xmi:id="_46i7cLR5Eeq9BrqiQ-oIdA" name="executionTimeBoundsProduction" srcExecutionGraphListSlot="executionTimeExecutionGraphList" outputDir="${output_dir}" executionTimeBoundsModelSlot="executionTimeBoundsModelSlot"/>
  <components xsi:type="workflow.components:ModelWriter" xmi:id="_50PN0LTMEeq5T5Fds5ngzg" name="modelWriter" modelSlot="executionTimeBoundsModelSlot" modelURI="${output_dir}/${execution_time_bounds_file_name}.executiontimebounds"/>
  <properties xmi:id="_h_1-oLImEeqBrOJiHe_Vxw" name="source_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_6OgiELImEeqBrOJiHe_Vxw" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_9t6BILImEeqBrOJiHe_Vxw" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_eKeAALTNEeq5T5Fds5ngzg" name="execution_time_bounds_file_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_f6W40LImEeqBrOJiHe_Vxw" fileURI="default_exectime.properties"/>
</workflow:Workflow>
