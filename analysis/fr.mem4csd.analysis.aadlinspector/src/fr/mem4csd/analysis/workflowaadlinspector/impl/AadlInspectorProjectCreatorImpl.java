/**
 */
package fr.mem4csd.analysis.workflowaadlinspector.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.osate.aadl2.AadlPackage;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import de.mdelab.workflow.util.WorkflowUtil;
import fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator;
import fr.mem4csd.analysis.workflowaadlinspector.WorkflowaadlinspectorPackage;
import fr.mem4csd.utils.emf.EMFUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aadl Inspector Project Creator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.analysis.workflowaadlinspector.impl.AadlInspectorProjectCreatorImpl#getAadlModelSlot <em>Aadl Model Slot</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.workflowaadlinspector.impl.AadlInspectorProjectCreatorImpl#getOutputDirectory <em>Output Directory</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.workflowaadlinspector.impl.AadlInspectorProjectCreatorImpl#getAicFileName <em>Aic File Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AadlInspectorProjectCreatorImpl extends WorkflowComponentImpl implements AadlInspectorProjectCreator {
	/**
	 * The default value of the '{@link #getAadlModelSlot() <em>Aadl Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAadlModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String AADL_MODEL_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAadlModelSlot() <em>Aadl Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAadlModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String aadlModelSlot = AADL_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutputDirectory() <em>Output Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDirectory()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTPUT_DIRECTORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutputDirectory() <em>Output Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDirectory()
	 * @generated
	 * @ordered
	 */
	protected String outputDirectory = OUTPUT_DIRECTORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getAicFileName() <em>Aic File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAicFileName()
	 * @generated
	 * @ordered
	 */
	protected static final String AIC_FILE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAicFileName() <em>Aic File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAicFileName()
	 * @generated
	 * @ordered
	 */
	protected String aicFileName = AIC_FILE_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AadlInspectorProjectCreatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowaadlinspectorPackage.Literals.AADL_INSPECTOR_PROJECT_CREATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getAadlModelSlot() {
		return aadlModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAadlModelSlot(String newAadlModelSlot) {
		String oldAadlModelSlot = aadlModelSlot;
		aadlModelSlot = newAadlModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__AADL_MODEL_SLOT, oldAadlModelSlot, aadlModelSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOutputDirectory() {
		return outputDirectory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOutputDirectory(String newOutputDirectory) {
		String oldOutputDirectory = outputDirectory;
		outputDirectory = newOutputDirectory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__OUTPUT_DIRECTORY, oldOutputDirectory, outputDirectory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getAicFileName() {
		return aicFileName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAicFileName(String newAicFileName) {
		String oldAicFileName = aicFileName;
		aicFileName = newAicFileName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__AIC_FILE_NAME, oldAicFileName, aicFileName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__AADL_MODEL_SLOT:
				return getAadlModelSlot();
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__OUTPUT_DIRECTORY:
				return getOutputDirectory();
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__AIC_FILE_NAME:
				return getAicFileName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__AADL_MODEL_SLOT:
				setAadlModelSlot((String)newValue);
				return;
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__OUTPUT_DIRECTORY:
				setOutputDirectory((String)newValue);
				return;
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__AIC_FILE_NAME:
				setAicFileName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__AADL_MODEL_SLOT:
				setAadlModelSlot(AADL_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__OUTPUT_DIRECTORY:
				setOutputDirectory(OUTPUT_DIRECTORY_EDEFAULT);
				return;
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__AIC_FILE_NAME:
				setAicFileName(AIC_FILE_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__AADL_MODEL_SLOT:
				return AADL_MODEL_SLOT_EDEFAULT == null ? aadlModelSlot != null : !AADL_MODEL_SLOT_EDEFAULT.equals(aadlModelSlot);
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__OUTPUT_DIRECTORY:
				return OUTPUT_DIRECTORY_EDEFAULT == null ? outputDirectory != null : !OUTPUT_DIRECTORY_EDEFAULT.equals(outputDirectory);
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR__AIC_FILE_NAME:
				return AIC_FILE_NAME_EDEFAULT == null ? aicFileName != null : !AIC_FILE_NAME_EDEFAULT.equals(aicFileName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (aadlModelSlot: ");
		result.append(aadlModelSlot);
		result.append(", outputDirectory: ");
		result.append(outputDirectory);
		result.append(", aicFileName: ");
		result.append(aicFileName);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void execute (final WorkflowExecutionContext context,
			final IProgressMonitor monitor) throws WorkflowExecutionException, IOException {
		
		EMap<String, Object> modelSlots = context.getModelSlots();
		
		AadlPackage aadlPackage = (AadlPackage) modelSlots.get(aadlModelSlot);
		
		File outputDir = new File(outputDirectory);
		
		URI aicFileURI = WorkflowUtil.getResolvedURI(URI.createURI(outputDir+File.separator+aicFileName+".aic"), context.getWorkflowFileURI());
		
		Set<String> aadlModelsPath = new LinkedHashSet<String>();
		
		aadlModelsPath.add(getModelPath(aicFileURI, aadlPackage.eResource().getURI()));
		
		for ( final Resource res : EMFUtil.allReferencedExtents( aadlPackage.eResource() ) ) {
			String path = getModelPath(aicFileURI, res.getURI());
			if(path!=null)
				aadlModelsPath.add(path);
		}
		
		final String filePath;
		if(false==Platform.isRunning())
			filePath = aicFileURI.toFileString();
		else
			filePath = aicFileURI.toPlatformString(true);
		
		writeProjectFile(filePath, aadlModelsPath);
		
	}
	
	
	private String getModelPath(URI aicFileURI, URI modelUri) {
		URI uri = WorkflowUtil.getResolvedURI(modelUri, aicFileURI);
		if(Platform.isRunning())
			return uri.toPlatformString(true);
		else
			return uri.toFileString();
	}

	private void writeProjectFile(String projectPath, Set<String> aadlModelsPath) throws FileNotFoundException, UnsupportedEncodingException
	{
		PrintWriter writer ;

		writer = new PrintWriter(projectPath, "UTF-8") ;
		writer.println("<PROJECT>");
		boolean first = true;
		for(String aadlModelPath: aadlModelsPath)
		{
			String opened = "false";
			if(first)
			{
				opened = "true";
				first = false;
			}
			writer.println("<AADL path=\""+aadlModelPath+"\""+" enabled=\"true\" opened=\""+opened+"\"/>");
		}
		writer.println("</PROJECT>");
		writer.close();


	}
	
} //AadlInspectorProjectCreatorImpl
