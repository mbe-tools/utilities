/**
 */
package fr.mem4csd.analysis.workflowaadlinspector.impl;

import de.mdelab.workflow.WorkflowPackage;

import de.mdelab.workflow.components.ComponentsPackage;

import fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator;
import fr.mem4csd.analysis.workflowaadlinspector.WorkflowaadlinspectorFactory;
import fr.mem4csd.analysis.workflowaadlinspector.WorkflowaadlinspectorPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowaadlinspectorPackageImpl extends EPackageImpl implements WorkflowaadlinspectorPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aadlInspectorProjectCreatorEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.analysis.workflowaadlinspector.WorkflowaadlinspectorPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorkflowaadlinspectorPackageImpl() {
		super(eNS_URI, WorkflowaadlinspectorFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link WorkflowaadlinspectorPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorkflowaadlinspectorPackage init() {
		if (isInited) return (WorkflowaadlinspectorPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowaadlinspectorPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWorkflowaadlinspectorPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		WorkflowaadlinspectorPackageImpl theWorkflowaadlinspectorPackage = registeredWorkflowaadlinspectorPackage instanceof WorkflowaadlinspectorPackageImpl ? (WorkflowaadlinspectorPackageImpl)registeredWorkflowaadlinspectorPackage : new WorkflowaadlinspectorPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		WorkflowPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorkflowaadlinspectorPackage.createPackageContents();

		// Initialize created meta-data
		theWorkflowaadlinspectorPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWorkflowaadlinspectorPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorkflowaadlinspectorPackage.eNS_URI, theWorkflowaadlinspectorPackage);
		return theWorkflowaadlinspectorPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAadlInspectorProjectCreator() {
		return aadlInspectorProjectCreatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAadlInspectorProjectCreator_AadlModelSlot() {
		return (EAttribute)aadlInspectorProjectCreatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAadlInspectorProjectCreator_OutputDirectory() {
		return (EAttribute)aadlInspectorProjectCreatorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAadlInspectorProjectCreator_AicFileName() {
		return (EAttribute)aadlInspectorProjectCreatorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WorkflowaadlinspectorFactory getWorkflowaadlinspectorFactory() {
		return (WorkflowaadlinspectorFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		aadlInspectorProjectCreatorEClass = createEClass(AADL_INSPECTOR_PROJECT_CREATOR);
		createEAttribute(aadlInspectorProjectCreatorEClass, AADL_INSPECTOR_PROJECT_CREATOR__AADL_MODEL_SLOT);
		createEAttribute(aadlInspectorProjectCreatorEClass, AADL_INSPECTOR_PROJECT_CREATOR__OUTPUT_DIRECTORY);
		createEAttribute(aadlInspectorProjectCreatorEClass, AADL_INSPECTOR_PROJECT_CREATOR__AIC_FILE_NAME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ComponentsPackage theComponentsPackage = (ComponentsPackage)EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		aadlInspectorProjectCreatorEClass.getESuperTypes().add(theComponentsPackage.getWorkflowComponent());

		// Initialize classes, features, and operations; add parameters
		initEClass(aadlInspectorProjectCreatorEClass, AadlInspectorProjectCreator.class, "AadlInspectorProjectCreator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAadlInspectorProjectCreator_AadlModelSlot(), ecorePackage.getEString(), "aadlModelSlot", null, 1, 1, AadlInspectorProjectCreator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAadlInspectorProjectCreator_OutputDirectory(), ecorePackage.getEString(), "outputDirectory", null, 1, 1, AadlInspectorProjectCreator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAadlInspectorProjectCreator_AicFileName(), ecorePackage.getEString(), "aicFileName", null, 1, 1, AadlInspectorProjectCreator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //WorkflowaadlinspectorPackageImpl
