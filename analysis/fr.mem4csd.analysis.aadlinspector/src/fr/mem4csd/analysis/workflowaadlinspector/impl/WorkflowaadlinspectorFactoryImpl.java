/**
 */
package fr.mem4csd.analysis.workflowaadlinspector.impl;

import fr.mem4csd.analysis.workflowaadlinspector.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowaadlinspectorFactoryImpl extends EFactoryImpl implements WorkflowaadlinspectorFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WorkflowaadlinspectorFactory init() {
		try {
			WorkflowaadlinspectorFactory theWorkflowaadlinspectorFactory = (WorkflowaadlinspectorFactory)EPackage.Registry.INSTANCE.getEFactory(WorkflowaadlinspectorPackage.eNS_URI);
			if (theWorkflowaadlinspectorFactory != null) {
				return theWorkflowaadlinspectorFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WorkflowaadlinspectorFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowaadlinspectorFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case WorkflowaadlinspectorPackage.AADL_INSPECTOR_PROJECT_CREATOR: return createAadlInspectorProjectCreator();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AadlInspectorProjectCreator createAadlInspectorProjectCreator() {
		AadlInspectorProjectCreatorImpl aadlInspectorProjectCreator = new AadlInspectorProjectCreatorImpl();
		return aadlInspectorProjectCreator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WorkflowaadlinspectorPackage getWorkflowaadlinspectorPackage() {
		return (WorkflowaadlinspectorPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WorkflowaadlinspectorPackage getPackage() {
		return WorkflowaadlinspectorPackage.eINSTANCE;
	}

} //WorkflowaadlinspectorFactoryImpl
