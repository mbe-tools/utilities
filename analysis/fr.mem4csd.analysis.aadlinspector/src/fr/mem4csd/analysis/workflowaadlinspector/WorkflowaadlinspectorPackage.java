/**
 */
package fr.mem4csd.analysis.workflowaadlinspector;

import de.mdelab.workflow.components.ComponentsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.analysis.workflowaadlinspector.WorkflowaadlinspectorFactory
 * @model kind="package"
 * @generated
 */
public interface WorkflowaadlinspectorPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workflowaadlinspector";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/analysis/aadlinspector";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workflowaadlinspector";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowaadlinspectorPackage eINSTANCE = fr.mem4csd.analysis.workflowaadlinspector.impl.WorkflowaadlinspectorPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.analysis.workflowaadlinspector.impl.AadlInspectorProjectCreatorImpl <em>Aadl Inspector Project Creator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.analysis.workflowaadlinspector.impl.AadlInspectorProjectCreatorImpl
	 * @see fr.mem4csd.analysis.workflowaadlinspector.impl.WorkflowaadlinspectorPackageImpl#getAadlInspectorProjectCreator()
	 * @generated
	 */
	int AADL_INSPECTOR_PROJECT_CREATOR = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSPECTOR_PROJECT_CREATOR__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSPECTOR_PROJECT_CREATOR__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSPECTOR_PROJECT_CREATOR__ENABLED = ComponentsPackage.WORKFLOW_COMPONENT__ENABLED;

	/**
	 * The feature id for the '<em><b>Aadl Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSPECTOR_PROJECT_CREATOR__AADL_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSPECTOR_PROJECT_CREATOR__OUTPUT_DIRECTORY = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Aic File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSPECTOR_PROJECT_CREATOR__AIC_FILE_NAME = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Aadl Inspector Project Creator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSPECTOR_PROJECT_CREATOR_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSPECTOR_PROJECT_CREATOR___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int AADL_INSPECTOR_PROJECT_CREATOR___EXECUTE__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSPECTOR_PROJECT_CREATOR___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSPECTOR_PROJECT_CREATOR___CHECK_CANCELED__IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Aadl Inspector Project Creator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSPECTOR_PROJECT_CREATOR_OPERATION_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator <em>Aadl Inspector Project Creator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aadl Inspector Project Creator</em>'.
	 * @see fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator
	 * @generated
	 */
	EClass getAadlInspectorProjectCreator();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator#getAadlModelSlot <em>Aadl Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Aadl Model Slot</em>'.
	 * @see fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator#getAadlModelSlot()
	 * @see #getAadlInspectorProjectCreator()
	 * @generated
	 */
	EAttribute getAadlInspectorProjectCreator_AadlModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator#getOutputDirectory <em>Output Directory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Directory</em>'.
	 * @see fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator#getOutputDirectory()
	 * @see #getAadlInspectorProjectCreator()
	 * @generated
	 */
	EAttribute getAadlInspectorProjectCreator_OutputDirectory();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator#getAicFileName <em>Aic File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Aic File Name</em>'.
	 * @see fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator#getAicFileName()
	 * @see #getAadlInspectorProjectCreator()
	 * @generated
	 */
	EAttribute getAadlInspectorProjectCreator_AicFileName();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkflowaadlinspectorFactory getWorkflowaadlinspectorFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.analysis.workflowaadlinspector.impl.AadlInspectorProjectCreatorImpl <em>Aadl Inspector Project Creator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.analysis.workflowaadlinspector.impl.AadlInspectorProjectCreatorImpl
		 * @see fr.mem4csd.analysis.workflowaadlinspector.impl.WorkflowaadlinspectorPackageImpl#getAadlInspectorProjectCreator()
		 * @generated
		 */
		EClass AADL_INSPECTOR_PROJECT_CREATOR = eINSTANCE.getAadlInspectorProjectCreator();

		/**
		 * The meta object literal for the '<em><b>Aadl Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AADL_INSPECTOR_PROJECT_CREATOR__AADL_MODEL_SLOT = eINSTANCE.getAadlInspectorProjectCreator_AadlModelSlot();

		/**
		 * The meta object literal for the '<em><b>Output Directory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AADL_INSPECTOR_PROJECT_CREATOR__OUTPUT_DIRECTORY = eINSTANCE.getAadlInspectorProjectCreator_OutputDirectory();

		/**
		 * The meta object literal for the '<em><b>Aic File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AADL_INSPECTOR_PROJECT_CREATOR__AIC_FILE_NAME = eINSTANCE.getAadlInspectorProjectCreator_AicFileName();

	}

} //WorkflowaadlinspectorPackage
