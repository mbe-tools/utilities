/**
 */
package fr.mem4csd.analysis.workflowaadlinspector;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aadl Inspector Project Creator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator#getAadlModelSlot <em>Aadl Model Slot</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator#getOutputDirectory <em>Output Directory</em>}</li>
 *   <li>{@link fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator#getAicFileName <em>Aic File Name</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.analysis.workflowaadlinspector.WorkflowaadlinspectorPackage#getAadlInspectorProjectCreator()
 * @model
 * @generated
 */
public interface AadlInspectorProjectCreator extends WorkflowComponent {
	/**
	 * Returns the value of the '<em><b>Aadl Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aadl Model Slot</em>' attribute.
	 * @see #setAadlModelSlot(String)
	 * @see fr.mem4csd.analysis.workflowaadlinspector.WorkflowaadlinspectorPackage#getAadlInspectorProjectCreator_AadlModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getAadlModelSlot();

	/**
	 * Sets the value of the '{@link fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator#getAadlModelSlot <em>Aadl Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aadl Model Slot</em>' attribute.
	 * @see #getAadlModelSlot()
	 * @generated
	 */
	void setAadlModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Output Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Directory</em>' attribute.
	 * @see #setOutputDirectory(String)
	 * @see fr.mem4csd.analysis.workflowaadlinspector.WorkflowaadlinspectorPackage#getAadlInspectorProjectCreator_OutputDirectory()
	 * @model required="true"
	 * @generated
	 */
	String getOutputDirectory();

	/**
	 * Sets the value of the '{@link fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator#getOutputDirectory <em>Output Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Directory</em>' attribute.
	 * @see #getOutputDirectory()
	 * @generated
	 */
	void setOutputDirectory(String value);

	/**
	 * Returns the value of the '<em><b>Aic File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aic File Name</em>' attribute.
	 * @see #setAicFileName(String)
	 * @see fr.mem4csd.analysis.workflowaadlinspector.WorkflowaadlinspectorPackage#getAadlInspectorProjectCreator_AicFileName()
	 * @model required="true"
	 * @generated
	 */
	String getAicFileName();

	/**
	 * Sets the value of the '{@link fr.mem4csd.analysis.workflowaadlinspector.AadlInspectorProjectCreator#getAicFileName <em>Aic File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aic File Name</em>' attribute.
	 * @see #getAicFileName()
	 * @generated
	 */
	void setAicFileName(String value);

} // AadlInspectorProjectCreator
