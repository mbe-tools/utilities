/**
 */
package fr.mem4csd.analysis.workflowaadlinspector;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.analysis.workflowaadlinspector.WorkflowaadlinspectorPackage
 * @generated
 */
public interface WorkflowaadlinspectorFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowaadlinspectorFactory eINSTANCE = fr.mem4csd.analysis.workflowaadlinspector.impl.WorkflowaadlinspectorFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Aadl Inspector Project Creator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aadl Inspector Project Creator</em>'.
	 * @generated
	 */
	AadlInspectorProjectCreator createAadlInspectorProjectCreator();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WorkflowaadlinspectorPackage getWorkflowaadlinspectorPackage();

} //WorkflowaadlinspectorFactory
