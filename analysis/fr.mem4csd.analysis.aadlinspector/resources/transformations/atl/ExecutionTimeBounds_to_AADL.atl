--
-- RAMSES 2.0
-- 
-- Copyright © 2012-2015 TELECOM Paris and CNRS
-- Copyright © 2016-2020 TELECOM Paris
-- 
-- TELECOM Paris
-- 
-- Authors: see AUTHORS
--
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program. 
-- If not, see https://www.eclipse.org/legal/epl-2.0/
--

-- @nsURI AADLBA=/fr.tpt.aadl.annex.behavior/model/aadlba.ecore
-- @atlcompiler emftvm

module ExecTimeBounds_To_AADL;
create OUT : AADLBA 	from 		IN: AADLI,
									ETB: EXECUTIONTIMEBOUNDS;

helper context AADLI!InstanceObject def : collectSelfPropertyAssociationImg() : Sequence(AADLBA!PropertyAssociation) =
	self.ownedPropertyAssociation->select(e | not e.property.name.equalsIgnoreCase('Compute_Entrypoint_Call_Sequence')
									)->collectSelfPropertyAssociationImg()
									->append(self.isRootSystem())
									->excluding(OclUndefined)
;

helper def: baseUnit: String = 'ms';

rule m_RootSystemInstance
{
	from
		systemExecutionTimeBound: EXECUTIONTIMEBOUNDS!TaskSet in ETB
	using
	{
		s: AADLI!SystemInstance = systemExecutionTimeBound.tasks->first().aadlThreadInstance.getRootSystemInstance();
		sImpl: AADLBA!SystemImplementation = s.componentImplementation;
		sPkg: AADLBA!AadlPackage = sImpl.eContainer().eContainer();
	}
	to
		pkg: AADLBA!AadlPackage
		(
			name <- sPkg.name + '_Execution_Time_Bounds',
			ownedPublicSection <- section
		),
		section : AADLBA!PublicPackageSection
		(
			ownedClassifier <- Sequence{sys, sysImpl}
		),
		sys:  AADLBA!SystemType
		(
			name <- sImpl.type.name,
			ownedExtension <- systemTypeExtension
		),
		systemTypeExtension: AADLBA!TypeExtension
		(
			extended <- sImpl.type
		),
		sysImpl:  AADLBA!SystemImplementation
		(
			name <- sImpl.type.name+'.etb', -- must be the same as sys
			ownedRealization <- thisModule.Realization(sys),
			ownedPropertyAssociation <- systemExecutionTimeBound.tasks->collect(e | e.h_Task_To_PropertyAssociationList())
										->flatten()->excluding(OclUndefined)
										->append(
											thisModule.CreatePropertyAssociation('Root_System', 'AI', thisModule.CreateStringLiteralPropertyExpression('selected'))
										),
			ownedExtension <- systemImplExtension
		),
		systemImplExtension: AADLBA!ImplementationExtension
		(
			extended <- sImpl
		)
	do
	{
		thisModule.addImportedUnit(thisModule.public(), 'AI');
		thisModule.addImportedUnitFromInputModel(thisModule.public(), sPkg);
		section.importedUnit <- thisModule.allImportedUnits->flatten()->asSet()->asSequence()->excluding(OclUndefined);
	}
}

helper context EXECUTIONTIMEBOUNDS!Task def: h_Task_To_PropertyAssociationList() : Sequence(AADLI!PropertyAssociation) = 
	Sequence{thisModule.ulr_Task_To_PropertyAssociation(self)}
	->union(
		self.criticalSections->collect(e | thisModule.ulr_CriticalSection_To_PropertyAssociation(e))
	)->flatten()
;

lazy rule ulr_Task_To_PropertyAssociation
{
	from
		t: EXECUTIONTIMEBOUNDS!Task
	using
	{
		ti: AADLI!ComponentInstance = t.aadlThreadInstance;
		l : Sequence(AADLI!InstanceObject) = ti.getContainmentPath(ti.getRootSystemInstance());
	}
	to
		computeExecutionTimePA: AADLBA!PropertyAssociation
		(
			property  <- 'Compute_Execution_Time'.asProperty('TIMING_PROPERTIES'),
	   	    ownedValue <- Sequence{thisModule.createTimeRangePropertyValue(t.bestCaseExecutionTime, t.worstCaseExecutionTime, 'ms')},
			appliesTo <- Sequence{cne}
		),
		cne: AADLBA!ContainedNamedElement (
			path <- thisModule.createContainmentPathElement(l)
		)
	do
	{
		computeExecutionTimePA;
	}
}

lazy rule ulr_CriticalSection_To_PropertyAssociation
{
	from 
		crit: EXECUTIONTIMEBOUNDS!CriticalSection
	using
	{
		da: AADLI!FeatureInstance = crit.aadlDataAccess;
		l : Sequence(AADLI!InstanceObject) = da.getContainmentPath(da.getRootSystemInstance());
	}
	to
		res: AADLBA!PropertyAssociation
		(
			property <- 'Critical_Section_Time'.asProperty('CHEDDAR_PROPERTIES'),
			ownedValue <- Sequence {thisModule.createTimeRangePropertyValue(crit.bestCaseCriticalSectionTime.longValue(), crit.worstCaseCriticalSectionTime.longValue(), thisModule.baseUnit)},
			appliesTo <- Sequence{cne}
		),
		cne: AADLBA!ContainedNamedElement (
			path <- thisModule.createContainmentPathElement(l)
		)
	do
	{
		thisModule.addImportedUnit(thisModule.public(), 'Cheddar_properties');
		res;
	}
}
