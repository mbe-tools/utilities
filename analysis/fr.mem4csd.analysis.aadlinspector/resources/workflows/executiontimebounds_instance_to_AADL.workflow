<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowemftvm="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowemftvm" xmi:id="_4VytwLbYEeqC0OmnAei34g" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_aC5IcLcDEeqC0OmnAei34g" name="workflowDelegation" workflowURI="${scheme}${atl_transformation_utilities_plugin}workflows/load_atl_transformation_utils.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="__yShQLboEeqkNY6l67PJRQ" name="modelReader" modelSlot="AI" modelURI="${scheme}${aadlinspector_plugin}AADLInspector/AI.aadl"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_FF4Q0LoVEeqvxv7A6LYpag" name="modelReader" modelSlot="CHEDDAR_PROPERTIES" modelURI="${scheme}${aadlinspector_plugin}Cheddar/Cheddar_Properties.aadl"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_XhM8oLoVEeqvxv7A6LYpag" name="modelReader" modelSlot="executionTimeBoundsModel" modelURI="${executiontimebounds_model_file}"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_3hCmQLopEeqWqckwg9f5bA" name="modelReader" modelSlot="ExecutionTimeBounds_to_AADL" modelURI="${scheme}${aadlinspector_plugin}transformations/atl/ExecutionTimeBounds_to_AADL.emftvm"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_NbiYkLozEeqWqckwg9f5bA" name="modelReader" modelSlot="execTimeAtlHelper" modelURI="${scheme}${aadlinspector_plugin}workflows/exectime_refinement.helpers"/>
  <components xsi:type="workflowemftvm:EmftVmTransformer" xmi:id="_fUDqYLoUEeqvxv7A6LYpag" name="emftVmTransformer" registerDependencyModels="true" discardExtraRootElements="true">
    <rulesModelSlot>IOHelpers</rulesModelSlot>
    <rulesModelSlot>AADLCopyHelpers</rulesModelSlot>
    <rulesModelSlot>AADLICopyHelpers</rulesModelSlot>
    <rulesModelSlot>BehaviorAnnexServices</rulesModelSlot>
    <rulesModelSlot>PropertiesTools</rulesModelSlot>
    <rulesModelSlot>PackagesTools</rulesModelSlot>
    <rulesModelSlot>FeaturesTools</rulesModelSlot>
    <rulesModelSlot>Services</rulesModelSlot>
    <rulesModelSlot>ExecutionTimeBounds_to_AADL</rulesModelSlot>
    <inputModels xmi:id="_K0PqEazpEeqYtKBdcZJ7ng" modelName="IN" modelSlot="srcInstanceModel" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_-hU-sLoUEeqvxv7A6LYpag" modelName="AI" modelSlot="AI" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba"/>
    <inputModels xmi:id="_LfT4ELoVEeqvxv7A6LYpag" modelName="CHEDDAR_PROPERTIES" modelSlot="CHEDDAR_PROPERTIES" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba"/>
    <inputModels xmi:id="_St8pALoVEeqvxv7A6LYpag" modelName="ETB" modelSlot="executionTimeBoundsModel" metamodelName="EXECUTIONTIMEBOUNDS" metamodelURI="http://mem4csd.telecom-paris.fr/analysis/executiontimebounds"/>
    <inputModels xmi:id="_leTOALozEeqWqckwg9f5bA" modelName="TOOLS" modelSlot="execTimeAtlHelper" metamodelName="ATLHELPER" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/helpers"/>
    <inputOutputModels xmi:id="_K0PqKazpEeqYtKBdcZJ7ng" modelName="OUT" modelSlot="aadlModelWithExecutionTimeBounds" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba"/>
  </components>
  <components xsi:type="workflow.components:ModelWriter" xmi:id="_-kNeILoVEeqvxv7A6LYpag" name="modelWriter" modelSlot="aadlModelWithExecutionTimeBounds" modelURI="${output_dir}/${output_model_file}"/>
  <properties xmi:id="_oVmo0LbyEeqkNY6l67PJRQ" name="scheme">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_IT_WILoWEeqvxv7A6LYpag" name="executiontimebounds_model_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_MKBMgLoWEeqvxv7A6LYpag" name="output_model_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_LnzPoCD1EeuqPYYqL8wSRw" fileURI="default_AI.properties"/>
</workflow:Workflow>
