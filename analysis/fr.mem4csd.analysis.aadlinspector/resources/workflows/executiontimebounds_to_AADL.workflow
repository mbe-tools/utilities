<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowosate="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowosate" xmi:id="_-Vgo4LoXEeqvxv7A6LYpag" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_m-XMMLorEeqWqckwg9f5bA" name="workflowDelegation" workflowURI="${scheme}${executiontime_plugin}workflows/load_osate_aadl_contributions.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_mbAiILoYEeqvxv7A6LYpag" name="modelReader" modelSlot="aadlSourceModel" modelURI="${source_aadl_file}"/>
  <components xsi:type="workflowosate:AadlInstanceModelCreator" xmi:id="_I4dDMLoYEeqvxv7A6LYpag" name="aadlInstanceModelCreator" systemImplementationName="${system_implementation_name}" packageModelSlot="aadlSourceModel" systemInstanceModelSlot="srcInstanceModel"/>
  <components xsi:type="workflow.components:ModelWriter" xmi:id="_-GAkwLpNEeqWqckwg9f5bA" name="modelWriter" modelSlot="srcInstanceModel" modelURI="${instance_model_file}" unloadAfter="true"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_LwMFULoYEeqvxv7A6LYpag" name="workflowDelegation" workflowURI="${scheme}${aadlinspector_plugin}workflows/executiontimebounds_instance_to_AADL.workflow">
    <propertyValues xmi:id="_mFDY4LobEeqvxv7A6LYpag" name="executiontimebounds_model_file" defaultValue="${executiontimebounds_model_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_qocEgLobEeqvxv7A6LYpag" name="output_model_file" defaultValue="${output_model_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_2KH8kLqrEeqWqckwg9f5bA" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="__Xd1ILoXEeqvxv7A6LYpag" name="source_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_EXB_ELobEeqvxv7A6LYpag" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_WXNnALobEeqvxv7A6LYpag" name="scheme" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_kTU8kLobEeqvxv7A6LYpag" name="executiontimebounds_model_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_pR5IULobEeqvxv7A6LYpag" name="output_model_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_9QiEMLofEeqWqckwg9f5bA" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_QeILkLpOEeqWqckwg9f5bA" name="instance_model_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_yeaiQLobEeqvxv7A6LYpag" fileURI="default_AI.properties"/>
  <allProperties xmi:id="_WAujkCD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_WAujkSD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_WAujkiD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_WAujkyD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_WAujlCD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_WAujlSD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_WAujliD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_WAujlyD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_WIsMwCD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_WIsMwSD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_WIsMwiD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_WIsMwyD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_WIsMxCD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_WIsMxSD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_WIsMxiD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_WIsMxyD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_WRLaYCD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_WRLaYSD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_WRLaYiD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_WRLaYyD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_WRLaZCD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_WRLaZSD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_WRLaZiD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_WRLaZyD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_WZtEQCD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_WZtEQSD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_WZtEQiD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_WZtEQyD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_WZtrUCD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_WZtrUSD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_WZtrUiD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_WZtrUyD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_YB3U4CD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_YB3U4SD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_YB3U4iD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_YB3U4yD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_YB3U5CD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_YB3U5SD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_YB3U5iD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_YB378CD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_YH2oICD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_YH2oISD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_YH2oIiD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_YH2oIyD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_YH3PMCD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_YH3PMSD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_YH3PMiD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_YH3PMyD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_YkvYcCD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_YkvYcSD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_YkvYciD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_YkvYcyD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_YkvYdCD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_YkvYdSD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_YkvYdiD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_YkvYdyD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_YxxH0CD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_YxxH0SD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_YxxH0iD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_YxxH0yD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
  <allProperties xmi:id="_Yxxu4CD7EeuqPYYqL8wSRw" name="is_gui_mode" defaultValue="true"/>
  <allProperties xmi:id="_Yxxu4SD7EeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_Yxxu4iD7EeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_Yxxu4yD7EeuqPYYqL8wSRw" name="aadlinspector_plugin" defaultValue="fr.mem4csd.analysis.aadlinspector/"/>
</workflow:Workflow>
