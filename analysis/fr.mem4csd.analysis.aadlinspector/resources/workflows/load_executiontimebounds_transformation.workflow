<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_y4cUQLbYEeqC0OmnAei34g" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_2PHGYLbeEeqkNY6l67PJRQ" name="workflowDelegation" workflowURI="${scheme}${atl_transformation_utilities_plugin}workflows/load_atl_transformation_utils.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_chg8QLbdEeqkNY6l67PJRQ" name="modelReader" modelSlot="ExecutionTimeBounds_to_AADL" modelURI="${scheme}${aadlinspector_plugin}/transformations/atl/ExecutionTimeBounds_to_AADL.emfvtm"/>
  <properties xmi:id="_UGqvgLbgEeqkNY6l67PJRQ" name="scheme">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_Wt7ssLbgEeqkNY6l67PJRQ" name="aadlinspector_plugin">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_bwMssLbgEeqkNY6l67PJRQ" name="atl_transformation_utilities_plugin">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
</workflow:Workflow>
