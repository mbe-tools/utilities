<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_V44NoLbYEeqC0OmnAei34g" name="workflow">
  <components xsi:type="workflow.components:SystemCommandComponent" xmi:id="_q_KjgLbbEeqkNY6l67PJRQ" name="systemCommandComponent" command="${aadlinspector_exec_file}">
    <arguments>-a</arguments>
    <arguments>${aic_project_file}</arguments>
    <arguments>--plugin</arguments>
    <arguments>schedulability.cheddarTheoTest</arguments>
    <arguments>--result</arguments>
    <arguments>${output_dir}/${output_file_name}.xml</arguments>
    <arguments>--show</arguments>
    <arguments>${is_gui_mode}</arguments>
  </components>
  <properties xmi:id="_gneW8LbbEeqkNY6l67PJRQ" name="aadlinspector_exec_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_nebAoLbbEeqkNY6l67PJRQ" name="aic_project_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_OV2kQLbcEeqkNY6l67PJRQ" name="output_file_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_9wQOgLbcEeqkNY6l67PJRQ" name="is_gui_mode" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
</workflow:Workflow>
