<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_RiyygLrbEeq7I8XSmsRFVg" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_V4bWALrbEeq7I8XSmsRFVg" name="workflowDelegation" workflowURI="${scheme}${executiontime_plugin}workflows/executiontime_analysis.workflow">
    <propertyValues xmi:id="_-eNR0LrbEeq7I8XSmsRFVg" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_d1fdwLrcEeq7I8XSmsRFVg" name="system_implementation_name" defaultValue="${system_implementation_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_DbRzULrdEeq7I8XSmsRFVg" name="execution_time_bounds_file_name" defaultValue="${intermediate_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_3ZRdMLrgEeq7I8XSmsRFVg" name="source_aadl_file" defaultValue="${source_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_Nh-hELreEeq7I8XSmsRFVg" name="workflowDelegation" workflowURI="${scheme}${aadlinspector_plugin}workflows/executiontimebounds_instance_to_AADL.workflow">
    <propertyValues xmi:id="_eq2GgLreEeq7I8XSmsRFVg" name="executiontimebounds_model_file" defaultValue="${output_dir}/${intermediate_file_name}.executiontimebounds">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_pLcXcLreEeq7I8XSmsRFVg" name="output_model_file" defaultValue="${intermediate_file_name}.aadl">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_Wn2ZELrhEeq7I8XSmsRFVg" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_WjGqsLrbEeq7I8XSmsRFVg" name="workflowDelegation" workflowURI="${scheme}${aadlinspector_plugin}workflows/execute_responsetimeanalysis.workflow">
    <propertyValues xmi:id="_79aE0LriEeq7I8XSmsRFVg" name="aadlinspector_exec_file" defaultValue="${aadlinspector_exec_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_-jTJALriEeq7I8XSmsRFVg" name="source_aadl_file" defaultValue="${output_dir}/${intermediate_file_name}.aadl"/>
    <propertyValues xmi:id="_GlQ0ULrjEeq7I8XSmsRFVg" name="is_gui_mode" defaultValue="${is_gui_mode}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_MPtOoLrjEeq7I8XSmsRFVg" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_N3BKsLrjEeq7I8XSmsRFVg" name="aic_file_name" defaultValue="${intermediate_file_name}.aic">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_Xwp3YLrlEeq7I8XSmsRFVg" name="output_file_name" defaultValue="${intermediate_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_T8Q5YLrdEeq7I8XSmsRFVg" name="intermediate_file_name" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_u3fboLrbEeq7I8XSmsRFVg" name="source_aadl_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_zfzqkLrbEeq7I8XSmsRFVg" name="aadlinspector_exec_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="__4_00LrbEeq7I8XSmsRFVg" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_gHBbgLrcEeq7I8XSmsRFVg" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_q-l3cLrbEeq7I8XSmsRFVg" fileURI="default_AI.properties"/>
</workflow:Workflow>
