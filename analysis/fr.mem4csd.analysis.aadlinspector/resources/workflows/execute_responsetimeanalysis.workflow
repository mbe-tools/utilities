<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowaadlinspector="https://mem4csd.telecom-paris.fr/analysis/aadlinspector" xmi:id="_CTEPMLbZEeqC0OmnAei34g" name="workflow">
  <components xsi:type="workflow.components:ModelReader" xmi:id="_I8zjULbZEeqC0OmnAei34g" name="modelReader" modelSlot="srcAadlModel" modelURI="${source_aadl_file}"/>
  <components xsi:type="workflowaadlinspector:AadlInspectorProjectCreator" xmi:id="_VIJKkLrFEeqt_4v8_O6zBg" name="aadlInspectorProjectCreator" aadlModelSlot="srcAadlModel" outputDirectory="${output_dir}" aicFileName="${aic_file_name}"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_KH8C8Lq5EeqWFZ5Sat-3HA" name="workflowDelegation" workflowURI="${scheme}${aadlinspector_plugin}workflows/launch_AADLInspector_from_aic_project.workflow">
    <propertyValues xmi:id="_lBJEkLq6Eeqt_4v8_O6zBg" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_m7TEILq6Eeqt_4v8_O6zBg" name="aadlinspector_exec_file" defaultValue="${aadlinspector_exec_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_rtj5QLq6Eeqt_4v8_O6zBg" name="aic_project_file" defaultValue="${output_dir}/${aic_file_name}.aic">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_Fome8Lq7Eeqt_4v8_O6zBg" name="is_gui_mode" defaultValue="${is_gui_mode}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_ZZ6GQLq7Eeqt_4v8_O6zBg" name="output_file_name" defaultValue="${output_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_1jwXwLq5EeqWFZ5Sat-3HA" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_30vi8Lq5EeqWFZ5Sat-3HA" name="source_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_8sCCALq5EeqWFZ5Sat-3HA" name="aadlinspector_plugin">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_BtmecLq6EeqWFZ5Sat-3HA" name="aic_file_name"/>
  <properties xmi:id="_7fUh0Lq6Eeqt_4v8_O6zBg" name="aadlinspector_exec_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_CI4PwLq7Eeqt_4v8_O6zBg" name="is_gui_mode" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_ZtEOoLrCEeqt_4v8_O6zBg" name="output_file_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_Aj2rULq-Eeqt_4v8_O6zBg" fileURI="default_AI.properties"/>
  <allProperties xmi:id="_6Q-OECDxEeuqPYYqL8wSRw" name="scheme" defaultValue="platform:/plugin/"/>
  <allProperties xmi:id="_6Q-OESDxEeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_6Q-OEiDxEeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_6Q-1ICDxEeuqPYYqL8wSRw" name="scheme" defaultValue="platform:/plugin/"/>
  <allProperties xmi:id="_6Q-1ISDxEeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_6Q-1IiDxEeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_6RXPoCDxEeuqPYYqL8wSRw" name="scheme" defaultValue="platform:/plugin/"/>
  <allProperties xmi:id="_6RXPoSDxEeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_6RX2sCDxEeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_7BGqgCDxEeuqPYYqL8wSRw" name="scheme" defaultValue="platform:/plugin/"/>
  <allProperties xmi:id="_7BGqgSDxEeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_7BGqgiDxEeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
  <allProperties xmi:id="_7tIqECDxEeuqPYYqL8wSRw" name="scheme" defaultValue="platform:/plugin/"/>
  <allProperties xmi:id="_7tIqESDxEeuqPYYqL8wSRw" name="atl_transformation_utilities_plugin" defaultValue="fr.mem4csd.transformations.utils.atl/"/>
  <allProperties xmi:id="_7tIqEiDxEeuqPYYqL8wSRw" name="executiontime_plugin" defaultValue="fr.mem4csd.analysis.executiontime/"/>
</workflow:Workflow>
