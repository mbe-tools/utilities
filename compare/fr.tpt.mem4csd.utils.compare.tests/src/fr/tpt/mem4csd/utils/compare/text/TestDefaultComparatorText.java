package fr.tpt.mem4csd.utils.compare.text;

import org.junit.Test;

import fr.tpt.mem4csd.utils.compare.IComparator;
import fr.tpt.mem4csd.utils.compare.tests.AbstractTestCompare;

public class TestDefaultComparatorText extends AbstractTestCompare {
	
	private static final String[] NULL_DIFFERENCES = new String[ 0 ];

	public TestDefaultComparatorText() {
		super();
	}

	@Override
	protected IComparator createComparator() {
		return new DefaultComparatorText();
	}

	@Test
	public void testIdenticalFiles() {
		testIdenticalFiles( "codeLeft.c" );
	}

	@Test
	public void testIdenticalUniformFile() {
		testIdenticalFiles( "uniformFile.txt");
	}

	@Test
	public void testIdenticalFilesOneExtraLine() {
		testCompareDifferentFiles( 	"codeLeft.c", 
									"codeRight.c", 
									"Delete Old line(s) 78-78 78: 	fep_set_l(&X_CWL_ctx, ((FEP_BUFFER_TYPE*)sig[chip2octet_ch_out_CP].pBuff)->num_samples);" );
	}

	@Test
	public void testDifferentDirectories() {
		testCompareDifferentFiles( 	"output", 
									"output_ref", 
									NULL_DIFFERENCES );
	}
}
