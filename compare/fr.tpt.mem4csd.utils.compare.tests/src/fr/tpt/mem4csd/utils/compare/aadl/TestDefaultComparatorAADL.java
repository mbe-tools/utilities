package fr.tpt.mem4csd.utils.compare.aadl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.junit.Test;
import org.osate.annexsupport.AnnexRegistry;
import org.osate.ba.AadlBaParserAction;
import org.osate.ba.AadlBaResolver;
import org.osate.ba.AadlBaUnParserAction;
import org.osate.ba.texteditor.AadlBaSemanticHighlighter;
import org.osate.ba.texteditor.AadlBaTextPositionResolver;

import fr.tpt.mem4csd.utils.compare.IComparator;
import fr.tpt.mem4csd.utils.compare.tests.AbstractTestCompare;
import fr.tpt.mem4csd.utils.osate.standalone.OsateStandaloneSetup;
import fr.tpt.mem4csd.utils.osate.standalone.StandaloneAnnexRegistry.AnnexExtensionRegistration;

public class TestDefaultComparatorAADL extends AbstractTestCompare {
	
	private static final URI SHARED_RESOURCES_DIR = URI.createFileURI( "shared/" );
	
	private static final String[] PREDEFINED_AADL_RESOURCES_NAMES = { 	"pok_properties.aadl",
																		"ARINC653_runtime.aadl",
																		"RAMSES_processors.aadl",
																		"RAMSES_buses.aadl",
																		"common_pkg.aadl" };
	private static final Set<String> PREDEFINED_AADL_RESOURCES_NAMES_SET = Set.of(PREDEFINED_AADL_RESOURCES_NAMES);
	
	private ResourceSet resourceSet;
	
	public TestDefaultComparatorAADL() {
		super();
		
		//OsateStandaloneSetup.loadPredefinedAadlResources( SHARED_RESOURCES_DIR, PREDEFINED_AADL_RESOURCES_NAMES, resourceSet );
	}
//	
//	private void loadSharedResources() {
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "AADL_Project.aadl" ), true );
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "Data_Model.aadl" ), true );
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "Deployment_Properties.aadl" ), true );
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "Memory_Properties.aadl" ), true );
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "Programming_Properties.aadl" ), true );
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "Thread_Properties.aadl" ), true );
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "Timing_Properties.aadl" ), true );
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "pok_properties.aadl" ), true );
//
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "Base_Types.aadl" ), true );
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "ARINC653.aadl" ), true );
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "ARINC653_runtime.aadl" ), true );
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "RAMSES_buses.aadl" ), true );
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "RAMSES_processors.aadl" ), true );
//		resourceSet.getResource( URI.createFileURI( SHARED_RESOURCES_DIR + "common_pkg.aadl" ), true );
//	}

	@Override
	protected IComparator createComparator() {
		final List<AnnexExtensionRegistration> annexExtensions = new ArrayList<AnnexExtensionRegistration>();
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_PARSER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaParserAction.class ));
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_UNPARSER_EXT_ID, AadlBaUnParserAction.ANNEX_NAME, AadlBaUnParserAction.class ));
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_RESOLVER_EXT_ID, AadlBaResolver.ANNEX_NAME, AadlBaResolver.class ));
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_TEXTPOSITIONRESOLVER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaTextPositionResolver.class ));
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_HIGHLIGHTER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaSemanticHighlighter.class ));
		final OsateStandaloneSetup setUp = new OsateStandaloneSetup( annexExtensions );
		
		resourceSet = setUp.createResourceSet( SHARED_RESOURCES_DIR, PREDEFINED_AADL_RESOURCES_NAMES_SET );
		
		return new DefaultComparatorAADL( resourceSet );
	}

	@Test
	public void testIdenticalTextFiles() {
		testIdenticalFiles( "left.aadl" );
	}

	@Test
	public void testSyntactlyIdenticalFilesDiffCodeFormat() {
		testIdenticalFiles( "left.aadl", "right_diff_code_format.aadl" );
	}

	@Test
	public void testSyntactlyIdenticalFilesDiffCodeOrder() {
		testIdenticalFiles( "left.aadl", "right_diff_code_order.aadl" );
	}
}
