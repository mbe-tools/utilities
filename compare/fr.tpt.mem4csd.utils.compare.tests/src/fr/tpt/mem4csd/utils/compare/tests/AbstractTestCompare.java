package fr.tpt.mem4csd.utils.compare.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import fr.tpt.mem4csd.utils.compare.IComparator;
import fr.tpt.mem4csd.utils.compare.IComparisonReport;

public abstract class AbstractTestCompare {
	
	private final IComparator comparator;

	protected AbstractTestCompare() {
		comparator = createComparator();
	}
	
	protected abstract IComparator createComparator();
	
	protected IComparator getComparator() {
		return comparator;
	}
	
	protected void handleException( final Throwable th ) {
		th.printStackTrace();
		
		fail( th.getLocalizedMessage() );
	}
	
	protected void testCompareFiles( 	final String fileNameLeft,
										final String fileNameRight,
										final String failMessage ) {
		try {
			final File fileLeft = new File( fileNameLeft );
			final File fileRight = new File( fileNameRight );
			
			final IComparisonReport report = getComparator().compare( fileLeft, fileRight );
			
			if ( report.containsDiff() ) {
				report.print();
				
				fail( failMessage );
			}
		}
		catch ( final IOException ex ) {
			handleException( ex );
		}
	}
	
	protected void testIdenticalFiles( final String fileName ) {
		testIdenticalFiles( fileName, fileName );
	}
	
	protected void testIdenticalFiles( 	final String fileNameLeft,
										final String fileNameRight ) {
		testCompareFiles( fileNameLeft, fileNameRight, "Equal files are found to be different!" );
	}
	
	protected void testCompareDifferentFiles( 	final String fileNameLeft,
												final String fileNameRight,
												final String... expectedDifferences ) {
		try {
			final File fileLeft = new File( fileNameLeft );
			final File fileRight = new File( fileNameRight );
			
			final IComparisonReport report = getComparator().compare( fileLeft, fileRight );
			
			if ( report.containsDiff() ) {
				report.print( System.out );
				
				if ( expectedDifferences == null || expectedDifferences.length != report.getEditCommands().size() ) {
					final int nbExpectedDiff = expectedDifferences == null ? 0 : expectedDifferences.length;
					
					fail( "Found " + report.getEditCommands().size() + " differences while expecting " + nbExpectedDiff + " differences!" );
				}
				
				int index = 0;
				
				for ( final String difference : expectedDifferences ) {
					assertEquals( difference, report.getEditCommands().get( index ).toString() );
				}
			}
			else if ( expectedDifferences != null && expectedDifferences.length != 0 ){
				fail( "Different files are found to be the same!" );
			}
		}
		catch ( final IOException ex ) {
			handleException( ex );
		}
	}
}
