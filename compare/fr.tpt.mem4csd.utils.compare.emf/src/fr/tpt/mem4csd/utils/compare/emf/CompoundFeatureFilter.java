package fr.tpt.mem4csd.utils.compare.emf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EStructuralFeature;

public class CompoundFeatureFilter extends ComposableFeatureFilter {
	
	private final List<ComposableFeatureFilter> featureFilters;

	public CompoundFeatureFilter( final Collection<ComposableFeatureFilter> p_featureFilters ) {
		featureFilters = new ArrayList<ComposableFeatureFilter>();
		featureFilters.addAll( p_featureFilters );
	}
	
	public void addFeatureFilter( final ComposableFeatureFilter filter ) {
		featureFilters.add( filter );
	}
	
	@Override
	public boolean isIgnoredAttribute( final EAttribute attribute ) {
		if ( super.isIgnoredAttribute( attribute ) ) {
			return true;
		}
		
		for ( final ComposableFeatureFilter filter : featureFilters ) {
			if ( filter.isIgnoredAttribute( attribute ) ) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public boolean checkForOrderingChanges( final EStructuralFeature feature ) {
		boolean ordered = super.checkForOrderingChanges (feature );
		
		for ( final ComposableFeatureFilter filter : featureFilters ) {
			ordered = filter.checkForOrderingChanges( feature );
		}
		
		return ordered;
	}
}
