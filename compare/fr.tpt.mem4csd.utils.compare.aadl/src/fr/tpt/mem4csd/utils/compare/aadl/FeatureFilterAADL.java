package fr.tpt.mem4csd.utils.compare.aadl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.osate.aadl2.Aadl2Package;

import fr.tpt.mem4csd.utils.compare.emf.ComposableFeatureFilter;

public class FeatureFilterAADL extends ComposableFeatureFilter {
	
	private final Boolean ordered;
	
	public FeatureFilterAADL( final Boolean ordered ) {
		this.ordered = ordered;
	}

	@Override
	public boolean isIgnoredAttribute( final EAttribute attribute ) {
		return 	attribute == Aadl2Package.eINSTANCE.getDefaultAnnexSubclause_SourceText() ||
				attribute == Aadl2Package.eINSTANCE.getDefaultAnnexLibrary_SourceText() ||
				super.isIgnoredAttribute( attribute );
	}
	
	@Override
	public boolean checkForOrderingChanges( final EStructuralFeature feature ) {
		if ( ordered == null || ordered ) {
		// The default implementation considers that containment references are always to be checked even if not ordered
			if ( feature.isMany()) {
				return feature.isOrdered();// || DiffUtil.isContainmentReference(feature);
			}
		}
		
		return false;
	}
}
