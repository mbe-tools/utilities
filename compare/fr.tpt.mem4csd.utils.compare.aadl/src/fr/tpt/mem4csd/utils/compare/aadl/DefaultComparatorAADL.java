package fr.tpt.mem4csd.utils.compare.aadl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.diff.FeatureFilter;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import com.google.common.base.Predicate;

import fr.tpt.mem4csd.utils.compare.IComparisonReport;
import fr.tpt.mem4csd.utils.compare.emf.ComposableFeatureFilter;
import fr.tpt.mem4csd.utils.compare.emf.CompoundFeatureFilter;
import fr.tpt.mem4csd.utils.compare.emf.DefaultComparatorEMF;

public class DefaultComparatorAADL extends DefaultComparatorEMF {
	
	private final CompoundFeatureFilter compoundFeatureFilter;
	
	//private final IQualifiedNameProvider qualifiedNameProvider;
	
	//private final Map<String, IComparatorEMF> annexComparators;
	
	public DefaultComparatorAADL( final ResourceSet resSet ) {
		this( resSet, Collections.<ComposableFeatureFilter>emptyList(), null );
	}
	
	public DefaultComparatorAADL( 	final ResourceSet resSet,
									final Boolean ordered ) {
		this( resSet, Collections.<ComposableFeatureFilter>emptyList(), ordered );
	}

	public DefaultComparatorAADL( 	final ResourceSet resSet,
									final Collection<ComposableFeatureFilter> annexFeatureFilters,
									final Boolean ordered ) {
		this( resSet, annexFeatureFilters, Collections.<String>emptyList(), ordered );
	}
	
	public DefaultComparatorAADL( 	final ResourceSet resSet,
									final Collection<ComposableFeatureFilter> annexFeatureFilters,
									final List<String> ignoredFileExtensions,
									final Boolean ordered ) {
		super( 	resSet,
				true, 
				Collections.<Predicate<? super Diff>>singletonList( new Predicate<Diff>() {
					@Override
					public boolean apply( final Diff diff ) {
						return diff.getRequiredBy().isEmpty();
					}
				} ),
				ignoredFileExtensions );
		
//		if ( p_qualifiedNameProvider == null && Platform.isRunning() ) {
//			qualifiedNameProvider = OsateCorePlugin.getDefault().getInjector( "org.osate.xtext.aadl2.properties.Properties" ).getInstance( Aadl2QualifiedNameProvider.class );
//		}
//		else {
//			qualifiedNameProvider = p_qualifiedNameProvider;
//		}

		compoundFeatureFilter = new CompoundFeatureFilter( annexFeatureFilters );
		compoundFeatureFilter.addFeatureFilter( new FeatureFilterAADL( ordered ) );
	}

	@Override
	protected FeatureFilter createFeatureFilter() {
		return compoundFeatureFilter;
	}
/*	
	private String getNameEnum( final List<? extends Enumerator> objects ) {
		final StringBuilder id = new StringBuilder( '[' );
		final Iterator<? extends Enumerator> it = objects.iterator();
		
		while ( it.hasNext() ) {
			final Enumerator obj = it.next();
			id.append( getName( obj ) );
			
			if ( it.hasNext() ) {
				id.append( ',' );
			}
		}
		
		id.append( ']' );
		
		return id.toString();
	}
	
	private String getName( final List<? extends EObject> objects ) {
		final StringBuilder id = new StringBuilder( '[' );
		final Iterator<? extends EObject> it = objects.iterator();
		
		while ( it.hasNext() ) {
			final EObject obj = it.next();
			id.append( getName( obj ) );
			
			if ( it.hasNext() ) {
				id.append( ',' );
			}
		}
		
		id.append( ']' );
		
		return id.toString();
	}

	private String getName( final EObject object ) {
		if ( object instanceof NamedElement ) {
			return getName( (NamedElement) object );
		}
		
		if ( object instanceof PropertyAssociation ) {
			return getName( (PropertyAssociation) object );
		}

		if ( object instanceof ContainmentPathElement ) {
			return getName( (ContainmentPathElement) object );
		}
		
		if ( object instanceof ContainedNamedElement ) {
			return getName( (ContainedNamedElement) object );
		}

		if ( object instanceof PropertyExpression ) {
			return getName( (PropertyExpression) object );
		}
		
		if ( object instanceof BasicPropertyAssociation ) {
			return getName( (BasicPropertyAssociation) object );
		}
		
		if ( object instanceof Realization ) {
			return getName( (Realization) object );
		}
		
		if ( object instanceof ConnectedElement ) {
			return getName( (ConnectedElement) object );
		}
		
		// BA
		if ( object instanceof ValueExpression ) {
			return getName( (ValueExpression) object );
		}

		if ( object instanceof Relation ) {
			return getName( (Relation) object );
		}

		if ( object instanceof SimpleExpression ) {
			return getName( (SimpleExpression) object );
		}

		if ( object instanceof Term ) {
			return getName( (Term) object );
		}

		if ( object instanceof Factor ) {
			return getName( (Factor) object );
		}
		
		throw new UnsupportedOperationException();
	}

	private String getName( final BasicPropertyAssociation element ) {
		return getName( element.eClass() ) + getName( element.getProperty() ) + getName( element.getOwnedValue() );
	}

	private String getName( final Realization element ) {
		return getName( element.eClass() ) + getName( element.getImplemented() );
	}

	private String getName( final ConnectedElement element ) {
		return getName( element.eClass() ) + getName( element.getContext() ) + getName( element.getConnectionEnd() );
	}

	private String getName( final PropertyAssociation element ) {
		return getName( element.eClass() ) + getName( element.getProperty() ) + getName( element.getAppliesTos() );
	}

	private String getName( final NamedElement element ) {
		if ( element == null ) {
			return "";
		}
		
		return element.getName();
	}

	private String getName( final ContainmentPathElement element ) {
		return getName( element.eClass() ) + getName( element.getNamedElement() );
	}

	private String getName( final ContainedNamedElement element ) {
		return getName( element.eClass() ) + getName( element.getPath() );
	}

	private String getName( final PropertyExpression element ) {
		if ( element instanceof ListValue ) {
			return getName( (ListValue) element );
		}
		
		if ( element instanceof RecordValue ) {
			return getName( (RecordValue) element );
		}
		
		if ( element instanceof ReferenceValue ) {
			return getName( (ContainedNamedElement) element );
		}
		
		if ( element instanceof NumberValue ) {
			return getName( (NumberValue) element );
		}
		
		if ( element instanceof BooleanLiteral ) {
			return getName( (BooleanLiteral) element );
		}
		
		if ( element instanceof StringLiteral ) {
			return getName( (StringLiteral) element );
		}
		
		if ( element instanceof NamedValue ) {
			return getName( (NamedValue) element );
		}
		
		if ( element instanceof RangeValue ) {
			return getName( (RangeValue) element );
		}
		
		if ( element instanceof ClassifierValue ) {
			return getName( (ClassifierValue) element );
		}
		
		throw new UnsupportedOperationException();
	}

	private String getName( final ListValue element ) {
		return getName( element.eClass() ) + getName( element.getOwnedListElements() );
	}

	private String getName( final RecordValue element ) {
		return getName( element.eClass() ) + getName( element.getOwnedFieldValues() );
	}

	private String getName( final ClassifierValue element ) {
		return getName( element.eClass() ) + getName( element.getClassifier() );
	}
	
	private String getName( final NumberValue element ) {
		return getName( element.eClass() ) + element.getScaledValue();
	}
	
	private String getName( final RangeValue element ) {
		return getName( element.eClass() ) + "[" + getName( element.getMinimum() ) + getName( element.getMaximum() ) + "]";
	}
	
	private String getName( final BooleanLiteral element ) {
		return getName( element.eClass() ) + element.getValue();
	}
	
	private String getName( final StringLiteral element ) {
		return getName( element.eClass() ) + element.getValue();
	}
	
	private String getName( final NamedValue element ) {
		return getName( element.eClass() ) + getName( element.getNamedValue() );
	}

	private String getName( final Enumerator element ) {
		return element.getClass() + "#" + element.getLiteral();
	}
	
	// BA
	private String getName( final ValueExpression element ) {
		return getName( element.eClass() ) + getNameEnum( element.getLogicalOperators() ) + getName( element.getRelations() );
	}

	private String getName( final Relation element ) {
		return getName( element.eClass() ) + getName( element.getFirstExpression() ) + getName( element.getRelationalOperator() ) + getName( element.getSecondExpression() ) ;
	}

	private String getName( final SimpleExpression element ) {
		return getName( element.eClass() ) + getName( element.getUnaryAddingOperator() ) + getName( element.getTerms() ) + getNameEnum( element.getBinaryAddingOperators() ) ;
	}

	private String getName( final Term element ) {
		return getName( element.eClass() ) + getName( element.getFactors() ) + getNameEnum( element.getMultiplyingOperators() );
	}
//
//	private String getName( final Factor element ) {
//		return getName( element.eClass() ) + getName( element.getFactors() ) + getNameEnum( element.getMultiplyingOperators() );
//	}

	private String getName( final EClass eClass ) {
		return eClass.getName() + "_";
	}
	
	private String getId( final EObject input ) {
		if ( input instanceof NamedElement ) {
			final String qualName =  ( (NamedElement) input ).qualifiedName();
			System.out.println( "Qualified name of element " + input + " is '" + qualName + "'." );
			
			return qualName;
		}
		
		return getId( input.eContainer() ) + "." + getName( input );
	}
	
	@Override
	protected IEObjectMatcher createEObjectMatcher( final boolean useIds ) {
		final Function<EObject, String> idFunction = new Function<EObject, String>() {
			
			@Override
			public String apply( final EObject input ) {
				return getId( input );
			}
		};
		
		// Using this matcher as fall back, EMF Compare will still search for XMI IDs on EObjects
		// for which we had no custom id function.
		//final IEObjectMatcher fallBackMatcher = DefaultMatchEngine.createDefaultEObjectMatcher( UseIdentifiers.WHEN_AVAILABLE );
		final FixedEditionDistance editionDistance = new FixedEditionDistance( createFeatureFilter() );
		final CachingDistance cachedDistance = new CachingDistance( editionDistance );
		final IEObjectMatcher contentMatcher = new ProximityEObjectMatcher( cachedDistance );
		
		return new IdentifierEObjectMatcher( contentMatcher, idFunction );
	}
*/
	@Override
	protected IComparisonReport createComparisonReport( final Resource p_leftResource,
														final Resource p_rightResource,
														final List<Diff> p_differences ) {
		return new ComparisonReportAADL( p_leftResource, p_rightResource, p_differences );
	}
}
