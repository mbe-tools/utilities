package fr.tpt.mem4csd.utils.compare.text;

import fr.tpt.mem4csd.utils.compare.IEditCommand;

/**
 * A ReportWriter is a visitor that is passed a series of EditCommands and
 * generates a report of some kind. See DefaultReportWriter.
 */
public interface IReportWriter {
	
    void write( String message );

    void report( IEditCommand aCommand);
}
