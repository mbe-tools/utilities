package fr.tpt.mem4csd.utils.compare.text;

/**
 * Change a block in the old file to a different block in the new file.
 */
public class ChangeCommand extends EditCommand {
	
    public ChangeCommand(	final FileInfo oldFileInfo, 
    						final FileInfo newFileInfo ) {
        super();

        command = "Change";
        oldLines = oldFileInfo.nextBlock();
        newLines = newFileInfo.nextBlock();
        oldLines.reportable = true;
        newLines.reportable = true;
    }
}
