package fr.tpt.mem4csd.utils.compare.text;


import java.util.HashMap;
import java.util.Map;

/**
 * Manages a set of Symbols. Current implementation is a HashMap.
 */
public class SymbolCollection
{
    private Map<String, Symbol> symbols = new HashMap<String, Symbol>();

    public Symbol getSymbolFor(	final String line,
    							final int lineNum ) {
    	
    	// FIXME: This does not work when there are identical lines
    	final String key = getKey( line, lineNum );
        Symbol symbol = symbols.get( key );
        if (null == symbol)
        {
            symbol = new Symbol();
            symbols.put( key, symbol );
        }
        return symbol;
    }

    public int registerSymbol(int fileIx, String line, int lineNum)
    {
        return getSymbolFor( line, lineNum ).countLine( fileIx, lineNum );
    }
    
    private String getKey( 	final String line,
    						final int lineNum ) {
    	return line;// + lineNum;
    }

    public int getStateOf(	final String line,
							final int lineNum ) {
        return getSymbolFor( line, lineNum ).getState();
    }
}
