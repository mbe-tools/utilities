package fr.tpt.mem4csd.utils.compare.text;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import fr.tpt.mem4csd.utils.compare.IComparisonReport;
import fr.tpt.mem4csd.utils.compare.IEditCommand;

/**
 * Creates and holds a List of edit commands that can be used to transform Old
 * into New. The constructor does all the work. Use any methods of the ancestor
 * ArrayList to access the commands. getCommand is a get wrapped to do the
 * casting to EditCommand.
 */
public class Report extends ArrayList<EditCommand> implements IComparisonReport {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2766751582838842998L;
	
	private final List<IComparisonReport> subReports;
	
	private final FileInfo oldFileInfo;
	private final FileInfo newFileInfo;

    public Report( 	final File oldFile,
    				final File newFile ) {
    	this( new FileInfo( oldFile, new String[ 0 ] ), new FileInfo( newFile, new String[ 0 ] ) );
    }

    /**
     * Create a Report (list of edit commands) from old and new file info.
     */
    public Report(	FileInfo p_oldFileInfo,
    				FileInfo p_newFileInfo ) {
    	oldFileInfo = p_oldFileInfo;
    	newFileInfo = p_newFileInfo;
    	subReports = new ArrayList<IComparisonReport>();
    	
    	if ( oldFileInfo != null && newFileInfo != null ) {
	        while (true) {
	            LineInfo oldLineInfo = oldFileInfo.currentLineInfo();
	            LineInfo newLineInfo = newFileInfo.currentLineInfo();
	            if (oldLineInfo.isEOF() && newLineInfo.isEOF())
	                break;
	
	            else if (oldLineInfo.isEOF() && newLineInfo.isNewOnly())
	                this.add( new AppendCommand( newFileInfo ) );
	
	            else if (oldLineInfo.isOldOnly() && newLineInfo.isNewOnly())
	                this.add( new ChangeCommand( oldFileInfo, newFileInfo ) );
	
	            else if (newLineInfo.isNewOnly())
	                this.add( new InsertCommand( oldFileInfo, newFileInfo ) );
	
	            else if (oldLineInfo.isOldOnly())
	                this.add( new DeleteCommand( oldFileInfo ) );
	
	            else if (oldLineInfo.isMatch())
	                this.add( new MatchCommand( oldFileInfo, newFileInfo ) );
	
	            else if (newLineInfo.isMatch())
	                newFileInfo.nextBlock(); // discard
	        }
    	}
    }

    /**
     * Write a report to standard out.
     */
    @Override
    public void print() {
        print( System.out );
    }

    /**
     * Write a report to any PrintStream
     */
    public void print(PrintStream stream) {
    	final IReportWriter writer = new DifferencesReportWriter( stream );
        write( writer );
        
    	for ( final IComparisonReport subReport : getSubReports() ) {
    		subReport.print( stream );
    	}
    }

    /**
     * Write a report using an IReportWriter. Use a custom report writer to
     * capture the report to something other than a PrintStream.
     */
    public void write(IReportWriter writer) {
    	final StringBuilder message = new StringBuilder();
    	final boolean containsDiff = containsDiff();
    	
    	if ( oldFileInfo.getFile().isDirectory() ) {
    		message.append( System.lineSeparator() );
    	}
    	
    	if ( containsDiff ) {
    		message.append( "Differences were found" );
    	}
    	else {
    		message.append( "No differences found" );
    	}
    	
    	message.append( " between " );
    	
    	if ( oldFileInfo.getFile().isDirectory() ) {
    		message.append( "directory" );
    	}
    	else {
    		message.append( "file" );
    	}
    	
    	message.append( " '" + oldFileInfo.getFile() + "' and " );
    	
    	if ( newFileInfo.getFile().isDirectory() ) {
    		message.append( "directory" );
    	}
    	else {
    		message.append( "file" );
    	}
    	
    	message.append( " '" + newFileInfo.getFile() + "'" );
    	
    	if ( containsDiff ) {
    		message.append( "!!!" );
    	}
    	else {
    		message.append( "." );
    	}
    	
    	writer.write( message.toString() );
    	
    	if ( containsDiff ) {
	        for (int i = 0; i < size(); i++) {
	            writer.report( getEditCommand( i ) );
	        }
    	}
    }

    @Override
    public IEditCommand getEditCommand(int lineNum) {
        return super.get( lineNum );
    }

	@Override
	public List<? extends IEditCommand> getEditCommands() {
		final List<? extends IEditCommand> pureEdits = new ArrayList<EditCommand>( this ); 
		pureEdits.removeIf( new Predicate<IEditCommand>() {

			@Override
			public boolean test( IEditCommand command ) {
				return command instanceof MatchCommand;
			}
		} );

		return pureEdits;
	}

    @Override
    public boolean containsDiff() {
    	if ( !getEditCommands().isEmpty() ) {
    		if(oldFileInfo.getFile().getPath().endsWith(".trace"))
        	{
        		int oldLineNb = oldFileInfo.getLineNum();
        		int newLineNb = newFileInfo.getLineNum();
        		final float tresh = 0.3f;
        		if((float)size()/oldLineNb<tresh && (float)size()/newLineNb<tresh)
        		{
        			System.out.println("Ignored diff on trace file, non-significant (<"+tresh+")");
        			return false;
        		}
        	}
    		return true;
    	}
//    	int count = this.size();
//    	for (int i = 0; i < count; i++)
//    	{
//    		IEditCommand ec = getEditCommand( i );
//    		if(ec instanceof MatchCommand)
//    			continue;
//    		else
//    			return true;
//    	}
    	
    	for ( final IComparisonReport subReport : getSubReports() ) {
    		if ( subReport.containsDiff() ) {
    			return true;
    		}
    	}

    	return false;
    }

    @Override
	public List<IComparisonReport> getSubReports() {
		return subReports;
	}
    
    @Override
    public String getMessage() {
    	final StringBuilder message = new StringBuilder();
    	final boolean containsDiff = containsDiff();
    	
    	if ( oldFileInfo.getFile().isDirectory() ) {
    		message.append( System.lineSeparator() );
    	}
    	
    	if ( containsDiff ) {
    		message.append( "Differences were found" );
    	}
    	else {
    		message.append( "No differences found" );
    	}
    	
    	message.append( " between " );
    	
    	if ( oldFileInfo.getFile().isDirectory() ) {
    		message.append( "directory" );
    	}
    	else {
    		message.append( "file" );
    	}
    	
    	message.append( " '" + oldFileInfo.getFile() + "' and " );
    	
    	if ( newFileInfo.getFile().isDirectory() ) {
    		message.append( "directory" );
    	}
    	else {
    		message.append( "file" );
    	}
    	
    	message.append( " '" + newFileInfo.getFile() + "'" );
    	
    	if ( containsDiff ) {
    		message.append( "!!!" );
    	}
    	else {
    		message.append( "." );
    	}
    	
    	if ( containsDiff ) {
	        for (int i = 0; i < size(); i++) {
	        	message.append( getEditCommand( i ).toString() );
	        }
    	}
        
    	for ( final IComparisonReport subReport : getSubReports() ) {
    		message.append( subReport.toString() );
    	}
    	
    	return message.toString();
    }

    @Override
    public String toString() {
    	return getMessage();
    }
}
