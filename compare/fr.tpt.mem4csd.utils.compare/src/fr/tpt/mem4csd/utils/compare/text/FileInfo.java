package fr.tpt.mem4csd.utils.compare.text;

import java.io.File;

/**
 * One of the two files being compared. lineNum supports nextBlock something like
 * an iterator.
 */
public class FileInfo {
	
    private static final LineInfo EOF = new LineInfo( -1, -1, -1, -1 );
	
    private final File file;
	private final String[] lines;
    private final LineInfo[] lineInfo;
    //private final int length;
    private int lineNum;

    public FileInfo(	final File p_file,
    					final String[] lines ) {
    	file = p_file;
        this.lines = lines;
        //length = lines.length;
        lineInfo = new LineInfo[ lines.length ];
        lineNum = 0;
    }

    public LineInfo currentLineInfo()
    {
        return lineInfoAt( lineNum );
    }

    public LineInfo lineInfoAt(int lineNum)
    {
        if (lineNum >= lines.length)
            return EOF;
        else
            return lineInfo[lineNum];
    }

    public LineBlock nextBlock()
    {
        LineBlock lineBlock = getBlockAt( lineNum );
        if (null != lineBlock)
            lineNum += lineBlock.lines.length; 
        return lineBlock;
    }

    public LineBlock getBlockAt(int lineNum)
    {
        if (lineNum >= lines.length)
            return null;
        int fromLineNum = lineNum;
        int blockNum = lineInfo[lineNum].blockNum;
        while (blockNum == lineInfoAt( lineNum ).blockNum)
        {
            lineNum++;
        }
        int thruLineNum = lineNum - 1;
        LineBlock lBlock = new LineBlock( lines, fromLineNum, thruLineNum );
        return lBlock;
    }

    public void setBlockNumber(int lineNum, int blockNum)
    {
        lineInfo[lineNum].setBlockNumber( blockNum );
    }

    public boolean isValidLineNum(int lineNum)
    {
        return ((lineNum >= 0) && (lineNum < lines.length));
    }
    public File getFile() {
		return file;
	}

	public String[] getLines() {
		return lines;
	}

	public LineInfo[] getLineInfo() {
		return lineInfo;
	}

	public int getLength() {
		return getLines().length;
	}

    public int getLineNum() {
		return lineNum;
	}
}