package fr.tpt.mem4csd.utils.compare;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class DefaultComparisonReport implements IComparisonReport {

	private final boolean equals;
	
	private final String message;
	
	private final List<IComparisonReport> subReports;
	
	public DefaultComparisonReport( final boolean equals,
									final String message ) {
		this.equals = equals;
		this.message = message;
		subReports = new ArrayList<IComparisonReport>();
	}

	@Override
	public boolean containsDiff() {
		return !equals;
	}

	@Override
	public void print() {
		print( System.out );
	}

	@Override
	public void print( final PrintStream stream ) {
		stream.println( getMessage() );
	}

	@Override
	public List<IComparisonReport> getSubReports() {
		return subReports;
	}

	@Override
	public IEditCommand getEditCommand(int lineNum) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<IEditCommand> getEditCommands() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getMessage() {
		return message;
	}
}
