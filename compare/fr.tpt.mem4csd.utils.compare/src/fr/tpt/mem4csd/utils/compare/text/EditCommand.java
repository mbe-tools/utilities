package fr.tpt.mem4csd.utils.compare.text;

import fr.tpt.mem4csd.utils.compare.IEditCommand;

/**
 * Can be used to transform Old into New.
 * Concrete classes fill in the command name and old and new lines affected as
 * appropriate.
 */
public abstract class EditCommand implements IEditCommand {
	
    public String command = "undefined";
    public LineBlock oldLines = null;
    public LineBlock newLines = null;
    public static String LINE_NUM_PAD = "    "; // Line numbers will be padded to this

    protected EditCommand() {
    }
    
    @Override
    public String toString() {
    	final StringBuilder builder = new StringBuilder( command );
    	builder.append( " " );
    	builder.append( toString( oldLines, "Old" ) );
    	builder.append( toString( newLines, "New" ) );
    	
    	return builder.toString();
    }
    
    protected String toString( 	final LineBlock lineBlock,
    							final String description ) {
    	final StringBuilder builder = new StringBuilder();
    	
    	if ( lineBlock != null ) {
        	builder.append( description );
        	builder.append( " line(s) from lines " );
        	builder.append( lineBlock.fromLineNum + 1 );
        	builder.append( " to " );
        	builder.append( lineBlock.thruLineNum + 1 );
        	builder.append( ":" );
        	builder.append( System.lineSeparator() );

        	if ( lineBlock.reportable ) {
                int lineNum = lineBlock.fromLineNum + 1;
                
                for ( int i = 0; i < lineBlock.lines.length; i++) {
                	builder.append( pad( lineNum++ ) );
                	builder.append( ": " );
                	builder.append( lineBlock.lines[i] );
                	builder.append( System.lineSeparator() );
                }
            }
    	}
    	
    	return builder.toString();
    }

    public static String pad( int lineNum ) {
        String paddedNum = "" + lineNum;
        
        if (paddedNum.length() < LINE_NUM_PAD.length()) {
            return (LINE_NUM_PAD + paddedNum).substring( LINE_NUM_PAD.length() );
        }

        return paddedNum;
    }
}