package fr.tpt.mem4csd.utils.compare.text;

/**
 * Append a block of lines to the end of the old file.
 */
public class AppendCommand extends EditCommand {
	
    public AppendCommand( final FileInfo newFileInfo ) {
        super();
        
        command = "Append";
        newLines = newFileInfo.nextBlock();
        newLines.reportable = true;
    }
}
