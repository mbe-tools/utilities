package fr.tpt.mem4csd.utils.compare;

import java.io.PrintStream;
import java.util.List;

public interface IComparisonReport {

	boolean containsDiff();
	
	String getMessage();

	void print();
	
	void print( PrintStream stream );

	List<IComparisonReport> getSubReports();

    IEditCommand getEditCommand( int lineNum );
    
    List<? extends IEditCommand> getEditCommands();
}
