--
-- MEM4CSD-Utilities
-- 
-- Copyright © 2012-2015 TELECOM Paris and CNRS
-- Copyright © 2016-2020 TELECOM Paris
-- 
-- TELECOM Paris
-- 
-- Authors: see AUTHORS
--
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program. 
-- If not, see https://www.eclipse.org/legal/epl-2.0/
--

-- @nsURI AADLI=http:///AADL2/instance
-- @atlcompiler emftvm

module PropertiesTools;

create 
	OUT 				   : AADLBA
from 
	IN 					   : AADLI,
	BASE_TYPES			   : AADLBA,
	AADL_RUNTIME		   : AADLBA,
	DATA_MODEL			   : AADLBA,
	-- Issue #4 Review of  the management of the predefined AADL files 
	--	SCHEDULER_CONSTANTS	   : AADLBA,
	SCHEDULER_REALIZATIONS : AADLBA,
	SCHEDULER_RUNTIME	   : AADLBA;


helper context AADLI!NamedElement def : hasProperty(propertyName: String) : Boolean =
	not self.getPropertyAssociation(propertyName).oclIsUndefined()
;

helper context AADLI!NamedElement def : getProperty(propertyName: String) : AADLI!PropertyAssociation =
	self.getPropertyAssociation(propertyName)
;

helper context AADLI!ComponentInstance def : hasPropertyAssigned(p: AADLI!Property): Boolean =
	self.ownedPropertyAssociation->exists(
				e | e.property = p
				)
;

--helper context AADLI!ComponentImplementation def : hasProperty(propertyName: String) : Boolean =
--	self.getAllPropertyAssociations()->exists(
--				e | e.property.name.toLower() = propertyName.toLower()
--				)
--	or
--	self.type.hasProperty(propertyName)
--;
--
--helper context AADLI!ComponentType def : hasProperty(propertyName: String) : Boolean =
--	self.getAllPropertyAssociations()->exists(
--				e | e.property.name.toLower() = propertyName.toLower()
--				)
--;
--
--helper context AADLI!NamedElement def : hasPropertyAssigned(p: AADLI!Property): Boolean =
--	self.getAllPropertyAssociations()->exists(
--				e | e.property = p
--				)
--;


helper context String def : asBaseType() : AADLBA!Classifier =
	AADLBA!Classifier->allInstancesFrom('BASE_TYPES')->any(c|c.name=self)
;

helper context String def : asDataType(model: String) : AADLBA!Classifier =
	AADLBA!Classifier->allInstancesFrom(model)->any(c|c.name=self)
;

helper context String def : asSubprogramType(model: String) : AADLBA!Classifier =
	AADLBA!Classifier->allInstancesFrom(model)->any(c|c.name=self)
;

helper context String def : asProperty(model : String) : AADLBA!Property =
	AADLBA!Property->allInstancesFrom(model)->any(p|p.name=self)
;

helper context String def : asEnumeration(model : String) : AADLBA!EnumerationLiteral =
	AADLBA!EnumerationLiteral->allInstancesFrom(model)->any(e|e.name=self)
;

helper context String def : asClassifier(model : String) : AADLBA!Classifier =
	AADLBA!Classifier->allInstancesFrom(model)->any(p|p.name=self)
;

rule toStringLiteral(input: String)
{
	to
		output : AADLBA!StringLiteral (value <- input)
	do
	{
		output;
	}
}

helper def: getStringListValue(v : AADLBA!ModalPropertyValue) : Sequence(String) = 
	v.ownedValue.getOwnedListElements()->collect(e|e.namedValue.name)
;

helper def : getProperty(propertyName : String) : AADLBA!Property =
	thisModule.theTOOLS.findProperty(AADLBA!PropertySet->allInstances(), propertyName)
--	if(propertyName.contains('::')) then
--		let index: Integer = propertyName.indexOf('::') in
--		let psName: String = propertyName.substring(1,index-1) in
--		let propName: String = propertyName.substring(index+2, propertyName.size()) in
--			AADLBA!PropertySet->allInstances()->select(ps| ps.name.toLower() = psName.toLower())
--											->collect(ps|ps.ownedProperty)
--											->flatten()->any(p|p.name.toLower()=propName.toLower())
--	else
--		AADLBA!PropertySet->allInstances()->collect(ps|ps.ownedProperty)
--					->flatten()->any(p|p.name.toLower()=propertyName.toLower())
--	endif	
;

helper context AADLBA!NamedElement def : getPropertyIntValue(
	propertyName : String) : Integer =
	let v : AADLBA!ModalPropertyValue = self.ownedPropertyAssociation
			->any(pa|pa.property.name=propertyName)
	in 
		if (v.oclIsUndefined()) then
			OclUndefined
		else
			v.ownedValue->first().ownedValue.value
		endif
;

helper def : getIntegerFieldValue(ne: AADLI!NamedElement, propertyName: String, fieldName: String) : Integer =
	let pa: AADLI!PropertyAssociation = ne.ownedPropertyAssociation
		->any(pa|pa.property.name.equalsIgnoreCase(propertyName)) in
	let rv: AADLI!RecordValue = pa.ownedValue->first().ownedValue	in
	rv.ownedFieldValue
			->any(fv|fv.property.name=fieldName).ownedValue.value
;

helper context AADLBA!NamedElement def : getPropertyAssociation(propertyName : String) : AADLBA!PropertyAssociation =
	thisModule.theTOOLS.findPropertyAssociation(propertyName, self)
;
	
helper context AADLBA!NamedElement def : getModalPropertyValue(propertyType : String) : AADLBA!ModalPropertyValue =
	if(not self.getPropertyAssociation(propertyType).oclIsUndefined()) then
		self.getPropertyAssociation(propertyType).ownedValue->first()
	else
		if(self.oclIsTypeOf(AADLBA!ComponentImplementation)) then
			self.type.getPropertyAssociation(propertyType).ownedValue->first()
		else
			OclUndefined
		endif
	endif
;

rule CreatePropertyAssociation (propertyName : String, model : String, value : AADLBA!PropertyExpression) {
	to
		p : AADLBA!PropertyAssociation (
			property <- propertyName.asProperty(model),
			ownedValue <- Sequence {mpv}
		),
		
		mpv : AADLBA!ModalPropertyValue (
			ownedValue <- value
		)
	do { p; }
}

rule CreatePropertyAssociationAppliesTo (propertyName : String, model : String, 
		value : AADLBA!PropertyExpression, 
		appliesTo: AADLBA!ContainedNamedElement) {
	to
		p : AADLBA!PropertyAssociation (
			property <- propertyName.asProperty(model),
			ownedValue <- Sequence {mpv},
			appliesTo <- Sequence {appliesTo}
		),
		
		mpv : AADLBA!ModalPropertyValue (
			ownedValue <- value
		)
	do { p; }
}

rule CreateStringLiteralPropertyExpression (value : String) {
	to
		pr : AADLBA!StringLiteral(value<-value)
	do
	{
		pr;
	}
}

rule CreateClassifierValuePropertyExpression(value: AADLBA!Classifier)
{
	to
		pr : AADLBA!ClassifierValue(classifier<-value)
	do
	{
		pr;
	}
}

rule CreateEnumValuePropertyExpression (value : String) {
	to
		pr : AADLBA!NamedValue(namedValue<-el),
		el : AADLBA!EnumerationLiteral(name<-value)
	do
	{
		pr;
	}
}

rule CreateIntegerLiteralPropertyExpression (value: Integer)
{
	to
		pr : AADLBA!IntegerLiteral(value<-value.longValue())
	do
	{
		pr;
	}
}

lazy rule CreateListValueFromPropertyExpression
{
	from
		initValuePE: AADLBA!PropertyExpression
	to
		lv: AADLBA!ListValue
		(
			ownedListElement <- Sequence{initValuePE}
		)
	do
	{
		lv;
	}
}

rule CreateListValueFromInteger (value: OclAny) -- OclAny could be long or integer values
{
	to
		il : AADLBA!IntegerLiteral(value<-value.longValue()),
		lv : AADLBA!ListValue(ownedListElement<-Sequence{il})
	do
	{
		lv;
	}
}

rule CreateListValueFromString (value: String)
{
	to
		lv : AADLBA!ListValue
		(
			ownedListElement <- Sequence{thisModule.CreateStringLiteralPropertyExpression(value)}
		)
	do
	{
		lv;
	}
}

rule CreateListValueFromListOfString(value: Sequence(String))
{
	to
		lv:AADLBA!ListValue
		(
			ownedListElement <- value->collect(e | thisModule.CreateStringLiteralPropertyExpression(e))
		)
	do
	{
		lv;
	}
}

rule CreateListValueFromListOfClassifier(value: Sequence(AADLBA!Classifier))
{
	to
		lv:AADLBA!ListValue
		(
			ownedListElement <- value->collect(e | thisModule.CreateClassifierValuePropertyExpression(e))
		)
	do
	{
		lv;
	}
}

rule CreateListValueFromClassifier (value: AADLBA!Classifier)
{
	to
		cv : AADLBA!ClassifierValue(classifier<-value),
		lv : AADLBA!ListValue(ownedListElement<-Sequence{cv})
	do
	{
		lv;
	}
}

lazy rule bindDatatypeEnumStringliteral
{
	from
		component: AADLI!Classifier,
		stringLiteral: String
	
	using
	{
		enum: AADLI!PropertyValue = component.getEnumerators();
	}
	         	 
		
	to
		classifierPropertyReference: AADLBA!ClassifierPropertyReference
		(
			classifier <- component,
			properties <- Sequence {enumerators_propertyNameHolder, bdes_propertyNameHolder}
		),
		enumerators_propertyNameHolder: AADLBA!PropertyNameHolder
		(
			property <- enumerators_PropertyAssociationHolder
		),
		enumerators_PropertyAssociationHolder: AADLBA!PropertyAssociationHolder
		(
			element <- enum
		),		
		bdes_propertyNameHolder: AADLBA!PropertyNameHolder
		(
			property <- bdes_PropertyExpressionHolder
		),
		bdes_PropertyExpressionHolder: AADLBA!PropertyExpressionHolder
--		(
--			element <- thisModule.getStringLiteral(enum,
--				                                   stringLiteral)
--		)
     do 
     {
     	bdes_PropertyExpressionHolder.element <- thisModule.getStringLiteral(enum,
				                                   stringLiteral);
     	classifierPropertyReference;	
     }
}

rule createComputeExecutionTimePropertyAssociation(minTime: OclAny,
												   maxTime: OclAny,
												   unit: String)
{
	to
		computeExecutionTimePA: AADLBA!PropertyAssociation
		(
			property  <- 'Compute_Execution_Time'.asProperty('TIMING_PROPERTIES'),
	   	    ownedValue <- Sequence{thisModule.createTimeRangePropertyValue(minTime, maxTime, unit)}
		)
	do
	{
		computeExecutionTimePA;
	}
}

lazy rule createContainedNamedElement
{
	from
		ne: AADLBA!NamedElement
	to
		cne: AADLBA!ContainedNamedElement
		(path <- cpe),
		cpe: AADLBA!ContainmentPathElement
		(namedElement<-ne)
	do
	{
		cne;
	}
}

helper context AADLI!InstanceObject def: getContainmentPath(rootInstance: AADLI!ComponentInstance): Sequence(AADLI!InstanceObject) =
	if(rootInstance.componentInstance->union(rootInstance.featureInstance)->exists(e| e=self)) then
		Sequence{self}
	else
		rootInstance.componentInstance->collect(e | 
			if(not self.getContainmentPath(e)->isEmpty()) then
				self.getContainmentPath(e)->prepend(e)
			else
				Sequence{}
			endif)->flatten()
	endif
;

helper def : createContainmentPathElement(neList: Sequence(AADLI!NamedElement)) : AADLBA!ContainmentPathElement =
	thisModule.createContainmentPathElementLazyRule
		(
			neList
		)
;

helper context OclAny def: needsCopy() : Boolean = true;


lazy rule createContainmentPathElementLazyRule
{
	from
		neList: Sequence(AADLI!NamedElement)
	using
	{
		neSubList: Sequence(AADLI!NamedElement) = 
			if(neList->size()>1) then
				neList->excluding(neList->first())
			else
				OclUndefined
			endif;
	}
	to
		cpe : AADLBA!ContainmentPathElement (
				namedElement<-if(neList->size()>0) then -- and neList->at(1).resolve().oclIsKindOf(AADLI!NamedElement)) then
								neList->at(1).resolve()
							else 
								OclUndefined
							endif,
				path <- if(not neSubList.oclIsUndefined()) then
							thisModule.createContainmentPathElement(neSubList)
						else
							OclUndefined
						endif,
				arrayRange <- 	if(neList->size()=0 or neList->at(1).needsCopy()) then
									Sequence{}
								else
									neList->at(1).index.getIndexes()
								endif
			)
	do
	{
		cpe;
	}
}

rule createTimeRangePropertyValue(minTime: OclAny,
								  maxTime: OclAny,
								  unit: String)
{
	to
		modalpropvalue : AADLBA!ModalPropertyValue 
		(
			ownedValue <- rangeV
	    ),
	   	rangeV : AADLBA!RangeValue 
		(
	   		minimum <- pemin,
	   		maximum <- pemax
		),
		pemin: AADLBA!IntegerLiteral 
		(
			value <- minTime.longValue(),
			unit <- ul
		),
		pemax: AADLBA!IntegerLiteral 
		(
			value <- maxTime.longValue(),
			unit <- ul
		),	
		ul: AADLBA!UnitLiteral
		(
			name <- unit
		)
	do
	{
		modalpropvalue;
	}
}