/**
 * AADL-RAMSES
 * 
 * Copyright © 2012 TELECOM ParisTech and CNRS
 * 
 * TELECOM ParisTech/LTCI
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.  If not, see 
 * http://www.eclipse.org/org/documents/epl-v10.php
 */

package fr.tpt.mem4csd.utils.osate.standalone;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.xtext.linking.lazy.LazyLinkingResource;
import org.eclipse.xtext.resource.XtextResource;
import org.osate.aadl2.instance.InstancePackage;
import org.osate.aadl2.instance.util.InstanceResourceFactoryImpl;
import org.osate.aadl2.modelsupport.FileNameConstants;
import org.osate.aadl2.modelsupport.util.AadlUtil;
import org.osate.xtext.aadl2.Aadl2StandaloneSetup;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.tpt.mem4csd.utils.osate.standalone.StandaloneAnnexRegistry.AnnexExtensionRegistration;

public class OsateStandaloneSetup extends Aadl2StandaloneSetup {
	
	
	public static final URI PREDECLARED_POPERTY_SETS_URI = URI.createURI( StandaloneXtextResourceSet.SCHEME + "resources/properties/Predeclared_Property_Sets" );


	private static final Set<String> PREDECLARED_POPERTY_SETS_NAMES = AadlUtil.getPredeclaredPropertySetNames();
//			new LinkedHashSet<String>( Arrays.asList( new String[] { 	"AADL_Project",
//																		"Communication_Properties",
//																		"Memory_Properties",
//																		"Deployment_Properties",
//																		"Programming_Properties", 
//																		"Thread_Properties",
//																		"Timing_Properties" } ) );

	public static final URI ANNEX_CONTRIBUTION_PROPERTY_SETS_URI = URI.createURI( StandaloneXtextResourceSet.SCHEME + "resources/properties" );
	private static final String[] ANNEX_CONTRIBUTION_PROPERTY_SETS_NAMES = { 	"ARINC429",
																				"ARINC653", 
																				"Code_Generation_Properties", 
																				"Data_Model",
																				"Physical",
																				"SEI" };
	public static final URI ANNEX_CONTRIBUTION_PACKAGES_URI = URI.createURI( StandaloneXtextResourceSet.SCHEME + "resources/packages" );
	private static final String[] ANNEX_CONTRIBUTION_PACKAGES_NAMES = { "Base_Types",
																		"PhysicalResources" };

	@SuppressWarnings("unused")
	private static final Object dummy = InstancePackage.eINSTANCE;
	
	private static final FileFilter aadlFilesFilter = new FileFilter() {
		
		@Override
		public boolean accept( final File file ) {
			return file.isDirectory() || file.getName().endsWith( "." + FileNameConstants.SOURCE_FILE_EXT );
		}
	};
	
	private final URI baseUri;
	
	private final Injector injector;

	public OsateStandaloneSetup() {
		this( null, null );
	}
	
	public OsateStandaloneSetup( final URI baseUri ) {
		this( baseUri, null );
	}
	
	public OsateStandaloneSetup( final Collection<AnnexExtensionRegistration> annexExtensions ) {
		this( null, annexExtensions );
	}

	public OsateStandaloneSetup( final URI baseUri , final Collection<AnnexExtensionRegistration> annexExtensions ) {
		if (annexExtensions != null) {
			StandaloneAnnexRegistry.putAllExtensionRegistrations(annexExtensions);
		}
		
		this.baseUri = baseUri;
		
		injector = createInjectorAndDoEMFRegistration();
	}

	@Override
	public Injector createInjector() {
		return Guice.createInjector( new StandaloneAadl2RuntimeModule() );
	}

	public ResourceSet createResourceSet() {
		return createResourceSet( (URI) null );
	}
	
	public ResourceSet createResourceSet( final Set<String> predefinedAadlResNames ) {
		return createResourceSet( null, predefinedAadlResNames );
	}

	public ResourceSet createResourceSet( final URI predefinedAadlResDir ) {
		return createResourceSet( predefinedAadlResDir, null );
	}

	public ResourceSet createResourceSet(  	final URI predefinedAadlResDir,
											final Set<String> predefinedAadlFileNames ) {
		final StandaloneXtextResourceSet resourceSet = injector.getInstance( StandaloneXtextResourceSet.class );
		resourceSet.addLoadOption( XtextResource.OPTION_RESOLVE_ALL, true );
		
		loadPredefinedOsateAadlResources( resourceSet );
		
		loadPredefinedAadlResources( 	predefinedAadlResDir,
										predefinedAadlFileNames,
										resourceSet,
										baseUri );
		
		return resourceSet;
	}
	
	public static void loadPredefinedOsateAadlResources( final ResourceSet resourceSet ) {
		loadPredefinedAadlResources( PREDECLARED_POPERTY_SETS_URI, PREDECLARED_POPERTY_SETS_NAMES, resourceSet );

		final Set<String> annexPropSetFileNames = new LinkedHashSet<>( Arrays.asList( ANNEX_CONTRIBUTION_PROPERTY_SETS_NAMES ) );
		loadPredefinedAadlResources( ANNEX_CONTRIBUTION_PROPERTY_SETS_URI, annexPropSetFileNames, resourceSet );
		
		final Set<String> annexPackageFileNames = new LinkedHashSet<>( Arrays.asList( ANNEX_CONTRIBUTION_PACKAGES_NAMES ) );
		loadPredefinedAadlResources( ANNEX_CONTRIBUTION_PACKAGES_URI, annexPackageFileNames, resourceSet );
	}
	
	public static void loadPredefinedAadlResources( final URI predefinedAadlResDir,
													final Set<String> predefinedAadlFiles,
													final ResourceSet resourceSet ) {
		loadPredefinedAadlResources( predefinedAadlResDir, predefinedAadlFiles, resourceSet, null );
	}

	public static void loadPredefinedAadlResources( URI predefinedAadlResDir,
													Set<String> predefinedAadlFileNames,
													final ResourceSet resourceSet,
													final URI baseUri ) {
//		final Set<String> predefinedAadlResources = new LinkedHashSet<String>();
		
//		if ( predefinedAadlFileNames != null ) {
//			predefinedAadlResources.addAll( Arrays.asList( predefinedAadlResNames ) );
//		}
//		else {
		if ( predefinedAadlFileNames == null ) {
			predefinedAadlFileNames = new LinkedHashSet<String>();

			if ( predefinedAadlResDir != null ) {
				predefinedAadlFileNames.addAll( findAadlFiles( new File( predefinedAadlResDir.toString() ) ) );
			}
		}
		
		 //URI predefinedAadlResDirUri = URI.createFileURI( predefinedAadlResDir );
		 
		if ( baseUri != null && predefinedAadlResDir != null ) {
			predefinedAadlResDir = predefinedAadlResDir.resolve( baseUri );
		}
		
		final Boolean resolve = (Boolean) resourceSet.getLoadOptions().get( XtextResource.OPTION_RESOLVE_ALL );
			
		// Only resolve resources at the end when all files are loaded to avoid the need to load files in the dependency order
		resourceSet.getLoadOptions().put( XtextResource.OPTION_RESOLVE_ALL, false );

		for ( final String aadlFileName : predefinedAadlFileNames ) {
		//while ( !predefinedAadlResources.isEmpty() ) {
			//final String aadlFileName = predefinedAadlResources.iterator().next();
			resourceSet.getResource( predefinedAadlResDir.appendSegment( aadlFileName ).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT ), true );
		}
		
		for ( final Resource resource : resourceSet.getResources() ) {
			if ( resolve != null && resolve && resource instanceof LazyLinkingResource ) {
				( (LazyLinkingResource) resource ).resolveLazyCrossReferences( null );
			}
			
			if ( !resource.getErrors().isEmpty() ) {
				final StringBuilder strBuild = new StringBuilder( "Resource " );
				strBuild.append( resource.getURI() );
				strBuild.append( " has errors!" );
				strBuild.append( System.lineSeparator() );
				strBuild.append( resource.getErrors().toString() );
				
				resource.unload();
				
				throw new IllegalStateException( strBuild.toString() );
			}
		}

		resourceSet.getLoadOptions().put( XtextResource.OPTION_RESOLVE_ALL, resolve );
	}
	
	private static List<String> findAadlFiles( final File file ) {
		final List<String> aadlFiles = new ArrayList<String>();
		
		if ( file.isFile() ) {
			if ( aadlFilesFilter.accept( file ) ) {
				aadlFiles.add( file.getName() );
			}
		}
		else if ( file.isDirectory() ) {
			for ( final File subFile : file.listFiles( aadlFilesFilter ) ) {
				aadlFiles.addAll( findAadlFiles( subFile ) );
			}
		}
		
		return aadlFiles;
	}
	
	@Override
	public void register(Injector injector) {
		super.register(injector);
		
		final Map<String, Object> extensionToFactoryMap = Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap();

		extensionToFactoryMap.put( Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl() );
		extensionToFactoryMap.put( FileNameConstants.INSTANCE_FILE_EXT, new InstanceResourceFactoryImpl() );
	}
}