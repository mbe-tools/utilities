package fr.tpt.mem4csd.utils.osate.standalone.tests;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.junit.jupiter.api.Test;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.PackageSection;
import org.osate.annexsupport.AnnexRegistry;
import org.osate.ba.AadlBaParserAction;
import org.osate.ba.AadlBaResolver;
import org.osate.ba.AadlBaUnParserAction;
import org.osate.ba.texteditor.AadlBaSemanticHighlighter;
import org.osate.ba.texteditor.AadlBaTextPositionResolver;

import fr.tpt.mem4csd.utils.osate.standalone.OsateStandaloneSetup;
import fr.tpt.mem4csd.utils.osate.standalone.StandaloneAnnexRegistry.AnnexExtensionRegistration;
import junit.framework.TestCase;

public class TestOsateStandalone extends TestCase {

	private static final URI SHARED_RESOURCES_DIR = URI.createFileURI( "resources/shared/" );
	
	private static final Set<String> PREDEFINED_AADL_RESOURCES_NAMES = new LinkedHashSet<String>( Arrays.asList(new String[]{ 	"pok_properties",
																																"ARINC653_runtime",
																																"RAMSES_processors",
																																"RAMSES_buses",
																																"common_pkg" } ) );
	
	private static final OsateStandaloneSetup osateStandaloneSetup;
	
	static {
		final List<AnnexExtensionRegistration> annexExtensions = new ArrayList<AnnexExtensionRegistration>();
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_PARSER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaParserAction.class ));
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_UNPARSER_EXT_ID, AadlBaUnParserAction.ANNEX_NAME, AadlBaUnParserAction.class ));
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_RESOLVER_EXT_ID, AadlBaResolver.ANNEX_NAME, AadlBaResolver.class ));
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_TEXTPOSITIONRESOLVER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaTextPositionResolver.class ));
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_HIGHLIGHTER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaSemanticHighlighter.class ));

		osateStandaloneSetup = new OsateStandaloneSetup( annexExtensions );
	}


	@Test
	public void testWithBa() {
		final ResourceSet resourceSet = osateStandaloneSetup.createResourceSet( SHARED_RESOURCES_DIR, PREDEFINED_AADL_RESOURCES_NAMES );
		final URI resUri = URI.createFileURI( "resources/test_with_ba.aadl");
		
		final Resource aadlRes = resourceSet.getResource( resUri,  true );
		
		assertTrue( aadlRes.getErrors().isEmpty() );
		
		assertFalse( aadlRes.getContents().isEmpty() );
		
		final AadlPackage aadlPackage = (AadlPackage) aadlRes.getContents().get( 0 );
		final PackageSection pubSection = aadlPackage.getOwnedPublicSection();

		assertFalse( pubSection.getOwnedClassifiers().isEmpty() );
	}
	
	@Test
	public void testURIclasspath() {
		final ResourceSet resourceSet = osateStandaloneSetup.createResourceSet();
		final URI resUri = URI.createPlatformPluginURI("fr.tpt.mem4csd.utils.osate.standalone.tests/dummy.ecore", true);
		
		final Resource resource = resourceSet.getResource( resUri,  true );
		
		assertFalse( resource.getContents().isEmpty() );
	}
	
	@Test
	public void testURIclasspathFromResource() {
		final ResourceSet resourceSet = osateStandaloneSetup.createResourceSet();
		final URI resUri = URI.createFileURI("resources/dummy.ecore");
		
		final Resource resource = resourceSet.getResource( resUri,  true );
		
		assertFalse( resource.getContents().isEmpty() );

	}
	
	@Test
	public void testURIconverterInputStream() {
		final ResourceSet resourceSet = osateStandaloneSetup.createResourceSet();
		final URI resUri = URI.createPlatformPluginURI("fr.tpt.mem4csd.utils.osate.standalone.tests/givenProperties.properties", true);
		final Properties props = new Properties();
		
		try {
			final InputStream inputStream = resourceSet.getURIConverter().createInputStream(resUri);
			assertTrue( inputStream != null );
			props.load( inputStream );
			inputStream.close();
			
			assertTrue(props.containsKey("firstProperty"));
			
		} catch (IOException e) {
			e.printStackTrace();
			fail( e.getMessage() );
		};	
	}
	
	@Test
	public void testURIconverterInputStreamExist() {
		final ResourceSet resourceSet = osateStandaloneSetup.createResourceSet();
		final URI resUri = URI.createPlatformPluginURI("fr.tpt.mem4csd.utils.osate.standalone.tests/givenProperties.properties", true);
		URIConverter uriConverter = resourceSet.getURIConverter();
		
		URI res = URI.createURI("classpath:/givenProperties.properties");
		
		assertTrue(uriConverter.exists(resUri, null));
		assertTrue(uriConverter.exists(res, null));
	}
	
	@Test
	public void testURIconverterInputStreamExistProperties() {
		final ResourceSet resourceSet = osateStandaloneSetup.createResourceSet();
		final URI resUri = URI.createPlatformPluginURI("fr.tpt.mem4csd.utils.osate.standalone.tests/givenProperties.properties", true);
		final Properties props = new Properties();
		
		try {
			final InputStream inputStream = resourceSet.getURIConverter().createInputStream(resUri);
			assertTrue( inputStream != null );
			props.load( inputStream );
			inputStream.close();
			
			assertTrue(props.containsKey("firstProperty"));
		} catch (IOException e) {
			e.printStackTrace();
			fail( e.getMessage() );
		};	
	}
	
	@Test
	public void testURIconverterNormalize() {
		final ResourceSet resourceSet = osateStandaloneSetup.createResourceSet();
		final URI resUri = URI.createPlatformPluginURI("fr.tpt.mem4csd.utils.osate.standalone.tests/dummy.ecore", true);
		
		final URI uriNormalize = resourceSet.getURIConverter().normalize(resUri);
			
		final Resource resource = resourceSet.getResource( uriNormalize,  true );
		
		assertFalse( resource.getContents().isEmpty() );
	}
	
	@Test
	public void testURIconverterFromResource() {
		final ResourceSet resourceSet = osateStandaloneSetup.createResourceSet();
		final URI resUri = URI.createFileURI("resources/dummy.ecore");
		
		try {
			final InputStream inputStream = resourceSet.getURIConverter().createInputStream(resUri);

			assertTrue( inputStream != null );
		} catch (IOException e) {
			e.printStackTrace();
			fail( e.getMessage() );
		};	
	}
}
