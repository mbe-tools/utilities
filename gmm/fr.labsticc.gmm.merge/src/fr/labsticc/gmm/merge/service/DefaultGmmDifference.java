package fr.labsticc.gmm.merge.service;

public class DefaultGmmDifference implements IGmmDifference {
	
	private final String description;
	
	public DefaultGmmDifference( final String p_description ) {
		description = p_description;
	}
	
	@Override
	public String toString() {
		return description;
	}
}
