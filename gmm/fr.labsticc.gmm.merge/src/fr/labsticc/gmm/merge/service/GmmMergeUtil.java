package fr.labsticc.gmm.merge.service;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

public class GmmMergeUtil {

	private static final String MERGE_SERVICE_EXT_ID = "mergeServiceExtension";
	private static final String MERGE_SERVICE_ELEM = "mergeService";

	private GmmMergeUtil() {
	}
	
	public static IGmmMergeService getMergeService() 
	throws CoreException {
		final IExtension[] extensions = getExtensionPointExtensions( GmmMergeActivator.getContext().getBundle().getSymbolicName(), MERGE_SERVICE_EXT_ID );
		
		if ( extensions != null ) {
			for( final IExtension extension : extensions ) {
				for( final IConfigurationElement confElem : extension.getConfigurationElements() ) {
					if ( MERGE_SERVICE_ELEM.equals ( confElem.getName() ) ) {
						return (IGmmMergeService) confElem.createExecutableExtension( "class" );
					}
				}
			}
		}

		return null;
	}
	
	public static IExtension[] getExtensionPointExtensions( final String p_extPointIdNs,
															final String p_extPointID ) {
		final IExtensionRegistry extensionRegistry = Platform.getExtensionRegistry();
		
		return extensionRegistry.getExtensionPoint( p_extPointIdNs, p_extPointID ).getExtensions();
	}
}
