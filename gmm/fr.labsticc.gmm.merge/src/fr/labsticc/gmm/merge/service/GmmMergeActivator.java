package fr.labsticc.gmm.merge.service;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


public class GmmMergeActivator implements BundleActivator {
	
	private static BundleContext context;

	public static BundleContext getContext() {
		return context;
	}

	@Override
	public void start(BundleContext p_context)
	throws Exception {
		context = p_context;
	}

	@Override
	public void stop(BundleContext context)
	throws Exception {
		context = null;
	}
}
