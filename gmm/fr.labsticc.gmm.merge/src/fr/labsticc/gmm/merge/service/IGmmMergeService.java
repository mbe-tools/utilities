package fr.labsticc.gmm.merge.service;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * @author dblouin
 * An interface for merge services to be used by the GMM framework. The merge process is slightly different from
 * standard merging tools such as EMF compare in the sense that it requires complete isolation between the source
 * and target resources. In other words, they cannot refer objects from the resource set of the other resources.
 */
public interface IGmmMergeService {
	
	/**
	 * @param p_sourceResource
	 * @param p_targetResource
	 * @param pb_useIds
	 * @param pb_threeWay
	 * @param p_orderAgnosticFeatures
	 * @param p_ignoredFeatures
	 * @param p_options
	 * @return true if merge was actually performed (the input resources were different). 
	 * false otherwise.
	 */
	boolean merge( 	Resource p_sourceResource,
					Resource p_targetResource,
					boolean pb_useIds,
					boolean pb_threeWay,
					Double p_nameSimilarityWeight,
					Double p_positionSimilarityWeight,
					Collection<EStructuralFeature> p_orderAgnosticFeatures,
					Collection<EStructuralFeature> p_ignoredFeatures,
					Map<?, ?> p_options );

	EList<IGmmDifference> differences( 	Resource p_resLeft,
										Resource p_resRight,
										boolean pb_useIds,
										boolean pb_threeWay,
										Double p_nameSimilarityWeight,
										Double p_positionSimilarityWeight,
										Collection<EStructuralFeature> p_orderAgnosticFeatures,
										Collection<EStructuralFeature> p_ignoredFeatures );
	
	boolean equals(	EObject p_objLeft, 
					EObject p_objRight,
					boolean pb_useIds,
					Double p_nameSimilarityWeight,
					Double p_positionSimilarityWeight );

	boolean equals(	Resource p_resLeft, 
					Resource p_resRight,
					boolean pb_useIds,
					Double p_nameSimilarityWeight,
					Double p_positionSimilarityWeight );
}
