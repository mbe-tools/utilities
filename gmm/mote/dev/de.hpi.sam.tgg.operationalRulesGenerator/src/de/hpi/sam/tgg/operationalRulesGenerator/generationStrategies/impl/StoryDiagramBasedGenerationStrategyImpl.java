/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.ActivityDiagram;
import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.StoryDiagramBasedGenerationStrategy;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.SDMInterpreterConstants;
import de.mdelab.sdm.interpreter.core.variables.Variable;
import de.mdelab.sdm.interpreter.sde.eclipse.SDEEclipseSDMInterpreter;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Story Diagram Based Generation Strategy</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * </p>
 * 
 * @generated
 */
public abstract class StoryDiagramBasedGenerationStrategyImpl extends
		GenerationStrategyImpl implements StoryDiagramBasedGenerationStrategy {
	private static final String TGG_DIAGRAM = "tggDiagram";
	private static final String RULES_PACKAGE = "rulesPackage";
	private static final String MOTE_PACKAGE = "motePackage";

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected StoryDiagramBasedGenerationStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenerationStrategiesPackage.Literals.STORY_DIAGRAM_BASED_GENERATION_STRATEGY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public URI getGeneratorStoryDiagramURI() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void generateRules(String projectName, String javaBasePackage,
			EPackage rulesPackage, TGGDiagram tggDiagram,
			final EList<TGGRule> rules )
			throws RuleGenerationException {

		ResourceSet resourceSet = new ResourceSetImpl();

		try {
			SDEEclipseSDMInterpreter sdi = new SDEEclipseSDMInterpreter(this
					.getClass().getClassLoader());

			/*
			 * Load the mote EPackage from the ecore file in the MoTE plugin
			 */
			Resource moteResource = resourceSet.getResource(URI
					.createPlatformPluginURI(
							"/de.hpi.sam.mote/model/mote.ecore", true), true);

			EPackage moteEPackage = (EPackage) moteResource.getContents()
					.get(0);

			/*
			 * Load generator story diagram
			 */
			Resource resource = resourceSet.getResource(
					this.getGeneratorStoryDiagramURI(), true);

			ActivityDiagram activityDiagram = (ActivityDiagram) resource
					.getContents().get(0);

			/*
			 * Get the activity to generate the rules
			 */
			Activity generate_rulesActivity = activityDiagram.getActivities()
					.get(0);

			/*
			 * Set up the activities parameters
			 */
			List<Variable<EClassifier>> variableBindings = new ArrayList<Variable<EClassifier>>();

			Variable<EClassifier> iv = new Variable<EClassifier>(RULES_PACKAGE,
					rulesPackage.eClass(), rulesPackage);
			variableBindings.add(iv);

			iv = new Variable<EClassifier>(MOTE_PACKAGE, moteEPackage.eClass(),
					moteEPackage);
			variableBindings.add(iv);

			iv = new Variable<EClassifier>(TGG_DIAGRAM, tggDiagram.eClass(),
					tggDiagram);
			variableBindings.add(iv);

			iv = new Variable<EClassifier>("projectName",
					EcorePackage.Literals.ESTRING, projectName);
			variableBindings.add(iv);

			iv = new Variable<EClassifier>("basePackage",
					EcorePackage.Literals.ESTRING, javaBasePackage);
			variableBindings.add(iv);

			List<ActivityDiagram> activityDiagrams = null;

			Map<String, Variable<EClassifier>> m = sdi.executeActivity(
					generate_rulesActivity, variableBindings);

			activityDiagrams = (List<ActivityDiagram>) m.get(
					SDMInterpreterConstants.RETURN_VALUE_VAR_NAME).getValue();

			/*
			 * Change the URIs of the reference packages to their nsURIs. This
			 * is necessary so references in the story diagrams are resolved
			 * using the package registry.
			 */

			URI oldRulesPackageURI = rulesPackage.eResource().getURI();
			URI oldMoteURI = moteResource.getURI();

			rulesPackage.eResource().setURI(
					URI.createURI(rulesPackage.getNsURI()));
			moteResource.setURI(URI.createURI(moteEPackage.getNsURI()));

			for (ActivityDiagram ad : activityDiagrams) {
				/*
				 * Save the generated activity diagram
				 */
				Resource generatedActivityDiagramResource = resourceSet
						.createResource(URI.createPlatformResourceURI("/"
								+ projectName + "/model/story/" + ad.getName()
								+ ".story", true));

				generatedActivityDiagramResource.getContents().add(ad);

				try {
					generatedActivityDiagramResource.save(Collections
							.emptyMap());
				} catch (IOException e) {
					throw new RuleGenerationException(
							"Could not save generated activity diagram '"
									+ ad.getName() + "'.", e);

				}
			}

			rulesPackage.eResource().setURI(oldRulesPackageURI);
			moteResource.setURI(oldMoteURI);
		} catch (SDMException e) {
			throw new RuleGenerationException("Error during rule generation.",
					e);
		}

	}

} // StoryDiagramBasedGenerationStrategyImpl
