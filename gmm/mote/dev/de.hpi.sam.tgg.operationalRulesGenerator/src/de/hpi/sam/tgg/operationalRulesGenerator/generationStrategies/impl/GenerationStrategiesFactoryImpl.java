/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.ConflictCheckTransformationGenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesFactory;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SimpleTransformationGenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SynchronizationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class GenerationStrategiesFactoryImpl extends EFactoryImpl implements
		GenerationStrategiesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static GenerationStrategiesFactory init() {
		try {
			GenerationStrategiesFactory theGenerationStrategiesFactory = (GenerationStrategiesFactory) EPackage.Registry.INSTANCE
					.getEFactory("http://operationalRulesGenerator/generationStrategies/1.0");
			if (theGenerationStrategiesFactory != null) {
				return theGenerationStrategiesFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new GenerationStrategiesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public GenerationStrategiesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case GenerationStrategiesPackage.SIMPLE_TRANSFORMATION_GENERATION_STRATEGY:
			return createSimpleTransformationGenerationStrategy();
		case GenerationStrategiesPackage.TRACEABILITY_LINK_STORE:
			return createTraceabilityLinkStore();
		case GenerationStrategiesPackage.TRACEABILITY_LINK:
			return createTraceabilityLink();
		case GenerationStrategiesPackage.CONFLICT_CHECK_TRANSFORMATION_GENERATION_STRATEGY:
			return createConflictCheckTransformationGenerationStrategy();
		case GenerationStrategiesPackage.GENERATION_INFO_STORE:
			return createGenerationInfoStore();
		case GenerationStrategiesPackage.RULE_INFO_STORE:
			return createRuleInfoStore();
		case GenerationStrategiesPackage.SYNCHRONIZATION_STRATEGY:
			return createSynchronizationStrategy();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case GenerationStrategiesPackage.RULE_GENERATION_EXCEPTION:
			return createRuleGenerationExceptionFromString(eDataType,
					initialValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case GenerationStrategiesPackage.RULE_GENERATION_EXCEPTION:
			return convertRuleGenerationExceptionToString(eDataType,
					instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleTransformationGenerationStrategy createSimpleTransformationGenerationStrategy() {
		SimpleTransformationGenerationStrategyImpl simpleTransformationGenerationStrategy = new SimpleTransformationGenerationStrategyImpl();
		return simpleTransformationGenerationStrategy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TraceabilityLinkStore createTraceabilityLinkStore() {
		TraceabilityLinkStoreImpl traceabilityLinkStore = new TraceabilityLinkStoreImpl();
		return traceabilityLinkStore;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TraceabilityLink createTraceabilityLink() {
		TraceabilityLinkImpl traceabilityLink = new TraceabilityLinkImpl();
		return traceabilityLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ConflictCheckTransformationGenerationStrategy createConflictCheckTransformationGenerationStrategy() {
		ConflictCheckTransformationGenerationStrategyImpl conflictCheckTransformationGenerationStrategy = new ConflictCheckTransformationGenerationStrategyImpl();
		return conflictCheckTransformationGenerationStrategy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public GenerationInfoStore createGenerationInfoStore() {
		GenerationInfoStoreImpl generationInfoStore = new GenerationInfoStoreImpl();
		return generationInfoStore;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RuleInfoStore createRuleInfoStore() {
		RuleInfoStoreImpl ruleInfoStore = new RuleInfoStoreImpl();
		return ruleInfoStore;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SynchronizationStrategy createSynchronizationStrategy() {
		SynchronizationStrategyImpl synchronizationStrategy = new SynchronizationStrategyImpl();
		return synchronizationStrategy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RuleGenerationException createRuleGenerationExceptionFromString(
			EDataType eDataType, String initialValue) {
		return (RuleGenerationException) super.createFromString(eDataType,
				initialValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRuleGenerationExceptionToString(EDataType eDataType,
			Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public GenerationStrategiesPackage getGenerationStrategiesPackage() {
		return (GenerationStrategiesPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static GenerationStrategiesPackage getPackage() {
		return GenerationStrategiesPackage.eINSTANCE;
	}

} // GenerationStrategiesFactoryImpl
