/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleGenerationException;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import de.mdelab.workflow.util.WorkflowUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Generation Strategy Component</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.GenerationStrategyComponentImpl#getTggFileURI <em>Tgg File URI</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.GenerationStrategyComponentImpl#getProjectName <em>Project Name</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.GenerationStrategyComponentImpl#getCorrespondenceMetamodelURI <em>Correspondence Metamodel URI</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.GenerationStrategyComponentImpl#getJavaBasePackage <em>Java Base Package</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.GenerationStrategyComponentImpl#getGenerationStrategy <em>Generation Strategy</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.GenerationStrategyComponentImpl#getRulesToGenerate <em>Rules To Generate</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GenerationStrategyComponentImpl extends WorkflowComponentImpl
		implements GenerationStrategyComponent {
	/**
	 * The default value of the '{@link #getTggFileURI() <em>Tgg File URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTggFileURI()
	 * @generated
	 * @ordered
	 */
	protected static final String TGG_FILE_URI_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getTggFileURI() <em>Tgg File URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTggFileURI()
	 * @generated
	 * @ordered
	 */
	protected String tggFileURI = TGG_FILE_URI_EDEFAULT;
	/**
	 * The default value of the '{@link #getProjectName() <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getProjectName()
	 * @generated
	 * @ordered
	 */
	protected static final String PROJECT_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getProjectName() <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getProjectName()
	 * @generated
	 * @ordered
	 */
	protected String projectName = PROJECT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getCorrespondenceMetamodelURI() <em>Correspondence Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getCorrespondenceMetamodelURI()
	 * @generated
	 * @ordered
	 */
	protected static final String CORRESPONDENCE_METAMODEL_URI_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getCorrespondenceMetamodelURI() <em>Correspondence Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getCorrespondenceMetamodelURI()
	 * @generated
	 * @ordered
	 */
	protected String correspondenceMetamodelURI = CORRESPONDENCE_METAMODEL_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getJavaBasePackage() <em>Java Base Package</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getJavaBasePackage()
	 * @generated
	 * @ordered
	 */
	protected static final String JAVA_BASE_PACKAGE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getJavaBasePackage() <em>Java Base Package</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getJavaBasePackage()
	 * @generated
	 * @ordered
	 */
	protected String javaBasePackage = JAVA_BASE_PACKAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getGenerationStrategy() <em>Generation Strategy</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getGenerationStrategy()
	 * @generated
	 * @ordered
	 */
	protected static final String GENERATION_STRATEGY_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getGenerationStrategy() <em>Generation Strategy</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getGenerationStrategy()
	 * @generated
	 * @ordered
	 */
	protected String generationStrategy = GENERATION_STRATEGY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRulesToGenerate() <em>Rules To Generate</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRulesToGenerate()
	 * @generated
	 * @ordered
	 */
	protected EList<TGGRule> rulesToGenerate;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected GenerationStrategyComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowComponentsPackage.Literals.GENERATION_STRATEGY_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getGenerationStrategy() {
		return generationStrategy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenerationStrategy(String newGenerationStrategy) {
		String oldGenerationStrategy = generationStrategy;
		generationStrategy = newGenerationStrategy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__GENERATION_STRATEGY,
					oldGenerationStrategy, generationStrategy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TGGRule> getRulesToGenerate() {
		if (rulesToGenerate == null) {
			rulesToGenerate = new EObjectResolvingEList<TGGRule>(
					TGGRule.class,
					this,
					WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__RULES_TO_GENERATE);
		}
		return rulesToGenerate;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getTggFileURI() {
		return tggFileURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTggFileURI(String newTggFileURI) {
		String oldTggFileURI = tggFileURI;
		tggFileURI = newTggFileURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__TGG_FILE_URI,
					oldTggFileURI, tggFileURI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setProjectName(String newProjectName) {
		String oldProjectName = projectName;
		projectName = newProjectName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__PROJECT_NAME,
					oldProjectName, projectName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getCorrespondenceMetamodelURI() {
		return correspondenceMetamodelURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCorrespondenceMetamodelURI(
			String newCorrespondenceMetamodelURI) {
		String oldCorrespondenceMetamodelURI = correspondenceMetamodelURI;
		correspondenceMetamodelURI = newCorrespondenceMetamodelURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__CORRESPONDENCE_METAMODEL_URI,
					oldCorrespondenceMetamodelURI, correspondenceMetamodelURI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getJavaBasePackage() {
		return javaBasePackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setJavaBasePackage(String newJavaBasePackage) {
		String oldJavaBasePackage = javaBasePackage;
		javaBasePackage = newJavaBasePackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__JAVA_BASE_PACKAGE,
					oldJavaBasePackage, javaBasePackage));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__TGG_FILE_URI:
			return getTggFileURI();
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__PROJECT_NAME:
			return getProjectName();
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__CORRESPONDENCE_METAMODEL_URI:
			return getCorrespondenceMetamodelURI();
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__JAVA_BASE_PACKAGE:
			return getJavaBasePackage();
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__GENERATION_STRATEGY:
			return getGenerationStrategy();
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__RULES_TO_GENERATE:
			return getRulesToGenerate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__TGG_FILE_URI:
			setTggFileURI((String) newValue);
			return;
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__PROJECT_NAME:
			setProjectName((String) newValue);
			return;
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__CORRESPONDENCE_METAMODEL_URI:
			setCorrespondenceMetamodelURI((String) newValue);
			return;
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__JAVA_BASE_PACKAGE:
			setJavaBasePackage((String) newValue);
			return;
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__GENERATION_STRATEGY:
			setGenerationStrategy((String) newValue);
			return;
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__RULES_TO_GENERATE:
			getRulesToGenerate().clear();
			getRulesToGenerate().addAll(
					(Collection<? extends TGGRule>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__TGG_FILE_URI:
			setTggFileURI(TGG_FILE_URI_EDEFAULT);
			return;
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__PROJECT_NAME:
			setProjectName(PROJECT_NAME_EDEFAULT);
			return;
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__CORRESPONDENCE_METAMODEL_URI:
			setCorrespondenceMetamodelURI(CORRESPONDENCE_METAMODEL_URI_EDEFAULT);
			return;
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__JAVA_BASE_PACKAGE:
			setJavaBasePackage(JAVA_BASE_PACKAGE_EDEFAULT);
			return;
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__GENERATION_STRATEGY:
			setGenerationStrategy(GENERATION_STRATEGY_EDEFAULT);
			return;
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__RULES_TO_GENERATE:
			getRulesToGenerate().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__TGG_FILE_URI:
			return TGG_FILE_URI_EDEFAULT == null ? tggFileURI != null
					: !TGG_FILE_URI_EDEFAULT.equals(tggFileURI);
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__PROJECT_NAME:
			return PROJECT_NAME_EDEFAULT == null ? projectName != null
					: !PROJECT_NAME_EDEFAULT.equals(projectName);
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__CORRESPONDENCE_METAMODEL_URI:
			return CORRESPONDENCE_METAMODEL_URI_EDEFAULT == null ? correspondenceMetamodelURI != null
					: !CORRESPONDENCE_METAMODEL_URI_EDEFAULT
							.equals(correspondenceMetamodelURI);
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__JAVA_BASE_PACKAGE:
			return JAVA_BASE_PACKAGE_EDEFAULT == null ? javaBasePackage != null
					: !JAVA_BASE_PACKAGE_EDEFAULT.equals(javaBasePackage);
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__GENERATION_STRATEGY:
			return GENERATION_STRATEGY_EDEFAULT == null ? generationStrategy != null
					: !GENERATION_STRATEGY_EDEFAULT.equals(generationStrategy);
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT__RULES_TO_GENERATE:
			return rulesToGenerate != null && !rulesToGenerate.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tggFileURI: ");
		result.append(tggFileURI);
		result.append(", projectName: ");
		result.append(projectName);
		result.append(", correspondenceMetamodelURI: ");
		result.append(correspondenceMetamodelURI);
		result.append(", javaBasePackage: ");
		result.append(javaBasePackage);
		result.append(", generationStrategy: ");
		result.append(generationStrategy);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean checkConfiguration(WorkflowExecutionContext context)
			throws IOException {
		boolean success = true;

		if (getGenerationStrategy() == null
				|| "".equals(getGenerationStrategy())) {
			context.getLogger().addError("no generation strategy set.", null,
					this);
			success = false;
		} else if (GenerationStrategiesPackage.eINSTANCE
				.getEClassifier(getGenerationStrategy()) == null) {
			context.getLogger().addError(
					"There is no generation strategy '"
							+ getGenerationStrategy() + "' in package "
							+ GenerationStrategiesPackage.eNS_URI + ".", null,
					this);
			success = false;
		}

		if (getProjectName() == null || "".equals(getProjectName())) {
			context.getLogger().addError("no project name set.", null, this);
			success = false;
		}

		if (getJavaBasePackage() == null || "".equals(getJavaBasePackage())) {
			context.getLogger().addError("no java base package set.", null,
					this);
			success = false;
		}

		if (getTggFileURI() == null || "".equals(getTggFileURI())) {
			context.getLogger().addError("no tggFileURI set.", null, this);
			success = false;
		}

		if (getCorrespondenceMetamodelURI() == null
				|| "".equals(getCorrespondenceMetamodelURI())) {
			context.getLogger().addError("no correspondenceMetamodelURI set.",
					null, this);
			success = false;
		}

		return success;
	}

	@Override
	public void execute(WorkflowExecutionContext context)
			throws WorkflowExecutionException, IOException {
		URI tggFileURI = WorkflowUtil.getResolvedURI(
				URI.createURI(getTggFileURI()), context.getWorkflowFileURI());
		URI correspondenceMetamodelURI = WorkflowUtil.getResolvedURI(
				URI.createURI(getCorrespondenceMetamodelURI()),
				context.getWorkflowFileURI());

		ResourceSet rs = context.getGlobalResourceSet();

		/*
		 * Get a generation strategy
		 */
		GenerationStrategy generationStrategy = (GenerationStrategy) GenerationStrategiesPackage.eINSTANCE
				.getEFactoryInstance().create(
						(EClass) GenerationStrategiesPackage.eINSTANCE
								.getEClassifier(getGenerationStrategy()));

		/*
		 * Get the TGGDiagram
		 */
		Resource tggDiagramResource = rs.getResource(tggFileURI, true);

		TGGDiagram tggDiagram = (TGGDiagram) tggDiagramResource.getContents()
				.get(0);

		/*
		 * Get the EPackage the will contain the generated rule classes
		 */
		Resource correspondenceMetamodelResource = rs.getResource(
				correspondenceMetamodelURI, true);

		EPackage correspondenceMetamodelPackage = (EPackage) correspondenceMetamodelResource
				.getContents().get(0);

		try {
			context.getLogger().addInfo("Generating rules...", this);
			
			// Prune disabled rules
			final Iterator<TGGRule> tggRulesIt = tggDiagram.getTggRules().iterator();
			
			while( tggRulesIt.hasNext() ) {
				final TGGRule rule = tggRulesIt.next();
				
				if ( rule.isDisabled() ) {
					tggRulesIt.remove();
				}
			}
			
			generationStrategy.generateRules(getProjectName(),
					getJavaBasePackage(), correspondenceMetamodelPackage,
					tggDiagram, getRulesToGenerate());
		} catch (RuleGenerationException e) {
			context.getLogger().addError("Error during rule generation.", e,
					this);

			throw new WorkflowExecutionException(
					"Error during rule generation.", e);
		}

		try {
			correspondenceMetamodelResource.save(Collections.emptyMap());
		} catch (IOException e) {
			context.getLogger().addError(
					"Could not save generated rules package.", e, this);

			throw new WorkflowExecutionException(
					"Could not save generated rules package.", e);
		}
	}

} // GenerationStrategyComponentImpl
