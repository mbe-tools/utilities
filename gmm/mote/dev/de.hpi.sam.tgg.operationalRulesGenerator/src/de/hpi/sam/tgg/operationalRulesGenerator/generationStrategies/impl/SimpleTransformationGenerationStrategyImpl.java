/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;

import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SimpleTransformationGenerationStrategy;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Simple Transformation Generation Strategy</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * </p>
 * 
 * @generated
 */
public class SimpleTransformationGenerationStrategyImpl extends
		StoryDiagramBasedGenerationStrategyImpl implements
		SimpleTransformationGenerationStrategy {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleTransformationGenerationStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenerationStrategiesPackage.Literals.SIMPLE_TRANSFORMATION_GENERATION_STRATEGY;
	}

	@Override
	public URI getGeneratorStoryDiagramURI() {
		return URI
				.createPlatformPluginURI(
						"/de.hpi.sam.tgg.operationalRulesGenerator/generatorResources/SimpleTransformationGenerationStrategy/generate_rules.story",
						true);
	}

} // SimpleTransformationGenerationStrategyImpl
