/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorPackage
 * @generated
 */
public interface OperationalRulesGeneratorFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	OperationalRulesGeneratorFactory eINSTANCE = de.hpi.sam.tgg.operationalRulesGenerator.impl.OperationalRulesGeneratorFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>Operational Rules Generator</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return a new object of class '<em>Operational Rules Generator</em>'.
	 * @generated
	 */
	OperationalRulesGenerator createOperationalRulesGenerator();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OperationalRulesGeneratorPackage getOperationalRulesGeneratorPackage();

} // OperationalRulesGeneratorFactory
