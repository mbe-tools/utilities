/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents;

import de.hpi.sam.tgg.TGGRule;
import de.mdelab.workflow.components.WorkflowComponent;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Generation Strategy Component</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Invokes a GenerationStrategy to generate the rules.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getTggFileURI <em>Tgg File URI</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getProjectName <em>Project Name</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getCorrespondenceMetamodelURI <em>Correspondence Metamodel URI</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getJavaBasePackage <em>Java Base Package</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getGenerationStrategy <em>Generation Strategy</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getRulesToGenerate <em>Rules To Generate</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getGenerationStrategyComponent()
 * @model
 * @generated
 */
public interface GenerationStrategyComponent extends WorkflowComponent {

	/**
	 * Returns the value of the '<em><b>Generation Strategy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generation Strategy</em>' containment
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generation Strategy</em>' attribute.
	 * @see #setGenerationStrategy(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getGenerationStrategyComponent_GenerationStrategy()
	 * @model required="true"
	 * @generated
	 */
	String getGenerationStrategy();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getGenerationStrategy <em>Generation Strategy</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Generation Strategy</em>' attribute.
	 * @see #getGenerationStrategy()
	 * @generated
	 */
	void setGenerationStrategy(String value);

	/**
	 * Returns the value of the '<em><b>Rules To Generate</b></em>' reference list.
	 * The list contents are of type {@link de.hpi.sam.tgg.TGGRule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rules To Generate</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules To Generate</em>' reference list.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getGenerationStrategyComponent_RulesToGenerate()
	 * @model
	 * @generated
	 */
	EList<TGGRule> getRulesToGenerate();

	/**
	 * Returns the value of the '<em><b>Tgg File URI</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgg File URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Tgg File URI</em>' attribute.
	 * @see #setTggFileURI(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getGenerationStrategyComponent_TggFileURI()
	 * @model required="true"
	 * @generated
	 */
	String getTggFileURI();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getTggFileURI <em>Tgg File URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Tgg File URI</em>' attribute.
	 * @see #getTggFileURI()
	 * @generated
	 */
	void setTggFileURI(String value);

	/**
	 * Returns the value of the '<em><b>Project Name</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Project Name</em>' attribute.
	 * @see #setProjectName(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getGenerationStrategyComponent_ProjectName()
	 * @model required="true"
	 * @generated
	 */
	String getProjectName();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getProjectName <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Project Name</em>' attribute.
	 * @see #getProjectName()
	 * @generated
	 */
	void setProjectName(String value);

	/**
	 * Returns the value of the '<em><b>Correspondence Metamodel URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correspondence Metamodel URI</em>' attribute
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correspondence Metamodel URI</em>' attribute.
	 * @see #setCorrespondenceMetamodelURI(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getGenerationStrategyComponent_CorrespondenceMetamodelURI()
	 * @model required="true"
	 * @generated
	 */
	String getCorrespondenceMetamodelURI();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getCorrespondenceMetamodelURI <em>Correspondence Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Correspondence Metamodel URI</em>' attribute.
	 * @see #getCorrespondenceMetamodelURI()
	 * @generated
	 */
	void setCorrespondenceMetamodelURI(String value);

	/**
	 * Returns the value of the '<em><b>Java Base Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Java Base Package</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Java Base Package</em>' attribute.
	 * @see #setJavaBasePackage(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getGenerationStrategyComponent_JavaBasePackage()
	 * @model required="true"
	 * @generated
	 */
	String getJavaBasePackage();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getJavaBasePackage <em>Java Base Package</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Java Base Package</em>' attribute.
	 * @see #getJavaBasePackage()
	 * @generated
	 */
	void setJavaBasePackage(String value);

} // GenerationStrategyComponent
