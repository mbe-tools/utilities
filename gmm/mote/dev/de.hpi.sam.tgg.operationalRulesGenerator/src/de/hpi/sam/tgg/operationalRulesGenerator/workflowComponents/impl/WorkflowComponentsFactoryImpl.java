/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsFactory;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class WorkflowComponentsFactoryImpl extends EFactoryImpl implements
		WorkflowComponentsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static WorkflowComponentsFactory init() {
		try {
			WorkflowComponentsFactory theWorkflowComponentsFactory = (WorkflowComponentsFactory) EPackage.Registry.INSTANCE
					.getEFactory("http://operationalRulesGenerator/workflowComponents/1.0");
			if (theWorkflowComponentsFactory != null) {
				return theWorkflowComponentsFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WorkflowComponentsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public WorkflowComponentsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case WorkflowComponentsPackage.MANIFEST_LOADER_COMPONENT:
			return createManifestLoaderComponent();
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT:
			return createGenerationStrategyComponent();
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT:
			return createEMFCodeGenerationComponent();
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT:
			return createManifestGeneratorComponent();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ManifestLoaderComponent createManifestLoaderComponent() {
		ManifestLoaderComponentImpl manifestLoaderComponent = new ManifestLoaderComponentImpl();
		return manifestLoaderComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public GenerationStrategyComponent createGenerationStrategyComponent() {
		GenerationStrategyComponentImpl generationStrategyComponent = new GenerationStrategyComponentImpl();
		return generationStrategyComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMFCodeGenerationComponent createEMFCodeGenerationComponent() {
		EMFCodeGenerationComponentImpl emfCodeGenerationComponent = new EMFCodeGenerationComponentImpl();
		return emfCodeGenerationComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ManifestGeneratorComponent createManifestGeneratorComponent() {
		ManifestGeneratorComponentImpl manifestGeneratorComponent = new ManifestGeneratorComponentImpl();
		return manifestGeneratorComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowComponentsPackage getWorkflowComponentsPackage() {
		return (WorkflowComponentsPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WorkflowComponentsPackage getPackage() {
		return WorkflowComponentsPackage.eINSTANCE;
	}

} // WorkflowComponentsFactoryImpl
