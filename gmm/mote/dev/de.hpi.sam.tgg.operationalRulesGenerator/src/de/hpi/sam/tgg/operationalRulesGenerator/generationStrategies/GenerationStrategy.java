/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleGenerationException;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Generation Strategy</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The GenerationStrategy is responsible for actually processing the TGG rules and creating the operational rules.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getGenerationStrategy()
 * @model abstract="true"
 * @generated
 */
public interface GenerationStrategy extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleGenerationException" rulesMany="true"
	 * @generated
	 */
	void generateRules(String projectURI, String javaBasePackage,
			EPackage rulesPackage, TGGDiagram tggDiagram, EList<TGGRule> rules)
			throws RuleGenerationException;

} // GenerationStrategy
