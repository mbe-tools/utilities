/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator;

import java.io.OutputStream;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Operational Rules Generator</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Generates a ruleSet plugin from a TGG file.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorPackage#getOperationalRulesGenerator()
 * @model
 * @generated
 */
public interface OperationalRulesGenerator extends EObject {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model outputStreamDataType="de.mdelab.workflow.helpers.OutputStream"
	 * @generated
	 */
	void generate(String tggFileURI, OutputStream outputStream);

} // OperationalRulesGenerator
