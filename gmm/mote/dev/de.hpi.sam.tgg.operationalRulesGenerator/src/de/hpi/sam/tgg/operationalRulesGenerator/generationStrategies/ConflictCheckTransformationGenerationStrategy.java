/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Conflict Check Transformation Generation Strategy</b></em>'. <!--
 * end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This generation strategy creates Story Diagrams that perform only model transformation but also include a runtime conflict check.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getConflictCheckTransformationGenerationStrategy()
 * @model
 * @generated
 */
public interface ConflictCheckTransformationGenerationStrategy extends
		GenerationStrategy {
} // ConflictCheckTransformationGenerationStrategy
