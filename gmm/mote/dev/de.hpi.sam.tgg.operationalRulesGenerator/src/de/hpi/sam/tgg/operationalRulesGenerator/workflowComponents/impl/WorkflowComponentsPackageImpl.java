/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl;
import de.hpi.sam.tgg.operationalRulesGenerator.impl.OperationalRulesGeneratorPackageImpl;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsFactory;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage;
import de.mdelab.workflow.WorkflowPackage;
import de.mdelab.workflow.components.ComponentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class WorkflowComponentsPackageImpl extends EPackageImpl implements
		WorkflowComponentsPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass manifestLoaderComponentEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass generationStrategyComponentEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass emfCodeGenerationComponentEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass manifestGeneratorComponentEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorkflowComponentsPackageImpl() {
		super(eNS_URI, WorkflowComponentsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link WorkflowComponentsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorkflowComponentsPackage init() {
		if (isInited)
			return (WorkflowComponentsPackage) EPackage.Registry.INSTANCE
					.getEPackage(WorkflowComponentsPackage.eNS_URI);

		// Obtain or create and register package
		WorkflowComponentsPackageImpl theWorkflowComponentsPackage = (WorkflowComponentsPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof WorkflowComponentsPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new WorkflowComponentsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		TggPackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		OperationalRulesGeneratorPackageImpl theOperationalRulesGeneratorPackage = (OperationalRulesGeneratorPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(OperationalRulesGeneratorPackage.eNS_URI) instanceof OperationalRulesGeneratorPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(OperationalRulesGeneratorPackage.eNS_URI)
				: OperationalRulesGeneratorPackage.eINSTANCE);
		GenerationStrategiesPackageImpl theGenerationStrategiesPackage = (GenerationStrategiesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(GenerationStrategiesPackage.eNS_URI) instanceof GenerationStrategiesPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(GenerationStrategiesPackage.eNS_URI)
				: GenerationStrategiesPackage.eINSTANCE);

		// Create package meta-data objects
		theWorkflowComponentsPackage.createPackageContents();
		theOperationalRulesGeneratorPackage.createPackageContents();
		theGenerationStrategiesPackage.createPackageContents();

		// Initialize created meta-data
		theWorkflowComponentsPackage.initializePackageContents();
		theOperationalRulesGeneratorPackage.initializePackageContents();
		theGenerationStrategiesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWorkflowComponentsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorkflowComponentsPackage.eNS_URI,
				theWorkflowComponentsPackage);
		return theWorkflowComponentsPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getManifestLoaderComponent() {
		return manifestLoaderComponentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getManifestLoaderComponent_ModelSlot() {
		return (EAttribute) manifestLoaderComponentEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getManifestLoaderComponent_ProjectName() {
		return (EAttribute) manifestLoaderComponentEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGenerationStrategyComponent() {
		return generationStrategyComponentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGenerationStrategyComponent_GenerationStrategy() {
		return (EAttribute) generationStrategyComponentEClass
				.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGenerationStrategyComponent_RulesToGenerate() {
		return (EReference) generationStrategyComponentEClass
				.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGenerationStrategyComponent_TggFileURI() {
		return (EAttribute) generationStrategyComponentEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGenerationStrategyComponent_ProjectName() {
		return (EAttribute) generationStrategyComponentEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGenerationStrategyComponent_CorrespondenceMetamodelURI() {
		return (EAttribute) generationStrategyComponentEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGenerationStrategyComponent_JavaBasePackage() {
		return (EAttribute) generationStrategyComponentEClass
				.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEMFCodeGenerationComponent() {
		return emfCodeGenerationComponentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEMFCodeGenerationComponent_ProjectName() {
		return (EAttribute) emfCodeGenerationComponentEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEMFCodeGenerationComponent_TggFileURI() {
		return (EAttribute) emfCodeGenerationComponentEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEMFCodeGenerationComponent_CorrespondenceMetamodelURI() {
		return (EAttribute) emfCodeGenerationComponentEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEMFCodeGenerationComponent_JavaBasePackage() {
		return (EAttribute) emfCodeGenerationComponentEClass
				.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getManifestGeneratorComponent() {
		return manifestGeneratorComponentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getManifestGeneratorComponent_ManifestSlot() {
		return (EAttribute) manifestGeneratorComponentEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getManifestGeneratorComponent_ProjectName() {
		return (EAttribute) manifestGeneratorComponentEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getManifestGeneratorComponent_TggFileURI() {
		return (EAttribute) manifestGeneratorComponentEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getManifestGeneratorComponent_CorrespondenceMetamodelURI() {
		return (EAttribute) manifestGeneratorComponentEClass
				.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowComponentsFactory getWorkflowComponentsFactory() {
		return (WorkflowComponentsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		manifestLoaderComponentEClass = createEClass(MANIFEST_LOADER_COMPONENT);
		createEAttribute(manifestLoaderComponentEClass,
				MANIFEST_LOADER_COMPONENT__MODEL_SLOT);
		createEAttribute(manifestLoaderComponentEClass,
				MANIFEST_LOADER_COMPONENT__PROJECT_NAME);

		generationStrategyComponentEClass = createEClass(GENERATION_STRATEGY_COMPONENT);
		createEAttribute(generationStrategyComponentEClass,
				GENERATION_STRATEGY_COMPONENT__TGG_FILE_URI);
		createEAttribute(generationStrategyComponentEClass,
				GENERATION_STRATEGY_COMPONENT__PROJECT_NAME);
		createEAttribute(generationStrategyComponentEClass,
				GENERATION_STRATEGY_COMPONENT__CORRESPONDENCE_METAMODEL_URI);
		createEAttribute(generationStrategyComponentEClass,
				GENERATION_STRATEGY_COMPONENT__JAVA_BASE_PACKAGE);
		createEAttribute(generationStrategyComponentEClass,
				GENERATION_STRATEGY_COMPONENT__GENERATION_STRATEGY);
		createEReference(generationStrategyComponentEClass,
				GENERATION_STRATEGY_COMPONENT__RULES_TO_GENERATE);

		emfCodeGenerationComponentEClass = createEClass(EMF_CODE_GENERATION_COMPONENT);
		createEAttribute(emfCodeGenerationComponentEClass,
				EMF_CODE_GENERATION_COMPONENT__PROJECT_NAME);
		createEAttribute(emfCodeGenerationComponentEClass,
				EMF_CODE_GENERATION_COMPONENT__TGG_FILE_URI);
		createEAttribute(emfCodeGenerationComponentEClass,
				EMF_CODE_GENERATION_COMPONENT__CORRESPONDENCE_METAMODEL_URI);
		createEAttribute(emfCodeGenerationComponentEClass,
				EMF_CODE_GENERATION_COMPONENT__JAVA_BASE_PACKAGE);

		manifestGeneratorComponentEClass = createEClass(MANIFEST_GENERATOR_COMPONENT);
		createEAttribute(manifestGeneratorComponentEClass,
				MANIFEST_GENERATOR_COMPONENT__MANIFEST_SLOT);
		createEAttribute(manifestGeneratorComponentEClass,
				MANIFEST_GENERATOR_COMPONENT__PROJECT_NAME);
		createEAttribute(manifestGeneratorComponentEClass,
				MANIFEST_GENERATOR_COMPONENT__TGG_FILE_URI);
		createEAttribute(manifestGeneratorComponentEClass,
				MANIFEST_GENERATOR_COMPONENT__CORRESPONDENCE_METAMODEL_URI);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ComponentsPackage theComponentsPackage = (ComponentsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ComponentsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage) EPackage.Registry.INSTANCE
				.getEPackage(EcorePackage.eNS_URI);
		TggPackage theTggPackage = (TggPackage) EPackage.Registry.INSTANCE
				.getEPackage(TggPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		manifestLoaderComponentEClass.getESuperTypes().add(
				theComponentsPackage.getWorkflowComponent());
		generationStrategyComponentEClass.getESuperTypes().add(
				theComponentsPackage.getWorkflowComponent());
		emfCodeGenerationComponentEClass.getESuperTypes().add(
				theComponentsPackage.getWorkflowComponent());
		manifestGeneratorComponentEClass.getESuperTypes().add(
				theComponentsPackage.getWorkflowComponent());

		// Initialize classes and features; add operations and parameters
		initEClass(manifestLoaderComponentEClass,
				ManifestLoaderComponent.class, "ManifestLoaderComponent",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getManifestLoaderComponent_ModelSlot(),
				ecorePackage.getEString(), "modelSlot", null, 1, 1,
				ManifestLoaderComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getManifestLoaderComponent_ProjectName(),
				ecorePackage.getEString(), "projectName", null, 1, 1,
				ManifestLoaderComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(generationStrategyComponentEClass,
				GenerationStrategyComponent.class,
				"GenerationStrategyComponent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGenerationStrategyComponent_TggFileURI(),
				ecorePackage.getEString(), "tggFileURI", null, 1, 1,
				GenerationStrategyComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getGenerationStrategyComponent_ProjectName(),
				ecorePackage.getEString(), "projectName", null, 1, 1,
				GenerationStrategyComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(
				getGenerationStrategyComponent_CorrespondenceMetamodelURI(),
				theEcorePackage.getEString(), "correspondenceMetamodelURI",
				null, 1, 1, GenerationStrategyComponent.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getGenerationStrategyComponent_JavaBasePackage(),
				theEcorePackage.getEString(), "javaBasePackage", null, 1, 1,
				GenerationStrategyComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getGenerationStrategyComponent_GenerationStrategy(),
				theEcorePackage.getEString(), "generationStrategy", null, 1, 1,
				GenerationStrategyComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getGenerationStrategyComponent_RulesToGenerate(),
				theTggPackage.getTGGRule(), null, "rulesToGenerate", null, 0,
				-1, GenerationStrategyComponent.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(emfCodeGenerationComponentEClass,
				EMFCodeGenerationComponent.class, "EMFCodeGenerationComponent",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEMFCodeGenerationComponent_ProjectName(),
				theEcorePackage.getEString(), "projectName", null, 1, 1,
				EMFCodeGenerationComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getEMFCodeGenerationComponent_TggFileURI(),
				theEcorePackage.getEString(), "tggFileURI", null, 1, 1,
				EMFCodeGenerationComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(
				getEMFCodeGenerationComponent_CorrespondenceMetamodelURI(),
				theEcorePackage.getEString(), "correspondenceMetamodelURI",
				null, 1, 1, EMFCodeGenerationComponent.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getEMFCodeGenerationComponent_JavaBasePackage(),
				theEcorePackage.getEString(), "javaBasePackage", null, 0, 1,
				EMFCodeGenerationComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(manifestGeneratorComponentEClass,
				ManifestGeneratorComponent.class, "ManifestGeneratorComponent",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getManifestGeneratorComponent_ManifestSlot(),
				theEcorePackage.getEString(), "manifestSlot", null, 1, 1,
				ManifestGeneratorComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getManifestGeneratorComponent_ProjectName(),
				theEcorePackage.getEString(), "projectName", null, 1, 1,
				ManifestGeneratorComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getManifestGeneratorComponent_TggFileURI(),
				theEcorePackage.getEString(), "tggFileURI", null, 1, 1,
				ManifestGeneratorComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(
				getManifestGeneratorComponent_CorrespondenceMetamodelURI(),
				theEcorePackage.getEString(), "correspondenceMetamodelURI",
				null, 1, 1, ManifestGeneratorComponent.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
	}

} // WorkflowComponentsPackageImpl
