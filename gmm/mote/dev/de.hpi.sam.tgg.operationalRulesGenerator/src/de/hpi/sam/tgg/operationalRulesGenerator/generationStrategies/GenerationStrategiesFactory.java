/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage
 * @generated
 */
public interface GenerationStrategiesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	GenerationStrategiesFactory eINSTANCE = de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>Simple Transformation Generation Strategy</em>'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple Transformation Generation Strategy</em>'.
	 * @generated
	 */
	SimpleTransformationGenerationStrategy createSimpleTransformationGenerationStrategy();

	/**
	 * Returns a new object of class '<em>Traceability Link Store</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Traceability Link Store</em>'.
	 * @generated
	 */
	TraceabilityLinkStore createTraceabilityLinkStore();

	/**
	 * Returns a new object of class '<em>Traceability Link</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Traceability Link</em>'.
	 * @generated
	 */
	TraceabilityLink createTraceabilityLink();

	/**
	 * Returns a new object of class '
	 * <em>Conflict Check Transformation Generation Strategy</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '
	 *         <em>Conflict Check Transformation Generation Strategy</em>'.
	 * @generated
	 */
	ConflictCheckTransformationGenerationStrategy createConflictCheckTransformationGenerationStrategy();

	/**
	 * Returns a new object of class '<em>Generation Info Store</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Generation Info Store</em>'.
	 * @generated
	 */
	GenerationInfoStore createGenerationInfoStore();

	/**
	 * Returns a new object of class '<em>Rule Info Store</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Rule Info Store</em>'.
	 * @generated
	 */
	RuleInfoStore createRuleInfoStore();

	/**
	 * Returns a new object of class '<em>Synchronization Strategy</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Synchronization Strategy</em>'.
	 * @generated
	 */
	SynchronizationStrategy createSynchronizationStrategy();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	GenerationStrategiesPackage getGenerationStrategiesPackage();

} // GenerationStrategiesFactory
