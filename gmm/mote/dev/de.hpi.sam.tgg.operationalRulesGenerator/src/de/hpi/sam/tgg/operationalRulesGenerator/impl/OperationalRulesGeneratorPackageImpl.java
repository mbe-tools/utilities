/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGenerator;
import de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorFactory;
import de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.WorkflowComponentsPackageImpl;
import de.mdelab.workflow.WorkflowPackage;
import de.mdelab.workflow.helpers.HelpersPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class OperationalRulesGeneratorPackageImpl extends EPackageImpl
		implements OperationalRulesGeneratorPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationalRulesGeneratorEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OperationalRulesGeneratorPackageImpl() {
		super(eNS_URI, OperationalRulesGeneratorFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OperationalRulesGeneratorPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OperationalRulesGeneratorPackage init() {
		if (isInited)
			return (OperationalRulesGeneratorPackage) EPackage.Registry.INSTANCE
					.getEPackage(OperationalRulesGeneratorPackage.eNS_URI);

		// Obtain or create and register package
		OperationalRulesGeneratorPackageImpl theOperationalRulesGeneratorPackage = (OperationalRulesGeneratorPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof OperationalRulesGeneratorPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new OperationalRulesGeneratorPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		TggPackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		WorkflowComponentsPackageImpl theWorkflowComponentsPackage = (WorkflowComponentsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(WorkflowComponentsPackage.eNS_URI) instanceof WorkflowComponentsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(WorkflowComponentsPackage.eNS_URI)
				: WorkflowComponentsPackage.eINSTANCE);
		GenerationStrategiesPackageImpl theGenerationStrategiesPackage = (GenerationStrategiesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(GenerationStrategiesPackage.eNS_URI) instanceof GenerationStrategiesPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(GenerationStrategiesPackage.eNS_URI)
				: GenerationStrategiesPackage.eINSTANCE);

		// Create package meta-data objects
		theOperationalRulesGeneratorPackage.createPackageContents();
		theWorkflowComponentsPackage.createPackageContents();
		theGenerationStrategiesPackage.createPackageContents();

		// Initialize created meta-data
		theOperationalRulesGeneratorPackage.initializePackageContents();
		theWorkflowComponentsPackage.initializePackageContents();
		theGenerationStrategiesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOperationalRulesGeneratorPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(
				OperationalRulesGeneratorPackage.eNS_URI,
				theOperationalRulesGeneratorPackage);
		return theOperationalRulesGeneratorPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationalRulesGenerator() {
		return operationalRulesGeneratorEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public OperationalRulesGeneratorFactory getOperationalRulesGeneratorFactory() {
		return (OperationalRulesGeneratorFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		operationalRulesGeneratorEClass = createEClass(OPERATIONAL_RULES_GENERATOR);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		WorkflowComponentsPackage theWorkflowComponentsPackage = (WorkflowComponentsPackage) EPackage.Registry.INSTANCE
				.getEPackage(WorkflowComponentsPackage.eNS_URI);
		GenerationStrategiesPackage theGenerationStrategiesPackage = (GenerationStrategiesPackage) EPackage.Registry.INSTANCE
				.getEPackage(GenerationStrategiesPackage.eNS_URI);
		HelpersPackage theHelpersPackage = (HelpersPackage) EPackage.Registry.INSTANCE
				.getEPackage(HelpersPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theWorkflowComponentsPackage);
		getESubpackages().add(theGenerationStrategiesPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(operationalRulesGeneratorEClass,
				OperationalRulesGenerator.class, "OperationalRulesGenerator",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(operationalRulesGeneratorEClass, null,
				"generate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "tggFileURI", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getOutputStream(), "outputStream",
				0, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} // OperationalRulesGeneratorPackageImpl
