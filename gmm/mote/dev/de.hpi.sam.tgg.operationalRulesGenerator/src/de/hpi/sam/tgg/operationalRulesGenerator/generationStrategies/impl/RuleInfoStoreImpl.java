/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.ActivityDiagram;
import de.hpi.sam.storyDiagramEcore.helpers.HelpersPackage;
import de.hpi.sam.storyDiagramEcore.helpers.impl.MapEntryImpl;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Rule Info Store</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getTggRule <em>Tgg Rule</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getRuleEClass <em>Rule EClass</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getForwardTransformationActivityDiagram <em>Forward Transformation Activity Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getMappingTransformationActivityDiagram <em>Mapping Transformation Activity Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getReverseTransformationActivityDiagram <em>Reverse Transformation Activity Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getForwardSynchronisationActivityDiagram <em>Forward Synchronisation Activity Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getMappingSynchronisationActivityDiagram <em>Mapping Synchronisation Activity Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getReverseSynchronizationActivityDiagram <em>Reverse Synchronization Activity Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getHelperActivityDiagrams <em>Helper Activity Diagrams</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getForwardTransformationOperation <em>Forward Transformation Operation</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getMappingTransformationOperation <em>Mapping Transformation Operation</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getReverseTransformationOperation <em>Reverse Transformation Operation</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getForwardSynchronizationOperation <em>Forward Synchronization Operation</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getMappingSynchronizationOperation <em>Mapping Synchronization Operation</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl#getReverseSynchronizationOperation <em>Reverse Synchronization Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RuleInfoStoreImpl extends EObjectImpl implements RuleInfoStore {
	/**
	 * The cached value of the '{@link #getTggRule() <em>Tgg Rule</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTggRule()
	 * @generated
	 * @ordered
	 */
	protected TGGRule tggRule;

	/**
	 * The cached value of the '{@link #getRuleEClass() <em>Rule EClass</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuleEClass()
	 * @generated
	 * @ordered
	 */
	protected EClass ruleEClass;

	/**
	 * The cached value of the '
	 * {@link #getForwardTransformationActivityDiagram()
	 * <em>Forward Transformation Activity Diagram</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getForwardTransformationActivityDiagram()
	 * @generated
	 * @ordered
	 */
	protected ActivityDiagram forwardTransformationActivityDiagram;

	/**
	 * The cached value of the '
	 * {@link #getMappingTransformationActivityDiagram()
	 * <em>Mapping Transformation Activity Diagram</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getMappingTransformationActivityDiagram()
	 * @generated
	 * @ordered
	 */
	protected ActivityDiagram mappingTransformationActivityDiagram;

	/**
	 * The cached value of the '
	 * {@link #getReverseTransformationActivityDiagram()
	 * <em>Reverse Transformation Activity Diagram</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getReverseTransformationActivityDiagram()
	 * @generated
	 * @ordered
	 */
	protected ActivityDiagram reverseTransformationActivityDiagram;

	/**
	 * The cached value of the '
	 * {@link #getForwardSynchronisationActivityDiagram()
	 * <em>Forward Synchronisation Activity Diagram</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getForwardSynchronisationActivityDiagram()
	 * @generated
	 * @ordered
	 */
	protected ActivityDiagram forwardSynchronisationActivityDiagram;

	/**
	 * The cached value of the '
	 * {@link #getMappingSynchronisationActivityDiagram()
	 * <em>Mapping Synchronisation Activity Diagram</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getMappingSynchronisationActivityDiagram()
	 * @generated
	 * @ordered
	 */
	protected ActivityDiagram mappingSynchronisationActivityDiagram;

	/**
	 * The cached value of the '
	 * {@link #getReverseSynchronizationActivityDiagram()
	 * <em>Reverse Synchronization Activity Diagram</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getReverseSynchronizationActivityDiagram()
	 * @generated
	 * @ordered
	 */
	protected ActivityDiagram reverseSynchronizationActivityDiagram;

	/**
	 * The cached value of the '{@link #getHelperActivityDiagrams() <em>Helper Activity Diagrams</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getHelperActivityDiagrams()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, ActivityDiagram> helperActivityDiagrams;

	/**
	 * The cached value of the '{@link #getForwardTransformationOperation()
	 * <em>Forward Transformation Operation</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getForwardTransformationOperation()
	 * @generated
	 * @ordered
	 */
	protected EOperation forwardTransformationOperation;

	/**
	 * The cached value of the '{@link #getMappingTransformationOperation()
	 * <em>Mapping Transformation Operation</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getMappingTransformationOperation()
	 * @generated
	 * @ordered
	 */
	protected EOperation mappingTransformationOperation;

	/**
	 * The cached value of the '{@link #getReverseTransformationOperation()
	 * <em>Reverse Transformation Operation</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getReverseTransformationOperation()
	 * @generated
	 * @ordered
	 */
	protected EOperation reverseTransformationOperation;

	/**
	 * The cached value of the '{@link #getForwardSynchronizationOperation()
	 * <em>Forward Synchronization Operation</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getForwardSynchronizationOperation()
	 * @generated
	 * @ordered
	 */
	protected EOperation forwardSynchronizationOperation;

	/**
	 * The cached value of the '{@link #getMappingSynchronizationOperation()
	 * <em>Mapping Synchronization Operation</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getMappingSynchronizationOperation()
	 * @generated
	 * @ordered
	 */
	protected EOperation mappingSynchronizationOperation;

	/**
	 * The cached value of the '{@link #getReverseSynchronizationOperation()
	 * <em>Reverse Synchronization Operation</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getReverseSynchronizationOperation()
	 * @generated
	 * @ordered
	 */
	protected EOperation reverseSynchronizationOperation;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected RuleInfoStoreImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenerationStrategiesPackage.Literals.RULE_INFO_STORE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGRule getTggRule() {
		if (tggRule != null && tggRule.eIsProxy()) {
			InternalEObject oldTggRule = (InternalEObject) tggRule;
			tggRule = (TGGRule) eResolveProxy(oldTggRule);
			if (tggRule != oldTggRule) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__TGG_RULE,
							oldTggRule, tggRule));
			}
		}
		return tggRule;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGRule basicGetTggRule() {
		return tggRule;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTggRule(TGGRule newTggRule) {
		TGGRule oldTggRule = tggRule;
		tggRule = newTggRule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__TGG_RULE,
					oldTggRule, tggRule));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleEClass() {
		if (ruleEClass != null && ruleEClass.eIsProxy()) {
			InternalEObject oldRuleEClass = (InternalEObject) ruleEClass;
			ruleEClass = (EClass) eResolveProxy(oldRuleEClass);
			if (ruleEClass != oldRuleEClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__RULE_ECLASS,
							oldRuleEClass, ruleEClass));
			}
		}
		return ruleEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass basicGetRuleEClass() {
		return ruleEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleEClass(EClass newRuleEClass) {
		EClass oldRuleEClass = ruleEClass;
		ruleEClass = newRuleEClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__RULE_ECLASS,
					oldRuleEClass, ruleEClass));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram getForwardTransformationActivityDiagram() {
		if (forwardTransformationActivityDiagram != null
				&& forwardTransformationActivityDiagram.eIsProxy()) {
			InternalEObject oldForwardTransformationActivityDiagram = (InternalEObject) forwardTransformationActivityDiagram;
			forwardTransformationActivityDiagram = (ActivityDiagram) eResolveProxy(oldForwardTransformationActivityDiagram);
			if (forwardTransformationActivityDiagram != oldForwardTransformationActivityDiagram) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_TRANSFORMATION_ACTIVITY_DIAGRAM,
							oldForwardTransformationActivityDiagram,
							forwardTransformationActivityDiagram));
			}
		}
		return forwardTransformationActivityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram basicGetForwardTransformationActivityDiagram() {
		return forwardTransformationActivityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setForwardTransformationActivityDiagram(
			ActivityDiagram newForwardTransformationActivityDiagram) {
		ActivityDiagram oldForwardTransformationActivityDiagram = forwardTransformationActivityDiagram;
		forwardTransformationActivityDiagram = newForwardTransformationActivityDiagram;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_TRANSFORMATION_ACTIVITY_DIAGRAM,
					oldForwardTransformationActivityDiagram,
					forwardTransformationActivityDiagram));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram getMappingTransformationActivityDiagram() {
		if (mappingTransformationActivityDiagram != null
				&& mappingTransformationActivityDiagram.eIsProxy()) {
			InternalEObject oldMappingTransformationActivityDiagram = (InternalEObject) mappingTransformationActivityDiagram;
			mappingTransformationActivityDiagram = (ActivityDiagram) eResolveProxy(oldMappingTransformationActivityDiagram);
			if (mappingTransformationActivityDiagram != oldMappingTransformationActivityDiagram) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_TRANSFORMATION_ACTIVITY_DIAGRAM,
							oldMappingTransformationActivityDiagram,
							mappingTransformationActivityDiagram));
			}
		}
		return mappingTransformationActivityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram basicGetMappingTransformationActivityDiagram() {
		return mappingTransformationActivityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMappingTransformationActivityDiagram(
			ActivityDiagram newMappingTransformationActivityDiagram) {
		ActivityDiagram oldMappingTransformationActivityDiagram = mappingTransformationActivityDiagram;
		mappingTransformationActivityDiagram = newMappingTransformationActivityDiagram;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_TRANSFORMATION_ACTIVITY_DIAGRAM,
					oldMappingTransformationActivityDiagram,
					mappingTransformationActivityDiagram));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram getReverseTransformationActivityDiagram() {
		if (reverseTransformationActivityDiagram != null
				&& reverseTransformationActivityDiagram.eIsProxy()) {
			InternalEObject oldReverseTransformationActivityDiagram = (InternalEObject) reverseTransformationActivityDiagram;
			reverseTransformationActivityDiagram = (ActivityDiagram) eResolveProxy(oldReverseTransformationActivityDiagram);
			if (reverseTransformationActivityDiagram != oldReverseTransformationActivityDiagram) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_TRANSFORMATION_ACTIVITY_DIAGRAM,
							oldReverseTransformationActivityDiagram,
							reverseTransformationActivityDiagram));
			}
		}
		return reverseTransformationActivityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram basicGetReverseTransformationActivityDiagram() {
		return reverseTransformationActivityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setReverseTransformationActivityDiagram(
			ActivityDiagram newReverseTransformationActivityDiagram) {
		ActivityDiagram oldReverseTransformationActivityDiagram = reverseTransformationActivityDiagram;
		reverseTransformationActivityDiagram = newReverseTransformationActivityDiagram;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_TRANSFORMATION_ACTIVITY_DIAGRAM,
					oldReverseTransformationActivityDiagram,
					reverseTransformationActivityDiagram));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram getForwardSynchronisationActivityDiagram() {
		if (forwardSynchronisationActivityDiagram != null
				&& forwardSynchronisationActivityDiagram.eIsProxy()) {
			InternalEObject oldForwardSynchronisationActivityDiagram = (InternalEObject) forwardSynchronisationActivityDiagram;
			forwardSynchronisationActivityDiagram = (ActivityDiagram) eResolveProxy(oldForwardSynchronisationActivityDiagram);
			if (forwardSynchronisationActivityDiagram != oldForwardSynchronisationActivityDiagram) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_SYNCHRONISATION_ACTIVITY_DIAGRAM,
							oldForwardSynchronisationActivityDiagram,
							forwardSynchronisationActivityDiagram));
			}
		}
		return forwardSynchronisationActivityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram basicGetForwardSynchronisationActivityDiagram() {
		return forwardSynchronisationActivityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setForwardSynchronisationActivityDiagram(
			ActivityDiagram newForwardSynchronisationActivityDiagram) {
		ActivityDiagram oldForwardSynchronisationActivityDiagram = forwardSynchronisationActivityDiagram;
		forwardSynchronisationActivityDiagram = newForwardSynchronisationActivityDiagram;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_SYNCHRONISATION_ACTIVITY_DIAGRAM,
					oldForwardSynchronisationActivityDiagram,
					forwardSynchronisationActivityDiagram));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram getMappingSynchronisationActivityDiagram() {
		if (mappingSynchronisationActivityDiagram != null
				&& mappingSynchronisationActivityDiagram.eIsProxy()) {
			InternalEObject oldMappingSynchronisationActivityDiagram = (InternalEObject) mappingSynchronisationActivityDiagram;
			mappingSynchronisationActivityDiagram = (ActivityDiagram) eResolveProxy(oldMappingSynchronisationActivityDiagram);
			if (mappingSynchronisationActivityDiagram != oldMappingSynchronisationActivityDiagram) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_SYNCHRONISATION_ACTIVITY_DIAGRAM,
							oldMappingSynchronisationActivityDiagram,
							mappingSynchronisationActivityDiagram));
			}
		}
		return mappingSynchronisationActivityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram basicGetMappingSynchronisationActivityDiagram() {
		return mappingSynchronisationActivityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMappingSynchronisationActivityDiagram(
			ActivityDiagram newMappingSynchronisationActivityDiagram) {
		ActivityDiagram oldMappingSynchronisationActivityDiagram = mappingSynchronisationActivityDiagram;
		mappingSynchronisationActivityDiagram = newMappingSynchronisationActivityDiagram;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_SYNCHRONISATION_ACTIVITY_DIAGRAM,
					oldMappingSynchronisationActivityDiagram,
					mappingSynchronisationActivityDiagram));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram getReverseSynchronizationActivityDiagram() {
		if (reverseSynchronizationActivityDiagram != null
				&& reverseSynchronizationActivityDiagram.eIsProxy()) {
			InternalEObject oldReverseSynchronizationActivityDiagram = (InternalEObject) reverseSynchronizationActivityDiagram;
			reverseSynchronizationActivityDiagram = (ActivityDiagram) eResolveProxy(oldReverseSynchronizationActivityDiagram);
			if (reverseSynchronizationActivityDiagram != oldReverseSynchronizationActivityDiagram) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_ACTIVITY_DIAGRAM,
							oldReverseSynchronizationActivityDiagram,
							reverseSynchronizationActivityDiagram));
			}
		}
		return reverseSynchronizationActivityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram basicGetReverseSynchronizationActivityDiagram() {
		return reverseSynchronizationActivityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setReverseSynchronizationActivityDiagram(
			ActivityDiagram newReverseSynchronizationActivityDiagram) {
		ActivityDiagram oldReverseSynchronizationActivityDiagram = reverseSynchronizationActivityDiagram;
		reverseSynchronizationActivityDiagram = newReverseSynchronizationActivityDiagram;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_ACTIVITY_DIAGRAM,
					oldReverseSynchronizationActivityDiagram,
					reverseSynchronizationActivityDiagram));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, ActivityDiagram> getHelperActivityDiagrams() {
		if (helperActivityDiagrams == null) {
			helperActivityDiagrams = new EcoreEMap<String, ActivityDiagram>(
					HelpersPackage.Literals.MAP_ENTRY,
					MapEntryImpl.class,
					this,
					GenerationStrategiesPackage.RULE_INFO_STORE__HELPER_ACTIVITY_DIAGRAMS);
		}
		return helperActivityDiagrams;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getForwardTransformationOperation() {
		if (forwardTransformationOperation != null
				&& forwardTransformationOperation.eIsProxy()) {
			InternalEObject oldForwardTransformationOperation = (InternalEObject) forwardTransformationOperation;
			forwardTransformationOperation = (EOperation) eResolveProxy(oldForwardTransformationOperation);
			if (forwardTransformationOperation != oldForwardTransformationOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_TRANSFORMATION_OPERATION,
							oldForwardTransformationOperation,
							forwardTransformationOperation));
			}
		}
		return forwardTransformationOperation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation basicGetForwardTransformationOperation() {
		return forwardTransformationOperation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setForwardTransformationOperation(
			EOperation newForwardTransformationOperation) {
		EOperation oldForwardTransformationOperation = forwardTransformationOperation;
		forwardTransformationOperation = newForwardTransformationOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_TRANSFORMATION_OPERATION,
					oldForwardTransformationOperation,
					forwardTransformationOperation));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMappingTransformationOperation() {
		if (mappingTransformationOperation != null
				&& mappingTransformationOperation.eIsProxy()) {
			InternalEObject oldMappingTransformationOperation = (InternalEObject) mappingTransformationOperation;
			mappingTransformationOperation = (EOperation) eResolveProxy(oldMappingTransformationOperation);
			if (mappingTransformationOperation != oldMappingTransformationOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_TRANSFORMATION_OPERATION,
							oldMappingTransformationOperation,
							mappingTransformationOperation));
			}
		}
		return mappingTransformationOperation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation basicGetMappingTransformationOperation() {
		return mappingTransformationOperation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMappingTransformationOperation(
			EOperation newMappingTransformationOperation) {
		EOperation oldMappingTransformationOperation = mappingTransformationOperation;
		mappingTransformationOperation = newMappingTransformationOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_TRANSFORMATION_OPERATION,
					oldMappingTransformationOperation,
					mappingTransformationOperation));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReverseTransformationOperation() {
		if (reverseTransformationOperation != null
				&& reverseTransformationOperation.eIsProxy()) {
			InternalEObject oldReverseTransformationOperation = (InternalEObject) reverseTransformationOperation;
			reverseTransformationOperation = (EOperation) eResolveProxy(oldReverseTransformationOperation);
			if (reverseTransformationOperation != oldReverseTransformationOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_TRANSFORMATION_OPERATION,
							oldReverseTransformationOperation,
							reverseTransformationOperation));
			}
		}
		return reverseTransformationOperation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation basicGetReverseTransformationOperation() {
		return reverseTransformationOperation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setReverseTransformationOperation(
			EOperation newReverseTransformationOperation) {
		EOperation oldReverseTransformationOperation = reverseTransformationOperation;
		reverseTransformationOperation = newReverseTransformationOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_TRANSFORMATION_OPERATION,
					oldReverseTransformationOperation,
					reverseTransformationOperation));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getForwardSynchronizationOperation() {
		if (forwardSynchronizationOperation != null
				&& forwardSynchronizationOperation.eIsProxy()) {
			InternalEObject oldForwardSynchronizationOperation = (InternalEObject) forwardSynchronizationOperation;
			forwardSynchronizationOperation = (EOperation) eResolveProxy(oldForwardSynchronizationOperation);
			if (forwardSynchronizationOperation != oldForwardSynchronizationOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_SYNCHRONIZATION_OPERATION,
							oldForwardSynchronizationOperation,
							forwardSynchronizationOperation));
			}
		}
		return forwardSynchronizationOperation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation basicGetForwardSynchronizationOperation() {
		return forwardSynchronizationOperation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setForwardSynchronizationOperation(
			EOperation newForwardSynchronizationOperation) {
		EOperation oldForwardSynchronizationOperation = forwardSynchronizationOperation;
		forwardSynchronizationOperation = newForwardSynchronizationOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_SYNCHRONIZATION_OPERATION,
					oldForwardSynchronizationOperation,
					forwardSynchronizationOperation));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMappingSynchronizationOperation() {
		if (mappingSynchronizationOperation != null
				&& mappingSynchronizationOperation.eIsProxy()) {
			InternalEObject oldMappingSynchronizationOperation = (InternalEObject) mappingSynchronizationOperation;
			mappingSynchronizationOperation = (EOperation) eResolveProxy(oldMappingSynchronizationOperation);
			if (mappingSynchronizationOperation != oldMappingSynchronizationOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_SYNCHRONIZATION_OPERATION,
							oldMappingSynchronizationOperation,
							mappingSynchronizationOperation));
			}
		}
		return mappingSynchronizationOperation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation basicGetMappingSynchronizationOperation() {
		return mappingSynchronizationOperation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMappingSynchronizationOperation(
			EOperation newMappingSynchronizationOperation) {
		EOperation oldMappingSynchronizationOperation = mappingSynchronizationOperation;
		mappingSynchronizationOperation = newMappingSynchronizationOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_SYNCHRONIZATION_OPERATION,
					oldMappingSynchronizationOperation,
					mappingSynchronizationOperation));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReverseSynchronizationOperation() {
		if (reverseSynchronizationOperation != null
				&& reverseSynchronizationOperation.eIsProxy()) {
			InternalEObject oldReverseSynchronizationOperation = (InternalEObject) reverseSynchronizationOperation;
			reverseSynchronizationOperation = (EOperation) eResolveProxy(oldReverseSynchronizationOperation);
			if (reverseSynchronizationOperation != oldReverseSynchronizationOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_OPERATION,
							oldReverseSynchronizationOperation,
							reverseSynchronizationOperation));
			}
		}
		return reverseSynchronizationOperation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation basicGetReverseSynchronizationOperation() {
		return reverseSynchronizationOperation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setReverseSynchronizationOperation(
			EOperation newReverseSynchronizationOperation) {
		EOperation oldReverseSynchronizationOperation = reverseSynchronizationOperation;
		reverseSynchronizationOperation = newReverseSynchronizationOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_OPERATION,
					oldReverseSynchronizationOperation,
					reverseSynchronizationOperation));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case GenerationStrategiesPackage.RULE_INFO_STORE__HELPER_ACTIVITY_DIAGRAMS:
			return ((InternalEList<?>) getHelperActivityDiagrams())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case GenerationStrategiesPackage.RULE_INFO_STORE__TGG_RULE:
			if (resolve)
				return getTggRule();
			return basicGetTggRule();
		case GenerationStrategiesPackage.RULE_INFO_STORE__RULE_ECLASS:
			if (resolve)
				return getRuleEClass();
			return basicGetRuleEClass();
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_TRANSFORMATION_ACTIVITY_DIAGRAM:
			if (resolve)
				return getForwardTransformationActivityDiagram();
			return basicGetForwardTransformationActivityDiagram();
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_TRANSFORMATION_ACTIVITY_DIAGRAM:
			if (resolve)
				return getMappingTransformationActivityDiagram();
			return basicGetMappingTransformationActivityDiagram();
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_TRANSFORMATION_ACTIVITY_DIAGRAM:
			if (resolve)
				return getReverseTransformationActivityDiagram();
			return basicGetReverseTransformationActivityDiagram();
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_SYNCHRONISATION_ACTIVITY_DIAGRAM:
			if (resolve)
				return getForwardSynchronisationActivityDiagram();
			return basicGetForwardSynchronisationActivityDiagram();
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_SYNCHRONISATION_ACTIVITY_DIAGRAM:
			if (resolve)
				return getMappingSynchronisationActivityDiagram();
			return basicGetMappingSynchronisationActivityDiagram();
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_ACTIVITY_DIAGRAM:
			if (resolve)
				return getReverseSynchronizationActivityDiagram();
			return basicGetReverseSynchronizationActivityDiagram();
		case GenerationStrategiesPackage.RULE_INFO_STORE__HELPER_ACTIVITY_DIAGRAMS:
			if (coreType)
				return getHelperActivityDiagrams();
			else
				return getHelperActivityDiagrams().map();
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_TRANSFORMATION_OPERATION:
			if (resolve)
				return getForwardTransformationOperation();
			return basicGetForwardTransformationOperation();
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_TRANSFORMATION_OPERATION:
			if (resolve)
				return getMappingTransformationOperation();
			return basicGetMappingTransformationOperation();
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_TRANSFORMATION_OPERATION:
			if (resolve)
				return getReverseTransformationOperation();
			return basicGetReverseTransformationOperation();
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_SYNCHRONIZATION_OPERATION:
			if (resolve)
				return getForwardSynchronizationOperation();
			return basicGetForwardSynchronizationOperation();
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_SYNCHRONIZATION_OPERATION:
			if (resolve)
				return getMappingSynchronizationOperation();
			return basicGetMappingSynchronizationOperation();
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_OPERATION:
			if (resolve)
				return getReverseSynchronizationOperation();
			return basicGetReverseSynchronizationOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case GenerationStrategiesPackage.RULE_INFO_STORE__TGG_RULE:
			setTggRule((TGGRule) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__RULE_ECLASS:
			setRuleEClass((EClass) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_TRANSFORMATION_ACTIVITY_DIAGRAM:
			setForwardTransformationActivityDiagram((ActivityDiagram) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_TRANSFORMATION_ACTIVITY_DIAGRAM:
			setMappingTransformationActivityDiagram((ActivityDiagram) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_TRANSFORMATION_ACTIVITY_DIAGRAM:
			setReverseTransformationActivityDiagram((ActivityDiagram) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_SYNCHRONISATION_ACTIVITY_DIAGRAM:
			setForwardSynchronisationActivityDiagram((ActivityDiagram) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_SYNCHRONISATION_ACTIVITY_DIAGRAM:
			setMappingSynchronisationActivityDiagram((ActivityDiagram) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_ACTIVITY_DIAGRAM:
			setReverseSynchronizationActivityDiagram((ActivityDiagram) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__HELPER_ACTIVITY_DIAGRAMS:
			((EStructuralFeature.Setting) getHelperActivityDiagrams())
					.set(newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_TRANSFORMATION_OPERATION:
			setForwardTransformationOperation((EOperation) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_TRANSFORMATION_OPERATION:
			setMappingTransformationOperation((EOperation) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_TRANSFORMATION_OPERATION:
			setReverseTransformationOperation((EOperation) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_SYNCHRONIZATION_OPERATION:
			setForwardSynchronizationOperation((EOperation) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_SYNCHRONIZATION_OPERATION:
			setMappingSynchronizationOperation((EOperation) newValue);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_OPERATION:
			setReverseSynchronizationOperation((EOperation) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case GenerationStrategiesPackage.RULE_INFO_STORE__TGG_RULE:
			setTggRule((TGGRule) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__RULE_ECLASS:
			setRuleEClass((EClass) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_TRANSFORMATION_ACTIVITY_DIAGRAM:
			setForwardTransformationActivityDiagram((ActivityDiagram) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_TRANSFORMATION_ACTIVITY_DIAGRAM:
			setMappingTransformationActivityDiagram((ActivityDiagram) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_TRANSFORMATION_ACTIVITY_DIAGRAM:
			setReverseTransformationActivityDiagram((ActivityDiagram) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_SYNCHRONISATION_ACTIVITY_DIAGRAM:
			setForwardSynchronisationActivityDiagram((ActivityDiagram) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_SYNCHRONISATION_ACTIVITY_DIAGRAM:
			setMappingSynchronisationActivityDiagram((ActivityDiagram) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_ACTIVITY_DIAGRAM:
			setReverseSynchronizationActivityDiagram((ActivityDiagram) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__HELPER_ACTIVITY_DIAGRAMS:
			getHelperActivityDiagrams().clear();
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_TRANSFORMATION_OPERATION:
			setForwardTransformationOperation((EOperation) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_TRANSFORMATION_OPERATION:
			setMappingTransformationOperation((EOperation) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_TRANSFORMATION_OPERATION:
			setReverseTransformationOperation((EOperation) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_SYNCHRONIZATION_OPERATION:
			setForwardSynchronizationOperation((EOperation) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_SYNCHRONIZATION_OPERATION:
			setMappingSynchronizationOperation((EOperation) null);
			return;
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_OPERATION:
			setReverseSynchronizationOperation((EOperation) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case GenerationStrategiesPackage.RULE_INFO_STORE__TGG_RULE:
			return tggRule != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__RULE_ECLASS:
			return ruleEClass != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_TRANSFORMATION_ACTIVITY_DIAGRAM:
			return forwardTransformationActivityDiagram != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_TRANSFORMATION_ACTIVITY_DIAGRAM:
			return mappingTransformationActivityDiagram != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_TRANSFORMATION_ACTIVITY_DIAGRAM:
			return reverseTransformationActivityDiagram != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_SYNCHRONISATION_ACTIVITY_DIAGRAM:
			return forwardSynchronisationActivityDiagram != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_SYNCHRONISATION_ACTIVITY_DIAGRAM:
			return mappingSynchronisationActivityDiagram != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_ACTIVITY_DIAGRAM:
			return reverseSynchronizationActivityDiagram != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__HELPER_ACTIVITY_DIAGRAMS:
			return helperActivityDiagrams != null
					&& !helperActivityDiagrams.isEmpty();
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_TRANSFORMATION_OPERATION:
			return forwardTransformationOperation != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_TRANSFORMATION_OPERATION:
			return mappingTransformationOperation != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_TRANSFORMATION_OPERATION:
			return reverseTransformationOperation != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__FORWARD_SYNCHRONIZATION_OPERATION:
			return forwardSynchronizationOperation != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__MAPPING_SYNCHRONIZATION_OPERATION:
			return mappingSynchronizationOperation != null;
		case GenerationStrategiesPackage.RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_OPERATION:
			return reverseSynchronizationOperation != null;
		}
		return super.eIsSet(featureID);
	}

} // RuleInfoStoreImpl
