/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Simple Transformation Generation Strategy</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <!-- begin-model-doc --> This generation strategy creates Story Diagrams that
 * perform only model transformation. <!-- end-model-doc -->
 * 
 * 
 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getSimpleTransformationGenerationStrategy()
 * @model
 * @generated
 */
public interface SimpleTransformationGenerationStrategy extends
		StoryDiagramBasedGenerationStrategy {

} // SimpleTransformationGenerationStrategy
