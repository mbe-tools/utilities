/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;

import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import de.mdelab.workflow.util.WorkflowUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Manifest Generator Component</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestGeneratorComponentImpl#getManifestSlot <em>Manifest Slot</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestGeneratorComponentImpl#getProjectName <em>Project Name</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestGeneratorComponentImpl#getTggFileURI <em>Tgg File URI</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestGeneratorComponentImpl#getCorrespondenceMetamodelURI <em>Correspondence Metamodel URI</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ManifestGeneratorComponentImpl extends WorkflowComponentImpl
		implements ManifestGeneratorComponent {
	/**
	 * The default value of the '{@link #getManifestSlot() <em>Manifest Slot</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getManifestSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String MANIFEST_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getManifestSlot() <em>Manifest Slot</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getManifestSlot()
	 * @generated
	 * @ordered
	 */
	protected String manifestSlot = MANIFEST_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getProjectName() <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getProjectName()
	 * @generated
	 * @ordered
	 */
	protected static final String PROJECT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProjectName() <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getProjectName()
	 * @generated
	 * @ordered
	 */
	protected String projectName = PROJECT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getTggFileURI() <em>Tgg File URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTggFileURI()
	 * @generated
	 * @ordered
	 */
	protected static final String TGG_FILE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTggFileURI() <em>Tgg File URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTggFileURI()
	 * @generated
	 * @ordered
	 */
	protected String tggFileURI = TGG_FILE_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getCorrespondenceMetamodelURI() <em>Correspondence Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getCorrespondenceMetamodelURI()
	 * @generated
	 * @ordered
	 */
	protected static final String CORRESPONDENCE_METAMODEL_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCorrespondenceMetamodelURI() <em>Correspondence Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getCorrespondenceMetamodelURI()
	 * @generated
	 * @ordered
	 */
	protected String correspondenceMetamodelURI = CORRESPONDENCE_METAMODEL_URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ManifestGeneratorComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowComponentsPackage.Literals.MANIFEST_GENERATOR_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getManifestSlot() {
		return manifestSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setManifestSlot(String newManifestSlot) {
		String oldManifestSlot = manifestSlot;
		manifestSlot = newManifestSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__MANIFEST_SLOT,
					oldManifestSlot, manifestSlot));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProjectName() {
		return projectName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProjectName(String newProjectName) {
		String oldProjectName = projectName;
		projectName = newProjectName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__PROJECT_NAME,
					oldProjectName, projectName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTggFileURI() {
		return tggFileURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTggFileURI(String newTggFileURI) {
		String oldTggFileURI = tggFileURI;
		tggFileURI = newTggFileURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__TGG_FILE_URI,
					oldTggFileURI, tggFileURI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCorrespondenceMetamodelURI() {
		return correspondenceMetamodelURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCorrespondenceMetamodelURI(
			String newCorrespondenceMetamodelURI) {
		String oldCorrespondenceMetamodelURI = correspondenceMetamodelURI;
		correspondenceMetamodelURI = newCorrespondenceMetamodelURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__CORRESPONDENCE_METAMODEL_URI,
					oldCorrespondenceMetamodelURI, correspondenceMetamodelURI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__MANIFEST_SLOT:
			return getManifestSlot();
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__PROJECT_NAME:
			return getProjectName();
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__TGG_FILE_URI:
			return getTggFileURI();
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__CORRESPONDENCE_METAMODEL_URI:
			return getCorrespondenceMetamodelURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__MANIFEST_SLOT:
			setManifestSlot((String) newValue);
			return;
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__PROJECT_NAME:
			setProjectName((String) newValue);
			return;
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__TGG_FILE_URI:
			setTggFileURI((String) newValue);
			return;
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__CORRESPONDENCE_METAMODEL_URI:
			setCorrespondenceMetamodelURI((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__MANIFEST_SLOT:
			setManifestSlot(MANIFEST_SLOT_EDEFAULT);
			return;
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__PROJECT_NAME:
			setProjectName(PROJECT_NAME_EDEFAULT);
			return;
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__TGG_FILE_URI:
			setTggFileURI(TGG_FILE_URI_EDEFAULT);
			return;
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__CORRESPONDENCE_METAMODEL_URI:
			setCorrespondenceMetamodelURI(CORRESPONDENCE_METAMODEL_URI_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__MANIFEST_SLOT:
			return MANIFEST_SLOT_EDEFAULT == null ? manifestSlot != null
					: !MANIFEST_SLOT_EDEFAULT.equals(manifestSlot);
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__PROJECT_NAME:
			return PROJECT_NAME_EDEFAULT == null ? projectName != null
					: !PROJECT_NAME_EDEFAULT.equals(projectName);
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__TGG_FILE_URI:
			return TGG_FILE_URI_EDEFAULT == null ? tggFileURI != null
					: !TGG_FILE_URI_EDEFAULT.equals(tggFileURI);
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT__CORRESPONDENCE_METAMODEL_URI:
			return CORRESPONDENCE_METAMODEL_URI_EDEFAULT == null ? correspondenceMetamodelURI != null
					: !CORRESPONDENCE_METAMODEL_URI_EDEFAULT
							.equals(correspondenceMetamodelURI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (manifestSlot: ");
		result.append(manifestSlot);
		result.append(", projectName: ");
		result.append(projectName);
		result.append(", tggFileURI: ");
		result.append(tggFileURI);
		result.append(", correspondenceMetamodelURI: ");
		result.append(correspondenceMetamodelURI);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean checkConfiguration(WorkflowExecutionContext context)
			throws IOException {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void execute(WorkflowExecutionContext context)
			throws WorkflowExecutionException, IOException {
		context.getLogger().addInfo(
				"Generating '" + this.getProjectName()
						+ "/META-INF/MANIFEST.MF'...", this);

		final String NL = System.getProperty("line.separator");

		URI tggFileURI = WorkflowUtil.getResolvedURI(
				URI.createURI(this.getTggFileURI()),
				context.getWorkflowFileURI());
		URI correspondenceMetamodelURI = WorkflowUtil.getResolvedURI(
				URI.createURI(this.getCorrespondenceMetamodelURI()),
				context.getWorkflowFileURI());

		ResourceSet rs = context.getGlobalResourceSet();

		/*
		 * Get the TGGDiagram
		 */
		Resource tggDiagramResource = rs.getResource(tggFileURI, true);

		TGGDiagram tggDiagram = (TGGDiagram) tggDiagramResource.getContents()
				.get(0);

		/*
		 * Get the CorrespondenceMetamodelPackage
		 */
		Resource correspondenceMetamodelResource = rs.getResource(
				correspondenceMetamodelURI, true);

		EPackage correspondenceMetamodelPackage = (EPackage) correspondenceMetamodelResource
				.getContents().get(0);

		/*
		 * Write plugin.xml
		 */

		java.io.BufferedReader reader = new BufferedReader(
				new InputStreamReader(
						URIConverter.INSTANCE.createInputStream(URI
								.createPlatformResourceURI(
										"/" + this.getProjectName()
												+ "/plugin.xml", true))));

		List<String> lines = new LinkedList<String>();

		String s = null;
		while ((s = reader.readLine()) != null) {
			lines.add(s + NL);
		}

		reader.close();

		/*
		 * Get the name of the ruleset
		 */
		String name = tggDiagram.getName();

		if (name == null || "".equals(name)) {
			throw new WorkflowExecutionException("name of TGGDiagram in '"
					+ this.getTggFileURI() + "' is not set.");
		}

		// Compute the options of the ruleSet
		int options = 0;

		// Add lines to the lines list
		s = "   <extension" + NL
				+ "          point=\"de.hpi.sam.mote.ruleSetExtension\">" + NL
				+ "       <ruleSet" + NL + "             id=\""
				+ correspondenceMetamodelPackage.getNsPrefix() + ".ruleSetID\""
				+ NL + "             name=\"" + name + "\"" + NL
				+ "             packageNsURI=\""
				+ correspondenceMetamodelPackage.getNsURI() + "\"" + NL
				+ "             ruleSetOptions=\"" + options + "\"" + NL
				+ "             sourceModelId=\"%sourceModelID\"" + NL
				+ "             targetModelId=\"%targetModelID\">" + NL
				+ "       </ruleSet>" + NL + "   </extension>" + NL;
		lines.add(lines.size() - 1, s);

		java.io.PrintStream printStream = new PrintStream(
				URIConverter.INSTANCE.createOutputStream(URI
						.createPlatformResourceURI("/" + this.getProjectName()
								+ "/plugin.xml", true)));

		for (String line : lines) {
			printStream.print(line);
		}

		printStream.close();

		/*
		 * Add required plugins to MANIFEST.MF
		 */
		Manifest oldManifest = (Manifest) context.getModelSlots().get(
				this.getManifestSlot());

		/*
		 * Get project
		 */
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IProject project = root.getProject(this.getProjectName());

		/*
		 * Create new manifest
		 */
		Manifest newManifest;
		try {
			newManifest = new Manifest(project.getFile("/META-INF/MANIFEST.MF")
					.getContents());
		} catch (CoreException e) {
			throw new WorkflowExecutionException("Error reading '"
					+ this.getProjectName() + "/META-INF/MANIFEST.MF'.", e);
		}

		/*
		 * Copy required bundles from old manifest
		 */
		Attributes attributes = newManifest.getMainAttributes();

		StringTokenizer tokenizer = new StringTokenizer(oldManifest
				.getMainAttributes().getValue("Require-Bundle"), ",");

		while (tokenizer.hasMoreTokens()) {
			this.addIfNotAlreadyContained(attributes, "Require-Bundle",
					tokenizer.nextToken());
		}

		this.addIfNotAlreadyContained(attributes, "Require-Bundle",
				"de.mdelab.sdm.interpreter.sde.eclipse;bundle-version=\"1.0.0\"");

		/*
		 * Save manifest
		 */
		printStream = new PrintStream(
				URIConverter.INSTANCE.createOutputStream(URI
						.createPlatformResourceURI("/" + this.projectName
								+ "/META-INF/MANIFEST.MF", true)));
		newManifest.write(printStream);
		printStream.close();
	}

	private void addIfNotAlreadyContained(Attributes attributes,
			final String key, final String value) {
		String currValue = attributes.getValue(key);
		if (!currValue.contains(value)) {
			currValue += ", " + value;
			attributes.putValue(key, currValue);
		}
	}
} // ManifestGeneratorComponentImpl
