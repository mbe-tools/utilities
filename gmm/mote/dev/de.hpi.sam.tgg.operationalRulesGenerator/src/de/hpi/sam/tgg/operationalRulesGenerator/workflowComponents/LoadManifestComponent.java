/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents;

import java.io.IOException;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.WorkflowComponent;
import de.mdelab.workflow.impl.WorkflowExecutionException;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Load Manifest Component</b></em>'. <!-- end-user-doc -->
 * 
 * <!-- begin-model-doc --> Loads the MANIFEST.MF file of a plugin project. <!--
 * end-model-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.LoadManifestComponent#getProjectURI
 * <em>Project URI</em>}</li>
 * <li>
 * {@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.LoadManifestComponent#getModelSlot
 * <em>Model Slot</em>}</li>
 * </ul>
 * </p>
 * 
 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getLoadManifestComponent()
 * @model
 * @generated
 */
public interface LoadManifestComponent extends WorkflowComponent
{
	/**
	 * Returns the value of the '<em><b>Project URI</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project URI</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Project URI</em>' attribute.
	 * @see #setProjectURI(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getLoadManifestComponent_ProjectURI()
	 * @model required="true"
	 * @generated
	 */
	String getProjectURI();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.LoadManifestComponent#getProjectURI
	 * <em>Project URI</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Project URI</em>' attribute.
	 * @see #getProjectURI()
	 * @generated
	 */
	void setProjectURI(String value);

	/**
	 * Returns the value of the '<em><b>Model Slot</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Slot</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Model Slot</em>' attribute.
	 * @see #setModelSlot(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getLoadManifestComponent_ModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getModelSlot();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.LoadManifestComponent#getModelSlot
	 * <em>Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Model Slot</em>' attribute.
	 * @see #getModelSlot()
	 * @generated
	 */
	void setModelSlot(String value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Executes this WorkflowComponent. The WorkflowExecutionContext provides
	 * access to global resources of the workflow, e.g., the global ResourceSet,
	 * the model slots, and the default OutputStream. Status and error messages
	 * should be written to this OutputStream. <!-- end-model-doc -->
	 * 
	 * @model exceptions=
	 *        "de.hpi.sam.workflow.helpers.IOException de.hpi.sam.workflow.helpers.WorkflowExecutionException"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='null'"
	 * @generated
	 */
	void execute(WorkflowExecutionContext workflowExecutionContext) throws IOException, WorkflowExecutionException;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Checks the configuration of this component. This method is called before
	 * the execution of the Workflow. It returns true, if this component's
	 * configuration is valid. The WorkflowExecutionContext provides access to
	 * global resources of the workflow, e.g., the global ResourceSet, the model
	 * slots, and the default OutputStream. Status and error messages should be
	 * written to this OutputStream. <!-- end-model-doc -->
	 * 
	 * @model exceptions="de.hpi.sam.workflow.helpers.IOException"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='null'"
	 * @generated
	 */
	boolean checkConfiguration(WorkflowExecutionContext workflowExecutionContext) throws IOException;

} // LoadManifestComponent
