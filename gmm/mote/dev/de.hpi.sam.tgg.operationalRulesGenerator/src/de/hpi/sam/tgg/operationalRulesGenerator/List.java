/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>List</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.List#getElements <em>
 * Elements</em>}</li>
 * </ul>
 * </p>
 * 
 * @see de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorPackage#getList()
 * @model
 * @generated
 */
public interface List extends EObject
{
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Elements</em>' reference.
	 * @see #setElements(EObject)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorPackage#getList_Elements()
	 * @model
	 * @generated
	 */
	EObject getElements();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.List#getElements
	 * <em>Elements</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Elements</em>' reference.
	 * @see #getElements()
	 * @generated
	 */
	void setElements(EObject value);

} // List
