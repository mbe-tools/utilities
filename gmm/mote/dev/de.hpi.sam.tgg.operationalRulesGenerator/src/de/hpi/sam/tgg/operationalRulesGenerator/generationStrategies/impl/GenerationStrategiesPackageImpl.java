/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.ConflictCheckTransformationGenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesFactory;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SimpleTransformationGenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.StoryDiagramBasedGenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SynchronizationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore;
import de.hpi.sam.tgg.operationalRulesGenerator.impl.OperationalRulesGeneratorPackageImpl;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.WorkflowComponentsPackageImpl;
import de.mdelab.workflow.WorkflowPackage;
import de.mdelab.workflow.helpers.HelpersPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class GenerationStrategiesPackageImpl extends EPackageImpl implements
		GenerationStrategiesPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass generationStrategyEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simpleTransformationGenerationStrategyEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass traceabilityLinkStoreEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass traceabilityLinkEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conflictCheckTransformationGenerationStrategyEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass storyDiagramBasedGenerationStrategyEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass generationInfoStoreEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleInfoStoreEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass synchronizationStrategyEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType ruleGenerationExceptionEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GenerationStrategiesPackageImpl() {
		super(eNS_URI, GenerationStrategiesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link GenerationStrategiesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GenerationStrategiesPackage init() {
		if (isInited)
			return (GenerationStrategiesPackage) EPackage.Registry.INSTANCE
					.getEPackage(GenerationStrategiesPackage.eNS_URI);

		// Obtain or create and register package
		GenerationStrategiesPackageImpl theGenerationStrategiesPackage = (GenerationStrategiesPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof GenerationStrategiesPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new GenerationStrategiesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		TggPackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		OperationalRulesGeneratorPackageImpl theOperationalRulesGeneratorPackage = (OperationalRulesGeneratorPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(OperationalRulesGeneratorPackage.eNS_URI) instanceof OperationalRulesGeneratorPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(OperationalRulesGeneratorPackage.eNS_URI)
				: OperationalRulesGeneratorPackage.eINSTANCE);
		WorkflowComponentsPackageImpl theWorkflowComponentsPackage = (WorkflowComponentsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(WorkflowComponentsPackage.eNS_URI) instanceof WorkflowComponentsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(WorkflowComponentsPackage.eNS_URI)
				: WorkflowComponentsPackage.eINSTANCE);

		// Create package meta-data objects
		theGenerationStrategiesPackage.createPackageContents();
		theOperationalRulesGeneratorPackage.createPackageContents();
		theWorkflowComponentsPackage.createPackageContents();

		// Initialize created meta-data
		theGenerationStrategiesPackage.initializePackageContents();
		theOperationalRulesGeneratorPackage.initializePackageContents();
		theWorkflowComponentsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGenerationStrategiesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GenerationStrategiesPackage.eNS_URI,
				theGenerationStrategiesPackage);
		return theGenerationStrategiesPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGenerationStrategy() {
		return generationStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimpleTransformationGenerationStrategy() {
		return simpleTransformationGenerationStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTraceabilityLinkStore() {
		return traceabilityLinkStoreEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTraceabilityLinkStore_TraceabilityLinks() {
		return (EReference) traceabilityLinkStoreEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTraceabilityLinkStore_Mappings() {
		return (EReference) traceabilityLinkStoreEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTraceabilityLink() {
		return traceabilityLinkEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTraceabilityLink_Sources() {
		return (EReference) traceabilityLinkEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTraceabilityLink_Targets() {
		return (EReference) traceabilityLinkEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConflictCheckTransformationGenerationStrategy() {
		return conflictCheckTransformationGenerationStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStoryDiagramBasedGenerationStrategy() {
		return storyDiagramBasedGenerationStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGenerationInfoStore() {
		return generationInfoStoreEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGenerationInfoStore_RuleInfos() {
		return (EReference) generationInfoStoreEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGenerationInfoStore_TggDiagram() {
		return (EReference) generationInfoStoreEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGenerationInfoStore_RuleSetEClass() {
		return (EReference) generationInfoStoreEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleInfoStore() {
		return ruleInfoStoreEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_TggRule() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_RuleEClass() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_ForwardTransformationActivityDiagram() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_MappingTransformationActivityDiagram() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_ReverseTransformationActivityDiagram() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_ForwardSynchronisationActivityDiagram() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_MappingSynchronisationActivityDiagram() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_ReverseSynchronizationActivityDiagram() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_HelperActivityDiagrams() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_ForwardTransformationOperation() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_MappingTransformationOperation() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures()
				.get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_ReverseTransformationOperation() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures()
				.get(11);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_ForwardSynchronizationOperation() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures()
				.get(12);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_MappingSynchronizationOperation() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures()
				.get(13);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleInfoStore_ReverseSynchronizationOperation() {
		return (EReference) ruleInfoStoreEClass.getEStructuralFeatures()
				.get(14);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSynchronizationStrategy() {
		return synchronizationStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getRuleGenerationException() {
		return ruleGenerationExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public GenerationStrategiesFactory getGenerationStrategiesFactory() {
		return (GenerationStrategiesFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		generationStrategyEClass = createEClass(GENERATION_STRATEGY);

		simpleTransformationGenerationStrategyEClass = createEClass(SIMPLE_TRANSFORMATION_GENERATION_STRATEGY);

		traceabilityLinkStoreEClass = createEClass(TRACEABILITY_LINK_STORE);
		createEReference(traceabilityLinkStoreEClass,
				TRACEABILITY_LINK_STORE__TRACEABILITY_LINKS);
		createEReference(traceabilityLinkStoreEClass,
				TRACEABILITY_LINK_STORE__MAPPINGS);

		traceabilityLinkEClass = createEClass(TRACEABILITY_LINK);
		createEReference(traceabilityLinkEClass, TRACEABILITY_LINK__SOURCES);
		createEReference(traceabilityLinkEClass, TRACEABILITY_LINK__TARGETS);

		conflictCheckTransformationGenerationStrategyEClass = createEClass(CONFLICT_CHECK_TRANSFORMATION_GENERATION_STRATEGY);

		storyDiagramBasedGenerationStrategyEClass = createEClass(STORY_DIAGRAM_BASED_GENERATION_STRATEGY);

		generationInfoStoreEClass = createEClass(GENERATION_INFO_STORE);
		createEReference(generationInfoStoreEClass,
				GENERATION_INFO_STORE__RULE_INFOS);
		createEReference(generationInfoStoreEClass,
				GENERATION_INFO_STORE__TGG_DIAGRAM);
		createEReference(generationInfoStoreEClass,
				GENERATION_INFO_STORE__RULE_SET_ECLASS);

		ruleInfoStoreEClass = createEClass(RULE_INFO_STORE);
		createEReference(ruleInfoStoreEClass, RULE_INFO_STORE__TGG_RULE);
		createEReference(ruleInfoStoreEClass, RULE_INFO_STORE__RULE_ECLASS);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__FORWARD_TRANSFORMATION_ACTIVITY_DIAGRAM);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__MAPPING_TRANSFORMATION_ACTIVITY_DIAGRAM);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__REVERSE_TRANSFORMATION_ACTIVITY_DIAGRAM);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__FORWARD_SYNCHRONISATION_ACTIVITY_DIAGRAM);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__MAPPING_SYNCHRONISATION_ACTIVITY_DIAGRAM);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_ACTIVITY_DIAGRAM);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__HELPER_ACTIVITY_DIAGRAMS);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__FORWARD_TRANSFORMATION_OPERATION);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__MAPPING_TRANSFORMATION_OPERATION);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__REVERSE_TRANSFORMATION_OPERATION);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__FORWARD_SYNCHRONIZATION_OPERATION);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__MAPPING_SYNCHRONIZATION_OPERATION);
		createEReference(ruleInfoStoreEClass,
				RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_OPERATION);

		synchronizationStrategyEClass = createEClass(SYNCHRONIZATION_STRATEGY);

		// Create data types
		ruleGenerationExceptionEDataType = createEDataType(RULE_GENERATION_EXCEPTION);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage) EPackage.Registry.INSTANCE
				.getEPackage(EcorePackage.eNS_URI);
		TggPackage theTggPackage = (TggPackage) EPackage.Registry.INSTANCE
				.getEPackage(TggPackage.eNS_URI);
		HelpersPackage theHelpersPackage = (HelpersPackage) EPackage.Registry.INSTANCE
				.getEPackage(HelpersPackage.eNS_URI);
		StoryDiagramEcorePackage theStoryDiagramEcorePackage = (StoryDiagramEcorePackage) EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI);
		de.hpi.sam.storyDiagramEcore.helpers.HelpersPackage theHelpersPackage_1 = (de.hpi.sam.storyDiagramEcore.helpers.HelpersPackage) EPackage.Registry.INSTANCE
				.getEPackage(de.hpi.sam.storyDiagramEcore.helpers.HelpersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		simpleTransformationGenerationStrategyEClass.getESuperTypes().add(
				this.getStoryDiagramBasedGenerationStrategy());
		conflictCheckTransformationGenerationStrategyEClass.getESuperTypes()
				.add(this.getGenerationStrategy());
		storyDiagramBasedGenerationStrategyEClass.getESuperTypes().add(
				this.getGenerationStrategy());
		synchronizationStrategyEClass.getESuperTypes().add(
				this.getGenerationStrategy());

		// Initialize classes and features; add operations and parameters
		initEClass(generationStrategyEClass, GenerationStrategy.class,
				"GenerationStrategy", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(generationStrategyEClass, null,
				"generateRules", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "projectURI", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "javaBasePackage", 0,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEPackage(), "rulesPackage", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getTGGDiagram(), "tggDiagram", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTggPackage.getTGGRule(), "rules", 0, -1,
				IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getRuleGenerationException());

		initEClass(simpleTransformationGenerationStrategyEClass,
				SimpleTransformationGenerationStrategy.class,
				"SimpleTransformationGenerationStrategy", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(traceabilityLinkStoreEClass, TraceabilityLinkStore.class,
				"TraceabilityLinkStore", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTraceabilityLinkStore_TraceabilityLinks(),
				this.getTraceabilityLink(), null, "traceabilityLinks", null, 0,
				-1, TraceabilityLinkStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(theHelpersPackage.getMapEntry());
		EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEReference(getTraceabilityLinkStore_Mappings(), g1, null,
				"mappings", null, 0, -1, TraceabilityLinkStore.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(traceabilityLinkEClass, TraceabilityLink.class,
				"TraceabilityLink", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTraceabilityLink_Sources(),
				theEcorePackage.getEObject(), null, "sources", null, 0, -1,
				TraceabilityLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTraceabilityLink_Targets(),
				theEcorePackage.getEObject(), null, "targets", null, 0, -1,
				TraceabilityLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conflictCheckTransformationGenerationStrategyEClass,
				ConflictCheckTransformationGenerationStrategy.class,
				"ConflictCheckTransformationGenerationStrategy", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(storyDiagramBasedGenerationStrategyEClass,
				StoryDiagramBasedGenerationStrategy.class,
				"StoryDiagramBasedGenerationStrategy", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(storyDiagramBasedGenerationStrategyEClass,
				theHelpersPackage.getURI(), "getGeneratorStoryDiagramURI", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEClass(generationInfoStoreEClass, GenerationInfoStore.class,
				"GenerationInfoStore", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGenerationInfoStore_RuleInfos(),
				this.getRuleInfoStore(), null, "ruleInfos", null, 0, -1,
				GenerationInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGenerationInfoStore_TggDiagram(),
				theTggPackage.getTGGDiagram(), null, "tggDiagram", null, 0, 1,
				GenerationInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGenerationInfoStore_RuleSetEClass(),
				theEcorePackage.getEClass(), null, "ruleSetEClass", null, 0, 1,
				GenerationInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ruleInfoStoreEClass, RuleInfoStore.class, "RuleInfoStore",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuleInfoStore_TggRule(), theTggPackage.getTGGRule(),
				null, "tggRule", null, 0, 1, RuleInfoStore.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getRuleInfoStore_RuleEClass(),
				theEcorePackage.getEClass(), null, "ruleEClass", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleInfoStore_ForwardTransformationActivityDiagram(),
				theStoryDiagramEcorePackage.getActivityDiagram(), null,
				"forwardTransformationActivityDiagram", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleInfoStore_MappingTransformationActivityDiagram(),
				theStoryDiagramEcorePackage.getActivityDiagram(), null,
				"mappingTransformationActivityDiagram", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleInfoStore_ReverseTransformationActivityDiagram(),
				theStoryDiagramEcorePackage.getActivityDiagram(), null,
				"reverseTransformationActivityDiagram", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(
				getRuleInfoStore_ForwardSynchronisationActivityDiagram(),
				theStoryDiagramEcorePackage.getActivityDiagram(), null,
				"forwardSynchronisationActivityDiagram", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(
				getRuleInfoStore_MappingSynchronisationActivityDiagram(),
				theStoryDiagramEcorePackage.getActivityDiagram(), null,
				"mappingSynchronisationActivityDiagram", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(
				getRuleInfoStore_ReverseSynchronizationActivityDiagram(),
				theStoryDiagramEcorePackage.getActivityDiagram(), null,
				"reverseSynchronizationActivityDiagram", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage_1.getMapEntry());
		g2 = createEGenericType(theEcorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theStoryDiagramEcorePackage
				.getActivityDiagram());
		g1.getETypeArguments().add(g2);
		initEReference(getRuleInfoStore_HelperActivityDiagrams(), g1, null,
				"helperActivityDiagrams", null, 0, -1, RuleInfoStore.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getRuleInfoStore_ForwardTransformationOperation(),
				theEcorePackage.getEOperation(), null,
				"forwardTransformationOperation", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleInfoStore_MappingTransformationOperation(),
				theEcorePackage.getEOperation(), null,
				"mappingTransformationOperation", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleInfoStore_ReverseTransformationOperation(),
				theEcorePackage.getEOperation(), null,
				"reverseTransformationOperation", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleInfoStore_ForwardSynchronizationOperation(),
				theEcorePackage.getEOperation(), null,
				"forwardSynchronizationOperation", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleInfoStore_MappingSynchronizationOperation(),
				theEcorePackage.getEOperation(), null,
				"mappingSynchronizationOperation", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleInfoStore_ReverseSynchronizationOperation(),
				theEcorePackage.getEOperation(), null,
				"reverseSynchronizationOperation", null, 0, 1,
				RuleInfoStore.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(synchronizationStrategyEClass,
				SynchronizationStrategy.class, "SynchronizationStrategy",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize data types
		initEDataType(ruleGenerationExceptionEDataType,
				RuleGenerationException.class, "RuleGenerationException",
				IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
	}

} // GenerationStrategiesPackageImpl
