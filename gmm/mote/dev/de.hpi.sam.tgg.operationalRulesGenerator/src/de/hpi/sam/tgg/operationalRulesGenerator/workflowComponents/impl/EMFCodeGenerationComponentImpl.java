/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl;

import java.io.IOException;
import java.util.Map.Entry;

import org.eclipse.core.runtime.Path;
import org.eclipse.emf.codegen.ecore.generator.Generator;
import org.eclipse.emf.codegen.ecore.genmodel.GenJDKLevel;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenBaseGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.util.GenModelUtil;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.importer.ModelImporter.EPackageImportInfo;
import org.eclipse.emf.importer.ecore.EcoreImporter;

import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import de.mdelab.workflow.util.WorkflowUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>EMF Code Generation Component</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.EMFCodeGenerationComponentImpl#getProjectName <em>Project Name</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.EMFCodeGenerationComponentImpl#getTggFileURI <em>Tgg File URI</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.EMFCodeGenerationComponentImpl#getCorrespondenceMetamodelURI <em>Correspondence Metamodel URI</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.EMFCodeGenerationComponentImpl#getJavaBasePackage <em>Java Base Package</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EMFCodeGenerationComponentImpl extends WorkflowComponentImpl
		implements EMFCodeGenerationComponent {
	/**
	 * The default value of the '{@link #getProjectName() <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getProjectName()
	 * @generated
	 * @ordered
	 */
	protected static final String PROJECT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProjectName() <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getProjectName()
	 * @generated
	 * @ordered
	 */
	protected String projectName = PROJECT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getTggFileURI() <em>Tgg File URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTggFileURI()
	 * @generated
	 * @ordered
	 */
	protected static final String TGG_FILE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTggFileURI() <em>Tgg File URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTggFileURI()
	 * @generated
	 * @ordered
	 */
	protected String tggFileURI = TGG_FILE_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getCorrespondenceMetamodelURI() <em>Correspondence Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getCorrespondenceMetamodelURI()
	 * @generated
	 * @ordered
	 */
	protected static final String CORRESPONDENCE_METAMODEL_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCorrespondenceMetamodelURI() <em>Correspondence Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getCorrespondenceMetamodelURI()
	 * @generated
	 * @ordered
	 */
	protected String correspondenceMetamodelURI = CORRESPONDENCE_METAMODEL_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getJavaBasePackage() <em>Java Base Package</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getJavaBasePackage()
	 * @generated
	 * @ordered
	 */
	protected static final String JAVA_BASE_PACKAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getJavaBasePackage() <em>Java Base Package</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getJavaBasePackage()
	 * @generated
	 * @ordered
	 */
	protected String javaBasePackage = JAVA_BASE_PACKAGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected EMFCodeGenerationComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowComponentsPackage.Literals.EMF_CODE_GENERATION_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setProjectName(String newProjectName) {
		String oldProjectName = projectName;
		projectName = newProjectName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__PROJECT_NAME,
					oldProjectName, projectName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getTggFileURI() {
		return tggFileURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTggFileURI(String newTggFileURI) {
		String oldTggFileURI = tggFileURI;
		tggFileURI = newTggFileURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__TGG_FILE_URI,
					oldTggFileURI, tggFileURI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getCorrespondenceMetamodelURI() {
		return correspondenceMetamodelURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCorrespondenceMetamodelURI(
			String newCorrespondenceMetamodelURI) {
		String oldCorrespondenceMetamodelURI = correspondenceMetamodelURI;
		correspondenceMetamodelURI = newCorrespondenceMetamodelURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__CORRESPONDENCE_METAMODEL_URI,
					oldCorrespondenceMetamodelURI, correspondenceMetamodelURI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getJavaBasePackage() {
		return javaBasePackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setJavaBasePackage(String newJavaBasePackage) {
		String oldJavaBasePackage = javaBasePackage;
		javaBasePackage = newJavaBasePackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__JAVA_BASE_PACKAGE,
					oldJavaBasePackage, javaBasePackage));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__PROJECT_NAME:
			return getProjectName();
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__TGG_FILE_URI:
			return getTggFileURI();
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__CORRESPONDENCE_METAMODEL_URI:
			return getCorrespondenceMetamodelURI();
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__JAVA_BASE_PACKAGE:
			return getJavaBasePackage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__PROJECT_NAME:
			setProjectName((String) newValue);
			return;
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__TGG_FILE_URI:
			setTggFileURI((String) newValue);
			return;
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__CORRESPONDENCE_METAMODEL_URI:
			setCorrespondenceMetamodelURI((String) newValue);
			return;
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__JAVA_BASE_PACKAGE:
			setJavaBasePackage((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__PROJECT_NAME:
			setProjectName(PROJECT_NAME_EDEFAULT);
			return;
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__TGG_FILE_URI:
			setTggFileURI(TGG_FILE_URI_EDEFAULT);
			return;
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__CORRESPONDENCE_METAMODEL_URI:
			setCorrespondenceMetamodelURI(CORRESPONDENCE_METAMODEL_URI_EDEFAULT);
			return;
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__JAVA_BASE_PACKAGE:
			setJavaBasePackage(JAVA_BASE_PACKAGE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__PROJECT_NAME:
			return PROJECT_NAME_EDEFAULT == null ? projectName != null
					: !PROJECT_NAME_EDEFAULT.equals(projectName);
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__TGG_FILE_URI:
			return TGG_FILE_URI_EDEFAULT == null ? tggFileURI != null
					: !TGG_FILE_URI_EDEFAULT.equals(tggFileURI);
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__CORRESPONDENCE_METAMODEL_URI:
			return CORRESPONDENCE_METAMODEL_URI_EDEFAULT == null ? correspondenceMetamodelURI != null
					: !CORRESPONDENCE_METAMODEL_URI_EDEFAULT
							.equals(correspondenceMetamodelURI);
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT__JAVA_BASE_PACKAGE:
			return JAVA_BASE_PACKAGE_EDEFAULT == null ? javaBasePackage != null
					: !JAVA_BASE_PACKAGE_EDEFAULT.equals(javaBasePackage);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (projectName: ");
		result.append(projectName);
		result.append(", tggFileURI: ");
		result.append(tggFileURI);
		result.append(", correspondenceMetamodelURI: ");
		result.append(correspondenceMetamodelURI);
		result.append(", javaBasePackage: ");
		result.append(javaBasePackage);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean checkConfiguration(WorkflowExecutionContext context)
			throws IOException {
		boolean success = true;

		if (getCorrespondenceMetamodelURI() == null
				|| "".equals(getCorrespondenceMetamodelURI())) {
			context.getLogger().addError(
					"correspondenceMetamodelURI is not set", null, this);
			success = false;
		}

		if (getJavaBasePackage() == null || "".equals(getJavaBasePackage())) {
			context.getLogger().addError("javaBasePackage is not set", null,
					this);
			success = false;
		}

		if (getProjectName() == null || "".equals(getProjectName())) {
			context.getLogger().addError("projectName is not set", null, this);
			success = false;
		}

		if (getTggFileURI() == null || "".equals(getTggFileURI())) {
			context.getLogger().addError("tggFileURI is not set", null, this);
			success = false;
		}

		return success;
	}

	@Override
	public void execute(WorkflowExecutionContext context)
			throws WorkflowExecutionException, IOException {
		context.getLogger().setTotalWork(6, this);

		context.getLogger().addInfo("Generating code...", this);

		URI correspondenceMetamodelURI = WorkflowUtil.getResolvedURI(
				URI.createURI(getCorrespondenceMetamodelURI()),
				context.getWorkflowFileURI());
		URI genmodelURI = URI.createPlatformResourceURI("/"
				+ getProjectName()
				+ "/src-gen/"
				+ correspondenceMetamodelURI.trimFileExtension()
						.appendFileExtension("genmodel").lastSegment(), true);

		ResourceSet rs = context.getGlobalResourceSet();

		Resource correspondenceMetamodelResource = rs.getResource(
				correspondenceMetamodelURI, true);

		EPackage correspondenceMetamodelPackage = (EPackage) correspondenceMetamodelResource
				.getContents().get(0);

		URI oldGenModelURI = EcorePlugin
				.getEPackageNsURIToGenModelLocationMap().remove(
						correspondenceMetamodelPackage.getNsURI());

		context.getLogger().worked(this);

		/*
		 * Create the generator model.
		 */

		context.getLogger().addInfo(
				"Creating genmodel '" + genmodelURI + "'...", this);

		// Adjust model importer
		EcoreImporter ecoreImporter = new EcoreImporter();
		ecoreImporter.setUsePlatformURI(true);
		ecoreImporter.setGenModelProjectLocation(new Path("/"
				+ getProjectName()));
		ecoreImporter.setModelPluginDirectory("/" + getProjectName()
				+ "/src-gen");
		ecoreImporter.setGenModelContainerPath(new Path("/" + getProjectName()
				+ "/src-gen"));
		ecoreImporter.setGenModelFileName(genmodelURI.lastSegment());
		ecoreImporter.setModelLocation(correspondenceMetamodelURI.toString());

		context.getLogger().worked(this);

		try {
			org.eclipse.emf.ecore.plugin.EcorePlugin
					.getEPackageNsURIToGenModelLocationMap().values()
					.contains(null);

			for (Entry<String, URI> entry : EcorePlugin
					.getEPackageNsURIToGenModelLocationMap().entrySet()) {
				if (entry.getValue() == null) {
					System.out.println("Key: " + entry.getKey()
							+ " has value null");
				}
			}

			// compute EPackages
			ecoreImporter.computeEPackages(new BasicMonitor());

			context.getLogger().worked(this);

			/*
			 * Adjust EPackages. Load the referenced GenModels and add their
			 * contained GenPackages to the importers referenceGenPackages list.
			 */

			for (GenModel genModel : ecoreImporter.getExternalGenModels()) {
				ecoreImporter.getReferencedGenPackages().addAll(
						genModel.getGenPackages());
			}

			/*
			 * Adjust the base package and prefix of the correspondence package
			 * in the genmodel
			 */
			for (EPackage ePackage : ecoreImporter.getEPackages()) {
				if (ePackage.eResource().getURI()
						.equals(correspondenceMetamodelURI)) {
					EPackageImportInfo importInfo = ecoreImporter
							.getEPackageImportInfo(ePackage);
					importInfo.setConvert(true);

					importInfo.setBasePackage(getJavaBasePackage());

					importInfo
							.setPrefix(firstLetterToUpperCase(correspondenceMetamodelPackage
									.getName()));
				}
			}

			ecoreImporter.adjustEPackages(new BasicMonitor());

			context.getLogger().worked(this);
			context.getLogger().addInfo( "Creating genmodel...", this);

			// Adjust GenModel
			GenModel genModel = ecoreImporter.getGenModel();
			genModel.setValidateModel(true);
			genModel.setComplianceLevel(GenJDKLevel.JDK60_LITERAL);
			genModel.setCodeFormatting(false);

			/*
			 * Save the genmodel
			 */
			ecoreImporter.prepareGenModelAndEPackages(new BasicMonitor());
			ecoreImporter.saveGenModelAndEPackages(new BasicMonitor());

			context.getLogger().worked(this);
		} catch (Exception e) {
			throw new WorkflowExecutionException(
					"Error during genmodel generation: " + e.getMessage(), e);
		}

		/*
		 * Generate source code
		 */
		context.getLogger().addInfo("Generating source code...", this);

		/*
		 * Open the genmodel again
		 */
		Resource genModelResource = rs.getResource(genmodelURI, true);

		GenModel genModel = (GenModel) genModelResource.getContents().get(0);
		genModel.setCanGenerate(true);

		/*
		 * Create the generator and set the model-level input object
		 */
		// DB
		Generator generator = GenModelUtil.createGenerator(genModel) /*new Generator()*/;
		//generator.setInput(genModel);
		context.getLogger().addInfo( "Starting generation...", this );

		/*
		 * Generate model code.
		 */
		generator.generate(genModel,
				GenBaseGeneratorAdapter.MODEL_PROJECT_TYPE, new BasicMonitor());

		context.getLogger().worked(this);
	}

	private String firstLetterToUpperCase(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}
} // EMFCodeGenerationComponentImpl
