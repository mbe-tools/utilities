/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies;

import org.eclipse.emf.common.util.URI;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Story Diagram Based Generation Strategy</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <!-- begin-model-doc --> This generation strategy creates Story Diagrams that
 * perform only model transformation but also include a runtime conflict check.
 * <!-- end-model-doc -->
 * 
 * 
 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getStoryDiagramBasedGenerationStrategy()
 * @model abstract="true"
 * @generated
 */
public interface StoryDiagramBasedGenerationStrategy extends GenerationStrategy {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Returns the URI of the *.story file that contains the main story activity
	 * of the generation strategy. The first activity in this file is executed.
	 * <!-- end-model-doc -->
	 * 
	 * @model kind="operation" dataType="de.mdelab.workflow.helpers.URI"
	 * @generated
	 */
	URI getGeneratorStoryDiagramURI();

} // StoryDiagramBasedGenerationStrategy
