/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.ConflictCheckTransformationGenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Conflict Check Transformation Generation Strategy</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ConflictCheckTransformationGenerationStrategyImpl extends
		GenerationStrategyImpl implements
		ConflictCheckTransformationGenerationStrategy {
//	private static final String TGG_DIAGRAM = "tggDiagram";
//	private static final String RULES_PACKAGE = "rulesPackage";
//	private static final String MOTE_PACKAGE = "motePackage";
//	private static final String RULE_INFO_STORE = "ruleInfoStore";

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ConflictCheckTransformationGenerationStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenerationStrategiesPackage.Literals.CONFLICT_CHECK_TRANSFORMATION_GENERATION_STRATEGY;
	}

	@Override
	public void generateRules(String projectName, String javaBasePackage,
			EPackage rulesPackage, TGGDiagram tggDiagram, EList<TGGRule> rules)
			throws RuleGenerationException {
		// ResourceSet resourceSet = new ResourceSetImpl();
		//
		// /*
		// * Load the mote EPackage from the ecore file in the MoTE plugin
		// */
		// Resource moteResource =
		// resourceSet.getResource(URI.createPlatformPluginURI("/de.hpi.sam.mote/model/mote.ecore",
		// true), true);
		//
		// EPackage moteEPackage = (EPackage) moteResource.getContents().get(0);
		//
		// /*
		// * Create EClasses for ruleSet and rules
		// */
		// GenerationInfoStore genInfos =
		// generateRuleSetAndRuleEClasses(resourceSet, rulesPackage,
		// moteEPackage, tggDiagram, projectName,
		// javaBasePackage);
		//
		// /*
		// * Create contents of activities
		// */
		// generateActivityContents(genInfos);
		//
		// /*
		// * Change the URIs of the reference packages to their nsURIs. This is
		// * necessary so references in the story diagrams are resolved using
		// the
		// * package registry.
		// */
		// URI oldRulesPackageURI = rulesPackage.eResource().getURI();
		// URI oldMoteURI = moteResource.getURI();
		//
		// rulesPackage.eResource().setURI(URI.createURI(rulesPackage.getNsURI()));
		// moteResource.setURI(URI.createURI(moteEPackage.getNsURI()));
		//
		// List<Resource> resources = new ArrayList<Resource>();
		//
		// for (RuleInfoStore ruleInfo : genInfos.getRuleInfos())
		// {
		// for (ActivityDiagram ad :
		// ruleInfo.getHelperActivityDiagrams().values())
		// {
		// resources.add(createResourceForActivityDiagram(ad, resourceSet,
		// projectName));
		// }
		//
		// resources.add(createResourceForActivityDiagram(ruleInfo.getForwardTransformationActivityDiagram(),
		// resourceSet, projectName));
		// resources.add(createResourceForActivityDiagram(ruleInfo.getMappingTransformationActivityDiagram(),
		// resourceSet, projectName));
		// resources.add(createResourceForActivityDiagram(ruleInfo.getReverseTransformationActivityDiagram(),
		// resourceSet, projectName));
		// }
		//
		// for (Resource r : resources)
		// {
		// try
		// {
		// r.save(Collections.EMPTY_MAP);
		// }
		// catch (IOException e)
		// {
		// e.printStackTrace();
		// throw new
		// RuleGenerationException("Could not save generated activity diagrams.",
		// e);
		// }
		// }
		//
		// rulesPackage.eResource().setURI(oldRulesPackageURI);
		// moteResource.setURI(oldMoteURI);

		throw new UnsupportedOperationException();
	}

	// private Resource createResourceForActivityDiagram(ActivityDiagram
	// activityDiagram, ResourceSet resourceSet, String projectName)
	// throws RuleGenerationException
	// {
	// if (activityDiagram == null)
	// {
	// return null;
	// }
	//
	// /*
	// * Save the generated activity diagram
	// */
	// Resource generatedActivityDiagramResource =
	// resourceSet.createResource(URI.createPlatformResourceURI("/" +
	// projectName
	// + "/model/story/" + activityDiagram.getName() + ".story", true));
	//
	// generatedActivityDiagramResource.getContents().add(activityDiagram);
	//
	// return generatedActivityDiagramResource;
	// }
	//
	// private GenerationInfoStore generateRuleSetAndRuleEClasses(ResourceSet
	// resourceSet, EPackage rulesPackage, EPackage moteEPackage,
	// TGGDiagram tggDiagram, String projectName, String javaBasePackage) throws
	// RuleGenerationException
	// {
	// /*
	// * Load generator story diagram
	// */
	// Resource resource = resourceSet
	// .getResource(
	// URI.createPlatformPluginURI(
	// "/de.hpi.sam.tgg.operationalRulesGenerator/generatorResources/ConflictCheckTransformationGenerationStrategy/generate_rules.story",
	// true), true);
	//
	// ActivityDiagram activityDiagram = (ActivityDiagram)
	// resource.getContents().get(0);
	//
	// /*
	// * Get the activity to generate the rules
	// */
	// Activity generate_rulesActivity = activityDiagram.getActivities().get(0);
	//
	// /*
	// * Set up the activities parameters
	// */
	// List<InterpreterVariable> variableBindings = new
	// ArrayList<InterpreterVariable>();
	//
	// InterpreterVariable iv =
	// StoryDiagramInterpreterFactory.eINSTANCE.createInterpreterVariable();
	// iv.setName(RULES_PACKAGE);
	// iv.setClassifier(rulesPackage.eClass());
	// iv.setValue(rulesPackage);
	// variableBindings.add(iv);
	//
	// iv =
	// StoryDiagramInterpreterFactory.eINSTANCE.createInterpreterVariable();
	// iv.setName(MOTE_PACKAGE);
	// iv.setClassifier(moteEPackage.eClass());
	// iv.setValue(moteEPackage);
	// variableBindings.add(iv);
	//
	// iv =
	// StoryDiagramInterpreterFactory.eINSTANCE.createInterpreterVariable();
	// iv.setName(TGG_DIAGRAM);
	// iv.setClassifier(tggDiagram.eClass());
	// iv.setValue(tggDiagram);
	// variableBindings.add(iv);
	//
	// iv =
	// StoryDiagramInterpreterFactory.eINSTANCE.createInterpreterVariable();
	// iv.setName("projectName");
	// iv.setClassifier(EcorePackage.Literals.ESTRING);
	// iv.setValue(projectName);
	// variableBindings.add(iv);
	//
	// iv =
	// StoryDiagramInterpreterFactory.eINSTANCE.createInterpreterVariable();
	// iv.setName("basePackage");
	// iv.setClassifier(EcorePackage.Literals.ESTRING);
	// iv.setValue(javaBasePackage);
	// variableBindings.add(iv);
	//
	// StoryDiagramInterpreter sdi =
	// StoryDiagramInterpreterFactory.eINSTANCE.createStoryDiagramInterpreter();
	//
	// try
	// {
	// sdi.eAdapters().add(new StoryDiagramInterpreterOutputStreamAdapter());
	// sdi.setEnableNotifications(false);
	// sdi.setAdditionalChecks(false);
	//
	// Map<String, InterpreterVariable> m =
	// sdi.executeStoryActivity(generate_rulesActivity, variableBindings,
	// getClass()
	// .getClassLoader());
	//
	// GenerationInfoStore genInfos = (GenerationInfoStore)
	// m.get(StoryDiagramInterpreterConstants.RETURN_VALUE).getValue();
	//
	// return genInfos;
	// }
	// catch (StoryDiagramInterpreterException e)
	// {
	// throw new RuleGenerationException("Error during rule generation.", e);
	// }
	// }
	//
	// private void generateActivityContents(GenerationInfoStore genInfos)
	// throws RuleGenerationException
	// {
	// ExecutorService executorService =
	// Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
	//
	// List<Future<?>> futures = new ArrayList<Future<?>>();
	//
	// for (RuleInfoStore ruleInfo : genInfos.getRuleInfos())
	// {
	// ActivityContentGenerator worker = new ActivityContentGenerator(ruleInfo);
	// futures.add(executorService.submit(worker));
	// }
	//
	// for (Future<?> future : futures)
	// {
	// try
	// {
	// future.get();
	// }
	// catch (InterruptedException e)
	// {
	// e.printStackTrace();
	// throw new RuleGenerationException("Error during rule generation.", e);
	// }
	// catch (ExecutionException e)
	// {
	// e.printStackTrace();
	// throw new RuleGenerationException("Error during rule generation.", e);
	// }
	// }
	// }
	//
	// private class ActivityContentGenerator implements Callable<Boolean>
	// {
	// private RuleInfoStore ruleInfo;
	//
	// public ActivityContentGenerator(RuleInfoStore ruleInfo)
	// {
	// this.ruleInfo = ruleInfo;
	// }
	//
	// @Override
	// public Boolean call() throws Exception
	// {
	// /*
	// * Load generator story diagram
	// */
	// ResourceSet resourceSet = new ResourceSetImpl();
	//
	// Resource resource = resourceSet
	// .getResource(
	// URI.createPlatformPluginURI(
	// "/de.hpi.sam.tgg.operationalRulesGenerator/generatorResources/ConflictCheckTransformationGenerationStrategy/generate_activityContents.story",
	// true), true);
	//
	// ActivityDiagram activityDiagram = (ActivityDiagram)
	// resource.getContents().get(0);
	//
	// /*
	// * Get the activity to generate the rules
	// */
	// Activity generate_rulesActivity = activityDiagram.getActivities().get(0);
	//
	// /*
	// * Set up the activities parameters
	// */
	// List<InterpreterVariable> variableBindings = new
	// ArrayList<InterpreterVariable>();
	//
	// InterpreterVariable iv =
	// StoryDiagramInterpreterFactory.eINSTANCE.createInterpreterVariable();
	// iv.setName(RULE_INFO_STORE);
	// iv.setClassifier(ruleInfo.eClass());
	// iv.setValue(ruleInfo);
	// variableBindings.add(iv);
	//
	// StoryDiagramInterpreter sdi =
	// StoryDiagramInterpreterFactory.eINSTANCE.createStoryDiagramInterpreter();
	//
	// try
	// {
	// sdi.eAdapters().add(new StoryDiagramInterpreterOutputStreamAdapter());
	// sdi.setEnableNotifications(false);
	// sdi.setAdditionalChecks(false);
	//
	// sdi.executeStoryActivity(generate_rulesActivity, variableBindings,
	// getClass().getClassLoader());
	// }
	// catch (StoryDiagramInterpreterException e)
	// {
	// throw new RuleGenerationException("Error during rule generation.", e);
	// }
	//
	// return true;
	// }
	// }
} // ConflictCheckTransformationGenerationStrategyImpl
