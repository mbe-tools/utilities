/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Manifest Loader Component</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Loads the MANIFEST.MF file of a plugin project.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent#getModelSlot <em>Model Slot</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent#getProjectName <em>Project Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getManifestLoaderComponent()
 * @model
 * @generated
 */
public interface ManifestLoaderComponent extends WorkflowComponent {

	/**
	 * Returns the value of the '<em><b>Model Slot</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Slot</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Model Slot</em>' attribute.
	 * @see #setModelSlot(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getManifestLoaderComponent_ModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getModelSlot();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent#getModelSlot <em>Model Slot</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Model Slot</em>' attribute.
	 * @see #getModelSlot()
	 * @generated
	 */
	void setModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Project Name</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Project Name</em>' attribute.
	 * @see #setProjectName(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getManifestLoaderComponent_ProjectName()
	 * @model required="true"
	 * @generated
	 */
	String getProjectName();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent#getProjectName <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Project Name</em>' attribute.
	 * @see #getProjectName()
	 * @generated
	 */
	void setProjectName(String value);

} // ManifestLoaderComponent
