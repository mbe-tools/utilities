/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import de.hpi.sam.tgg.TGGDiagram;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Generation Strategy</b></em>'. <!-- end-user-doc -->
 * 
 * <!-- begin-model-doc --> The GenerationStrategy is responsible for actually
 * processing the TGG rules and creating the opertional rules. <!--
 * end-model-doc -->
 * 
 * 
 * @see de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorPackage#getGenerationStrategy()
 * @model abstract="true"
 * @generated
 */
public interface GenerationStrategy extends EObject
{
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @model
	 * @generated
	 */
	void generate_rules(String projectURI, EPackage rulesPackage, TGGDiagram tggDiagram);

} // GenerationStrategy
