/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage;
import de.mdelab.workflow.NamedComponent;
import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance
 * hierarchy. It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the
 * inheritance hierarchy until a non-null result is returned, which is the
 * result of the switch. <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage
 * @generated
 */
public class WorkflowComponentsSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static WorkflowComponentsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public WorkflowComponentsSwitch() {
		if (modelPackage == null) {
			modelPackage = WorkflowComponentsPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		} else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return eSuperTypes.isEmpty() ? defaultCase(theEObject) : doSwitch(
					eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case WorkflowComponentsPackage.MANIFEST_LOADER_COMPONENT: {
			ManifestLoaderComponent manifestLoaderComponent = (ManifestLoaderComponent) theEObject;
			T result = caseManifestLoaderComponent(manifestLoaderComponent);
			if (result == null)
				result = caseWorkflowComponent(manifestLoaderComponent);
			if (result == null)
				result = caseNamedComponent(manifestLoaderComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WorkflowComponentsPackage.GENERATION_STRATEGY_COMPONENT: {
			GenerationStrategyComponent generationStrategyComponent = (GenerationStrategyComponent) theEObject;
			T result = caseGenerationStrategyComponent(generationStrategyComponent);
			if (result == null)
				result = caseWorkflowComponent(generationStrategyComponent);
			if (result == null)
				result = caseNamedComponent(generationStrategyComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WorkflowComponentsPackage.EMF_CODE_GENERATION_COMPONENT: {
			EMFCodeGenerationComponent emfCodeGenerationComponent = (EMFCodeGenerationComponent) theEObject;
			T result = caseEMFCodeGenerationComponent(emfCodeGenerationComponent);
			if (result == null)
				result = caseWorkflowComponent(emfCodeGenerationComponent);
			if (result == null)
				result = caseNamedComponent(emfCodeGenerationComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WorkflowComponentsPackage.MANIFEST_GENERATOR_COMPONENT: {
			ManifestGeneratorComponent manifestGeneratorComponent = (ManifestGeneratorComponent) theEObject;
			T result = caseManifestGeneratorComponent(manifestGeneratorComponent);
			if (result == null)
				result = caseWorkflowComponent(manifestGeneratorComponent);
			if (result == null)
				result = caseNamedComponent(manifestGeneratorComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Manifest Loader Component</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Manifest Loader Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseManifestLoaderComponent(ManifestLoaderComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generation Strategy Component</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generation Strategy Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenerationStrategyComponent(GenerationStrategyComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EMF Code Generation Component</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EMF Code Generation Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEMFCodeGenerationComponent(EMFCodeGenerationComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Manifest Generator Component</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Manifest Generator Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseManifestGeneratorComponent(ManifestGeneratorComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Component</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedComponent(NamedComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Workflow Component</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Workflow Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorkflowComponent(WorkflowComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch, but this is
	 * the last case anyway. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} // WorkflowComponentsSwitch
