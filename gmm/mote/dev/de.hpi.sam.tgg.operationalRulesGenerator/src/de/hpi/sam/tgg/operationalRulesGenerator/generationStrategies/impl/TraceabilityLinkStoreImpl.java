/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore;
import de.mdelab.workflow.helpers.HelpersPackage;
import de.mdelab.workflow.helpers.impl.MapEntryImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Traceability Link Store</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.TraceabilityLinkStoreImpl#getTraceabilityLinks <em>Traceability Links</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.TraceabilityLinkStoreImpl#getMappings <em>Mappings</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TraceabilityLinkStoreImpl extends EObjectImpl implements
		TraceabilityLinkStore {
	/**
	 * The cached value of the '{@link #getTraceabilityLinks()
	 * <em>Traceability Links</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getTraceabilityLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<TraceabilityLink> traceabilityLinks;

	/**
	 * The cached value of the '{@link #getMappings() <em>Mappings</em>}' map.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMappings()
	 * @generated
	 * @ordered
	 */
	protected EMap<Object, Object> mappings;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected TraceabilityLinkStoreImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenerationStrategiesPackage.Literals.TRACEABILITY_LINK_STORE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TraceabilityLink> getTraceabilityLinks() {
		if (traceabilityLinks == null) {
			traceabilityLinks = new EObjectContainmentEList<TraceabilityLink>(
					TraceabilityLink.class,
					this,
					GenerationStrategiesPackage.TRACEABILITY_LINK_STORE__TRACEABILITY_LINKS);
		}
		return traceabilityLinks;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Object, Object> getMappings() {
		if (mappings == null) {
			mappings = new EcoreEMap<Object, Object>(
					HelpersPackage.Literals.MAP_ENTRY,
					MapEntryImpl.class,
					this,
					GenerationStrategiesPackage.TRACEABILITY_LINK_STORE__MAPPINGS);
		}
		return mappings;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case GenerationStrategiesPackage.TRACEABILITY_LINK_STORE__TRACEABILITY_LINKS:
			return ((InternalEList<?>) getTraceabilityLinks()).basicRemove(
					otherEnd, msgs);
		case GenerationStrategiesPackage.TRACEABILITY_LINK_STORE__MAPPINGS:
			return ((InternalEList<?>) getMappings()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case GenerationStrategiesPackage.TRACEABILITY_LINK_STORE__TRACEABILITY_LINKS:
			return getTraceabilityLinks();
		case GenerationStrategiesPackage.TRACEABILITY_LINK_STORE__MAPPINGS:
			if (coreType)
				return getMappings();
			else
				return getMappings().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case GenerationStrategiesPackage.TRACEABILITY_LINK_STORE__TRACEABILITY_LINKS:
			getTraceabilityLinks().clear();
			getTraceabilityLinks().addAll(
					(Collection<? extends TraceabilityLink>) newValue);
			return;
		case GenerationStrategiesPackage.TRACEABILITY_LINK_STORE__MAPPINGS:
			((EStructuralFeature.Setting) getMappings()).set(newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case GenerationStrategiesPackage.TRACEABILITY_LINK_STORE__TRACEABILITY_LINKS:
			getTraceabilityLinks().clear();
			return;
		case GenerationStrategiesPackage.TRACEABILITY_LINK_STORE__MAPPINGS:
			getMappings().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case GenerationStrategiesPackage.TRACEABILITY_LINK_STORE__TRACEABILITY_LINKS:
			return traceabilityLinks != null && !traceabilityLinks.isEmpty();
		case GenerationStrategiesPackage.TRACEABILITY_LINK_STORE__MAPPINGS:
			return mappings != null && !mappings.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // TraceabilityLinkStoreImpl
