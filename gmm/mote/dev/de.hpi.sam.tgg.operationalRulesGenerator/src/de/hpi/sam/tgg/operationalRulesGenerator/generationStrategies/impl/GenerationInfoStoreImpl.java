/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Generation Info Store</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationInfoStoreImpl#getRuleInfos <em>Rule Infos</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationInfoStoreImpl#getTggDiagram <em>Tgg Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationInfoStoreImpl#getRuleSetEClass <em>Rule Set EClass</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GenerationInfoStoreImpl extends EObjectImpl implements
		GenerationInfoStore {
	/**
	 * The cached value of the '{@link #getRuleInfos() <em>Rule Infos</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuleInfos()
	 * @generated
	 * @ordered
	 */
	protected EList<RuleInfoStore> ruleInfos;

	/**
	 * The cached value of the '{@link #getTggDiagram() <em>Tgg Diagram</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTggDiagram()
	 * @generated
	 * @ordered
	 */
	protected TGGDiagram tggDiagram;
	/**
	 * The cached value of the '{@link #getRuleSetEClass() <em>Rule Set EClass</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getRuleSetEClass()
	 * @generated
	 * @ordered
	 */
	protected EClass ruleSetEClass;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected GenerationInfoStoreImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenerationStrategiesPackage.Literals.GENERATION_INFO_STORE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RuleInfoStore> getRuleInfos() {
		if (ruleInfos == null) {
			ruleInfos = new EObjectContainmentEList<RuleInfoStore>(
					RuleInfoStore.class,
					this,
					GenerationStrategiesPackage.GENERATION_INFO_STORE__RULE_INFOS);
		}
		return ruleInfos;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGDiagram getTggDiagram() {
		if (tggDiagram != null && tggDiagram.eIsProxy()) {
			InternalEObject oldTggDiagram = (InternalEObject) tggDiagram;
			tggDiagram = (TGGDiagram) eResolveProxy(oldTggDiagram);
			if (tggDiagram != oldTggDiagram) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.GENERATION_INFO_STORE__TGG_DIAGRAM,
							oldTggDiagram, tggDiagram));
			}
		}
		return tggDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGDiagram basicGetTggDiagram() {
		return tggDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTggDiagram(TGGDiagram newTggDiagram) {
		TGGDiagram oldTggDiagram = tggDiagram;
		tggDiagram = newTggDiagram;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.GENERATION_INFO_STORE__TGG_DIAGRAM,
					oldTggDiagram, tggDiagram));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleSetEClass() {
		if (ruleSetEClass != null && ruleSetEClass.eIsProxy()) {
			InternalEObject oldRuleSetEClass = (InternalEObject) ruleSetEClass;
			ruleSetEClass = (EClass) eResolveProxy(oldRuleSetEClass);
			if (ruleSetEClass != oldRuleSetEClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							GenerationStrategiesPackage.GENERATION_INFO_STORE__RULE_SET_ECLASS,
							oldRuleSetEClass, ruleSetEClass));
			}
		}
		return ruleSetEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass basicGetRuleSetEClass() {
		return ruleSetEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleSetEClass(EClass newRuleSetEClass) {
		EClass oldRuleSetEClass = ruleSetEClass;
		ruleSetEClass = newRuleSetEClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					GenerationStrategiesPackage.GENERATION_INFO_STORE__RULE_SET_ECLASS,
					oldRuleSetEClass, ruleSetEClass));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__RULE_INFOS:
			return ((InternalEList<?>) getRuleInfos()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__RULE_INFOS:
			return getRuleInfos();
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__TGG_DIAGRAM:
			if (resolve)
				return getTggDiagram();
			return basicGetTggDiagram();
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__RULE_SET_ECLASS:
			if (resolve)
				return getRuleSetEClass();
			return basicGetRuleSetEClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__RULE_INFOS:
			getRuleInfos().clear();
			getRuleInfos().addAll(
					(Collection<? extends RuleInfoStore>) newValue);
			return;
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__TGG_DIAGRAM:
			setTggDiagram((TGGDiagram) newValue);
			return;
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__RULE_SET_ECLASS:
			setRuleSetEClass((EClass) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__RULE_INFOS:
			getRuleInfos().clear();
			return;
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__TGG_DIAGRAM:
			setTggDiagram((TGGDiagram) null);
			return;
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__RULE_SET_ECLASS:
			setRuleSetEClass((EClass) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__RULE_INFOS:
			return ruleInfos != null && !ruleInfos.isEmpty();
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__TGG_DIAGRAM:
			return tggDiagram != null;
		case GenerationStrategiesPackage.GENERATION_INFO_STORE__RULE_SET_ECLASS:
			return ruleSetEClass != null;
		}
		return super.eIsSet(featureID);
	}

} // GenerationInfoStoreImpl
