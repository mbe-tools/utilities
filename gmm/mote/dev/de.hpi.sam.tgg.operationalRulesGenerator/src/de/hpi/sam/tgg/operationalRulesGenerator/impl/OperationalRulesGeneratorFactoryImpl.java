/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGenerator;
import de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorFactory;
import de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class OperationalRulesGeneratorFactoryImpl extends EFactoryImpl
		implements OperationalRulesGeneratorFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static OperationalRulesGeneratorFactory init() {
		try {
			OperationalRulesGeneratorFactory theOperationalRulesGeneratorFactory = (OperationalRulesGeneratorFactory) EPackage.Registry.INSTANCE
					.getEFactory("http://operationalRulesGenerator/1.0");
			if (theOperationalRulesGeneratorFactory != null) {
				return theOperationalRulesGeneratorFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OperationalRulesGeneratorFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public OperationalRulesGeneratorFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case OperationalRulesGeneratorPackage.OPERATIONAL_RULES_GENERATOR:
			return createOperationalRulesGenerator();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public OperationalRulesGenerator createOperationalRulesGenerator() {
		OperationalRulesGeneratorImpl operationalRulesGenerator = new OperationalRulesGeneratorImpl();
		return operationalRulesGenerator;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public OperationalRulesGeneratorPackage getOperationalRulesGeneratorPackage() {
		return (OperationalRulesGeneratorPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OperationalRulesGeneratorPackage getPackage() {
		return OperationalRulesGeneratorPackage.eINSTANCE;
	}

} // OperationalRulesGeneratorFactoryImpl
