/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.ConflictCheckTransformationGenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SimpleTransformationGenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.StoryDiagramBasedGenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SynchronizationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance
 * hierarchy. It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the
 * inheritance hierarchy until a non-null result is returned, which is the
 * result of the switch. <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage
 * @generated
 */
public class GenerationStrategiesSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static GenerationStrategiesPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public GenerationStrategiesSwitch() {
		if (modelPackage == null) {
			modelPackage = GenerationStrategiesPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		} else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return eSuperTypes.isEmpty() ? defaultCase(theEObject) : doSwitch(
					eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case GenerationStrategiesPackage.GENERATION_STRATEGY: {
			GenerationStrategy generationStrategy = (GenerationStrategy) theEObject;
			T result = caseGenerationStrategy(generationStrategy);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GenerationStrategiesPackage.SIMPLE_TRANSFORMATION_GENERATION_STRATEGY: {
			SimpleTransformationGenerationStrategy simpleTransformationGenerationStrategy = (SimpleTransformationGenerationStrategy) theEObject;
			T result = caseSimpleTransformationGenerationStrategy(simpleTransformationGenerationStrategy);
			if (result == null)
				result = caseStoryDiagramBasedGenerationStrategy(simpleTransformationGenerationStrategy);
			if (result == null)
				result = caseGenerationStrategy(simpleTransformationGenerationStrategy);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GenerationStrategiesPackage.TRACEABILITY_LINK_STORE: {
			TraceabilityLinkStore traceabilityLinkStore = (TraceabilityLinkStore) theEObject;
			T result = caseTraceabilityLinkStore(traceabilityLinkStore);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GenerationStrategiesPackage.TRACEABILITY_LINK: {
			TraceabilityLink traceabilityLink = (TraceabilityLink) theEObject;
			T result = caseTraceabilityLink(traceabilityLink);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GenerationStrategiesPackage.CONFLICT_CHECK_TRANSFORMATION_GENERATION_STRATEGY: {
			ConflictCheckTransformationGenerationStrategy conflictCheckTransformationGenerationStrategy = (ConflictCheckTransformationGenerationStrategy) theEObject;
			T result = caseConflictCheckTransformationGenerationStrategy(conflictCheckTransformationGenerationStrategy);
			if (result == null)
				result = caseGenerationStrategy(conflictCheckTransformationGenerationStrategy);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GenerationStrategiesPackage.STORY_DIAGRAM_BASED_GENERATION_STRATEGY: {
			StoryDiagramBasedGenerationStrategy storyDiagramBasedGenerationStrategy = (StoryDiagramBasedGenerationStrategy) theEObject;
			T result = caseStoryDiagramBasedGenerationStrategy(storyDiagramBasedGenerationStrategy);
			if (result == null)
				result = caseGenerationStrategy(storyDiagramBasedGenerationStrategy);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GenerationStrategiesPackage.GENERATION_INFO_STORE: {
			GenerationInfoStore generationInfoStore = (GenerationInfoStore) theEObject;
			T result = caseGenerationInfoStore(generationInfoStore);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GenerationStrategiesPackage.RULE_INFO_STORE: {
			RuleInfoStore ruleInfoStore = (RuleInfoStore) theEObject;
			T result = caseRuleInfoStore(ruleInfoStore);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GenerationStrategiesPackage.SYNCHRONIZATION_STRATEGY: {
			SynchronizationStrategy synchronizationStrategy = (SynchronizationStrategy) theEObject;
			T result = caseSynchronizationStrategy(synchronizationStrategy);
			if (result == null)
				result = caseGenerationStrategy(synchronizationStrategy);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generation Strategy</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generation Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenerationStrategy(GenerationStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Transformation Generation Strategy</em>'.
	 * <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will
	 * terminate the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Transformation Generation Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleTransformationGenerationStrategy(
			SimpleTransformationGenerationStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Traceability Link Store</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Traceability Link Store</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTraceabilityLinkStore(TraceabilityLinkStore object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Traceability Link</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Traceability Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTraceabilityLink(TraceabilityLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Conflict Check Transformation Generation Strategy</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null
	 * result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Conflict Check Transformation Generation Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConflictCheckTransformationGenerationStrategy(
			ConflictCheckTransformationGenerationStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Story Diagram Based Generation Strategy</em>'.
	 * <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will
	 * terminate the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Story Diagram Based Generation Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStoryDiagramBasedGenerationStrategy(
			StoryDiagramBasedGenerationStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generation Info Store</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generation Info Store</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenerationInfoStore(GenerationInfoStore object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Info Store</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Info Store</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleInfoStore(RuleInfoStore object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Synchronization Strategy</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Synchronization Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSynchronizationStrategy(SynchronizationStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch, but this is
	 * the last case anyway. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} // GenerationStrategiesSwitch
