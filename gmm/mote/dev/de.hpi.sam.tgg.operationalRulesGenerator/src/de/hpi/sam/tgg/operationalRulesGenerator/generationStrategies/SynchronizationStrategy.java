/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Synchronization Strategy</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getSynchronizationStrategy()
 * @model
 * @generated
 */
public interface SynchronizationStrategy extends GenerationStrategy {
} // SynchronizationStrategy
