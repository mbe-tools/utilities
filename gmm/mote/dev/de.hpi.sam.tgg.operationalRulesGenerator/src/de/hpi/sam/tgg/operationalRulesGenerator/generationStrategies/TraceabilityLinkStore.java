/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Traceability Link Store</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore#getTraceabilityLinks <em>Traceability Links</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore#getMappings <em>Mappings</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getTraceabilityLinkStore()
 * @model
 * @generated
 */
public interface TraceabilityLinkStore extends EObject {
	/**
	 * Returns the value of the '<em><b>Traceability Links</b></em>' containment reference list.
	 * The list contents are of type {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Traceability Links</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Traceability Links</em>' containment reference list.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getTraceabilityLinkStore_TraceabilityLinks()
	 * @model containment="true"
	 * @generated
	 */
	EList<TraceabilityLink> getTraceabilityLinks();

	/**
	 * Returns the value of the '<em><b>Mappings</b></em>' map. The key is of
	 * type {@link KeyType}, and the value is of type {@link ValueType}, <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mappings</em>' map isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Mappings</em>' map.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getTraceabilityLinkStore_Mappings()
	 * @model mapType="de.mdelab.workflow.helpers.MapEntry<KeyType, ValueType>"
	 * @generated
	 */
	EMap<Object, Object> getMappings();

} // TraceabilityLinkStore
