/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage;
import de.mdelab.workflow.NamedComponent;
import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides
 * an adapter <code>createXXX</code> method for each class of the model. <!--
 * end-user-doc -->
 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage
 * @generated
 */
public class WorkflowComponentsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static WorkflowComponentsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public WorkflowComponentsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = WorkflowComponentsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc --> This implementation returns <code>true</code> if
	 * the object is either the model's package or is an instance object of the
	 * model. <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected WorkflowComponentsSwitch<Adapter> modelSwitch = new WorkflowComponentsSwitch<Adapter>() {
		@Override
		public Adapter caseManifestLoaderComponent(
				ManifestLoaderComponent object) {
			return createManifestLoaderComponentAdapter();
		}

		@Override
		public Adapter caseGenerationStrategyComponent(
				GenerationStrategyComponent object) {
			return createGenerationStrategyComponentAdapter();
		}

		@Override
		public Adapter caseEMFCodeGenerationComponent(
				EMFCodeGenerationComponent object) {
			return createEMFCodeGenerationComponentAdapter();
		}

		@Override
		public Adapter caseManifestGeneratorComponent(
				ManifestGeneratorComponent object) {
			return createManifestGeneratorComponentAdapter();
		}

		@Override
		public Adapter caseNamedComponent(NamedComponent object) {
			return createNamedComponentAdapter();
		}

		@Override
		public Adapter caseWorkflowComponent(WorkflowComponent object) {
			return createWorkflowComponentAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent <em>Manifest Loader Component</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent
	 * @generated
	 */
	public Adapter createManifestLoaderComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent <em>Generation Strategy Component</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent
	 * @generated
	 */
	public Adapter createGenerationStrategyComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent <em>EMF Code Generation Component</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent
	 * @generated
	 */
	public Adapter createEMFCodeGenerationComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent <em>Manifest Generator Component</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent
	 * @generated
	 */
	public Adapter createManifestGeneratorComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link de.mdelab.workflow.NamedComponent <em>Named Component</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we
	 * can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see de.mdelab.workflow.NamedComponent
	 * @generated
	 */
	public Adapter createNamedComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link de.mdelab.workflow.WorkflowComponent <em>Component</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we
	 * can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see de.mdelab.workflow.WorkflowComponent
	 * @generated
	 */
	public Adapter createWorkflowComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} // WorkflowComponentsAdapterFactory
