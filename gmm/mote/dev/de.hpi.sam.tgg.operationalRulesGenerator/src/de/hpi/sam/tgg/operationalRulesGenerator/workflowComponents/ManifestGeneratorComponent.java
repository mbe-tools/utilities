/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Manifest Generator Component</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getManifestSlot <em>Manifest Slot</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getProjectName <em>Project Name</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getTggFileURI <em>Tgg File URI</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getCorrespondenceMetamodelURI <em>Correspondence Metamodel URI</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getManifestGeneratorComponent()
 * @model
 * @generated
 */
public interface ManifestGeneratorComponent extends WorkflowComponent {
	/**
	 * Returns the value of the '<em><b>Manifest Slot</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Manifest Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Manifest Slot</em>' attribute.
	 * @see #setManifestSlot(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getManifestGeneratorComponent_ManifestSlot()
	 * @model required="true"
	 * @generated
	 */
	String getManifestSlot();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getManifestSlot <em>Manifest Slot</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Manifest Slot</em>' attribute.
	 * @see #getManifestSlot()
	 * @generated
	 */
	void setManifestSlot(String value);

	/**
	 * Returns the value of the '<em><b>Project Name</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Project Name</em>' attribute.
	 * @see #setProjectName(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getManifestGeneratorComponent_ProjectName()
	 * @model required="true"
	 * @generated
	 */
	String getProjectName();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getProjectName <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Project Name</em>' attribute.
	 * @see #getProjectName()
	 * @generated
	 */
	void setProjectName(String value);

	/**
	 * Returns the value of the '<em><b>Tgg File URI</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgg File URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Tgg File URI</em>' attribute.
	 * @see #setTggFileURI(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getManifestGeneratorComponent_TggFileURI()
	 * @model required="true"
	 * @generated
	 */
	String getTggFileURI();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getTggFileURI <em>Tgg File URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Tgg File URI</em>' attribute.
	 * @see #getTggFileURI()
	 * @generated
	 */
	void setTggFileURI(String value);

	/**
	 * Returns the value of the '<em><b>Correspondence Metamodel URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correspondence Metamodel URI</em>' attribute
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correspondence Metamodel URI</em>' attribute.
	 * @see #setCorrespondenceMetamodelURI(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getManifestGeneratorComponent_CorrespondenceMetamodelURI()
	 * @model required="true"
	 * @generated
	 */
	String getCorrespondenceMetamodelURI();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getCorrespondenceMetamodelURI <em>Correspondence Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Correspondence Metamodel URI</em>' attribute.
	 * @see #getCorrespondenceMetamodelURI()
	 * @generated
	 */
	void setCorrespondenceMetamodelURI(String value);

} // ManifestGeneratorComponent
