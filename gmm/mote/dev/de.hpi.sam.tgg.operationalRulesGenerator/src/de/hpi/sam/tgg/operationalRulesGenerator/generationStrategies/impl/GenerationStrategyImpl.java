/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategy;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Generation Strategy</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class GenerationStrategyImpl extends EObjectImpl implements
		GenerationStrategy {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected GenerationStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenerationStrategiesPackage.Literals.GENERATION_STRATEGY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void generateRules(String projectURI, String javaBasePackage,
			EPackage rulesPackage, TGGDiagram tggDiagram, EList<TGGRule> rules)
			throws RuleGenerationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

} // GenerationStrategyImpl
