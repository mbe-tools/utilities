/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage
 * @generated
 */
public interface WorkflowComponentsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	WorkflowComponentsFactory eINSTANCE = de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.WorkflowComponentsFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>Manifest Loader Component</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Manifest Loader Component</em>'.
	 * @generated
	 */
	ManifestLoaderComponent createManifestLoaderComponent();

	/**
	 * Returns a new object of class '<em>Generation Strategy Component</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return a new object of class '<em>Generation Strategy Component</em>'.
	 * @generated
	 */
	GenerationStrategyComponent createGenerationStrategyComponent();

	/**
	 * Returns a new object of class '<em>EMF Code Generation Component</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return a new object of class '<em>EMF Code Generation Component</em>'.
	 * @generated
	 */
	EMFCodeGenerationComponent createEMFCodeGenerationComponent();

	/**
	 * Returns a new object of class '<em>Manifest Generator Component</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return a new object of class '<em>Manifest Generator Component</em>'.
	 * @generated
	 */
	ManifestGeneratorComponent createManifestGeneratorComponent();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WorkflowComponentsPackage getWorkflowComponentsPackage();

} // WorkflowComponentsFactory
