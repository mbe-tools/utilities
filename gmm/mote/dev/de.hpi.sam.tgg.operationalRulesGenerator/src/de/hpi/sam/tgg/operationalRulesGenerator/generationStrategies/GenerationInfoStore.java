/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.tgg.TGGDiagram;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Generation Info Store</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore#getRuleInfos <em>Rule Infos</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore#getTggDiagram <em>Tgg Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore#getRuleSetEClass <em>Rule Set EClass</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getGenerationInfoStore()
 * @model
 * @generated
 */
public interface GenerationInfoStore extends EObject {
	/**
	 * Returns the value of the '<em><b>Rule Infos</b></em>' containment reference list.
	 * The list contents are of type {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Infos</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Infos</em>' containment reference list.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getGenerationInfoStore_RuleInfos()
	 * @model containment="true"
	 * @generated
	 */
	EList<RuleInfoStore> getRuleInfos();

	/**
	 * Returns the value of the '<em><b>Tgg Diagram</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgg Diagram</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Tgg Diagram</em>' reference.
	 * @see #setTggDiagram(TGGDiagram)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getGenerationInfoStore_TggDiagram()
	 * @model
	 * @generated
	 */
	TGGDiagram getTggDiagram();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore#getTggDiagram <em>Tgg Diagram</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Tgg Diagram</em>' reference.
	 * @see #getTggDiagram()
	 * @generated
	 */
	void setTggDiagram(TGGDiagram value);

	/**
	 * Returns the value of the '<em><b>Rule Set EClass</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Set EClass</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Set EClass</em>' reference.
	 * @see #setRuleSetEClass(EClass)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getGenerationInfoStore_RuleSetEClass()
	 * @model
	 * @generated
	 */
	EClass getRuleSetEClass();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore#getRuleSetEClass <em>Rule Set EClass</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Rule Set EClass</em>' reference.
	 * @see #getRuleSetEClass()
	 * @generated
	 */
	void setRuleSetEClass(EClass value);

} // GenerationInfoStore
