/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.ConflictCheckTransformationGenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SimpleTransformationGenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.StoryDiagramBasedGenerationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SynchronizationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides
 * an adapter <code>createXXX</code> method for each class of the model. <!--
 * end-user-doc -->
 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage
 * @generated
 */
public class GenerationStrategiesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static GenerationStrategiesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public GenerationStrategiesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = GenerationStrategiesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc --> This implementation returns <code>true</code> if
	 * the object is either the model's package or is an instance object of the
	 * model. <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected GenerationStrategiesSwitch<Adapter> modelSwitch = new GenerationStrategiesSwitch<Adapter>() {
		@Override
		public Adapter caseGenerationStrategy(GenerationStrategy object) {
			return createGenerationStrategyAdapter();
		}

		@Override
		public Adapter caseSimpleTransformationGenerationStrategy(
				SimpleTransformationGenerationStrategy object) {
			return createSimpleTransformationGenerationStrategyAdapter();
		}

		@Override
		public Adapter caseTraceabilityLinkStore(TraceabilityLinkStore object) {
			return createTraceabilityLinkStoreAdapter();
		}

		@Override
		public Adapter caseTraceabilityLink(TraceabilityLink object) {
			return createTraceabilityLinkAdapter();
		}

		@Override
		public Adapter caseConflictCheckTransformationGenerationStrategy(
				ConflictCheckTransformationGenerationStrategy object) {
			return createConflictCheckTransformationGenerationStrategyAdapter();
		}

		@Override
		public Adapter caseStoryDiagramBasedGenerationStrategy(
				StoryDiagramBasedGenerationStrategy object) {
			return createStoryDiagramBasedGenerationStrategyAdapter();
		}

		@Override
		public Adapter caseGenerationInfoStore(GenerationInfoStore object) {
			return createGenerationInfoStoreAdapter();
		}

		@Override
		public Adapter caseRuleInfoStore(RuleInfoStore object) {
			return createRuleInfoStoreAdapter();
		}

		@Override
		public Adapter caseSynchronizationStrategy(
				SynchronizationStrategy object) {
			return createSynchronizationStrategyAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategy <em>Generation Strategy</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategy
	 * @generated
	 */
	public Adapter createGenerationStrategyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SimpleTransformationGenerationStrategy <em>Simple Transformation Generation Strategy</em>}'.
	 * <!-- begin-user-doc
	 * --> This default implementation returns null so that we can easily ignore
	 * cases; it's useful to ignore a case when inheritance will catch all the
	 * cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SimpleTransformationGenerationStrategy
	 * @generated
	 */
	public Adapter createSimpleTransformationGenerationStrategyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore <em>Traceability Link Store</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore
	 * @generated
	 */
	public Adapter createTraceabilityLinkStoreAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink <em>Traceability Link</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink
	 * @generated
	 */
	public Adapter createTraceabilityLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.ConflictCheckTransformationGenerationStrategy
	 * <em>Conflict Check Transformation Generation Strategy</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we
	 * can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.ConflictCheckTransformationGenerationStrategy
	 * @generated
	 */
	public Adapter createConflictCheckTransformationGenerationStrategyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.StoryDiagramBasedGenerationStrategy <em>Story Diagram Based Generation Strategy</em>}'.
	 * <!-- begin-user-doc
	 * --> This default implementation returns null so that we can easily ignore
	 * cases; it's useful to ignore a case when inheritance will catch all the
	 * cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.StoryDiagramBasedGenerationStrategy
	 * @generated
	 */
	public Adapter createStoryDiagramBasedGenerationStrategyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore <em>Generation Info Store</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore
	 * @generated
	 */
	public Adapter createGenerationInfoStoreAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore <em>Rule Info Store</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore
	 * @generated
	 */
	public Adapter createRuleInfoStoreAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SynchronizationStrategy <em>Synchronization Strategy</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SynchronizationStrategy
	 * @generated
	 */
	public Adapter createSynchronizationStrategyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} // GenerationStrategiesAdapterFactory
