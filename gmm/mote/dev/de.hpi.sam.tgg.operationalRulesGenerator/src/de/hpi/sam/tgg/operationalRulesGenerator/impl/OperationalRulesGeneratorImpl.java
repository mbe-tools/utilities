/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGenerator;
import de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorPackage;
import de.mdelab.workflow.Workflow;
import de.mdelab.workflow.impl.WorkflowExecutionException;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Operational Rules Generator</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class OperationalRulesGeneratorImpl extends EObjectImpl implements
		OperationalRulesGenerator {
	private static final String PROJECT_NAME = "projectName";
	private static final String TGG_FILE_URI = "tggFileURI";
	private static final String GENERATOR_WORKFLOW_FILE_URI = "/de.hpi.sam.tgg.operationalRulesGenerator/generatorResources/generator.workflow";

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationalRulesGeneratorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperationalRulesGeneratorPackage.Literals.OPERATIONAL_RULES_GENERATOR;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	public void generate(String tggFileURI, OutputStream outputStream) {
		/*
		 * Open the workflow file
		 */
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource workflowResource = resourceSet.getResource(
				URI.createPlatformPluginURI(GENERATOR_WORKFLOW_FILE_URI, true),
				true);

		Workflow workflow = (Workflow) workflowResource.getContents().get(0);

		/*
		 * Execute the workflow
		 */
		URI tggURI = URI.createURI(tggFileURI);

		Map<String, String> propertyValues = new HashMap<String, String>();
		propertyValues.put(PROJECT_NAME, tggURI.segments()[1]);
		propertyValues.put(TGG_FILE_URI, tggFileURI);

		try {
			workflow.execute(new NullProgressMonitor(), outputStream,
					propertyValues);
		} catch (WorkflowExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

} // OperationalRulesGeneratorImpl
