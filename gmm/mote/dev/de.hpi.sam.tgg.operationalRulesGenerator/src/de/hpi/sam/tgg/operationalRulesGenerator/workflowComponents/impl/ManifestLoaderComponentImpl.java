/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl;

import java.io.IOException;
import java.util.jar.Manifest;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent;
import de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Manifest Loader Component</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestLoaderComponentImpl#getModelSlot <em>Model Slot</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestLoaderComponentImpl#getProjectName <em>Project Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ManifestLoaderComponentImpl extends WorkflowComponentImpl
		implements ManifestLoaderComponent {
	/**
	 * The default value of the '{@link #getModelSlot() <em>Model Slot</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_SLOT_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getModelSlot() <em>Model Slot</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String modelSlot = MODEL_SLOT_EDEFAULT;
	/**
	 * The default value of the '{@link #getProjectName() <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getProjectName()
	 * @generated
	 * @ordered
	 */
	protected static final String PROJECT_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getProjectName() <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getProjectName()
	 * @generated
	 * @ordered
	 */
	protected String projectName = PROJECT_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ManifestLoaderComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowComponentsPackage.Literals.MANIFEST_LOADER_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getModelSlot() {
		return modelSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelSlot(String newModelSlot) {
		String oldModelSlot = modelSlot;
		modelSlot = newModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.MANIFEST_LOADER_COMPONENT__MODEL_SLOT,
					oldModelSlot, modelSlot));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setProjectName(String newProjectName) {
		String oldProjectName = projectName;
		projectName = newProjectName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.MANIFEST_LOADER_COMPONENT__PROJECT_NAME,
					oldProjectName, projectName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case WorkflowComponentsPackage.MANIFEST_LOADER_COMPONENT__MODEL_SLOT:
			return getModelSlot();
		case WorkflowComponentsPackage.MANIFEST_LOADER_COMPONENT__PROJECT_NAME:
			return getProjectName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case WorkflowComponentsPackage.MANIFEST_LOADER_COMPONENT__MODEL_SLOT:
			setModelSlot((String) newValue);
			return;
		case WorkflowComponentsPackage.MANIFEST_LOADER_COMPONENT__PROJECT_NAME:
			setProjectName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case WorkflowComponentsPackage.MANIFEST_LOADER_COMPONENT__MODEL_SLOT:
			setModelSlot(MODEL_SLOT_EDEFAULT);
			return;
		case WorkflowComponentsPackage.MANIFEST_LOADER_COMPONENT__PROJECT_NAME:
			setProjectName(PROJECT_NAME_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case WorkflowComponentsPackage.MANIFEST_LOADER_COMPONENT__MODEL_SLOT:
			return MODEL_SLOT_EDEFAULT == null ? modelSlot != null
					: !MODEL_SLOT_EDEFAULT.equals(modelSlot);
		case WorkflowComponentsPackage.MANIFEST_LOADER_COMPONENT__PROJECT_NAME:
			return PROJECT_NAME_EDEFAULT == null ? projectName != null
					: !PROJECT_NAME_EDEFAULT.equals(projectName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (modelSlot: ");
		result.append(modelSlot);
		result.append(", projectName: ");
		result.append(projectName);
		result.append(')');
		return result.toString();
	}

	@Override
	public void execute(WorkflowExecutionContext context)
			throws WorkflowExecutionException, IOException {
		final String MANIFEST_PATH = "META-INF/MANIFEST.MF";

		/*
		 * Get the project from the workspace
		 */
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IProject project = root.getProject(getProjectName());

		if (project == null) {
			context.getLogger().addError(
					"Project '" + getProjectName() + "' does not exist.", null,
					this);

			throw new WorkflowExecutionException("Project '" + getProjectName()
					+ "' does not exist.");
		}

		try {
			context.getLogger().addInfo(
					"Loading manifest '" + getProjectName() + "/"
							+ MANIFEST_PATH + "'...", this);

			/*
			 * Create a manifest object
			 */
			Manifest mf = new Manifest(project.getFile(MANIFEST_PATH)
					.getContents());

			/*
			 * Put manifest into model slot
			 */
			context.getModelSlots().put(getModelSlot(), mf);
		} catch (org.eclipse.core.runtime.CoreException e) {
			context.getLogger().addError(
					"Could not open '" + getProjectName() + "/" + MANIFEST_PATH
							+ "'.", e, this);

			throw new WorkflowExecutionException("Could not open '"
					+ getProjectName() + "/" + MANIFEST_PATH + "'.", e);
		} catch (java.io.IOException e) {
			context.getLogger().addError(
					"Could not open '" + getProjectName() + "/" + MANIFEST_PATH
							+ "'.", e, this);

			throw new WorkflowExecutionException("Could not open '"
					+ getProjectName() + "/" + MANIFEST_PATH + "'.", e);
		}
	}

	@Override
	public boolean checkConfiguration(WorkflowExecutionContext context)
			throws IOException {
		boolean success = true;

		if (getModelSlot() == null || "".equals(getModelSlot())) {
			context.getLogger().addError("model slot is not set.", null, this);
			success = false;
		}

		if (getProjectName() == null || "".equals(getProjectName())) {
			context.getLogger()
					.addError("project name is not set.", null, this);
			success = false;
		}

		return success;
	}
} // ManifestLoaderComponentImpl
