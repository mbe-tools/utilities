/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;

import de.hpi.sam.storyDiagramEcore.ActivityDiagram;
import de.hpi.sam.tgg.TGGRule;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Rule Info Store</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getTggRule <em>Tgg Rule</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getRuleEClass <em>Rule EClass</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardTransformationActivityDiagram <em>Forward Transformation Activity Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingTransformationActivityDiagram <em>Mapping Transformation Activity Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseTransformationActivityDiagram <em>Reverse Transformation Activity Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardSynchronisationActivityDiagram <em>Forward Synchronisation Activity Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingSynchronisationActivityDiagram <em>Mapping Synchronisation Activity Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseSynchronizationActivityDiagram <em>Reverse Synchronization Activity Diagram</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getHelperActivityDiagrams <em>Helper Activity Diagrams</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardTransformationOperation <em>Forward Transformation Operation</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingTransformationOperation <em>Mapping Transformation Operation</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseTransformationOperation <em>Reverse Transformation Operation</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardSynchronizationOperation <em>Forward Synchronization Operation</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingSynchronizationOperation <em>Mapping Synchronization Operation</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseSynchronizationOperation <em>Reverse Synchronization Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore()
 * @model
 * @generated
 */
public interface RuleInfoStore extends EObject {
	/**
	 * Returns the value of the '<em><b>Tgg Rule</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgg Rule</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Tgg Rule</em>' reference.
	 * @see #setTggRule(TGGRule)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_TggRule()
	 * @model
	 * @generated
	 */
	TGGRule getTggRule();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getTggRule
	 * <em>Tgg Rule</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Tgg Rule</em>' reference.
	 * @see #getTggRule()
	 * @generated
	 */
	void setTggRule(TGGRule value);

	/**
	 * Returns the value of the '<em><b>Rule EClass</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule EClass</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Rule EClass</em>' reference.
	 * @see #setRuleEClass(EClass)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_RuleEClass()
	 * @model
	 * @generated
	 */
	EClass getRuleEClass();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getRuleEClass <em>Rule EClass</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Rule EClass</em>' reference.
	 * @see #getRuleEClass()
	 * @generated
	 */
	void setRuleEClass(EClass value);

	/**
	 * Returns the value of the '
	 * <em><b>Forward Transformation Activity Diagram</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Forward Transformation Activity Diagram</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '
	 *         <em>Forward Transformation Activity Diagram</em>' reference.
	 * @see #setForwardTransformationActivityDiagram(ActivityDiagram)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_ForwardTransformationActivityDiagram()
	 * @model
	 * @generated
	 */
	ActivityDiagram getForwardTransformationActivityDiagram();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardTransformationActivityDiagram
	 * <em>Forward Transformation Activity Diagram</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '
	 *            <em>Forward Transformation Activity Diagram</em>' reference.
	 * @see #getForwardTransformationActivityDiagram()
	 * @generated
	 */
	void setForwardTransformationActivityDiagram(ActivityDiagram value);

	/**
	 * Returns the value of the '
	 * <em><b>Mapping Transformation Activity Diagram</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapping Transformation Activity Diagram</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '
	 *         <em>Mapping Transformation Activity Diagram</em>' reference.
	 * @see #setMappingTransformationActivityDiagram(ActivityDiagram)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_MappingTransformationActivityDiagram()
	 * @model
	 * @generated
	 */
	ActivityDiagram getMappingTransformationActivityDiagram();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingTransformationActivityDiagram
	 * <em>Mapping Transformation Activity Diagram</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '
	 *            <em>Mapping Transformation Activity Diagram</em>' reference.
	 * @see #getMappingTransformationActivityDiagram()
	 * @generated
	 */
	void setMappingTransformationActivityDiagram(ActivityDiagram value);

	/**
	 * Returns the value of the '
	 * <em><b>Reverse Transformation Activity Diagram</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reverse Transformation Activity Diagram</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '
	 *         <em>Reverse Transformation Activity Diagram</em>' reference.
	 * @see #setReverseTransformationActivityDiagram(ActivityDiagram)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_ReverseTransformationActivityDiagram()
	 * @model
	 * @generated
	 */
	ActivityDiagram getReverseTransformationActivityDiagram();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseTransformationActivityDiagram
	 * <em>Reverse Transformation Activity Diagram</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '
	 *            <em>Reverse Transformation Activity Diagram</em>' reference.
	 * @see #getReverseTransformationActivityDiagram()
	 * @generated
	 */
	void setReverseTransformationActivityDiagram(ActivityDiagram value);

	/**
	 * Returns the value of the '
	 * <em><b>Forward Synchronisation Activity Diagram</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Forward Synchronisation Activity Diagram</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '
	 *         <em>Forward Synchronisation Activity Diagram</em>' reference.
	 * @see #setForwardSynchronisationActivityDiagram(ActivityDiagram)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_ForwardSynchronisationActivityDiagram()
	 * @model
	 * @generated
	 */
	ActivityDiagram getForwardSynchronisationActivityDiagram();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardSynchronisationActivityDiagram
	 * <em>Forward Synchronisation Activity Diagram</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '
	 *            <em>Forward Synchronisation Activity Diagram</em>' reference.
	 * @see #getForwardSynchronisationActivityDiagram()
	 * @generated
	 */
	void setForwardSynchronisationActivityDiagram(ActivityDiagram value);

	/**
	 * Returns the value of the '
	 * <em><b>Mapping Synchronisation Activity Diagram</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapping Synchronisation Activity Diagram</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '
	 *         <em>Mapping Synchronisation Activity Diagram</em>' reference.
	 * @see #setMappingSynchronisationActivityDiagram(ActivityDiagram)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_MappingSynchronisationActivityDiagram()
	 * @model
	 * @generated
	 */
	ActivityDiagram getMappingSynchronisationActivityDiagram();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingSynchronisationActivityDiagram
	 * <em>Mapping Synchronisation Activity Diagram</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '
	 *            <em>Mapping Synchronisation Activity Diagram</em>' reference.
	 * @see #getMappingSynchronisationActivityDiagram()
	 * @generated
	 */
	void setMappingSynchronisationActivityDiagram(ActivityDiagram value);

	/**
	 * Returns the value of the '
	 * <em><b>Reverse Synchronization Activity Diagram</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reverse Synchronization Activity Diagram</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '
	 *         <em>Reverse Synchronization Activity Diagram</em>' reference.
	 * @see #setReverseSynchronizationActivityDiagram(ActivityDiagram)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_ReverseSynchronizationActivityDiagram()
	 * @model
	 * @generated
	 */
	ActivityDiagram getReverseSynchronizationActivityDiagram();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseSynchronizationActivityDiagram
	 * <em>Reverse Synchronization Activity Diagram</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '
	 *            <em>Reverse Synchronization Activity Diagram</em>' reference.
	 * @see #getReverseSynchronizationActivityDiagram()
	 * @generated
	 */
	void setReverseSynchronizationActivityDiagram(ActivityDiagram value);

	/**
	 * Returns the value of the '<em><b>Helper Activity Diagrams</b></em>' map.
	 * The key is of type {@link KeyType},
	 * and the value is of type {@link ValueType},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Helper Activity Diagrams</em>' reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Helper Activity Diagrams</em>' map.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_HelperActivityDiagrams()
	 * @model mapType="de.hpi.sam.storyDiagramEcore.helpers.MapEntry<KeyType, ValueType>"
	 * @generated
	 */
	EMap<String, ActivityDiagram> getHelperActivityDiagrams();

	/**
	 * Returns the value of the '
	 * <em><b>Forward Transformation Operation</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Forward Transformation Operation</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Forward Transformation Operation</em>'
	 *         reference.
	 * @see #setForwardTransformationOperation(EOperation)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_ForwardTransformationOperation()
	 * @model
	 * @generated
	 */
	EOperation getForwardTransformationOperation();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardTransformationOperation
	 * <em>Forward Transformation Operation</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '
	 *            <em>Forward Transformation Operation</em>' reference.
	 * @see #getForwardTransformationOperation()
	 * @generated
	 */
	void setForwardTransformationOperation(EOperation value);

	/**
	 * Returns the value of the '
	 * <em><b>Mapping Transformation Operation</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapping Transformation Operation</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Mapping Transformation Operation</em>'
	 *         reference.
	 * @see #setMappingTransformationOperation(EOperation)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_MappingTransformationOperation()
	 * @model
	 * @generated
	 */
	EOperation getMappingTransformationOperation();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingTransformationOperation
	 * <em>Mapping Transformation Operation</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '
	 *            <em>Mapping Transformation Operation</em>' reference.
	 * @see #getMappingTransformationOperation()
	 * @generated
	 */
	void setMappingTransformationOperation(EOperation value);

	/**
	 * Returns the value of the '
	 * <em><b>Reverse Transformation Operation</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reverse Transformation Operation</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Reverse Transformation Operation</em>'
	 *         reference.
	 * @see #setReverseTransformationOperation(EOperation)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_ReverseTransformationOperation()
	 * @model
	 * @generated
	 */
	EOperation getReverseTransformationOperation();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseTransformationOperation
	 * <em>Reverse Transformation Operation</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '
	 *            <em>Reverse Transformation Operation</em>' reference.
	 * @see #getReverseTransformationOperation()
	 * @generated
	 */
	void setReverseTransformationOperation(EOperation value);

	/**
	 * Returns the value of the '
	 * <em><b>Forward Synchronization Operation</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Forward Synchronization Operation</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Forward Synchronization Operation</em>'
	 *         reference.
	 * @see #setForwardSynchronizationOperation(EOperation)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_ForwardSynchronizationOperation()
	 * @model
	 * @generated
	 */
	EOperation getForwardSynchronizationOperation();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardSynchronizationOperation
	 * <em>Forward Synchronization Operation</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '
	 *            <em>Forward Synchronization Operation</em>' reference.
	 * @see #getForwardSynchronizationOperation()
	 * @generated
	 */
	void setForwardSynchronizationOperation(EOperation value);

	/**
	 * Returns the value of the '
	 * <em><b>Mapping Synchronization Operation</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapping Synchronization Operation</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Mapping Synchronization Operation</em>'
	 *         reference.
	 * @see #setMappingSynchronizationOperation(EOperation)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_MappingSynchronizationOperation()
	 * @model
	 * @generated
	 */
	EOperation getMappingSynchronizationOperation();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingSynchronizationOperation
	 * <em>Mapping Synchronization Operation</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '
	 *            <em>Mapping Synchronization Operation</em>' reference.
	 * @see #getMappingSynchronizationOperation()
	 * @generated
	 */
	void setMappingSynchronizationOperation(EOperation value);

	/**
	 * Returns the value of the '
	 * <em><b>Reverse Synchronization Operation</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reverse Synchronization Operation</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Reverse Synchronization Operation</em>'
	 *         reference.
	 * @see #setReverseSynchronizationOperation(EOperation)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage#getRuleInfoStore_ReverseSynchronizationOperation()
	 * @model
	 * @generated
	 */
	EOperation getReverseSynchronizationOperation();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseSynchronizationOperation
	 * <em>Reverse Synchronization Operation</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '
	 *            <em>Reverse Synchronization Operation</em>' reference.
	 * @see #getReverseSynchronizationOperation()
	 * @generated
	 */
	void setReverseSynchronizationOperation(EOperation value);

} // RuleInfoStore
