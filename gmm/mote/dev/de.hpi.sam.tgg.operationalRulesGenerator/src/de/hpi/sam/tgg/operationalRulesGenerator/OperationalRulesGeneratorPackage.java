/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGeneratorFactory
 * @model kind="package"
 * @generated
 */
public interface OperationalRulesGeneratorPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "operationalRulesGenerator";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://operationalRulesGenerator/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "operationalRulesGenerator";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	OperationalRulesGeneratorPackage eINSTANCE = de.hpi.sam.tgg.operationalRulesGenerator.impl.OperationalRulesGeneratorPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.impl.OperationalRulesGeneratorImpl <em>Operational Rules Generator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.impl.OperationalRulesGeneratorImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.impl.OperationalRulesGeneratorPackageImpl#getOperationalRulesGenerator()
	 * @generated
	 */
	int OPERATIONAL_RULES_GENERATOR = 0;

	/**
	 * The number of structural features of the '<em>Operational Rules Generator</em>' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONAL_RULES_GENERATOR_FEATURE_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGenerator <em>Operational Rules Generator</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Operational Rules Generator</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.OperationalRulesGenerator
	 * @generated
	 */
	EClass getOperationalRulesGenerator();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OperationalRulesGeneratorFactory getOperationalRulesGeneratorFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.impl.OperationalRulesGeneratorImpl <em>Operational Rules Generator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.impl.OperationalRulesGeneratorImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.impl.OperationalRulesGeneratorPackageImpl#getOperationalRulesGenerator()
		 * @generated
		 */
		EClass OPERATIONAL_RULES_GENERATOR = eINSTANCE
				.getOperationalRulesGenerator();

	}

} // OperationalRulesGeneratorPackage
