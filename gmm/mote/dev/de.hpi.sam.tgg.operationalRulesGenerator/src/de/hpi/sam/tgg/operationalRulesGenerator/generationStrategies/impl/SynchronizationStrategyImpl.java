/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesPackage;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SynchronizationStrategy;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.synchronization.OptimizedIncrRepairGenerationStrategy;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Synchronization Strategy</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SynchronizationStrategyImpl extends GenerationStrategyImpl
		implements SynchronizationStrategy {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected SynchronizationStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenerationStrategiesPackage.Literals.SYNCHRONIZATION_STRATEGY;
	}

	@Override
	public void generateRules(String projectURI, String javaBasePackage,
			EPackage rulesPackage, TGGDiagram tggDiagram, EList<TGGRule> rules)
			throws RuleGenerationException {
//		IncrementalRepairGenerationStrategy incrementalRepairGenerationStrategy = new IncrementalRepairGenerationStrategy();
		final OptimizedIncrRepairGenerationStrategy incrementalRepairGenerationStrategy = new OptimizedIncrRepairGenerationStrategy();

		incrementalRepairGenerationStrategy.createRules( projectURI, javaBasePackage, rulesPackage, tggDiagram, rules );
	}
} // SynchronizationStrategyImpl
