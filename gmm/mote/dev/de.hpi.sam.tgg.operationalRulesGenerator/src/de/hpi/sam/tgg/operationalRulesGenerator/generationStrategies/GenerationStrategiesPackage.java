/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategiesFactory
 * @model kind="package"
 * @generated
 */
public interface GenerationStrategiesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "generationStrategies";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://operationalRulesGenerator/generationStrategies/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "operationalRulesGenerator.generationStrategies";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	GenerationStrategiesPackage eINSTANCE = de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategyImpl <em>Generation Strategy</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategyImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getGenerationStrategy()
	 * @generated
	 */
	int GENERATION_STRATEGY = 0;

	/**
	 * The number of structural features of the '<em>Generation Strategy</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATION_STRATEGY_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.StoryDiagramBasedGenerationStrategyImpl
	 * <em>Story Diagram Based Generation Strategy</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.StoryDiagramBasedGenerationStrategyImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getStoryDiagramBasedGenerationStrategy()
	 * @generated
	 */
	int STORY_DIAGRAM_BASED_GENERATION_STRATEGY = 5;

	/**
	 * The number of structural features of the '
	 * <em>Story Diagram Based Generation Strategy</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_DIAGRAM_BASED_GENERATION_STRATEGY_FEATURE_COUNT = GENERATION_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.SimpleTransformationGenerationStrategyImpl
	 * <em>Simple Transformation Generation Strategy</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.SimpleTransformationGenerationStrategyImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getSimpleTransformationGenerationStrategy()
	 * @generated
	 */
	int SIMPLE_TRANSFORMATION_GENERATION_STRATEGY = 1;

	/**
	 * The number of structural features of the '
	 * <em>Simple Transformation Generation Strategy</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TRANSFORMATION_GENERATION_STRATEGY_FEATURE_COUNT = STORY_DIAGRAM_BASED_GENERATION_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.TraceabilityLinkStoreImpl <em>Traceability Link Store</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.TraceabilityLinkStoreImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getTraceabilityLinkStore()
	 * @generated
	 */
	int TRACEABILITY_LINK_STORE = 2;

	/**
	 * The feature id for the '<em><b>Traceability Links</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACEABILITY_LINK_STORE__TRACEABILITY_LINKS = 0;

	/**
	 * The feature id for the '<em><b>Mappings</b></em>' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TRACEABILITY_LINK_STORE__MAPPINGS = 1;

	/**
	 * The number of structural features of the '<em>Traceability Link Store</em>' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACEABILITY_LINK_STORE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.TraceabilityLinkImpl <em>Traceability Link</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.TraceabilityLinkImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getTraceabilityLink()
	 * @generated
	 */
	int TRACEABILITY_LINK = 3;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TRACEABILITY_LINK__SOURCES = 0;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TRACEABILITY_LINK__TARGETS = 1;

	/**
	 * The number of structural features of the '<em>Traceability Link</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACEABILITY_LINK_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.ConflictCheckTransformationGenerationStrategyImpl
	 * <em>Conflict Check Transformation Generation Strategy</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.ConflictCheckTransformationGenerationStrategyImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getConflictCheckTransformationGenerationStrategy()
	 * @generated
	 */
	int CONFLICT_CHECK_TRANSFORMATION_GENERATION_STRATEGY = 4;

	/**
	 * The number of structural features of the '
	 * <em>Conflict Check Transformation Generation Strategy</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CONFLICT_CHECK_TRANSFORMATION_GENERATION_STRATEGY_FEATURE_COUNT = GENERATION_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationInfoStoreImpl <em>Generation Info Store</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationInfoStoreImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getGenerationInfoStore()
	 * @generated
	 */
	int GENERATION_INFO_STORE = 6;

	/**
	 * The feature id for the '<em><b>Rule Infos</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATION_INFO_STORE__RULE_INFOS = 0;

	/**
	 * The feature id for the '<em><b>Tgg Diagram</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GENERATION_INFO_STORE__TGG_DIAGRAM = 1;

	/**
	 * The feature id for the '<em><b>Rule Set EClass</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GENERATION_INFO_STORE__RULE_SET_ECLASS = 2;

	/**
	 * The number of structural features of the '<em>Generation Info Store</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATION_INFO_STORE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl <em>Rule Info Store</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getRuleInfoStore()
	 * @generated
	 */
	int RULE_INFO_STORE = 7;

	/**
	 * The feature id for the '<em><b>Tgg Rule</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__TGG_RULE = 0;

	/**
	 * The feature id for the '<em><b>Rule EClass</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__RULE_ECLASS = 1;

	/**
	 * The feature id for the '
	 * <em><b>Forward Transformation Activity Diagram</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__FORWARD_TRANSFORMATION_ACTIVITY_DIAGRAM = 2;

	/**
	 * The feature id for the '
	 * <em><b>Mapping Transformation Activity Diagram</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__MAPPING_TRANSFORMATION_ACTIVITY_DIAGRAM = 3;

	/**
	 * The feature id for the '
	 * <em><b>Reverse Transformation Activity Diagram</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__REVERSE_TRANSFORMATION_ACTIVITY_DIAGRAM = 4;

	/**
	 * The feature id for the '
	 * <em><b>Forward Synchronisation Activity Diagram</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__FORWARD_SYNCHRONISATION_ACTIVITY_DIAGRAM = 5;

	/**
	 * The feature id for the '
	 * <em><b>Mapping Synchronisation Activity Diagram</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__MAPPING_SYNCHRONISATION_ACTIVITY_DIAGRAM = 6;

	/**
	 * The feature id for the '
	 * <em><b>Reverse Synchronization Activity Diagram</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_ACTIVITY_DIAGRAM = 7;

	/**
	 * The feature id for the '<em><b>Helper Activity Diagrams</b></em>' map.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__HELPER_ACTIVITY_DIAGRAMS = 8;

	/**
	 * The feature id for the '<em><b>Forward Transformation Operation</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__FORWARD_TRANSFORMATION_OPERATION = 9;

	/**
	 * The feature id for the '<em><b>Mapping Transformation Operation</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__MAPPING_TRANSFORMATION_OPERATION = 10;

	/**
	 * The feature id for the '<em><b>Reverse Transformation Operation</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__REVERSE_TRANSFORMATION_OPERATION = 11;

	/**
	 * The feature id for the '<em><b>Forward Synchronization Operation</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__FORWARD_SYNCHRONIZATION_OPERATION = 12;

	/**
	 * The feature id for the '<em><b>Mapping Synchronization Operation</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__MAPPING_SYNCHRONIZATION_OPERATION = 13;

	/**
	 * The feature id for the '<em><b>Reverse Synchronization Operation</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_OPERATION = 14;

	/**
	 * The number of structural features of the '<em>Rule Info Store</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_INFO_STORE_FEATURE_COUNT = 15;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.SynchronizationStrategyImpl <em>Synchronization Strategy</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.SynchronizationStrategyImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getSynchronizationStrategy()
	 * @generated
	 */
	int SYNCHRONIZATION_STRATEGY = 8;

	/**
	 * The number of structural features of the '<em>Synchronization Strategy</em>' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION_STRATEGY_FEATURE_COUNT = GENERATION_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '<em>Rule Generation Exception</em>' data type.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleGenerationException
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getRuleGenerationException()
	 * @generated
	 */
	int RULE_GENERATION_EXCEPTION = 9;

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategy
	 * <em>Generation Strategy</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Generation Strategy</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationStrategy
	 * @generated
	 */
	EClass getGenerationStrategy();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SimpleTransformationGenerationStrategy <em>Simple Transformation Generation Strategy</em>}'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Transformation Generation Strategy</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SimpleTransformationGenerationStrategy
	 * @generated
	 */
	EClass getSimpleTransformationGenerationStrategy();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore <em>Traceability Link Store</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Traceability Link Store</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore
	 * @generated
	 */
	EClass getTraceabilityLinkStore();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore#getTraceabilityLinks
	 * <em>Traceability Links</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the containment reference list '
	 *         <em>Traceability Links</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore#getTraceabilityLinks()
	 * @see #getTraceabilityLinkStore()
	 * @generated
	 */
	EReference getTraceabilityLinkStore_TraceabilityLinks();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore#getMappings <em>Mappings</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Mappings</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLinkStore#getMappings()
	 * @see #getTraceabilityLinkStore()
	 * @generated
	 */
	EReference getTraceabilityLinkStore_Mappings();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink
	 * <em>Traceability Link</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Traceability Link</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink
	 * @generated
	 */
	EClass getTraceabilityLink();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink#getSources <em>Sources</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sources</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink#getSources()
	 * @see #getTraceabilityLink()
	 * @generated
	 */
	EReference getTraceabilityLink_Sources();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink#getTargets <em>Targets</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Targets</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.TraceabilityLink#getTargets()
	 * @see #getTraceabilityLink()
	 * @generated
	 */
	EReference getTraceabilityLink_Targets();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.ConflictCheckTransformationGenerationStrategy
	 * <em>Conflict Check Transformation Generation Strategy</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '
	 *         <em>Conflict Check Transformation Generation Strategy</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.ConflictCheckTransformationGenerationStrategy
	 * @generated
	 */
	EClass getConflictCheckTransformationGenerationStrategy();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.StoryDiagramBasedGenerationStrategy <em>Story Diagram Based Generation Strategy</em>}'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Story Diagram Based Generation Strategy</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.StoryDiagramBasedGenerationStrategy
	 * @generated
	 */
	EClass getStoryDiagramBasedGenerationStrategy();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore <em>Generation Info Store</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Generation Info Store</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore
	 * @generated
	 */
	EClass getGenerationInfoStore();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore#getRuleInfos <em>Rule Infos</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rule Infos</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore#getRuleInfos()
	 * @see #getGenerationInfoStore()
	 * @generated
	 */
	EReference getGenerationInfoStore_RuleInfos();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore#getTggDiagram <em>Tgg Diagram</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tgg Diagram</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore#getTggDiagram()
	 * @see #getGenerationInfoStore()
	 * @generated
	 */
	EReference getGenerationInfoStore_TggDiagram();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore#getRuleSetEClass <em>Rule Set EClass</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Rule Set EClass</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.GenerationInfoStore#getRuleSetEClass()
	 * @see #getGenerationInfoStore()
	 * @generated
	 */
	EReference getGenerationInfoStore_RuleSetEClass();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore <em>Rule Info Store</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Info Store</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore
	 * @generated
	 */
	EClass getRuleInfoStore();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getTggRule <em>Tgg Rule</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tgg Rule</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getTggRule()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_TggRule();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getRuleEClass <em>Rule EClass</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Rule EClass</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getRuleEClass()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_RuleEClass();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardTransformationActivityDiagram <em>Forward Transformation Activity Diagram</em>}'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Forward Transformation Activity Diagram</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardTransformationActivityDiagram()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_ForwardTransformationActivityDiagram();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingTransformationActivityDiagram <em>Mapping Transformation Activity Diagram</em>}'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Mapping Transformation Activity Diagram</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingTransformationActivityDiagram()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_MappingTransformationActivityDiagram();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseTransformationActivityDiagram <em>Reverse Transformation Activity Diagram</em>}'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reverse Transformation Activity Diagram</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseTransformationActivityDiagram()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_ReverseTransformationActivityDiagram();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardSynchronisationActivityDiagram <em>Forward Synchronisation Activity Diagram</em>}'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Forward Synchronisation Activity Diagram</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardSynchronisationActivityDiagram()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_ForwardSynchronisationActivityDiagram();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingSynchronisationActivityDiagram <em>Mapping Synchronisation Activity Diagram</em>}'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Mapping Synchronisation Activity Diagram</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingSynchronisationActivityDiagram()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_MappingSynchronisationActivityDiagram();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseSynchronizationActivityDiagram <em>Reverse Synchronization Activity Diagram</em>}'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reverse Synchronization Activity Diagram</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseSynchronizationActivityDiagram()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_ReverseSynchronizationActivityDiagram();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getHelperActivityDiagrams <em>Helper Activity Diagrams</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the map '<em>Helper Activity Diagrams</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getHelperActivityDiagrams()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_HelperActivityDiagrams();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardTransformationOperation <em>Forward Transformation Operation</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference '<em>Forward Transformation Operation</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardTransformationOperation()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_ForwardTransformationOperation();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingTransformationOperation <em>Mapping Transformation Operation</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference '<em>Mapping Transformation Operation</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingTransformationOperation()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_MappingTransformationOperation();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseTransformationOperation <em>Reverse Transformation Operation</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference '<em>Reverse Transformation Operation</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseTransformationOperation()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_ReverseTransformationOperation();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardSynchronizationOperation <em>Forward Synchronization Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Forward Synchronization Operation</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getForwardSynchronizationOperation()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_ForwardSynchronizationOperation();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingSynchronizationOperation <em>Mapping Synchronization Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Mapping Synchronization Operation</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getMappingSynchronizationOperation()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_MappingSynchronizationOperation();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseSynchronizationOperation <em>Reverse Synchronization Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reverse Synchronization Operation</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.RuleInfoStore#getReverseSynchronizationOperation()
	 * @see #getRuleInfoStore()
	 * @generated
	 */
	EReference getRuleInfoStore_ReverseSynchronizationOperation();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SynchronizationStrategy <em>Synchronization Strategy</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Synchronization Strategy</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.SynchronizationStrategy
	 * @generated
	 */
	EClass getSynchronizationStrategy();

	/**
	 * Returns the meta object for data type '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleGenerationException <em>Rule Generation Exception</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for data type '<em>Rule Generation Exception</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleGenerationException
	 * @model instanceClass="de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleGenerationException"
	 * @generated
	 */
	EDataType getRuleGenerationException();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GenerationStrategiesFactory getGenerationStrategiesFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategyImpl <em>Generation Strategy</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategyImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getGenerationStrategy()
		 * @generated
		 */
		EClass GENERATION_STRATEGY = eINSTANCE.getGenerationStrategy();

		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.SimpleTransformationGenerationStrategyImpl
		 * <em>Simple Transformation Generation Strategy</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.SimpleTransformationGenerationStrategyImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getSimpleTransformationGenerationStrategy()
		 * @generated
		 */
		EClass SIMPLE_TRANSFORMATION_GENERATION_STRATEGY = eINSTANCE
				.getSimpleTransformationGenerationStrategy();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.TraceabilityLinkStoreImpl <em>Traceability Link Store</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.TraceabilityLinkStoreImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getTraceabilityLinkStore()
		 * @generated
		 */
		EClass TRACEABILITY_LINK_STORE = eINSTANCE.getTraceabilityLinkStore();

		/**
		 * The meta object literal for the '<em><b>Traceability Links</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference TRACEABILITY_LINK_STORE__TRACEABILITY_LINKS = eINSTANCE
				.getTraceabilityLinkStore_TraceabilityLinks();

		/**
		 * The meta object literal for the '<em><b>Mappings</b></em>' map feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRACEABILITY_LINK_STORE__MAPPINGS = eINSTANCE
				.getTraceabilityLinkStore_Mappings();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.TraceabilityLinkImpl <em>Traceability Link</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.TraceabilityLinkImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getTraceabilityLink()
		 * @generated
		 */
		EClass TRACEABILITY_LINK = eINSTANCE.getTraceabilityLink();

		/**
		 * The meta object literal for the '<em><b>Sources</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRACEABILITY_LINK__SOURCES = eINSTANCE
				.getTraceabilityLink_Sources();

		/**
		 * The meta object literal for the '<em><b>Targets</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRACEABILITY_LINK__TARGETS = eINSTANCE
				.getTraceabilityLink_Targets();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.ConflictCheckTransformationGenerationStrategyImpl <em>Conflict Check Transformation Generation Strategy</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.ConflictCheckTransformationGenerationStrategyImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getConflictCheckTransformationGenerationStrategy()
		 * @generated
		 */
		EClass CONFLICT_CHECK_TRANSFORMATION_GENERATION_STRATEGY = eINSTANCE
				.getConflictCheckTransformationGenerationStrategy();

		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.StoryDiagramBasedGenerationStrategyImpl
		 * <em>Story Diagram Based Generation Strategy</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.StoryDiagramBasedGenerationStrategyImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getStoryDiagramBasedGenerationStrategy()
		 * @generated
		 */
		EClass STORY_DIAGRAM_BASED_GENERATION_STRATEGY = eINSTANCE
				.getStoryDiagramBasedGenerationStrategy();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationInfoStoreImpl <em>Generation Info Store</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationInfoStoreImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getGenerationInfoStore()
		 * @generated
		 */
		EClass GENERATION_INFO_STORE = eINSTANCE.getGenerationInfoStore();

		/**
		 * The meta object literal for the '<em><b>Rule Infos</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference GENERATION_INFO_STORE__RULE_INFOS = eINSTANCE
				.getGenerationInfoStore_RuleInfos();

		/**
		 * The meta object literal for the '<em><b>Tgg Diagram</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERATION_INFO_STORE__TGG_DIAGRAM = eINSTANCE
				.getGenerationInfoStore_TggDiagram();

		/**
		 * The meta object literal for the '<em><b>Rule Set EClass</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERATION_INFO_STORE__RULE_SET_ECLASS = eINSTANCE
				.getGenerationInfoStore_RuleSetEClass();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl <em>Rule Info Store</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleInfoStoreImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getRuleInfoStore()
		 * @generated
		 */
		EClass RULE_INFO_STORE = eINSTANCE.getRuleInfoStore();

		/**
		 * The meta object literal for the '<em><b>Tgg Rule</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__TGG_RULE = eINSTANCE
				.getRuleInfoStore_TggRule();

		/**
		 * The meta object literal for the '<em><b>Rule EClass</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__RULE_ECLASS = eINSTANCE
				.getRuleInfoStore_RuleEClass();

		/**
		 * The meta object literal for the '<em><b>Forward Transformation Activity Diagram</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__FORWARD_TRANSFORMATION_ACTIVITY_DIAGRAM = eINSTANCE
				.getRuleInfoStore_ForwardTransformationActivityDiagram();

		/**
		 * The meta object literal for the '<em><b>Mapping Transformation Activity Diagram</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__MAPPING_TRANSFORMATION_ACTIVITY_DIAGRAM = eINSTANCE
				.getRuleInfoStore_MappingTransformationActivityDiagram();

		/**
		 * The meta object literal for the '<em><b>Reverse Transformation Activity Diagram</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__REVERSE_TRANSFORMATION_ACTIVITY_DIAGRAM = eINSTANCE
				.getRuleInfoStore_ReverseTransformationActivityDiagram();

		/**
		 * The meta object literal for the '<em><b>Forward Synchronisation Activity Diagram</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__FORWARD_SYNCHRONISATION_ACTIVITY_DIAGRAM = eINSTANCE
				.getRuleInfoStore_ForwardSynchronisationActivityDiagram();

		/**
		 * The meta object literal for the '<em><b>Mapping Synchronisation Activity Diagram</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__MAPPING_SYNCHRONISATION_ACTIVITY_DIAGRAM = eINSTANCE
				.getRuleInfoStore_MappingSynchronisationActivityDiagram();

		/**
		 * The meta object literal for the '<em><b>Reverse Synchronization Activity Diagram</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_ACTIVITY_DIAGRAM = eINSTANCE
				.getRuleInfoStore_ReverseSynchronizationActivityDiagram();

		/**
		 * The meta object literal for the '
		 * <em><b>Helper Activity Diagrams</b></em>' map feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference RULE_INFO_STORE__HELPER_ACTIVITY_DIAGRAMS = eINSTANCE
				.getRuleInfoStore_HelperActivityDiagrams();

		/**
		 * The meta object literal for the '<em><b>Forward Transformation Operation</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__FORWARD_TRANSFORMATION_OPERATION = eINSTANCE
				.getRuleInfoStore_ForwardTransformationOperation();

		/**
		 * The meta object literal for the '<em><b>Mapping Transformation Operation</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__MAPPING_TRANSFORMATION_OPERATION = eINSTANCE
				.getRuleInfoStore_MappingTransformationOperation();

		/**
		 * The meta object literal for the '<em><b>Reverse Transformation Operation</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__REVERSE_TRANSFORMATION_OPERATION = eINSTANCE
				.getRuleInfoStore_ReverseTransformationOperation();

		/**
		 * The meta object literal for the '<em><b>Forward Synchronization Operation</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__FORWARD_SYNCHRONIZATION_OPERATION = eINSTANCE
				.getRuleInfoStore_ForwardSynchronizationOperation();

		/**
		 * The meta object literal for the '<em><b>Mapping Synchronization Operation</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__MAPPING_SYNCHRONIZATION_OPERATION = eINSTANCE
				.getRuleInfoStore_MappingSynchronizationOperation();

		/**
		 * The meta object literal for the '<em><b>Reverse Synchronization Operation</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_INFO_STORE__REVERSE_SYNCHRONIZATION_OPERATION = eINSTANCE
				.getRuleInfoStore_ReverseSynchronizationOperation();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.SynchronizationStrategyImpl <em>Synchronization Strategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.SynchronizationStrategyImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getSynchronizationStrategy()
		 * @generated
		 */
		EClass SYNCHRONIZATION_STRATEGY = eINSTANCE
				.getSynchronizationStrategy();

		/**
		 * The meta object literal for the '<em>Rule Generation Exception</em>' data type.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleGenerationException
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationStrategiesPackageImpl#getRuleGenerationException()
		 * @generated
		 */
		EDataType RULE_GENERATION_EXCEPTION = eINSTANCE
				.getRuleGenerationException();

	}

} // GenerationStrategiesPackage
