/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import de.mdelab.workflow.components.ComponentsPackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsFactory
 * @model kind="package"
 * @generated
 */
public interface WorkflowComponentsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workflowComponents";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://operationalRulesGenerator/workflowComponents/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "operationalRulesGenerator.workflowComponents";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	WorkflowComponentsPackage eINSTANCE = de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.WorkflowComponentsPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestLoaderComponentImpl <em>Manifest Loader Component</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestLoaderComponentImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.WorkflowComponentsPackageImpl#getManifestLoaderComponent()
	 * @generated
	 */
	int MANIFEST_LOADER_COMPONENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MANIFEST_LOADER_COMPONENT__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MANIFEST_LOADER_COMPONENT__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Model Slot</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MANIFEST_LOADER_COMPONENT__MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Project Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MANIFEST_LOADER_COMPONENT__PROJECT_NAME = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Manifest Loader Component</em>' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANIFEST_LOADER_COMPONENT_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.GenerationStrategyComponentImpl <em>Generation Strategy Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.GenerationStrategyComponentImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.WorkflowComponentsPackageImpl#getGenerationStrategyComponent()
	 * @generated
	 */
	int GENERATION_STRATEGY_COMPONENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GENERATION_STRATEGY_COMPONENT__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GENERATION_STRATEGY_COMPONENT__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tgg File URI</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GENERATION_STRATEGY_COMPONENT__TGG_FILE_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Project Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GENERATION_STRATEGY_COMPONENT__PROJECT_NAME = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Correspondence Metamodel URI</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATION_STRATEGY_COMPONENT__CORRESPONDENCE_METAMODEL_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Java Base Package</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATION_STRATEGY_COMPONENT__JAVA_BASE_PACKAGE = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Generation Strategy</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATION_STRATEGY_COMPONENT__GENERATION_STRATEGY = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Rules To Generate</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATION_STRATEGY_COMPONENT__RULES_TO_GENERATE = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Generation Strategy Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATION_STRATEGY_COMPONENT_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.EMFCodeGenerationComponentImpl <em>EMF Code Generation Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.EMFCodeGenerationComponentImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.WorkflowComponentsPackageImpl#getEMFCodeGenerationComponent()
	 * @generated
	 */
	int EMF_CODE_GENERATION_COMPONENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int EMF_CODE_GENERATION_COMPONENT__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int EMF_CODE_GENERATION_COMPONENT__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Project Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int EMF_CODE_GENERATION_COMPONENT__PROJECT_NAME = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Tgg File URI</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int EMF_CODE_GENERATION_COMPONENT__TGG_FILE_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Correspondence Metamodel URI</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMF_CODE_GENERATION_COMPONENT__CORRESPONDENCE_METAMODEL_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Java Base Package</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMF_CODE_GENERATION_COMPONENT__JAVA_BASE_PACKAGE = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>EMF Code Generation Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMF_CODE_GENERATION_COMPONENT_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestGeneratorComponentImpl <em>Manifest Generator Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestGeneratorComponentImpl
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.WorkflowComponentsPackageImpl#getManifestGeneratorComponent()
	 * @generated
	 */
	int MANIFEST_GENERATOR_COMPONENT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MANIFEST_GENERATOR_COMPONENT__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MANIFEST_GENERATOR_COMPONENT__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Manifest Slot</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MANIFEST_GENERATOR_COMPONENT__MANIFEST_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Project Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MANIFEST_GENERATOR_COMPONENT__PROJECT_NAME = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tgg File URI</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MANIFEST_GENERATOR_COMPONENT__TGG_FILE_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Correspondence Metamodel URI</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANIFEST_GENERATOR_COMPONENT__CORRESPONDENCE_METAMODEL_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Manifest Generator Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANIFEST_GENERATOR_COMPONENT_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent <em>Manifest Loader Component</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Manifest Loader Component</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent
	 * @generated
	 */
	EClass getManifestLoaderComponent();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent#getModelSlot <em>Model Slot</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Slot</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent#getModelSlot()
	 * @see #getManifestLoaderComponent()
	 * @generated
	 */
	EAttribute getManifestLoaderComponent_ModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent#getProjectName <em>Project Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Project Name</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestLoaderComponent#getProjectName()
	 * @see #getManifestLoaderComponent()
	 * @generated
	 */
	EAttribute getManifestLoaderComponent_ProjectName();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent <em>Generation Strategy Component</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Generation Strategy Component</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent
	 * @generated
	 */
	EClass getGenerationStrategyComponent();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getGenerationStrategy
	 * <em>Generation Strategy</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Generation Strategy</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getGenerationStrategy()
	 * @see #getGenerationStrategyComponent()
	 * @generated
	 */
	EAttribute getGenerationStrategyComponent_GenerationStrategy();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getRulesToGenerate <em>Rules To Generate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rules To Generate</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getRulesToGenerate()
	 * @see #getGenerationStrategyComponent()
	 * @generated
	 */
	EReference getGenerationStrategyComponent_RulesToGenerate();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getTggFileURI <em>Tgg File URI</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tgg File URI</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getTggFileURI()
	 * @see #getGenerationStrategyComponent()
	 * @generated
	 */
	EAttribute getGenerationStrategyComponent_TggFileURI();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getProjectName <em>Project Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Project Name</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getProjectName()
	 * @see #getGenerationStrategyComponent()
	 * @generated
	 */
	EAttribute getGenerationStrategyComponent_ProjectName();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getCorrespondenceMetamodelURI <em>Correspondence Metamodel URI</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Correspondence Metamodel URI</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getCorrespondenceMetamodelURI()
	 * @see #getGenerationStrategyComponent()
	 * @generated
	 */
	EAttribute getGenerationStrategyComponent_CorrespondenceMetamodelURI();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getJavaBasePackage
	 * <em>Java Base Package</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Java Base Package</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.GenerationStrategyComponent#getJavaBasePackage()
	 * @see #getGenerationStrategyComponent()
	 * @generated
	 */
	EAttribute getGenerationStrategyComponent_JavaBasePackage();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent <em>EMF Code Generation Component</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>EMF Code Generation Component</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent
	 * @generated
	 */
	EClass getEMFCodeGenerationComponent();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getProjectName <em>Project Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Project Name</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getProjectName()
	 * @see #getEMFCodeGenerationComponent()
	 * @generated
	 */
	EAttribute getEMFCodeGenerationComponent_ProjectName();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getTggFileURI <em>Tgg File URI</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tgg File URI</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getTggFileURI()
	 * @see #getEMFCodeGenerationComponent()
	 * @generated
	 */
	EAttribute getEMFCodeGenerationComponent_TggFileURI();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getCorrespondenceMetamodelURI <em>Correspondence Metamodel URI</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Correspondence Metamodel URI</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getCorrespondenceMetamodelURI()
	 * @see #getEMFCodeGenerationComponent()
	 * @generated
	 */
	EAttribute getEMFCodeGenerationComponent_CorrespondenceMetamodelURI();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getJavaBasePackage
	 * <em>Java Base Package</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Java Base Package</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getJavaBasePackage()
	 * @see #getEMFCodeGenerationComponent()
	 * @generated
	 */
	EAttribute getEMFCodeGenerationComponent_JavaBasePackage();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent <em>Manifest Generator Component</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Manifest Generator Component</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent
	 * @generated
	 */
	EClass getManifestGeneratorComponent();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getManifestSlot <em>Manifest Slot</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Manifest Slot</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getManifestSlot()
	 * @see #getManifestGeneratorComponent()
	 * @generated
	 */
	EAttribute getManifestGeneratorComponent_ManifestSlot();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getProjectName <em>Project Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Project Name</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getProjectName()
	 * @see #getManifestGeneratorComponent()
	 * @generated
	 */
	EAttribute getManifestGeneratorComponent_ProjectName();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getTggFileURI <em>Tgg File URI</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tgg File URI</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getTggFileURI()
	 * @see #getManifestGeneratorComponent()
	 * @generated
	 */
	EAttribute getManifestGeneratorComponent_TggFileURI();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getCorrespondenceMetamodelURI <em>Correspondence Metamodel URI</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Correspondence Metamodel URI</em>'.
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.ManifestGeneratorComponent#getCorrespondenceMetamodelURI()
	 * @see #getManifestGeneratorComponent()
	 * @generated
	 */
	EAttribute getManifestGeneratorComponent_CorrespondenceMetamodelURI();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkflowComponentsFactory getWorkflowComponentsFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestLoaderComponentImpl <em>Manifest Loader Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestLoaderComponentImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.WorkflowComponentsPackageImpl#getManifestLoaderComponent()
		 * @generated
		 */
		EClass MANIFEST_LOADER_COMPONENT = eINSTANCE
				.getManifestLoaderComponent();

		/**
		 * The meta object literal for the '<em><b>Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MANIFEST_LOADER_COMPONENT__MODEL_SLOT = eINSTANCE
				.getManifestLoaderComponent_ModelSlot();

		/**
		 * The meta object literal for the '<em><b>Project Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MANIFEST_LOADER_COMPONENT__PROJECT_NAME = eINSTANCE
				.getManifestLoaderComponent_ProjectName();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.GenerationStrategyComponentImpl <em>Generation Strategy Component</em>}' class.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.GenerationStrategyComponentImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.WorkflowComponentsPackageImpl#getGenerationStrategyComponent()
		 * @generated
		 */
		EClass GENERATION_STRATEGY_COMPONENT = eINSTANCE
				.getGenerationStrategyComponent();

		/**
		 * The meta object literal for the '<em><b>Generation Strategy</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERATION_STRATEGY_COMPONENT__GENERATION_STRATEGY = eINSTANCE
				.getGenerationStrategyComponent_GenerationStrategy();

		/**
		 * The meta object literal for the '<em><b>Rules To Generate</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERATION_STRATEGY_COMPONENT__RULES_TO_GENERATE = eINSTANCE
				.getGenerationStrategyComponent_RulesToGenerate();

		/**
		 * The meta object literal for the '<em><b>Tgg File URI</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERATION_STRATEGY_COMPONENT__TGG_FILE_URI = eINSTANCE
				.getGenerationStrategyComponent_TggFileURI();

		/**
		 * The meta object literal for the '<em><b>Project Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERATION_STRATEGY_COMPONENT__PROJECT_NAME = eINSTANCE
				.getGenerationStrategyComponent_ProjectName();

		/**
		 * The meta object literal for the '
		 * <em><b>Correspondence Metamodel URI</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute GENERATION_STRATEGY_COMPONENT__CORRESPONDENCE_METAMODEL_URI = eINSTANCE
				.getGenerationStrategyComponent_CorrespondenceMetamodelURI();

		/**
		 * The meta object literal for the '<em><b>Java Base Package</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERATION_STRATEGY_COMPONENT__JAVA_BASE_PACKAGE = eINSTANCE
				.getGenerationStrategyComponent_JavaBasePackage();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.EMFCodeGenerationComponentImpl <em>EMF Code Generation Component</em>}' class.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.EMFCodeGenerationComponentImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.WorkflowComponentsPackageImpl#getEMFCodeGenerationComponent()
		 * @generated
		 */
		EClass EMF_CODE_GENERATION_COMPONENT = eINSTANCE
				.getEMFCodeGenerationComponent();

		/**
		 * The meta object literal for the '<em><b>Project Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMF_CODE_GENERATION_COMPONENT__PROJECT_NAME = eINSTANCE
				.getEMFCodeGenerationComponent_ProjectName();

		/**
		 * The meta object literal for the '<em><b>Tgg File URI</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMF_CODE_GENERATION_COMPONENT__TGG_FILE_URI = eINSTANCE
				.getEMFCodeGenerationComponent_TggFileURI();

		/**
		 * The meta object literal for the '
		 * <em><b>Correspondence Metamodel URI</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute EMF_CODE_GENERATION_COMPONENT__CORRESPONDENCE_METAMODEL_URI = eINSTANCE
				.getEMFCodeGenerationComponent_CorrespondenceMetamodelURI();

		/**
		 * The meta object literal for the '<em><b>Java Base Package</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMF_CODE_GENERATION_COMPONENT__JAVA_BASE_PACKAGE = eINSTANCE
				.getEMFCodeGenerationComponent_JavaBasePackage();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestGeneratorComponentImpl <em>Manifest Generator Component</em>}' class.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.ManifestGeneratorComponentImpl
		 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.impl.WorkflowComponentsPackageImpl#getManifestGeneratorComponent()
		 * @generated
		 */
		EClass MANIFEST_GENERATOR_COMPONENT = eINSTANCE
				.getManifestGeneratorComponent();

		/**
		 * The meta object literal for the '<em><b>Manifest Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MANIFEST_GENERATOR_COMPONENT__MANIFEST_SLOT = eINSTANCE
				.getManifestGeneratorComponent_ManifestSlot();

		/**
		 * The meta object literal for the '<em><b>Project Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MANIFEST_GENERATOR_COMPONENT__PROJECT_NAME = eINSTANCE
				.getManifestGeneratorComponent_ProjectName();

		/**
		 * The meta object literal for the '<em><b>Tgg File URI</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MANIFEST_GENERATOR_COMPONENT__TGG_FILE_URI = eINSTANCE
				.getManifestGeneratorComponent_TggFileURI();

		/**
		 * The meta object literal for the '
		 * <em><b>Correspondence Metamodel URI</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute MANIFEST_GENERATOR_COMPONENT__CORRESPONDENCE_METAMODEL_URI = eINSTANCE
				.getManifestGeneratorComponent_CorrespondenceMetamodelURI();

	}

} // WorkflowComponentsPackage
