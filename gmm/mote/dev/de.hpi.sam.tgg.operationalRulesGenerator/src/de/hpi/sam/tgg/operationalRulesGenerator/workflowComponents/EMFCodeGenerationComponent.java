/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>EMF Code Generation Component</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getProjectName <em>Project Name</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getTggFileURI <em>Tgg File URI</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getCorrespondenceMetamodelURI <em>Correspondence Metamodel URI</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getJavaBasePackage <em>Java Base Package</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getEMFCodeGenerationComponent()
 * @model
 * @generated
 */
public interface EMFCodeGenerationComponent extends WorkflowComponent {
	/**
	 * Returns the value of the '<em><b>Project Name</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Project Name</em>' attribute.
	 * @see #setProjectName(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getEMFCodeGenerationComponent_ProjectName()
	 * @model required="true"
	 * @generated
	 */
	String getProjectName();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getProjectName <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Project Name</em>' attribute.
	 * @see #getProjectName()
	 * @generated
	 */
	void setProjectName(String value);

	/**
	 * Returns the value of the '<em><b>Tgg File URI</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgg File URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Tgg File URI</em>' attribute.
	 * @see #setTggFileURI(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getEMFCodeGenerationComponent_TggFileURI()
	 * @model required="true"
	 * @generated
	 */
	String getTggFileURI();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getTggFileURI <em>Tgg File URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Tgg File URI</em>' attribute.
	 * @see #getTggFileURI()
	 * @generated
	 */
	void setTggFileURI(String value);

	/**
	 * Returns the value of the '<em><b>Correspondence Metamodel URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correspondence Metamodel URI</em>' attribute
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correspondence Metamodel URI</em>' attribute.
	 * @see #setCorrespondenceMetamodelURI(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getEMFCodeGenerationComponent_CorrespondenceMetamodelURI()
	 * @model required="true"
	 * @generated
	 */
	String getCorrespondenceMetamodelURI();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getCorrespondenceMetamodelURI <em>Correspondence Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Correspondence Metamodel URI</em>' attribute.
	 * @see #getCorrespondenceMetamodelURI()
	 * @generated
	 */
	void setCorrespondenceMetamodelURI(String value);

	/**
	 * Returns the value of the '<em><b>Java Base Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Java Base Package</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Java Base Package</em>' attribute.
	 * @see #setJavaBasePackage(String)
	 * @see de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.WorkflowComponentsPackage#getEMFCodeGenerationComponent_JavaBasePackage()
	 * @model
	 * @generated
	 */
	String getJavaBasePackage();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.operationalRulesGenerator.workflowComponents.EMFCodeGenerationComponent#getJavaBasePackage <em>Java Base Package</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Java Base Package</em>' attribute.
	 * @see #getJavaBasePackage()
	 * @generated
	 */
	void setJavaBasePackage(String value);

} // EMFCodeGenerationComponent
