<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_match_LHS_axiom" uuid="_f2pxUXUrEd-AwpZ_Fdb-iA">
  <activities name="create_storyPattern_match_LHS_axiom" uuid="_QBQAwIBhEd-Ol7HhHmHbiA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_VLFBUIBhEd-Ol7HhHmHbiA" outgoing="_ivxVYIBhEd-Ol7HhHmHbiA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_Vrg04IBhEd-Ol7HhHmHbiA" incoming="_ivxVYIBhEd-Ol7HhHmHbiA" outgoing="_qlbbQAxUEeCp2NlniWjADQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_Wshl8IBhEd-Ol7HhHmHbiA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_Z-vkoIBhEd-Ol7HhHmHbiA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_dt-PcIBhEd-Ol7HhHmHbiA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_l0Q0UPPUEd-cAK4OkgyYOw" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set source assignment" uuid="_Dq_wUPZCEd-o55Ll0oEtLw" incoming="_F8OeIA0iEeCQ056ROwfPtw" outgoing="_aA9F0A0kEeCk-oIAfEiwIw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_JjvFUPZCEd-o55Ll0oEtLw" outgoingStoryLinks="_FoAA0PZIEd-o55Ll0oEtLw _axN6YPcFEd-_yp3IZC6S-Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sourceDomain" uuid="_K0XmwPZCEd-o55Ll0oEtLw" outgoingStoryLinks="_qoC-UPZDEd-o55Ll0oEtLw" incomingStoryLinks="_FoAA0PZIEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//SourceModelDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_L25awPZCEd-o55Ll0oEtLw" incomingStoryLinks="_qoC-UPZDEd-o55Ll0oEtLw _axN6YPcFEd-_yp3IZC6S-Q _bpm4MC6uEeCGBYVZiTe_4g">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_NdlrwPZCEd-o55Ll0oEtLw" outgoingStoryLinks="_bpm4MC6uEeCGBYVZiTe_4g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_PWu-sPZCEd-o55Ll0oEtLw" outgoingStoryLinks="_vslPMPZDEd-o55Ll0oEtLw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_QVvkoPZCEd-o55Ll0oEtLw" outgoingStoryLinks="_NMx9sPZEEd-o55Ll0oEtLw" incomingStoryLinks="_vslPMPZDEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_FgCUcPZEEd-o55Ll0oEtLw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_G95ZkPZEEd-o55Ll0oEtLw" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_KbaCQPZEEd-o55Ll0oEtLw" modifier="CREATE" outgoingStoryLinks="_Se-QEPZEEd-o55Ll0oEtLw" incomingStoryLinks="_NMx9sPZEEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varRef" uuid="_QmzNgPZEEd-o55Ll0oEtLw" modifier="CREATE" incomingStoryLinks="_Se-QEPZEEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_T2O98PZEEd-o55Ll0oEtLw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VVfxQPZEEd-o55Ll0oEtLw" expressionString="'source'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_WspxcPZEEd-o55Ll0oEtLw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_YZVVsPZEEd-o55Ll0oEtLw" expressionString="ecore::EObject" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qoC-UPZDEd-o55Ll0oEtLw" source="_K0XmwPZCEd-o55Ll0oEtLw" target="_L25awPZCEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_vslPMPZDEd-o55Ll0oEtLw" source="_PWu-sPZCEd-o55Ll0oEtLw" target="_QVvkoPZCEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_NMx9sPZEEd-o55Ll0oEtLw" modifier="CREATE" source="_QVvkoPZCEd-o55Ll0oEtLw" target="_KbaCQPZEEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Se-QEPZEEd-o55Ll0oEtLw" modifier="CREATE" source="_KbaCQPZEEd-o55Ll0oEtLw" target="_QmzNgPZEEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_FoAA0PZIEd-o55Ll0oEtLw" source="_JjvFUPZCEd-o55Ll0oEtLw" target="_K0XmwPZCEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/sourceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_axN6YPcFEd-_yp3IZC6S-Q" source="_JjvFUPZCEd-o55Ll0oEtLw" target="_L25awPZCEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/inputElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_bpm4MC6uEeCGBYVZiTe_4g" source="_NdlrwPZCEd-o55Ll0oEtLw" target="_L25awPZCEd-o55Ll0oEtLw" valueTarget="_QVvkoPZCEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set target assignment" uuid="_ljUDsPZEEd-o55Ll0oEtLw" incoming="_YArMAAxnEeCBD4Y73qjs0g" outgoing="_gVUm4A0kEeCk-oIAfEiwIw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_qlyfEPZEEd-o55Ll0oEtLw" outgoingStoryLinks="_DwjXAPZIEd-o55Ll0oEtLw _bpIZ0PcFEd-_yp3IZC6S-Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="targetDomain" uuid="_ri0vEPZEEd-o55Ll0oEtLw" outgoingStoryLinks="_87Js0PZEEd-o55Ll0oEtLw" incomingStoryLinks="_DwjXAPZIEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TargetModelDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_sQK9QPZEEd-o55Ll0oEtLw" incomingStoryLinks="_87Js0PZEEd-o55Ll0oEtLw _bpIZ0PcFEd-_yp3IZC6S-Q _fP_wEC6uEeCGBYVZiTe_4g">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_s5zJEPZEEd-o55Ll0oEtLw" outgoingStoryLinks="_fP_wEC6uEeCGBYVZiTe_4g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_uztRAPZEEd-o55Ll0oEtLw" outgoingStoryLinks="_ALW5UPZFEd-o55Ll0oEtLw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_v1F1gPZEEd-o55Ll0oEtLw" outgoingStoryLinks="_Ank7QPZFEd-o55Ll0oEtLw" incomingStoryLinks="_ALW5UPZFEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_OcjHAPZFEd-o55Ll0oEtLw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_QnmpUPZFEd-o55Ll0oEtLw" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_wZaDgPZEEd-o55Ll0oEtLw" modifier="CREATE" outgoingStoryLinks="_D3tPQPZFEd-o55Ll0oEtLw" incomingStoryLinks="_Ank7QPZFEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varRef" uuid="_w8uL8PZEEd-o55Ll0oEtLw" modifier="CREATE" incomingStoryLinks="_D3tPQPZFEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_E605IPZFEd-o55Ll0oEtLw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GJKUgPZFEd-o55Ll0oEtLw" expressionString="'target'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_Hbp3IPZFEd-o55Ll0oEtLw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ii9dcPZFEd-o55Ll0oEtLw" expressionString="ecore::EObject" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_87Js0PZEEd-o55Ll0oEtLw" source="_ri0vEPZEEd-o55Ll0oEtLw" target="_sQK9QPZEEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ALW5UPZFEd-o55Ll0oEtLw" source="_uztRAPZEEd-o55Ll0oEtLw" target="_v1F1gPZEEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ank7QPZFEd-o55Ll0oEtLw" modifier="CREATE" source="_v1F1gPZEEd-o55Ll0oEtLw" target="_wZaDgPZEEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_D3tPQPZFEd-o55Ll0oEtLw" modifier="CREATE" source="_wZaDgPZEEd-o55Ll0oEtLw" target="_w8uL8PZEEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_DwjXAPZIEd-o55Ll0oEtLw" source="_qlyfEPZEEd-o55Ll0oEtLw" target="_ri0vEPZEEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/targetDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_bpIZ0PcFEd-_yp3IZC6S-Q" source="_qlyfEPZEEd-o55Ll0oEtLw" target="_sQK9QPZEEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/inputElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_fP_wEC6uEeCGBYVZiTe_4g" source="_s5zJEPZEEd-o55Ll0oEtLw" target="_sQK9QPZEEd-o55Ll0oEtLw" valueTarget="_v1F1gPZEEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create this and ruleSet SPOs" uuid="_OPNzoAxUEeCp2NlniWjADQ" incoming="_qlbbQAxUEeCp2NlniWjADQ" outgoing="_3uN2cAxUEeCp2NlniWjADQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisSpo" uuid="_PZICAAxUEeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_UAB7sAxUEeCp2NlniWjADQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_UZEGwAxUEeCp2NlniWjADQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_rule_spo.story#_Kpsp0AwlEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_YJ8XYAxUEeCp2NlniWjADQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZsquYAxUEeCp2NlniWjADQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="isAxiom" uuid="_as9twAxUEeCp2NlniWjADQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_cB-_8AxUEeCp2NlniWjADQ" expressionString="tggRule.isAxiom" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_Q6IhwAxUEeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_hVcdoAxUEeCp2NlniWjADQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_huU3sAxUEeCp2NlniWjADQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_ruleSet_spo.story#_ZDP-wAwkEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_jOqpEAxUEeCp2NlniWjADQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kdDHwAxUEeCp2NlniWjADQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="bindingType" uuid="_lh53AAxUEeCp2NlniWjADQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_naFgoAxUEeCp2NlniWjADQ" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create this->ruleSet link" uuid="_rPz1AAxUEeCp2NlniWjADQ" incoming="_3uN2cAxUEeCp2NlniWjADQ" outgoing="_CAWNkAxVEeCp2NlniWjADQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_torJ0AxUEeCp2NlniWjADQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_wNxj0AxUEeCp2NlniWjADQ">
          <activity href="../common/create_rule_ruleSet_link.story#_qOEaEAwmEeCU3uEhqcP0Dg"/>
          <parameters name="ruleSpo" uuid="_xIVuoAxUEeCp2NlniWjADQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_yqF1QAxUEeCp2NlniWjADQ" expressionString="thisSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_zeBzoAxUEeCp2NlniWjADQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1mKk0AxUEeCp2NlniWjADQ" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_-9SFMAxUEeCp2NlniWjADQ" incoming="_CAWNkAxVEeCp2NlniWjADQ" outgoing="_Cs-qEAxVEeCp2NlniWjADQ _QDD1MAxnEeCBD4Y73qjs0g"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create source model elements" uuid="_AZ5NIQxVEeCp2NlniWjADQ" incoming="_Cs-qEAxVEeCp2NlniWjADQ" outgoing="_QsU0oAxnEeCBD4Y73qjs0g">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_LYj6IAxVEeCp2NlniWjADQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_PHgREAxVEeCp2NlniWjADQ">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_SfzCkAxVEeCp2NlniWjADQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_UTIocAxVEeCp2NlniWjADQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_W1DLkAxVEeCp2NlniWjADQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y5JxUAxVEeCp2NlniWjADQ" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_aRcZ8AxVEeCp2NlniWjADQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bw0iAAxVEeCp2NlniWjADQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_fOe7sAxVEeCp2NlniWjADQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hTHF4AxVEeCp2NlniWjADQ" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_rTry8AxVEeCp2NlniWjADQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0jnIYAxkEeCaq5npjsM-Pw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_4Mq5wAxkEeCaq5npjsM-Pw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6Osb4AxkEeCaq5npjsM-Pw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_8E2sQAxkEeCaq5npjsM-Pw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-FdSEAxkEeCaq5npjsM-Pw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="__MFi8AxkEeCaq5npjsM-Pw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AvCjcAxlEeCaq5npjsM-Pw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_GRs6wAxlEeCaq5npjsM-Pw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H_FCkAxlEeCaq5npjsM-Pw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_JGwcUAxlEeCaq5npjsM-Pw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_LE5kAAxlEeCaq5npjsM-Pw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="bindingType" uuid="_ADNQYBQ1EeC2b9EUvlq2TQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Bp0o4BQ1EeC2b9EUvlq2TQ" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_hMFOkCV0EeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jERfQCV0EeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_MgwzIAxnEeCBD4Y73qjs0g" incoming="_R9UicAxnEeCBD4Y73qjs0g" outgoing="_StAS8AxnEeCBD4Y73qjs0g _YmcykAxnEeCBD4Y73qjs0g"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_Mx0t8AxnEeCBD4Y73qjs0g" incoming="_QDD1MAxnEeCBD4Y73qjs0g _aA9F0A0kEeCk-oIAfEiwIw" outgoing="_R9UicAxnEeCBD4Y73qjs0g"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_NE6pEAxnEeCBD4Y73qjs0g" incoming="_YmcykAxnEeCBD4Y73qjs0g _gVUm4A0kEeCk-oIAfEiwIw" outgoing="_mLqrEAxnEeCczauSUZKYlg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_lwP6YAxnEeCczauSUZKYlg" incoming="_mLqrEAxnEeCczauSUZKYlg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create target model elements" uuid="_k7cucAxpEeCzheQs-jRYlQ" incoming="_StAS8AxnEeCBD4Y73qjs0g" outgoing="_LXlTkA0iEeCQ056ROwfPtw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_k7dVgAxpEeCzheQs-jRYlQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_k7dVkgxpEeCzheQs-jRYlQ">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_k7dVnwxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVoAxpEeCzheQs-jRYlQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_k7dVmwxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVnAxpEeCzheQs-jRYlQ" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_k7dVoQxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVogxpEeCzheQs-jRYlQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_k7dVmQxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVmgxpEeCzheQs-jRYlQ" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_k7dVpQxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVpgxpEeCzheQs-jRYlQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_k7dVlwxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVmAxpEeCzheQs-jRYlQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_k7dVlQxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVlgxpEeCzheQs-jRYlQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_k7dVnQxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVngxpEeCzheQs-jRYlQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_k7dVowxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVpAxpEeCzheQs-jRYlQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_k7dVkwxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVlAxpEeCzheQs-jRYlQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="bindingType" uuid="_E5Bv0BQ1EeC2b9EUvlq2TQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_HaUn4BQ1EeC2b9EUvlq2TQ" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_lal2YCV0EeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nD6koCV0EeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create source model links" uuid="_15Qw0A0hEeCQ056ROwfPtw" incoming="_QsU0oAxnEeCBD4Y73qjs0g" outgoing="_FBXYYA0iEeCQ056ROwfPtw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_97XmEA0hEeCQ056ROwfPtw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_Kk2qoAxoEeCczauSUZKYlg">
          <activity href="../common/transform_modelLinks.story#_tdw5IAw6EeCAUcBgGC58lA"/>
          <parameters name="tggRule" uuid="_mHL-QAxoEeCczauSUZKYlg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_FiQeAAxpEeCzheQs-jRYlQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_HXKKIAxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KTTLIAxpEeCzheQs-jRYlQ" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_L9xI4AxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Oe2lkAxpEeCzheQs-jRYlQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_PsRa8AxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SNR_IAxpEeCzheQs-jRYlQ" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentLinks" uuid="_VA6kYAxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ajSAwAxpEeCzheQs-jRYlQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedLinks" uuid="_cFQw4AxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dnib8AxpEeCzheQs-jRYlQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_fBxlUAxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_geO8QAxpEeCzheQs-jRYlQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_hlYxkAxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jIaqkAxpEeCzheQs-jRYlQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create source uncovered model elements links" uuid="_2_OTUA0hEeCQ056ROwfPtw" incoming="_FBXYYA0iEeCQ056ROwfPtw" outgoing="_F8OeIA0iEeCQ056ROwfPtw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="__RzNgA0hEeCQ056ROwfPtw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_deH3oA0hEeCQ056ROwfPtw">
          <activity href="../common/create_ruleSet_uncoveredElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_eW4roA0hEeCQ056ROwfPtw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gk5bUA0hEeCQ056ROwfPtw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_h10QoA0hEeCQ056ROwfPtw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kQfjUA0hEeCQ056ROwfPtw" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_ljbK0A0hEeCQ056ROwfPtw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_n_qjwA0hEeCQ056ROwfPtw" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_pd2ZAA0hEeCQ056ROwfPtw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rocBQA0hEeCQ056ROwfPtw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_ufcqoA0hEeCQ056ROwfPtw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wJGJAA0hEeCQ056ROwfPtw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_xwBqkA0hEeCQ056ROwfPtw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0bTlEA0hEeCQ056ROwfPtw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create target model links" uuid="_4eYY8A0hEeCQ056ROwfPtw" incoming="_LXlTkA0iEeCQ056ROwfPtw" outgoing="_Lp2IQA0iEeCQ056ROwfPtw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_AUpxoA0iEeCQ056ROwfPtw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_k7dVgQxpEeCzheQs-jRYlQ">
          <activity href="../common/transform_modelLinks.story#_tdw5IAw6EeCAUcBgGC58lA"/>
          <parameters name="tggRule" uuid="_k7dVkAxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVkQxpEeCzheQs-jRYlQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_k7dVigxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dViwxpEeCzheQs-jRYlQ" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_k7dVhAxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVhQxpEeCzheQs-jRYlQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_k7dVjAxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVjQxpEeCzheQs-jRYlQ" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentLinks" uuid="_k7dViAxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dViQxpEeCzheQs-jRYlQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedLinks" uuid="_k7dVggxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVgwxpEeCzheQs-jRYlQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_k7dVjgxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVjwxpEeCzheQs-jRYlQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_k7dVhgxpEeCzheQs-jRYlQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7dVhwxpEeCzheQs-jRYlQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create source uncovered model elements links" uuid="_JMmCvw0iEeCQ056ROwfPtw" incoming="_Lp2IQA0iEeCQ056ROwfPtw" outgoing="_YArMAAxnEeCBD4Y73qjs0g">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_JMmCwA0iEeCQ056ROwfPtw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_JMmCwQ0iEeCQ056ROwfPtw">
          <activity href="../common/create_ruleSet_uncoveredElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_JMmCzA0iEeCQ056ROwfPtw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JMmCzQ0iEeCQ056ROwfPtw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_JMmCxA0iEeCQ056ROwfPtw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JMmCxQ0iEeCQ056ROwfPtw" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_JMmCyg0iEeCQ056ROwfPtw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JMmCyw0iEeCQ056ROwfPtw" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_JMmCxg0iEeCQ056ROwfPtw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JMmCxw0iEeCQ056ROwfPtw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_JMmCyA0iEeCQ056ROwfPtw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JMmCyQ0iEeCQ056ROwfPtw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_JMmCwg0iEeCQ056ROwfPtw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JMmCww0iEeCQ056ROwfPtw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <edges uuid="_ivxVYIBhEd-Ol7HhHmHbiA" source="_VLFBUIBhEd-Ol7HhHmHbiA" target="_Vrg04IBhEd-Ol7HhHmHbiA"/>
    <edges uuid="_qlbbQAxUEeCp2NlniWjADQ" source="_Vrg04IBhEd-Ol7HhHmHbiA" target="_OPNzoAxUEeCp2NlniWjADQ"/>
    <edges uuid="_3uN2cAxUEeCp2NlniWjADQ" source="_OPNzoAxUEeCp2NlniWjADQ" target="_rPz1AAxUEeCp2NlniWjADQ"/>
    <edges uuid="_CAWNkAxVEeCp2NlniWjADQ" source="_rPz1AAxUEeCp2NlniWjADQ" target="_-9SFMAxUEeCp2NlniWjADQ"/>
    <edges uuid="_Cs-qEAxVEeCp2NlniWjADQ" source="_-9SFMAxUEeCp2NlniWjADQ" target="_AZ5NIQxVEeCp2NlniWjADQ" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EvXYkAxVEeCp2NlniWjADQ" expressionString="direction = mote::TransformationDirection::FORWARD or&#xA;direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_QDD1MAxnEeCBD4Y73qjs0g" source="_-9SFMAxUEeCp2NlniWjADQ" target="_Mx0t8AxnEeCBD4Y73qjs0g" guardType="ELSE"/>
    <edges uuid="_QsU0oAxnEeCBD4Y73qjs0g" source="_AZ5NIQxVEeCp2NlniWjADQ" target="_15Qw0A0hEeCQ056ROwfPtw"/>
    <edges uuid="_R9UicAxnEeCBD4Y73qjs0g" source="_Mx0t8AxnEeCBD4Y73qjs0g" target="_MgwzIAxnEeCBD4Y73qjs0g"/>
    <edges uuid="_StAS8AxnEeCBD4Y73qjs0g" source="_MgwzIAxnEeCBD4Y73qjs0g" target="_k7cucAxpEeCzheQs-jRYlQ" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_UOALoAxnEeCBD4Y73qjs0g" expressionString="direction = mote::TransformationDirection::REVERSE or&#xA;direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_YArMAAxnEeCBD4Y73qjs0g" source="_JMmCvw0iEeCQ056ROwfPtw" target="_ljUDsPZEEd-o55Ll0oEtLw"/>
    <edges uuid="_YmcykAxnEeCBD4Y73qjs0g" source="_MgwzIAxnEeCBD4Y73qjs0g" target="_NE6pEAxnEeCBD4Y73qjs0g" guardType="ELSE"/>
    <edges uuid="_mLqrEAxnEeCczauSUZKYlg" source="_NE6pEAxnEeCBD4Y73qjs0g" target="_lwP6YAxnEeCczauSUZKYlg"/>
    <edges uuid="_FBXYYA0iEeCQ056ROwfPtw" source="_15Qw0A0hEeCQ056ROwfPtw" target="_2_OTUA0hEeCQ056ROwfPtw"/>
    <edges uuid="_F8OeIA0iEeCQ056ROwfPtw" source="_2_OTUA0hEeCQ056ROwfPtw" target="_Dq_wUPZCEd-o55Ll0oEtLw"/>
    <edges uuid="_LXlTkA0iEeCQ056ROwfPtw" source="_k7cucAxpEeCzheQs-jRYlQ" target="_4eYY8A0hEeCQ056ROwfPtw"/>
    <edges uuid="_Lp2IQA0iEeCQ056ROwfPtw" source="_4eYY8A0hEeCQ056ROwfPtw" target="_JMmCvw0iEeCQ056ROwfPtw"/>
    <edges uuid="_aA9F0A0kEeCk-oIAfEiwIw" source="_Dq_wUPZCEd-o55Ll0oEtLw" target="_Mx0t8AxnEeCBD4Y73qjs0g"/>
    <edges uuid="_gVUm4A0kEeCk-oIAfEiwIw" source="_ljUDsPZEEd-o55Ll0oEtLw" target="_NE6pEAxnEeCBD4Y73qjs0g"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
