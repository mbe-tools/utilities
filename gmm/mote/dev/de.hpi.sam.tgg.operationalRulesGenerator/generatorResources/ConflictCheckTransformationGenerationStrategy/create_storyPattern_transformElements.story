<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_transformElements" uuid="_Kxxw8C4VEeC0Rpt37j6jRA">
  <activities name="create_storyPattern_transformElements" uuid="_LVs9YC4VEeC0Rpt37j6jRA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_NCaW0C4VEeC0Rpt37j6jRA" outgoing="_Y7CVMC4WEeC0Rpt37j6jRA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_NZNtQC4VEeC0Rpt37j6jRA" incoming="_Y7CVMC4WEeC0Rpt37j6jRA" outgoing="_ZRxaMC4WEeC0Rpt37j6jRA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_OTb50C4VEeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_PuYN0C4VEeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_QanowC4VEeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_DA6qoC4_EeCAE8IccnF7-w" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create thisSpo and ruleSetSpo" uuid="_5MhVAC4VEeC0Rpt37j6jRA" incoming="_ZRxaMC4WEeC0Rpt37j6jRA" outgoing="_n8OccC4WEeC0Rpt37j6jRA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisSpo" uuid="_68EAAC4VEeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_B9y9YC4WEeC0Rpt37j6jRA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_CSPjYC4WEeC0Rpt37j6jRA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_rule_spo.story#_Kpsp0AwlEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_DtgAcC4WEeC0Rpt37j6jRA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Fq1P0C4WEeC0Rpt37j6jRA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="isAxiom" uuid="_GtTZcC4WEeC0Rpt37j6jRA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IdztwC4WEeC0Rpt37j6jRA" expressionString="tggRule.isAxiom" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_KDSd0C4WEeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_NOeAYC4WEeC0Rpt37j6jRA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_NfKu0C4WEeC0Rpt37j6jRA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_ruleSet_spo.story#_ZDP-wAwkEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_Q9xjsC4WEeC0Rpt37j6jRA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Te0kIC4WEeC0Rpt37j6jRA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="bindingType" uuid="_USjHIC4WEeC0Rpt37j6jRA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Vs4XIC4WEeC0Rpt37j6jRA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create this->ruleSet link" uuid="_ZoppIC4WEeC0Rpt37j6jRA" incoming="_n8OccC4WEeC0Rpt37j6jRA" outgoing="_pX2s8C4WEeC0Rpt37j6jRA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_bskBoC4WEeC0Rpt37j6jRA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_eHbhkC4WEeC0Rpt37j6jRA">
          <activity href="../common/create_rule_ruleSet_link.story#_qOEaEAwmEeCU3uEhqcP0Dg"/>
          <parameters name="ruleSpo" uuid="_hZERcC4WEeC0Rpt37j6jRA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jh8CcC4WEeC0Rpt37j6jRA" expressionString="thisSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_kZU9cC4WEeC0Rpt37j6jRA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mTY2YC4WEeC0Rpt37j6jRA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_o5of4C4WEeC0Rpt37j6jRA" incoming="_DFZcMC6mEeCRIK6l8KTurA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform parent model elements" uuid="_7hz0wC4-EeCAE8IccnF7-w" incoming="_pX2s8C4WEeC0Rpt37j6jRA" outgoing="_-XPUUC4_EeCAE8IccnF7-w">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_TBqKYC4_EeCAE8IccnF7-w">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_VbPQ4C4_EeCAE8IccnF7-w">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_WXrrAC4_EeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_YKfFYC4_EeCAE8IccnF7-w" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_ZHYLcC4_EeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_b1L1gC4_EeCAE8IccnF7-w" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_czURsC4_EeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fBn8UC4_EeCAE8IccnF7-w" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_hgX2sC4_EeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jfgc4C4_EeCAE8IccnF7-w" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_lTj0cC4_EeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mfD-QC4_EeCAE8IccnF7-w" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_p-pDgC4_EeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rXNY8C4_EeCAE8IccnF7-w" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_sJcuMC4_EeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_t_-yAC4_EeCAE8IccnF7-w" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_u-nkgC4_EeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wqScIC4_EeCAE8IccnF7-w" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_yweFgC4_EeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0LqRIC4_EeCAE8IccnF7-w" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_1WPN4C4_EeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_29JhUC4_EeCAE8IccnF7-w" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_39vboC4_EeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5bAD0C4_EeCAE8IccnF7-w" expressionString="direction = mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_6Q374C4_EeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7q0KUC4_EeCAE8IccnF7-w" expressionString="direction = mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform parent correspondence nodes" uuid="_Y06tAC5AEeCAE8IccnF7-w" incoming="_-XPUUC4_EeCAE8IccnF7-w" outgoing="_najgsC5AEeCAE8IccnF7-w">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_Y06tAS5AEeCAE8IccnF7-w">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_Y06tAi5AEeCAE8IccnF7-w">
          <activity href="../common/transform_correspondenceNodes.story#_gUE7UBNnEeCRUJdROfc5HA"/>
          <parameters name="tggRule" uuid="_Y06tDy5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y06tEC5AEeCAE8IccnF7-w" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_Y06tAy5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y06tBC5AEeCAE8IccnF7-w" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_Y06tBS5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y06tBi5AEeCAE8IccnF7-w" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_Y06tDS5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y06tDi5AEeCAE8IccnF7-w" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_Y06tFS5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y06tFi5AEeCAE8IccnF7-w" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_Y06tEy5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y06tFC5AEeCAE8IccnF7-w" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_Y06tCy5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y06tDC5AEeCAE8IccnF7-w" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created correspondence nodes" uuid="_kREnQC5AEeCAE8IccnF7-w" incoming="_najgsC5AEeCAE8IccnF7-w" outgoing="_nxNGIC5AEeCAE8IccnF7-w">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_kREnQS5AEeCAE8IccnF7-w">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_kREnQi5AEeCAE8IccnF7-w">
          <activity href="../common/transform_correspondenceNodes.story#_gUE7UBNnEeCRUJdROfc5HA"/>
          <parameters name="tggRule" uuid="_kREnTS5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kREnTi5AEeCAE8IccnF7-w" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_kREnSy5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kREnTC5AEeCAE8IccnF7-w" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_kREnSS5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kREnSi5AEeCAE8IccnF7-w" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_kREnRy5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kREnSC5AEeCAE8IccnF7-w" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_kREnQy5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kREnRC5AEeCAE8IccnF7-w" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_kREnTy5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kREnUC5AEeCAE8IccnF7-w" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_kREnRS5AEeCAE8IccnF7-w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kREnRi5AEeCAE8IccnF7-w" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_YVM7cC6eEeC0NuS4qSSasw" incoming="_nxNGIC5AEeCAE8IccnF7-w" outgoing="_ocRssC6eEeC0NuS4qSSasw __KB6MC6iEeCRIK6l8KTurA _BMQQoC6jEeCRIK6l8KTurA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created source model elements" uuid="_coR6PC6eEeC0NuS4qSSasw" incoming="_ocRssC6eEeC0NuS4qSSasw" outgoing="_o4pfoC6eEeC0NuS4qSSasw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_coR6PS6eEeC0NuS4qSSasw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_coR6Pi6eEeC0NuS4qSSasw">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_coR6QS6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_coR6Qi6eEeC0NuS4qSSasw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_coShTi6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_coShTy6eEeC0NuS4qSSasw" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_coShTC6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_coShTS6eEeC0NuS4qSSasw" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_coShSi6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_coShSy6eEeC0NuS4qSSasw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_coShRC6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_coShRS6eEeC0NuS4qSSasw" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_coR6Py6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_coR6QC6eEeC0NuS4qSSasw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_coR6Qy6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_coR6RC6eEeC0NuS4qSSasw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_coShSC6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_coShSS6eEeC0NuS4qSSasw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_coShQi6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_coShQy6eEeC0NuS4qSSasw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_coShRi6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_coShRy6eEeC0NuS4qSSasw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_coShUC6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_coShUS6eEeC0NuS4qSSasw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_coShQC6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_coShQS6eEeC0NuS4qSSasw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created target model elements" uuid="_ijcREC6eEeC0NuS4qSSasw" incoming="_o4pfoC6eEeC0NuS4qSSasw" outgoing="_5fG7kC6eEeC0NuS4qSSasw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_ijcRES6eEeC0NuS4qSSasw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_ijcREi6eEeC0NuS4qSSasw">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_ijcRIy6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ijcRJC6eEeC0NuS4qSSasw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_ijcRKS6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ijcRKi6eEeC0NuS4qSSasw" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_ijcRJy6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ijcRKC6eEeC0NuS4qSSasw" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_ijcRGy6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ijcRHC6eEeC0NuS4qSSasw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_ijcREy6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ijcRFC6eEeC0NuS4qSSasw" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_ijcRIS6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ijcRIi6eEeC0NuS4qSSasw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_ijcRFS6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ijcRFi6eEeC0NuS4qSSasw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_ijcRHS6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ijcRHi6eEeC0NuS4qSSasw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_ijcRJS6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ijcRJi6eEeC0NuS4qSSasw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_ijcRFy6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ijcRGC6eEeC0NuS4qSSasw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_ijcRHy6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ijcRIC6eEeC0NuS4qSSasw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_ijcRGS6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ijcRGi6eEeC0NuS4qSSasw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_nanbYC6eEeC0NuS4qSSasw" incoming="_pT2N4C6eEeC0NuS4qSSasw _N75MAC6jEeCRIK6l8KTurA _aam6AC6kEeCRIK6l8KTurA" outgoing="_bjUOgC6kEeCRIK6l8KTurA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created target model links" uuid="_2T4Vyy6eEeC0NuS4qSSasw" incoming="_5fG7kC6eEeC0NuS4qSSasw" outgoing="_avSJgC6kEeCRIK6l8KTurA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_2T4VzC6eEeC0NuS4qSSasw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_2T4VzS6eEeC0NuS4qSSasw">
          <activity href="../common/transform_modelLinks.story#_tdw5IAw6EeCAUcBgGC58lA"/>
          <parameters name="tggRule" uuid="_2T480S6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2T480i6eEeC0NuS4qSSasw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_2T48wy6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2T48xC6eEeC0NuS4qSSasw" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_2T48zS6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2T48zi6eEeC0NuS4qSSasw" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_2T48yS6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2T48yi6eEeC0NuS4qSSasw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_2T48xy6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2T48yC6eEeC0NuS4qSSasw" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformParentLinks" uuid="_2T4V0i6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2T48wC6eEeC0NuS4qSSasw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedLinks" uuid="_2T4V0C6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2T4V0S6eEeC0NuS4qSSasw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_2T48xS6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2T48xi6eEeC0NuS4qSSasw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_2T4Vzi6eEeC0NuS4qSSasw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2T4Vzy6eEeC0NuS4qSSasw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created source model elements" uuid="_7rSiiy6iEeCRIK6l8KTurA" incoming="__KB6MC6iEeCRIK6l8KTurA" outgoing="__fw5oC6iEeCRIK6l8KTurA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_7rSijC6iEeCRIK6l8KTurA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_7rSijS6iEeCRIK6l8KTurA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_7rSinC6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7rSinS6iEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_7rTJgy6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7rTJhC6iEeCRIK6l8KTurA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_7rSimC6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7rSimS6iEeCRIK6l8KTurA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_7rSilC6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7rSilS6iEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_7rSini6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7rSiny6iEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_7rSili6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7rSily6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_7rSioC6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7rTJgC6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_7rTJgS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7rTJgi6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_7rSikC6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7rSikS6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_7rSimi6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7rSimy6iEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_7rSiji6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7rSijy6iEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_7rSiki6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7rSiky6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created target model elements" uuid="_8xeucC6iEeCRIK6l8KTurA" incoming="__fw5oC6iEeCRIK6l8KTurA" outgoing="_AExEIC6jEeCRIK6l8KTurA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_8xeucS6iEeCRIK6l8KTurA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_8xeuci6iEeCRIK6l8KTurA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_8xeudy6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8xeueC6iEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_8xeuiS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8xeuii6iEeCRIK6l8KTurA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_8xeugy6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8xeuhC6iEeCRIK6l8KTurA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_8xeugS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8xeugi6iEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_8xeufS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8xeufi6iEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_8xeudS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8xeudi6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_8xeuhy6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8xeuiC6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_8xeuhS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8xeuhi6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_8xeuey6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8xeufC6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_8xeueS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8xeuei6iEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_8xeucy6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8xeudC6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_8xeufy6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8xeugC6iEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created source model elements" uuid="_9oG0cC6iEeCRIK6l8KTurA" incoming="_BMQQoC6jEeCRIK6l8KTurA" outgoing="_NpgbgC6jEeCRIK6l8KTurA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_9oG0cS6iEeCRIK6l8KTurA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_9oG0ci6iEeCRIK6l8KTurA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_9oG0cy6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9oG0dC6iEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_9oG0dS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9oG0di6iEeCRIK6l8KTurA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_9oG0iS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9oG0ii6iEeCRIK6l8KTurA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_9oG0ey6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9oG0fC6iEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_9oG0eS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9oG0ei6iEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_9oG0hS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9oG0hi6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_9oG0fS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9oG0fi6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_9oG0dy6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9oG0eC6iEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_9oG0gS6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9oG0gi6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_9oG0hy6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9oG0iC6iEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_9oG0gy6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9oG0hC6iEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_9oG0fy6iEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9oG0gC6iEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created target model elements" uuid="_NDPs2y6jEeCRIK6l8KTurA" incoming="_NpgbgC6jEeCRIK6l8KTurA" outgoing="_ZzsrAC6kEeCRIK6l8KTurA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_NDPs3C6jEeCRIK6l8KTurA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_NDPs3S6jEeCRIK6l8KTurA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_NDPs4i6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NDPs4y6jEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_NDPs6i6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NDPs6y6jEeCRIK6l8KTurA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_NDQT0S6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NDQT0i6jEeCRIK6l8KTurA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_NDPs6C6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NDPs6S6jEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_NDPs4C6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NDPs4S6jEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_NDQT1S6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NDQT1i6jEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_NDPs5i6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NDPs5y6jEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_NDPs3i6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NDPs3y6jEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_NDQT1y6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NDQT2C6jEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_NDPs7C6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NDQT0C6jEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_NDQT0y6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NDQT1C6jEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_NDPs5C6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NDPs5S6jEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->modelElements links" uuid="_V9GpYC6jEeCRIK6l8KTurA" incoming="_bjUOgC6kEeCRIK6l8KTurA" outgoing="_taAK8C6eEeC0NuS4qSSasw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_ZS9K4C6jEeCRIK6l8KTurA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_b5UwMC6jEeCRIK6l8KTurA">
          <activity href="../common/create_ruleSet_modelElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_c15GIC6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fIEkIC6jEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_f4cRIC6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hqvVMC6jEeCRIK6l8KTurA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="modifier" uuid="_kaMlEC6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mH6EEC6jEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_pXZe4C6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rauokC6jEeCRIK6l8KTurA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_sT33EC6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tgA6EC6jEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_uN7wAC6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_vu07AC6jEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentLinks" uuid="_wk3LIC6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_x4Z2oC6jEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedLinks" uuid="_znnxgC6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1C3ngC6jEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->uncoveredElements links" uuid="_-5fucy6jEeCRIK6l8KTurA" incoming="_avSJgC6kEeCRIK6l8KTurA" outgoing="_pT2N4C6eEeC0NuS4qSSasw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_-5fudC6jEeCRIK6l8KTurA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_-5fudS6jEeCRIK6l8KTurA">
          <activity href="../common/create_ruleSet_uncoveredElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_-5fufi6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-5fufy6jEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_-5fugC6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-5fugS6jEeCRIK6l8KTurA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="modifier" uuid="_-5fueC6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-5fueS6jEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::DESTROY" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_-5fufC6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-5fufS6jEeCRIK6l8KTurA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_-5fuhC6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-5gVcC6jEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_-5fugi6jEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-5fugy6jEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->uncoveredElements links" uuid="_NMtVUC6kEeCRIK6l8KTurA" incoming="_AExEIC6jEeCRIK6l8KTurA" outgoing="_aam6AC6kEeCRIK6l8KTurA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_NMtVUS6kEeCRIK6l8KTurA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_NMtVUi6kEeCRIK6l8KTurA">
          <activity href="../common/create_ruleSet_uncoveredElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_NMtVVS6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NMtVVi6kEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_NMtVWS6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NMtVWi6kEeCRIK6l8KTurA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="modifier" uuid="_NMtVXS6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NMtVXi6kEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::DESTROY" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_NMtVUy6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NMtVVC6kEeCRIK6l8KTurA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_NMtVWy6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NMtVXC6kEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_NMtVVy6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NMtVWC6kEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->uncoveredElements links" uuid="_TbVazy6kEeCRIK6l8KTurA" incoming="_aEaAgC6kEeCRIK6l8KTurA" outgoing="_N75MAC6jEeCRIK6l8KTurA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_TbVa0C6kEeCRIK6l8KTurA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_TbVa0S6kEeCRIK6l8KTurA">
          <activity href="../common/create_ruleSet_uncoveredElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_TbWB0y6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_TbWB1C6kEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_TbWB1y6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_TbWB2C6kEeCRIK6l8KTurA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="modifier" uuid="_TbVa1C6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_TbWB0C6kEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::DESTROY" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_TbWB0S6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_TbWB0i6kEeCRIK6l8KTurA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_TbVa0i6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_TbVa0y6kEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_TbWB1S6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_TbWB1i6kEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created source model links" uuid="_WhGR0C6kEeCRIK6l8KTurA" incoming="_ZzsrAC6kEeCRIK6l8KTurA" outgoing="_aEaAgC6kEeCRIK6l8KTurA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_WhGR0S6kEeCRIK6l8KTurA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_WhGR0i6kEeCRIK6l8KTurA">
          <activity href="../common/transform_modelLinks.story#_tdw5IAw6EeCAUcBgGC58lA"/>
          <parameters name="tggRule" uuid="_WhGR2S6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WhGR2i6kEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_WhGR3y6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WhGR4C6kEeCRIK6l8KTurA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_WhGR1y6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WhGR2C6kEeCRIK6l8KTurA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_WhGR3S6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WhGR3i6kEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_WhGR1S6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WhGR1i6kEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformParentLinks" uuid="_WhGR4y6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WhGR5C6kEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedLinks" uuid="_WhGR4S6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WhGR4i6kEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_WhGR0y6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WhGR1C6kEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_WhGR2y6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WhGR3C6kEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create corrNode->sources and targets links" uuid="_ifkO8C6kEeCRIK6l8KTurA" incoming="_taAK8C6eEeC0NuS4qSSasw" outgoing="_lTPQcC6kEeCRIK6l8KTurA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_oX1p4C6kEeCRIK6l8KTurA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_qYIGoC6kEeCRIK6l8KTurA">
          <activity href="../common/create_corrNode_sourcesTargets_links.story#_uYYuIBNoEeCRUJdROfc5HA"/>
          <parameters name="tggRule" uuid="_rP5cIC6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tLuPoC6kEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_t-1uoC6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_v2elsC6kEeCRIK6l8KTurA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_wrQRkC6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0OrVAC6kEeCRIK6l8KTurA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_1_d9MC6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3qoegC6kEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourcesLinks" uuid="_6so28C6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8CyxkC6kEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetsLinks" uuid="_9L9ZEC6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-f7iYC6kEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentLinks" uuid="__Q1a4C6kEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AqeuYC6lEeCRIK6l8KTurA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedLinks" uuid="_BbZ1AC6lEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_CxAWAC6lEeCRIK6l8KTurA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create parentCorrNode->next links" uuid="_P3_aYC6lEeCRIK6l8KTurA" incoming="_lTPQcC6kEeCRIK6l8KTurA" outgoing="_ka0lQC6lEeCRIK6l8KTurA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_SKjS4C6lEeCRIK6l8KTurA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_UJKUoC6lEeCRIK6l8KTurA">
          <activity href="../common/create_corrNode_next_links.story#_TmMGgBWjEeC7l7ZPwVRGQg"/>
          <parameters name="tggRule" uuid="_UxI6oC6lEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WlyvIC6lEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_XcREIC6lEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Zh7wIC6lEeCRIK6l8KTurA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_aaKYoC6lEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_cP39EC6lEeCRIK6l8KTurA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_dKkDsC6lEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ezS8EC6lEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create createdCorrNode->ruleSet link" uuid="_qjGyIC6lEeCRIK6l8KTurA" incoming="_ka0lQC6lEeCRIK6l8KTurA" outgoing="_DFZcMC6mEeCRIK6l8KTurA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_DfvO0C6mEeCRIK6l8KTurA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_HdTQkC6mEeCRIK6l8KTurA">
          <activity href="../common/create_corrNode_ruleSetLink.story#_XbROsBQDEeCDO6mN3ETDMg"/>
          <parameters name="ruleSetSpo" uuid="_JGbxkC6mEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_LFiikC6mEeCRIK6l8KTurA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="tggRule" uuid="_L_vhAC6mEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NxkrAC6mEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_On0WgC6mEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SmWBkC6mEeCRIK6l8KTurA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_TnRTEC6mEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VWkGcC6mEeCRIK6l8KTurA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <edges uuid="_Y7CVMC4WEeC0Rpt37j6jRA" source="_NCaW0C4VEeC0Rpt37j6jRA" target="_NZNtQC4VEeC0Rpt37j6jRA"/>
    <edges uuid="_ZRxaMC4WEeC0Rpt37j6jRA" source="_NZNtQC4VEeC0Rpt37j6jRA" target="_5MhVAC4VEeC0Rpt37j6jRA"/>
    <edges uuid="_n8OccC4WEeC0Rpt37j6jRA" source="_5MhVAC4VEeC0Rpt37j6jRA" target="_ZoppIC4WEeC0Rpt37j6jRA"/>
    <edges uuid="_pX2s8C4WEeC0Rpt37j6jRA" source="_ZoppIC4WEeC0Rpt37j6jRA" target="_7hz0wC4-EeCAE8IccnF7-w"/>
    <edges uuid="_-XPUUC4_EeCAE8IccnF7-w" source="_7hz0wC4-EeCAE8IccnF7-w" target="_Y06tAC5AEeCAE8IccnF7-w"/>
    <edges uuid="_najgsC5AEeCAE8IccnF7-w" source="_Y06tAC5AEeCAE8IccnF7-w" target="_kREnQC5AEeCAE8IccnF7-w"/>
    <edges uuid="_nxNGIC5AEeCAE8IccnF7-w" source="_kREnQC5AEeCAE8IccnF7-w" target="_YVM7cC6eEeC0NuS4qSSasw"/>
    <edges uuid="_ocRssC6eEeC0NuS4qSSasw" source="_YVM7cC6eEeC0NuS4qSSasw" target="_coR6PC6eEeC0NuS4qSSasw" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rB98kC6eEeC0NuS4qSSasw" expressionString="direction = mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_o4pfoC6eEeC0NuS4qSSasw" source="_coR6PC6eEeC0NuS4qSSasw" target="_ijcREC6eEeC0NuS4qSSasw"/>
    <edges uuid="_pT2N4C6eEeC0NuS4qSSasw" source="_-5fucy6jEeCRIK6l8KTurA" target="_nanbYC6eEeC0NuS4qSSasw"/>
    <edges uuid="_taAK8C6eEeC0NuS4qSSasw" source="_V9GpYC6jEeCRIK6l8KTurA" target="_ifkO8C6kEeCRIK6l8KTurA"/>
    <edges uuid="_5fG7kC6eEeC0NuS4qSSasw" source="_ijcREC6eEeC0NuS4qSSasw" target="_2T4Vyy6eEeC0NuS4qSSasw"/>
    <edges uuid="__KB6MC6iEeCRIK6l8KTurA" source="_YVM7cC6eEeC0NuS4qSSasw" target="_7rSiiy6iEeCRIK6l8KTurA" guardType="ELSE"/>
    <edges uuid="__fw5oC6iEeCRIK6l8KTurA" source="_7rSiiy6iEeCRIK6l8KTurA" target="_8xeucC6iEeCRIK6l8KTurA"/>
    <edges uuid="_AExEIC6jEeCRIK6l8KTurA" source="_8xeucC6iEeCRIK6l8KTurA" target="_NMtVUC6kEeCRIK6l8KTurA"/>
    <edges uuid="_BMQQoC6jEeCRIK6l8KTurA" source="_YVM7cC6eEeC0NuS4qSSasw" target="_9oG0cC6iEeCRIK6l8KTurA" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BtyQYC6jEeCRIK6l8KTurA" expressionString="direction = mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_NpgbgC6jEeCRIK6l8KTurA" source="_9oG0cC6iEeCRIK6l8KTurA" target="_NDPs2y6jEeCRIK6l8KTurA"/>
    <edges uuid="_N75MAC6jEeCRIK6l8KTurA" source="_TbVazy6kEeCRIK6l8KTurA" target="_nanbYC6eEeC0NuS4qSSasw"/>
    <edges uuid="_ZzsrAC6kEeCRIK6l8KTurA" source="_NDPs2y6jEeCRIK6l8KTurA" target="_WhGR0C6kEeCRIK6l8KTurA"/>
    <edges uuid="_aEaAgC6kEeCRIK6l8KTurA" source="_WhGR0C6kEeCRIK6l8KTurA" target="_TbVazy6kEeCRIK6l8KTurA"/>
    <edges uuid="_aam6AC6kEeCRIK6l8KTurA" source="_NMtVUC6kEeCRIK6l8KTurA" target="_nanbYC6eEeC0NuS4qSSasw"/>
    <edges uuid="_avSJgC6kEeCRIK6l8KTurA" source="_2T4Vyy6eEeC0NuS4qSSasw" target="_-5fucy6jEeCRIK6l8KTurA"/>
    <edges uuid="_bjUOgC6kEeCRIK6l8KTurA" source="_nanbYC6eEeC0NuS4qSSasw" target="_V9GpYC6jEeCRIK6l8KTurA"/>
    <edges uuid="_lTPQcC6kEeCRIK6l8KTurA" source="_ifkO8C6kEeCRIK6l8KTurA" target="_P3_aYC6lEeCRIK6l8KTurA"/>
    <edges uuid="_ka0lQC6lEeCRIK6l8KTurA" source="_P3_aYC6lEeCRIK6l8KTurA" target="_qjGyIC6lEeCRIK6l8KTurA"/>
    <edges uuid="_DFZcMC6mEeCRIK6l8KTurA" source="_qjGyIC6lEeCRIK6l8KTurA" target="_o5of4C4WEeC0Rpt37j6jRA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
