<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_conflictCheck_activity" uuid="_3veyUBmoEeCeMPIFRfkCeQ">
  <activities name="create_conflictCheck_activity" uuid="_5mXbcBmoEeCeMPIFRfkCeQ">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_85xs8BmoEeCeMPIFRfkCeQ" outgoing="_SHBt0BmpEeCeMPIFRfkCeQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_9W5t0BmoEeCeMPIFRfkCeQ" incoming="_SHBt0BmpEeCeMPIFRfkCeQ" outgoing="_SbF5UBmpEeCeMPIFRfkCeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_-aR2YBmoEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_LDjrEBmpEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_Tt0sgBmpEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfos" uuid="_zXyjICosEeCjTcyrv1LVTA" incomingStoryLinks="_4MwYwEQcEeCj3vsEcE-cfw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="genInfoStore" uuid="_1WL0QEQcEeCj3vsEcE-cfw" outgoingStoryLinks="_4MwYwEQcEeCj3vsEcE-cfw">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/GenerationInfoStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4MwYwEQcEeCj3vsEcE-cfw" source="_1WL0QEQcEeCj3vsEcE-cfw" target="_zXyjICosEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/GenerationInfoStore/ruleInfos"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create initial node" uuid="_Q8i3sBmpEeCeMPIFRfkCeQ" incoming="_SbF5UBmpEeCeMPIFRfkCeQ" outgoing="_lcH6QBmpEeCeMPIFRfkCeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_Zjh7oBmpEeCeMPIFRfkCeQ" outgoingStoryLinks="_gK8UgBmpEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_c-veQBmpEeCeMPIFRfkCeQ" modifier="CREATE" incomingStoryLinks="_gK8UgBmpEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_gK8UgBmpEeCeMPIFRfkCeQ" modifier="CREATE" source="_Zjh7oBmpEeCeMPIFRfkCeQ" target="_c-veQBmpEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create activity node to match rule set" uuid="_huHXcBmpEeCeMPIFRfkCeQ" incoming="_lcH6QBmpEeCeMPIFRfkCeQ" outgoing="_7RJFwBmpEeCeMPIFRfkCeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_m7NHYBmpEeCeMPIFRfkCeQ" outgoingStoryLinks="_1dd28BmpEeCeMPIFRfkCeQ _2pT_ABmpEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_peKzYBmpEeCeMPIFRfkCeQ" incomingStoryLinks="_zIdHYBmpEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchRuleSetSAN" uuid="_q65QEBmpEeCeMPIFRfkCeQ" modifier="CREATE" incomingStoryLinks="_0ZLvcBmpEeCeMPIFRfkCeQ _2pT_ABmpEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_s6hlgBmpEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_uEvV4BmpEeCeMPIFRfkCeQ" expressionString="'match ruleSet'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_x4bC4BmpEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_zIdHYBmpEeCeMPIFRfkCeQ _0ZLvcBmpEeCeMPIFRfkCeQ" incomingStoryLinks="_1dd28BmpEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zIdHYBmpEeCeMPIFRfkCeQ" modifier="CREATE" source="_x4bC4BmpEeCeMPIFRfkCeQ" target="_peKzYBmpEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0ZLvcBmpEeCeMPIFRfkCeQ" modifier="CREATE" source="_x4bC4BmpEeCeMPIFRfkCeQ" target="_q65QEBmpEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1dd28BmpEeCeMPIFRfkCeQ" modifier="CREATE" source="_m7NHYBmpEeCeMPIFRfkCeQ" target="_x4bC4BmpEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2pT_ABmpEeCeMPIFRfkCeQ" modifier="CREATE" source="_m7NHYBmpEeCeMPIFRfkCeQ" target="_q65QEBmpEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of matchRuleSetSAN" uuid="_5MnpQBmpEeCeMPIFRfkCeQ" incoming="_7RJFwBmpEeCeMPIFRfkCeQ" outgoing="_CzrqIBmqEeCeMPIFRfkCeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_fpVuQCn6EeC1o_ZuFAaWCA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_gMNKwCn6EeC1o_ZuFAaWCA">
          <activity href="../common/create_storyPattern_matchRuleSet.story#_yGfh8EQYEeCj3vsEcE-cfw"/>
          <parameters name="storyActionNode" uuid="_h30_ECn6EeC1o_ZuFAaWCA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_j4OJgCn6EeC1o_ZuFAaWCA" expressionString="matchRuleSetSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="isAxiom" uuid="_lAYEYCn6EeC1o_ZuFAaWCA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mqSBcCn6EeC1o_ZuFAaWCA" expressionString="tggRule.isAxiom" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to iterate TGG rules" uuid="_8DaQMBmpEeCeMPIFRfkCeQ" incoming="_CzrqIBmqEeCeMPIFRfkCeQ" outgoing="_jgL8kBmqEeCeMPIFRfkCeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_DtbVkBmqEeCeMPIFRfkCeQ" outgoingStoryLinks="_W4ElgBmqEeCeMPIFRfkCeQ _XVwnEBmqEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchRuleSetSAN" uuid="_EW7lkBmqEeCeMPIFRfkCeQ" incomingStoryLinks="_XxwmkBmqEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateRulesSAN" uuid="_K7iiEBmqEeCeMPIFRfkCeQ" modifier="CREATE" incomingStoryLinks="_XVwnEBmqEeCeMPIFRfkCeQ _ZDZ0oBmqEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_NHdAABmqEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OPe_EBmqEeCeMPIFRfkCeQ" expressionString="'iterate TGG rules'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_WgmAIEQcEeCj3vsEcE-cfw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/forEach"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XyfF0EQcEeCj3vsEcE-cfw" expressionString="true" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_UYNQ0BmqEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_XxwmkBmqEeCeMPIFRfkCeQ _ZDZ0oBmqEeCeMPIFRfkCeQ" incomingStoryLinks="_W4ElgBmqEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_W4ElgBmqEeCeMPIFRfkCeQ" modifier="CREATE" source="_DtbVkBmqEeCeMPIFRfkCeQ" target="_UYNQ0BmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XVwnEBmqEeCeMPIFRfkCeQ" modifier="CREATE" source="_DtbVkBmqEeCeMPIFRfkCeQ" target="_K7iiEBmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XxwmkBmqEeCeMPIFRfkCeQ" modifier="CREATE" source="_UYNQ0BmqEeCeMPIFRfkCeQ" target="_EW7lkBmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZDZ0oBmqEeCeMPIFRfkCeQ" modifier="CREATE" source="_UYNQ0BmqEeCeMPIFRfkCeQ" target="_K7iiEBmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of iterateRulesSAN" uuid="_gtkD8BmqEeCeMPIFRfkCeQ" incoming="_jgL8kBmqEeCeMPIFRfkCeQ" outgoing="_nvX50BmqEeCeMPIFRfkCeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_vFmN0CovEeCjTcyrv1LVTA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_vZZTkCovEeCjTcyrv1LVTA">
          <activity href="../common/create_storyPattern_iterateTGGRules.story#_tDkSICrTEeCoHJjELNAVvA"/>
          <parameters name="storyActionNode" uuid="_wax4ECovEeCjTcyrv1LVTA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_zZlykCovEeCjTcyrv1LVTA" expressionString="iterateRulesSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create final node" uuid="_kajTEBmqEeCeMPIFRfkCeQ" incoming="_nvX50BmqEeCeMPIFRfkCeQ" outgoing="_k2zOoFOcEeCY5NYqK2RsYA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_oby68BmqEeCeMPIFRfkCeQ" outgoingStoryLinks="_RZwtQBmrEeCeMPIFRfkCeQ _Y77mUBo8EeC1-8m2yhOEkQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateRulesSAN" uuid="_p6fGgBmqEeCeMPIFRfkCeQ" incomingStoryLinks="_SFjcQBmrEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="finalNode" uuid="_Ob51ABmrEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_4xLjEBmsEeCeMPIFRfkCeQ" incomingStoryLinks="_S9QgUBmrEeCeMPIFRfkCeQ _Y77mUBo8EeC1-8m2yhOEkQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_P3KSEBmrEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_SFjcQBmrEeCeMPIFRfkCeQ _S9QgUBmrEeCeMPIFRfkCeQ" incomingStoryLinks="_RZwtQBmrEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_U-k30BmrEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WA0X8BmrEeCeMPIFRfkCeQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::END" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="falseExpression" uuid="_wygcUBmsEeCeMPIFRfkCeQ" modifier="CREATE" incomingStoryLinks="_4xLjEBmsEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_zmx6wBmsEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1N2mQBmsEeCeMPIFRfkCeQ" expressionString="'false'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_2VIXYBmsEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3VSz4BmsEeCeMPIFRfkCeQ" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RZwtQBmrEeCeMPIFRfkCeQ" modifier="CREATE" source="_oby68BmqEeCeMPIFRfkCeQ" target="_P3KSEBmrEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_SFjcQBmrEeCeMPIFRfkCeQ" modifier="CREATE" source="_P3KSEBmrEeCeMPIFRfkCeQ" target="_p6fGgBmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_S9QgUBmrEeCeMPIFRfkCeQ" modifier="CREATE" source="_P3KSEBmrEeCeMPIFRfkCeQ" target="_Ob51ABmrEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4xLjEBmsEeCeMPIFRfkCeQ" modifier="CREATE" source="_Ob51ABmrEeCeMPIFRfkCeQ" target="_wygcUBmsEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Y77mUBo8EeC1-8m2yhOEkQ" modifier="CREATE" source="_oby68BmqEeCeMPIFRfkCeQ" target="_Ob51ABmrEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to find conflicts" uuid="_yQtIIBo6EeC1-8m2yhOEkQ" incoming="_AtZRED5fEeCS07xt5ugfeQ" outgoing="_y2tiwBo7EeC1-8m2yhOEkQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findConflictsSAN" uuid="_3tbPgBo6EeC1-8m2yhOEkQ" modifier="CREATE" incomingStoryLinks="_N7M5QBo7EeC1-8m2yhOEkQ _9NUckD5eEeCS07xt5ugfeQ _kLvC8FOdEeCY5NYqK2RsYA _sRY7cFOdEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_6S_jkBo6EeC1-8m2yhOEkQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7drOABo6EeC1-8m2yhOEkQ" expressionString="'find conflicts with rule '.concat(rule.name)" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="__EwCYBo6EeC1-8m2yhOEkQ" outgoingStoryLinks="_N7M5QBo7EeC1-8m2yhOEkQ _8W2uoD5eEeCS07xt5ugfeQ _QB820EQeEeCj3vsEcE-cfw _mIPy8FOdEeCY5NYqK2RsYA _nUT9cFOdEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="decisionNode" uuid="_Dh6KMBo7EeC1-8m2yhOEkQ" incomingStoryLinks="_rbIo4FOdEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/DecisionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="successEdge" uuid="_wTfDYD5eEeCS07xt5ugfeQ" modifier="CREATE" outgoingStoryLinks="_9NUckD5eEeCS07xt5ugfeQ _-8fUID5eEeCS07xt5ugfeQ" incomingStoryLinks="_8W2uoD5eEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_4FBGAD5eEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5v890D5eEeCS07xt5ugfeQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="trueFinalNode" uuid="_y-q3QD5eEeCS07xt5ugfeQ" modifier="CREATE" outgoingStoryLinks="_lRWpQEQdEeCj3vsEcE-cfw" incomingStoryLinks="_-8fUID5eEeCS07xt5ugfeQ _QB820EQeEeCj3vsEcE-cfw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="trueExpression" uuid="_kMkLcEQdEeCj3vsEcE-cfw" modifier="CREATE" incomingStoryLinks="_lRWpQEQdEeCj3vsEcE-cfw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_kMkLc0QdEeCj3vsEcE-cfw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kMkLdEQdEeCj3vsEcE-cfw" expressionString="'true'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_kMkLcUQdEeCj3vsEcE-cfw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kMkLckQdEeCj3vsEcE-cfw" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mergeNode" uuid="_ehtfYFOdEeCY5NYqK2RsYA" incomingStoryLinks="_lJ6icFOdEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/MergeNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="typeCheckEdge" uuid="_hsCtYFOdEeCY5NYqK2RsYA" modifier="CREATE" outgoingStoryLinks="_rbIo4FOdEeCY5NYqK2RsYA _sRY7cFOdEeCY5NYqK2RsYA _0nHQQFOdEeCY5NYqK2RsYA" incomingStoryLinks="_nUT9cFOdEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_tdvZ0FOdEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_uaXaIFOdEeCY5NYqK2RsYA" expressionString="nodes::ActivityEdgeGuardEnumeration::BOOLEAN" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_iNmiUFOdEeCY5NYqK2RsYA" modifier="CREATE" outgoingStoryLinks="_kLvC8FOdEeCY5NYqK2RsYA _lJ6icFOdEeCY5NYqK2RsYA" incomingStoryLinks="_mIPy8FOdEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_n3BB4FOdEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ozyzMFOdEeCY5NYqK2RsYA" expressionString="nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="typeCheckExpression" uuid="_zIIw0FOdEeCY5NYqK2RsYA" modifier="CREATE" incomingStoryLinks="_0nHQQFOdEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_1XtmwFOdEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2MtVEFOdEeCY5NYqK2RsYA" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_3E0o0FOdEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_36aNAFOdEeCY5NYqK2RsYA" expressionString="'rule.oclIsKindOf('.concat(ri.ruleEClass.ePackage.name).concat('::').concat(ri.ruleEClass.name).concat(')')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_N7M5QBo7EeC1-8m2yhOEkQ" modifier="CREATE" source="__EwCYBo6EeC1-8m2yhOEkQ" target="_3tbPgBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8W2uoD5eEeCS07xt5ugfeQ" modifier="CREATE" source="__EwCYBo6EeC1-8m2yhOEkQ" target="_wTfDYD5eEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9NUckD5eEeCS07xt5ugfeQ" modifier="CREATE" source="_wTfDYD5eEeCS07xt5ugfeQ" target="_3tbPgBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_-8fUID5eEeCS07xt5ugfeQ" modifier="CREATE" source="_wTfDYD5eEeCS07xt5ugfeQ" target="_y-q3QD5eEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lRWpQEQdEeCj3vsEcE-cfw" modifier="CREATE" source="_y-q3QD5eEeCS07xt5ugfeQ" target="_kMkLcEQdEeCj3vsEcE-cfw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_QB820EQeEeCj3vsEcE-cfw" modifier="CREATE" source="__EwCYBo6EeC1-8m2yhOEkQ" target="_y-q3QD5eEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kLvC8FOdEeCY5NYqK2RsYA" modifier="CREATE" source="_iNmiUFOdEeCY5NYqK2RsYA" target="_3tbPgBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lJ6icFOdEeCY5NYqK2RsYA" modifier="CREATE" source="_iNmiUFOdEeCY5NYqK2RsYA" target="_ehtfYFOdEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_mIPy8FOdEeCY5NYqK2RsYA" modifier="CREATE" source="__EwCYBo6EeC1-8m2yhOEkQ" target="_iNmiUFOdEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_nUT9cFOdEeCY5NYqK2RsYA" modifier="CREATE" source="__EwCYBo6EeC1-8m2yhOEkQ" target="_hsCtYFOdEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_rbIo4FOdEeCY5NYqK2RsYA" modifier="CREATE" source="_hsCtYFOdEeCY5NYqK2RsYA" target="_Dh6KMBo7EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_sRY7cFOdEeCY5NYqK2RsYA" modifier="CREATE" source="_hsCtYFOdEeCY5NYqK2RsYA" target="_3tbPgBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0nHQQFOdEeCY5NYqK2RsYA" modifier="CREATE" source="_hsCtYFOdEeCY5NYqK2RsYA" target="_zIIw0FOdEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardExpression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of findConflictsSAN" uuid="_hm5n0Bo7EeC1-8m2yhOEkQ" incoming="_y2tiwBo7EeC1-8m2yhOEkQ" outgoing="_CGB38D5fEeCS07xt5ugfeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_9wNvgFOtEeCY5NYqK2RsYA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_-Q-6QFOtEeCY5NYqK2RsYA">
          <activity href="create_storyPattern_findConflictBetweenMatches.story#_cfNB0FOtEeCY5NYqK2RsYA"/>
          <parameters name="storyActionNode" uuid="__DVkQFOtEeCY5NYqK2RsYA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Bev2wFOuEeCY5NYqK2RsYA" expressionString="findConflictsSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="ruleInfoStore" uuid="_C6NvMFOuEeCY5NYqK2RsYA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_FLBUMFOuEeCY5NYqK2RsYA" expressionString="ri" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
          </parameters>
          <parameters name="direction" uuid="_GBHOsFOuEeCY5NYqK2RsYA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H2XgIFOuEeCY5NYqK2RsYA" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="iterate through all rules" uuid="_W6OKgD5dEeCS07xt5ugfeQ" incoming="_CGB38D5fEeCS07xt5ugfeQ _UJB0UFOdEeCY5NYqK2RsYA" outgoing="_AtZRED5fEeCS07xt5ugfeQ _WkRuwFOdEeCY5NYqK2RsYA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rule" uuid="_dRlVMD5dEeCS07xt5ugfeQ" incomingStoryLinks="_rfLcUD5dEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_c-knsD5fEeCS07xt5ugfeQ" expressionString="not self.isAxiom" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="genInfoStore" uuid="_j__G4D5dEeCS07xt5ugfeQ" outgoingStoryLinks="_qtSsYD5dEeCS07xt5ugfeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/GenerationInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ri" uuid="_o-HNwD5dEeCS07xt5ugfeQ" outgoingStoryLinks="_rfLcUD5dEeCS07xt5ugfeQ" incomingStoryLinks="_qtSsYD5dEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qtSsYD5dEeCS07xt5ugfeQ" source="_j__G4D5dEeCS07xt5ugfeQ" target="_o-HNwD5dEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/GenerationInfoStore/ruleInfos"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_rfLcUD5dEeCS07xt5ugfeQ" source="_o-HNwD5dEeCS07xt5ugfeQ" target="_dRlVMD5dEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/tggRule"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_1_bJEEQdEeCj3vsEcE-cfw" incoming="__RB5oFO7EeC7IfU7lGY1jQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to iterate through matched model elements" uuid="_6OGY0FObEeCY5NYqK2RsYA" incoming="_k2zOoFOcEeCY5NYqK2RsYA" outgoing="_n8HZwFOcEeCY5NYqK2RsYA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_86XKwFObEeCY5NYqK2RsYA" outgoingStoryLinks="_KriboFOcEeCY5NYqK2RsYA _Ny_UwFOcEeCY5NYqK2RsYA _d-mAsFOcEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateRulesSAN" uuid="_-patkFObEeCY5NYqK2RsYA" incomingStoryLinks="_LeOc0FOcEeCY5NYqK2RsYA _fsYYMFOcEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_B8OiIFOcEeCY5NYqK2RsYA" modifier="CREATE" outgoingStoryLinks="_LeOc0FOcEeCY5NYqK2RsYA _MY4HMFOcEeCY5NYqK2RsYA" incomingStoryLinks="_KriboFOcEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_V4xJEFOcEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WuqPQFOcEeCY5NYqK2RsYA" expressionString="nodes::ActivityEdgeGuardEnumeration::FOR_EACH" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateMatchedModelElementsSAN" uuid="_Cd2ogFOcEeCY5NYqK2RsYA" modifier="CREATE" incomingStoryLinks="_MY4HMFOcEeCY5NYqK2RsYA _Ny_UwFOcEeCY5NYqK2RsYA _elpZoFOcEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_O5Y8IFOcEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_P45RUFOcEeCY5NYqK2RsYA" expressionString="'iterate matched model elements'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_Rrl-AFOcEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/forEach"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SldlQFOcEeCY5NYqK2RsYA" expressionString="true" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_cSfEMFOcEeCY5NYqK2RsYA" modifier="CREATE" outgoingStoryLinks="_elpZoFOcEeCY5NYqK2RsYA _fsYYMFOcEeCY5NYqK2RsYA" incomingStoryLinks="_d-mAsFOcEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_gqtBoFOcEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hrXNYFOcEeCY5NYqK2RsYA" expressionString="nodes::ActivityEdgeGuardEnumeration::END" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_KriboFOcEeCY5NYqK2RsYA" modifier="CREATE" source="_86XKwFObEeCY5NYqK2RsYA" target="_B8OiIFOcEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LeOc0FOcEeCY5NYqK2RsYA" modifier="CREATE" source="_B8OiIFOcEeCY5NYqK2RsYA" target="_-patkFObEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_MY4HMFOcEeCY5NYqK2RsYA" modifier="CREATE" source="_B8OiIFOcEeCY5NYqK2RsYA" target="_Cd2ogFOcEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ny_UwFOcEeCY5NYqK2RsYA" modifier="CREATE" source="_86XKwFObEeCY5NYqK2RsYA" target="_Cd2ogFOcEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_d-mAsFOcEeCY5NYqK2RsYA" modifier="CREATE" source="_86XKwFObEeCY5NYqK2RsYA" target="_cSfEMFOcEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_elpZoFOcEeCY5NYqK2RsYA" modifier="CREATE" source="_cSfEMFOcEeCY5NYqK2RsYA" target="_Cd2ogFOcEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fsYYMFOcEeCY5NYqK2RsYA" modifier="CREATE" source="_cSfEMFOcEeCY5NYqK2RsYA" target="_-patkFObEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of iterateMatchedModelElementsSAN" uuid="_lV3JMFOcEeCY5NYqK2RsYA" incoming="_n8HZwFOcEeCY5NYqK2RsYA" outgoing="_0Pq2EFOcEeCY5NYqK2RsYA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_AFAywFOlEeCY5NYqK2RsYA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_Aj4gEFOlEeCY5NYqK2RsYA">
          <activity href="create_storyPattern_iterateMatchedModelElements.story#_rb-usFOjEeCY5NYqK2RsYA"/>
          <parameters name="storyActionNode" uuid="_CP_ckFOlEeCY5NYqK2RsYA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_FRCywFOlEeCY5NYqK2RsYA" expressionString="iterateMatchedModelElementsSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="direction" uuid="_K5UbUFPQEeCn1cdUG-DPzw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NAMBMFPQEeCn1cdUG-DPzw" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create decision node to branch according to type of rule" uuid="_xy54sFOcEeCY5NYqK2RsYA" incoming="_0Pq2EFOcEeCY5NYqK2RsYA" outgoing="_GkwXAFOdEeCY5NYqK2RsYA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_0jM2EFOcEeCY5NYqK2RsYA" outgoingStoryLinks="_ATsBkFOdEeCY5NYqK2RsYA _BuKbgFOdEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateMatchedModelElementsSAN" uuid="_1XFKEFOcEeCY5NYqK2RsYA" incomingStoryLinks="_CYCe8FOdEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_202voFOcEeCY5NYqK2RsYA" modifier="CREATE" outgoingStoryLinks="_CYCe8FOdEeCY5NYqK2RsYA _DUI60FOdEeCY5NYqK2RsYA" incomingStoryLinks="_ATsBkFOdEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_9Or_gFOcEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-KncQFOcEeCY5NYqK2RsYA" expressionString="nodes::ActivityEdgeGuardEnumeration::FOR_EACH" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="decisionNode" uuid="_3I67IFOcEeCY5NYqK2RsYA" modifier="CREATE" incomingStoryLinks="_BuKbgFOdEeCY5NYqK2RsYA _DUI60FOdEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/DecisionNode"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ATsBkFOdEeCY5NYqK2RsYA" modifier="CREATE" source="_0jM2EFOcEeCY5NYqK2RsYA" target="_202voFOcEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BuKbgFOdEeCY5NYqK2RsYA" modifier="CREATE" source="_0jM2EFOcEeCY5NYqK2RsYA" target="_3I67IFOcEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_CYCe8FOdEeCY5NYqK2RsYA" modifier="CREATE" source="_202voFOcEeCY5NYqK2RsYA" target="_1XFKEFOcEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_DUI60FOdEeCY5NYqK2RsYA" modifier="CREATE" source="_202voFOcEeCY5NYqK2RsYA" target="_3I67IFOcEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create merge node to join branches" uuid="_FENKQFOdEeCY5NYqK2RsYA" incoming="_GkwXAFOdEeCY5NYqK2RsYA" outgoing="_UJB0UFOdEeCY5NYqK2RsYA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_HiiN4FOdEeCY5NYqK2RsYA" outgoingStoryLinks="_NqylkFOdEeCY5NYqK2RsYA _Q4pBoFOdEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateMatchedModelElementsSAN" uuid="_IF3kcFOdEeCY5NYqK2RsYA" incomingStoryLinks="_Odd_sFOdEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_KK3iEFOdEeCY5NYqK2RsYA" modifier="CREATE" outgoingStoryLinks="_Odd_sFOdEeCY5NYqK2RsYA _P0RakFOdEeCY5NYqK2RsYA" incomingStoryLinks="_NqylkFOdEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mergeNode" uuid="_KwGWEFOdEeCY5NYqK2RsYA" modifier="CREATE" incomingStoryLinks="_P0RakFOdEeCY5NYqK2RsYA _Q4pBoFOdEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/MergeNode"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_NqylkFOdEeCY5NYqK2RsYA" modifier="CREATE" source="_HiiN4FOdEeCY5NYqK2RsYA" target="_KK3iEFOdEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Odd_sFOdEeCY5NYqK2RsYA" modifier="CREATE" source="_KK3iEFOdEeCY5NYqK2RsYA" target="_IF3kcFOdEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_P0RakFOdEeCY5NYqK2RsYA" modifier="CREATE" source="_KK3iEFOdEeCY5NYqK2RsYA" target="_KwGWEFOdEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Q4pBoFOdEeCY5NYqK2RsYA" modifier="CREATE" source="_HiiN4FOdEeCY5NYqK2RsYA" target="_KwGWEFOdEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set condition of last type check edge to else" uuid="_M9FPYFO7EeC7IfU7lGY1jQ" incoming="_WkRuwFOdEeCY5NYqK2RsYA" outgoing="__RB5oFO7EeC7IfU7lGY1jQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="typeCheckEdge" uuid="_QkHngFO7EeC7IfU7lGY1jQ" outgoingStoryLinks="_7rU-QFO7EeC7IfU7lGY1jQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_pNOrQFO7EeC7IfU7lGY1jQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_070ZUFO7EeC7IfU7lGY1jQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::ELSE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="expression" uuid="_5sghIFO7EeC7IfU7lGY1jQ" modifier="DESTROY" incomingStoryLinks="_7rU-QFO7EeC7IfU7lGY1jQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7rU-QFO7EeC7IfU7lGY1jQ" modifier="DESTROY" source="_QkHngFO7EeC7IfU7lGY1jQ" target="_5sghIFO7EeC7IfU7lGY1jQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardExpression"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_SHBt0BmpEeCeMPIFRfkCeQ" source="_85xs8BmoEeCeMPIFRfkCeQ" target="_9W5t0BmoEeCeMPIFRfkCeQ"/>
    <edges uuid="_SbF5UBmpEeCeMPIFRfkCeQ" source="_9W5t0BmoEeCeMPIFRfkCeQ" target="_Q8i3sBmpEeCeMPIFRfkCeQ"/>
    <edges uuid="_lcH6QBmpEeCeMPIFRfkCeQ" source="_Q8i3sBmpEeCeMPIFRfkCeQ" target="_huHXcBmpEeCeMPIFRfkCeQ"/>
    <edges uuid="_7RJFwBmpEeCeMPIFRfkCeQ" source="_huHXcBmpEeCeMPIFRfkCeQ" target="_5MnpQBmpEeCeMPIFRfkCeQ"/>
    <edges uuid="_CzrqIBmqEeCeMPIFRfkCeQ" source="_5MnpQBmpEeCeMPIFRfkCeQ" target="_8DaQMBmpEeCeMPIFRfkCeQ"/>
    <edges uuid="_jgL8kBmqEeCeMPIFRfkCeQ" source="_8DaQMBmpEeCeMPIFRfkCeQ" target="_gtkD8BmqEeCeMPIFRfkCeQ"/>
    <edges uuid="_nvX50BmqEeCeMPIFRfkCeQ" source="_gtkD8BmqEeCeMPIFRfkCeQ" target="_kajTEBmqEeCeMPIFRfkCeQ"/>
    <edges uuid="_y2tiwBo7EeC1-8m2yhOEkQ" source="_yQtIIBo6EeC1-8m2yhOEkQ" target="_hm5n0Bo7EeC1-8m2yhOEkQ"/>
    <edges uuid="_AtZRED5fEeCS07xt5ugfeQ" source="_W6OKgD5dEeCS07xt5ugfeQ" target="_yQtIIBo6EeC1-8m2yhOEkQ" guardType="FOR_EACH"/>
    <edges uuid="_CGB38D5fEeCS07xt5ugfeQ" source="_hm5n0Bo7EeC1-8m2yhOEkQ" target="_W6OKgD5dEeCS07xt5ugfeQ"/>
    <edges uuid="_k2zOoFOcEeCY5NYqK2RsYA" source="_kajTEBmqEeCeMPIFRfkCeQ" target="_6OGY0FObEeCY5NYqK2RsYA"/>
    <edges uuid="_n8HZwFOcEeCY5NYqK2RsYA" source="_6OGY0FObEeCY5NYqK2RsYA" target="_lV3JMFOcEeCY5NYqK2RsYA"/>
    <edges uuid="_0Pq2EFOcEeCY5NYqK2RsYA" source="_lV3JMFOcEeCY5NYqK2RsYA" target="_xy54sFOcEeCY5NYqK2RsYA"/>
    <edges uuid="_GkwXAFOdEeCY5NYqK2RsYA" source="_xy54sFOcEeCY5NYqK2RsYA" target="_FENKQFOdEeCY5NYqK2RsYA"/>
    <edges uuid="_UJB0UFOdEeCY5NYqK2RsYA" source="_FENKQFOdEeCY5NYqK2RsYA" target="_W6OKgD5dEeCS07xt5ugfeQ"/>
    <edges uuid="_WkRuwFOdEeCY5NYqK2RsYA" source="_W6OKgD5dEeCS07xt5ugfeQ" target="_M9FPYFO7EeC7IfU7lGY1jQ" guardType="END"/>
    <edges uuid="__RB5oFO7EeC7IfU7lGY1jQ" source="_M9FPYFO7EeC7IfU7lGY1jQ" target="_1_bJEEQdEeCj3vsEcE-cfw"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
