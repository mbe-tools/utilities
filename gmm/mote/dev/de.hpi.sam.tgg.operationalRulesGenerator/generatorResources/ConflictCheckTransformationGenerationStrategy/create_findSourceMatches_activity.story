<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_findSourceMatches_activity" uuid="_LMuL4EPfEeCIrIF3qEr5xA">
  <activities name="create_findSourceMatches_activity" uuid="_MBPZEEPfEeCIrIF3qEr5xA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_OWARAEPfEeCIrIF3qEr5xA" outgoing="_mXcQwEPfEeCIrIF3qEr5xA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_OwtQAEPfEeCIrIF3qEr5xA" incoming="_mXcQwEPfEeCIrIF3qEr5xA" outgoing="_mxr8wEPfEeCIrIF3qEr5xA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_RYPS8EPfEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_R-7fgEPfEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_Szeh4EPfEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_TVbYYEPfEeCIrIF3qEr5xA" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create initial node" uuid="_fURI8EPfEeCIrIF3qEr5xA" incoming="_mxr8wEPfEeCIrIF3qEr5xA" outgoing="_6GGMsEPfEeCIrIF3qEr5xA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_gC9a8EPfEeCIrIF3qEr5xA" outgoingStoryLinks="_kkK8UEPfEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_gflskEPfEeCIrIF3qEr5xA" modifier="CREATE" incomingStoryLinks="_kkK8UEPfEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kkK8UEPfEeCIrIF3qEr5xA" modifier="CREATE" source="_gC9a8EPfEeCIrIF3qEr5xA" target="_gflskEPfEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to create match store object" uuid="_oZpZEEPfEeCIrIF3qEr5xA" incoming="_6GGMsEPfEeCIrIF3qEr5xA" outgoing="_Bhlt4EPgEeCIrIF3qEr5xA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_qIaoAEPfEeCIrIF3qEr5xA" outgoingStoryLinks="_1QoSkEPfEeCIrIF3qEr5xA _1ygQkEPfEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_rRz5AEPfEeCIrIF3qEr5xA" incomingStoryLinks="_2IVWoEPfEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_r48KcEPfEeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_2IVWoEPfEeCIrIF3qEr5xA _3X6IIEPfEeCIrIF3qEr5xA" incomingStoryLinks="_1QoSkEPfEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createMatchStoreSAN" uuid="_sOAbgEPfEeCIrIF3qEr5xA" modifier="CREATE" incomingStoryLinks="_1ygQkEPfEeCIrIF3qEr5xA _3X6IIEPfEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_xWNEYEPfEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_x-B5YEPfEeCIrIF3qEr5xA" expressionString="'create match store'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1QoSkEPfEeCIrIF3qEr5xA" modifier="CREATE" source="_qIaoAEPfEeCIrIF3qEr5xA" target="_r48KcEPfEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1ygQkEPfEeCIrIF3qEr5xA" modifier="CREATE" source="_qIaoAEPfEeCIrIF3qEr5xA" target="_sOAbgEPfEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2IVWoEPfEeCIrIF3qEr5xA" modifier="CREATE" source="_r48KcEPfEeCIrIF3qEr5xA" target="_rRz5AEPfEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3X6IIEPfEeCIrIF3qEr5xA" modifier="CREATE" source="_r48KcEPfEeCIrIF3qEr5xA" target="_sOAbgEPfEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of createMatchStoreSAN" uuid="_-kXHwEPfEeCIrIF3qEr5xA" incoming="_Bhlt4EPgEeCIrIF3qEr5xA" outgoing="_VVJX8EPgEeCIrIF3qEr5xA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_B95PYEPgEeCIrIF3qEr5xA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_F5MnUEPgEeCIrIF3qEr5xA">
          <activity href="../common/create_matchStore_spo.story#_X128ABgAEeCaHaBYHoSzPw"/>
          <parameters name="storyActionNode" uuid="_G_s8YEPgEeCIrIF3qEr5xA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_J4B0YEPgEeCIrIF3qEr5xA" expressionString="createMatchStoreSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_HGQQUEPgEeCIrIF3qEr5xA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_PceUYEPgEeCIrIF3qEr5xA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_HLTIYEPgEeCIrIF3qEr5xA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SdB7UEPgEeCIrIF3qEr5xA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_U560gEPgEeCIrIF3qEr5xA" incoming="_k6F_oEP4EeCIrIF3qEr5xA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set last node" uuid="_f3BpAEPlEeCIrIF3qEr5xA" incoming="_VVJX8EPgEeCIrIF3qEr5xA" outgoing="_QZ13YEPnEeCIrIF3qEr5xA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="lastNode" uuid="_gbH0kEPlEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityNode"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_y7qXkEPlEeCIrIF3qEr5xA" expressionString="createMatchStoreSAN" expressionLanguage="OCL"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="iterate through created source elements" uuid="_6LbJ4EPlEeCIrIF3qEr5xA" incoming="_QZ13YEPnEeCIrIF3qEr5xA _6cp-oEP3EeCIrIF3qEr5xA" outgoing="_iYP9sEPnEeCIrIF3qEr5xA _CyUCAEP4EeCIrIF3qEr5xA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_-EqsUEPlEeCIrIF3qEr5xA" outgoingStoryLinks="_Cu_GYEPmEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelDomain" uuid="_-0C60EPlEeCIrIF3qEr5xA" outgoingStoryLinks="_EwENUEPmEeCIrIF3qEr5xA" incomingStoryLinks="_Cu_GYEPmEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_FhMIQEPmEeCIrIF3qEr5xA" expressionString="direction = mote::TransformationDirection::FORWARD and modelDomain.oclIsKindOf(tgg::SourceModelDomain) or&#xA;direction = mote::TransformationDirection::MAPPING or&#xA;direction = mote::TransformationDirection::REVERSE and modelDomain.oclIsKindOf(tgg::TargetModelDomain)" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="__gZqgEPlEeCIrIF3qEr5xA" incomingStoryLinks="_EwENUEPmEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-vi_4EPmEeCIrIF3qEr5xA" expressionString="self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_Cu_GYEPmEeCIrIF3qEr5xA" source="_-EqsUEPlEeCIrIF3qEr5xA" target="_-0C60EPlEeCIrIF3qEr5xA"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_EwENUEPmEeCIrIF3qEr5xA" source="_-0C60EPlEeCIrIF3qEr5xA" target="__gZqgEPlEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to find matches" uuid="_R8n4wEPnEeCIrIF3qEr5xA" incoming="_iYP9sEPnEeCIrIF3qEr5xA" outgoing="_-HyKYEPoEeCIrIF3qEr5xA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_U5AKUEPnEeCIrIF3qEr5xA" outgoingStoryLinks="_bTwnQEPnEeCIrIF3qEr5xA _cMmTwEPnEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="lastNode" uuid="_VbjdwEPnEeCIrIF3qEr5xA" incomingStoryLinks="_fXwoMEPnEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_V_3rwEPnEeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_fXwoMEPnEeCIrIF3qEr5xA _g54iQEPnEeCIrIF3qEr5xA" incomingStoryLinks="_bTwnQEPnEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_mOvuIEPnEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_msy8EEPnEeCIrIF3qEr5xA" expressionString="if lastNode = createMatchStoreSAN then&#xA;&#x9;storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::NONE&#xA;else&#xA;&#x9;storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::END&#xA;endif" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesSAN" uuid="_WTP6wEPnEeCIrIF3qEr5xA" modifier="CREATE" incomingStoryLinks="_cMmTwEPnEeCIrIF3qEr5xA _g54iQEPnEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_2VcPcEPoEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_28tq0EPoEeCIrIF3qEr5xA" expressionString="'find matches'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_423qYEPoEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/forEach"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6XS7UEPoEeCIrIF3qEr5xA" expressionString="true" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_bTwnQEPnEeCIrIF3qEr5xA" modifier="CREATE" source="_U5AKUEPnEeCIrIF3qEr5xA" target="_V_3rwEPnEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_cMmTwEPnEeCIrIF3qEr5xA" modifier="CREATE" source="_U5AKUEPnEeCIrIF3qEr5xA" target="_WTP6wEPnEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fXwoMEPnEeCIrIF3qEr5xA" modifier="CREATE" source="_V_3rwEPnEeCIrIF3qEr5xA" target="_VbjdwEPnEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_g54iQEPnEeCIrIF3qEr5xA" modifier="CREATE" source="_V_3rwEPnEeCIrIF3qEr5xA" target="_WTP6wEPnEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of findMatchesSAN" uuid="_8VuW4EPoEeCIrIF3qEr5xA" incoming="_-HyKYEPoEeCIrIF3qEr5xA" outgoing="_4Ih1UEP2EeCIrIF3qEr5xA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_px5xMEP6EeCxT7Y0iFA1WA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_qNNNIEP6EeCxT7Y0iFA1WA">
          <activity href="create_storyPattern_findSourceMatches.story#_J_YCcBgCEeCaHaBYHoSzPw"/>
          <parameters name="tggRule" uuid="_rgSloEP6EeCxT7Y0iFA1WA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tjNfoEP6EeCxT7Y0iFA1WA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_rpI_oEP6EeCxT7Y0iFA1WA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_zSZ3EEP6EeCxT7Y0iFA1WA" expressionString="findMatchesSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="direction" uuid="_rsWEoEP6EeCxT7Y0iFA1WA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2eGXAEP6EeCxT7Y0iFA1WA" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="modelObject" uuid="_rxiGoEP6EeCxT7Y0iFA1WA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_4h2-gEP6EeCxT7Y0iFA1WA" expressionString="modelObject" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to evaluate rule variables" uuid="_keswAEPrEeCIrIF3qEr5xA" incoming="_4Ih1UEP2EeCIrIF3qEr5xA" outgoing="_6BdF0EP2EeCIrIF3qEr5xA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_nkZVoEPrEeCIrIF3qEr5xA" outgoingStoryLinks="_33Nc0EPrEeCIrIF3qEr5xA _5cc8UEPrEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesSAN" uuid="_ok3UIEPrEeCIrIF3qEr5xA" incomingStoryLinks="_6KTg0EPrEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evaluateRuleVarsEAN" uuid="_p6UEIEPrEeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_uCxWMEQKEeCj3vsEcE-cfw" incomingStoryLinks="_5cc8UEPrEeCIrIF3qEr5xA _9q1BQEPrEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_0KeFUEPrEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1JQBwEPrEeCIrIF3qEr5xA" expressionString="'evaluate rule variables'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_rA7t8EPrEeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_6KTg0EPrEeCIrIF3qEr5xA _9q1BQEPrEeCIrIF3qEr5xA" incomingStoryLinks="_33Nc0EPrEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_wFoW4EPrEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xIm20EPrEeCIrIF3qEr5xA" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FOR_EACH" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="expression" uuid="_rssPoEQKEeCj3vsEcE-cfw" incomingStoryLinks="_uCxWMEQKEeCj3vsEcE-cfw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_xD16gEQKEeCj3vsEcE-cfw">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_xVAjAEQKEeCj3vsEcE-cfw">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
            <activity href="../common/create_evaluateRuleVariables_expression.story#_uabxYfZwEd-i2JdGlqLvVQ"/>
            <parameters name="tggRule" uuid="_zVB6AEQKEeCj3vsEcE-cfw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2egzcEQKEeCj3vsEcE-cfw" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_zYCKsEQKEeCj3vsEcE-cfw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-n5VYEQKEeCj3vsEcE-cfw" expressionString="direction" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_33Nc0EPrEeCIrIF3qEr5xA" modifier="CREATE" source="_nkZVoEPrEeCIrIF3qEr5xA" target="_rA7t8EPrEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5cc8UEPrEeCIrIF3qEr5xA" modifier="CREATE" source="_nkZVoEPrEeCIrIF3qEr5xA" target="_p6UEIEPrEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_6KTg0EPrEeCIrIF3qEr5xA" modifier="CREATE" source="_rA7t8EPrEeCIrIF3qEr5xA" target="_ok3UIEPrEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9q1BQEPrEeCIrIF3qEr5xA" modifier="CREATE" source="_rA7t8EPrEeCIrIF3qEr5xA" target="_p6UEIEPrEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_uCxWMEQKEeCj3vsEcE-cfw" modifier="CREATE" source="_p6UEIEPrEeCIrIF3qEr5xA" target="_rssPoEQKEeCj3vsEcE-cfw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to check constraints" uuid="_ED-UoEPsEeCIrIF3qEr5xA" incoming="_6BdF0EP2EeCIrIF3qEr5xA" outgoing="_-uzlwEP2EeCIrIF3qEr5xA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_GSy8oEPsEeCIrIF3qEr5xA" outgoingStoryLinks="_1MOyYEPtEeCIrIF3qEr5xA _2UyV4EPtEeCIrIF3qEr5xA _3P6hYEPtEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evaluateRuleVarsEAN" uuid="_apghoEPtEeCIrIF3qEr5xA" incomingStoryLinks="_wDaecEPtEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkConstraintsSAN" uuid="_cb33IEPtEeCIrIF3qEr5xA" modifier="CREATE" incomingStoryLinks="_xXQE4EPtEeCIrIF3qEr5xA _ynwDcEPtEeCIrIF3qEr5xA _3P6hYEPtEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_rJ6_8EPtEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sRtHYEPtEeCIrIF3qEr5xA" expressionString="'check constraints'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesSAN" uuid="_eYTHkEPtEeCIrIF3qEr5xA" incomingStoryLinks="_z1QYYEPtEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_fFxRkEPtEeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_wDaecEPtEeCIrIF3qEr5xA _xXQE4EPtEeCIrIF3qEr5xA" incomingStoryLinks="_1MOyYEPtEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_fa-skEPtEeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_ynwDcEPtEeCIrIF3qEr5xA _z1QYYEPtEeCIrIF3qEr5xA" incomingStoryLinks="_2UyV4EPtEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_nO7KAEPtEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_oVty8EPtEeCIrIF3qEr5xA" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_wDaecEPtEeCIrIF3qEr5xA" modifier="CREATE" source="_fFxRkEPtEeCIrIF3qEr5xA" target="_apghoEPtEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xXQE4EPtEeCIrIF3qEr5xA" modifier="CREATE" source="_fFxRkEPtEeCIrIF3qEr5xA" target="_cb33IEPtEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ynwDcEPtEeCIrIF3qEr5xA" modifier="CREATE" source="_fa-skEPtEeCIrIF3qEr5xA" target="_cb33IEPtEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_z1QYYEPtEeCIrIF3qEr5xA" modifier="CREATE" source="_fa-skEPtEeCIrIF3qEr5xA" target="_eYTHkEPtEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1MOyYEPtEeCIrIF3qEr5xA" modifier="CREATE" source="_GSy8oEPsEeCIrIF3qEr5xA" target="_fFxRkEPtEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2UyV4EPtEeCIrIF3qEr5xA" modifier="CREATE" source="_GSy8oEPsEeCIrIF3qEr5xA" target="_fa-skEPtEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3P6hYEPtEeCIrIF3qEr5xA" modifier="CREATE" source="_GSy8oEPsEeCIrIF3qEr5xA" target="_cb33IEPtEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of checkConstraintsSAN" uuid="_8-8xsEP2EeCIrIF3qEr5xA" incoming="_-uzlwEP2EeCIrIF3qEr5xA" outgoing="_DqRaMEP3EeCIrIF3qEr5xA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_S5PHoEQLEeCj3vsEcE-cfw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_TJRuwEQLEeCj3vsEcE-cfw">
          <activity href="create_storyPattern_check_constraints.story#_2MC_APbdEd-JnYtEWuN3-g"/>
          <parameters name="tggRule" uuid="_T6PRoEQLEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_euOYoEQLEeCj3vsEcE-cfw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_U4HPIEQLEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_f4InAEQLEeCj3vsEcE-cfw" expressionString="checkConstraintsSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="createSourceConstraints" uuid="_Xp4MIEQLEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hGaYAEQLEeCj3vsEcE-cfw" expressionString="direction = mote::TransformationDirection::FORWARD or&#xA;direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetConstraints" uuid="_ZUvygEQLEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_iIQPgEQLEeCj3vsEcE-cfw" expressionString="direction = mote::TransformationDirection::REVERSE or&#xA;direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentNodesConstraints" uuid="_bxKKkEQLEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jQWgAEQLEeCj3vsEcE-cfw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedNodesConstraints" uuid="_c2NHEEQLEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kjXAAEQLEeCj3vsEcE-cfw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to create match object" uuid="_CB5HIEP3EeCIrIF3qEr5xA" incoming="_DqRaMEP3EeCIrIF3qEr5xA" outgoing="_2x0roEP3EeCIrIF3qEr5xA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_FqIZIEP3EeCIrIF3qEr5xA" outgoingStoryLinks="_b9Qc8EP3EeCIrIF3qEr5xA _ekyf4EP3EeCIrIF3qEr5xA _fR4PYEP3EeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkConstraintsSAN" uuid="_Krn9EEP3EeCIrIF3qEr5xA" incomingStoryLinks="_cf-IcEP3EeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_MnraEEP3EeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_cf-IcEP3EeCIrIF3qEr5xA _hwME4EP3EeCIrIF3qEr5xA" incomingStoryLinks="_b9Qc8EP3EeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_YTfg8EP3EeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZZhU4EP3EeCIrIF3qEr5xA" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createMatchSAN" uuid="_NTn6EEP3EeCIrIF3qEr5xA" modifier="CREATE" incomingStoryLinks="_ekyf4EP3EeCIrIF3qEr5xA _hwME4EP3EeCIrIF3qEr5xA _j3iL4EP3EeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_UOCHcEP3EeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Vp9S4EP3EeCIrIF3qEr5xA" expressionString="'create match object'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesSAN" uuid="_PgjrsEP3EeCIrIF3qEr5xA" incomingStoryLinks="_lVNq0EP3EeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_QPX5gEP3EeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_j3iL4EP3EeCIrIF3qEr5xA _lVNq0EP3EeCIrIF3qEr5xA" incomingStoryLinks="_fR4PYEP3EeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_b9Qc8EP3EeCIrIF3qEr5xA" modifier="CREATE" source="_FqIZIEP3EeCIrIF3qEr5xA" target="_MnraEEP3EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_cf-IcEP3EeCIrIF3qEr5xA" modifier="CREATE" source="_MnraEEP3EeCIrIF3qEr5xA" target="_Krn9EEP3EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ekyf4EP3EeCIrIF3qEr5xA" modifier="CREATE" source="_FqIZIEP3EeCIrIF3qEr5xA" target="_NTn6EEP3EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fR4PYEP3EeCIrIF3qEr5xA" modifier="CREATE" source="_FqIZIEP3EeCIrIF3qEr5xA" target="_QPX5gEP3EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hwME4EP3EeCIrIF3qEr5xA" modifier="CREATE" source="_MnraEEP3EeCIrIF3qEr5xA" target="_NTn6EEP3EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_j3iL4EP3EeCIrIF3qEr5xA" modifier="CREATE" source="_QPX5gEP3EeCIrIF3qEr5xA" target="_NTn6EEP3EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lVNq0EP3EeCIrIF3qEr5xA" modifier="CREATE" source="_QPX5gEP3EeCIrIF3qEr5xA" target="_PgjrsEP3EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of createMatchSAN" uuid="_ukLUwEP3EeCIrIF3qEr5xA" incoming="_2x0roEP3EeCIrIF3qEr5xA" outgoing="_3QasIEP3EeCIrIF3qEr5xA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_YWV0EEQMEeCj3vsEcE-cfw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_YqPAcEQMEeCj3vsEcE-cfw">
          <activity href="create_storyPattern_createNewMatch.story#_0VoY8BioEeCm4IbFa7I4cA"/>
          <parameters name="tggRule" uuid="_aPUH4EQMEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_e1gP4EQMEeCj3vsEcE-cfw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_aTqcYEQMEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gqOV0EQMEeCj3vsEcE-cfw" expressionString="createMatchSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="direction" uuid="_aYFpYEQMEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lmEkwEQMEeCj3vsEcE-cfw" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set last node" uuid="_xfZIsEP3EeCIrIF3qEr5xA" incoming="_3QasIEP3EeCIrIF3qEr5xA" outgoing="_6cp-oEP3EeCIrIF3qEr5xA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="lastNode" uuid="_yLjrIEP3EeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityNode"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0UMLkEP3EeCIrIF3qEr5xA" expressionString="findMatchesSAN" expressionLanguage="OCL"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create final node" uuid="__lYzAEP3EeCIrIF3qEr5xA" incoming="_CyUCAEP4EeCIrIF3qEr5xA" outgoing="_k6F_oEP4EeCIrIF3qEr5xA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_E9KwAEP4EeCIrIF3qEr5xA" outgoingStoryLinks="_bF_BQEP4EeCIrIF3qEr5xA _b9mlwEP4EeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="lastNode" uuid="_FadI8EP4EeCIrIF3qEr5xA" incomingStoryLinks="_cjSswEP4EeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_GKv9cEP4EeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_cjSswEP4EeCIrIF3qEr5xA _djiBwEP4EeCIrIF3qEr5xA" incomingStoryLinks="_bF_BQEP4EeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_gKTPsEP4EeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hQyWoEP4EeCIrIF3qEr5xA" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::END" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="finalNode" uuid="_GYQN8EP4EeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_ZNhq0EP4EeCIrIF3qEr5xA" incomingStoryLinks="_b9mlwEP4EeCIrIF3qEr5xA _djiBwEP4EeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_HemK8EP4EeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_aKLgUEP4EeCIrIF3qEr5xA" incomingStoryLinks="_ZNhq0EP4EeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varRef" uuid="_HwJN8EP4EeCIrIF3qEr5xA" modifier="CREATE" incomingStoryLinks="_aKLgUEP4EeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_S86BUEP4EeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_UIWgwEP4EeCIrIF3qEr5xA" expressionString="'matchStore'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_VcNVUEP4EeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XLgIsEP4EeCIrIF3qEr5xA" expressionString="mote::rules::MatchStorage" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZNhq0EP4EeCIrIF3qEr5xA" modifier="CREATE" source="_GYQN8EP4EeCIrIF3qEr5xA" target="_HemK8EP4EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_aKLgUEP4EeCIrIF3qEr5xA" modifier="CREATE" source="_HemK8EP4EeCIrIF3qEr5xA" target="_HwJN8EP4EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_bF_BQEP4EeCIrIF3qEr5xA" modifier="CREATE" source="_E9KwAEP4EeCIrIF3qEr5xA" target="_GKv9cEP4EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_b9mlwEP4EeCIrIF3qEr5xA" modifier="CREATE" source="_E9KwAEP4EeCIrIF3qEr5xA" target="_GYQN8EP4EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_cjSswEP4EeCIrIF3qEr5xA" modifier="CREATE" source="_GKv9cEP4EeCIrIF3qEr5xA" target="_FadI8EP4EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_djiBwEP4EeCIrIF3qEr5xA" modifier="CREATE" source="_GKv9cEP4EeCIrIF3qEr5xA" target="_GYQN8EP4EeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_mXcQwEPfEeCIrIF3qEr5xA" source="_OWARAEPfEeCIrIF3qEr5xA" target="_OwtQAEPfEeCIrIF3qEr5xA"/>
    <edges uuid="_mxr8wEPfEeCIrIF3qEr5xA" source="_OwtQAEPfEeCIrIF3qEr5xA" target="_fURI8EPfEeCIrIF3qEr5xA"/>
    <edges uuid="_6GGMsEPfEeCIrIF3qEr5xA" source="_fURI8EPfEeCIrIF3qEr5xA" target="_oZpZEEPfEeCIrIF3qEr5xA"/>
    <edges uuid="_Bhlt4EPgEeCIrIF3qEr5xA" source="_oZpZEEPfEeCIrIF3qEr5xA" target="_-kXHwEPfEeCIrIF3qEr5xA"/>
    <edges uuid="_VVJX8EPgEeCIrIF3qEr5xA" source="_-kXHwEPfEeCIrIF3qEr5xA" target="_f3BpAEPlEeCIrIF3qEr5xA"/>
    <edges uuid="_QZ13YEPnEeCIrIF3qEr5xA" source="_f3BpAEPlEeCIrIF3qEr5xA" target="_6LbJ4EPlEeCIrIF3qEr5xA"/>
    <edges uuid="_iYP9sEPnEeCIrIF3qEr5xA" source="_6LbJ4EPlEeCIrIF3qEr5xA" target="_R8n4wEPnEeCIrIF3qEr5xA" guardType="FOR_EACH"/>
    <edges uuid="_-HyKYEPoEeCIrIF3qEr5xA" source="_R8n4wEPnEeCIrIF3qEr5xA" target="_8VuW4EPoEeCIrIF3qEr5xA"/>
    <edges uuid="_4Ih1UEP2EeCIrIF3qEr5xA" source="_8VuW4EPoEeCIrIF3qEr5xA" target="_keswAEPrEeCIrIF3qEr5xA"/>
    <edges uuid="_6BdF0EP2EeCIrIF3qEr5xA" source="_keswAEPrEeCIrIF3qEr5xA" target="_ED-UoEPsEeCIrIF3qEr5xA"/>
    <edges uuid="_-uzlwEP2EeCIrIF3qEr5xA" source="_ED-UoEPsEeCIrIF3qEr5xA" target="_8-8xsEP2EeCIrIF3qEr5xA"/>
    <edges uuid="_DqRaMEP3EeCIrIF3qEr5xA" source="_8-8xsEP2EeCIrIF3qEr5xA" target="_CB5HIEP3EeCIrIF3qEr5xA"/>
    <edges uuid="_2x0roEP3EeCIrIF3qEr5xA" source="_CB5HIEP3EeCIrIF3qEr5xA" target="_ukLUwEP3EeCIrIF3qEr5xA"/>
    <edges uuid="_3QasIEP3EeCIrIF3qEr5xA" source="_ukLUwEP3EeCIrIF3qEr5xA" target="_xfZIsEP3EeCIrIF3qEr5xA"/>
    <edges uuid="_6cp-oEP3EeCIrIF3qEr5xA" source="_xfZIsEP3EeCIrIF3qEr5xA" target="_6LbJ4EPlEeCIrIF3qEr5xA"/>
    <edges uuid="_CyUCAEP4EeCIrIF3qEr5xA" source="_6LbJ4EPlEeCIrIF3qEr5xA" target="__lYzAEP3EeCIrIF3qEr5xA" guardType="END"/>
    <edges uuid="_k6F_oEP4EeCIrIF3qEr5xA" source="__lYzAEP3EeCIrIF3qEr5xA" target="_U560gEPgEeCIrIF3qEr5xA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
