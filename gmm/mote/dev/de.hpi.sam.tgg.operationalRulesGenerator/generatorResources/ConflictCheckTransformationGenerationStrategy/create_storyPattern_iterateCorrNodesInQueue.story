<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_iterateCorrNodesInQueue" uuid="_toc_YCn4EeCF_aVH6mO9gA">
  <activities name="create_storyPattern_iterateCorrNodesInQueue" uuid="_uYqUUCn4EeCF_aVH6mO9gA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_wWf6ACn4EeCF_aVH6mO9gA" outgoing="_ztMwECn4EeCF_aVH6mO9gA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_xJh5cCn4EeCF_aVH6mO9gA" incoming="_ztMwECn4EeCF_aVH6mO9gA" outgoing="_EcpTcCn6EeC1o_ZuFAaWCA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_z-JWICn4EeCF_aVH6mO9gA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_RmDPICn5EeCF_aVH6mO9gA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create this and ruleSet SPOs" uuid="_8PLYMCn4EeCF_aVH6mO9gA" incoming="_EcpTcCn6EeC1o_ZuFAaWCA" outgoing="_YcftECn6EeC1o_ZuFAaWCA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisSpo" uuid="__4Q-wCn4EeCF_aVH6mO9gA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_JmRVICn5EeCF_aVH6mO9gA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_KB0BoCn5EeCF_aVH6mO9gA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_rule_spo.story#_Kpsp0AwlEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_LyNBMCn5EeCF_aVH6mO9gA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OUcUcCn5EeCF_aVH6mO9gA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="isAxiom" uuid="_PVPDECn5EeCF_aVH6mO9gA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_V5XecCn5EeCF_aVH6mO9gA" expressionString="tggRule.isAxiom" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_CL5OQCn5EeCF_aVH6mO9gA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_3RNz8Cn5EeC1o_ZuFAaWCA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_3zA5cCn5EeC1o_ZuFAaWCA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_ruleSet_spo.story#_ZDP-wAwkEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_8hqZ4Cn5EeC1o_ZuFAaWCA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-s9MwCn5EeC1o_ZuFAaWCA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="bindingType" uuid="_ABfWwCn6EeC1o_ZuFAaWCA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BWQKQCn6EeC1o_ZuFAaWCA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create rule->ruleSet link" uuid="_IPiWQCn6EeC1o_ZuFAaWCA" incoming="_YcftECn6EeC1o_ZuFAaWCA" outgoing="_aG_gACn6EeC1o_ZuFAaWCA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_LXkeMCn6EeC1o_ZuFAaWCA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_L1FgoCn6EeC1o_ZuFAaWCA">
          <activity href="../common/create_rule_ruleSet_link.story#_qOEaEAwmEeCU3uEhqcP0Dg"/>
          <parameters name="ruleSpo" uuid="_My4loCn6EeC1o_ZuFAaWCA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_PAj-ICn6EeC1o_ZuFAaWCA" expressionString="thisSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_P4oOkCn6EeC1o_ZuFAaWCA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SSIckCn6EeC1o_ZuFAaWCA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_ZqETcCn6EeC1o_ZuFAaWCA" incoming="_WzUocCn9EeC1o_ZuFAaWCA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create transformation queue SPO" uuid="_6-EDsCn6EeC1o_ZuFAaWCA" incoming="_aG_gACn6EeC1o_ZuFAaWCA" outgoing="_yI_rACn7EeC1o_ZuFAaWCA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="__OCEECn6EeC1o_ZuFAaWCA" outgoingStoryLinks="_ikcYACn7EeC1o_ZuFAaWCA _mSnS4Cn7EeC1o_ZuFAaWCA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_Br84ACn7EeC1o_ZuFAaWCA" incomingStoryLinks="_jbTHgCn7EeC1o_ZuFAaWCA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformationQueueSpo" uuid="_Chf_8Cn7EeC1o_ZuFAaWCA" modifier="CREATE" outgoingStoryLinks="_BTXjcCn8EeC1o_ZuFAaWCA" incomingStoryLinks="_kfuY8Cn7EeC1o_ZuFAaWCA _mSnS4Cn7EeC1o_ZuFAaWCA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_nfbrUCn7EeC1o_ZuFAaWCA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ofbIsCn7EeC1o_ZuFAaWCA" expressionString="'transformationQueue'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformationQueueSpl" uuid="_DWrUcCn7EeC1o_ZuFAaWCA" modifier="CREATE" outgoingStoryLinks="_fM7poCn7EeC1o_ZuFAaWCA _jbTHgCn7EeC1o_ZuFAaWCA _kfuY8Cn7EeC1o_ZuFAaWCA" incomingStoryLinks="_ikcYACn7EeC1o_ZuFAaWCA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetEClass" uuid="_FCWMECn7EeC1o_ZuFAaWCA" outgoingStoryLinks="_dUBAMCn7EeC1o_ZuFAaWCA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_X6sLECn7EeC1o_ZuFAaWCA" expressionString="mote::rules::TGGRuleSet" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformationQueueEReference" uuid="_GSRi4Cn7EeC1o_ZuFAaWCA" incomingStoryLinks="_dUBAMCn7EeC1o_ZuFAaWCA _fM7poCn7EeC1o_ZuFAaWCA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gPrgECn7EeC1o_ZuFAaWCA" expressionString="self.name = 'transformationQueue'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformationQueueEClass" uuid="_5xE2sCn7EeC1o_ZuFAaWCA" incomingStoryLinks="_BTXjcCn8EeC1o_ZuFAaWCA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__vn4wCn7EeC1o_ZuFAaWCA" expressionString="mote::rules::TransformationQueue" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_dUBAMCn7EeC1o_ZuFAaWCA" source="_FCWMECn7EeC1o_ZuFAaWCA" target="_GSRi4Cn7EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fM7poCn7EeC1o_ZuFAaWCA" modifier="CREATE" source="_DWrUcCn7EeC1o_ZuFAaWCA" target="_GSRi4Cn7EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ikcYACn7EeC1o_ZuFAaWCA" modifier="CREATE" source="__OCEECn6EeC1o_ZuFAaWCA" target="_DWrUcCn7EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jbTHgCn7EeC1o_ZuFAaWCA" modifier="CREATE" source="_DWrUcCn7EeC1o_ZuFAaWCA" target="_Br84ACn7EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kfuY8Cn7EeC1o_ZuFAaWCA" modifier="CREATE" source="_DWrUcCn7EeC1o_ZuFAaWCA" target="_Chf_8Cn7EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_mSnS4Cn7EeC1o_ZuFAaWCA" modifier="CREATE" source="__OCEECn6EeC1o_ZuFAaWCA" target="_Chf_8Cn7EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BTXjcCn8EeC1o_ZuFAaWCA" modifier="CREATE" source="_Chf_8Cn7EeC1o_ZuFAaWCA" target="_5xE2sCn7EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create correspondence node SPO" uuid="_FUrcwCn8EeC1o_ZuFAaWCA" incoming="_yI_rACn7EeC1o_ZuFAaWCA" outgoing="_l6wXECn8EeC1o_ZuFAaWCA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_KFcdECn8EeC1o_ZuFAaWCA" outgoingStoryLinks="_ZiARsCn8EeC1o_ZuFAaWCA _aCqHsCn8EeC1o_ZuFAaWCA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformationQueueSpo" uuid="_KwF8kCn8EeC1o_ZuFAaWCA" incomingStoryLinks="_bdXLICn8EeC1o_ZuFAaWCA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodeSpo" uuid="_MhUpoCn8EeC1o_ZuFAaWCA" modifier="CREATE" outgoingStoryLinks="_Y-1TMCn8EeC1o_ZuFAaWCA" incomingStoryLinks="_aCqHsCn8EeC1o_ZuFAaWCA _abYJsCn8EeC1o_ZuFAaWCA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_sKEZACn8EeC1o_ZuFAaWCA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tMNygCn8EeC1o_ZuFAaWCA" expressionString="'corrNode'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggNodeEClass" uuid="_N_Wt4Cn8EeC1o_ZuFAaWCA" incomingStoryLinks="_Y-1TMCn8EeC1o_ZuFAaWCA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XueekCn8EeC1o_ZuFAaWCA" expressionString="mote::TGGNode" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="expressionLink" uuid="_O0Ad8Cn8EeC1o_ZuFAaWCA" modifier="CREATE" outgoingStoryLinks="_abYJsCn8EeC1o_ZuFAaWCA _bdXLICn8EeC1o_ZuFAaWCA _cxNYoCn8EeC1o_ZuFAaWCA" incomingStoryLinks="_ZiARsCn8EeC1o_ZuFAaWCA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternExpressionLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="expression" uuid="_PyPn0Cn8EeC1o_ZuFAaWCA" modifier="CREATE" incomingStoryLinks="_cxNYoCn8EeC1o_ZuFAaWCA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_diLikCn8EeC1o_ZuFAaWCA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_et02UCn8EeC1o_ZuFAaWCA" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_fuLgECn8EeC1o_ZuFAaWCA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gpTEgCn8EeC1o_ZuFAaWCA" expressionString="'transformationQueue.getElements()'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Y-1TMCn8EeC1o_ZuFAaWCA" modifier="CREATE" source="_MhUpoCn8EeC1o_ZuFAaWCA" target="_N_Wt4Cn8EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZiARsCn8EeC1o_ZuFAaWCA" modifier="CREATE" source="_KFcdECn8EeC1o_ZuFAaWCA" target="_O0Ad8Cn8EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_aCqHsCn8EeC1o_ZuFAaWCA" modifier="CREATE" source="_KFcdECn8EeC1o_ZuFAaWCA" target="_MhUpoCn8EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_abYJsCn8EeC1o_ZuFAaWCA" modifier="CREATE" source="_O0Ad8Cn8EeC1o_ZuFAaWCA" target="_MhUpoCn8EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_bdXLICn8EeC1o_ZuFAaWCA" modifier="CREATE" source="_O0Ad8Cn8EeC1o_ZuFAaWCA" target="_KwF8kCn8EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_cxNYoCn8EeC1o_ZuFAaWCA" modifier="CREATE" source="_O0Ad8Cn8EeC1o_ZuFAaWCA" target="_PyPn0Cn8EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternExpressionLink/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create match SPO" uuid="_32_6MCn8EeC1o_ZuFAaWCA" incoming="_l6wXECn8EeC1o_ZuFAaWCA" outgoing="_VPmy8Cn9EeC1o_ZuFAaWCA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_6GVfkCn8EeC1o_ZuFAaWCA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_6oP50Cn8EeC1o_ZuFAaWCA">
          <activity href="../common/create_match_spo.story#_6d114Bi_EeCuQ-PF2tTDdw"/>
          <parameters name="storyActionNode" uuid="_8-WOgCn8EeC1o_ZuFAaWCA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__GUAkCn8EeC1o_ZuFAaWCA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="__-JngCn8EeC1o_ZuFAaWCA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BhHPECn9EeC1o_ZuFAaWCA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_DuAkcCn9EeC1o_ZuFAaWCA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_E5VIECn9EeC1o_ZuFAaWCA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create parentCorrNode SPO" uuid="_NrrHcCn9EeC1o_ZuFAaWCA" incoming="_VPmy8Cn9EeC1o_ZuFAaWCA" outgoing="_WzUocCn9EeC1o_ZuFAaWCA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_PCJLICn9EeC1o_ZuFAaWCA" outgoingStoryLinks="_XTXacCn9EeC1o_ZuFAaWCA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="parentCorrNodeSPO" uuid="_PoVocCn9EeC1o_ZuFAaWCA" modifier="CREATE" outgoingStoryLinks="_ouJ2UCn9EeC1o_ZuFAaWCA" incomingStoryLinks="_XTXacCn9EeC1o_ZuFAaWCA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_ZiSJECn9EeC1o_ZuFAaWCA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_aZVF0Cn9EeC1o_ZuFAaWCA" expressionString="'parentCorrNode'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_cgm7YCn9EeC1o_ZuFAaWCA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_djGTICn9EeC1o_ZuFAaWCA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggNodeEClass" uuid="_l-5awCn9EeC1o_ZuFAaWCA" incomingStoryLinks="_ouJ2UCn9EeC1o_ZuFAaWCA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XTXacCn9EeC1o_ZuFAaWCA" modifier="CREATE" source="_PCJLICn9EeC1o_ZuFAaWCA" target="_PoVocCn9EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ouJ2UCn9EeC1o_ZuFAaWCA" modifier="CREATE" source="_PoVocCn9EeC1o_ZuFAaWCA" target="_l-5awCn9EeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_ztMwECn4EeCF_aVH6mO9gA" source="_wWf6ACn4EeCF_aVH6mO9gA" target="_xJh5cCn4EeCF_aVH6mO9gA"/>
    <edges uuid="_EcpTcCn6EeC1o_ZuFAaWCA" source="_xJh5cCn4EeCF_aVH6mO9gA" target="_8PLYMCn4EeCF_aVH6mO9gA"/>
    <edges uuid="_YcftECn6EeC1o_ZuFAaWCA" source="_8PLYMCn4EeCF_aVH6mO9gA" target="_IPiWQCn6EeC1o_ZuFAaWCA"/>
    <edges uuid="_aG_gACn6EeC1o_ZuFAaWCA" source="_IPiWQCn6EeC1o_ZuFAaWCA" target="_6-EDsCn6EeC1o_ZuFAaWCA"/>
    <edges uuid="_yI_rACn7EeC1o_ZuFAaWCA" source="_6-EDsCn6EeC1o_ZuFAaWCA" target="_FUrcwCn8EeC1o_ZuFAaWCA"/>
    <edges uuid="_l6wXECn8EeC1o_ZuFAaWCA" source="_FUrcwCn8EeC1o_ZuFAaWCA" target="_32_6MCn8EeC1o_ZuFAaWCA"/>
    <edges uuid="_VPmy8Cn9EeC1o_ZuFAaWCA" source="_32_6MCn8EeC1o_ZuFAaWCA" target="_NrrHcCn9EeC1o_ZuFAaWCA"/>
    <edges uuid="_WzUocCn9EeC1o_ZuFAaWCA" source="_NrrHcCn9EeC1o_ZuFAaWCA" target="_ZqETcCn6EeC1o_ZuFAaWCA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
