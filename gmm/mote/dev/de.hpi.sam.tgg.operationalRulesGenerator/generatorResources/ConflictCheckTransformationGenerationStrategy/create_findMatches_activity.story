<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_findMatches_activity" uuid="_t0haQBfVEeCVjoDp-muf0Q">
  <activities name="create_findMatches_activity" uuid="_vRRsIBfVEeCVjoDp-muf0Q">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_xe-SwBfVEeCVjoDp-muf0Q" outgoing="_F58QYBf9EeCaHaBYHoSzPw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_yAPMwBfVEeCVjoDp-muf0Q" incoming="_F58QYBf9EeCaHaBYHoSzPw" outgoing="_GQmc4Bf9EeCaHaBYHoSzPw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_zgv9QBfVEeCVjoDp-muf0Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_0D6UsBfVEeCVjoDp-muf0Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_2iX7MBfVEeCVjoDp-muf0Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_3vSzwBf1EeCvZOwhS7-eiQ" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create initial node" uuid="_pV8rcBf8EeCaHaBYHoSzPw" incoming="_GQmc4Bf9EeCaHaBYHoSzPw" outgoing="_GvCsYBf9EeCaHaBYHoSzPw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_rCMK0Bf8EeCaHaBYHoSzPw" outgoingStoryLinks="_u2yd0Bf8EeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_rfmfkBf8EeCaHaBYHoSzPw" modifier="CREATE" incomingStoryLinks="_u2yd0Bf8EeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_u2yd0Bf8EeCaHaBYHoSzPw" modifier="CREATE" source="_rCMK0Bf8EeCaHaBYHoSzPw" target="_rfmfkBf8EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to create MatchStore object" uuid="_xNl9IBf8EeCaHaBYHoSzPw" incoming="_GvCsYBf9EeCaHaBYHoSzPw" outgoing="_M_eTMBf-EeCaHaBYHoSzPw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_z7d4oBf8EeCaHaBYHoSzPw" incomingStoryLinks="_DTLCcBf9EeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createMatchStoreSAN" uuid="_0ijGwBf8EeCaHaBYHoSzPw" modifier="CREATE" incomingStoryLinks="_B12v4Bf9EeCaHaBYHoSzPw _CsHCcBf9EeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_7uLnABf8EeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8wmGQBf8EeCaHaBYHoSzPw" expressionString="'create MatchStore'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_1uJ-QBf8EeCaHaBYHoSzPw" outgoingStoryLinks="_Bi9CABf9EeCaHaBYHoSzPw _B12v4Bf9EeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_2QiSkBf8EeCaHaBYHoSzPw" modifier="CREATE" outgoingStoryLinks="_CsHCcBf9EeCaHaBYHoSzPw _DTLCcBf9EeCaHaBYHoSzPw" incomingStoryLinks="_Bi9CABf9EeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Bi9CABf9EeCaHaBYHoSzPw" modifier="CREATE" source="_1uJ-QBf8EeCaHaBYHoSzPw" target="_2QiSkBf8EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_B12v4Bf9EeCaHaBYHoSzPw" modifier="CREATE" source="_1uJ-QBf8EeCaHaBYHoSzPw" target="_0ijGwBf8EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_CsHCcBf9EeCaHaBYHoSzPw" modifier="CREATE" source="_2QiSkBf8EeCaHaBYHoSzPw" target="_0ijGwBf8EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_DTLCcBf9EeCaHaBYHoSzPw" modifier="CREATE" source="_2QiSkBf8EeCaHaBYHoSzPw" target="_z7d4oBf8EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to find matches" uuid="_HhO-UBf9EeCaHaBYHoSzPw" incoming="_0cP-kBgJEeCaHaBYHoSzPw" outgoing="_OFITsBf-EeCaHaBYHoSzPw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_OdZfMBf9EeCaHaBYHoSzPw" outgoingStoryLinks="_cZoJgBf9EeCaHaBYHoSzPw _de-oABf9EeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="lastNode" uuid="_P2lfsBf9EeCaHaBYHoSzPw" incomingStoryLinks="_d4AMABf9EeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesSAN" uuid="_Q0mnIBf9EeCaHaBYHoSzPw" modifier="CREATE" incomingStoryLinks="_de-oABf9EeCaHaBYHoSzPw _e5T4ABf9EeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_geYYYBf9EeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mFSWgBf9EeCaHaBYHoSzPw" expressionString="'find matches'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_yC_eoBf9EeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/forEach"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_yrBH8Bf9EeCaHaBYHoSzPw" expressionString="true" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_SJD4oBf9EeCaHaBYHoSzPw" modifier="CREATE" outgoingStoryLinks="_d4AMABf9EeCaHaBYHoSzPw _e5T4ABf9EeCaHaBYHoSzPw" incomingStoryLinks="_cZoJgBf9EeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_1n1LcBgKEeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_4KxCQBgKEeCaHaBYHoSzPw" expressionString="if lastNode = createMatchStoreSAN then&#xA;&#x9;storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::NONE&#xA;else&#xA;&#x9;storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::END&#xA;endif" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_cZoJgBf9EeCaHaBYHoSzPw" modifier="CREATE" source="_OdZfMBf9EeCaHaBYHoSzPw" target="_SJD4oBf9EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_de-oABf9EeCaHaBYHoSzPw" modifier="CREATE" source="_OdZfMBf9EeCaHaBYHoSzPw" target="_Q0mnIBf9EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_d4AMABf9EeCaHaBYHoSzPw" modifier="CREATE" source="_SJD4oBf9EeCaHaBYHoSzPw" target="_P2lfsBf9EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_e5T4ABf9EeCaHaBYHoSzPw" modifier="CREATE" source="_SJD4oBf9EeCaHaBYHoSzPw" target="_Q0mnIBf9EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of createMatchStoreSAN" uuid="_IH_cUBf9EeCaHaBYHoSzPw" incoming="_M_eTMBf-EeCaHaBYHoSzPw" outgoing="_xS7SoBgIEeCaHaBYHoSzPw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_0pUD0BgBEeCaHaBYHoSzPw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_1QKocBgBEeCaHaBYHoSzPw">
          <activity href="../common/create_matchStore_spo.story#_X128ABgAEeCaHaBYHoSzPw"/>
          <parameters name="storyActionNode" uuid="_2vjXkBgBEeCaHaBYHoSzPw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5Q1BgBgBEeCaHaBYHoSzPw" expressionString="createMatchStoreSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_mkZSIBiyEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_oiZ28BiyEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_qYvGcBiyEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rqrPcBiyEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of findMatchesSAN" uuid="_ohHfwBf9EeCaHaBYHoSzPw" incoming="_OFITsBf-EeCaHaBYHoSzPw" outgoing="_P-y8UBgEEeCaHaBYHoSzPw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_48VP0BgPEeC-pfj64BNTVg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_5iIrkBgPEeC-pfj64BNTVg">
          <activity href="create_storyPattern_findMatches.story#_J_YCcBgCEeCaHaBYHoSzPw"/>
          <parameters name="tggRule" uuid="_EAC04BgQEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GQso4BgQEeC-pfj64BNTVg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="parentCorrNode" uuid="_JbQgYBgQEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_PYxnsBgQEeC-pfj64BNTVg" expressionString="parentCorrNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_UICMIBgQEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WZc1IBgQEeC-pfj64BNTVg" expressionString="findMatchesSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="direction" uuid="_ZGBJEBgQEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_a_UNABgQEeC-pfj64BNTVg" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create final node" uuid="_tiVW4Bf9EeCaHaBYHoSzPw" incoming="_I3abMBgKEeCaHaBYHoSzPw" outgoing="_QQILoBf-EeCaHaBYHoSzPw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_v8tuoBf9EeCaHaBYHoSzPw" outgoingStoryLinks="_DJY-4Bf-EeCaHaBYHoSzPw _EephoBf-EeCaHaBYHoSzPw _E4x44Bf-EeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="lastNode" uuid="_wsZfIBf9EeCaHaBYHoSzPw" incomingStoryLinks="_DJY-4Bf-EeCaHaBYHoSzPw _GEdo4Bf-EeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_4RTbABf9EeCaHaBYHoSzPw" modifier="CREATE" outgoingStoryLinks="_Fw7o4Bf-EeCaHaBYHoSzPw _GEdo4Bf-EeCaHaBYHoSzPw" incomingStoryLinks="_EephoBf-EeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_9wiq4Bf9EeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-36ioBf9EeCaHaBYHoSzPw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::END" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="finalNode" uuid="_4mDjABf9EeCaHaBYHoSzPw" modifier="CREATE" outgoingStoryLinks="_KqcqoBgAEeCaHaBYHoSzPw" incomingStoryLinks="_E4x44Bf-EeCaHaBYHoSzPw _Fw7o4Bf-EeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_8vl5UBf_EeCaHaBYHoSzPw" modifier="CREATE" outgoingStoryLinks="_JoO_sBgAEeCaHaBYHoSzPw" incomingStoryLinks="_KqcqoBgAEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varRef" uuid="_BX6IQBgAEeCaHaBYHoSzPw" modifier="CREATE" incomingStoryLinks="_JoO_sBgAEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_DB3vsBgAEeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EagWkBgAEeCaHaBYHoSzPw" expressionString="'matchStore'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_F6-DwBgAEeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_HKt0YBgAEeCaHaBYHoSzPw" expressionString="mote::rules::MatchStorage" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_DJY-4Bf-EeCaHaBYHoSzPw" source="_v8tuoBf9EeCaHaBYHoSzPw" target="_wsZfIBf9EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_EephoBf-EeCaHaBYHoSzPw" modifier="CREATE" source="_v8tuoBf9EeCaHaBYHoSzPw" target="_4RTbABf9EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_E4x44Bf-EeCaHaBYHoSzPw" modifier="CREATE" source="_v8tuoBf9EeCaHaBYHoSzPw" target="_4mDjABf9EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Fw7o4Bf-EeCaHaBYHoSzPw" modifier="CREATE" source="_4RTbABf9EeCaHaBYHoSzPw" target="_4mDjABf9EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GEdo4Bf-EeCaHaBYHoSzPw" modifier="CREATE" source="_4RTbABf9EeCaHaBYHoSzPw" target="_wsZfIBf9EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_JoO_sBgAEeCaHaBYHoSzPw" modifier="CREATE" source="_8vl5UBf_EeCaHaBYHoSzPw" target="_BX6IQBgAEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_KqcqoBgAEeCaHaBYHoSzPw" modifier="CREATE" source="_4mDjABf9EeCaHaBYHoSzPw" target="_8vl5UBf_EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_PdPWIBf-EeCaHaBYHoSzPw" incoming="_QQILoBf-EeCaHaBYHoSzPw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to evaluate rule variables" uuid="_DJ6xEBgDEeCaHaBYHoSzPw" incoming="_P-y8UBgEEeCaHaBYHoSzPw" outgoing="_ikBLABgEEeCaHaBYHoSzPw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_FEqmgBgDEeCaHaBYHoSzPw" outgoingStoryLinks="_2Z93MBgDEeCaHaBYHoSzPw _3JHcMBgDEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesSAN" uuid="_GVQEoBgDEeCaHaBYHoSzPw" incomingStoryLinks="_3oL90BgDEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_HIam8BgDEeCaHaBYHoSzPw" modifier="CREATE" outgoingStoryLinks="_3oL90BgDEeCaHaBYHoSzPw _487jMBgDEeCaHaBYHoSzPw" incomingStoryLinks="_2Z93MBgDEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_wKzmQBgDEeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xF58kBgDEeCaHaBYHoSzPw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FOR_EACH" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evaluateRuleVariablesEAN" uuid="_HefkoBgDEeCaHaBYHoSzPw" modifier="CREATE" outgoingStoryLinks="_5RJfsBgDEeCaHaBYHoSzPw" incomingStoryLinks="_3JHcMBgDEeCaHaBYHoSzPw _487jMBgDEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_9WqjkBgDEeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9zjT4BgDEeCaHaBYHoSzPw" expressionString="'evaluate rule variables'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_I_MiYBgDEeCaHaBYHoSzPw" incomingStoryLinks="_5RJfsBgDEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_FO2n0BgEEeCaHaBYHoSzPw">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_Fk-BwBgEEeCaHaBYHoSzPw">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
            <activity href="../common/create_evaluateRuleVariables_expression.story#_uabxYfZwEd-i2JdGlqLvVQ"/>
            <parameters name="tggRule" uuid="_HegWQBgEEeCaHaBYHoSzPw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JuO9MBgEEeCaHaBYHoSzPw" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_K5TCIBgEEeCaHaBYHoSzPw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NuuXIBgEEeCaHaBYHoSzPw" expressionString="direction" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2Z93MBgDEeCaHaBYHoSzPw" modifier="CREATE" source="_FEqmgBgDEeCaHaBYHoSzPw" target="_HIam8BgDEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3JHcMBgDEeCaHaBYHoSzPw" modifier="CREATE" source="_FEqmgBgDEeCaHaBYHoSzPw" target="_HefkoBgDEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3oL90BgDEeCaHaBYHoSzPw" modifier="CREATE" source="_HIam8BgDEeCaHaBYHoSzPw" target="_GVQEoBgDEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_487jMBgDEeCaHaBYHoSzPw" modifier="CREATE" source="_HIam8BgDEeCaHaBYHoSzPw" target="_HefkoBgDEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5RJfsBgDEeCaHaBYHoSzPw" modifier="CREATE" source="_HefkoBgDEeCaHaBYHoSzPw" target="_I_MiYBgDEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to check constraints" uuid="_eVYnYBgEEeCaHaBYHoSzPw" incoming="_ikBLABgEEeCaHaBYHoSzPw" outgoing="_yaqosBgGEeCaHaBYHoSzPw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_jG09IBgEEeCaHaBYHoSzPw" outgoingStoryLinks="_d1vokBgFEeCaHaBYHoSzPw _fPZjIBgFEeCaHaBYHoSzPw _f39_ABgFEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evaluateRuleVariablesEAN" uuid="_lF7HEBgEEeCaHaBYHoSzPw" incomingStoryLinks="_iA_hABgFEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_meUdYBgEEeCaHaBYHoSzPw" modifier="CREATE" outgoingStoryLinks="_iA_hABgFEeCaHaBYHoSzPw _jKPBABgFEeCaHaBYHoSzPw" incomingStoryLinks="_d1vokBgFEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkConstraintsSAN" uuid="_m-dWABgEEeCaHaBYHoSzPw" modifier="CREATE" incomingStoryLinks="_fPZjIBgFEeCaHaBYHoSzPw _jKPBABgFEeCaHaBYHoSzPw _kCTRcBgFEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_x_vcsBgFEeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_y99_gBgFEeCaHaBYHoSzPw" expressionString="'check constraints'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesSAN" uuid="_Y78oEBgFEeCaHaBYHoSzPw" incomingStoryLinks="_kpIn8BgFEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_a-ypkBgFEeCaHaBYHoSzPw" modifier="CREATE" outgoingStoryLinks="_kCTRcBgFEeCaHaBYHoSzPw _kpIn8BgFEeCaHaBYHoSzPw" incomingStoryLinks="_f39_ABgFEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_n8EYUBgFEeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_oXvnsBgFEeCaHaBYHoSzPw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_d1vokBgFEeCaHaBYHoSzPw" modifier="CREATE" source="_jG09IBgEEeCaHaBYHoSzPw" target="_meUdYBgEEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fPZjIBgFEeCaHaBYHoSzPw" modifier="CREATE" source="_jG09IBgEEeCaHaBYHoSzPw" target="_m-dWABgEEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_f39_ABgFEeCaHaBYHoSzPw" modifier="CREATE" source="_jG09IBgEEeCaHaBYHoSzPw" target="_a-ypkBgFEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iA_hABgFEeCaHaBYHoSzPw" modifier="CREATE" source="_meUdYBgEEeCaHaBYHoSzPw" target="_lF7HEBgEEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jKPBABgFEeCaHaBYHoSzPw" modifier="CREATE" source="_meUdYBgEEeCaHaBYHoSzPw" target="_m-dWABgEEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kCTRcBgFEeCaHaBYHoSzPw" modifier="CREATE" source="_a-ypkBgFEeCaHaBYHoSzPw" target="_m-dWABgEEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kpIn8BgFEeCaHaBYHoSzPw" modifier="CREATE" source="_a-ypkBgFEeCaHaBYHoSzPw" target="_Y78oEBgFEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of checkConstraintsSAN (parent nodes constraints)" uuid="_4GCskBgFEeCaHaBYHoSzPw" incoming="_yaqosBgGEeCaHaBYHoSzPw" outgoing="_y4a7sBgGEeCaHaBYHoSzPw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_pO6DgBilEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_qoDnwBilEeCm4IbFa7I4cA">
          <activity href="create_storyPattern_check_constraints.story#_2MC_APbdEd-JnYtEWuN3-g"/>
          <parameters name="tggRule" uuid="_u3MhsBilEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KU50ABimEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_LMkb0BimEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OwGM8BimEeCm4IbFa7I4cA" expressionString="checkConstraintsSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="createSourceConstraints" uuid="_SdIfYBimEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_UXqSYBimEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetConstraints" uuid="_VRkV4BimEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XPI10BimEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentNodesConstraints" uuid="_YKfq0BimEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_aNuG0BimEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedNodesConstraints" uuid="_bQyGUBimEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dAP40BimEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to create match object" uuid="_6WPNkBgFEeCaHaBYHoSzPw" incoming="_KLEQoBioEeCm4IbFa7I4cA" outgoing="_zg7tMBgGEeCaHaBYHoSzPw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="___n38BgFEeCaHaBYHoSzPw" outgoingStoryLinks="_Q8uSQBgGEeCaHaBYHoSzPw _RiU5sBgGEeCaHaBYHoSzPw _jolUcBgGEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkConstraintsSAN" uuid="_A33HgBgGEeCaHaBYHoSzPw" incomingStoryLinks="_RzapsBgGEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_CB7t8BgGEeCaHaBYHoSzPw" modifier="CREATE" outgoingStoryLinks="_RzapsBgGEeCaHaBYHoSzPw _SVSnsBgGEeCaHaBYHoSzPw" incomingStoryLinks="_Q8uSQBgGEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_NZcYwBgGEeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ONEOEBgGEeCaHaBYHoSzPw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createMatchSAN" uuid="_DPAlEBgGEeCaHaBYHoSzPw" modifier="CREATE" incomingStoryLinks="_RiU5sBgGEeCaHaBYHoSzPw _SVSnsBgGEeCaHaBYHoSzPw _nWOq4BgGEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_KyIYQBgGEeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_LurgEBgGEeCaHaBYHoSzPw" expressionString="'create new match'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesSAN" uuid="_EmfVYBgGEeCaHaBYHoSzPw" incomingStoryLinks="_pRvVUBgGEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_S-3JIBgGEeCaHaBYHoSzPw" modifier="CREATE" outgoingStoryLinks="_nWOq4BgGEeCaHaBYHoSzPw _pRvVUBgGEeCaHaBYHoSzPw" incomingStoryLinks="_jolUcBgGEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Q8uSQBgGEeCaHaBYHoSzPw" modifier="CREATE" source="___n38BgFEeCaHaBYHoSzPw" target="_CB7t8BgGEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RiU5sBgGEeCaHaBYHoSzPw" modifier="CREATE" source="___n38BgFEeCaHaBYHoSzPw" target="_DPAlEBgGEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RzapsBgGEeCaHaBYHoSzPw" modifier="CREATE" source="_CB7t8BgGEeCaHaBYHoSzPw" target="_A33HgBgGEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_SVSnsBgGEeCaHaBYHoSzPw" modifier="CREATE" source="_CB7t8BgGEeCaHaBYHoSzPw" target="_DPAlEBgGEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jolUcBgGEeCaHaBYHoSzPw" modifier="CREATE" source="___n38BgFEeCaHaBYHoSzPw" target="_S-3JIBgGEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_nWOq4BgGEeCaHaBYHoSzPw" modifier="CREATE" source="_S-3JIBgGEeCaHaBYHoSzPw" target="_DPAlEBgGEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_pRvVUBgGEeCaHaBYHoSzPw" modifier="CREATE" source="_S-3JIBgGEeCaHaBYHoSzPw" target="_EmfVYBgGEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of createMatchSAN" uuid="_slSwwBgGEeCaHaBYHoSzPw" incoming="_zg7tMBgGEeCaHaBYHoSzPw" outgoing="_Eb6gQBgKEeCaHaBYHoSzPw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_IlrGgBiwEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_JB8LwBiwEeCm4IbFa7I4cA">
          <activity href="create_storyPattern_createNewMatch.story#_0VoY8BioEeCm4IbFa7I4cA"/>
          <parameters name="tggRule" uuid="_NMQQEBiwEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_PEOeUBiwEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_P-hjYBiwEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SbHhoBiwEeCm4IbFa7I4cA" expressionString="createMatchSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="direction" uuid="_UAb5oBiwEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_V9VrMBiwEeCm4IbFa7I4cA" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set last node" uuid="_pBYTMBgIEeCaHaBYHoSzPw" incoming="_xS7SoBgIEeCaHaBYHoSzPw" outgoing="_wAmSoBgJEeCaHaBYHoSzPw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="lastNode" uuid="_q1WLMBgIEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityNode"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_uKaCgBgIEeCaHaBYHoSzPw" expressionString="createMatchStoreSAN" expressionLanguage="OCL"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="iterate through parent correspondence nodes" uuid="_yWXFkBgIEeCaHaBYHoSzPw" incoming="_wAmSoBgJEeCaHaBYHoSzPw _GrDRUBgKEeCaHaBYHoSzPw" outgoing="_0cP-kBgJEeCaHaBYHoSzPw _I3abMBgKEeCaHaBYHoSzPw" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_1t9TgBgIEeCaHaBYHoSzPw" outgoingStoryLinks="_VfDGkBgJEeCaHaBYHoSzPw _WbT6gBgJEeCaHaBYHoSzPw _YI8hABgJEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_2ejqABgIEeCaHaBYHoSzPw" outgoingStoryLinks="_WsBQABgJEeCaHaBYHoSzPw" incomingStoryLinks="_WbT6gBgJEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="parentCorrNode" uuid="_3DVLABgIEeCaHaBYHoSzPw" incomingStoryLinks="_WsBQABgJEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_b_lbYBgJEeCaHaBYHoSzPw" expressionString="self.modifier = tgg::TGGModifierEnumeration::NONE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sourceDomain" uuid="_5_YscBgIEeCaHaBYHoSzPw" incomingStoryLinks="_VfDGkBgJEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//SourceModelDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="targetDomain" uuid="_6k2J8BgIEeCaHaBYHoSzPw" incomingStoryLinks="_YI8hABgJEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TargetModelDomain"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_VfDGkBgJEeCaHaBYHoSzPw" source="_1t9TgBgIEeCaHaBYHoSzPw" target="_5_YscBgIEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/sourceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WbT6gBgJEeCaHaBYHoSzPw" source="_1t9TgBgIEeCaHaBYHoSzPw" target="_2ejqABgIEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WsBQABgJEeCaHaBYHoSzPw" source="_2ejqABgIEeCaHaBYHoSzPw" target="_3DVLABgIEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_YI8hABgJEeCaHaBYHoSzPw" source="_1t9TgBgIEeCaHaBYHoSzPw" target="_6k2J8BgIEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/targetDomain"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set last node" uuid="_8EMnYBgJEeCaHaBYHoSzPw" incoming="_Eb6gQBgKEeCaHaBYHoSzPw" outgoing="_GrDRUBgKEeCaHaBYHoSzPw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="lastNode" uuid="_9ceB4BgJEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityNode"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BtHXsBgKEeCaHaBYHoSzPw" expressionString="findMatchesSAN" expressionLanguage="OCL"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of checkConstraintsSAN (created nodes constraints)" uuid="_AhegMBioEeCm4IbFa7I4cA" incoming="_y4a7sBgGEeCaHaBYHoSzPw" outgoing="_KLEQoBioEeCm4IbFa7I4cA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_AhegMRioEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_AhegMhioEeCm4IbFa7I4cA">
          <activity href="create_storyPattern_check_constraints.story#_2MC_APbdEd-JnYtEWuN3-g"/>
          <parameters name="tggRule" uuid="_AhegOxioEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AhegPBioEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_AhegMxioEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AhegNBioEeCm4IbFa7I4cA" expressionString="checkConstraintsSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="createSourceConstraints" uuid="_AhegPRioEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AhegPhioEeCm4IbFa7I4cA" expressionString="direction = mote::TransformationDirection::FORWARD or&#xA;direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetConstraints" uuid="_AhegORioEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AhegOhioEeCm4IbFa7I4cA" expressionString="direction = mote::TransformationDirection::MAPPING or&#xA;direction = mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentNodesConstraints" uuid="_AhegNRioEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AhegNhioEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedNodesConstraints" uuid="_AhegNxioEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AhegOBioEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <edges uuid="_F58QYBf9EeCaHaBYHoSzPw" source="_xe-SwBfVEeCVjoDp-muf0Q" target="_yAPMwBfVEeCVjoDp-muf0Q"/>
    <edges uuid="_GQmc4Bf9EeCaHaBYHoSzPw" source="_yAPMwBfVEeCVjoDp-muf0Q" target="_pV8rcBf8EeCaHaBYHoSzPw"/>
    <edges uuid="_GvCsYBf9EeCaHaBYHoSzPw" source="_pV8rcBf8EeCaHaBYHoSzPw" target="_xNl9IBf8EeCaHaBYHoSzPw"/>
    <edges uuid="_M_eTMBf-EeCaHaBYHoSzPw" source="_xNl9IBf8EeCaHaBYHoSzPw" target="_IH_cUBf9EeCaHaBYHoSzPw"/>
    <edges uuid="_OFITsBf-EeCaHaBYHoSzPw" source="_HhO-UBf9EeCaHaBYHoSzPw" target="_ohHfwBf9EeCaHaBYHoSzPw"/>
    <edges uuid="_QQILoBf-EeCaHaBYHoSzPw" source="_tiVW4Bf9EeCaHaBYHoSzPw" target="_PdPWIBf-EeCaHaBYHoSzPw"/>
    <edges uuid="_P-y8UBgEEeCaHaBYHoSzPw" source="_ohHfwBf9EeCaHaBYHoSzPw" target="_DJ6xEBgDEeCaHaBYHoSzPw"/>
    <edges uuid="_ikBLABgEEeCaHaBYHoSzPw" source="_DJ6xEBgDEeCaHaBYHoSzPw" target="_eVYnYBgEEeCaHaBYHoSzPw"/>
    <edges uuid="_yaqosBgGEeCaHaBYHoSzPw" source="_eVYnYBgEEeCaHaBYHoSzPw" target="_4GCskBgFEeCaHaBYHoSzPw"/>
    <edges uuid="_y4a7sBgGEeCaHaBYHoSzPw" source="_4GCskBgFEeCaHaBYHoSzPw" target="_AhegMBioEeCm4IbFa7I4cA"/>
    <edges uuid="_zg7tMBgGEeCaHaBYHoSzPw" source="_6WPNkBgFEeCaHaBYHoSzPw" target="_slSwwBgGEeCaHaBYHoSzPw"/>
    <edges uuid="_xS7SoBgIEeCaHaBYHoSzPw" source="_IH_cUBf9EeCaHaBYHoSzPw" target="_pBYTMBgIEeCaHaBYHoSzPw"/>
    <edges uuid="_wAmSoBgJEeCaHaBYHoSzPw" source="_pBYTMBgIEeCaHaBYHoSzPw" target="_yWXFkBgIEeCaHaBYHoSzPw"/>
    <edges uuid="_0cP-kBgJEeCaHaBYHoSzPw" source="_yWXFkBgIEeCaHaBYHoSzPw" target="_HhO-UBf9EeCaHaBYHoSzPw" guardType="FOR_EACH"/>
    <edges uuid="_Eb6gQBgKEeCaHaBYHoSzPw" source="_slSwwBgGEeCaHaBYHoSzPw" target="_8EMnYBgJEeCaHaBYHoSzPw"/>
    <edges uuid="_GrDRUBgKEeCaHaBYHoSzPw" source="_8EMnYBgJEeCaHaBYHoSzPw" target="_yWXFkBgIEeCaHaBYHoSzPw"/>
    <edges uuid="_I3abMBgKEeCaHaBYHoSzPw" source="_yWXFkBgIEeCaHaBYHoSzPw" target="_tiVW4Bf9EeCaHaBYHoSzPw" guardType="END"/>
    <edges uuid="_KLEQoBioEeCm4IbFa7I4cA" source="_AhegMBioEeCm4IbFa7I4cA" target="_6WPNkBgFEeCaHaBYHoSzPw"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
