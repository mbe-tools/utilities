<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="generate_activityContents" uuid="_5GolACobEeCV66-7fXb9cw">
  <activities name="generate_activityContents" uuid="_5vU8sCobEeCV66-7fXb9cw">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_8ETQACobEeCV66-7fXb9cw" outgoing="_H4a-UCocEeCV66-7fXb9cw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_8pSMYCobEeCV66-7fXb9cw" incoming="_H4a-UCocEeCV66-7fXb9cw" outgoing="_QU6NACodEeCV66-7fXb9cw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfoStore" uuid="_9LdFUCobEeCV66-7fXb9cw" outgoingStoryLinks="_Bh79YCocEeCV66-7fXb9cw" incomingStoryLinks="_9c8OMCosEeCjTcyrv1LVTA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_Aa5c0CocEeCV66-7fXb9cw" incomingStoryLinks="_Bh79YCocEeCV66-7fXb9cw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="genInfos" uuid="_7o0lMCosEeCjTcyrv1LVTA" outgoingStoryLinks="_9c8OMCosEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/GenerationInfoStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Bh79YCocEeCV66-7fXb9cw" source="_9LdFUCobEeCV66-7fXb9cw" target="_Aa5c0CocEeCV66-7fXb9cw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/tggRule"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9c8OMCosEeCjTcyrv1LVTA" source="_7o0lMCosEeCjTcyrv1LVTA" target="_9LdFUCobEeCV66-7fXb9cw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/GenerationInfoStore/ruleInfos"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create findMatches activities" uuid="_2QeawCocEeCV66-7fXb9cw" incoming="_2FwRICofEeCrWJNIJyAZ9w" outgoing="_diVwMD5sEeCS07xt5ugfeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfoStore" uuid="_4rvjUCocEeCV66-7fXb9cw" outgoingStoryLinks="_9q_SACrAEeCPWq47ABciVw __vpRYCrAEeCPWq47ABciVw _BwaPQCrBEeCPWq47ABciVw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardFindMatchesAD" uuid="_6Pnw4CocEeCV66-7fXb9cw" outgoingStoryLinks="_M2ciECodEeCV66-7fXb9cw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardFindMatchesActivity" uuid="_Bv2oICodEeCV66-7fXb9cw" incomingStoryLinks="_M2ciECodEeCV66-7fXb9cw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingFindMatchesActivity" uuid="_YMc7kCoeEeCrWJNIJyAZ9w" incomingStoryLinks="_kYpLoCoeEeCrWJNIJyAZ9w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingFindMatchesAD" uuid="_YMc7mSoeEeCrWJNIJyAZ9w" outgoingStoryLinks="_kYpLoCoeEeCrWJNIJyAZ9w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseFindMatchesAD" uuid="_ZYScmSoeEeCrWJNIJyAZ9w" outgoingStoryLinks="_k1opoCoeEeCrWJNIJyAZ9w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseFindMatchesActivity" uuid="_ZYScnCoeEeCrWJNIJyAZ9w" incomingStoryLinks="_k1opoCoeEeCrWJNIJyAZ9w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ffm" uuid="_yB02gCrAEeCPWq47ABciVw" incomingStoryLinks="_9q_SACrAEeCPWq47ABciVw" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_29UgICrAEeCPWq47ABciVw" expressionString="'forwardFindMatches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mfm" uuid="_zf0egCrAEeCPWq47ABciVw" incomingStoryLinks="__vpRYCrAEeCPWq47ABciVw" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5XyXcCrAEeCPWq47ABciVw" expressionString="'mappingFindMatches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rfm" uuid="_0BxVACrAEeCPWq47ABciVw" incomingStoryLinks="_BwaPQCrBEeCPWq47ABciVw" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6_6L0CrAEeCPWq47ABciVw" expressionString="'reverseFindMatches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_M2ciECodEeCV66-7fXb9cw" source="_6Pnw4CocEeCV66-7fXb9cw" target="_Bv2oICodEeCV66-7fXb9cw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kYpLoCoeEeCrWJNIJyAZ9w" source="_YMc7mSoeEeCrWJNIJyAZ9w" target="_YMc7kCoeEeCrWJNIJyAZ9w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_k1opoCoeEeCrWJNIJyAZ9w" source="_ZYScmSoeEeCrWJNIJyAZ9w" target="_ZYScnCoeEeCrWJNIJyAZ9w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_9q_SACrAEeCPWq47ABciVw" source="_4rvjUCocEeCV66-7fXb9cw" target="_yB02gCrAEeCPWq47ABciVw" valueTarget="_6Pnw4CocEeCV66-7fXb9cw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="__vpRYCrAEeCPWq47ABciVw" source="_4rvjUCocEeCV66-7fXb9cw" target="_zf0egCrAEeCPWq47ABciVw" valueTarget="_YMc7mSoeEeCrWJNIJyAZ9w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_BwaPQCrBEeCPWq47ABciVw" source="_4rvjUCocEeCV66-7fXb9cw" target="_0BxVACrAEeCPWq47ABciVw" valueTarget="_ZYScmSoeEeCrWJNIJyAZ9w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_tIa0oCodEeCV66-7fXb9cw" incoming="_QU6NACodEeCV66-7fXb9cw" outgoing="_yZVCsCofEeCrWJNIJyAZ9w _2FwRICofEeCrWJNIJyAZ9w"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="generate axiom activity contents" uuid="_phyaQCofEeCrWJNIJyAZ9w" incoming="_yZVCsCofEeCrWJNIJyAZ9w" outgoing="_0nxQMCofEeCrWJNIJyAZ9w">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_q56DwCofEeCrWJNIJyAZ9w">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_tNvuoCofEeCrWJNIJyAZ9w">
          <activity href="generate_axiom_activities.story#_NTSQ0HhtEd-F3I0fuD-eTQ"/>
          <parameters name="ruleInfoStore" uuid="_vHvWICofEeCrWJNIJyAZ9w">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xAEJsCofEeCrWJNIJyAZ9w" expressionString="ruleInfoStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_0Ss_ICofEeCrWJNIJyAZ9w" incoming="_0nxQMCofEeCrWJNIJyAZ9w"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_3CUnECofEeCrWJNIJyAZ9w" incoming="_qQPYcC1QEeCupczvU2Jg8w"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create conflictCheck activities" uuid="_VwjYgCosEeCjTcyrv1LVTA" incoming="_jLdcIEQPEeCj3vsEcE-cfw" outgoing="_v2ckQD5tEeCS07xt5ugfeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfoStore" uuid="_ZFWKECosEeCjTcyrv1LVTA" outgoingStoryLinks="_N3Ma4CrBEeCPWq47ABciVw _dqBUQC1PEeCupczvU2Jg8w _e8MGwC1PEeCupczvU2Jg8w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardConflictCheckAD" uuid="_Z_qdQCosEeCjTcyrv1LVTA" outgoingStoryLinks="_Ptz9gCotEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardConflictCheckActivity" uuid="_hHyWgCosEeCjTcyrv1LVTA" incomingStoryLinks="_Ptz9gCotEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="fcc" uuid="_I9L_ACrBEeCPWq47ABciVw" incomingStoryLinks="_N3Ma4CrBEeCPWq47ABciVw" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_LN75oCrBEeCPWq47ABciVw" expressionString="'forwardConflictCheck'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mcc" uuid="_9F3skC1OEeCupczvU2Jg8w" incomingStoryLinks="_dqBUQC1PEeCupczvU2Jg8w" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_P1DIgC1PEeCupczvU2Jg8w" expressionString="'mappingConflictCheck'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rcc" uuid="__qQ78C1OEeCupczvU2Jg8w" incomingStoryLinks="_e8MGwC1PEeCupczvU2Jg8w" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NWmJEC1PEeCupczvU2Jg8w" expressionString="'reverseConflictCheck'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingConflictCheckAD" uuid="_AIjacC1PEeCupczvU2Jg8w" outgoingStoryLinks="_ri5IkC1PEeCupczvU2Jg8w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseConflictCheckAD" uuid="_C7AT8C1PEeCupczvU2Jg8w" outgoingStoryLinks="_sR0rIC1PEeCupczvU2Jg8w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingConflictCheckActivity" uuid="_j5OBcC1PEeCupczvU2Jg8w" incomingStoryLinks="_ri5IkC1PEeCupczvU2Jg8w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseConflictCheckActivity" uuid="_kcNZyy1PEeCupczvU2Jg8w" incomingStoryLinks="_sR0rIC1PEeCupczvU2Jg8w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ptz9gCotEeCjTcyrv1LVTA" source="_Z_qdQCosEeCjTcyrv1LVTA" target="_hHyWgCosEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_N3Ma4CrBEeCPWq47ABciVw" source="_ZFWKECosEeCjTcyrv1LVTA" target="_I9L_ACrBEeCPWq47ABciVw" valueTarget="_Z_qdQCosEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_dqBUQC1PEeCupczvU2Jg8w" source="_ZFWKECosEeCjTcyrv1LVTA" target="_9F3skC1OEeCupczvU2Jg8w" valueTarget="_AIjacC1PEeCupczvU2Jg8w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_e8MGwC1PEeCupczvU2Jg8w" source="_ZFWKECosEeCjTcyrv1LVTA" target="__qQ78C1OEeCupczvU2Jg8w" valueTarget="_C7AT8C1PEeCupczvU2Jg8w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ri5IkC1PEeCupczvU2Jg8w" source="_AIjacC1PEeCupczvU2Jg8w" target="_j5OBcC1PEeCupczvU2Jg8w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_sR0rIC1PEeCupczvU2Jg8w" source="_C7AT8C1PEeCupczvU2Jg8w" target="_kcNZyy1PEeCupczvU2Jg8w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create transformation activity diagrams" uuid="_QUV3IC1QEeCupczvU2Jg8w" incoming="_SsjEoC1QEeCupczvU2Jg8w" outgoing="_OpQtAD51EeCS07xt5ugfeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfoStore" uuid="_TPQwIC1QEeCupczvU2Jg8w" outgoingStoryLinks="_aUIhEC1QEeCupczvU2Jg8w _12A0UC6mEeCRIK6l8KTurA _3nFJUC6mEeCRIK6l8KTurA _hhdjwC6rEeCEjs6G7WC8Uw _ofnBoC6rEeCEjs6G7WC8Uw _p3zjoC6rEeCEjs6G7WC8Uw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardTransformationAD" uuid="_UAy6wC1QEeCupczvU2Jg8w" outgoingStoryLinks="_ZX3tIC1QEeCupczvU2Jg8w" incomingStoryLinks="_aUIhEC1QEeCupczvU2Jg8w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardTransformationActivity" uuid="_VDmbkC1QEeCupczvU2Jg8w" outgoingStoryLinks="_i1TKMC6rEeCEjs6G7WC8Uw" incomingStoryLinks="_ZX3tIC1QEeCupczvU2Jg8w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingTransformationAD" uuid="_qAZ4QC6mEeCRIK6l8KTurA" outgoingStoryLinks="_0VRaUC6mEeCRIK6l8KTurA" incomingStoryLinks="_12A0UC6mEeCRIK6l8KTurA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingTransformationActivity" uuid="_qAZ4Qy6mEeCRIK6l8KTurA" outgoingStoryLinks="_lGtMIC6rEeCEjs6G7WC8Uw" incomingStoryLinks="_0VRaUC6mEeCRIK6l8KTurA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseTransformationAD" uuid="_qzC2LC6mEeCRIK6l8KTurA" outgoingStoryLinks="_1EWG0C6mEeCRIK6l8KTurA" incomingStoryLinks="_3nFJUC6mEeCRIK6l8KTurA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseTransformationActivity" uuid="_qzC2Ly6mEeCRIK6l8KTurA" outgoingStoryLinks="_mFfvoC6rEeCEjs6G7WC8Uw" incomingStoryLinks="_1EWG0C6mEeCRIK6l8KTurA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardOperation" uuid="_Ze5bgC6rEeCEjs6G7WC8Uw" incomingStoryLinks="_hhdjwC6rEeCEjs6G7WC8Uw _i1TKMC6rEeCEjs6G7WC8Uw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingOperation" uuid="_aip-kC6rEeCEjs6G7WC8Uw" incomingStoryLinks="_lGtMIC6rEeCEjs6G7WC8Uw _ofnBoC6rEeCEjs6G7WC8Uw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseOperation" uuid="_bO4ycC6rEeCEjs6G7WC8Uw" incomingStoryLinks="_mFfvoC6rEeCEjs6G7WC8Uw _p3zjoC6rEeCEjs6G7WC8Uw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZX3tIC1QEeCupczvU2Jg8w" source="_UAy6wC1QEeCupczvU2Jg8w" target="_VDmbkC1QEeCupczvU2Jg8w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_aUIhEC1QEeCupczvU2Jg8w" source="_TPQwIC1QEeCupczvU2Jg8w" target="_UAy6wC1QEeCupczvU2Jg8w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/forwardTransformationActivityDiagram"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0VRaUC6mEeCRIK6l8KTurA" source="_qAZ4QC6mEeCRIK6l8KTurA" target="_qAZ4Qy6mEeCRIK6l8KTurA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1EWG0C6mEeCRIK6l8KTurA" source="_qzC2LC6mEeCRIK6l8KTurA" target="_qzC2Ly6mEeCRIK6l8KTurA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_12A0UC6mEeCRIK6l8KTurA" source="_TPQwIC1QEeCupczvU2Jg8w" target="_qAZ4QC6mEeCRIK6l8KTurA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/mappingTransformationActivityDiagram"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3nFJUC6mEeCRIK6l8KTurA" source="_TPQwIC1QEeCupczvU2Jg8w" target="_qzC2LC6mEeCRIK6l8KTurA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/reverseTransformationActivityDiagram"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hhdjwC6rEeCEjs6G7WC8Uw" source="_TPQwIC1QEeCupczvU2Jg8w" target="_Ze5bgC6rEeCEjs6G7WC8Uw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/forwardTransformationOperation"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_i1TKMC6rEeCEjs6G7WC8Uw" source="_VDmbkC1QEeCupczvU2Jg8w" target="_Ze5bgC6rEeCEjs6G7WC8Uw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lGtMIC6rEeCEjs6G7WC8Uw" source="_qAZ4Qy6mEeCRIK6l8KTurA" target="_aip-kC6rEeCEjs6G7WC8Uw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_mFfvoC6rEeCEjs6G7WC8Uw" source="_qzC2Ly6mEeCRIK6l8KTurA" target="_bO4ycC6rEeCEjs6G7WC8Uw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ofnBoC6rEeCEjs6G7WC8Uw" source="_TPQwIC1QEeCupczvU2Jg8w" target="_aip-kC6rEeCEjs6G7WC8Uw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/mappingTransformationOperation"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_p3zjoC6rEeCEjs6G7WC8Uw" source="_TPQwIC1QEeCupczvU2Jg8w" target="_bO4ycC6rEeCEjs6G7WC8Uw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/reverseTransformationOperation"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of forwardFindMatchesActivity" uuid="_FOgyMD5nEeCS07xt5ugfeQ" incoming="_diVwMD5sEeCS07xt5ugfeQ" outgoing="_i11qID5sEeCS07xt5ugfeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_QKVmkD5nEeCS07xt5ugfeQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_QvStwD5nEeCS07xt5ugfeQ">
          <activity href="create_findMatches_activity.story#_vRRsIBfVEeCVjoDp-muf0Q"/>
          <parameters name="tggRule" uuid="_ThvnQD5nEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Rc9SwD5sEeCS07xt5ugfeQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_UQqiwD5nEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_URlWgD5sEeCS07xt5ugfeQ" expressionString="mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="activity" uuid="_U87L0D5nEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_V4gRAD5sEeCS07xt5ugfeQ" expressionString="forwardFindMatchesActivity" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of mappingFindMatchesActivity" uuid="_ZW9U6T5sEeCS07xt5ugfeQ" incoming="_i11qID5sEeCS07xt5ugfeQ" outgoing="_jSYcMD5sEeCS07xt5ugfeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_ZW9U6j5sEeCS07xt5ugfeQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_ZW9U6z5sEeCS07xt5ugfeQ">
          <activity href="create_findMatches_activity.story#_vRRsIBfVEeCVjoDp-muf0Q"/>
          <parameters name="tggRule" uuid="_ZW9U8D5sEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZW9U8T5sEeCS07xt5ugfeQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_ZW9U7D5sEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZW9U7T5sEeCS07xt5ugfeQ" expressionString="mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="activity" uuid="_ZW9U7j5sEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZW9U7z5sEeCS07xt5ugfeQ" expressionString="mappingFindMatchesActivity" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of reverseFindMatchesActivity" uuid="_ap48aT5sEeCS07xt5ugfeQ" incoming="_jSYcMD5sEeCS07xt5ugfeQ" outgoing="_jq8GID5sEeCS07xt5ugfeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_ap48aj5sEeCS07xt5ugfeQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_ap48az5sEeCS07xt5ugfeQ">
          <activity href="create_findMatches_activity.story#_vRRsIBfVEeCVjoDp-muf0Q"/>
          <parameters name="tggRule" uuid="_ap48bD5sEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ap48bT5sEeCS07xt5ugfeQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_ap48bj5sEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ap48bz5sEeCS07xt5ugfeQ" expressionString="mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="activity" uuid="_ap48cD5sEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ap48cT5sEeCS07xt5ugfeQ" expressionString="reverseFindMatchesActivity" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of forwardConflictCheckActivity" uuid="_7BCC0D5sEeCS07xt5ugfeQ" incoming="_v2ckQD5tEeCS07xt5ugfeQ" outgoing="_wkTIwD5tEeCS07xt5ugfeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_9n9o0D5sEeCS07xt5ugfeQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_-mLkkD5sEeCS07xt5ugfeQ">
          <activity href="create_conflictCheck_activity.story#_5mXbcBmoEeCeMPIFRfkCeQ"/>
          <parameters name="tggRule" uuid="_CZjIgD5tEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gelCYD5tEeCS07xt5ugfeQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_Ccr8ED5tEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k_UCoD5tEeCS07xt5ugfeQ" expressionString="mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="ruleInfos" uuid="_Che8gD5tEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_oafwED5tEeCS07xt5ugfeQ" expressionString="ruleInfoStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
          </parameters>
          <parameters name="activity" uuid="_8yb1YD50EeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-KP84D50EeCS07xt5ugfeQ" expressionString="forwardConflictCheckActivity" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of mappingConflictCheckActivity" uuid="_rZIreT5tEeCS07xt5ugfeQ" incoming="_wkTIwD5tEeCS07xt5ugfeQ" outgoing="_w2iIQD5tEeCS07xt5ugfeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_rZIrej5tEeCS07xt5ugfeQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_rZIrez5tEeCS07xt5ugfeQ">
          <activity href="create_conflictCheck_activity.story#_5mXbcBmoEeCeMPIFRfkCeQ"/>
          <parameters name="tggRule" uuid="_rZIrfD5tEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rZIrfT5tEeCS07xt5ugfeQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_rZIrgD5tEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rZIrgT5tEeCS07xt5ugfeQ" expressionString="mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="ruleInfos" uuid="_rZIrfj5tEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rZIrfz5tEeCS07xt5ugfeQ" expressionString="ruleInfoStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
          </parameters>
          <parameters name="activity" uuid="___Ia4D50EeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BNo1YD51EeCS07xt5ugfeQ" expressionString="mappingConflictCheckActivity" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of reverseConflictCheckActivity" uuid="_seLn-T5tEeCS07xt5ugfeQ" incoming="_w2iIQD5tEeCS07xt5ugfeQ" outgoing="_SsjEoC1QEeCupczvU2Jg8w">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_seLn-j5tEeCS07xt5ugfeQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_seLn-z5tEeCS07xt5ugfeQ">
          <activity href="create_conflictCheck_activity.story#_5mXbcBmoEeCeMPIFRfkCeQ"/>
          <parameters name="tggRule" uuid="_seLoAD5tEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_seLoAT5tEeCS07xt5ugfeQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_seLn_D5tEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_seLn_T5tEeCS07xt5ugfeQ" expressionString="mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="ruleInfos" uuid="_seLn_j5tEeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_seLn_z5tEeCS07xt5ugfeQ" expressionString="ruleInfoStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
          </parameters>
          <parameters name="activity" uuid="_CoV40D51EeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ds2p0D51EeCS07xt5ugfeQ" expressionString="reverseConflictCheckActivity" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of forwardTransformationActivity" uuid="_lFuIcD50EeCS07xt5ugfeQ" incoming="_OpQtAD51EeCS07xt5ugfeQ" outgoing="_PUrBgD51EeCS07xt5ugfeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_rVrDkD50EeCS07xt5ugfeQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_r75WED50EeCS07xt5ugfeQ">
          <activity href="create_transformation_activity.story#_vO1ZYC1QEeCupczvU2Jg8w"/>
          <parameters name="ruleInfoStore" uuid="_uYwaED50EeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_yERCgD50EeCS07xt5ugfeQ" expressionString="ruleInfoStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
          </parameters>
          <parameters name="direction" uuid="_udLnED50EeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0J7ugD50EeCS07xt5ugfeQ" expressionString="mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="activity" uuid="_ugxGkD50EeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3T368D50EeCS07xt5ugfeQ" expressionString="forwardTransformationActivity" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of mappingTransformationActivity" uuid="_GjWV2T51EeCS07xt5ugfeQ" incoming="_PUrBgD51EeCS07xt5ugfeQ" outgoing="_P1jhAD51EeCS07xt5ugfeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_GjWV2j51EeCS07xt5ugfeQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_GjWV2z51EeCS07xt5ugfeQ">
          <activity href="create_transformation_activity.story#_vO1ZYC1QEeCupczvU2Jg8w"/>
          <parameters name="ruleInfoStore" uuid="_GjWV3j51EeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GjWV3z51EeCS07xt5ugfeQ" expressionString="ruleInfoStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
          </parameters>
          <parameters name="direction" uuid="_GjWV3D51EeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GjWV3T51EeCS07xt5ugfeQ" expressionString="mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="activity" uuid="_GjWV4D51EeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GjWV4T51EeCS07xt5ugfeQ" expressionString="mappingTransformationActivity" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of reverseTransformationActivity" uuid="_HSq5-T51EeCS07xt5ugfeQ" incoming="_P1jhAD51EeCS07xt5ugfeQ" outgoing="_qQPYcC1QEeCupczvU2Jg8w">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_HSq5-j51EeCS07xt5ugfeQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_HSq5-z51EeCS07xt5ugfeQ">
          <activity href="create_transformation_activity.story#_vO1ZYC1QEeCupczvU2Jg8w"/>
          <parameters name="ruleInfoStore" uuid="_HSq6AD51EeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_HSq6AT51EeCS07xt5ugfeQ" expressionString="ruleInfoStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
          </parameters>
          <parameters name="direction" uuid="_HSq5_j51EeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_HSq5_z51EeCS07xt5ugfeQ" expressionString="mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="activity" uuid="_HSq5_D51EeCS07xt5ugfeQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_HSq5_T51EeCS07xt5ugfeQ" expressionString="reverseTransformationActivity" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create findSourceMatches activities" uuid="_r1Go1EPeEeCIrIF3qEr5xA" incoming="_jq8GID5sEeCS07xt5ugfeQ" outgoing="_bK3OIEPgEeCIrIF3qEr5xA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfoStore" uuid="_r1HP4EPeEeCIrIF3qEr5xA" outgoingStoryLinks="_r1HP3UPeEeCIrIF3qEr5xA _r1HP1EPeEeCIrIF3qEr5xA _r1HP30PeEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardFindSourceMatchesAD" uuid="_r1HP2kPeEeCIrIF3qEr5xA" outgoingStoryLinks="_r1HP0EPeEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardFindSourceMatchesActivity" uuid="_r1HP3EPeEeCIrIF3qEr5xA" incomingStoryLinks="_r1HP0EPeEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingFindSourceMatchesActivity" uuid="_r1HP0kPeEeCIrIF3qEr5xA" incomingStoryLinks="_r1HP2UPeEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingFindSourceMatchesAD" uuid="_r1HP0UPeEeCIrIF3qEr5xA" outgoingStoryLinks="_r1HP2UPeEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseFindSourceMatchesAD" uuid="_r1HP3kPeEeCIrIF3qEr5xA" outgoingStoryLinks="_r1HP00PeEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseFindSourceMatchesActivity" uuid="_r1HP20PeEeCIrIF3qEr5xA" incomingStoryLinks="_r1HP00PeEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ffsm" uuid="_r1HP4UPeEeCIrIF3qEr5xA" incomingStoryLinks="_r1HP3UPeEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r1HP4kPeEeCIrIF3qEr5xA" expressionString="'forwardFindSourceMatches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mfsm" uuid="_r1HP10PeEeCIrIF3qEr5xA" incomingStoryLinks="_r1HP1EPeEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r1HP2EPeEeCIrIF3qEr5xA" expressionString="'mappingFindSourceMatches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rfsm" uuid="_r1HP1UPeEeCIrIF3qEr5xA" incomingStoryLinks="_r1HP30PeEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r1HP1kPeEeCIrIF3qEr5xA" expressionString="'reverseFindSourceMatches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_r1HP0EPeEeCIrIF3qEr5xA" source="_r1HP2kPeEeCIrIF3qEr5xA" target="_r1HP3EPeEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_r1HP2UPeEeCIrIF3qEr5xA" source="_r1HP0UPeEeCIrIF3qEr5xA" target="_r1HP0kPeEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_r1HP00PeEeCIrIF3qEr5xA" source="_r1HP3kPeEeCIrIF3qEr5xA" target="_r1HP20PeEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_r1HP3UPeEeCIrIF3qEr5xA" source="_r1HP4EPeEeCIrIF3qEr5xA" target="_r1HP4UPeEeCIrIF3qEr5xA" valueTarget="_r1HP2kPeEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_r1HP1EPeEeCIrIF3qEr5xA" source="_r1HP4EPeEeCIrIF3qEr5xA" target="_r1HP10PeEeCIrIF3qEr5xA" valueTarget="_r1HP0UPeEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_r1HP30PeEeCIrIF3qEr5xA" source="_r1HP4EPeEeCIrIF3qEr5xA" target="_r1HP1UPeEeCIrIF3qEr5xA" valueTarget="_r1HP3kPeEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of forwardFindSourceMatchesActivity" uuid="_XBxR2UPgEeCIrIF3qEr5xA" incoming="_bK3OIEPgEeCIrIF3qEr5xA" outgoing="_blt-IEPgEeCIrIF3qEr5xA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_XBxR2kPgEeCIrIF3qEr5xA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_XBxR20PgEeCIrIF3qEr5xA">
          <activity href="create_findSourceMatches_activity.story#_MBPZEEPfEeCIrIF3qEr5xA"/>
          <parameters name="tggRule" uuid="_XBxR3EPgEeCIrIF3qEr5xA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XBxR3UPgEeCIrIF3qEr5xA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_XBxR4EPgEeCIrIF3qEr5xA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XBxR4UPgEeCIrIF3qEr5xA" expressionString="mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="activity" uuid="_XBxR3kPgEeCIrIF3qEr5xA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XBxR30PgEeCIrIF3qEr5xA" expressionString="forwardFindSourceMatchesActivity" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of mappingFindSourceMatchesActivity" uuid="_g7mSUEQPEeCj3vsEcE-cfw" incoming="_blt-IEPgEeCIrIF3qEr5xA" outgoing="_i6r1MEQPEeCj3vsEcE-cfw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_g7mSUUQPEeCj3vsEcE-cfw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_g7mSUkQPEeCj3vsEcE-cfw">
          <activity href="create_findSourceMatches_activity.story#_MBPZEEPfEeCIrIF3qEr5xA"/>
          <parameters name="tggRule" uuid="_g7mSU0QPEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_g7mSVEQPEeCj3vsEcE-cfw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_g7mSVUQPEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_g7mSVkQPEeCj3vsEcE-cfw" expressionString="mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="activity" uuid="_g7mSV0QPEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_g7mSWEQPEeCj3vsEcE-cfw" expressionString="mappingFindSourceMatchesActivity" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of reverseFindSourceMatchesActivity" uuid="_hTnwyUQPEeCj3vsEcE-cfw" incoming="_i6r1MEQPEeCj3vsEcE-cfw" outgoing="_jLdcIEQPEeCj3vsEcE-cfw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_hTnwykQPEeCj3vsEcE-cfw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_hTnwy0QPEeCj3vsEcE-cfw">
          <activity href="create_findSourceMatches_activity.story#_MBPZEEPfEeCIrIF3qEr5xA"/>
          <parameters name="tggRule" uuid="_hTnwzEQPEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hTnwzUQPEeCj3vsEcE-cfw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_hTnw0EQPEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hTnw0UQPEeCj3vsEcE-cfw" expressionString="mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="activity" uuid="_hTnwzkQPEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hTnwz0QPEeCj3vsEcE-cfw" expressionString="reverseFindSourceMatchesActivity" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <edges uuid="_H4a-UCocEeCV66-7fXb9cw" source="_8ETQACobEeCV66-7fXb9cw" target="_8pSMYCobEeCV66-7fXb9cw"/>
    <edges uuid="_QU6NACodEeCV66-7fXb9cw" source="_8pSMYCobEeCV66-7fXb9cw" target="_tIa0oCodEeCV66-7fXb9cw"/>
    <edges uuid="_yZVCsCofEeCrWJNIJyAZ9w" source="_tIa0oCodEeCV66-7fXb9cw" target="_phyaQCofEeCrWJNIJyAZ9w" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_y4r4MCofEeCrWJNIJyAZ9w" expressionString="tggRule.isAxiom" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_0nxQMCofEeCrWJNIJyAZ9w" source="_phyaQCofEeCrWJNIJyAZ9w" target="_0Ss_ICofEeCrWJNIJyAZ9w"/>
    <edges uuid="_2FwRICofEeCrWJNIJyAZ9w" source="_tIa0oCodEeCV66-7fXb9cw" target="_2QeawCocEeCV66-7fXb9cw" guardType="ELSE"/>
    <edges uuid="_SsjEoC1QEeCupczvU2Jg8w" source="_seLn-T5tEeCS07xt5ugfeQ" target="_QUV3IC1QEeCupczvU2Jg8w"/>
    <edges uuid="_qQPYcC1QEeCupczvU2Jg8w" source="_HSq5-T51EeCS07xt5ugfeQ" target="_3CUnECofEeCrWJNIJyAZ9w"/>
    <edges uuid="_diVwMD5sEeCS07xt5ugfeQ" source="_2QeawCocEeCV66-7fXb9cw" target="_FOgyMD5nEeCS07xt5ugfeQ"/>
    <edges uuid="_i11qID5sEeCS07xt5ugfeQ" source="_FOgyMD5nEeCS07xt5ugfeQ" target="_ZW9U6T5sEeCS07xt5ugfeQ"/>
    <edges uuid="_jSYcMD5sEeCS07xt5ugfeQ" source="_ZW9U6T5sEeCS07xt5ugfeQ" target="_ap48aT5sEeCS07xt5ugfeQ"/>
    <edges uuid="_jq8GID5sEeCS07xt5ugfeQ" source="_ap48aT5sEeCS07xt5ugfeQ" target="_r1Go1EPeEeCIrIF3qEr5xA"/>
    <edges uuid="_v2ckQD5tEeCS07xt5ugfeQ" source="_VwjYgCosEeCjTcyrv1LVTA" target="_7BCC0D5sEeCS07xt5ugfeQ"/>
    <edges uuid="_wkTIwD5tEeCS07xt5ugfeQ" source="_7BCC0D5sEeCS07xt5ugfeQ" target="_rZIreT5tEeCS07xt5ugfeQ"/>
    <edges uuid="_w2iIQD5tEeCS07xt5ugfeQ" source="_rZIreT5tEeCS07xt5ugfeQ" target="_seLn-T5tEeCS07xt5ugfeQ"/>
    <edges uuid="_OpQtAD51EeCS07xt5ugfeQ" source="_QUV3IC1QEeCupczvU2Jg8w" target="_lFuIcD50EeCS07xt5ugfeQ"/>
    <edges uuid="_PUrBgD51EeCS07xt5ugfeQ" source="_lFuIcD50EeCS07xt5ugfeQ" target="_GjWV2T51EeCS07xt5ugfeQ"/>
    <edges uuid="_P1jhAD51EeCS07xt5ugfeQ" source="_GjWV2T51EeCS07xt5ugfeQ" target="_HSq5-T51EeCS07xt5ugfeQ"/>
    <edges uuid="_bK3OIEPgEeCIrIF3qEr5xA" source="_r1Go1EPeEeCIrIF3qEr5xA" target="_XBxR2UPgEeCIrIF3qEr5xA"/>
    <edges uuid="_blt-IEPgEeCIrIF3qEr5xA" source="_XBxR2UPgEeCIrIF3qEr5xA" target="_g7mSUEQPEeCj3vsEcE-cfw"/>
    <edges uuid="_i6r1MEQPEeCj3vsEcE-cfw" source="_g7mSUEQPEeCj3vsEcE-cfw" target="_hTnwyUQPEeCj3vsEcE-cfw"/>
    <edges uuid="_jLdcIEQPEeCj3vsEcE-cfw" source="_hTnwyUQPEeCj3vsEcE-cfw" target="_VwjYgCosEeCjTcyrv1LVTA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
