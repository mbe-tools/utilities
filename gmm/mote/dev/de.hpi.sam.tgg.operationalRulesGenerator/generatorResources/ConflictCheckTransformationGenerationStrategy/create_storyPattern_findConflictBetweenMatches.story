<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_findConflictBetweenMatches" uuid="_b7yLsFOtEeCY5NYqK2RsYA">
  <activities name="create_storyPattern_findConflictBetweenMatches" uuid="_cfNB0FOtEeCY5NYqK2RsYA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_eIxAoFOtEeCY5NYqK2RsYA" outgoing="_nVcLkFOtEeCY5NYqK2RsYA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_eeIMoFOtEeCY5NYqK2RsYA" incoming="_nVcLkFOtEeCY5NYqK2RsYA" outgoing="_wl1_8FOtEeCY5NYqK2RsYA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfoStore" uuid="_gR8ToFOtEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_jeK_EFOtEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_oqujgFOtEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create otherMatchStorageSPO, otherMatchSPO and matchSPO" uuid="_qcZVcFOtEeCY5NYqK2RsYA" incoming="_wl1_8FOtEeCY5NYqK2RsYA" outgoing="_xl7j8FOtEeCY5NYqK2RsYA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="otherMatchStorageSPO" uuid="_ztpeYFOtEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_mipnQFOuEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nX50QFOuEeCY5NYqK2RsYA" expressionString="'otherMatchStore'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_VHCsAFOuEeCY5NYqK2RsYA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_Va4OAFOuEeCY5NYqK2RsYA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_matchStore_spo.story#_X128ABgAEeCaHaBYHoSzPw"/>
            <parameters name="storyActionNode" uuid="_X3k58FOuEeCY5NYqK2RsYA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_aqQc8FOuEeCY5NYqK2RsYA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="modifier" uuid="_aITmcFOuEeCY5NYqK2RsYA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ePgsYFOuEeCY5NYqK2RsYA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
            </parameters>
            <parameters name="bindingType" uuid="_gC3gYFOuEeCY5NYqK2RsYA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_heRHYFOuEeCY5NYqK2RsYA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="otherMatchSPO" uuid="_rqjVMFOuEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_vv6WIFOwEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wybjEFOwEeCY5NYqK2RsYA" expressionString="'otherMatch'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_vQResFOuEeCY5NYqK2RsYA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_vjkOIFOuEeCY5NYqK2RsYA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_match_spo.story#_6d114Bi_EeCuQ-PF2tTDdw"/>
            <parameters name="storyActionNode" uuid="_yBQ_oFOuEeCY5NYqK2RsYA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_z-KKIFOuEeCY5NYqK2RsYA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="modifier" uuid="_07ackFOuEeCY5NYqK2RsYA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2lsNEFOuEeCY5NYqK2RsYA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
            </parameters>
            <parameters name="bindingType" uuid="_4dCJMFOuEeCY5NYqK2RsYA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5n2WgFOuEeCY5NYqK2RsYA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchSPO" uuid="_CJYn0FOxEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_CJYn2VOxEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_CJYn2lOxEeCY5NYqK2RsYA" expressionString="'match'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_CJYn0VOxEeCY5NYqK2RsYA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_CJYn0lOxEeCY5NYqK2RsYA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_match_spo.story#_6d114Bi_EeCuQ-PF2tTDdw"/>
            <parameters name="storyActionNode" uuid="_CJYn1VOxEeCY5NYqK2RsYA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_CJYn1lOxEeCY5NYqK2RsYA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="modifier" uuid="_CJYn11OxEeCY5NYqK2RsYA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_CJYn2FOxEeCY5NYqK2RsYA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
            </parameters>
            <parameters name="bindingType" uuid="_CJYn01OxEeCY5NYqK2RsYA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_CJYn1FOxEeCY5NYqK2RsYA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_w7NL8FOtEeCY5NYqK2RsYA" incoming="_lfc9gFOxEeCY5NYqK2RsYA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create otherMatchStorage->matches link" uuid="_-9V0gFOuEeCY5NYqK2RsYA" incoming="_xl7j8FOtEeCY5NYqK2RsYA" outgoing="_obKEMFOyEeCY5NYqK2RsYA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_C3WL8FOvEeCY5NYqK2RsYA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_DXY98FOvEeCY5NYqK2RsYA">
          <activity href="../common/create_matchStore_matches_link.story#_57M7gBjCEeCuQ-PF2tTDdw"/>
          <parameters name="matchStoreSpo" uuid="_EBV54FOvEeCY5NYqK2RsYA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IMDm0FOvEeCY5NYqK2RsYA" expressionString="otherMatchStorageSPO" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="matchSpo" uuid="_JoS7UFOvEeCY5NYqK2RsYA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_LtDBUFOvEeCY5NYqK2RsYA" expressionString="otherMatchSPO" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="modifier" uuid="_gQNt8FOwEeCY5NYqK2RsYA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ilMBQFOwEeCY5NYqK2RsYA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set story pattern constraint" uuid="_SDR4sFOxEeCY5NYqK2RsYA" incoming="_qNqjoFOyEeCY5NYqK2RsYA" outgoing="_lfc9gFOxEeCY5NYqK2RsYA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_UwiwMFOxEeCY5NYqK2RsYA" outgoingStoryLinks="_ZIExIFOxEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="stringExpression" uuid="_XXY2oFOxEeCY5NYqK2RsYA" modifier="CREATE" incomingStoryLinks="_ZIExIFOxEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_ZozfoFOxEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_abm1kFOxEeCY5NYqK2RsYA" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_bYt-EFOxEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_c-MHEFOxEeCY5NYqK2RsYA" expressionString="if direction = mote::TransformationDirection::FORWARD then&#xA;&#x9;'not (match.sourceCreatedElements.value->asSet()->intersection(otherMatch.sourceCreatedElements.value->asSet())->isEmpty() or (this = rule and match.sourceCreatedElements.value->asSet()->includesAll(otherMatch.sourceCreatedElements.value->asSet()) and match.applicationContext.value->asSet()->includesAll(otherMatch.applicationContext.value->asSet()) and otherMatch.sourceCreatedElements.value->asSet()->includesAll(match.sourceCreatedElements.value->asSet()) and otherMatch.applicationContext.value->asSet()->includesAll(match.applicationContext.value->asSet())))'&#xA;else&#xA;&#x9;if direction = mote::TransformationDirection::MAPPING then&#xA;&#x9;&#x9;'not (match.corrCreatedElements.value->asSet()->intersection(otherMatch.corrCreatedElements.value->asSet())->isEmpty() or (this = rule and match.corrCreatedElements.value->asSet()->includesAll(otherMatch.corrCreatedElements.value->asSet()) and match.applicationContext.value->asSet()->includesAll(otherMatch.applicationContext.value->asSet()) and otherMatch.corrCreatedElements.value->asSet()->includesAll(match.corrCreatedElements.value->asSet()) and otherMatch.applicationContext.value->asSet()->includesAll(match.applicationContext.value->asSet())))'&#xA;&#x9;else&#xA;&#x9;&#x9;'not (match.targetCreatedElements.value->asSet()->intersection(otherMatch.targetCreatedElements.value->asSet())->isEmpty() or (this = rule and match.targetCreatedElements.value->asSet()->includesAll(otherMatch.targetCreatedElements.value->asSet()) and match.applicationContext.value->asSet()->includesAll(otherMatch.applicationContext.value->asSet()) and otherMatch.targetCreatedElements.value->asSet()->includesAll(match.targetCreatedElements.value->asSet()) and otherMatch.applicationContext.value->asSet()->includesAll(match.applicationContext.value->asSet())))'&#xA;&#x9;endif&#xA;endif" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZIExIFOxEeCY5NYqK2RsYA" modifier="CREATE" source="_UwiwMFOxEeCY5NYqK2RsYA" target="_XXY2oFOxEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/constraints"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create direct assignment expression for otherMatchStorageSPO" uuid="_mdp1sFOyEeCY5NYqK2RsYA" incoming="_obKEMFOyEeCY5NYqK2RsYA" outgoing="_qNqjoFOyEeCY5NYqK2RsYA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="otherMatchStorageSPO" uuid="_ssHjEFOyEeCY5NYqK2RsYA" outgoingStoryLinks="_xHZbkFOyEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_va6EkFOyEeCY5NYqK2RsYA" modifier="CREATE" outgoingStoryLinks="_8mPykFO0EeCY5NYqK2RsYA" incomingStoryLinks="_xHZbkFOyEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="callSDIAction" uuid="_6WldEFO0EeCY5NYqK2RsYA" modifier="CREATE" outgoingStoryLinks="_OBE1QFO2EeCY5NYqK2RsYA _bbgaEFO2EeCY5NYqK2RsYA _7xr3MFO2EeCY5NYqK2RsYA _BAw7cFPIEeCMH6_3CQDAJg" incomingStoryLinks="_8mPykFO0EeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_-i-lAFO0EeCY5NYqK2RsYA" incomingStoryLinks="_gt5WMFO1EeCY5NYqK2RsYA _OBE1QFO2EeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfoStore" uuid="__cHzgFO0EeCY5NYqK2RsYA" outgoingStoryLinks="_CQhYUFO2EeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activityDiagram" uuid="_emZeMFO1EeCY5NYqK2RsYA" outgoingStoryLinks="_gt5WMFO1EeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="key" uuid="_0snucFO1EeCY5NYqK2RsYA" incomingStoryLinks="_CQhYUFO2EeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5xeIYFO1EeCY5NYqK2RsYA" expressionString="if direction = mote::TransformationDirection::FORWARD then&#xA;&#x9;'forwardFindSourceMatches'&#xA;else&#xA;&#x9;if direction = mote::TransformationDirection::MAPPING then&#xA;&#x9;&#x9;'mappingFindSourceMatches'&#xA;&#x9;else&#xA;&#x9;&#x9;'reverseFindSourceMatches'&#xA;&#x9;endif&#xA;endif" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchStorageEClass" uuid="_VcFOQFO2EeCY5NYqK2RsYA" incomingStoryLinks="_bbgaEFO2EeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y4iuEFO2EeCY5NYqK2RsYA" expressionString="mote::rules::MatchStorage" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="parameter" uuid="_0p1qwFO2EeCY5NYqK2RsYA" modifier="CREATE" outgoingStoryLinks="_8demMFO2EeCY5NYqK2RsYA _AVbIIFO3EeCY5NYqK2RsYA" incomingStoryLinks="_7xr3MFO2EeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_CtPUEFO3EeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Db7mEFO3EeCY5NYqK2RsYA" expressionString="'matchedModelElement'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eObjectEClass" uuid="_5NMhMFO2EeCY5NYqK2RsYA" incomingStoryLinks="_8demMFO2EeCY5NYqK2RsYA _LORhAFO3EeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6tP-sFO2EeCY5NYqK2RsYA" expressionString="ecore::EObject" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae2" uuid="_-LLeIFO2EeCY5NYqK2RsYA" modifier="CREATE" outgoingStoryLinks="_F9-sEFO3EeCY5NYqK2RsYA" incomingStoryLinks="_AVbIIFO3EeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varRef" uuid="_A_TyoFO3EeCY5NYqK2RsYA" modifier="CREATE" outgoingStoryLinks="_LORhAFO3EeCY5NYqK2RsYA" incomingStoryLinks="_F9-sEFO3EeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_HSC8AFO3EeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_J2J3gFO3EeCY5NYqK2RsYA" expressionString="'matchedModelElement'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisParameter" uuid="_z6PKEFPHEeCMH6_3CQDAJg" modifier="CREATE" outgoingStoryLinks="__nW4gFPHEeCMH6_3CQDAJg _H0stYFPIEeCMH6_3CQDAJg" incomingStoryLinks="_BAw7cFPIEeCMH6_3CQDAJg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_7Kd2cFPHEeCMH6_3CQDAJg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9gVhoFPHEeCMH6_3CQDAJg" expressionString="'this'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae3" uuid="_1KpCAFPHEeCMH6_3CQDAJg" modifier="CREATE" outgoingStoryLinks="_HScU4FPIEeCMH6_3CQDAJg" incomingStoryLinks="__nW4gFPHEeCMH6_3CQDAJg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisVarRef" uuid="_1oxvgFPHEeCMH6_3CQDAJg" modifier="CREATE" outgoingStoryLinks="_IkJ0YFPIEeCMH6_3CQDAJg" incomingStoryLinks="_HScU4FPIEeCMH6_3CQDAJg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_JQ2iUFPIEeCMH6_3CQDAJg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KNES8FPIEeCMH6_3CQDAJg" expressionString="'this'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleEClass" uuid="_Cjjj4FPIEeCMH6_3CQDAJg" incomingStoryLinks="_H0stYFPIEeCMH6_3CQDAJg _IkJ0YFPIEeCMH6_3CQDAJg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ETzZgFPIEeCMH6_3CQDAJg" expressionString="mote::rules::TGGRule" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xHZbkFOyEeCY5NYqK2RsYA" modifier="CREATE" source="_ssHjEFOyEeCY5NYqK2RsYA" target="_va6EkFOyEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8mPykFO0EeCY5NYqK2RsYA" modifier="CREATE" source="_va6EkFOyEeCY5NYqK2RsYA" target="_6WldEFO0EeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_gt5WMFO1EeCY5NYqK2RsYA" source="_emZeMFO1EeCY5NYqK2RsYA" target="_-i-lAFO0EeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_CQhYUFO2EeCY5NYqK2RsYA" source="__cHzgFO0EeCY5NYqK2RsYA" target="_0snucFO1EeCY5NYqK2RsYA" valueTarget="_emZeMFO1EeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_OBE1QFO2EeCY5NYqK2RsYA" modifier="CREATE" source="_6WldEFO0EeCY5NYqK2RsYA" target="_-i-lAFO0EeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/activity"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_bbgaEFO2EeCY5NYqK2RsYA" modifier="CREATE" source="_6WldEFO0EeCY5NYqK2RsYA" target="_VcFOQFO2EeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7xr3MFO2EeCY5NYqK2RsYA" modifier="CREATE" source="_6WldEFO0EeCY5NYqK2RsYA" target="_0p1qwFO2EeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8demMFO2EeCY5NYqK2RsYA" modifier="CREATE" source="_0p1qwFO2EeCY5NYqK2RsYA" target="_5NMhMFO2EeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_AVbIIFO3EeCY5NYqK2RsYA" modifier="CREATE" source="_0p1qwFO2EeCY5NYqK2RsYA" target="_-LLeIFO2EeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_F9-sEFO3EeCY5NYqK2RsYA" modifier="CREATE" source="_-LLeIFO2EeCY5NYqK2RsYA" target="_A_TyoFO3EeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LORhAFO3EeCY5NYqK2RsYA" modifier="CREATE" source="_A_TyoFO3EeCY5NYqK2RsYA" target="_5NMhMFO2EeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="__nW4gFPHEeCMH6_3CQDAJg" modifier="CREATE" source="_z6PKEFPHEeCMH6_3CQDAJg" target="_1KpCAFPHEeCMH6_3CQDAJg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BAw7cFPIEeCMH6_3CQDAJg" modifier="CREATE" source="_6WldEFO0EeCY5NYqK2RsYA" target="_z6PKEFPHEeCMH6_3CQDAJg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_HScU4FPIEeCMH6_3CQDAJg" modifier="CREATE" source="_1KpCAFPHEeCMH6_3CQDAJg" target="_1oxvgFPHEeCMH6_3CQDAJg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_H0stYFPIEeCMH6_3CQDAJg" modifier="CREATE" source="_z6PKEFPHEeCMH6_3CQDAJg" target="_Cjjj4FPIEeCMH6_3CQDAJg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_IkJ0YFPIEeCMH6_3CQDAJg" modifier="CREATE" source="_1oxvgFPHEeCMH6_3CQDAJg" target="_Cjjj4FPIEeCMH6_3CQDAJg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_nVcLkFOtEeCY5NYqK2RsYA" source="_eIxAoFOtEeCY5NYqK2RsYA" target="_eeIMoFOtEeCY5NYqK2RsYA"/>
    <edges uuid="_wl1_8FOtEeCY5NYqK2RsYA" source="_eeIMoFOtEeCY5NYqK2RsYA" target="_qcZVcFOtEeCY5NYqK2RsYA"/>
    <edges uuid="_xl7j8FOtEeCY5NYqK2RsYA" source="_qcZVcFOtEeCY5NYqK2RsYA" target="_-9V0gFOuEeCY5NYqK2RsYA"/>
    <edges uuid="_lfc9gFOxEeCY5NYqK2RsYA" source="_SDR4sFOxEeCY5NYqK2RsYA" target="_w7NL8FOtEeCY5NYqK2RsYA"/>
    <edges uuid="_obKEMFOyEeCY5NYqK2RsYA" source="_-9V0gFOuEeCY5NYqK2RsYA" target="_mdp1sFOyEeCY5NYqK2RsYA"/>
    <edges uuid="_qNqjoFOyEeCY5NYqK2RsYA" source="_mdp1sFOyEeCY5NYqK2RsYA" target="_SDR4sFOxEeCY5NYqK2RsYA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
