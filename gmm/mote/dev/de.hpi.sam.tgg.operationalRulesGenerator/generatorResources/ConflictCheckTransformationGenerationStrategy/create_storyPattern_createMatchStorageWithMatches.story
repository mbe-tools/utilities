<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_createMatchStorageWithMatches" uuid="_4ip-0C34EeCSbYYn7Hqbqw">
  <activities name="create_storyPattern_createMatchStorageWithMatches" uuid="_5Jt-0C34EeCSbYYn7Hqbqw">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_Tedd8C35EeCuxJPrY_7nRQ" outgoing="_8DD4kC36EeCuxJPrY_7nRQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_UNQdoC35EeCuxJPrY_7nRQ" incoming="_8DD4kC36EeCuxJPrY_7nRQ" outgoing="_ypYaIC3_EeCuxJPrY_7nRQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfoStore" uuid="_vuLmkC36EeCuxJPrY_7nRQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_w8YfEC36EeCuxJPrY_7nRQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_ySIKAC36EeCuxJPrY_7nRQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create matchStoreSpo" uuid="__ymFYC36EeCuxJPrY_7nRQ" incoming="_ypYaIC3_EeCuxJPrY_7nRQ" outgoing="_z-6CoC3_EeCuxJPrY_7nRQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchStoreSpo" uuid="_DOx4YC37EeCuxJPrY_7nRQ" outgoingStoryLinks="__qomwC37EeCuxJPrY_7nRQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_TpsLoC37EeCuxJPrY_7nRQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_UAlBoC37EeCuxJPrY_7nRQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_matchStore_spo.story#_X128ABgAEeCaHaBYHoSzPw"/>
            <parameters name="storyActionNode" uuid="_V6kpIC37EeCuxJPrY_7nRQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZBO4EC37EeCuxJPrY_7nRQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="modifier" uuid="_broo0C37EeCuxJPrY_7nRQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dU8wAC37EeCuxJPrY_7nRQ" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
            </parameters>
            <parameters name="bindingType" uuid="_fLDWAC37EeCuxJPrY_7nRQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gY8sgC37EeCuxJPrY_7nRQ" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_k4gBAC37EeCuxJPrY_7nRQ" modifier="CREATE" outgoingStoryLinks="_HPKvMC38EeCuxJPrY_7nRQ" incomingStoryLinks="__qomwC37EeCuxJPrY_7nRQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="callSDIExpression" uuid="_lhAygC37EeCuxJPrY_7nRQ" modifier="CREATE" outgoingStoryLinks="_YNt7AC38EeCuxJPrY_7nRQ _kf6FYC38EeCuxJPrY_7nRQ _lXNg0C38EeCuxJPrY_7nRQ" incomingStoryLinks="_HPKvMC38EeCuxJPrY_7nRQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction"/>
        <attributeAssignments uuid="_pnHPwC38EeCuxJPrY_7nRQ">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qKWfsC38EeCuxJPrY_7nRQ" expressionString="mote::rules::MatchStorage" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfoStore" uuid="_KL-ekC38EeCuxJPrY_7nRQ" outgoingStoryLinks="_ZIPCgC38EeCuxJPrY_7nRQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="fmKey" uuid="_MO1uMC38EeCuxJPrY_7nRQ" incomingStoryLinks="_ZIPCgC38EeCuxJPrY_7nRQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N_CggC38EeCuxJPrY_7nRQ" expressionString="if direction = mote::TransformationDirection::FORWARD then&#xA;&#x9;'forwardFindMatches'&#xA;else&#xA;&#x9;if direction = mote::TransformationDirection::MAPPING then&#xA;&#x9;&#x9;'mappingFindMatches'&#xA;&#x9;else&#xA;&#x9;&#x9;'reverseFindMatches'&#xA;&#x9;endif&#xA;endif" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="fmActivity" uuid="_VkB78C38EeCuxJPrY_7nRQ" incomingStoryLinks="_YNt7AC38EeCuxJPrY_7nRQ _EQOqIC4CEeCIitj69w6fwg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleParameter" uuid="_fKCM4C38EeCuxJPrY_7nRQ" modifier="CREATE" outgoingStoryLinks="_yXVIMC38EeCuxJPrY_7nRQ _AxP0EC39EeCuxJPrY_7nRQ" incomingStoryLinks="_kf6FYC38EeCuxJPrY_7nRQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_UqWoUC4EEeCuKsO6aOUhRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZGte4C4EEeCuKsO6aOUhRA" expressionString="'this'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="parentCorrNodeParameter" uuid="_gQF2AC38EeCuxJPrY_7nRQ" modifier="CREATE" outgoingStoryLinks="_Jg_ycC39EeCuxJPrY_7nRQ _rPmwMC3_EeCuxJPrY_7nRQ" incomingStoryLinks="_lXNg0C38EeCuxJPrY_7nRQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_aKeB8C4EEeCuKsO6aOUhRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bAjVYC4EEeCuKsO6aOUhRA" expressionString="'parentCorrNode'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleCae" uuid="_wC3LMC38EeCuxJPrY_7nRQ" modifier="CREATE" outgoingStoryLinks="_5YxKoC38EeCuxJPrY_7nRQ" incomingStoryLinks="_yXVIMC38EeCuxJPrY_7nRQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleVarRef" uuid="_zVmuUC38EeCuxJPrY_7nRQ" modifier="CREATE" outgoingStoryLinks="__MdnkC38EeCuxJPrY_7nRQ" incomingStoryLinks="_5YxKoC38EeCuxJPrY_7nRQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_2IbbQC38EeCuxJPrY_7nRQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_36nKkC38EeCuxJPrY_7nRQ" expressionString="'this'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleEClass" uuid="_6kIxkC38EeCuxJPrY_7nRQ" incomingStoryLinks="__MdnkC38EeCuxJPrY_7nRQ _AxP0EC39EeCuxJPrY_7nRQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9CH28C38EeCuxJPrY_7nRQ" expressionString="mote::rules::TGGRule" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="parentCorrNodeCae" uuid="_Eyfb8C39EeCuxJPrY_7nRQ" modifier="CREATE" outgoingStoryLinks="_KJDQ8C39EeCuxJPrY_7nRQ" incomingStoryLinks="_Jg_ycC39EeCuxJPrY_7nRQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="parentCorrNodeVarRef" uuid="_FkT6cC39EeCuxJPrY_7nRQ" modifier="CREATE" outgoingStoryLinks="_p2H0wC3_EeCuxJPrY_7nRQ" incomingStoryLinks="_KJDQ8C39EeCuxJPrY_7nRQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_utAEIC3_EeCuxJPrY_7nRQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_vY3EkC3_EeCuxJPrY_7nRQ" expressionString="'parentCorrNode'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggNodeEClass" uuid="_L21BYC39EeCuxJPrY_7nRQ" incomingStoryLinks="_p2H0wC3_EeCuxJPrY_7nRQ _rPmwMC3_EeCuxJPrY_7nRQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nYQEIC3_EeCuxJPrY_7nRQ" expressionString="mote::TGGNode" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="fmAD" uuid="_BTkEsC4CEeCIitj69w6fwg" outgoingStoryLinks="_EQOqIC4CEeCIitj69w6fwg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="__qomwC37EeCuxJPrY_7nRQ" modifier="CREATE" source="_DOx4YC37EeCuxJPrY_7nRQ" target="_k4gBAC37EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_HPKvMC38EeCuxJPrY_7nRQ" modifier="CREATE" source="_k4gBAC37EeCuxJPrY_7nRQ" target="_lhAygC37EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_YNt7AC38EeCuxJPrY_7nRQ" modifier="CREATE" source="_lhAygC37EeCuxJPrY_7nRQ" target="_VkB78C38EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/activity"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_ZIPCgC38EeCuxJPrY_7nRQ" source="_KL-ekC38EeCuxJPrY_7nRQ" target="_MO1uMC38EeCuxJPrY_7nRQ" valueTarget="_BTkEsC4CEeCIitj69w6fwg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kf6FYC38EeCuxJPrY_7nRQ" modifier="CREATE" source="_lhAygC37EeCuxJPrY_7nRQ" target="_fKCM4C38EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lXNg0C38EeCuxJPrY_7nRQ" modifier="CREATE" source="_lhAygC37EeCuxJPrY_7nRQ" target="_gQF2AC38EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_yXVIMC38EeCuxJPrY_7nRQ" modifier="CREATE" source="_fKCM4C38EeCuxJPrY_7nRQ" target="_wC3LMC38EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5YxKoC38EeCuxJPrY_7nRQ" modifier="CREATE" source="_wC3LMC38EeCuxJPrY_7nRQ" target="_zVmuUC38EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="__MdnkC38EeCuxJPrY_7nRQ" modifier="CREATE" source="_zVmuUC38EeCuxJPrY_7nRQ" target="_6kIxkC38EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_AxP0EC39EeCuxJPrY_7nRQ" modifier="CREATE" source="_fKCM4C38EeCuxJPrY_7nRQ" target="_6kIxkC38EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Jg_ycC39EeCuxJPrY_7nRQ" modifier="CREATE" source="_gQF2AC38EeCuxJPrY_7nRQ" target="_Eyfb8C39EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_KJDQ8C39EeCuxJPrY_7nRQ" modifier="CREATE" source="_Eyfb8C39EeCuxJPrY_7nRQ" target="_FkT6cC39EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_p2H0wC3_EeCuxJPrY_7nRQ" modifier="CREATE" source="_FkT6cC39EeCuxJPrY_7nRQ" target="_L21BYC39EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_rPmwMC3_EeCuxJPrY_7nRQ" modifier="CREATE" source="_gQF2AC38EeCuxJPrY_7nRQ" target="_L21BYC39EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_EQOqIC4CEeCIitj69w6fwg" source="_BTkEsC4CEeCIitj69w6fwg" target="_VkB78C38EeCuxJPrY_7nRQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_zBLPEC3_EeCuxJPrY_7nRQ" incoming="_z-6CoC3_EeCuxJPrY_7nRQ"/>
    <edges uuid="_8DD4kC36EeCuxJPrY_7nRQ" source="_Tedd8C35EeCuxJPrY_7nRQ" target="_UNQdoC35EeCuxJPrY_7nRQ"/>
    <edges uuid="_ypYaIC3_EeCuxJPrY_7nRQ" source="_UNQdoC35EeCuxJPrY_7nRQ" target="__ymFYC36EeCuxJPrY_7nRQ"/>
    <edges uuid="_z-6CoC3_EeCuxJPrY_7nRQ" source="__ymFYC36EeCuxJPrY_7nRQ" target="_zBLPEC3_EeCuxJPrY_7nRQ"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
