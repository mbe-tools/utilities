<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_transformation_activity" uuid="_umXrMC1QEeCupczvU2Jg8w">
  <activities name="create_transformation_activity" uuid="_vO1ZYC1QEeCupczvU2Jg8w">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_xQ1GUC1QEeCupczvU2Jg8w" outgoing="_aC67YC33EeCSbYYn7Hqbqw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_x041oC1QEeCupczvU2Jg8w" incoming="_aC67YC33EeCSbYYn7Hqbqw" outgoing="_gFC34C3hEeCupczvU2Jg8w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfoStore" uuid="_wBs-UC3gEeCupczvU2Jg8w" outgoingStoryLinks="_0nlkUC3gEeCupczvU2Jg8w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_zO220C3gEeCupczvU2Jg8w" incomingStoryLinks="_0nlkUC3gEeCupczvU2Jg8w">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_2YpSQC3gEeCupczvU2Jg8w" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_YW-Y8C3hEeCupczvU2Jg8w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0nlkUC3gEeCupczvU2Jg8w" source="_wBs-UC3gEeCupczvU2Jg8w" target="_zO220C3gEeCupczvU2Jg8w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/tggRule"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create initial node" uuid="_Xlht4C3hEeCupczvU2Jg8w" incoming="_gFC34C3hEeCupczvU2Jg8w" outgoing="_YcVyYC3nEeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_aFl24C3hEeCupczvU2Jg8w" outgoingStoryLinks="_dxVv4C3hEeCupczvU2Jg8w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_b43yYC3hEeCupczvU2Jg8w" modifier="CREATE" incomingStoryLinks="_dxVv4C3hEeCupczvU2Jg8w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_dxVv4C3hEeCupczvU2Jg8w" modifier="CREATE" source="_aFl24C3hEeCupczvU2Jg8w" target="_b43yYC3hEeCupczvU2Jg8w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to create return value variable" uuid="_HPvjgC3nEeCSbYYn7Hqbqw" incoming="_YcVyYC3nEeCSbYYn7Hqbqw" outgoing="_xeBHIC3nEeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_KC4ZgC3nEeCSbYYn7Hqbqw" outgoingStoryLinks="_QdBycC3nEeCSbYYn7Hqbqw _T0QM8C3nEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_KkAwoC3nEeCSbYYn7Hqbqw" incomingStoryLinks="_U0L_8C3nEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createReturnValueEAN" uuid="_LJXgcC3nEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_gApooC34EeCSbYYn7Hqbqw" incomingStoryLinks="_QdBycC3nEeCSbYYn7Hqbqw _WStMYC3nEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_pennoC3nEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qtRMEC3nEeCSbYYn7Hqbqw" expressionString="'create return value variable'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_RA4GYC3nEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_U0L_8C3nEeCSbYYn7Hqbqw _WStMYC3nEeCSbYYn7Hqbqw" incomingStoryLinks="_T0QM8C3nEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="expression" uuid="_Pgm9wC34EeCSbYYn7Hqbqw" incomingStoryLinks="_gApooC34EeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_XXmIIC34EeCSbYYn7Hqbqw">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_XtVuoC34EeCSbYYn7Hqbqw">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
            <activity href="../common/create_returnValue_expression.story#_qoFvMPxlEd--caqh5zaNfg"/>
            <parameters name="resultLiteral" uuid="_aSvqoC34EeCSbYYn7Hqbqw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_b5u2kC34EeCSbYYn7Hqbqw" expressionString="mote::rules::TransformationResult::RULE_NOT_APPLIED" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//rules/TransformationResult"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_QdBycC3nEeCSbYYn7Hqbqw" modifier="CREATE" source="_KC4ZgC3nEeCSbYYn7Hqbqw" target="_LJXgcC3nEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_T0QM8C3nEeCSbYYn7Hqbqw" modifier="CREATE" source="_KC4ZgC3nEeCSbYYn7Hqbqw" target="_RA4GYC3nEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_U0L_8C3nEeCSbYYn7Hqbqw" modifier="CREATE" source="_RA4GYC3nEeCSbYYn7Hqbqw" target="_KkAwoC3nEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WStMYC3nEeCSbYYn7Hqbqw" modifier="CREATE" source="_RA4GYC3nEeCSbYYn7Hqbqw" target="_LJXgcC3nEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_gApooC34EeCSbYYn7Hqbqw" modifier="CREATE" source="_LJXgcC3nEeCSbYYn7Hqbqw" target="_Pgm9wC34EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to create matchStorage" uuid="_ZTCJ0C3nEeCSbYYn7Hqbqw" incoming="_xeBHIC3nEeCSbYYn7Hqbqw" outgoing="_1ainEC3nEeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_dKjN8C3nEeCSbYYn7Hqbqw" outgoingStoryLinks="_juD-QC3nEeCSbYYn7Hqbqw _kvmTwC3nEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createReturnValueEAN" uuid="_dtpT8C3nEeCSbYYn7Hqbqw" incomingStoryLinks="_lFpcQC3nEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_elxOwC3nEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_lFpcQC3nEeCSbYYn7Hqbqw _mbd_sC3nEeCSbYYn7Hqbqw" incomingStoryLinks="_juD-QC3nEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createMatchStorageSAN" uuid="_e9a5wC3nEeCSbYYn7Hqbqw" modifier="CREATE" incomingStoryLinks="_kvmTwC3nEeCSbYYn7Hqbqw _mbd_sC3nEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_tpqrwC3nEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_vQF3AC3nEeCSbYYn7Hqbqw" expressionString="'create matchStorage'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_juD-QC3nEeCSbYYn7Hqbqw" modifier="CREATE" source="_dKjN8C3nEeCSbYYn7Hqbqw" target="_elxOwC3nEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kvmTwC3nEeCSbYYn7Hqbqw" modifier="CREATE" source="_dKjN8C3nEeCSbYYn7Hqbqw" target="_e9a5wC3nEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lFpcQC3nEeCSbYYn7Hqbqw" modifier="CREATE" source="_elxOwC3nEeCSbYYn7Hqbqw" target="_dtpT8C3nEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_mbd_sC3nEeCSbYYn7Hqbqw" modifier="CREATE" source="_elxOwC3nEeCSbYYn7Hqbqw" target="_e9a5wC3nEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of createMatchStorageSAN" uuid="_x725sC3nEeCSbYYn7Hqbqw" incoming="_1ainEC3nEeCSbYYn7Hqbqw" outgoing="_MqdqUC3oEeCSbYYn7Hqbqw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_4ykl8C3_EeCuxJPrY_7nRQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_5GHNAC3_EeCuxJPrY_7nRQ">
          <activity href="create_storyPattern_createMatchStorageWithMatches.story#_5Jt-0C34EeCSbYYn7Hqbqw"/>
          <parameters name="ruleInfoStore" uuid="_7r908C3_EeCuxJPrY_7nRQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8L218C3_EeCuxJPrY_7nRQ" expressionString="ruleInfoStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
          </parameters>
          <parameters name="direction" uuid="_vyi6cC4AEeCuxJPrY_7nRQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xyoi4C4AEeCuxJPrY_7nRQ" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_ynMzYC4AEeCuxJPrY_7nRQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_03FLUC4AEeCuxJPrY_7nRQ" expressionString="createMatchStorageSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_0KulAC3nEeCSbYYn7Hqbqw" incoming="_YJiX4C33EeCSbYYn7Hqbqw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to iterate through matches" uuid="_183RAC3nEeCSbYYn7Hqbqw" incoming="_MqdqUC3oEeCSbYYn7Hqbqw" outgoing="_Sr95wC3oEeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_4SGDAC3nEeCSbYYn7Hqbqw" outgoingStoryLinks="_BJjy8C3oEeCSbYYn7Hqbqw _CBA_YC3oEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createMatchStorageSAN" uuid="_5x_IcC3nEeCSbYYn7Hqbqw" incomingStoryLinks="_CYlK0C3oEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_7SwXoC3nEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_CYlK0C3oEeCSbYYn7Hqbqw _DejUYC3oEeCSbYYn7Hqbqw" incomingStoryLinks="_BJjy8C3oEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateMatchesSAN" uuid="_7ounoC3nEeCSbYYn7Hqbqw" modifier="CREATE" incomingStoryLinks="_CBA_YC3oEeCSbYYn7Hqbqw _DejUYC3oEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_FLVmUC3oEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GaXlQC3oEeCSbYYn7Hqbqw" expressionString="'iterate through matches'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_I41y0C3oEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/forEach"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_J_QBQC3oEeCSbYYn7Hqbqw" expressionString="true" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BJjy8C3oEeCSbYYn7Hqbqw" modifier="CREATE" source="_4SGDAC3nEeCSbYYn7Hqbqw" target="_7SwXoC3nEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_CBA_YC3oEeCSbYYn7Hqbqw" modifier="CREATE" source="_4SGDAC3nEeCSbYYn7Hqbqw" target="_7ounoC3nEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_CYlK0C3oEeCSbYYn7Hqbqw" modifier="CREATE" source="_7SwXoC3nEeCSbYYn7Hqbqw" target="_5x_IcC3nEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_DejUYC3oEeCSbYYn7Hqbqw" modifier="CREATE" source="_7SwXoC3nEeCSbYYn7Hqbqw" target="_7ounoC3nEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of iterateMatchesSAN" uuid="_NEUUwC3oEeCSbYYn7Hqbqw" incoming="_Sr95wC3oEeCSbYYn7Hqbqw" outgoing="_E9M8UC3rEeCSbYYn7Hqbqw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_dttF4C4JEeCuKsO6aOUhRA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_d89p4C4JEeCuKsO6aOUhRA">
          <activity href="create_storyPattern_iterateMatches.story#_4teRQC4EEeCuKsO6aOUhRA"/>
          <parameters name="storyActionNode" uuid="_fOAbAC4JEeCuKsO6aOUhRA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_h_NXUC4JEeCuKsO6aOUhRA" expressionString="iterateMatchesSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="tggRule" uuid="_4MqxYC6gEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_52Ls4C6gEeCRIK6l8KTurA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_6m70YC6gEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8bm3AC6gEeCRIK6l8KTurA" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create final node to return returnVariable" uuid="_VKkqMC3oEeCSbYYn7Hqbqw" incoming="_E9M8UC3rEeCSbYYn7Hqbqw" outgoing="_e3y2EC3rEeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_W_7pUC3oEeCSbYYn7Hqbqw" outgoingStoryLinks="_jA79EC3oEeCSbYYn7Hqbqw _kIzkEC3oEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateMatchesSAN" uuid="_Xli30C3oEeCSbYYn7Hqbqw" incomingStoryLinks="_kZg5kC3oEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_Yd5cIC3oEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_kZg5kC3oEeCSbYYn7Hqbqw _Fce5UC3rEeCSbYYn7Hqbqw" incomingStoryLinks="_jA79EC3oEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_eh8pQC3oEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fw8zAC3oEeCSbYYn7Hqbqw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::END" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="finalNode" uuid="_Y4JIIC3oEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_oTB3EC4PEeC0Rpt37j6jRA" incomingStoryLinks="_kIzkEC3oEeCSbYYn7Hqbqw _Fce5UC3rEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_dnHuAC4PEeC0Rpt37j6jRA" modifier="CREATE" outgoingStoryLinks="_pAMfEC4PEeC0Rpt37j6jRA" incomingStoryLinks="_oTB3EC4PEeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="returnValueVarRef" uuid="_eYlnMC4PEeC0Rpt37j6jRA" modifier="CREATE" incomingStoryLinks="_pAMfEC4PEeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_ieyAgC4PEeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jtYhoC4PEeC0Rpt37j6jRA" expressionString="'__returnValue'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_k__ZAC4PEeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mL6ZkC4PEeC0Rpt37j6jRA" expressionString="mote::rules::TransformationResult" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jA79EC3oEeCSbYYn7Hqbqw" modifier="CREATE" source="_W_7pUC3oEeCSbYYn7Hqbqw" target="_Yd5cIC3oEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kIzkEC3oEeCSbYYn7Hqbqw" modifier="CREATE" source="_W_7pUC3oEeCSbYYn7Hqbqw" target="_Y4JIIC3oEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kZg5kC3oEeCSbYYn7Hqbqw" modifier="CREATE" source="_Yd5cIC3oEeCSbYYn7Hqbqw" target="_Xli30C3oEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Fce5UC3rEeCSbYYn7Hqbqw" modifier="CREATE" source="_Yd5cIC3oEeCSbYYn7Hqbqw" target="_Y4JIIC3oEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_oTB3EC4PEeC0Rpt37j6jRA" modifier="CREATE" source="_Y4JIIC3oEeCSbYYn7Hqbqw" target="_dnHuAC4PEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_pAMfEC4PEeC0Rpt37j6jRA" modifier="CREATE" source="_dnHuAC4PEeC0Rpt37j6jRA" target="_eYlnMC4PEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create decision node to check for conflicts" uuid="_JWfQwC3rEeCSbYYn7Hqbqw" incoming="_e3y2EC3rEeCSbYYn7Hqbqw" outgoing="_n5eR8C3rEeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_Lio_QC3rEeCSbYYn7Hqbqw" outgoingStoryLinks="_UvZCsC3rEeCSbYYn7Hqbqw _dVmqkC3rEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateMatchesSAN" uuid="_Mo5csC3rEeCSbYYn7Hqbqw" incomingStoryLinks="_VTspoC3rEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_NgSXsC3rEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_VTspoC3rEeCSbYYn7Hqbqw _WhSeIC3rEeCSbYYn7Hqbqw" incomingStoryLinks="_UvZCsC3rEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_YYOKkC3rEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZdLngC3rEeCSbYYn7Hqbqw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FOR_EACH" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkConflictsDN" uuid="_N1ucMC3rEeCSbYYn7Hqbqw" modifier="CREATE" incomingStoryLinks="_WhSeIC3rEeCSbYYn7Hqbqw _dVmqkC3rEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/DecisionNode"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_UvZCsC3rEeCSbYYn7Hqbqw" modifier="CREATE" source="_Lio_QC3rEeCSbYYn7Hqbqw" target="_NgSXsC3rEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_VTspoC3rEeCSbYYn7Hqbqw" modifier="CREATE" source="_NgSXsC3rEeCSbYYn7Hqbqw" target="_Mo5csC3rEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WhSeIC3rEeCSbYYn7Hqbqw" modifier="CREATE" source="_NgSXsC3rEeCSbYYn7Hqbqw" target="_N1ucMC3rEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_dVmqkC3rEeCSbYYn7Hqbqw" modifier="CREATE" source="_Lio_QC3rEeCSbYYn7Hqbqw" target="_N1ucMC3rEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create final node to return in case of conflicts" uuid="_in9VAC3rEeCSbYYn7Hqbqw" incoming="_n5eR8C3rEeCSbYYn7Hqbqw" outgoing="_I9IgIC3sEeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkConflictsDN" uuid="_kiGGcC3rEeCSbYYn7Hqbqw" incomingStoryLinks="_xEFnYC3rEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/DecisionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_lV-acC3rEeCSbYYn7Hqbqw" outgoingStoryLinks="_v66_4C3rEeCSbYYn7Hqbqw _wrhWYC3rEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_l9a08C3rEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_xEFnYC3rEeCSbYYn7Hqbqw _zKvx4C3rEeCSbYYn7Hqbqw _41gK4C4PEeC0Rpt37j6jRA" incomingStoryLinks="_v66_4C3rEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_r-KPYC3rEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tPab4C3rEeCSbYYn7Hqbqw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::BOOLEAN" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="finalNode" uuid="_mPMhcC3rEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_d8SI4C4TEeC0Rpt37j6jRA" incomingStoryLinks="_wrhWYC3rEeCSbYYn7Hqbqw _zKvx4C3rEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="guardCAE" uuid="_3GnAIC4PEeC0Rpt37j6jRA" modifier="CREATE" outgoingStoryLinks="_7hAu4C4PEeC0Rpt37j6jRA" incomingStoryLinks="_41gK4C4PEeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="callSDIAction" uuid="_53aT0C4PEeC0Rpt37j6jRA" modifier="CREATE" outgoingStoryLinks="_ONCJIC4QEeC0Rpt37j6jRA _tZBrEC4QEeC0Rpt37j6jRA _HI6JoC4REeC0Rpt37j6jRA _cIdFsC4REeC0Rpt37j6jRA" incomingStoryLinks="_7hAu4C4PEeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction"/>
        <attributeAssignments uuid="_9AvcQC4PEeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-xHNsC4PEeC0Rpt37j6jRA" expressionString="ecore::EBoolean" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfoStore" uuid="_GwCzIC4QEeC0Rpt37j6jRA" outgoingStoryLinks="_8IpPQC4SEeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ccKey" uuid="_JrQnEC4QEeC0Rpt37j6jRA" incomingStoryLinks="_8IpPQC4SEeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_TqcNAC4QEeC0Rpt37j6jRA" expressionString="if direction = mote::TransformationDirection::FORWARD then&#xA;&#x9;'forwardConflictCheck'&#xA;else&#xA;&#x9;if direction = mote::TransformationDirection::MAPPING then&#xA;&#x9;&#x9;'mappingConflictCheck'&#xA;&#x9;else&#xA;&#x9;&#x9;'reverseConflictCheck'&#xA;&#x9;endif&#xA;endif" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ccAD" uuid="_KSK2EC4QEeC0Rpt37j6jRA" outgoingStoryLinks="_N1J0oC4QEeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="conflictCheckActivity" uuid="_LHf7kC4QEeC0Rpt37j6jRA" incomingStoryLinks="_N1J0oC4QEeC0Rpt37j6jRA _ONCJIC4QEeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisParameter" uuid="_j1rskC4QEeC0Rpt37j6jRA" modifier="CREATE" outgoingStoryLinks="_uYC4EC4QEeC0Rpt37j6jRA _3QL9cC4QEeC0Rpt37j6jRA" incomingStoryLinks="_tZBrEC4QEeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_7bgHUC4QEeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9FBC0C4QEeC0Rpt37j6jRA" expressionString="'this'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodeParameter" uuid="_kaOkEC4QEeC0Rpt37j6jRA" modifier="CREATE" outgoingStoryLinks="_RYeU8C4REeC0Rpt37j6jRA _Sg8_8C4REeC0Rpt37j6jRA" incomingStoryLinks="_HI6JoC4REeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_E1Pd4C4REeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_F6ZIEC4REeC0Rpt37j6jRA" expressionString="'parentCorrNode'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchParameter" uuid="_l05LQC4QEeC0Rpt37j6jRA" modifier="CREATE" outgoingStoryLinks="_v8htIC4REeC0Rpt37j6jRA _xWjbIC4REeC0Rpt37j6jRA" incomingStoryLinks="_cIdFsC4REeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_rDuLIC4REeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sEtuEC4REeC0Rpt37j6jRA" expressionString="'match'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleEClass" uuid="_ppwkAC4QEeC0Rpt37j6jRA" incomingStoryLinks="_uYC4EC4QEeC0Rpt37j6jRA _6Frj4C4QEeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rnVD8C4QEeC0Rpt37j6jRA" expressionString="mote::rules::TGGRule" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisCae" uuid="_vfoLMC4QEeC0Rpt37j6jRA" modifier="CREATE" outgoingStoryLinks="_4u264C4QEeC0Rpt37j6jRA" incomingStoryLinks="_3QL9cC4QEeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisVarRef" uuid="_yxs_8C4QEeC0Rpt37j6jRA" modifier="CREATE" outgoingStoryLinks="_6Frj4C4QEeC0Rpt37j6jRA" incomingStoryLinks="_4u264C4QEeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_06wXIC4QEeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2MC_4C4QEeC0Rpt37j6jRA" expressionString="'this'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodeCae" uuid="_IQ66kC4REeC0Rpt37j6jRA" modifier="CREATE" outgoingStoryLinks="_UC564C4REeC0Rpt37j6jRA" incomingStoryLinks="_Sg8_8C4REeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodeVarRef" uuid="_KOCukC4REeC0Rpt37j6jRA" modifier="CREATE" outgoingStoryLinks="_Vj3-YC4REeC0Rpt37j6jRA" incomingStoryLinks="_UC564C4REeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_hxdVkC4REeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_i6Z6oC4REeC0Rpt37j6jRA" expressionString="'parentCorrNode'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggNodeEClass" uuid="_NY5hAC4REeC0Rpt37j6jRA" incomingStoryLinks="_RYeU8C4REeC0Rpt37j6jRA _Vj3-YC4REeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_PnF24C4REeC0Rpt37j6jRA" expressionString="mote::TGGNode" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchCae" uuid="_dR1voC4REeC0Rpt37j6jRA" modifier="CREATE" outgoingStoryLinks="_y12DoC4REeC0Rpt37j6jRA" incomingStoryLinks="_xWjbIC4REeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchVarRef" uuid="_f1yTEC4REeC0Rpt37j6jRA" modifier="CREATE" outgoingStoryLinks="_0ERloC4REeC0Rpt37j6jRA" incomingStoryLinks="_y12DoC4REeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_tQBqoC4REeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ue1AEC4REeC0Rpt37j6jRA" expressionString="'match'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchEClass" uuid="_mIPvoC4REeC0Rpt37j6jRA" incomingStoryLinks="_v8htIC4REeC0Rpt37j6jRA _0ERloC4REeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_oHayEC4REeC0Rpt37j6jRA" expressionString="mote::rules::Match" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="stringExpression" uuid="_VDWZUC4TEeC0Rpt37j6jRA" modifier="CREATE" incomingStoryLinks="_d8SI4C4TEeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_YsH20C4TEeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZrFZcC4TEeC0Rpt37j6jRA" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_akg70C4TEeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bXLH0C4TEeC0Rpt37j6jRA" expressionString="'mote::rules::TransformationResult::CONFLICT_DETECTED'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_v66_4C3rEeCSbYYn7Hqbqw" modifier="CREATE" source="_lV-acC3rEeCSbYYn7Hqbqw" target="_l9a08C3rEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_wrhWYC3rEeCSbYYn7Hqbqw" modifier="CREATE" source="_lV-acC3rEeCSbYYn7Hqbqw" target="_mPMhcC3rEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xEFnYC3rEeCSbYYn7Hqbqw" modifier="CREATE" source="_l9a08C3rEeCSbYYn7Hqbqw" target="_kiGGcC3rEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zKvx4C3rEeCSbYYn7Hqbqw" modifier="CREATE" source="_l9a08C3rEeCSbYYn7Hqbqw" target="_mPMhcC3rEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_41gK4C4PEeC0Rpt37j6jRA" modifier="CREATE" source="_l9a08C3rEeCSbYYn7Hqbqw" target="_3GnAIC4PEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7hAu4C4PEeC0Rpt37j6jRA" modifier="CREATE" source="_3GnAIC4PEeC0Rpt37j6jRA" target="_53aT0C4PEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_N1J0oC4QEeC0Rpt37j6jRA" source="_KSK2EC4QEeC0Rpt37j6jRA" target="_LHf7kC4QEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ONCJIC4QEeC0Rpt37j6jRA" modifier="CREATE" source="_53aT0C4PEeC0Rpt37j6jRA" target="_LHf7kC4QEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/activity"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tZBrEC4QEeC0Rpt37j6jRA" modifier="CREATE" source="_53aT0C4PEeC0Rpt37j6jRA" target="_j1rskC4QEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_uYC4EC4QEeC0Rpt37j6jRA" modifier="CREATE" source="_j1rskC4QEeC0Rpt37j6jRA" target="_ppwkAC4QEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3QL9cC4QEeC0Rpt37j6jRA" modifier="CREATE" source="_j1rskC4QEeC0Rpt37j6jRA" target="_vfoLMC4QEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4u264C4QEeC0Rpt37j6jRA" modifier="CREATE" source="_vfoLMC4QEeC0Rpt37j6jRA" target="_yxs_8C4QEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_6Frj4C4QEeC0Rpt37j6jRA" modifier="CREATE" source="_yxs_8C4QEeC0Rpt37j6jRA" target="_ppwkAC4QEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_HI6JoC4REeC0Rpt37j6jRA" modifier="CREATE" source="_53aT0C4PEeC0Rpt37j6jRA" target="_kaOkEC4QEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RYeU8C4REeC0Rpt37j6jRA" modifier="CREATE" source="_kaOkEC4QEeC0Rpt37j6jRA" target="_NY5hAC4REeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Sg8_8C4REeC0Rpt37j6jRA" modifier="CREATE" source="_kaOkEC4QEeC0Rpt37j6jRA" target="_IQ66kC4REeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_UC564C4REeC0Rpt37j6jRA" modifier="CREATE" source="_IQ66kC4REeC0Rpt37j6jRA" target="_KOCukC4REeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Vj3-YC4REeC0Rpt37j6jRA" modifier="CREATE" source="_KOCukC4REeC0Rpt37j6jRA" target="_NY5hAC4REeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_cIdFsC4REeC0Rpt37j6jRA" modifier="CREATE" source="_53aT0C4PEeC0Rpt37j6jRA" target="_l05LQC4QEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_v8htIC4REeC0Rpt37j6jRA" modifier="CREATE" source="_l05LQC4QEeC0Rpt37j6jRA" target="_mIPvoC4REeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xWjbIC4REeC0Rpt37j6jRA" modifier="CREATE" source="_l05LQC4QEeC0Rpt37j6jRA" target="_dR1voC4REeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_y12DoC4REeC0Rpt37j6jRA" modifier="CREATE" source="_dR1voC4REeC0Rpt37j6jRA" target="_f1yTEC4REeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0ERloC4REeC0Rpt37j6jRA" modifier="CREATE" source="_f1yTEC4REeC0Rpt37j6jRA" target="_mIPvoC4REeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_8IpPQC4SEeC0Rpt37j6jRA" source="_GwCzIC4QEeC0Rpt37j6jRA" target="_JrQnEC4QEeC0Rpt37j6jRA" valueTarget="_KSK2EC4QEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_d8SI4C4TEeC0Rpt37j6jRA" modifier="CREATE" source="_mPMhcC3rEeCSbYYn7Hqbqw" target="_VDWZUC4TEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to evaluate rule variables" uuid="_2fb1wC3rEeCSbYYn7Hqbqw" incoming="_I9IgIC3sEeCSbYYn7Hqbqw" outgoing="_iFIfUC3sEeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_4jRVwC3rEeCSbYYn7Hqbqw" outgoingStoryLinks="_DkSzMC3sEeCSbYYn7Hqbqw _EdqrMC3sEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkConflictsDN" uuid="_5Z5bwC3rEeCSbYYn7Hqbqw" incomingStoryLinks="_FI6noC3sEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/DecisionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_6GmwwC3rEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_FI6noC3sEeCSbYYn7Hqbqw _GnSqIC3sEeCSbYYn7Hqbqw" incomingStoryLinks="_DkSzMC3sEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="__IR64C3rEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AjfUoC3sEeCSbYYn7Hqbqw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::ELSE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evalRuleVarsEAN" uuid="_6efFQC3rEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_tOXL0C4TEeC0Rpt37j6jRA" incomingStoryLinks="_EdqrMC3sEeCSbYYn7Hqbqw _GnSqIC3sEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_bkWS4C3sEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ci-eUC3sEeCSbYYn7Hqbqw" expressionString="'evaluate rule variables'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_hic-UC4TEeC0Rpt37j6jRA" incomingStoryLinks="_tOXL0C4TEeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_lKc_wC4TEeC0Rpt37j6jRA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_ldEZwC4TEeC0Rpt37j6jRA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
            <activity href="../common/create_evaluateRuleVariables_expression.story#_uabxYfZwEd-i2JdGlqLvVQ"/>
            <parameters name="tggRule" uuid="_nN-9wC4TEeC0Rpt37j6jRA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_pITVYC4TEeC0Rpt37j6jRA" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_p9_nQC4TEeC0Rpt37j6jRA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r98FwC4TEeC0Rpt37j6jRA" expressionString="direction" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_DkSzMC3sEeCSbYYn7Hqbqw" modifier="CREATE" source="_4jRVwC3rEeCSbYYn7Hqbqw" target="_6GmwwC3rEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_EdqrMC3sEeCSbYYn7Hqbqw" modifier="CREATE" source="_4jRVwC3rEeCSbYYn7Hqbqw" target="_6efFQC3rEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_FI6noC3sEeCSbYYn7Hqbqw" modifier="CREATE" source="_6GmwwC3rEeCSbYYn7Hqbqw" target="_5Z5bwC3rEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GnSqIC3sEeCSbYYn7Hqbqw" modifier="CREATE" source="_6GmwwC3rEeCSbYYn7Hqbqw" target="_6efFQC3rEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tOXL0C4TEeC0Rpt37j6jRA" modifier="CREATE" source="_6efFQC3rEeCSbYYn7Hqbqw" target="_hic-UC4TEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to transform elements" uuid="_M-T2kC3sEeCSbYYn7Hqbqw" incoming="_iFIfUC3sEeCSbYYn7Hqbqw" outgoing="_lffGUC3sEeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_PJ8nsC3sEeCSbYYn7Hqbqw" outgoingStoryLinks="_XYt_8C3sEeCSbYYn7Hqbqw _YORu8C3sEeCSbYYn7Hqbqw _vpUNQC7BEeCDMMgUmpccLw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evalRuleVarsEAN" uuid="_P8CzAC3sEeCSbYYn7Hqbqw" incomingStoryLinks="_Y4iz8C3sEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_QuBCgC3sEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_Y4iz8C3sEeCSbYYn7Hqbqw _Z_ktcC3sEeCSbYYn7Hqbqw" incomingStoryLinks="_XYt_8C3sEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformElementsSAN" uuid="_RD6aAC3sEeCSbYYn7Hqbqw" modifier="CREATE" incomingStoryLinks="_YORu8C3sEeCSbYYn7Hqbqw _Z_ktcC3sEeCSbYYn7Hqbqw _t9wDUC7BEeCDMMgUmpccLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_e4XBUC3sEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gAFeYC3sEeCSbYYn7Hqbqw" expressionString="'transform elements'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="outgoingEdge" uuid="_quG3gC7BEeCDMMgUmpccLw" modifier="CREATE" outgoingStoryLinks="_t9wDUC7BEeCDMMgUmpccLw" incomingStoryLinks="_vpUNQC7BEeCDMMgUmpccLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XYt_8C3sEeCSbYYn7Hqbqw" modifier="CREATE" source="_PJ8nsC3sEeCSbYYn7Hqbqw" target="_QuBCgC3sEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_YORu8C3sEeCSbYYn7Hqbqw" modifier="CREATE" source="_PJ8nsC3sEeCSbYYn7Hqbqw" target="_RD6aAC3sEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Y4iz8C3sEeCSbYYn7Hqbqw" modifier="CREATE" source="_QuBCgC3sEeCSbYYn7Hqbqw" target="_P8CzAC3sEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Z_ktcC3sEeCSbYYn7Hqbqw" modifier="CREATE" source="_QuBCgC3sEeCSbYYn7Hqbqw" target="_RD6aAC3sEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_t9wDUC7BEeCDMMgUmpccLw" modifier="CREATE" source="_quG3gC7BEeCDMMgUmpccLw" target="_RD6aAC3sEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_vpUNQC7BEeCDMMgUmpccLw" modifier="CREATE" source="_PJ8nsC3sEeCSbYYn7Hqbqw" target="_quG3gC7BEeCDMMgUmpccLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of transformElementsSAN" uuid="_jC2r0C3sEeCSbYYn7Hqbqw" incoming="_lffGUC3sEeCSbYYn7Hqbqw" outgoing="_qfElwC3sEeCSbYYn7Hqbqw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_vENy0C4WEeC0Rpt37j6jRA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_vUPL0C4WEeC0Rpt37j6jRA">
          <activity href="create_storyPattern_transformElements.story#_LVs9YC4VEeC0Rpt37j6jRA"/>
          <parameters name="tggRule" uuid="_wWAK0C4WEeC0Rpt37j6jRA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_yI-kUC4WEeC0Rpt37j6jRA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_y-OKQC4WEeC0Rpt37j6jRA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1AEtQC4WEeC0Rpt37j6jRA" expressionString="transformElementsSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="direction" uuid="_2HzKUC4WEeC0Rpt37j6jRA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3_8-wC4WEeC0Rpt37j6jRA" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to evaluate rule variables (opposite direction)" uuid="_mgtSwC3sEeCSbYYn7Hqbqw" incoming="_HzddEC7CEeCDMMgUmpccLw" outgoing="_vLVxgC31EeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_qv7FMC3sEeCSbYYn7Hqbqw" outgoingStoryLinks="_3cv9IC3sEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="outgoingEdge" uuid="_sZqqMC3sEeCSbYYn7Hqbqw" outgoingStoryLinks="_5P3gkC3sEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evalRuleVarsEAN" uuid="_sv8cMC3sEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_7qHqwC4TEeC0Rpt37j6jRA" incomingStoryLinks="_3cv9IC3sEeCSbYYn7Hqbqw _5P3gkC3sEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_yQpEoC3sEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_zboREC3sEeCSbYYn7Hqbqw" expressionString="'evaluate rule variables (opposite direction)'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_6vl8MC4TEeC0Rpt37j6jRA" incomingStoryLinks="_7qHqwC4TEeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_6vmjQC4TEeC0Rpt37j6jRA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_6vmjQS4TEeC0Rpt37j6jRA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
            <activity href="../common/create_evaluateRuleVariables_expression.story#_uabxYfZwEd-i2JdGlqLvVQ"/>
            <parameters name="tggRule" uuid="_6vmjRC4TEeC0Rpt37j6jRA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6vmjRS4TEeC0Rpt37j6jRA" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_6vmjQi4TEeC0Rpt37j6jRA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6vmjQy4TEeC0Rpt37j6jRA" expressionString="if direction = mote::TransformationDirection::FORWARD or&#xA;&#x9;direction = mote::TransformationDirection::MAPPING then&#xA;&#x9;mote::TransformationDirection::REVERSE&#xA;else&#xA;&#x9;mote::TransformationDirection::FORWARD&#xA;endif" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3cv9IC3sEeCSbYYn7Hqbqw" modifier="CREATE" source="_qv7FMC3sEeCSbYYn7Hqbqw" target="_sv8cMC3sEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5P3gkC3sEeCSbYYn7Hqbqw" modifier="CREATE" source="_sZqqMC3sEeCSbYYn7Hqbqw" target="_sv8cMC3sEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7qHqwC4TEeC0Rpt37j6jRA" modifier="CREATE" source="_sv8cMC3sEeCSbYYn7Hqbqw" target="_6vl8MC4TEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to check attribute values" uuid="_smNiUC31EeCSbYYn7Hqbqw" incoming="_vLVxgC31EeCSbYYn7Hqbqw" outgoing="_B3Xy0C32EeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_ve8qAC31EeCSbYYn7Hqbqw" outgoingStoryLinks="_5reIYC31EeCSbYYn7Hqbqw _8KoAgC33EeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evalRuleVarsEAN" uuid="_wAIrgC31EeCSbYYn7Hqbqw" incomingStoryLinks="_68kj4C31EeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_w-ApAC31EeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_68kj4C31EeCSbYYn7Hqbqw _8KxcYC31EeCSbYYn7Hqbqw" incomingStoryLinks="_5reIYC31EeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkAttributesSAN" uuid="_xWEjsC31EeCSbYYn7Hqbqw" modifier="CREATE" incomingStoryLinks="_8KxcYC31EeCSbYYn7Hqbqw _8KoAgC33EeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_zgOuIC31EeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0y5P4C31EeCSbYYn7Hqbqw" expressionString="'check attribute values'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5reIYC31EeCSbYYn7Hqbqw" modifier="CREATE" source="_ve8qAC31EeCSbYYn7Hqbqw" target="_w-ApAC31EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_68kj4C31EeCSbYYn7Hqbqw" modifier="CREATE" source="_w-ApAC31EeCSbYYn7Hqbqw" target="_wAIrgC31EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8KxcYC31EeCSbYYn7Hqbqw" modifier="CREATE" source="_w-ApAC31EeCSbYYn7Hqbqw" target="_xWEjsC31EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8KoAgC33EeCSbYYn7Hqbqw" modifier="CREATE" source="_ve8qAC31EeCSbYYn7Hqbqw" target="_xWEjsC31EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of checkAttributesSAN" uuid="_AGJs0C32EeCSbYYn7Hqbqw" incoming="_B3Xy0C32EeCSbYYn7Hqbqw" outgoing="_HlkrwC32EeCSbYYn7Hqbqw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_oGELIC6pEeC6LMtfP_jwGg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_oUoyoC6pEeC6LMtfP_jwGg">
          <activity href="create_storyPattern_check_constraints.story#_2MC_APbdEd-JnYtEWuN3-g"/>
          <parameters name="tggRule" uuid="_pOFjIC6pEeC6LMtfP_jwGg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_q_--kC6pEeC6LMtfP_jwGg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_wwjPAC6pEeC6LMtfP_jwGg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_zYs9AC6pEeC6LMtfP_jwGg" expressionString="checkAttributesSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="createSourceConstraints" uuid="_1pCn8C6pEeC6LMtfP_jwGg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3OlpcC6pEeC6LMtfP_jwGg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetConstraints" uuid="_3_zD8C6pEeC6LMtfP_jwGg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6jSUYC6pEeC6LMtfP_jwGg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentNodesConstraints" uuid="_7TqBYC6pEeC6LMtfP_jwGg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-aZv4C6pEeC6LMtfP_jwGg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedNodesConstraints" uuid="_A_kbUC6qEeC6LMtfP_jwGg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_C1NuUC6qEeC6LMtfP_jwGg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create final to return in case of wrong attribute values" uuid="_DOuAQC32EeCSbYYn7Hqbqw" incoming="_HlkrwC32EeCSbYYn7Hqbqw" outgoing="_gqYNAC32EeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_H5BMMC32EeCSbYYn7Hqbqw" outgoingStoryLinks="_XaqVIC32EeCSbYYn7Hqbqw _YdfEEC32EeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkAttributesSAN" uuid="_IfFtsC32EeCSbYYn7Hqbqw" incomingStoryLinks="_ZKukkC32EeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_JYnWsC32EeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_ZKukkC32EeCSbYYn7Hqbqw _aXZzEC32EeCSbYYn7Hqbqw" incomingStoryLinks="_XaqVIC32EeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_TPWLQC32EeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_UaOqAC32EeCSbYYn7Hqbqw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="finalNode" uuid="_JtLRcC32EeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_Vz5QAC4UEeC0Rpt37j6jRA" incomingStoryLinks="_YdfEEC32EeCSbYYn7Hqbqw _aXZzEC32EeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="stringExpression" uuid="_OGSEEC4UEeC0Rpt37j6jRA" modifier="CREATE" incomingStoryLinks="_Vz5QAC4UEeC0Rpt37j6jRA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_QUHNkC4UEeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_RQYBgC4UEeC0Rpt37j6jRA" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_SEzIEC4UEeC0Rpt37j6jRA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_S5vzEC4UEeC0Rpt37j6jRA" expressionString="'mote::rules::TransformationResult::CONFLICT_DETECTED'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XaqVIC32EeCSbYYn7Hqbqw" modifier="CREATE" source="_H5BMMC32EeCSbYYn7Hqbqw" target="_JYnWsC32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_YdfEEC32EeCSbYYn7Hqbqw" modifier="CREATE" source="_H5BMMC32EeCSbYYn7Hqbqw" target="_JtLRcC32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZKukkC32EeCSbYYn7Hqbqw" modifier="CREATE" source="_JYnWsC32EeCSbYYn7Hqbqw" target="_IfFtsC32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_aXZzEC32EeCSbYYn7Hqbqw" modifier="CREATE" source="_JYnWsC32EeCSbYYn7Hqbqw" target="_JtLRcC32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Vz5QAC4UEeC0Rpt37j6jRA" modifier="CREATE" source="_JtLRcC32EeCSbYYn7Hqbqw" target="_OGSEEC4UEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to add modification tag to queue" uuid="_eV0wcC32EeCSbYYn7Hqbqw" incoming="_gqYNAC32EeCSbYYn7Hqbqw" outgoing="_9bOiMC32EeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_hEO3cC32EeCSbYYn7Hqbqw" outgoingStoryLinks="_tOyv0C32EeCSbYYn7Hqbqw _uYCP0C32EeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkAttributesSAN" uuid="_hoxu8C32EeCSbYYn7Hqbqw" incomingStoryLinks="_vKtC4C32EeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_ieB78C32EeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_vKtC4C32EeCSbYYn7Hqbqw _wStz0C32EeCSbYYn7Hqbqw" incomingStoryLinks="_tOyv0C32EeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_oL4PkC32EeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_q-hWUC32EeCSbYYn7Hqbqw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="addModTagEAN" uuid="_i1YE8C32EeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_h73ucC4UEeC0Rpt37j6jRA" incomingStoryLinks="_uYCP0C32EeCSbYYn7Hqbqw _wStz0C32EeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_2DDawC32EeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3Pgm0C32EeCSbYYn7Hqbqw" expressionString="'add modification tag to queue'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="expression" uuid="_XJTjwC4UEeC0Rpt37j6jRA" incomingStoryLinks="_h73ucC4UEeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_c0yf4C4UEeC0Rpt37j6jRA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_dFue4C4UEeC0Rpt37j6jRA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
            <activity href="../common/create_addModifcationTagToQueue_expression.story#_lXQIIPcJEd-_yp3IZC6S-Q"/>
            <parameters name="tggRule" uuid="_ekj0YC4UEeC0Rpt37j6jRA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gpjK8C4UEeC0Rpt37j6jRA" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tOyv0C32EeCSbYYn7Hqbqw" modifier="CREATE" source="_hEO3cC32EeCSbYYn7Hqbqw" target="_ieB78C32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_uYCP0C32EeCSbYYn7Hqbqw" modifier="CREATE" source="_hEO3cC32EeCSbYYn7Hqbqw" target="_i1YE8C32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_vKtC4C32EeCSbYYn7Hqbqw" modifier="CREATE" source="_ieB78C32EeCSbYYn7Hqbqw" target="_hoxu8C32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_wStz0C32EeCSbYYn7Hqbqw" modifier="CREATE" source="_ieB78C32EeCSbYYn7Hqbqw" target="_i1YE8C32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_h73ucC4UEeC0Rpt37j6jRA" modifier="CREATE" source="_i1YE8C32EeCSbYYn7Hqbqw" target="_XJTjwC4UEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to set return value variable" uuid="_zsOGQC32EeCSbYYn7Hqbqw" incoming="_9bOiMC32EeCSbYYn7Hqbqw" outgoing="_YJiX4C33EeCSbYYn7Hqbqw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_5iqVMC32EeCSbYYn7Hqbqw" outgoingStoryLinks="_FjUDoC33EeCSbYYn7Hqbqw _GZ2qEC33EeCSbYYn7Hqbqw _V64H8C33EeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="addModTagEAN" uuid="_6ZvuMC32EeCSbYYn7Hqbqw" incomingStoryLinks="_HGVVkC33EeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_7SDPMC32EeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_HGVVkC33EeCSbYYn7Hqbqw _IaynEC33EeCSbYYn7Hqbqw" incomingStoryLinks="_FjUDoC33EeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="setReturnValueEAN" uuid="_7mMTMC32EeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_BJyjAC4VEeC0Rpt37j6jRA" incomingStoryLinks="_GZ2qEC33EeCSbYYn7Hqbqw _IaynEC33EeCSbYYn7Hqbqw _Q9qAAC33EeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_CrlokC33EeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_D3IOoC33EeCSbYYn7Hqbqw" expressionString="'set return value'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateMatchesSAN" uuid="_MXshgC33EeCSbYYn7Hqbqw" incomingStoryLinks="_SOIwcC33EeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_OvIS8C33EeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_Q9qAAC33EeCSbYYn7Hqbqw _SOIwcC33EeCSbYYn7Hqbqw" incomingStoryLinks="_V64H8C33EeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="expression" uuid="_-jmj5S4UEeC0Rpt37j6jRA" incomingStoryLinks="_BJyjAC4VEeC0Rpt37j6jRA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_-jmj5i4UEeC0Rpt37j6jRA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_-jmj5y4UEeC0Rpt37j6jRA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
            <activity href="../common/create_returnValue_expression.story#_qoFvMPxlEd--caqh5zaNfg"/>
            <parameters name="resultLiteral" uuid="_-jmj6C4UEeC0Rpt37j6jRA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-jmj6S4UEeC0Rpt37j6jRA" expressionString="mote::rules::TransformationResult::RULE_APPLIED" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//rules/TransformationResult"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_FjUDoC33EeCSbYYn7Hqbqw" modifier="CREATE" source="_5iqVMC32EeCSbYYn7Hqbqw" target="_7SDPMC32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GZ2qEC33EeCSbYYn7Hqbqw" modifier="CREATE" source="_5iqVMC32EeCSbYYn7Hqbqw" target="_7mMTMC32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_HGVVkC33EeCSbYYn7Hqbqw" modifier="CREATE" source="_7SDPMC32EeCSbYYn7Hqbqw" target="_6ZvuMC32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_IaynEC33EeCSbYYn7Hqbqw" modifier="CREATE" source="_7SDPMC32EeCSbYYn7Hqbqw" target="_7mMTMC32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Q9qAAC33EeCSbYYn7Hqbqw" modifier="CREATE" source="_OvIS8C33EeCSbYYn7Hqbqw" target="_7mMTMC32EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_SOIwcC33EeCSbYYn7Hqbqw" modifier="CREATE" source="_OvIS8C33EeCSbYYn7Hqbqw" target="_MXshgC33EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_V64H8C33EeCSbYYn7Hqbqw" modifier="CREATE" source="_5iqVMC32EeCSbYYn7Hqbqw" target="_OvIS8C33EeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BJyjAC4VEeC0Rpt37j6jRA" modifier="CREATE" source="_7mMTMC32EeCSbYYn7Hqbqw" target="_-jmj5S4UEeC0Rpt37j6jRA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create post-creation expressions" uuid="_n0nh4C7BEeCDMMgUmpccLw" incoming="_qfElwC3sEeCSbYYn7Hqbqw" outgoing="_HzddEC7CEeCDMMgUmpccLw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_zAJmMC7BEeCDMMgUmpccLw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:VariableDeclarationAction" uuid="_10rjUC7BEeCDMMgUmpccLw" variableName="outgoingEdge">
          <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
          <valueAssignment xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_4jAx0C7BEeCDMMgUmpccLw">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_9UGiQC7BEeCDMMgUmpccLw">
              <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
              <activity href="../common/create_executePostCreation_eans.story#_aZJ3ICfCEeCkHOR1O7gzpA"/>
              <parameters name="tggRule" uuid="_-pBGwC7BEeCDMMgUmpccLw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AeWQsC7CEeCDMMgUmpccLw" expressionString="tggRule" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
              </parameters>
              <parameters name="incomingEdge" uuid="_BoH8MC7CEeCDMMgUmpccLw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Cy5GMC7CEeCDMMgUmpccLw" expressionString="outgoingEdge" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
              </parameters>
              <parameters name="direction" uuid="_DpD5MC7CEeCDMMgUmpccLw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_FaDVsC7CEeCDMMgUmpccLw" expressionString="direction" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
              </parameters>
            </callActions>
          </valueAssignment>
        </callActions>
      </expression>
    </nodes>
    <edges uuid="_gFC34C3hEeCupczvU2Jg8w" source="_x041oC1QEeCupczvU2Jg8w" target="_Xlht4C3hEeCupczvU2Jg8w"/>
    <edges uuid="_YcVyYC3nEeCSbYYn7Hqbqw" source="_Xlht4C3hEeCupczvU2Jg8w" target="_HPvjgC3nEeCSbYYn7Hqbqw"/>
    <edges uuid="_xeBHIC3nEeCSbYYn7Hqbqw" source="_HPvjgC3nEeCSbYYn7Hqbqw" target="_ZTCJ0C3nEeCSbYYn7Hqbqw"/>
    <edges uuid="_1ainEC3nEeCSbYYn7Hqbqw" source="_ZTCJ0C3nEeCSbYYn7Hqbqw" target="_x725sC3nEeCSbYYn7Hqbqw"/>
    <edges uuid="_MqdqUC3oEeCSbYYn7Hqbqw" source="_x725sC3nEeCSbYYn7Hqbqw" target="_183RAC3nEeCSbYYn7Hqbqw"/>
    <edges uuid="_Sr95wC3oEeCSbYYn7Hqbqw" source="_183RAC3nEeCSbYYn7Hqbqw" target="_NEUUwC3oEeCSbYYn7Hqbqw"/>
    <edges uuid="_E9M8UC3rEeCSbYYn7Hqbqw" source="_NEUUwC3oEeCSbYYn7Hqbqw" target="_VKkqMC3oEeCSbYYn7Hqbqw"/>
    <edges uuid="_e3y2EC3rEeCSbYYn7Hqbqw" source="_VKkqMC3oEeCSbYYn7Hqbqw" target="_JWfQwC3rEeCSbYYn7Hqbqw"/>
    <edges uuid="_n5eR8C3rEeCSbYYn7Hqbqw" source="_JWfQwC3rEeCSbYYn7Hqbqw" target="_in9VAC3rEeCSbYYn7Hqbqw"/>
    <edges uuid="_I9IgIC3sEeCSbYYn7Hqbqw" source="_in9VAC3rEeCSbYYn7Hqbqw" target="_2fb1wC3rEeCSbYYn7Hqbqw"/>
    <edges uuid="_iFIfUC3sEeCSbYYn7Hqbqw" source="_2fb1wC3rEeCSbYYn7Hqbqw" target="_M-T2kC3sEeCSbYYn7Hqbqw"/>
    <edges uuid="_lffGUC3sEeCSbYYn7Hqbqw" source="_M-T2kC3sEeCSbYYn7Hqbqw" target="_jC2r0C3sEeCSbYYn7Hqbqw"/>
    <edges uuid="_qfElwC3sEeCSbYYn7Hqbqw" source="_jC2r0C3sEeCSbYYn7Hqbqw" target="_n0nh4C7BEeCDMMgUmpccLw"/>
    <edges uuid="_vLVxgC31EeCSbYYn7Hqbqw" source="_mgtSwC3sEeCSbYYn7Hqbqw" target="_smNiUC31EeCSbYYn7Hqbqw"/>
    <edges uuid="_B3Xy0C32EeCSbYYn7Hqbqw" source="_smNiUC31EeCSbYYn7Hqbqw" target="_AGJs0C32EeCSbYYn7Hqbqw"/>
    <edges uuid="_HlkrwC32EeCSbYYn7Hqbqw" source="_AGJs0C32EeCSbYYn7Hqbqw" target="_DOuAQC32EeCSbYYn7Hqbqw"/>
    <edges uuid="_gqYNAC32EeCSbYYn7Hqbqw" source="_DOuAQC32EeCSbYYn7Hqbqw" target="_eV0wcC32EeCSbYYn7Hqbqw"/>
    <edges uuid="_9bOiMC32EeCSbYYn7Hqbqw" source="_eV0wcC32EeCSbYYn7Hqbqw" target="_zsOGQC32EeCSbYYn7Hqbqw"/>
    <edges uuid="_YJiX4C33EeCSbYYn7Hqbqw" source="_zsOGQC32EeCSbYYn7Hqbqw" target="_0KulAC3nEeCSbYYn7Hqbqw"/>
    <edges uuid="_aC67YC33EeCSbYYn7Hqbqw" source="_xQ1GUC1QEeCupczvU2Jg8w" target="_x041oC1QEeCupczvU2Jg8w"/>
    <edges uuid="_HzddEC7CEeCDMMgUmpccLw" source="_n0nh4C7BEeCDMMgUmpccLw" target="_mgtSwC3sEeCSbYYn7Hqbqw"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
