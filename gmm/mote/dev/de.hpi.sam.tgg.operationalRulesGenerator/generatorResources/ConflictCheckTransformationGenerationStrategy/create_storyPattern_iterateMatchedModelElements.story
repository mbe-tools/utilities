<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_iterateMatchedModelElements" uuid="_q3vZMFOjEeCY5NYqK2RsYA">
  <activities name="create_storyPattern_iterateMatchedModelElements" uuid="_rb-usFOjEeCY5NYqK2RsYA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_ta3dQFOjEeCY5NYqK2RsYA" outgoing="_vzEqwFOjEeCY5NYqK2RsYA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_txEWwFOjEeCY5NYqK2RsYA" incoming="_vzEqwFOjEeCY5NYqK2RsYA" outgoing="_RDbRIFOkEeCY5NYqK2RsYA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_xGKDQFOjEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_jT37IFPPEeCn1cdUG-DPzw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create SPOs" uuid="_3kHlMFOjEeCY5NYqK2RsYA" incoming="_RDbRIFOkEeCY5NYqK2RsYA" outgoing="_8BEcMFOkEeCY5NYqK2RsYA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchSpo" uuid="_9eOzMFOjEeCY5NYqK2RsYA" incomingStoryLinks="_c-zeAFOkEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_DRP6sFOkEeCY5NYqK2RsYA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_D2KloFOkEeCY5NYqK2RsYA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_match_spo.story#_6d114Bi_EeCuQ-PF2tTDdw"/>
            <parameters name="storyActionNode" uuid="_FqvhoFOkEeCY5NYqK2RsYA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H9wtIFOkEeCY5NYqK2RsYA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="modifier" uuid="_I0eSsFOkEeCY5NYqK2RsYA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KV5CIFOkEeCY5NYqK2RsYA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
            </parameters>
            <parameters name="bindingType" uuid="_L7NaIFOkEeCY5NYqK2RsYA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NFW5EFOkEeCY5NYqK2RsYA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_RWCEEFOkEeCY5NYqK2RsYA" outgoingStoryLinks="_bSFdgFOkEeCY5NYqK2RsYA _cgDsgFOkEeCY5NYqK2RsYA _emp3sFPHEeCMH6_3CQDAJg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchedModelElementSPO" uuid="_SkiekFOkEeCY5NYqK2RsYA" modifier="CREATE" incomingStoryLinks="_cgDsgFOkEeCY5NYqK2RsYA _MC6PsFPHEeCoOLYcY7U-og">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_Xwj3gFOkEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_YwV5gFOkEeCY5NYqK2RsYA" expressionString="'matchedModelElement'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_gVGrcFOkEeCY5NYqK2RsYA">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hq2WYFOkEeCY5NYqK2RsYA" expressionString="ecore::EObject" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="link" uuid="_TyWVgFOkEeCY5NYqK2RsYA" modifier="CREATE" outgoingStoryLinks="_c-zeAFOkEeCY5NYqK2RsYA _d2psAFOkEeCY5NYqK2RsYA _0fNNQFOkEeCY5NYqK2RsYA _5av6MFOkEeCY5NYqK2RsYA _MC6PsFPHEeCoOLYcY7U-og" incomingStoryLinks="_bSFdgFOkEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchEClass" uuid="_kXgJ4FOkEeCY5NYqK2RsYA" outgoingStoryLinks="_zW0B0FOkEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nO500FOkEeCY5NYqK2RsYA" expressionString="mote::rules::Match" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createdElementsERef" uuid="_qboPgFOkEeCY5NYqK2RsYA" incomingStoryLinks="_zW0B0FOkEeCY5NYqK2RsYA _0fNNQFOkEeCY5NYqK2RsYA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xpF7wFOkEeCY5NYqK2RsYA" expressionString="direction = mote::TransformationDirection::FORWARD and self.name = 'sourceCreatedElements' or&#xA;direction = mote::TransformationDirection::MAPPING and self.name = 'corrCreatedElements' or&#xA;direction = mote::TransformationDirection::REVERSE and self.name = 'targetCreatedElements'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mapEntryEClass" uuid="_06I1wFOkEeCY5NYqK2RsYA" incomingStoryLinks="_5av6MFOkEeCY5NYqK2RsYA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2xc8sFOkEeCY5NYqK2RsYA" expressionString="mote::helpers::MapEntry" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="keySpo" uuid="_E0ln0FPHEeCoOLYcY7U-og" modifier="CREATE" incomingStoryLinks="_d2psAFOkEeCY5NYqK2RsYA _emp3sFPHEeCMH6_3CQDAJg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_JjhcIFPHEeCoOLYcY7U-og">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KYyQMFPHEeCoOLYcY7U-og" expressionString="'keyString'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_M8aqkFPHEeCoOLYcY7U-og">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OCY0IFPHEeCoOLYcY7U-og" expressionString="ecore::EString" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_bSFdgFOkEeCY5NYqK2RsYA" modifier="CREATE" source="_RWCEEFOkEeCY5NYqK2RsYA" target="_TyWVgFOkEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_cgDsgFOkEeCY5NYqK2RsYA" modifier="CREATE" source="_RWCEEFOkEeCY5NYqK2RsYA" target="_SkiekFOkEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_c-zeAFOkEeCY5NYqK2RsYA" modifier="CREATE" source="_TyWVgFOkEeCY5NYqK2RsYA" target="_9eOzMFOjEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_d2psAFOkEeCY5NYqK2RsYA" modifier="CREATE" source="_TyWVgFOkEeCY5NYqK2RsYA" target="_E0ln0FPHEeCoOLYcY7U-og">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zW0B0FOkEeCY5NYqK2RsYA" source="_kXgJ4FOkEeCY5NYqK2RsYA" target="_qboPgFOkEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0fNNQFOkEeCY5NYqK2RsYA" modifier="CREATE" source="_TyWVgFOkEeCY5NYqK2RsYA" target="_qboPgFOkEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5av6MFOkEeCY5NYqK2RsYA" modifier="CREATE" source="_TyWVgFOkEeCY5NYqK2RsYA" target="_06I1wFOkEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_MC6PsFPHEeCoOLYcY7U-og" modifier="CREATE" source="_TyWVgFOkEeCY5NYqK2RsYA" target="_SkiekFOkEeCY5NYqK2RsYA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/valueTarget"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_emp3sFPHEeCMH6_3CQDAJg" modifier="CREATE" source="_RWCEEFOkEeCY5NYqK2RsYA" target="_E0ln0FPHEeCoOLYcY7U-og">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_7Dk5MFOkEeCY5NYqK2RsYA" incoming="_8BEcMFOkEeCY5NYqK2RsYA"/>
    <edges uuid="_vzEqwFOjEeCY5NYqK2RsYA" source="_ta3dQFOjEeCY5NYqK2RsYA" target="_txEWwFOjEeCY5NYqK2RsYA"/>
    <edges uuid="_RDbRIFOkEeCY5NYqK2RsYA" source="_txEWwFOjEeCY5NYqK2RsYA" target="_3kHlMFOjEeCY5NYqK2RsYA"/>
    <edges uuid="_8BEcMFOkEeCY5NYqK2RsYA" source="_3kHlMFOjEeCY5NYqK2RsYA" target="_7Dk5MFOkEeCY5NYqK2RsYA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
