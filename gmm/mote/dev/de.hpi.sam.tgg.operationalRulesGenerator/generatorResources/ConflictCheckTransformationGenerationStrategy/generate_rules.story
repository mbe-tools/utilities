<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="Simple Transformation Generation Strategy" uuid="_f2pxUXUrEd-AwpZ_Fdb-iA">
  <activities name="generate_rules" uuid="_Ps8bcHefEd-UQsRE6oiJVQ">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_c7k_8HefEd-UQsRE6oiJVQ" outgoing="_HhaH8HegEd-UQsRE6oiJVQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story diagram and traceability link store" uuid="_soasQHefEd-UQsRE6oiJVQ" incoming="_HhaH8HegEd-UQsRE6oiJVQ" outgoing="_hxg_4He6Ed-4c678KwnPnw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggDiagram" uuid="_vYXEUHefEd-UQsRE6oiJVQ" incomingStoryLinks="_fN73wCoSEeC1fMsDpsxl5Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesPackage" uuid="_MW-vwHegEd-UQsRE6oiJVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="motePackage" uuid="_BwVrwHehEd-UQsRE6oiJVQ" outgoingStoryLinks="_eG21YHehEd-UQsRE6oiJVQ _0L7J4HezEd-4c678KwnPnw _PpojIPI9Ed-7z74s9UiuPA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleEClass" uuid="_cQvBQHehEd-UQsRE6oiJVQ" outgoingStoryLinks="_qbLGsHehEd-UQsRE6oiJVQ _qtNR4HehEd-UQsRE6oiJVQ _q8PMYHehEd-UQsRE6oiJVQ _fn2DgAt8EeC6TdZK2gc2HA" incomingStoryLinks="_eG21YHehEd-UQsRE6oiJVQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ksz1sHehEd-UQsRE6oiJVQ" expressionString="self.name = 'TGGRule'&#xD;&#xA;" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ft1" uuid="_eoVKwHehEd-UQsRE6oiJVQ" incomingStoryLinks="_qbLGsHehEd-UQsRE6oiJVQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_i_3ywHehEd-UQsRE6oiJVQ" expressionString="self.name = 'forwardTransformation'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mt1" uuid="_fFBGwHehEd-UQsRE6oiJVQ" incomingStoryLinks="_qtNR4HehEd-UQsRE6oiJVQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mWtywHehEd-UQsRE6oiJVQ" expressionString="self.name = 'mappingTransformation'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rt1" uuid="_f2XrMHehEd-UQsRE6oiJVQ" incomingStoryLinks="_q8PMYHehEd-UQsRE6oiJVQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_n89X0HehEd-UQsRE6oiJVQ" expressionString="self.name = 'reverseTransformation'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggAxiomEClass" uuid="_xukWoHezEd-4c678KwnPnw" outgoingStoryLinks="_m0wXoHe3Ed-4c678KwnPnw _nu2oYHe3Ed-4c678KwnPnw _oeGUAHe3Ed-4c678KwnPnw" incomingStoryLinks="_0L7J4HezEd-4c678KwnPnw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tb6qIHe9Ed-INsK5Dz3ubg" expressionString="self.name = 'TGGAxiom'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="fta1" uuid="_11DD0HezEd-4c678KwnPnw" incomingStoryLinks="_m0wXoHe3Ed-4c678KwnPnw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hzkVsHe3Ed-4c678KwnPnw" expressionString="self.name = 'forwardTransformation'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mta1" uuid="_4LJ_kHezEd-4c678KwnPnw" incomingStoryLinks="_nu2oYHe3Ed-4c678KwnPnw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jh-_UHe3Ed-4c678KwnPnw" expressionString="self.name = 'mappingTransformation'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rta1" uuid="__W8Q0HezEd-4c678KwnPnw" incomingStoryLinks="_oeGUAHe3Ed-4c678KwnPnw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lOxRQHe3Ed-4c678KwnPnw" expressionString="self.name = 'reverseTransformation'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleSetEClass" uuid="_L1OdYPI9Ed-7z74s9UiuPA" outgoingStoryLinks="_oNf7MPh3Ed-R0aK4IAlsBA" incomingStoryLinks="_PpojIPI9Ed-7z74s9UiuPA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y0_fcPI9Ed-7z74s9UiuPA" expressionString="self.name = 'TGGRuleSet'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createRulesOperation" uuid="_jENtMPh3Ed-R0aK4IAlsBA" incomingStoryLinks="_oNf7MPh3Ed-R0aK4IAlsBA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7disPh3Ed-R0aK4IAlsBA" expressionString="self.name = 'createRules'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="projectName" uuid="_ygKrkPh8Ed-R0aK4IAlsBA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="acceptsParentNodeOperation" uuid="_csV7sAt8EeC6TdZK2gc2HA" incomingStoryLinks="_fn2DgAt8EeC6TdZK2gc2HA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gqhogAt8EeC6TdZK2gc2HA" expressionString="self.name = 'acceptsParentCorrNode'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="basePackage" uuid="_AzzxwAuAEeClm-1TBJQXDQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="genInfos" uuid="_SvDgcBfjEeCDKYu_f7BVug" modifier="CREATE" outgoingStoryLinks="_fN73wCoSEeC1fMsDpsxl5Q">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/GenerationInfoStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_eG21YHehEd-UQsRE6oiJVQ" source="_BwVrwHehEd-UQsRE6oiJVQ" target="_cQvBQHehEd-UQsRE6oiJVQ"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qbLGsHehEd-UQsRE6oiJVQ" source="_cQvBQHehEd-UQsRE6oiJVQ" target="_eoVKwHehEd-UQsRE6oiJVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qtNR4HehEd-UQsRE6oiJVQ" source="_cQvBQHehEd-UQsRE6oiJVQ" target="_fFBGwHehEd-UQsRE6oiJVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_q8PMYHehEd-UQsRE6oiJVQ" source="_cQvBQHehEd-UQsRE6oiJVQ" target="_f2XrMHehEd-UQsRE6oiJVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_0L7J4HezEd-4c678KwnPnw" source="_BwVrwHehEd-UQsRE6oiJVQ" target="_xukWoHezEd-4c678KwnPnw"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_m0wXoHe3Ed-4c678KwnPnw" source="_xukWoHezEd-4c678KwnPnw" target="_11DD0HezEd-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_nu2oYHe3Ed-4c678KwnPnw" source="_xukWoHezEd-4c678KwnPnw" target="_4LJ_kHezEd-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_oeGUAHe3Ed-4c678KwnPnw" source="_xukWoHezEd-4c678KwnPnw" target="__W8Q0HezEd-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_PpojIPI9Ed-7z74s9UiuPA" source="_BwVrwHehEd-UQsRE6oiJVQ" target="_L1OdYPI9Ed-7z74s9UiuPA"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_oNf7MPh3Ed-R0aK4IAlsBA" source="_L1OdYPI9Ed-7z74s9UiuPA" target="_jENtMPh3Ed-R0aK4IAlsBA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fn2DgAt8EeC6TdZK2gc2HA" source="_cQvBQHehEd-UQsRE6oiJVQ" target="_csV7sAt8EeC6TdZK2gc2HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fN73wCoSEeC1fMsDpsxl5Q" modifier="CREATE" source="_SvDgcBfjEeCDKYu_f7BVug" target="_vYXEUHefEd-UQsRE6oiJVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/GenerationInfoStore/tggDiagram"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="__itc8HeoEd-4c678KwnPnw" incoming="_DrqqEPiEEd-StusXmfUQQA">
      <returnValue xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r3kjsCoQEeC4u-viSitsOw" expressionString="genInfos" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create EClass for each rule" uuid="_bs6EAHe4Ed-4c678KwnPnw" incoming="_6bInsHe_Ed-INsK5Dz3ubg _gHh2wPcQEd-XR4VMarhb7w" outgoing="_ViuK8He-Ed-INsK5Dz3ubg _Y4ZigHhcEd-NMp17Gm153Q" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesPackage" uuid="_tGxS0He4Ed-4c678KwnPnw" outgoingStoryLinks="_7JowoHe4Ed-4c678KwnPnw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleEClass" uuid="_vqWp4He4Ed-4c678KwnPnw" modifier="CREATE" incomingStoryLinks="_7JowoHe4Ed-4c678KwnPnw _FTtqwBflEeCO-aRUjt8-yQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <attributeAssignments uuid="_ykCz0He4Ed-4c678KwnPnw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//ENamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0fudYHe4Ed-4c678KwnPnw" expressionString="tggRule.name" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_JSq8ABfXEeCVjoDp-muf0Q" incomingStoryLinks="_Ngu34BfXEeCVjoDp-muf0Q _EzXWwBflEeCO-aRUjt8-yQ">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggDiagram" uuid="_K5Cc4BfXEeCVjoDp-muf0Q" outgoingStoryLinks="_Ngu34BfXEeCVjoDp-muf0Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="genInfos" uuid="_-GU3ABfkEeCO-aRUjt8-yQ" outgoingStoryLinks="_EJQp0BflEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/GenerationInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_BbOWQBflEeCO-aRUjt8-yQ" modifier="CREATE" outgoingStoryLinks="_EzXWwBflEeCO-aRUjt8-yQ _FTtqwBflEeCO-aRUjt8-yQ" incomingStoryLinks="_EJQp0BflEeCO-aRUjt8-yQ">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7JowoHe4Ed-4c678KwnPnw" modifier="CREATE" source="_tGxS0He4Ed-4c678KwnPnw" target="_vqWp4He4Ed-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage/eClassifiers"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ngu34BfXEeCVjoDp-muf0Q" source="_K5Cc4BfXEeCVjoDp-muf0Q" target="_JSq8ABfXEeCVjoDp-muf0Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGDiagram/tggRules"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_EJQp0BflEeCO-aRUjt8-yQ" modifier="CREATE" source="_-GU3ABfkEeCO-aRUjt8-yQ" target="_BbOWQBflEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/GenerationInfoStore/ruleInfos"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_EzXWwBflEeCO-aRUjt8-yQ" modifier="CREATE" source="_BbOWQBflEeCO-aRUjt8-yQ" target="_JSq8ABfXEeCVjoDp-muf0Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/tggRule"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_FTtqwBflEeCO-aRUjt8-yQ" modifier="CREATE" source="_BbOWQBflEeCO-aRUjt8-yQ" target="_vqWp4He4Ed-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/ruleEClass"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="clone EOperations for axiom" uuid="_99wEwHe9Ed-INsK5Dz3ubg" incoming="_LURZ8He-Ed-INsK5Dz3ubg" outgoing="_yj5Y0BfoEeCO-aRUjt8-yQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggAxiomEClass" uuid="_XOqvYHe-Ed-INsK5Dz3ubg" incomingStoryLinks="_bv0mYHe-Ed-INsK5Dz3ubg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleEClass" uuid="_aHmrYHe-Ed-INsK5Dz3ubg" outgoingStoryLinks="_bv0mYHe-Ed-INsK5Dz3ubg _9gMJkHe-Ed-INsK5Dz3ubg _-qI0MHe-Ed-INsK5Dz3ubg __wbt4He-Ed-INsK5Dz3ubg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardOperation" uuid="_ePa1UHe-Ed-INsK5Dz3ubg" incomingStoryLinks="_9gMJkHe-Ed-INsK5Dz3ubg _RK4tEBflEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_p1drYHe-Ed-INsK5Dz3ubg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_qsA44He-Ed-INsK5Dz3ubg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters uuid="_tLPUYHe-Ed-INsK5Dz3ubg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_uHb24He-Ed-INsK5Dz3ubg" expressionString="fta1" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseOperation" uuid="_gE_P0He-Ed-INsK5Dz3ubg" incomingStoryLinks="__wbt4He-Ed-INsK5Dz3ubg _RvufgBflEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_4PtHYHe-Ed-INsK5Dz3ubg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_46DE4He-Ed-INsK5Dz3ubg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters uuid="_7QCE0He-Ed-INsK5Dz3ubg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8Sp_cHe-Ed-INsK5Dz3ubg" expressionString="rta1" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingOperation" uuid="_wLQv0He-Ed-INsK5Dz3ubg" incomingStoryLinks="_-qI0MHe-Ed-INsK5Dz3ubg _RdMlEBflEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_ykxk4He-Ed-INsK5Dz3ubg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_zJxvYHe-Ed-INsK5Dz3ubg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters uuid="_1jqX4He-Ed-INsK5Dz3ubg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2b0H4He-Ed-INsK5Dz3ubg" expressionString="mta1" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_OBZMkBflEeCO-aRUjt8-yQ" outgoingStoryLinks="_RK4tEBflEeCO-aRUjt8-yQ _RdMlEBflEeCO-aRUjt8-yQ _RvufgBflEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_bv0mYHe-Ed-INsK5Dz3ubg" modifier="CREATE" source="_aHmrYHe-Ed-INsK5Dz3ubg" target="_XOqvYHe-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eSuperTypes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9gMJkHe-Ed-INsK5Dz3ubg" modifier="CREATE" source="_aHmrYHe-Ed-INsK5Dz3ubg" target="_ePa1UHe-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_-qI0MHe-Ed-INsK5Dz3ubg" modifier="CREATE" source="_aHmrYHe-Ed-INsK5Dz3ubg" target="_wLQv0He-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="__wbt4He-Ed-INsK5Dz3ubg" modifier="CREATE" source="_aHmrYHe-Ed-INsK5Dz3ubg" target="_gE_P0He-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RK4tEBflEeCO-aRUjt8-yQ" modifier="CREATE" source="_OBZMkBflEeCO-aRUjt8-yQ" target="_ePa1UHe-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/forwardTransformationOperation"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RdMlEBflEeCO-aRUjt8-yQ" modifier="CREATE" source="_OBZMkBflEeCO-aRUjt8-yQ" target="_wLQv0He-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/mappingTransformationOperation"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RvufgBflEeCO-aRUjt8-yQ" modifier="CREATE" source="_OBZMkBflEeCO-aRUjt8-yQ" target="_gE_P0He-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/reverseTransformationOperation"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_JQQ60He-Ed-INsK5Dz3ubg" incoming="_aNDBQHhcEd-NMp17Gm153Q" outgoing="_LURZ8He-Ed-INsK5Dz3ubg _1xeU8He_Ed-INsK5Dz3ubg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="clone EOperations for rules" uuid="_ONhpkHe_Ed-INsK5Dz3ubg" incoming="_1xeU8He_Ed-INsK5Dz3ubg" outgoing="_0LuSQBfoEeCO-aRUjt8-yQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleEClass" uuid="_QNlc0He_Ed-INsK5Dz3ubg" incomingStoryLinks="_TzQjAHe_Ed-INsK5Dz3ubg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleEClass" uuid="_SVxRUHe_Ed-INsK5Dz3ubg" outgoingStoryLinks="_TzQjAHe_Ed-INsK5Dz3ubg _retrsHe_Ed-INsK5Dz3ubg _sS46oHe_Ed-INsK5Dz3ubg _tOBtMHe_Ed-INsK5Dz3ubg _wxr5UAt8EeC6TdZK2gc2HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardOperation" uuid="_U0_s0He_Ed-INsK5Dz3ubg" incomingStoryLinks="_retrsHe_Ed-INsK5Dz3ubg _7aLyIBfqEeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_WlKC4He_Ed-INsK5Dz3ubg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_XDdIcHe_Ed-INsK5Dz3ubg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters uuid="_ZfY_YHe_Ed-INsK5Dz3ubg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_aYnGYHe_Ed-INsK5Dz3ubg" expressionString="ft1" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingOperation" uuid="_byEMoHe_Ed-INsK5Dz3ubg" incomingStoryLinks="_sS46oHe_Ed-INsK5Dz3ubg _7q0PIBfqEeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_e8gvYHe_Ed-INsK5Dz3ubg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_fcjhYHe_Ed-INsK5Dz3ubg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters uuid="_hdQ04He_Ed-INsK5Dz3ubg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_iPYOUHe_Ed-INsK5Dz3ubg" expressionString="mt1" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseOperation" uuid="_jYpjgHe_Ed-INsK5Dz3ubg" incomingStoryLinks="_tOBtMHe_Ed-INsK5Dz3ubg _8BUqoBfqEeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_lt3ucHe_Ed-INsK5Dz3ubg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_mToG4He_Ed-INsK5Dz3ubg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters uuid="_oVZxYHe_Ed-INsK5Dz3ubg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_pZr44He_Ed-INsK5Dz3ubg" expressionString="rt1" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="clonedAcceptsParentCorrNodeOperation" uuid="_sb0FQAt8EeC6TdZK2gc2HA" outgoingStoryLinks="_BgRWgAt9EeC6TdZK2gc2HA" incomingStoryLinks="_wxr5UAt8EeC6TdZK2gc2HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_0TDuUAt8EeC6TdZK2gc2HA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_0wVgMAt8EeC6TdZK2gc2HA" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
            <parameters name="" uuid="_3Z6KgAt8EeC6TdZK2gc2HA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5ONZsAt8EeC6TdZK2gc2HA" expressionString="acceptsParentNodeOperation" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="annotation" uuid="__7-SMAt8EeC6TdZK2gc2HA" outgoingStoryLinks="_oTMNkAt9EeC6TdZK2gc2HA" incomingStoryLinks="_BgRWgAt9EeC6TdZK2gc2HA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_DGtI0At9EeC6TdZK2gc2HA" expressionString="self.source ='http://www.eclipse.org/emf/2002/GenModel'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="detailsEntry" uuid="_Dm99QAt9EeC6TdZK2gc2HA" modifier="CREATE" incomingStoryLinks="_oTMNkAt9EeC6TdZK2gc2HA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry"/>
        <attributeAssignments uuid="_HPXnUAt9EeC6TdZK2gc2HA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/key"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IiWSIAt9EeC6TdZK2gc2HA" expressionString="'body'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_Kb3_kAt9EeC6TdZK2gc2HA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/value"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_LV-3YAt9EeC6TdZK2gc2HA">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_L5EWUAt9EeC6TdZK2gc2HA" methodClassName="de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationHelpers" methodName="generateAcceptsParentNodeCode">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              <parameters name="" uuid="_hpYoAAt9EeC6TdZK2gc2HA">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lpFXwAt9EeC6TdZK2gc2HA" expressionString="basePackage.concat('.').concat(rulesPackage.name)" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_8YvUoAt_EeClm-1TBJQXDQ">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9YgvkAt_EeClm-1TBJQXDQ" expressionString="tggRule" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
              </parameters>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_3g2JEBfqEeCvZOwhS7-eiQ" outgoingStoryLinks="_7aLyIBfqEeCvZOwhS7-eiQ _7q0PIBfqEeCvZOwhS7-eiQ _8BUqoBfqEeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_TzQjAHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_SVxRUHe_Ed-INsK5Dz3ubg" target="_QNlc0He_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eSuperTypes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_retrsHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_SVxRUHe_Ed-INsK5Dz3ubg" target="_U0_s0He_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_sS46oHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_SVxRUHe_Ed-INsK5Dz3ubg" target="_byEMoHe_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tOBtMHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_SVxRUHe_Ed-INsK5Dz3ubg" target="_jYpjgHe_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_wxr5UAt8EeC6TdZK2gc2HA" modifier="CREATE" source="_SVxRUHe_Ed-INsK5Dz3ubg" target="_sb0FQAt8EeC6TdZK2gc2HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BgRWgAt9EeC6TdZK2gc2HA" source="_sb0FQAt8EeC6TdZK2gc2HA" target="__7-SMAt8EeC6TdZK2gc2HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EModelElement/eAnnotations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_oTMNkAt9EeC6TdZK2gc2HA" modifier="CREATE" source="__7-SMAt8EeC6TdZK2gc2HA" target="_Dm99QAt9EeC6TdZK2gc2HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation/details"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7aLyIBfqEeCvZOwhS7-eiQ" modifier="CREATE" source="_3g2JEBfqEeCvZOwhS7-eiQ" target="_U0_s0He_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/forwardTransformationOperation"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7q0PIBfqEeCvZOwhS7-eiQ" modifier="CREATE" source="_3g2JEBfqEeCvZOwhS7-eiQ" target="_byEMoHe_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/mappingTransformationOperation"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8BUqoBfqEeCvZOwhS7-eiQ" modifier="CREATE" source="_3g2JEBfqEeCvZOwhS7-eiQ" target="_jYpjgHe_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/reverseTransformationOperation"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_5NLm0He_Ed-INsK5Dz3ubg" incoming="_yj5Y0BfoEeCO-aRUjt8-yQ _LkqG0D5lEeCS07xt5ugfeQ" outgoing="_upKacPigEd-bw8R_eH98gw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="delete old EClasses" uuid="_WmaRwHhcEd-NMp17Gm153Q" incoming="_Y4ZigHhcEd-NMp17Gm153Q" outgoing="_aNDBQHhcEd-NMp17Gm153Q" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesPackage" uuid="_cUufcHhcEd-NMp17Gm153Q" outgoingStoryLinks="_i6M-oHhcEd-NMp17Gm153Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleEClass" uuid="_eGsMUHhcEd-NMp17Gm153Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="existingEClass" uuid="_gauEcHhcEd-NMp17Gm153Q" modifier="DESTROY" incomingStoryLinks="_i6M-oHhcEd-NMp17Gm153Q">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kGrY0HhcEd-NMp17Gm153Q" expressionString="self.name = ruleEClass.name" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_i6M-oHhcEd-NMp17Gm153Q" modifier="DESTROY" source="_cUufcHhcEd-NMp17Gm153Q" target="_gauEcHhcEd-NMp17Gm153Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage/eClassifiers"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="delete old ruleSet EClass" uuid="_YRThYPcQEd-XR4VMarhb7w" incoming="_hxg_4He6Ed-4c678KwnPnw" outgoing="_fcNB0PcQEd-XR4VMarhb7w" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="oldRuleSetEClass" uuid="_acJoUPcQEd-XR4VMarhb7w" modifier="DESTROY" outgoingStoryLinks="_wNVcsPcQEd-XR4VMarhb7w" incomingStoryLinks="_rROvEPcQEd-XR4VMarhb7w">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesPackage" uuid="_cwr2wPcQEd-XR4VMarhb7w" outgoingStoryLinks="_rROvEPcQEd-XR4VMarhb7w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eClass" uuid="_uS2tAPcQEd-XR4VMarhb7w" incomingStoryLinks="_wNVcsPcQEd-XR4VMarhb7w">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mctIgPeyEd-Na8LTUpHsgA" expressionString="self.name = 'TGGRuleSet'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_rROvEPcQEd-XR4VMarhb7w" modifier="DESTROY" source="_cwr2wPcQEd-XR4VMarhb7w" target="_acJoUPcQEd-XR4VMarhb7w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage/eClassifiers"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_wNVcsPcQEd-XR4VMarhb7w" modifier="DESTROY" source="_acJoUPcQEd-XR4VMarhb7w" target="_uS2tAPcQEd-XR4VMarhb7w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eSuperTypes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create new ruleSet EClass" uuid="_eUP7QPcQEd-XR4VMarhb7w" incoming="_fcNB0PcQEd-XR4VMarhb7w" outgoing="_gHh2wPcQEd-XR4VMarhb7w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesPackage" uuid="_izBzsPcQEd-XR4VMarhb7w" outgoingStoryLinks="_0qQT8PcQEd-XR4VMarhb7w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="generatedRuleSetEClass" uuid="_QpKGcPI9Ed-7z74s9UiuPA" modifier="CREATE" outgoingStoryLinks="_1l7SAPcQEd-XR4VMarhb7w _P7W14PiLEd-S_r9zK9ICeQ" incomingStoryLinks="_0qQT8PcQEd-XR4VMarhb7w _70XbcBfkEeCO-aRUjt8-yQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <attributeAssignments uuid="_c6vz4PI9Ed-7z74s9UiuPA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//ENamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fho9oPI9Ed-7z74s9UiuPA" expressionString="tggDiagram.name.concat('RuleSet')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleSetEClass" uuid="_lu7kIPcQEd-XR4VMarhb7w" incomingStoryLinks="_1l7SAPcQEd-XR4VMarhb7w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="resourceSetAttribute" uuid="_HnrvgPiLEd-S_r9zK9ICeQ" modifier="CREATE" incomingStoryLinks="_P7W14PiLEd-S_r9zK9ICeQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAttribute"/>
        <attributeAssignments uuid="_KJe94PiLEd-S_r9zK9ICeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//ENamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NpIUkPiLEd-S_r9zK9ICeQ" expressionString="'resourceSet'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_K29H4PiLEd-S_r9zK9ICeQ">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//ETypedElement/eType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_MEIFoPiLEd-S_r9zK9ICeQ" expressionString="ecore::EResourceSet" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="genInfos" uuid="_5CoToBfkEeCO-aRUjt8-yQ" outgoingStoryLinks="_70XbcBfkEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/GenerationInfoStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0qQT8PcQEd-XR4VMarhb7w" modifier="CREATE" source="_izBzsPcQEd-XR4VMarhb7w" target="_QpKGcPI9Ed-7z74s9UiuPA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage/eClassifiers"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1l7SAPcQEd-XR4VMarhb7w" modifier="CREATE" source="_QpKGcPI9Ed-7z74s9UiuPA" target="_lu7kIPcQEd-XR4VMarhb7w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eSuperTypes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_P7W14PiLEd-S_r9zK9ICeQ" modifier="CREATE" source="_QpKGcPI9Ed-7z74s9UiuPA" target="_HnrvgPiLEd-S_r9zK9ICeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eStructuralFeatures"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_70XbcBfkEeCO-aRUjt8-yQ" modifier="CREATE" source="_5CoToBfkEeCO-aRUjt8-yQ" target="_QpKGcPI9Ed-7z74s9UiuPA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/GenerationInfoStore/ruleSetEClass"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create operation for createRules" uuid="_VlrrgPh3Ed-R0aK4IAlsBA" incoming="_ViuK8He-Ed-INsK5Dz3ubg" outgoing="_DrqqEPiEEd-StusXmfUQQA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="generatedRuleSetEClass" uuid="_go9ywPh3Ed-R0aK4IAlsBA" outgoingStoryLinks="_82Fv0Ph3Ed-R0aK4IAlsBA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="clonedCreateRulesOperation" uuid="_vpekkPh3Ed-R0aK4IAlsBA" modifier="CREATE" outgoingStoryLinks="_FzGuIPh4Ed-R0aK4IAlsBA" incomingStoryLinks="_82Fv0Ph3Ed-R0aK4IAlsBA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <attributeAssignments uuid="_M156IPiGEd-G6YvKstjLMQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//ENamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OJ7t0PiGEd-G6YvKstjLMQ" expressionString="'createRules'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="codeAnnotation" uuid="_DjmJoPh4Ed-R0aK4IAlsBA" modifier="CREATE" outgoingStoryLinks="_WDLqYPh4Ed-R0aK4IAlsBA" incomingStoryLinks="_FzGuIPh4Ed-R0aK4IAlsBA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation"/>
        <attributeAssignments uuid="_HJ13kPh4Ed-R0aK4IAlsBA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation/source"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_L0easPh4Ed-R0aK4IAlsBA" expressionString="'http://www.eclipse.org/emf/2002/GenModel'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="codeEntry" uuid="_OPxYcPh4Ed-R0aK4IAlsBA" modifier="CREATE" incomingStoryLinks="_WDLqYPh4Ed-R0aK4IAlsBA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry"/>
        <attributeAssignments uuid="_YBSV0Ph4Ed-R0aK4IAlsBA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/key"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZGviAPh4Ed-R0aK4IAlsBA" expressionString="'body'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_abE3sPh4Ed-R0aK4IAlsBA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/value"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_c-mkYPh4Ed-R0aK4IAlsBA">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_WlZfQPh7Ed-R0aK4IAlsBA" methodClassName="de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationHelpers" methodName="generateCreateRulesCode">
              <parameters name="" uuid="_LNG74Ph8Ed-R0aK4IAlsBA">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kC7l8Ph8Ed-R0aK4IAlsBA" expressionString="tggDiagram" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGDiagram"/>
              </parameters>
              <parameters name="" uuid="_17N2IPh8Ed-R0aK4IAlsBA">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3kvYsPh8Ed-R0aK4IAlsBA" expressionString="rulesPackage" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
              </parameters>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_82Fv0Ph3Ed-R0aK4IAlsBA" modifier="CREATE" source="_go9ywPh3Ed-R0aK4IAlsBA" target="_vpekkPh3Ed-R0aK4IAlsBA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_FzGuIPh4Ed-R0aK4IAlsBA" modifier="CREATE" source="_vpekkPh3Ed-R0aK4IAlsBA" target="_DjmJoPh4Ed-R0aK4IAlsBA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EModelElement/eAnnotations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WDLqYPh4Ed-R0aK4IAlsBA" modifier="CREATE" source="_DjmJoPh4Ed-R0aK4IAlsBA" target="_OPxYcPh4Ed-R0aK4IAlsBA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation/details"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create source code annotation" uuid="_ie7SMPigEd-bw8R_eH98gw" incoming="_SRSbICoSEeCKlbWGrdxQtQ" outgoing="_6bInsHe_Ed-INsK5Dz3ubg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardOperation" uuid="_kPpo8PigEd-bw8R_eH98gw" outgoingStoryLinks="_CKxY4PihEd-bw8R_eH98gw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingOperation" uuid="_k9pXYPigEd-bw8R_eH98gw" outgoingStoryLinks="_Ia6hQPihEd-bw8R_eH98gw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseOperation" uuid="_lqDKYPigEd-bw8R_eH98gw" outgoingStoryLinks="_I94EYPihEd-bw8R_eH98gw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardAnnotation" uuid="_mYWa0PigEd-bw8R_eH98gw" outgoingStoryLinks="_KPLUMPihEd-bw8R_eH98gw" incomingStoryLinks="_CKxY4PihEd-bw8R_eH98gw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xk1PcPiiEd-bw8R_eH98gw" expressionString="self.source ='http://www.eclipse.org/emf/2002/GenModel'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingAnnotation" uuid="_nfTb0PigEd-bw8R_eH98gw" outgoingStoryLinks="_KiG3QPihEd-bw8R_eH98gw" incomingStoryLinks="_Ia6hQPihEd-bw8R_eH98gw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3B7xUPiiEd-bw8R_eH98gw" expressionString="self.source ='http://www.eclipse.org/emf/2002/GenModel'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseAnnotation" uuid="_oNxrYPigEd-bw8R_eH98gw" outgoingStoryLinks="_LbPesPihEd-bw8R_eH98gw" incomingStoryLinks="_I94EYPihEd-bw8R_eH98gw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3YhsYPiiEd-bw8R_eH98gw" expressionString="self.source ='http://www.eclipse.org/emf/2002/GenModel'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardDetailsEntry" uuid="_pPwFwPigEd-bw8R_eH98gw" modifier="CREATE" incomingStoryLinks="_KPLUMPihEd-bw8R_eH98gw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry"/>
        <attributeAssignments uuid="_aQhQ4PihEd-bw8R_eH98gw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/key"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bfNRkPihEd-bw8R_eH98gw" expressionString="'body'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_fxmT0PihEd-bw8R_eH98gw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/value"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_g0dfAPihEd-bw8R_eH98gw">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_hYA4APihEd-bw8R_eH98gw" methodClassName="de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationHelpers" methodName="generateRullCallStoryDiagramCode">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              <parameters name="" uuid="_lrNLgPihEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nfYe4PihEd-bw8R_eH98gw" expressionString="projectName" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_ooFzYPihEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qmuqUPihEd-bw8R_eH98gw" expressionString="generatedRuleSetEClass.name" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_us3ZQPihEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_voEdQPihEd-bw8R_eH98gw" expressionString="ruleInfo.forwardTransformationActivityDiagram.name" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_zj-5MPihEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1d0vsPihEd-bw8R_eH98gw" expressionString="forwardOperation" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
              </parameters>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingDetailsEntry" uuid="_qXUKwPigEd-bw8R_eH98gw" modifier="CREATE" incomingStoryLinks="_KiG3QPihEd-bw8R_eH98gw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry"/>
        <attributeAssignments uuid="_cnHU0PihEd-bw8R_eH98gw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/key"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_daVhgPihEd-bw8R_eH98gw" expressionString="'body'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_8UYX0PihEd-bw8R_eH98gw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/value"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_-BqZAPihEd-bw8R_eH98gw">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_-q3HAPihEd-bw8R_eH98gw" methodClassName="de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationHelpers" methodName="generateRullCallStoryDiagramCode">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              <parameters name="" uuid="_CZ92APiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_FmfcYPiiEd-bw8R_eH98gw" expressionString="projectName" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_CveL8PiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Hc6ygPiiEd-bw8R_eH98gw" expressionString="generatedRuleSetEClass.name" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_C2FxUPiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JxSB0PiiEd-bw8R_eH98gw" expressionString="ruleInfo.mappingTransformationActivityDiagram.name" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_C8abwPiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_LNrHUPiiEd-bw8R_eH98gw" expressionString="mappingOperation" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
              </parameters>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseDetailsEntry" uuid="_rI28cPigEd-bw8R_eH98gw" modifier="CREATE" incomingStoryLinks="_LbPesPihEd-bw8R_eH98gw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry"/>
        <attributeAssignments uuid="_eEbAUPihEd-bw8R_eH98gw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/key"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_e3kUgPihEd-bw8R_eH98gw" expressionString="'body'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_M9Z_kPiiEd-bw8R_eH98gw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/value"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_N--KQPiiEd-bw8R_eH98gw">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_OhTbQPiiEd-bw8R_eH98gw" methodClassName="de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationHelpers" methodName="generateRullCallStoryDiagramCode">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              <parameters name="" uuid="_Re4msPiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_T2un0PiiEd-bw8R_eH98gw" expressionString="projectName" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_RlI_sPiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VaL-oPiiEd-bw8R_eH98gw" expressionString="generatedRuleSetEClass.name" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_Rre4QPiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XuRhIPiiEd-bw8R_eH98gw" expressionString="ruleInfo.reverseTransformationActivityDiagram.name" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_RxsN8PiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Zd99IPiiEd-bw8R_eH98gw" expressionString="reverseOperation" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
              </parameters>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_CKxY4PihEd-bw8R_eH98gw" source="_kPpo8PigEd-bw8R_eH98gw" target="_mYWa0PigEd-bw8R_eH98gw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EModelElement/eAnnotations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ia6hQPihEd-bw8R_eH98gw" source="_k9pXYPigEd-bw8R_eH98gw" target="_nfTb0PigEd-bw8R_eH98gw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EModelElement/eAnnotations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_I94EYPihEd-bw8R_eH98gw" source="_lqDKYPigEd-bw8R_eH98gw" target="_oNxrYPigEd-bw8R_eH98gw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EModelElement/eAnnotations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_KPLUMPihEd-bw8R_eH98gw" modifier="CREATE" source="_mYWa0PigEd-bw8R_eH98gw" target="_pPwFwPigEd-bw8R_eH98gw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation/details"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_KiG3QPihEd-bw8R_eH98gw" modifier="CREATE" source="_nfTb0PigEd-bw8R_eH98gw" target="_qXUKwPigEd-bw8R_eH98gw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation/details"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LbPesPihEd-bw8R_eH98gw" modifier="CREATE" source="_oNxrYPigEd-bw8R_eH98gw" target="_rI28cPigEd-bw8R_eH98gw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation/details"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create activity diagrams and activities" uuid="_Em02QCoSEeCKlbWGrdxQtQ" incoming="_upKacPigEd-bw8R_eH98gw" outgoing="_SRSbICoSEeCKlbWGrdxQtQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardActivityDiagram" uuid="_GTcwKioSEeCKlbWGrdxQtQ" modifier="CREATE" outgoingStoryLinks="_NFMJsCoSEeCKlbWGrdxQtQ" incomingStoryLinks="_MCvOMCoSEeCKlbWGrdxQtQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_GTcwKyoSEeCKlbWGrdxQtQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GTdXMCoSEeCKlbWGrdxQtQ" expressionString="tggRule.name.concat('_forwardTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingActivityDiagram" uuid="_GTdXMSoSEeCKlbWGrdxQtQ" modifier="CREATE" outgoingStoryLinks="_NUX1MCoSEeCKlbWGrdxQtQ" incomingStoryLinks="_MYVDsCoSEeCKlbWGrdxQtQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_GTdXMioSEeCKlbWGrdxQtQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GTdXMyoSEeCKlbWGrdxQtQ" expressionString="tggRule.name.concat('_mappingTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseActivityDiagram" uuid="_GTdXNCoSEeCKlbWGrdxQtQ" modifier="CREATE" outgoingStoryLinks="_Nio6sCoSEeCKlbWGrdxQtQ" incomingStoryLinks="_MykvsCoSEeCKlbWGrdxQtQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_GTdXNSoSEeCKlbWGrdxQtQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GTdXNioSEeCKlbWGrdxQtQ" expressionString="tggRule.name.concat('_reverseTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseActivity" uuid="_GTdXNyoSEeCKlbWGrdxQtQ" modifier="CREATE" outgoingStoryLinks="_St82ECofEeCrWJNIJyAZ9w" incomingStoryLinks="_Nio6sCoSEeCKlbWGrdxQtQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_GTdXOCoSEeCKlbWGrdxQtQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GTdXOSoSEeCKlbWGrdxQtQ" expressionString="tggRule.name.concat('_reverseTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingActivity" uuid="_GTdXOioSEeCKlbWGrdxQtQ" modifier="CREATE" outgoingStoryLinks="_SEwIECofEeCrWJNIJyAZ9w" incomingStoryLinks="_NUX1MCoSEeCKlbWGrdxQtQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_GTdXOyoSEeCKlbWGrdxQtQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GTdXPCoSEeCKlbWGrdxQtQ" expressionString="tggRule.name.concat('_mappingTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardActivity" uuid="_GTdXPSoSEeCKlbWGrdxQtQ" modifier="CREATE" outgoingStoryLinks="_RtthECofEeCrWJNIJyAZ9w" incomingStoryLinks="_NFMJsCoSEeCKlbWGrdxQtQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_GTdXPioSEeCKlbWGrdxQtQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GTdXPyoSEeCKlbWGrdxQtQ" expressionString="tggRule.name.concat('_forwardTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_GTdXQCoSEeCKlbWGrdxQtQ" outgoingStoryLinks="_MCvOMCoSEeCKlbWGrdxQtQ _MYVDsCoSEeCKlbWGrdxQtQ _MykvsCoSEeCKlbWGrdxQtQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardOperation" uuid="_MYmdkCofEeCrWJNIJyAZ9w" incomingStoryLinks="_RtthECofEeCrWJNIJyAZ9w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingOperation" uuid="_OASNECofEeCrWJNIJyAZ9w" incomingStoryLinks="_SEwIECofEeCrWJNIJyAZ9w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseOperation" uuid="_Ouq9ECofEeCrWJNIJyAZ9w" incomingStoryLinks="_St82ECofEeCrWJNIJyAZ9w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_MCvOMCoSEeCKlbWGrdxQtQ" modifier="CREATE" source="_GTdXQCoSEeCKlbWGrdxQtQ" target="_GTcwKioSEeCKlbWGrdxQtQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/forwardTransformationActivityDiagram"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_MYVDsCoSEeCKlbWGrdxQtQ" modifier="CREATE" source="_GTdXQCoSEeCKlbWGrdxQtQ" target="_GTdXMSoSEeCKlbWGrdxQtQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/mappingTransformationActivityDiagram"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_MykvsCoSEeCKlbWGrdxQtQ" modifier="CREATE" source="_GTdXQCoSEeCKlbWGrdxQtQ" target="_GTdXNCoSEeCKlbWGrdxQtQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/reverseTransformationActivityDiagram"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_NFMJsCoSEeCKlbWGrdxQtQ" modifier="CREATE" source="_GTcwKioSEeCKlbWGrdxQtQ" target="_GTdXPSoSEeCKlbWGrdxQtQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_NUX1MCoSEeCKlbWGrdxQtQ" modifier="CREATE" source="_GTdXMSoSEeCKlbWGrdxQtQ" target="_GTdXOioSEeCKlbWGrdxQtQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Nio6sCoSEeCKlbWGrdxQtQ" modifier="CREATE" source="_GTdXNCoSEeCKlbWGrdxQtQ" target="_GTdXNyoSEeCKlbWGrdxQtQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RtthECofEeCrWJNIJyAZ9w" modifier="CREATE" source="_GTdXPSoSEeCKlbWGrdxQtQ" target="_MYmdkCofEeCrWJNIJyAZ9w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_SEwIECofEeCrWJNIJyAZ9w" modifier="CREATE" source="_GTdXOioSEeCKlbWGrdxQtQ" target="_OASNECofEeCrWJNIJyAZ9w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_St82ECofEeCrWJNIJyAZ9w" modifier="CREATE" source="_GTdXNyoSEeCKlbWGrdxQtQ" target="_Ouq9ECofEeCrWJNIJyAZ9w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create find matches activities" uuid="_7Wm1wD5iEeCS07xt5ugfeQ" incoming="_0LuSQBfoEeCO-aRUjt8-yQ" outgoing="_DL3P4D5lEeCS07xt5ugfeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_KSOggD5jEeCS07xt5ugfeQ" outgoingStoryLinks="_FUK1oD5kEeCS07xt5ugfeQ _IxfRED5kEeCS07xt5ugfeQ _LwJakD5kEeCS07xt5ugfeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ffm" uuid="_OrtCMD5jEeCS07xt5ugfeQ" incomingStoryLinks="_FUK1oD5kEeCS07xt5ugfeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SXmFID5jEeCS07xt5ugfeQ" expressionString="'forwardFindMatches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mfm" uuid="_PLyQcD5jEeCS07xt5ugfeQ" incomingStoryLinks="_IxfRED5kEeCS07xt5ugfeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VXioED5jEeCS07xt5ugfeQ" expressionString="'mappingFindMatches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rfm" uuid="_PqPHAD5jEeCS07xt5ugfeQ" incomingStoryLinks="_LwJakD5kEeCS07xt5ugfeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XYelED5jEeCS07xt5ugfeQ" expressionString="'reverseFindMatches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardFindMatchesAD" uuid="_bvXFwD5jEeCS07xt5ugfeQ" modifier="CREATE" outgoingStoryLinks="_Rzs4cD5kEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_rCLIgD5jEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sRBhQD5jEeCS07xt5ugfeQ" expressionString="tggRule.name.concat('_forwardFindMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingFindMatchesAD" uuid="_cu2M0D5jEeCS07xt5ugfeQ" modifier="CREATE" outgoingStoryLinks="_SUM9cD5kEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_0eQBYD5jEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1m2oMD5jEeCS07xt5ugfeQ" expressionString="tggRule.name.concat('_mappingFindMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseFindMatchesAD" uuid="_dlKJwD5jEeCS07xt5ugfeQ" modifier="CREATE" outgoingStoryLinks="_S5IPcD5kEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_3a1uUD5jEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_4ZrVID5jEeCS07xt5ugfeQ" expressionString="tggRule.name.concat('_reverseFindMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardFindMatchesActivity" uuid="_ee51MD5jEeCS07xt5ugfeQ" modifier="CREATE" incomingStoryLinks="_Rzs4cD5kEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="__oUGED5jEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AHMacD5kEeCS07xt5ugfeQ" expressionString="tggRule.name.concat('_forwardFindMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingFindMatchesActivity" uuid="_lHddkD5jEeCS07xt5ugfeQ" modifier="CREATE" incomingStoryLinks="_SUM9cD5kEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_967-QD5jEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-VDucD5jEeCS07xt5ugfeQ" expressionString="tggRule.name.concat('_mappingFindMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseFindMatchesActivity" uuid="_mFCgID5jEeCS07xt5ugfeQ" modifier="CREATE" incomingStoryLinks="_S5IPcD5kEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_7ZCpQD5jEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_76kpAD5jEeCS07xt5ugfeQ" expressionString="tggRule.name.concat('_reverseFindMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_FUK1oD5kEeCS07xt5ugfeQ" modifier="CREATE" source="_KSOggD5jEeCS07xt5ugfeQ" target="_OrtCMD5jEeCS07xt5ugfeQ" valueTarget="_bvXFwD5jEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_IxfRED5kEeCS07xt5ugfeQ" modifier="CREATE" source="_KSOggD5jEeCS07xt5ugfeQ" target="_PLyQcD5jEeCS07xt5ugfeQ" valueTarget="_cu2M0D5jEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_LwJakD5kEeCS07xt5ugfeQ" modifier="CREATE" source="_KSOggD5jEeCS07xt5ugfeQ" target="_PqPHAD5jEeCS07xt5ugfeQ" valueTarget="_dlKJwD5jEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Rzs4cD5kEeCS07xt5ugfeQ" modifier="CREATE" source="_bvXFwD5jEeCS07xt5ugfeQ" target="_ee51MD5jEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_SUM9cD5kEeCS07xt5ugfeQ" modifier="CREATE" source="_cu2M0D5jEeCS07xt5ugfeQ" target="_lHddkD5jEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_S5IPcD5kEeCS07xt5ugfeQ" modifier="CREATE" source="_dlKJwD5jEeCS07xt5ugfeQ" target="_mFCgID5jEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create conflict check activities" uuid="_eeMOED5kEeCS07xt5ugfeQ" incoming="_DL3P4D5lEeCS07xt5ugfeQ" outgoing="_I7XpwEPeEeCIrIF3qEr5xA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_eeM1Ij5kEeCS07xt5ugfeQ" outgoingStoryLinks="_eeM1Jj5kEeCS07xt5ugfeQ _eeMOGD5kEeCS07xt5ugfeQ _eeMOET5kEeCS07xt5ugfeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="fcc" uuid="_eeMOJj5kEeCS07xt5ugfeQ" incomingStoryLinks="_eeM1Jj5kEeCS07xt5ugfeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_eeMOJz5kEeCS07xt5ugfeQ" expressionString="'forwardConflictCheck'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mcc" uuid="_eeMOHz5kEeCS07xt5ugfeQ" incomingStoryLinks="_eeMOGD5kEeCS07xt5ugfeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_eeMOID5kEeCS07xt5ugfeQ" expressionString="'mappingConflictCheck'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rcc" uuid="_eeMOEz5kEeCS07xt5ugfeQ" incomingStoryLinks="_eeMOET5kEeCS07xt5ugfeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_eeMOFD5kEeCS07xt5ugfeQ" expressionString="'reverseConflictCheck'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardConflictCheckAD" uuid="_eeMOHD5kEeCS07xt5ugfeQ" modifier="CREATE" outgoingStoryLinks="_eeMOEj5kEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_eeMOHT5kEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_eeMOHj5kEeCS07xt5ugfeQ" expressionString="tggRule.name.concat('_forwardConflictCheck')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingConflictCheckAD" uuid="_eeMOFT5kEeCS07xt5ugfeQ" modifier="CREATE" outgoingStoryLinks="_eeMOIT5kEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_eeMOFj5kEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_eeMOFz5kEeCS07xt5ugfeQ" expressionString="tggRule.name.concat('_mappingConflictCheck')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseConflictCheckAD" uuid="_eeMOGT5kEeCS07xt5ugfeQ" modifier="CREATE" outgoingStoryLinks="_eeMOIj5kEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_eeMOGj5kEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_eeMOGz5kEeCS07xt5ugfeQ" expressionString="tggRule.name.concat('_reverseConflictCheck')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardConflictCheckActivity" uuid="_eeM1Iz5kEeCS07xt5ugfeQ" modifier="CREATE" incomingStoryLinks="_eeMOEj5kEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_eeM1JD5kEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_eeM1JT5kEeCS07xt5ugfeQ" expressionString="tggRule.name.concat('_forwardConflictCheck')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingConflictCheckActivity" uuid="_eeMOIz5kEeCS07xt5ugfeQ" modifier="CREATE" incomingStoryLinks="_eeMOIT5kEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_eeMOJD5kEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_eeMOJT5kEeCS07xt5ugfeQ" expressionString="tggRule.name.concat('_mappingConflictCheck')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseConflictCheckActivity" uuid="_eeMOKD5kEeCS07xt5ugfeQ" modifier="CREATE" incomingStoryLinks="_eeMOIj5kEeCS07xt5ugfeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_eeM1ID5kEeCS07xt5ugfeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_eeM1IT5kEeCS07xt5ugfeQ" expressionString="tggRule.name.concat('_reverseConflictCheck')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_eeM1Jj5kEeCS07xt5ugfeQ" modifier="CREATE" source="_eeM1Ij5kEeCS07xt5ugfeQ" target="_eeMOJj5kEeCS07xt5ugfeQ" valueTarget="_eeMOHD5kEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_eeMOGD5kEeCS07xt5ugfeQ" modifier="CREATE" source="_eeM1Ij5kEeCS07xt5ugfeQ" target="_eeMOHz5kEeCS07xt5ugfeQ" valueTarget="_eeMOFT5kEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_eeMOET5kEeCS07xt5ugfeQ" modifier="CREATE" source="_eeM1Ij5kEeCS07xt5ugfeQ" target="_eeMOEz5kEeCS07xt5ugfeQ" valueTarget="_eeMOGT5kEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_eeMOEj5kEeCS07xt5ugfeQ" modifier="CREATE" source="_eeMOHD5kEeCS07xt5ugfeQ" target="_eeM1Iz5kEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_eeMOIT5kEeCS07xt5ugfeQ" modifier="CREATE" source="_eeMOFT5kEeCS07xt5ugfeQ" target="_eeMOIz5kEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_eeMOIj5kEeCS07xt5ugfeQ" modifier="CREATE" source="_eeMOGT5kEeCS07xt5ugfeQ" target="_eeMOKD5kEeCS07xt5ugfeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create find matches activities" uuid="_BIpUUEPeEeCIrIF3qEr5xA" incoming="_I7XpwEPeEeCIrIF3qEr5xA" outgoing="_LkqG0D5lEeCS07xt5ugfeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_BIpUa0PeEeCIrIF3qEr5xA" outgoingStoryLinks="_BIpUVkPeEeCIrIF3qEr5xA _BIpUb0PeEeCIrIF3qEr5xA _BIpUU0PeEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ffsm" uuid="_BIpUVEPeEeCIrIF3qEr5xA" incomingStoryLinks="_BIpUVkPeEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BIpUVUPeEeCIrIF3qEr5xA" expressionString="'forwardFindSourceMatches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mfsm" uuid="_BIpUaEPeEeCIrIF3qEr5xA" incomingStoryLinks="_BIpUb0PeEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BIpUaUPeEeCIrIF3qEr5xA" expressionString="'mappingFindSourceMatches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rfsm" uuid="_BIpUUUPeEeCIrIF3qEr5xA" incomingStoryLinks="_BIpUU0PeEeCIrIF3qEr5xA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BIpUUkPeEeCIrIF3qEr5xA" expressionString="'reverseFindSourceMatches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardFindSourceMatchesAD" uuid="_BIpUbEPeEeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_BIpUakPeEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_BIpUbUPeEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BIpUbkPeEeCIrIF3qEr5xA" expressionString="tggRule.name.concat('_forwardFindSourceMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingFindSourceMatchesAD" uuid="_BIpUV0PeEeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_BIpUYUPeEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_BIpUWEPeEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BIpUWUPeEeCIrIF3qEr5xA" expressionString="tggRule.name.concat('_mappingFindSourceMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseFindSourceMatchesAD" uuid="_BIpUZUPeEeCIrIF3qEr5xA" modifier="CREATE" outgoingStoryLinks="_BIpUXUPeEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_BIpUZkPeEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BIpUZ0PeEeCIrIF3qEr5xA" expressionString="tggRule.name.concat('_reverseFindSourceMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardFindSourceMatchesActivity" uuid="_BIpUWkPeEeCIrIF3qEr5xA" modifier="CREATE" incomingStoryLinks="_BIpUakPeEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_BIpUW0PeEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BIpUXEPeEeCIrIF3qEr5xA" expressionString="tggRule.name.concat('_forwardFindSourceMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingFindSourceMatchesActivity" uuid="_BIpUYkPeEeCIrIF3qEr5xA" modifier="CREATE" incomingStoryLinks="_BIpUYUPeEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_BIpUY0PeEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BIpUZEPeEeCIrIF3qEr5xA" expressionString="tggRule.name.concat('_mappingFindSourceMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseFindSourceMatchesActivity" uuid="_BIpUXkPeEeCIrIF3qEr5xA" modifier="CREATE" incomingStoryLinks="_BIpUXUPeEeCIrIF3qEr5xA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_BIpUX0PeEeCIrIF3qEr5xA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BIpUYEPeEeCIrIF3qEr5xA" expressionString="tggRule.name.concat('_reverseFindSourceMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_BIpUVkPeEeCIrIF3qEr5xA" modifier="CREATE" source="_BIpUa0PeEeCIrIF3qEr5xA" target="_BIpUVEPeEeCIrIF3qEr5xA" valueTarget="_BIpUbEPeEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_BIpUb0PeEeCIrIF3qEr5xA" modifier="CREATE" source="_BIpUa0PeEeCIrIF3qEr5xA" target="_BIpUaEPeEeCIrIF3qEr5xA" valueTarget="_BIpUV0PeEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_BIpUU0PeEeCIrIF3qEr5xA" modifier="CREATE" source="_BIpUa0PeEeCIrIF3qEr5xA" target="_BIpUUUPeEeCIrIF3qEr5xA" valueTarget="_BIpUZUPeEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BIpUakPeEeCIrIF3qEr5xA" modifier="CREATE" source="_BIpUbEPeEeCIrIF3qEr5xA" target="_BIpUWkPeEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BIpUYUPeEeCIrIF3qEr5xA" modifier="CREATE" source="_BIpUV0PeEeCIrIF3qEr5xA" target="_BIpUYkPeEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BIpUXUPeEeCIrIF3qEr5xA" modifier="CREATE" source="_BIpUZUPeEeCIrIF3qEr5xA" target="_BIpUXkPeEeCIrIF3qEr5xA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_HhaH8HegEd-UQsRE6oiJVQ" source="_c7k_8HefEd-UQsRE6oiJVQ" target="_soasQHefEd-UQsRE6oiJVQ"/>
    <edges uuid="_hxg_4He6Ed-4c678KwnPnw" source="_soasQHefEd-UQsRE6oiJVQ" target="_YRThYPcQEd-XR4VMarhb7w"/>
    <edges uuid="_LURZ8He-Ed-INsK5Dz3ubg" source="_JQQ60He-Ed-INsK5Dz3ubg" target="_99wEwHe9Ed-INsK5Dz3ubg" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_MQACYHe-Ed-INsK5Dz3ubg" expressionString="tggRule.isAxiom" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_ViuK8He-Ed-INsK5Dz3ubg" source="_bs6EAHe4Ed-4c678KwnPnw" target="_VlrrgPh3Ed-R0aK4IAlsBA" guardType="END"/>
    <edges uuid="_1xeU8He_Ed-INsK5Dz3ubg" source="_JQQ60He-Ed-INsK5Dz3ubg" target="_ONhpkHe_Ed-INsK5Dz3ubg" guardType="ELSE"/>
    <edges uuid="_6bInsHe_Ed-INsK5Dz3ubg" source="_ie7SMPigEd-bw8R_eH98gw" target="_bs6EAHe4Ed-4c678KwnPnw"/>
    <edges uuid="_Y4ZigHhcEd-NMp17Gm153Q" source="_bs6EAHe4Ed-4c678KwnPnw" target="_WmaRwHhcEd-NMp17Gm153Q" guardType="FOR_EACH"/>
    <edges uuid="_aNDBQHhcEd-NMp17Gm153Q" source="_WmaRwHhcEd-NMp17Gm153Q" target="_JQQ60He-Ed-INsK5Dz3ubg" guardType="END"/>
    <edges uuid="_fcNB0PcQEd-XR4VMarhb7w" source="_YRThYPcQEd-XR4VMarhb7w" target="_eUP7QPcQEd-XR4VMarhb7w" guardType="END"/>
    <edges uuid="_gHh2wPcQEd-XR4VMarhb7w" source="_eUP7QPcQEd-XR4VMarhb7w" target="_bs6EAHe4Ed-4c678KwnPnw"/>
    <edges uuid="_DrqqEPiEEd-StusXmfUQQA" source="_VlrrgPh3Ed-R0aK4IAlsBA" target="__itc8HeoEd-4c678KwnPnw"/>
    <edges uuid="_upKacPigEd-bw8R_eH98gw" source="_5NLm0He_Ed-INsK5Dz3ubg" target="_Em02QCoSEeCKlbWGrdxQtQ"/>
    <edges uuid="_yj5Y0BfoEeCO-aRUjt8-yQ" source="_99wEwHe9Ed-INsK5Dz3ubg" target="_5NLm0He_Ed-INsK5Dz3ubg"/>
    <edges uuid="_0LuSQBfoEeCO-aRUjt8-yQ" source="_ONhpkHe_Ed-INsK5Dz3ubg" target="_7Wm1wD5iEeCS07xt5ugfeQ"/>
    <edges uuid="_SRSbICoSEeCKlbWGrdxQtQ" source="_Em02QCoSEeCKlbWGrdxQtQ" target="_ie7SMPigEd-bw8R_eH98gw"/>
    <edges uuid="_DL3P4D5lEeCS07xt5ugfeQ" source="_7Wm1wD5iEeCS07xt5ugfeQ" target="_eeMOED5kEeCS07xt5ugfeQ"/>
    <edges uuid="_LkqG0D5lEeCS07xt5ugfeQ" source="_BIpUUEPeEeCIrIF3qEr5xA" target="_5NLm0He_Ed-INsK5Dz3ubg"/>
    <edges uuid="_I7XpwEPeEeCIrIF3qEr5xA" source="_eeMOED5kEeCS07xt5ugfeQ" target="_BIpUUEPeEeCIrIF3qEr5xA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
