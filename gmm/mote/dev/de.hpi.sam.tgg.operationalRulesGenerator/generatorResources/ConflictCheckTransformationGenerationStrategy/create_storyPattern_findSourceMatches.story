<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_findSourceMatches" uuid="_Jat2MBgCEeCaHaBYHoSzPw">
  <activities name="create_storyPattern_findSourceMatches" uuid="_J_YCcBgCEeCaHaBYHoSzPw">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_wuvgIBgLEeC-pfj64BNTVg" outgoing="_bl1sYBgOEeC-pfj64BNTVg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_xU9LkBgLEeC-pfj64BNTVg" incoming="_bl1sYBgOEeC-pfj64BNTVg" outgoing="_cCcv4BgOEeC-pfj64BNTVg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_x7iDYBgLEeC-pfj64BNTVg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_ye4BABgLEeC-pfj64BNTVg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_zXLiABgLEeC-pfj64BNTVg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_0RnJ8BgLEeC-pfj64BNTVg" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_3bSwUBgOEeC-pfj64BNTVg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create this and ruleSet objects" uuid="_lai9IBgNEeC-pfj64BNTVg" incoming="_cCcv4BgOEeC-pfj64BNTVg" outgoing="_rb4_EBgOEeC-pfj64BNTVg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisSpo" uuid="_nJfLMBgNEeC-pfj64BNTVg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_wYAwQBgNEeC-pfj64BNTVg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_wxXrcBgNEeC-pfj64BNTVg">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_rule_spo.story#_Kpsp0AwlEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_3Mn3oBgNEeC-pfj64BNTVg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_CjEygBgOEeC-pfj64BNTVg" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="isAxiom" uuid="_ENuWcBgOEeC-pfj64BNTVg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Fa7wcBgOEeC-pfj64BNTVg" expressionString="tggRule.isAxiom" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_no0ykBgNEeC-pfj64BNTVg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_NEkbUBgOEeC-pfj64BNTVg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_NaUB0BgOEeC-pfj64BNTVg">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_ruleSet_spo.story#_ZDP-wAwkEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_PIKqwBgOEeC-pfj64BNTVg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_RlCV0BgOEeC-pfj64BNTVg" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="bindingType" uuid="_WnSHsBgOEeC-pfj64BNTVg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_YQ3UoBgOEeC-pfj64BNTVg" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create this->ruleSet link" uuid="_dmhxwBgOEeC-pfj64BNTVg" incoming="_rb4_EBgOEeC-pfj64BNTVg" outgoing="_qaldoBgPEeC-pfj64BNTVg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_gbLqsBgOEeC-pfj64BNTVg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_hMRwcBgOEeC-pfj64BNTVg">
          <activity href="../common/create_rule_ruleSet_link.story#_qOEaEAwmEeCU3uEhqcP0Dg"/>
          <parameters name="ruleSpo" uuid="_lCDIIBgOEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nKI2ABgOEeC-pfj64BNTVg" expressionString="thisSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_oEovYBgOEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_p4vxUBgOEeC-pfj64BNTVg" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform parent model objects" uuid="_w22y8BgOEeC-pfj64BNTVg" incoming="_qaldoBgPEeC-pfj64BNTVg" outgoing="_rj4oABgPEeC-pfj64BNTVg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="__r_bMBgOEeC-pfj64BNTVg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_AQvHABgPEeC-pfj64BNTVg">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_EfeYUBgPEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Glr24BgPEeC-pfj64BNTVg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_H_8OYBgPEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KJacUBgPEeC-pfj64BNTVg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_MotJQBgPEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_O3wawBgPEeC-pfj64BNTVg" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_QKAFwBgPEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_R-81MBgPEeC-pfj64BNTVg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_VclZsBgPEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WxVmIBgPEeC-pfj64BNTVg" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_Yt_gEBgPEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ahWUEBgPEeC-pfj64BNTVg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_bapTkBgPEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dHqPABgPEeC-pfj64BNTVg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_fqZRgBgPEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hLDL8BgPEeC-pfj64BNTVg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_iAF9kBgPEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jpSI8BgPEeC-pfj64BNTVg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_khlC4BgPEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mJVq4BgPEeC-pfj64BNTVg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_nL3e4BgPEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_os-sUBgPEeC-pfj64BNTVg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_R4XM4CV0EeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_T2UuYCV0EeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_qxYNABgPEeC-pfj64BNTVg" incoming="_6O_yUBijEeCm4IbFa7I4cA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform parent model links" uuid="_rlTu4BgQEeC-pfj64BNTVg" incoming="_rj4oABgPEeC-pfj64BNTVg" outgoing="_VLvlgEP6EeCxT7Y0iFA1WA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_sstb0BgQEeC-pfj64BNTVg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_vgcHsBgQEeC-pfj64BNTVg">
          <activity href="../common/transform_modelLinks.story#_tdw5IAw6EeCAUcBgGC58lA"/>
          <parameters name="tggRule" uuid="_yjNVIBgQEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0ozIoBgQEeC-pfj64BNTVg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_1s64EBgQEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_30H1IBgQEeC-pfj64BNTVg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_4p0uEBgQEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6nAzgBgQEeC-pfj64BNTVg" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_7hmMcBgQEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_98loMBgQEeC-pfj64BNTVg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentLinks" uuid="_BVmLYBgREeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Cvsx4BgREeC-pfj64BNTVg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedLinks" uuid="_DsM2YBgREeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_F--xUBgREeC-pfj64BNTVg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_G5HeUBgREeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IOpG0BgREeC-pfj64BNTVg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_JHUbQBgREeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KaU7QBgREeC-pfj64BNTVg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->modelElements links" uuid="_Sc2KwBgSEeC-pfj64BNTVg" incoming="_03JQMFUlEeCvXfnk6nbowQ" outgoing="_I4PLgBgTEeC-pfj64BNTVg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_f9hhgBgSEeC-pfj64BNTVg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_gyrn4BgSEeC-pfj64BNTVg">
          <activity href="../common/create_ruleSet_modelElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_npiK8BgSEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rSpmsBgSEeC-pfj64BNTVg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_sH-sMBgSEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_uqjWoBgSEeC-pfj64BNTVg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_vmv5IBgSEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xstgEBgSEeC-pfj64BNTVg" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="modifier" uuid="_3mqWABgSEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5YLW8BgSEeC-pfj64BNTVg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_8x1aYBgSEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-fUP4BgSEeC-pfj64BNTVg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="__sOH4BgSEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BNfGUBgTEeC-pfj64BNTVg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentLinks" uuid="_CBXaUBgTEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_DnXu0BgTEeC-pfj64BNTVg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedLinks" uuid="_Eb2fwBgTEeC-pfj64BNTVg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GHMnQBgTEeC-pfj64BNTVg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_K3rjMBihEeCm4IbFa7I4cA" incoming="_I4PLgBgTEeC-pfj64BNTVg" outgoing="_VMT0sBihEeCm4IbFa7I4cA _513ykBihEeCm4IbFa7I4cA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_TwHjgBihEeCm4IbFa7I4cA" incoming="_VMT0sBihEeCm4IbFa7I4cA _6tGVgBihEeCm4IbFa7I4cA" outgoing="_V_uOoBihEeCm4IbFa7I4cA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_UEIrsBihEeCm4IbFa7I4cA" incoming="_V_uOoBihEeCm4IbFa7I4cA" outgoing="_Z7PpABihEeCm4IbFa7I4cA _-3SeABihEeCm4IbFa7I4cA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_UakOsBihEeCm4IbFa7I4cA" incoming="_Z7PpABihEeCm4IbFa7I4cA __b6OABihEeCm4IbFa7I4cA" outgoing="_cRdScBihEeCm4IbFa7I4cA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created model objects" uuid="_mr5RABihEeCm4IbFa7I4cA" incoming="_513ykBihEeCm4IbFa7I4cA" outgoing="_6JstgBihEeCm4IbFa7I4cA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_mr5RARihEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_mr5RAhihEeCm4IbFa7I4cA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_mr5RERihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mr5REhihEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_mr5RBxihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mr5RCBihEeCm4IbFa7I4cA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_mr5RDRihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mr5RDhihEeCm4IbFa7I4cA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_mr5RFRihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mr54EBihEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_mr5RCxihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mr5RDBihEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_mr5RExihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mr5RFBihEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_mr54ERihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mr54EhihEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_mr5RDxihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mr5REBihEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_mr5RBRihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mr5RBhihEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_mr5RCRihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mr5RChihEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_mr5RAxihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mr5RBBihEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_WowZwCV0EeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_YgDSkCV0EeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created model objects" uuid="_rn5Q8BihEeCm4IbFa7I4cA" incoming="_-3SeABihEeCm4IbFa7I4cA" outgoing="__HYvgBihEeCm4IbFa7I4cA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_rn5Q8RihEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_rn5Q8hihEeCm4IbFa7I4cA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_rn54BBihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rn54BRihEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_rn54CBihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rn54CRihEeCm4IbFa7I4cA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_rn5Q9RihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rn5Q9hihEeCm4IbFa7I4cA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_rn5Q_RihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rn5Q_hihEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_rn54BhihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rn54BxihEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_rn5Q9xihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rn5Q-BihEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_rn5Q-xihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rn5Q_BihEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_rn5Q-RihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rn5Q-hihEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_rn54AhihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rn54AxihEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_rn5Q8xihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rn5Q9BihEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_rn54ABihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rn54ARihEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_a7hPcCV0EeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_cqQCICV0EeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created model links" uuid="_wtIFYBihEeCm4IbFa7I4cA" incoming="_6JstgBihEeCm4IbFa7I4cA" outgoing="_7aNdsBiiEeCm4IbFa7I4cA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_wtIFYRihEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_wtIFYhihEeCm4IbFa7I4cA">
          <activity href="../common/transform_modelLinks.story#_tdw5IAw6EeCAUcBgGC58lA"/>
          <parameters name="tggRule" uuid="_wtIFZRihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wtIFZhihEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_wtIFZxihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wtIFaBihEeCm4IbFa7I4cA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_wtIFbxihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wtIFcBihEeCm4IbFa7I4cA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_wtIFbRihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wtIFbhihEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentLinks" uuid="_wtIFaRihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wtIFahihEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedLinks" uuid="_wtIFYxihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wtIFZBihEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_wtIFaxihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wtIFbBihEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_wtIFcRihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wtIFchihEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created model links" uuid="_1FRKYxihEeCm4IbFa7I4cA" incoming="__HYvgBihEeCm4IbFa7I4cA" outgoing="_9e_Y4BiiEeCm4IbFa7I4cA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_1FRKZBihEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_1FRKZRihEeCm4IbFa7I4cA">
          <activity href="../common/transform_modelLinks.story#_tdw5IAw6EeCAUcBgGC58lA"/>
          <parameters name="tggRule" uuid="_1FRKahihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1FRKaxihEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_1FRxZRihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1FRxZhihEeCm4IbFa7I4cA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_1FRxYRihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1FRxYhihEeCm4IbFa7I4cA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_1FRKbBihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1FRxYBihEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentLinks" uuid="_1FRxYxihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1FRxZBihEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedLinks" uuid="_1FRxZxihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1FRxaBihEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_1FRKaBihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1FRKaRihEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_1FRKZhihEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1FRKZxihEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->uncoveredModelElements links" uuid="_Xgx8QBiiEeCm4IbFa7I4cA" incoming="_7aNdsBiiEeCm4IbFa7I4cA" outgoing="_6tGVgBihEeCm4IbFa7I4cA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_cwPDsBiiEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_de9x8BiiEeCm4IbFa7I4cA">
          <activity href="../common/create_ruleSet_uncoveredElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_hc3x8BiiEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jytn8BiiEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_mlPZ8BiiEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_o3zScBiiEeCm4IbFa7I4cA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_pxiW0BiiEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r8LpcBiiEeCm4IbFa7I4cA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_s7H98BiiEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_uxOj8BiiEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_xu85UBiiEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_zGUU4BiiEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_06wG8BiiEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2U2GYBiiEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->uncoveredModelElements links" uuid="_3c9lDxiiEeCm4IbFa7I4cA" incoming="_9e_Y4BiiEeCm4IbFa7I4cA" outgoing="__b6OABihEeCm4IbFa7I4cA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_3c9lEBiiEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_3c9lERiiEeCm4IbFa7I4cA">
          <activity href="../common/create_ruleSet_uncoveredElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_3c-MFRiiEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3c-MFhiiEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_3c-MGRiiEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3c-MGhiiEeCm4IbFa7I4cA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_3c-MFxiiEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3c-MGBiiEeCm4IbFa7I4cA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_3c-MERiiEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3c-MEhiiEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_3c9lEhiiEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3c-MEBiiEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_3c-MExiiEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3c-MFBiiEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create direct assignment for the designated parent correspondence node" uuid="_Ps1CUBijEeCm4IbFa7I4cA" incoming="_cRdScBihEeCm4IbFa7I4cA" outgoing="_6O_yUBijEeCm4IbFa7I4cA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_Y3kTkBijEeCm4IbFa7I4cA" outgoingStoryLinks="_FMrLsC07EeChaYbFgphjsw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_aI0gEBijEeCm4IbFa7I4cA" incomingStoryLinks="_FMrLsC07EeChaYbFgphjsw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_cairEBijEeCm4IbFa7I4cA" outgoingStoryLinks="_opjloBijEeCm4IbFa7I4cA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_rAiEEBijEeCm4IbFa7I4cA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sOlyoBijEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_dXWRkBijEeCm4IbFa7I4cA" modifier="CREATE" outgoingStoryLinks="_ppfYoBijEeCm4IbFa7I4cA" incomingStoryLinks="_opjloBijEeCm4IbFa7I4cA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varRef" uuid="_dvsgIBijEeCm4IbFa7I4cA" modifier="CREATE" incomingStoryLinks="_ppfYoBijEeCm4IbFa7I4cA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_xuS8kBijEeCm4IbFa7I4cA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_zarl4BijEeCm4IbFa7I4cA" expressionString="'matchedModelElement'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_1c4HIBijEeCm4IbFa7I4cA">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2dsD4BijEeCm4IbFa7I4cA" expressionString="ecore::EObject" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_opjloBijEeCm4IbFa7I4cA" modifier="CREATE" source="_cairEBijEeCm4IbFa7I4cA" target="_dXWRkBijEeCm4IbFa7I4cA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ppfYoBijEeCm4IbFa7I4cA" modifier="CREATE" source="_dXWRkBijEeCm4IbFa7I4cA" target="_dvsgIBijEeCm4IbFa7I4cA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_FMrLsC07EeChaYbFgphjsw" source="_Y3kTkBijEeCm4IbFa7I4cA" target="_aI0gEBijEeCm4IbFa7I4cA" valueTarget="_cairEBijEeCm4IbFa7I4cA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create parent correspondence nodes" uuid="_wTAWoFUlEeCvXfnk6nbowQ" incoming="_VLvlgEP6EeCxT7Y0iFA1WA" outgoing="_0m3_kFUlEeCvXfnk6nbowQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_4oTNoFUlEeCvXfnk6nbowQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_68kWUFUlEeCvXfnk6nbowQ">
          <activity href="../common/transform_correspondenceNodes.story#_gUE7UBNnEeCRUJdROfc5HA"/>
          <parameters name="tggRule" uuid="_7xz8QFUlEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9vslQFUlEeCvXfnk6nbowQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_ANL7YFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_CMC0wFUmEeCvXfnk6nbowQ" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_DFQ7wFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_FGbiQFUmEeCvXfnk6nbowQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_HFsEQFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IwbHwFUmEeCvXfnk6nbowQ" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_LqDAQFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NU70wFUmEeCvXfnk6nbowQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_O0eU4FUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_QgyFsFUmEeCvXfnk6nbowQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="bindingType" uuid="_RXymMFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SiBksFUmEeCvXfnk6nbowQ" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create parent correspondence links" uuid="_yHB5AFUlEeCvXfnk6nbowQ" incoming="_0m3_kFUlEeCvXfnk6nbowQ" outgoing="_03JQMFUlEeCvXfnk6nbowQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_YTfNAFUmEeCvXfnk6nbowQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_cghOIFUmEeCvXfnk6nbowQ">
          <activity href="../common/create_corrNode_sourcesTargets_links.story#_uYYuIBNoEeCRUJdROfc5HA"/>
          <parameters name="tggRule" uuid="_f02FoFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hjZSIFUmEeCvXfnk6nbowQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_iQ8UoFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kMwhEFUmEeCvXfnk6nbowQ" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_k_zHkFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nB4UEFUmEeCvXfnk6nbowQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_psPBgFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rS75kFUmEeCvXfnk6nbowQ" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourcesLinks" uuid="_uQIqgFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_vyjfgFUmEeCvXfnk6nbowQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetsLinks" uuid="_wlD6gFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_yQ7mcFUmEeCvXfnk6nbowQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentLinks" uuid="_zEDFcFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0rRh8FUmEeCvXfnk6nbowQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedLinks" uuid="_1fOucFUmEeCvXfnk6nbowQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3TC1cFUmEeCvXfnk6nbowQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <edges uuid="_bl1sYBgOEeC-pfj64BNTVg" source="_wuvgIBgLEeC-pfj64BNTVg" target="_xU9LkBgLEeC-pfj64BNTVg"/>
    <edges uuid="_cCcv4BgOEeC-pfj64BNTVg" source="_xU9LkBgLEeC-pfj64BNTVg" target="_lai9IBgNEeC-pfj64BNTVg"/>
    <edges uuid="_rb4_EBgOEeC-pfj64BNTVg" source="_lai9IBgNEeC-pfj64BNTVg" target="_dmhxwBgOEeC-pfj64BNTVg"/>
    <edges uuid="_qaldoBgPEeC-pfj64BNTVg" source="_dmhxwBgOEeC-pfj64BNTVg" target="_w22y8BgOEeC-pfj64BNTVg"/>
    <edges uuid="_rj4oABgPEeC-pfj64BNTVg" source="_w22y8BgOEeC-pfj64BNTVg" target="_rlTu4BgQEeC-pfj64BNTVg"/>
    <edges uuid="_I4PLgBgTEeC-pfj64BNTVg" source="_Sc2KwBgSEeC-pfj64BNTVg" target="_K3rjMBihEeCm4IbFa7I4cA"/>
    <edges uuid="_VMT0sBihEeCm4IbFa7I4cA" source="_K3rjMBihEeCm4IbFa7I4cA" target="_TwHjgBihEeCm4IbFa7I4cA" guardType="ELSE"/>
    <edges uuid="_V_uOoBihEeCm4IbFa7I4cA" source="_TwHjgBihEeCm4IbFa7I4cA" target="_UEIrsBihEeCm4IbFa7I4cA"/>
    <edges uuid="_Z7PpABihEeCm4IbFa7I4cA" source="_UEIrsBihEeCm4IbFa7I4cA" target="_UakOsBihEeCm4IbFa7I4cA" guardType="ELSE"/>
    <edges uuid="_cRdScBihEeCm4IbFa7I4cA" source="_UakOsBihEeCm4IbFa7I4cA" target="_Ps1CUBijEeCm4IbFa7I4cA"/>
    <edges uuid="_513ykBihEeCm4IbFa7I4cA" source="_K3rjMBihEeCm4IbFa7I4cA" target="_mr5RABihEeCm4IbFa7I4cA" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7S-CsBihEeCm4IbFa7I4cA" expressionString="direction = mote::TransformationDirection::FORWARD or&#xA;direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_6JstgBihEeCm4IbFa7I4cA" source="_mr5RABihEeCm4IbFa7I4cA" target="_wtIFYBihEeCm4IbFa7I4cA"/>
    <edges uuid="_6tGVgBihEeCm4IbFa7I4cA" source="_Xgx8QBiiEeCm4IbFa7I4cA" target="_TwHjgBihEeCm4IbFa7I4cA"/>
    <edges uuid="_-3SeABihEeCm4IbFa7I4cA" source="_UEIrsBihEeCm4IbFa7I4cA" target="_rn5Q8BihEeCm4IbFa7I4cA" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AVYMoBiiEeCm4IbFa7I4cA" expressionString="direction = mote::TransformationDirection::MAPPING or&#xA;direction = mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="__HYvgBihEeCm4IbFa7I4cA" source="_rn5Q8BihEeCm4IbFa7I4cA" target="_1FRKYxihEeCm4IbFa7I4cA"/>
    <edges uuid="__b6OABihEeCm4IbFa7I4cA" source="_3c9lDxiiEeCm4IbFa7I4cA" target="_UakOsBihEeCm4IbFa7I4cA"/>
    <edges uuid="_7aNdsBiiEeCm4IbFa7I4cA" source="_wtIFYBihEeCm4IbFa7I4cA" target="_Xgx8QBiiEeCm4IbFa7I4cA"/>
    <edges uuid="_9e_Y4BiiEeCm4IbFa7I4cA" source="_1FRKYxihEeCm4IbFa7I4cA" target="_3c9lDxiiEeCm4IbFa7I4cA"/>
    <edges uuid="_6O_yUBijEeCm4IbFa7I4cA" source="_Ps1CUBijEeCm4IbFa7I4cA" target="_qxYNABgPEeC-pfj64BNTVg"/>
    <edges uuid="_VLvlgEP6EeCxT7Y0iFA1WA" source="_rlTu4BgQEeC-pfj64BNTVg" target="_wTAWoFUlEeCvXfnk6nbowQ"/>
    <edges uuid="_0m3_kFUlEeCvXfnk6nbowQ" source="_wTAWoFUlEeCvXfnk6nbowQ" target="_yHB5AFUlEeCvXfnk6nbowQ"/>
    <edges uuid="_03JQMFUlEeCvXfnk6nbowQ" source="_yHB5AFUlEeCvXfnk6nbowQ" target="_Sc2KwBgSEeC-pfj64BNTVg"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
