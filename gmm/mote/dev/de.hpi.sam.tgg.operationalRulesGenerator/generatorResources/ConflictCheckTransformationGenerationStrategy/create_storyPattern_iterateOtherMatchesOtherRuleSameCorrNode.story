<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_iterateOtherMatchesOtherRuleSameCorrNode" uuid="_qMs3ICotEeCjTcyrv1LVTA">
  <activities name="create_storyPattern_iterateOtherMatchesOtherRuleSameCorrNode" uuid="_r3JmwCotEeCjTcyrv1LVTA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_ug3bACotEeCjTcyrv1LVTA" outgoing="_2RYJ8CotEeCjTcyrv1LVTA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_vZKU8CotEeCjTcyrv1LVTA" incoming="_2RYJ8CotEeCjTcyrv1LVTA" outgoing="_Yndz4CovEeCjTcyrv1LVTA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfos" uuid="_ySLJcCotEeCjTcyrv1LVTA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_3fLZ0CotEeCjTcyrv1LVTA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_7ztUoCouEeCjTcyrv1LVTA" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_WG0uwCrBEeCPWq47ABciVw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="fmKey" uuid="_aD3MECrBEeCPWq47ABciVw" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_b0HowCrBEeCPWq47ABciVw" expressionString="if direction = mote::TransformationDirection::FORWARD then&#xA;&#x9;'forwardFindMatches'&#xA;else&#xA;&#x9;if direction = mote::TransformationDirection::MAPPING then&#xA;&#x9;&#x9;'mappingFindMatches'&#xA;&#x9;else&#xA;&#x9;&#x9;'reverseFindMatches'&#xA;&#x9;endif&#xA;endif" expressionLanguage="OCL"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create otherMatchStoreSpo" uuid="_50JtICotEeCjTcyrv1LVTA" incoming="_Yndz4CovEeCjTcyrv1LVTA" outgoing="_ZyAUYCovEeCjTcyrv1LVTA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="otherMatchStoreSpo" uuid="_9wyh0CotEeCjTcyrv1LVTA" outgoingStoryLinks="_esl0gCouEeCjTcyrv1LVTA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_YuDZgCouEeCjTcyrv1LVTA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_aDpTcCouEeCjTcyrv1LVTA" expressionString="'otherMatchStore'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_LtnpECouEeCjTcyrv1LVTA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_MMcTECouEeCjTcyrv1LVTA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_matchStore_spo.story#_X128ABgAEeCaHaBYHoSzPw"/>
            <parameters name="storyActionNode" uuid="_N0ICkCouEeCjTcyrv1LVTA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_P6OMYCouEeCjTcyrv1LVTA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="modifier" uuid="_QxHYICouEeCjTcyrv1LVTA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SZ23kCouEeCjTcyrv1LVTA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
            </parameters>
            <parameters name="bindingType" uuid="_T5hTgCouEeCjTcyrv1LVTA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_U_6T0CouEeCjTcyrv1LVTA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_bs7lcCouEeCjTcyrv1LVTA" modifier="CREATE" outgoingStoryLinks="_00QPMCouEeCjTcyrv1LVTA" incomingStoryLinks="_esl0gCouEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="callSDIAction" uuid="_woL3YCouEeCjTcyrv1LVTA" modifier="CREATE" outgoingStoryLinks="_9R7mICouEeCjTcyrv1LVTA _B0w4ICovEeCjTcyrv1LVTA _FudtkCovEeCjTcyrv1LVTA _EUP-0CreEeCoHJjELNAVvA" incomingStoryLinks="_00QPMCouEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchEClass" uuid="_2cO5oCouEeCjTcyrv1LVTA" incomingStoryLinks="_9R7mICouEeCjTcyrv1LVTA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_46BKsCouEeCjTcyrv1LVTA" expressionString="mote::rules::Match" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesActivity" uuid="__RQZkCouEeCjTcyrv1LVTA" incomingStoryLinks="_B0w4ICovEeCjTcyrv1LVTA _Erms8CowEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="caParameter" uuid="_C2cXkCovEeCjTcyrv1LVTA" modifier="CREATE" outgoingStoryLinks="_LAIbgCovEeCjTcyrv1LVTA _N4iMACovEeCjTcyrv1LVTA" incomingStoryLinks="_FudtkCovEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_QbChkC07EeChaYbFgphjsw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SRP1QC07EeChaYbFgphjsw" expressionString="'parentCorrNode'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggNodeEClass" uuid="_HKQWICovEeCjTcyrv1LVTA" incomingStoryLinks="_LAIbgCovEeCjTcyrv1LVTA _RR4tcCovEeCjTcyrv1LVTA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JicVgCovEeCjTcyrv1LVTA" expressionString="mote::TGGNode" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae2" uuid="_MIhm8CovEeCjTcyrv1LVTA" modifier="CREATE" outgoingStoryLinks="_WsLL4CovEeCjTcyrv1LVTA" incomingStoryLinks="_N4iMACovEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varRef" uuid="_PWXb8CovEeCjTcyrv1LVTA" modifier="CREATE" outgoingStoryLinks="_RR4tcCovEeCjTcyrv1LVTA" incomingStoryLinks="_WsLL4CovEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_TA4l4CovEeCjTcyrv1LVTA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_UWUu0CovEeCjTcyrv1LVTA" expressionString="'parentCorrNode'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfos" uuid="_9mC_gCovEeCjTcyrv1LVTA" outgoingStoryLinks="_1PVe4CrBEeCPWq47ABciVw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ad" uuid="_BE0McCowEeCjTcyrv1LVTA" outgoingStoryLinks="_Erms8CowEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="fmKey" uuid="_zQrZ0CrBEeCPWq47ABciVw" incomingStoryLinks="_1PVe4CrBEeCPWq47ABciVw" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisParameter" uuid="_B9N2ACreEeCoHJjELNAVvA" modifier="CREATE" outgoingStoryLinks="_G8PUwCreEeCoHJjELNAVvA _hcLuYC07EeChaYbFgphjsw" incomingStoryLinks="_EUP-0CreEeCoHJjELNAVvA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_UX3jgC07EeChaYbFgphjsw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VdZoMC07EeChaYbFgphjsw" expressionString="'this'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae3" uuid="_EyEjQCreEeCoHJjELNAVvA" modifier="CREATE" outgoingStoryLinks="_yzc8MC07EeChaYbFgphjsw" incomingStoryLinks="_G8PUwCreEeCoHJjELNAVvA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleEClass" uuid="_Yt8y8C07EeChaYbFgphjsw" incomingStoryLinks="_hcLuYC07EeChaYbFgphjsw _zRWZIC07EeChaYbFgphjsw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bdh-oC07EeChaYbFgphjsw" expressionString="mote::rules::TGGRule" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="otherRuleVarRef" uuid="_rvQgsC07EeChaYbFgphjsw" modifier="CREATE" outgoingStoryLinks="_zRWZIC07EeChaYbFgphjsw" incomingStoryLinks="_yzc8MC07EeChaYbFgphjsw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_ujmQkC07EeChaYbFgphjsw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_veUzcC07EeChaYbFgphjsw" expressionString="'otherRule'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_esl0gCouEeCjTcyrv1LVTA" modifier="CREATE" source="_9wyh0CotEeCjTcyrv1LVTA" target="_bs7lcCouEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_00QPMCouEeCjTcyrv1LVTA" modifier="CREATE" source="_bs7lcCouEeCjTcyrv1LVTA" target="_woL3YCouEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9R7mICouEeCjTcyrv1LVTA" modifier="CREATE" source="_woL3YCouEeCjTcyrv1LVTA" target="_2cO5oCouEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_B0w4ICovEeCjTcyrv1LVTA" modifier="CREATE" source="_woL3YCouEeCjTcyrv1LVTA" target="__RQZkCouEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/activity"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_FudtkCovEeCjTcyrv1LVTA" modifier="CREATE" source="_woL3YCouEeCjTcyrv1LVTA" target="_C2cXkCovEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LAIbgCovEeCjTcyrv1LVTA" modifier="CREATE" source="_C2cXkCovEeCjTcyrv1LVTA" target="_HKQWICovEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_N4iMACovEeCjTcyrv1LVTA" modifier="CREATE" source="_C2cXkCovEeCjTcyrv1LVTA" target="_MIhm8CovEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RR4tcCovEeCjTcyrv1LVTA" modifier="CREATE" source="_PWXb8CovEeCjTcyrv1LVTA" target="_HKQWICovEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WsLL4CovEeCjTcyrv1LVTA" modifier="CREATE" source="_MIhm8CovEeCjTcyrv1LVTA" target="_PWXb8CovEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Erms8CowEeCjTcyrv1LVTA" source="_BE0McCowEeCjTcyrv1LVTA" target="__RQZkCouEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_1PVe4CrBEeCPWq47ABciVw" source="_9mC_gCovEeCjTcyrv1LVTA" target="_zQrZ0CrBEeCPWq47ABciVw" valueTarget="_BE0McCowEeCjTcyrv1LVTA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_EUP-0CreEeCoHJjELNAVvA" modifier="CREATE" source="_woL3YCouEeCjTcyrv1LVTA" target="_B9N2ACreEeCoHJjELNAVvA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallStoryDiagramInterpreterAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_G8PUwCreEeCoHJjELNAVvA" modifier="CREATE" source="_B9N2ACreEeCoHJjELNAVvA" target="_EyEjQCreEeCoHJjELNAVvA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hcLuYC07EeChaYbFgphjsw" modifier="CREATE" source="_B9N2ACreEeCoHJjELNAVvA" target="_Yt8y8C07EeChaYbFgphjsw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_yzc8MC07EeChaYbFgphjsw" modifier="CREATE" source="_EyEjQCreEeCoHJjELNAVvA" target="_rvQgsC07EeChaYbFgphjsw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zRWZIC07EeChaYbFgphjsw" modifier="CREATE" source="_rvQgsC07EeChaYbFgphjsw" target="_Yt8y8C07EeChaYbFgphjsw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_Y-lTYCovEeCjTcyrv1LVTA" incoming="_j7ZGgD54EeCWcJGYTNCOIA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create otherMatchSpo" uuid="_8xNioCrGEeCUpeLLM-x8VQ" incoming="_ZyAUYCovEeCjTcyrv1LVTA" outgoing="_sY1wkCrHEeCUpeLLM-x8VQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="otherMatchSpo" uuid="_CiVzwCrHEeCUpeLLM-x8VQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_XLrPQCrHEeCUpeLLM-x8VQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Yx5mMCrHEeCUpeLLM-x8VQ" expressionString="'otherMatch'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_GJtjECrHEeCUpeLLM-x8VQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_GhIkkCrHEeCUpeLLM-x8VQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_match_spo.story#_6d114Bi_EeCuQ-PF2tTDdw"/>
            <parameters name="storyActionNode" uuid="_KCUzYCrHEeCUpeLLM-x8VQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_M-UDYCrHEeCUpeLLM-x8VQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="modifier" uuid="_N4Du0CrHEeCUpeLLM-x8VQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ppcz8CrHEeCUpeLLM-x8VQ" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
            </parameters>
            <parameters name="bindingType" uuid="_R71tUCrHEeCUpeLLM-x8VQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_THXFQCrHEeCUpeLLM-x8VQ" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create otherMatchStore->matches link" uuid="_cJJO0CrHEeCUpeLLM-x8VQ" incoming="_sY1wkCrHEeCUpeLLM-x8VQ" outgoing="_tAveECrHEeCUpeLLM-x8VQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_dx8_sCrHEeCUpeLLM-x8VQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_fbPRsCrHEeCUpeLLM-x8VQ">
          <activity href="../common/create_matchStore_matches_link.story#_57M7gBjCEeCuQ-PF2tTDdw"/>
          <parameters name="matchStoreSpo" uuid="_gZZjECrHEeCUpeLLM-x8VQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ipXakCrHEeCUpeLLM-x8VQ" expressionString="otherMatchStoreSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="matchSpo" uuid="_jwxukCrHEeCUpeLLM-x8VQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_l3zsgCrHEeCUpeLLM-x8VQ" expressionString="otherMatchSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="modifier" uuid="_nUH5gCrHEeCUpeLLM-x8VQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qSehACrHEeCUpeLLM-x8VQ" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="add constraint" uuid="_4hp8gCrHEeCUpeLLM-x8VQ" incoming="_tAveECrHEeCUpeLLM-x8VQ" outgoing="_ImTE0CrIEeCUpeLLM-x8VQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_59wHECrHEeCUpeLLM-x8VQ" outgoingStoryLinks="_HYP9UCrIEeCUpeLLM-x8VQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="constraint" uuid="_80wJYCrHEeCUpeLLM-x8VQ" modifier="CREATE" incomingStoryLinks="_HYP9UCrIEeCUpeLLM-x8VQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_-ytq4CrHEeCUpeLLM-x8VQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ABl40CrIEeCUpeLLM-x8VQ" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_A792YCrIEeCUpeLLM-x8VQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_B1LWUCrIEeCUpeLLM-x8VQ" expressionString="'not (match.createdElements.value->asSet()->intersection(otherMatch.createdElements.value->asSet())->isEmpty() or \n match.createdElements.value->includesAll(otherMatch.createdElements.value) and \n otherMatch.createdElements.value->includesAll(match.createdElements.value) and \n match.applicationContext.value->includesAll(otherMatch.applicationContext.value) and \n otherMatch.applicationContext.value->includesAll(match.applicationContext.value))'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_HYP9UCrIEeCUpeLLM-x8VQ" modifier="CREATE" source="_59wHECrHEeCUpeLLM-x8VQ" target="_80wJYCrHEeCUpeLLM-x8VQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/constraints"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create spo for rule" uuid="_8u__gD53EeCWcJGYTNCOIA" incoming="_ImTE0CrIEeCUpeLLM-x8VQ" outgoing="_j7ZGgD54EeCWcJGYTNCOIA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSpo" uuid="__H_3MD53EeCWcJGYTNCOIA" modifier="CREATE" outgoingStoryLinks="_TucWwD54EeCWcJGYTNCOIA _a-00ID54EeCWcJGYTNCOIA" incomingStoryLinks="_xT_fUD54EeCWcJGYTNCOIA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_NPevQD54EeCWcJGYTNCOIA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OO6L8D54EeCWcJGYTNCOIA" expressionString="'r'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_Y6nrQD55EeCWcJGYTNCOIA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Z7gggD55EeCWcJGYTNCOIA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eClassifier" uuid="_QEX4wD54EeCWcJGYTNCOIA" incomingStoryLinks="_TAcBQD54EeCWcJGYTNCOIA _TucWwD54EeCWcJGYTNCOIA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfos" uuid="_RUA7sD54EeCWcJGYTNCOIA" outgoingStoryLinks="_TAcBQD54EeCWcJGYTNCOIA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="directAssignmentExpression" uuid="_VwbcoD54EeCWcJGYTNCOIA" modifier="CREATE" outgoingStoryLinks="_b3vZID54EeCWcJGYTNCOIA" incomingStoryLinks="_a-00ID54EeCWcJGYTNCOIA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varRef" uuid="_XYpXoD54EeCWcJGYTNCOIA" modifier="CREATE" incomingStoryLinks="_b3vZID54EeCWcJGYTNCOIA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_chPCED54EeCWcJGYTNCOIA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dt3NQD54EeCWcJGYTNCOIA" expressionString="'otherRule'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_f1j5kD54EeCWcJGYTNCOIA">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gwQAMD54EeCWcJGYTNCOIA" expressionString="mote::rules::TGGRule" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_vpDAcD54EeCWcJGYTNCOIA" outgoingStoryLinks="_xT_fUD54EeCWcJGYTNCOIA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_TAcBQD54EeCWcJGYTNCOIA" source="_RUA7sD54EeCWcJGYTNCOIA" target="_QEX4wD54EeCWcJGYTNCOIA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/ruleEClass"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_TucWwD54EeCWcJGYTNCOIA" modifier="CREATE" source="__H_3MD53EeCWcJGYTNCOIA" target="_QEX4wD54EeCWcJGYTNCOIA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_a-00ID54EeCWcJGYTNCOIA" modifier="CREATE" source="__H_3MD53EeCWcJGYTNCOIA" target="_VwbcoD54EeCWcJGYTNCOIA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_b3vZID54EeCWcJGYTNCOIA" modifier="CREATE" source="_VwbcoD54EeCWcJGYTNCOIA" target="_XYpXoD54EeCWcJGYTNCOIA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xT_fUD54EeCWcJGYTNCOIA" modifier="CREATE" source="_vpDAcD54EeCWcJGYTNCOIA" target="__H_3MD53EeCWcJGYTNCOIA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_2RYJ8CotEeCjTcyrv1LVTA" source="_ug3bACotEeCjTcyrv1LVTA" target="_vZKU8CotEeCjTcyrv1LVTA"/>
    <edges uuid="_Yndz4CovEeCjTcyrv1LVTA" source="_vZKU8CotEeCjTcyrv1LVTA" target="_50JtICotEeCjTcyrv1LVTA"/>
    <edges uuid="_ZyAUYCovEeCjTcyrv1LVTA" source="_50JtICotEeCjTcyrv1LVTA" target="_8xNioCrGEeCUpeLLM-x8VQ"/>
    <edges uuid="_sY1wkCrHEeCUpeLLM-x8VQ" source="_8xNioCrGEeCUpeLLM-x8VQ" target="_cJJO0CrHEeCUpeLLM-x8VQ"/>
    <edges uuid="_tAveECrHEeCUpeLLM-x8VQ" source="_cJJO0CrHEeCUpeLLM-x8VQ" target="_4hp8gCrHEeCUpeLLM-x8VQ"/>
    <edges uuid="_ImTE0CrIEeCUpeLLM-x8VQ" source="_4hp8gCrHEeCUpeLLM-x8VQ" target="_8u__gD53EeCWcJGYTNCOIA"/>
    <edges uuid="_j7ZGgD54EeCWcJGYTNCOIA" source="_8u__gD53EeCWcJGYTNCOIA" target="_Y-lTYCovEeCjTcyrv1LVTA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
