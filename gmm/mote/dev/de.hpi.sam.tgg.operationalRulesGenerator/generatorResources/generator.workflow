<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:operationalRulesGenerator.workflowComponents="http://operationalRulesGenerator/workflowComponents/1.0" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" name="Rule Generator Workflow" description="This workflow generates the operational rules plugin from a TGG file.">
  <components xsi:type="workflow.components:DirectoryCleaner" name="directoryCleaner">
    <directoryURIs>../src-gen</directoryURIs>
    <directoryURIs>../plugin.xml</directoryURIs>
    <directoryURIs>../model/story</directoryURIs>
  </components>
  <components xsi:type="operationalRulesGenerator.workflowComponents:ManifestLoaderComponent" name="manifestLoaderComponent" modelSlot="manifest" projectName="${projectName}"/>
  <components xsi:type="operationalRulesGenerator.workflowComponents:GenerationStrategyComponent" name="generationStrategyComponent" tggFileURI="${tggFileURI}" projectName="${projectName}" correspondenceMetamodelURI="${correspondenceMetamodelURI}" javaBasePackage="${javaBasePackage}" generationStrategy="${generationStrategy}"/>
  <components xsi:type="operationalRulesGenerator.workflowComponents:EMFCodeGenerationComponent" name="eMFCodeGenerationComponent" projectName="${projectName}" tggFileURI="${tggFileURI}" correspondenceMetamodelURI="${correspondenceMetamodelURI}" javaBasePackage="${javaBasePackage}"/>
  <components xsi:type="operationalRulesGenerator.workflowComponents:ManifestGeneratorComponent" name="manifestGeneratorComponent" manifestSlot="manifest" projectName="${projectName}" tggFileURI="${tggFileURI}" correspondenceMetamodelURI="${correspondenceMetamodelURI}"/>
  <properties name="projectName"/>
  <properties name="tggFileURI"/>
  <properties name="correspondenceMetamodelURI"/>
  <properties name="javaBasePackage"/>
  <properties name="generationStrategy" defaultValue="SynchronizationStrategy"/>
</workflow:Workflow>
