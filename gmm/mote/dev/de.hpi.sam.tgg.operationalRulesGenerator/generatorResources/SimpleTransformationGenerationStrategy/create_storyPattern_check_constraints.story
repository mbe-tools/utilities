<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_check_constraints" uuid="_1uKwMPbdEd-JnYtEWuN3-g">
  <activities name="create_storyPattern_check_constraints" uuid="_2MC_APbdEd-JnYtEWuN3-g">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_6_mNkPbdEd-JnYtEWuN3-g" outgoing="_aLC88PbeEd-JnYtEWuN3-g"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_7kg4gPbdEd-JnYtEWuN3-g" incoming="_aLC88PbeEd-JnYtEWuN3-g" outgoing="_ElWKYPbfEd-JnYtEWuN3-g">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_9i20gPbdEd-JnYtEWuN3-g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_RLqAEPbeEd-JnYtEWuN3-g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createSourceConstraints" uuid="_Znn9cBRDEeC9udBuGs2jiA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createTargetConstraints" uuid="_cHzbMBRDEeC9udBuGs2jiA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createParentNodesConstraints" uuid="_f81zEBRDEeC9udBuGs2jiA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createCreatedNodesConstraints" uuid="_hCyugBRDEeC9udBuGs2jiA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="transform model objects" uuid="_oCdYsPbeEd-JnYtEWuN3-g" incoming="_ElWKYPbfEd-JnYtEWuN3-g _78q2IA4QEeCVYKbk8vub6g" outgoing="_GDl_IPbhEd-JnYtEWuN3-g _7HLYkA4QEeCVYKbk8vub6g" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_rl8GgPbeEd-JnYtEWuN3-g" outgoingStoryLinks="_jX4_oPbhEd-JnYtEWuN3-g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelDomain" uuid="_tD22APbeEd-JnYtEWuN3-g" outgoingStoryLinks="_z8mPcPbeEd-JnYtEWuN3-g" incomingStoryLinks="_jX4_oPbhEd-JnYtEWuN3-g">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sm1bcPbhEd-JnYtEWuN3-g" expressionString="createSourceConstraints and self.oclIsKindOf(tgg::SourceModelDomain) or&#xA;createTargetConstraints and self.oclIsKindOf(tgg::TargetModelDomain)" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_x1upkPbeEd-JnYtEWuN3-g" outgoingStoryLinks="_CAirUPbfEd-JnYtEWuN3-g" incomingStoryLinks="_z8mPcPbeEd-JnYtEWuN3-g">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_zijsYARTEeCJf73xSIA6Gg" expressionString="self.attributeAssignments->notEmpty() or self.constraintExpressions->notEmpty()" expressionLanguage="OCL"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wQfnwBRDEeC9udBuGs2jiA" expressionString="createParentNodesConstraints and self.modifier = tgg::TGGModifierEnumeration::NONE or&#xA;createCreatedNodesConstraints and self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_0r6MgPbeEd-JnYtEWuN3-g" outgoingStoryLinks="_Ddi00PbfEd-JnYtEWuN3-g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_2FLskPbeEd-JnYtEWuN3-g" modifier="CREATE" outgoingStoryLinks="_CXH_UPbfEd-JnYtEWuN3-g" incomingStoryLinks="_Ddi00PbfEd-JnYtEWuN3-g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_4OLZYPbeEd-JnYtEWuN3-g">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5k24cPbeEd-JnYtEWuN3-g" expressionString="modelObject.name" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_6xlxUPbeEd-JnYtEWuN3-g">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9Rp6UPbeEd-JnYtEWuN3-g" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="classifier" uuid="_ApkRUPbfEd-JnYtEWuN3-g" incomingStoryLinks="_CAirUPbfEd-JnYtEWuN3-g _CXH_UPbfEd-JnYtEWuN3-g">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClassifier"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_z8mPcPbeEd-JnYtEWuN3-g" source="_tD22APbeEd-JnYtEWuN3-g" target="_x1upkPbeEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_CAirUPbfEd-JnYtEWuN3-g" source="_x1upkPbeEd-JnYtEWuN3-g" target="_ApkRUPbfEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_CXH_UPbfEd-JnYtEWuN3-g" modifier="CREATE" source="_2FLskPbeEd-JnYtEWuN3-g" target="_ApkRUPbfEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ddi00PbfEd-JnYtEWuN3-g" modifier="CREATE" source="_0r6MgPbeEd-JnYtEWuN3-g" target="_2FLskPbeEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_jX4_oPbhEd-JnYtEWuN3-g" source="_rl8GgPbeEd-JnYtEWuN3-g" target="_tD22APbeEd-JnYtEWuN3-g"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_E5lDEPbhEd-JnYtEWuN3-g" incoming="_PIKxcCfKEeCkKvPvoL1v4g _Pw1T8CfKEeCkKvPvoL1v4g"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform constraints" uuid="_rNlpsA4QEeCVYKbk8vub6g" incoming="_7HLYkA4QEeCVYKbk8vub6g" outgoing="_78q2IA4QEeCVYKbk8vub6g">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_s3SycA4QEeCVYKbk8vub6g">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_u6L3QA4QEeCVYKbk8vub6g">
          <activity href="../common/transform_constraints.story#_3AxAwAw3EeCzq8Eugced9g"/>
          <parameters name="modelObject" uuid="_1J8lIA4QEeCVYKbk8vub6g">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3BOP0A4QEeCVYKbk8vub6g" expressionString="modelObject" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
          </parameters>
          <parameters name="storyPatternObject" uuid="_4GDw8A4QEeCVYKbk8vub6g">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6Dl0oA4QEeCVYKbk8vub6g" expressionString="spo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_fMjF8CVXEeCVE9iKiEiTJw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gmEdoCVXEeCVE9iKiEiTJw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_qvBQwC6xEeCxraw6f_ZwmA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sKkowC6xEeCxraw6f_ZwmA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="make sure there is at least one element contained in the storyActionNode" uuid="_2KSXkCfJEeCkKvPvoL1v4g" incoming="_K6Au8CfKEeCkKvPvoL1v4g" outgoing="_PIKxcCfKEeCkKvPvoL1v4g">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_2KSXlyfJEeCkKvPvoL1v4g" outgoingStoryLinks="_2KSXmyfJEeCkKvPvoL1v4g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelDomain" uuid="_2KSXkSfJEeCkKvPvoL1v4g" outgoingStoryLinks="_2KSXmifJEeCkKvPvoL1v4g" incomingStoryLinks="_2KSXmyfJEeCkKvPvoL1v4g">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2KSXkifJEeCkKvPvoL1v4g" expressionString="createSourceConstraints and self.oclIsKindOf(tgg::SourceModelDomain) or&#xA;createTargetConstraints and self.oclIsKindOf(tgg::TargetModelDomain)" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_2KSXkyfJEeCkKvPvoL1v4g" outgoingStoryLinks="_2KSXmCfJEeCkKvPvoL1v4g" incomingStoryLinks="_2KSXmifJEeCkKvPvoL1v4g">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2KSXlCfJEeCkKvPvoL1v4g" expressionString="createParentNodesConstraints and self.modifier = tgg::TGGModifierEnumeration::NONE or&#xA;createCreatedNodesConstraints and self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_2KSXnSfJEeCkKvPvoL1v4g" outgoingStoryLinks="_2KSXmSfJEeCkKvPvoL1v4g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_2KSXnifJEeCkKvPvoL1v4g" modifier="CREATE" outgoingStoryLinks="_2KSXlifJEeCkKvPvoL1v4g" incomingStoryLinks="_2KSXmSfJEeCkKvPvoL1v4g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_2KSXnyfJEeCkKvPvoL1v4g">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2KSXoCfJEeCkKvPvoL1v4g" expressionString="modelObject.name" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_2KSXoSfJEeCkKvPvoL1v4g">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2KSXoifJEeCkKvPvoL1v4g" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="classifier" uuid="_2KSXnCfJEeCkKvPvoL1v4g" incomingStoryLinks="_2KSXmCfJEeCkKvPvoL1v4g _2KSXlifJEeCkKvPvoL1v4g">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClassifier"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2KSXmifJEeCkKvPvoL1v4g" source="_2KSXkSfJEeCkKvPvoL1v4g" target="_2KSXkyfJEeCkKvPvoL1v4g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2KSXmCfJEeCkKvPvoL1v4g" source="_2KSXkyfJEeCkKvPvoL1v4g" target="_2KSXnCfJEeCkKvPvoL1v4g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2KSXlifJEeCkKvPvoL1v4g" modifier="CREATE" source="_2KSXnifJEeCkKvPvoL1v4g" target="_2KSXnCfJEeCkKvPvoL1v4g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2KSXmSfJEeCkKvPvoL1v4g" modifier="CREATE" source="_2KSXnSfJEeCkKvPvoL1v4g" target="_2KSXnifJEeCkKvPvoL1v4g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_2KSXmyfJEeCkKvPvoL1v4g" source="_2KSXlyfJEeCkKvPvoL1v4g" target="_2KSXkSfJEeCkKvPvoL1v4g"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_JHpZcCfKEeCkKvPvoL1v4g" incoming="_GDl_IPbhEd-JnYtEWuN3-g" outgoing="_K6Au8CfKEeCkKvPvoL1v4g _Pw1T8CfKEeCkKvPvoL1v4g"/>
    <edges uuid="_aLC88PbeEd-JnYtEWuN3-g" source="_6_mNkPbdEd-JnYtEWuN3-g" target="_7kg4gPbdEd-JnYtEWuN3-g"/>
    <edges uuid="_ElWKYPbfEd-JnYtEWuN3-g" source="_7kg4gPbdEd-JnYtEWuN3-g" target="_oCdYsPbeEd-JnYtEWuN3-g"/>
    <edges uuid="_GDl_IPbhEd-JnYtEWuN3-g" source="_oCdYsPbeEd-JnYtEWuN3-g" target="_JHpZcCfKEeCkKvPvoL1v4g" guardType="END"/>
    <edges uuid="_7HLYkA4QEeCVYKbk8vub6g" source="_oCdYsPbeEd-JnYtEWuN3-g" target="_rNlpsA4QEeCVYKbk8vub6g" guardType="FOR_EACH"/>
    <edges uuid="_78q2IA4QEeCVYKbk8vub6g" source="_rNlpsA4QEeCVYKbk8vub6g" target="_oCdYsPbeEd-JnYtEWuN3-g"/>
    <edges uuid="_K6Au8CfKEeCkKvPvoL1v4g" source="_JHpZcCfKEeCkKvPvoL1v4g" target="_2KSXkCfJEeCkKvPvoL1v4g" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_L9MDMCfKEeCkKvPvoL1v4g" expressionString="storyActionNode.storyPatternObjects->isEmpty()" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_PIKxcCfKEeCkKvPvoL1v4g" source="_2KSXkCfJEeCkKvPvoL1v4g" target="_E5lDEPbhEd-JnYtEWuN3-g"/>
    <edges uuid="_Pw1T8CfKEeCkKvPvoL1v4g" source="_JHpZcCfKEeCkKvPvoL1v4g" target="_E5lDEPbhEd-JnYtEWuN3-g" guardType="ELSE"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
