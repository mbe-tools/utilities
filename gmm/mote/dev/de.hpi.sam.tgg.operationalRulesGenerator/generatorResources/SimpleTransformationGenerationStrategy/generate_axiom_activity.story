<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="generate_axiom_activity" uuid="_f2pxUXUrEd-AwpZ_Fdb-iA">
  <activities name="generate_axiom_activity" uuid="_NTSQ0HhtEd-F3I0fuD-eTQ">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_Uli9gHhtEd-F3I0fuD-eTQ" outgoing="_YBSEkHhtEd-F3I0fuD-eTQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_y-wI4HhzEd-rKZ33ykOJHw" incoming="_B5dh4CkyEeCPsqbG4H8XYA">
      <outParameterValues xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r8ghwPPQEd-NkIb-Q3PMvQ" expressionString="activity" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_-FElwHrTEd-C3vhV8ZLu-w" incoming="_YBSEkHhtEd-F3I0fuD-eTQ" outgoing="_-QcCwAxTEeCp2NlniWjADQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_CJFNwHrUEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_EqQKAHrUEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create node to match LHS" uuid="_V8DpAHrUEd-C3vhV8ZLu-w" incoming="_-QcCwAxTEeCp2NlniWjADQ" outgoing="_7q-scHrUEd-C3vhV8ZLu-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_qtqPsHrUEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_1cg_EHrUEd-C3vhV8ZLu-w _18v-UHrUEd-C3vhV8ZLu-w _72HakPZIEd-o55Ll0oEtLw _ZG-skPZJEd-o55Ll0oEtLw _7bSqgAxnEeCczauSUZKYlg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_sWkuQHrUEd-C3vhV8ZLu-w" modifier="CREATE" incomingStoryLinks="_z51EUHrUEd-C3vhV8ZLu-w _7bSqgAxnEeCczauSUZKYlg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_u5nSwHrUEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_z51EUHrUEd-C3vhV8ZLu-w _0uWRgHrUEd-C3vhV8ZLu-w" incomingStoryLinks="_1cg_EHrUEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchLHSSAN" uuid="_vnAkQHrUEd-C3vhV8ZLu-w" modifier="CREATE" incomingStoryLinks="_0uWRgHrUEd-C3vhV8ZLu-w _18v-UHrUEd-C3vhV8ZLu-w _8TfTEPZIEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_RSUBQHrVEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SfmTwHrVEd-C3vhV8ZLu-w" expressionString="'match LHS'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleNotAppliedFinalNode" uuid="_4cOGkPZIEd-o55Ll0oEtLw" modifier="CREATE" outgoingStoryLinks="_hwt7wPcIEd-_yp3IZC6S-Q" incomingStoryLinks="_9HXnEPZIEd-o55Ll0oEtLw _ZG-skPZJEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_6SZlEPZIEd-o55Ll0oEtLw" modifier="CREATE" outgoingStoryLinks="_8TfTEPZIEd-o55Ll0oEtLw _9HXnEPZIEd-o55Ll0oEtLw" incomingStoryLinks="_72HakPZIEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="__WIksPZIEd-o55Ll0oEtLw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Al3uQPZJEd-o55Ll0oEtLw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleNotAppliedExpression" uuid="_XH-bcPcIEd-_yp3IZC6S-Q" modifier="CREATE" incomingStoryLinks="_hwt7wPcIEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_b8ZzwPcIEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dBpkkPcIEd-_yp3IZC6S-Q" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_eFSL0PcIEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_e_w3EPcIEd-_yp3IZC6S-Q" expressionString="'mote::rules::TransformationResult::RULE_NOT_APPLIED'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_z51EUHrUEd-C3vhV8ZLu-w" modifier="CREATE" source="_u5nSwHrUEd-C3vhV8ZLu-w" target="_sWkuQHrUEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0uWRgHrUEd-C3vhV8ZLu-w" modifier="CREATE" source="_u5nSwHrUEd-C3vhV8ZLu-w" target="_vnAkQHrUEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1cg_EHrUEd-C3vhV8ZLu-w" modifier="CREATE" source="_qtqPsHrUEd-C3vhV8ZLu-w" target="_u5nSwHrUEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_18v-UHrUEd-C3vhV8ZLu-w" modifier="CREATE" source="_qtqPsHrUEd-C3vhV8ZLu-w" target="_vnAkQHrUEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_72HakPZIEd-o55Ll0oEtLw" modifier="CREATE" source="_qtqPsHrUEd-C3vhV8ZLu-w" target="_6SZlEPZIEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8TfTEPZIEd-o55Ll0oEtLw" modifier="CREATE" source="_6SZlEPZIEd-o55Ll0oEtLw" target="_vnAkQHrUEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9HXnEPZIEd-o55Ll0oEtLw" modifier="CREATE" source="_6SZlEPZIEd-o55Ll0oEtLw" target="_4cOGkPZIEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZG-skPZJEd-o55Ll0oEtLw" modifier="CREATE" source="_qtqPsHrUEd-C3vhV8ZLu-w" target="_4cOGkPZIEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hwt7wPcIEd-_yp3IZC6S-Q" modifier="CREATE" source="_4cOGkPZIEd-o55Ll0oEtLw" target="_XH-bcPcIEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7bSqgAxnEeCczauSUZKYlg" modifier="CREATE" source="_qtqPsHrUEd-C3vhV8ZLu-w" target="_sWkuQHrUEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of match LHS" uuid="_5km10HrUEd-C3vhV8ZLu-w" incoming="_7q-scHrUEd-C3vhV8ZLu-w" outgoing="__k1S4HrUEd-C3vhV8ZLu-w">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_LJAIEPPTEd-cf4nuhI1Eog">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_LbxTEPPTEd-cf4nuhI1Eog">
          <activity href="create_storyPattern_match_LHS_axiom.story#_QBQAwIBhEd-Ol7HhHmHbiA"/>
          <parameters name="tggRule" uuid="_i2svoPPXEd-cAK4OkgyYOw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lKPfkPPXEd-cAK4OkgyYOw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_pEa2IPPXEd-cAK4OkgyYOw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_quDtcPPXEd-cAK4OkgyYOw" expressionString="matchLHSSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="direction" uuid="_uIP8YPPXEd-cAK4OkgyYOw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_05IaUPPXEd-cAK4OkgyYOw" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create node to evaluate rule variables" uuid="_9w4pAHrUEd-C3vhV8ZLu-w" incoming="__k1S4HrUEd-C3vhV8ZLu-w" outgoing="_IzVAAPbaEd-JnYtEWuN3-g">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_AOO1MHrVEd-C3vhV8ZLu-w" outgoingStoryLinks="_O9oB0HrVEd-C3vhV8ZLu-w _PsddwHrVEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchLHSSAN" uuid="_BvM4sHrVEd-C3vhV8ZLu-w" incomingStoryLinks="_E-0PUHrVEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_DuKfwHrVEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_E-0PUHrVEd-C3vhV8ZLu-w _QLXnUHrVEd-C3vhV8ZLu-w" incomingStoryLinks="_O9oB0HrVEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_HF6T0PZJEd-o55Ll0oEtLw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ISLSoPZJEd-o55Ll0oEtLw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evaluateRuleVariablesEAN" uuid="_GCerwHrVEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_fp3RUPZ2Ed-i2JdGlqLvVQ" incomingStoryLinks="_PsddwHrVEd-C3vhV8ZLu-w _QLXnUHrVEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_UGW2MHrVEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VGdBQHrVEd-C3vhV8ZLu-w" expressionString="'evaluate rule variables'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_I6O3sPZ2Ed-i2JdGlqLvVQ" incomingStoryLinks="_fp3RUPZ2Ed-i2JdGlqLvVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_UBp4EPZ2Ed-i2JdGlqLvVQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_UYhf8PZ2Ed-i2JdGlqLvVQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
            <activity href="../common/create_evaluateRuleVariables_expression.story#_uabxYfZwEd-i2JdGlqLvVQ"/>
            <parameters name="tggRule" uuid="_YM2GIPZ2Ed-i2JdGlqLvVQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZzOOEPZ2Ed-i2JdGlqLvVQ" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_brCEQPZ2Ed-i2JdGlqLvVQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_c_A0oPZ2Ed-i2JdGlqLvVQ" expressionString="direction" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_E-0PUHrVEd-C3vhV8ZLu-w" modifier="CREATE" source="_DuKfwHrVEd-C3vhV8ZLu-w" target="_BvM4sHrVEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_O9oB0HrVEd-C3vhV8ZLu-w" modifier="CREATE" source="_AOO1MHrVEd-C3vhV8ZLu-w" target="_DuKfwHrVEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_PsddwHrVEd-C3vhV8ZLu-w" modifier="CREATE" source="_AOO1MHrVEd-C3vhV8ZLu-w" target="_GCerwHrVEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_QLXnUHrVEd-C3vhV8ZLu-w" modifier="CREATE" source="_DuKfwHrVEd-C3vhV8ZLu-w" target="_GCerwHrVEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fp3RUPZ2Ed-i2JdGlqLvVQ" modifier="CREATE" source="_GCerwHrVEd-C3vhV8ZLu-w" target="_I6O3sPZ2Ed-i2JdGlqLvVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create node to create elements" uuid="__J2KkHrVEd-C3vhV8ZLu-w" incoming="_KeSF8PbaEd-JnYtEWuN3-g" outgoing="_wDYFQHrWEd-C3vhV8ZLu-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkLHSConstraintsSAN" uuid="_B8BXQHrWEd-C3vhV8ZLu-w" incomingStoryLinks="_MB8eYHrWEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_EPlVUHrWEd-C3vhV8ZLu-w" outgoingStoryLinks="_KsjYwHrWEd-C3vhV8ZLu-w _LNNOwHrWEd-C3vhV8ZLu-w _itlI0HrWEd-C3vhV8ZLu-w _jQ5RQHrWEd-C3vhV8ZLu-w" incomingStoryLinks="_FamSMCfFEeCkHOR1O7gzpA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createElementsSAN" uuid="_F3yCMHrWEd-C3vhV8ZLu-w" modifier="CREATE" incomingStoryLinks="_LNNOwHrWEd-C3vhV8ZLu-w _NATkEHrWEd-C3vhV8ZLu-w _fcekcHrWEd-C3vhV8ZLu-w _Dy6isCfFEeCkHOR1O7gzpA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_N8rFsHrWEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_O8Kz0HrWEd-C3vhV8ZLu-w" expressionString="'create elements'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_JbJbQHrWEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_MB8eYHrWEd-C3vhV8ZLu-w _NATkEHrWEd-C3vhV8ZLu-w" incomingStoryLinks="_KsjYwHrWEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_g0V2QPcMEd-CI4Kg3lq6HA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hqeNAPcMEd-CI4Kg3lq6HA" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleNotAppliedFinalNode" uuid="_Wp24QHrWEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_8MZwYPcIEd-_yp3IZC6S-Q" incomingStoryLinks="_hmioQHrWEd-C3vhV8ZLu-w _jQ5RQHrWEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_eQRQAHrWEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_fcekcHrWEd-C3vhV8ZLu-w _hmioQHrWEd-C3vhV8ZLu-w" incomingStoryLinks="_itlI0HrWEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_klqEwHrWEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lj7D0HrWEd-C3vhV8ZLu-w" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleNotAppliedExpression" uuid="_094UMPcIEd-_yp3IZC6S-Q" modifier="CREATE" incomingStoryLinks="_8MZwYPcIEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_2__V4PcIEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_33gMsPcIEd-_yp3IZC6S-Q" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_4usTYPcIEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5uqioPcIEd-_yp3IZC6S-Q" expressionString="'mote::rules::TransformationResult::RULE_NOT_APPLIED'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="outgoingEdge" uuid="__hLnwCfEEeCkHOR1O7gzpA" modifier="CREATE" outgoingStoryLinks="_Dy6isCfFEeCkHOR1O7gzpA _FamSMCfFEeCkHOR1O7gzpA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_PsxFkCfFEeCkHOR1O7gzpA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Q_Ea8CfFEeCkHOR1O7gzpA" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_KsjYwHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_EPlVUHrWEd-C3vhV8ZLu-w" target="_JbJbQHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LNNOwHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_EPlVUHrWEd-C3vhV8ZLu-w" target="_F3yCMHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_MB8eYHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_JbJbQHrWEd-C3vhV8ZLu-w" target="_B8BXQHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_NATkEHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_JbJbQHrWEd-C3vhV8ZLu-w" target="_F3yCMHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fcekcHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_eQRQAHrWEd-C3vhV8ZLu-w" target="_F3yCMHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hmioQHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_eQRQAHrWEd-C3vhV8ZLu-w" target="_Wp24QHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_itlI0HrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_EPlVUHrWEd-C3vhV8ZLu-w" target="_eQRQAHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jQ5RQHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_EPlVUHrWEd-C3vhV8ZLu-w" target="_Wp24QHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8MZwYPcIEd-_yp3IZC6S-Q" modifier="CREATE" source="_Wp24QHrWEd-C3vhV8ZLu-w" target="_094UMPcIEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Dy6isCfFEeCkHOR1O7gzpA" modifier="CREATE" source="__hLnwCfEEeCkHOR1O7gzpA" target="_F3yCMHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_FamSMCfFEeCkHOR1O7gzpA" modifier="CREATE" source="__hLnwCfEEeCkHOR1O7gzpA" target="_EPlVUHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/activity"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of create elements" uuid="_RZBQwHrWEd-C3vhV8ZLu-w" incoming="_wDYFQHrWEd-C3vhV8ZLu-w" outgoing="_SNfgcCfGEeCkHOR1O7gzpA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_tD9FMPb1Ed-JnYtEWuN3-g">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_tYZrMPb1Ed-JnYtEWuN3-g">
          <activity href="create_storyPattern_create_RHS_axiom.story#_emvIUPbiEd-JnYtEWuN3-g"/>
          <parameters name="tggRule" uuid="_zAkNkPb1Ed-JnYtEWuN3-g">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0peFEPb1Ed-JnYtEWuN3-g" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_1i3LMPb1Ed-JnYtEWuN3-g">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3CqKAPb1Ed-JnYtEWuN3-g" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_3-YLYPb1Ed-JnYtEWuN3-g">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5ty6kPb1Ed-JnYtEWuN3-g" expressionString="createElementsSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create node to add modification tag to queue" uuid="_UcPUsHrXEd-C3vhV8ZLu-w" incoming="__CSgcCkxEeCPsqbG4H8XYA" outgoing="_B5dh4CkyEeCPsqbG4H8XYA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_alxewHrXEd-C3vhV8ZLu-w" outgoingStoryLinks="_u_P5UHrXEd-C3vhV8ZLu-w _yjXgUHrXEd-C3vhV8ZLu-w _Bg_9QHrYEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="addModTagEAN" uuid="_i4UjwHrXEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_IaGegPcYEd-PZbH3UvWGmw" incomingStoryLinks="_tsiUQHrXEd-C3vhV8ZLu-w _u_P5UHrXEd-C3vhV8ZLu-w _xsNO0HrXEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_ltXBwHrYEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_m9ZtUHrYEd-C3vhV8ZLu-w" expressionString="'add modification tag to queue'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="outgoingEdge" uuid="_me3zsHrXEd-C3vhV8ZLu-w" outgoingStoryLinks="_tsiUQHrXEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_vtoCQHrXEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_xsNO0HrXEd-C3vhV8ZLu-w _0_rxwHrXEd-C3vhV8ZLu-w" incomingStoryLinks="_yjXgUHrXEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleAppliedFinalNode" uuid="_zQ0cMHrXEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_GUL4sPcJEd-_yp3IZC6S-Q" incomingStoryLinks="_0_rxwHrXEd-C3vhV8ZLu-w _Bg_9QHrYEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleAppliedExpression" uuid="_-XaPYPcIEd-_yp3IZC6S-Q" modifier="CREATE" incomingStoryLinks="_GUL4sPcJEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_A0QFQPcJEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_B7GYkPcJEd-_yp3IZC6S-Q" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_C4w6sPcJEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_D4M-cPcJEd-_yp3IZC6S-Q" expressionString="'mote::rules::TransformationResult::RULE_APPLIED'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="addModTagExpression" uuid="_91ZJoPcXEd-PZbH3UvWGmw" incomingStoryLinks="_IaGegPcYEd-PZbH3UvWGmw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_CtKJ0PcYEd-PZbH3UvWGmw">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_DEWh0PcYEd-PZbH3UvWGmw">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
            <activity href="../common/create_addModifcationTagToQueue_expression.story#_lXQIIPcJEd-_yp3IZC6S-Q"/>
            <parameters name="tggRule" uuid="_E9z90PcYEd-PZbH3UvWGmw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GhmEwPcYEd-PZbH3UvWGmw" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tsiUQHrXEd-C3vhV8ZLu-w" modifier="CREATE" source="_me3zsHrXEd-C3vhV8ZLu-w" target="_i4UjwHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_u_P5UHrXEd-C3vhV8ZLu-w" modifier="CREATE" source="_alxewHrXEd-C3vhV8ZLu-w" target="_i4UjwHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xsNO0HrXEd-C3vhV8ZLu-w" modifier="CREATE" source="_vtoCQHrXEd-C3vhV8ZLu-w" target="_i4UjwHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_yjXgUHrXEd-C3vhV8ZLu-w" modifier="CREATE" source="_alxewHrXEd-C3vhV8ZLu-w" target="_vtoCQHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0_rxwHrXEd-C3vhV8ZLu-w" modifier="CREATE" source="_vtoCQHrXEd-C3vhV8ZLu-w" target="_zQ0cMHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Bg_9QHrYEd-C3vhV8ZLu-w" modifier="CREATE" source="_alxewHrXEd-C3vhV8ZLu-w" target="_zQ0cMHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GUL4sPcJEd-_yp3IZC6S-Q" modifier="CREATE" source="_zQ0cMHrXEd-C3vhV8ZLu-w" target="_-XaPYPcIEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_IaGegPcYEd-PZbH3UvWGmw" modifier="CREATE" source="_i4UjwHrXEd-C3vhV8ZLu-w" target="_91ZJoPcXEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create node to check constraints of LHS" uuid="_HJ0EgPbaEd-JnYtEWuN3-g" incoming="_IzVAAPbaEd-JnYtEWuN3-g" outgoing="_JX7scPbbEd-JnYtEWuN3-g">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evaluateRuleVariablesEAN" uuid="_LGKlUPbaEd-JnYtEWuN3-g" incomingStoryLinks="_jNaSIPbaEd-JnYtEWuN3-g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_PLOWMPbaEd-JnYtEWuN3-g" modifier="CREATE" outgoingStoryLinks="_jNaSIPbaEd-JnYtEWuN3-g _kSTdoPbaEd-JnYtEWuN3-g" incomingStoryLinks="_4QLhUPbaEd-JnYtEWuN3-g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_PxIfoPbaEd-JnYtEWuN3-g" outgoingStoryLinks="_4QLhUPbaEd-JnYtEWuN3-g _4uM6EPbaEd-JnYtEWuN3-g _5B__0PbaEd-JnYtEWuN3-g _5XKXgPbaEd-JnYtEWuN3-g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkLHSConstraintsSAN" uuid="_Q0uqoPbaEd-JnYtEWuN3-g" modifier="CREATE" incomingStoryLinks="_kSTdoPbaEd-JnYtEWuN3-g _x2ur8PbaEd-JnYtEWuN3-g _4uM6EPbaEd-JnYtEWuN3-g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_vAv18PbdEd-JnYtEWuN3-g">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wcCvQPbdEd-JnYtEWuN3-g" expressionString="'check LHS constaints'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleNotAppliedFinalNode" uuid="_WCfI8PbaEd-JnYtEWuN3-g" modifier="CREATE" outgoingStoryLinks="_unI0kPcIEd-_yp3IZC6S-Q" incomingStoryLinks="_zGbZQPbaEd-JnYtEWuN3-g _5XKXgPbaEd-JnYtEWuN3-g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_o2QJ8PbaEd-JnYtEWuN3-g" modifier="CREATE" outgoingStoryLinks="_x2ur8PbaEd-JnYtEWuN3-g _zGbZQPbaEd-JnYtEWuN3-g" incomingStoryLinks="_5B__0PbaEd-JnYtEWuN3-g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_qig3cPbaEd-JnYtEWuN3-g">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r0WSwPbaEd-JnYtEWuN3-g" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleNotAppliedExpression" uuid="_n8zDgPcIEd-_yp3IZC6S-Q" modifier="CREATE" incomingStoryLinks="_unI0kPcIEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_paz5oPcIEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qP8x4PcIEd-_yp3IZC6S-Q" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_rGmtEPcIEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r8vq4PcIEd-_yp3IZC6S-Q" expressionString="'mote::rules::TransformationResult::RULE_NOT_APPLIED'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jNaSIPbaEd-JnYtEWuN3-g" modifier="CREATE" source="_PLOWMPbaEd-JnYtEWuN3-g" target="_LGKlUPbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kSTdoPbaEd-JnYtEWuN3-g" modifier="CREATE" source="_PLOWMPbaEd-JnYtEWuN3-g" target="_Q0uqoPbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_x2ur8PbaEd-JnYtEWuN3-g" modifier="CREATE" source="_o2QJ8PbaEd-JnYtEWuN3-g" target="_Q0uqoPbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zGbZQPbaEd-JnYtEWuN3-g" modifier="CREATE" source="_o2QJ8PbaEd-JnYtEWuN3-g" target="_WCfI8PbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4QLhUPbaEd-JnYtEWuN3-g" modifier="CREATE" source="_PxIfoPbaEd-JnYtEWuN3-g" target="_PLOWMPbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4uM6EPbaEd-JnYtEWuN3-g" modifier="CREATE" source="_PxIfoPbaEd-JnYtEWuN3-g" target="_Q0uqoPbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5B__0PbaEd-JnYtEWuN3-g" modifier="CREATE" source="_PxIfoPbaEd-JnYtEWuN3-g" target="_o2QJ8PbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5XKXgPbaEd-JnYtEWuN3-g" modifier="CREATE" source="_PxIfoPbaEd-JnYtEWuN3-g" target="_WCfI8PbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_unI0kPcIEd-_yp3IZC6S-Q" modifier="CREATE" source="_WCfI8PbaEd-JnYtEWuN3-g" target="_n8zDgPcIEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of checkLHSConstraintsSAN" uuid="_DcPxIPbbEd-JnYtEWuN3-g" incoming="_JX7scPbbEd-JnYtEWuN3-g" outgoing="_KeSF8PbaEd-JnYtEWuN3-g">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_OLCnYPbhEd-JnYtEWuN3-g">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_O26O4PbhEd-JnYtEWuN3-g">
          <activity href="create_storyPattern_check_constraints.story#_2MC_APbdEd-JnYtEWuN3-g"/>
          <parameters name="tggRule" uuid="_ToE30PbhEd-JnYtEWuN3-g">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VPGgAPbhEd-JnYtEWuN3-g" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_ZXkxQPbhEd-JnYtEWuN3-g">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_doMR4PbhEd-JnYtEWuN3-g" expressionString="checkLHSConstraintsSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="createSourceConstraints" uuid="_G35ScBREEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IyTJoBREEeC9udBuGs2jiA" expressionString="direction = mote::TransformationDirection::FORWARD or&#xA;direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetConstraints" uuid="_N7OyUBREEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_PzSgIBREEeC9udBuGs2jiA" expressionString="direction = mote::TransformationDirection::MAPPING or&#xA;direction = mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentNodesConstraints" uuid="_Ss_4MBREEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_V-GckBREEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedNodesConstraints" uuid="_XJf4sBREEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZMo1IBREEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create nodes to execute post-creation expressions" uuid="_d8rg4CfFEeCkHOR1O7gzpA" incoming="_SNfgcCfGEeCkHOR1O7gzpA" outgoing="__CSgcCkxEeCPsqbG4H8XYA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_jx3LkCfFEeCkHOR1O7gzpA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:VariableDeclarationAction" uuid="_k8CfsCfFEeCkHOR1O7gzpA" variableName="outgoingEdge">
          <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
          <valueAssignment xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_nBJLACfFEeCkHOR1O7gzpA">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_nYKj4CfFEeCkHOR1O7gzpA">
              <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
              <activity href="../common/create_executePostCreation_eans.story#_aZJ3ICfCEeCkHOR1O7gzpA"/>
              <parameters name="tggRule" uuid="_9fu38CfFEeCkHOR1O7gzpA">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_DarMYCfGEeCkHOR1O7gzpA" expressionString="tggRule" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
              </parameters>
              <parameters name="incomingEdge" uuid="_LIDu0CfGEeCkHOR1O7gzpA">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_M_S9QCfGEeCkHOR1O7gzpA" expressionString="outgoingEdge" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
              </parameters>
              <parameters name="direction" uuid="_OJEowCfGEeCkHOR1O7gzpA">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_QJ7tQCfGEeCkHOR1O7gzpA" expressionString="direction" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
              </parameters>
            </callActions>
          </valueAssignment>
        </callActions>
      </expression>
    </nodes>
    <edges uuid="_YBSEkHhtEd-F3I0fuD-eTQ" source="_Uli9gHhtEd-F3I0fuD-eTQ" target="_-FElwHrTEd-C3vhV8ZLu-w"/>
    <edges uuid="_7q-scHrUEd-C3vhV8ZLu-w" source="_V8DpAHrUEd-C3vhV8ZLu-w" target="_5km10HrUEd-C3vhV8ZLu-w"/>
    <edges uuid="__k1S4HrUEd-C3vhV8ZLu-w" source="_5km10HrUEd-C3vhV8ZLu-w" target="_9w4pAHrUEd-C3vhV8ZLu-w"/>
    <edges uuid="_wDYFQHrWEd-C3vhV8ZLu-w" source="__J2KkHrVEd-C3vhV8ZLu-w" target="_RZBQwHrWEd-C3vhV8ZLu-w"/>
    <edges uuid="_IzVAAPbaEd-JnYtEWuN3-g" source="_9w4pAHrUEd-C3vhV8ZLu-w" target="_HJ0EgPbaEd-JnYtEWuN3-g"/>
    <edges uuid="_KeSF8PbaEd-JnYtEWuN3-g" source="_DcPxIPbbEd-JnYtEWuN3-g" target="__J2KkHrVEd-C3vhV8ZLu-w"/>
    <edges uuid="_JX7scPbbEd-JnYtEWuN3-g" source="_HJ0EgPbaEd-JnYtEWuN3-g" target="_DcPxIPbbEd-JnYtEWuN3-g"/>
    <edges uuid="_-QcCwAxTEeCp2NlniWjADQ" source="_-FElwHrTEd-C3vhV8ZLu-w" target="_V8DpAHrUEd-C3vhV8ZLu-w"/>
    <edges uuid="_SNfgcCfGEeCkHOR1O7gzpA" source="_RZBQwHrWEd-C3vhV8ZLu-w" target="_d8rg4CfFEeCkHOR1O7gzpA"/>
    <edges uuid="__CSgcCkxEeCPsqbG4H8XYA" source="_d8rg4CfFEeCkHOR1O7gzpA" target="_UcPUsHrXEd-C3vhV8ZLu-w"/>
    <edges uuid="_B5dh4CkyEeCPsqbG4H8XYA" source="_UcPUsHrXEd-C3vhV8ZLu-w" target="_y-wI4HhzEd-rKZ33ykOJHw"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
