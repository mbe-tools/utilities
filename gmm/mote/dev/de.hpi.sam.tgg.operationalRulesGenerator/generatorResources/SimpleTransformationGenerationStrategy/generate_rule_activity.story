<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="generate_rule_activity" uuid="_EMzjAPlaEd-fSKxQcXgteA">
  <activities name="generate_rule_activity" uuid="_Ej074PlaEd-fSKxQcXgteA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_Klm4IPlaEd-fSKxQcXgteA" outgoing="_VueqAPlaEd-fSKxQcXgteA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_L70dIPlaEd-fSKxQcXgteA" incoming="_VueqAPlaEd-fSKxQcXgteA" outgoing="_dCXuYPlaEd-fSKxQcXgteA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_M0pikPlaEd-fSKxQcXgteA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_O8_IEPlaEd-fSKxQcXgteA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_YAGuYPlaEd-fSKxQcXgteA" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create initial node" uuid="_bnfr0PlaEd-fSKxQcXgteA" incoming="_dCXuYPlaEd-fSKxQcXgteA" outgoing="_BdIbQPlbEd-fSKxQcXgteA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_dR9pkPlaEd-fSKxQcXgteA" outgoingStoryLinks="_gxY90PlaEd-fSKxQcXgteA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_fSsyQPlaEd-fSKxQcXgteA" modifier="CREATE" incomingStoryLinks="_gxY90PlaEd-fSKxQcXgteA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_gxY90PlaEd-fSKxQcXgteA" modifier="CREATE" source="_dR9pkPlaEd-fSKxQcXgteA" target="_fSsyQPlaEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to create return value variable" uuid="_u7j9gPlaEd-fSKxQcXgteA" incoming="_BdIbQPlbEd-fSKxQcXgteA" outgoing="_L0QAEPlbEd-fSKxQcXgteA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_xudi8PlaEd-fSKxQcXgteA" outgoingStoryLinks="_7ilIYPlaEd-fSKxQcXgteA _74P2YPlaEd-fSKxQcXgteA _8OSX0PlaEd-fSKxQcXgteA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_z_MPcPlaEd-fSKxQcXgteA" incomingStoryLinks="_5yRoYPlaEd-fSKxQcXgteA _7ilIYPlaEd-fSKxQcXgteA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createReturnValueNode" uuid="_10EGYPlaEd-fSKxQcXgteA" modifier="CREATE" outgoingStoryLinks="_xz_fwPxmEd--caqh5zaNfg" incomingStoryLinks="_69k94PlaEd-fSKxQcXgteA _8OSX0PlaEd-fSKxQcXgteA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_-gOlQPlaEd-fSKxQcXgteA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__p6xMPlaEd-fSKxQcXgteA" expressionString="'create return value'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_3x324PlaEd-fSKxQcXgteA" modifier="CREATE" outgoingStoryLinks="_5yRoYPlaEd-fSKxQcXgteA _69k94PlaEd-fSKxQcXgteA" incomingStoryLinks="_74P2YPlaEd-fSKxQcXgteA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_rLmPcPxmEd--caqh5zaNfg" incomingStoryLinks="_xz_fwPxmEd--caqh5zaNfg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_usIW8PxmEd--caqh5zaNfg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_vDef8PxmEd--caqh5zaNfg">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
            <activity href="../common/create_returnValue_expression.story#_qoFvMPxlEd--caqh5zaNfg"/>
            <parameters name="resultLiteral" uuid="_QfE9YAg2EeC3CKyH_xgb5w">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_S9NMsAg2EeC3CKyH_xgb5w" expressionString="mote::rules::TransformationResult::RULE_NOT_APPLIED" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//rules/TransformationResult"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5yRoYPlaEd-fSKxQcXgteA" modifier="CREATE" source="_3x324PlaEd-fSKxQcXgteA" target="_z_MPcPlaEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_69k94PlaEd-fSKxQcXgteA" modifier="CREATE" source="_3x324PlaEd-fSKxQcXgteA" target="_10EGYPlaEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7ilIYPlaEd-fSKxQcXgteA" source="_xudi8PlaEd-fSKxQcXgteA" target="_z_MPcPlaEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_74P2YPlaEd-fSKxQcXgteA" modifier="CREATE" source="_xudi8PlaEd-fSKxQcXgteA" target="_3x324PlaEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8OSX0PlaEd-fSKxQcXgteA" modifier="CREATE" source="_xudi8PlaEd-fSKxQcXgteA" target="_10EGYPlaEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xz_fwPxmEd--caqh5zaNfg" modifier="CREATE" source="_10EGYPlaEd-fSKxQcXgteA" target="_rLmPcPxmEd--caqh5zaNfg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="iterate through the rule's parent correspondence nodes" uuid="_DfsI4PlbEd-fSKxQcXgteA" incoming="_d72nMPlyEd-8sINpd2aU-w _TloA8Pl3Ed-8sINpd2aU-w" outgoing="_8_FtMPlbEd-fSKxQcXgteA _HqW9EPlcEd-fSKxQcXgteA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_MZL5IPlbEd-fSKxQcXgteA" outgoingStoryLinks="_RMP_gPlbEd-fSKxQcXgteA _l49fIPlbEd-fSKxQcXgteA _nc5-IPlbEd-fSKxQcXgteA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_ONgWcPlbEd-fSKxQcXgteA" outgoingStoryLinks="_Ttcw8PlbEd-fSKxQcXgteA" incomingStoryLinks="_RMP_gPlbEd-fSKxQcXgteA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="parentCorrNode" uuid="_Rh5fYPlbEd-fSKxQcXgteA" incomingStoryLinks="_Ttcw8PlbEd-fSKxQcXgteA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tprpcPlbEd-fSKxQcXgteA" expressionString="self.modifier = tgg::TGGModifierEnumeration::NONE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sourceDomain" uuid="_kcorEPlbEd-fSKxQcXgteA" incomingStoryLinks="_l49fIPlbEd-fSKxQcXgteA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//SourceModelDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="targetDomain" uuid="_mLfZkPlbEd-fSKxQcXgteA" incomingStoryLinks="_nc5-IPlbEd-fSKxQcXgteA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TargetModelDomain"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RMP_gPlbEd-fSKxQcXgteA" source="_MZL5IPlbEd-fSKxQcXgteA" target="_ONgWcPlbEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ttcw8PlbEd-fSKxQcXgteA" source="_ONgWcPlbEd-fSKxQcXgteA" target="_Rh5fYPlbEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_l49fIPlbEd-fSKxQcXgteA" source="_MZL5IPlbEd-fSKxQcXgteA" target="_kcorEPlbEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/sourceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_nc5-IPlbEd-fSKxQcXgteA" source="_MZL5IPlbEd-fSKxQcXgteA" target="_mLfZkPlbEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/targetDomain"/>
      </storyPatternLinks>
      <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_uqpnwC7QEeC1Gslgwvz5vw" expressionString="parentCorrNode.outgoingCorrespondenceLinks->select(e|sourceDomain.modelElements->includes(e.oclAsType(tgg::CorrespondenceLink).target))->notEmpty()" expressionLanguage="OCL"/>
      <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_DFnQEC7REeC1Gslgwvz5vw" expressionString="parentCorrNode.outgoingCorrespondenceLinks->select(e|targetDomain.modelElements->includes(e.oclAsType(tgg::CorrespondenceLink).target))->notEmpty()" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_8iZxMPlbEd-fSKxQcXgteA" incoming="_LpnXMPl4Ed-8sINpd2aU-w">
      <outParameterValues xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_92AuIPlbEd-fSKxQcXgteA" expressionString="activity" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create for-each node to match LHS" uuid="_DLV0EPlcEd-fSKxQcXgteA" incoming="_HqW9EPlcEd-fSKxQcXgteA" outgoing="_D6HuMPlzEd-8sINpd2aU-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_LVTk0PlcEd-fSKxQcXgteA" outgoingStoryLinks="_YitaQPlcEd-fSKxQcXgteA _Y1BSQPlcEd-fSKxQcXgteA _ZKYeQPlcEd-fSKxQcXgteA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="lastNode" uuid="_PN4Y4PlcEd-fSKxQcXgteA" incomingStoryLinks="_YitaQPlcEd-fSKxQcXgteA _ZfImQPlcEd-fSKxQcXgteA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_QmS9UPlcEd-fSKxQcXgteA" modifier="CREATE" outgoingStoryLinks="_ZfImQPlcEd-fSKxQcXgteA _aLc5sPlcEd-fSKxQcXgteA" incomingStoryLinks="_Y1BSQPlcEd-fSKxQcXgteA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_xwVeUPlyEd-8sINpd2aU-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_yt2PcPlyEd-8sINpd2aU-w" expressionString="if lastNode.oclIsKindOf(storyDiagramEcore::nodes::ExpressionActivityNode) then&#xA;&#x9;storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::NONE&#xA;else&#xA;&#x9;storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::END&#xA;endif" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchLHSNode" uuid="_RUYLUPlcEd-fSKxQcXgteA" modifier="CREATE" incomingStoryLinks="_ZKYeQPlcEd-fSKxQcXgteA _aLc5sPlcEd-fSKxQcXgteA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_d6FHkPlcEd-fSKxQcXgteA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fFXO8PlcEd-fSKxQcXgteA" expressionString="'match LHS for parent node '.concat(parentCorrNode.name)" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_kiQ8gPlcEd-fSKxQcXgteA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/forEach"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ldnKcPlcEd-fSKxQcXgteA" expressionString="true" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_YitaQPlcEd-fSKxQcXgteA" source="_LVTk0PlcEd-fSKxQcXgteA" target="_PN4Y4PlcEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Y1BSQPlcEd-fSKxQcXgteA" modifier="CREATE" source="_LVTk0PlcEd-fSKxQcXgteA" target="_QmS9UPlcEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZKYeQPlcEd-fSKxQcXgteA" modifier="CREATE" source="_LVTk0PlcEd-fSKxQcXgteA" target="_RUYLUPlcEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZfImQPlcEd-fSKxQcXgteA" modifier="CREATE" source="_QmS9UPlcEd-fSKxQcXgteA" target="_PN4Y4PlcEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_aLc5sPlcEd-fSKxQcXgteA" modifier="CREATE" source="_QmS9UPlcEd-fSKxQcXgteA" target="_RUYLUPlcEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set lastNode" uuid="_bguokPlyEd-8sINpd2aU-w" incoming="_L0QAEPlbEd-fSKxQcXgteA" outgoing="_d72nMPlyEd-8sINpd2aU-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="lastNode" uuid="_cdJNgPlyEd-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityNode"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_eigXgPlyEd-8sINpd2aU-w" expressionString="createReturnValueNode" expressionLanguage="OCL"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to evaluate rule variables" uuid="__LdmsPlyEd-8sINpd2aU-w" incoming="_atGVUPxpEd--caqh5zaNfg" outgoing="_ps4IwPlzEd-8sINpd2aU-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_EeyhgPlzEd-8sINpd2aU-w" outgoingStoryLinks="_RhMA0PlzEd-8sINpd2aU-w _R23V4PlzEd-8sINpd2aU-w _SOHYQPlzEd-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchLHSNode" uuid="_FWWboPlzEd-8sINpd2aU-w" incomingStoryLinks="_RhMA0PlzEd-8sINpd2aU-w _ShZgoPlzEd-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_G2xFgPlzEd-8sINpd2aU-w" modifier="CREATE" outgoingStoryLinks="_ShZgoPlzEd-8sINpd2aU-w _TQRY0PlzEd-8sINpd2aU-w" incomingStoryLinks="_R23V4PlzEd-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_VeWZ8PlzEd-8sINpd2aU-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WTw_APlzEd-8sINpd2aU-w" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FOR_EACH" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evalRuleVarsNode" uuid="_HM01EPlzEd-8sINpd2aU-w" modifier="CREATE" outgoingStoryLinks="_n8NmQARREeCJf73xSIA6Gg" incomingStoryLinks="_SOHYQPlzEd-8sINpd2aU-w _TQRY0PlzEd-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_crHioPlzEd-8sINpd2aU-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_d8nmwPlzEd-8sINpd2aU-w" expressionString="'evaluate rule variables'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evalRuleVarsExpression" uuid="_RRjMMARREeCJf73xSIA6Gg" incomingStoryLinks="_n8NmQARREeCJf73xSIA6Gg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_VXZnQARREeCJf73xSIA6Gg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_VvRUsARREeCJf73xSIA6Gg">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
            <activity href="../common/create_evaluateRuleVariables_expression.story#_uabxYfZwEd-i2JdGlqLvVQ"/>
            <parameters name="tggRule" uuid="_gdJMoARREeCJf73xSIA6Gg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jA2fgARREeCJf73xSIA6Gg" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_kP66sARREeCJf73xSIA6Gg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mJymYARREeCJf73xSIA6Gg" expressionString="direction" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RhMA0PlzEd-8sINpd2aU-w" source="_EeyhgPlzEd-8sINpd2aU-w" target="_FWWboPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_R23V4PlzEd-8sINpd2aU-w" modifier="CREATE" source="_EeyhgPlzEd-8sINpd2aU-w" target="_G2xFgPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_SOHYQPlzEd-8sINpd2aU-w" modifier="CREATE" source="_EeyhgPlzEd-8sINpd2aU-w" target="_HM01EPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ShZgoPlzEd-8sINpd2aU-w" modifier="CREATE" source="_G2xFgPlzEd-8sINpd2aU-w" target="_FWWboPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_TQRY0PlzEd-8sINpd2aU-w" modifier="CREATE" source="_G2xFgPlzEd-8sINpd2aU-w" target="_HM01EPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_n8NmQARREeCJf73xSIA6Gg" modifier="CREATE" source="_HM01EPlzEd-8sINpd2aU-w" target="_RRjMMARREeCJf73xSIA6Gg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to check constraints" uuid="_lx3rwPlzEd-8sINpd2aU-w" incoming="_ps4IwPlzEd-8sINpd2aU-w" outgoing="_Y9Q7YPl0Ed-8sINpd2aU-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_qoBiYPlzEd-8sINpd2aU-w" outgoingStoryLinks="_9GUCoPlzEd-8sINpd2aU-w _9u_zQPlzEd-8sINpd2aU-w _-EAZ8PlzEd-8sINpd2aU-w _-mp0APlzEd-8sINpd2aU-w __CJdMPlzEd-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evalRuleVarsNode" uuid="_rZ32EPlzEd-8sINpd2aU-w" incomingStoryLinks="_9GUCoPlzEd-8sINpd2aU-w __mcdEPlzEd-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchLHSNode" uuid="_vW5FQPlzEd-8sINpd2aU-w" incomingStoryLinks="_9u_zQPlzEd-8sINpd2aU-w _DDLpsPl0Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_xP2K8PlzEd-8sINpd2aU-w" modifier="CREATE" outgoingStoryLinks="__mcdEPlzEd-8sINpd2aU-w _A41SAPl0Ed-8sINpd2aU-w" incomingStoryLinks="_-mp0APlzEd-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_xtGusPlzEd-8sINpd2aU-w" modifier="CREATE" outgoingStoryLinks="_CDj_wPl0Ed-8sINpd2aU-w _DDLpsPl0Ed-8sINpd2aU-w" incomingStoryLinks="__CJdMPlzEd-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_LFcP8Pl0Ed-8sINpd2aU-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_MKksAPl0Ed-8sINpd2aU-w" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkConstraintsNode" uuid="_yWkicPlzEd-8sINpd2aU-w" modifier="CREATE" incomingStoryLinks="_-EAZ8PlzEd-8sINpd2aU-w _A41SAPl0Ed-8sINpd2aU-w _CDj_wPl0Ed-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_GYXc0Pl0Ed-8sINpd2aU-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Hmgq8Pl0Ed-8sINpd2aU-w" expressionString="'check constraints'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9GUCoPlzEd-8sINpd2aU-w" source="_qoBiYPlzEd-8sINpd2aU-w" target="_rZ32EPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9u_zQPlzEd-8sINpd2aU-w" source="_qoBiYPlzEd-8sINpd2aU-w" target="_vW5FQPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_-EAZ8PlzEd-8sINpd2aU-w" modifier="CREATE" source="_qoBiYPlzEd-8sINpd2aU-w" target="_yWkicPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_-mp0APlzEd-8sINpd2aU-w" modifier="CREATE" source="_qoBiYPlzEd-8sINpd2aU-w" target="_xP2K8PlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="__CJdMPlzEd-8sINpd2aU-w" modifier="CREATE" source="_qoBiYPlzEd-8sINpd2aU-w" target="_xtGusPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="__mcdEPlzEd-8sINpd2aU-w" modifier="CREATE" source="_xP2K8PlzEd-8sINpd2aU-w" target="_rZ32EPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_A41SAPl0Ed-8sINpd2aU-w" modifier="CREATE" source="_xP2K8PlzEd-8sINpd2aU-w" target="_yWkicPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_CDj_wPl0Ed-8sINpd2aU-w" modifier="CREATE" source="_xtGusPlzEd-8sINpd2aU-w" target="_yWkicPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_DDLpsPl0Ed-8sINpd2aU-w" modifier="CREATE" source="_xtGusPlzEd-8sINpd2aU-w" target="_vW5FQPlzEd-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to create elements" uuid="_WGydgPl0Ed-8sINpd2aU-w" incoming="_BoaesARSEeCJf73xSIA6Gg" outgoing="_0qw9MPl0Ed-8sINpd2aU-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_ZcjfcPl0Ed-8sINpd2aU-w" outgoingStoryLinks="_hjDFcPl0Ed-8sINpd2aU-w _h3K7UPl0Ed-8sINpd2aU-w _iOHbsPl0Ed-8sINpd2aU-w _CmuKcCfHEeCkHOR1O7gzpA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkConstraintsNode" uuid="_aWKoAPl0Ed-8sINpd2aU-w" incomingStoryLinks="_hjDFcPl0Ed-8sINpd2aU-w _ilI0kPl0Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_cKcpEPl0Ed-8sINpd2aU-w" modifier="CREATE" outgoingStoryLinks="_ilI0kPl0Ed-8sINpd2aU-w _jZJrcPl0Ed-8sINpd2aU-w" incomingStoryLinks="_h3K7UPl0Ed-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_lScIUPl0Ed-8sINpd2aU-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mddJ8Pl0Ed-8sINpd2aU-w" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createElementsNode" uuid="_cqXfQPl0Ed-8sINpd2aU-w" modifier="CREATE" incomingStoryLinks="_iOHbsPl0Ed-8sINpd2aU-w _jZJrcPl0Ed-8sINpd2aU-w _FHPmcCfHEeCkHOR1O7gzpA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_qjhAYPl0Ed-8sINpd2aU-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rd_roPl0Ed-8sINpd2aU-w" expressionString="'create elements'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="outgoingEdge" uuid="_AhqicCfHEeCkHOR1O7gzpA" modifier="CREATE" outgoingStoryLinks="_FHPmcCfHEeCkHOR1O7gzpA" incomingStoryLinks="_CmuKcCfHEeCkHOR1O7gzpA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hjDFcPl0Ed-8sINpd2aU-w" source="_ZcjfcPl0Ed-8sINpd2aU-w" target="_aWKoAPl0Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_h3K7UPl0Ed-8sINpd2aU-w" modifier="CREATE" source="_ZcjfcPl0Ed-8sINpd2aU-w" target="_cKcpEPl0Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iOHbsPl0Ed-8sINpd2aU-w" modifier="CREATE" source="_ZcjfcPl0Ed-8sINpd2aU-w" target="_cqXfQPl0Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ilI0kPl0Ed-8sINpd2aU-w" modifier="CREATE" source="_cKcpEPl0Ed-8sINpd2aU-w" target="_aWKoAPl0Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jZJrcPl0Ed-8sINpd2aU-w" modifier="CREATE" source="_cKcpEPl0Ed-8sINpd2aU-w" target="_cqXfQPl0Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_CmuKcCfHEeCkHOR1O7gzpA" modifier="CREATE" source="_ZcjfcPl0Ed-8sINpd2aU-w" target="_AhqicCfHEeCkHOR1O7gzpA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_FHPmcCfHEeCkHOR1O7gzpA" modifier="CREATE" source="_AhqicCfHEeCkHOR1O7gzpA" target="_cqXfQPl0Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to add modification tag to queue" uuid="_cZ6L8Pl1Ed-8sINpd2aU-w" incoming="_I3xX0CkyEeCPsqbG4H8XYA" outgoing="_ABmdIPl2Ed-8sINpd2aU-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_gsQx8Pl1Ed-8sINpd2aU-w" outgoingStoryLinks="_tNvVgPl1Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="outgoingEdge" uuid="_iKN9sPl1Ed-8sINpd2aU-w" outgoingStoryLinks="_tuq4UPl1Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="addModTagNode" uuid="_i68QAPl1Ed-8sINpd2aU-w" modifier="CREATE" outgoingStoryLinks="_BlKqMAgzEeC3CKyH_xgb5w" incomingStoryLinks="_tNvVgPl1Ed-8sINpd2aU-w _tuq4UPl1Ed-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_4Oj-oPl1Ed-8sINpd2aU-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5MLdcPl1Ed-8sINpd2aU-w" expressionString="'add modification tag to queue'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_wyc68AgyEeC3CKyH_xgb5w" incomingStoryLinks="_BlKqMAgzEeC3CKyH_xgb5w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_8Z3qAAgyEeC3CKyH_xgb5w">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_8vxBgAgyEeC3CKyH_xgb5w">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
            <activity href="../common/create_addModifcationTagToQueue_expression.story#_lXQIIPcJEd-_yp3IZC6S-Q"/>
            <parameters name="tggRule" uuid="_-Y9z8AgyEeC3CKyH_xgb5w">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_AWi68AgzEeC3CKyH_xgb5w" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tNvVgPl1Ed-8sINpd2aU-w" modifier="CREATE" source="_gsQx8Pl1Ed-8sINpd2aU-w" target="_i68QAPl1Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tuq4UPl1Ed-8sINpd2aU-w" modifier="CREATE" source="_iKN9sPl1Ed-8sINpd2aU-w" target="_i68QAPl1Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BlKqMAgzEeC3CKyH_xgb5w" modifier="CREATE" source="_i68QAPl1Ed-8sINpd2aU-w" target="_wyc68AgyEeC3CKyH_xgb5w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to set return value" uuid="_8SOoYPl1Ed-8sINpd2aU-w" incoming="_ABmdIPl2Ed-8sINpd2aU-w" outgoing="_Sfv-sCkyEeCPsqbG4H8XYA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_Agc8UPl2Ed-8sINpd2aU-w" outgoingStoryLinks="_NjvzgPl2Ed-8sINpd2aU-w _OIQ10Pl2Ed-8sINpd2aU-w _Och1oPl2Ed-8sINpd2aU-w _PD3icPl2Ed-8sINpd2aU-w _PbjpsPl2Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="addModTagNode" uuid="_BX0CIPl2Ed-8sINpd2aU-w" incomingStoryLinks="_OIQ10Pl2Ed-8sINpd2aU-w _PuCg0Pl2Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_Cb-N0Pl2Ed-8sINpd2aU-w" modifier="CREATE" outgoingStoryLinks="_PuCg0Pl2Ed-8sINpd2aU-w _Qc05cPl2Ed-8sINpd2aU-w" incomingStoryLinks="_NjvzgPl2Ed-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="setReturnValueNode" uuid="_D9S2oPl2Ed-8sINpd2aU-w" modifier="CREATE" outgoingStoryLinks="_HUmK0Ag2EeC3CKyH_xgb5w" incomingStoryLinks="_PD3icPl2Ed-8sINpd2aU-w _Qc05cPl2Ed-8sINpd2aU-w _RJ6B4Pl2Ed-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_UuC3APl2Ed-8sINpd2aU-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_V0JjcPl2Ed-8sINpd2aU-w" expressionString="'set return value'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_FKPx8Pl2Ed-8sINpd2aU-w" modifier="CREATE" outgoingStoryLinks="_RJ6B4Pl2Ed-8sINpd2aU-w _R0mksPl2Ed-8sINpd2aU-w" incomingStoryLinks="_PbjpsPl2Ed-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchLHSNode" uuid="_FoIAwPl2Ed-8sINpd2aU-w" incomingStoryLinks="_Och1oPl2Ed-8sINpd2aU-w _R0mksPl2Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_qhLCwAg1EeC3CKyH_xgb5w" incomingStoryLinks="_HUmK0Ag2EeC3CKyH_xgb5w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_sE_l8Ag1EeC3CKyH_xgb5w">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_scHFcAg1EeC3CKyH_xgb5w">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
            <activity href="../common/create_returnValue_expression.story#_qoFvMPxlEd--caqh5zaNfg"/>
            <parameters name="resultLiteral" uuid="_0RlM4Ag1EeC3CKyH_xgb5w">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EZGqEAg2EeC3CKyH_xgb5w" expressionString="mote::rules::TransformationResult::RULE_APPLIED" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//rules/TransformationResult"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_NjvzgPl2Ed-8sINpd2aU-w" modifier="CREATE" source="_Agc8UPl2Ed-8sINpd2aU-w" target="_Cb-N0Pl2Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_OIQ10Pl2Ed-8sINpd2aU-w" source="_Agc8UPl2Ed-8sINpd2aU-w" target="_BX0CIPl2Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Och1oPl2Ed-8sINpd2aU-w" source="_Agc8UPl2Ed-8sINpd2aU-w" target="_FoIAwPl2Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_PD3icPl2Ed-8sINpd2aU-w" modifier="CREATE" source="_Agc8UPl2Ed-8sINpd2aU-w" target="_D9S2oPl2Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_PbjpsPl2Ed-8sINpd2aU-w" modifier="CREATE" source="_Agc8UPl2Ed-8sINpd2aU-w" target="_FKPx8Pl2Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_PuCg0Pl2Ed-8sINpd2aU-w" modifier="CREATE" source="_Cb-N0Pl2Ed-8sINpd2aU-w" target="_BX0CIPl2Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Qc05cPl2Ed-8sINpd2aU-w" modifier="CREATE" source="_Cb-N0Pl2Ed-8sINpd2aU-w" target="_D9S2oPl2Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RJ6B4Pl2Ed-8sINpd2aU-w" modifier="CREATE" source="_FKPx8Pl2Ed-8sINpd2aU-w" target="_D9S2oPl2Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_R0mksPl2Ed-8sINpd2aU-w" modifier="CREATE" source="_FKPx8Pl2Ed-8sINpd2aU-w" target="_FoIAwPl2Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_HUmK0Ag2EeC3CKyH_xgb5w" modifier="CREATE" source="_D9S2oPl2Ed-8sINpd2aU-w" target="_qhLCwAg1EeC3CKyH_xgb5w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set last node" uuid="_ZtJ_4Pl3Ed-8sINpd2aU-w" incoming="_Sfv-sCkyEeCPsqbG4H8XYA" outgoing="_TloA8Pl3Ed-8sINpd2aU-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="lastNode" uuid="_dbkyYPl3Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityNode"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_g2VCAPl3Ed-8sINpd2aU-w" expressionString="matchLHSNode" expressionLanguage="OCL"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create final node" uuid="_kWjngPl3Ed-8sINpd2aU-w" incoming="_8_FtMPlbEd-fSKxQcXgteA" outgoing="_LpnXMPl4Ed-8sINpd2aU-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_n-xEUPl3Ed-8sINpd2aU-w" outgoingStoryLinks="_2ujSgPl3Ed-8sINpd2aU-w _3G6vMPl3Ed-8sINpd2aU-w _3dHosPl3Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="lastNode" uuid="_ocuLoPl3Ed-8sINpd2aU-w" incomingStoryLinks="_2ujSgPl3Ed-8sINpd2aU-w _3tJBsPl3Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_pSumkPl3Ed-8sINpd2aU-w" modifier="CREATE" outgoingStoryLinks="_3tJBsPl3Ed-8sINpd2aU-w _4eBFAPl3Ed-8sINpd2aU-w" incomingStoryLinks="_3G6vMPl3Ed-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_8PkEAPl3Ed-8sINpd2aU-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9Ged4Pl3Ed-8sINpd2aU-w" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::END" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="finalNode" uuid="_pgIwcPl3Ed-8sINpd2aU-w" modifier="CREATE" outgoingStoryLinks="_5NLREPl3Ed-8sINpd2aU-w" incomingStoryLinks="_3dHosPl3Ed-8sINpd2aU-w _4eBFAPl3Ed-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="returnValueExpression" uuid="_qmCBgPl3Ed-8sINpd2aU-w" modifier="CREATE" outgoingStoryLinks="_5ni44Pl3Ed-8sINpd2aU-w" incomingStoryLinks="_5NLREPl3Ed-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="returnVarRef" uuid="_sTx8wPl3Ed-8sINpd2aU-w" modifier="CREATE" incomingStoryLinks="_5ni44Pl3Ed-8sINpd2aU-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_Cs3eoPl4Ed-8sINpd2aU-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ewb44Pl4Ed-8sINpd2aU-w" expressionString="'__returnValue'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_GoUnkPl4Ed-8sINpd2aU-w">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_HtA-wPl4Ed-8sINpd2aU-w" expressionString="mote::rules::TransformationResult" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2ujSgPl3Ed-8sINpd2aU-w" source="_n-xEUPl3Ed-8sINpd2aU-w" target="_ocuLoPl3Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3G6vMPl3Ed-8sINpd2aU-w" modifier="CREATE" source="_n-xEUPl3Ed-8sINpd2aU-w" target="_pSumkPl3Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3dHosPl3Ed-8sINpd2aU-w" modifier="CREATE" source="_n-xEUPl3Ed-8sINpd2aU-w" target="_pgIwcPl3Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3tJBsPl3Ed-8sINpd2aU-w" modifier="CREATE" source="_pSumkPl3Ed-8sINpd2aU-w" target="_ocuLoPl3Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4eBFAPl3Ed-8sINpd2aU-w" modifier="CREATE" source="_pSumkPl3Ed-8sINpd2aU-w" target="_pgIwcPl3Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5NLREPl3Ed-8sINpd2aU-w" modifier="CREATE" source="_pgIwcPl3Ed-8sINpd2aU-w" target="_qmCBgPl3Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5ni44Pl3Ed-8sINpd2aU-w" modifier="CREATE" source="_qmCBgPl3Ed-8sINpd2aU-w" target="_sTx8wPl3Ed-8sINpd2aU-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create match LHS story pattern" uuid="_YofZQPxpEd--caqh5zaNfg" incoming="_D6HuMPlzEd-8sINpd2aU-w" outgoing="_atGVUPxpEd--caqh5zaNfg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_bayhwPxpEd--caqh5zaNfg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_cEatkPxpEd--caqh5zaNfg">
          <activity href="create_storyPattern_match_LHS_rule.story#__-3qEPxmEd--caqh5zaNfg"/>
          <parameters name="tggRule" uuid="_fqW5gPxpEd--caqh5zaNfg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ivRcAPxpEd--caqh5zaNfg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="parentCorrNode" uuid="_g2CpgPxpEd--caqh5zaNfg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kArZgPxpEd--caqh5zaNfg" expressionString="parentCorrNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_lS7EgPxpEd--caqh5zaNfg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nuZocPxpEd--caqh5zaNfg" expressionString="matchLHSNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="direction" uuid="_qHN58PxpEd--caqh5zaNfg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rVuUcPxpEd--caqh5zaNfg" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create story pattern to check LHS constraints" uuid="_732OQARREeCJf73xSIA6Gg" incoming="_Y9Q7YPl0Ed-8sINpd2aU-w" outgoing="_BoaesARSEeCJf73xSIA6Gg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_CMVrIARSEeCJf73xSIA6Gg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_G4pCMARSEeCJf73xSIA6Gg">
          <activity href="create_storyPattern_check_constraints.story#_2MC_APbdEd-JnYtEWuN3-g"/>
          <parameters name="tggRule" uuid="_Ij_wwARSEeCJf73xSIA6Gg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KLONQARSEeCJf73xSIA6Gg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_NzJWMARSEeCJf73xSIA6Gg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_P1FYwARSEeCJf73xSIA6Gg" expressionString="checkConstraintsNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="createSourceConstraints" uuid="_9oqQ0BRFEeCs8_2SYawiug">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__oeMcBRFEeCs8_2SYawiug" expressionString="direction = mote::TransformationDirection::FORWARD or&#xA;direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetConstraints" uuid="_DYJjMBRGEeCs8_2SYawiug">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_FSeh4BRGEeCs8_2SYawiug" expressionString="direction = mote::TransformationDirection::MAPPING or&#xA;direction = mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentNodesConstraints" uuid="_IrYXYBRGEeCs8_2SYawiug">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_K1yZcBRGEeCs8_2SYawiug" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedNodesConstraints" uuid="_MH9L8BRGEeCs8_2SYawiug">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OdMlABRGEeCs8_2SYawiug" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create story pattern to create RHS" uuid="_GstnkARpEeCTt5eJIKjk1Q" incoming="_0qw9MPl0Ed-8sINpd2aU-w" outgoing="_KLkUEARpEeCTt5eJIKjk1Q">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_INrrEARpEeCTt5eJIKjk1Q">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_Nc3FsARpEeCTt5eJIKjk1Q">
          <activity href="create_storyPattern_create_RHS_rule.story#_emvIUPbiEd-JnYtEWuN3-g"/>
          <parameters name="tggRule" uuid="_Osru0ARpEeCTt5eJIKjk1Q">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_QVaAIARpEeCTt5eJIKjk1Q" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_RQr8oARpEeCTt5eJIKjk1Q">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SiZcIARpEeCTt5eJIKjk1Q" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_TlOyIARpEeCTt5eJIKjk1Q">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VTFbEARpEeCTt5eJIKjk1Q" expressionString="createElementsNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create expression activity nodes to execute post-creation expressions" uuid="_9cHGwCfGEeCkHOR1O7gzpA" incoming="_KLkUEARpEeCTt5eJIKjk1Q" outgoing="_I3xX0CkyEeCPsqbG4H8XYA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_NPxMwCfHEeCkHOR1O7gzpA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:VariableDeclarationAction" uuid="_OMeFkCfHEeCkHOR1O7gzpA" variableName="outgoingEdge">
          <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
          <valueAssignment xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_SZVHkCfHEeCkHOR1O7gzpA">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_TsVAgCfHEeCkHOR1O7gzpA">
              <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
              <activity href="../common/create_executePostCreation_eans.story#_aZJ3ICfCEeCkHOR1O7gzpA"/>
              <parameters name="tggRule" uuid="_U9DokCfHEeCkHOR1O7gzpA">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XA9aACfHEeCkHOR1O7gzpA" expressionString="tggRule" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
              </parameters>
              <parameters name="incomingEdge" uuid="_X-EigCfHEeCkHOR1O7gzpA">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Zt13ACfHEeCkHOR1O7gzpA" expressionString="outgoingEdge" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
              </parameters>
              <parameters name="direction" uuid="_cA7T8CfHEeCkHOR1O7gzpA">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_d_ui8CfHEeCkHOR1O7gzpA" expressionString="direction" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
              </parameters>
            </callActions>
          </valueAssignment>
        </callActions>
      </expression>
    </nodes>
    <edges uuid="_VueqAPlaEd-fSKxQcXgteA" source="_Klm4IPlaEd-fSKxQcXgteA" target="_L70dIPlaEd-fSKxQcXgteA"/>
    <edges uuid="_dCXuYPlaEd-fSKxQcXgteA" source="_L70dIPlaEd-fSKxQcXgteA" target="_bnfr0PlaEd-fSKxQcXgteA"/>
    <edges uuid="_BdIbQPlbEd-fSKxQcXgteA" source="_bnfr0PlaEd-fSKxQcXgteA" target="_u7j9gPlaEd-fSKxQcXgteA"/>
    <edges uuid="_L0QAEPlbEd-fSKxQcXgteA" source="_u7j9gPlaEd-fSKxQcXgteA" target="_bguokPlyEd-8sINpd2aU-w"/>
    <edges uuid="_8_FtMPlbEd-fSKxQcXgteA" source="_DfsI4PlbEd-fSKxQcXgteA" target="_kWjngPl3Ed-8sINpd2aU-w" guardType="END"/>
    <edges uuid="_HqW9EPlcEd-fSKxQcXgteA" source="_DfsI4PlbEd-fSKxQcXgteA" target="_DLV0EPlcEd-fSKxQcXgteA" guardType="FOR_EACH"/>
    <edges uuid="_d72nMPlyEd-8sINpd2aU-w" source="_bguokPlyEd-8sINpd2aU-w" target="_DfsI4PlbEd-fSKxQcXgteA"/>
    <edges uuid="_D6HuMPlzEd-8sINpd2aU-w" source="_DLV0EPlcEd-fSKxQcXgteA" target="_YofZQPxpEd--caqh5zaNfg"/>
    <edges uuid="_ps4IwPlzEd-8sINpd2aU-w" source="__LdmsPlyEd-8sINpd2aU-w" target="_lx3rwPlzEd-8sINpd2aU-w"/>
    <edges uuid="_Y9Q7YPl0Ed-8sINpd2aU-w" source="_lx3rwPlzEd-8sINpd2aU-w" target="_732OQARREeCJf73xSIA6Gg"/>
    <edges uuid="_0qw9MPl0Ed-8sINpd2aU-w" source="_WGydgPl0Ed-8sINpd2aU-w" target="_GstnkARpEeCTt5eJIKjk1Q"/>
    <edges uuid="_ABmdIPl2Ed-8sINpd2aU-w" source="_cZ6L8Pl1Ed-8sINpd2aU-w" target="_8SOoYPl1Ed-8sINpd2aU-w"/>
    <edges uuid="_TloA8Pl3Ed-8sINpd2aU-w" source="_ZtJ_4Pl3Ed-8sINpd2aU-w" target="_DfsI4PlbEd-fSKxQcXgteA"/>
    <edges uuid="_LpnXMPl4Ed-8sINpd2aU-w" source="_kWjngPl3Ed-8sINpd2aU-w" target="_8iZxMPlbEd-fSKxQcXgteA"/>
    <edges uuid="_atGVUPxpEd--caqh5zaNfg" source="_YofZQPxpEd--caqh5zaNfg" target="__LdmsPlyEd-8sINpd2aU-w"/>
    <edges uuid="_BoaesARSEeCJf73xSIA6Gg" source="_732OQARREeCJf73xSIA6Gg" target="_WGydgPl0Ed-8sINpd2aU-w"/>
    <edges uuid="_KLkUEARpEeCTt5eJIKjk1Q" source="_GstnkARpEeCTt5eJIKjk1Q" target="_9cHGwCfGEeCkHOR1O7gzpA"/>
    <edges uuid="_I3xX0CkyEeCPsqbG4H8XYA" source="_9cHGwCfGEeCkHOR1O7gzpA" target="_cZ6L8Pl1Ed-8sINpd2aU-w"/>
    <edges uuid="_Sfv-sCkyEeCPsqbG4H8XYA" source="_8SOoYPl1Ed-8sINpd2aU-w" target="_ZtJ_4Pl3Ed-8sINpd2aU-w"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
