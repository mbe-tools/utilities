<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_match_LHS_rule" uuid="_-VLvcPxmEd--caqh5zaNfg">
  <activities name="create_storyPattern_match_LHS_rule" uuid="__-3qEPxmEd--caqh5zaNfg">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_Cu6v0PxnEd--caqh5zaNfg" outgoing="_VhVtAPxnEd--caqh5zaNfg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_DXvDUPxnEd--caqh5zaNfg" incoming="_VhVtAPxnEd--caqh5zaNfg" outgoing="_ebp6YBWGEeCv-a_w4vT5EQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_EdPS0PxnEd--caqh5zaNfg" outgoingStoryLinks="_LpvVwPxnEd--caqh5zaNfg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="parentCorrNode" uuid="_HjcOwPxnEd--caqh5zaNfg" incomingStoryLinks="_L56fwPxnEd--caqh5zaNfg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_KK0gsPxnEd--caqh5zaNfg" outgoingStoryLinks="_L56fwPxnEd--caqh5zaNfg" incomingStoryLinks="_LpvVwPxnEd--caqh5zaNfg">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_NsaYIPxnEd--caqh5zaNfg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_QRgLEPxnEd--caqh5zaNfg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_Z9lO0PxnEd--caqh5zaNfg" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LpvVwPxnEd--caqh5zaNfg" source="_EdPS0PxnEd--caqh5zaNfg" target="_KK0gsPxnEd--caqh5zaNfg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_L56fwPxnEd--caqh5zaNfg" source="_KK0gsPxnEd--caqh5zaNfg" target="_HjcOwPxnEd--caqh5zaNfg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create this and ruleSet SPOs" uuid="_b2yJ4BWGEeCv-a_w4vT5EQ" incoming="_ebp6YBWGEeCv-a_w4vT5EQ" outgoing="_vWEXoBWHEeCv-a_w4vT5EQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisSpo" uuid="_dpRbMBWGEeCv-a_w4vT5EQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_kQ1lEBWGEeCv-a_w4vT5EQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_kpumMBWGEeCv-a_w4vT5EQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_rule_spo.story#_Kpsp0AwlEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_mm9u8BWGEeCv-a_w4vT5EQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sljNQBWGEeCv-a_w4vT5EQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="isAxiom" uuid="_tszwQBWGEeCv-a_w4vT5EQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_vUQ2QBWGEeCv-a_w4vT5EQ" expressionString="false" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_xTAa4BWGEeCv-a_w4vT5EQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_2b4ZMBWGEeCv-a_w4vT5EQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_20zPgBWGEeCv-a_w4vT5EQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_ruleSet_spo.story#_ZDP-wAwkEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_4p8MMBWGEeCv-a_w4vT5EQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7gZb8BWGEeCv-a_w4vT5EQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="bindingType" uuid="_9ClncBWGEeCv-a_w4vT5EQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-eu1UBWGEeCv-a_w4vT5EQ" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform parent model objects" uuid="_DMwqsBWHEeCv-a_w4vT5EQ" incoming="_XEOu8BWIEeC7l7ZPwVRGQg" outgoing="_wjPVYBWHEeCv-a_w4vT5EQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_Htx-0BWHEeCv-a_w4vT5EQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_IgywIBWHEeCv-a_w4vT5EQ">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_JarlgBWHEeCv-a_w4vT5EQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_LgWRgBWHEeCv-a_w4vT5EQ" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_MhNRkBWHEeCv-a_w4vT5EQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_PIk8cBWHEeCv-a_w4vT5EQ" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_P9i1kBWHEeCv-a_w4vT5EQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SJGuMBWHEeCv-a_w4vT5EQ" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_TD0qABWHEeCv-a_w4vT5EQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VX7aoBWHEeCv-a_w4vT5EQ" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_XjWJUBWHEeCv-a_w4vT5EQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZFhGsBWHEeCv-a_w4vT5EQ" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_bDKfIBWHEeCv-a_w4vT5EQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dzLvsBWHEeCv-a_w4vT5EQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_e0Xf4BWHEeCv-a_w4vT5EQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hZP3cBWHEeCv-a_w4vT5EQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_iQOiwBWHEeCv-a_w4vT5EQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kEnRgBWHEeCv-a_w4vT5EQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_k9c-ABWHEeCv-a_w4vT5EQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_m83RABWHEeCv-a_w4vT5EQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_oDVw4BWHEeCv-a_w4vT5EQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_pzBl0BWHEeCv-a_w4vT5EQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_qvvswBWHEeCv-a_w4vT5EQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sgdccBWHEeCv-a_w4vT5EQ" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_RTJGICVXEeCVE9iKiEiTJw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Snd0wCVXEeCVE9iKiEiTJw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_wARLMBWHEeCv-a_w4vT5EQ" incoming="_EKUlgBWPEeC7l7ZPwVRGQg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create this->ruleSet link" uuid="_UDT7oBWIEeC7l7ZPwVRGQg" incoming="_vWEXoBWHEeCv-a_w4vT5EQ" outgoing="_XEOu8BWIEeC7l7ZPwVRGQg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_YFA2gBWIEeC7l7ZPwVRGQg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_b_M0IBWIEeC7l7ZPwVRGQg">
          <activity href="../common/create_rule_ruleSet_link.story#_qOEaEAwmEeCU3uEhqcP0Dg"/>
          <parameters name="ruleSpo" uuid="_f2ensBWIEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_iH6e0BWIEeC7l7ZPwVRGQg" expressionString="thisSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_jQTDMBWIEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lT-LIBWIEeC7l7ZPwVRGQg" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform parent model links" uuid="_sQKhMBWIEeC7l7ZPwVRGQg" incoming="_wjPVYBWHEeCv-a_w4vT5EQ" outgoing="_1O1P4BWJEeC7l7ZPwVRGQg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_zok5MBWIEeC7l7ZPwVRGQg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_0Iw1IBWIEeC7l7ZPwVRGQg">
          <activity href="../common/transform_modelLinks.story#_tdw5IAw6EeCAUcBgGC58lA"/>
          <parameters name="tggRule" uuid="_04K40BWIEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3M1DEBWIEeC7l7ZPwVRGQg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_4LlxYBWIEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6miw4BWIEeC7l7ZPwVRGQg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_7cjy4BWIEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jCdmEBWJEeC7l7ZPwVRGQg" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_j5SgYBWJEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_l126wBWJEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentLinks" uuid="_ntlRYBWJEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qDkRUBWJEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedLinks" uuid="_pqKS0BWJEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sa4G8BWJEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_t5pyEBWJEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_vUBeUBWJEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_wHuzMBWJEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xxrMgBWJEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform parent correspondence nodes" uuid="_DudiMBWKEeC7l7ZPwVRGQg" incoming="_1O1P4BWJEeC7l7ZPwVRGQg" outgoing="_kSsCYBWKEeC7l7ZPwVRGQg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_IfkgwBWKEeC7l7ZPwVRGQg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_Kt4ycBWKEeC7l7ZPwVRGQg">
          <activity href="../common/transform_correspondenceNodes.story#_gUE7UBNnEeCRUJdROfc5HA"/>
          <parameters name="tggRule" uuid="_Lc_7MBWKEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OEPqQBWKEeC7l7ZPwVRGQg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_RuV9cBWKEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_UTvSYBWKEeC7l7ZPwVRGQg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_VLrnABWKEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XL_R4BWKEeC7l7ZPwVRGQg" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_YFfFsBWKEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_aMqNkBWKEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_cFZ34BWKEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dtmkwBWKEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_fgLVoBWKEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hP6N4BWKEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="bindingType" uuid="_MsE3QBWnEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N7Nj4BWnEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create corrNode->sources and targets links" uuid="_ruPN8BWKEeC7l7ZPwVRGQg" incoming="_kSsCYBWKEeC7l7ZPwVRGQg" outgoing="_M85LMBWLEeC7l7ZPwVRGQg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_tzqpYBWKEeC7l7ZPwVRGQg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_xSsVABWKEeC7l7ZPwVRGQg">
          <activity href="../common/create_corrNode_sourcesTargets_links.story#_uYYuIBNoEeCRUJdROfc5HA"/>
          <parameters name="tggRule" uuid="_yGoTYBWKEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0VW0wBWKEeC7l7ZPwVRGQg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_1ZIl8BWKEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3vuC0BWKEeC7l7ZPwVRGQg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_4oy_4BWKEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6oaHMBWKEeC7l7ZPwVRGQg" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_7ox_EBWKEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9jjpsBWKEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourcesLinks" uuid="__YdV0BWKEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BVfDMBWLEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetsLinks" uuid="_ClU6cBWLEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EKkZ8BWLEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentLinks" uuid="_FAzeYBWLEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_HDE4IBWLEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedLinks" uuid="_IMxrIBWLEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KPcGcBWLEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->modelElements links" uuid="_W_kxEBWLEeC7l7ZPwVRGQg" incoming="_M85LMBWLEeC7l7ZPwVRGQg" outgoing="_fAJf0BWMEeC7l7ZPwVRGQg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_c_fFEBWLEeC7l7ZPwVRGQg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_fJrrwBWLEeC7l7ZPwVRGQg">
          <activity href="../common/create_ruleSet_modelElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_f3OHMBWLEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_h2TDABWLEeC7l7ZPwVRGQg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_ipfagBWLEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ktWIoBWLEeC7l7ZPwVRGQg" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="modifier" uuid="_lybhYBWLEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nuHK8BWLEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_pgK-cBWLEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rlxZABWLEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_se6ngBWLEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tzemsBWLEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_vFkgsBWLEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xbQlsBWLEeC7l7ZPwVRGQg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="createParentLinks" uuid="_gx0RwBWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_iny78BWMEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedLinks" uuid="_g26NIBWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kkB_IBWMEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_y7SYkBWMEeC7l7ZPwVRGQg" incoming="_fAJf0BWMEeC7l7ZPwVRGQg" outgoing="_-llcgBWMEeC7l7ZPwVRGQg _HAZssBWNEeC7l7ZPwVRGQg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created source model objects" uuid="_5zCTgBWMEeC7l7ZPwVRGQg" incoming="_-llcgBWMEeC7l7ZPwVRGQg" outgoing="_El9DgBWNEeC7l7ZPwVRGQg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_5zC6kBWMEeC7l7ZPwVRGQg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_5zC6kRWMEeC7l7ZPwVRGQg">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_5zC6oBWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5zC6oRWMEeC7l7ZPwVRGQg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_5zC6ohWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5zC6oxWMEeC7l7ZPwVRGQg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_5zC6nhWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5zC6nxWMEeC7l7ZPwVRGQg" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_5zC6khWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5zC6kxWMEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_5zC6mBWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5zC6mRWMEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_5zC6lhWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5zC6lxWMEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_5zC6nBWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5zC6nRWMEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_5zC6mhWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5zC6mxWMEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_5zC6pBWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5zC6pRWMEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_5zC6phWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5zC6pxWMEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_5zC6lBWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5zC6lRWMEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_Fv9W0CVXEeCVE9iKiEiTJw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IAtRcCVXEeCVE9iKiEiTJw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created source model links" uuid="__x5eoBWMEeC7l7ZPwVRGQg" incoming="_El9DgBWNEeC7l7ZPwVRGQg" outgoing="_Uib44BWOEeC7l7ZPwVRGQg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="__x5eoRWMEeC7l7ZPwVRGQg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="__x5eohWMEeC7l7ZPwVRGQg">
          <activity href="../common/transform_modelLinks.story#_tdw5IAw6EeCAUcBgGC58lA"/>
          <parameters name="tggRule" uuid="__x5eqRWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__x5eqhWMEeC7l7ZPwVRGQg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="__x5erRWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__x5erhWMEeC7l7ZPwVRGQg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="__x5eqxWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__x5erBWMEeC7l7ZPwVRGQg" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="__x5epxWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__x5eqBWMEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentLinks" uuid="__x5epRWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__x5ephWMEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedLinks" uuid="__x5esRWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__x5eshWMEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="__x5eoxWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__x5epBWMEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="__x5erxWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__x5esBWMEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_FbXBgBWNEeC7l7ZPwVRGQg" incoming="_GJ2fMBWNEeC7l7ZPwVRGQg _HAZssBWNEeC7l7ZPwVRGQg" outgoing="_NYyLEBWNEeC7l7ZPwVRGQg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_ME6vcBWNEeC7l7ZPwVRGQg" incoming="_NYyLEBWNEeC7l7ZPwVRGQg" outgoing="_UI7CIBWNEeC7l7ZPwVRGQg _WmfQwBWNEeC7l7ZPwVRGQg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created target model objects" uuid="_N6cGoBWNEeC7l7ZPwVRGQg" incoming="_UI7CIBWNEeC7l7ZPwVRGQg" outgoing="_UiUZkBWNEeC7l7ZPwVRGQg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_N6cGoRWNEeC7l7ZPwVRGQg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_N6cGohWNEeC7l7ZPwVRGQg">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_N6ctvxWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N6ctwBWNEeC7l7ZPwVRGQg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_N6cGpRWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N6cGphWNEeC7l7ZPwVRGQg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_N6cttRWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N6ctthWNEeC7l7ZPwVRGQg" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_N6cGpxWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N6ctsBWNEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_N6ctuRWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N6ctuhWNEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_N6ctsRWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N6ctshWNEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_N6ctuxWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N6ctvBWNEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_N6ctvRWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N6ctvhWNEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_N6cttxWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N6ctuBWNEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_N6cGoxWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N6cGpBWNEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_N6ctsxWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N6cttBWNEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_MT494CVXEeCVE9iKiEiTJw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OFRb8CVXEeCVE9iKiEiTJw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created target model links" uuid="_Q4VbIBWNEeC7l7ZPwVRGQg" incoming="_UiUZkBWNEeC7l7ZPwVRGQg" outgoing="_U2ePMBWOEeC7l7ZPwVRGQg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_Q4VbIRWNEeC7l7ZPwVRGQg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_Q4VbIhWNEeC7l7ZPwVRGQg">
          <activity href="../common/transform_modelLinks.story#_tdw5IAw6EeCAUcBgGC58lA"/>
          <parameters name="tggRule" uuid="_Q4VbLRWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Q4VbLhWNEeC7l7ZPwVRGQg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_Q4VbIxWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Q4VbJBWNEeC7l7ZPwVRGQg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_Q4VbKxWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Q4VbLBWNEeC7l7ZPwVRGQg" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_Q4VbKRWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Q4VbKhWNEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentLinks" uuid="_Q4VbJRWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Q4VbJhWNEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedLinks" uuid="_Q4VbMRWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Q4VbMhWNEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_Q4VbJxWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Q4VbKBWNEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_Q4VbLxWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Q4VbMBWNEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_VGzmsBWNEeC7l7ZPwVRGQg" incoming="_VqI9QBWNEeC7l7ZPwVRGQg _WmfQwBWNEeC7l7ZPwVRGQg" outgoing="_X-7DUBWNEeC7l7ZPwVRGQg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->uncoveredModelElements links" uuid="_1zzoYBWNEeC7l7ZPwVRGQg" incoming="_Uib44BWOEeC7l7ZPwVRGQg" outgoing="_GJ2fMBWNEeC7l7ZPwVRGQg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_6GXCsBWNEeC7l7ZPwVRGQg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_7sBY8BWNEeC7l7ZPwVRGQg">
          <activity href="../common/create_ruleSet_uncoveredElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_8hxVMBWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-yeMgBWNEeC7l7ZPwVRGQg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="__4y7YBWNEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_CQ73cBWOEeC7l7ZPwVRGQg" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_DY-dkBWOEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_F1DecBWOEeC7l7ZPwVRGQg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_GtUjMBWOEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IxfaYBWOEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_K-dBMBWOEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Mz3qsBWOEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_NmBgYBWOEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_O-hkYBWOEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->uncoveredModelElements links" uuid="_QHBdjxWOEeC7l7ZPwVRGQg" incoming="_U2ePMBWOEeC7l7ZPwVRGQg" outgoing="_VqI9QBWNEeC7l7ZPwVRGQg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_QHBdkBWOEeC7l7ZPwVRGQg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_QHBdkRWOEeC7l7ZPwVRGQg">
          <activity href="../common/create_ruleSet_uncoveredElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_QHCEkRWOEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_QHCEkhWOEeC7l7ZPwVRGQg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_QHCEkxWOEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_QHCElBWOEeC7l7ZPwVRGQg" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_QHCElxWOEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_QHCEmBWOEeC7l7ZPwVRGQg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_QHBdlBWOEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_QHCEkBWOEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_QHCElRWOEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_QHCElhWOEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_QHBdkhWOEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_QHBdkxWOEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create direct assignment for the designated parent correspondence node" uuid="_d7SeoBWOEeC7l7ZPwVRGQg" incoming="_X-7DUBWNEeC7l7ZPwVRGQg" outgoing="_EKUlgBWPEeC7l7ZPwVRGQg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_ivDvoBWOEeC7l7ZPwVRGQg" outgoingStoryLinks="_Oz_z8ClZEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="parentCorrNode" uuid="_kDBR4BWOEeC7l7ZPwVRGQg" incomingStoryLinks="_Oz_z8ClZEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_ktMQQBWOEeC7l7ZPwVRGQg" outgoingStoryLinks="_zsmO0BWOEeC7l7ZPwVRGQg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="__ZIHYBWOEeC7l7ZPwVRGQg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ad-PkBWPEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_lhjscBWOEeC7l7ZPwVRGQg" modifier="CREATE" outgoingStoryLinks="_0s5OMBWOEeC7l7ZPwVRGQg" incomingStoryLinks="_zsmO0BWOEeC7l7ZPwVRGQg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varRef" uuid="_mnARkBWOEeC7l7ZPwVRGQg" modifier="CREATE" incomingStoryLinks="_0s5OMBWOEeC7l7ZPwVRGQg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_1VZ_sBWOEeC7l7ZPwVRGQg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5S8zUBWOEeC7l7ZPwVRGQg" expressionString="'parentCorrNode'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_7GmiQBWOEeC7l7ZPwVRGQg">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_79z3EBWOEeC7l7ZPwVRGQg" expressionString="mote::TGGNode" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zsmO0BWOEeC7l7ZPwVRGQg" modifier="CREATE" source="_ktMQQBWOEeC7l7ZPwVRGQg" target="_lhjscBWOEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0s5OMBWOEeC7l7ZPwVRGQg" modifier="CREATE" source="_lhjscBWOEeC7l7ZPwVRGQg" target="_mnARkBWOEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_Oz_z8ClZEeCFgb_w6FqGaA" source="_ivDvoBWOEeC7l7ZPwVRGQg" target="_kDBR4BWOEeC7l7ZPwVRGQg" valueTarget="_ktMQQBWOEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_VhVtAPxnEd--caqh5zaNfg" source="_Cu6v0PxnEd--caqh5zaNfg" target="_DXvDUPxnEd--caqh5zaNfg"/>
    <edges uuid="_ebp6YBWGEeCv-a_w4vT5EQ" source="_DXvDUPxnEd--caqh5zaNfg" target="_b2yJ4BWGEeCv-a_w4vT5EQ"/>
    <edges uuid="_vWEXoBWHEeCv-a_w4vT5EQ" source="_b2yJ4BWGEeCv-a_w4vT5EQ" target="_UDT7oBWIEeC7l7ZPwVRGQg"/>
    <edges uuid="_wjPVYBWHEeCv-a_w4vT5EQ" source="_DMwqsBWHEeCv-a_w4vT5EQ" target="_sQKhMBWIEeC7l7ZPwVRGQg"/>
    <edges uuid="_XEOu8BWIEeC7l7ZPwVRGQg" source="_UDT7oBWIEeC7l7ZPwVRGQg" target="_DMwqsBWHEeCv-a_w4vT5EQ"/>
    <edges uuid="_1O1P4BWJEeC7l7ZPwVRGQg" source="_sQKhMBWIEeC7l7ZPwVRGQg" target="_DudiMBWKEeC7l7ZPwVRGQg"/>
    <edges uuid="_kSsCYBWKEeC7l7ZPwVRGQg" source="_DudiMBWKEeC7l7ZPwVRGQg" target="_ruPN8BWKEeC7l7ZPwVRGQg"/>
    <edges uuid="_M85LMBWLEeC7l7ZPwVRGQg" source="_ruPN8BWKEeC7l7ZPwVRGQg" target="_W_kxEBWLEeC7l7ZPwVRGQg"/>
    <edges uuid="_fAJf0BWMEeC7l7ZPwVRGQg" source="_W_kxEBWLEeC7l7ZPwVRGQg" target="_y7SYkBWMEeC7l7ZPwVRGQg"/>
    <edges uuid="_-llcgBWMEeC7l7ZPwVRGQg" source="_y7SYkBWMEeC7l7ZPwVRGQg" target="_5zCTgBWMEeC7l7ZPwVRGQg" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IrsJ0BWNEeC7l7ZPwVRGQg" expressionString="direction = mote::TransformationDirection::FORWARD or&#xA;direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_El9DgBWNEeC7l7ZPwVRGQg" source="_5zCTgBWMEeC7l7ZPwVRGQg" target="__x5eoBWMEeC7l7ZPwVRGQg"/>
    <edges uuid="_GJ2fMBWNEeC7l7ZPwVRGQg" source="_1zzoYBWNEeC7l7ZPwVRGQg" target="_FbXBgBWNEeC7l7ZPwVRGQg"/>
    <edges uuid="_HAZssBWNEeC7l7ZPwVRGQg" source="_y7SYkBWMEeC7l7ZPwVRGQg" target="_FbXBgBWNEeC7l7ZPwVRGQg" guardType="ELSE"/>
    <edges uuid="_NYyLEBWNEeC7l7ZPwVRGQg" source="_FbXBgBWNEeC7l7ZPwVRGQg" target="_ME6vcBWNEeC7l7ZPwVRGQg"/>
    <edges uuid="_UI7CIBWNEeC7l7ZPwVRGQg" source="_ME6vcBWNEeC7l7ZPwVRGQg" target="_N6cGoBWNEeC7l7ZPwVRGQg" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_aDuzsBWNEeC7l7ZPwVRGQg" expressionString="direction = mote::TransformationDirection::MAPPING or&#xA;direction = mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_UiUZkBWNEeC7l7ZPwVRGQg" source="_N6cGoBWNEeC7l7ZPwVRGQg" target="_Q4VbIBWNEeC7l7ZPwVRGQg"/>
    <edges uuid="_VqI9QBWNEeC7l7ZPwVRGQg" source="_QHBdjxWOEeC7l7ZPwVRGQg" target="_VGzmsBWNEeC7l7ZPwVRGQg"/>
    <edges uuid="_WmfQwBWNEeC7l7ZPwVRGQg" source="_ME6vcBWNEeC7l7ZPwVRGQg" target="_VGzmsBWNEeC7l7ZPwVRGQg" guardType="ELSE"/>
    <edges uuid="_X-7DUBWNEeC7l7ZPwVRGQg" source="_VGzmsBWNEeC7l7ZPwVRGQg" target="_d7SeoBWOEeC7l7ZPwVRGQg"/>
    <edges uuid="_Uib44BWOEeC7l7ZPwVRGQg" source="__x5eoBWMEeC7l7ZPwVRGQg" target="_1zzoYBWNEeC7l7ZPwVRGQg"/>
    <edges uuid="_U2ePMBWOEeC7l7ZPwVRGQg" source="_Q4VbIBWNEeC7l7ZPwVRGQg" target="_QHBdjxWOEeC7l7ZPwVRGQg"/>
    <edges uuid="_EKUlgBWPEeC7l7ZPwVRGQg" source="_d7SeoBWOEeC7l7ZPwVRGQg" target="_wARLMBWHEeCv-a_w4vT5EQ"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
