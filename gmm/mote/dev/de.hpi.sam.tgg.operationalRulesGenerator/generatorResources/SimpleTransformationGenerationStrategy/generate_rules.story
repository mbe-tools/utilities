<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="Simple Transformation Generation Strategy" uuid="_f2pxUXUrEd-AwpZ_Fdb-iA">
  <activities name="generate_rules" uuid="_Ps8bcHefEd-UQsRE6oiJVQ">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_c7k_8HefEd-UQsRE6oiJVQ" outgoing="_HhaH8HegEd-UQsRE6oiJVQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story diagram and traceability link store" uuid="_soasQHefEd-UQsRE6oiJVQ" incoming="_HhaH8HegEd-UQsRE6oiJVQ" outgoing="_hxg_4He6Ed-4c678KwnPnw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggDiagram" uuid="_vYXEUHefEd-UQsRE6oiJVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_zgQt0HefEd-UQsRE6oiJVQ" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesPackage" uuid="_MW-vwHegEd-UQsRE6oiJVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="motePackage" uuid="_BwVrwHehEd-UQsRE6oiJVQ" outgoingStoryLinks="_eG21YHehEd-UQsRE6oiJVQ _0L7J4HezEd-4c678KwnPnw _PpojIPI9Ed-7z74s9UiuPA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleEClass" uuid="_cQvBQHehEd-UQsRE6oiJVQ" outgoingStoryLinks="_qbLGsHehEd-UQsRE6oiJVQ _qtNR4HehEd-UQsRE6oiJVQ _q8PMYHehEd-UQsRE6oiJVQ _fn2DgAt8EeC6TdZK2gc2HA" incomingStoryLinks="_eG21YHehEd-UQsRE6oiJVQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ksz1sHehEd-UQsRE6oiJVQ" expressionString="self.name = 'TGGRule'&#xD;&#xA;" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ft1" uuid="_eoVKwHehEd-UQsRE6oiJVQ" incomingStoryLinks="_qbLGsHehEd-UQsRE6oiJVQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_i_3ywHehEd-UQsRE6oiJVQ" expressionString="self.name = 'forwardTransformation'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mt1" uuid="_fFBGwHehEd-UQsRE6oiJVQ" incomingStoryLinks="_qtNR4HehEd-UQsRE6oiJVQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mWtywHehEd-UQsRE6oiJVQ" expressionString="self.name = 'mappingTransformation'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rt1" uuid="_f2XrMHehEd-UQsRE6oiJVQ" incomingStoryLinks="_q8PMYHehEd-UQsRE6oiJVQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_n89X0HehEd-UQsRE6oiJVQ" expressionString="self.name = 'reverseTransformation'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggAxiomEClass" uuid="_xukWoHezEd-4c678KwnPnw" outgoingStoryLinks="_m0wXoHe3Ed-4c678KwnPnw _nu2oYHe3Ed-4c678KwnPnw _oeGUAHe3Ed-4c678KwnPnw" incomingStoryLinks="_0L7J4HezEd-4c678KwnPnw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tb6qIHe9Ed-INsK5Dz3ubg" expressionString="self.name = 'TGGAxiom'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="fta1" uuid="_11DD0HezEd-4c678KwnPnw" incomingStoryLinks="_m0wXoHe3Ed-4c678KwnPnw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hzkVsHe3Ed-4c678KwnPnw" expressionString="self.name = 'forwardTransformation'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mta1" uuid="_4LJ_kHezEd-4c678KwnPnw" incomingStoryLinks="_nu2oYHe3Ed-4c678KwnPnw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jh-_UHe3Ed-4c678KwnPnw" expressionString="self.name = 'mappingTransformation'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rta1" uuid="__W8Q0HezEd-4c678KwnPnw" incomingStoryLinks="_oeGUAHe3Ed-4c678KwnPnw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lOxRQHe3Ed-4c678KwnPnw" expressionString="self.name = 'reverseTransformation'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleSetEClass" uuid="_L1OdYPI9Ed-7z74s9UiuPA" outgoingStoryLinks="_oNf7MPh3Ed-R0aK4IAlsBA" incomingStoryLinks="_PpojIPI9Ed-7z74s9UiuPA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y0_fcPI9Ed-7z74s9UiuPA" expressionString="self.name = 'TGGRuleSet'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createRulesOperation" uuid="_jENtMPh3Ed-R0aK4IAlsBA" incomingStoryLinks="_oNf7MPh3Ed-R0aK4IAlsBA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_k7disPh3Ed-R0aK4IAlsBA" expressionString="self.name = 'createRules'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="projectName" uuid="_ygKrkPh8Ed-R0aK4IAlsBA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="acceptsParentNodeOperation" uuid="_csV7sAt8EeC6TdZK2gc2HA" incomingStoryLinks="_fn2DgAt8EeC6TdZK2gc2HA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gqhogAt8EeC6TdZK2gc2HA" expressionString="self.name = 'acceptsParentCorrNode'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="basePackage" uuid="_AzzxwAuAEeClm-1TBJQXDQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_eG21YHehEd-UQsRE6oiJVQ" source="_BwVrwHehEd-UQsRE6oiJVQ" target="_cQvBQHehEd-UQsRE6oiJVQ"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qbLGsHehEd-UQsRE6oiJVQ" source="_cQvBQHehEd-UQsRE6oiJVQ" target="_eoVKwHehEd-UQsRE6oiJVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qtNR4HehEd-UQsRE6oiJVQ" source="_cQvBQHehEd-UQsRE6oiJVQ" target="_fFBGwHehEd-UQsRE6oiJVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_q8PMYHehEd-UQsRE6oiJVQ" source="_cQvBQHehEd-UQsRE6oiJVQ" target="_f2XrMHehEd-UQsRE6oiJVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_0L7J4HezEd-4c678KwnPnw" source="_BwVrwHehEd-UQsRE6oiJVQ" target="_xukWoHezEd-4c678KwnPnw"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_m0wXoHe3Ed-4c678KwnPnw" source="_xukWoHezEd-4c678KwnPnw" target="_11DD0HezEd-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_nu2oYHe3Ed-4c678KwnPnw" source="_xukWoHezEd-4c678KwnPnw" target="_4LJ_kHezEd-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_oeGUAHe3Ed-4c678KwnPnw" source="_xukWoHezEd-4c678KwnPnw" target="__W8Q0HezEd-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_PpojIPI9Ed-7z74s9UiuPA" source="_BwVrwHehEd-UQsRE6oiJVQ" target="_L1OdYPI9Ed-7z74s9UiuPA"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_oNf7MPh3Ed-R0aK4IAlsBA" source="_L1OdYPI9Ed-7z74s9UiuPA" target="_jENtMPh3Ed-R0aK4IAlsBA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fn2DgAt8EeC6TdZK2gc2HA" source="_cQvBQHehEd-UQsRE6oiJVQ" target="_csV7sAt8EeC6TdZK2gc2HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="__itc8HeoEd-4c678KwnPnw" incoming="_DrqqEPiEEd-StusXmfUQQA">
      <outParameterValues xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZpV-4He6Ed-4c678KwnPnw" expressionString="traceabilityLinkStore.traceabilityLinks.targets->select(e|e.oclIsKindOf(storyDiagramEcore::ActivityDiagram))" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create elements for axioms" uuid="_bs6EAHe4Ed-4c678KwnPnw" incoming="_6bInsHe_Ed-INsK5Dz3ubg _gHh2wPcQEd-XR4VMarhb7w" outgoing="_ViuK8He-Ed-INsK5Dz3ubg _Y4ZigHhcEd-NMp17Gm153Q" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggDiagram" uuid="_c19WwHe4Ed-4c678KwnPnw" outgoingStoryLinks="_maK38He4Ed-4c678KwnPnw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_eKnckHe4Ed-4c678KwnPnw" incomingStoryLinks="_maK38He4Ed-4c678KwnPnw _kQjDkHe5Ed-4c678KwnPnw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_ne4dQHe4Ed-4c678KwnPnw" outgoingStoryLinks="_iNyhoHe5Ed-4c678KwnPnw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesPackage" uuid="_tGxS0He4Ed-4c678KwnPnw" outgoingStoryLinks="_7JowoHe4Ed-4c678KwnPnw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleEClass" uuid="_vqWp4He4Ed-4c678KwnPnw" modifier="CREATE" incomingStoryLinks="_7JowoHe4Ed-4c678KwnPnw _i-AdoHe5Ed-4c678KwnPnw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <attributeAssignments uuid="_ykCz0He4Ed-4c678KwnPnw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//ENamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0fudYHe4Ed-4c678KwnPnw" expressionString="tggRule.name" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tl" uuid="_gyGmwHe5Ed-4c678KwnPnw" modifier="CREATE" outgoingStoryLinks="_i-AdoHe5Ed-4c678KwnPnw _kQjDkHe5Ed-4c678KwnPnw _LOgnMPIzEd-pLqh2_twcbw _NW3a0PIzEd-pLqh2_twcbw _PToCcPIzEd-pLqh2_twcbw" incomingStoryLinks="_iNyhoHe5Ed-4c678KwnPnw">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardStoryDiagram" uuid="_4yKAoPIyEd-pLqh2_twcbw" modifier="CREATE" incomingStoryLinks="_LOgnMPIzEd-pLqh2_twcbw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_RBZL0PIzEd-pLqh2_twcbw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_STE2IPIzEd-pLqh2_twcbw" expressionString="tggRule.name.concat('_forwardTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingStoryDiagram" uuid="_5wcN0PIyEd-pLqh2_twcbw" modifier="CREATE" incomingStoryLinks="_NW3a0PIzEd-pLqh2_twcbw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_ZQhwcPIzEd-pLqh2_twcbw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_aOhpwPIzEd-pLqh2_twcbw" expressionString="tggRule.name.concat('_mappingTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseStoryDiagram" uuid="_6vbloPIyEd-pLqh2_twcbw" modifier="CREATE" incomingStoryLinks="_PToCcPIzEd-pLqh2_twcbw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_cPeN0PIzEd-pLqh2_twcbw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dMpAsPIzEd-pLqh2_twcbw" expressionString="tggRule.name.concat('_reverseTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_maK38He4Ed-4c678KwnPnw" source="_c19WwHe4Ed-4c678KwnPnw" target="_eKnckHe4Ed-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGDiagram/tggRules"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7JowoHe4Ed-4c678KwnPnw" modifier="CREATE" source="_tGxS0He4Ed-4c678KwnPnw" target="_vqWp4He4Ed-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage/eClassifiers"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iNyhoHe5Ed-4c678KwnPnw" modifier="CREATE" source="_ne4dQHe4Ed-4c678KwnPnw" target="_gyGmwHe5Ed-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/traceabilityLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_i-AdoHe5Ed-4c678KwnPnw" modifier="CREATE" source="_gyGmwHe5Ed-4c678KwnPnw" target="_vqWp4He4Ed-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLink/targets"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kQjDkHe5Ed-4c678KwnPnw" modifier="CREATE" source="_gyGmwHe5Ed-4c678KwnPnw" target="_eKnckHe4Ed-4c678KwnPnw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLink/sources"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LOgnMPIzEd-pLqh2_twcbw" modifier="CREATE" source="_gyGmwHe5Ed-4c678KwnPnw" target="_4yKAoPIyEd-pLqh2_twcbw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLink/targets"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_NW3a0PIzEd-pLqh2_twcbw" modifier="CREATE" source="_gyGmwHe5Ed-4c678KwnPnw" target="_5wcN0PIyEd-pLqh2_twcbw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLink/targets"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_PToCcPIzEd-pLqh2_twcbw" modifier="CREATE" source="_gyGmwHe5Ed-4c678KwnPnw" target="_6vbloPIyEd-pLqh2_twcbw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLink/targets"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="clone EOperations for axiom" uuid="_99wEwHe9Ed-INsK5Dz3ubg" incoming="_LURZ8He-Ed-INsK5Dz3ubg" outgoing="_9IFSYPPPEd-NkIb-Q3PMvQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggAxiomEClass" uuid="_XOqvYHe-Ed-INsK5Dz3ubg" incomingStoryLinks="_bv0mYHe-Ed-INsK5Dz3ubg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleEClass" uuid="_aHmrYHe-Ed-INsK5Dz3ubg" outgoingStoryLinks="_bv0mYHe-Ed-INsK5Dz3ubg _9gMJkHe-Ed-INsK5Dz3ubg _-qI0MHe-Ed-INsK5Dz3ubg __wbt4He-Ed-INsK5Dz3ubg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardOperation" uuid="_ePa1UHe-Ed-INsK5Dz3ubg" incomingStoryLinks="_9gMJkHe-Ed-INsK5Dz3ubg _EaTcAHe_Ed-INsK5Dz3ubg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_p1drYHe-Ed-INsK5Dz3ubg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_qsA44He-Ed-INsK5Dz3ubg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters uuid="_tLPUYHe-Ed-INsK5Dz3ubg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_uHb24He-Ed-INsK5Dz3ubg" expressionString="fta1" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseOperation" uuid="_gE_P0He-Ed-INsK5Dz3ubg" incomingStoryLinks="__wbt4He-Ed-INsK5Dz3ubg _KDQokHe_Ed-INsK5Dz3ubg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_4PtHYHe-Ed-INsK5Dz3ubg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_46DE4He-Ed-INsK5Dz3ubg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters uuid="_7QCE0He-Ed-INsK5Dz3ubg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8Sp_cHe-Ed-INsK5Dz3ubg" expressionString="rta1" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingOperation" uuid="_wLQv0He-Ed-INsK5Dz3ubg" incomingStoryLinks="_-qI0MHe-Ed-INsK5Dz3ubg _H8VYUHe_Ed-INsK5Dz3ubg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_ykxk4He-Ed-INsK5Dz3ubg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_zJxvYHe-Ed-INsK5Dz3ubg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters uuid="_1jqX4He-Ed-INsK5Dz3ubg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2b0H4He-Ed-INsK5Dz3ubg" expressionString="mta1" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardActivity" uuid="_CpsaAHe_Ed-INsK5Dz3ubg" outgoingStoryLinks="_EaTcAHe_Ed-INsK5Dz3ubg" incomingStoryLinks="_0HjlsPPSEd-cf4nuhI1Eog" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="__GoCsPPPEd-NkIb-Q3PMvQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BOAl8PPQEd-NkIb-Q3PMvQ" expressionString="forwardStoryDiagram.name" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_tgPa0PPPEd-NkIb-Q3PMvQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_t_W_wPPPEd-NkIb-Q3PMvQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
            <activity href="generate_axiom_activity.story#_NTSQ0HhtEd-F3I0fuD-eTQ"/>
            <parameters name="tggRule" uuid="_xKoB4PPPEd-NkIb-Q3PMvQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_zu9m4PPPEd-NkIb-Q3PMvQ" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_2AfkoPPPEd-NkIb-Q3PMvQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_4ZOWkPPPEd-NkIb-Q3PMvQ" expressionString="mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingActivity" uuid="_FB8q0He_Ed-INsK5Dz3ubg" outgoingStoryLinks="_H8VYUHe_Ed-INsK5Dz3ubg" incomingStoryLinks="_1b3GMPPSEd-cf4nuhI1Eog" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_bewD8PPSEd-cf4nuhI1Eog">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ceg30PPSEd-cf4nuhI1Eog" expressionString="mappingStoryDiagram.name" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_Dy8n4PPQEd-NkIb-Q3PMvQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_E_AyYPPQEd-NkIb-Q3PMvQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
            <activity href="generate_axiom_activity.story#_NTSQ0HhtEd-F3I0fuD-eTQ"/>
            <parameters name="tggRule" uuid="_GMq4UPPQEd-NkIb-Q3PMvQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IG5JUPPQEd-NkIb-Q3PMvQ" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_JV7vUPPQEd-NkIb-Q3PMvQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_LMj5wPPQEd-NkIb-Q3PMvQ" expressionString="mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseActivity" uuid="_IdXo0He_Ed-INsK5Dz3ubg" outgoingStoryLinks="_KDQokHe_Ed-INsK5Dz3ubg" incomingStoryLinks="_1zwBwPPSEd-cf4nuhI1Eog" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_eXPUAPPSEd-cf4nuhI1Eog">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_e7cNQPPSEd-cf4nuhI1Eog" expressionString="reverseStoryDiagram.name" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_QJoQsPPQEd-NkIb-Q3PMvQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_Qxh-MPPQEd-NkIb-Q3PMvQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
            <activity href="generate_axiom_activity.story#_NTSQ0HhtEd-F3I0fuD-eTQ"/>
            <parameters name="tggRule" uuid="_SE_xMPPQEd-NkIb-Q3PMvQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_UHTAIPPQEd-NkIb-Q3PMvQ" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_VTNZoPPQEd-NkIb-Q3PMvQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XIxNEPPQEd-NkIb-Q3PMvQ" expressionString="mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardStoryDiagram" uuid="_pTls0PPSEd-cf4nuhI1Eog" outgoingStoryLinks="_0HjlsPPSEd-cf4nuhI1Eog" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingStoryDiagram" uuid="_uzcAsPPSEd-cf4nuhI1Eog" outgoingStoryLinks="_1b3GMPPSEd-cf4nuhI1Eog" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseStoryDiagram" uuid="_vrS1wPPSEd-cf4nuhI1Eog" outgoingStoryLinks="_1zwBwPPSEd-cf4nuhI1Eog" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_bv0mYHe-Ed-INsK5Dz3ubg" modifier="CREATE" source="_aHmrYHe-Ed-INsK5Dz3ubg" target="_XOqvYHe-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eSuperTypes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9gMJkHe-Ed-INsK5Dz3ubg" modifier="CREATE" source="_aHmrYHe-Ed-INsK5Dz3ubg" target="_ePa1UHe-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_-qI0MHe-Ed-INsK5Dz3ubg" modifier="CREATE" source="_aHmrYHe-Ed-INsK5Dz3ubg" target="_wLQv0He-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="__wbt4He-Ed-INsK5Dz3ubg" modifier="CREATE" source="_aHmrYHe-Ed-INsK5Dz3ubg" target="_gE_P0He-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_EaTcAHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_CpsaAHe_Ed-INsK5Dz3ubg" target="_ePa1UHe-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_H8VYUHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_FB8q0He_Ed-INsK5Dz3ubg" target="_wLQv0He-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_KDQokHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_IdXo0He_Ed-INsK5Dz3ubg" target="_gE_P0He-Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0HjlsPPSEd-cf4nuhI1Eog" modifier="CREATE" source="_pTls0PPSEd-cf4nuhI1Eog" target="_CpsaAHe_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1b3GMPPSEd-cf4nuhI1Eog" modifier="CREATE" source="_uzcAsPPSEd-cf4nuhI1Eog" target="_FB8q0He_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1zwBwPPSEd-cf4nuhI1Eog" modifier="CREATE" source="_vrS1wPPSEd-cf4nuhI1Eog" target="_IdXo0He_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_JQQ60He-Ed-INsK5Dz3ubg" incoming="_aNDBQHhcEd-NMp17Gm153Q" outgoing="_LURZ8He-Ed-INsK5Dz3ubg _1xeU8He_Ed-INsK5Dz3ubg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="clone EOperations for rules" uuid="_ONhpkHe_Ed-INsK5Dz3ubg" incoming="_1xeU8He_Ed-INsK5Dz3ubg" outgoing="_5uouEHe_Ed-INsK5Dz3ubg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleEClass" uuid="_QNlc0He_Ed-INsK5Dz3ubg" incomingStoryLinks="_TzQjAHe_Ed-INsK5Dz3ubg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleEClass" uuid="_SVxRUHe_Ed-INsK5Dz3ubg" outgoingStoryLinks="_TzQjAHe_Ed-INsK5Dz3ubg _retrsHe_Ed-INsK5Dz3ubg _sS46oHe_Ed-INsK5Dz3ubg _tOBtMHe_Ed-INsK5Dz3ubg _wxr5UAt8EeC6TdZK2gc2HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardOperation" uuid="_U0_s0He_Ed-INsK5Dz3ubg" incomingStoryLinks="_retrsHe_Ed-INsK5Dz3ubg _y7VOQHe_Ed-INsK5Dz3ubg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_WlKC4He_Ed-INsK5Dz3ubg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_XDdIcHe_Ed-INsK5Dz3ubg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters uuid="_ZfY_YHe_Ed-INsK5Dz3ubg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_aYnGYHe_Ed-INsK5Dz3ubg" expressionString="ft1" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingOperation" uuid="_byEMoHe_Ed-INsK5Dz3ubg" incomingStoryLinks="_sS46oHe_Ed-INsK5Dz3ubg _zkMlEHe_Ed-INsK5Dz3ubg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_e8gvYHe_Ed-INsK5Dz3ubg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_fcjhYHe_Ed-INsK5Dz3ubg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters uuid="_hdQ04He_Ed-INsK5Dz3ubg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_iPYOUHe_Ed-INsK5Dz3ubg" expressionString="mt1" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseOperation" uuid="_jYpjgHe_Ed-INsK5Dz3ubg" incomingStoryLinks="_tOBtMHe_Ed-INsK5Dz3ubg _0BHxoHe_Ed-INsK5Dz3ubg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_lt3ucHe_Ed-INsK5Dz3ubg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_mToG4He_Ed-INsK5Dz3ubg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters uuid="_oVZxYHe_Ed-INsK5Dz3ubg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_pZr44He_Ed-INsK5Dz3ubg" expressionString="rt1" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardActivity" uuid="_uUx54He_Ed-INsK5Dz3ubg" outgoingStoryLinks="_y7VOQHe_Ed-INsK5Dz3ubg" incomingStoryLinks="_V_CdcPl5Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_zPnbsAt5EeC4qM2gBSDt-A">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0lu6EAt5EeC4qM2gBSDt-A" expressionString="forwardStoryDiagram.name" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_ZrIKIPl4Ed-8sINpd2aU-w">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_aDV10Pl4Ed-8sINpd2aU-w">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
            <activity href="generate_rule_activity.story#_Ej074PlaEd-fSKxQcXgteA"/>
            <parameters name="tggRule" uuid="_cKJKQPl4Ed-8sINpd2aU-w">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dwMiEPl4Ed-8sINpd2aU-w" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_fiHysPl4Ed-8sINpd2aU-w">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hy5igPl4Ed-8sINpd2aU-w" expressionString="mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingActivity" uuid="_v53BUHe_Ed-INsK5Dz3ubg" outgoingStoryLinks="_zkMlEHe_Ed-INsK5Dz3ubg" incomingStoryLinks="_WWNnUPl5Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_zsJmsAt5EeC4qM2gBSDt-A">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3YhKwAt5EeC4qM2gBSDt-A" expressionString="mappingStoryDiagram.name" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_l6ZjYPl4Ed-8sINpd2aU-w">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_mK9H4Pl4Ed-8sINpd2aU-w">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
            <activity href="generate_rule_activity.story#_Ej074PlaEd-fSKxQcXgteA"/>
            <parameters name="tggRule" uuid="_ni2u8Pl4Ed-8sINpd2aU-w">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_o0TIsPl4Ed-8sINpd2aU-w" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_pzCB0Pl4Ed-8sINpd2aU-w">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rwqMIPl4Ed-8sINpd2aU-w" expressionString="mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseActivity" uuid="_xbXQUHe_Ed-INsK5Dz3ubg" outgoingStoryLinks="_0BHxoHe_Ed-INsK5Dz3ubg" incomingStoryLinks="_WkhJEPl5Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_z6rx8At5EeC4qM2gBSDt-A">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_49MCgAt5EeC4qM2gBSDt-A" expressionString="reverseStoryDiagram.name" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_wKlZwPl4Ed-8sINpd2aU-w">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_wexhEPl4Ed-8sINpd2aU-w">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
            <activity href="generate_rule_activity.story#_Ej074PlaEd-fSKxQcXgteA"/>
            <parameters name="tggRule" uuid="_x-wGEPl4Ed-8sINpd2aU-w">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_zXeMgPl4Ed-8sINpd2aU-w" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_0bpmUPl4Ed-8sINpd2aU-w">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2anNYPl4Ed-8sINpd2aU-w" expressionString="mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardStoryDiagram" uuid="_OAvKIPl5Ed-8sINpd2aU-w" outgoingStoryLinks="_V_CdcPl5Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingStoryDiagram" uuid="_PQPDIPl5Ed-8sINpd2aU-w" outgoingStoryLinks="_WWNnUPl5Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseStoryDiagram" uuid="_QCJoQPl5Ed-8sINpd2aU-w" outgoingStoryLinks="_WkhJEPl5Ed-8sINpd2aU-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="clonedAcceptsParentCorrNodeOperation" uuid="_sb0FQAt8EeC6TdZK2gc2HA" outgoingStoryLinks="_BgRWgAt9EeC6TdZK2gc2HA" incomingStoryLinks="_wxr5UAt8EeC6TdZK2gc2HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_0TDuUAt8EeC6TdZK2gc2HA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_0wVgMAt8EeC6TdZK2gc2HA" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
            <parameters name="" uuid="_3Z6KgAt8EeC6TdZK2gc2HA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5ONZsAt8EeC6TdZK2gc2HA" expressionString="acceptsParentNodeOperation" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="annotation" uuid="__7-SMAt8EeC6TdZK2gc2HA" outgoingStoryLinks="_oTMNkAt9EeC6TdZK2gc2HA" incomingStoryLinks="_BgRWgAt9EeC6TdZK2gc2HA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_DGtI0At9EeC6TdZK2gc2HA" expressionString="self.source ='http://www.eclipse.org/emf/2002/GenModel'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="detailsEntry" uuid="_Dm99QAt9EeC6TdZK2gc2HA" modifier="CREATE" incomingStoryLinks="_oTMNkAt9EeC6TdZK2gc2HA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry"/>
        <attributeAssignments uuid="_HPXnUAt9EeC6TdZK2gc2HA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/key"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IiWSIAt9EeC6TdZK2gc2HA" expressionString="'body'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_Kb3_kAt9EeC6TdZK2gc2HA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/value"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_LV-3YAt9EeC6TdZK2gc2HA">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_L5EWUAt9EeC6TdZK2gc2HA" methodClassName="de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationHelpers" methodName="generateAcceptsParentNodeCode">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              <parameters name="" uuid="_hpYoAAt9EeC6TdZK2gc2HA">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lpFXwAt9EeC6TdZK2gc2HA" expressionString="basePackage.concat('.').concat(rulesPackage.name)" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_8YvUoAt_EeClm-1TBJQXDQ">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9YgvkAt_EeClm-1TBJQXDQ" expressionString="tggRule" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
              </parameters>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_TzQjAHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_SVxRUHe_Ed-INsK5Dz3ubg" target="_QNlc0He_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eSuperTypes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_retrsHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_SVxRUHe_Ed-INsK5Dz3ubg" target="_U0_s0He_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_sS46oHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_SVxRUHe_Ed-INsK5Dz3ubg" target="_byEMoHe_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tOBtMHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_SVxRUHe_Ed-INsK5Dz3ubg" target="_jYpjgHe_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_y7VOQHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_uUx54He_Ed-INsK5Dz3ubg" target="_U0_s0He_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zkMlEHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_v53BUHe_Ed-INsK5Dz3ubg" target="_byEMoHe_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0BHxoHe_Ed-INsK5Dz3ubg" modifier="CREATE" source="_xbXQUHe_Ed-INsK5Dz3ubg" target="_jYpjgHe_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_V_CdcPl5Ed-8sINpd2aU-w" modifier="CREATE" source="_OAvKIPl5Ed-8sINpd2aU-w" target="_uUx54He_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WWNnUPl5Ed-8sINpd2aU-w" modifier="CREATE" source="_PQPDIPl5Ed-8sINpd2aU-w" target="_v53BUHe_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WkhJEPl5Ed-8sINpd2aU-w" modifier="CREATE" source="_QCJoQPl5Ed-8sINpd2aU-w" target="_xbXQUHe_Ed-INsK5Dz3ubg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_wxr5UAt8EeC6TdZK2gc2HA" modifier="CREATE" source="_SVxRUHe_Ed-INsK5Dz3ubg" target="_sb0FQAt8EeC6TdZK2gc2HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BgRWgAt9EeC6TdZK2gc2HA" source="_sb0FQAt8EeC6TdZK2gc2HA" target="__7-SMAt8EeC6TdZK2gc2HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EModelElement/eAnnotations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_oTMNkAt9EeC6TdZK2gc2HA" modifier="CREATE" source="__7-SMAt8EeC6TdZK2gc2HA" target="_Dm99QAt9EeC6TdZK2gc2HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation/details"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_5NLm0He_Ed-INsK5Dz3ubg" incoming="_5uouEHe_Ed-INsK5Dz3ubg _9IFSYPPPEd-NkIb-Q3PMvQ" outgoing="_upKacPigEd-bw8R_eH98gw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="delete old EClasses" uuid="_WmaRwHhcEd-NMp17Gm153Q" incoming="_Y4ZigHhcEd-NMp17Gm153Q" outgoing="_aNDBQHhcEd-NMp17Gm153Q" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesPackage" uuid="_cUufcHhcEd-NMp17Gm153Q" outgoingStoryLinks="_i6M-oHhcEd-NMp17Gm153Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleEClass" uuid="_eGsMUHhcEd-NMp17Gm153Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="existingEClass" uuid="_gauEcHhcEd-NMp17Gm153Q" modifier="DESTROY" incomingStoryLinks="_i6M-oHhcEd-NMp17Gm153Q">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kGrY0HhcEd-NMp17Gm153Q" expressionString="self.name = ruleEClass.name" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_i6M-oHhcEd-NMp17Gm153Q" modifier="DESTROY" source="_cUufcHhcEd-NMp17Gm153Q" target="_gauEcHhcEd-NMp17Gm153Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage/eClassifiers"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="delete old ruleSet EClass" uuid="_YRThYPcQEd-XR4VMarhb7w" incoming="_hxg_4He6Ed-4c678KwnPnw" outgoing="_em_OANiREeC7T7T-Qy6ZGA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="oldRuleSetEClass" uuid="_acJoUPcQEd-XR4VMarhb7w" modifier="DESTROY" outgoingStoryLinks="_wNVcsPcQEd-XR4VMarhb7w" incomingStoryLinks="_rROvEPcQEd-XR4VMarhb7w">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesPackage" uuid="_cwr2wPcQEd-XR4VMarhb7w" outgoingStoryLinks="_rROvEPcQEd-XR4VMarhb7w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eClass" uuid="_uS2tAPcQEd-XR4VMarhb7w" incomingStoryLinks="_wNVcsPcQEd-XR4VMarhb7w">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mctIgPeyEd-Na8LTUpHsgA" expressionString="self.name = 'TGGRuleSet'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_rROvEPcQEd-XR4VMarhb7w" modifier="DESTROY" source="_cwr2wPcQEd-XR4VMarhb7w" target="_acJoUPcQEd-XR4VMarhb7w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage/eClassifiers"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_wNVcsPcQEd-XR4VMarhb7w" modifier="DESTROY" source="_acJoUPcQEd-XR4VMarhb7w" target="_uS2tAPcQEd-XR4VMarhb7w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eSuperTypes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create new ruleSet EClass" uuid="_eUP7QPcQEd-XR4VMarhb7w" incoming="_fcNB0PcQEd-XR4VMarhb7w" outgoing="_gHh2wPcQEd-XR4VMarhb7w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesPackage" uuid="_izBzsPcQEd-XR4VMarhb7w" outgoingStoryLinks="_0qQT8PcQEd-XR4VMarhb7w _77Y8MNiREeC7T7T-Qy6ZGA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="generatedRuleSetEClass" uuid="_QpKGcPI9Ed-7z74s9UiuPA" modifier="CREATE" outgoingStoryLinks="_1l7SAPcQEd-XR4VMarhb7w _P7W14PiLEd-S_r9zK9ICeQ _Ld6EUNiREeC7T7T-Qy6ZGA" incomingStoryLinks="_0qQT8PcQEd-XR4VMarhb7w">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <attributeAssignments uuid="_c6vz4PI9Ed-7z74s9UiuPA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//ENamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fho9oPI9Ed-7z74s9UiuPA" expressionString="tggDiagram.name.substring(1,1).toUpper().concat(tggDiagram.name.substring(2, tggDiagram.name.size())).concat('RuleSet')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleSetEClass" uuid="_lu7kIPcQEd-XR4VMarhb7w" incomingStoryLinks="_1l7SAPcQEd-XR4VMarhb7w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="resourceSetAttribute" uuid="_HnrvgPiLEd-S_r9zK9ICeQ" modifier="CREATE" incomingStoryLinks="_P7W14PiLEd-S_r9zK9ICeQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAttribute"/>
        <attributeAssignments uuid="_KJe94PiLEd-S_r9zK9ICeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//ENamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NpIUkPiLEd-S_r9zK9ICeQ" expressionString="'resourceSet'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_K29H4PiLEd-S_r9zK9ICeQ">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//ETypedElement/eType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_MEIFoPiLEd-S_r9zK9ICeQ" expressionString="ecore::EResourceSet" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="interpreterAttribute" uuid="_FW30oNiREeC7T7T-Qy6ZGA" modifier="CREATE" outgoingStoryLinks="_-uPeUNiREeC7T7T-Qy6ZGA" incomingStoryLinks="_Ld6EUNiREeC7T7T-Qy6ZGA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAttribute"/>
        <attributeAssignments uuid="_AbXugNiSEeC7T7T-Qy6ZGA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//ENamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BmRbYNiSEeC7T7T-Qy6ZGA" expressionString="'sdmInterpreter'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="interpreterDataType" uuid="_mhg0kNiREeC7T7T-Qy6ZGA" modifier="CREATE" incomingStoryLinks="_77Y8MNiREeC7T7T-Qy6ZGA _-uPeUNiREeC7T7T-Qy6ZGA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EDataType"/>
        <attributeAssignments uuid="_obvsoNiREeC7T7T-Qy6ZGA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//ENamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_pnr7UNiREeC7T7T-Qy6ZGA" expressionString="'SDMInterpreter'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_q7gToNiREeC7T7T-Qy6ZGA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EClassifier/instanceClassName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sMCucNiREeC7T7T-Qy6ZGA" expressionString="'de.mdelab.sdm.interpreter.sde.eclipse.SDEEclipseSDMInterpreter'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0qQT8PcQEd-XR4VMarhb7w" modifier="CREATE" source="_izBzsPcQEd-XR4VMarhb7w" target="_QpKGcPI9Ed-7z74s9UiuPA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage/eClassifiers"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1l7SAPcQEd-XR4VMarhb7w" modifier="CREATE" source="_QpKGcPI9Ed-7z74s9UiuPA" target="_lu7kIPcQEd-XR4VMarhb7w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eSuperTypes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_P7W14PiLEd-S_r9zK9ICeQ" modifier="CREATE" source="_QpKGcPI9Ed-7z74s9UiuPA" target="_HnrvgPiLEd-S_r9zK9ICeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eStructuralFeatures"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ld6EUNiREeC7T7T-Qy6ZGA" modifier="CREATE" source="_QpKGcPI9Ed-7z74s9UiuPA" target="_FW30oNiREeC7T7T-Qy6ZGA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eStructuralFeatures"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_77Y8MNiREeC7T7T-Qy6ZGA" modifier="CREATE" source="_izBzsPcQEd-XR4VMarhb7w" target="_mhg0kNiREeC7T7T-Qy6ZGA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage/eClassifiers"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_-uPeUNiREeC7T7T-Qy6ZGA" modifier="CREATE" source="_FW30oNiREeC7T7T-Qy6ZGA" target="_mhg0kNiREeC7T7T-Qy6ZGA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//ETypedElement/eType"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create operation for createRules" uuid="_VlrrgPh3Ed-R0aK4IAlsBA" incoming="_ViuK8He-Ed-INsK5Dz3ubg" outgoing="_DrqqEPiEEd-StusXmfUQQA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="generatedRuleSetEClass" uuid="_go9ywPh3Ed-R0aK4IAlsBA" outgoingStoryLinks="_82Fv0Ph3Ed-R0aK4IAlsBA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="clonedCreateRulesOperation" uuid="_vpekkPh3Ed-R0aK4IAlsBA" modifier="CREATE" outgoingStoryLinks="_FzGuIPh4Ed-R0aK4IAlsBA" incomingStoryLinks="_82Fv0Ph3Ed-R0aK4IAlsBA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <attributeAssignments uuid="_M156IPiGEd-G6YvKstjLMQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//ENamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OJ7t0PiGEd-G6YvKstjLMQ" expressionString="'createRules'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="codeAnnotation" uuid="_DjmJoPh4Ed-R0aK4IAlsBA" modifier="CREATE" outgoingStoryLinks="_WDLqYPh4Ed-R0aK4IAlsBA" incomingStoryLinks="_FzGuIPh4Ed-R0aK4IAlsBA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation"/>
        <attributeAssignments uuid="_HJ13kPh4Ed-R0aK4IAlsBA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation/source"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_L0easPh4Ed-R0aK4IAlsBA" expressionString="'http://www.eclipse.org/emf/2002/GenModel'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="codeEntry" uuid="_OPxYcPh4Ed-R0aK4IAlsBA" modifier="CREATE" incomingStoryLinks="_WDLqYPh4Ed-R0aK4IAlsBA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry"/>
        <attributeAssignments uuid="_YBSV0Ph4Ed-R0aK4IAlsBA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/key"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZGviAPh4Ed-R0aK4IAlsBA" expressionString="'body'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_abE3sPh4Ed-R0aK4IAlsBA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/value"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_c-mkYPh4Ed-R0aK4IAlsBA">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_WlZfQPh7Ed-R0aK4IAlsBA" methodClassName="de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationHelpers" methodName="generateCreateRulesCode">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              <parameters name="" uuid="_LNG74Ph8Ed-R0aK4IAlsBA">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kC7l8Ph8Ed-R0aK4IAlsBA" expressionString="tggDiagram" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGDiagram"/>
              </parameters>
              <parameters name="" uuid="_17N2IPh8Ed-R0aK4IAlsBA">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3kvYsPh8Ed-R0aK4IAlsBA" expressionString="rulesPackage" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
              </parameters>
              <parameters name="" uuid="_7bKAwFbbEeC5kKCQcBmy4Q">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_74T20FbbEeC5kKCQcBmy4Q" expressionString="basePackage" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_82Fv0Ph3Ed-R0aK4IAlsBA" modifier="CREATE" source="_go9ywPh3Ed-R0aK4IAlsBA" target="_vpekkPh3Ed-R0aK4IAlsBA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_FzGuIPh4Ed-R0aK4IAlsBA" modifier="CREATE" source="_vpekkPh3Ed-R0aK4IAlsBA" target="_DjmJoPh4Ed-R0aK4IAlsBA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EModelElement/eAnnotations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WDLqYPh4Ed-R0aK4IAlsBA" modifier="CREATE" source="_DjmJoPh4Ed-R0aK4IAlsBA" target="_OPxYcPh4Ed-R0aK4IAlsBA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation/details"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create source code annotation" uuid="_ie7SMPigEd-bw8R_eH98gw" incoming="_upKacPigEd-bw8R_eH98gw" outgoing="_6bInsHe_Ed-INsK5Dz3ubg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardOperation" uuid="_kPpo8PigEd-bw8R_eH98gw" outgoingStoryLinks="_CKxY4PihEd-bw8R_eH98gw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingOperation" uuid="_k9pXYPigEd-bw8R_eH98gw" outgoingStoryLinks="_Ia6hQPihEd-bw8R_eH98gw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseOperation" uuid="_lqDKYPigEd-bw8R_eH98gw" outgoingStoryLinks="_I94EYPihEd-bw8R_eH98gw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardAnnotation" uuid="_mYWa0PigEd-bw8R_eH98gw" outgoingStoryLinks="_KPLUMPihEd-bw8R_eH98gw" incomingStoryLinks="_CKxY4PihEd-bw8R_eH98gw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xk1PcPiiEd-bw8R_eH98gw" expressionString="self.source ='http://www.eclipse.org/emf/2002/GenModel'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingAnnotation" uuid="_nfTb0PigEd-bw8R_eH98gw" outgoingStoryLinks="_KiG3QPihEd-bw8R_eH98gw" incomingStoryLinks="_Ia6hQPihEd-bw8R_eH98gw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3B7xUPiiEd-bw8R_eH98gw" expressionString="self.source ='http://www.eclipse.org/emf/2002/GenModel'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseAnnotation" uuid="_oNxrYPigEd-bw8R_eH98gw" outgoingStoryLinks="_LbPesPihEd-bw8R_eH98gw" incomingStoryLinks="_I94EYPihEd-bw8R_eH98gw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3YhsYPiiEd-bw8R_eH98gw" expressionString="self.source ='http://www.eclipse.org/emf/2002/GenModel'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="forwardDetailsEntry" uuid="_pPwFwPigEd-bw8R_eH98gw" modifier="CREATE" incomingStoryLinks="_KPLUMPihEd-bw8R_eH98gw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry"/>
        <attributeAssignments uuid="_aQhQ4PihEd-bw8R_eH98gw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/key"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bfNRkPihEd-bw8R_eH98gw" expressionString="'body'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_fxmT0PihEd-bw8R_eH98gw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/value"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_g0dfAPihEd-bw8R_eH98gw">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_hYA4APihEd-bw8R_eH98gw" methodClassName="de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationHelpers" methodName="generateRullCallStoryDiagramCode">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              <parameters name="" uuid="_lrNLgPihEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nfYe4PihEd-bw8R_eH98gw" expressionString="projectName" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_ooFzYPihEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qmuqUPihEd-bw8R_eH98gw" expressionString="generatedRuleSetEClass.name" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_us3ZQPihEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_voEdQPihEd-bw8R_eH98gw" expressionString="forwardActivity.name" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_zj-5MPihEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1d0vsPihEd-bw8R_eH98gw" expressionString="forwardOperation" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
              </parameters>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mappingDetailsEntry" uuid="_qXUKwPigEd-bw8R_eH98gw" modifier="CREATE" incomingStoryLinks="_KiG3QPihEd-bw8R_eH98gw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry"/>
        <attributeAssignments uuid="_cnHU0PihEd-bw8R_eH98gw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/key"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_daVhgPihEd-bw8R_eH98gw" expressionString="'body'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_8UYX0PihEd-bw8R_eH98gw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/value"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_-BqZAPihEd-bw8R_eH98gw">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_-q3HAPihEd-bw8R_eH98gw" methodClassName="de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationHelpers" methodName="generateRullCallStoryDiagramCode">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              <parameters name="" uuid="_CZ92APiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_FmfcYPiiEd-bw8R_eH98gw" expressionString="projectName" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_CveL8PiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Hc6ygPiiEd-bw8R_eH98gw" expressionString="generatedRuleSetEClass.name" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_C2FxUPiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JxSB0PiiEd-bw8R_eH98gw" expressionString="mappingActivity.name" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_C8abwPiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_LNrHUPiiEd-bw8R_eH98gw" expressionString="mappingOperation" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
              </parameters>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reverseDetailsEntry" uuid="_rI28cPigEd-bw8R_eH98gw" modifier="CREATE" incomingStoryLinks="_LbPesPihEd-bw8R_eH98gw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry"/>
        <attributeAssignments uuid="_eEbAUPihEd-bw8R_eH98gw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/key"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_e3kUgPihEd-bw8R_eH98gw" expressionString="'body'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_M9Z_kPiiEd-bw8R_eH98gw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://www.eclipse.org/emf/2002/Ecore#//EStringToStringMapEntry/value"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_N--KQPiiEd-bw8R_eH98gw">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_OhTbQPiiEd-bw8R_eH98gw" methodClassName="de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationHelpers" methodName="generateRullCallStoryDiagramCode">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              <parameters name="" uuid="_Re4msPiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_T2un0PiiEd-bw8R_eH98gw" expressionString="projectName" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_RlI_sPiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VaL-oPiiEd-bw8R_eH98gw" expressionString="generatedRuleSetEClass.name" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_Rre4QPiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XuRhIPiiEd-bw8R_eH98gw" expressionString="reverseActivity.name" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="" uuid="_RxsN8PiiEd-bw8R_eH98gw">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Zd99IPiiEd-bw8R_eH98gw" expressionString="reverseOperation" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
              </parameters>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_CKxY4PihEd-bw8R_eH98gw" source="_kPpo8PigEd-bw8R_eH98gw" target="_mYWa0PigEd-bw8R_eH98gw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EModelElement/eAnnotations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ia6hQPihEd-bw8R_eH98gw" source="_k9pXYPigEd-bw8R_eH98gw" target="_nfTb0PigEd-bw8R_eH98gw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EModelElement/eAnnotations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_I94EYPihEd-bw8R_eH98gw" source="_lqDKYPigEd-bw8R_eH98gw" target="_oNxrYPigEd-bw8R_eH98gw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EModelElement/eAnnotations"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_KPLUMPihEd-bw8R_eH98gw" modifier="CREATE" source="_mYWa0PigEd-bw8R_eH98gw" target="_pPwFwPigEd-bw8R_eH98gw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation/details"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_KiG3QPihEd-bw8R_eH98gw" modifier="CREATE" source="_nfTb0PigEd-bw8R_eH98gw" target="_qXUKwPigEd-bw8R_eH98gw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation/details"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LbPesPihEd-bw8R_eH98gw" modifier="CREATE" source="_oNxrYPigEd-bw8R_eH98gw" target="_rI28cPigEd-bw8R_eH98gw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EAnnotation/details"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="delete old interpreter datatype" uuid="_XAUXkNiREeC7T7T-Qy6ZGA" incoming="_em_OANiREeC7T7T-Qy6ZGA" outgoing="_fcNB0PcQEd-XR4VMarhb7w" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesPackage" uuid="_Zr50ENiREeC7T7T-Qy6ZGA" outgoingStoryLinks="_izC-wNiREeC7T7T-Qy6ZGA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="interpreterDataType" uuid="_aoL2INiREeC7T7T-Qy6ZGA" modifier="DESTROY" incomingStoryLinks="_izC-wNiREeC7T7T-Qy6ZGA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EDataType"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ji2rENiREeC7T7T-Qy6ZGA" expressionString="self.name = 'SDMInterpreter'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_izC-wNiREeC7T7T-Qy6ZGA" modifier="DESTROY" source="_Zr50ENiREeC7T7T-Qy6ZGA" target="_aoL2INiREeC7T7T-Qy6ZGA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage/eClassifiers"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_HhaH8HegEd-UQsRE6oiJVQ" source="_c7k_8HefEd-UQsRE6oiJVQ" target="_soasQHefEd-UQsRE6oiJVQ"/>
    <edges uuid="_hxg_4He6Ed-4c678KwnPnw" source="_soasQHefEd-UQsRE6oiJVQ" target="_YRThYPcQEd-XR4VMarhb7w"/>
    <edges uuid="_LURZ8He-Ed-INsK5Dz3ubg" source="_JQQ60He-Ed-INsK5Dz3ubg" target="_99wEwHe9Ed-INsK5Dz3ubg" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_MQACYHe-Ed-INsK5Dz3ubg" expressionString="tggRule.correspondenceDomain.correspondenceElements->select(e|e.modifier = tgg::TGGModifierEnumeration::NONE)->isEmpty()" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_ViuK8He-Ed-INsK5Dz3ubg" source="_bs6EAHe4Ed-4c678KwnPnw" target="_VlrrgPh3Ed-R0aK4IAlsBA" guardType="END"/>
    <edges uuid="_1xeU8He_Ed-INsK5Dz3ubg" source="_JQQ60He-Ed-INsK5Dz3ubg" target="_ONhpkHe_Ed-INsK5Dz3ubg" guardType="ELSE"/>
    <edges uuid="_5uouEHe_Ed-INsK5Dz3ubg" source="_ONhpkHe_Ed-INsK5Dz3ubg" target="_5NLm0He_Ed-INsK5Dz3ubg"/>
    <edges uuid="_6bInsHe_Ed-INsK5Dz3ubg" source="_ie7SMPigEd-bw8R_eH98gw" target="_bs6EAHe4Ed-4c678KwnPnw"/>
    <edges uuid="_Y4ZigHhcEd-NMp17Gm153Q" source="_bs6EAHe4Ed-4c678KwnPnw" target="_WmaRwHhcEd-NMp17Gm153Q" guardType="FOR_EACH"/>
    <edges uuid="_aNDBQHhcEd-NMp17Gm153Q" source="_WmaRwHhcEd-NMp17Gm153Q" target="_JQQ60He-Ed-INsK5Dz3ubg" guardType="END"/>
    <edges uuid="_9IFSYPPPEd-NkIb-Q3PMvQ" source="_99wEwHe9Ed-INsK5Dz3ubg" target="_5NLm0He_Ed-INsK5Dz3ubg"/>
    <edges uuid="_fcNB0PcQEd-XR4VMarhb7w" source="_XAUXkNiREeC7T7T-Qy6ZGA" target="_eUP7QPcQEd-XR4VMarhb7w" guardType="END"/>
    <edges uuid="_gHh2wPcQEd-XR4VMarhb7w" source="_eUP7QPcQEd-XR4VMarhb7w" target="_bs6EAHe4Ed-4c678KwnPnw"/>
    <edges uuid="_DrqqEPiEEd-StusXmfUQQA" source="_VlrrgPh3Ed-R0aK4IAlsBA" target="__itc8HeoEd-4c678KwnPnw"/>
    <edges uuid="_upKacPigEd-bw8R_eH98gw" source="_5NLm0He_Ed-INsK5Dz3ubg" target="_ie7SMPigEd-bw8R_eH98gw"/>
    <edges uuid="_em_OANiREeC7T7T-Qy6ZGA" source="_YRThYPcQEd-XR4VMarhb7w" target="_XAUXkNiREeC7T7T-Qy6ZGA" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
