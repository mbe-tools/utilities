<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" uuid="_Kin60PP7EeCF48ORi0d7mg">
  <activities uuid="_Kin60fP7EeCF48ORi0d7mg">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_K9EbIPP7EeCF48ORi0d7mg" outgoing="_xZO_EPP7EeCF48ORi0d7mg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_gM_8kPP7EeCF48ORi0d7mg" incoming="_xZO_EPP7EeCF48ORi0d7mg" outgoing="_xyRKIPP7EeCF48ORi0d7mg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="projectName" uuid="_gr10sPP7EeCF48ORi0d7mg" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="basePackage" uuid="_mmenIPP7EeCF48ORi0d7mg" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="motePackage" uuid="_oOJvkPP7EeCF48ORi0d7mg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggDiagram" uuid="_rn1BIPP7EeCF48ORi0d7mg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGDiagram"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesPackage" uuid="_6039oPP8EeCduJAA-bgDbg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create story diagrams for transformation" uuid="_vyxXkPP7EeCF48ORi0d7mg" incoming="_xyRKIPP7EeCF48ORi0d7mg" outgoing="_m3BvIPP8EeCF48ORi0d7mg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_x_ZAIPP7EeCF48ORi0d7mg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:VariableDeclarationAction" uuid="_Uv-DJfP8EeCF48ORi0d7mg" variableName="activityDiagrams">
          <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EJavaObject"/>
          <valueAssignment xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_ZFxlwPP8EeCF48ORi0d7mg">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_y2_9kPP7EeCF48ORi0d7mg">
              <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
              <activity href="../SimpleTransformationGenerationStrategy/generate_rules.story#_Ps8bcHefEd-UQsRE6oiJVQ"/>
              <parameters name="projectName" uuid="_DqIjkPP8EeCF48ORi0d7mg">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_HDpdEPP8EeCF48ORi0d7mg" expressionString="projectName" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="basePackage" uuid="_IVlmEPP8EeCF48ORi0d7mg">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KGWZEPP8EeCF48ORi0d7mg" expressionString="basePackage" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              </parameters>
              <parameters name="motePackage" uuid="_LI4NEPP8EeCF48ORi0d7mg">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hZg9kPP8EeCF48ORi0d7mg" expressionString="motePackage" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
              </parameters>
              <parameters name="tggDiagram" uuid="_iqAVEPP8EeCF48ORi0d7mg">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kwDbkPP8EeCF48ORi0d7mg" expressionString="tggDiagram" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGDiagram"/>
              </parameters>
              <parameters name="rulesPackage" uuid="_-M7ekPP8EeCduJAA-bgDbg">
                <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__ytJkPP8EeCduJAA-bgDbg" expressionString="rulesPackage" expressionLanguage="OCL"/>
                <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EPackage"/>
              </parameters>
            </callActions>
          </valueAssignment>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_mbVRoPP8EeCF48ORi0d7mg" incoming="_m3BvIPP8EeCF48ORi0d7mg">
      <outParameterValues xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nn7AkPP8EeCF48ORi0d7mg" expressionString="activityDiagrams" expressionLanguage="OCL"/>
    </nodes>
    <edges uuid="_xZO_EPP7EeCF48ORi0d7mg" source="_K9EbIPP7EeCF48ORi0d7mg" target="_gM_8kPP7EeCF48ORi0d7mg"/>
    <edges uuid="_xyRKIPP7EeCF48ORi0d7mg" source="_gM_8kPP7EeCF48ORi0d7mg" target="_vyxXkPP7EeCF48ORi0d7mg"/>
    <edges uuid="_m3BvIPP8EeCF48ORi0d7mg" source="_vyxXkPP7EeCF48ORi0d7mg" target="_mbVRoPP8EeCF48ORi0d7mg"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
