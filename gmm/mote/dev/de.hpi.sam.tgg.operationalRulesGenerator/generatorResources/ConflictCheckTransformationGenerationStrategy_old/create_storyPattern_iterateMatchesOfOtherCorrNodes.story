<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_iterateMatchesOfOtherCorrNodes" uuid="_5T8ucCn-EeC1o_ZuFAaWCA">
  <activities name="create_storyPattern_iterateMatchesOfOtherCorrNodes" uuid="_56B3ACn-EeC1o_ZuFAaWCA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_AWSIwCn_EeC1o_ZuFAaWCA" outgoing="_QvMXQCoHEeC1o_ZuFAaWCA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_AxXiQCn_EeC1o_ZuFAaWCA" incoming="_QvMXQCoHEeC1o_ZuFAaWCA" outgoing="_WUxfsCoHEeC1o_ZuFAaWCA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_BVm3wCn_EeC1o_ZuFAaWCA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create matchStoreSPO" uuid="_RNFNICoHEeC1o_ZuFAaWCA" incoming="_WUxfsCoHEeC1o_ZuFAaWCA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchStoreSPO" uuid="_Wq45oCoHEeC1o_ZuFAaWCA" outgoingStoryLinks="_zZImwCoHEeC1o_ZuFAaWCA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_sDP-UCoHEeC1o_ZuFAaWCA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ty7zQCoHEeC1o_ZuFAaWCA" expressionString="'otherMatchStorage'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_agTsACoHEeC1o_ZuFAaWCA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_a_0SgCoHEeC1o_ZuFAaWCA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_matchStore_spo.story#_X128ABgAEeCaHaBYHoSzPw"/>
            <parameters name="storyActionNode" uuid="_dIiSgCoHEeC1o_ZuFAaWCA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fHQB8CoHEeC1o_ZuFAaWCA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="modifier" uuid="_hXwE8CoHEeC1o_ZuFAaWCA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ix_1YCoHEeC1o_ZuFAaWCA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
            </parameters>
            <parameters name="bindingType" uuid="_lB8ewCoHEeC1o_ZuFAaWCA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mOtM0CoHEeC1o_ZuFAaWCA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_wKwmQCoHEeC1o_ZuFAaWCA" modifier="CREATE" incomingStoryLinks="_zZImwCoHEeC1o_ZuFAaWCA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zZImwCoHEeC1o_ZuFAaWCA" modifier="CREATE" source="_Wq45oCoHEeC1o_ZuFAaWCA" target="_wKwmQCoHEeC1o_ZuFAaWCA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_QvMXQCoHEeC1o_ZuFAaWCA" source="_AWSIwCn_EeC1o_ZuFAaWCA" target="_AxXiQCn_EeC1o_ZuFAaWCA"/>
    <edges uuid="_WUxfsCoHEeC1o_ZuFAaWCA" source="_AxXiQCn_EeC1o_ZuFAaWCA" target="_RNFNICoHEeC1o_ZuFAaWCA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
