<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_conflictCheck_activity" uuid="_3veyUBmoEeCeMPIFRfkCeQ">
  <activities name="create_conflictCheck_activity" uuid="_5mXbcBmoEeCeMPIFRfkCeQ">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_85xs8BmoEeCeMPIFRfkCeQ" outgoing="_SHBt0BmpEeCeMPIFRfkCeQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_9W5t0BmoEeCeMPIFRfkCeQ" incoming="_SHBt0BmpEeCeMPIFRfkCeQ" outgoing="_SbF5UBmpEeCeMPIFRfkCeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_-aR2YBmoEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_LDjrEBmpEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_Tt0sgBmpEeCeMPIFRfkCeQ" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create initial node" uuid="_Q8i3sBmpEeCeMPIFRfkCeQ" incoming="_SbF5UBmpEeCeMPIFRfkCeQ" outgoing="_lcH6QBmpEeCeMPIFRfkCeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_Zjh7oBmpEeCeMPIFRfkCeQ" outgoingStoryLinks="_gK8UgBmpEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_c-veQBmpEeCeMPIFRfkCeQ" modifier="CREATE" incomingStoryLinks="_gK8UgBmpEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_gK8UgBmpEeCeMPIFRfkCeQ" modifier="CREATE" source="_Zjh7oBmpEeCeMPIFRfkCeQ" target="_c-veQBmpEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create activity node to iterate through corrNodes in queue" uuid="_huHXcBmpEeCeMPIFRfkCeQ" incoming="_lcH6QBmpEeCeMPIFRfkCeQ" outgoing="_7RJFwBmpEeCeMPIFRfkCeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_m7NHYBmpEeCeMPIFRfkCeQ" outgoingStoryLinks="_1dd28BmpEeCeMPIFRfkCeQ _2pT_ABmpEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_peKzYBmpEeCeMPIFRfkCeQ" incomingStoryLinks="_zIdHYBmpEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateCorrNodesSAN" uuid="_q65QEBmpEeCeMPIFRfkCeQ" modifier="CREATE" incomingStoryLinks="_0ZLvcBmpEeCeMPIFRfkCeQ _2pT_ABmpEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_s6hlgBmpEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_uEvV4BmpEeCeMPIFRfkCeQ" expressionString="'iterate through correspondence node in queue'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_ayglUBmrEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/forEach"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bwlXIBmrEeCeMPIFRfkCeQ" expressionString="true" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_x4bC4BmpEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_zIdHYBmpEeCeMPIFRfkCeQ _0ZLvcBmpEeCeMPIFRfkCeQ" incomingStoryLinks="_1dd28BmpEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zIdHYBmpEeCeMPIFRfkCeQ" modifier="CREATE" source="_x4bC4BmpEeCeMPIFRfkCeQ" target="_peKzYBmpEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0ZLvcBmpEeCeMPIFRfkCeQ" modifier="CREATE" source="_x4bC4BmpEeCeMPIFRfkCeQ" target="_q65QEBmpEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1dd28BmpEeCeMPIFRfkCeQ" modifier="CREATE" source="_m7NHYBmpEeCeMPIFRfkCeQ" target="_x4bC4BmpEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2pT_ABmpEeCeMPIFRfkCeQ" modifier="CREATE" source="_m7NHYBmpEeCeMPIFRfkCeQ" target="_q65QEBmpEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_3q7NABmpEeCeMPIFRfkCeQ" incoming="_0SLbMBo7EeC1-8m2yhOEkQ">
      <returnValue xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1WRVcBo7EeC1-8m2yhOEkQ" expressionString="activity" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of iterateCorrNodesSAN" uuid="_5MnpQBmpEeCeMPIFRfkCeQ" incoming="_7RJFwBmpEeCeMPIFRfkCeQ" outgoing="_CzrqIBmqEeCeMPIFRfkCeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_fpVuQCn6EeC1o_ZuFAaWCA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_gMNKwCn6EeC1o_ZuFAaWCA">
          <activity href="create_storyPattern_iterateCorrNodesInQueue.story#_uYqUUCn4EeCF_aVH6mO9gA"/>
          <parameters name="storyActionNode" uuid="_h30_ECn6EeC1o_ZuFAaWCA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_j4OJgCn6EeC1o_ZuFAaWCA" expressionString="iterateCorrNodesSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="tggRule" uuid="_lAYEYCn6EeC1o_ZuFAaWCA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mqSBcCn6EeC1o_ZuFAaWCA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to iterate through other matches for current corrNode" uuid="_8DaQMBmpEeCeMPIFRfkCeQ" incoming="_CzrqIBmqEeCeMPIFRfkCeQ" outgoing="_jgL8kBmqEeCeMPIFRfkCeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_DtbVkBmqEeCeMPIFRfkCeQ" outgoingStoryLinks="_W4ElgBmqEeCeMPIFRfkCeQ _XVwnEBmqEeCeMPIFRfkCeQ _0oC1YBmqEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateCorrNodesSAN" uuid="_EW7lkBmqEeCeMPIFRfkCeQ" incomingStoryLinks="_XxwmkBmqEeCeMPIFRfkCeQ _zR5h0BmqEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sameRuleOtherCorrNodeConflictsSAN" uuid="_K7iiEBmqEeCeMPIFRfkCeQ" modifier="CREATE" incomingStoryLinks="_XVwnEBmqEeCeMPIFRfkCeQ _ZDZ0oBmqEeCeMPIFRfkCeQ _x_CLwBmqEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_NHdAABmqEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OPe_EBmqEeCeMPIFRfkCeQ" expressionString="'search for conflicts with the same rule and other correspondence nodes'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_oLh08Cn-EeC1o_ZuFAaWCA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/forEach"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_pPsnsCn-EeC1o_ZuFAaWCA" expressionString="true" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_UYNQ0BmqEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_XxwmkBmqEeCeMPIFRfkCeQ _ZDZ0oBmqEeCeMPIFRfkCeQ" incomingStoryLinks="_W4ElgBmqEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_ari3IBmqEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_cBJYIBmqEeCeMPIFRfkCeQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FOR_EACH" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_sv1jABmqEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_x_CLwBmqEeCeMPIFRfkCeQ _zR5h0BmqEeCeMPIFRfkCeQ" incomingStoryLinks="_0oC1YBmqEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_uHR3EBmqEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_u7s9oBmqEeCeMPIFRfkCeQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_W4ElgBmqEeCeMPIFRfkCeQ" modifier="CREATE" source="_DtbVkBmqEeCeMPIFRfkCeQ" target="_UYNQ0BmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XVwnEBmqEeCeMPIFRfkCeQ" modifier="CREATE" source="_DtbVkBmqEeCeMPIFRfkCeQ" target="_K7iiEBmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XxwmkBmqEeCeMPIFRfkCeQ" modifier="CREATE" source="_UYNQ0BmqEeCeMPIFRfkCeQ" target="_EW7lkBmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZDZ0oBmqEeCeMPIFRfkCeQ" modifier="CREATE" source="_UYNQ0BmqEeCeMPIFRfkCeQ" target="_K7iiEBmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_x_CLwBmqEeCeMPIFRfkCeQ" modifier="CREATE" source="_sv1jABmqEeCeMPIFRfkCeQ" target="_K7iiEBmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zR5h0BmqEeCeMPIFRfkCeQ" modifier="CREATE" source="_sv1jABmqEeCeMPIFRfkCeQ" target="_EW7lkBmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0oC1YBmqEeCeMPIFRfkCeQ" modifier="CREATE" source="_DtbVkBmqEeCeMPIFRfkCeQ" target="_sv1jABmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of iterateOtherMatchesSAN" uuid="_gtkD8BmqEeCeMPIFRfkCeQ" incoming="_jgL8kBmqEeCeMPIFRfkCeQ" outgoing="_nvX50BmqEeCeMPIFRfkCeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_iytLgBmqEeCeMPIFRfkCeQ" expressionString="true" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create final node to abort in case of conflict" uuid="_kajTEBmqEeCeMPIFRfkCeQ" incoming="_nvX50BmqEeCeMPIFRfkCeQ" outgoing="_vCLjoBmrEeCeMPIFRfkCeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_oby68BmqEeCeMPIFRfkCeQ" outgoingStoryLinks="_RZwtQBmrEeCeMPIFRfkCeQ _Y77mUBo8EeC1-8m2yhOEkQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sameRuleOtherCorrNodeConflictsSAN" uuid="_p6fGgBmqEeCeMPIFRfkCeQ" incomingStoryLinks="_SFjcQBmrEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="finalNode" uuid="_Ob51ABmrEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_4xLjEBmsEeCeMPIFRfkCeQ" incomingStoryLinks="_S9QgUBmrEeCeMPIFRfkCeQ _Y77mUBo8EeC1-8m2yhOEkQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_P3KSEBmrEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_SFjcQBmrEeCeMPIFRfkCeQ _S9QgUBmrEeCeMPIFRfkCeQ" incomingStoryLinks="_RZwtQBmrEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_U-k30BmrEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WA0X8BmrEeCeMPIFRfkCeQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="trueExpression" uuid="_wygcUBmsEeCeMPIFRfkCeQ" modifier="CREATE" incomingStoryLinks="_4xLjEBmsEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_zmx6wBmsEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1N2mQBmsEeCeMPIFRfkCeQ" expressionString="'true'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_2VIXYBmsEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3VSz4BmsEeCeMPIFRfkCeQ" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RZwtQBmrEeCeMPIFRfkCeQ" modifier="CREATE" source="_oby68BmqEeCeMPIFRfkCeQ" target="_P3KSEBmrEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_SFjcQBmrEeCeMPIFRfkCeQ" modifier="CREATE" source="_P3KSEBmrEeCeMPIFRfkCeQ" target="_p6fGgBmqEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_S9QgUBmrEeCeMPIFRfkCeQ" modifier="CREATE" source="_P3KSEBmrEeCeMPIFRfkCeQ" target="_Ob51ABmrEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4xLjEBmsEeCeMPIFRfkCeQ" modifier="CREATE" source="_Ob51ABmrEeCeMPIFRfkCeQ" target="_wygcUBmsEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Y77mUBo8EeC1-8m2yhOEkQ" modifier="CREATE" source="_oby68BmqEeCeMPIFRfkCeQ" target="_Ob51ABmrEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to search for conflicts of the same rule with the same correspondence node" uuid="_pQ3FQBmrEeCeMPIFRfkCeQ" incoming="_vCLjoBmrEeCeMPIFRfkCeQ" outgoing="_JiJbUBmsEeCeMPIFRfkCeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_wHUmwBmrEeCeMPIFRfkCeQ" outgoingStoryLinks="_DjHREBmsEeCeMPIFRfkCeQ _EFSxEBmsEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateCorrNodesSAN" uuid="_xkyqUBmrEeCeMPIFRfkCeQ" incomingStoryLinks="_Ei5TEBmsEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sameRuleSameCorrNodeConflictsSAN" uuid="_zp3gcBmrEeCeMPIFRfkCeQ" modifier="CREATE" incomingStoryLinks="_EFSxEBmsEeCeMPIFRfkCeQ _F1dHIBmsEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_4DkroBmrEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_50IqUBmrEeCeMPIFRfkCeQ" expressionString="'search for conflicts with the same rule and the same correspondence node'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_3etrEBmrEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_Ei5TEBmsEeCeMPIFRfkCeQ _F1dHIBmsEeCeMPIFRfkCeQ" incomingStoryLinks="_DjHREBmsEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_-y0K0BmrEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__i2goBmrEeCeMPIFRfkCeQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::END" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_DjHREBmsEeCeMPIFRfkCeQ" modifier="CREATE" source="_wHUmwBmrEeCeMPIFRfkCeQ" target="_3etrEBmrEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_EFSxEBmsEeCeMPIFRfkCeQ" modifier="CREATE" source="_wHUmwBmrEeCeMPIFRfkCeQ" target="_zp3gcBmrEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ei5TEBmsEeCeMPIFRfkCeQ" modifier="CREATE" source="_3etrEBmrEeCeMPIFRfkCeQ" target="_xkyqUBmrEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_F1dHIBmsEeCeMPIFRfkCeQ" modifier="CREATE" source="_3etrEBmrEeCeMPIFRfkCeQ" target="_zp3gcBmrEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of sameRuleSameCorrNodeConflictsSAN" uuid="_Gzv7YBmsEeCeMPIFRfkCeQ" incoming="_JiJbUBmsEeCeMPIFRfkCeQ" outgoing="_MuCIgBmsEeCeMPIFRfkCeQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_I-DPwBmsEeCeMPIFRfkCeQ" expressionString="true" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create final node to abort if conflict was found" uuid="_KcSvYBmsEeCeMPIFRfkCeQ" incoming="_MuCIgBmsEeCeMPIFRfkCeQ" outgoing="_6gZzEBmuEeCeMPIFRfkCeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_NyAuABmsEeCeMPIFRfkCeQ" outgoingStoryLinks="_8-htQBmsEeCeMPIFRfkCeQ __sMNYBmsEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sameRuleSameCorrNodeConflictsSAN" uuid="_ppz7UBmsEeCeMPIFRfkCeQ" incomingStoryLinks="_9QN6MBmsEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="finalNode" uuid="_rmts4BmsEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_KO7MUBmtEeCeMPIFRfkCeQ" incomingStoryLinks="_9wCpwBmsEeCeMPIFRfkCeQ __sMNYBmsEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_7E-KoBmsEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_9QN6MBmsEeCeMPIFRfkCeQ _9wCpwBmsEeCeMPIFRfkCeQ" incomingStoryLinks="_8-htQBmsEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_A3814BmtEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_B5B4YBmtEeCeMPIFRfkCeQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="trueExpression" uuid="_I0cLUBmtEeCeMPIFRfkCeQ" modifier="CREATE" incomingStoryLinks="_KO7MUBmtEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_I0cLUxmtEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_I0cLVBmtEeCeMPIFRfkCeQ" expressionString="'true'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_I0cLURmtEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_I0cLUhmtEeCeMPIFRfkCeQ" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8-htQBmsEeCeMPIFRfkCeQ" modifier="CREATE" source="_NyAuABmsEeCeMPIFRfkCeQ" target="_7E-KoBmsEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9QN6MBmsEeCeMPIFRfkCeQ" modifier="CREATE" source="_7E-KoBmsEeCeMPIFRfkCeQ" target="_ppz7UBmsEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9wCpwBmsEeCeMPIFRfkCeQ" modifier="CREATE" source="_7E-KoBmsEeCeMPIFRfkCeQ" target="_rmts4BmsEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="__sMNYBmsEeCeMPIFRfkCeQ" modifier="CREATE" source="_NyAuABmsEeCeMPIFRfkCeQ" target="_rmts4BmsEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_KO7MUBmtEeCeMPIFRfkCeQ" modifier="CREATE" source="_rmts4BmsEeCeMPIFRfkCeQ" target="_I0cLUBmtEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to iterate through other rule" uuid="_4SyE8BmuEeCeMPIFRfkCeQ" incoming="_6gZzEBmuEeCeMPIFRfkCeQ" outgoing="_sD1mYBmvEeCeMPIFRfkCeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_60nIgBmuEeCeMPIFRfkCeQ" outgoingStoryLinks="_bYJFIBmvEeCeMPIFRfkCeQ _iXfc4BmvEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sameRuleSameCorrNodeConflictsSAN" uuid="_L6aL0BmvEeCeMPIFRfkCeQ" incomingStoryLinks="_iqppcBmvEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_UswyQBmvEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_iqppcBmvEeCeMPIFRfkCeQ _jZWigBmvEeCeMPIFRfkCeQ" incomingStoryLinks="_bYJFIBmvEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_X8dBYBmvEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_YsXbYBmvEeCeMPIFRfkCeQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateOtherRulesSAN" uuid="_VJGI8BmvEeCeMPIFRfkCeQ" modifier="CREATE" incomingStoryLinks="_iXfc4BmvEeCeMPIFRfkCeQ _jZWigBmvEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_bqcWEBmvEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_cvG4EBmvEeCeMPIFRfkCeQ" expressionString="'iterate through other TGG rules'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_ff7Z4BmvEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/forEach"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gYhOwBmvEeCeMPIFRfkCeQ" expressionString="true" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_bYJFIBmvEeCeMPIFRfkCeQ" modifier="CREATE" source="_60nIgBmuEeCeMPIFRfkCeQ" target="_UswyQBmvEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iXfc4BmvEeCeMPIFRfkCeQ" modifier="CREATE" source="_60nIgBmuEeCeMPIFRfkCeQ" target="_VJGI8BmvEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iqppcBmvEeCeMPIFRfkCeQ" modifier="CREATE" source="_UswyQBmvEeCeMPIFRfkCeQ" target="_L6aL0BmvEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jZWigBmvEeCeMPIFRfkCeQ" modifier="CREATE" source="_UswyQBmvEeCeMPIFRfkCeQ" target="_VJGI8BmvEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of iterateOtherRulesSAN" uuid="_qDXjcBmvEeCeMPIFRfkCeQ" incoming="_sD1mYBmvEeCeMPIFRfkCeQ" outgoing="_6Iwe8Bo5EeC1-8m2yhOEkQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sRQ-YBmvEeCeMPIFRfkCeQ" expressionString="true" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create final node" uuid="_tcy9YBmvEeCeMPIFRfkCeQ" incoming="_6Iwe8Bo5EeC1-8m2yhOEkQ" outgoing="_smBdQBo6EeC1-8m2yhOEkQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_v6vmgBmvEeCeMPIFRfkCeQ" outgoingStoryLinks="_2mwLgBmvEeCeMPIFRfkCeQ _4w_OcBmvEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateOtherRulesSAN" uuid="_xUK3kBmvEeCeMPIFRfkCeQ" incomingStoryLinks="_3C4t4BmvEeCeMPIFRfkCeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="finalNode" uuid="_zxz-sBmvEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_9jpsMBmvEeCeMPIFRfkCeQ" incomingStoryLinks="_379q8BmvEeCeMPIFRfkCeQ _4w_OcBmvEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_1sAagBmvEeCeMPIFRfkCeQ" modifier="CREATE" outgoingStoryLinks="_3C4t4BmvEeCeMPIFRfkCeQ _379q8BmvEeCeMPIFRfkCeQ" incomingStoryLinks="_2mwLgBmvEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_-lkcMBmvEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__X2NsBmvEeCeMPIFRfkCeQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::END" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="falseExpression" uuid="_6ibW5RmvEeCeMPIFRfkCeQ" modifier="CREATE" incomingStoryLinks="_9jpsMBmvEeCeMPIFRfkCeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_6ibW6BmvEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6ibW6RmvEeCeMPIFRfkCeQ" expressionString="'false'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_6ibW5hmvEeCeMPIFRfkCeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6ibW5xmvEeCeMPIFRfkCeQ" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2mwLgBmvEeCeMPIFRfkCeQ" modifier="CREATE" source="_v6vmgBmvEeCeMPIFRfkCeQ" target="_1sAagBmvEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3C4t4BmvEeCeMPIFRfkCeQ" modifier="CREATE" source="_1sAagBmvEeCeMPIFRfkCeQ" target="_xUK3kBmvEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_379q8BmvEeCeMPIFRfkCeQ" modifier="CREATE" source="_1sAagBmvEeCeMPIFRfkCeQ" target="_zxz-sBmvEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4w_OcBmvEeCeMPIFRfkCeQ" modifier="CREATE" source="_v6vmgBmvEeCeMPIFRfkCeQ" target="_zxz-sBmvEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9jpsMBmvEeCeMPIFRfkCeQ" modifier="CREATE" source="_zxz-sBmvEeCeMPIFRfkCeQ" target="_6ibW5RmvEeCeMPIFRfkCeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to iterate through correspondence nodes in queue" uuid="_-ll2oBo5EeC1-8m2yhOEkQ" incoming="_smBdQBo6EeC1-8m2yhOEkQ" outgoing="_xHs4sBo6EeC1-8m2yhOEkQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_BZ80oBo6EeC1-8m2yhOEkQ" outgoingStoryLinks="_dUsoQBo6EeC1-8m2yhOEkQ _hSvyMBo6EeC1-8m2yhOEkQ _iDDNwBo6EeC1-8m2yhOEkQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateOtherRulesSAN" uuid="_Dr-hoBo6EeC1-8m2yhOEkQ" incomingStoryLinks="_ipVxsBo6EeC1-8m2yhOEkQ _kY98QBo6EeC1-8m2yhOEkQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_FrFSoBo6EeC1-8m2yhOEkQ" modifier="CREATE" outgoingStoryLinks="_ipVxsBo6EeC1-8m2yhOEkQ _jiwtABo6EeC1-8m2yhOEkQ" incomingStoryLinks="_dUsoQBo6EeC1-8m2yhOEkQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_QiMmIBo6EeC1-8m2yhOEkQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_RnIN4Bo6EeC1-8m2yhOEkQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FOR_EACH" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateCorrNodesSAN" uuid="_GxEqUBo6EeC1-8m2yhOEkQ" modifier="CREATE" incomingStoryLinks="_hSvyMBo6EeC1-8m2yhOEkQ _jiwtABo6EeC1-8m2yhOEkQ _lrLLABo6EeC1-8m2yhOEkQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_MclboBo6EeC1-8m2yhOEkQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NuyqYBo6EeC1-8m2yhOEkQ" expressionString="'iterate through correspondence nodes in queue'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_oplc4Bo6EeC1-8m2yhOEkQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/forEach"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_p_Vu4Bo6EeC1-8m2yhOEkQ" expressionString="true" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_Y2btMBo6EeC1-8m2yhOEkQ" modifier="CREATE" outgoingStoryLinks="_kY98QBo6EeC1-8m2yhOEkQ _lrLLABo6EeC1-8m2yhOEkQ" incomingStoryLinks="_iDDNwBo6EeC1-8m2yhOEkQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_aAgToBo6EeC1-8m2yhOEkQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_a5OrYBo6EeC1-8m2yhOEkQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::END" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_dUsoQBo6EeC1-8m2yhOEkQ" modifier="CREATE" source="_BZ80oBo6EeC1-8m2yhOEkQ" target="_FrFSoBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hSvyMBo6EeC1-8m2yhOEkQ" modifier="CREATE" source="_BZ80oBo6EeC1-8m2yhOEkQ" target="_GxEqUBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iDDNwBo6EeC1-8m2yhOEkQ" modifier="CREATE" source="_BZ80oBo6EeC1-8m2yhOEkQ" target="_Y2btMBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ipVxsBo6EeC1-8m2yhOEkQ" modifier="CREATE" source="_FrFSoBo6EeC1-8m2yhOEkQ" target="_Dr-hoBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jiwtABo6EeC1-8m2yhOEkQ" modifier="CREATE" source="_FrFSoBo6EeC1-8m2yhOEkQ" target="_GxEqUBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kY98QBo6EeC1-8m2yhOEkQ" modifier="CREATE" source="_Y2btMBo6EeC1-8m2yhOEkQ" target="_Dr-hoBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lrLLABo6EeC1-8m2yhOEkQ" modifier="CREATE" source="_Y2btMBo6EeC1-8m2yhOEkQ" target="_GxEqUBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of iterateCorrNodesSAN" uuid="_uxtRsBo6EeC1-8m2yhOEkQ" incoming="_xHs4sBo6EeC1-8m2yhOEkQ" outgoing="_UsZgQBo7EeC1-8m2yhOEkQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wN3tsBo6EeC1-8m2yhOEkQ" expressionString="true" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story action node to find conflicts with other rules and other correspondence nodes" uuid="_yQtIIBo6EeC1-8m2yhOEkQ" incoming="_UsZgQBo7EeC1-8m2yhOEkQ" outgoing="_y2tiwBo7EeC1-8m2yhOEkQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="otherRuleOtherCorrNodeConflictsSAN" uuid="_3tbPgBo6EeC1-8m2yhOEkQ" modifier="CREATE" incomingStoryLinks="_N7M5QBo7EeC1-8m2yhOEkQ _QJt_QBo7EeC1-8m2yhOEkQ _SEZjQBo7EeC1-8m2yhOEkQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_6S_jkBo6EeC1-8m2yhOEkQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7drOABo6EeC1-8m2yhOEkQ" expressionString="'find conflicts with other rules and other correspondence nodes'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="__EwCYBo6EeC1-8m2yhOEkQ" outgoingStoryLinks="_MlmYQBo7EeC1-8m2yhOEkQ _N7M5QBo7EeC1-8m2yhOEkQ _OaoAMBo7EeC1-8m2yhOEkQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="iterateCorrNodesSAN" uuid="_A60zMBo7EeC1-8m2yhOEkQ" incomingStoryLinks="_PNmVQBo7EeC1-8m2yhOEkQ _RMPzQBo7EeC1-8m2yhOEkQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_Dh6KMBo7EeC1-8m2yhOEkQ" modifier="CREATE" outgoingStoryLinks="_PNmVQBo7EeC1-8m2yhOEkQ _QJt_QBo7EeC1-8m2yhOEkQ" incomingStoryLinks="_MlmYQBo7EeC1-8m2yhOEkQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_F8ogMBo7EeC1-8m2yhOEkQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_G1SmgBo7EeC1-8m2yhOEkQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FOR_EACH" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_Ekg2sBo7EeC1-8m2yhOEkQ" modifier="CREATE" outgoingStoryLinks="_RMPzQBo7EeC1-8m2yhOEkQ _SEZjQBo7EeC1-8m2yhOEkQ" incomingStoryLinks="_OaoAMBo7EeC1-8m2yhOEkQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_JWlekBo7EeC1-8m2yhOEkQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KOVl8Bo7EeC1-8m2yhOEkQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_MlmYQBo7EeC1-8m2yhOEkQ" modifier="CREATE" source="__EwCYBo6EeC1-8m2yhOEkQ" target="_Dh6KMBo7EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_N7M5QBo7EeC1-8m2yhOEkQ" modifier="CREATE" source="__EwCYBo6EeC1-8m2yhOEkQ" target="_3tbPgBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_OaoAMBo7EeC1-8m2yhOEkQ" modifier="CREATE" source="__EwCYBo6EeC1-8m2yhOEkQ" target="_Ekg2sBo7EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_PNmVQBo7EeC1-8m2yhOEkQ" modifier="CREATE" source="_Dh6KMBo7EeC1-8m2yhOEkQ" target="_A60zMBo7EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_QJt_QBo7EeC1-8m2yhOEkQ" modifier="CREATE" source="_Dh6KMBo7EeC1-8m2yhOEkQ" target="_3tbPgBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RMPzQBo7EeC1-8m2yhOEkQ" modifier="CREATE" source="_Ekg2sBo7EeC1-8m2yhOEkQ" target="_A60zMBo7EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_SEZjQBo7EeC1-8m2yhOEkQ" modifier="CREATE" source="_Ekg2sBo7EeC1-8m2yhOEkQ" target="_3tbPgBo6EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create final node" uuid="_fFRYkBo7EeC1-8m2yhOEkQ" incoming="_zw220Bo7EeC1-8m2yhOEkQ" outgoing="_0SLbMBo7EeC1-8m2yhOEkQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_fFRYkxo7EeC1-8m2yhOEkQ" outgoingStoryLinks="_fFRYkRo7EeC1-8m2yhOEkQ _fFR_qBo7EeC1-8m2yhOEkQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="otherRuleOtherCorrNodeConflictsSAN" uuid="_fFRYkho7EeC1-8m2yhOEkQ" incomingStoryLinks="_fFR_oho7EeC1-8m2yhOEkQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="finalNode" uuid="_fFR_qho7EeC1-8m2yhOEkQ" modifier="CREATE" outgoingStoryLinks="_fFR_qRo7EeC1-8m2yhOEkQ" incomingStoryLinks="_fFRYlBo7EeC1-8m2yhOEkQ _fFR_qBo7EeC1-8m2yhOEkQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_fFRYlRo7EeC1-8m2yhOEkQ" modifier="CREATE" outgoingStoryLinks="_fFR_oho7EeC1-8m2yhOEkQ _fFRYlBo7EeC1-8m2yhOEkQ" incomingStoryLinks="_fFRYkRo7EeC1-8m2yhOEkQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_fFR_oBo7EeC1-8m2yhOEkQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fFR_oRo7EeC1-8m2yhOEkQ" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="trueExpression" uuid="_fFR_oxo7EeC1-8m2yhOEkQ" modifier="CREATE" incomingStoryLinks="_fFR_qRo7EeC1-8m2yhOEkQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_fFR_pBo7EeC1-8m2yhOEkQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fFR_pRo7EeC1-8m2yhOEkQ" expressionString="'true'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_fFR_pho7EeC1-8m2yhOEkQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fFR_pxo7EeC1-8m2yhOEkQ" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fFRYkRo7EeC1-8m2yhOEkQ" modifier="CREATE" source="_fFRYkxo7EeC1-8m2yhOEkQ" target="_fFRYlRo7EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fFR_oho7EeC1-8m2yhOEkQ" modifier="CREATE" source="_fFRYlRo7EeC1-8m2yhOEkQ" target="_fFRYkho7EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fFRYlBo7EeC1-8m2yhOEkQ" modifier="CREATE" source="_fFRYlRo7EeC1-8m2yhOEkQ" target="_fFR_qho7EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fFR_qBo7EeC1-8m2yhOEkQ" modifier="CREATE" source="_fFRYkxo7EeC1-8m2yhOEkQ" target="_fFR_qho7EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fFR_qRo7EeC1-8m2yhOEkQ" modifier="CREATE" source="_fFR_qho7EeC1-8m2yhOEkQ" target="_fFR_oxo7EeC1-8m2yhOEkQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of otherRuleOtherCorrNodeConflictsSAN" uuid="_hm5n0Bo7EeC1-8m2yhOEkQ" incoming="_y2tiwBo7EeC1-8m2yhOEkQ" outgoing="_zw220Bo7EeC1-8m2yhOEkQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jtikMBo7EeC1-8m2yhOEkQ" expressionString="true" expressionLanguage="OCL"/>
    </nodes>
    <edges uuid="_SHBt0BmpEeCeMPIFRfkCeQ" source="_85xs8BmoEeCeMPIFRfkCeQ" target="_9W5t0BmoEeCeMPIFRfkCeQ"/>
    <edges uuid="_SbF5UBmpEeCeMPIFRfkCeQ" source="_9W5t0BmoEeCeMPIFRfkCeQ" target="_Q8i3sBmpEeCeMPIFRfkCeQ"/>
    <edges uuid="_lcH6QBmpEeCeMPIFRfkCeQ" source="_Q8i3sBmpEeCeMPIFRfkCeQ" target="_huHXcBmpEeCeMPIFRfkCeQ"/>
    <edges uuid="_7RJFwBmpEeCeMPIFRfkCeQ" source="_huHXcBmpEeCeMPIFRfkCeQ" target="_5MnpQBmpEeCeMPIFRfkCeQ"/>
    <edges uuid="_CzrqIBmqEeCeMPIFRfkCeQ" source="_5MnpQBmpEeCeMPIFRfkCeQ" target="_8DaQMBmpEeCeMPIFRfkCeQ"/>
    <edges uuid="_jgL8kBmqEeCeMPIFRfkCeQ" source="_8DaQMBmpEeCeMPIFRfkCeQ" target="_gtkD8BmqEeCeMPIFRfkCeQ"/>
    <edges uuid="_nvX50BmqEeCeMPIFRfkCeQ" source="_gtkD8BmqEeCeMPIFRfkCeQ" target="_kajTEBmqEeCeMPIFRfkCeQ"/>
    <edges uuid="_vCLjoBmrEeCeMPIFRfkCeQ" source="_kajTEBmqEeCeMPIFRfkCeQ" target="_pQ3FQBmrEeCeMPIFRfkCeQ"/>
    <edges uuid="_JiJbUBmsEeCeMPIFRfkCeQ" source="_pQ3FQBmrEeCeMPIFRfkCeQ" target="_Gzv7YBmsEeCeMPIFRfkCeQ"/>
    <edges uuid="_MuCIgBmsEeCeMPIFRfkCeQ" source="_Gzv7YBmsEeCeMPIFRfkCeQ" target="_KcSvYBmsEeCeMPIFRfkCeQ"/>
    <edges uuid="_6gZzEBmuEeCeMPIFRfkCeQ" source="_KcSvYBmsEeCeMPIFRfkCeQ" target="_4SyE8BmuEeCeMPIFRfkCeQ"/>
    <edges uuid="_sD1mYBmvEeCeMPIFRfkCeQ" source="_4SyE8BmuEeCeMPIFRfkCeQ" target="_qDXjcBmvEeCeMPIFRfkCeQ"/>
    <edges uuid="_6Iwe8Bo5EeC1-8m2yhOEkQ" source="_qDXjcBmvEeCeMPIFRfkCeQ" target="_tcy9YBmvEeCeMPIFRfkCeQ"/>
    <edges uuid="_smBdQBo6EeC1-8m2yhOEkQ" source="_tcy9YBmvEeCeMPIFRfkCeQ" target="_-ll2oBo5EeC1-8m2yhOEkQ"/>
    <edges uuid="_xHs4sBo6EeC1-8m2yhOEkQ" source="_-ll2oBo5EeC1-8m2yhOEkQ" target="_uxtRsBo6EeC1-8m2yhOEkQ"/>
    <edges uuid="_UsZgQBo7EeC1-8m2yhOEkQ" source="_uxtRsBo6EeC1-8m2yhOEkQ" target="_yQtIIBo6EeC1-8m2yhOEkQ"/>
    <edges uuid="_y2tiwBo7EeC1-8m2yhOEkQ" source="_yQtIIBo6EeC1-8m2yhOEkQ" target="_hm5n0Bo7EeC1-8m2yhOEkQ"/>
    <edges uuid="_zw220Bo7EeC1-8m2yhOEkQ" source="_hm5n0Bo7EeC1-8m2yhOEkQ" target="_fFRYkBo7EeC1-8m2yhOEkQ"/>
    <edges uuid="_0SLbMBo7EeC1-8m2yhOEkQ" source="_fFRYkBo7EeC1-8m2yhOEkQ" target="_3q7NABmpEeCeMPIFRfkCeQ"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
