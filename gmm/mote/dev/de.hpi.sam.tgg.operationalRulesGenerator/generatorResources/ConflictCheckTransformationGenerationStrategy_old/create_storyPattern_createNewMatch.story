<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_createNewMatch" uuid="_zpg50BioEeCm4IbFa7I4cA">
  <activities name="create_storyPattern_createNewMatch" uuid="_0VoY8BioEeCm4IbFa7I4cA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_2S7zIBioEeCm4IbFa7I4cA" outgoing="_N6RekBiuEeCm4IbFa7I4cA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_2tGmoBioEeCm4IbFa7I4cA" incoming="_N6RekBiuEeCm4IbFa7I4cA" outgoing="_QLEcgBiuEeCm4IbFa7I4cA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_3rT7UBioEeCm4IbFa7I4cA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_4c5JQBioEeCm4IbFa7I4cA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_9U1IkBioEeCm4IbFa7I4cA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_dPEeUBiuEeCm4IbFa7I4cA" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform parent model objects" uuid="_OZtzoBiuEeCm4IbFa7I4cA" incoming="_ERbDEBiwEeCm4IbFa7I4cA" outgoing="_KmQ6sBivEeCm4IbFa7I4cA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_VAuj4BiuEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_XUOQgBiuEeCm4IbFa7I4cA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_YKjbkBiuEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bMXmwBiuEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_gsm8MBiuEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_i_eWsBiuEeCm4IbFa7I4cA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_kNqoIBiuEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mm7lkBiuEeCm4IbFa7I4cA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_qJcqEBiuEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r5g5gBiuEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_t4sjABiuEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_vNE8ABiuEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_x6tm8BiuEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_zhjo8BiuEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_0nmrABiuEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2KTz4BiuEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_5KLeUBiuEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6w1TEBiuEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_7s6g0BiuEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9KX9UBiuEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="__iCYQBiuEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BLd0MBivEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_CC06ABivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_DW2tsBivEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_BmObICV0EeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_DZlPICV0EeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created model objects" uuid="_EZku8BivEeCm4IbFa7I4cA" incoming="_SFJHEBivEeCm4IbFa7I4cA" outgoing="_SeFykBivEeCm4IbFa7I4cA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_EZku8RivEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_EZku8hivEeCm4IbFa7I4cA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_EZku_xivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EZkvABivEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_EZku-xivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EZku_BivEeCm4IbFa7I4cA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_EZku-RivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EZku-hivEeCm4IbFa7I4cA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_EZkvBRivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EZkvBhivEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_EZkvAxivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EZkvBBivEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_EZkvARivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EZkvAhivEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_EZku9RivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EZku9hivEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_EZkvBxivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EZkvCBivEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_EZku8xivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EZku9BivEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_EZku_RivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EZku_hivEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_EZku9xivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EZku-BivEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_Fi0zkCV0EeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_HUYQwCV0EeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_H_tvMBivEeCm4IbFa7I4cA" incoming="_KmQ6sBivEeCm4IbFa7I4cA" outgoing="_LAvQMBivEeCm4IbFa7I4cA _SFJHEBivEeCm4IbFa7I4cA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_IbGqsBivEeCm4IbFa7I4cA" incoming="_LAvQMBivEeCm4IbFa7I4cA _SeFykBivEeCm4IbFa7I4cA" outgoing="_MHsRMBivEeCm4IbFa7I4cA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_IxOrsBivEeCm4IbFa7I4cA" incoming="_Yy5YsBjUEeCuQ-PF2tTDdw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_LRb-oBivEeCm4IbFa7I4cA" incoming="_MHsRMBivEeCm4IbFa7I4cA" outgoing="_Me4pMBivEeCm4IbFa7I4cA _dezG8BivEeCm4IbFa7I4cA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_LwB_IBivEeCm4IbFa7I4cA" incoming="_Me4pMBivEeCm4IbFa7I4cA _d7QZcBivEeCm4IbFa7I4cA" outgoing="_M6CUIBivEeCm4IbFa7I4cA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created model objects" uuid="_axArGRivEeCm4IbFa7I4cA" incoming="_dezG8BivEeCm4IbFa7I4cA" outgoing="_d7QZcBivEeCm4IbFa7I4cA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_axArGhivEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_axArGxivEeCm4IbFa7I4cA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_axBSFRivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_axBSFhivEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_axArHBivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_axBSEBivEeCm4IbFa7I4cA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_axBSGxivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_axBSHBivEeCm4IbFa7I4cA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_axBSGRivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_axBSGhivEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_axBSIxivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_axBSJBivEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_axBSIRivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_axBSIhivEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_axBSHxivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_axBSIBivEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_axBSHRivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_axBSHhivEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_axBSExivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_axBSFBivEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_axBSFxivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_axBSGBivEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_axBSERivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_axBSEhivEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_KefcUCV0EeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Mc8tECV0EeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform parent correspondence nodes" uuid="_jJ3MUBivEeCm4IbFa7I4cA" incoming="_QLEcgBiuEeCm4IbFa7I4cA" outgoing="_ERbDEBiwEeCm4IbFa7I4cA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_mKpcwBivEeCm4IbFa7I4cA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_m6x5MBivEeCm4IbFa7I4cA">
          <activity href="../common/transform_correspondenceNodes.story#_gUE7UBNnEeCRUJdROfc5HA"/>
          <parameters name="tggRule" uuid="_qLSAoBivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r_fJMBivEeCm4IbFa7I4cA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_tLoMMBivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_vPXlkBivEeCm4IbFa7I4cA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_wRSVkBivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_y5vlkBivEeCm4IbFa7I4cA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_zwu38BivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1p-RgBivEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="bindingType" uuid="_5hFF8BivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6t-98BivEeCm4IbFa7I4cA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_9Vhn8BivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-3SVoBivEeCm4IbFa7I4cA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="__yO68BivEeCm4IbFa7I4cA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Bgi24BiwEeCm4IbFa7I4cA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create matchStore and match SPOs" uuid="_7t2YIBiyEeCm4IbFa7I4cA" incoming="_M6CUIBivEeCm4IbFa7I4cA" outgoing="_IqMKYBjBEeCuQ-PF2tTDdw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchStoreSpo" uuid="__VWqUBiyEeCm4IbFa7I4cA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_C0yloBizEeCm4IbFa7I4cA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_DQWgQBizEeCm4IbFa7I4cA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_matchStore_spo.story#_X128ABgAEeCaHaBYHoSzPw"/>
            <parameters name="storyActionNode" uuid="_FXX3IBizEeCm4IbFa7I4cA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_HYSmABizEeCm4IbFa7I4cA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="modifier" uuid="_mJt1YBi_EeCuQ-PF2tTDdw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_oDrAoBi_EeCuQ-PF2tTDdw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
            </parameters>
            <parameters name="bindingType" uuid="_p00OIBi_EeCuQ-PF2tTDdw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rVeIkBi_EeCuQ-PF2tTDdw" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchSpo" uuid="_py9-kBjAEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_upKRcBjAEeCuQ-PF2tTDdw">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_vFJp4BjAEeCuQ-PF2tTDdw">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_match_spo.story#_6d114Bi_EeCuQ-PF2tTDdw"/>
            <parameters name="storyActionNode" uuid="_xSiugBjAEeCuQ-PF2tTDdw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_zhVhUBjAEeCuQ-PF2tTDdw" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="modifier" uuid="_0Sn0UBjAEeCuQ-PF2tTDdw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2LKDQBjAEeCuQ-PF2tTDdw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
            </parameters>
            <parameters name="bindingType" uuid="_4h4DABjAEeCuQ-PF2tTDdw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_50wnMBjAEeCuQ-PF2tTDdw" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create matchStore->matches link" uuid="_tSIPQBjPEeCuQ-PF2tTDdw" incoming="_IqMKYBjBEeCuQ-PF2tTDdw" outgoing="_b53GcBjQEeCuQ-PF2tTDdw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_0b5HQBjPEeCuQ-PF2tTDdw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_IkkFsBjQEeCuQ-PF2tTDdw">
          <activity href="../common/create_matchStore_matches_link.story#_57M7gBjCEeCuQ-PF2tTDdw"/>
          <parameters name="matchStoreSpo" uuid="_JrSdMBjQEeCuQ-PF2tTDdw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_MkSDkBjQEeCuQ-PF2tTDdw" expressionString="matchStoreSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="matchSpo" uuid="_NzaJIBjQEeCuQ-PF2tTDdw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_P-LXkBjQEeCuQ-PF2tTDdw" expressionString="matchSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="modifier" uuid="_WfGt8BjQEeCuQ-PF2tTDdw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_YG2u4BjQEeCuQ-PF2tTDdw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create match->applicationContext links" uuid="_CCHacBjSEeCuQ-PF2tTDdw" incoming="_b53GcBjQEeCuQ-PF2tTDdw" outgoing="_Vu01oBjSEeCuQ-PF2tTDdw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_Fhay4BjSEeCuQ-PF2tTDdw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_GQHE4BjSEeCuQ-PF2tTDdw">
          <activity href="../common/create_match_applicationContext_links.story#_oE-YcBjQEeCuQ-PF2tTDdw"/>
          <parameters name="matchSpo" uuid="_G_LKUBjSEeCuQ-PF2tTDdw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JA38UBjSEeCuQ-PF2tTDdw" expressionString="matchSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="tggRule" uuid="_KAuPwBjSEeCuQ-PF2tTDdw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_L0ONsBjSEeCuQ-PF2tTDdw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_MshusBjSEeCuQ-PF2tTDdw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ox0AMBjSEeCuQ-PF2tTDdw" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_Pq3IEBjSEeCuQ-PF2tTDdw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_RdsXoBjSEeCuQ-PF2tTDdw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create match->createdElements links" uuid="_FboCABjUEeCuQ-PF2tTDdw" incoming="_Vu01oBjSEeCuQ-PF2tTDdw" outgoing="_Yy5YsBjUEeCuQ-PF2tTDdw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_GwAbABjUEeCuQ-PF2tTDdw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_IbQb4BjUEeCuQ-PF2tTDdw">
          <activity href="../common/create_match_createdElements_links.story#_oE-YcBjQEeCuQ-PF2tTDdw"/>
          <parameters name="matchSpo" uuid="_JQ974BjUEeCuQ-PF2tTDdw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_K_bo0BjUEeCuQ-PF2tTDdw" expressionString="matchSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="tggRule" uuid="_L8ZAUBjUEeCuQ-PF2tTDdw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Nv0s0BjUEeCuQ-PF2tTDdw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_Oo97UBjUEeCuQ-PF2tTDdw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_RhNTwBjUEeCuQ-PF2tTDdw" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_SmeSsBjUEeCuQ-PF2tTDdw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VVfdsBjUEeCuQ-PF2tTDdw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <edges uuid="_N6RekBiuEeCm4IbFa7I4cA" source="_2S7zIBioEeCm4IbFa7I4cA" target="_2tGmoBioEeCm4IbFa7I4cA"/>
    <edges uuid="_QLEcgBiuEeCm4IbFa7I4cA" source="_2tGmoBioEeCm4IbFa7I4cA" target="_jJ3MUBivEeCm4IbFa7I4cA"/>
    <edges uuid="_KmQ6sBivEeCm4IbFa7I4cA" source="_OZtzoBiuEeCm4IbFa7I4cA" target="_H_tvMBivEeCm4IbFa7I4cA"/>
    <edges uuid="_LAvQMBivEeCm4IbFa7I4cA" source="_H_tvMBivEeCm4IbFa7I4cA" target="_IbGqsBivEeCm4IbFa7I4cA" guardType="ELSE"/>
    <edges uuid="_MHsRMBivEeCm4IbFa7I4cA" source="_IbGqsBivEeCm4IbFa7I4cA" target="_LRb-oBivEeCm4IbFa7I4cA"/>
    <edges uuid="_Me4pMBivEeCm4IbFa7I4cA" source="_LRb-oBivEeCm4IbFa7I4cA" target="_LwB_IBivEeCm4IbFa7I4cA" guardType="ELSE"/>
    <edges uuid="_M6CUIBivEeCm4IbFa7I4cA" source="_LwB_IBivEeCm4IbFa7I4cA" target="_7t2YIBiyEeCm4IbFa7I4cA"/>
    <edges uuid="_SFJHEBivEeCm4IbFa7I4cA" source="_H_tvMBivEeCm4IbFa7I4cA" target="_EZku8BivEeCm4IbFa7I4cA" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XivYMBivEeCm4IbFa7I4cA" expressionString="direction = mote::TransformationDirection::FORWARD or&#xA;direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_SeFykBivEeCm4IbFa7I4cA" source="_EZku8BivEeCm4IbFa7I4cA" target="_IbGqsBivEeCm4IbFa7I4cA"/>
    <edges uuid="_dezG8BivEeCm4IbFa7I4cA" source="_LRb-oBivEeCm4IbFa7I4cA" target="_axArGRivEeCm4IbFa7I4cA" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fHwo0BivEeCm4IbFa7I4cA" expressionString="direction = mote::TransformationDirection::MAPPING or&#xA;direction = mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_d7QZcBivEeCm4IbFa7I4cA" source="_axArGRivEeCm4IbFa7I4cA" target="_LwB_IBivEeCm4IbFa7I4cA"/>
    <edges uuid="_ERbDEBiwEeCm4IbFa7I4cA" source="_jJ3MUBivEeCm4IbFa7I4cA" target="_OZtzoBiuEeCm4IbFa7I4cA"/>
    <edges uuid="_IqMKYBjBEeCuQ-PF2tTDdw" source="_7t2YIBiyEeCm4IbFa7I4cA" target="_tSIPQBjPEeCuQ-PF2tTDdw"/>
    <edges uuid="_b53GcBjQEeCuQ-PF2tTDdw" source="_tSIPQBjPEeCuQ-PF2tTDdw" target="_CCHacBjSEeCuQ-PF2tTDdw"/>
    <edges uuid="_Vu01oBjSEeCuQ-PF2tTDdw" source="_CCHacBjSEeCuQ-PF2tTDdw" target="_FboCABjUEeCuQ-PF2tTDdw"/>
    <edges uuid="_Yy5YsBjUEeCuQ-PF2tTDdw" source="_FboCABjUEeCuQ-PF2tTDdw" target="_IxOrsBivEeCm4IbFa7I4cA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
