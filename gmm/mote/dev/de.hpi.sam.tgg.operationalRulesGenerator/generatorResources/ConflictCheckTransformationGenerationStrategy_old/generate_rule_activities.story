<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="generate_rule_activities" uuid="_EMzjAPlaEd-fSKxQcXgteA">
  <activities name="generate_rule_activities" uuid="_Ej074PlaEd-fSKxQcXgteA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_Klm4IPlaEd-fSKxQcXgteA" outgoing="_VueqAPlaEd-fSKxQcXgteA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_L70dIPlaEd-fSKxQcXgteA" incoming="_VueqAPlaEd-fSKxQcXgteA" outgoing="_uHF1UBfzEeCvZOwhS7-eiQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_M0pikPlaEd-fSKxQcXgteA" incomingStoryLinks="_stLuABfxEeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_gDhe0BfxEeCvZOwhS7-eiQ" outgoingStoryLinks="_stLuABfxEeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_stLuABfxEeCvZOwhS7-eiQ" source="_gDhe0BfxEeCvZOwhS7-eiQ" target="_M0pikPlaEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/tggRule"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create initial node" uuid="_bnfr0PlaEd-fSKxQcXgteA" incoming="_DiPS0Bf1EeCvZOwhS7-eiQ" outgoing="_-Te0YBdDEeCR3_w4EGJGeQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_dR9pkPlaEd-fSKxQcXgteA" outgoingStoryLinks="_gxY90PlaEd-fSKxQcXgteA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_fSsyQPlaEd-fSKxQcXgteA" modifier="CREATE" incomingStoryLinks="_gxY90PlaEd-fSKxQcXgteA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_gxY90PlaEd-fSKxQcXgteA" modifier="CREATE" source="_dR9pkPlaEd-fSKxQcXgteA" target="_fSsyQPlaEd-fSKxQcXgteA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to create return value variable" uuid="_dZVnMBdGEeCR3_w4EGJGeQ" incoming="_-Te0YBdDEeCR3_w4EGJGeQ" outgoing="_4m6DIBfzEeCvZOwhS7-eiQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_m86PMBdGEeCR3_w4EGJGeQ" outgoingStoryLinks="_z6op4BdGEeCR3_w4EGJGeQ _0PT5YBdGEeCR3_w4EGJGeQ _LkjhgBdHEeCR3_w4EGJGeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_oaCUgBdGEeCR3_w4EGJGeQ" incomingStoryLinks="_z6op4BdGEeCR3_w4EGJGeQ _1Goi8BdGEeCR3_w4EGJGeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_pY5wgBdGEeCR3_w4EGJGeQ" modifier="CREATE" outgoingStoryLinks="_1Goi8BdGEeCR3_w4EGJGeQ _19i80BdGEeCR3_w4EGJGeQ" incomingStoryLinks="_0PT5YBdGEeCR3_w4EGJGeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createReturnValueNode" uuid="_p_blABdGEeCR3_w4EGJGeQ" modifier="CREATE" outgoingStoryLinks="_6n2IwBdGEeCR3_w4EGJGeQ" incomingStoryLinks="_19i80BdGEeCR3_w4EGJGeQ _LkjhgBdHEeCR3_w4EGJGeQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_3e4zwBdGEeCR3_w4EGJGeQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_4ok_sBdGEeCR3_w4EGJGeQ" expressionString="'create return value variable'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_rUpEcBdGEeCR3_w4EGJGeQ" incomingStoryLinks="_6n2IwBdGEeCR3_w4EGJGeQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="__MjqEBdGEeCR3_w4EGJGeQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="__j-rkBdGEeCR3_w4EGJGeQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
            <activity href="../common/create_returnValue_expression.story#_qoFvMPxlEd--caqh5zaNfg"/>
            <parameters name="resultLiteral" uuid="_BLgDABdHEeCR3_w4EGJGeQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_DqzXABdHEeCR3_w4EGJGeQ" expressionString="mote::rules::TransformationResult::RULE_NOT_APPLIED" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//rules/TransformationResult"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_z6op4BdGEeCR3_w4EGJGeQ" source="_m86PMBdGEeCR3_w4EGJGeQ" target="_oaCUgBdGEeCR3_w4EGJGeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0PT5YBdGEeCR3_w4EGJGeQ" modifier="CREATE" source="_m86PMBdGEeCR3_w4EGJGeQ" target="_pY5wgBdGEeCR3_w4EGJGeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1Goi8BdGEeCR3_w4EGJGeQ" modifier="CREATE" source="_pY5wgBdGEeCR3_w4EGJGeQ" target="_oaCUgBdGEeCR3_w4EGJGeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_19i80BdGEeCR3_w4EGJGeQ" modifier="CREATE" source="_pY5wgBdGEeCR3_w4EGJGeQ" target="_p_blABdGEeCR3_w4EGJGeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_6n2IwBdGEeCR3_w4EGJGeQ" modifier="CREATE" source="_p_blABdGEeCR3_w4EGJGeQ" target="_rUpEcBdGEeCR3_w4EGJGeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LkjhgBdHEeCR3_w4EGJGeQ" modifier="CREATE" source="_m86PMBdGEeCR3_w4EGJGeQ" target="_p_blABdGEeCR3_w4EGJGeQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_1j_9sBfxEeCvZOwhS7-eiQ" incoming="_uHF1UBfzEeCvZOwhS7-eiQ _4m6DIBfzEeCvZOwhS7-eiQ" outgoing="_uq8JQBfzEeCvZOwhS7-eiQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_15SRMBfxEeCvZOwhS7-eiQ" incoming="_uq8JQBfzEeCvZOwhS7-eiQ" outgoing="_jqKjcBfzEeCvZOwhS7-eiQ _mfRvYBfzEeCvZOwhS7-eiQ _qASYABfzEeCvZOwhS7-eiQ _wCDtMBfzEeCvZOwhS7-eiQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set forward parameters" uuid="_2Mnc4BfxEeCvZOwhS7-eiQ" incoming="_jqKjcBfzEeCvZOwhS7-eiQ" outgoing="_zedioBfzEeCvZOwhS7-eiQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_55n6IBfxEeCvZOwhS7-eiQ" outgoingStoryLinks="_UALMQBfyEeCvZOwhS7-eiQ _EOnikBfzEeCvZOwhS7-eiQ _T8U8IBf0EeCvZOwhS7-eiQ _j5BCUBf0EeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eOperation" uuid="_D91KABfyEeCvZOwhS7-eiQ" incomingStoryLinks="_UALMQBfyEeCvZOwhS7-eiQ _xn2NABf4EeC2X7AVcx_M6A">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activityDiagram" uuid="_NgKb4BfyEeCvZOwhS7-eiQ" modifier="CREATE" outgoingStoryLinks="_Fu9T8BfzEeCvZOwhS7-eiQ" incomingStoryLinks="_EOnikBfzEeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_WOmysBfyEeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XC1E8BfyEeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_forwardTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_OJz10BfyEeCvZOwhS7-eiQ" modifier="CREATE" outgoingStoryLinks="_xn2NABf4EeC2X7AVcx_M6A" incomingStoryLinks="_Fu9T8BfzEeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_aBw7QBfyEeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bQwd8BfyEeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_forwardTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_Olz1UBfyEeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KCVNoBfzEeCvZOwhS7-eiQ" expressionString="mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesActivity" uuid="_FOTogBf0EeCvZOwhS7-eiQ" incomingStoryLinks="_Si2nwBf0EeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_QFIoIBf0EeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Q_diYBf0EeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_forwardFindMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_BfQDMBf8EeCaHaBYHoSzPw">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_B6aVMBf8EeCaHaBYHoSzPw">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
            <activity href="create_findMatches_activity.story#_vRRsIBfVEeCVjoDp-muf0Q"/>
            <parameters name="tggRule" uuid="_DnNOMBf8EeCaHaBYHoSzPw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GH8soBf8EeCaHaBYHoSzPw" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_HI82oBf8EeCaHaBYHoSzPw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KCaXEBf8EeCaHaBYHoSzPw" expressionString="mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesActivityDiagram" uuid="_GQalwBf0EeCvZOwhS7-eiQ" modifier="CREATE" outgoingStoryLinks="_Si2nwBf0EeCvZOwhS7-eiQ" incomingStoryLinks="_T8U8IBf0EeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_K9xFsBf0EeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_L47tcBf0EeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_forwardFindMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="conflictCheckActivityDiagram" uuid="_XElGgBf0EeCvZOwhS7-eiQ" modifier="CREATE" outgoingStoryLinks="_it9kcBf0EeCvZOwhS7-eiQ" incomingStoryLinks="_j5BCUBf0EeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_aIMBcBf0EeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bPVPsBf0EeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_forwardCheckConflicts')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkConflictsActivity" uuid="_eAqu4Bf0EeCvZOwhS7-eiQ" incomingStoryLinks="_it9kcBf0EeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_gu6d0Bf0EeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hlb2IBf0EeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_forwardCheckConflicts')" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="__XwogBo7EeC1-8m2yhOEkQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="__tu4gBo7EeC1-8m2yhOEkQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
            <activity href="create_conflictCheck_activity.story#_5mXbcBmoEeCeMPIFRfkCeQ"/>
            <parameters name="tggRule" uuid="_CQnsABo8EeC1-8m2yhOEkQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EJZLgBo8EeC1-8m2yhOEkQ" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_FF0XgBo8EeC1-8m2yhOEkQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_G3fJcBo8EeC1-8m2yhOEkQ" expressionString="mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_UALMQBfyEeCvZOwhS7-eiQ" source="_55n6IBfxEeCvZOwhS7-eiQ" target="_D91KABfyEeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/forwardTransformationOperation"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_EOnikBfzEeCvZOwhS7-eiQ" modifier="CREATE" source="_55n6IBfxEeCvZOwhS7-eiQ" target="_NgKb4BfyEeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/forwardTransformationActivityDiagram"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Fu9T8BfzEeCvZOwhS7-eiQ" modifier="CREATE" source="_NgKb4BfyEeCvZOwhS7-eiQ" target="_OJz10BfyEeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Si2nwBf0EeCvZOwhS7-eiQ" modifier="CREATE" source="_GQalwBf0EeCvZOwhS7-eiQ" target="_FOTogBf0EeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_T8U8IBf0EeCvZOwhS7-eiQ" modifier="CREATE" source="_55n6IBfxEeCvZOwhS7-eiQ" target="_GQalwBf0EeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_it9kcBf0EeCvZOwhS7-eiQ" modifier="CREATE" source="_XElGgBf0EeCvZOwhS7-eiQ" target="_eAqu4Bf0EeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_j5BCUBf0EeCvZOwhS7-eiQ" modifier="CREATE" source="_55n6IBfxEeCvZOwhS7-eiQ" target="_XElGgBf0EeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xn2NABf4EeC2X7AVcx_M6A" modifier="CREATE" source="_OJz10BfyEeCvZOwhS7-eiQ" target="_D91KABfyEeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set mapping parameters" uuid="_MTbGgBfzEeCvZOwhS7-eiQ" incoming="_mfRvYBfzEeCvZOwhS7-eiQ" outgoing="_yjV-MBfzEeCvZOwhS7-eiQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_MTbGiRfzEeCvZOwhS7-eiQ" outgoingStoryLinks="_MTbtkRfzEeCvZOwhS7-eiQ _MTbGgRfzEeCvZOwhS7-eiQ _qAPfQBf0EeCvZOwhS7-eiQ _qZbbUBf0EeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eOperation" uuid="_MTbtkBfzEeCvZOwhS7-eiQ" incomingStoryLinks="_MTbtkRfzEeCvZOwhS7-eiQ _w1VyABf4EeC2X7AVcx_M6A">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activityDiagram" uuid="_MTbGhRfzEeCvZOwhS7-eiQ" modifier="CREATE" outgoingStoryLinks="_MTbGiBfzEeCvZOwhS7-eiQ" incomingStoryLinks="_MTbGgRfzEeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_MTbGhhfzEeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_MTbGhxfzEeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_mappingTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_MTbGghfzEeCvZOwhS7-eiQ" modifier="CREATE" outgoingStoryLinks="_w1VyABf4EeC2X7AVcx_M6A" incomingStoryLinks="_MTbGiBfzEeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_MTbGgxfzEeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_MTbGhBfzEeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_mappingTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_MTbtkhfzEeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_MTbtkxfzEeCvZOwhS7-eiQ" expressionString="mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesActivityDiagram" uuid="_m2zCExf0EeCvZOwhS7-eiQ" modifier="CREATE" outgoingStoryLinks="_TH4GwBf8EeCaHaBYHoSzPw" incomingStoryLinks="_qAPfQBf0EeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_m2zCFBf0EeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_m2zpIBf0EeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_mappingFindMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="conflictCheckActivityDiagram" uuid="_ntzikxf0EeCvZOwhS7-eiQ" modifier="CREATE" outgoingStoryLinks="_prCEQBf0EeCvZOwhS7-eiQ" incomingStoryLinks="_qZbbUBf0EeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_ntzilBf0EeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ntzilRf0EeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_mappingCheckConflicts')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkConflictsActivity" uuid="_oyZMExf0EeCvZOwhS7-eiQ" modifier="CREATE" incomingStoryLinks="_prCEQBf0EeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_oyZMFBf0EeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_oyZMFRf0EeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_mappingCheckConflicts')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesActivity" uuid="_RfxggBf8EeCaHaBYHoSzPw" incomingStoryLinks="_TH4GwBf8EeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_RfyHkBf8EeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_RfyHkRf8EeCaHaBYHoSzPw" expressionString="tggRule.name.concat('_mappingFindMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_RfyHkhf8EeCaHaBYHoSzPw">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_RfyHkxf8EeCaHaBYHoSzPw">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
            <activity href="create_findMatches_activity.story#_vRRsIBfVEeCVjoDp-muf0Q"/>
            <parameters name="tggRule" uuid="_RfyHlhf8EeCaHaBYHoSzPw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_RfyHlxf8EeCaHaBYHoSzPw" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_RfyHlBf8EeCaHaBYHoSzPw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_RfyHlRf8EeCaHaBYHoSzPw" expressionString="mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_MTbtkRfzEeCvZOwhS7-eiQ" source="_MTbGiRfzEeCvZOwhS7-eiQ" target="_MTbtkBfzEeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/mappingTransformationOperation"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_MTbGgRfzEeCvZOwhS7-eiQ" modifier="CREATE" source="_MTbGiRfzEeCvZOwhS7-eiQ" target="_MTbGhRfzEeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/mappingTransformationActivityDiagram"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_MTbGiBfzEeCvZOwhS7-eiQ" modifier="CREATE" source="_MTbGhRfzEeCvZOwhS7-eiQ" target="_MTbGghfzEeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_prCEQBf0EeCvZOwhS7-eiQ" modifier="CREATE" source="_ntzikxf0EeCvZOwhS7-eiQ" target="_oyZMExf0EeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qAPfQBf0EeCvZOwhS7-eiQ" modifier="CREATE" source="_MTbGiRfzEeCvZOwhS7-eiQ" target="_m2zCExf0EeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qZbbUBf0EeCvZOwhS7-eiQ" modifier="CREATE" source="_MTbGiRfzEeCvZOwhS7-eiQ" target="_ntzikxf0EeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_w1VyABf4EeC2X7AVcx_M6A" modifier="CREATE" source="_MTbGghfzEeCvZOwhS7-eiQ" target="_MTbtkBfzEeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_TH4GwBf8EeCaHaBYHoSzPw" modifier="CREATE" source="_m2zCExf0EeCvZOwhS7-eiQ" target="_RfxggBf8EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set reverse parameters" uuid="_UdzuDhfzEeCvZOwhS7-eiQ" incoming="_qASYABfzEeCvZOwhS7-eiQ" outgoing="_x80JsBfzEeCvZOwhS7-eiQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_UdzuExfzEeCvZOwhS7-eiQ" outgoingStoryLinks="_Ud0VFBfzEeCvZOwhS7-eiQ _Ud0VEBfzEeCvZOwhS7-eiQ _5_me8Bf0EeCvZOwhS7-eiQ _6VHb8Bf0EeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eOperation" uuid="_UdzuEhfzEeCvZOwhS7-eiQ" incomingStoryLinks="_Ud0VFBfzEeCvZOwhS7-eiQ _vjBOgBf4EeC2X7AVcx_M6A">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activityDiagram" uuid="_Ud0VERfzEeCvZOwhS7-eiQ" modifier="CREATE" outgoingStoryLinks="_Ud0VFRfzEeCvZOwhS7-eiQ" incomingStoryLinks="_Ud0VEBfzEeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_Ud0VEhfzEeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ud0VExfzEeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_reverseTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_UdzuDxfzEeCvZOwhS7-eiQ" modifier="CREATE" outgoingStoryLinks="_vjBOgBf4EeC2X7AVcx_M6A" incomingStoryLinks="_Ud0VFRfzEeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_UdzuEBfzEeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_UdzuERfzEeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_reverseTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_Ud0VFhfzEeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ud0VFxfzEeCvZOwhS7-eiQ" expressionString="mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesActivityDiagram" uuid="_3GpU0xf0EeCvZOwhS7-eiQ" modifier="CREATE" outgoingStoryLinks="_5JNpgBf0EeCvZOwhS7-eiQ" incomingStoryLinks="_5_me8Bf0EeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_3GpU1Bf0EeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3GpU1Rf0EeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_reverseFindMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="conflictCheckActivityDiagram" uuid="_3jQYUxf0EeCvZOwhS7-eiQ" modifier="CREATE" outgoingStoryLinks="_5pjWcBf0EeCvZOwhS7-eiQ" incomingStoryLinks="_6VHb8Bf0EeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_3jQYVBf0EeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3jQYVRf0EeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_reverseCheckConflicts')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkConflictsActivity" uuid="_4enNUxf0EeCvZOwhS7-eiQ" modifier="CREATE" incomingStoryLinks="_5pjWcBf0EeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_4enNVBf0EeCvZOwhS7-eiQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_4enNVRf0EeCvZOwhS7-eiQ" expressionString="tggRule.name.concat('_reverseCheckConflicts')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="findMatchesActivity" uuid="_WC-l-Rf8EeCaHaBYHoSzPw" incomingStoryLinks="_5JNpgBf0EeCvZOwhS7-eiQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_WC-l-hf8EeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WC-l-xf8EeCaHaBYHoSzPw" expressionString="tggRule.name.concat('_reverseFindMatches')" expressionLanguage="OCL"/>
        </attributeAssignments>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_WC-l_Bf8EeCaHaBYHoSzPw">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_WC-l_Rf8EeCaHaBYHoSzPw">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
            <activity href="create_findMatches_activity.story#_vRRsIBfVEeCVjoDp-muf0Q"/>
            <parameters name="tggRule" uuid="_WC_NABf8EeCaHaBYHoSzPw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WC_NARf8EeCaHaBYHoSzPw" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_WC_NAhf8EeCaHaBYHoSzPw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WC_NAxf8EeCaHaBYHoSzPw" expressionString="mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ud0VFBfzEeCvZOwhS7-eiQ" source="_UdzuExfzEeCvZOwhS7-eiQ" target="_UdzuEhfzEeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/reverseTransformationOperation"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ud0VEBfzEeCvZOwhS7-eiQ" modifier="CREATE" source="_UdzuExfzEeCvZOwhS7-eiQ" target="_Ud0VERfzEeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/reverseTransformationActivityDiagram"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ud0VFRfzEeCvZOwhS7-eiQ" modifier="CREATE" source="_Ud0VERfzEeCvZOwhS7-eiQ" target="_UdzuDxfzEeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5JNpgBf0EeCvZOwhS7-eiQ" modifier="CREATE" source="_3GpU0xf0EeCvZOwhS7-eiQ" target="_WC-l-Rf8EeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5pjWcBf0EeCvZOwhS7-eiQ" modifier="CREATE" source="_3jQYUxf0EeCvZOwhS7-eiQ" target="_4enNUxf0EeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5_me8Bf0EeCvZOwhS7-eiQ" modifier="CREATE" source="_UdzuExfzEeCvZOwhS7-eiQ" target="_3GpU0xf0EeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_6VHb8Bf0EeCvZOwhS7-eiQ" modifier="CREATE" source="_UdzuExfzEeCvZOwhS7-eiQ" target="_3jQYUxf0EeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/helperActivityDiagrams"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_vjBOgBf4EeC2X7AVcx_M6A" modifier="CREATE" source="_UdzuDxfzEeCvZOwhS7-eiQ" target="_UdzuEhfzEeCvZOwhS7-eiQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_vqjzMBfzEeCvZOwhS7-eiQ" incoming="_wCDtMBfzEeCvZOwhS7-eiQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_xVvioBfzEeCvZOwhS7-eiQ" incoming="_x80JsBfzEeCvZOwhS7-eiQ _yjV-MBfzEeCvZOwhS7-eiQ _zedioBfzEeCvZOwhS7-eiQ" outgoing="_DiPS0Bf1EeCvZOwhS7-eiQ"/>
    <edges uuid="_VueqAPlaEd-fSKxQcXgteA" source="_Klm4IPlaEd-fSKxQcXgteA" target="_L70dIPlaEd-fSKxQcXgteA"/>
    <edges uuid="_-Te0YBdDEeCR3_w4EGJGeQ" source="_bnfr0PlaEd-fSKxQcXgteA" target="_dZVnMBdGEeCR3_w4EGJGeQ"/>
    <edges uuid="_jqKjcBfzEeCvZOwhS7-eiQ" source="_15SRMBfxEeCvZOwhS7-eiQ" target="_2Mnc4BfxEeCvZOwhS7-eiQ" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kKLgQBfzEeCvZOwhS7-eiQ" expressionString="ruleInfo.forwardTransformationActivityDiagram = null" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_mfRvYBfzEeCvZOwhS7-eiQ" source="_15SRMBfxEeCvZOwhS7-eiQ" target="_MTbGgBfzEeCvZOwhS7-eiQ" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ne8coBfzEeCvZOwhS7-eiQ" expressionString="ruleInfo.mappingTransformationActivityDiagram = null" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_qASYABfzEeCvZOwhS7-eiQ" source="_15SRMBfxEeCvZOwhS7-eiQ" target="_UdzuDhfzEeCvZOwhS7-eiQ" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rODysBfzEeCvZOwhS7-eiQ" expressionString="ruleInfo.reverseTransformationActivityDiagram = null" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_uHF1UBfzEeCvZOwhS7-eiQ" source="_L70dIPlaEd-fSKxQcXgteA" target="_1j_9sBfxEeCvZOwhS7-eiQ"/>
    <edges uuid="_uq8JQBfzEeCvZOwhS7-eiQ" source="_1j_9sBfxEeCvZOwhS7-eiQ" target="_15SRMBfxEeCvZOwhS7-eiQ"/>
    <edges uuid="_wCDtMBfzEeCvZOwhS7-eiQ" source="_15SRMBfxEeCvZOwhS7-eiQ" target="_vqjzMBfzEeCvZOwhS7-eiQ" guardType="ELSE"/>
    <edges uuid="_x80JsBfzEeCvZOwhS7-eiQ" source="_UdzuDhfzEeCvZOwhS7-eiQ" target="_xVvioBfzEeCvZOwhS7-eiQ"/>
    <edges uuid="_yjV-MBfzEeCvZOwhS7-eiQ" source="_MTbGgBfzEeCvZOwhS7-eiQ" target="_xVvioBfzEeCvZOwhS7-eiQ"/>
    <edges uuid="_zedioBfzEeCvZOwhS7-eiQ" source="_2Mnc4BfxEeCvZOwhS7-eiQ" target="_xVvioBfzEeCvZOwhS7-eiQ"/>
    <edges uuid="_4m6DIBfzEeCvZOwhS7-eiQ" source="_dZVnMBdGEeCR3_w4EGJGeQ" target="_1j_9sBfxEeCvZOwhS7-eiQ"/>
    <edges uuid="_DiPS0Bf1EeCvZOwhS7-eiQ" source="_xVvioBfzEeCvZOwhS7-eiQ" target="_bnfr0PlaEd-fSKxQcXgteA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
