<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="generate_axiom_activities" uuid="_f2pxUXUrEd-AwpZ_Fdb-iA">
  <activities name="generate_axiom_activities" uuid="_NTSQ0HhtEd-F3I0fuD-eTQ">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_Uli9gHhtEd-F3I0fuD-eTQ" outgoing="_YBSEkHhtEd-F3I0fuD-eTQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_-FElwHrTEd-C3vhV8ZLu-w" incoming="_YBSEkHhtEd-F3I0fuD-eTQ" outgoing="_OtF_gBfmEeCO-aRUjt8-yQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_nNWesBflEeCO-aRUjt8-yQ" outgoingStoryLinks="_4ugL0BfmEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_3Z5JUBfmEeCO-aRUjt8-yQ" incomingStoryLinks="_4ugL0BfmEeCO-aRUjt8-yQ">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_L__hoClhEeCw86_PpaAodg" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4ugL0BfmEeCO-aRUjt8-yQ" source="_nNWesBflEeCO-aRUjt8-yQ" target="_3Z5JUBfmEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/tggRule"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create node to match LHS" uuid="_V8DpAHrUEd-C3vhV8ZLu-w" incoming="_1xhA0BfnEeCO-aRUjt8-yQ" outgoing="_7q-scHrUEd-C3vhV8ZLu-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_qtqPsHrUEd-C3vhV8ZLu-w" outgoingStoryLinks="_1cg_EHrUEd-C3vhV8ZLu-w _18v-UHrUEd-C3vhV8ZLu-w _72HakPZIEd-o55Ll0oEtLw _ZG-skPZJEd-o55Ll0oEtLw _7bSqgAxnEeCczauSUZKYlg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="initialNode" uuid="_sWkuQHrUEd-C3vhV8ZLu-w" modifier="CREATE" incomingStoryLinks="_z51EUHrUEd-C3vhV8ZLu-w _7bSqgAxnEeCczauSUZKYlg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/InitialNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_u5nSwHrUEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_z51EUHrUEd-C3vhV8ZLu-w _0uWRgHrUEd-C3vhV8ZLu-w" incomingStoryLinks="_1cg_EHrUEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchLHSSAN" uuid="_vnAkQHrUEd-C3vhV8ZLu-w" modifier="CREATE" incomingStoryLinks="_0uWRgHrUEd-C3vhV8ZLu-w _18v-UHrUEd-C3vhV8ZLu-w _8TfTEPZIEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_RSUBQHrVEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SfmTwHrVEd-C3vhV8ZLu-w" expressionString="'match LHS'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleNotAppliedFinalNode" uuid="_4cOGkPZIEd-o55Ll0oEtLw" modifier="CREATE" outgoingStoryLinks="_hwt7wPcIEd-_yp3IZC6S-Q" incomingStoryLinks="_9HXnEPZIEd-o55Ll0oEtLw _ZG-skPZJEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_6SZlEPZIEd-o55Ll0oEtLw" modifier="CREATE" outgoingStoryLinks="_8TfTEPZIEd-o55Ll0oEtLw _9HXnEPZIEd-o55Ll0oEtLw" incomingStoryLinks="_72HakPZIEd-o55Ll0oEtLw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="__WIksPZIEd-o55Ll0oEtLw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Al3uQPZJEd-o55Ll0oEtLw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleNotAppliedExpression" uuid="_XH-bcPcIEd-_yp3IZC6S-Q" modifier="CREATE" incomingStoryLinks="_hwt7wPcIEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_b8ZzwPcIEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dBpkkPcIEd-_yp3IZC6S-Q" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_eFSL0PcIEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_e_w3EPcIEd-_yp3IZC6S-Q" expressionString="'mote::rules::TransformationResult::RULE_NOT_APPLIED'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_z51EUHrUEd-C3vhV8ZLu-w" modifier="CREATE" source="_u5nSwHrUEd-C3vhV8ZLu-w" target="_sWkuQHrUEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0uWRgHrUEd-C3vhV8ZLu-w" modifier="CREATE" source="_u5nSwHrUEd-C3vhV8ZLu-w" target="_vnAkQHrUEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1cg_EHrUEd-C3vhV8ZLu-w" modifier="CREATE" source="_qtqPsHrUEd-C3vhV8ZLu-w" target="_u5nSwHrUEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_18v-UHrUEd-C3vhV8ZLu-w" modifier="CREATE" source="_qtqPsHrUEd-C3vhV8ZLu-w" target="_vnAkQHrUEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_72HakPZIEd-o55Ll0oEtLw" modifier="CREATE" source="_qtqPsHrUEd-C3vhV8ZLu-w" target="_6SZlEPZIEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8TfTEPZIEd-o55Ll0oEtLw" modifier="CREATE" source="_6SZlEPZIEd-o55Ll0oEtLw" target="_vnAkQHrUEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9HXnEPZIEd-o55Ll0oEtLw" modifier="CREATE" source="_6SZlEPZIEd-o55Ll0oEtLw" target="_4cOGkPZIEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZG-skPZJEd-o55Ll0oEtLw" modifier="CREATE" source="_qtqPsHrUEd-C3vhV8ZLu-w" target="_4cOGkPZIEd-o55Ll0oEtLw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hwt7wPcIEd-_yp3IZC6S-Q" modifier="CREATE" source="_4cOGkPZIEd-o55Ll0oEtLw" target="_XH-bcPcIEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7bSqgAxnEeCczauSUZKYlg" modifier="CREATE" source="_qtqPsHrUEd-C3vhV8ZLu-w" target="_sWkuQHrUEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of match LHS" uuid="_5km10HrUEd-C3vhV8ZLu-w" incoming="_7q-scHrUEd-C3vhV8ZLu-w" outgoing="__k1S4HrUEd-C3vhV8ZLu-w">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_LJAIEPPTEd-cf4nuhI1Eog">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_LbxTEPPTEd-cf4nuhI1Eog">
          <activity href="create_storyPattern_match_LHS_axiom.story#_QBQAwIBhEd-Ol7HhHmHbiA"/>
          <parameters name="tggRule" uuid="_i2svoPPXEd-cAK4OkgyYOw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lKPfkPPXEd-cAK4OkgyYOw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_pEa2IPPXEd-cAK4OkgyYOw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_quDtcPPXEd-cAK4OkgyYOw" expressionString="matchLHSSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="direction" uuid="_uIP8YPPXEd-cAK4OkgyYOw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_05IaUPPXEd-cAK4OkgyYOw" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create node to evaluate rule variables" uuid="_9w4pAHrUEd-C3vhV8ZLu-w" incoming="__k1S4HrUEd-C3vhV8ZLu-w" outgoing="_IzVAAPbaEd-JnYtEWuN3-g">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_AOO1MHrVEd-C3vhV8ZLu-w" outgoingStoryLinks="_O9oB0HrVEd-C3vhV8ZLu-w _PsddwHrVEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchLHSSAN" uuid="_BvM4sHrVEd-C3vhV8ZLu-w" incomingStoryLinks="_E-0PUHrVEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_DuKfwHrVEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_E-0PUHrVEd-C3vhV8ZLu-w _QLXnUHrVEd-C3vhV8ZLu-w" incomingStoryLinks="_O9oB0HrVEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_HF6T0PZJEd-o55Ll0oEtLw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ISLSoPZJEd-o55Ll0oEtLw" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evaluateRuleVariablesEAN" uuid="_GCerwHrVEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_fp3RUPZ2Ed-i2JdGlqLvVQ" incomingStoryLinks="_PsddwHrVEd-C3vhV8ZLu-w _QLXnUHrVEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_UGW2MHrVEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VGdBQHrVEd-C3vhV8ZLu-w" expressionString="'evaluate rule variables'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_I6O3sPZ2Ed-i2JdGlqLvVQ" incomingStoryLinks="_fp3RUPZ2Ed-i2JdGlqLvVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_UBp4EPZ2Ed-i2JdGlqLvVQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_UYhf8PZ2Ed-i2JdGlqLvVQ">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
            <activity href="../common/create_evaluateRuleVariables_expression.story#_uabxYfZwEd-i2JdGlqLvVQ"/>
            <parameters name="tggRule" uuid="_YM2GIPZ2Ed-i2JdGlqLvVQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZzOOEPZ2Ed-i2JdGlqLvVQ" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_brCEQPZ2Ed-i2JdGlqLvVQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_c_A0oPZ2Ed-i2JdGlqLvVQ" expressionString="direction" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_E-0PUHrVEd-C3vhV8ZLu-w" modifier="CREATE" source="_DuKfwHrVEd-C3vhV8ZLu-w" target="_BvM4sHrVEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_O9oB0HrVEd-C3vhV8ZLu-w" modifier="CREATE" source="_AOO1MHrVEd-C3vhV8ZLu-w" target="_DuKfwHrVEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_PsddwHrVEd-C3vhV8ZLu-w" modifier="CREATE" source="_AOO1MHrVEd-C3vhV8ZLu-w" target="_GCerwHrVEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_QLXnUHrVEd-C3vhV8ZLu-w" modifier="CREATE" source="_DuKfwHrVEd-C3vhV8ZLu-w" target="_GCerwHrVEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fp3RUPZ2Ed-i2JdGlqLvVQ" modifier="CREATE" source="_GCerwHrVEd-C3vhV8ZLu-w" target="_I6O3sPZ2Ed-i2JdGlqLvVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create node to create elements" uuid="__J2KkHrVEd-C3vhV8ZLu-w" incoming="_KeSF8PbaEd-JnYtEWuN3-g" outgoing="_wDYFQHrWEd-C3vhV8ZLu-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkLHSConstraintsSAN" uuid="_B8BXQHrWEd-C3vhV8ZLu-w" incomingStoryLinks="_MB8eYHrWEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_EPlVUHrWEd-C3vhV8ZLu-w" outgoingStoryLinks="_KsjYwHrWEd-C3vhV8ZLu-w _LNNOwHrWEd-C3vhV8ZLu-w _itlI0HrWEd-C3vhV8ZLu-w _jQ5RQHrWEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createElementsSAN" uuid="_F3yCMHrWEd-C3vhV8ZLu-w" modifier="CREATE" incomingStoryLinks="_LNNOwHrWEd-C3vhV8ZLu-w _NATkEHrWEd-C3vhV8ZLu-w _fcekcHrWEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_N8rFsHrWEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_O8Kz0HrWEd-C3vhV8ZLu-w" expressionString="'create elements'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_JbJbQHrWEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_MB8eYHrWEd-C3vhV8ZLu-w _NATkEHrWEd-C3vhV8ZLu-w" incomingStoryLinks="_KsjYwHrWEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_g0V2QPcMEd-CI4Kg3lq6HA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hqeNAPcMEd-CI4Kg3lq6HA" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleNotAppliedFinalNode" uuid="_Wp24QHrWEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_8MZwYPcIEd-_yp3IZC6S-Q" incomingStoryLinks="_hmioQHrWEd-C3vhV8ZLu-w _jQ5RQHrWEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_eQRQAHrWEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_fcekcHrWEd-C3vhV8ZLu-w _hmioQHrWEd-C3vhV8ZLu-w" incomingStoryLinks="_itlI0HrWEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_klqEwHrWEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lj7D0HrWEd-C3vhV8ZLu-w" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleNotAppliedExpression" uuid="_094UMPcIEd-_yp3IZC6S-Q" modifier="CREATE" incomingStoryLinks="_8MZwYPcIEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_2__V4PcIEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_33gMsPcIEd-_yp3IZC6S-Q" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_4usTYPcIEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5uqioPcIEd-_yp3IZC6S-Q" expressionString="'mote::rules::TransformationResult::RULE_NOT_APPLIED'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_KsjYwHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_EPlVUHrWEd-C3vhV8ZLu-w" target="_JbJbQHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LNNOwHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_EPlVUHrWEd-C3vhV8ZLu-w" target="_F3yCMHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_MB8eYHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_JbJbQHrWEd-C3vhV8ZLu-w" target="_B8BXQHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_NATkEHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_JbJbQHrWEd-C3vhV8ZLu-w" target="_F3yCMHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_fcekcHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_eQRQAHrWEd-C3vhV8ZLu-w" target="_F3yCMHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hmioQHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_eQRQAHrWEd-C3vhV8ZLu-w" target="_Wp24QHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_itlI0HrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_EPlVUHrWEd-C3vhV8ZLu-w" target="_eQRQAHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jQ5RQHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_EPlVUHrWEd-C3vhV8ZLu-w" target="_Wp24QHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8MZwYPcIEd-_yp3IZC6S-Q" modifier="CREATE" source="_Wp24QHrWEd-C3vhV8ZLu-w" target="_094UMPcIEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of create elements" uuid="_RZBQwHrWEd-C3vhV8ZLu-w" incoming="_wDYFQHrWEd-C3vhV8ZLu-w" outgoing="_E-dYsPcFEd-_yp3IZC6S-Q">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_tD9FMPb1Ed-JnYtEWuN3-g">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_tYZrMPb1Ed-JnYtEWuN3-g">
          <activity href="create_storyPattern_create_RHS_axiom.story#_emvIUPbiEd-JnYtEWuN3-g"/>
          <parameters name="tggRule" uuid="_zAkNkPb1Ed-JnYtEWuN3-g">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0peFEPb1Ed-JnYtEWuN3-g" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="direction" uuid="_1i3LMPb1Ed-JnYtEWuN3-g">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3CqKAPb1Ed-JnYtEWuN3-g" expressionString="direction" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_3-YLYPb1Ed-JnYtEWuN3-g">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5ty6kPb1Ed-JnYtEWuN3-g" expressionString="createElementsSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create node to check target constraints" uuid="_ynUosHrWEd-C3vhV8ZLu-w" incoming="_FdW7MPcFEd-_yp3IZC6S-Q" outgoing="_TRfY0HrXEd-C3vhV8ZLu-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evaluateRuleVariables2EAN" uuid="_15N3QHrWEd-C3vhV8ZLu-w" incomingStoryLinks="_-JV6YHrWEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_39k7sHrWEd-C3vhV8ZLu-w" outgoingStoryLinks="_9KwyQHrWEd-C3vhV8ZLu-w _9o-_UHrWEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_5dGNsHrWEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_-JV6YHrWEd-C3vhV8ZLu-w __DU2YHrWEd-C3vhV8ZLu-w" incomingStoryLinks="_9KwyQHrWEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkTargetConstraintsSAN" uuid="_6zqYAHrWEd-C3vhV8ZLu-w" modifier="CREATE" incomingStoryLinks="_9o-_UHrWEd-C3vhV8ZLu-w __DU2YHrWEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_iJ_osHrYEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jodx0HrYEd-C3vhV8ZLu-w" expressionString="'check all constraints'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9KwyQHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_39k7sHrWEd-C3vhV8ZLu-w" target="_5dGNsHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9o-_UHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_39k7sHrWEd-C3vhV8ZLu-w" target="_6zqYAHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_-JV6YHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_5dGNsHrWEd-C3vhV8ZLu-w" target="_15N3QHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="__DU2YHrWEd-C3vhV8ZLu-w" modifier="CREATE" source="_5dGNsHrWEd-C3vhV8ZLu-w" target="_6zqYAHrWEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of check target constraints" uuid="_RZxCMHrXEd-C3vhV8ZLu-w" incoming="_TRfY0HrXEd-C3vhV8ZLu-w" outgoing="_ZdwGwHrXEd-C3vhV8ZLu-w">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_ErufYPcEEd-_yp3IZC6S-Q">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_FOhDYPcEEd-_yp3IZC6S-Q">
          <activity href="create_storyPattern_check_constraints.story#_2MC_APbdEd-JnYtEWuN3-g"/>
          <parameters name="tggRule" uuid="_GwZs4PcEEd-_yp3IZC6S-Q">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IvWF0PcEEd-_yp3IZC6S-Q" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_MQ3EwPcEEd-_yp3IZC6S-Q">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_N5J4QPcEEd-_yp3IZC6S-Q" expressionString="checkTargetConstraintsSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="createSourceConstraints" uuid="_lMwYEBREEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_m1kwABREEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetConstraints" uuid="_pu2DMBREEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rXc_wBREEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentNodesConstraints" uuid="_sO5lIBREEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_uMprQBREEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedNodesConstraints" uuid="_vWzKMBREEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xtG6QBREEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create node to add modification tag to queue" uuid="_UcPUsHrXEd-C3vhV8ZLu-w" incoming="_ZdwGwHrXEd-C3vhV8ZLu-w" outgoing="_EZaU0HrYEd-C3vhV8ZLu-w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_alxewHrXEd-C3vhV8ZLu-w" outgoingStoryLinks="_udXUQHrXEd-C3vhV8ZLu-w _u_P5UHrXEd-C3vhV8ZLu-w _yjXgUHrXEd-C3vhV8ZLu-w _Bg_9QHrYEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkTargetConstraintsSAN" uuid="_fbt6AHrXEd-C3vhV8ZLu-w" incomingStoryLinks="_s8U_UHrXEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="addModTagEAN" uuid="_i4UjwHrXEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_IaGegPcYEd-PZbH3UvWGmw" incomingStoryLinks="_tsiUQHrXEd-C3vhV8ZLu-w _u_P5UHrXEd-C3vhV8ZLu-w _xsNO0HrXEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_ltXBwHrYEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_m9ZtUHrYEd-C3vhV8ZLu-w" expressionString="'add modification tag to queue'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_me3zsHrXEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_s8U_UHrXEd-C3vhV8ZLu-w _tsiUQHrXEd-C3vhV8ZLu-w" incomingStoryLinks="_udXUQHrXEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_n4l_sHrXEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_o09hUHrXEd-C3vhV8ZLu-w" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_vtoCQHrXEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_xsNO0HrXEd-C3vhV8ZLu-w _0_rxwHrXEd-C3vhV8ZLu-w" incomingStoryLinks="_yjXgUHrXEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleAppliedFinalNode" uuid="_zQ0cMHrXEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_GUL4sPcJEd-_yp3IZC6S-Q" incomingStoryLinks="_0_rxwHrXEd-C3vhV8ZLu-w _Bg_9QHrYEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleAppliedExpression" uuid="_-XaPYPcIEd-_yp3IZC6S-Q" modifier="CREATE" incomingStoryLinks="_GUL4sPcJEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_A0QFQPcJEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_B7GYkPcJEd-_yp3IZC6S-Q" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_C4w6sPcJEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_D4M-cPcJEd-_yp3IZC6S-Q" expressionString="'mote::rules::TransformationResult::RULE_APPLIED'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="addModTagExpression" uuid="_91ZJoPcXEd-PZbH3UvWGmw" incomingStoryLinks="_IaGegPcYEd-PZbH3UvWGmw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_CtKJ0PcYEd-PZbH3UvWGmw">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_DEWh0PcYEd-PZbH3UvWGmw">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
            <activity href="../common/create_addModifcationTagToQueue_expression.story#_lXQIIPcJEd-_yp3IZC6S-Q"/>
            <parameters name="tggRule" uuid="_E9z90PcYEd-PZbH3UvWGmw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GhmEwPcYEd-PZbH3UvWGmw" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_s8U_UHrXEd-C3vhV8ZLu-w" modifier="CREATE" source="_me3zsHrXEd-C3vhV8ZLu-w" target="_fbt6AHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tsiUQHrXEd-C3vhV8ZLu-w" modifier="CREATE" source="_me3zsHrXEd-C3vhV8ZLu-w" target="_i4UjwHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_udXUQHrXEd-C3vhV8ZLu-w" modifier="CREATE" source="_alxewHrXEd-C3vhV8ZLu-w" target="_me3zsHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_u_P5UHrXEd-C3vhV8ZLu-w" modifier="CREATE" source="_alxewHrXEd-C3vhV8ZLu-w" target="_i4UjwHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xsNO0HrXEd-C3vhV8ZLu-w" modifier="CREATE" source="_vtoCQHrXEd-C3vhV8ZLu-w" target="_i4UjwHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_yjXgUHrXEd-C3vhV8ZLu-w" modifier="CREATE" source="_alxewHrXEd-C3vhV8ZLu-w" target="_vtoCQHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0_rxwHrXEd-C3vhV8ZLu-w" modifier="CREATE" source="_vtoCQHrXEd-C3vhV8ZLu-w" target="_zQ0cMHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Bg_9QHrYEd-C3vhV8ZLu-w" modifier="CREATE" source="_alxewHrXEd-C3vhV8ZLu-w" target="_zQ0cMHrXEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GUL4sPcJEd-_yp3IZC6S-Q" modifier="CREATE" source="_zQ0cMHrXEd-C3vhV8ZLu-w" target="_-XaPYPcIEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_IaGegPcYEd-PZbH3UvWGmw" modifier="CREATE" source="_i4UjwHrXEd-C3vhV8ZLu-w" target="_91ZJoPcXEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create node to report errors" uuid="_FTpvgHrYEd-C3vhV8ZLu-w" incoming="_EZaU0HrYEd-C3vhV8ZLu-w" outgoing="_5bpwQBfnEeCO-aRUjt8-yQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkTargetConstraintsSAN" uuid="_IcaQMHrYEd-C3vhV8ZLu-w" incomingStoryLinks="_SzPhIHrYEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_K6VrMHrYEd-C3vhV8ZLu-w" outgoingStoryLinks="_UqvOQHrYEd-C3vhV8ZLu-w _VMsEwHrYEd-C3vhV8ZLu-w _1Ck4QHrYEd-C3vhV8ZLu-w _1f1cAHrYEd-C3vhV8ZLu-w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="reportErrorEAN" uuid="_MVrAwHrYEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_7tDXYPcHEd-_yp3IZC6S-Q" incomingStoryLinks="_TofHEHrYEd-C3vhV8ZLu-w _UqvOQHrYEd-C3vhV8ZLu-w _wmiKwHrYEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_pKOxQHrYEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qHC-0HrYEd-C3vhV8ZLu-w" expressionString="'report error'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_ObalQHrYEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_SzPhIHrYEd-C3vhV8ZLu-w _TofHEHrYEd-C3vhV8ZLu-w" incomingStoryLinks="_VMsEwHrYEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_PtbmwHrYEd-C3vhV8ZLu-w">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Qkg_wHrYEd-C3vhV8ZLu-w" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_uiIDAHrYEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_wmiKwHrYEd-C3vhV8ZLu-w _0JO1cHrYEd-C3vhV8ZLu-w" incomingStoryLinks="_1Ck4QHrYEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="errorFinalNode" uuid="_ytf3QHrYEd-C3vhV8ZLu-w" modifier="CREATE" outgoingStoryLinks="_PMogEPcJEd-_yp3IZC6S-Q" incomingStoryLinks="_0JO1cHrYEd-C3vhV8ZLu-w _1f1cAHrYEd-C3vhV8ZLu-w">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="printErrorExpression" uuid="_rNZuEPcHEd-_yp3IZC6S-Q" incomingStoryLinks="_7tDXYPcHEd-_yp3IZC6S-Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_uyoIUPcHEd-_yp3IZC6S-Q">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_xgQzQPcHEd-_yp3IZC6S-Q">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
            <activity href="../common/create_printError_expression.story#_pwLNAPcFEd-_yp3IZC6S-Q"/>
            <parameters name="message" uuid="_zCkTgPcHEd-_yp3IZC6S-Q">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1QrJ0PcHEd-_yp3IZC6S-Q" expressionString="'Error: Constraints of rule '.concat(tggRule.name).concat(' are not satisfied after transformation.')" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="errorExpression" uuid="_I6rZ0PcJEd-_yp3IZC6S-Q" modifier="CREATE" incomingStoryLinks="_PMogEPcJEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_KoRkEPcJEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_LXZT4PcJEd-_yp3IZC6S-Q" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_MGHbEPcJEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_M2MNIPcJEd-_yp3IZC6S-Q" expressionString="'mote::rules::TransformationResult::CONFLICT_DETECTED'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_SzPhIHrYEd-C3vhV8ZLu-w" modifier="CREATE" source="_ObalQHrYEd-C3vhV8ZLu-w" target="_IcaQMHrYEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_TofHEHrYEd-C3vhV8ZLu-w" modifier="CREATE" source="_ObalQHrYEd-C3vhV8ZLu-w" target="_MVrAwHrYEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_UqvOQHrYEd-C3vhV8ZLu-w" modifier="CREATE" source="_K6VrMHrYEd-C3vhV8ZLu-w" target="_MVrAwHrYEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_VMsEwHrYEd-C3vhV8ZLu-w" modifier="CREATE" source="_K6VrMHrYEd-C3vhV8ZLu-w" target="_ObalQHrYEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_wmiKwHrYEd-C3vhV8ZLu-w" modifier="CREATE" source="_uiIDAHrYEd-C3vhV8ZLu-w" target="_MVrAwHrYEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0JO1cHrYEd-C3vhV8ZLu-w" modifier="CREATE" source="_uiIDAHrYEd-C3vhV8ZLu-w" target="_ytf3QHrYEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1Ck4QHrYEd-C3vhV8ZLu-w" modifier="CREATE" source="_K6VrMHrYEd-C3vhV8ZLu-w" target="_uiIDAHrYEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1f1cAHrYEd-C3vhV8ZLu-w" modifier="CREATE" source="_K6VrMHrYEd-C3vhV8ZLu-w" target="_ytf3QHrYEd-C3vhV8ZLu-w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7tDXYPcHEd-_yp3IZC6S-Q" modifier="CREATE" source="_MVrAwHrYEd-C3vhV8ZLu-w" target="_rNZuEPcHEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_PMogEPcJEd-_yp3IZC6S-Q" modifier="CREATE" source="_ytf3QHrYEd-C3vhV8ZLu-w" target="_I6rZ0PcJEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create node to check constraints of LHS" uuid="_HJ0EgPbaEd-JnYtEWuN3-g" incoming="_IzVAAPbaEd-JnYtEWuN3-g" outgoing="_JX7scPbbEd-JnYtEWuN3-g">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evaluateRuleVariablesEAN" uuid="_LGKlUPbaEd-JnYtEWuN3-g" incomingStoryLinks="_jNaSIPbaEd-JnYtEWuN3-g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_PLOWMPbaEd-JnYtEWuN3-g" modifier="CREATE" outgoingStoryLinks="_jNaSIPbaEd-JnYtEWuN3-g _kSTdoPbaEd-JnYtEWuN3-g" incomingStoryLinks="_4QLhUPbaEd-JnYtEWuN3-g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_PxIfoPbaEd-JnYtEWuN3-g" outgoingStoryLinks="_4QLhUPbaEd-JnYtEWuN3-g _4uM6EPbaEd-JnYtEWuN3-g _5B__0PbaEd-JnYtEWuN3-g _5XKXgPbaEd-JnYtEWuN3-g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="checkLHSConstraintsSAN" uuid="_Q0uqoPbaEd-JnYtEWuN3-g" modifier="CREATE" incomingStoryLinks="_kSTdoPbaEd-JnYtEWuN3-g _x2ur8PbaEd-JnYtEWuN3-g _4uM6EPbaEd-JnYtEWuN3-g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
        <attributeAssignments uuid="_vAv18PbdEd-JnYtEWuN3-g">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wcCvQPbdEd-JnYtEWuN3-g" expressionString="'check LHS constaints'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleNotAppliedFinalNode" uuid="_WCfI8PbaEd-JnYtEWuN3-g" modifier="CREATE" outgoingStoryLinks="_unI0kPcIEd-_yp3IZC6S-Q" incomingStoryLinks="_zGbZQPbaEd-JnYtEWuN3-g _5XKXgPbaEd-JnYtEWuN3-g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge2" uuid="_o2QJ8PbaEd-JnYtEWuN3-g" modifier="CREATE" outgoingStoryLinks="_x2ur8PbaEd-JnYtEWuN3-g _zGbZQPbaEd-JnYtEWuN3-g" incomingStoryLinks="_5B__0PbaEd-JnYtEWuN3-g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_qig3cPbaEd-JnYtEWuN3-g">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r0WSwPbaEd-JnYtEWuN3-g" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::FAILURE" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleNotAppliedExpression" uuid="_n8zDgPcIEd-_yp3IZC6S-Q" modifier="CREATE" incomingStoryLinks="_unI0kPcIEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_paz5oPcIEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qP8x4PcIEd-_yp3IZC6S-Q" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_rGmtEPcIEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r8vq4PcIEd-_yp3IZC6S-Q" expressionString="'mote::rules::TransformationResult::RULE_NOT_APPLIED'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jNaSIPbaEd-JnYtEWuN3-g" modifier="CREATE" source="_PLOWMPbaEd-JnYtEWuN3-g" target="_LGKlUPbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kSTdoPbaEd-JnYtEWuN3-g" modifier="CREATE" source="_PLOWMPbaEd-JnYtEWuN3-g" target="_Q0uqoPbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_x2ur8PbaEd-JnYtEWuN3-g" modifier="CREATE" source="_o2QJ8PbaEd-JnYtEWuN3-g" target="_Q0uqoPbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zGbZQPbaEd-JnYtEWuN3-g" modifier="CREATE" source="_o2QJ8PbaEd-JnYtEWuN3-g" target="_WCfI8PbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4QLhUPbaEd-JnYtEWuN3-g" modifier="CREATE" source="_PxIfoPbaEd-JnYtEWuN3-g" target="_PLOWMPbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4uM6EPbaEd-JnYtEWuN3-g" modifier="CREATE" source="_PxIfoPbaEd-JnYtEWuN3-g" target="_Q0uqoPbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5B__0PbaEd-JnYtEWuN3-g" modifier="CREATE" source="_PxIfoPbaEd-JnYtEWuN3-g" target="_o2QJ8PbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5XKXgPbaEd-JnYtEWuN3-g" modifier="CREATE" source="_PxIfoPbaEd-JnYtEWuN3-g" target="_WCfI8PbaEd-JnYtEWuN3-g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_unI0kPcIEd-_yp3IZC6S-Q" modifier="CREATE" source="_WCfI8PbaEd-JnYtEWuN3-g" target="_n8zDgPcIEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityFinalNode/returnValue"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create content of checkLHSConstraintsSAN" uuid="_DcPxIPbbEd-JnYtEWuN3-g" incoming="_JX7scPbbEd-JnYtEWuN3-g" outgoing="_KeSF8PbaEd-JnYtEWuN3-g">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_OLCnYPbhEd-JnYtEWuN3-g">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_O26O4PbhEd-JnYtEWuN3-g">
          <activity href="create_storyPattern_check_constraints.story#_2MC_APbdEd-JnYtEWuN3-g"/>
          <parameters name="tggRule" uuid="_ToE30PbhEd-JnYtEWuN3-g">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VPGgAPbhEd-JnYtEWuN3-g" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_ZXkxQPbhEd-JnYtEWuN3-g">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_doMR4PbhEd-JnYtEWuN3-g" expressionString="checkLHSConstraintsSAN" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="createSourceConstraints" uuid="_G35ScBREEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IyTJoBREEeC9udBuGs2jiA" expressionString="direction = mote::TransformationDirection::FORWARD or&#xA;direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetConstraints" uuid="_N7OyUBREEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_PzSgIBREEeC9udBuGs2jiA" expressionString="direction = mote::TransformationDirection::MAPPING or&#xA;direction = mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentNodesConstraints" uuid="_Ss_4MBREEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_V-GckBREEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedNodesConstraints" uuid="_XJf4sBREEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZMo1IBREEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node to evaluate rule variables using the opposite computation specification" uuid="_cnsJQPcEEd-_yp3IZC6S-Q" incoming="_E-dYsPcFEd-_yp3IZC6S-Q" outgoing="_FdW7MPcFEd-_yp3IZC6S-Q">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_lZLNAPcEEd-_yp3IZC6S-Q" outgoingStoryLinks="_lZLNEPcEEd-_yp3IZC6S-Q _lZLNEfcEEd-_yp3IZC6S-Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createElementsSAN" uuid="_lZLNAfcEEd-_yp3IZC6S-Q" incomingStoryLinks="_lZLND_cEEd-_yp3IZC6S-Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="edge" uuid="_lZLNAvcEEd-_yp3IZC6S-Q" modifier="CREATE" outgoingStoryLinks="_lZLND_cEEd-_yp3IZC6S-Q _lZLNEvcEEd-_yp3IZC6S-Q" incomingStoryLinks="_lZLNEPcEEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
        <attributeAssignments uuid="_lZLNA_cEEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/guardType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lZLNBPcEEd-_yp3IZC6S-Q" expressionString="storyDiagramEcore::nodes::ActivityEdgeGuardEnumeration::SUCCESS" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="evaluateRuleVariables2EAN" uuid="_lZLNBfcEEd-_yp3IZC6S-Q" modifier="CREATE" outgoingStoryLinks="_lZLNE_cEEd-_yp3IZC6S-Q" incomingStoryLinks="_lZLNEfcEEd-_yp3IZC6S-Q _lZLNEvcEEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_lZLNBvcEEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lZLNB_cEEd-_yp3IZC6S-Q" expressionString="'evaluate rule  variables 2'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_lZLNCPcEEd-_yp3IZC6S-Q" incomingStoryLinks="_lZLNE_cEEd-_yp3IZC6S-Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_lZLNCfcEEd-_yp3IZC6S-Q">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_lZLNCvcEEd-_yp3IZC6S-Q">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
            <activity href="../common/create_evaluateRuleVariables_expression.story#_uabxYfZwEd-i2JdGlqLvVQ"/>
            <parameters name="tggRule" uuid="_lZLNC_cEEd-_yp3IZC6S-Q">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lZLNDPcEEd-_yp3IZC6S-Q" expressionString="tggRule" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
            </parameters>
            <parameters name="direction" uuid="_lZLNDfcEEd-_yp3IZC6S-Q">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lZLNDvcEEd-_yp3IZC6S-Q" expressionString="if (direction = mote::TransformationDirection::FORWARD or &#xA;&#x9;direction = mote::TransformationDirection::MAPPING) then&#xA;&#x9;mote::TransformationDirection::REVERSE&#xA;else&#xA;&#x9;mote::TransformationDirection::FORWARD&#xA;endif" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lZLND_cEEd-_yp3IZC6S-Q" modifier="CREATE" source="_lZLNAvcEEd-_yp3IZC6S-Q" target="_lZLNAfcEEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lZLNEPcEEd-_yp3IZC6S-Q" modifier="CREATE" source="_lZLNAPcEEd-_yp3IZC6S-Q" target="_lZLNAvcEEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lZLNEfcEEd-_yp3IZC6S-Q" modifier="CREATE" source="_lZLNAPcEEd-_yp3IZC6S-Q" target="_lZLNBfcEEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lZLNEvcEEd-_yp3IZC6S-Q" modifier="CREATE" source="_lZLNAvcEEd-_yp3IZC6S-Q" target="_lZLNBfcEEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lZLNE_cEEd-_yp3IZC6S-Q" modifier="CREATE" source="_lZLNBfcEEd-_yp3IZC6S-Q" target="_lZLNCPcEEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_N8HOgBfmEeCO-aRUjt8-yQ" incoming="_OtF_gBfmEeCO-aRUjt8-yQ _5bpwQBfnEeCO-aRUjt8-yQ" outgoing="_PEDuABfmEeCO-aRUjt8-yQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_OQs-cBfmEeCO-aRUjt8-yQ" incoming="_PEDuABfmEeCO-aRUjt8-yQ" outgoing="_QCpdMBfmEeCO-aRUjt8-yQ _b9-KQBfnEeCO-aRUjt8-yQ _qjAhABfnEeCO-aRUjt8-yQ _v9cwcBfnEeCO-aRUjt8-yQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set forward parameters" uuid="_VCRY4BfmEeCO-aRUjt8-yQ" incoming="_QCpdMBfmEeCO-aRUjt8-yQ" outgoing="_0RFI0BfnEeCO-aRUjt8-yQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_bWJxwBfmEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sl0iwBfmEeCO-aRUjt8-yQ" expressionString="mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eOperation" uuid="_dd6IcBfmEeCO-aRUjt8-yQ" incomingStoryLinks="_p5SrEBfmEeCO-aRUjt8-yQ _JrpocBfyEeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_eIXasBfmEeCO-aRUjt8-yQ" modifier="CREATE" outgoingStoryLinks="_JrpocBfyEeCvZOwhS7-eiQ" incomingStoryLinks="_0u2fYBfmEeCO-aRUjt8-yQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_As4XoBfnEeCO-aRUjt8-yQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_BMvjcBfnEeCO-aRUjt8-yQ" expressionString="tggRule.name.concat('_forwardTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activityDiagram" uuid="_j3HGMBfmEeCO-aRUjt8-yQ" modifier="CREATE" outgoingStoryLinks="_0u2fYBfmEeCO-aRUjt8-yQ" incomingStoryLinks="_zSw74BfmEeCO-aRUjt8-yQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_8J1DMBfmEeCO-aRUjt8-yQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_886F8BfmEeCO-aRUjt8-yQ" expressionString="tggRule.name.concat('_forwardTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_lUylIBfmEeCO-aRUjt8-yQ" outgoingStoryLinks="_p5SrEBfmEeCO-aRUjt8-yQ _zSw74BfmEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_p5SrEBfmEeCO-aRUjt8-yQ" source="_lUylIBfmEeCO-aRUjt8-yQ" target="_dd6IcBfmEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/forwardTransformationOperation"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zSw74BfmEeCO-aRUjt8-yQ" modifier="CREATE" source="_lUylIBfmEeCO-aRUjt8-yQ" target="_j3HGMBfmEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/forwardTransformationActivityDiagram"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0u2fYBfmEeCO-aRUjt8-yQ" modifier="CREATE" source="_j3HGMBfmEeCO-aRUjt8-yQ" target="_eIXasBfmEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_JrpocBfyEeCvZOwhS7-eiQ" modifier="CREATE" source="_eIXasBfmEeCO-aRUjt8-yQ" target="_dd6IcBfmEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set reverse parameters" uuid="_Ic1s8BfnEeCO-aRUjt8-yQ" incoming="_qjAhABfnEeCO-aRUjt8-yQ" outgoing="_1Q8qYBfnEeCO-aRUjt8-yQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_Ic2UABfnEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ic2UARfnEeCO-aRUjt8-yQ" expressionString="mote::TransformationDirection::REVERSE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eOperation" uuid="_Ic2UBhfnEeCO-aRUjt8-yQ" incomingStoryLinks="_Ic1s8hfnEeCO-aRUjt8-yQ _HqHOgBfyEeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_Ic2UCBfnEeCO-aRUjt8-yQ" modifier="CREATE" outgoingStoryLinks="_HqHOgBfyEeCvZOwhS7-eiQ" incomingStoryLinks="_Ic2UAhfnEeCO-aRUjt8-yQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_Ic2UCRfnEeCO-aRUjt8-yQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ic2UChfnEeCO-aRUjt8-yQ" expressionString="tggRule.name.concat('_reverseTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activityDiagram" uuid="_Ic2UAxfnEeCO-aRUjt8-yQ" modifier="CREATE" outgoingStoryLinks="_Ic2UAhfnEeCO-aRUjt8-yQ" incomingStoryLinks="_Ic1s8RfnEeCO-aRUjt8-yQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_Ic2UBBfnEeCO-aRUjt8-yQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ic2UBRfnEeCO-aRUjt8-yQ" expressionString="tggRule.name.concat('_reverseTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_Ic2UBxfnEeCO-aRUjt8-yQ" outgoingStoryLinks="_Ic1s8hfnEeCO-aRUjt8-yQ _Ic1s8RfnEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ic1s8hfnEeCO-aRUjt8-yQ" source="_Ic2UBxfnEeCO-aRUjt8-yQ" target="_Ic2UBhfnEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/reverseTransformationOperation"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ic1s8RfnEeCO-aRUjt8-yQ" modifier="CREATE" source="_Ic2UBxfnEeCO-aRUjt8-yQ" target="_Ic2UAxfnEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/reverseTransformationActivityDiagram"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ic2UAhfnEeCO-aRUjt8-yQ" modifier="CREATE" source="_Ic2UAxfnEeCO-aRUjt8-yQ" target="_Ic2UCBfnEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_HqHOgBfyEeCvZOwhS7-eiQ" modifier="CREATE" source="_Ic2UCBfnEeCO-aRUjt8-yQ" target="_Ic2UBhfnEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set mapping parameters" uuid="_K7mOYBfnEeCO-aRUjt8-yQ" incoming="_b9-KQBfnEeCO-aRUjt8-yQ" outgoing="_05JOYBfnEeCO-aRUjt8-yQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_K7mOaBfnEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_K7mOaRfnEeCO-aRUjt8-yQ" expressionString="mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eOperation" uuid="_K7mOahfnEeCO-aRUjt8-yQ" incomingStoryLinks="_K7mObBfnEeCO-aRUjt8-yQ _IveUEBfyEeCvZOwhS7-eiQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_K7mOZRfnEeCO-aRUjt8-yQ" modifier="CREATE" outgoingStoryLinks="_IveUEBfyEeCvZOwhS7-eiQ" incomingStoryLinks="_K7mOZBfnEeCO-aRUjt8-yQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
        <attributeAssignments uuid="_K7mOZhfnEeCO-aRUjt8-yQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_K7mOZxfnEeCO-aRUjt8-yQ" expressionString="tggRule.name.concat('_mappingTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activityDiagram" uuid="_K7mOYRfnEeCO-aRUjt8-yQ" modifier="CREATE" outgoingStoryLinks="_K7mOZBfnEeCO-aRUjt8-yQ" incomingStoryLinks="_K7mOaxfnEeCO-aRUjt8-yQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram"/>
        <attributeAssignments uuid="_K7mOYhfnEeCO-aRUjt8-yQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_K7mOYxfnEeCO-aRUjt8-yQ" expressionString="tggRule.name.concat('_mappingTransformation')" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleInfo" uuid="_K7mObRfnEeCO-aRUjt8-yQ" outgoingStoryLinks="_K7mObBfnEeCO-aRUjt8-yQ _K7mOaxfnEeCO-aRUjt8-yQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_K7mObBfnEeCO-aRUjt8-yQ" source="_K7mObRfnEeCO-aRUjt8-yQ" target="_K7mOahfnEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/mappingTransformationOperation"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_K7mOaxfnEeCO-aRUjt8-yQ" modifier="CREATE" source="_K7mObRfnEeCO-aRUjt8-yQ" target="_K7mOYRfnEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/RuleInfoStore/mappingTransformationActivityDiagram"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_K7mOZBfnEeCO-aRUjt8-yQ" modifier="CREATE" source="_K7mOYRfnEeCO-aRUjt8-yQ" target="_K7mOZRfnEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//ActivityDiagram/activities"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_IveUEBfyEeCvZOwhS7-eiQ" modifier="CREATE" source="_K7mOZRfnEeCO-aRUjt8-yQ" target="_K7mOahfnEeCO-aRUjt8-yQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/specification"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_vUKi4BfnEeCO-aRUjt8-yQ" incoming="_v9cwcBfnEeCO-aRUjt8-yQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_zq7u0BfnEeCO-aRUjt8-yQ" incoming="_0RFI0BfnEeCO-aRUjt8-yQ _05JOYBfnEeCO-aRUjt8-yQ _1Q8qYBfnEeCO-aRUjt8-yQ" outgoing="_1xhA0BfnEeCO-aRUjt8-yQ"/>
    <edges uuid="_YBSEkHhtEd-F3I0fuD-eTQ" source="_Uli9gHhtEd-F3I0fuD-eTQ" target="_-FElwHrTEd-C3vhV8ZLu-w"/>
    <edges uuid="_7q-scHrUEd-C3vhV8ZLu-w" source="_V8DpAHrUEd-C3vhV8ZLu-w" target="_5km10HrUEd-C3vhV8ZLu-w"/>
    <edges uuid="__k1S4HrUEd-C3vhV8ZLu-w" source="_5km10HrUEd-C3vhV8ZLu-w" target="_9w4pAHrUEd-C3vhV8ZLu-w"/>
    <edges uuid="_wDYFQHrWEd-C3vhV8ZLu-w" source="__J2KkHrVEd-C3vhV8ZLu-w" target="_RZBQwHrWEd-C3vhV8ZLu-w"/>
    <edges uuid="_TRfY0HrXEd-C3vhV8ZLu-w" source="_ynUosHrWEd-C3vhV8ZLu-w" target="_RZxCMHrXEd-C3vhV8ZLu-w"/>
    <edges uuid="_ZdwGwHrXEd-C3vhV8ZLu-w" source="_RZxCMHrXEd-C3vhV8ZLu-w" target="_UcPUsHrXEd-C3vhV8ZLu-w"/>
    <edges uuid="_EZaU0HrYEd-C3vhV8ZLu-w" source="_UcPUsHrXEd-C3vhV8ZLu-w" target="_FTpvgHrYEd-C3vhV8ZLu-w"/>
    <edges uuid="_IzVAAPbaEd-JnYtEWuN3-g" source="_9w4pAHrUEd-C3vhV8ZLu-w" target="_HJ0EgPbaEd-JnYtEWuN3-g"/>
    <edges uuid="_KeSF8PbaEd-JnYtEWuN3-g" source="_DcPxIPbbEd-JnYtEWuN3-g" target="__J2KkHrVEd-C3vhV8ZLu-w"/>
    <edges uuid="_JX7scPbbEd-JnYtEWuN3-g" source="_HJ0EgPbaEd-JnYtEWuN3-g" target="_DcPxIPbbEd-JnYtEWuN3-g"/>
    <edges uuid="_E-dYsPcFEd-_yp3IZC6S-Q" source="_RZBQwHrWEd-C3vhV8ZLu-w" target="_cnsJQPcEEd-_yp3IZC6S-Q"/>
    <edges uuid="_FdW7MPcFEd-_yp3IZC6S-Q" source="_cnsJQPcEEd-_yp3IZC6S-Q" target="_ynUosHrWEd-C3vhV8ZLu-w"/>
    <edges uuid="_OtF_gBfmEeCO-aRUjt8-yQ" source="_-FElwHrTEd-C3vhV8ZLu-w" target="_N8HOgBfmEeCO-aRUjt8-yQ"/>
    <edges uuid="_PEDuABfmEeCO-aRUjt8-yQ" source="_N8HOgBfmEeCO-aRUjt8-yQ" target="_OQs-cBfmEeCO-aRUjt8-yQ"/>
    <edges uuid="_QCpdMBfmEeCO-aRUjt8-yQ" source="_OQs-cBfmEeCO-aRUjt8-yQ" target="_VCRY4BfmEeCO-aRUjt8-yQ" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Q9Co4BfmEeCO-aRUjt8-yQ" expressionString="ruleInfo.forwardTransformationActivityDiagram = null" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_b9-KQBfnEeCO-aRUjt8-yQ" source="_OQs-cBfmEeCO-aRUjt8-yQ" target="_K7mOYBfnEeCO-aRUjt8-yQ" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dS2SgBfnEeCO-aRUjt8-yQ" expressionString="ruleInfo.mappingTransformationActivityDiagram = null" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_qjAhABfnEeCO-aRUjt8-yQ" source="_OQs-cBfmEeCO-aRUjt8-yQ" target="_Ic1s8BfnEeCO-aRUjt8-yQ" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sLe6sBfnEeCO-aRUjt8-yQ" expressionString="ruleInfo.reverseTransformationActivityDiagram = null" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_v9cwcBfnEeCO-aRUjt8-yQ" source="_OQs-cBfmEeCO-aRUjt8-yQ" target="_vUKi4BfnEeCO-aRUjt8-yQ" guardType="ELSE"/>
    <edges uuid="_0RFI0BfnEeCO-aRUjt8-yQ" source="_VCRY4BfmEeCO-aRUjt8-yQ" target="_zq7u0BfnEeCO-aRUjt8-yQ"/>
    <edges uuid="_05JOYBfnEeCO-aRUjt8-yQ" source="_K7mOYBfnEeCO-aRUjt8-yQ" target="_zq7u0BfnEeCO-aRUjt8-yQ"/>
    <edges uuid="_1Q8qYBfnEeCO-aRUjt8-yQ" source="_Ic1s8BfnEeCO-aRUjt8-yQ" target="_zq7u0BfnEeCO-aRUjt8-yQ"/>
    <edges uuid="_1xhA0BfnEeCO-aRUjt8-yQ" source="_zq7u0BfnEeCO-aRUjt8-yQ" target="_V8DpAHrUEd-C3vhV8ZLu-w"/>
    <edges uuid="_5bpwQBfnEeCO-aRUjt8-yQ" source="_FTpvgHrYEd-C3vhV8ZLu-w" target="_N8HOgBfmEeCO-aRUjt8-yQ"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
