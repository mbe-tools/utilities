<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" uuid="_dHp7MPbiEd-JnYtEWuN3-g">
  <activities name="create_storyPattern_create_RHS_axiom" uuid="_emvIUPbiEd-JnYtEWuN3-g">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_hxtPgPbiEd-JnYtEWuN3-g" outgoing="_xesFwPbiEd-JnYtEWuN3-g"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_ib0jgPbiEd-JnYtEWuN3-g" incoming="_xesFwPbiEd-JnYtEWuN3-g" outgoing="_xLf5QBQIEeCDO6mN3ETDMg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_jaEUcPbiEd-JnYtEWuN3-g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_ma2k4PbiEd-JnYtEWuN3-g" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_qAB70PbiEd-JnYtEWuN3-g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_ssOcUPbiEd-JnYtEWuN3-g" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create this and ruleSet SPOs" uuid="_HW2WkBQIEeCDO6mN3ETDMg" incoming="_xLf5QBQIEeCDO6mN3ETDMg" outgoing="_x1Zx4BQIEeCDO6mN3ETDMg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisSpo" uuid="_JdhvMBQIEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_TljvIBQIEeCDO6mN3ETDMg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_T71hIBQIEeCDO6mN3ETDMg">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_rule_spo.story#_Kpsp0AwlEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_V-3v4BQIEeCDO6mN3ETDMg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_YuCr4BQIEeCDO6mN3ETDMg" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="isAxiom" uuid="_dK4qoBQIEeCDO6mN3ETDMg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_eqA7EBQIEeCDO6mN3ETDMg" expressionString="true" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_J-1FcBQIEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_iU__EBQIEeCDO6mN3ETDMg">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_isKh4BQIEeCDO6mN3ETDMg">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="../common/create_ruleSet_spo.story#_ZDP-wAwkEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_k5lbsBQIEeCDO6mN3ETDMg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_m9qzUBQIEeCDO6mN3ETDMg" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="bindingType" uuid="_rgYwkBQIEeCDO6mN3ETDMg">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_s-yBMBQIEeCDO6mN3ETDMg" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_xdrOYBQIEeCDO6mN3ETDMg" incoming="_wysyYBRCEeC9udBuGs2jiA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform created correspondence node" uuid="__gPKMBQIEeCDO6mN3ETDMg" incoming="_x1Zx4BQIEeCDO6mN3ETDMg" outgoing="_kM1N8BQJEeCDO6mN3ETDMg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_E-Oc4BQJEeCDO6mN3ETDMg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_FsQAgBQJEeCDO6mN3ETDMg">
          <activity href="../common/transform_correspondenceNodes.story#_gUE7UBNnEeCRUJdROfc5HA"/>
          <parameters name="tggRule" uuid="_Gp-0EBQJEeCDO6mN3ETDMg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KXVPkBQJEeCDO6mN3ETDMg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_LNhQsBQJEeCDO6mN3ETDMg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NojvwBQJEeCDO6mN3ETDMg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_S21ygBQJEeCDO6mN3ETDMg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VPNYEBQJEeCDO6mN3ETDMg" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_WrH8cBQJEeCDO6mN3ETDMg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_YpNZwBQJEeCDO6mN3ETDMg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_b2o_EBQJEeCDO6mN3ETDMg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dqRf4BQJEeCDO6mN3ETDMg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_etPYwBQJEeCDO6mN3ETDMg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gkEXgBQJEeCDO6mN3ETDMg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="bindingType" uuid="_XJDMcBWnEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_YXmDMBWnEeC7l7ZPwVRGQg" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->rootCorrNode link" uuid="_zO-XcBQJEeCDO6mN3ETDMg" incoming="_kM1N8BQJEeCDO6mN3ETDMg" outgoing="_1roKoBQKEeCDO6mN3ETDMg">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_3KJzkBQJEeCDO6mN3ETDMg">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_31M7sBQJEeCDO6mN3ETDMg">
          <activity href="../common/create_ruleSet_rootCorrNode_link.story#_CHWoUBQHEeCDO6mN3ETDMg"/>
          <parameters name="ruleSetSpo" uuid="_438yIBQJEeCDO6mN3ETDMg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7HAqsBQJEeCDO6mN3ETDMg" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="tggRule" uuid="_8KWXABQJEeCDO6mN3ETDMg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qZV-EBQKEeCDO6mN3ETDMg" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_rdwocBQKEeCDO6mN3ETDMg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tuEeMBQKEeCDO6mN3ETDMg" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_vD_IQBQKEeCDO6mN3ETDMg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xCjtwBQKEeCDO6mN3ETDMg" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create rule->createdCorrNodes link" uuid="_pn3twBQeEeCkcYaoegFQZw" incoming="_1roKoBQKEeCDO6mN3ETDMg" outgoing="_4P5RoBQfEeCkcYaoegFQZw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_hFzwoBQfEeCkcYaoegFQZw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_jgRA4BQfEeCkcYaoegFQZw">
          <activity href="../common/create_rule_createdCorrNodes_link.story#_NdRmcBQBEeCDO6mN3ETDMg"/>
          <parameters name="ruleSpo" uuid="_kvLrEBQfEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nMNuMBQfEeCkcYaoegFQZw" expressionString="thisSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="tggRule" uuid="_oKRR4BQfEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rf2GkBQfEeCkcYaoegFQZw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_skVpcBQfEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_vRNfYBQfEeCkcYaoegFQZw" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_wkynIBQfEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ylmBQBQfEeCkcYaoegFQZw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create corrNode->ruleSet link" uuid="_0T9LEBQgEeCkcYaoegFQZw" incoming="_4P5RoBQfEeCkcYaoegFQZw" outgoing="_8ykWIBQgEeCkcYaoegFQZw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_0T9LERQgEeCkcYaoegFQZw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_0T9LEhQgEeCkcYaoegFQZw">
          <activity href="../common/create_corrNode_ruleSetLink.story#_XbROsBQDEeCDO6mN3ETDMg"/>
          <parameters name="ruleSetSpo" uuid="_0T9LGRQgEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0T9LGhQgEeCkcYaoegFQZw" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="tggRule" uuid="_0T9LFRQgEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0T9LFhQgEeCkcYaoegFQZw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_0T9LFxQgEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0T9LGBQgEeCkcYaoegFQZw" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_0T9LExQgEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0T9LFBQgEeCkcYaoegFQZw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->correspondenceNodes link" uuid="_0YX7mxQhEeCkcYaoegFQZw" incoming="_8ykWIBQgEeCkcYaoegFQZw" outgoing="_6V0xcBQhEeCkcYaoegFQZw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_0YX7nBQhEeCkcYaoegFQZw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_0YX7nRQhEeCkcYaoegFQZw">
          <activity href="../common/create_ruleSet_corrNodesLink.story#_NnQzcBQCEeCDO6mN3ETDMg"/>
          <parameters name="ruleSetSpo" uuid="_0YX7oBQhEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0YX7oRQhEeCkcYaoegFQZw" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="tggRule" uuid="_0YX7pBQhEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0YX7pRQhEeCkcYaoegFQZw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_0YX7ohQhEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0YX7oxQhEeCkcYaoegFQZw" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_0YX7nhQhEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0YX7nxQhEeCkcYaoegFQZw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_HrHXkBQiEeCkcYaoegFQZw" incoming="_6V0xcBQhEeCkcYaoegFQZw" outgoing="_K-HZYBQiEeCkcYaoegFQZw _FpaJEBQjEeCkcYaoegFQZw _-piGsBQ_EeC9udBuGs2jiA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_KbwTMBQiEeCkcYaoegFQZw" incoming="_tiYYgBQ_EeC9udBuGs2jiA _EYjfABRAEeC9udBuGs2jiA _pMfg4BRAEeC9udBuGs2jiA" outgoing="_LS3r8BQjEeCkcYaoegFQZw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform left created nodes" uuid="_MwqVEBQiEeCkcYaoegFQZw" incoming="_FpaJEBQjEeCkcYaoegFQZw" outgoing="_GMV3ABQjEeCkcYaoegFQZw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_S2UrwBQiEeCkcYaoegFQZw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_jT6fkBQiEeCkcYaoegFQZw">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_k73U0BQiEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nc2D0BQiEeCkcYaoegFQZw" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_oNtgEBQiEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_quuEQBQiEeCkcYaoegFQZw" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_rltWoBQiEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ts6TsBQiEeCkcYaoegFQZw" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_uymwcBQiEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_w8bjsBQiEeCkcYaoegFQZw" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_zMuyYBQiEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1DuwQBQiEeCkcYaoegFQZw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_2RPFMBQiEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_4TTDkBQiEeCkcYaoegFQZw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_5KX1gBQiEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7BV-MBQiEeCkcYaoegFQZw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_8HAlwBQiEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_-JwgoBQiEeCkcYaoegFQZw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="__QamsBQiEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_A-xl8BQjEeCkcYaoegFQZw" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_B6m8EBQjEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Dg6ykBQjEeCkcYaoegFQZw" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="bindingType" uuid="_pn8M8BQjEeCkcYaoegFQZw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_rE2P0BQjEeCkcYaoegFQZw" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_iNgEwCVzEeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kBrYICVzEeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform right created nodes" uuid="_H2X7oBQ-EeC9udBuGs2jiA" incoming="_GMV3ABQjEeCkcYaoegFQZw" outgoing="_Z5NO8BQ-EeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_H2X7oRQ-EeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_H2X7ohQ-EeC9udBuGs2jiA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_H2X7oxQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H2X7pBQ-EeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_H2YivBQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H2YivRQ-EeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_H2YiuBQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H2YiuRQ-EeC9udBuGs2jiA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_H2YivhQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H2YivxQ-EeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_H2X7pxQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H2X7qBQ-EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_H2YiuhQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H2YiuxQ-EeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_H2X7pRQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H2X7phQ-EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_H2YithQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H2YitxQ-EeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_H2YitBQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H2YitRQ-EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_H2YishQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H2YisxQ-EeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="bindingType" uuid="_H2YisBQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_H2YisRQ-EeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_mQsNYCVzEeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_oKTaYCVzEeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform right created links" uuid="_jRGLYBQ-EeC9udBuGs2jiA" incoming="_Z5NO8BQ-EeC9udBuGs2jiA" outgoing="_JkueYBQ_EeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_o-RJkBQ-EeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_rHh8IBQ-EeC9udBuGs2jiA">
          <activity href="../common/transform_modelLinks.story#_tdw5IAw6EeCAUcBgGC58lA"/>
          <parameters name="tggRule" uuid="_sHxRIBQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ub8TMBQ-EeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_vvC50BQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_yRSNEBQ-EeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_zY25IBQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1raKkBQ-EeC9udBuGs2jiA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_2jnk8BQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_4yhFcBQ-EeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentLinks" uuid="_7PJf8BQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9PsbYBQ-EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedLinks" uuid="_-YhEoBQ-EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ab2OUBQ_EeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_B6kPEBQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EGrhUBQ_EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_FKH7UBQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Gp2osBQ_EeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->uncoveredModelElementLinks" uuid="_NPrGIBQ_EeC9udBuGs2jiA" incoming="_JkueYBQ_EeC9udBuGs2jiA" outgoing="_tiYYgBQ_EeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_RS65IBQ_EeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_SMSxIBQ_EeC9udBuGs2jiA">
          <activity href="../common/create_ruleSet_uncoveredElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_TGBOcBQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Ye2UUBQ_EeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_ZjjSkBQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bxqI4BQ_EeC9udBuGs2jiA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_c7eQoBQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fV8H8BQ_EeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_gqxM4BQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ikSTQBQ_EeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::DESTROY" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_lbCd8BQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ngThUBQ_EeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_oiKm8BQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qVlsYBQ_EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform left created nodes" uuid="_1Upa8BQ_EeC9udBuGs2jiA" incoming="_-piGsBQ_EeC9udBuGs2jiA" outgoing="_CpulsBRAEeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_1Upa8RQ_EeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_1Upa8hQ_EeC9udBuGs2jiA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_1Upa8xQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1Upa9BQ_EeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_1UpbBRQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UpbBhQ_EeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_1UpbARQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UpbAhQ_EeC9udBuGs2jiA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_1UpbBxQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UpbCBQ_EeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_1Upa9xQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1Upa-BQ_EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_1UpbAxQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UpbBBQ_EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_1Upa9RQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1Upa9hQ_EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_1Upa_xQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UpbABQ_EeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_1Upa_RQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1Upa_hQ_EeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_1Upa-xQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1Upa_BQ_EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="bindingType" uuid="_1Upa-RQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1Upa-hQ_EeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_ribMwCVzEeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tVshMCVzEeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform right created nodes" uuid="_1UpbCRQ_EeC9udBuGs2jiA" incoming="_CpulsBRAEeC9udBuGs2jiA" outgoing="_Ljgp0BRAEeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_1UpbChQ_EeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_1UpbCxQ_EeC9udBuGs2jiA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_1UpbEBQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UpbERQ_EeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_1UqCDRQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqCDhQ_EeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_1UqCBRQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqCBhQ_EeC9udBuGs2jiA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_1UqCCRQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqCChQ_EeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_1UqCAxQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqCBBQ_EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_1UqCARQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqCAhQ_EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_1UpbDBQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UpbDRQ_EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_1UpbDhQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UpbDxQ_EeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_1UqCBxQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqCCBQ_EeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_1UpbEhQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqCABQ_EeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="bindingType" uuid="_1UqCCxQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqCDBQ_EeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_vREowCVzEeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_xN-aUCVzEeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->uncoveredModelElementLinks" uuid="_1UqCIhQ_EeC9udBuGs2jiA" incoming="_Ljgp0BRAEeC9udBuGs2jiA" outgoing="_EYjfABRAEeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_1UqCIxQ_EeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_1UqCJBQ_EeC9udBuGs2jiA">
          <activity href="../common/create_ruleSet_uncoveredElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_1UqpGBQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqpGRQ_EeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_1UqpEhQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqpExQ_EeC9udBuGs2jiA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_1UqpFhQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqpFxQ_EeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_1UqpFBQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqpFRQ_EeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::DESTROY" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_1UqCJRQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqCJhQ_EeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_1UqpEBQ_EeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1UqpERQ_EeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform left created nodes" uuid="_NvojIBRAEeC9udBuGs2jiA" incoming="_K-HZYBQiEeCkcYaoegFQZw" outgoing="_XqMu0BRAEeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_NvojIRRAEeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_NvojIhRAEeC9udBuGs2jiA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_NvojIxRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NvojJBRAEeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_NvojNRRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NvojNhRAEeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_NvojMRRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NvojMhRAEeC9udBuGs2jiA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_NvojNxRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NvojOBRAEeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_NvojJxRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NvojKBRAEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_NvojMxRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NvojNBRAEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_NvojJRRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NvojJhRAEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_NvojLxRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NvojMBRAEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_NvojLRRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NvojLhRAEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_NvojKxRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NvojLBRAEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="bindingType" uuid="_NvojKRRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NvojKhRAEeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_z0l3QCVzEeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1jdM0CVzEeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform right created nodes" uuid="_OznIoBRAEeC9udBuGs2jiA" incoming="_XqMu0BRAEeC9udBuGs2jiA" outgoing="_YEGckBRAEeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_OznIoRRAEeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_OznIohRAEeC9udBuGs2jiA">
          <activity href="../common/transform_modelObjects.story#_-o6DYAwiEeCU3uEhqcP0Dg"/>
          <parameters name="tggRule" uuid="_OznIpxRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OznIqBRAEeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_OznItxRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OznIuBRAEeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_OznIrxRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OznIsBRAEeC9udBuGs2jiA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_OznIsxRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OznItBRAEeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::NONE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_OznIrRRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OznIrhRAEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformAttributeAssignments" uuid="_OznIqxRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OznIrBRAEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformParentNodes" uuid="_OznIoxRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OznIpBRAEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedNodes" uuid="_OznIpRRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OznIphRAEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_OznIsRRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OznIshRAEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_OznIqRRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OznIqhRAEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="bindingType" uuid="_OznItRRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OznIthRAEeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_3sze8CVzEeC_MuN-Znf0pQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5VMZECVzEeC_MuN-Znf0pQ" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform left created links" uuid="_Px7yGxRAEeC9udBuGs2jiA" incoming="_YEGckBRAEeC9udBuGs2jiA" outgoing="_YbVQ0BRAEeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_Px8ZIBRAEeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_Px8ZIRRAEeC9udBuGs2jiA">
          <activity href="../common/transform_modelLinks.story#_tdw5IAw6EeCAUcBgGC58lA"/>
          <parameters name="tggRule" uuid="_Px8ZJBRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Px8ZJRRAEeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_Px8ZKhRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Px8ZKxRAEeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_Px8ZLhRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Px8ZLxRAEeC9udBuGs2jiA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_Px8ZLBRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Px8ZLRRAEeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="transformParentLinks" uuid="_Px8ZIhRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Px8ZIxRAEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformCreatedLinks" uuid="_Px8ZJhRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Px8ZJxRAEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformSource" uuid="_Px8ZMBRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Px8ZMRRAEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformTarget" uuid="_Px8ZKBRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Px8ZKRRAEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->uncoveredModelElementLinks" uuid="_SK-GDxRAEeC9udBuGs2jiA" incoming="_YbVQ0BRAEeC9udBuGs2jiA" outgoing="_pMfg4BRAEeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_SK-tEBRAEeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_SK-tERRAEeC9udBuGs2jiA">
          <activity href="../common/create_ruleSet_uncoveredElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_SK-tHBRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SK-tHRRAEeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_SK-tFhRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SK-tFxRAEeC9udBuGs2jiA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_SK-tGhRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SK-tGxRAEeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="modifier" uuid="_SK-tGBRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SK-tGRRAEeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::DESTROY" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="_SK-tEhRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SK-tExRAEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_SK-tFBRAEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_SK-tFRRAEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create corrNode->sources and targets links" uuid="_8TSyoBRAEeC9udBuGs2jiA" incoming="_LS3r8BQjEeCkcYaoegFQZw" outgoing="_fm5qMBRBEeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_91ygIBRAEeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_CMwgYBRBEeC9udBuGs2jiA">
          <activity href="../common/create_corrNode_sourcesTargets_links.story#_uYYuIBNoEeCRUJdROfc5HA"/>
          <parameters name="tggRule" uuid="_DIbecBRBEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_FXUX4BRBEeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_GXzkgBRBEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JNbt0BRBEeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="storyActionNode" uuid="_KL9LkBRBEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_M6pmcBRBEeC9udBuGs2jiA" expressionString="storyActionNode" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
          </parameters>
          <parameters name="modifier" uuid="_N7TyMBRBEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_P4IEMBRBEeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourcesLinks" uuid="_R9dZABRBEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Twp08BRBEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetsLinks" uuid="_UqlGkBRBEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WUyloBRBEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createParentLinks" uuid="_XeG-IBRBEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZmHzgBRBEeC9udBuGs2jiA" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedLinks" uuid="_avsqsBRBEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dGHvgBRBEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->modelElements links" uuid="_m0iVkBRBEeC9udBuGs2jiA" incoming="_fm5qMBRBEeC9udBuGs2jiA" outgoing="_KfbrwBRCEeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_xqHYABRBEeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_0KUq8BRBEeC9udBuGs2jiA">
          <activity href="../common/create_ruleSet_modelElements_links.story#_E_gekA0fEeCQ056ROwfPtw"/>
          <parameters name="tggRule" uuid="_1B0ToBRBEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3BDngBRBEeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_4GlFIBRBEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6Xgl8BRBEeC9udBuGs2jiA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="modifier" uuid="_7cbmoBRBEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9Y0a0BRBEeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceLinks" uuid="__aYp8BRBEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Bfy3QBRCEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetLinks" uuid="_CL4hMBRCEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_D0mygBRCEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_Ezrp4BRCEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_HQm_UBRCEeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="createParentLinks" uuid="_Yv8tEBWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_af-gQBWMEeC7l7ZPwVRGQg" expressionString="false" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createCreatedLinks" uuid="_Y01NEBWMEeC7l7ZPwVRGQg">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_cfEqMBWMEeC7l7ZPwVRGQg" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create ruleSet->modelRoot links" uuid="_SmTsQBRCEeC9udBuGs2jiA" incoming="_KfbrwBRCEeC9udBuGs2jiA" outgoing="_wysyYBRCEeC9udBuGs2jiA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_WiQkcBRCEeC9udBuGs2jiA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_Y9Wt4BRCEeC9udBuGs2jiA">
          <activity href="../common/create_ruleSet_modelRoot_links.story#_FIjsAA4TEeCVYKbk8vub6g"/>
          <parameters name="tggRule" uuid="_Z0LBIBRCEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_b73tcBRCEeC9udBuGs2jiA" expressionString="tggRule" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
          </parameters>
          <parameters name="traceabilityLinkStore" uuid="_dfhRgBRCEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_f4GScBRCEeC9udBuGs2jiA" expressionString="traceabilityLinkStore" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_hCvgoBRCEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jIbawBRCEeC9udBuGs2jiA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="modifier" uuid="_kJ4QsBRCEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mUd48BRCEeC9udBuGs2jiA" expressionString="storyDiagramEcore::sdm::StoryPatternModifierEnumeration::CREATE" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
          </parameters>
          <parameters name="createSourceModelRootLink" uuid="_pC7DQBRCEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_sD0BYBRCEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="createTargetModelRootLink" uuid="_tTGfABRCEeC9udBuGs2jiA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_vBYlwBRCEeC9udBuGs2jiA" expressionString="true" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <edges uuid="_xesFwPbiEd-JnYtEWuN3-g" source="_hxtPgPbiEd-JnYtEWuN3-g" target="_ib0jgPbiEd-JnYtEWuN3-g"/>
    <edges uuid="_xLf5QBQIEeCDO6mN3ETDMg" source="_ib0jgPbiEd-JnYtEWuN3-g" target="_HW2WkBQIEeCDO6mN3ETDMg"/>
    <edges uuid="_x1Zx4BQIEeCDO6mN3ETDMg" source="_HW2WkBQIEeCDO6mN3ETDMg" target="__gPKMBQIEeCDO6mN3ETDMg"/>
    <edges uuid="_kM1N8BQJEeCDO6mN3ETDMg" source="__gPKMBQIEeCDO6mN3ETDMg" target="_zO-XcBQJEeCDO6mN3ETDMg"/>
    <edges uuid="_1roKoBQKEeCDO6mN3ETDMg" source="_zO-XcBQJEeCDO6mN3ETDMg" target="_pn3twBQeEeCkcYaoegFQZw"/>
    <edges uuid="_4P5RoBQfEeCkcYaoegFQZw" source="_pn3twBQeEeCkcYaoegFQZw" target="_0T9LEBQgEeCkcYaoegFQZw"/>
    <edges uuid="_8ykWIBQgEeCkcYaoegFQZw" source="_0T9LEBQgEeCkcYaoegFQZw" target="_0YX7mxQhEeCkcYaoegFQZw"/>
    <edges uuid="_6V0xcBQhEeCkcYaoegFQZw" source="_0YX7mxQhEeCkcYaoegFQZw" target="_HrHXkBQiEeCkcYaoegFQZw"/>
    <edges uuid="_K-HZYBQiEeCkcYaoegFQZw" source="_HrHXkBQiEeCkcYaoegFQZw" target="_NvojIBRAEeC9udBuGs2jiA" guardType="ELSE"/>
    <edges uuid="_FpaJEBQjEeCkcYaoegFQZw" source="_HrHXkBQiEeCkcYaoegFQZw" target="_MwqVEBQiEeCkcYaoegFQZw" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_HqeB4BQjEeCkcYaoegFQZw" expressionString="direction = mote::TransformationDirection::FORWARD" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_GMV3ABQjEeCkcYaoegFQZw" source="_MwqVEBQiEeCkcYaoegFQZw" target="_H2X7oBQ-EeC9udBuGs2jiA"/>
    <edges uuid="_LS3r8BQjEeCkcYaoegFQZw" source="_KbwTMBQiEeCkcYaoegFQZw" target="_8TSyoBRAEeC9udBuGs2jiA"/>
    <edges uuid="_Z5NO8BQ-EeC9udBuGs2jiA" source="_H2X7oBQ-EeC9udBuGs2jiA" target="_jRGLYBQ-EeC9udBuGs2jiA"/>
    <edges uuid="_JkueYBQ_EeC9udBuGs2jiA" source="_jRGLYBQ-EeC9udBuGs2jiA" target="_NPrGIBQ_EeC9udBuGs2jiA"/>
    <edges uuid="_tiYYgBQ_EeC9udBuGs2jiA" source="_NPrGIBQ_EeC9udBuGs2jiA" target="_KbwTMBQiEeCkcYaoegFQZw"/>
    <edges uuid="_-piGsBQ_EeC9udBuGs2jiA" source="_HrHXkBQiEeCkcYaoegFQZw" target="_1Upa8BQ_EeC9udBuGs2jiA" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__tooABQ_EeC9udBuGs2jiA" expressionString="direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_CpulsBRAEeC9udBuGs2jiA" source="_1Upa8BQ_EeC9udBuGs2jiA" target="_1UpbCRQ_EeC9udBuGs2jiA"/>
    <edges uuid="_EYjfABRAEeC9udBuGs2jiA" source="_1UqCIhQ_EeC9udBuGs2jiA" target="_KbwTMBQiEeCkcYaoegFQZw"/>
    <edges uuid="_Ljgp0BRAEeC9udBuGs2jiA" source="_1UpbCRQ_EeC9udBuGs2jiA" target="_1UqCIhQ_EeC9udBuGs2jiA"/>
    <edges uuid="_XqMu0BRAEeC9udBuGs2jiA" source="_NvojIBRAEeC9udBuGs2jiA" target="_OznIoBRAEeC9udBuGs2jiA"/>
    <edges uuid="_YEGckBRAEeC9udBuGs2jiA" source="_OznIoBRAEeC9udBuGs2jiA" target="_Px7yGxRAEeC9udBuGs2jiA"/>
    <edges uuid="_YbVQ0BRAEeC9udBuGs2jiA" source="_Px7yGxRAEeC9udBuGs2jiA" target="_SK-GDxRAEeC9udBuGs2jiA"/>
    <edges uuid="_pMfg4BRAEeC9udBuGs2jiA" source="_SK-GDxRAEeC9udBuGs2jiA" target="_KbwTMBQiEeCkcYaoegFQZw"/>
    <edges uuid="_fm5qMBRBEeC9udBuGs2jiA" source="_8TSyoBRAEeC9udBuGs2jiA" target="_m0iVkBRBEeC9udBuGs2jiA"/>
    <edges uuid="_KfbrwBRCEeC9udBuGs2jiA" source="_m0iVkBRBEeC9udBuGs2jiA" target="_SmTsQBRCEeC9udBuGs2jiA"/>
    <edges uuid="_wysyYBRCEeC9udBuGs2jiA" source="_SmTsQBRCEeC9udBuGs2jiA" target="_xdrOYBQIEeCDO6mN3ETDMg"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
