<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="transform_constraints" uuid="_1BU4kAw3EeCzq8Eugced9g">
  <activities name="transform_constraints" uuid="_3AxAwAw3EeCzq8Eugced9g">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_5V8IYAw3EeCzq8Eugced9g" outgoing="_4Ln04Aw4EeCAUcBgGC58lA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_58Jz0Aw3EeCzq8Eugced9g" incoming="_4Ln04Aw4EeCAUcBgGC58lA" outgoing="_o18rYAw4EeCAUcBgGC58lA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_9aQ5cAw3EeCzq8Eugced9g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyPatternObject" uuid="_-L7m8Aw3EeCzq8Eugced9g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformAttributeConstraints" uuid="_ThTcQCVWEeCVE9iKiEiTJw" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformConstraints" uuid="_aw8PAC6nEeCRIK6l8KTurA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="transform constraints" uuid="_FAiHQAw4EeCzq8Eugced9g" incoming="_woT0cAw4EeCAUcBgGC58lA _iBHRAC6nEeCRIK6l8KTurA" outgoing="_nwll0Aw4EeCAUcBgGC58lA _v5Xq0Aw4EeCAUcBgGC58lA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_GeONQAw4EeCzq8Eugced9g" outgoingStoryLinks="_LZ75UAw4EeCzq8Eugced9g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="constraint" uuid="_JbZJAAw4EeCzq8Eugced9g" incomingStoryLinks="_LZ75UAw4EeCzq8Eugced9g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LZ75UAw4EeCzq8Eugced9g" source="_GeONQAw4EeCzq8Eugced9g" target="_JbZJAAw4EeCzq8Eugced9g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelObject/constraintExpressions"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="clone constraint" uuid="_OZgo0Aw4EeCzq8Eugced9g" incoming="_nwll0Aw4EeCAUcBgGC58lA" outgoing="_woT0cAw4EeCAUcBgGC58lA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyPatternObject" uuid="_RZAf0Aw4EeCzq8Eugced9g" outgoingStoryLinks="_lYa0kAw4EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="clonedConstraint" uuid="_THvSgAw4EeCzq8Eugced9g" incomingStoryLinks="_lYa0kAw4EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_Vhms4Aw4EeCzq8Eugced9g">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_WGEE0Aw4EeCzq8Eugced9g" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters name="" uuid="_iL3ZAAw4EeCAUcBgGC58lA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_jhd6AAw4EeCAUcBgGC58lA" expressionString="constraint" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lYa0kAw4EeCAUcBgGC58lA" modifier="CREATE" source="_RZAf0Aw4EeCzq8Eugced9g" target="_THvSgAw4EeCzq8Eugced9g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/constraints"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create constraints for attribute assignments" uuid="_tZrVQAw4EeCAUcBgGC58lA" incoming="_uRHeIAw5EeCAUcBgGC58lA _aC2dsCVWEeCVE9iKiEiTJw" outgoing="_D29aoAw5EeCAUcBgGC58lA _w5J3YAw5EeCAUcBgGC58lA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_xsQkwAw4EeCAUcBgGC58lA" outgoingStoryLinks="_13jggAw4EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="attributeAssignment" uuid="_z69Q8Aw4EeCAUcBgGC58lA" outgoingStoryLinks="_C2RZsAw5EeCAUcBgGC58lA" incomingStoryLinks="_13jggAw4EeCAUcBgGC58lA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AttributeAssignment"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="assignmentExpression" uuid="_A7GGcAw5EeCAUcBgGC58lA" incomingStoryLinks="_C2RZsAw5EeCAUcBgGC58lA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_13jggAw4EeCAUcBgGC58lA" source="_xsQkwAw4EeCAUcBgGC58lA" target="_z69Q8Aw4EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelObject/attributeAssignments"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_C2RZsAw5EeCAUcBgGC58lA" source="_z69Q8Aw4EeCAUcBgGC58lA" target="_A7GGcAw5EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AttributeAssignment/assignmentExpression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create operator expression" uuid="_5APvwAw4EeCAUcBgGC58lA" incoming="_D29aoAw5EeCAUcBgGC58lA" outgoing="_uRHeIAw5EeCAUcBgGC58lA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyPatternObject" uuid="_FObj4Aw5EeCAUcBgGC58lA" outgoingStoryLinks="_J4DaYAw5EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="callActionExpression" uuid="_IAOWEAw5EeCAUcBgGC58lA" modifier="CREATE" outgoingStoryLinks="_N6PdcAw5EeCAUcBgGC58lA" incomingStoryLinks="_J4DaYAw5EeCAUcBgGC58lA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="operationAction" uuid="_MPKbsAw5EeCAUcBgGC58lA" modifier="CREATE" outgoingStoryLinks="_hKN5UAw5EeCAUcBgGC58lA _sZOIYAw5EeCAUcBgGC58lA" incomingStoryLinks="_N6PdcAw5EeCAUcBgGC58lA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/OperationAction"/>
        <attributeAssignments uuid="_PkupUAw5EeCAUcBgGC58lA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/OperationAction/operator"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_QqjB4Aw5EeCAUcBgGC58lA" expressionString="storyDiagramEcore::callActions::Operators::EQUALS" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_UJktgAw5EeCAUcBgGC58lA">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VJDNgAw5EeCAUcBgGC58lA" expressionString="ecore::EBoolean" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="attributeReference" uuid="_ZVKooAw5EeCAUcBgGC58lA" modifier="CREATE" incomingStoryLinks="_hKN5UAw5EeCAUcBgGC58lA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression"/>
        <attributeAssignments uuid="_cJpicAw5EeCAUcBgGC58lA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionLanguage"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_c-_PAAw5EeCAUcBgGC58lA" expressionString="'OCL'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_d9gswAw5EeCAUcBgGC58lA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/StringExpression/expressionString"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_gEAfMAw5EeCAUcBgGC58lA" expressionString="'self.'.concat(attributeAssignment.eStructuralFeature.name)" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="clonedAssignmentExpression" uuid="_i15YAAw5EeCAUcBgGC58lA" incomingStoryLinks="_sZOIYAw5EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_ltMVQAw5EeCAUcBgGC58lA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_mTpRQAw5EeCAUcBgGC58lA" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters name="" uuid="_pRp6gAw5EeCAUcBgGC58lA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_q_y3UAw5EeCAUcBgGC58lA" expressionString="assignmentExpression" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_J4DaYAw5EeCAUcBgGC58lA" modifier="CREATE" source="_FObj4Aw5EeCAUcBgGC58lA" target="_IAOWEAw5EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/constraints"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_N6PdcAw5EeCAUcBgGC58lA" modifier="CREATE" source="_IAOWEAw5EeCAUcBgGC58lA" target="_MPKbsAw5EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hKN5UAw5EeCAUcBgGC58lA" modifier="CREATE" source="_MPKbsAw5EeCAUcBgGC58lA" target="_ZVKooAw5EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/OperationAction/operand1"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_sZOIYAw5EeCAUcBgGC58lA" modifier="CREATE" source="_MPKbsAw5EeCAUcBgGC58lA" target="_i15YAAw5EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/OperationAction/operand2"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_wWpnQAw5EeCAUcBgGC58lA" incoming="_NfWpQCoqEeCoaqx-xISKDg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_YsE4ACVWEeCVE9iKiEiTJw" incoming="_j3mRgC6nEeCRIK6l8KTurA" outgoing="_aC2dsCVWEeCVE9iKiEiTJw _bZzCgCVWEeCVE9iKiEiTJw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set new UUIDs" uuid="_58g7wCopEeCoaqx-xISKDg" incoming="_QneQwCoqEeCoaqx-xISKDg" outgoing="_NfWpQCoqEeCoaqx-xISKDg" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyPatternObject" uuid="__Q0G8CopEeCoaqx-xISKDg" outgoingStoryLinks="_ElTfYCoqEeCoaqx-xISKDg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="element" uuid="_CKiGECoqEeCoaqx-xISKDg" incomingStoryLinks="_ElTfYCoqEeCoaqx-xISKDg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement"/>
        <attributeAssignments uuid="_GRtW0CoqEeCoaqx-xISKDg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/uuid"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_Hw88ACoqEeCoaqx-xISKDg">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_ITQX0CoqEeCoaqx-xISKDg" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="generateUUID">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_ElTfYCoqEeCoaqx-xISKDg" source="__Q0G8CopEeCoaqx-xISKDg" target="_CKiGECoqEeCoaqx-xISKDg"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_OyH4sCoqEeCoaqx-xISKDg" incoming="_w5J3YAw5EeCAUcBgGC58lA _bZzCgCVWEeCVE9iKiEiTJw" outgoing="_QneQwCoqEeCoaqx-xISKDg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_gxifgC6nEeCRIK6l8KTurA" incoming="_o18rYAw4EeCAUcBgGC58lA" outgoing="_iBHRAC6nEeCRIK6l8KTurA _ka_5gC6nEeCRIK6l8KTurA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_i8AL8C6nEeCRIK6l8KTurA" incoming="_v5Xq0Aw4EeCAUcBgGC58lA _ka_5gC6nEeCRIK6l8KTurA" outgoing="_j3mRgC6nEeCRIK6l8KTurA"/>
    <edges uuid="_nwll0Aw4EeCAUcBgGC58lA" source="_FAiHQAw4EeCzq8Eugced9g" target="_OZgo0Aw4EeCzq8Eugced9g" guardType="FOR_EACH"/>
    <edges uuid="_o18rYAw4EeCAUcBgGC58lA" source="_58Jz0Aw3EeCzq8Eugced9g" target="_gxifgC6nEeCRIK6l8KTurA"/>
    <edges uuid="_v5Xq0Aw4EeCAUcBgGC58lA" source="_FAiHQAw4EeCzq8Eugced9g" target="_i8AL8C6nEeCRIK6l8KTurA" guardType="END"/>
    <edges uuid="_woT0cAw4EeCAUcBgGC58lA" source="_OZgo0Aw4EeCzq8Eugced9g" target="_FAiHQAw4EeCzq8Eugced9g"/>
    <edges uuid="_4Ln04Aw4EeCAUcBgGC58lA" source="_5V8IYAw3EeCzq8Eugced9g" target="_58Jz0Aw3EeCzq8Eugced9g"/>
    <edges uuid="_D29aoAw5EeCAUcBgGC58lA" source="_tZrVQAw4EeCAUcBgGC58lA" target="_5APvwAw4EeCAUcBgGC58lA" guardType="FOR_EACH"/>
    <edges uuid="_uRHeIAw5EeCAUcBgGC58lA" source="_5APvwAw4EeCAUcBgGC58lA" target="_tZrVQAw4EeCAUcBgGC58lA"/>
    <edges uuid="_w5J3YAw5EeCAUcBgGC58lA" source="_tZrVQAw4EeCAUcBgGC58lA" target="_OyH4sCoqEeCoaqx-xISKDg" guardType="END"/>
    <edges uuid="_aC2dsCVWEeCVE9iKiEiTJw" source="_YsE4ACVWEeCVE9iKiEiTJw" target="_tZrVQAw4EeCAUcBgGC58lA" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_c8-sgCVWEeCVE9iKiEiTJw" expressionString="transformAttributeConstraints" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_bZzCgCVWEeCVE9iKiEiTJw" source="_YsE4ACVWEeCVE9iKiEiTJw" target="_OyH4sCoqEeCoaqx-xISKDg" guardType="ELSE"/>
    <edges uuid="_NfWpQCoqEeCoaqx-xISKDg" source="_58g7wCopEeCoaqx-xISKDg" target="_wWpnQAw5EeCAUcBgGC58lA" guardType="END"/>
    <edges uuid="_QneQwCoqEeCoaqx-xISKDg" source="_OyH4sCoqEeCoaqx-xISKDg" target="_58g7wCopEeCoaqx-xISKDg"/>
    <edges uuid="_iBHRAC6nEeCRIK6l8KTurA" source="_gxifgC6nEeCRIK6l8KTurA" target="_FAiHQAw4EeCzq8Eugced9g" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mSSLQC6nEeCRIK6l8KTurA" expressionString="transformConstraints" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_j3mRgC6nEeCRIK6l8KTurA" source="_i8AL8C6nEeCRIK6l8KTurA" target="_YsE4ACVWEeCVE9iKiEiTJw"/>
    <edges uuid="_ka_5gC6nEeCRIK6l8KTurA" source="_gxifgC6nEeCRIK6l8KTurA" target="_i8AL8C6nEeCRIK6l8KTurA" guardType="ELSE"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
