<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_match_spo" uuid="_41U_8Bi_EeCuQ-PF2tTDdw">
  <activities name="create_match_spo" uuid="_6d114Bi_EeCuQ-PF2tTDdw">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_8gVSEBi_EeCuQ-PF2tTDdw" outgoing="_jdV5QBjAEeCuQ-PF2tTDdw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_9EPQYBi_EeCuQ-PF2tTDdw" incoming="_jdV5QBjAEeCuQ-PF2tTDdw" outgoing="_j0J2wBjAEeCuQ-PF2tTDdw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_9kjIIBi_EeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_-SDHUBi_EeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="bindingType" uuid="_AWRB0BjAEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create match spo" uuid="_JHiDIBjAEeCuQ-PF2tTDdw" incoming="_j0J2wBjAEeCuQ-PF2tTDdw" outgoing="_kTDZQBjAEeCuQ-PF2tTDdw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_M3PPEBjAEeCuQ-PF2tTDdw" outgoingStoryLinks="_aA5M8BjAEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_Nq_AMBjAEeCuQ-PF2tTDdw" modifier="CREATE" outgoingStoryLinks="_Yr1egBjAEeCuQ-PF2tTDdw _Y_vR8BjAEeCuQ-PF2tTDdw" incomingStoryLinks="_aA5M8BjAEeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_arJD0BjAEeCuQ-PF2tTDdw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bsjdgBjAEeCuQ-PF2tTDdw" expressionString="'match'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_dcNdQBjAEeCuQ-PF2tTDdw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_e8BqMBjAEeCuQ-PF2tTDdw" expressionString="mote::rules::Match" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_O8N-kBjAEeCuQ-PF2tTDdw" incomingStoryLinks="_Yr1egBjAEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="bindingType" uuid="_PZ_fsBjAEeCuQ-PF2tTDdw" incomingStoryLinks="_Y_vR8BjAEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_QrjOMBjAEeCuQ-PF2tTDdw" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Yr1egBjAEeCuQ-PF2tTDdw" modifier="CREATE" source="_Nq_AMBjAEeCuQ-PF2tTDdw" target="_O8N-kBjAEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Y_vR8BjAEeCuQ-PF2tTDdw" modifier="CREATE" source="_Nq_AMBjAEeCuQ-PF2tTDdw" target="_PZ_fsBjAEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_aA5M8BjAEeCuQ-PF2tTDdw" modifier="CREATE" source="_M3PPEBjAEeCuQ-PF2tTDdw" target="_Nq_AMBjAEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_iqECMBjAEeCuQ-PF2tTDdw" incoming="_kTDZQBjAEeCuQ-PF2tTDdw">
      <returnValue xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lPS_EBjAEeCuQ-PF2tTDdw" expressionString="spo" expressionLanguage="OCL"/>
    </nodes>
    <edges uuid="_jdV5QBjAEeCuQ-PF2tTDdw" source="_8gVSEBi_EeCuQ-PF2tTDdw" target="_9EPQYBi_EeCuQ-PF2tTDdw"/>
    <edges uuid="_j0J2wBjAEeCuQ-PF2tTDdw" source="_9EPQYBi_EeCuQ-PF2tTDdw" target="_JHiDIBjAEeCuQ-PF2tTDdw"/>
    <edges uuid="_kTDZQBjAEeCuQ-PF2tTDdw" source="_JHiDIBjAEeCuQ-PF2tTDdw" target="_iqECMBjAEeCuQ-PF2tTDdw"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
