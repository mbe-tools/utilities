<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_rule_ruleSet_link" uuid="_oSyZIAwmEeCU3uEhqcP0Dg">
  <activities name="create_rule_ruleSet_link" uuid="_qOEaEAwmEeCU3uEhqcP0Dg">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_sYm_AAwmEeCU3uEhqcP0Dg" outgoing="__ClK0AwmEeCU3uEhqcP0Dg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_s7ykkAwmEeCU3uEhqcP0Dg" incoming="__ClK0AwmEeCU3uEhqcP0Dg" outgoing="__5I_YAwmEeCU3uEhqcP0Dg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSpo" uuid="_tlELEAwmEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_uaQGoAwmEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create ruleSet link" uuid="_3kJDUAwmEeCU3uEhqcP0Dg" incoming="__5I_YAwmEeCU3uEhqcP0Dg" outgoing="_YqagoAwnEeCU3uEhqcP0Dg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSpo" uuid="_41tY4AwmEeCU3uEhqcP0Dg" outgoingStoryLinks="_LAP2sAwnEeCU3uEhqcP0Dg" incomingStoryLinks="_Dcw3IAwnEeCU3uEhqcP0Dg _GybLYAwnEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_50II8AwmEeCU3uEhqcP0Dg" incomingStoryLinks="_D0zjsAwnEeCU3uEhqcP0Dg _H0WicAwnEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_6ZccgAwmEeCU3uEhqcP0Dg" outgoingStoryLinks="_Dcw3IAwnEeCU3uEhqcP0Dg _D0zjsAwnEeCU3uEhqcP0Dg _JR--EAwnEeCU3uEhqcP0Dg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetEReference" uuid="_7ISfgAwmEeCU3uEhqcP0Dg" incomingStoryLinks="_LaQ5MAwnEeCU3uEhqcP0Dg _OXp3YAwnEeCU3uEhqcP0Dg">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_PrNJ8AwnEeCU3uEhqcP0Dg" expressionString="self.name = 'ruleSet'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleEClass" uuid="_83ZFoAwmEeCU3uEhqcP0Dg" outgoingStoryLinks="_LaQ5MAwnEeCU3uEhqcP0Dg" incomingStoryLinks="_LAP2sAwnEeCU3uEhqcP0Dg">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpl" uuid="_ErDPMAwnEeCU3uEhqcP0Dg" modifier="CREATE" outgoingStoryLinks="_GybLYAwnEeCU3uEhqcP0Dg _H0WicAwnEeCU3uEhqcP0Dg _OXp3YAwnEeCU3uEhqcP0Dg" incomingStoryLinks="_JR--EAwnEeCU3uEhqcP0Dg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Dcw3IAwnEeCU3uEhqcP0Dg" source="_6ZccgAwmEeCU3uEhqcP0Dg" target="_41tY4AwmEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_D0zjsAwnEeCU3uEhqcP0Dg" source="_6ZccgAwmEeCU3uEhqcP0Dg" target="_50II8AwmEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GybLYAwnEeCU3uEhqcP0Dg" modifier="CREATE" source="_ErDPMAwnEeCU3uEhqcP0Dg" target="_41tY4AwmEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_H0WicAwnEeCU3uEhqcP0Dg" modifier="CREATE" source="_ErDPMAwnEeCU3uEhqcP0Dg" target="_50II8AwmEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_JR--EAwnEeCU3uEhqcP0Dg" modifier="CREATE" source="_6ZccgAwmEeCU3uEhqcP0Dg" target="_ErDPMAwnEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LAP2sAwnEeCU3uEhqcP0Dg" source="_41tY4AwmEeCU3uEhqcP0Dg" target="_83ZFoAwmEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LaQ5MAwnEeCU3uEhqcP0Dg" source="_83ZFoAwmEeCU3uEhqcP0Dg" target="_7ISfgAwmEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eAllReferences"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_OXp3YAwnEeCU3uEhqcP0Dg" modifier="CREATE" source="_ErDPMAwnEeCU3uEhqcP0Dg" target="_7ISfgAwmEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_XgnnAAwnEeCU3uEhqcP0Dg" incoming="_YqagoAwnEeCU3uEhqcP0Dg"/>
    <edges uuid="__ClK0AwmEeCU3uEhqcP0Dg" source="_sYm_AAwmEeCU3uEhqcP0Dg" target="_s7ykkAwmEeCU3uEhqcP0Dg"/>
    <edges uuid="__5I_YAwmEeCU3uEhqcP0Dg" source="_s7ykkAwmEeCU3uEhqcP0Dg" target="_3kJDUAwmEeCU3uEhqcP0Dg"/>
    <edges uuid="_YqagoAwnEeCU3uEhqcP0Dg" source="_3kJDUAwmEeCU3uEhqcP0Dg" target="_XgnnAAwnEeCU3uEhqcP0Dg"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
