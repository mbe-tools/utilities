<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_ruleSet_modelRoot_links" uuid="_D5TDkA4TEeCVYKbk8vub6g">
  <activities name="create_ruleSet_modelRoot_links" uuid="_FIjsAA4TEeCVYKbk8vub6g">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_HT47IA4TEeCVYKbk8vub6g" outgoing="_X4_FIA4TEeCVYKbk8vub6g"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_H0g78A4TEeCVYKbk8vub6g" incoming="_X4_FIA4TEeCVYKbk8vub6g" outgoing="_cDL0sA4TEeCVYKbk8vub6g">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_Iz58YA4TEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_J16zAA4TEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_LhHwkA4TEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createSourceModelRootLink" uuid="_MLgxYA4TEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createTargetModelRootLink" uuid="_NYUiwA4TEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_OPmwEA4TEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetEClass" uuid="_SoIaEA4UEeCVYKbk8vub6g" outgoingStoryLinks="_jK7eAA4UEeCVYKbk8vub6g _jl7X8A4UEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_YaFxoA4UEeCVYKbk8vub6g" expressionString="mote::rules::TGGRuleSet" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sourceRootEReference" uuid="_UUwT8A4UEeCVYKbk8vub6g" incomingStoryLinks="_jK7eAA4UEeCVYKbk8vub6g">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lF7yIA4UEeCVYKbk8vub6g" expressionString="self.name = 'sourceModelRootNode'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="targetRootEReference" uuid="_VWsSEA4UEeCVYKbk8vub6g" incomingStoryLinks="_jl7X8A4UEeCVYKbk8vub6g">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_pb33oA4UEeCVYKbk8vub6g" expressionString="self.name = 'targetModelRootNode'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_ernaEA4UEeCVYKbk8vub6g" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jK7eAA4UEeCVYKbk8vub6g" source="_SoIaEA4UEeCVYKbk8vub6g" target="_UUwT8A4UEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jl7X8A4UEeCVYKbk8vub6g" source="_SoIaEA4UEeCVYKbk8vub6g" target="_VWsSEA4UEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="iterate through model objects" uuid="_YcEkEA4TEeCVYKbk8vub6g" incoming="_cDL0sA4TEeCVYKbk8vub6g _5k5JoA4UEeCVYKbk8vub6g" outgoing="_LiMSIA4UEeCVYKbk8vub6g _7ljZ0A4UEeCVYKbk8vub6g" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_ccEOwA4TEeCVYKbk8vub6g" outgoingStoryLinks="_rQwLEA4TEeCVYKbk8vub6g _HFKGIA4UEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_eAt4YA4TEeCVYKbk8vub6g" expressionString="self.isAxiom" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelDomain" uuid="_gfGmYA4TEeCVYKbk8vub6g" outgoingStoryLinks="_slFgwA4TEeCVYKbk8vub6g" incomingStoryLinks="_rQwLEA4TEeCVYKbk8vub6g">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_haZxAA4TEeCVYKbk8vub6g" incomingStoryLinks="_slFgwA4TEeCVYKbk8vub6g _HFKGIA4UEeCVYKbk8vub6g _F-N7gClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_jkwIsA4TEeCVYKbk8vub6g" outgoingStoryLinks="_F-N7gClYEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_lDXbwA4TEeCVYKbk8vub6g" outgoingStoryLinks="_yVeyEA4TEeCVYKbk8vub6g _1aATAA4TEeCVYKbk8vub6g _23I_YA4TEeCVYKbk8vub6g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_lsVgQA4TEeCVYKbk8vub6g" incomingStoryLinks="_yVeyEA4TEeCVYKbk8vub6g _8IoHIA4TEeCVYKbk8vub6g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_mGaNIA4TEeCVYKbk8vub6g" incomingStoryLinks="_23I_YA4TEeCVYKbk8vub6g _7HbI0A4TEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelRootSpl" uuid="_mtFyoA4TEeCVYKbk8vub6g" modifier="CREATE" outgoingStoryLinks="_6cEesA4TEeCVYKbk8vub6g _7HbI0A4TEeCVYKbk8vub6g _8IoHIA4TEeCVYKbk8vub6g" incomingStoryLinks="_1aATAA4TEeCVYKbk8vub6g">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_pifScA4TEeCVYKbk8vub6g" incomingStoryLinks="_6cEesA4TEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_rQwLEA4TEeCVYKbk8vub6g" source="_ccEOwA4TEeCVYKbk8vub6g" target="_gfGmYA4TEeCVYKbk8vub6g"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_slFgwA4TEeCVYKbk8vub6g" source="_gfGmYA4TEeCVYKbk8vub6g" target="_haZxAA4TEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_yVeyEA4TEeCVYKbk8vub6g" source="_lDXbwA4TEeCVYKbk8vub6g" target="_lsVgQA4TEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1aATAA4TEeCVYKbk8vub6g" modifier="CREATE" source="_lDXbwA4TEeCVYKbk8vub6g" target="_mtFyoA4TEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_23I_YA4TEeCVYKbk8vub6g" source="_lDXbwA4TEeCVYKbk8vub6g" target="_mGaNIA4TEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_6cEesA4TEeCVYKbk8vub6g" modifier="CREATE" source="_mtFyoA4TEeCVYKbk8vub6g" target="_pifScA4TEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7HbI0A4TEeCVYKbk8vub6g" modifier="CREATE" source="_mtFyoA4TEeCVYKbk8vub6g" target="_mGaNIA4TEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8IoHIA4TEeCVYKbk8vub6g" modifier="CREATE" source="_mtFyoA4TEeCVYKbk8vub6g" target="_lsVgQA4TEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_HFKGIA4UEeCVYKbk8vub6g" source="_ccEOwA4TEeCVYKbk8vub6g" target="_haZxAA4TEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/inputElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_F-N7gClYEeCFgb_w6FqGaA" source="_jkwIsA4TEeCVYKbk8vub6g" target="_haZxAA4TEeCVYKbk8vub6g" valueTarget="_lsVgQA4TEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
      <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__qymUA4TEeCVYKbk8vub6g" expressionString="createSourceModelRootLink and modelDomain.oclIsKindOf(tgg::SourceModelDomain) or&#xA;createTargetModelRootLink and modelDomain.oclIsKindOf(tgg::TargetModelDomain)" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="handle source" uuid="_IFmPcA4UEeCVYKbk8vub6g" incoming="_MF9GgA4UEeCVYKbk8vub6g" outgoing="_4MArIA4UEeCVYKbk8vub6g">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelRootSpl" uuid="_qlGwkA4UEeCVYKbk8vub6g" outgoingStoryLinks="_zfQoIA4UEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sourceRootEReference" uuid="_rLxvAA4UEeCVYKbk8vub6g" incomingStoryLinks="_zfQoIA4UEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zfQoIA4UEeCVYKbk8vub6g" modifier="CREATE" source="_qlGwkA4UEeCVYKbk8vub6g" target="_rLxvAA4UEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="handle target" uuid="_JfSmQA4UEeCVYKbk8vub6g" incoming="_M6LYwA4UEeCVYKbk8vub6g" outgoing="_5FKgsA4UEeCVYKbk8vub6g">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelRootSpl" uuid="_r9sUIA4UEeCVYKbk8vub6g" outgoingStoryLinks="_17jrcA4UEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="targetRootEReference" uuid="_so6bYA4UEeCVYKbk8vub6g" incomingStoryLinks="_17jrcA4UEeCVYKbk8vub6g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_17jrcA4UEeCVYKbk8vub6g" modifier="CREATE" source="_r9sUIA4UEeCVYKbk8vub6g" target="_so6bYA4UEeCVYKbk8vub6g">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_LBn7sA4UEeCVYKbk8vub6g" incoming="_LiMSIA4UEeCVYKbk8vub6g" outgoing="_MF9GgA4UEeCVYKbk8vub6g _M6LYwA4UEeCVYKbk8vub6g"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_3KBCoA4UEeCVYKbk8vub6g" incoming="_4MArIA4UEeCVYKbk8vub6g _5FKgsA4UEeCVYKbk8vub6g" outgoing="_5k5JoA4UEeCVYKbk8vub6g"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_67GHkA4UEeCVYKbk8vub6g" incoming="_7ljZ0A4UEeCVYKbk8vub6g"/>
    <edges uuid="_X4_FIA4TEeCVYKbk8vub6g" source="_HT47IA4TEeCVYKbk8vub6g" target="_H0g78A4TEeCVYKbk8vub6g"/>
    <edges uuid="_cDL0sA4TEeCVYKbk8vub6g" source="_H0g78A4TEeCVYKbk8vub6g" target="_YcEkEA4TEeCVYKbk8vub6g"/>
    <edges uuid="_LiMSIA4UEeCVYKbk8vub6g" source="_YcEkEA4TEeCVYKbk8vub6g" target="_LBn7sA4UEeCVYKbk8vub6g" guardType="FOR_EACH"/>
    <edges uuid="_MF9GgA4UEeCVYKbk8vub6g" source="_LBn7sA4UEeCVYKbk8vub6g" target="_IFmPcA4UEeCVYKbk8vub6g" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OyCSQA4UEeCVYKbk8vub6g" expressionString="modelDomain.oclIsKindOf(tgg::SourceModelDomain)" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_M6LYwA4UEeCVYKbk8vub6g" source="_LBn7sA4UEeCVYKbk8vub6g" target="_JfSmQA4UEeCVYKbk8vub6g" guardType="ELSE"/>
    <edges uuid="_4MArIA4UEeCVYKbk8vub6g" source="_IFmPcA4UEeCVYKbk8vub6g" target="_3KBCoA4UEeCVYKbk8vub6g"/>
    <edges uuid="_5FKgsA4UEeCVYKbk8vub6g" source="_JfSmQA4UEeCVYKbk8vub6g" target="_3KBCoA4UEeCVYKbk8vub6g"/>
    <edges uuid="_5k5JoA4UEeCVYKbk8vub6g" source="_3KBCoA4UEeCVYKbk8vub6g" target="_YcEkEA4TEeCVYKbk8vub6g"/>
    <edges uuid="_7ljZ0A4UEeCVYKbk8vub6g" source="_YcEkEA4TEeCVYKbk8vub6g" target="_67GHkA4UEeCVYKbk8vub6g" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
