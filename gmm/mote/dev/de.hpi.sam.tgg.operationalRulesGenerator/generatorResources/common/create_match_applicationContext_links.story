<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_match_applicationContext_links" uuid="_md9-YBjQEeCuQ-PF2tTDdw">
  <activities name="create_match_applicationContext_links" uuid="_oE-YcBjQEeCuQ-PF2tTDdw">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_qNkcoBjQEeCuQ-PF2tTDdw" outgoing="_5rLksBjREeCuQ-PF2tTDdw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_qq8VIBjQEeCuQ-PF2tTDdw" incoming="_5rLksBjREeCuQ-PF2tTDdw" outgoing="_6ifAIBjREeCuQ-PF2tTDdw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchSpo" uuid="_rzG3EBjQEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_s89bEBjQEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_tdTvEBjQEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_uStGABjQEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchEClass" uuid="_B6m5sBjREeCuQ-PF2tTDdw" outgoingStoryLinks="_Lat-ABjREeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_I__oABjREeCuQ-PF2tTDdw" expressionString="mote::rules::Match" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="acEReference" uuid="_ClmXcBjREeCuQ-PF2tTDdw" incomingStoryLinks="_Lat-ABjREeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_XaJ5kBjTEeCuQ-PF2tTDdw" expressionString="self.name = 'applicationContext'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_GTMqkBjREeCuQ-PF2tTDdw" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Lat-ABjREeCuQ-PF2tTDdw" source="_B6m5sBjREeCuQ-PF2tTDdw" target="_ClmXcBjREeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="iterate through parent model objects" uuid="_29nnsBjQEeCuQ-PF2tTDdw" incoming="_6ifAIBjREeCuQ-PF2tTDdw" outgoing="_8L_7oBjREeCuQ-PF2tTDdw" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_7hyWcBjQEeCuQ-PF2tTDdw" outgoingStoryLinks="_lelvkBjREeCuQ-PF2tTDdw _qRMi8BjREeCuQ-PF2tTDdw _qnKy8BjREeCuQ-PF2tTDdw _Vg01wC3iEeC5YsI6nJcRyg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_8POrQBjQEeCuQ-PF2tTDdw" outgoingStoryLinks="_VzcFUBjREeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelDomain" uuid="_8y71QBjQEeCuQ-PF2tTDdw" outgoingStoryLinks="_WvjvUBjREeCuQ-PF2tTDdw" incomingStoryLinks="_VzcFUBjREeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_9XyOwBjQEeCuQ-PF2tTDdw" incomingStoryLinks="_WvjvUBjREeCuQ-PF2tTDdw _ozsFkClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZsD8sBjREeCuQ-PF2tTDdw" expressionString="self.modifier = tgg::TGGModifierEnumeration::NONE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_-U0ewBjQEeCuQ-PF2tTDdw" incomingStoryLinks="_qnKy8BjREeCuQ-PF2tTDdw _u3_H4BjREeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchSpo" uuid="__BtZ8BjQEeCuQ-PF2tTDdw" incomingStoryLinks="_lelvkBjREeCuQ-PF2tTDdw _uhBZYBjREeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="acSpl" uuid="_ASnoMBjREeCuQ-PF2tTDdw" modifier="CREATE" outgoingStoryLinks="_uhBZYBjREeCuQ-PF2tTDdw _u3_H4BjREeCuQ-PF2tTDdw _vnNlYBjREeCuQ-PF2tTDdw _cJsnMC3iEeC5YsI6nJcRyg _uLBzwC3kEeCbk5MohfLeow _A6h_AFUlEeCe3uBV0PSH_A" incomingStoryLinks="_qRMi8BjREeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_A_QEsBjREeCuQ-PF2tTDdw" incomingStoryLinks="_vnNlYBjREeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_meRq8BjREeCuQ-PF2tTDdw" outgoingStoryLinks="_ozsFkClYEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="nameSpo" uuid="_QxxssC3iEeC5YsI6nJcRyg" modifier="CREATE" outgoingStoryLinks="_keWlAC3iEeC5YsI6nJcRyg _q01l8C3iEeC5YsI6nJcRyg" incomingStoryLinks="_Vg01wC3iEeC5YsI6nJcRyg _cJsnMC3iEeC5YsI6nJcRyg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_W1NOwC3iEeC5YsI6nJcRyg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Yee5sC3iEeC5YsI6nJcRyg" expressionString="modelObject.name.concat('String')" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_d5hmEC3iEeC5YsI6nJcRyg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fN6mIC3iEeC5YsI6nJcRyg" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_igi0gC3iEeC5YsI6nJcRyg" modifier="CREATE" outgoingStoryLinks="_ynBv4C3iEeC5YsI6nJcRyg" incomingStoryLinks="_keWlAC3iEeC5YsI6nJcRyg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eStringEDataType" uuid="_mvrucC3iEeC5YsI6nJcRyg" incomingStoryLinks="_q01l8C3iEeC5YsI6nJcRyg _yC3S4C3iEeC5YsI6nJcRyg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EDataType"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_pNKdgC3iEeC5YsI6nJcRyg" expressionString="ecore::EString" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="stringLiteral" uuid="_rz_84C3iEeC5YsI6nJcRyg" modifier="CREATE" outgoingStoryLinks="_yC3S4C3iEeC5YsI6nJcRyg" incomingStoryLinks="_ynBv4C3iEeC5YsI6nJcRyg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction"/>
        <attributeAssignments uuid="_t_1iUC3iEeC5YsI6nJcRyg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction/literal"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wLs88C3iEeC5YsI6nJcRyg" expressionString="modelObject.name" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mapEntryEClass" uuid="_qIUzUC3kEeCbk5MohfLeow" incomingStoryLinks="_uLBzwC3kEeCbk5MohfLeow" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_seVBYC3kEeCbk5MohfLeow" expressionString="mote::helpers::MapEntry" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="acEReference" uuid="_9vmUEVUkEeCe3uBV0PSH_A" incomingStoryLinks="_A6h_AFUlEeCe3uBV0PSH_A" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_VzcFUBjREeCuQ-PF2tTDdw" source="_8POrQBjQEeCuQ-PF2tTDdw" target="_8y71QBjQEeCuQ-PF2tTDdw"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WvjvUBjREeCuQ-PF2tTDdw" source="_8y71QBjQEeCuQ-PF2tTDdw" target="_9XyOwBjQEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lelvkBjREeCuQ-PF2tTDdw" source="_7hyWcBjQEeCuQ-PF2tTDdw" target="__BtZ8BjQEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qRMi8BjREeCuQ-PF2tTDdw" modifier="CREATE" source="_7hyWcBjQEeCuQ-PF2tTDdw" target="_ASnoMBjREeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qnKy8BjREeCuQ-PF2tTDdw" source="_7hyWcBjQEeCuQ-PF2tTDdw" target="_-U0ewBjQEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_uhBZYBjREeCuQ-PF2tTDdw" modifier="CREATE" source="_ASnoMBjREeCuQ-PF2tTDdw" target="__BtZ8BjQEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_u3_H4BjREeCuQ-PF2tTDdw" modifier="CREATE" source="_ASnoMBjREeCuQ-PF2tTDdw" target="_-U0ewBjQEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/valueTarget"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_vnNlYBjREeCuQ-PF2tTDdw" modifier="CREATE" source="_ASnoMBjREeCuQ-PF2tTDdw" target="_A_QEsBjREeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_ozsFkClYEeCFgb_w6FqGaA" source="_meRq8BjREeCuQ-PF2tTDdw" target="_9XyOwBjQEeCuQ-PF2tTDdw" valueTarget="_-U0ewBjQEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Vg01wC3iEeC5YsI6nJcRyg" modifier="CREATE" source="_7hyWcBjQEeCuQ-PF2tTDdw" target="_QxxssC3iEeC5YsI6nJcRyg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_cJsnMC3iEeC5YsI6nJcRyg" modifier="CREATE" source="_ASnoMBjREeCuQ-PF2tTDdw" target="_QxxssC3iEeC5YsI6nJcRyg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_keWlAC3iEeC5YsI6nJcRyg" modifier="CREATE" source="_QxxssC3iEeC5YsI6nJcRyg" target="_igi0gC3iEeC5YsI6nJcRyg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_q01l8C3iEeC5YsI6nJcRyg" modifier="CREATE" source="_QxxssC3iEeC5YsI6nJcRyg" target="_mvrucC3iEeC5YsI6nJcRyg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_yC3S4C3iEeC5YsI6nJcRyg" modifier="CREATE" source="_rz_84C3iEeC5YsI6nJcRyg" target="_mvrucC3iEeC5YsI6nJcRyg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ynBv4C3iEeC5YsI6nJcRyg" modifier="CREATE" source="_igi0gC3iEeC5YsI6nJcRyg" target="_rz_84C3iEeC5YsI6nJcRyg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_uLBzwC3kEeCbk5MohfLeow" modifier="CREATE" source="_ASnoMBjREeCuQ-PF2tTDdw" target="_qIUzUC3kEeCbk5MohfLeow">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_A6h_AFUlEeCe3uBV0PSH_A" modifier="CREATE" source="_ASnoMBjREeCuQ-PF2tTDdw" target="_9vmUEVUkEeCe3uBV0PSH_A">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_69LYEBjREeCuQ-PF2tTDdw" incoming="_ychJIBjSEeCuQ-PF2tTDdw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="iterate through parent model objects" uuid="_iL4zQBjSEeCuQ-PF2tTDdw" incoming="_8L_7oBjREeCuQ-PF2tTDdw" outgoing="_ychJIBjSEeCuQ-PF2tTDdw" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_iL6ocRjSEeCuQ-PF2tTDdw" outgoingStoryLinks="_iL6BYxjSEeCuQ-PF2tTDdw _iL5aVRjSEeCuQ-PF2tTDdw _iL6ocBjSEeCuQ-PF2tTDdw __7u1QC3kEeCbk5MohfLeow">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_iL6BYRjSEeCuQ-PF2tTDdw" outgoingStoryLinks="_tKxisBjSEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_iL5aVhjSEeCuQ-PF2tTDdw" outgoingStoryLinks="_iL6BZBjSEeCuQ-PF2tTDdw" incomingStoryLinks="_tKxisBjSEeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNode" uuid="_iL6odhjSEeCuQ-PF2tTDdw" incomingStoryLinks="_iL6BZBjSEeCuQ-PF2tTDdw _rispgClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_iL6odxjSEeCuQ-PF2tTDdw" expressionString="self.modifier = tgg::TGGModifierEnumeration::NONE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_iL5aVBjSEeCuQ-PF2tTDdw" incomingStoryLinks="_iL6ocBjSEeCuQ-PF2tTDdw _iL5aUhjSEeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchSpo" uuid="_iL5aUxjSEeCuQ-PF2tTDdw" incomingStoryLinks="_iL6BYxjSEeCuQ-PF2tTDdw _iL6BYBjSEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="acSpl" uuid="_iL6BaBjSEeCuQ-PF2tTDdw" modifier="CREATE" outgoingStoryLinks="_iL6BYBjSEeCuQ-PF2tTDdw _iL5aUhjSEeCuQ-PF2tTDdw _iL6BZxjSEeCuQ-PF2tTDdw _iL5aURjSEeCuQ-PF2tTDdw _9NlM8C3kEeCbk5MohfLeow _F0eKQC3lEeCbk5MohfLeow" incomingStoryLinks="_iL5aVRjSEeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_iL6odRjSEeCuQ-PF2tTDdw" incomingStoryLinks="_iL5aURjSEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="acEReference" uuid="_iL6BZhjSEeCuQ-PF2tTDdw" incomingStoryLinks="_iL6BZxjSEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_iL5aUBjSEeCuQ-PF2tTDdw" outgoingStoryLinks="_rispgClYEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mapEntryEClass" uuid="_2TE48C3kEeCbk5MohfLeow" incomingStoryLinks="_F0eKQC3lEeCbk5MohfLeow" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2TE48S3kEeCbk5MohfLeow" expressionString="mote::helpers::MapEntry" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eStringEDataType" uuid="_2TE48i3kEeCbk5MohfLeow" incomingStoryLinks="_B3Qt0C3lEeCbk5MohfLeow _EqPy0C3lEeCbk5MohfLeow" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EDataType"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2TE48y3kEeCbk5MohfLeow" expressionString="ecore::EString" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="stringLiteral" uuid="_2TFgAC3kEeCbk5MohfLeow" modifier="CREATE" outgoingStoryLinks="_EqPy0C3lEeCbk5MohfLeow" incomingStoryLinks="_D-m00C3lEeCbk5MohfLeow">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction"/>
        <attributeAssignments uuid="_2TFgAS3kEeCbk5MohfLeow">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction/literal"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2TFgAi3kEeCbk5MohfLeow" expressionString="corrNode.name" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_2TFgAy3kEeCbk5MohfLeow" modifier="CREATE" outgoingStoryLinks="_D-m00C3lEeCbk5MohfLeow" incomingStoryLinks="_DBpdUC3lEeCbk5MohfLeow">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="nameSpo" uuid="_2TFgBC3kEeCbk5MohfLeow" modifier="CREATE" outgoingStoryLinks="_B3Qt0C3lEeCbk5MohfLeow _DBpdUC3lEeCbk5MohfLeow" incomingStoryLinks="_9NlM8C3kEeCbk5MohfLeow __7u1QC3kEeCbk5MohfLeow">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_2TFgBy3kEeCbk5MohfLeow">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2TFgCC3kEeCbk5MohfLeow" expressionString="corrNode.name.concat('String')" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_2TFgBS3kEeCbk5MohfLeow">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2TFgBi3kEeCbk5MohfLeow" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL6BZBjSEeCuQ-PF2tTDdw" source="_iL5aVhjSEeCuQ-PF2tTDdw" target="_iL6odhjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL6BYxjSEeCuQ-PF2tTDdw" source="_iL6ocRjSEeCuQ-PF2tTDdw" target="_iL5aUxjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL5aVRjSEeCuQ-PF2tTDdw" modifier="CREATE" source="_iL6ocRjSEeCuQ-PF2tTDdw" target="_iL6BaBjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL6ocBjSEeCuQ-PF2tTDdw" source="_iL6ocRjSEeCuQ-PF2tTDdw" target="_iL5aVBjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL6BYBjSEeCuQ-PF2tTDdw" modifier="CREATE" source="_iL6BaBjSEeCuQ-PF2tTDdw" target="_iL5aUxjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL5aUhjSEeCuQ-PF2tTDdw" modifier="CREATE" source="_iL6BaBjSEeCuQ-PF2tTDdw" target="_iL5aVBjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/valueTarget"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL6BZxjSEeCuQ-PF2tTDdw" modifier="CREATE" source="_iL6BaBjSEeCuQ-PF2tTDdw" target="_iL6BZhjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL5aURjSEeCuQ-PF2tTDdw" modifier="CREATE" source="_iL6BaBjSEeCuQ-PF2tTDdw" target="_iL6odRjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tKxisBjSEeCuQ-PF2tTDdw" source="_iL6BYRjSEeCuQ-PF2tTDdw" target="_iL5aVhjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_rispgClYEeCFgb_w6FqGaA" source="_iL5aUBjSEeCuQ-PF2tTDdw" target="_iL6odhjSEeCuQ-PF2tTDdw" valueTarget="_iL5aVBjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9NlM8C3kEeCbk5MohfLeow" modifier="CREATE" source="_iL6BaBjSEeCuQ-PF2tTDdw" target="_2TFgBC3kEeCbk5MohfLeow">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="__7u1QC3kEeCbk5MohfLeow" modifier="CREATE" source="_iL6ocRjSEeCuQ-PF2tTDdw" target="_2TFgBC3kEeCbk5MohfLeow">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_B3Qt0C3lEeCbk5MohfLeow" modifier="CREATE" source="_2TFgBC3kEeCbk5MohfLeow" target="_2TE48i3kEeCbk5MohfLeow">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_DBpdUC3lEeCbk5MohfLeow" modifier="CREATE" source="_2TFgBC3kEeCbk5MohfLeow" target="_2TFgAy3kEeCbk5MohfLeow">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_D-m00C3lEeCbk5MohfLeow" modifier="CREATE" source="_2TFgAy3kEeCbk5MohfLeow" target="_2TFgAC3kEeCbk5MohfLeow">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_EqPy0C3lEeCbk5MohfLeow" modifier="CREATE" source="_2TFgAC3kEeCbk5MohfLeow" target="_2TE48i3kEeCbk5MohfLeow">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_F0eKQC3lEeCbk5MohfLeow" modifier="CREATE" source="_iL6BaBjSEeCuQ-PF2tTDdw" target="_2TE48C3kEeCbk5MohfLeow">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/classifier"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_5rLksBjREeCuQ-PF2tTDdw" source="_qNkcoBjQEeCuQ-PF2tTDdw" target="_qq8VIBjQEeCuQ-PF2tTDdw"/>
    <edges uuid="_6ifAIBjREeCuQ-PF2tTDdw" source="_qq8VIBjQEeCuQ-PF2tTDdw" target="_29nnsBjQEeCuQ-PF2tTDdw"/>
    <edges uuid="_8L_7oBjREeCuQ-PF2tTDdw" source="_29nnsBjQEeCuQ-PF2tTDdw" target="_iL4zQBjSEeCuQ-PF2tTDdw" guardType="END"/>
    <edges uuid="_ychJIBjSEeCuQ-PF2tTDdw" source="_iL4zQBjSEeCuQ-PF2tTDdw" target="_69LYEBjREeCuQ-PF2tTDdw" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
