<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="transform_modelObjects" uuid="_9wnJcAwiEeCU3uEhqcP0Dg">
  <activities name="transform_modelObjects" uuid="_-o6DYAwiEeCU3uEhqcP0Dg">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_C00DIAwjEeCU3uEhqcP0Dg" outgoing="_VIG0UAwjEeCU3uEhqcP0Dg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_DZ2p4AwjEeCU3uEhqcP0Dg" incoming="_VIG0UAwjEeCU3uEhqcP0Dg" outgoing="_VjwOgAwjEeCU3uEhqcP0Dg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_Eo7FEAwjEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_FZEIkAwjEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_GLENQAwjEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_G33B0AwjEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformConstraints" uuid="_OO3a4Aw_EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformAttributeAssignments" uuid="_9D2bMAxQEeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformParentNodes" uuid="_-JOu4AxQEeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformCreatedNodes" uuid="_Fbe0gAxREeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformSource" uuid="_G4BD8AxREeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformTarget" uuid="_HkM0gAxREeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="bindingType" uuid="_nC9uwBQjEeCkcYaoegFQZw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformAttributeConstraints" uuid="_l_yw0CVWEeCVE9iKiEiTJw" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="iterate through model objects" uuid="_RbKogAwjEeCU3uEhqcP0Dg" incoming="_VjwOgAwjEeCU3uEhqcP0Dg _QvCW8AxSEeCp2NlniWjADQ" outgoing="_5-25oAwjEeCU3uEhqcP0Dg _H7caYAxSEeCp2NlniWjADQ" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_V_5_AAwjEeCU3uEhqcP0Dg" outgoingStoryLinks="_Xi_zgAxREeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelDomain" uuid="_W8fjEAwjEeCU3uEhqcP0Dg" outgoingStoryLinks="_bAIXoAwjEeCU3uEhqcP0Dg" incomingStoryLinks="_Xi_zgAxREeCp2NlniWjADQ">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_ZqIOAAwjEeCU3uEhqcP0Dg" incomingStoryLinks="_bAIXoAwjEeCU3uEhqcP0Dg">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_bAIXoAwjEeCU3uEhqcP0Dg" source="_W8fjEAwjEeCU3uEhqcP0Dg" target="_ZqIOAAwjEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_Xi_zgAxREeCp2NlniWjADQ" source="_V_5_AAwjEeCU3uEhqcP0Dg" target="_W8fjEAwjEeCU3uEhqcP0Dg"/>
      <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_c3aTcAxREeCp2NlniWjADQ" expressionString="transformSource and modelDomain.oclIsKindOf(tgg::SourceModelDomain) or&#xA;transformTarget and modelDomain.oclIsKindOf(tgg::TargetModelDomain)" expressionLanguage="OCL"/>
      <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IuADQAxTEeCp2NlniWjADQ" expressionString="transformParentNodes and modelObject.modifier = tgg::TGGModifierEnumeration::NONE or&#xA;transformCreatedNodes and modelObject.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_4iS1AAwjEeCU3uEhqcP0Dg" incoming="_5-25oAwjEeCU3uEhqcP0Dg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform constraints" uuid="_3avVEAw5EeCAUcBgGC58lA" incoming="_N4V2oAxSEeCp2NlniWjADQ" outgoing="_OJKg4AxSEeCp2NlniWjADQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_5MltMAw5EeCAUcBgGC58lA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_7a2UgAw5EeCAUcBgGC58lA">
          <activity href="transform_constraints.story#_3AxAwAw3EeCzq8Eugced9g"/>
          <parameters name="modelObject" uuid="_8Xos4Aw5EeCAUcBgGC58lA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__EXY4Aw5EeCAUcBgGC58lA" expressionString="modelObject" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
          </parameters>
          <parameters name="storyPatternObject" uuid="_AjrPgAw6EeCAUcBgGC58lA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_CE_RQAw6EeCAUcBgGC58lA" expressionString="spo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="transformAttributeConstraints" uuid="_qXRucCVWEeCVE9iKiEiTJw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ssIF8CVWEeCVE9iKiEiTJw" expressionString="transformAttributeConstraints" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
          <parameters name="transformConstraints" uuid="_0F0zoC6nEeCRIK6l8KTurA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1mLzIC6nEeCRIK6l8KTurA" expressionString="transformConstraints" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="transform model objects" uuid="_mIxKEAxREeCp2NlniWjADQ" incoming="_H7caYAxSEeCp2NlniWjADQ" outgoing="_NlVbEAxSEeCp2NlniWjADQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_o6BJsAxREeCp2NlniWjADQ" outgoingStoryLinks="_5XkhQAxREeCp2NlniWjADQ" incomingStoryLinks="_qkKW8ClTEeCNurcZhXhx4w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_phpxcAxREeCp2NlniWjADQ" outgoingStoryLinks="_qkKW8ClTEeCNurcZhXhx4w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_qpVLMAxREeCp2NlniWjADQ" outgoingStoryLinks="_4eIX0AxREeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_r-3awAxREeCp2NlniWjADQ" incomingStoryLinks="_4x3zMAxREeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eClass" uuid="_tHXT4AxREeCp2NlniWjADQ" incomingStoryLinks="_5FGRMAxREeCp2NlniWjADQ _5XkhQAxREeCp2NlniWjADQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_tpyrgAxREeCp2NlniWjADQ" modifier="CREATE" outgoingStoryLinks="_4x3zMAxREeCp2NlniWjADQ _5FGRMAxREeCp2NlniWjADQ _LW-TYBQkEeCkcYaoegFQZw" incomingStoryLinks="_4eIX0AxREeCp2NlniWjADQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_9NJrsAxREeCp2NlniWjADQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__HtT4AxREeCp2NlniWjADQ" expressionString="modelObject.name" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="bindingType" uuid="_I1TA0BQkEeCkcYaoegFQZw" incomingStoryLinks="_LW-TYBQkEeCkcYaoegFQZw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4eIX0AxREeCp2NlniWjADQ" modifier="CREATE" source="_qpVLMAxREeCp2NlniWjADQ" target="_tpyrgAxREeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_4x3zMAxREeCp2NlniWjADQ" modifier="CREATE" source="_tpyrgAxREeCp2NlniWjADQ" target="_r-3awAxREeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5FGRMAxREeCp2NlniWjADQ" modifier="CREATE" source="_tpyrgAxREeCp2NlniWjADQ" target="_tHXT4AxREeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5XkhQAxREeCp2NlniWjADQ" source="_o6BJsAxREeCp2NlniWjADQ" target="_tHXT4AxREeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LW-TYBQkEeCkcYaoegFQZw" modifier="CREATE" source="_tpyrgAxREeCp2NlniWjADQ" target="_I1TA0BQkEeCkcYaoegFQZw">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_qkKW8ClTEeCNurcZhXhx4w" modifier="CREATE" source="_phpxcAxREeCp2NlniWjADQ" target="_o6BJsAxREeCp2NlniWjADQ" valueTarget="_tpyrgAxREeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_K_PikAxSEeCp2NlniWjADQ" incoming="_NlVbEAxSEeCp2NlniWjADQ" outgoing="_N4V2oAxSEeCp2NlniWjADQ _OcGD8AxSEeCp2NlniWjADQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_LetFwAxSEeCp2NlniWjADQ" incoming="_OJKg4AxSEeCp2NlniWjADQ _OcGD8AxSEeCp2NlniWjADQ" outgoing="_OubxIAxSEeCp2NlniWjADQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_L21R4AxSEeCp2NlniWjADQ" incoming="_OubxIAxSEeCp2NlniWjADQ" outgoing="_PByyAAxSEeCp2NlniWjADQ _PuRdgAxSEeCp2NlniWjADQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_MQ1tUAxSEeCp2NlniWjADQ" incoming="_PXswkAxSEeCp2NlniWjADQ _PuRdgAxSEeCp2NlniWjADQ" outgoing="_QvCW8AxSEeCp2NlniWjADQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="transform attribute assignments" uuid="_Mkcl0AxSEeCp2NlniWjADQ" incoming="_PByyAAxSEeCp2NlniWjADQ" outgoing="_PXswkAxSEeCp2NlniWjADQ">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_Mkcl0QxSEeCp2NlniWjADQ">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_Mkcl0gxSEeCp2NlniWjADQ">
          <activity href="transform_attributeAssignments.story#_Kc8TUAw9EeCAUcBgGC58lA"/>
          <parameters name="modelObject" uuid="_Mkcl0wxSEeCp2NlniWjADQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Mkcl1AxSEeCp2NlniWjADQ" expressionString="modelObject" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
          </parameters>
          <parameters name="storyPatternObject" uuid="_Mkcl1QxSEeCp2NlniWjADQ">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Mkcl1gxSEeCp2NlniWjADQ" expressionString="spo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <edges uuid="_VIG0UAwjEeCU3uEhqcP0Dg" source="_C00DIAwjEeCU3uEhqcP0Dg" target="_DZ2p4AwjEeCU3uEhqcP0Dg"/>
    <edges uuid="_VjwOgAwjEeCU3uEhqcP0Dg" source="_DZ2p4AwjEeCU3uEhqcP0Dg" target="_RbKogAwjEeCU3uEhqcP0Dg"/>
    <edges uuid="_5-25oAwjEeCU3uEhqcP0Dg" source="_RbKogAwjEeCU3uEhqcP0Dg" target="_4iS1AAwjEeCU3uEhqcP0Dg" guardType="END"/>
    <edges uuid="_H7caYAxSEeCp2NlniWjADQ" source="_RbKogAwjEeCU3uEhqcP0Dg" target="_mIxKEAxREeCp2NlniWjADQ" guardType="FOR_EACH"/>
    <edges uuid="_NlVbEAxSEeCp2NlniWjADQ" source="_mIxKEAxREeCp2NlniWjADQ" target="_K_PikAxSEeCp2NlniWjADQ"/>
    <edges uuid="_N4V2oAxSEeCp2NlniWjADQ" source="_K_PikAxSEeCp2NlniWjADQ" target="_3avVEAw5EeCAUcBgGC58lA" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y0jskAxSEeCp2NlniWjADQ" expressionString="transformConstraints or transformAttributeConstraints" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_OJKg4AxSEeCp2NlniWjADQ" source="_3avVEAw5EeCAUcBgGC58lA" target="_LetFwAxSEeCp2NlniWjADQ"/>
    <edges uuid="_OcGD8AxSEeCp2NlniWjADQ" source="_K_PikAxSEeCp2NlniWjADQ" target="_LetFwAxSEeCp2NlniWjADQ" guardType="ELSE"/>
    <edges uuid="_OubxIAxSEeCp2NlniWjADQ" source="_LetFwAxSEeCp2NlniWjADQ" target="_L21R4AxSEeCp2NlniWjADQ"/>
    <edges uuid="_PByyAAxSEeCp2NlniWjADQ" source="_L21R4AxSEeCp2NlniWjADQ" target="_Mkcl0AxSEeCp2NlniWjADQ" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bZ4wEAxSEeCp2NlniWjADQ" expressionString="transformAttributeAssignments" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_PXswkAxSEeCp2NlniWjADQ" source="_Mkcl0AxSEeCp2NlniWjADQ" target="_MQ1tUAxSEeCp2NlniWjADQ"/>
    <edges uuid="_PuRdgAxSEeCp2NlniWjADQ" source="_L21R4AxSEeCp2NlniWjADQ" target="_MQ1tUAxSEeCp2NlniWjADQ" guardType="ELSE"/>
    <edges uuid="_QvCW8AxSEeCp2NlniWjADQ" source="_MQ1tUAxSEeCp2NlniWjADQ" target="_RbKogAwjEeCU3uEhqcP0Dg"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
