<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_ruleSet_corrNodesLink" uuid="_NFwB0BQCEeCDO6mN3ETDMg">
  <activities name="create_ruleSet_corrNodesLink" uuid="_NnQzcBQCEeCDO6mN3ETDMg">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_Pmp4UBQCEeCDO6mN3ETDMg" outgoing="__ZXU4BQCEeCDO6mN3ETDMg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_QmasMBQCEeCDO6mN3ETDMg" incoming="__ZXU4BQCEeCDO6mN3ETDMg" outgoing="_AdK7QBQDEeCDO6mN3ETDMg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_RT4PIBQCEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_SwewABQCEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleSetEClass" uuid="_UUijwBQCEeCDO6mN3ETDMg" outgoingStoryLinks="_icDRABQCEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fwV4sBQCEeCDO6mN3ETDMg" expressionString="mote::rules::TGGRuleSet" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_Vjb_0BQCEeCDO6mN3ETDMg" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodesEReference" uuid="_V_he4BQCEeCDO6mN3ETDMg" incomingStoryLinks="_icDRABQCEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_j2WEwBQCEeCDO6mN3ETDMg" expressionString="self.name = 'correspondenceNodes'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mapEntryEClass" uuid="_ohQUsBQCEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qlpOUBQCEeCDO6mN3ETDMg" expressionString="mote::helpers::MapEntry" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_aSGWQBQhEeCkcYaoegFQZw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_azelABQhEeCkcYaoegFQZw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_icDRABQCEeCDO6mN3ETDMg" source="_UUijwBQCEeCDO6mN3ETDMg" target="_V_he4BQCEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create link" uuid="_svCjwBQCEeCDO6mN3ETDMg" incoming="_AdK7QBQDEeCDO6mN3ETDMg" outgoing="_BEjrYBQDEeCDO6mN3ETDMg" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_tY1HoBQCEeCDO6mN3ETDMg" incomingStoryLinks="_5fTxQBQCEeCDO6mN3ETDMg _6F-vsBQCEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodeSpo" uuid="_uBKS8BQCEeCDO6mN3ETDMg" incomingStoryLinks="_52s9kBQCEeCDO6mN3ETDMg _6WXVEBQCEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_u2VAYBQCEeCDO6mN3ETDMg" outgoingStoryLinks="_48cUwBQCEeCDO6mN3ETDMg _5fTxQBQCEeCDO6mN3ETDMg _52s9kBQCEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="link" uuid="_vuleEBQCEeCDO6mN3ETDMg" modifier="CREATE" outgoingStoryLinks="_6F-vsBQCEeCDO6mN3ETDMg _6WXVEBQCEeCDO6mN3ETDMg _6qVZ8BQCEeCDO6mN3ETDMg _7NRu8BQCEeCDO6mN3ETDMg _JSYocBQDEeCDO6mN3ETDMg" incomingStoryLinks="_48cUwBQCEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodesEReference" uuid="_xR5D4BQCEeCDO6mN3ETDMg" incomingStoryLinks="_7NRu8BQCEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mapEntryEClass" uuid="_yMwJoBQCEeCDO6mN3ETDMg" incomingStoryLinks="_6qVZ8BQCEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_GzF7gBQDEeCDO6mN3ETDMg" incomingStoryLinks="_JSYocBQDEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_hv5kkBQhEeCkcYaoegFQZw" outgoingStoryLinks="_WmZ1wClYEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_i-3SEBQhEeCkcYaoegFQZw" outgoingStoryLinks="_tavbEBQhEeCkcYaoegFQZw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_jcYUgBQhEeCkcYaoegFQZw" outgoingStoryLinks="_tzQBsBQhEeCkcYaoegFQZw" incomingStoryLinks="_tavbEBQhEeCkcYaoegFQZw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNode" uuid="_j-gKIBQhEeCkcYaoegFQZw" incomingStoryLinks="_tzQBsBQhEeCkcYaoegFQZw _WmZ1wClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_pfuXABQhEeCkcYaoegFQZw" expressionString="self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_48cUwBQCEeCDO6mN3ETDMg" modifier="CREATE" source="_u2VAYBQCEeCDO6mN3ETDMg" target="_vuleEBQCEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5fTxQBQCEeCDO6mN3ETDMg" source="_u2VAYBQCEeCDO6mN3ETDMg" target="_tY1HoBQCEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_52s9kBQCEeCDO6mN3ETDMg" source="_u2VAYBQCEeCDO6mN3ETDMg" target="_uBKS8BQCEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_6F-vsBQCEeCDO6mN3ETDMg" modifier="CREATE" source="_vuleEBQCEeCDO6mN3ETDMg" target="_tY1HoBQCEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_6WXVEBQCEeCDO6mN3ETDMg" modifier="CREATE" source="_vuleEBQCEeCDO6mN3ETDMg" target="_uBKS8BQCEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_6qVZ8BQCEeCDO6mN3ETDMg" modifier="CREATE" source="_vuleEBQCEeCDO6mN3ETDMg" target="_yMwJoBQCEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7NRu8BQCEeCDO6mN3ETDMg" modifier="CREATE" source="_vuleEBQCEeCDO6mN3ETDMg" target="_xR5D4BQCEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_JSYocBQDEeCDO6mN3ETDMg" modifier="CREATE" source="_vuleEBQCEeCDO6mN3ETDMg" target="_GzF7gBQDEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tavbEBQhEeCkcYaoegFQZw" source="_i-3SEBQhEeCkcYaoegFQZw" target="_jcYUgBQhEeCkcYaoegFQZw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tzQBsBQhEeCkcYaoegFQZw" source="_jcYUgBQhEeCkcYaoegFQZw" target="_j-gKIBQhEeCkcYaoegFQZw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_WmZ1wClYEeCFgb_w6FqGaA" source="_hv5kkBQhEeCkcYaoegFQZw" target="_j-gKIBQhEeCkcYaoegFQZw" valueTarget="_uBKS8BQCEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="__m9sABQCEeCDO6mN3ETDMg" incoming="_BEjrYBQDEeCDO6mN3ETDMg"/>
    <edges uuid="__ZXU4BQCEeCDO6mN3ETDMg" source="_Pmp4UBQCEeCDO6mN3ETDMg" target="_QmasMBQCEeCDO6mN3ETDMg"/>
    <edges uuid="_AdK7QBQDEeCDO6mN3ETDMg" source="_QmasMBQCEeCDO6mN3ETDMg" target="_svCjwBQCEeCDO6mN3ETDMg"/>
    <edges uuid="_BEjrYBQDEeCDO6mN3ETDMg" source="_svCjwBQCEeCDO6mN3ETDMg" target="__m9sABQCEeCDO6mN3ETDMg" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
