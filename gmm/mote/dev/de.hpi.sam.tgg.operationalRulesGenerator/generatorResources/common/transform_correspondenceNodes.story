<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="transform_correspondenceNodes" uuid="_e9UjwBNnEeCRUJdROfc5HA">
  <activities name="transform_correspondenceNodes" uuid="_gUE7UBNnEeCRUJdROfc5HA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_ixCF8BNnEeCRUJdROfc5HA" outgoing="_B4uWsBNoEeCRUJdROfc5HA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_jVrrIBNnEeCRUJdROfc5HA" incoming="_B4uWsBNoEeCRUJdROfc5HA" outgoing="_kZk7oBNoEeCRUJdROfc5HA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_kV-DcBNnEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_k2174BNnEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_l21ZQBNnEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_mrvA8BNnEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformParentNodes" uuid="_nP8hQBNnEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformCreatedNodes" uuid="_oB2fUBNnEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="bindingType" uuid="_A1xXoBWnEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="transform correspondence nodes" uuid="_Cy3DsBNoEeCRUJdROfc5HA" incoming="_kZk7oBNoEeCRUJdROfc5HA" outgoing="_llNBQBNoEeCRUJdROfc5HA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_GIQ5QBNoEeCRUJdROfc5HA" outgoingStoryLinks="_UT5RgBNoEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_GrSGwBNoEeCRUJdROfc5HA" outgoingStoryLinks="_UuHvYBNoEeCRUJdROfc5HA" incomingStoryLinks="_UT5RgBNoEeCRUJdROfc5HA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNode" uuid="_HRqKQBNoEeCRUJdROfc5HA" outgoingStoryLinks="_Yh7YMBNoEeCRUJdROfc5HA" incomingStoryLinks="_UuHvYBNoEeCRUJdROfc5HA _BXs40ClXEeC9R-RLOzTG5g">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GPNZUBNpEeCRUJdROfc5HA" expressionString="transformParentNodes and self.modifier = tgg::TGGModifierEnumeration::NONE or&#xA;transformCreatedNodes and self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_HyXDkBNoEeCRUJdROfc5HA" outgoingStoryLinks="_BXs40ClXEeC9R-RLOzTG5g" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_JLQJIBNoEeCRUJdROfc5HA" outgoingStoryLinks="_ZxHIIBNoEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyPatternObject" uuid="_J3uNkBNoEeCRUJdROfc5HA" modifier="CREATE" outgoingStoryLinks="_Y2J7wBNoEeCRUJdROfc5HA _aVnjYBNoEeCRUJdROfc5HA _EvI14BWnEeC7l7ZPwVRGQg" incomingStoryLinks="_ZxHIIBNoEeCRUJdROfc5HA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_fwoakBNoEeCRUJdROfc5HA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_g9VeQBNoEeCRUJdROfc5HA" expressionString="corrNode.name" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_LO5b4BNoEeCRUJdROfc5HA" incomingStoryLinks="_aVnjYBNoEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eClass" uuid="_XUetoBNoEeCRUJdROfc5HA" incomingStoryLinks="_Yh7YMBNoEeCRUJdROfc5HA _Y2J7wBNoEeCRUJdROfc5HA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="bindingType" uuid="_DSdcgBWnEeC7l7ZPwVRGQg" incomingStoryLinks="_EvI14BWnEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_UT5RgBNoEeCRUJdROfc5HA" source="_GIQ5QBNoEeCRUJdROfc5HA" target="_GrSGwBNoEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_UuHvYBNoEeCRUJdROfc5HA" source="_GrSGwBNoEeCRUJdROfc5HA" target="_HRqKQBNoEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Yh7YMBNoEeCRUJdROfc5HA" source="_HRqKQBNoEeCRUJdROfc5HA" target="_XUetoBNoEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Y2J7wBNoEeCRUJdROfc5HA" modifier="CREATE" source="_J3uNkBNoEeCRUJdROfc5HA" target="_XUetoBNoEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZxHIIBNoEeCRUJdROfc5HA" modifier="CREATE" source="_JLQJIBNoEeCRUJdROfc5HA" target="_J3uNkBNoEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_aVnjYBNoEeCRUJdROfc5HA" modifier="CREATE" source="_J3uNkBNoEeCRUJdROfc5HA" target="_LO5b4BNoEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_EvI14BWnEeC7l7ZPwVRGQg" modifier="CREATE" source="_J3uNkBNoEeCRUJdROfc5HA" target="_DSdcgBWnEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_BXs40ClXEeC9R-RLOzTG5g" modifier="CREATE" source="_HyXDkBNoEeCRUJdROfc5HA" target="_HRqKQBNoEeCRUJdROfc5HA" valueTarget="_J3uNkBNoEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_k3K2kBNoEeCRUJdROfc5HA" incoming="_llNBQBNoEeCRUJdROfc5HA"/>
    <edges uuid="_B4uWsBNoEeCRUJdROfc5HA" source="_ixCF8BNnEeCRUJdROfc5HA" target="_jVrrIBNnEeCRUJdROfc5HA"/>
    <edges uuid="_kZk7oBNoEeCRUJdROfc5HA" source="_jVrrIBNnEeCRUJdROfc5HA" target="_Cy3DsBNoEeCRUJdROfc5HA"/>
    <edges uuid="_llNBQBNoEeCRUJdROfc5HA" source="_Cy3DsBNoEeCRUJdROfc5HA" target="_k3K2kBNoEeCRUJdROfc5HA" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
