<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_evaluateRuleVariables_expression" uuid="_uabxYPZwEd-i2JdGlqLvVQ">
  <activities name="create_evaluateRuleVariables_expression" uuid="_uabxYfZwEd-i2JdGlqLvVQ">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_zcNpMPZwEd-i2JdGlqLvVQ" outgoing="_MgT_0PZxEd-i2JdGlqLvVQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_0Bv_MPZwEd-i2JdGlqLvVQ" incoming="_MgT_0PZxEd-i2JdGlqLvVQ" outgoing="_PEFkIPZxEd-i2JdGlqLvVQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_39lioPZwEd-i2JdGlqLvVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_5MV0wPZwEd-i2JdGlqLvVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create call action expression" uuid="_M6c-IPZxEd-i2JdGlqLvVQ" incoming="_PEFkIPZxEd-i2JdGlqLvVQ" outgoing="_rgaa0PZxEd-i2JdGlqLvVQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_PW7AkPZxEd-i2JdGlqLvVQ" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create rule variable assignments" uuid="_WmYQ4PZxEd-i2JdGlqLvVQ" incoming="_lQXRsPZ0Ed-i2JdGlqLvVQ _GvWxAPZ1Ed-i2JdGlqLvVQ" outgoing="_0TXBsPZyEd-i2JdGlqLvVQ _1AY04PZ0Ed-i2JdGlqLvVQ" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_sNs-oPZxEd-i2JdGlqLvVQ" outgoingStoryLinks="_GA7FEPZyEd-i2JdGlqLvVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_ucFhwPZxEd-i2JdGlqLvVQ" outgoingStoryLinks="_zbEkQPZxEd-i2JdGlqLvVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleVar" uuid="_x346QPZxEd-i2JdGlqLvVQ" incomingStoryLinks="_zbEkQPZxEd-i2JdGlqLvVQ">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//RuleVariable"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varDecl" uuid="_EGRWQPZyEd-i2JdGlqLvVQ" modifier="CREATE" incomingStoryLinks="_GA7FEPZyEd-i2JdGlqLvVQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableDeclarationAction"/>
        <attributeAssignments uuid="_HtQrEPZyEd-i2JdGlqLvVQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_J5ocAPZyEd-i2JdGlqLvVQ" expressionString="ruleVar.name" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_LrHAsPZyEd-i2JdGlqLvVQ">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NmGGsPZyEd-i2JdGlqLvVQ" expressionString="ruleVar.classifier" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zbEkQPZxEd-i2JdGlqLvVQ" source="_ucFhwPZxEd-i2JdGlqLvVQ" target="_x346QPZxEd-i2JdGlqLvVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/ruleVariables"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GA7FEPZyEd-i2JdGlqLvVQ" modifier="CREATE" source="_sNs-oPZxEd-i2JdGlqLvVQ" target="_EGRWQPZyEd-i2JdGlqLvVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_z0IvEPZyEd-i2JdGlqLvVQ" incoming="_0TXBsPZyEd-i2JdGlqLvVQ" outgoing="_51gboPZyEd-i2JdGlqLvVQ _6Q264PZyEd-i2JdGlqLvVQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="clone forward assignment" uuid="_1eibYPZyEd-i2JdGlqLvVQ" incoming="_51gboPZyEd-i2JdGlqLvVQ" outgoing="_kjNQwPZ0Ed-i2JdGlqLvVQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varDecl" uuid="_I9E3sPZzEd-i2JdGlqLvVQ" outgoingStoryLinks="_anrRMPZzEd-i2JdGlqLvVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableDeclarationAction"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="assignment" uuid="_KpjAkPZzEd-i2JdGlqLvVQ" incomingStoryLinks="_anrRMPZzEd-i2JdGlqLvVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_NgeKYPZzEd-i2JdGlqLvVQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_OFNPIPZzEd-i2JdGlqLvVQ" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters name="" uuid="_Th-Z0PZzEd-i2JdGlqLvVQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Y8N08PZzEd-i2JdGlqLvVQ" expressionString="ruleVar.forwardCalculationExpression" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_anrRMPZzEd-i2JdGlqLvVQ" modifier="CREATE" source="_I9E3sPZzEd-i2JdGlqLvVQ" target="_KpjAkPZzEd-i2JdGlqLvVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableDeclarationAction/valueAssignment"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="clone reverse assignment" uuid="_3LK8UPZyEd-i2JdGlqLvVQ" incoming="_6Q264PZyEd-i2JdGlqLvVQ" outgoing="_k3QOIPZ0Ed-i2JdGlqLvVQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varDecl" uuid="_J2EVMPZzEd-i2JdGlqLvVQ" outgoingStoryLinks="_c47ssPZ0Ed-i2JdGlqLvVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableDeclarationAction"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="assignment" uuid="_v075IPZzEd-i2JdGlqLvVQ" incomingStoryLinks="_c47ssPZ0Ed-i2JdGlqLvVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_RC3doPZ0Ed-i2JdGlqLvVQ">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_RqgFYPZ0Ed-i2JdGlqLvVQ" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters name="" uuid="_Yr9V8PZ0Ed-i2JdGlqLvVQ">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_adTXwPZ0Ed-i2JdGlqLvVQ" expressionString="ruleVar.reverseCalculationExpression" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" name="" uuid="_c47ssPZ0Ed-i2JdGlqLvVQ" modifier="CREATE" source="_J2EVMPZzEd-i2JdGlqLvVQ" target="_v075IPZzEd-i2JdGlqLvVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableDeclarationAction/valueAssignment"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_j0oTgPZ0Ed-i2JdGlqLvVQ" incoming="_kjNQwPZ0Ed-i2JdGlqLvVQ _k3QOIPZ0Ed-i2JdGlqLvVQ" outgoing="_lQXRsPZ0Ed-i2JdGlqLvVQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_0g3AQPZ0Ed-i2JdGlqLvVQ" incoming="_JzlssCorEeCjTcyrv1LVTA">
      <outParameterValues xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_u6D6YPZ1Ed-i2JdGlqLvVQ" expressionString="cae" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_F4I1IPZ1Ed-i2JdGlqLvVQ" incoming="_rgaa0PZxEd-i2JdGlqLvVQ" outgoing="_GvWxAPZ1Ed-i2JdGlqLvVQ _I70BgPZ1Ed-i2JdGlqLvVQ"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create true literal" uuid="_HpihUPZ1Ed-i2JdGlqLvVQ" incoming="_I70BgPZ1Ed-i2JdGlqLvVQ" outgoing="_tKidgPZ1Ed-i2JdGlqLvVQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_eT-48PZ1Ed-i2JdGlqLvVQ" outgoingStoryLinks="_l3WjwPZ1Ed-i2JdGlqLvVQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="trueLiteral" uuid="_fBp3QPZ1Ed-i2JdGlqLvVQ" modifier="CREATE" incomingStoryLinks="_l3WjwPZ1Ed-i2JdGlqLvVQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction"/>
        <attributeAssignments uuid="_m1C7EPZ1Ed-i2JdGlqLvVQ">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction/literal"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ocUa4PZ1Ed-i2JdGlqLvVQ" expressionString="'true'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_qH6aAPZ1Ed-i2JdGlqLvVQ">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qwvUkPZ1Ed-i2JdGlqLvVQ" expressionString="ecore::EBoolean" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_l3WjwPZ1Ed-i2JdGlqLvVQ" modifier="CREATE" source="_eT-48PZ1Ed-i2JdGlqLvVQ" target="_fBp3QPZ1Ed-i2JdGlqLvVQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_s2L-IPZ1Ed-i2JdGlqLvVQ" incoming="_tKidgPZ1Ed-i2JdGlqLvVQ">
      <outParameterValues xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tooHsPZ1Ed-i2JdGlqLvVQ" expressionString="cae" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set new UUIDs" uuid="_41DvACoqEeCjTcyrv1LVTA" incoming="_1AY04PZ0Ed-i2JdGlqLvVQ" outgoing="_JzlssCorEeCjTcyrv1LVTA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_815uQCoqEeCjTcyrv1LVTA" outgoingStoryLinks="_GeDUQCorEeCjTcyrv1LVTA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="element" uuid="_9NZoQCoqEeCjTcyrv1LVTA" incomingStoryLinks="_GeDUQCorEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement"/>
        <attributeAssignments uuid="_BMEdQCorEeCjTcyrv1LVTA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/uuid"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_COh_0CorEeCjTcyrv1LVTA">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_CyaI8CorEeCjTcyrv1LVTA" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="generateUUID">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_GeDUQCorEeCjTcyrv1LVTA" source="_815uQCoqEeCjTcyrv1LVTA" target="_9NZoQCoqEeCjTcyrv1LVTA"/>
    </nodes>
    <edges uuid="_MgT_0PZxEd-i2JdGlqLvVQ" source="_zcNpMPZwEd-i2JdGlqLvVQ" target="_0Bv_MPZwEd-i2JdGlqLvVQ"/>
    <edges uuid="_PEFkIPZxEd-i2JdGlqLvVQ" source="_0Bv_MPZwEd-i2JdGlqLvVQ" target="_M6c-IPZxEd-i2JdGlqLvVQ"/>
    <edges uuid="_rgaa0PZxEd-i2JdGlqLvVQ" source="_M6c-IPZxEd-i2JdGlqLvVQ" target="_F4I1IPZ1Ed-i2JdGlqLvVQ"/>
    <edges uuid="_0TXBsPZyEd-i2JdGlqLvVQ" source="_WmYQ4PZxEd-i2JdGlqLvVQ" target="_z0IvEPZyEd-i2JdGlqLvVQ" guardType="FOR_EACH"/>
    <edges uuid="_51gboPZyEd-i2JdGlqLvVQ" source="_z0IvEPZyEd-i2JdGlqLvVQ" target="_1eibYPZyEd-i2JdGlqLvVQ" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7SMcEPZyEd-i2JdGlqLvVQ" expressionString="direction = mote::TransformationDirection::FORWARD or&#xA;direction = mote::TransformationDirection::MAPPING" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_6Q264PZyEd-i2JdGlqLvVQ" source="_z0IvEPZyEd-i2JdGlqLvVQ" target="_3LK8UPZyEd-i2JdGlqLvVQ" guardType="ELSE"/>
    <edges uuid="_kjNQwPZ0Ed-i2JdGlqLvVQ" source="_1eibYPZyEd-i2JdGlqLvVQ" target="_j0oTgPZ0Ed-i2JdGlqLvVQ"/>
    <edges uuid="_k3QOIPZ0Ed-i2JdGlqLvVQ" source="_3LK8UPZyEd-i2JdGlqLvVQ" target="_j0oTgPZ0Ed-i2JdGlqLvVQ"/>
    <edges uuid="_lQXRsPZ0Ed-i2JdGlqLvVQ" source="_j0oTgPZ0Ed-i2JdGlqLvVQ" target="_WmYQ4PZxEd-i2JdGlqLvVQ"/>
    <edges uuid="_1AY04PZ0Ed-i2JdGlqLvVQ" source="_WmYQ4PZxEd-i2JdGlqLvVQ" target="_41DvACoqEeCjTcyrv1LVTA" guardType="END"/>
    <edges uuid="_GvWxAPZ1Ed-i2JdGlqLvVQ" source="_F4I1IPZ1Ed-i2JdGlqLvVQ" target="_WmYQ4PZxEd-i2JdGlqLvVQ" guardType="ELSE"/>
    <edges uuid="_I70BgPZ1Ed-i2JdGlqLvVQ" source="_F4I1IPZ1Ed-i2JdGlqLvVQ" target="_HpihUPZ1Ed-i2JdGlqLvVQ" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JhNNkPZ1Ed-i2JdGlqLvVQ" expressionString="tggRule.ruleVariables->isEmpty()" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_tKidgPZ1Ed-i2JdGlqLvVQ" source="_HpihUPZ1Ed-i2JdGlqLvVQ" target="_s2L-IPZ1Ed-i2JdGlqLvVQ"/>
    <edges uuid="_JzlssCorEeCjTcyrv1LVTA" source="_41DvACoqEeCjTcyrv1LVTA" target="_0g3AQPZ0Ed-i2JdGlqLvVQ" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
