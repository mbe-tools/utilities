<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_match_createdElements_links" uuid="_md9-YBjQEeCuQ-PF2tTDdw">
  <activities name="create_match_createdElements_links" uuid="_oE-YcBjQEeCuQ-PF2tTDdw">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_qNkcoBjQEeCuQ-PF2tTDdw" outgoing="_5rLksBjREeCuQ-PF2tTDdw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_qq8VIBjQEeCuQ-PF2tTDdw" incoming="_5rLksBjREeCuQ-PF2tTDdw" outgoing="_6ifAIBjREeCuQ-PF2tTDdw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchSpo" uuid="_rzG3EBjQEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_s89bEBjQEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_tdTvEBjQEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_uStGABjQEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchEClass" uuid="_B6m5sBjREeCuQ-PF2tTDdw" outgoingStoryLinks="_Lat-ABjREeCuQ-PF2tTDdw _olzPIFPMEeCP5oWyIYxF2w _o_W-oFPMEeCP5oWyIYxF2w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_I__oABjREeCuQ-PF2tTDdw" expressionString="mote::rules::Match" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sceEReference" uuid="_ClmXcBjREeCuQ-PF2tTDdw" incomingStoryLinks="_Lat-ABjREeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_dkxowBjTEeCuQ-PF2tTDdw" expressionString="self.name = 'sourceCreatedElements'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_GTMqkBjREeCuQ-PF2tTDdw" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cceEReference" uuid="_kxvusVPMEeCP5oWyIYxF2w" incomingStoryLinks="_olzPIFPMEeCP5oWyIYxF2w">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kxvuslPMEeCP5oWyIYxF2w" expressionString="self.name = 'corrCreatedElements'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tceEReference" uuid="_mY4rolPMEeCP5oWyIYxF2w" incomingStoryLinks="_o_W-oFPMEeCP5oWyIYxF2w">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_mY4ro1PMEeCP5oWyIYxF2w" expressionString="self.name = 'targetCreatedElements'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Lat-ABjREeCuQ-PF2tTDdw" source="_B6m5sBjREeCuQ-PF2tTDdw" target="_ClmXcBjREeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_olzPIFPMEeCP5oWyIYxF2w" source="_B6m5sBjREeCuQ-PF2tTDdw" target="_kxvusVPMEeCP5oWyIYxF2w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_o_W-oFPMEeCP5oWyIYxF2w" source="_B6m5sBjREeCuQ-PF2tTDdw" target="_mY4rolPMEeCP5oWyIYxF2w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="iterate through created model objects" uuid="_29nnsBjQEeCuQ-PF2tTDdw" incoming="_6ifAIBjREeCuQ-PF2tTDdw __0HI0FPMEeCP5oWyIYxF2w" outgoing="_8L_7oBjREeCuQ-PF2tTDdw _366X8FPMEeCP5oWyIYxF2w" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_7hyWcBjQEeCuQ-PF2tTDdw" outgoingStoryLinks="_lelvkBjREeCuQ-PF2tTDdw _qRMi8BjREeCuQ-PF2tTDdw _qnKy8BjREeCuQ-PF2tTDdw _AB-xwC3mEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_8POrQBjQEeCuQ-PF2tTDdw" outgoingStoryLinks="_VzcFUBjREeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelDomain" uuid="_8y71QBjQEeCuQ-PF2tTDdw" outgoingStoryLinks="_WvjvUBjREeCuQ-PF2tTDdw" incomingStoryLinks="_VzcFUBjREeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_9XyOwBjQEeCuQ-PF2tTDdw" incomingStoryLinks="_WvjvUBjREeCuQ-PF2tTDdw _iTDqIClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZsD8sBjREeCuQ-PF2tTDdw" expressionString="self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_-U0ewBjQEeCuQ-PF2tTDdw" incomingStoryLinks="_qnKy8BjREeCuQ-PF2tTDdw _u3_H4BjREeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchSpo" uuid="__BtZ8BjQEeCuQ-PF2tTDdw" incomingStoryLinks="_lelvkBjREeCuQ-PF2tTDdw _uhBZYBjREeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="acSpl" uuid="_ASnoMBjREeCuQ-PF2tTDdw" modifier="CREATE" outgoingStoryLinks="_uhBZYBjREeCuQ-PF2tTDdw _u3_H4BjREeCuQ-PF2tTDdw _vnNlYBjREeCuQ-PF2tTDdw _GWxwoC3mEeCSbYYn7Hqbqw _c1Qv4C3mEeCSbYYn7Hqbqw" incomingStoryLinks="_qRMi8BjREeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_A_QEsBjREeCuQ-PF2tTDdw" incomingStoryLinks="_vnNlYBjREeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_meRq8BjREeCuQ-PF2tTDdw" outgoingStoryLinks="_iTDqIClYEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="nameSpo" uuid="_1DPMQC3lEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_TG3X8C3mEeCSbYYn7Hqbqw _WTym8C3mEeCSbYYn7Hqbqw" incomingStoryLinks="_AB-xwC3mEeCSbYYn7Hqbqw _GWxwoC3mEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_3OjNQC3lEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5Rac4C3lEeCSbYYn7Hqbqw" expressionString="modelObject.name.concat('String')" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_8GqywC3lEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8kunwC3lEeCSbYYn7Hqbqw" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_AyfBoC3mEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_VABR8C3mEeCSbYYn7Hqbqw" incomingStoryLinks="_TG3X8C3mEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="literal" uuid="_BH7GIC3mEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_Vor0cC3mEeCSbYYn7Hqbqw" incomingStoryLinks="_VABR8C3mEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction"/>
        <attributeAssignments uuid="_MraXcC3mEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction/literal"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OwRLIC3mEeCSbYYn7Hqbqw" expressionString="modelObject.name" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eStringEDataType" uuid="_BuwcoC3mEeCSbYYn7Hqbqw" incomingStoryLinks="_Vor0cC3mEeCSbYYn7Hqbqw _WTym8C3mEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EDataType"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_RijskC3mEeCSbYYn7Hqbqw" expressionString="ecore::EString" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mapEntryEClass" uuid="_CzLuEC3mEeCSbYYn7Hqbqw" incomingStoryLinks="_c1Qv4C3mEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Yx3y8C3mEeCSbYYn7Hqbqw" expressionString="mote::helpers::MapEntry" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_VzcFUBjREeCuQ-PF2tTDdw" source="_8POrQBjQEeCuQ-PF2tTDdw" target="_8y71QBjQEeCuQ-PF2tTDdw"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WvjvUBjREeCuQ-PF2tTDdw" source="_8y71QBjQEeCuQ-PF2tTDdw" target="_9XyOwBjQEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lelvkBjREeCuQ-PF2tTDdw" source="_7hyWcBjQEeCuQ-PF2tTDdw" target="__BtZ8BjQEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qRMi8BjREeCuQ-PF2tTDdw" modifier="CREATE" source="_7hyWcBjQEeCuQ-PF2tTDdw" target="_ASnoMBjREeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qnKy8BjREeCuQ-PF2tTDdw" source="_7hyWcBjQEeCuQ-PF2tTDdw" target="_-U0ewBjQEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_uhBZYBjREeCuQ-PF2tTDdw" modifier="CREATE" source="_ASnoMBjREeCuQ-PF2tTDdw" target="__BtZ8BjQEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_u3_H4BjREeCuQ-PF2tTDdw" modifier="CREATE" source="_ASnoMBjREeCuQ-PF2tTDdw" target="_-U0ewBjQEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/valueTarget"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_vnNlYBjREeCuQ-PF2tTDdw" modifier="CREATE" source="_ASnoMBjREeCuQ-PF2tTDdw" target="_A_QEsBjREeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_iTDqIClYEeCFgb_w6FqGaA" source="_meRq8BjREeCuQ-PF2tTDdw" target="_9XyOwBjQEeCuQ-PF2tTDdw" valueTarget="_-U0ewBjQEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_AB-xwC3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_7hyWcBjQEeCuQ-PF2tTDdw" target="_1DPMQC3lEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GWxwoC3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_ASnoMBjREeCuQ-PF2tTDdw" target="_1DPMQC3lEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_TG3X8C3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_1DPMQC3lEeCSbYYn7Hqbqw" target="_AyfBoC3mEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_VABR8C3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_AyfBoC3mEeCSbYYn7Hqbqw" target="_BH7GIC3mEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Vor0cC3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_BH7GIC3mEeCSbYYn7Hqbqw" target="_BuwcoC3mEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WTym8C3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_1DPMQC3lEeCSbYYn7Hqbqw" target="_BuwcoC3mEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_c1Qv4C3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_ASnoMBjREeCuQ-PF2tTDdw" target="_CzLuEC3mEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/classifier"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_69LYEBjREeCuQ-PF2tTDdw" incoming="_ychJIBjSEeCuQ-PF2tTDdw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="iterate through created correspondence nodes" uuid="_iL4zQBjSEeCuQ-PF2tTDdw" incoming="_8L_7oBjREeCuQ-PF2tTDdw" outgoing="_ychJIBjSEeCuQ-PF2tTDdw" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_iL6ocRjSEeCuQ-PF2tTDdw" outgoingStoryLinks="_iL6BYxjSEeCuQ-PF2tTDdw _iL5aVRjSEeCuQ-PF2tTDdw _iL6ocBjSEeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_iL6BYRjSEeCuQ-PF2tTDdw" outgoingStoryLinks="_tKxisBjSEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_iL5aVhjSEeCuQ-PF2tTDdw" outgoingStoryLinks="_iL6BZBjSEeCuQ-PF2tTDdw" incomingStoryLinks="_tKxisBjSEeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNode" uuid="_iL6odhjSEeCuQ-PF2tTDdw" incomingStoryLinks="_iL6BZBjSEeCuQ-PF2tTDdw _lyvdEClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_iL6odxjSEeCuQ-PF2tTDdw" expressionString="self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_iL5aVBjSEeCuQ-PF2tTDdw" incomingStoryLinks="_iL6ocBjSEeCuQ-PF2tTDdw _iL5aUhjSEeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchSpo" uuid="_iL5aUxjSEeCuQ-PF2tTDdw" incomingStoryLinks="_iL6BYxjSEeCuQ-PF2tTDdw _iL6BYBjSEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="acSpl" uuid="_iL6BaBjSEeCuQ-PF2tTDdw" modifier="CREATE" outgoingStoryLinks="_iL6BYBjSEeCuQ-PF2tTDdw _iL5aUhjSEeCuQ-PF2tTDdw _iL6BZxjSEeCuQ-PF2tTDdw _iL5aURjSEeCuQ-PF2tTDdw _qI03sC3mEeCSbYYn7Hqbqw _vEv_IC3mEeCSbYYn7Hqbqw" incomingStoryLinks="_iL5aVRjSEeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_iL6odRjSEeCuQ-PF2tTDdw" incomingStoryLinks="_iL5aURjSEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cceEReference" uuid="_iL6BZhjSEeCuQ-PF2tTDdw" incomingStoryLinks="_iL6BZxjSEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_iL5aUBjSEeCuQ-PF2tTDdw" outgoingStoryLinks="_lyvdEClYEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mapEntryEClass" uuid="_kbx1UC3mEeCSbYYn7Hqbqw" incomingStoryLinks="_vEv_IC3mEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kbx1US3mEeCSbYYn7Hqbqw" expressionString="mote::helpers::MapEntry" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eStringEDataType" uuid="_kbx1Ui3mEeCSbYYn7Hqbqw" incomingStoryLinks="_s7CgoC3mEeCSbYYn7Hqbqw _t8bFIC3mEeCSbYYn7Hqbqw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EDataType"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kbx1Uy3mEeCSbYYn7Hqbqw" expressionString="ecore::EString" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="literal" uuid="_kbx1VC3mEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_s7CgoC3mEeCSbYYn7Hqbqw" incomingStoryLinks="_sUbzoC3mEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction"/>
        <attributeAssignments uuid="_kbx1VS3mEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction/literal"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kbx1Vi3mEeCSbYYn7Hqbqw" expressionString="corrNode.name" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_kbx1Vy3mEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_sUbzoC3mEeCSbYYn7Hqbqw" incomingStoryLinks="_rPoHsC3mEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="nameSpo" uuid="_kbx1WC3mEeCSbYYn7Hqbqw" modifier="CREATE" outgoingStoryLinks="_rPoHsC3mEeCSbYYn7Hqbqw _t8bFIC3mEeCSbYYn7Hqbqw" incomingStoryLinks="_qI03sC3mEeCSbYYn7Hqbqw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_kbx1WS3mEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kbx1Wi3mEeCSbYYn7Hqbqw" expressionString="corrNode.name.concat('String')" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_kbx1Wy3mEeCSbYYn7Hqbqw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kbx1XC3mEeCSbYYn7Hqbqw" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL6BZBjSEeCuQ-PF2tTDdw" source="_iL5aVhjSEeCuQ-PF2tTDdw" target="_iL6odhjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL6BYxjSEeCuQ-PF2tTDdw" source="_iL6ocRjSEeCuQ-PF2tTDdw" target="_iL5aUxjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL5aVRjSEeCuQ-PF2tTDdw" modifier="CREATE" source="_iL6ocRjSEeCuQ-PF2tTDdw" target="_iL6BaBjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL6ocBjSEeCuQ-PF2tTDdw" source="_iL6ocRjSEeCuQ-PF2tTDdw" target="_iL5aVBjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL6BYBjSEeCuQ-PF2tTDdw" modifier="CREATE" source="_iL6BaBjSEeCuQ-PF2tTDdw" target="_iL5aUxjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL5aUhjSEeCuQ-PF2tTDdw" modifier="CREATE" source="_iL6BaBjSEeCuQ-PF2tTDdw" target="_iL5aVBjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/valueTarget"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL6BZxjSEeCuQ-PF2tTDdw" modifier="CREATE" source="_iL6BaBjSEeCuQ-PF2tTDdw" target="_iL6BZhjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iL5aURjSEeCuQ-PF2tTDdw" modifier="CREATE" source="_iL6BaBjSEeCuQ-PF2tTDdw" target="_iL6odRjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_tKxisBjSEeCuQ-PF2tTDdw" source="_iL6BYRjSEeCuQ-PF2tTDdw" target="_iL5aVhjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_lyvdEClYEeCFgb_w6FqGaA" source="_iL5aUBjSEeCuQ-PF2tTDdw" target="_iL6odhjSEeCuQ-PF2tTDdw" valueTarget="_iL5aVBjSEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qI03sC3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_iL6BaBjSEeCuQ-PF2tTDdw" target="_kbx1WC3mEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_rPoHsC3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_kbx1WC3mEeCSbYYn7Hqbqw" target="_kbx1Vy3mEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/directAssignmentExpression"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_sUbzoC3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_kbx1Vy3mEeCSbYYn7Hqbqw" target="_kbx1VC3mEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_s7CgoC3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_kbx1VC3mEeCSbYYn7Hqbqw" target="_kbx1Ui3mEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_t8bFIC3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_kbx1WC3mEeCSbYYn7Hqbqw" target="_kbx1Ui3mEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_vEv_IC3mEeCSbYYn7Hqbqw" modifier="CREATE" source="_iL6BaBjSEeCuQ-PF2tTDdw" target="_kbx1UC3mEeCSbYYn7Hqbqw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/classifier"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_rom40FPMEeCP5oWyIYxF2w" incoming="_366X8FPMEeCP5oWyIYxF2w" outgoing="_43QEYFPMEeCP5oWyIYxF2w _9QKlYFPMEeCP5oWyIYxF2w"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_r45XkFPMEeCP5oWyIYxF2w" incoming="_-8FUoFPMEeCP5oWyIYxF2w __Py60FPMEeCP5oWyIYxF2w" outgoing="__0HI0FPMEeCP5oWyIYxF2w"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set EReference for source" uuid="_sJD6gFPMEeCP5oWyIYxF2w" incoming="_43QEYFPMEeCP5oWyIYxF2w" outgoing="_-8FUoFPMEeCP5oWyIYxF2w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sceEReference" uuid="_udrogFPMEeCP5oWyIYxF2w" incomingStoryLinks="_0sFEcFPMEeCP5oWyIYxF2w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="acSpl" uuid="_x5BG8FPMEeCP5oWyIYxF2w" outgoingStoryLinks="_0sFEcFPMEeCP5oWyIYxF2w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0sFEcFPMEeCP5oWyIYxF2w" modifier="CREATE" source="_x5BG8FPMEeCP5oWyIYxF2w" target="_udrogFPMEeCP5oWyIYxF2w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set EReference for target" uuid="_tMMygFPMEeCP5oWyIYxF2w" incoming="_9QKlYFPMEeCP5oWyIYxF2w" outgoing="__Py60FPMEeCP5oWyIYxF2w">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tceEReference" uuid="_1RZYAVPMEeCP5oWyIYxF2w" incomingStoryLinks="_2NlTcFPMEeCP5oWyIYxF2w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="acSpl" uuid="_1RZYAlPMEeCP5oWyIYxF2w" outgoingStoryLinks="_2NlTcFPMEeCP5oWyIYxF2w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2NlTcFPMEeCP5oWyIYxF2w" modifier="CREATE" source="_1RZYAlPMEeCP5oWyIYxF2w" target="_1RZYAVPMEeCP5oWyIYxF2w">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_5rLksBjREeCuQ-PF2tTDdw" source="_qNkcoBjQEeCuQ-PF2tTDdw" target="_qq8VIBjQEeCuQ-PF2tTDdw"/>
    <edges uuid="_6ifAIBjREeCuQ-PF2tTDdw" source="_qq8VIBjQEeCuQ-PF2tTDdw" target="_29nnsBjQEeCuQ-PF2tTDdw"/>
    <edges uuid="_8L_7oBjREeCuQ-PF2tTDdw" source="_29nnsBjQEeCuQ-PF2tTDdw" target="_iL4zQBjSEeCuQ-PF2tTDdw" guardType="END"/>
    <edges uuid="_ychJIBjSEeCuQ-PF2tTDdw" source="_iL4zQBjSEeCuQ-PF2tTDdw" target="_69LYEBjREeCuQ-PF2tTDdw" guardType="END"/>
    <edges uuid="_366X8FPMEeCP5oWyIYxF2w" source="_29nnsBjQEeCuQ-PF2tTDdw" target="_rom40FPMEeCP5oWyIYxF2w" guardType="FOR_EACH"/>
    <edges uuid="_43QEYFPMEeCP5oWyIYxF2w" source="_rom40FPMEeCP5oWyIYxF2w" target="_sJD6gFPMEeCP5oWyIYxF2w" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5chUoFPMEeCP5oWyIYxF2w" expressionString="modelDomain.oclIsKindOf(tgg::SourceModelDomain)" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_9QKlYFPMEeCP5oWyIYxF2w" source="_rom40FPMEeCP5oWyIYxF2w" target="_tMMygFPMEeCP5oWyIYxF2w" guardType="ELSE"/>
    <edges uuid="_-8FUoFPMEeCP5oWyIYxF2w" source="_sJD6gFPMEeCP5oWyIYxF2w" target="_r45XkFPMEeCP5oWyIYxF2w"/>
    <edges uuid="__Py60FPMEeCP5oWyIYxF2w" source="_tMMygFPMEeCP5oWyIYxF2w" target="_r45XkFPMEeCP5oWyIYxF2w"/>
    <edges uuid="__0HI0FPMEeCP5oWyIYxF2w" source="_r45XkFPMEeCP5oWyIYxF2w" target="_29nnsBjQEeCuQ-PF2tTDdw"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
