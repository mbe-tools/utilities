<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_rule_createdCorrNodes_link" uuid="_Lva9gRQBEeCDO6mN3ETDMg">
  <activities name="create_rule_createdCorrNodes_link" uuid="_NdRmcBQBEeCDO6mN3ETDMg">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_QYyVUBQBEeCDO6mN3ETDMg" outgoing="_Cvn7gBQCEeCDO6mN3ETDMg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_Q3H3IBQBEeCDO6mN3ETDMg" incoming="_Cvn7gBQCEeCDO6mN3ETDMg" outgoing="_DL3LkBQCEeCDO6mN3ETDMg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSpo" uuid="_RgPFkBQBEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_Tx7bYBQBEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggMappingEClass" uuid="_bTjZwBQBEeCDO6mN3ETDMg" outgoingStoryLinks="_l5SCUBQBEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_d-_FQBQBEeCDO6mN3ETDMg" expressionString="mote::rules::TGGMapping" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_hJt74BQBEeCDO6mN3ETDMg" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createdCorrNodesEReference" uuid="_ivHzcBQBEeCDO6mN3ETDMg" incomingStoryLinks="_l5SCUBQBEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_nd0-QBQBEeCDO6mN3ETDMg" expressionString="self.name = 'createdCorrNodes'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_Ca4UYBQfEeCkcYaoegFQZw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_DA3WUBQfEeCkcYaoegFQZw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_l5SCUBQBEeCDO6mN3ETDMg" source="_bTjZwBQBEeCDO6mN3ETDMg" target="_ivHzcBQBEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create link" uuid="_vOZ-oBQBEeCDO6mN3ETDMg" incoming="_DL3LkBQCEeCDO6mN3ETDMg" outgoing="_EfTwcBQCEeCDO6mN3ETDMg" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSpo" uuid="_wPJp8BQBEeCDO6mN3ETDMg" incomingStoryLinks="_7jSQoBQBEeCDO6mN3ETDMg _9SRiABQBEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodeSpo" uuid="_wxapgBQBEeCDO6mN3ETDMg" incomingStoryLinks="_8OP5MBQBEeCDO6mN3ETDMg _9lyT4BQBEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_xZbEsBQBEeCDO6mN3ETDMg" outgoingStoryLinks="_7jSQoBQBEeCDO6mN3ETDMg _72oqcBQBEeCDO6mN3ETDMg _8OP5MBQBEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_yZy8kBQBEeCDO6mN3ETDMg" incomingStoryLinks="_8-rQkBQBEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createdCorrNodesEReference" uuid="_y_kjIBQBEeCDO6mN3ETDMg" incomingStoryLinks="_8pqp4BQBEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spl" uuid="_1K-DsBQBEeCDO6mN3ETDMg" modifier="CREATE" outgoingStoryLinks="_8pqp4BQBEeCDO6mN3ETDMg _8-rQkBQBEeCDO6mN3ETDMg _9SRiABQBEeCDO6mN3ETDMg _9lyT4BQBEeCDO6mN3ETDMg" incomingStoryLinks="_72oqcBQBEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_LUGX0BQfEeCkcYaoegFQZw" outgoingStoryLinks="_YpFb8BQfEeCkcYaoegFQZw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_MAkcQBQfEeCkcYaoegFQZw" outgoingStoryLinks="_bMhFQClYEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_N6ADEBQfEeCkcYaoegFQZw" outgoingStoryLinks="_Y8PBcBQfEeCkcYaoegFQZw" incomingStoryLinks="_YpFb8BQfEeCkcYaoegFQZw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNode" uuid="_OevH0BQfEeCkcYaoegFQZw" incomingStoryLinks="_Y8PBcBQfEeCkcYaoegFQZw _bMhFQClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_U1Yg0BQfEeCkcYaoegFQZw" expressionString="self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7jSQoBQBEeCDO6mN3ETDMg" source="_xZbEsBQBEeCDO6mN3ETDMg" target="_wPJp8BQBEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_72oqcBQBEeCDO6mN3ETDMg" modifier="CREATE" source="_xZbEsBQBEeCDO6mN3ETDMg" target="_1K-DsBQBEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8OP5MBQBEeCDO6mN3ETDMg" source="_xZbEsBQBEeCDO6mN3ETDMg" target="_wxapgBQBEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8pqp4BQBEeCDO6mN3ETDMg" modifier="CREATE" source="_1K-DsBQBEeCDO6mN3ETDMg" target="_y_kjIBQBEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8-rQkBQBEeCDO6mN3ETDMg" modifier="CREATE" source="_1K-DsBQBEeCDO6mN3ETDMg" target="_yZy8kBQBEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9SRiABQBEeCDO6mN3ETDMg" modifier="CREATE" source="_1K-DsBQBEeCDO6mN3ETDMg" target="_wPJp8BQBEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9lyT4BQBEeCDO6mN3ETDMg" modifier="CREATE" source="_1K-DsBQBEeCDO6mN3ETDMg" target="_wxapgBQBEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_YpFb8BQfEeCkcYaoegFQZw" source="_LUGX0BQfEeCkcYaoegFQZw" target="_N6ADEBQfEeCkcYaoegFQZw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Y8PBcBQfEeCkcYaoegFQZw" source="_N6ADEBQfEeCkcYaoegFQZw" target="_OevH0BQfEeCkcYaoegFQZw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_bMhFQClYEeCFgb_w6FqGaA" source="_MAkcQBQfEeCkcYaoegFQZw" target="_OevH0BQfEeCkcYaoegFQZw" valueTarget="_wxapgBQBEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_Dj_XsBQCEeCDO6mN3ETDMg" incoming="_EfTwcBQCEeCDO6mN3ETDMg"/>
    <edges uuid="_Cvn7gBQCEeCDO6mN3ETDMg" source="_QYyVUBQBEeCDO6mN3ETDMg" target="_Q3H3IBQBEeCDO6mN3ETDMg"/>
    <edges uuid="_DL3LkBQCEeCDO6mN3ETDMg" source="_Q3H3IBQBEeCDO6mN3ETDMg" target="_vOZ-oBQBEeCDO6mN3ETDMg"/>
    <edges uuid="_EfTwcBQCEeCDO6mN3ETDMg" source="_vOZ-oBQBEeCDO6mN3ETDMg" target="_Dj_XsBQCEeCDO6mN3ETDMg" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
