<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" uuid="_kb89gPcJEd-_yp3IZC6S-Q">
  <activities name="create_addModifcationTagToQueue_expression.story" uuid="_lXQIIPcJEd-_yp3IZC6S-Q">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_n9RIIPcJEd-_yp3IZC6S-Q" outgoing="_uG47UPcVEd-PZbH3UvWGmw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_orfgEPcJEd-_yp3IZC6S-Q" incoming="_uG47UPcVEd-PZbH3UvWGmw" outgoing="_1cTpsPcVEd-PZbH3UvWGmw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_o8U68PcVEd-PZbH3UvWGmw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_w_p4MPcVEd-PZbH3UvWGmw" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_bJiEAPcWEd-PZbH3UvWGmw" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleSetEClass" uuid="_UyCrkPcYEd-PZbH3UvWGmw" outgoingStoryLinks="_cSOfgPcYEd-PZbH3UvWGmw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_YE6KgPcYEd-PZbH3UvWGmw" expressionString="mote::rules::TGGRuleSet" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="addModTagOperation" uuid="_aOftMPcYEd-PZbH3UvWGmw" incomingStoryLinks="_cSOfgPcYEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fPCO4PcYEd-PZbH3UvWGmw" expressionString="self.name = 'addModificationTagToQueue'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_cSOfgPcYEd-PZbH3UvWGmw" source="_UyCrkPcYEd-PZbH3UvWGmw" target="_aOftMPcYEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eOperations"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create method call action for each created correspondence node" uuid="_u5A70PcVEd-PZbH3UvWGmw" incoming="_1cTpsPcVEd-PZbH3UvWGmw _wZNRUPcXEd-PZbH3UvWGmw" outgoing="_vPHcwPcXEd-PZbH3UvWGmw _yBGcMPcXEd-PZbH3UvWGmw" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_2qpsIPcVEd-PZbH3UvWGmw" outgoingStoryLinks="_OTSKUPcWEd-PZbH3UvWGmw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_3RRnQPcVEd-PZbH3UvWGmw" outgoingStoryLinks="_Ol-c0PcWEd-PZbH3UvWGmw" incomingStoryLinks="_OTSKUPcWEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNode" uuid="_313iEPcVEd-PZbH3UvWGmw" incomingStoryLinks="_Ol-c0PcWEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_S4m2AAg1EeC3CKyH_xgb5w" expressionString="self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="methodCallAction" uuid="_44UdkPcVEd-PZbH3UvWGmw" modifier="CREATE" outgoingStoryLinks="_WuMhIPcWEd-PZbH3UvWGmw _JylJ0PcXEd-PZbH3UvWGmw _kZPC4PcYEd-PZbH3UvWGmw" incomingStoryLinks="_O7Vo0PcWEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="instanceCae" uuid="_55tCEPcVEd-PZbH3UvWGmw" modifier="CREATE" outgoingStoryLinks="_XJR6oPcWEd-PZbH3UvWGmw" incomingStoryLinks="_WuMhIPcWEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="instanceVarRef" uuid="_7QDxAPcVEd-PZbH3UvWGmw" modifier="CREATE" incomingStoryLinks="_XJR6oPcWEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_XjNdkPcWEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Yj-XAPcWEd-PZbH3UvWGmw" expressionString="'ruleSet'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_f3V28PcWEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_g7xIYPcWEd-PZbH3UvWGmw" expressionString="mote::rules::TGGRuleSet" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_JySEUPcWEd-PZbH3UvWGmw" outgoingStoryLinks="_O7Vo0PcWEd-PZbH3UvWGmw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodeParameter" uuid="_D19aYPcXEd-PZbH3UvWGmw" modifier="CREATE" outgoingStoryLinks="_LD1WUPcXEd-PZbH3UvWGmw" incomingStoryLinks="_JylJ0PcXEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_sQD1MPcYEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_tJzgoPcYEd-PZbH3UvWGmw" expressionString="mote::TGGNode" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodeParameterCae" uuid="_EzmuYPcXEd-PZbH3UvWGmw" modifier="CREATE" outgoingStoryLinks="_NxDxkPcXEd-PZbH3UvWGmw" incomingStoryLinks="_LD1WUPcXEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodeReference" uuid="_GsN10PcXEd-PZbH3UvWGmw" modifier="CREATE" incomingStoryLinks="_NxDxkPcXEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_OsqeMPcXEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_P2NgMPcXEd-PZbH3UvWGmw" expressionString="corrNode.name" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_TJnKoPcXEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_UW5dIPcXEd-PZbH3UvWGmw" expressionString="corrNode.classifier" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="addModTagOperation" uuid="_icQ_4PcYEd-PZbH3UvWGmw" incomingStoryLinks="_kZPC4PcYEd-PZbH3UvWGmw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EOperation"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_OTSKUPcWEd-PZbH3UvWGmw" source="_2qpsIPcVEd-PZbH3UvWGmw" target="_3RRnQPcVEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ol-c0PcWEd-PZbH3UvWGmw" source="_3RRnQPcVEd-PZbH3UvWGmw" target="_313iEPcVEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_O7Vo0PcWEd-PZbH3UvWGmw" modifier="CREATE" source="_JySEUPcWEd-PZbH3UvWGmw" target="_44UdkPcVEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WuMhIPcWEd-PZbH3UvWGmw" modifier="CREATE" source="_44UdkPcVEd-PZbH3UvWGmw" target="_55tCEPcVEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction/instanceVariable"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XJR6oPcWEd-PZbH3UvWGmw" modifier="CREATE" source="_55tCEPcVEd-PZbH3UvWGmw" target="_7QDxAPcVEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_JylJ0PcXEd-PZbH3UvWGmw" modifier="CREATE" source="_44UdkPcVEd-PZbH3UvWGmw" target="_D19aYPcXEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LD1WUPcXEd-PZbH3UvWGmw" modifier="CREATE" source="_D19aYPcXEd-PZbH3UvWGmw" target="_EzmuYPcXEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_NxDxkPcXEd-PZbH3UvWGmw" modifier="CREATE" source="_EzmuYPcXEd-PZbH3UvWGmw" target="_GsN10PcXEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kZPC4PcYEd-PZbH3UvWGmw" modifier="CREATE" source="_44UdkPcVEd-PZbH3UvWGmw" target="_icQ_4PcYEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction/method"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create transform parameter" uuid="_l8GOsPcWEd-PZbH3UvWGmw" incoming="_vPHcwPcXEd-PZbH3UvWGmw" outgoing="_vsy3QPcXEd-PZbH3UvWGmw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformParameter" uuid="_8QGRsPcVEd-PZbH3UvWGmw" modifier="CREATE" outgoingStoryLinks="_3MNroPcWEd-PZbH3UvWGmw" incomingStoryLinks="_y5LwMPcWEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_z39soPcWEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_03M8EPcWEd-PZbH3UvWGmw" expressionString="ecore::EBoolean" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformParameterCae" uuid="_9paOAPcVEd-PZbH3UvWGmw" modifier="CREATE" outgoingStoryLinks="_3lnqIPcWEd-PZbH3UvWGmw" incomingStoryLinks="_3MNroPcWEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformLiteral" uuid="__CT6oPcVEd-PZbH3UvWGmw" modifier="CREATE" incomingStoryLinks="_3lnqIPcWEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction"/>
        <attributeAssignments uuid="_4M5skPcWEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction/literal"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_5PWBAPcWEd-PZbH3UvWGmw" expressionString="'true'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_6LdrAPcWEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7F56APcWEd-PZbH3UvWGmw" expressionString="ecore::EBoolean" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="methodCallAction" uuid="_wLieMPcWEd-PZbH3UvWGmw" outgoingStoryLinks="_y5LwMPcWEd-PZbH3UvWGmw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_y5LwMPcWEd-PZbH3UvWGmw" modifier="CREATE" source="_wLieMPcWEd-PZbH3UvWGmw" target="_8QGRsPcVEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3MNroPcWEd-PZbH3UvWGmw" modifier="CREATE" source="_8QGRsPcVEd-PZbH3UvWGmw" target="_9paOAPcVEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3lnqIPcWEd-PZbH3UvWGmw" modifier="CREATE" source="_9paOAPcVEd-PZbH3UvWGmw" target="__CT6oPcVEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create synchronize parameter" uuid="_-FoagPcWEd-PZbH3UvWGmw" incoming="_vsy3QPcXEd-PZbH3UvWGmw" outgoing="_wAKfMPcXEd-PZbH3UvWGmw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="synchronizeParameter" uuid="_A0zL8PcWEd-PZbH3UvWGmw" modifier="CREATE" outgoingStoryLinks="_ZK9pEPcXEd-PZbH3UvWGmw" incomingStoryLinks="_Y6VMEPcXEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_akckgPcXEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_cC9w8PcXEd-PZbH3UvWGmw" expressionString="ecore::EBoolean" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="synchronizeParameterCae" uuid="_BlK48PcWEd-PZbH3UvWGmw" modifier="CREATE" outgoingStoryLinks="_ZbSkEPcXEd-PZbH3UvWGmw" incomingStoryLinks="_ZK9pEPcXEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="synchronizeLiteral" uuid="_CySzYPcWEd-PZbH3UvWGmw" modifier="CREATE" incomingStoryLinks="_ZbSkEPcXEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction"/>
        <attributeAssignments uuid="_eRVF8PcXEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction/literal"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fKeUcPcXEd-PZbH3UvWGmw" expressionString="'false'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_gJgIgPcXEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hD7JYPcXEd-PZbH3UvWGmw" expressionString="ecore::EBoolean" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="methodCallAction" uuid="_W7oqwPcXEd-PZbH3UvWGmw" outgoingStoryLinks="_Y6VMEPcXEd-PZbH3UvWGmw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Y6VMEPcXEd-PZbH3UvWGmw" modifier="CREATE" source="_W7oqwPcXEd-PZbH3UvWGmw" target="_A0zL8PcWEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZK9pEPcXEd-PZbH3UvWGmw" modifier="CREATE" source="_A0zL8PcWEd-PZbH3UvWGmw" target="_BlK48PcWEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZbSkEPcXEd-PZbH3UvWGmw" modifier="CREATE" source="_BlK48PcWEd-PZbH3UvWGmw" target="_CySzYPcWEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create synchronize self parameter" uuid="_AaA38PcXEd-PZbH3UvWGmw" incoming="_wAKfMPcXEd-PZbH3UvWGmw" outgoing="_wZNRUPcXEd-PZbH3UvWGmw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="synchronizeSelfParameter" uuid="_ELtdYPcWEd-PZbH3UvWGmw" modifier="CREATE" outgoingStoryLinks="_lpp-YPcXEd-PZbH3UvWGmw" incomingStoryLinks="_lbUAYPcXEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_nDNyUPcXEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_oD06wPcXEd-PZbH3UvWGmw" expressionString="ecore::EBoolean" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="synchronizeSelfParameterCae" uuid="_Fpeb4PcWEd-PZbH3UvWGmw" modifier="CREATE" outgoingStoryLinks="_l3PHYPcXEd-PZbH3UvWGmw" incomingStoryLinks="_lpp-YPcXEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="synchronizeSelfLiteral" uuid="_HGd-UPcWEd-PZbH3UvWGmw" modifier="CREATE" incomingStoryLinks="_l3PHYPcXEd-PZbH3UvWGmw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction"/>
        <attributeAssignments uuid="_pnKVwPcXEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction/literal"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qeeYQPcXEd-PZbH3UvWGmw" expressionString="'false'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_rqPAwPcXEd-PZbH3UvWGmw">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_snCAMPcXEd-PZbH3UvWGmw" expressionString="ecore::EBoolean" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="methodCallAction" uuid="_jrAgYPcXEd-PZbH3UvWGmw" outgoingStoryLinks="_lbUAYPcXEd-PZbH3UvWGmw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lbUAYPcXEd-PZbH3UvWGmw" modifier="CREATE" source="_jrAgYPcXEd-PZbH3UvWGmw" target="_ELtdYPcWEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_lpp-YPcXEd-PZbH3UvWGmw" modifier="CREATE" source="_ELtdYPcWEd-PZbH3UvWGmw" target="_Fpeb4PcWEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_l3PHYPcXEd-PZbH3UvWGmw" modifier="CREATE" source="_Fpeb4PcWEd-PZbH3UvWGmw" target="_HGd-UPcWEd-PZbH3UvWGmw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_xl2qoPcXEd-PZbH3UvWGmw" incoming="_yBGcMPcXEd-PZbH3UvWGmw">
      <outParameterValues xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_zF0okPcXEd-PZbH3UvWGmw" expressionString="cae" expressionLanguage="OCL"/>
    </nodes>
    <edges uuid="_uG47UPcVEd-PZbH3UvWGmw" source="_n9RIIPcJEd-_yp3IZC6S-Q" target="_orfgEPcJEd-_yp3IZC6S-Q"/>
    <edges uuid="_1cTpsPcVEd-PZbH3UvWGmw" source="_orfgEPcJEd-_yp3IZC6S-Q" target="_u5A70PcVEd-PZbH3UvWGmw"/>
    <edges uuid="_vPHcwPcXEd-PZbH3UvWGmw" source="_u5A70PcVEd-PZbH3UvWGmw" target="_l8GOsPcWEd-PZbH3UvWGmw" guardType="FOR_EACH"/>
    <edges uuid="_vsy3QPcXEd-PZbH3UvWGmw" source="_l8GOsPcWEd-PZbH3UvWGmw" target="_-FoagPcWEd-PZbH3UvWGmw"/>
    <edges uuid="_wAKfMPcXEd-PZbH3UvWGmw" source="_-FoagPcWEd-PZbH3UvWGmw" target="_AaA38PcXEd-PZbH3UvWGmw"/>
    <edges uuid="_wZNRUPcXEd-PZbH3UvWGmw" source="_AaA38PcXEd-PZbH3UvWGmw" target="_u5A70PcVEd-PZbH3UvWGmw"/>
    <edges uuid="_yBGcMPcXEd-PZbH3UvWGmw" source="_u5A70PcVEd-PZbH3UvWGmw" target="_xl2qoPcXEd-PZbH3UvWGmw" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
