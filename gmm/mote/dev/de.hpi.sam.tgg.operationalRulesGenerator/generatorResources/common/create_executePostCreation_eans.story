<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_executePostCreation_eans" uuid="_YlqgQCfCEeCkHOR1O7gzpA">
  <activities name="create_executePostCreation_eans" uuid="_aZJ3ICfCEeCkHOR1O7gzpA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_mYev4CfCEeCkHOR1O7gzpA" outgoing="_7em1ECfCEeCkHOR1O7gzpA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_nUpdMCfCEeCkHOR1O7gzpA" incoming="_7em1ECfCEeCkHOR1O7gzpA" outgoing="_cHBRsCfDEeCkHOR1O7gzpA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_pArhMCfCEeCkHOR1O7gzpA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="direction" uuid="_qipDMCfCEeCkHOR1O7gzpA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//TransformationDirection"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="incomingEdge" uuid="_szmZMCfCEeCkHOR1O7gzpA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="iterate through post creation expressions" uuid="_-Mia8CfCEeCkHOR1O7gzpA" incoming="_cHBRsCfDEeCkHOR1O7gzpA _KJWVwCfEEeCkHOR1O7gzpA" outgoing="_IdFBMCfEEeCkHOR1O7gzpA _L9sBMCfEEeCkHOR1O7gzpA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="__wQQcCfCEeCkHOR1O7gzpA" outgoingStoryLinks="_FyEo8CfDEeCkHOR1O7gzpA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelDomain" uuid="_AtSgcCfDEeCkHOR1O7gzpA" outgoingStoryLinks="_GP078CfDEeCkHOR1O7gzpA" incomingStoryLinks="_FyEo8CfDEeCkHOR1O7gzpA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JPAp4CfDEeCkHOR1O7gzpA" expressionString="direction = mote::TransformationDirection::FORWARD and self.oclIsKindOf(tgg::TargetModelDomain) or&#xA;direction = mote::TransformationDirection::REVERSE and self.oclIsKindOf(tgg::SourceModelDomain)" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_BSI58CfDEeCkHOR1O7gzpA" outgoingStoryLinks="_GuzW8CfDEeCkHOR1O7gzpA" incomingStoryLinks="_GP078CfDEeCkHOR1O7gzpA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="expression" uuid="_BztV8CfDEeCkHOR1O7gzpA" incomingStoryLinks="_GuzW8CfDEeCkHOR1O7gzpA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_FyEo8CfDEeCkHOR1O7gzpA" source="__wQQcCfCEeCkHOR1O7gzpA" target="_AtSgcCfDEeCkHOR1O7gzpA"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GP078CfDEeCkHOR1O7gzpA" source="_AtSgcCfDEeCkHOR1O7gzpA" target="_BSI58CfDEeCkHOR1O7gzpA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GuzW8CfDEeCkHOR1O7gzpA" source="_BSI58CfDEeCkHOR1O7gzpA" target="_BztV8CfDEeCkHOR1O7gzpA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelObject/postCreationExpressions"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression activity node" uuid="_kzQpECfDEeCkHOR1O7gzpA" incoming="_IdFBMCfEEeCkHOR1O7gzpA" outgoing="_Jv7wMCfEEeCkHOR1O7gzpA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_mN0ikCfDEeCkHOR1O7gzpA" outgoingStoryLinks="_6d37YCfDEeCkHOR1O7gzpA _61S84CfDEeCkHOR1O7gzpA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="incomingEdge" uuid="_nOTvMCfDEeCkHOR1O7gzpA" outgoingStoryLinks="_7G64YCfDEeCkHOR1O7gzpA" incomingStoryLinks="_6d37YCfDEeCkHOR1O7gzpA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ean" uuid="_n2fJgCfDEeCkHOR1O7gzpA" modifier="CREATE" outgoingStoryLinks="_8tEW0CfDEeCkHOR1O7gzpA" incomingStoryLinks="_61S84CfDEeCkHOR1O7gzpA _7G64YCfDEeCkHOR1O7gzpA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
        <attributeAssignments uuid="_1kJzYCfDEeCkHOR1O7gzpA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_2b_aUCfDEeCkHOR1O7gzpA" expressionString="'execute post-creation expression of '.concat(modelObject.name)" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="clonedExpression" uuid="_oe7CgCfDEeCkHOR1O7gzpA" incomingStoryLinks="_8tEW0CfDEeCkHOR1O7gzpA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/Expression"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_tpDlECfDEeCkHOR1O7gzpA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_uYCyACfDEeCkHOR1O7gzpA" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters name="" uuid="_w0F9sCfDEeCkHOR1O7gzpA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_yO688CfDEeCkHOR1O7gzpA" expressionString="expression" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_6d37YCfDEeCkHOR1O7gzpA" source="_mN0ikCfDEeCkHOR1O7gzpA" target="_nOTvMCfDEeCkHOR1O7gzpA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_61S84CfDEeCkHOR1O7gzpA" modifier="CREATE" source="_mN0ikCfDEeCkHOR1O7gzpA" target="_n2fJgCfDEeCkHOR1O7gzpA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/nodes"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7G64YCfDEeCkHOR1O7gzpA" modifier="CREATE" source="_nOTvMCfDEeCkHOR1O7gzpA" target="_n2fJgCfDEeCkHOR1O7gzpA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_8tEW0CfDEeCkHOR1O7gzpA" modifier="CREATE" source="_n2fJgCfDEeCkHOR1O7gzpA" target="_oe7CgCfDEeCkHOR1O7gzpA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode/expression"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create new edge" uuid="_90i8QCfDEeCkHOR1O7gzpA" incoming="_fzeEYCorEeCjTcyrv1LVTA" outgoing="_KJWVwCfEEeCkHOR1O7gzpA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="activity" uuid="_ADv-wCfEEeCkHOR1O7gzpA" outgoingStoryLinks="_FrtFwCfEEeCkHOR1O7gzpA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ean" uuid="_Ap-RQCfEEeCkHOR1O7gzpA" incomingStoryLinks="_GK1RwCfEEeCkHOR1O7gzpA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="incomingEdge" uuid="_BBjDwCfEEeCkHOR1O7gzpA" modifier="CREATE" outgoingStoryLinks="_GK1RwCfEEeCkHOR1O7gzpA" incomingStoryLinks="_FrtFwCfEEeCkHOR1O7gzpA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_FrtFwCfEEeCkHOR1O7gzpA" modifier="CREATE" source="_ADv-wCfEEeCkHOR1O7gzpA" target="_BBjDwCfEEeCkHOR1O7gzpA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//Activity/edges"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GK1RwCfEEeCkHOR1O7gzpA" modifier="CREATE" source="_BBjDwCfEEeCkHOR1O7gzpA" target="_Ap-RQCfEEeCkHOR1O7gzpA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ActivityEdge/source"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_LeUkoCfEEeCkHOR1O7gzpA" incoming="_L9sBMCfEEeCkHOR1O7gzpA">
      <outParameterValues xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_MkgJkCfEEeCkHOR1O7gzpA" expressionString="incomingEdge" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set new UUIDs" uuid="_dymY0CorEeCjTcyrv1LVTA" incoming="_Jv7wMCfEEeCkHOR1O7gzpA" outgoing="_fzeEYCorEeCjTcyrv1LVTA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ean" uuid="_g4RwUCorEeCjTcyrv1LVTA" outgoingStoryLinks="_o9PFQCorEeCjTcyrv1LVTA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/ExpressionActivityNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="element" uuid="_hj110CorEeCjTcyrv1LVTA" incomingStoryLinks="_o9PFQCorEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement"/>
        <attributeAssignments uuid="_jHGYUCorEeCjTcyrv1LVTA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/uuid"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_kO1ccCorEeCjTcyrv1LVTA">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_lzxZ8CorEeCjTcyrv1LVTA" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="generateUUID">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_o9PFQCorEeCjTcyrv1LVTA" source="_g4RwUCorEeCjTcyrv1LVTA" target="_hj110CorEeCjTcyrv1LVTA"/>
    </nodes>
    <edges uuid="_7em1ECfCEeCkHOR1O7gzpA" source="_mYev4CfCEeCkHOR1O7gzpA" target="_nUpdMCfCEeCkHOR1O7gzpA"/>
    <edges uuid="_cHBRsCfDEeCkHOR1O7gzpA" source="_nUpdMCfCEeCkHOR1O7gzpA" target="_-Mia8CfCEeCkHOR1O7gzpA"/>
    <edges uuid="_IdFBMCfEEeCkHOR1O7gzpA" source="_-Mia8CfCEeCkHOR1O7gzpA" target="_kzQpECfDEeCkHOR1O7gzpA" guardType="FOR_EACH"/>
    <edges uuid="_Jv7wMCfEEeCkHOR1O7gzpA" source="_kzQpECfDEeCkHOR1O7gzpA" target="_dymY0CorEeCjTcyrv1LVTA"/>
    <edges uuid="_KJWVwCfEEeCkHOR1O7gzpA" source="_90i8QCfDEeCkHOR1O7gzpA" target="_-Mia8CfCEeCkHOR1O7gzpA"/>
    <edges uuid="_L9sBMCfEEeCkHOR1O7gzpA" source="_-Mia8CfCEeCkHOR1O7gzpA" target="_LeUkoCfEEeCkHOR1O7gzpA" guardType="END"/>
    <edges uuid="_fzeEYCorEeCjTcyrv1LVTA" source="_dymY0CorEeCjTcyrv1LVTA" target="_90i8QCfDEeCkHOR1O7gzpA" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
