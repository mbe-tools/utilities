<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_ruleSet_modelElements_links" uuid="_DX3LUA0fEeCQ056ROwfPtw">
  <activities name="create_ruleSet_modelElements_links" uuid="_E_gekA0fEeCQ056ROwfPtw">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_GtCXYA0fEeCQ056ROwfPtw" outgoing="_MFQj0A0gEeCQ056ROwfPtw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_HMn2YA0fEeCQ056ROwfPtw" incoming="_MFQj0A0gEeCQ056ROwfPtw" outgoing="_Mf7GkA0gEeCQ056ROwfPtw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_KBGwMA0fEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_LGhgIA0fEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_L8xLoA0fEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createSourceLinks" uuid="_UmTNQA0fEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createTargetLinks" uuid="_WXRboA0fEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_bq9i0A0fEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetEClass" uuid="_b-dRIA0gEeCQ056ROwfPtw" outgoingStoryLinks="_omXM0A0gEeCQ056ROwfPtw _pj1hsA0gEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lCrqsA0gEeCQ056ROwfPtw" expressionString="mote::rules::TGGRuleSet" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sourceElementsEReference" uuid="_fDAnQA0gEeCQ056ROwfPtw" incomingStoryLinks="_omXM0A0gEeCQ056ROwfPtw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_qxPwAA0gEeCQ056ROwfPtw" expressionString="self.name = 'sourceModelElements'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="targetElementsEReference" uuid="_gvDSUA0gEeCQ056ROwfPtw" incomingStoryLinks="_pj1hsA0gEeCQ056ROwfPtw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_yajDsA0gEeCQ056ROwfPtw" expressionString="self.name = 'targetModelElements'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createParentLinks" uuid="_A9YPwBWMEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createCreatedLinks" uuid="_CMXycBWMEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_omXM0A0gEeCQ056ROwfPtw" source="_b-dRIA0gEeCQ056ROwfPtw" target="_fDAnQA0gEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_pj1hsA0gEeCQ056ROwfPtw" source="_b-dRIA0gEeCQ056ROwfPtw" target="_gvDSUA0gEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create elements links" uuid="_Q9-bsA0fEeCQ056ROwfPtw" incoming="_Mf7GkA0gEeCQ056ROwfPtw _qKphAEP9EeCj3vsEcE-cfw" outgoing="_B-XW4A0hEeCQ056ROwfPtw _Ue-yYA0hEeCQ056ROwfPtw" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_YsEIwA0fEeCQ056ROwfPtw" outgoingStoryLinks="_tNdJMA0fEeCQ056ROwfPtw _g1vroBQwEeCkcYaoegFQZw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelDomain" uuid="_hpy4wA0fEeCQ056ROwfPtw" outgoingStoryLinks="_xnJGQA0fEeCQ056ROwfPtw" incomingStoryLinks="_tNdJMA0fEeCQ056ROwfPtw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_iTXaMA0fEeCQ056ROwfPtw" incomingStoryLinks="_xnJGQA0fEeCQ056ROwfPtw _j6vGoBQwEeCkcYaoegFQZw _NXrcYClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_uh7o0A0fEeCQ056ROwfPtw" expressionString="createParentLinks and self.modifier = tgg::TGGModifierEnumeration::NONE or&#xA;createCreatedLinks and self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_jJiNMA0fEeCQ056ROwfPtw" outgoingStoryLinks="_NXrcYClYEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_kdlPAA0fEeCQ056ROwfPtw" outgoingStoryLinks="_AH5Z8A0gEeCQ056ROwfPtw _Ax1u0A0gEeCQ056ROwfPtw _BZQUIA0gEeCQ056ROwfPtw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo" uuid="_lTTWEA0fEeCQ056ROwfPtw" incomingStoryLinks="_AH5Z8A0gEeCQ056ROwfPtw _ByEcwA0gEeCQ056ROwfPtw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_lzMXEA0fEeCQ056ROwfPtw" incomingStoryLinks="_Ax1u0A0gEeCQ056ROwfPtw _Cc1RAA0gEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mapEntryEClass" uuid="_olMkoA0fEeCQ056ROwfPtw" incomingStoryLinks="_DStJEA0gEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_9s-2sA0fEeCQ056ROwfPtw" expressionString="mote::helpers::MapEntry" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelElementsLink" uuid="_qDmcUA0fEeCQ056ROwfPtw" modifier="CREATE" outgoingStoryLinks="_ByEcwA0gEeCQ056ROwfPtw _Cc1RAA0gEeCQ056ROwfPtw _DStJEA0gEeCQ056ROwfPtw _HJoXUA0gEeCQ056ROwfPtw" incomingStoryLinks="_BZQUIA0gEeCQ056ROwfPtw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_6Of9oA0fEeCQ056ROwfPtw" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_FdzusA0gEeCQ056ROwfPtw" incomingStoryLinks="_HJoXUA0gEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_SZhSoBQwEeCkcYaoegFQZw" outgoingStoryLinks="_hw5sUBQwEeCkcYaoegFQZw _iTdm0BQwEeCkcYaoegFQZw" incomingStoryLinks="_g1vroBQwEeCkcYaoegFQZw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNode" uuid="_T6mq4BQwEeCkcYaoegFQZw" incomingStoryLinks="_iTdm0BQwEeCkcYaoegFQZw _jhBmIBQwEeCkcYaoegFQZw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrLink" uuid="_Usfa0BQwEeCkcYaoegFQZw" outgoingStoryLinks="_jhBmIBQwEeCkcYaoegFQZw _j6vGoBQwEeCkcYaoegFQZw" incomingStoryLinks="_hw5sUBQwEeCkcYaoegFQZw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceLink"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_tNdJMA0fEeCQ056ROwfPtw" source="_YsEIwA0fEeCQ056ROwfPtw" target="_hpy4wA0fEeCQ056ROwfPtw"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xnJGQA0fEeCQ056ROwfPtw" source="_hpy4wA0fEeCQ056ROwfPtw" target="_iTXaMA0fEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_AH5Z8A0gEeCQ056ROwfPtw" source="_kdlPAA0fEeCQ056ROwfPtw" target="_lTTWEA0fEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ax1u0A0gEeCQ056ROwfPtw" source="_kdlPAA0fEeCQ056ROwfPtw" target="_lzMXEA0fEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BZQUIA0gEeCQ056ROwfPtw" modifier="CREATE" source="_kdlPAA0fEeCQ056ROwfPtw" target="_qDmcUA0fEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ByEcwA0gEeCQ056ROwfPtw" modifier="CREATE" source="_qDmcUA0fEeCQ056ROwfPtw" target="_lTTWEA0fEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Cc1RAA0gEeCQ056ROwfPtw" modifier="CREATE" source="_qDmcUA0fEeCQ056ROwfPtw" target="_lzMXEA0fEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_DStJEA0gEeCQ056ROwfPtw" modifier="CREATE" source="_qDmcUA0fEeCQ056ROwfPtw" target="_olMkoA0fEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_HJoXUA0gEeCQ056ROwfPtw" modifier="CREATE" source="_qDmcUA0fEeCQ056ROwfPtw" target="_FdzusA0gEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_g1vroBQwEeCkcYaoegFQZw" source="_YsEIwA0fEeCQ056ROwfPtw" target="_SZhSoBQwEeCkcYaoegFQZw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hw5sUBQwEeCkcYaoegFQZw" source="_SZhSoBQwEeCkcYaoegFQZw" target="_Usfa0BQwEeCkcYaoegFQZw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iTdm0BQwEeCkcYaoegFQZw" source="_SZhSoBQwEeCkcYaoegFQZw" target="_T6mq4BQwEeCkcYaoegFQZw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_jhBmIBQwEeCkcYaoegFQZw" source="_Usfa0BQwEeCkcYaoegFQZw" target="_T6mq4BQwEeCkcYaoegFQZw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_j6vGoBQwEeCkcYaoegFQZw" source="_Usfa0BQwEeCkcYaoegFQZw" target="_iTXaMA0fEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_NXrcYClYEeCFgb_w6FqGaA" source="_jJiNMA0fEeCQ056ROwfPtw" target="_iTXaMA0fEeCQ056ROwfPtw" valueTarget="_lTTWEA0fEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
      <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_M69cwA0gEeCQ056ROwfPtw" expressionString="createSourceLinks and modelDomain.oclIsKindOf(tgg::SourceModelDomain) or&#xA;createTargetLinks and modelDomain.oclIsKindOf(tgg::TargetModelDomain)" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="handle source links" uuid="_WGO5gA0gEeCQ056ROwfPtw" incoming="_DBkgUA0hEeCQ056ROwfPtw" outgoing="_OkP5UA0hEeCQ056ROwfPtw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelElementsLink" uuid="_ZHPMYA0gEeCQ056ROwfPtw" outgoingStoryLinks="_3PGX0A0gEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sourceElementsEReference" uuid="_1AGwsA0gEeCQ056ROwfPtw" incomingStoryLinks="_3PGX0A0gEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_3PGX0A0gEeCQ056ROwfPtw" modifier="CREATE" source="_ZHPMYA0gEeCQ056ROwfPtw" target="_1AGwsA0gEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="handle target links" uuid="_6Qz1UA0gEeCQ056ROwfPtw" incoming="_D3Xf4A0hEeCQ056ROwfPtw" outgoing="_O8xHAA0hEeCQ056ROwfPtw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelElementsLink" uuid="_7CIkkA0gEeCQ056ROwfPtw" outgoingStoryLinks="__tmOIA0gEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="targetElementsEReference" uuid="_8ONWIA0gEeCQ056ROwfPtw" incomingStoryLinks="__tmOIA0gEeCQ056ROwfPtw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="__tmOIA0gEeCQ056ROwfPtw" modifier="CREATE" source="_7CIkkA0gEeCQ056ROwfPtw" target="_8ONWIA0gEeCQ056ROwfPtw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_BVmtwA0hEeCQ056ROwfPtw" incoming="_B-XW4A0hEeCQ056ROwfPtw" outgoing="_DBkgUA0hEeCQ056ROwfPtw _D3Xf4A0hEeCQ056ROwfPtw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_MbmKwA0hEeCQ056ROwfPtw" incoming="_OkP5UA0hEeCQ056ROwfPtw _O8xHAA0hEeCQ056ROwfPtw" outgoing="_QMykcA0hEeCQ056ROwfPtw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_T8aQ0A0hEeCQ056ROwfPtw" incoming="_Ue-yYA0hEeCQ056ROwfPtw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create value target link if the correspondence node has also been transformed" uuid="_EP-gkEP9EeCj3vsEcE-cfw" incoming="_QMykcA0hEeCQ056ROwfPtw" outgoing="_qKphAEP9EeCj3vsEcE-cfw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNode" uuid="_JEsz0EP9EeCj3vsEcE-cfw" incomingStoryLinks="_j5lLkEP9EeCj3vsEcE-cfw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelElementsLink" uuid="_J_mV0EP9EeCj3vsEcE-cfw" outgoingStoryLinks="_XVdjsEP9EeCj3vsEcE-cfw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_MFGpwEP9EeCj3vsEcE-cfw" outgoingStoryLinks="_XGXXwEP9EeCj3vsEcE-cfw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_OEXLwEP9EeCj3vsEcE-cfw" outgoingStoryLinks="_j5lLkEP9EeCj3vsEcE-cfw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodeSpo" uuid="_VSke4EP9EeCj3vsEcE-cfw" incomingStoryLinks="_XGXXwEP9EeCj3vsEcE-cfw _XVdjsEP9EeCj3vsEcE-cfw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XGXXwEP9EeCj3vsEcE-cfw" source="_MFGpwEP9EeCj3vsEcE-cfw" target="_VSke4EP9EeCj3vsEcE-cfw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XVdjsEP9EeCj3vsEcE-cfw" modifier="CREATE" source="_J_mV0EP9EeCj3vsEcE-cfw" target="_VSke4EP9EeCj3vsEcE-cfw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/MapEntryStoryPatternLink/valueTarget"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_j5lLkEP9EeCj3vsEcE-cfw" source="_OEXLwEP9EeCj3vsEcE-cfw" target="_JEsz0EP9EeCj3vsEcE-cfw" valueTarget="_VSke4EP9EeCj3vsEcE-cfw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_MFQj0A0gEeCQ056ROwfPtw" source="_GtCXYA0fEeCQ056ROwfPtw" target="_HMn2YA0fEeCQ056ROwfPtw"/>
    <edges uuid="_Mf7GkA0gEeCQ056ROwfPtw" source="_HMn2YA0fEeCQ056ROwfPtw" target="_Q9-bsA0fEeCQ056ROwfPtw"/>
    <edges uuid="_B-XW4A0hEeCQ056ROwfPtw" source="_Q9-bsA0fEeCQ056ROwfPtw" target="_BVmtwA0hEeCQ056ROwfPtw" guardType="FOR_EACH"/>
    <edges uuid="_DBkgUA0hEeCQ056ROwfPtw" source="_BVmtwA0hEeCQ056ROwfPtw" target="_WGO5gA0gEeCQ056ROwfPtw" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_FPS8IA0hEeCQ056ROwfPtw" expressionString="modelDomain.oclIsKindOf(tgg::SourceModelDomain)" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_D3Xf4A0hEeCQ056ROwfPtw" source="_BVmtwA0hEeCQ056ROwfPtw" target="_6Qz1UA0gEeCQ056ROwfPtw" guardType="ELSE"/>
    <edges uuid="_OkP5UA0hEeCQ056ROwfPtw" source="_WGO5gA0gEeCQ056ROwfPtw" target="_MbmKwA0hEeCQ056ROwfPtw"/>
    <edges uuid="_O8xHAA0hEeCQ056ROwfPtw" source="_6Qz1UA0gEeCQ056ROwfPtw" target="_MbmKwA0hEeCQ056ROwfPtw"/>
    <edges uuid="_QMykcA0hEeCQ056ROwfPtw" source="_MbmKwA0hEeCQ056ROwfPtw" target="_EP-gkEP9EeCj3vsEcE-cfw"/>
    <edges uuid="_Ue-yYA0hEeCQ056ROwfPtw" source="_Q9-bsA0fEeCQ056ROwfPtw" target="_T8aQ0A0hEeCQ056ROwfPtw" guardType="END"/>
    <edges uuid="_qKphAEP9EeCj3vsEcE-cfw" source="_EP-gkEP9EeCj3vsEcE-cfw" target="_Q9-bsA0fEeCQ056ROwfPtw"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
