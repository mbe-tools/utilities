<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_corrNode_sourcesTargets_links" uuid="_sUkcQBNoEeCRUJdROfc5HA">
  <activities name="create_corrNode_sourcesTargets_links" uuid="_uYYuIBNoEeCRUJdROfc5HA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_xSIicBNoEeCRUJdROfc5HA" outgoing="_C9yr4BNpEeCRUJdROfc5HA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_xtwHcBNoEeCRUJdROfc5HA" incoming="_C9yr4BNpEeCRUJdROfc5HA" outgoing="_u4A-gBNqEeCRUJdROfc5HA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_ycfcwBNoEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_y8wRMBNoEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_0CoUIBNoEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_1Gzt8BNoEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createSourcesLinks" uuid="_1px4IBNoEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createTargetsLinks" uuid="_2xpfIBNoEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createParentLinks" uuid="_4LC7ABNoEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createCreatedLinks" uuid="_43tzwBNoEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggNodeEClass" uuid="_QCKhIBNpEeCRUJdROfc5HA" outgoingStoryLinks="_WgkvABNpEeCRUJdROfc5HA _XUCzUBNpEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_T6_M0BNpEeCRUJdROfc5HA" expressionString="mote::TGGNode" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sourcesEReference" uuid="_Q0J-wBNpEeCRUJdROfc5HA" incomingStoryLinks="_WgkvABNpEeCRUJdROfc5HA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Zxd4ABNpEeCRUJdROfc5HA" expressionString="self.name = 'sources'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="targetsEReference" uuid="_RjPSUBNpEeCRUJdROfc5HA" incomingStoryLinks="_XUCzUBNpEeCRUJdROfc5HA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bJ-jEBNpEeCRUJdROfc5HA" expressionString="self.name = 'targets'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WgkvABNpEeCRUJdROfc5HA" source="_QCKhIBNpEeCRUJdROfc5HA" target="_Q0J-wBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XUCzUBNpEeCRUJdROfc5HA" source="_QCKhIBNpEeCRUJdROfc5HA" target="_RjPSUBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create links" uuid="_DyqeYBNpEeCRUJdROfc5HA" incoming="_u4A-gBNqEeCRUJdROfc5HA _R_qdsBNrEeCRUJdROfc5HA" outgoing="_zTgSYBNqEeCRUJdROfc5HA _HzAccBNrEeCRUJdROfc5HA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_fUeNkBNpEeCRUJdROfc5HA" outgoingStoryLinks="_3dL6ABNpEeCRUJdROfc5HA _34hyMBNpEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelDomain" uuid="_grpb4BNpEeCRUJdROfc5HA" outgoingStoryLinks="_5S5ecBNpEeCRUJdROfc5HA" incomingStoryLinks="_3dL6ABNpEeCRUJdROfc5HA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_FRXbkBNqEeCRUJdROfc5HA" expressionString="createSourcesLinks and self.oclIsKindOf(tgg::SourceModelDomain) or&#xA;createTargetsLinks and self.oclIsKindOf(tgg::TargetModelDomain)" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_hhEA8BNpEeCRUJdROfc5HA" outgoingStoryLinks="_5wVBUBNpEeCRUJdROfc5HA _6X4wkBNpEeCRUJdROfc5HA" incomingStoryLinks="_34hyMBNpEeCRUJdROfc5HA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_iIGy0BNpEeCRUJdROfc5HA" incomingStoryLinks="_5S5ecBNpEeCRUJdROfc5HA _7vcZYBNpEeCRUJdROfc5HA _0g898ClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNode" uuid="_i_UHoBNpEeCRUJdROfc5HA" incomingStoryLinks="_5wVBUBNpEeCRUJdROfc5HA _7ClTYBNpEeCRUJdROfc5HA _w7n2AClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrLink" uuid="_lmuOwBNpEeCRUJdROfc5HA" outgoingStoryLinks="_7ClTYBNpEeCRUJdROfc5HA _7vcZYBNpEeCRUJdROfc5HA" incomingStoryLinks="_6X4wkBNpEeCRUJdROfc5HA _y8Za8ClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceLink"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_956s4BNpEeCRUJdROfc5HA" expressionString="createParentLinks and self.modifier = tgg::TGGModifierEnumeration::NONE or&#xA;createCreatedLinks and self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_n3kQABNpEeCRUJdROfc5HA" outgoingStoryLinks="_w7n2AClYEeCFgb_w6FqGaA _y8Za8ClYEeCFgb_w6FqGaA _0g898ClYEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_o5DiMBNpEeCRUJdROfc5HA" outgoingStoryLinks="_XLe3wBNqEeCRUJdROfc5HA _X4txMBNqEeCRUJdROfc5HA _Yb2TcBNqEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mSpo" uuid="_p8CCIBNpEeCRUJdROfc5HA" incomingStoryLinks="_X4txMBNqEeCRUJdROfc5HA _ZocCYBNqEeCRUJdROfc5HA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cnSpo" uuid="_reXXkBNpEeCRUJdROfc5HA" incomingStoryLinks="_XLe3wBNqEeCRUJdROfc5HA _ZEQXQBNqEeCRUJdROfc5HA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spl" uuid="_vDzNMBNpEeCRUJdROfc5HA" modifier="CREATE" outgoingStoryLinks="_ZEQXQBNqEeCRUJdROfc5HA _ZocCYBNqEeCRUJdROfc5HA _rqw_YBNqEeCRUJdROfc5HA" incomingStoryLinks="_Yb2TcBNqEeCRUJdROfc5HA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_ovcdwBNqEeCRUJdROfc5HA" incomingStoryLinks="_rqw_YBNqEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_3dL6ABNpEeCRUJdROfc5HA" source="_fUeNkBNpEeCRUJdROfc5HA" target="_grpb4BNpEeCRUJdROfc5HA"/>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_34hyMBNpEeCRUJdROfc5HA" source="_fUeNkBNpEeCRUJdROfc5HA" target="_hhEA8BNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5S5ecBNpEeCRUJdROfc5HA" source="_grpb4BNpEeCRUJdROfc5HA" target="_iIGy0BNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_5wVBUBNpEeCRUJdROfc5HA" source="_hhEA8BNpEeCRUJdROfc5HA" target="_i_UHoBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_6X4wkBNpEeCRUJdROfc5HA" source="_hhEA8BNpEeCRUJdROfc5HA" target="_lmuOwBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7ClTYBNpEeCRUJdROfc5HA" source="_lmuOwBNpEeCRUJdROfc5HA" target="_i_UHoBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_7vcZYBNpEeCRUJdROfc5HA" source="_lmuOwBNpEeCRUJdROfc5HA" target="_iIGy0BNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XLe3wBNqEeCRUJdROfc5HA" source="_o5DiMBNpEeCRUJdROfc5HA" target="_reXXkBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_X4txMBNqEeCRUJdROfc5HA" source="_o5DiMBNpEeCRUJdROfc5HA" target="_p8CCIBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Yb2TcBNqEeCRUJdROfc5HA" modifier="CREATE" source="_o5DiMBNpEeCRUJdROfc5HA" target="_vDzNMBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZEQXQBNqEeCRUJdROfc5HA" modifier="CREATE" source="_vDzNMBNpEeCRUJdROfc5HA" target="_reXXkBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZocCYBNqEeCRUJdROfc5HA" modifier="CREATE" source="_vDzNMBNpEeCRUJdROfc5HA" target="_p8CCIBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_rqw_YBNqEeCRUJdROfc5HA" modifier="CREATE" source="_vDzNMBNpEeCRUJdROfc5HA" target="_ovcdwBNqEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_w7n2AClYEeCFgb_w6FqGaA" source="_n3kQABNpEeCRUJdROfc5HA" target="_i_UHoBNpEeCRUJdROfc5HA" valueTarget="_reXXkBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_y8Za8ClYEeCFgb_w6FqGaA" modifier="CREATE" source="_n3kQABNpEeCRUJdROfc5HA" target="_lmuOwBNpEeCRUJdROfc5HA" valueTarget="_vDzNMBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_0g898ClYEeCFgb_w6FqGaA" source="_n3kQABNpEeCRUJdROfc5HA" target="_iIGy0BNpEeCRUJdROfc5HA" valueTarget="_p8CCIBNpEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_yEuLEBNqEeCRUJdROfc5HA" incoming="_zTgSYBNqEeCRUJdROfc5HA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_2RfGcBNqEeCRUJdROfc5HA" incoming="_HzAccBNrEeCRUJdROfc5HA" outgoing="_IJWf4BNrEeCRUJdROfc5HA _IffH8BNrEeCRUJdROfc5HA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="handle sources links" uuid="_6ZUegBNqEeCRUJdROfc5HA" incoming="_IJWf4BNrEeCRUJdROfc5HA" outgoing="_RIIYwBNrEeCRUJdROfc5HA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spl" uuid="_-r2qsBNqEeCRUJdROfc5HA" outgoingStoryLinks="_FlcYsBNrEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="sourcesEReference" uuid="__u-UkBNqEeCRUJdROfc5HA" incomingStoryLinks="_FlcYsBNrEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_FlcYsBNrEeCRUJdROfc5HA" modifier="CREATE" source="_-r2qsBNqEeCRUJdROfc5HA" target="__u-UkBNqEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="handle targets links" uuid="_9ZRogBNqEeCRUJdROfc5HA" incoming="_IffH8BNrEeCRUJdROfc5HA" outgoing="_QtoOEBNrEeCRUJdROfc5HA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spl" uuid="__EyvIBNqEeCRUJdROfc5HA" outgoingStoryLinks="_GBKEUBNrEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="targetsEReference" uuid="_AjI8cBNrEeCRUJdROfc5HA" incomingStoryLinks="_GBKEUBNrEeCRUJdROfc5HA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GBKEUBNrEeCRUJdROfc5HA" modifier="CREATE" source="__EyvIBNqEeCRUJdROfc5HA" target="_AjI8cBNrEeCRUJdROfc5HA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_OSSNABNrEeCRUJdROfc5HA" incoming="_QtoOEBNrEeCRUJdROfc5HA _RIIYwBNrEeCRUJdROfc5HA" outgoing="_R_qdsBNrEeCRUJdROfc5HA"/>
    <edges uuid="_C9yr4BNpEeCRUJdROfc5HA" source="_xSIicBNoEeCRUJdROfc5HA" target="_xtwHcBNoEeCRUJdROfc5HA"/>
    <edges uuid="_u4A-gBNqEeCRUJdROfc5HA" source="_xtwHcBNoEeCRUJdROfc5HA" target="_DyqeYBNpEeCRUJdROfc5HA"/>
    <edges uuid="_zTgSYBNqEeCRUJdROfc5HA" source="_DyqeYBNpEeCRUJdROfc5HA" target="_yEuLEBNqEeCRUJdROfc5HA" guardType="END"/>
    <edges uuid="_HzAccBNrEeCRUJdROfc5HA" source="_DyqeYBNpEeCRUJdROfc5HA" target="_2RfGcBNqEeCRUJdROfc5HA" guardType="FOR_EACH"/>
    <edges uuid="_IJWf4BNrEeCRUJdROfc5HA" source="_2RfGcBNqEeCRUJdROfc5HA" target="_6ZUegBNqEeCRUJdROfc5HA" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_KH0-wBNrEeCRUJdROfc5HA" expressionString="modelDomain.oclIsKindOf(tgg::SourceModelDomain)" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_IffH8BNrEeCRUJdROfc5HA" source="_2RfGcBNqEeCRUJdROfc5HA" target="_9ZRogBNqEeCRUJdROfc5HA" guardType="ELSE"/>
    <edges uuid="_QtoOEBNrEeCRUJdROfc5HA" source="_9ZRogBNqEeCRUJdROfc5HA" target="_OSSNABNrEeCRUJdROfc5HA"/>
    <edges uuid="_RIIYwBNrEeCRUJdROfc5HA" source="_6ZUegBNqEeCRUJdROfc5HA" target="_OSSNABNrEeCRUJdROfc5HA"/>
    <edges uuid="_R_qdsBNrEeCRUJdROfc5HA" source="_OSSNABNrEeCRUJdROfc5HA" target="_DyqeYBNpEeCRUJdROfc5HA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
