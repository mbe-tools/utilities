<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_iterateOtherTGGRules" uuid="_sbSxMCrTEeCoHJjELNAVvA">
  <activities name="create_storyPattern_iterateOtherTGGRules" uuid="_tDkSICrTEeCoHJjELNAVvA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_vQxwkCrTEeCoHJjELNAVvA" outgoing="_ZjXzgCrUEeCoHJjELNAVvA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_vpi14CrTEeCoHJjELNAVvA" incoming="_ZjXzgCrUEeCoHJjELNAVvA" outgoing="_Z6pEACrUEeCoHJjELNAVvA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_wokC4CrTEeCoHJjELNAVvA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_zAj1ACrTEeCoHJjELNAVvA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_075gUCrTEeCoHJjELNAVvA" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create this and ruleSet SPOs" uuid="_7ArtQCrTEeCoHJjELNAVvA" incoming="_Z6pEACrUEeCoHJjELNAVvA" outgoing="_pTgEYCrUEeCoHJjELNAVvA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisSpo" uuid="_8DYgYCrTEeCoHJjELNAVvA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_AfsToCrUEeCoHJjELNAVvA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_A255wCrUEeCoHJjELNAVvA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="create_rule_spo.story#_Kpsp0AwlEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_CkDYECrUEeCoHJjELNAVvA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_GAY8ECrUEeCoHJjELNAVvA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="isAxiom" uuid="_G38PICrUEeCoHJjELNAVvA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_IEAZoCrUEeCoHJjELNAVvA" expressionString="tggRule.isAxiom" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_K-_kECrUEeCoHJjELNAVvA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_MXGmgCrUEeCoHJjELNAVvA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_MpL1ACrUEeCoHJjELNAVvA">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="create_ruleSet_spo.story#_ZDP-wAwkEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_QeZzECrUEeCoHJjELNAVvA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_S2vjcCrUEeCoHJjELNAVvA" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="bindingType" uuid="_VUNrcCrUEeCoHJjELNAVvA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WiGa4CrUEeCoHJjELNAVvA" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create this->ruleSet link" uuid="_abKXICrUEeCoHJjELNAVvA" incoming="_pTgEYCrUEeCoHJjELNAVvA" outgoing="_qkSW0CrUEeCoHJjELNAVvA">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_d650cCrUEeCoHJjELNAVvA">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_flixUCrUEeCoHJjELNAVvA">
          <activity href="create_rule_ruleSet_link.story#_qOEaEAwmEeCU3uEhqcP0Dg"/>
          <parameters name="ruleSpo" uuid="_jaSOQCrUEeCoHJjELNAVvA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_lM9FwCrUEeCoHJjELNAVvA" expressionString="thisSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_mS_gwCrUEeCoHJjELNAVvA">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_n3yUUCrUEeCoHJjELNAVvA" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_p-05UCrUEeCoHJjELNAVvA" incoming="_rPc-QCrVEeCoHJjELNAVvA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create otherRuleSpo" uuid="_F7TyUCrVEeCoHJjELNAVvA" incoming="_qkSW0CrUEeCoHJjELNAVvA" outgoing="_rPc-QCrVEeCoHJjELNAVvA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_JJMqoCrVEeCoHJjELNAVvA" outgoingStoryLinks="_hWAM4CrVEeCoHJjELNAVvA _hr-c4CrVEeCoHJjELNAVvA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_KtH7gCrVEeCoHJjELNAVvA" incomingStoryLinks="_iufp0CrVEeCoHJjELNAVvA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="otherRuleSpo" uuid="_LWeagCrVEeCoHJjELNAVvA" modifier="CREATE" outgoingStoryLinks="_h7JhUCrVEeCoHJjELNAVvA" incomingStoryLinks="_hr-c4CrVEeCoHJjELNAVvA _iINF4CrVEeCoHJjELNAVvA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_nSARQCrVEeCoHJjELNAVvA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_oJ_pMCrVEeCoHJjELNAVvA" expressionString="'otherRule'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesSpl" uuid="_MO20ACrVEeCoHJjELNAVvA" modifier="CREATE" outgoingStoryLinks="_gB7V4CrVEeCoHJjELNAVvA _iINF4CrVEeCoHJjELNAVvA _iufp0CrVEeCoHJjELNAVvA" incomingStoryLinks="_hWAM4CrVEeCoHJjELNAVvA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleSetEClass" uuid="_NuroACrVEeCoHJjELNAVvA" outgoingStoryLinks="_WiaHcCrVEeCoHJjELNAVvA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_UEn2YCrVEeCoHJjELNAVvA" expressionString="mote::rules::TGGRuleSet" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rulesEReference" uuid="_OzpE8CrVEeCoHJjELNAVvA" incomingStoryLinks="_WiaHcCrVEeCoHJjELNAVvA _gB7V4CrVEeCoHJjELNAVvA">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_X-8W4CrVEeCoHJjELNAVvA" expressionString="self.name = 'rules'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleEClass" uuid="_QCEm8CrVEeCoHJjELNAVvA" incomingStoryLinks="_h7JhUCrVEeCoHJjELNAVvA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_cHv_UCrVEeCoHJjELNAVvA" expressionString="mote::rules::TGGRule" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WiaHcCrVEeCoHJjELNAVvA" source="_NuroACrVEeCoHJjELNAVvA" target="_OzpE8CrVEeCoHJjELNAVvA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_gB7V4CrVEeCoHJjELNAVvA" modifier="CREATE" source="_MO20ACrVEeCoHJjELNAVvA" target="_OzpE8CrVEeCoHJjELNAVvA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hWAM4CrVEeCoHJjELNAVvA" modifier="CREATE" source="_JJMqoCrVEeCoHJjELNAVvA" target="_MO20ACrVEeCoHJjELNAVvA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_hr-c4CrVEeCoHJjELNAVvA" modifier="CREATE" source="_JJMqoCrVEeCoHJjELNAVvA" target="_LWeagCrVEeCoHJjELNAVvA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_h7JhUCrVEeCoHJjELNAVvA" modifier="CREATE" source="_LWeagCrVEeCoHJjELNAVvA" target="_QCEm8CrVEeCoHJjELNAVvA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iINF4CrVEeCoHJjELNAVvA" modifier="CREATE" source="_MO20ACrVEeCoHJjELNAVvA" target="_LWeagCrVEeCoHJjELNAVvA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_iufp0CrVEeCoHJjELNAVvA" modifier="CREATE" source="_MO20ACrVEeCoHJjELNAVvA" target="_KtH7gCrVEeCoHJjELNAVvA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_ZjXzgCrUEeCoHJjELNAVvA" source="_vQxwkCrTEeCoHJjELNAVvA" target="_vpi14CrTEeCoHJjELNAVvA"/>
    <edges uuid="_Z6pEACrUEeCoHJjELNAVvA" source="_vpi14CrTEeCoHJjELNAVvA" target="_7ArtQCrTEeCoHJjELNAVvA"/>
    <edges uuid="_pTgEYCrUEeCoHJjELNAVvA" source="_7ArtQCrTEeCoHJjELNAVvA" target="_abKXICrUEeCoHJjELNAVvA"/>
    <edges uuid="_qkSW0CrUEeCoHJjELNAVvA" source="_abKXICrUEeCoHJjELNAVvA" target="_F7TyUCrVEeCoHJjELNAVvA"/>
    <edges uuid="_rPc-QCrVEeCoHJjELNAVvA" source="_F7TyUCrVEeCoHJjELNAVvA" target="_p-05UCrUEeCoHJjELNAVvA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
