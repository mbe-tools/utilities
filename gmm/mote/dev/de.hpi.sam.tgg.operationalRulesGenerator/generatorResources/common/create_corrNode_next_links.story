<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_corrNode_next_links" uuid="_R1e94BWjEeC7l7ZPwVRGQg">
  <activities name="create_corrNode_next_links" uuid="_TmMGgBWjEeC7l7ZPwVRGQg">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_VtFhkBWjEeC7l7ZPwVRGQg" outgoing="_fIJ4YBWkEeC7l7ZPwVRGQg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_WLyPwBWjEeC7l7ZPwVRGQg" incoming="_fIJ4YBWkEeC7l7ZPwVRGQg" outgoing="_hXWT0BWkEeC7l7ZPwVRGQg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_W3-AUBWjEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_XXB64BWjEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_YMOdgBWjEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_Y4LkkBWjEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggNodeEClass" uuid="_uua4EBWjEeC7l7ZPwVRGQg" outgoingStoryLinks="_0d4VQBWjEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_8bg6MBWjEeC7l7ZPwVRGQg" expressionString="mote::TGGNode" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="nextEReference" uuid="_v1WD4BWjEeC7l7ZPwVRGQg" incomingStoryLinks="_0d4VQBWjEeC7l7ZPwVRGQg">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_3mqEEBWjEeC7l7ZPwVRGQg" expressionString="self.name = 'next'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_xETxYBWjEeC7l7ZPwVRGQg" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_0d4VQBWjEeC7l7ZPwVRGQg" source="_uua4EBWjEeC7l7ZPwVRGQg" target="_v1WD4BWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create next links" uuid="_hAwOMBWjEeC7l7ZPwVRGQg" incoming="_hXWT0BWkEeC7l7ZPwVRGQg" outgoing="_ibOysBWkEeC7l7ZPwVRGQg" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_jt0RYBWjEeC7l7ZPwVRGQg" outgoingStoryLinks="_KbJd0BWkEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_kTwQABWjEeC7l7ZPwVRGQg" outgoingStoryLinks="_8tYz4ClYEeCFgb_w6FqGaA _8_2c4ClYEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_ldI58BWjEeC7l7ZPwVRGQg" outgoingStoryLinks="_UusKMBWkEeC7l7ZPwVRGQg _VVDmoBWkEeC7l7ZPwVRGQg _V5HV8BWkEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_mqe20BWjEeC7l7ZPwVRGQg" incomingStoryLinks="_WeqTABWkEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_nKpkoBWjEeC7l7ZPwVRGQg" outgoingStoryLinks="_LPxYsBWkEeC7l7ZPwVRGQg _L04Q4BWkEeC7l7ZPwVRGQg" incomingStoryLinks="_KbJd0BWkEeC7l7ZPwVRGQg">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="parentNode" uuid="_nxBoIBWjEeC7l7ZPwVRGQg" incomingStoryLinks="_LPxYsBWkEeC7l7ZPwVRGQg _8tYz4ClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_MLFKYBWkEeC7l7ZPwVRGQg" expressionString="self.modifier = tgg::TGGModifierEnumeration::NONE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createdNode" uuid="_oW4HMBWjEeC7l7ZPwVRGQg" incomingStoryLinks="_L04Q4BWkEeC7l7ZPwVRGQg _8_2c4ClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_OhMGIBWkEeC7l7ZPwVRGQg" expressionString="self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="parentNodeSpo" uuid="_qfEiwBWjEeC7l7ZPwVRGQg" incomingStoryLinks="_UusKMBWkEeC7l7ZPwVRGQg _X1dt4BWkEeC7l7ZPwVRGQg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="createdNodeSpo" uuid="_rEsYUBWjEeC7l7ZPwVRGQg" incomingStoryLinks="_VVDmoBWkEeC7l7ZPwVRGQg _XHfNkBWkEeC7l7ZPwVRGQg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="nextSpl" uuid="_stI80BWjEeC7l7ZPwVRGQg" modifier="CREATE" outgoingStoryLinks="_WeqTABWkEeC7l7ZPwVRGQg _WxERoBWkEeC7l7ZPwVRGQg _XHfNkBWkEeC7l7ZPwVRGQg _X1dt4BWkEeC7l7ZPwVRGQg" incomingStoryLinks="_V5HV8BWkEeC7l7ZPwVRGQg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="nextEReference" uuid="_tpDLcBWjEeC7l7ZPwVRGQg" incomingStoryLinks="_WxERoBWkEeC7l7ZPwVRGQg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_KbJd0BWkEeC7l7ZPwVRGQg" source="_jt0RYBWjEeC7l7ZPwVRGQg" target="_nKpkoBWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LPxYsBWkEeC7l7ZPwVRGQg" source="_nKpkoBWjEeC7l7ZPwVRGQg" target="_nxBoIBWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_L04Q4BWkEeC7l7ZPwVRGQg" source="_nKpkoBWjEeC7l7ZPwVRGQg" target="_oW4HMBWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_UusKMBWkEeC7l7ZPwVRGQg" source="_ldI58BWjEeC7l7ZPwVRGQg" target="_qfEiwBWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_VVDmoBWkEeC7l7ZPwVRGQg" source="_ldI58BWjEeC7l7ZPwVRGQg" target="_rEsYUBWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_V5HV8BWkEeC7l7ZPwVRGQg" modifier="CREATE" source="_ldI58BWjEeC7l7ZPwVRGQg" target="_stI80BWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WeqTABWkEeC7l7ZPwVRGQg" modifier="CREATE" source="_stI80BWjEeC7l7ZPwVRGQg" target="_mqe20BWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_WxERoBWkEeC7l7ZPwVRGQg" modifier="CREATE" source="_stI80BWjEeC7l7ZPwVRGQg" target="_tpDLcBWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XHfNkBWkEeC7l7ZPwVRGQg" modifier="CREATE" source="_stI80BWjEeC7l7ZPwVRGQg" target="_rEsYUBWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_X1dt4BWkEeC7l7ZPwVRGQg" modifier="CREATE" source="_stI80BWjEeC7l7ZPwVRGQg" target="_qfEiwBWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_8tYz4ClYEeCFgb_w6FqGaA" source="_kTwQABWjEeC7l7ZPwVRGQg" target="_nxBoIBWjEeC7l7ZPwVRGQg" valueTarget="_qfEiwBWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_8_2c4ClYEeCFgb_w6FqGaA" source="_kTwQABWjEeC7l7ZPwVRGQg" target="_oW4HMBWjEeC7l7ZPwVRGQg" valueTarget="_rEsYUBWjEeC7l7ZPwVRGQg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_hrOSEBWkEeC7l7ZPwVRGQg" incoming="_ibOysBWkEeC7l7ZPwVRGQg"/>
    <edges uuid="_fIJ4YBWkEeC7l7ZPwVRGQg" source="_VtFhkBWjEeC7l7ZPwVRGQg" target="_WLyPwBWjEeC7l7ZPwVRGQg"/>
    <edges uuid="_hXWT0BWkEeC7l7ZPwVRGQg" source="_WLyPwBWjEeC7l7ZPwVRGQg" target="_hAwOMBWjEeC7l7ZPwVRGQg"/>
    <edges uuid="_ibOysBWkEeC7l7ZPwVRGQg" source="_hAwOMBWjEeC7l7ZPwVRGQg" target="_hrOSEBWkEeC7l7ZPwVRGQg" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
