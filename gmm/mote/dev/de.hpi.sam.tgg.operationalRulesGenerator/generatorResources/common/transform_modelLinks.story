<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="transform_modelLinks" uuid="_s2GcMAw6EeCAUcBgGC58lA">
  <activities name="transform_modelLinks" uuid="_tdw5IAw6EeCAUcBgGC58lA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_v06WsAw6EeCAUcBgGC58lA" outgoing="_-zB70Aw6EeCAUcBgGC58lA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_wQ_1wAw6EeCAUcBgGC58lA" incoming="_-zB70Aw6EeCAUcBgGC58lA" outgoing="_et2lwAxTEeCp2NlniWjADQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_w1IdkAw6EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_xW4fwAw6EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_yZIm8Aw6EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_0MjsYAw6EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformParentLinks" uuid="_tyxL4AxSEeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformCreatedLinks" uuid="_u-q-UAxSEeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformSource" uuid="_v9aegAxSEeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="transformTarget" uuid="_wofb0AxSEeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="iterate through model links" uuid="_8bigAAw6EeCAUcBgGC58lA" incoming="_et2lwAxTEeCp2NlniWjADQ _0zpYMAxTEeCp2NlniWjADQ" outgoing="_9UdPkAw7EeCAUcBgGC58lA _0dPDUAxTEeCp2NlniWjADQ" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="__0mGgAw6EeCAUcBgGC58lA" outgoingStoryLinks="_An4QsAxTEeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelDomain" uuid="_CI0L4Aw7EeCAUcBgGC58lA" outgoingStoryLinks="_Wc03oAw7EeCAUcBgGC58lA _XSU8QAw7EeCAUcBgGC58lA _XyEzUAw7EeCAUcBgGC58lA" incomingStoryLinks="_An4QsAxTEeCp2NlniWjADQ">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mo1" uuid="_D4lgYAw7EeCAUcBgGC58lA" incomingStoryLinks="_Wc03oAw7EeCAUcBgGC58lA _ZSL7MAw7EeCAUcBgGC58lA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ml" uuid="_EWK0QAw7EeCAUcBgGC58lA" outgoingStoryLinks="_ZSL7MAw7EeCAUcBgGC58lA _aHUzcAw7EeCAUcBgGC58lA" incomingStoryLinks="_XSU8QAw7EeCAUcBgGC58lA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mo2" uuid="_E5MBwAw7EeCAUcBgGC58lA" incomingStoryLinks="_XyEzUAw7EeCAUcBgGC58lA _aHUzcAw7EeCAUcBgGC58lA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Wc03oAw7EeCAUcBgGC58lA" source="_CI0L4Aw7EeCAUcBgGC58lA" target="_D4lgYAw7EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XSU8QAw7EeCAUcBgGC58lA" source="_CI0L4Aw7EeCAUcBgGC58lA" target="_EWK0QAw7EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_XyEzUAw7EeCAUcBgGC58lA" source="_CI0L4Aw7EeCAUcBgGC58lA" target="_E5MBwAw7EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelDomain/modelElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ZSL7MAw7EeCAUcBgGC58lA" source="_EWK0QAw7EeCAUcBgGC58lA" target="_D4lgYAw7EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_aHUzcAw7EeCAUcBgGC58lA" source="_EWK0QAw7EeCAUcBgGC58lA" target="_E5MBwAw7EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_An4QsAxTEeCp2NlniWjADQ" source="__0mGgAw6EeCAUcBgGC58lA" target="_CI0L4Aw7EeCAUcBgGC58lA"/>
      <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_B-JgEAxTEeCp2NlniWjADQ" expressionString="transformSource and modelDomain.oclIsKindOf(tgg::SourceModelDomain) or&#xA;transformTarget and modelDomain.oclIsKindOf(tgg::TargetModelDomain)" expressionLanguage="OCL"/>
      <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_UCrcgAxTEeCp2NlniWjADQ" expressionString="transformParentLinks and ml.modifier = tgg::TGGModifierEnumeration::NONE or&#xA;transformCreatedLinks and ml.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_8cRDUAw7EeCAUcBgGC58lA" incoming="_9UdPkAw7EeCAUcBgGC58lA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="transform source model links" uuid="_2rWWIAxSEeCp2NlniWjADQ" incoming="_0dPDUAxTEeCp2NlniWjADQ" outgoing="_0zpYMAxTEeCp2NlniWjADQ">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_2rW9NgxSEeCp2NlniWjADQ" outgoingStoryLinks="_AI0LMClUEeCNurcZhXhx4w _CQsdsClUEeCNurcZhXhx4w _Hk4UIClUEeCNurcZhXhx4w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_2rW9MwxSEeCp2NlniWjADQ" outgoingStoryLinks="_2rW9OgxSEeCp2NlniWjADQ _2rW9UAxSEeCp2NlniWjADQ _2rW9SwxSEeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mo1" uuid="_2rW9OQxSEeCp2NlniWjADQ" incomingStoryLinks="_2rW9PAxSEeCp2NlniWjADQ _AI0LMClUEeCNurcZhXhx4w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ml" uuid="_2rWWIgxSEeCp2NlniWjADQ" outgoingStoryLinks="_2rW9PAxSEeCp2NlniWjADQ _2rW9MgxSEeCp2NlniWjADQ _2rW9PgxSEeCp2NlniWjADQ" incomingStoryLinks="_CQsdsClUEeCNurcZhXhx4w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="mo2" uuid="_2rW9QAxSEeCp2NlniWjADQ" incomingStoryLinks="_2rW9MgxSEeCp2NlniWjADQ _Hk4UIClUEeCNurcZhXhx4w" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo1" uuid="_2rW9TQxSEeCp2NlniWjADQ" incomingStoryLinks="_2rW9OgxSEeCp2NlniWjADQ _2rWWIwxSEeCp2NlniWjADQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spl" uuid="_2rW9RgxSEeCp2NlniWjADQ" modifier="CREATE" outgoingStoryLinks="_2rWWIwxSEeCp2NlniWjADQ _2rW9QgxSEeCp2NlniWjADQ _2rW9NwxSEeCp2NlniWjADQ _2rW9OAxSEeCp2NlniWjADQ" incomingStoryLinks="_2rW9UAxSEeCp2NlniWjADQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="spo2" uuid="_2rWWJAxSEeCp2NlniWjADQ" incomingStoryLinks="_2rW9SwxSEeCp2NlniWjADQ _2rW9QgxSEeCp2NlniWjADQ">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="eReference" uuid="_2rW9RQxSEeCp2NlniWjADQ" incomingStoryLinks="_2rW9PgxSEeCp2NlniWjADQ _2rW9NwxSEeCp2NlniWjADQ">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_2rW9PQxSEeCp2NlniWjADQ" incomingStoryLinks="_2rW9OAxSEeCp2NlniWjADQ" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2rW9PAxSEeCp2NlniWjADQ" source="_2rWWIgxSEeCp2NlniWjADQ" target="_2rW9OQxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2rW9MgxSEeCp2NlniWjADQ" source="_2rWWIgxSEeCp2NlniWjADQ" target="_2rW9QAxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2rW9PgxSEeCp2NlniWjADQ" source="_2rWWIgxSEeCp2NlniWjADQ" target="_2rW9RQxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelLink/eReference"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2rW9OgxSEeCp2NlniWjADQ" source="_2rW9MwxSEeCp2NlniWjADQ" target="_2rW9TQxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2rW9UAxSEeCp2NlniWjADQ" modifier="CREATE" source="_2rW9MwxSEeCp2NlniWjADQ" target="_2rW9RgxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2rW9SwxSEeCp2NlniWjADQ" source="_2rW9MwxSEeCp2NlniWjADQ" target="_2rWWJAxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2rWWIwxSEeCp2NlniWjADQ" modifier="CREATE" source="_2rW9RgxSEeCp2NlniWjADQ" target="_2rW9TQxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2rW9QgxSEeCp2NlniWjADQ" modifier="CREATE" source="_2rW9RgxSEeCp2NlniWjADQ" target="_2rWWJAxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2rW9NwxSEeCp2NlniWjADQ" modifier="CREATE" source="_2rW9RgxSEeCp2NlniWjADQ" target="_2rW9RQxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_2rW9OAxSEeCp2NlniWjADQ" modifier="CREATE" source="_2rW9RgxSEeCp2NlniWjADQ" target="_2rW9PQxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_AI0LMClUEeCNurcZhXhx4w" source="_2rW9NgxSEeCp2NlniWjADQ" target="_2rW9OQxSEeCp2NlniWjADQ" valueTarget="_2rW9TQxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_CQsdsClUEeCNurcZhXhx4w" modifier="CREATE" source="_2rW9NgxSEeCp2NlniWjADQ" target="_2rWWIgxSEeCp2NlniWjADQ" valueTarget="_2rW9RgxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_Hk4UIClUEeCNurcZhXhx4w" source="_2rW9NgxSEeCp2NlniWjADQ" target="_2rW9QAxSEeCp2NlniWjADQ" valueTarget="_2rWWJAxSEeCp2NlniWjADQ">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <edges uuid="_-zB70Aw6EeCAUcBgGC58lA" source="_v06WsAw6EeCAUcBgGC58lA" target="_wQ_1wAw6EeCAUcBgGC58lA"/>
    <edges uuid="_9UdPkAw7EeCAUcBgGC58lA" source="_8bigAAw6EeCAUcBgGC58lA" target="_8cRDUAw7EeCAUcBgGC58lA" guardType="END"/>
    <edges uuid="_et2lwAxTEeCp2NlniWjADQ" source="_wQ_1wAw6EeCAUcBgGC58lA" target="_8bigAAw6EeCAUcBgGC58lA"/>
    <edges uuid="_0dPDUAxTEeCp2NlniWjADQ" source="_8bigAAw6EeCAUcBgGC58lA" target="_2rWWIAxSEeCp2NlniWjADQ" guardType="FOR_EACH"/>
    <edges uuid="_0zpYMAxTEeCp2NlniWjADQ" source="_2rWWIAxSEeCp2NlniWjADQ" target="_8bigAAw6EeCAUcBgGC58lA"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
