<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_ruleSet_rootCorrNode_link" uuid="_Bm8C4BQHEeCDO6mN3ETDMg">
  <activities name="create_ruleSet_rootCorrNode_link" uuid="_CHWoUBQHEeCDO6mN3ETDMg">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_EBv4cBQHEeCDO6mN3ETDMg" outgoing="_ecGQUBQHEeCDO6mN3ETDMg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_Ee02ABQHEeCDO6mN3ETDMg" incoming="_ecGQUBQHEeCDO6mN3ETDMg" outgoing="_fK7FMBQHEeCDO6mN3ETDMg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_Fzb4gBQHEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_HLJ5YBQHEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_Ho6zcBQHEeCDO6mN3ETDMg" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleSetEClass" uuid="_IFokoBQHEeCDO6mN3ETDMg" outgoingStoryLinks="_a5H40BQHEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_WeVvkBQHEeCDO6mN3ETDMg" expressionString="mote::rules::TGGRuleSet" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rootCorrNodeEReference" uuid="_JHecIBQHEeCDO6mN3ETDMg" incomingStoryLinks="_a5H40BQHEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_LaeZgBQHEeCDO6mN3ETDMg" expressionString="self.name = 'rootCorrNode'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_FY9boBQKEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_GG2cYBQKEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_a5H40BQHEeCDO6mN3ETDMg" source="_IFokoBQHEeCDO6mN3ETDMg" target="_JHecIBQHEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create link" uuid="_dm7i4BQHEeCDO6mN3ETDMg" incoming="_fK7FMBQHEeCDO6mN3ETDMg" outgoing="_vrbqIBQHEeCDO6mN3ETDMg" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_gow8MBQHEeCDO6mN3ETDMg" incomingStoryLinks="_p3Ee0BQHEeCDO6mN3ETDMg _q2QD4BQHEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_hSHbMBQHEeCDO6mN3ETDMg" outgoingStoryLinks="_p3Ee0BQHEeCDO6mN3ETDMg _qKXOQBQHEeCDO6mN3ETDMg _qj1eMBQHEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodeSpo" uuid="_iG8KYBQHEeCDO6mN3ETDMg" incomingStoryLinks="_qj1eMBQHEeCDO6mN3ETDMg _rH7CsBQHEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_iuXWwBQHEeCDO6mN3ETDMg" incomingStoryLinks="_sC8ggBQHEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="rootCorrNodeEReference" uuid="_jR1QMBQHEeCDO6mN3ETDMg" incomingStoryLinks="_rormYBQHEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="link" uuid="_oOUYUBQHEeCDO6mN3ETDMg" modifier="CREATE" outgoingStoryLinks="_q2QD4BQHEeCDO6mN3ETDMg _rH7CsBQHEeCDO6mN3ETDMg _rormYBQHEeCDO6mN3ETDMg _sC8ggBQHEeCDO6mN3ETDMg" incomingStoryLinks="_qKXOQBQHEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_QOzj0BQKEeCDO6mN3ETDMg" outgoingStoryLinks="_ceSYcBQKEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_QvhrQBQKEeCDO6mN3ETDMg" outgoingStoryLinks="_cycDgBQKEeCDO6mN3ETDMg" incomingStoryLinks="_ceSYcBQKEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNode" uuid="_RSxiQBQKEeCDO6mN3ETDMg" incomingStoryLinks="_cycDgBQKEeCDO6mN3ETDMg _A8MMEClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fW6LYBQKEeCDO6mN3ETDMg" expressionString="self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_TCE8sBQKEeCDO6mN3ETDMg" outgoingStoryLinks="_A8MMEClYEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_p3Ee0BQHEeCDO6mN3ETDMg" source="_hSHbMBQHEeCDO6mN3ETDMg" target="_gow8MBQHEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qKXOQBQHEeCDO6mN3ETDMg" modifier="CREATE" source="_hSHbMBQHEeCDO6mN3ETDMg" target="_oOUYUBQHEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_qj1eMBQHEeCDO6mN3ETDMg" source="_hSHbMBQHEeCDO6mN3ETDMg" target="_iG8KYBQHEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_q2QD4BQHEeCDO6mN3ETDMg" modifier="CREATE" source="_oOUYUBQHEeCDO6mN3ETDMg" target="_gow8MBQHEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_rH7CsBQHEeCDO6mN3ETDMg" modifier="CREATE" source="_oOUYUBQHEeCDO6mN3ETDMg" target="_iG8KYBQHEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_rormYBQHEeCDO6mN3ETDMg" modifier="CREATE" source="_oOUYUBQHEeCDO6mN3ETDMg" target="_jR1QMBQHEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_sC8ggBQHEeCDO6mN3ETDMg" modifier="CREATE" source="_oOUYUBQHEeCDO6mN3ETDMg" target="_iuXWwBQHEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ceSYcBQKEeCDO6mN3ETDMg" source="_QOzj0BQKEeCDO6mN3ETDMg" target="_QvhrQBQKEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_cycDgBQKEeCDO6mN3ETDMg" source="_QvhrQBQKEeCDO6mN3ETDMg" target="_RSxiQBQKEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_A8MMEClYEeCFgb_w6FqGaA" source="_TCE8sBQKEeCDO6mN3ETDMg" target="_RSxiQBQKEeCDO6mN3ETDMg" valueTarget="_iG8KYBQHEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_u9rzUBQHEeCDO6mN3ETDMg" incoming="_vrbqIBQHEeCDO6mN3ETDMg"/>
    <edges uuid="_ecGQUBQHEeCDO6mN3ETDMg" source="_EBv4cBQHEeCDO6mN3ETDMg" target="_Ee02ABQHEeCDO6mN3ETDMg"/>
    <edges uuid="_fK7FMBQHEeCDO6mN3ETDMg" source="_Ee02ABQHEeCDO6mN3ETDMg" target="_dm7i4BQHEeCDO6mN3ETDMg"/>
    <edges uuid="_vrbqIBQHEeCDO6mN3ETDMg" source="_dm7i4BQHEeCDO6mN3ETDMg" target="_u9rzUBQHEeCDO6mN3ETDMg" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
