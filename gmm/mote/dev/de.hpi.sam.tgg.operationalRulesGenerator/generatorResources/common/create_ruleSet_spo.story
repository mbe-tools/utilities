<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_ruleSet_spo" uuid="_XruyMAwkEeCU3uEhqcP0Dg">
  <activities name="create_ruleSet_spo" uuid="_ZDP-wAwkEeCU3uEhqcP0Dg">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_bKtagAwkEeCU3uEhqcP0Dg" outgoing="_yvG48AwkEeCU3uEhqcP0Dg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create ruleSet story pattern object" uuid="_bp7tIAwkEeCU3uEhqcP0Dg" incoming="_zKTnMAwkEeCU3uEhqcP0Dg" outgoing="_zn9zkAwkEeCU3uEhqcP0Dg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_mnQWUAwkEeCU3uEhqcP0Dg" modifier="CREATE" outgoingStoryLinks="_xDkkMAwkEeCU3uEhqcP0Dg _-g9WYAwkEeCU3uEhqcP0Dg" incomingStoryLinks="_woJMcAwkEeCU3uEhqcP0Dg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_oX7pwAwkEeCU3uEhqcP0Dg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_pd6aYAwkEeCU3uEhqcP0Dg" expressionString="'ruleSet'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_sWWAEAwkEeCU3uEhqcP0Dg" outgoingStoryLinks="_woJMcAwkEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="bindingType" uuid="_tLs6wAwkEeCU3uEhqcP0Dg" incomingStoryLinks="_xDkkMAwkEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetEClass" uuid="_2MRjYAwkEeCU3uEhqcP0Dg" incomingStoryLinks="_-g9WYAwkEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7-CtsAwkEeCU3uEhqcP0Dg" expressionString="mote::rules::TGGRuleSet" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_3adNwAwkEeCU3uEhqcP0Dg" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_woJMcAwkEeCU3uEhqcP0Dg" modifier="CREATE" source="_sWWAEAwkEeCU3uEhqcP0Dg" target="_mnQWUAwkEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xDkkMAwkEeCU3uEhqcP0Dg" modifier="CREATE" source="_mnQWUAwkEeCU3uEhqcP0Dg" target="_tLs6wAwkEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_-g9WYAwkEeCU3uEhqcP0Dg" modifier="CREATE" source="_mnQWUAwkEeCU3uEhqcP0Dg" target="_2MRjYAwkEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_donnYAwkEeCU3uEhqcP0Dg" incoming="_yvG48AwkEeCU3uEhqcP0Dg" outgoing="_zKTnMAwkEeCU3uEhqcP0Dg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="bindingType" uuid="_e31MgAwkEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_kcM0AAwkEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_eW-iMAwkEeCU3uEhqcP0Dg" incoming="_zn9zkAwkEeCU3uEhqcP0Dg">
      <outParameterValues xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_0MjHUAwkEeCU3uEhqcP0Dg" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
    </nodes>
    <edges uuid="_yvG48AwkEeCU3uEhqcP0Dg" source="_bKtagAwkEeCU3uEhqcP0Dg" target="_donnYAwkEeCU3uEhqcP0Dg"/>
    <edges uuid="_zKTnMAwkEeCU3uEhqcP0Dg" source="_donnYAwkEeCU3uEhqcP0Dg" target="_bp7tIAwkEeCU3uEhqcP0Dg"/>
    <edges uuid="_zn9zkAwkEeCU3uEhqcP0Dg" source="_bp7tIAwkEeCU3uEhqcP0Dg" target="_eW-iMAwkEeCU3uEhqcP0Dg"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
