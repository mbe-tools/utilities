<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_returnValue_expression" uuid="_o_ziwPxlEd--caqh5zaNfg">
  <activities name="create_returnValue_expression" uuid="_qoFvMPxlEd--caqh5zaNfg">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_sx24EPxlEd--caqh5zaNfg" outgoing="_aZT-APxmEd--caqh5zaNfg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create expression" uuid="_t0KCkPxlEd--caqh5zaNfg" incoming="_Tq9fkBTkEeCBLYx6WVJ0aw" outgoing="_a2r2gPxmEd--caqh5zaNfg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_xd1fAPxlEd--caqh5zaNfg" modifier="CREATE" outgoingStoryLinks="_AREp0PxmEd--caqh5zaNfg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="varDecl" uuid="_1zsE8PxlEd--caqh5zaNfg" modifier="CREATE" outgoingStoryLinks="_F0qOMPxmEd--caqh5zaNfg" incomingStoryLinks="_AREp0PxmEd--caqh5zaNfg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableDeclarationAction"/>
        <attributeAssignments uuid="_HYzhgPxmEd--caqh5zaNfg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JCifcPxmEd--caqh5zaNfg" expressionString="'__returnValue'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_K-nKkPxmEd--caqh5zaNfg">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NyAfQPxmEd--caqh5zaNfg" expressionString="mote::rules::TransformationResult" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="literalDecl" uuid="_4YBC4PxlEd--caqh5zaNfg" modifier="CREATE" incomingStoryLinks="_GVJsIPxmEd--caqh5zaNfg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction"/>
        <attributeAssignments uuid="_QUIdwPxmEd--caqh5zaNfg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction/literal"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_dvvFQBTmEeCckZwKGfHj3Q">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_eWsXkBTmEeCckZwKGfHj3Q" methodClassName="de.hpi.sam.mote.rules.TransformationResult" methodName="getLiteral">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
              <instanceVariable xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hlz-8BTmEeCckZwKGfHj3Q" expressionString="resultLiteral" expressionLanguage="OCL"/>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
        <attributeAssignments uuid="_UOgooPxmEd--caqh5zaNfg">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_VJBJEPxmEd--caqh5zaNfg" expressionString="mote::rules::TransformationResult" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="literalCae" uuid="_EBckIPxmEd--caqh5zaNfg" modifier="CREATE" outgoingStoryLinks="_GVJsIPxmEd--caqh5zaNfg" incomingStoryLinks="_F0qOMPxmEd--caqh5zaNfg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_AREp0PxmEd--caqh5zaNfg" modifier="CREATE" source="_xd1fAPxlEd--caqh5zaNfg" target="_1zsE8PxlEd--caqh5zaNfg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_F0qOMPxmEd--caqh5zaNfg" modifier="CREATE" source="_1zsE8PxlEd--caqh5zaNfg" target="_EBckIPxmEd--caqh5zaNfg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableDeclarationAction/valueAssignment"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GVJsIPxmEd--caqh5zaNfg" modifier="CREATE" source="_EBckIPxmEd--caqh5zaNfg" target="_4YBC4PxlEd--caqh5zaNfg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_Zrhq8PxmEd--caqh5zaNfg" incoming="_a2r2gPxmEd--caqh5zaNfg">
      <outParameterValues xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bduBUPxmEd--caqh5zaNfg" expressionString="cae" expressionLanguage="OCL"/>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_PufC8BTkEeCBLYx6WVJ0aw" incoming="_aZT-APxmEd--caqh5zaNfg" outgoing="_Tq9fkBTkEeCBLYx6WVJ0aw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="resultLiteral" uuid="_KbrFgBTkEeCBLYx6WVJ0aw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://mote/1.0#//rules/TransformationResult"/>
      </storyPatternObjects>
    </nodes>
    <edges uuid="_aZT-APxmEd--caqh5zaNfg" source="_sx24EPxlEd--caqh5zaNfg" target="_PufC8BTkEeCBLYx6WVJ0aw"/>
    <edges uuid="_a2r2gPxmEd--caqh5zaNfg" source="_t0KCkPxlEd--caqh5zaNfg" target="_Zrhq8PxmEd--caqh5zaNfg"/>
    <edges uuid="_Tq9fkBTkEeCBLYx6WVJ0aw" source="_PufC8BTkEeCBLYx6WVJ0aw" target="_t0KCkPxlEd--caqh5zaNfg"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
