<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_corrNode_ruleSetLink" uuid="_WBsMoBQDEeCDO6mN3ETDMg">
  <activities name="create_corrNode_ruleSetLink" uuid="_XbROsBQDEeCDO6mN3ETDMg">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_ZzMIUBQDEeCDO6mN3ETDMg" outgoing="_5h51cBQGEeCDO6mN3ETDMg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_aSLxcBQDEeCDO6mN3ETDMg" incoming="_5h51cBQGEeCDO6mN3ETDMg" outgoing="_6fAW4BQGEeCDO6mN3ETDMg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_cCYjwBQDEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_cv6YIBQDEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_jEU8gBQDEeCDO6mN3ETDMg" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggNodeEClass" uuid="_l3NT0BQDEeCDO6mN3ETDMg" outgoingStoryLinks="_uWRx4BQDEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_oE1pABQDEeCDO6mN3ETDMg" expressionString="mote::TGGNode" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetEReference" uuid="_shNtsBQDEeCDO6mN3ETDMg" incomingStoryLinks="_uWRx4BQDEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_wpQhIBQDEeCDO6mN3ETDMg" expressionString="self.name = 'ruleSet'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_Tac-cBQgEeCkcYaoegFQZw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_UdnroBQgEeCkcYaoegFQZw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_uWRx4BQDEeCDO6mN3ETDMg" source="_l3NT0BQDEeCDO6mN3ETDMg" target="_shNtsBQDEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create link" uuid="_zyDeEBQDEeCDO6mN3ETDMg" incoming="_6fAW4BQGEeCDO6mN3ETDMg" outgoing="_7Xk9oBQGEeCDO6mN3ETDMg" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNodeSpo" uuid="_0d20IBQDEeCDO6mN3ETDMg" incomingStoryLinks="_xTfCgBQGEeCDO6mN3ETDMg _ywP7cBQGEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_1EAOIBQDEeCDO6mN3ETDMg" incomingStoryLinks="_yAVhcBQGEeCDO6mN3ETDMg _zWxI4BQGEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_1rP0UBQDEeCDO6mN3ETDMg" outgoingStoryLinks="_xTfCgBQGEeCDO6mN3ETDMg _xnBCgBQGEeCDO6mN3ETDMg _yAVhcBQGEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="link" uuid="_2ccAsBQDEeCDO6mN3ETDMg" modifier="CREATE" outgoingStoryLinks="_ywP7cBQGEeCDO6mN3ETDMg _zWxI4BQGEeCDO6mN3ETDMg _1RLAEBQGEeCDO6mN3ETDMg _1sOkYBQGEeCDO6mN3ETDMg" incomingStoryLinks="_xnBCgBQGEeCDO6mN3ETDMg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_qGOKkBQGEeCDO6mN3ETDMg" incomingStoryLinks="_1sOkYBQGEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetEReference" uuid="_qwMUoBQGEeCDO6mN3ETDMg" incomingStoryLinks="_1RLAEBQGEeCDO6mN3ETDMg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="traceabilityLinkStore" uuid="_baYCYBQgEeCkcYaoegFQZw" outgoingStoryLinks="_5mab4ClYEeCFgb_w6FqGaA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRule" uuid="_c5jWIBQgEeCkcYaoegFQZw" outgoingStoryLinks="_oMJrwBQgEeCkcYaoegFQZw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//TGGRule"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrDomain" uuid="_dd1u8BQgEeCkcYaoegFQZw" outgoingStoryLinks="_olQvUBQgEeCkcYaoegFQZw" incomingStoryLinks="_oMJrwBQgEeCkcYaoegFQZw">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="corrNode" uuid="_eBJ3YBQgEeCkcYaoegFQZw" incomingStoryLinks="_olQvUBQgEeCkcYaoegFQZw _5mab4ClYEeCFgb_w6FqGaA">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceNode"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_kTCsMBQgEeCkcYaoegFQZw" expressionString="self.modifier = tgg::TGGModifierEnumeration::CREATE" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xTfCgBQGEeCDO6mN3ETDMg" source="_1rP0UBQDEeCDO6mN3ETDMg" target="_0d20IBQDEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_xnBCgBQGEeCDO6mN3ETDMg" modifier="CREATE" source="_1rP0UBQDEeCDO6mN3ETDMg" target="_2ccAsBQDEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_yAVhcBQGEeCDO6mN3ETDMg" source="_1rP0UBQDEeCDO6mN3ETDMg" target="_1EAOIBQDEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ywP7cBQGEeCDO6mN3ETDMg" modifier="CREATE" source="_2ccAsBQDEeCDO6mN3ETDMg" target="_0d20IBQDEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_zWxI4BQGEeCDO6mN3ETDMg" modifier="CREATE" source="_2ccAsBQDEeCDO6mN3ETDMg" target="_1EAOIBQDEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1RLAEBQGEeCDO6mN3ETDMg" modifier="CREATE" source="_2ccAsBQDEeCDO6mN3ETDMg" target="_qwMUoBQGEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1sOkYBQGEeCDO6mN3ETDMg" modifier="CREATE" source="_2ccAsBQDEeCDO6mN3ETDMg" target="_qGOKkBQGEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_oMJrwBQgEeCkcYaoegFQZw" source="_c5jWIBQgEeCkcYaoegFQZw" target="_dd1u8BQgEeCkcYaoegFQZw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//TGGRule/correspondenceDomain"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_olQvUBQgEeCkcYaoegFQZw" source="_dd1u8BQgEeCkcYaoegFQZw" target="_eBJ3YBQgEeCkcYaoegFQZw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//CorrespondenceDomain/correspondenceElements"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:MapEntryStoryPatternLink" uuid="_5mab4ClYEeCFgb_w6FqGaA" source="_baYCYBQgEeCkcYaoegFQZw" target="_eBJ3YBQgEeCkcYaoegFQZw" valueTarget="_0d20IBQDEeCDO6mN3ETDMg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://operationalRulesGenerator/1.0#//generationStrategies/TraceabilityLinkStore/mappings"/>
        <classifier xsi:type="ecore:EClass" href="http://mdelab/workflow/1.0#//helpers/MapEntry"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_4gElABQGEeCDO6mN3ETDMg" incoming="_7Xk9oBQGEeCDO6mN3ETDMg"/>
    <edges uuid="_5h51cBQGEeCDO6mN3ETDMg" source="_ZzMIUBQDEeCDO6mN3ETDMg" target="_aSLxcBQDEeCDO6mN3ETDMg"/>
    <edges uuid="_6fAW4BQGEeCDO6mN3ETDMg" source="_aSLxcBQDEeCDO6mN3ETDMg" target="_zyDeEBQDEeCDO6mN3ETDMg"/>
    <edges uuid="_7Xk9oBQGEeCDO6mN3ETDMg" source="_zyDeEBQDEeCDO6mN3ETDMg" target="_4gElABQGEeCDO6mN3ETDMg" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
