<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_storyPattern_matchRuleSet" uuid="_xEYksEQYEeCj3vsEcE-cfw">
  <activities name="create_storyPattern_matchRuleSet" uuid="_yGfh8EQYEeCj3vsEcE-cfw">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_0OHVwEQYEeCj3vsEcE-cfw" outgoing="_3kxTEEQZEeCj3vsEcE-cfw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_09CRQEQYEeCj3vsEcE-cfw" incoming="_3kxTEEQZEeCj3vsEcE-cfw" outgoing="_35wroEQZEeCj3vsEcE-cfw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_1oFZYEQYEeCj3vsEcE-cfw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleSetEClass" uuid="_5KHVsEQYEeCj3vsEcE-cfw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_7OjSoEQYEeCj3vsEcE-cfw" expressionString="mote::rules::TGGRuleSet" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="isAxiom" uuid="_EzGyEEQZEeCj3vsEcE-cfw" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create this spo" uuid="_9plKoEQYEeCj3vsEcE-cfw" incoming="_35wroEQZEeCj3vsEcE-cfw" outgoing="_6C7XkEQZEeCj3vsEcE-cfw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisSpo" uuid="_-wr8oEQYEeCj3vsEcE-cfw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_XeXXUEQZEeCj3vsEcE-cfw">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_XuYwUEQZEeCj3vsEcE-cfw">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="create_rule_spo.story#_Kpsp0AwlEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_ZN1J0EQZEeCj3vsEcE-cfw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bWT5QEQZEeCj3vsEcE-cfw" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="isAxiom" uuid="_ZQ4d0EQZEeCj3vsEcE-cfw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ddqAQEQZEeCj3vsEcE-cfw" expressionString="isAxiom" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetSpo" uuid="_fQyx0EQZEeCj3vsEcE-cfw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_mMk4MEQZEeCj3vsEcE-cfw">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_mfChMEQZEeCj3vsEcE-cfw">
            <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
            <activity href="create_ruleSet_spo.story#_ZDP-wAwkEeCU3uEhqcP0Dg"/>
            <parameters name="storyActionNode" uuid="_oH7xoEQZEeCj3vsEcE-cfw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_r2gVIEQZEeCj3vsEcE-cfw" expressionString="storyActionNode" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
            </parameters>
            <parameters name="bindingType" uuid="_s2cIIEQZEeCj3vsEcE-cfw">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_uEGOEEQZEeCj3vsEcE-cfw" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::UNBOUND" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ExpressionActivityNode" name="create rule->ruleSet link" uuid="_4fiSMEQZEeCj3vsEcE-cfw" incoming="_6C7XkEQZEeCj3vsEcE-cfw" outgoing="_Ugh3wEQaEeCj3vsEcE-cfw">
      <expression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_6qNaAEQZEeCj3vsEcE-cfw">
        <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:CallStoryDiagramInterpreterAction" uuid="_BKEZYEQaEeCj3vsEcE-cfw">
          <activity href="create_rule_ruleSet_link.story#_qOEaEAwmEeCU3uEhqcP0Dg"/>
          <parameters name="ruleSpo" uuid="_H8oSUEQaEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_JwrC0EQaEeCj3vsEcE-cfw" expressionString="thisSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
          </parameters>
          <parameters name="ruleSetSpo" uuid="_H-TtUEQaEeCj3vsEcE-cfw">
            <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_NAaVQEQaEeCj3vsEcE-cfw" expressionString="ruleSetSpo" expressionLanguage="OCL"/>
            <parameterClassfier xsi:type="ecore:EClass" href="http://mote/1.0#//rules/TGGRuleSet"/>
          </parameters>
        </callActions>
      </expression>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_TxTaQEQaEeCj3vsEcE-cfw" incoming="_Ugh3wEQaEeCj3vsEcE-cfw"/>
    <edges uuid="_3kxTEEQZEeCj3vsEcE-cfw" source="_0OHVwEQYEeCj3vsEcE-cfw" target="_09CRQEQYEeCj3vsEcE-cfw"/>
    <edges uuid="_35wroEQZEeCj3vsEcE-cfw" source="_09CRQEQYEeCj3vsEcE-cfw" target="_9plKoEQYEeCj3vsEcE-cfw"/>
    <edges uuid="_6C7XkEQZEeCj3vsEcE-cfw" source="_9plKoEQYEeCj3vsEcE-cfw" target="_4fiSMEQZEeCj3vsEcE-cfw"/>
    <edges uuid="_Ugh3wEQaEeCj3vsEcE-cfw" source="_4fiSMEQZEeCj3vsEcE-cfw" target="_TxTaQEQaEeCj3vsEcE-cfw"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
