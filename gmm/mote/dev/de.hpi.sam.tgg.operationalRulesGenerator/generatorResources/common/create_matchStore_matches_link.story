<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_matchStore_matches_link" uuid="_4gQAcBjCEeCuQ-PF2tTDdw">
  <activities name="create_matchStore_matches_link" uuid="_57M7gBjCEeCuQ-PF2tTDdw">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_7zc2kBjCEeCuQ-PF2tTDdw" outgoing="_hdarEBjPEeCuQ-PF2tTDdw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_8VBSkBjCEeCuQ-PF2tTDdw" incoming="_hdarEBjPEeCuQ-PF2tTDdw" outgoing="_iRcJABjPEeCuQ-PF2tTDdw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchStoreSpo" uuid="_9fC1sBjCEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchSpo" uuid="_-NHcoBjCEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="__kwlABjCEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" uuid="_F3-PgBjDEeCuQ-PF2tTDdw" incoming="_iRcJABjPEeCuQ-PF2tTDdw" outgoing="_k5gXcBjPEeCuQ-PF2tTDdw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_G77m4BjDEeCuQ-PF2tTDdw" outgoingStoryLinks="_LjWAcBjPEeCuQ-PF2tTDdw _L3aL8BjPEeCuQ-PF2tTDdw _Ms968BjPEeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchStoreSpo" uuid="_IA-jYBjDEeCuQ-PF2tTDdw" incomingStoryLinks="_LjWAcBjPEeCuQ-PF2tTDdw _Uf6S0BjPEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchesSpl" uuid="_IvcL4BjDEeCuQ-PF2tTDdw" modifier="CREATE" outgoingStoryLinks="_RJ6nYBjPEeCuQ-PF2tTDdw _ThMn0BjPEeCuQ-PF2tTDdw _T7Ix0BjPEeCuQ-PF2tTDdw _Uf6S0BjPEeCuQ-PF2tTDdw" incomingStoryLinks="_L3aL8BjPEeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchSpo" uuid="_JrF70BjDEeCuQ-PF2tTDdw" incomingStoryLinks="_Ms968BjPEeCuQ-PF2tTDdw _T7Ix0BjPEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_K8WvYBjDEeCuQ-PF2tTDdw" incomingStoryLinks="_ThMn0BjPEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchStorageEClass" uuid="_Ldr60BjDEeCuQ-PF2tTDdw" outgoingStoryLinks="_STOY0BjPEeCuQ-PF2tTDdw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_cX8mEBjPEeCuQ-PF2tTDdw" expressionString="mote::rules::MatchStorage" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchesEReference" uuid="_MbtpUBjDEeCuQ-PF2tTDdw" incomingStoryLinks="_RJ6nYBjPEeCuQ-PF2tTDdw _STOY0BjPEeCuQ-PF2tTDdw">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EReference"/>
        <constraints xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_fL6igBjPEeCuQ-PF2tTDdw" expressionString="self.name = 'matches'" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_X9EWMBjPEeCuQ-PF2tTDdw" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_LjWAcBjPEeCuQ-PF2tTDdw" source="_G77m4BjDEeCuQ-PF2tTDdw" target="_IA-jYBjDEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_L3aL8BjPEeCuQ-PF2tTDdw" modifier="CREATE" source="_G77m4BjDEeCuQ-PF2tTDdw" target="_IvcL4BjDEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternLinks"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ms968BjPEeCuQ-PF2tTDdw" source="_G77m4BjDEeCuQ-PF2tTDdw" target="_JrF70BjDEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_RJ6nYBjPEeCuQ-PF2tTDdw" modifier="CREATE" source="_IvcL4BjDEeCuQ-PF2tTDdw" target="_MbtpUBjDEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternLink/eStructuralFeature"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_STOY0BjPEeCuQ-PF2tTDdw" source="_Ldr60BjDEeCuQ-PF2tTDdw" target="_MbtpUBjDEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://www.eclipse.org/emf/2002/Ecore#//EClass/eReferences"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_ThMn0BjPEeCuQ-PF2tTDdw" modifier="CREATE" source="_IvcL4BjDEeCuQ-PF2tTDdw" target="_K8WvYBjDEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_T7Ix0BjPEeCuQ-PF2tTDdw" modifier="CREATE" source="_IvcL4BjDEeCuQ-PF2tTDdw" target="_JrF70BjDEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/target"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Uf6S0BjPEeCuQ-PF2tTDdw" modifier="CREATE" source="_IvcL4BjDEeCuQ-PF2tTDdw" target="_IA-jYBjDEeCuQ-PF2tTDdw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternLink/source"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_j4gNcBjPEeCuQ-PF2tTDdw" incoming="_k5gXcBjPEeCuQ-PF2tTDdw"/>
    <edges uuid="_hdarEBjPEeCuQ-PF2tTDdw" source="_7zc2kBjCEeCuQ-PF2tTDdw" target="_8VBSkBjCEeCuQ-PF2tTDdw"/>
    <edges uuid="_iRcJABjPEeCuQ-PF2tTDdw" source="_8VBSkBjCEeCuQ-PF2tTDdw" target="_F3-PgBjDEeCuQ-PF2tTDdw"/>
    <edges uuid="_k5gXcBjPEeCuQ-PF2tTDdw" source="_F3-PgBjDEeCuQ-PF2tTDdw" target="_j4gNcBjPEeCuQ-PF2tTDdw"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
