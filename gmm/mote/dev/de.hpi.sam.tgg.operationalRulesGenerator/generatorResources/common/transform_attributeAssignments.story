<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.callActions="http://de/hpi/sam/storyDiagramEcore/callActions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="transform_attributeAssignments" uuid="_JA3W4Aw9EeCAUcBgGC58lA">
  <activities name="transform_attributeAssignments" uuid="_Kc8TUAw9EeCAUcBgGC58lA">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_MmeysAw9EeCAUcBgGC58lA" outgoing="_nqTlYAw9EeCAUcBgGC58lA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_NB0q4Aw9EeCAUcBgGC58lA" incoming="_nqTlYAw9EeCAUcBgGC58lA" outgoing="_oCFMMAw9EeCAUcBgGC58lA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_NmuHsAw9EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyPatternObject" uuid="_OQ9-kAw9EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="match attribute assignments" uuid="_TDzbcAw9EeCAUcBgGC58lA" incoming="_m5M4kAw9EeCAUcBgGC58lA _oCFMMAw9EeCAUcBgGC58lA" outgoing="_mAfu8Aw9EeCAUcBgGC58lA _puuUMAw9EeCAUcBgGC58lA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modelObject" uuid="_VL4iQAw9EeCAUcBgGC58lA" outgoingStoryLinks="_YBonYAw9EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http:///de/hpi/sam/tgg.ecore#//ModelObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="attributeAssignment" uuid="_Vu6W0Aw9EeCAUcBgGC58lA" incomingStoryLinks="_YBonYAw9EeCAUcBgGC58lA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AttributeAssignment"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_YBonYAw9EeCAUcBgGC58lA" source="_VL4iQAw9EeCAUcBgGC58lA" target="_Vu6W0Aw9EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http:///de/hpi/sam/tgg.ecore#//ModelObject/attributeAssignments"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="clone attribute assignment" uuid="_ZEGxAAw9EeCAUcBgGC58lA" incoming="_mAfu8Aw9EeCAUcBgGC58lA" outgoing="_m5M4kAw9EeCAUcBgGC58lA">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="clonedAttributeAssignment" uuid="_aFOPwAw9EeCAUcBgGC58lA" incomingStoryLinks="_koahsAw9EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AttributeAssignment"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_cMUfIAw9EeCAUcBgGC58lA">
          <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_c1EhMAw9EeCAUcBgGC58lA" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="copy">
            <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            <parameters name="" uuid="_gSwwEAw9EeCAUcBgGC58lA">
              <parameterValueAction xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hjTx8Aw9EeCAUcBgGC58lA" expressionString="attributeAssignment" expressionLanguage="OCL"/>
              <parameterClassfier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EObject"/>
            </parameters>
          </callActions>
        </directAssignmentExpression>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyPatternObject" uuid="_jNy90Aw9EeCAUcBgGC58lA" outgoingStoryLinks="_koahsAw9EeCAUcBgGC58lA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_koahsAw9EeCAUcBgGC58lA" modifier="CREATE" source="_jNy90Aw9EeCAUcBgGC58lA" target="_aFOPwAw9EeCAUcBgGC58lA">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/attributeAssignments"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_oiWAoAw9EeCAUcBgGC58lA" incoming="_7U-RkCorEeCjTcyrv1LVTA"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="set new UUIDs" uuid="_ueFesCorEeCjTcyrv1LVTA" incoming="_puuUMAw9EeCAUcBgGC58lA" outgoing="_7U-RkCorEeCjTcyrv1LVTA" forEach="true">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyPatternObject" uuid="_vMeOsCorEeCjTcyrv1LVTA" outgoingStoryLinks="_yxbjMCorEeCjTcyrv1LVTA" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="element" uuid="_xOKZoCorEeCjTcyrv1LVTA" incomingStoryLinks="_yxbjMCorEeCjTcyrv1LVTA">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement"/>
        <attributeAssignments uuid="_zBNroCorEeCjTcyrv1LVTA">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/uuid"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:CallActionExpression" uuid="_0Jka0CorEeCjTcyrv1LVTA">
            <callActions xsi:type="de.hpi.sam.storyDiagramEcore.callActions:MethodCallAction" uuid="_0uMx4CorEeCjTcyrv1LVTA" methodClassName="org.eclipse.emf.ecore.util.EcoreUtil" methodName="generateUUID">
              <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </callActions>
          </assignmentExpression>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternContainmentLink" uuid="_yxbjMCorEeCjTcyrv1LVTA" source="_vMeOsCorEeCjTcyrv1LVTA" target="_xOKZoCorEeCjTcyrv1LVTA"/>
    </nodes>
    <edges uuid="_mAfu8Aw9EeCAUcBgGC58lA" source="_TDzbcAw9EeCAUcBgGC58lA" target="_ZEGxAAw9EeCAUcBgGC58lA" guardType="FOR_EACH"/>
    <edges uuid="_m5M4kAw9EeCAUcBgGC58lA" source="_ZEGxAAw9EeCAUcBgGC58lA" target="_TDzbcAw9EeCAUcBgGC58lA"/>
    <edges uuid="_nqTlYAw9EeCAUcBgGC58lA" source="_MmeysAw9EeCAUcBgGC58lA" target="_NB0q4Aw9EeCAUcBgGC58lA"/>
    <edges uuid="_oCFMMAw9EeCAUcBgGC58lA" source="_NB0q4Aw9EeCAUcBgGC58lA" target="_TDzbcAw9EeCAUcBgGC58lA"/>
    <edges uuid="_puuUMAw9EeCAUcBgGC58lA" source="_TDzbcAw9EeCAUcBgGC58lA" target="_ueFesCorEeCjTcyrv1LVTA" guardType="END"/>
    <edges uuid="_7U-RkCorEeCjTcyrv1LVTA" source="_ueFesCorEeCjTcyrv1LVTA" target="_oiWAoAw9EeCAUcBgGC58lA" guardType="END"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
