<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_rule_spo" uuid="_Jl_KEAwlEeCU3uEhqcP0Dg">
  <activities name="create_rule_spo" uuid="_Kpsp0AwlEeCU3uEhqcP0Dg">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_MRuXkAwlEeCU3uEhqcP0Dg" outgoing="_SzDWkAwlEeCU3uEhqcP0Dg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_M-ORMAwlEeCU3uEhqcP0Dg" incoming="_SzDWkAwlEeCU3uEhqcP0Dg" outgoing="_w_SEoAwlEeCU3uEhqcP0Dg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_Nm6B0AwlEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="isAxiom" uuid="_nzFa0AwlEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create THIS story pattern object" uuid="_TXL-YAwlEeCU3uEhqcP0Dg" incoming="_w_SEoAwlEeCU3uEhqcP0Dg" outgoing="_yHMu8AwlEeCU3uEhqcP0Dg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_VV8xIAwlEeCU3uEhqcP0Dg" outgoingStoryLinks="_vw0GYAwlEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisSpo" uuid="_YDShIAwlEeCU3uEhqcP0Dg" modifier="CREATE" incomingStoryLinks="_vw0GYAwlEeCU3uEhqcP0Dg">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_Z9MpEAwlEeCU3uEhqcP0Dg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bLOicAwlEeCU3uEhqcP0Dg" expressionString="'this'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_dfYg8AwmEeCU3uEhqcP0Dg">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ew3-AAwmEeCU3uEhqcP0Dg" expressionString="storyDiagramEcore::sdm::BindingTypeEnumeration::BOUND" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_cIteYAwlEeCU3uEhqcP0Dg" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_vw0GYAwlEeCU3uEhqcP0Dg" modifier="CREATE" source="_VV8xIAwlEeCU3uEhqcP0Dg" target="_YDShIAwlEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:DecisionNode" uuid="_rLbPoAwlEeCU3uEhqcP0Dg" incoming="_yHMu8AwlEeCU3uEhqcP0Dg" outgoing="_CG7iEAwmEeCU3uEhqcP0Dg _C4wnoAwmEeCU3uEhqcP0Dg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="handle axiom" uuid="_r8AYAAwlEeCU3uEhqcP0Dg" incoming="_CG7iEAwmEeCU3uEhqcP0Dg" outgoing="_K1a8MAwmEeCU3uEhqcP0Dg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggAxiomEClass" uuid="_ch-S8AwlEeCU3uEhqcP0Dg" incomingStoryLinks="_1OnL0AwlEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hULdsAwlEeCU3uEhqcP0Dg" expressionString="mote::rules::TGGAxiom" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisSpo" uuid="_zTHvgAwlEeCU3uEhqcP0Dg" outgoingStoryLinks="_1OnL0AwlEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_1OnL0AwlEeCU3uEhqcP0Dg" modifier="CREATE" source="_zTHvgAwlEeCU3uEhqcP0Dg" target="_ch-S8AwlEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="handle rule" uuid="_tj9NQAwlEeCU3uEhqcP0Dg" incoming="_C4wnoAwmEeCU3uEhqcP0Dg" outgoing="_Jx5CoAwmEeCU3uEhqcP0Dg">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="thisSpo" uuid="_2gqCgAwlEeCU3uEhqcP0Dg" outgoingStoryLinks="_BV8KAAwmEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="tggRuleEClass" uuid="_4ieJQAwlEeCU3uEhqcP0Dg" incomingStoryLinks="_BV8KAAwmEeCU3uEhqcP0Dg" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="__V92UAwlEeCU3uEhqcP0Dg" expressionString="mote::rules::TGGRule" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_BV8KAAwmEeCU3uEhqcP0Dg" modifier="CREATE" source="_2gqCgAwlEeCU3uEhqcP0Dg" target="_4ieJQAwlEeCU3uEhqcP0Dg">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:MergeNode" uuid="_HY-DcAwmEeCU3uEhqcP0Dg" incoming="_Jx5CoAwmEeCU3uEhqcP0Dg _K1a8MAwmEeCU3uEhqcP0Dg" outgoing="_LmGyQAwmEeCU3uEhqcP0Dg"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_ILQcAAwmEeCU3uEhqcP0Dg" incoming="_LmGyQAwmEeCU3uEhqcP0Dg">
      <outParameterValues xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_MUCPQAwmEeCU3uEhqcP0Dg" expressionString="thisSpo" expressionLanguage="OCL"/>
    </nodes>
    <edges uuid="_SzDWkAwlEeCU3uEhqcP0Dg" source="_MRuXkAwlEeCU3uEhqcP0Dg" target="_M-ORMAwlEeCU3uEhqcP0Dg"/>
    <edges uuid="_w_SEoAwlEeCU3uEhqcP0Dg" source="_M-ORMAwlEeCU3uEhqcP0Dg" target="_TXL-YAwlEeCU3uEhqcP0Dg"/>
    <edges uuid="_yHMu8AwlEeCU3uEhqcP0Dg" source="_TXL-YAwlEeCU3uEhqcP0Dg" target="_rLbPoAwlEeCU3uEhqcP0Dg"/>
    <edges uuid="_CG7iEAwmEeCU3uEhqcP0Dg" source="_rLbPoAwlEeCU3uEhqcP0Dg" target="_r8AYAAwlEeCU3uEhqcP0Dg" guardType="BOOLEAN">
      <guardExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_EItzoAwmEeCU3uEhqcP0Dg" expressionString="isAxiom" expressionLanguage="OCL"/>
    </edges>
    <edges uuid="_C4wnoAwmEeCU3uEhqcP0Dg" source="_rLbPoAwlEeCU3uEhqcP0Dg" target="_tj9NQAwlEeCU3uEhqcP0Dg" guardType="ELSE"/>
    <edges uuid="_Jx5CoAwmEeCU3uEhqcP0Dg" source="_tj9NQAwlEeCU3uEhqcP0Dg" target="_HY-DcAwmEeCU3uEhqcP0Dg"/>
    <edges uuid="_K1a8MAwmEeCU3uEhqcP0Dg" source="_r8AYAAwlEeCU3uEhqcP0Dg" target="_HY-DcAwmEeCU3uEhqcP0Dg"/>
    <edges uuid="_LmGyQAwmEeCU3uEhqcP0Dg" source="_HY-DcAwmEeCU3uEhqcP0Dg" target="_ILQcAAwmEeCU3uEhqcP0Dg"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
