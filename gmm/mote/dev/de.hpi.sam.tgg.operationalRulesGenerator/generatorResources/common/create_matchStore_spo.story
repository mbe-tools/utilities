<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="create_matchStore_spo" uuid="_WQmOYBgAEeCaHaBYHoSzPw">
  <activities name="create_matchStore_spo" uuid="_X128ABgAEeCaHaBYHoSzPw">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_aa-kIBgAEeCaHaBYHoSzPw" outgoing="_mW48gBgAEeCaHaBYHoSzPw"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_a9ieoBgAEeCaHaBYHoSzPw" incoming="_mW48gBgAEeCaHaBYHoSzPw" outgoing="_VhkIsBgBEeCaHaBYHoSzPw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_b-rykBgAEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_YVC3gBiyEeCm4IbFa7I4cA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="bindingType" uuid="_ZaKFcBiyEeCm4IbFa7I4cA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create story pattern" uuid="_m2fCkBgAEeCaHaBYHoSzPw" incoming="_VhkIsBgBEeCaHaBYHoSzPw" outgoing="_WWIZMBgBEeCaHaBYHoSzPw">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="storyActionNode" uuid="_owpCIBgAEeCaHaBYHoSzPw" outgoingStoryLinks="_9cHLoBgAEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchStoreSpo" uuid="_swaDUBgAEeCaHaBYHoSzPw" modifier="CREATE" outgoingStoryLinks="_GFv68BgBEeCaHaBYHoSzPw _kvxS0BiyEeCm4IbFa7I4cA _laHQUBiyEeCm4IbFa7I4cA" incomingStoryLinks="_9cHLoBgAEeCaHaBYHoSzPw">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject"/>
        <attributeAssignments uuid="_xM0kQBgAEeCaHaBYHoSzPw">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//NamedElement/name"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_1TbNQBgAEeCaHaBYHoSzPw" expressionString="'matchStore'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="matchStorageEClass" uuid="_2pnkIBgAEeCaHaBYHoSzPw" incomingStoryLinks="_GFv68BgBEeCaHaBYHoSzPw" bindingType="BOUND">
        <classifier xsi:type="ecore:EClass" href="http://www.eclipse.org/emf/2002/Ecore#//EClass"/>
        <directAssignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_PtyMMBgBEeCaHaBYHoSzPw" expressionString="mote::rules::MatchStorage" expressionLanguage="OCL"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_IFTX4BgBEeCaHaBYHoSzPw" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="modifier" uuid="_hd1A8BiyEeCm4IbFa7I4cA" incomingStoryLinks="_kvxS0BiyEeCm4IbFa7I4cA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternModifierEnumeration"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="bindingType" uuid="_iI1s0BiyEeCm4IbFa7I4cA" incomingStoryLinks="_laHQUBiyEeCm4IbFa7I4cA" bindingType="BOUND">
        <classifier xsi:type="ecore:EEnum" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/BindingTypeEnumeration"/>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_9cHLoBgAEeCaHaBYHoSzPw" modifier="CREATE" source="_owpCIBgAEeCaHaBYHoSzPw" target="_swaDUBgAEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//nodes/StoryActionNode/storyPatternObjects"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_GFv68BgBEeCaHaBYHoSzPw" modifier="CREATE" source="_swaDUBgAEeCaHaBYHoSzPw" target="_2pnkIBgAEeCaHaBYHoSzPw">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/AbstractStoryPatternObject/classifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_kvxS0BiyEeCm4IbFa7I4cA" modifier="CREATE" source="_swaDUBgAEeCaHaBYHoSzPw" target="_hd1A8BiyEeCm4IbFa7I4cA">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternElement/modifier"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_laHQUBiyEeCm4IbFa7I4cA" modifier="CREATE" source="_swaDUBgAEeCaHaBYHoSzPw" target="_iI1s0BiyEeCm4IbFa7I4cA">
        <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//sdm/StoryPatternObject/bindingType"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_UWCJsBgBEeCaHaBYHoSzPw" incoming="_WWIZMBgBEeCaHaBYHoSzPw">
      <returnValue xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_6DXMQBiyEeCm4IbFa7I4cA" expressionString="matchStoreSpo" expressionLanguage="OCL"/>
    </nodes>
    <edges uuid="_mW48gBgAEeCaHaBYHoSzPw" source="_aa-kIBgAEeCaHaBYHoSzPw" target="_a9ieoBgAEeCaHaBYHoSzPw"/>
    <edges uuid="_VhkIsBgBEeCaHaBYHoSzPw" source="_a9ieoBgAEeCaHaBYHoSzPw" target="_m2fCkBgAEeCaHaBYHoSzPw"/>
    <edges uuid="_WWIZMBgBEeCaHaBYHoSzPw" source="_m2fCkBgAEeCaHaBYHoSzPw" target="_UWCJsBgBEeCaHaBYHoSzPw"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
