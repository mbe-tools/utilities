<?xml version="1.0" encoding="UTF-8"?>
<de.hpi.sam.storyDiagramEcore:ActivityDiagram xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:de.hpi.sam.storyDiagramEcore="http://de/hpi/sam/storyDiagramEcore.ecore" xmlns:de.hpi.sam.storyDiagramEcore.expressions="http://de/hpi/sam/storyDiagramEcore/expressions.ecore" xmlns:de.hpi.sam.storyDiagramEcore.nodes="http://de/hpi/sam/storyDiagramEcore/nodes.ecore" xmlns:de.hpi.sam.storyDiagramEcore.sdm="http://de/hpi/sam/storyDiagramEcore/sdm.ecore" xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" uuid="_o6I84PcFEd-_yp3IZC6S-Q">
  <activities name="create_printError_expression" uuid="_pwLNAPcFEd-_yp3IZC6S-Q">
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:InitialNode" uuid="_64djoPcFEd-_yp3IZC6S-Q" outgoing="_G1D4gPcGEd-_yp3IZC6S-Q"/>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="parameters" uuid="_7md5IPcFEd-_yp3IZC6S-Q" incoming="_G1D4gPcGEd-_yp3IZC6S-Q" outgoing="_d1tUMPcHEd-_yp3IZC6S-Q">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="message" uuid="_-vSEMPcFEd-_yp3IZC6S-Q" bindingType="BOUND">
        <classifier xsi:type="ecore:EDataType" href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="dummy" uuid="_zsugQPcGEd-_yp3IZC6S-Q" modifier="CREATE">
        <classifier xsi:type="ecore:EClass" href="http://mote/1.0#//helpers/ModificationTag"/>
      </storyPatternObjects>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:StoryActionNode" name="create call action expression" uuid="_IKWQcPcGEd-_yp3IZC6S-Q" incoming="_d1tUMPcHEd-_yp3IZC6S-Q" outgoing="_gQzdoPcHEd-_yp3IZC6S-Q">
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="cae" uuid="_JR_N8PcGEd-_yp3IZC6S-Q" modifier="CREATE" outgoingStoryLinks="_Q8KrYPcGEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="printMessageCall" uuid="_NMJWYPcGEd-_yp3IZC6S-Q" modifier="CREATE" outgoingStoryLinks="_trFG4PcGEd-_yp3IZC6S-Q _O14e4PcHEd-_yp3IZC6S-Q" incomingStoryLinks="_Q8KrYPcGEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction"/>
        <attributeAssignments uuid="_Z-N6sPcGEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction/methodClassName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bColEPcGEd-_yp3IZC6S-Q" expressionString="'de.hpi.sam.mote.rules.TGGRuleSet'" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_gVYREPcGEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction/methodName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hjMIAPcGEd-_yp3IZC6S-Q" expressionString="'printMessage'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetReference" uuid="_jvQ-APcGEd-_yp3IZC6S-Q" modifier="CREATE" incomingStoryLinks="_un4tYPcGEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction"/>
        <attributeAssignments uuid="_xS2e0PcGEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_Fz0BcPcHEd-_yp3IZC6S-Q" expressionString="mote::rules::TGGRuleSet" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_J3D0cPcHEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/VariableReferenceAction/variableName"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_K9Tq0PcHEd-_yp3IZC6S-Q" expressionString="'ruleSet'" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="ruleSetReferenceCAE" uuid="_rxAm4PcGEd-_yp3IZC6S-Q" modifier="CREATE" outgoingStoryLinks="_un4tYPcGEd-_yp3IZC6S-Q" incomingStoryLinks="_trFG4PcGEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="messageParameter" uuid="_MrVS4PcHEd-_yp3IZC6S-Q" modifier="CREATE" outgoingStoryLinks="_Ruqp4PcHEd-_yp3IZC6S-Q" incomingStoryLinks="_O14e4PcHEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter"/>
        <attributeAssignments uuid="_T3q9wPcHEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterClassfier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_U8GPMPcHEd-_yp3IZC6S-Q" expressionString="ecore::EString" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="parameterCAE" uuid="_Pu8N0PcHEd-_yp3IZC6S-Q" modifier="CREATE" outgoingStoryLinks="_c4v8sPcHEd-_yp3IZC6S-Q" incomingStoryLinks="_Ruqp4PcHEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression"/>
      </storyPatternObjects>
      <storyPatternObjects xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternObject" name="messageLiteral" uuid="_SSg90PcHEd-_yp3IZC6S-Q" modifier="CREATE" incomingStoryLinks="_c4v8sPcHEd-_yp3IZC6S-Q">
        <classifier xsi:type="ecore:EClass" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction"/>
        <attributeAssignments uuid="_YWhusPcHEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EAttribute" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/LiteralDeclarationAction/literal"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_ZjuhoPcHEd-_yp3IZC6S-Q" expressionString="message" expressionLanguage="OCL"/>
        </attributeAssignments>
        <attributeAssignments uuid="_aWL5UPcHEd-_yp3IZC6S-Q">
          <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallAction/classifier"/>
          <assignmentExpression xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_bZIkEPcHEd-_yp3IZC6S-Q" expressionString="ecore::EString" expressionLanguage="OCL"/>
        </attributeAssignments>
      </storyPatternObjects>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Q8KrYPcGEd-_yp3IZC6S-Q" modifier="CREATE" source="_JR_N8PcGEd-_yp3IZC6S-Q" target="_NMJWYPcGEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_trFG4PcGEd-_yp3IZC6S-Q" modifier="CREATE" source="_NMJWYPcGEd-_yp3IZC6S-Q" target="_rxAm4PcGEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction/instanceVariable"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_un4tYPcGEd-_yp3IZC6S-Q" modifier="CREATE" source="_rxAm4PcGEd-_yp3IZC6S-Q" target="_jvQ-APcGEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_O14e4PcHEd-_yp3IZC6S-Q" modifier="CREATE" source="_NMJWYPcGEd-_yp3IZC6S-Q" target="_MrVS4PcHEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/MethodCallAction/parameters"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_Ruqp4PcHEd-_yp3IZC6S-Q" modifier="CREATE" source="_MrVS4PcHEd-_yp3IZC6S-Q" target="_Pu8N0PcHEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//callActions/CallActionParameter/parameterValueAction"/>
      </storyPatternLinks>
      <storyPatternLinks xsi:type="de.hpi.sam.storyDiagramEcore.sdm:StoryPatternLink" uuid="_c4v8sPcHEd-_yp3IZC6S-Q" modifier="CREATE" source="_Pu8N0PcHEd-_yp3IZC6S-Q" target="_SSg90PcHEd-_yp3IZC6S-Q">
        <eStructuralFeature xsi:type="ecore:EReference" href="http://de/hpi/sam/storyDiagramEcore.ecore#//expressions/CallActionExpression/callActions"/>
      </storyPatternLinks>
    </nodes>
    <nodes xsi:type="de.hpi.sam.storyDiagramEcore.nodes:ActivityFinalNode" uuid="_eWbboPcHEd-_yp3IZC6S-Q" incoming="_gQzdoPcHEd-_yp3IZC6S-Q">
      <returnValue xsi:type="de.hpi.sam.storyDiagramEcore.expressions:StringExpression" uuid="_hkjkgPcHEd-_yp3IZC6S-Q" expressionString="cae" expressionLanguage="OCL"/>
    </nodes>
    <edges uuid="_G1D4gPcGEd-_yp3IZC6S-Q" source="_64djoPcFEd-_yp3IZC6S-Q" target="_7md5IPcFEd-_yp3IZC6S-Q"/>
    <edges uuid="_d1tUMPcHEd-_yp3IZC6S-Q" source="_7md5IPcFEd-_yp3IZC6S-Q" target="_IKWQcPcGEd-_yp3IZC6S-Q"/>
    <edges uuid="_gQzdoPcHEd-_yp3IZC6S-Q" source="_IKWQcPcGEd-_yp3IZC6S-Q" target="_eWbboPcHEd-_yp3IZC6S-Q"/>
  </activities>
</de.hpi.sam.storyDiagramEcore:ActivityDiagram>
