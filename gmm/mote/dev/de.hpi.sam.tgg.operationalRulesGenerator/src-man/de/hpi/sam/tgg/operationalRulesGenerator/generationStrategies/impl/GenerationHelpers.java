package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl;

import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.operationalRulesGenerator.jet.RuleCallStoryDiagramTemplate;
import de.hpi.sam.tgg.operationalRulesGenerator.jet.RuleSetCreateRulesTemplate;

public class GenerationHelpers
{
	public static String firstLetterToUpperCase(String s)
	{
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

	public static String generateCreateRulesCode(	TGGDiagram tggDiagram,
													EPackage rulesPackage,
													String basePackage ) {
		RuleSetCreateRulesTemplate t = new RuleSetCreateRulesTemplate();

		return t.generate(tggDiagram, rulesPackage, basePackage);
	}

	public static String generateRullCallStoryDiagramCode(String projectName, String ruleSetName, String activityDiagramName,
			EOperation eOperation)
	{
		RuleCallStoryDiagramTemplate t = new RuleCallStoryDiagramTemplate();

		return t.generate(projectName, ruleSetName, activityDiagramName, eOperation, "");
	}
//
//	public static String generateAcceptsParentNodeCode(String basePackage, TGGRule tggRule)
//	{
//		AcceptsParentCorrNodeOperationTemplate t = new AcceptsParentCorrNodeOperationTemplate();
//		return t.generate(basePackage, tggRule);
//	}
}
