package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.synchronization;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.tgg.CorrespondenceElement;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGModifierEnumeration;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleGenerationException;
import de.hpi.sam.tgg.operationalRulesGenerator.jet.RuleCallStoryDiagramTemplate;
import de.hpi.sam.tgg.operationalRulesGenerator.jet.RuleSetCreateRulesTemplate;

public abstract class GenerationStrategy
{
	private static final String		SDM_INTERPRETER					= "SDMInterpreter";
	protected static final String	MODIFICATION_TAG				= "modificationTag";
	protected static final String	TRANSFORMATION_QUEUE			= "transformationQueue";
	protected static final String	ECORE_GENMODEL_NSURI			= "http://www.eclipse.org/emf/2002/GenModel";
	protected static final String	ANNOTATION_DOCUMENTATION_KEY	= "documentation";

	public static final String		FORWARD_METHOD_NAME				= "forward";
	public static final String		MAPPING_METHOD_NAME				= "mapping";
	public static final String		REVERSE_METHOD_NAME				= "reverse";

	protected static final String	FORWARD_TRANSFORMATION			= "forwardTransformation";
	protected static final String	MAPPING_TRANSFORMATION			= "mappingTransformation";
	protected static final String	REVERSE_TRANSFORMATION			= "reverseTransformation";

	protected static final String	FORWARD_SYNCHRONIZATION			= "forwardSynchronization";
	protected static final String	MAPPING_SYNCHRONIZATION			= "mappingSynchronization";
	protected static final String	REVERSE_SYNCHRONIZATION			= "reverseSynchronization";

	protected static final String	ANNOTATION_BODY_KEY				= "body";
	protected static final String	RULE_SET						= "ruleSet";
	protected static final String	ACTIVITIES_REFERENCE_NAME		= "activities";
	protected static final String	CREATE_RULES_OPERATION_NAME		= "createRules";
	protected static final String	THIS							= "this";
	protected static final String	UPDATE_DEPTH_OPERATION_NAME		= "updateDepth";
	protected static final String	ADD_OPERATION_NAME				= "add";
	protected static final String	IS_APPLICABLE_FOR_CORR_NODE		= "isApplicableForCorrNode";
	protected static final String	CORR_NODE						= "corrNode";
	protected static final String	CHILD_CORR_NODE_TO_CHECK		= "childCorrNodeToCheck";
	protected static final String	HAS_MULTIPLE_CONTEXT_CORR_NODES	= "hasMultipleContextCorrNodes";

	public abstract void createRules(String projectURI, String javaBasePackage, EPackage rulesPackage, TGGDiagram tggDiagram, EList<TGGRule> rules)
			throws RuleGenerationException;

	/**
	 * Returns the first EOperation from the list with the given name or null if
	 * there is no EOperation with that name.
	 * 
	 * @param operations
	 * @param operationName
	 * @return
	 */
	public static EOperation getEOperation(Collection<EOperation> operations, String operationName)
	{
		for (EOperation eOperation : operations)
		{
			if (eOperation.getName().equals(operationName))
			{
				return eOperation;
			}
		}
		return null;
	}

	/**
	 * Returns the Mote and StoryDiagram genmodels.
	 * 
	 * @return
	 */
	public List<GenModel> getReferencedGenModels()
	{
		List<GenModel> genModels = new LinkedList<GenModel>();

		ResourceSet resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.getResource(URI.createPlatformPluginURI("/de.hpi.sam.mote/model/mote.genmodel", true), true);

		genModels.add((GenModel) resource.getContents().get(0));

		resource = resourceSet.getResource(
				URI.createPlatformPluginURI("/de.hpi.sam.storyDiagramEcore/model/storyDiagramEcore.genmodel", true), true);

		genModels.add((GenModel) resource.getContents().get(0));

		return genModels;
	}

	/**
	 * This method reads the packages from the MoTE, StoryDiagram and Ecore meta
	 * model files and returns a map, that maps the nsPrefix of the packages to
	 * the EPackage.
	 * 
	 * @param rs
	 * @return
	 * @throws RuleGenerationException
	 */
	protected Map<String, EPackage> getRequiredPackages(ResourceSet rs) throws RuleGenerationException
	{
		/*
		 * Open the mote ecore file
		 */
		Resource moteMetaModelResource = rs.createResource(URI.createPlatformPluginURI("/de.hpi.sam.mote/model/mote.ecore", true));

		try
		{
			moteMetaModelResource.load(Collections.EMPTY_MAP);
		}
		catch (IOException e1)
		{
			e1.printStackTrace();

			throw new RuleGenerationException("Could not load MoTE meta model from " + moteMetaModelResource.getURI() + ".");
		}

		/*
		 * Get the subpackages from the motePackage that will be needed later
		 * on.
		 */
		Map<String, EPackage> packages = new HashMap<String, EPackage>();
		EPackage motePackage = (EPackage) moteMetaModelResource.getContents().get(0);

		packages.put(motePackage.getNsPrefix(), motePackage);

		for (EPackage subpackage : motePackage.getESubpackages())
		{
			packages.put(subpackage.getNsPrefix(), subpackage);
		}

		/*
		 * Open the storydiagram meta model ecore file
		 */
		Resource sdMetaModelResource = rs.createResource(URI.createPlatformPluginURI(
				"/de.hpi.sam.storyDiagramEcore/model/storyDiagramEcore.ecore", true));

		try
		{
			sdMetaModelResource.load(Collections.EMPTY_MAP);
		}
		catch (IOException e)
		{
			e.printStackTrace();

			throw new RuleGenerationException("Could not load StoryDiagramEcore meta model from " + sdMetaModelResource.getURI() + ".");
		}

		/*
		 * Get subpackages
		 */
		EPackage sdPackage = (EPackage) sdMetaModelResource.getContents().get(0);

		packages.put(sdPackage.getNsPrefix(), sdPackage);

		for (EPackage subpackage : sdPackage.getESubpackages())
		{
			packages.put(subpackage.getNsPrefix(), subpackage);
		}

		/*
		 * Open the Ecore meta model ecore file
		 */
		Resource ecoreMetaModelResource = rs.createResource(URI.createPlatformPluginURI("/org.eclipse.emf.ecore/model/Ecore.ecore", true));

		try
		{
			ecoreMetaModelResource.load(Collections.EMPTY_MAP);
		}
		catch (IOException e)
		{
			e.printStackTrace();

			throw new RuleGenerationException("Could not load StoryDiagramEcore meta model from " + ecoreMetaModelResource.getURI() + ".");
		}

		/*
		 * Get subpackages
		 */
		EPackage ecorePackage = (EPackage) ecoreMetaModelResource.getContents().get(0);

		packages.put(ecorePackage.getNsPrefix(), ecorePackage);

		for (EPackage subpackage : sdPackage.getESubpackages())
		{
			packages.put(subpackage.getNsPrefix(), subpackage);
		}
		return packages;
	}

	/**
	 * Creates the XXXRuleSet class and returns it. The method createRuleSet is
	 * also created along with its body, that loads the activity diagram and
	 * creates all transformation rule objects. Furthermore, an "activities"
	 * reference is created, that stores the activities of the diagrams for
	 * faster access.
	 * 
	 * @param tggDiagram
	 * @param packageName
	 * @param nsPrefix
	 * @param motePackages
	 * @param rulesPackage
	 * @param basePackage
	 * @return
	 */
	protected EClass createRuleSetClass(TGGDiagram tggDiagram, EPackage rulesPackage, String basePackage)
	{
		/*
		 * Delete exsisting SDMInterpreter datatype
		 */
		if (rulesPackage.getEClassifier(GenerationStrategy.SDM_INTERPRETER) != null)
		{
			EcoreUtil.delete(rulesPackage.getEClassifier(GenerationStrategy.SDM_INTERPRETER));
		}

		/*
		 * Create EDataType for SDMInterpreter
		 */
		EDataType sdmInterpreterDataType = EcoreFactory.eINSTANCE.createEDataType();
		sdmInterpreterDataType.setName(GenerationStrategy.SDM_INTERPRETER);
		sdmInterpreterDataType.setInstanceTypeName("de.mdelab.sdm.interpreter.sde.eclipse.SDEEclipseSDMInterpreter");

		rulesPackage.getEClassifiers().add(sdmInterpreterDataType);

		/*
		 * Create RuleSet
		 */

		String ruleSetName = SDGeneratorHelper.firstLetterToUpperCase(tggDiagram.getName()
				+ SDGeneratorHelper.firstLetterToUpperCase(GenerationStrategy.RULE_SET));

		/*
		 * Delete an existing RuleSet
		 */
		if (rulesPackage.getEClassifier(ruleSetName) != null)
		{
			rulesPackage.getEClassifiers().remove(rulesPackage.getEClassifier(ruleSetName));
		}

		/*
		 * Create RuleSet class in correspondence package
		 */
		EClass ruleSetClass = EcoreFactory.eINSTANCE.createEClass();
		ruleSetClass.setName(ruleSetName);

		EClass tggRuleSetEClass = (EClass) rulesPackage.eResource().getResourceSet()
				.getEObject(URI.createURI("platform:/plugin/de.hpi.sam.mote/model/mote.ecore#//rules/TGGRuleSet"), true);

		ruleSetClass.getESuperTypes().add(tggRuleSetEClass);

		rulesPackage.getEClassifiers().add(ruleSetClass);

		/*
		 * Create EAttribute sdmInterpreter
		 */
		EAttribute sdmInterpreterAttribute = EcoreFactory.eINSTANCE.createEAttribute();
		sdmInterpreterAttribute.setName("sdmInterpreter");
		sdmInterpreterAttribute.setUpperBound(1);
		sdmInterpreterAttribute.setLowerBound(1);
		sdmInterpreterAttribute.setEType(sdmInterpreterDataType);
		
		// DB: Needed for debugger to avoid serializing the interpreter
		sdmInterpreterAttribute.setTransient( true );

		ruleSetClass.getEStructuralFeatures().add(sdmInterpreterAttribute);

		/*
		 * Create EAttribute ruleSet
		 */
		// DB: Now the resource set is in the super class
//		EAttribute resourceSetAttribute = EcoreFactory.eINSTANCE.createEAttribute();
//		resourceSetAttribute.setName("resourceSet");
//		resourceSetAttribute.setUpperBound(1);
//		resourceSetAttribute.setLowerBound(1);
//		resourceSetAttribute.setEType(EcorePackage.Literals.ERESOURCE_SET);
//		resourceSetAttribute.setTransient(true);
//
//		ruleSetClass.getEStructuralFeatures().add(resourceSetAttribute);

		/*
		 * Create EOperation createRuleSet() in ruleSetClass
		 */
		EOperation createRuleSetOperation = EcoreFactory.eINSTANCE.createEOperation();
		createRuleSetOperation.setName(GenerationStrategy.CREATE_RULES_OPERATION_NAME);
		ruleSetClass.getEOperations().add(createRuleSetOperation);

		EAnnotation codeAnnotation = EcoreFactory.eINSTANCE.createEAnnotation();
		codeAnnotation.setSource(GenerationStrategy.ECORE_GENMODEL_NSURI);

		String code = RuleSetCreateRulesTemplate.create(null).generate(tggDiagram, rulesPackage, basePackage);

		codeAnnotation.getDetails().put(GenerationStrategy.ANNOTATION_BODY_KEY, code);
		createRuleSetOperation.getEAnnotations().add(codeAnnotation);

		return ruleSetClass;
	}

	/**
	 * Creates an EAnnotation the contains the implementation code of a rule
	 * method. The code only gets the right activity from the ruleSet and
	 * executes it. !!!RETURN VALUES ARE NOT SUPPORTED RIGHT NOW!!!
	 * 
	 * @param projectName
	 * @param ruleSetName
	 * @param activityDiagramName
	 * @param eOperation
	 * 
	 * @param generatedPackage
	 * @param ruleName
	 * @param operation
	 * @param isAxiom
	 * @param direction
	 * @return
	 */
	protected EAnnotation createRuleMethodBody(String projectName, String ruleSetName, String activityDiagramName, EOperation eOperation)
	{
		EAnnotation codeAnnotation = EcoreFactory.eINSTANCE.createEAnnotation();
		codeAnnotation.setSource(GenerationStrategy.ECORE_GENMODEL_NSURI);

		List<String> parameterNames = new LinkedList<String>();

		for (EParameter parameter : eOperation.getEParameters())
		{
			parameterNames.add(parameter.getName());
		}

		// String code =
		// ExecuteActivityMethodTemplate.create(null).generate(ruleName + "." +
		// operation.getName(), parameterNames, null,
		// "TEST");

		String code = RuleCallStoryDiagramTemplate.create(null).generate(projectName, ruleSetName, activityDiagramName, eOperation, "");

		codeAnnotation.getDetails().put(GenerationStrategy.ANNOTATION_BODY_KEY, code);

		return codeAnnotation;
	}

	/**
	 * Creates the method body of the isApplicableForCorrNode method.
	 * 
	 * @param rule
	 * @return
	 */
	protected String createIsApplicableForCorrNodeMethodBody(TGGRule rule)
	{
		String parameterName = GenerationStrategy
				.getEOperation(RulesPackage.eINSTANCE.getTGGRule().getEOperations(), GenerationStrategy.IS_APPLICABLE_FOR_CORR_NODE)
				.getEParameters().get(0).getName();

		String code = "if (";

		for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
		{
			if (ce.getModifier() == TGGModifierEnumeration.NONE && ce instanceof CorrespondenceNode)
			{
				code += parameterName + " instanceof " + this.getFQN(((CorrespondenceNode) ce).getClassifier()) + " || ";
			}
		}

		code = code.substring(0, code.length() - 4);

		code += ")";
		code += "{";
		code += "return true;";
		code += "} else {";
		code += "return false; }";

		return code;
	}

	/**
	 * Returns the fully qualified name of the classfier.
	 * 
	 * @param classifier
	 * @return
	 */
	protected String getFQN(EClassifier classifier)
	{
		return classifier.getEPackage().getNsPrefix() + "." + classifier.getName();
	}

	protected String createHasMultipleContextCorrNodesMethodBody(TGGRule rule)
	{
		int contextCorrNodesCount = 0;
		for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
		{
			if (ce.getModifier() == TGGModifierEnumeration.NONE && ce instanceof CorrespondenceNode)
			{
				contextCorrNodesCount++;
			}
		}

		if (contextCorrNodesCount > 1)
		{
			return "return true;";
		}
		else
		{
			return "return false;";
		}
	}

	protected EOperation createIsApplicableForCorrNodeOperation(TGGRule rule, Map<String, EPackage> requiredPackages)
	{

		EAnnotation annotation;
		EOperation isApplicableOperation = EcoreUtil.copy(GenerationStrategy.getEOperation(
				((EClass) requiredPackages.get(RulesPackage.eNS_PREFIX).getEClassifier(RulesPackage.eINSTANCE.getTGGRule().getName()))
						.getEOperations(), GenerationStrategy.IS_APPLICABLE_FOR_CORR_NODE));

		/*
		 * Add method body
		 */
		isApplicableOperation.getEAnnotations().clear();

		annotation = EcoreFactory.eINSTANCE.createEAnnotation();
		annotation.setSource(GenerationStrategy.ECORE_GENMODEL_NSURI);

		isApplicableOperation.getEAnnotations().add(annotation);

		annotation.getDetails().put(GenerationStrategy.ANNOTATION_BODY_KEY, this.createIsApplicableForCorrNodeMethodBody(rule));

		return isApplicableOperation;
	}

	protected EOperation createHasMultipleContextCorrNodesOperation(TGGRule rule, Map<String, EPackage> requiredPackages)
	{
		EOperation hasMultipleContextCorrNodesOperation = EcoreUtil.copy(GenerationStrategy.getEOperation(
				((EClass) requiredPackages.get(RulesPackage.eNS_PREFIX).getEClassifier(RulesPackage.eINSTANCE.getTGGRule().getName()))
						.getEOperations(), GenerationStrategy.HAS_MULTIPLE_CONTEXT_CORR_NODES));

		/*
		 * Add method body
		 */
		EAnnotation annotation = EcoreFactory.eINSTANCE.createEAnnotation();
		annotation.setSource(GenerationStrategy.ECORE_GENMODEL_NSURI);

		hasMultipleContextCorrNodesOperation.getEAnnotations().add(annotation);

		annotation.getDetails().put(GenerationStrategy.ANNOTATION_BODY_KEY, this.createHasMultipleContextCorrNodesMethodBody(rule));

		return hasMultipleContextCorrNodesOperation;
	}

	protected EClassifier getEClassifierFromPackageRegistry(EClassifier eClassifier)
	{
		EPackage ePackage = Registry.INSTANCE.getEPackage(eClassifier.getEPackage().getNsURI());

		return ePackage.getEClassifier(eClassifier.getName());
	}

	protected EObject getObjectFromPackageRegistry(EObject eObject)
	{
		if (eObject instanceof EClassifier)
		{
			return this.getEClassifierFromPackageRegistry((EClassifier) eObject);
		}
		else if (eObject instanceof EOperation)
		{
			EClass eClass = (EClass) this.getEClassifierFromPackageRegistry(((EOperation) eObject).getEContainingClass());
			return GenerationStrategy.getEOperation(eClass.getEOperations(), ((EOperation) eObject).getName());
		}
		else if (eObject instanceof EStructuralFeature)
		{
			EClass eClass = (EClass) this.getEClassifierFromPackageRegistry(((EStructuralFeature) eObject).getEContainingClass());
			return eClass.getEStructuralFeature(((EStructuralFeature) eObject).getName());
		}

		return null;
	}
}
