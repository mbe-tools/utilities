package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.synchronization;

import org.eclipse.emf.ecore.EClass;

import de.hpi.sam.mote.TransformationDirection;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;

public class StoryPatternGeneratorOptions
{
	public TransformationDirection			direction																= TransformationDirection.FORWARD;

	public boolean							isAxiom																	= false;

	/*
	 * this
	 */
	public boolean							createThisObject														= false;
	public boolean							createThisToRuleSet_ruleSet_Link										= false;
	public boolean							createThisToChildCorrespondenceNodes_createdCorrNodes_Link				= false;
	public EClass							thisEClass																= null;
	public boolean							createRuleSetToChildCorrespondenceNodes_checkedCorrNodes_Link			= false;
	public StoryPatternModifierEnumeration	checkedCorrNodes_LinkModifier											= StoryPatternModifierEnumeration.NONE;

	/*
	 * ruleSet
	 */
	public boolean							createRuleSetObject														= false;
	public BindingTypeEnumeration			ruleSetObjectBinding													= BindingTypeEnumeration.UNBOUND;
	public boolean							createRuleSetToTransformationQueueLink									= false;
	public boolean							createRuleSetToChildCorrespondenceNode_root_Link						= false;

	public boolean							createRuleSetToChildCorrespondenceNodes_correspondenceNodes_Link		= false;
	public boolean							createRuleSetToParentCorrespondenceNodes_correspondenceNodes_Link		= false;

	public boolean							createRuleSetToCreatedNodes_sourceModelElementsLinks					= false;
	public boolean							createRuleSetToCreatedNodes_targetModelElementsLinks					= false;

	public boolean							createRuleSetToParentNodes_sourceModelElementsLinks						= false;
	public boolean							createRuleSetToParentNodes_targetModelElementsLinks						= false;

	public boolean							createRuleSetToCreatedNodes_uncoveredSourceElementsLinks				= false;
	public boolean							createRuleSetToCreatedNodes_uncoveredTargetElementsLinks				= false;

	public StoryPatternModifierEnumeration	ruleSetToParentNodes_sourceModelElements_Modifier						= StoryPatternModifierEnumeration.NONE;
	public StoryPatternModifierEnumeration	ruleSetToParentNodes_targetModelElements_Modifier						= StoryPatternModifierEnumeration.NONE;

	public StoryPatternModifierEnumeration	ruleSetToCreatedNodes_sourceModelElements_Modifier						= StoryPatternModifierEnumeration.NONE;
	public StoryPatternModifierEnumeration	ruleSetToCreatedNodes_targetModelElements_Modifier						= StoryPatternModifierEnumeration.NONE;

	public StoryPatternModifierEnumeration	ruleSetToCreatedNodes_uncoveredSourceElements_Modifier					= StoryPatternModifierEnumeration.NONE;
	public StoryPatternModifierEnumeration	ruleSetToCreatedNodes_uncoveredTargetElements_Modifier					= StoryPatternModifierEnumeration.NONE;

	/*
	 * transformation queue
	 */
	public boolean							createTransformationQueueObject											= false;
	public BindingTypeEnumeration			transformationQueueBinding												= BindingTypeEnumeration.UNBOUND;

	/*
	 * left parent nodes
	 */
	public boolean							createLeftParentNodes													= false;
	public StoryPatternModifierEnumeration	leftParentNodesModifier													= StoryPatternModifierEnumeration.NONE;
	public BindingTypeEnumeration			leftParentNodesBinding													= BindingTypeEnumeration.UNBOUND;

	public boolean							createLeftParentLinks													= false;

	/*
	 * left created nodes
	 */
	public boolean							createLeftCreatedNodes													= false;
	public StoryPatternModifierEnumeration	leftCreatedNodesModifier												= StoryPatternModifierEnumeration.NONE;
	public BindingTypeEnumeration			leftCreatedNodesBinding													= BindingTypeEnumeration.UNBOUND;

	public boolean							createLeftCreatedLinks													= false;

	public boolean							createLeftConstraints													= false;
	public boolean							createLeftAttributeAssignments											= false;
	public boolean							createLeftAttributeChecks												= false;
	
	// DB: Added
	public boolean 							createLeftAttAssignForPropValueConst 									= false;

	/*
	 * right parent nodes
	 */
	public boolean							createRightParentNodes													= false;
	public StoryPatternModifierEnumeration	rightParentNodesModifier												= StoryPatternModifierEnumeration.NONE;
	public BindingTypeEnumeration			rightParentNodesBinding													= BindingTypeEnumeration.UNBOUND;

	public boolean							createRightParentLinks													= false;
	/*
	 * right created nodes
	 */
	public boolean							createRightCreatedNodes													= false;
	public StoryPatternModifierEnumeration	rightCreatedNodesModifier												= StoryPatternModifierEnumeration.NONE;
	public BindingTypeEnumeration			rightCreatedNodesBinding												= BindingTypeEnumeration.UNBOUND;

	public boolean							createRightCreatedLinks													= false;

	public boolean							createRightConstraints													= false;
	public boolean							createRightAttributeAssignments											= false;
	public boolean							createRightAttributeChecks												= false;
	
	// DB: Added
	public boolean 							createRightAttAssignForPropValueConst 									= false;

	/*
	 * parent correspondence nodes
	 */
	public boolean							createParentCorrespondenceNodes											= false;
	public StoryPatternModifierEnumeration	parentCorrespondenceNodesModifier										= StoryPatternModifierEnumeration.NONE;
	public BindingTypeEnumeration			parentCorrespondenceNodesBinding										= BindingTypeEnumeration.UNBOUND;

	public boolean							createInputCorrespondenceNode											= false;
	public boolean							assignInputCorrespondenceNode											= false;
	public Expression						inputCorrespondenceNodeAssignmentExpression								= null;
	public boolean							createParentCorrespondenceNodesToChildCorrespondenceNodes_next_Links	= false;

	public boolean							createParentCorrespondenceNodesSourcesLinks								= false;
	public boolean							createParentCorrespondenceNodesTargetsLinks								= false;

	/*
	 * child correspondence nodes
	 */
	public boolean							createChildCorrespondenceNodes											= false;
	public StoryPatternModifierEnumeration	childCorrespondenceNodesModifier										= StoryPatternModifierEnumeration.NONE;
	public BindingTypeEnumeration			childCorrespondenceNodesBinding											= BindingTypeEnumeration.UNBOUND;
	public boolean							createChildCorrespondenceNodesToRuleSet_ruleSet_Links					= false;
	public boolean							createChildCorrespondenceNodesToInputCorrespondenceNode_inputNode_Link	= false;

	public boolean							createChildCorrespondenceNodesSourcesLinks								= false;
	public boolean							createChildCorrespondenceNodesTargetsLinks								= false;
}
