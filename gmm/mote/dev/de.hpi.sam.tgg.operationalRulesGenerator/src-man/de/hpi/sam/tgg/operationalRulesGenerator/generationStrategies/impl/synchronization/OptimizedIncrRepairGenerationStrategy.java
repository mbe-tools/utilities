package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.synchronization;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.hpi.sam.mote.MotePackage;
import de.hpi.sam.mote.TransformationDirection;
import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.ActivityDiagram;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreFactory;
import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsFactory;
import de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction;
import de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction;
import de.hpi.sam.storyDiagramEcore.callActions.OperationAction;
import de.hpi.sam.storyDiagramEcore.callActions.Operators;
import de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction;
import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.expressions.StringExpression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.DecisionNode;
import de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.InitialNode;
import de.hpi.sam.storyDiagramEcore.nodes.MergeNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesFactory;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;
import de.hpi.sam.tgg.CorrespondenceElement;
import de.hpi.sam.tgg.CorrespondenceLink;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.ModelDomain;
import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.RuleVariable;
import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGModifierEnumeration;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleGenerationException;
import de.hpi.sam.tgg.operationalRulesGenerator.jet.InitInputCorrNodeTypesOperationTemplate;
import de.hpi.sam.tgg.operationalRulesGenerator.jet.RuleCallStoryDiagramTemplate;
import de.hpi.sam.tgg.operationalRulesGenerator.jet.RuleCallStoryDiagramTemplateSync;

public class OptimizedIncrRepairGenerationStrategy extends GenerationStrategy {
	protected static final String	FORWARD_DELETE_METHOD_NAME		= "forwardDeleteElements";
	protected static final String	MAPPING_DELETE_METHOD_NAME		= "mappingDeleteElements";
	protected static final String	REVERSE_DELETE_METHOD_NAME		= "reverseDeleteElements";

	protected static final String	FORWARD_SYNCHRONIZE_ATTRIBUTES	= "forwardSynchronizeAttributes";
	protected static final String	MAPPING_SYNCHRONIZE_ATTRIBUTES	= "mappingSynchronizeAttributes";
	protected static final String	REVERSE_SYNCHRONIZE_ATTRIBUTES	= "reverseSynchronizeAttributes";

	protected static final String	FORWARD_REPAIR_STRUCTURE		= "forwardRepairStructure";
	protected static final String	MAPPING_REPAIR_STRUCTURE		= "mappingRepairStructure";
	protected static final String	REVERSE_REPAIR_STRUCTURE		= "reverseRepairStructure";

	protected static final String	FORWARD_RUNTIME_CHECK			= "forwardRuntimeCheck";
	protected static final String	REVERSE_RUNTIME_CHECK			= "reverseRuntimeCheck";

	protected static final String	PARAMETER						= "Parameter";
	//protected static final String	ACCEPTS_PARENT_CORR_NODE		= "acceptsParentCorrNode";
	protected static final String	INIT_INPUT_CORR_NODE_TYPES		= "initInputCorrNodeTypes";
	protected static final String	ACCEPTS_CREATED_CORR_NODE		= "acceptsCreatedCorrNode";

	protected static final String	RETURN_VALUE					= "__returnValue";
	protected static final String	CORR_NODE						= "corrNode";
	protected static final String	PARENT_CORR_NODE				= "parentCorrNode";
	protected static final String	ADD_MODIFICATION_TAG_TO_QUEUE	= "addModificationTagToQueue";
	protected static final String	SYNCHRONIZE						= "synchronize";
	protected static final String	SYNCHRONIZE_SELF				= "synchronizeSelf";
	protected static final String	TRANSFORM						= "transform";
	protected static final String	CALL_OTHER_REPAIR_OPERATION		= "callOtherRepairOperation";
	protected static final String	RULE							= "rule";

	protected static final String	TRUE							= "true";

	protected static final String	FALSE							= "false";

	protected static final String	PRINT_MESSAGE					= "printMessage";

	protected static final String	OLD								= "Old";
	protected static final String	NEW								= "New";

	protected static final String	NL								= System.getProperty("line.separator");
	
	private static EOperation existingModelObjectOper;
	
	static {
		for ( final EOperation oper : RulesPackage.Literals.TGG_RULE_SET.getEOperations() ) {
			if ( "existingModelObject".equals( oper.getName() ) ) {
				existingModelObjectOper = oper;
				
				break;
			}
		}
	}

	protected class RuleInformationContainer
	{
		protected boolean		isAxiom	= false;

		protected TGGRule		tggRule;
		protected EClass		ruleClass;

		protected EOperation	transformationForwardOperation;
		protected EOperation	transformationMappingOperation;
		protected EOperation	transformationReverseOperation;

		protected Activity		transformationForwardActivity;
		protected Activity		transformationMappingActivity;
		protected Activity		transformationReverseActivity;

		protected EOperation	synchronizationForwardOperation;
		protected EOperation	synchronizationMappingOperation;
		protected EOperation	synchronizationReverseOperation;

		protected Activity		synchronizationForwardActivity;
		protected Activity		synchronizationMappingActivity;
		protected Activity		synchronizationReverseActivity;

		protected Activity		synchronizeAttributesForwardActivity;
		protected Activity		synchronizeAttributesMappingActivity;
		protected Activity		synchronizeAttributesReverseActivity;

		protected Activity		repairForwardActivity;
		protected Activity		repairMappingActivity;
		protected Activity		repairReverseActivity;

		protected EOperation	acceptsParentCorrNodeOperation;
	}

	protected class AxtiomTransformationActivityGenerator {

		protected void createTransformationActivity(	TGGRule rule,
														TransformationDirection direction,
														EClass ruleClass,
														EOperation operation,
														Activity activity )
		throws RuleGenerationException {
			/*
			 * Create activity.
			 */
			activity.setName(rule.getName() + "_" + operation.getName());
			activity.setDescription(rule.getDescription());

			activity.setSpecification(operation);

			/*
			 * Create InitialNode
			 */
			InitialNode initialNode = NodesFactory.eINSTANCE.createInitialNode();
			initialNode.setName("start");

			activity.getNodes().add(initialNode);

			/*
			 * Create story action node to match elements
			 */
			StoryActionNode matchElementsStoryActionNode = NodesFactory.eINSTANCE.createStoryActionNode();
			matchElementsStoryActionNode.setName("match elements");

			activity.getNodes().add(matchElementsStoryActionNode);

			// Create content
			this.createMatchElementsStoryActionNodeContent(rule, direction, ruleClass, operation, matchElementsStoryActionNode);

			/*
			 * Create expression activity node to evaluate rule variables
			 */
			ExpressionActivityNode evaluateRuleVarsEAN = NodesFactory.eINSTANCE.createExpressionActivityNode();
			evaluateRuleVarsEAN.setName("evaluate rule variables");

			activity.getNodes().add(evaluateRuleVarsEAN);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createEvaluateRuleVariablesExpressionActivityNodeContent(rule, evaluateRuleVarsEAN,
					direction);

			/*
			 * Create story action node that transforms new elements
			 */
			StoryActionNode transformNewElementsStoryActionNode = NodesFactory.eINSTANCE.createStoryActionNode();
			transformNewElementsStoryActionNode.setName("transform new elements");

			activity.getNodes().add(transformNewElementsStoryActionNode);

			// Create content
			this.createTransformNewElementsStoryActionNodeContent(rule, direction, ruleClass, operation,
					transformNewElementsStoryActionNode);

			// DB: Take into account the post creation expressions
			final ModelDomain domain;

			switch ( direction ) {
				case FORWARD:
					domain = rule.getTargetDomain();
					break;
				case REVERSE:
					domain = rule.getSourceDomain();
					break;
				default:
					domain = null;
			} 

			ActivityNode source = transformNewElementsStoryActionNode;

			if ( domain != null ) {
				source = createPostCreationActions( activity, domain, source );
			}

			/*
			 * Create call action node that adds a modification tag to the
			 * transformation queue and attaches the ruleSet's model adapters to
			 * the newly created elements.
			 */
			ExpressionActivityNode addModificationTagToQueueExpressionActivityNode = NodesFactory.eINSTANCE.createExpressionActivityNode();
			addModificationTagToQueueExpressionActivityNode.setName("add modification tag to transformation queue");

			activity.getNodes().add(addModificationTagToQueueExpressionActivityNode);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createAddModificationTagToQueueExpressionActivityNodeContent(
					addModificationTagToQueueExpressionActivityNode, rule, ruleClass, /*ruleInformationMap,*/ true, false, false, direction);

			/*
			 * Create final node with return value true
			 */
			ActivityFinalNode trueFinalNode = NodesFactory.eINSTANCE.createActivityFinalNode();

			activity.getNodes().add(trueFinalNode);

			// LiteralDeclarationAction trueLiteral =
			// SDGeneratorHelper.createLiteralDeclarationAction(null, null,
			// "true",
			// EcorePackage.eINSTANCE.getEBooleanObject());
			//
			// trueFinalNode.setReturnValue(SDGeneratorHelper.createCallActionExpression(null,
			// null, trueLiteral));

			trueFinalNode
					.setReturnValue(SDGeneratorHelper.createOCLStringExpression( "mote::rules::TransformationResult::RULE_APPLIED"));

			/*
			 * Create final node with return value false
			 */
			ActivityFinalNode falseFinalNode = NodesFactory.eINSTANCE.createActivityFinalNode();

			activity.getNodes().add(falseFinalNode);

			// LiteralDeclarationAction falseLiteral =
			// SDGeneratorHelper.createLiteralDeclarationAction(null, null,
			// "false",
			// EcorePackage.eINSTANCE.getEBooleanObject());
			//
			// falseFinalNode.setReturnValue(SDGeneratorHelper.createCallActionExpression(null,
			// null, falseLiteral));

			falseFinalNode.setReturnValue(SDGeneratorHelper.createOCLStringExpression( "mote::rules::TransformationResult::RULE_NOT_APPLIED"));

			/*
			 * Create activity edges
			 */
			activity.getEdges().add(
					SDGeneratorHelper
							.createActivityEdge(initialNode, matchElementsStoryActionNode, ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(matchElementsStoryActionNode, evaluateRuleVarsEAN,
							ActivityEdgeGuardEnumeration.SUCCESS, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(matchElementsStoryActionNode, falseFinalNode,
							ActivityEdgeGuardEnumeration.FAILURE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(evaluateRuleVarsEAN, transformNewElementsStoryActionNode,
							ActivityEdgeGuardEnumeration.NONE, null));

			final ActivityEdgeGuardEnumeration guard;
			
			if ( source == transformNewElementsStoryActionNode ) {
				guard = ActivityEdgeGuardEnumeration.SUCCESS;
			}
			else {
				guard = ActivityEdgeGuardEnumeration.NONE;
			}
			
			activity.getEdges().add(					//DB: Use Source
					SDGeneratorHelper.createActivityEdge(source /*transformNewElementsStoryActionNode*/,
							addModificationTagToQueueExpressionActivityNode, guard /*ActivityEdgeGuardEnumeration.SUCCESS*/, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(transformNewElementsStoryActionNode, falseFinalNode,
							ActivityEdgeGuardEnumeration.FAILURE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(addModificationTagToQueueExpressionActivityNode, trueFinalNode,
							ActivityEdgeGuardEnumeration.NONE, null));
		}

		private void createMatchElementsStoryActionNodeContent(TGGRule rule, TransformationDirection direction, EClass ruleClass,
				EOperation operation, StoryActionNode storyActionNode) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.isAxiom = true;
			options.direction = direction;

			options.createThisObject = true;
			options.thisEClass = ruleClass;

			options.createThisToRuleSet_ruleSet_Link = true;

			options.createRuleSetObject = true;

			if (direction == TransformationDirection.FORWARD)
			{
				options.createLeftCreatedNodes = true;
				
				// DB: Added. Constraints were not handled on axiom
				options.createLeftConstraints = true;

				options.createRuleSetToCreatedNodes_uncoveredSourceElementsLinks = true;
			}
			else if (direction == TransformationDirection.MAPPING)
			{
				options.createLeftCreatedNodes = true;
				options.createRightCreatedNodes = true;

				// DB: Added. Constraints were not handled on axiom
				options.createLeftConstraints = true;
				options.createRightConstraints = true;

				options.createRuleSetToCreatedNodes_uncoveredSourceElementsLinks = true;

				options.createRuleSetToCreatedNodes_uncoveredTargetElementsLinks = true;
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.createRightCreatedNodes = true;

				// DB: Added. Constraints were not handled on axiom
				options.createRightConstraints = true;

				options.createRuleSetToCreatedNodes_uncoveredTargetElementsLinks = true;
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);
			
			// DB: Generate the action node constraints.
			storyActionNode.getConstraints().addAll( SDGeneratorHelper.copyAllWithNewUuids( rule.getConstraintExpressions() ) );

			/*
			 * Create direct assignment constraints if this is an axiom.
			 */
			if (direction == TransformationDirection.FORWARD || direction == TransformationDirection.MAPPING)
			{
				StoryPatternObject spo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(rule.getInputElements().get(
						0));
				spo.setBindingType(BindingTypeEnumeration.BOUND);

				spo.setDirectAssignmentExpression(SDGeneratorHelper.createStringExpression(SDGeneratorHelper.OCL_EXPRESSION_LANGUAGE,
						operation.getEParameters().get(0).getName()));

				if ( direction == TransformationDirection.FORWARD ) {
					storyActionNode.getConstraints().addAll( SDGeneratorHelper.copyAllWithNewUuids( rule.getForwardConstraintExpressions() ) );
				}
			}

			if (direction == TransformationDirection.MAPPING || direction == TransformationDirection.REVERSE)
			{
				StoryPatternObject spo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(rule.getInputElements().get(
						1));
				spo.setBindingType(BindingTypeEnumeration.BOUND);

				if (direction == TransformationDirection.REVERSE)
				{
					spo.setDirectAssignmentExpression(SDGeneratorHelper.createStringExpression(SDGeneratorHelper.OCL_EXPRESSION_LANGUAGE,
							operation.getEParameters().get(0).getName()));
				}
				else
				{
					spo.setDirectAssignmentExpression(SDGeneratorHelper.createStringExpression(SDGeneratorHelper.OCL_EXPRESSION_LANGUAGE,
							operation.getEParameters().get(1).getName()));
				}

				if ( direction == TransformationDirection.REVERSE ) {
					storyActionNode.getConstraints().addAll( SDGeneratorHelper.copyAllWithNewUuids( rule.getReverseConstraintExpressions() ) );
				}
			}

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

		private void createTransformNewElementsStoryActionNodeContent(TGGRule rule, TransformationDirection direction, EClass ruleClass,
				EOperation operation, StoryActionNode storyActionNode) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.isAxiom = true;
			options.direction = direction;

			options.createThisObject = true;
			options.thisEClass = ruleClass;
			options.createThisToChildCorrespondenceNodes_createdCorrNodes_Link = true;

			options.createThisToRuleSet_ruleSet_Link = true;

			options.createRuleSetObject = true;
			options.ruleSetObjectBinding = BindingTypeEnumeration.BOUND;

			options.createRuleSetToCreatedNodes_sourceModelElementsLinks = true;
			options.createRuleSetToCreatedNodes_targetModelElementsLinks = true;

			options.createRuleSetToChildCorrespondenceNodes_correspondenceNodes_Link = true;
			options.createChildCorrespondenceNodesToRuleSet_ruleSet_Links = true;

			options.createChildCorrespondenceNodes = true;
			options.childCorrespondenceNodesModifier = StoryPatternModifierEnumeration.CREATE;

			options.createChildCorrespondenceNodesSourcesLinks = true;
			options.createChildCorrespondenceNodesTargetsLinks = true;

			options.createLeftCreatedNodes = true;
			options.createRightCreatedNodes = true;

			options.createLeftCreatedLinks = true;
			options.createRightCreatedLinks = true;

			options.ruleSetToCreatedNodes_sourceModelElements_Modifier = StoryPatternModifierEnumeration.CREATE;
			options.ruleSetToCreatedNodes_targetModelElements_Modifier = StoryPatternModifierEnumeration.CREATE;

			if (direction == TransformationDirection.FORWARD)
			{
				options.rightCreatedNodesModifier = StoryPatternModifierEnumeration.CREATE;
				options.createRightAttributeAssignments = true;

				options.createRuleSetToCreatedNodes_uncoveredSourceElementsLinks = true;
				options.ruleSetToCreatedNodes_uncoveredSourceElements_Modifier = StoryPatternModifierEnumeration.DESTROY;

				options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;
				
				// DB: Added
				options.createLeftConstraints= true;
			}
			else if (direction == TransformationDirection.MAPPING)
			{
				options.createLeftConstraints = true;
				options.createRightConstraints = true;

				options.createRuleSetToCreatedNodes_uncoveredSourceElementsLinks = true;
				options.ruleSetToCreatedNodes_uncoveredSourceElements_Modifier = StoryPatternModifierEnumeration.DESTROY;

				options.createRuleSetToCreatedNodes_uncoveredTargetElementsLinks = true;
				options.ruleSetToCreatedNodes_uncoveredTargetElements_Modifier = StoryPatternModifierEnumeration.DESTROY;

				options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;
				options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.leftCreatedNodesModifier = StoryPatternModifierEnumeration.CREATE;
				options.createLeftAttributeAssignments = true;

				options.createRuleSetToCreatedNodes_uncoveredTargetElementsLinks = true;
				options.ruleSetToCreatedNodes_uncoveredTargetElements_Modifier = StoryPatternModifierEnumeration.DESTROY;

				options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;

				// DB: Added
				options.createRightConstraints= true;
			}

			options.createRuleSetToChildCorrespondenceNode_root_Link = true;

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			/*
			 * Create sourceModelRootNode link from ruleSet to source element
			 */
			{
				StoryPatternLink spl = SDGeneratorHelper.createStoryPatternLink(result.ruleSetObject,
						(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(result.leftCreatedNodes.get(0)),
						RulesPackage.Literals.TGG_RULE_SET__SOURCE_MODEL_ROOT_NODE, null, null, null, StoryPatternModifierEnumeration.CREATE, null);

				result.storyPatternLinks.add(spl);
			}

			/*
			 * Create targetModelRootNode link from ruleSet to source element
			 */
			{
				StoryPatternLink spl = SDGeneratorHelper.createStoryPatternLink(result.ruleSetObject,
						(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(result.rightCreatedNodes.get(0)),
						RulesPackage.Literals.TGG_RULE_SET__TARGET_MODEL_ROOT_NODE, null, null, null, StoryPatternModifierEnumeration.CREATE, null);

				result.storyPatternLinks.add(spl);
			}

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

	}

	protected class AxtiomSynchronizationActivityGenerator
	{

		protected void createSynchronizationActivity(TGGRule rule, TransformationDirection direction, EClass ruleClass,
				EOperation operation, Activity activity/*, Map<TGGRule, RuleInformationContainer> ruleInformationMap*/ )
				throws RuleGenerationException
		{
			/*
			 * Create activity.
			 */
			activity.setName(rule.getName() + "_" + operation.getName());
			activity.setDescription(rule.getDescription());

			activity.setSpecification(operation);

			/*
			 * Create InitialNode
			 */
			InitialNode initialNode = NodesFactory.eINSTANCE.createInitialNode();
			initialNode.setName("start");

			activity.getNodes().add(initialNode);

			/*
			 * Create story action node that matches the rule pattern
			 */
			StoryActionNode n1MatchRulePattern = NodesFactory.eINSTANCE.createStoryActionNode();
			n1MatchRulePattern.setName("match rule pattern");

			activity.getNodes().add(n1MatchRulePattern);

			// Create content
			this.createMatchRulePatternStoryActionNodeContent(n1MatchRulePattern, rule, direction, operation, ruleClass);

			/*
			 * Create expression activity node that deletes obsolete elements
			 */
			ExpressionActivityNode n2DeleteObsoleteElements = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n2DeleteObsoleteElements.setName("delete obsolete elements");

			activity.getNodes().add(n2DeleteObsoleteElements);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createCallDeleteElementsExpressionActivityNodeContent(rule, direction, ruleClass,
					n2DeleteObsoleteElements);

			/*
			 * Create expression activity node to evaluate rule variables
			 */
			ExpressionActivityNode n2aEvaluateRuleVariables = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n2aEvaluateRuleVariables.setName("evaluate rule variables");

			activity.getNodes().add(n2aEvaluateRuleVariables);

			// Create content

			OptimizedIncrRepairGenerationStrategy.this.createEvaluateRuleVariablesExpressionActivityNodeContent(rule,
					n2aEvaluateRuleVariables, direction);

			/*
			 * Create story action node to compare attribute values
			 */
			StoryActionNode n4CompareAttributeValues = NodesFactory.eINSTANCE.createStoryActionNode();
			n4CompareAttributeValues.setName("compare attribute values");

			activity.getNodes().add(n4CompareAttributeValues);

			// Create content
			this.createCompareAttributeValuesStoryActionNodeContent(n4CompareAttributeValues, rule, direction, operation, ruleClass);

			/*
			 * Create expression activity node to add modification tag to queue
			 */
			ExpressionActivityNode n5AddModTag = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n5AddModTag.setName("add modification tag to queue");

			activity.getNodes().add(n5AddModTag);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createAddModificationTagToQueueExpressionActivityNodeContent(n5AddModTag, rule,
					ruleClass, /*ruleInformationMap,*/ true, false, false, direction);

			/*
			 * Create story action node to synchronize attribute values. Not
			 * required for TransformationDirection.MAPPING.
			 */
			StoryActionNode n6SynchronizeAttributes = null;

			if (direction == TransformationDirection.FORWARD || direction == TransformationDirection.REVERSE)
			{
				n6SynchronizeAttributes = NodesFactory.eINSTANCE.createStoryActionNode();
				n6SynchronizeAttributes.setName("synchronize attribute values");

				activity.getNodes().add(n6SynchronizeAttributes);

				// Create content
				this.createSynchronizeAttributeValuesStoryActionNodeContent(n6SynchronizeAttributes, rule, direction, operation, ruleClass);
			}

			/*
			 * Create expression activity node to add modification tag to queue
			 */
			ExpressionActivityNode n7AddModTag = null;

			if (direction == TransformationDirection.FORWARD || direction == TransformationDirection.REVERSE)
			{
				n7AddModTag = NodesFactory.eINSTANCE.createExpressionActivityNode();
				n7AddModTag.setName("add modification tag to queue");

				activity.getNodes().add(n7AddModTag);

				// Create content
				OptimizedIncrRepairGenerationStrategy.this.createAddModificationTagToQueueExpressionActivityNodeContent(n7AddModTag, rule,
						ruleClass, /*ruleInformationMap,*/ true, true, false, direction);
			}

			/*
			 * Create expression activity node to delete elements. Only required
			 * for TransformationDirection.MAPPING
			 */
			ExpressionActivityNode n8DeleteElements = null;

			if (direction == TransformationDirection.MAPPING)
			{
				n8DeleteElements = NodesFactory.eINSTANCE.createExpressionActivityNode();
				n8DeleteElements.setName("delete elements");

				activity.getNodes().add(n8DeleteElements);

				// Create content
				OptimizedIncrRepairGenerationStrategy.this.createCallDeleteElementsExpressionActivityNodeContent(rule, direction, ruleClass,
						n8DeleteElements);
			}

			/*
			 * Create final node with return value true
			 */
			ActivityFinalNode trueFinalNode = null;

			if (direction == TransformationDirection.FORWARD || direction == TransformationDirection.REVERSE)
			{
				trueFinalNode = NodesFactory.eINSTANCE.createActivityFinalNode();
				trueFinalNode.setName("true final");

				activity.getNodes().add(trueFinalNode);

				LiteralDeclarationAction trueLiteral = SDGeneratorHelper.createLiteralDeclarationAction(null, null, "true",
						EcorePackage.eINSTANCE.getEBooleanObject());

				trueFinalNode.setReturnValue(SDGeneratorHelper.createCallActionExpression(null, null, trueLiteral));
			}
			/*
			 * Create final node with return value false
			 */
			ActivityFinalNode falseFinalNode = null;

			falseFinalNode = NodesFactory.eINSTANCE.createActivityFinalNode();
			falseFinalNode.setName("false final");

			activity.getNodes().add(falseFinalNode);

			LiteralDeclarationAction falseLiteral = SDGeneratorHelper.createLiteralDeclarationAction(null, null, "false",
					EcorePackage.eINSTANCE.getEBooleanObject());

			falseFinalNode.setReturnValue(SDGeneratorHelper.createCallActionExpression(null, null, falseLiteral));

			/*
			 * Create activity edges
			 */
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(initialNode, n1MatchRulePattern, ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n1MatchRulePattern, n2aEvaluateRuleVariables,
							ActivityEdgeGuardEnumeration.SUCCESS, null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n1MatchRulePattern, n2DeleteObsoleteElements,
							ActivityEdgeGuardEnumeration.FAILURE, null));

			activity.getEdges()
					.add(SDGeneratorHelper.createActivityEdge(n2DeleteObsoleteElements, falseFinalNode, ActivityEdgeGuardEnumeration.NONE,
							null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n2aEvaluateRuleVariables, n4CompareAttributeValues,
							ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges()
					.add(SDGeneratorHelper.createActivityEdge(n4CompareAttributeValues, n5AddModTag, ActivityEdgeGuardEnumeration.SUCCESS,
							null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n5AddModTag, falseFinalNode, ActivityEdgeGuardEnumeration.NONE, null));

			if (direction == TransformationDirection.FORWARD || direction == TransformationDirection.REVERSE)
			{
				activity.getEdges().add(
						SDGeneratorHelper.createActivityEdge(n4CompareAttributeValues, n6SynchronizeAttributes,
								ActivityEdgeGuardEnumeration.FAILURE, null));
				activity.getEdges()
						.add(SDGeneratorHelper.createActivityEdge(n6SynchronizeAttributes, n7AddModTag, ActivityEdgeGuardEnumeration.NONE,
								null));
				activity.getEdges().add(
						SDGeneratorHelper.createActivityEdge(n7AddModTag, trueFinalNode, ActivityEdgeGuardEnumeration.NONE, null));
			}
			else
			{
				activity.getEdges().add(
						SDGeneratorHelper.createActivityEdge(n4CompareAttributeValues, n8DeleteElements,
								ActivityEdgeGuardEnumeration.FAILURE, null));
				activity.getEdges().add(
						SDGeneratorHelper.createActivityEdge(n8DeleteElements, falseFinalNode, ActivityEdgeGuardEnumeration.NONE, null));
			}
		}

		private void createSynchronizeAttributeValuesStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EOperation operation, EClass ruleClass) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.isAxiom = true;
			options.direction = direction;

			options.createThisObject = false;
			options.thisEClass = ruleClass;

			options.createChildCorrespondenceNodes = true;
			options.childCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftCreatedNodes = true;
			options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;

			options.createRightCreatedNodes = true;
			options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;

			if (direction == TransformationDirection.FORWARD)
			{
				options.createRightAttributeAssignments = true;
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.createLeftAttributeAssignments = true;
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

		private void createCompareAttributeValuesStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EOperation operation, EClass ruleClass) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.isAxiom = true;
			options.direction = direction;

			options.createThisObject = false;
			options.thisEClass = ruleClass;

			options.createChildCorrespondenceNodes = true;
			options.childCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftCreatedNodes = true;
			options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;

			options.createRightCreatedNodes = true;
			options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftAttributeChecks = true;
			options.createRightAttributeChecks = true;

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

		private void createMatchRulePatternStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EOperation operation, EClass ruleClass) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.isAxiom = true;
			options.direction = direction;

			options.createThisObject = true;
			options.thisEClass = ruleClass;

			options.createThisToChildCorrespondenceNodes_createdCorrNodes_Link = true;
			options.createThisToRuleSet_ruleSet_Link = true;

			options.createRuleSetObject = true;

			options.createRuleSetToChildCorrespondenceNodes_correspondenceNodes_Link = true;

			options.createRuleSetToCreatedNodes_sourceModelElementsLinks = true;
			options.createRuleSetToCreatedNodes_targetModelElementsLinks = true;

			options.createChildCorrespondenceNodes = true;
			options.childCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createChildCorrespondenceNodesSourcesLinks = true;
			options.createChildCorrespondenceNodesTargetsLinks = true;

			options.createChildCorrespondenceNodesToRuleSet_ruleSet_Links = true;

			options.createLeftCreatedNodes = true;
			options.createLeftCreatedLinks = true;

			options.createRightCreatedNodes = true;
			options.createRightCreatedLinks = true;

			options.createLeftConstraints = true;
			options.createRightConstraints = true;

			options.createRuleSetToChildCorrespondenceNode_root_Link = true;

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			/*
			 * Bind corresponde node to parameter
			 */
			StoryPatternObject spo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(result.childCorrespondenceNodes
					.get(0));
			spo.setBindingType(BindingTypeEnumeration.BOUND);

			spo.setDirectAssignmentExpression(SDGeneratorHelper.createStringExpression(SDGeneratorHelper.OCL_EXPRESSION_LANGUAGE, operation
					.getEParameters().get(0).getName()));

			/*
			 * Create sourceModelRootNode link from ruleSet to source element
			 */
			{
				StoryPatternLink spl = SDGeneratorHelper.createStoryPatternLink(result.ruleSetObject,
						(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(result.leftCreatedNodes.get(0)),
						RulesPackage.Literals.TGG_RULE_SET__SOURCE_MODEL_ROOT_NODE, null, null, null, StoryPatternModifierEnumeration.NONE, null);

				result.storyPatternLinks.add(spl);
			}

			/*
			 * Create targetModelRootNode link from ruleSet to source element
			 */
			{
				StoryPatternLink spl = SDGeneratorHelper.createStoryPatternLink(result.ruleSetObject,
						(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(result.rightCreatedNodes.get(0)),
						RulesPackage.Literals.TGG_RULE_SET__TARGET_MODEL_ROOT_NODE, null, null, null, StoryPatternModifierEnumeration.NONE, null);

				result.storyPatternLinks.add(spl);
			}

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}
	}
	
	private ActivityNode createPostCreationActions( final Activity activity,
													final ModelDomain domain,
													final ActivityNode sourceActNode ) {
		ActivityNode returnActNode = sourceActNode;
		
		for ( final ModelElement modelElement : domain.getModelElements() ) {
			if (  modelElement.getModifier() == TGGModifierEnumeration.CREATE && modelElement instanceof ModelObject ) {
				final ModelObject modelObj = (ModelObject) modelElement;
				
				for ( final Expression postCreatExpr : modelObj.getPostCreationExpressions() ) {
					
					// Create post creation expression activity
					final ExpressionActivityNode postCreationAction = NodesFactory.eINSTANCE.createExpressionActivityNode();
					postCreationAction.setName("post creation action " + postCreatExpr.getName() );
					postCreationAction.setActivity(activity);
					// DB: Set new UUIDs
					postCreationAction.setExpression( SDGeneratorHelper.copyWithNewUuids( postCreatExpr ) );
//					postCreationAction.setExpression( EcoreUtil.copy( postCreatExpr ) );
					
					// Create link
					activity.getEdges().add( SDGeneratorHelper.createActivityEdge(	returnActNode, 
																					postCreationAction,
																					ActivityEdgeGuardEnumeration.SUCCESS, 
																					null ) );
					returnActNode = postCreationAction;
				}
			}
		}
		
		return returnActNode;
	}

	protected class TransformationActivityGenerator
	{
		protected void createTransformationActivity(TGGRule rule, TransformationDirection direction, EClass ruleClass,
				EOperation operation, Activity activity/*, Map<TGGRule, RuleInformationContainer> ruleInformationMap*/)
				throws RuleGenerationException
		{
			/*
			 * Create activity.
			 */
			activity.setName(rule.getName() + "_" + operation.getName());
			activity.setDescription(rule.getDescription());

			activity.setSpecification(operation);

			/*
			 * Create InitialNode
			 */
			InitialNode initialNode = NodesFactory.eINSTANCE.createInitialNode();
			initialNode.setName("start");

			activity.getNodes().add(initialNode);

			/*
			 * Create action node to create return value
			 */
			ExpressionActivityNode n1CreateReturnValue = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n1CreateReturnValue.setName("create return value");

			n1CreateReturnValue.setActivity(activity);

			// Create content
			this.createCreateReturnValueExpressionActivityNodeContent(n1CreateReturnValue);

			// create transition
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(initialNode, n1CreateReturnValue, ActivityEdgeGuardEnumeration.NONE, null));

			/*
			 * Create story action nodes that perform the transformation. First,
			 * find out how many parent correspondence nodes are in the rule.
			 * Then, create the story action nodes for each of them.
			 */
//			int count = 0;
			boolean first = true;
			ActivityNode lastActivityNode = n1CreateReturnValue;

			for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
			{
				if (ce.getModifier() == TGGModifierEnumeration.NONE && ce instanceof CorrespondenceNode) {
					final CorrespondenceNode corrNode = (CorrespondenceNode) ce;
					
					if (OptimizedIncrRepairGenerationStrategy.this.checkIfTransformationPossible(rule, corrNode ))
					{
						final ActivityNode actNode = createTransformationStoryActionNodes(activity, rule, direction, ruleClass, corrNode );
	
						if (first/*i == 0*/) {
							first = false;
							activity.getEdges().add(
									SDGeneratorHelper.createActivityEdge(lastActivityNode, actNode, ActivityEdgeGuardEnumeration.NONE, null));
						}
						else {
							activity.getEdges().add(
									SDGeneratorHelper.createActivityEdge(lastActivityNode, actNode, ActivityEdgeGuardEnumeration.END, null));
						}
	
						lastActivityNode = actNode;
					}
				}
			}

			/*
			 * Create final node
			 */
			ActivityFinalNode finalNode = NodesFactory.eINSTANCE.createActivityFinalNode();
			finalNode.setName("stop");

			finalNode.setActivity(activity);

			// Create return value expression
			VariableReferenceAction returnValueReferenceAction = SDGeneratorHelper.createVariableReferenceAction(
					OptimizedIncrRepairGenerationStrategy.RETURN_VALUE, null, OptimizedIncrRepairGenerationStrategy.RETURN_VALUE,
					RulesPackage.Literals.TRANSFORMATION_RESULT);

			CallActionExpression returnValueExpression = SDGeneratorHelper.createCallActionExpression(
					OptimizedIncrRepairGenerationStrategy.RETURN_VALUE, null, returnValueReferenceAction);

			finalNode.setReturnValue(returnValueExpression);

			// Create transition
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(lastActivityNode, finalNode, ActivityEdgeGuardEnumeration.END, null));
		}

		private ActivityNode createTransformationStoryActionNodes(	Activity activity,
																	TGGRule rule,
																	TransformationDirection direction,
																	EClass ruleClass,
																	CorrespondenceNode parentCorrNode )
		throws RuleGenerationException {

			/*
			 * Create action node to search for new elements
			 */
			StoryActionNode n1SearchForNewElements = NodesFactory.eINSTANCE.createStoryActionNode();
			n1SearchForNewElements.setName("search for new elements from " + parentCorrNode.getName() );
			n1SearchForNewElements.setForEach(true);

			n1SearchForNewElements.setActivity(activity);

			// Create content
			createSearchForNewElementsStoryActionNodeContent(n1SearchForNewElements, rule, direction, ruleClass, parentCorrNode );
			final String nameSuffix = " after match from " + parentCorrNode.getName();

			/*
			 * create expression activity node to evaluate rule variables
			 */
			ExpressionActivityNode n1aEvaluateRuleVarsEAN = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n1aEvaluateRuleVarsEAN.setName("evaluate rule variables" + nameSuffix );

			n1aEvaluateRuleVarsEAN.setActivity(activity);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createEvaluateRuleVariablesExpressionActivityNodeContent(rule, n1aEvaluateRuleVarsEAN,
					direction);

			/*
			 * Create action node to transform elements
			 */
			StoryActionNode n2TransformElements = NodesFactory.eINSTANCE.createStoryActionNode();
			n2TransformElements.setName("transform new elements" + nameSuffix );

			n2TransformElements.setActivity(activity);

			// Create content
			this.createTransformElementsStoryActionNodeContent(n2TransformElements, rule, direction, ruleClass, parentCorrNode );

			// DB: Take into account the post creation expressions
			final ModelDomain domain;

			switch ( direction ) {
				case FORWARD:
					domain = rule.getTargetDomain();
					break;
				case REVERSE:
					domain = rule.getSourceDomain();
					break;
				default:
					domain = null;
			} 

			ActivityNode source = n2TransformElements;

			if ( domain != null ) {
				source = createPostCreationActions( activity, domain, source );
			}

			/*
			 * Create action node to add a modification tag to queue
			 */
			ExpressionActivityNode n3AddModificationTagToQueue = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n3AddModificationTagToQueue.setName("add modification tag to queue" + nameSuffix );

			n3AddModificationTagToQueue.setActivity(activity);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createAddModificationTagToQueueExpressionActivityNodeContent(
					n3AddModificationTagToQueue, rule, ruleClass, /*ruleInformationMap,*/ true, false, false, direction);

			/*
			 * Create action node to set the return value to true
			 */
			ExpressionActivityNode n4SetReturnValueTrue = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n4SetReturnValueTrue.setName("set return value to true" + nameSuffix );

			n4SetReturnValueTrue.setActivity(activity);

			// create content
			this.createSetReturnValueTrueExpressionActivityNodeContent(n4SetReturnValueTrue);

			/*
			 * Create transitions
			 */
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n1SearchForNewElements, n1aEvaluateRuleVarsEAN,
							ActivityEdgeGuardEnumeration.FOR_EACH, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n1aEvaluateRuleVarsEAN, n2TransformElements, ActivityEdgeGuardEnumeration.NONE,
							null));

			// DB: Use source
			final ActivityEdgeGuardEnumeration guard;
			
			if ( source == n2TransformElements ) {
				guard = ActivityEdgeGuardEnumeration.SUCCESS;
			}
			else {
				guard = ActivityEdgeGuardEnumeration.NONE;
			}
					
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(source, n3AddModificationTagToQueue,
							guard , null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n2TransformElements, n1SearchForNewElements, ActivityEdgeGuardEnumeration.FAILURE,
							null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n3AddModificationTagToQueue, n4SetReturnValueTrue,
							ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n4SetReturnValueTrue, n1SearchForNewElements, ActivityEdgeGuardEnumeration.NONE,
							null));

			return n1SearchForNewElements;
		}

		private void createSetReturnValueTrueExpressionActivityNodeContent(ExpressionActivityNode expressionActivityNode)
		{
			/*
			 * Create literal "true"
			 */
			// LiteralDeclarationAction literalDeclarationAction =
			// SDGeneratorHelper.createLiteralDeclarationAction("true", null,
			// "true",
			// EcorePackage.eINSTANCE.getEBooleanObject());
			//
			// CallActionExpression literalDeclarationExpression =
			// SDGeneratorHelper.createCallActionExpression("true", null,
			// literalDeclarationAction);

			StringExpression trueExpression = SDGeneratorHelper.createOCLStringExpression( "mote::rules::TransformationResult::RULE_APPLIED");

			/*
			 * Create variable declaration for returnValue
			 */
			VariableDeclarationAction variableDeclarationAction = SDGeneratorHelper.createVariableDeclarationAction(
					OptimizedIncrRepairGenerationStrategy.RETURN_VALUE, null, OptimizedIncrRepairGenerationStrategy.RETURN_VALUE,
					RulesPackage.Literals.TRANSFORMATION_RESULT, trueExpression);

			CallActionExpression variableDeclarationExpression = SDGeneratorHelper.createCallActionExpression(
					OptimizedIncrRepairGenerationStrategy.RETURN_VALUE, null, variableDeclarationAction);

			expressionActivityNode.setExpression(variableDeclarationExpression);
		}

		private void createTransformElementsStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EClass ruleClass, CorrespondenceNode parentCorrNode ) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.direction = direction;

			options.createThisObject = true;
			options.thisEClass = ruleClass;

			options.createThisToChildCorrespondenceNodes_createdCorrNodes_Link = true;

			options.createRuleSetObject = true;
			options.ruleSetObjectBinding = BindingTypeEnumeration.BOUND;

			options.createRuleSetToChildCorrespondenceNodes_correspondenceNodes_Link = true;

			options.createRuleSetToCreatedNodes_sourceModelElementsLinks = true;
			options.createRuleSetToCreatedNodes_targetModelElementsLinks = true;

			options.ruleSetToCreatedNodes_sourceModelElements_Modifier = StoryPatternModifierEnumeration.CREATE;
			options.ruleSetToCreatedNodes_targetModelElements_Modifier = StoryPatternModifierEnumeration.CREATE;

			options.createRuleSetToParentNodes_sourceModelElementsLinks = true;
			options.createRuleSetToParentNodes_targetModelElementsLinks = true;

			options.createChildCorrespondenceNodesToRuleSet_ruleSet_Links = true;

			options.createParentCorrespondenceNodes = true;

			options.createChildCorrespondenceNodes = true;
			options.childCorrespondenceNodesModifier = StoryPatternModifierEnumeration.CREATE;

			options.createParentCorrespondenceNodesToChildCorrespondenceNodes_next_Links = true;
			options.createChildCorrespondenceNodesToInputCorrespondenceNode_inputNode_Link = true;

			options.createParentCorrespondenceNodesSourcesLinks = true;
			options.createParentCorrespondenceNodesTargetsLinks = true;

			options.createChildCorrespondenceNodesSourcesLinks = true;
			options.createChildCorrespondenceNodesTargetsLinks = true;

			options.createLeftParentNodes = true;
			options.createLeftParentLinks = true;

			options.createLeftCreatedNodes = true;
			options.createLeftCreatedLinks = true;

			options.createRightParentNodes = true;
			options.createRightParentLinks = true;

			options.createRightCreatedNodes = true;
			options.createRightCreatedLinks = true;
			
			// DB: Create constraints for all directions
			options.createLeftConstraints = true;
			options.createRightConstraints = true;

			if (direction == TransformationDirection.FORWARD)
			{
				options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;
				
				// DB: Changed to can be bound so that the newly created object can be passed to post-creation actions.
				options.rightCreatedNodesBinding = BindingTypeEnumeration.CAN_BE_BOUND;

				options.rightCreatedNodesModifier = StoryPatternModifierEnumeration.CREATE;

				options.createRuleSetToCreatedNodes_uncoveredSourceElementsLinks = true;
				options.ruleSetToCreatedNodes_uncoveredSourceElements_Modifier = StoryPatternModifierEnumeration.DESTROY;

				options.createRightAttributeAssignments = true;
			}
			else if (direction == TransformationDirection.MAPPING)
			{
				options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;

				options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;

				options.createRuleSetToCreatedNodes_uncoveredSourceElementsLinks = true;
				options.ruleSetToCreatedNodes_uncoveredSourceElements_Modifier = StoryPatternModifierEnumeration.DESTROY;

				options.createRuleSetToCreatedNodes_uncoveredTargetElementsLinks = true;
				options.ruleSetToCreatedNodes_uncoveredTargetElements_Modifier = StoryPatternModifierEnumeration.DESTROY;

				options.createLeftAttributeChecks = true;
				options.createRightAttributeChecks = true;
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;
				
				// DB: Changed to can be bound so that the newly created object can be passed to post-creation actions.
				options.leftCreatedNodesBinding = BindingTypeEnumeration.CAN_BE_BOUND;

				options.leftCreatedNodesModifier = StoryPatternModifierEnumeration.CREATE;

				options.createRuleSetToCreatedNodes_uncoveredTargetElementsLinks = true;
				options.ruleSetToCreatedNodes_uncoveredTargetElements_Modifier = StoryPatternModifierEnumeration.DESTROY;

				options.createLeftAttributeAssignments = true;
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			/*
			 * Set the correspondence node specified by parentCorrNodeNo to
			 * bound, as well their connected target elements
			 */
			//int count = 0;

//			for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
//			{
//				if (ce.getModifier() == TGGModifierEnumeration.NONE && ce instanceof CorrespondenceNode)
//				{
//					if (count == parentCorrNodeNo)
//					{
//						CorrespondenceNode cn = (CorrespondenceNode) ce;

						((StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(parentCorrNode))
								.setBindingType(BindingTypeEnumeration.BOUND);

						for (CorrespondenceLink cl : parentCorrNode.getOutgoingCorrespondenceLinks())
						{
							((StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getTarget()))
									.setBindingType(BindingTypeEnumeration.BOUND);
						}
//					}
//
//					count++;
//				}
//			}

			/*
			 * Set all elements on the source side to bound
			 */
			if (direction == TransformationDirection.FORWARD || direction == TransformationDirection.MAPPING)
			{
				for (ModelElement me : rule.getSourceDomain().getModelElements())
				{
					if (me instanceof ModelObject)
					{
						((StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(me))
								.setBindingType(BindingTypeEnumeration.BOUND);
					}
				}
			}

			if (direction == TransformationDirection.REVERSE || direction == TransformationDirection.MAPPING)
			{
				for (ModelElement me : rule.getTargetDomain().getModelElements())
				{
					if (me instanceof ModelObject)
					{
						((StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(me))
								.setBindingType(BindingTypeEnumeration.BOUND);
					}
				}
			}

			/*
			 * Set the uncovered elements map entries to bound
			 */
			// for (AbstractStoryPatternLink spl : result.storyPatternLinks)
			// {
			// if (spl instanceof StoryPatternLink)
			// {
			// if (((StoryPatternLink) spl).getEStructuralFeature() ==
			// RulesPackage.eINSTANCE.getTGGRuleSet_UncoveredSourceElements()
			// || ((StoryPatternLink) spl).getEStructuralFeature() ==
			// RulesPackage.eINSTANCE
			// .getTGGRuleSet_UncoveredTargetElements())
			// {
			// ((StoryPatternObject)
			// spl.getTarget()).setBindingType(BindingTypeEnumeration.BOUND);
			// }
			// }
			// }

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

		private void createSearchForNewElementsStoryActionNodeContent(	StoryActionNode storyActionNode,
																		TGGRule rule,
																		TransformationDirection direction,
																		EClass ruleClass,
																		final CorrespondenceNode parentCorrNode )
		throws RuleGenerationException {
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.direction = direction;

			options.createThisObject = true;
			options.thisEClass = ruleClass;

			options.createRuleSetObject = true;

			options.createThisToRuleSet_ruleSet_Link = true;

			options.createParentCorrespondenceNodes = true;
			options.parentCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createParentCorrespondenceNodesSourcesLinks = true;
			options.createParentCorrespondenceNodesTargetsLinks = true;
			
			// DB: Create constraints for all directions
			options.createLeftConstraints = true;
			options.createRightConstraints = true;
			
			// DB: Generate the action node constraints.
			storyActionNode.getConstraints().addAll( SDGeneratorHelper.copyAllWithNewUuids( rule.getConstraintExpressions() ) );

			if (direction == TransformationDirection.FORWARD)
			{
				options.createLeftParentNodes = true;
				options.createLeftParentLinks = true;

				options.createRightParentNodes = true;
				options.createRightParentLinks = true;

				options.createLeftCreatedNodes = true;
				options.createLeftCreatedLinks = true;

//				options.createLeftConstraints = true;

				options.createRuleSetToCreatedNodes_uncoveredSourceElementsLinks = true;
				
				// DB: Rule directional transformation constraints.
				storyActionNode.getConstraints().addAll( SDGeneratorHelper.copyAllWithNewUuids( rule.getForwardConstraintExpressions() ) );
			}
			else if (direction == TransformationDirection.MAPPING)
			{
				options.createLeftParentNodes = true;
				options.createLeftParentLinks = true;

				options.createLeftCreatedNodes = true;
				options.createLeftCreatedLinks = true;

				options.createRightParentNodes = true;
				options.createRightParentLinks = true;

				options.createRightCreatedNodes = true;
				options.createRightCreatedLinks = true;

//				options.createLeftConstraints = true;
//				options.createRightConstraints = true;

				options.createRuleSetToCreatedNodes_uncoveredSourceElementsLinks = true;
				options.createRuleSetToCreatedNodes_uncoveredTargetElementsLinks = true;
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.createLeftParentNodes = true;
				options.createLeftParentLinks = true;

				options.createRightParentNodes = true;
				options.createRightParentLinks = true;

				options.createRightCreatedNodes = true;
				options.createRightCreatedLinks = true;

				//options.createRightConstraints = true;

				options.createRuleSetToCreatedNodes_uncoveredTargetElementsLinks = true;

				// DB: Rule directional transformation constraints.
				storyActionNode.getConstraints().addAll( SDGeneratorHelper.copyAllWithNewUuids( rule.getReverseConstraintExpressions() ) );
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			/*
			 * Remove all model elements on the target side connected to a
			 * correspondence node, that is not the correspondence node
			 * specified by parentCorrNodeNo.
			 */
			//int count = 0;

			for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
			{
				if (ce.getModifier() == TGGModifierEnumeration.NONE && ce instanceof CorrespondenceNode)
				{
					CorrespondenceNode cn = (CorrespondenceNode) ce;

					if (cn == parentCorrNode)
					{
						/*
						 * Create a direct assignment
						 */
						VariableReferenceAction variableReferenceAction = SDGeneratorHelper.createVariableReferenceAction(
								OptimizedIncrRepairGenerationStrategy.PARENT_CORR_NODE, null,
								OptimizedIncrRepairGenerationStrategy.PARENT_CORR_NODE, MotePackage.eINSTANCE.getTGGNode());

						((StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn))
								.setDirectAssignmentExpression(SDGeneratorHelper.createCallActionExpression(
										OptimizedIncrRepairGenerationStrategy.PARENT_CORR_NODE, null, variableReferenceAction));
					}
					else
					{
						if (direction == TransformationDirection.FORWARD || direction == TransformationDirection.REVERSE)
						{
							/*
							 * Delete model elements in the target side
							 */
							for (CorrespondenceLink cl : cn.getOutgoingCorrespondenceLinks())
							{
								ModelObject mo = cl.getTarget();

								if (direction == TransformationDirection.FORWARD && rule.getTargetDomain().getModelElements().contains(mo)
										|| direction == TransformationDirection.REVERSE
										&& rule.getSourceDomain().getModelElements().contains(mo))
								{
									StoryPatternObject spo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(mo);

									for (AbstractStoryPatternLink spl : spo.getOutgoingStoryLinks())
									{
										spl.setTarget(null);
										result.storyPatternLinks.remove(spl);
									}

									for (AbstractStoryPatternLink spl : spo.getIncomingStoryLinks())
									{
										spl.setSource(null);
										result.storyPatternLinks.remove(spl);
									}

									spo.getIncomingStoryLinks().clear();
									spo.getOutgoingStoryLinks().clear();

									result.storyPatternObjects.remove(spo);
								}
							}
						}

						/*
						 * Delete the correspondence node
						 */
						StoryPatternObject spo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn);

						for (AbstractStoryPatternLink spl : spo.getOutgoingStoryLinks())
						{
							spl.setTarget(null);
							result.storyPatternLinks.remove(spl);
						}

						for (AbstractStoryPatternLink spl : spo.getIncomingStoryLinks())
						{
							spl.setSource(null);
							result.storyPatternLinks.remove(spl);
						}

						result.storyPatternObjects.remove(spo);
					}

					//count++;
				}
			}

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

		private void createCreateReturnValueExpressionActivityNodeContent(ExpressionActivityNode n1CreateReturnValue)
		{
			/*
			 * Create "false" literal
			 */
			StringExpression falseExpression = SDGeneratorHelper.createOCLStringExpression( "mote::rules::TransformationResult::RULE_NOT_APPLIED");

			/*
			 * Create variable declaration
			 */
			VariableDeclarationAction variableDeclarationAction = SDGeneratorHelper.createVariableDeclarationAction(
					OptimizedIncrRepairGenerationStrategy.RETURN_VALUE, null, OptimizedIncrRepairGenerationStrategy.RETURN_VALUE,
					RulesPackage.Literals.TRANSFORMATION_RESULT, falseExpression);

			CallActionExpression variableDeclarationExpression = SDGeneratorHelper.createCallActionExpression(
					OptimizedIncrRepairGenerationStrategy.RETURN_VALUE, null, variableDeclarationAction);

			n1CreateReturnValue.setExpression(variableDeclarationExpression);
		}
	}

	protected class SynchronizationActivityGenerator
	{
		protected void createSynchronizationActivity(TGGRule rule, TransformationDirection direction, EClass ruleClass,
				EOperation operation, Activity activity, Map<TGGRule, RuleInformationContainer> ruleInformationMap )
				throws RuleGenerationException
		{
			/*
			 * Create activity.
			 */
			activity.setName(rule.getName() + "_" + operation.getName());
			activity.setDescription(rule.getDescription());

			activity.setSpecification(operation);

			/*
			 * Create InitialNode
			 */
			InitialNode initialNode = NodesFactory.eINSTANCE.createInitialNode();
			initialNode.setName("start");

			activity.getNodes().add(initialNode);

			/*
			 * Create story action node to match the ruleSet
			 */
			StoryActionNode n0MatchRuleSet = NodesFactory.eINSTANCE.createStoryActionNode();
			n0MatchRuleSet.setName("match ruleSet");

			n0MatchRuleSet.setActivity(activity);

			// Create content
			this.createMatchRuleSetStoryActionNodeContent(n0MatchRuleSet, rule, ruleClass);

			/*
			 * Create story action node to match pattern
			 */
			StoryActionNode n1MatchRulePattern = NodesFactory.eINSTANCE.createStoryActionNode();
			n1MatchRulePattern.setName("match rule pattern");

			n1MatchRulePattern.setActivity(activity);

			// Create content
			this.createMatchRulePatternStoryActionNodeContent(n1MatchRulePattern, rule, direction, ruleClass);

			/*
			 * Create decision node to check if attributes were synchronized
			 */
			DecisionNode n2CheckIfAttributesSynchronized = NodesFactory.eINSTANCE.createDecisionNode();
			n2CheckIfAttributesSynchronized.setActivity(activity);

			/*
			 * Create expression activity node to add modification tag to queue
			 */
			ExpressionActivityNode n3AddModTagToQueue = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n3AddModTagToQueue.setName("add modification tag to queue");

			n3AddModTagToQueue.setActivity(activity);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createAddModificationTagToQueueExpressionActivityNodeContent(n3AddModTagToQueue, rule,
					ruleClass, /*ruleInformationMap,*/ true, false, false, direction);

			/*
			 * create expression activity node to add modification tag to queue
			 */
			ExpressionActivityNode n4AddModTagToQueue = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n4AddModTagToQueue.setName("add modification to queue");

			n4AddModTagToQueue.setActivity(activity);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createAddModificationTagToQueueExpressionActivityNodeContent(n4AddModTagToQueue, rule,
					ruleClass, /*ruleInformationMap,*/ true, true, false, direction);

			/*
			 * Create decision node to check if all source elements are still
			 * contained in the model
			 */
			DecisionNode n5CheckIfModelElementsContainedInModel = NodesFactory.eINSTANCE.createDecisionNode();
			n5CheckIfModelElementsContainedInModel.setActivity(activity);

			/*
			 * create decision node to check if repair was successful
			 */
			DecisionNode n6CheckIfRepairSuccessful = NodesFactory.eINSTANCE.createDecisionNode();
			n6CheckIfRepairSuccessful.setActivity(activity);

			/*
			 * create story action node to match rules
			 */
			StoryActionNode n7MatchRules = null;

			if (((TGGDiagram) rule.eContainer()).getTggRules().size() > 2)
			{
				n7MatchRules = NodesFactory.eINSTANCE.createStoryActionNode();
				n7MatchRules.setName("match rules");
				n7MatchRules.setForEach(true);

				n7MatchRules.setActivity(activity);

				// Create content
				this.createMatchRulesStoryActionNodeContent(n7MatchRules, rule, ruleClass);
			}

			/*
			 * Create true final node
			 */
			ActivityFinalNode trueFinalNode = NodesFactory.eINSTANCE.createActivityFinalNode();
			trueFinalNode.setName("stop true");

			trueFinalNode.setActivity(activity);

			// Create expression
			LiteralDeclarationAction trueLiteralAction = SDGeneratorHelper.createLiteralDeclarationAction(
					OptimizedIncrRepairGenerationStrategy.TRUE, null, OptimizedIncrRepairGenerationStrategy.TRUE,
					EcorePackage.eINSTANCE.getEBooleanObject());

			CallActionExpression trueLiteralExpression = SDGeneratorHelper.createCallActionExpression(
					OptimizedIncrRepairGenerationStrategy.TRUE, null, trueLiteralAction);

			trueFinalNode.setReturnValue(trueLiteralExpression);

			/*
			 * Create story action node for each rule
			 */
			ActivityNode lastNode = n7MatchRules;

			if (((TGGDiagram) rule.eContainer()).getTggRules().size() > 2)
			{
				for (TGGRule r : ((TGGDiagram) rule.eContainer()).getTggRules())
				{
					// Skip this rule
					if ( r != rule && !SDGeneratorHelper.isAxiom( r ) ) {
						final RuleInformationContainer ric = ruleInformationMap.get( r );
						
//						// Skip this rule
//						if (r != rule && !ric.isAxiom)
//						{
						/*
						 * Create story action node to match the rule
						 */
						StoryActionNode n8CheckRuleType = NodesFactory.eINSTANCE.createStoryActionNode();
						n8CheckRuleType.setName("check rule type " + r.getName());

						n8CheckRuleType.setActivity(activity);

						// Create content
						this.createCheckRuleTypeStoryActionNodeContent(n8CheckRuleType, r, ruleInformationMap );

						/*
						 * Create decision node to check if repair was
						 * successful
						 */
						DecisionNode n9CheckIfRepairSuccessful = NodesFactory.eINSTANCE.createDecisionNode();
						n9CheckIfRepairSuccessful.setActivity(activity);

						/*
						 * Create transitions
						 */
						if (lastNode == n7MatchRules)
						{
							activity.getEdges().add(
									SDGeneratorHelper.createActivityEdge(lastNode, n8CheckRuleType, ActivityEdgeGuardEnumeration.FOR_EACH,
											null));
						}
						else
						{
							activity.getEdges().add(
									SDGeneratorHelper.createActivityEdge(lastNode, n8CheckRuleType, ActivityEdgeGuardEnumeration.FAILURE,
											null));
						}

						activity.getEdges().add(
								SDGeneratorHelper.createActivityEdge(n8CheckRuleType, n9CheckIfRepairSuccessful,
										ActivityEdgeGuardEnumeration.SUCCESS, null));

						Expression repairCallExpression = null;

						switch (direction)
						{
							case FORWARD:
							{
								repairCallExpression = this.createCallRepairExpression(r.getName(), ric.ruleClass,
										ric.repairForwardActivity);
								break;
							}
							case MAPPING:
							{
								repairCallExpression = this.createCallRepairExpression(r.getName(), ric.ruleClass,
										ric.repairMappingActivity);
								break;
							}
							case REVERSE:
							{
								repairCallExpression = this.createCallRepairExpression(r.getName(), ric.ruleClass,
										ric.repairReverseActivity);
								break;
							}
						}

						// activity.getEdges().add(
						// SDGeneratorHelper.createActivityEdge(n9CheckIfRepairSuccessful,
						// n10MergeFlows,
						// ActivityEdgeGuardEnumeration.BOOLEAN,
						// repairCallExpression));

						activity.getEdges().add(
								SDGeneratorHelper.createActivityEdge(n9CheckIfRepairSuccessful, trueFinalNode,
										ActivityEdgeGuardEnumeration.BOOLEAN, repairCallExpression));

						activity.getEdges().add(
								SDGeneratorHelper.createActivityEdge(n9CheckIfRepairSuccessful, n7MatchRules,
										ActivityEdgeGuardEnumeration.ELSE, null));

							lastNode = n8CheckRuleType;
					}
				}

				/*
				 * Create transition from the last activity node back to n7
				 */
				activity.getEdges().add(
						SDGeneratorHelper.createActivityEdge(lastNode, n7MatchRules, ActivityEdgeGuardEnumeration.FAILURE, null));
			}

			/*
			 * Create expression activity node to delete elements
			 */
			ExpressionActivityNode n13DeleteElements = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n13DeleteElements.setName("delete elements");

			n13DeleteElements.setActivity(activity);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createCallDeleteElementsExpressionActivityNodeContent(rule, direction, ruleClass,
					n13DeleteElements);

			/*
			 * Create another expression activity node to delete elements
			 */
			// ExpressionActivityNode n13DeleteElements =
			// NodesFactory.eINSTANCE.createExpressionActivityNode();
			// n13DeleteElements.setName("delete elements");
			//
			// n13DeleteElements.setActivity(activity);
			//
			// // Create content
			// createCallDeleteElementsExpressionActivityNodeContent(rule,
			// direction, ruleClass, n13DeleteElements);

			/*
			 * Create story action node to add correspondence node to the
			 * ruleSet's list of correspondence nodes to delete
			 */
			StoryActionNode n12AddToCorrNodesToDelete = NodesFactory.eINSTANCE.createStoryActionNode();
			n12AddToCorrNodesToDelete.setName("add to corrNodesToDelete");

			n12AddToCorrNodesToDelete.setActivity(activity);

			// Create content
			this.createAddToCorrNodesToDeleteStoryActionNodeContent(rule, n12AddToCorrNodesToDelete);

			/*
			 * Create false final node
			 */
			ActivityFinalNode falseFinalNode = NodesFactory.eINSTANCE.createActivityFinalNode();
			falseFinalNode.setName("stop false");

			falseFinalNode.setActivity(activity);

			// Create expression
			LiteralDeclarationAction falseLiteralAction = SDGeneratorHelper.createLiteralDeclarationAction(
					OptimizedIncrRepairGenerationStrategy.FALSE, null, OptimizedIncrRepairGenerationStrategy.FALSE,
					EcorePackage.eINSTANCE.getEBooleanObject());

			CallActionExpression falseLiteralExpression = SDGeneratorHelper.createCallActionExpression(
					OptimizedIncrRepairGenerationStrategy.FALSE, null, falseLiteralAction);

			falseFinalNode.setReturnValue(falseLiteralExpression);

			/*
			 * Create transitions
			 */
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(initialNode, n0MatchRuleSet, ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n0MatchRuleSet, n1MatchRulePattern, ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n1MatchRulePattern, n2CheckIfAttributesSynchronized,
							ActivityEdgeGuardEnumeration.SUCCESS, null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n1MatchRulePattern, n5CheckIfModelElementsContainedInModel,
							ActivityEdgeGuardEnumeration.FAILURE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n5CheckIfModelElementsContainedInModel, n6CheckIfRepairSuccessful,
							ActivityEdgeGuardEnumeration.BOOLEAN,
							this.createCheckIfModelElementsContainedInModelExpression(rule, direction)));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n5CheckIfModelElementsContainedInModel, n13DeleteElements,
							ActivityEdgeGuardEnumeration.ELSE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n13DeleteElements, falseFinalNode, ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n2CheckIfAttributesSynchronized, n3AddModTagToQueue,
							ActivityEdgeGuardEnumeration.ELSE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n2CheckIfAttributesSynchronized, n4AddModTagToQueue,
							ActivityEdgeGuardEnumeration.BOOLEAN,
							this.createCallSynchronizeAttributesExpression(rule, ruleInformationMap, direction)));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n3AddModTagToQueue, falseFinalNode, ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n4AddModTagToQueue, trueFinalNode, ActivityEdgeGuardEnumeration.NONE, null));
			final RuleInformationContainer ric = ruleInformationMap.get( rule );

			switch (direction)
			{
				case FORWARD:
				{
					activity.getEdges().add(
							SDGeneratorHelper.createActivityEdge(n6CheckIfRepairSuccessful, trueFinalNode,
									ActivityEdgeGuardEnumeration.BOOLEAN, this.createCallRepairExpression(GenerationStrategy.THIS,
											ruleClass, ric.repairForwardActivity)));
					break;
				}
				case MAPPING:
				{
					activity.getEdges().add(
							SDGeneratorHelper.createActivityEdge(n6CheckIfRepairSuccessful, trueFinalNode,
									ActivityEdgeGuardEnumeration.BOOLEAN, this.createCallRepairExpression(GenerationStrategy.THIS,
											ruleClass, ric.repairMappingActivity)));
					break;
				}
				case REVERSE:
				{
					activity.getEdges().add(
							SDGeneratorHelper.createActivityEdge(n6CheckIfRepairSuccessful, trueFinalNode,
									ActivityEdgeGuardEnumeration.BOOLEAN, this.createCallRepairExpression(GenerationStrategy.THIS,
											ruleClass, ric.repairReverseActivity)));
					break;
				}
			}

			if (((TGGDiagram) rule.eContainer()).getTggRules().size() > 2)
			{
				activity.getEdges().add(
						SDGeneratorHelper.createActivityEdge(n6CheckIfRepairSuccessful, n7MatchRules, ActivityEdgeGuardEnumeration.ELSE,
								null));

				activity.getEdges().add(
						SDGeneratorHelper.createActivityEdge(n7MatchRules, n12AddToCorrNodesToDelete, ActivityEdgeGuardEnumeration.END,
								null));
			}
			else
			{
				activity.getEdges().add(
						SDGeneratorHelper.createActivityEdge(n6CheckIfRepairSuccessful, n12AddToCorrNodesToDelete,
								ActivityEdgeGuardEnumeration.ELSE, null));
			}

			activity.getEdges().add(
					SDGeneratorHelper
							.createActivityEdge(n12AddToCorrNodesToDelete, falseFinalNode, ActivityEdgeGuardEnumeration.NONE, null));

		}

		private void createAddToCorrNodesToDeleteStoryActionNodeContent(TGGRule rule, StoryActionNode storyActionNode)
				throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.createRuleSetObject = true;
			options.ruleSetObjectBinding = BindingTypeEnumeration.BOUND;

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			/*
			 * Create story pattern object for corrNode
			 */
			StoryPatternObject spo = SDGeneratorHelper.createStoryPatternObject(OptimizedIncrRepairGenerationStrategy.CORR_NODE, null,
					MotePackage.Literals.TGG_NODE, StoryPatternModifierEnumeration.NONE, BindingTypeEnumeration.BOUND);

			result.storyPatternObjects.add(spo);

			/*
			 * Create corrNodesToDelete link between ruleSet and corrNode
			 */
			StoryPatternLink spl = SDGeneratorHelper.createStoryPatternLink(result.ruleSetObject, spo,
					RulesPackage.Literals.TGG_RULE_SET__CORR_NODES_TO_DELETE, null, null, null, StoryPatternModifierEnumeration.CREATE, null);

			result.storyPatternLinks.add(spl);

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

		private void createMatchRuleSetStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule, EClass ruleClass)
				throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.createThisObject = true;
			options.thisEClass = ruleClass;

			options.createRuleSetObject = true;
			options.createThisToRuleSet_ruleSet_Link = true;

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

		private Expression createCheckIfModelElementsContainedInModelExpression(TGGRule rule, TransformationDirection direction)
		{
			/*
			 * Create an expression that checks if a source element is still
			 * contained in the model, i.e. if eContainer() is not null.
			 */
			String expressionString = "";

			if (direction == TransformationDirection.FORWARD || direction == TransformationDirection.MAPPING)
			{
				expressionString += OptimizedIncrRepairGenerationStrategy.CORR_NODE
						+ ".sources->exists(e|e.oclAsType(ecore::EObject).eContainer() <> null)";

			}

			if (direction == TransformationDirection.MAPPING)
			{
				expressionString += " or ";
			}

			if (direction == TransformationDirection.REVERSE || direction == TransformationDirection.MAPPING)
			{
				expressionString += OptimizedIncrRepairGenerationStrategy.CORR_NODE
						+ ".targets->exists(e|e.oclAsType(ecore::EObject).eContainer() <> null)";
			}

			return SDGeneratorHelper.createStringExpression(SDGeneratorHelper.OCL_EXPRESSION_LANGUAGE, expressionString);
		}

		private Expression createCallSynchronizeAttributesExpression(TGGRule rule,
				Map<TGGRule, RuleInformationContainer> ruleInformationMap, TransformationDirection direction)
		{
			RuleInformationContainer ric = ruleInformationMap.get(rule);

			Activity activity = null;

			switch (direction)
			{
				case FORWARD:
				{
					activity = ric.synchronizeAttributesForwardActivity;
					break;
				}
				case MAPPING:
				{
					activity = ric.synchronizeAttributesMappingActivity;
					break;
				}
				case REVERSE:
				{
					activity = ric.synchronizeAttributesReverseActivity;
					break;
				}
			}

			/*
			 * Create variable reference for THIS
			 */
			VariableReferenceAction thisReferenceAction = SDGeneratorHelper.createVariableReferenceAction(GenerationStrategy.THIS, null,
					GenerationStrategy.THIS, ric.ruleClass);

			CallActionExpression thisReferenceExpression = SDGeneratorHelper.createCallActionExpression(GenerationStrategy.THIS, null,
					thisReferenceAction);

			/*
			 * Crate call SDI expression
			 */
			CallStoryDiagramInterpreterAction callSDIAction = SDGeneratorHelper.createCallSDIAction(activity.getName(), null,
					thisReferenceExpression, ric.ruleClass, EcorePackage.eINSTANCE.getEBooleanObject(), activity);

			CallActionExpression callSDIExpression = SDGeneratorHelper.createCallActionExpression(activity.getName(), null, callSDIAction);

			/*
			 * Create parameters
			 */

			/*
			 * Correspondence nodes
			 */
			for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
			{
				if (ce instanceof CorrespondenceNode)
				{
					CorrespondenceNode cn = (CorrespondenceNode) ce;

					VariableReferenceAction variableReferenceAction = SDGeneratorHelper.createVariableReferenceAction(cn.getName(), null,
							cn.getName(), cn.getClassifier());

					CallActionExpression variableReferenceExpression = SDGeneratorHelper.createCallActionExpression(cn.getName(), null,
							variableReferenceAction);

					CallActionParameter callActionParameter = SDGeneratorHelper.createCallActionParameter(ce.getName(), null,
							((CorrespondenceNode) ce).getClassifier(), variableReferenceExpression);

					callSDIAction.getParameters().add(callActionParameter);
				}
			}

			/*
			 * Create parameters for the source domain elements
			 */
			for (ModelElement me : rule.getSourceDomain().getModelElements())
			{
				if (me instanceof ModelObject)
				{
					ModelObject mo = (ModelObject) me;

					VariableReferenceAction variableReferenceAction = SDGeneratorHelper.createVariableReferenceAction(mo.getName(), null,
							mo.getName(), mo.getClassifier());

					CallActionExpression variableReferenceExpression = SDGeneratorHelper.createCallActionExpression(mo.getName(), null,
							variableReferenceAction);

					CallActionParameter callActionParameter = SDGeneratorHelper.createCallActionParameter(mo.getName()
							+ OptimizedIncrRepairGenerationStrategy.PARAMETER, null, EcorePackage.Literals.EOBJECT,
							variableReferenceExpression);

					callSDIAction.getParameters().add(callActionParameter);
				}
			}

			/*
			 * Create parameters for the target domain elements
			 */
			for (ModelElement me : rule.getTargetDomain().getModelElements())
			{
				if (me instanceof ModelObject)
				{
					ModelObject mo = (ModelObject) me;

					VariableReferenceAction variableReferenceAction = SDGeneratorHelper.createVariableReferenceAction(mo.getName(), null,
							mo.getName(), mo.getClassifier());

					CallActionExpression variableReferenceExpression = SDGeneratorHelper.createCallActionExpression(mo.getName(), null,
							variableReferenceAction);

					CallActionParameter callActionParameter = SDGeneratorHelper.createCallActionParameter(mo.getName()
							+ OptimizedIncrRepairGenerationStrategy.PARAMETER, null, EcorePackage.Literals.EOBJECT,
							variableReferenceExpression);

					callSDIAction.getParameters().add(callActionParameter);
				}
			}

			return callSDIExpression;
		}

		private Expression createCallRepairExpression(String instanceVariableName, EClass instanceVariableEClass, Activity repairActivity)
		{
			/*
			 * Create reference for instance variable
			 */
			VariableReferenceAction instanceReferenceAction = SDGeneratorHelper.createVariableReferenceAction(instanceVariableName, null,
					instanceVariableName, instanceVariableEClass);

			CallActionExpression instanceReferenceExpression = SDGeneratorHelper.createCallActionExpression(instanceVariableName, null,
					instanceReferenceAction);

			/*
			 * Create parameter for correspondence node
			 */
			VariableReferenceAction corrNodeReferenceAction = SDGeneratorHelper.createVariableReferenceAction(
					OptimizedIncrRepairGenerationStrategy.CORR_NODE, null, OptimizedIncrRepairGenerationStrategy.CORR_NODE,
					MotePackage.eINSTANCE.getTGGNode());

			CallActionExpression corrNodeReferenceExpression = SDGeneratorHelper.createCallActionExpression(
					OptimizedIncrRepairGenerationStrategy.CORR_NODE, null, corrNodeReferenceAction);

			/*
			 * Create action to call SD interpreter
			 */
			CallStoryDiagramInterpreterAction callSDIAction = SDGeneratorHelper.createCallSDIAction(repairActivity.getName(), null,
					instanceReferenceExpression, instanceVariableEClass, EcorePackage.eINSTANCE.getEBooleanObject(), repairActivity);

			CallActionParameter callActionParameter = SDGeneratorHelper.createCallActionParameter(
					OptimizedIncrRepairGenerationStrategy.CORR_NODE, null, MotePackage.Literals.TGG_NODE, corrNodeReferenceExpression);

			callSDIAction.getParameters().add(callActionParameter);

			CallActionExpression callSDIExpression = SDGeneratorHelper.createCallActionExpression(repairActivity.getName(), null,
					callSDIAction);

			return callSDIExpression;

		}

		private void createCheckRuleTypeStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				Map<TGGRule, RuleInformationContainer> ruleInformationMap) throws RuleGenerationException
		{
			final RuleInformationContainer ric = ruleInformationMap.get(rule );
			/*
			 * Create story pattern object for the rule to match
			 */
			StoryPatternObject ruleSpo = SDGeneratorHelper.createStoryPatternObject(rule.getName(), null,
					ric.ruleClass, StoryPatternModifierEnumeration.NONE, BindingTypeEnumeration.BOUND);

			storyActionNode.getStoryPatternObjects().add(ruleSpo);

			/*
			 * Create direct assignment expression
			 */
			VariableReferenceAction ruleReferenceAction = SDGeneratorHelper.createVariableReferenceAction(
					OptimizedIncrRepairGenerationStrategy.RULE, null, OptimizedIncrRepairGenerationStrategy.RULE,
					RulesPackage.eINSTANCE.getTGGRule());

			CallActionExpression ruleReferenceExpression = SDGeneratorHelper.createCallActionExpression(
					OptimizedIncrRepairGenerationStrategy.RULE, null, ruleReferenceAction);

			ruleSpo.setDirectAssignmentExpression(ruleReferenceExpression);
		}

		private void createMatchRulesStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule, EClass ruleClass)
				throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.createRuleSetObject = true;
			options.ruleSetObjectBinding = BindingTypeEnumeration.BOUND;

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);

			/*
			 * Create story pattern object for the rule to match
			 */
			StoryPatternObject ruleSpo = SDGeneratorHelper.createStoryPatternObject(OptimizedIncrRepairGenerationStrategy.RULE, null,
					RulesPackage.eINSTANCE.getTGGRule(), StoryPatternModifierEnumeration.NONE, BindingTypeEnumeration.UNBOUND);

			storyActionNode.getStoryPatternObjects().add(ruleSpo);

			/*
			 * Create link from ruleSet to ruleSpo
			 */
			StoryPatternLink rulesLink = SDGeneratorHelper.createStoryPatternLink(result.ruleSetObject, ruleSpo,
					RulesPackage.eINSTANCE.getTGGRuleSet_Rules(), null, null, null, StoryPatternModifierEnumeration.NONE, null);

			storyActionNode.getStoryPatternLinks().add(rulesLink);

		}

		private void createMatchRulePatternStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EClass ruleClass) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.direction = direction;

			options.createThisObject = true;
			options.thisEClass = ruleClass;

			options.createThisToChildCorrespondenceNodes_createdCorrNodes_Link = true;

			options.createRuleSetObject = true;
			options.ruleSetObjectBinding = BindingTypeEnumeration.BOUND;

			options.createRuleSetToChildCorrespondenceNodes_correspondenceNodes_Link = true;

			options.createRuleSetToCreatedNodes_sourceModelElementsLinks = true;
			options.createRuleSetToCreatedNodes_targetModelElementsLinks = true;

			options.createParentCorrespondenceNodes = true;

			options.createParentCorrespondenceNodesSourcesLinks = true;
			options.createParentCorrespondenceNodesTargetsLinks = true;

			options.createParentCorrespondenceNodesToChildCorrespondenceNodes_next_Links = true;

			options.createChildCorrespondenceNodes = true;
			options.childCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createChildCorrespondenceNodesSourcesLinks = true;
			options.createChildCorrespondenceNodesTargetsLinks = true;

			options.createChildCorrespondenceNodesToInputCorrespondenceNode_inputNode_Link = true;
			options.createChildCorrespondenceNodesToRuleSet_ruleSet_Links = true;

			options.createLeftParentNodes = true;
			options.createLeftParentLinks = true;

			options.createRightParentNodes = true;
			options.createRightParentLinks = true;

			options.createLeftCreatedNodes = true;
			options.createLeftCreatedLinks = true;

			options.createRightCreatedNodes = true;
			options.createRightCreatedLinks = true;

			options.createLeftConstraints = true;
			options.createRightConstraints = true;

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			/*
			 * Create direct assignment expression for the created
			 * correspondence node
			 */
			VariableReferenceAction corrNodeReferenceAction = SDGeneratorHelper.createVariableReferenceAction(
					OptimizedIncrRepairGenerationStrategy.CORR_NODE, null, OptimizedIncrRepairGenerationStrategy.CORR_NODE,
					MotePackage.eINSTANCE.getTGGNode());

			((StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(result.childCorrespondenceNodes.get(0)))
					.setDirectAssignmentExpression(SDGeneratorHelper.createCallActionExpression(
							OptimizedIncrRepairGenerationStrategy.CORR_NODE, null, corrNodeReferenceAction));

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
			
			// DB: Generate the action node constraints.
			storyActionNode.getConstraints().addAll( SDGeneratorHelper.copyAllWithNewUuids( rule.getConstraintExpressions() ) );
			
			if ( direction == TransformationDirection.FORWARD ) {
				
				// DB: Rule directional transformation constraints.
				storyActionNode.getConstraints().addAll( SDGeneratorHelper.copyAllWithNewUuids( rule.getForwardConstraintExpressions() ) );
			}
			else if ( direction == TransformationDirection.REVERSE ) {
				
				// DB: Rule directional transformation constraints.
				storyActionNode.getConstraints().addAll( SDGeneratorHelper.copyAllWithNewUuids( rule.getReverseConstraintExpressions() ) );
			}
		}
	}
	
	private RuleInformationContainer copy( final RuleInformationContainer p_ruleInformationContainer ) {
		final RuleInformationContainer ruleInfoCont = new RuleInformationContainer();
		ruleInfoCont.isAxiom = p_ruleInformationContainer.isAxiom;
		ruleInfoCont.tggRule = p_ruleInformationContainer.tggRule;
		ruleInfoCont.ruleClass = p_ruleInformationContainer.ruleClass;

		ruleInfoCont.transformationForwardOperation = p_ruleInformationContainer.transformationForwardOperation;
		ruleInfoCont.transformationMappingOperation = p_ruleInformationContainer.transformationMappingOperation;
		ruleInfoCont.transformationReverseOperation = p_ruleInformationContainer.transformationReverseOperation;

		ruleInfoCont.transformationForwardActivity = EcoreUtil.copy( p_ruleInformationContainer.transformationForwardActivity );
		ruleInfoCont.transformationMappingActivity = EcoreUtil.copy( p_ruleInformationContainer.transformationMappingActivity );
		ruleInfoCont.transformationReverseActivity = EcoreUtil.copy( p_ruleInformationContainer.transformationReverseActivity );


		ruleInfoCont.synchronizationForwardOperation = p_ruleInformationContainer.synchronizationForwardOperation;
		ruleInfoCont.synchronizationMappingOperation = p_ruleInformationContainer.synchronizationMappingOperation;
		ruleInfoCont.synchronizationReverseOperation = p_ruleInformationContainer.synchronizationReverseOperation;

		ruleInfoCont.synchronizationForwardActivity = EcoreUtil.copy( p_ruleInformationContainer.synchronizationForwardActivity );
		ruleInfoCont.synchronizationMappingActivity = EcoreUtil.copy( p_ruleInformationContainer.synchronizationMappingActivity );
		ruleInfoCont.synchronizationReverseActivity = EcoreUtil.copy( p_ruleInformationContainer.synchronizationReverseActivity );

		ruleInfoCont.synchronizeAttributesForwardActivity = EcoreUtil.copy( p_ruleInformationContainer.synchronizeAttributesForwardActivity );
		ruleInfoCont.synchronizeAttributesMappingActivity = EcoreUtil.copy( p_ruleInformationContainer.synchronizeAttributesMappingActivity );
		ruleInfoCont.synchronizeAttributesReverseActivity = EcoreUtil.copy( p_ruleInformationContainer.synchronizeAttributesReverseActivity );

		ruleInfoCont.repairForwardActivity = EcoreUtil.copy( p_ruleInformationContainer.repairForwardActivity );
		ruleInfoCont.repairMappingActivity = EcoreUtil.copy( p_ruleInformationContainer.repairMappingActivity );
		ruleInfoCont.repairReverseActivity = EcoreUtil.copy( p_ruleInformationContainer.repairReverseActivity );
		
		ruleInfoCont.acceptsParentCorrNodeOperation = p_ruleInformationContainer.acceptsParentCorrNodeOperation;
		
		return ruleInfoCont;
	}
	
	private Activity getExistActivity( 	final TGGRule p_rule,
										final String p_activityName ) {
		final Resource tggResource = p_rule.eResource();
		final String ruleModelDir = tggResource.getURI().trimSegments( 1 ).trimFileExtension().toString();
		final URI resUri = URI.createURI( ruleModelDir + "/story/" + p_activityName + ".story", true );
		
		// Instantiate new resource set to avoid too large memory footprint
		final ResourceSet resourceSet = new ResourceSetImpl();
		
		if ( resourceSet.getURIConverter().exists( resUri, null ) ) {
			final Resource repairRes = resourceSet.getResource( resUri, true );
			final ActivityDiagram diagram = (ActivityDiagram) repairRes.getContents().get( 0 );
		
			return diagram.getActivities().get( 0 );
		}
		
		return null;
	}

	protected class SynchronizeAttributesActivityGenerator
	{

		protected void createSynchronizeAttributesActivity(TGGRule rule, TransformationDirection direction, EClass ruleClass,
				Activity activity/*, Map<TGGRule, RuleInformationContainer> ruleInformationMap*/) throws RuleGenerationException
		{
			/*
			 * Set activity's attributes
			 */
			activity.setName(rule.getName() + "_" + direction.getName().toLowerCase() + "SynchronizeAttributes");
			activity.setDescription(rule.getDescription());

			/*
			 * Create initial node
			 */
			InitialNode initialNode = NodesFactory.eINSTANCE.createInitialNode();
			initialNode.setName("start");

			initialNode.setActivity(activity);

			/*
			 * Create story action node to bind elements
			 */
			StoryActionNode an0BindElementsSAN = NodesFactory.eINSTANCE.createStoryActionNode();
			an0BindElementsSAN.setName("bind elements");

			an0BindElementsSAN.setActivity(activity);

			// Create content
			this.createBindElementsStoryActionNodeContent(an0BindElementsSAN, rule);

			/*
			 * Create expression activity node to evaluate rule variables
			 */
			ExpressionActivityNode an0aEvaluateRuleVarsEAN = NodesFactory.eINSTANCE.createExpressionActivityNode();
			an0aEvaluateRuleVarsEAN.setName("evaluate rule variables");

			an0aEvaluateRuleVarsEAN.setActivity(activity);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createEvaluateRuleVariablesExpressionActivityNodeContent(rule,
					an0aEvaluateRuleVarsEAN, direction);

			/*
			 * Create story action node to compare attribute values
			 */
			StoryActionNode an1CompareAttributeValues = NodesFactory.eINSTANCE.createStoryActionNode();
			an1CompareAttributeValues.setName("compare attribute values");

			an1CompareAttributeValues.setActivity(activity);

			// Create content
			this.createCompareAttributeValuesStoryActionNodeContent(an1CompareAttributeValues, rule, direction);

			/*
			 * Create story action node to synchronize attribute values
			 */
			StoryActionNode an2SynchronizeAttributeValues = NodesFactory.eINSTANCE.createStoryActionNode();
			an2SynchronizeAttributeValues.setName("synchronize attribute values");

			an2SynchronizeAttributeValues.setActivity(activity);

			// Create content
			this.createSynchronizeAttributeValuesStoryActionNodeContent(an2SynchronizeAttributeValues, rule, direction);

			/*
			 * Create expression activity node to print error message
			 */
			ExpressionActivityNode an3PrintErrorMessage = NodesFactory.eINSTANCE.createExpressionActivityNode();
			an3PrintErrorMessage.setName("print error");

			an3PrintErrorMessage.setActivity(activity);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createPrintErrorMessageExpressionActivityNodeContent(an3PrintErrorMessage, rule,
					ruleClass, "ERROR in synchronizeAttributeValues: Structure is invalid.");

			/*
			 * Create final node with return value true
			 */
			ActivityFinalNode trueFinalNode = NodesFactory.eINSTANCE.createActivityFinalNode();
			trueFinalNode.setName("stop true");

			trueFinalNode.setActivity(activity);

			// Create literal true
			LiteralDeclarationAction trueLiteralAction = SDGeneratorHelper.createLiteralDeclarationAction(
					OptimizedIncrRepairGenerationStrategy.TRUE, null, OptimizedIncrRepairGenerationStrategy.TRUE,
					EcorePackage.eINSTANCE.getEBooleanObject());
			CallActionExpression trueLiteralExpression = SDGeneratorHelper.createCallActionExpression(
					OptimizedIncrRepairGenerationStrategy.TRUE, null, trueLiteralAction);
			trueFinalNode.setReturnValue(trueLiteralExpression);

			/*
			 * Create final node with return value false
			 */
			ActivityFinalNode falseFinalNode = NodesFactory.eINSTANCE.createActivityFinalNode();
			falseFinalNode.setName("stop false");

			falseFinalNode.setActivity(activity);

			// Create literal false
			LiteralDeclarationAction falseLiteralAction = SDGeneratorHelper.createLiteralDeclarationAction(
					OptimizedIncrRepairGenerationStrategy.FALSE, null, OptimizedIncrRepairGenerationStrategy.FALSE,
					EcorePackage.eINSTANCE.getEBooleanObject());
			CallActionExpression falseLiteralExpression = SDGeneratorHelper.createCallActionExpression(
					OptimizedIncrRepairGenerationStrategy.FALSE, null, falseLiteralAction);
			falseFinalNode.setReturnValue(falseLiteralExpression);

			/*
			 * Create transitions
			 */
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(initialNode, an0BindElementsSAN, ActivityEdgeGuardEnumeration.NONE, null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(an0BindElementsSAN, an0aEvaluateRuleVarsEAN, ActivityEdgeGuardEnumeration.NONE,
							null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(an0aEvaluateRuleVarsEAN, an1CompareAttributeValues,
							ActivityEdgeGuardEnumeration.NONE, null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(an1CompareAttributeValues, falseFinalNode, ActivityEdgeGuardEnumeration.SUCCESS,
							null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(an1CompareAttributeValues, an2SynchronizeAttributeValues,
							ActivityEdgeGuardEnumeration.FAILURE, null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(an2SynchronizeAttributeValues, trueFinalNode,
							ActivityEdgeGuardEnumeration.SUCCESS, null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(an2SynchronizeAttributeValues, an3PrintErrorMessage,
							ActivityEdgeGuardEnumeration.FAILURE, null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(an3PrintErrorMessage, falseFinalNode, ActivityEdgeGuardEnumeration.NONE, null));
		}

		private void createBindElementsStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.createParentCorrespondenceNodes = true;
			options.parentCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createChildCorrespondenceNodes = true;
			options.childCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftParentNodes = true;
			options.leftParentNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftCreatedNodes = true;
			options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;

			options.createRightParentNodes = true;
			options.rightParentNodesBinding = BindingTypeEnumeration.BOUND;

			options.createRightCreatedNodes = true;
			options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			/*
			 * Set direct assignment expressions for left model elements
			 */
			for (ModelElement me : rule.getSourceDomain().getModelElements())
			{
				if (me instanceof ModelObject)
				{
					StoryPatternObject spo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(me);

					VariableReferenceAction variableReferenceAction = SDGeneratorHelper.createVariableReferenceAction(spo.getName()
							+ OptimizedIncrRepairGenerationStrategy.PARAMETER, null, spo.getName()
							+ OptimizedIncrRepairGenerationStrategy.PARAMETER, EcorePackage.eINSTANCE.getEObject());

					CallActionExpression variableReferenceExpression = SDGeneratorHelper.createCallActionExpression(spo.getName()
							+ OptimizedIncrRepairGenerationStrategy.PARAMETER, null, variableReferenceAction);

					spo.setDirectAssignmentExpression(variableReferenceExpression);
				}
			}

			/*
			 * Set direct assignment expressions for right model elements
			 */
			for (ModelElement me : rule.getTargetDomain().getModelElements())
			{
				if (me instanceof ModelObject)
				{
					StoryPatternObject spo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(me);

					VariableReferenceAction variableReferenceAction = SDGeneratorHelper.createVariableReferenceAction(spo.getName()
							+ OptimizedIncrRepairGenerationStrategy.PARAMETER, null, spo.getName()
							+ OptimizedIncrRepairGenerationStrategy.PARAMETER, EcorePackage.eINSTANCE.getEObject());

					CallActionExpression variableReferenceExpression = SDGeneratorHelper.createCallActionExpression(spo.getName()
							+ OptimizedIncrRepairGenerationStrategy.PARAMETER, null, variableReferenceAction);

					spo.setDirectAssignmentExpression(variableReferenceExpression);
				}
			}

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

		private void createCompareAttributeValuesStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.createParentCorrespondenceNodes = true;
			options.parentCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createChildCorrespondenceNodes = true;
			options.childCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftParentNodes = true;
			options.leftParentNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftCreatedNodes = true;
			options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;

			options.createRightParentNodes = true;
			options.rightParentNodesBinding = BindingTypeEnumeration.BOUND;

			options.createRightCreatedNodes = true;
			options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftAttributeChecks = true;
			options.createRightAttributeChecks = true;

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

		private void createSynchronizeAttributeValuesStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.createParentCorrespondenceNodes = true;
			options.parentCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createChildCorrespondenceNodes = true;
			options.childCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftParentNodes = true;
			options.leftParentNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftCreatedNodes = true;
			options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;

			options.createRightParentNodes = true;
			options.rightParentNodesBinding = BindingTypeEnumeration.BOUND;

			options.createRightCreatedNodes = true;
			options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;

			if (direction == TransformationDirection.FORWARD)
			{
				options.createRightAttributeAssignments = true;
			}
			else if (direction == TransformationDirection.MAPPING)
			{

			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.createLeftAttributeAssignments = true;
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}
	}

	protected class RepairStructureActivityGenerator
	{
		protected void createRepairStructureActivity(TGGRule rule, TransformationDirection direction, EClass ruleClass, Activity activity/*,
				Map<TGGRule, RuleInformationContainer> ruleInformationMap*/) throws RuleGenerationException
		{
			/*
			 * set activity's attributes
			 */
			activity.setName(rule.getName() + "_" + direction.getName().toLowerCase() + "RepairStructure");
			activity.setDescription(rule.getDescription());

			/*
			 * Create initial node
			 */
			InitialNode initialNode = NodesFactory.eINSTANCE.createInitialNode();
			initialNode.setName("start");

			initialNode.setActivity(activity);

			/*
			 * Create story action nodes to match created source elements.
			 * Create one story action node for each source element
			 */
			List<StoryActionNode> n1MatchCreatedSourceElements = this.createMatchCreatedSourceElementsStoryActionNodes(rule, direction,
					ruleClass, activity);

			/*
			 * Create merge node to merge control flows
			 */
			MergeNode n2MergeFlows = NodesFactory.eINSTANCE.createMergeNode();
			n2MergeFlows.setName("merge flows");

			n2MergeFlows.setActivity(activity);

			/*
			 * Create story action node to match source and parent elements
			 */
			StoryActionNode n3MatchSourceAndParentElements = NodesFactory.eINSTANCE.createStoryActionNode();
			n3MatchSourceAndParentElements.setName("match source and parent elements");

			n3MatchSourceAndParentElements.setActivity(activity);

			// Create content
			this.createMatchSourceAndParentElementsStoryActionNodeContent(n3MatchSourceAndParentElements, rule, direction, ruleClass);

			/*
			 * Create expression activity node to evaluate rule variables
			 */
			ExpressionActivityNode n3aEvaluateRuleVars = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n3aEvaluateRuleVars.setName("evaluate rule variables");

			n3aEvaluateRuleVars.setActivity(activity);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createEvaluateRuleVariablesExpressionActivityNodeContent(rule, n3aEvaluateRuleVars,
					direction);

			/*
			 * Create decision node to check if the correspondence nodes of the
			 * created source elements are either null or the existing
			 * correspondence node
			 */
			DecisionNode n4CheckIfCorrNodesNullOrExistingCorrNode = NodesFactory.eINSTANCE.createDecisionNode();
			n4CheckIfCorrNodesNullOrExistingCorrNode.setActivity(activity);

			/*
			 * Create story action node to match target elements
			 */
			StoryActionNode n5MatchTargetElements = NodesFactory.eINSTANCE.createStoryActionNode();
			n5MatchTargetElements.setName("match target elements");

			n5MatchTargetElements.setActivity(activity);

			// Create content
			this.createMatchTargetElementsStoryActionNodeContent(n5MatchTargetElements, rule, direction, ruleClass);

			/*
			 * Create decision node to check if the new input node is an
			 * ancestor of the correspondence node
			 */
			DecisionNode n6CheckIfInputNodeIsAncestor = NodesFactory.eINSTANCE.createDecisionNode();
			n6CheckIfInputNodeIsAncestor.setActivity(activity);

			/*
			 * Create expression activity node to add modification tag to queue
			 */
			ExpressionActivityNode n7AddModTag = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n7AddModTag.setName("add modification tag to queue");

			n7AddModTag.setActivity(activity);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createAddModificationTagToQueueExpressionActivityNodeContent(n7AddModTag, rule,
					ruleClass, /*ruleInformationMap,*/ false, false, true, direction);

			/*
			 * Create expression activity node to delete correspondence links to
			 * parents
			 */
			ExpressionActivityNode n8DeleteCorrespondenceLinks = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n8DeleteCorrespondenceLinks.setName("delete correspondence links to parents");

			n8DeleteCorrespondenceLinks.setActivity(activity);

			// Create content
			this.createDeleteCorrespondenceLinksExpressionActivityNodeContent(n8DeleteCorrespondenceLinks, rule);

			/*
			 * Create story action node to delete target links to parents
			 */
			StoryActionNode n9DeleteTargetLinksToParents = NodesFactory.eINSTANCE.createStoryActionNode();
			n9DeleteTargetLinksToParents.setName("delete target links to parents");

			n9DeleteTargetLinksToParents.setActivity(activity);

			// Create content
			this.createDeleteTargetLinksToParentsStoryActionNodeContent(n9DeleteTargetLinksToParents, rule, direction, ruleClass);

			/*
			 * Create story action node to create links to new parents
			 */
			StoryActionNode n10CreateLinksToNewParents = NodesFactory.eINSTANCE.createStoryActionNode();
			n10CreateLinksToNewParents.setName("create links to new parents");

			n10CreateLinksToNewParents.setActivity(activity);

			// Create content
			this.createLinksToNewParentsStoryActionNodeContent(n10CreateLinksToNewParents, rule, direction, ruleClass);

			/*
			 * Create story action node to delete sources links of the existing
			 * correspondence node
			 */
			StoryActionNode n11DeleteSourcesLinks = NodesFactory.eINSTANCE.createStoryActionNode();
			n11DeleteSourcesLinks.setName("delete links to source elements");
			n11DeleteSourcesLinks.setForEach(true);

			n11DeleteSourcesLinks.setActivity(activity);

			// Create content
			this.createSourcesLinksStoryActionNodeContent(n11DeleteSourcesLinks, rule, direction, ruleClass);

			/*
			 * Create story action node to delete the targets links of the
			 * existing correspondence node
			 */
			StoryActionNode n12DeleteTargetsLinks = NodesFactory.eINSTANCE.createStoryActionNode();
			n12DeleteTargetsLinks.setName("delete links to target elements");
			n12DeleteTargetsLinks.setForEach(true);

			n12DeleteTargetsLinks.setActivity(activity);

			// Create content
			this.createTargetsLinksStoryActionNodeContent(n12DeleteTargetsLinks, rule, direction, ruleClass);

			/*
			 * Create story action node to create new correspondence and target
			 * elements
			 */
			StoryActionNode n13CreateNewCorrespondenceAndTargetElements = NodesFactory.eINSTANCE.createStoryActionNode();
			n13CreateNewCorrespondenceAndTargetElements.setName("create new correspondence and target elements");

			n13CreateNewCorrespondenceAndTargetElements.setActivity(activity);

			// Create content
			this.createCreateNewCorrespondenceAndTargetElementsStoryActionNodeContent(n13CreateNewCorrespondenceAndTargetElements, rule,
					direction, ruleClass);

			/*
			 * Create story action node to reroute next-Links
			 */
			StoryActionNode n14RerouteNextLinks = NodesFactory.eINSTANCE.createStoryActionNode();
			n14RerouteNextLinks.setName("reroute next links");
			n14RerouteNextLinks.setForEach(true);

			n14RerouteNextLinks.setActivity(activity);

			// Create content
			this.createRerouteNextLinksStoryActionNodeContent(n14RerouteNextLinks, rule, direction);

			/*
			 * Create story action node to reroute input node links
			 */
			// StoryActionNode n15RerouteInputNodeLinks =
			// NodesFactory.eINSTANCE.createStoryActionNode();
			// n15RerouteInputNodeLinks.setName("reroute input node links");
			// n15RerouteInputNodeLinks.setForEach(true);
			//
			// n15RerouteInputNodeLinks.setActivity(activity);
			//
			// // Create content
			// createRerouteInputNodeLinksStoryActionNode(n15RerouteInputNodeLinks,
			// rule, direction);

			/*
			 * Create expression activity node to delete old elements
			 */
			ExpressionActivityNode n16DeleteOldElements = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n16DeleteOldElements.setName("delete old elements");

			n16DeleteOldElements.setActivity(activity);

			// Create content
			this.createDeleteOldElementsExpressionActivityNodeContent(rule, direction, ruleClass, n16DeleteOldElements);

			/*
			 * Create expression activity node to print error messages
			 */
			ExpressionActivityNode n17PrintErrorMessage = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n17PrintErrorMessage.setName("print error message");

			n17PrintErrorMessage.setActivity(activity);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createPrintErrorMessageExpressionActivityNodeContent(n17PrintErrorMessage, rule,
					ruleClass, "ERROR in repairStructure.");

			/*
			 * Create MergeNode
			 */
			MergeNode n18MergeFlows = NodesFactory.eINSTANCE.createMergeNode();
			n18MergeFlows.setActivity(activity);

			/*
			 * Create expression activity node to add the node to the
			 * transformation queue
			 */
			ExpressionActivityNode n19AddModTagToQueue = NodesFactory.eINSTANCE.createExpressionActivityNode();
			n19AddModTagToQueue.setName("add modification tag to queue");

			n19AddModTagToQueue.setActivity(activity);

			// Create content
			OptimizedIncrRepairGenerationStrategy.this.createAddModificationTagToQueueExpressionActivityNodeContent(n19AddModTagToQueue,
					rule, ruleClass, /*ruleInformationMap,*/ true, true, false, direction);

			/*
			 * Create final node with return value true
			 */
			ActivityFinalNode trueFinalNode = NodesFactory.eINSTANCE.createActivityFinalNode();
			trueFinalNode.setName("stop true");
			trueFinalNode.setActivity(activity);

			LiteralDeclarationAction trueLiteralDeclarationAction = SDGeneratorHelper.createLiteralDeclarationAction(
					OptimizedIncrRepairGenerationStrategy.TRUE, null, OptimizedIncrRepairGenerationStrategy.TRUE,
					EcorePackage.eINSTANCE.getEBooleanObject());
			CallActionExpression trueLiteralExpression = SDGeneratorHelper.createCallActionExpression(
					OptimizedIncrRepairGenerationStrategy.TRUE, null, trueLiteralDeclarationAction);
			trueFinalNode.setReturnValue(trueLiteralExpression);

			/*
			 * Create final node with return value false
			 */
			ActivityFinalNode falseFinalNode = NodesFactory.eINSTANCE.createActivityFinalNode();
			falseFinalNode.setName("stop false");
			falseFinalNode.setActivity(activity);

			LiteralDeclarationAction falseLiteralDeclarationAction = SDGeneratorHelper.createLiteralDeclarationAction(
					OptimizedIncrRepairGenerationStrategy.FALSE, null, OptimizedIncrRepairGenerationStrategy.FALSE,
					EcorePackage.eINSTANCE.getEBooleanObject());
			CallActionExpression falseLiteralExpression = SDGeneratorHelper.createCallActionExpression(
					OptimizedIncrRepairGenerationStrategy.FALSE, null, falseLiteralDeclarationAction);
			falseFinalNode.setReturnValue(falseLiteralExpression);

			/*
			 * Create transitions
			 */

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(initialNode, n1MatchCreatedSourceElements.get(0),
							ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n1MatchCreatedSourceElements.get(0), n2MergeFlows,
							ActivityEdgeGuardEnumeration.SUCCESS, null));

			for (int i = 1; i < n1MatchCreatedSourceElements.size(); i++)
			{
				activity.getEdges().add(
						SDGeneratorHelper.createActivityEdge(n1MatchCreatedSourceElements.get(i), n2MergeFlows,
								ActivityEdgeGuardEnumeration.SUCCESS, null));

				activity.getEdges().add(
						SDGeneratorHelper.createActivityEdge(n1MatchCreatedSourceElements.get(i - 1), n1MatchCreatedSourceElements.get(i),
								ActivityEdgeGuardEnumeration.FAILURE, null));
			}

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n1MatchCreatedSourceElements.get(n1MatchCreatedSourceElements.size() - 1),
							falseFinalNode, ActivityEdgeGuardEnumeration.FAILURE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n2MergeFlows, n3MatchSourceAndParentElements, ActivityEdgeGuardEnumeration.NONE,
							null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n3MatchSourceAndParentElements, n3aEvaluateRuleVars,
							ActivityEdgeGuardEnumeration.SUCCESS, null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n3MatchSourceAndParentElements, falseFinalNode,
							ActivityEdgeGuardEnumeration.FAILURE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n3aEvaluateRuleVars, n4CheckIfCorrNodesNullOrExistingCorrNode,
							ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n4CheckIfCorrNodesNullOrExistingCorrNode, n5MatchTargetElements,
							ActivityEdgeGuardEnumeration.BOOLEAN, this.createCheckCorrNodesExpression(rule, direction)));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n4CheckIfCorrNodesNullOrExistingCorrNode, falseFinalNode,
							ActivityEdgeGuardEnumeration.ELSE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n5MatchTargetElements, n6CheckIfInputNodeIsAncestor,
							ActivityEdgeGuardEnumeration.SUCCESS, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n6CheckIfInputNodeIsAncestor, n7AddModTag, ActivityEdgeGuardEnumeration.BOOLEAN,
							this.createCheckIfInputNodeIsAncestorExpression(rule, ruleClass)));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n6CheckIfInputNodeIsAncestor, n8DeleteCorrespondenceLinks,
							ActivityEdgeGuardEnumeration.ELSE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n7AddModTag, trueFinalNode, ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n5MatchTargetElements, n11DeleteSourcesLinks,
							ActivityEdgeGuardEnumeration.FAILURE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n11DeleteSourcesLinks, n12DeleteTargetsLinks, ActivityEdgeGuardEnumeration.END,
							null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n12DeleteTargetsLinks, n13CreateNewCorrespondenceAndTargetElements,
							ActivityEdgeGuardEnumeration.END, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n8DeleteCorrespondenceLinks, n9DeleteTargetLinksToParents,
							ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n9DeleteTargetLinksToParents, n10CreateLinksToNewParents,
							ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n10CreateLinksToNewParents, n18MergeFlows, ActivityEdgeGuardEnumeration.SUCCESS,
							null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n10CreateLinksToNewParents, n17PrintErrorMessage,
							ActivityEdgeGuardEnumeration.FAILURE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n13CreateNewCorrespondenceAndTargetElements, n14RerouteNextLinks,
							ActivityEdgeGuardEnumeration.SUCCESS, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n13CreateNewCorrespondenceAndTargetElements, n17PrintErrorMessage,
							ActivityEdgeGuardEnumeration.FAILURE, null));

			activity.getEdges()
					.add(SDGeneratorHelper.createActivityEdge(n14RerouteNextLinks, n16DeleteOldElements, ActivityEdgeGuardEnumeration.END,
							null));

			// activity.getEdges().add(
			// SDGeneratorHelper.createActivityEdge(n15RerouteInputNodeLinks,
			// n16DeleteOldElements, ActivityEdgeGuardEnumeration.END,
			// null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n16DeleteOldElements, n18MergeFlows, ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n18MergeFlows, n19AddModTagToQueue, ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n19AddModTagToQueue, trueFinalNode, ActivityEdgeGuardEnumeration.NONE, null));

			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(n17PrintErrorMessage, falseFinalNode, ActivityEdgeGuardEnumeration.NONE, null));

		}

		private Expression createCheckIfInputNodeIsAncestorExpression(TGGRule rule, EClass ruleClass) throws RuleGenerationException
		{
			/*
			 * Create an expression that calls isPredecessor(parentNode,
			 * childNode) for each parent node
			 */
			CorrespondenceNode childNode = null;

			for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
			{
				if (ce.getModifier() == TGGModifierEnumeration.CREATE && ce instanceof CorrespondenceNode)
				{
					childNode = (CorrespondenceNode) ce;
					break;
				}
			}

			if (childNode == null)
			{
				throw new RuleGenerationException("No child correspondence node found in rule " + rule.getName() + ".");
			}

			OperationAction lastOperationAction = null;
			CallActionExpression operationExpression = null;

			for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
			{
				if (ce.getModifier() == TGGModifierEnumeration.NONE && ce instanceof CorrespondenceNode)
				{
					CorrespondenceNode parentNode = (CorrespondenceNode) ce;

					OperationAction o = CallActionsFactory.eINSTANCE.createOperationAction();
					o.setClassifier(EcorePackage.Literals.EBOOLEAN);
					o.setOperator(Operators.OR);

					if (lastOperationAction != null)
					{
						lastOperationAction.setOperand2(SDGeneratorHelper.createCallActionExpression(null, null, o));
					}
					else
					{
						operationExpression = SDGeneratorHelper.createCallActionExpression(null, null, o);
					}

					lastOperationAction = o;

					/*
					 * Create variable reference for the child node
					 */
					VariableReferenceAction childNodeReference = SDGeneratorHelper.createVariableReferenceAction(childNode.getName(), null,
							childNode.getName(), childNode.getClassifier());

					CallActionExpression childNodeExpression = SDGeneratorHelper.createCallActionExpression(childNode.getName(), null,
							childNodeReference);

					CallActionParameter childNodeParameter = SDGeneratorHelper.createCallActionParameter(childNode.getName(), null,
							MotePackage.Literals.TGG_NODE, childNodeExpression);

					/*
					 * Create variable reference for the parent node
					 */
					VariableReferenceAction parentNodeReference = SDGeneratorHelper.createVariableReferenceAction(parentNode.getName(),
							null, parentNode.getName(), parentNode.getClassifier());

					CallActionExpression parentNodeExpression = SDGeneratorHelper.createCallActionExpression(parentNode.getName(), null,
							parentNodeReference);

					CallActionParameter parentNodeParameter = SDGeneratorHelper.createCallActionParameter(parentNode.getName(), null,
							MotePackage.Literals.TGG_NODE, parentNodeExpression);

					/*
					 * Create variable reference for ruleSet
					 */
					VariableReferenceAction ruleSetReference = SDGeneratorHelper.createVariableReferenceAction(GenerationStrategy.RULE_SET,
							null, GenerationStrategy.RULE_SET, RulesPackage.Literals.TGG_RULE_SET);

					CallActionExpression ruleSetReferenceExpression = SDGeneratorHelper.createCallActionExpression(
							GenerationStrategy.RULE_SET, null, ruleSetReference);

					/*
					 * Create method call action to call EcoreUtil.isAncestor()
					 */
					MethodCallAction methodCallAction = SDGeneratorHelper.createMethodCallAction(null, null, null, null,
							"de.hpi.sam.mote.rules.impl.TGGRuleSetImpl", "isPredecessor", EcorePackage.eINSTANCE.getEBoolean());

					methodCallAction.setInstanceVariable(ruleSetReferenceExpression);

					methodCallAction.getParameters().add(parentNodeParameter);
					methodCallAction.getParameters().add(childNodeParameter);

					CallActionExpression methodCallExpression = SDGeneratorHelper.createCallActionExpression(null, null, methodCallAction);

					lastOperationAction.setOperand1(methodCallExpression);
				}
			}

			lastOperationAction.setOperand2(SDGeneratorHelper.createCallActionExpression(OptimizedIncrRepairGenerationStrategy.FALSE, null,
					SDGeneratorHelper.createLiteralDeclarationAction(OptimizedIncrRepairGenerationStrategy.FALSE, null,
							OptimizedIncrRepairGenerationStrategy.FALSE, EcorePackage.Literals.EBOOLEAN)));

			return operationExpression;
		}

		private void createSourcesLinksStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EClass ruleClass)
		{
			/*
			 * Create correspondence node
			 */
			StoryPatternObject corrNodeSpo = SDGeneratorHelper.createStoryPatternObject(OptimizedIncrRepairGenerationStrategy.CORR_NODE,
					null, MotePackage.eINSTANCE.getTGGNode(), StoryPatternModifierEnumeration.NONE, BindingTypeEnumeration.BOUND);

			storyActionNode.getStoryPatternObjects().add(corrNodeSpo);

			/*
			 * Create ruleSet object
			 */
			StoryPatternObject ruleSetSpo = SDGeneratorHelper.createStoryPatternObject(GenerationStrategy.RULE_SET, null,
					RulesPackage.eINSTANCE.getTGGRuleSet(), StoryPatternModifierEnumeration.NONE, BindingTypeEnumeration.BOUND);

			storyActionNode.getStoryPatternObjects().add(ruleSetSpo);

			/*
			 * Create model object
			 */
			StoryPatternObject modelObjectSpo = SDGeneratorHelper.createStoryPatternObject("x", null, EcorePackage.eINSTANCE.getEObject(),
					StoryPatternModifierEnumeration.NONE, BindingTypeEnumeration.UNBOUND);

			storyActionNode.getStoryPatternObjects().add(modelObjectSpo);

			/*
			 * Create sources link
			 */
			StoryPatternLink sourcesLink = SDGeneratorHelper.createStoryPatternLink(corrNodeSpo, modelObjectSpo,
					MotePackage.Literals.TGG_NODE__SOURCES, null, null, null, StoryPatternModifierEnumeration.NONE, null);

			storyActionNode.getStoryPatternLinks().add(sourcesLink);

			if (direction == TransformationDirection.FORWARD)
			{
				sourcesLink.setModifier(StoryPatternModifierEnumeration.DESTROY);
			}

			/*
			 * Create sourceModelElements link
			 */
			MapEntryStoryPatternLink sourceModelElementsLink = SDGeneratorHelper.createMapEntryStoryPatternLink(null, null,
					RulesPackage.eINSTANCE.getTGGRuleSet_SourceModelElements(), StoryPatternModifierEnumeration.DESTROY, ruleSetSpo,
					modelObjectSpo, corrNodeSpo);

			storyActionNode.getStoryPatternLinks().add(sourceModelElementsLink);

			/*
			 * Create uncoveredSourceModelElements link
			 */
			MapEntryStoryPatternLink uncoveredSourceModelElementsLink = SDGeneratorHelper.createMapEntryStoryPatternLink(null, null,
					RulesPackage.eINSTANCE.getTGGRuleSet_UncoveredSourceModelElements(), StoryPatternModifierEnumeration.CREATE,
					ruleSetSpo, modelObjectSpo, null);

			storyActionNode.getStoryPatternLinks().add(uncoveredSourceModelElementsLink);
		}

		private void createTargetsLinksStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EClass ruleClass)
		{
			/*
			 * Create correspondence node
			 */
			StoryPatternObject corrNodeSpo = SDGeneratorHelper.createStoryPatternObject(OptimizedIncrRepairGenerationStrategy.CORR_NODE,
					null, MotePackage.eINSTANCE.getTGGNode(), StoryPatternModifierEnumeration.NONE, BindingTypeEnumeration.BOUND);

			storyActionNode.getStoryPatternObjects().add(corrNodeSpo);

			/*
			 * Create ruleSet object
			 */
			StoryPatternObject ruleSetSpo = SDGeneratorHelper.createStoryPatternObject(GenerationStrategy.RULE_SET, null,
					RulesPackage.eINSTANCE.getTGGRuleSet(), StoryPatternModifierEnumeration.NONE, BindingTypeEnumeration.BOUND);

			storyActionNode.getStoryPatternObjects().add(ruleSetSpo);

			/*
			 * Create model object
			 */
			StoryPatternObject modelObjectSpo = SDGeneratorHelper.createStoryPatternObject("x", null, EcorePackage.eINSTANCE.getEObject(),
					StoryPatternModifierEnumeration.NONE, BindingTypeEnumeration.UNBOUND);

			storyActionNode.getStoryPatternObjects().add(modelObjectSpo);

			/*
			 * Create targets link
			 */
			StoryPatternLink targetsLink = SDGeneratorHelper.createStoryPatternLink(corrNodeSpo, modelObjectSpo,
					MotePackage.Literals.TGG_NODE__TARGETS, null, null, null, StoryPatternModifierEnumeration.NONE, null);

			storyActionNode.getStoryPatternLinks().add(targetsLink);

			if (direction == TransformationDirection.REVERSE)
			{
				targetsLink.setModifier(StoryPatternModifierEnumeration.DESTROY);
			}

			/*
			 * Create targetModelElements link
			 */
			MapEntryStoryPatternLink targetModelElementsLink = SDGeneratorHelper.createMapEntryStoryPatternLink(null, null,
					RulesPackage.eINSTANCE.getTGGRuleSet_TargetModelElements(), StoryPatternModifierEnumeration.DESTROY, ruleSetSpo,
					modelObjectSpo, corrNodeSpo);

			storyActionNode.getStoryPatternLinks().add(targetModelElementsLink);

			/*
			 * Create uncoveredTargetModelElements link
			 */
			MapEntryStoryPatternLink uncoveredTargetModelElementsLink = SDGeneratorHelper.createMapEntryStoryPatternLink(null, null,
					RulesPackage.eINSTANCE.getTGGRuleSet_UncoveredTargetModelElements(), StoryPatternModifierEnumeration.CREATE,
					ruleSetSpo, modelObjectSpo, null);

			storyActionNode.getStoryPatternLinks().add(uncoveredTargetModelElementsLink);
		}

		private void createDeleteOldElementsExpressionActivityNodeContent(TGGRule rule, TransformationDirection direction,
				EClass ruleClass, ExpressionActivityNode expressionActivityNode)
		{
			CallActionExpression expression = SDGeneratorHelper.createCallActionExpression("delete obsolete elements", null, null);

			expressionActivityNode.setExpression(expression);

			for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
			{
				if (ce.getModifier() == TGGModifierEnumeration.CREATE && ce instanceof CorrespondenceNode)
				{
					//CorrespondenceNode cn = (CorrespondenceNode) ce;

					/*
					 * Create method call action
					 */
					MethodCallAction methodCallAction = null;

					methodCallAction = SDGeneratorHelper.createMethodCallAction("invoke "
							+ OptimizedIncrRepairGenerationStrategy.FORWARD_DELETE_METHOD_NAME, null, null, null,
							"de.hpi.sam.mote.rules.TGGRuleSetImpl", "deleteElements", null);

					expression.getCallActions().add(methodCallAction);

					/*
					 * Create variable reference action for the instance object
					 * that references the ruleSet object
					 */
					VariableReferenceAction variableReferenceAction = SDGeneratorHelper.createVariableReferenceAction(
							GenerationStrategy.RULE_SET, null, GenerationStrategy.RULE_SET, RulesPackage.Literals.TGG_RULE_SET);

					CallActionExpression variableReferenceExpression = SDGeneratorHelper.createCallActionExpression(null, null,
							variableReferenceAction);

					methodCallAction.setInstanceVariable(variableReferenceExpression);

					/*
					 * Create parameter that references the created
					 * correspondence node
					 */
					CallActionParameter variableReferenceParameter = CallActionsFactory.eINSTANCE.createCallActionParameter();
					variableReferenceParameter.setParameterClassfier(MotePackage.eINSTANCE.getTGGNode());

					methodCallAction.getParameters().add(variableReferenceParameter);

					/*
					 * Create variable reference action that references the
					 * created correspondence node
					 */
					variableReferenceAction = SDGeneratorHelper.createVariableReferenceAction(
							OptimizedIncrRepairGenerationStrategy.CORR_NODE, null, OptimizedIncrRepairGenerationStrategy.CORR_NODE,
							MotePackage.Literals.TGG_NODE);

					variableReferenceExpression = SDGeneratorHelper.createCallActionExpression(null, null, variableReferenceAction);

					variableReferenceParameter.setParameterValueAction(variableReferenceExpression);

					/*
					 * Create parameter that references the appropriate
					 * TransformationDirection literal
					 */
					LiteralDeclarationAction directionLiteralAction = SDGeneratorHelper.createLiteralDeclarationAction(null, null,
							direction.getLiteral(), MotePackage.Literals.TRANSFORMATION_DIRECTION);

					CallActionExpression directionLiteralExpression = SDGeneratorHelper.createCallActionExpression(null, null,
							directionLiteralAction);

					CallActionParameter directionLiteralParameter = SDGeneratorHelper.createCallActionParameter(null, null,
							MotePackage.Literals.TRANSFORMATION_DIRECTION, directionLiteralExpression);

					methodCallAction.getParameters().add(directionLiteralParameter);
				}
			}
		}

		private List<StoryActionNode> createMatchCreatedSourceElementsStoryActionNodes(TGGRule rule, TransformationDirection direction,
				EClass ruleClass, Activity activity) throws RuleGenerationException
		{
			CorrespondenceNode cn = null;

			for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
			{
				if (ce.getModifier() == TGGModifierEnumeration.CREATE && ce instanceof CorrespondenceNode)
				{
					cn = (CorrespondenceNode) ce;
					break;
				}
			}

			List<StoryActionNode> sans = new LinkedList<StoryActionNode>();

			if (direction == TransformationDirection.FORWARD || direction == TransformationDirection.MAPPING)
			{
				for (ModelElement me : rule.getSourceDomain().getModelElements())
				{
					if (me.getModifier() == TGGModifierEnumeration.CREATE && me instanceof ModelObject)
					{
						StoryActionNode san = NodesFactory.eINSTANCE.createStoryActionNode();
						san.setName("match source element " + me.getName());

						san.setActivity(activity);

						// Create content
						this.createMatchCreatedSourceElementStoryActionNodeContent(san, (ModelObject) me, cn, rule,
								TransformationDirection.FORWARD, ruleClass);

						sans.add(san);
					}
				}
			}

			if (direction == TransformationDirection.REVERSE)
			{
				for (ModelElement me : rule.getTargetDomain().getModelElements())
				{
					if (me.getModifier() == TGGModifierEnumeration.CREATE && me instanceof ModelObject)
					{
						StoryActionNode san = NodesFactory.eINSTANCE.createStoryActionNode();
						san.setName("match source element " + me.getName());

						san.setActivity(activity);

						// Create content
						this.createMatchCreatedSourceElementStoryActionNodeContent(san, (ModelObject) me, cn, rule,
								TransformationDirection.REVERSE, ruleClass);

						sans.add(san);
					}
				}

			}

			return sans;
		}

		private void createMatchCreatedSourceElementStoryActionNodeContent(StoryActionNode storyActionNode, ModelObject modelObject,
				CorrespondenceNode correspondenceNode, TGGRule rule, TransformationDirection direction, EClass ruleClass)
				throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.createChildCorrespondenceNodes = true;
			options.childCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			if (direction == TransformationDirection.FORWARD)
			{
				options.createLeftCreatedNodes = true;
				options.createLeftCreatedLinks = true;

				options.createLeftConstraints = true;
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.createRightCreatedNodes = true;
				options.createRightCreatedLinks = true;

				options.createRightConstraints = true;
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			StoryPatternObject createdCorrNodeSpo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap
					.get(result.childCorrespondenceNodes.get(0));

			createdCorrNodeSpo.setName(OptimizedIncrRepairGenerationStrategy.CORR_NODE);
			createdCorrNodeSpo.setClassifier(MotePackage.eINSTANCE.getTGGNode());

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);

			StoryPatternObject corrNodeSpo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap
					.get(result.childCorrespondenceNodes.get(0));

			/*
			 * Create sources link
			 */
			if (direction == TransformationDirection.FORWARD)
			{
				StoryPatternLink spl = SDGeneratorHelper.createStoryPatternLink(corrNodeSpo,
						(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(modelObject),
						MotePackage.Literals.TGG_NODE__SOURCES, null, null, null, StoryPatternModifierEnumeration.NONE, null);

				storyActionNode.getStoryPatternLinks().add(spl);
			}
			/*
			 * Create targets link
			 */
			else if (direction == TransformationDirection.REVERSE)
			{
				StoryPatternLink spl = SDGeneratorHelper.createStoryPatternLink(corrNodeSpo,
						(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(modelObject),
						MotePackage.Literals.TGG_NODE__TARGETS, null, null, null, StoryPatternModifierEnumeration.NONE, null);

				storyActionNode.getStoryPatternLinks().add(spl);
			}
		}

		private void createMatchSourceAndParentElementsStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EClass ruleClass) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.direction = direction;

			options.createThisObject = true;
			options.thisEClass = ruleClass;

			options.createThisToRuleSet_ruleSet_Link = true;

			options.createRuleSetObject = true;
			options.createRuleSetToParentNodes_sourceModelElementsLinks = true;
			options.createRuleSetToParentNodes_targetModelElementsLinks = true;

			options.createParentCorrespondenceNodes = true;
			options.createParentCorrespondenceNodesSourcesLinks = true;
			options.createParentCorrespondenceNodesTargetsLinks = true;

			options.createLeftParentNodes = true;
			options.createLeftParentLinks = true;

			options.createRightParentNodes = true;
			options.createRightParentLinks = true;

			options.createLeftConstraints = true;
			options.createRightConstraints = true;

			if (direction == TransformationDirection.FORWARD)
			{
				options.createLeftCreatedNodes = true;
				options.createLeftCreatedLinks = true;

				options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;
			}
			else if (direction == TransformationDirection.MAPPING)
			{
				options.createLeftCreatedNodes = true;
				options.createLeftCreatedLinks = true;

				options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;

				options.createRightCreatedNodes = true;
				options.createRightCreatedLinks = true;

				options.createLeftAttributeChecks = true;
				options.createRightAttributeChecks = true;
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.createRightCreatedNodes = true;
				options.createRightCreatedLinks = true;

				options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

		private Expression createCheckCorrNodesExpression(TGGRule rule, TransformationDirection direction)
		{
			StringBuffer buffer = new StringBuffer();

			/*
			 * Create constraints for each created source model element to check
			 * if it is connected to the created correspondence node or to no
			 * correspondence node at all.
			 */
			if (direction == TransformationDirection.FORWARD || direction == TransformationDirection.MAPPING)
			{
				for (ModelElement me : rule.getSourceDomain().getModelElements())
				{
					if (me.getModifier() == TGGModifierEnumeration.CREATE && me instanceof ModelObject)
					{
						/*
						 * Create a string expression
						 */
						buffer.append("(ruleSet.sourceModelElements->select(e|e.key = " + me.getName() + ")->isEmpty() or"
								+ OptimizedIncrRepairGenerationStrategy.NL + "ruleSet.sourceModelElements->select(e|e.key = " + me.getName()
								+ ")->asOrderedSet()->first().value = " + OptimizedIncrRepairGenerationStrategy.CORR_NODE + ") and"
								+ OptimizedIncrRepairGenerationStrategy.NL);
					}
				}
			}

			if (direction == TransformationDirection.REVERSE || direction == TransformationDirection.MAPPING)
			{
				for (ModelElement me : rule.getTargetDomain().getModelElements())
				{
					if (me.getModifier() == TGGModifierEnumeration.CREATE && me instanceof ModelObject)
					{
						/*
						 * Create a string expression
						 */
						buffer.append("(ruleSet.targetModelElements->select(e|e.key = " + me.getName() + ")->isEmpty() or"
								+ OptimizedIncrRepairGenerationStrategy.NL + "ruleSet.targetModelElements->select(e|e.key = " + me.getName()
								+ ")->asOrderedSet()->first().value = " + OptimizedIncrRepairGenerationStrategy.CORR_NODE + ") and"
								+ OptimizedIncrRepairGenerationStrategy.NL);
					}
				}
			}

			buffer.delete(buffer.length() - 5, buffer.length());

			return SDGeneratorHelper.createStringExpression(SDGeneratorHelper.OCL_EXPRESSION_LANGUAGE, buffer.toString());
		}

		private void createMatchTargetElementsStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EClass ruleClass) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.direction = direction;

			options.createThisObject = true;
			options.thisEClass = ruleClass;

			options.createThisToChildCorrespondenceNodes_createdCorrNodes_Link = true;

			options.createChildCorrespondenceNodes = true;
			options.childCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			if (direction == TransformationDirection.FORWARD)
			{
				options.createRightCreatedNodes = true;
				options.createRightCreatedLinks = true;

				options.createRightConstraints = true;

				options.createChildCorrespondenceNodesTargetsLinks = true;
			}
			else if (direction == TransformationDirection.MAPPING)
			{

			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.createLeftCreatedNodes = true;
				options.createLeftCreatedLinks = true;

				options.createLeftConstraints = true;

				options.createChildCorrespondenceNodesSourcesLinks = true;
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);

			/*
			 * Create direct assignment for the created correspondence node
			 */
			StoryPatternObject createdCorrNodeSpo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap
					.get(result.childCorrespondenceNodes.get(0));

			VariableReferenceAction corrNodeReferenceAction = SDGeneratorHelper.createVariableReferenceAction(
					OptimizedIncrRepairGenerationStrategy.CORR_NODE, null, OptimizedIncrRepairGenerationStrategy.CORR_NODE,
					MotePackage.eINSTANCE.getTGGNode());
			createdCorrNodeSpo.setDirectAssignmentExpression(SDGeneratorHelper.createCallActionExpression(
					OptimizedIncrRepairGenerationStrategy.CORR_NODE, null, corrNodeReferenceAction));

			/*
			 * Create constraint to check the size of the sources/targets
			 * association
			 */
			int count = 0;

			if (direction == TransformationDirection.FORWARD)
			{
				for (ModelElement me : rule.getTargetDomain().getModelElements())
				{
					if (me.getModifier() == TGGModifierEnumeration.CREATE && me instanceof ModelObject)
					{
						count++;
					}
				}

				String expressionString = result.childCorrespondenceNodes.get(0).getName() + ".targets->size() = " + count;

				StringExpression stringExpression = SDGeneratorHelper.createStringExpression(SDGeneratorHelper.OCL_EXPRESSION_LANGUAGE,
						expressionString);

				storyActionNode.getConstraints().add(stringExpression);
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				for (ModelElement me : rule.getSourceDomain().getModelElements())
				{
					if (me.getModifier() == TGGModifierEnumeration.CREATE && me instanceof ModelObject)
					{
						count++;
					}
				}

				String expressionString = result.childCorrespondenceNodes.get(0).getName() + ".sources->size() = " + count;

				StringExpression stringExpression = SDGeneratorHelper.createStringExpression(SDGeneratorHelper.OCL_EXPRESSION_LANGUAGE,
						expressionString);

				storyActionNode.getConstraints().add(stringExpression);
			}
		}

		private void createDeleteCorrespondenceLinksExpressionActivityNodeContent(ExpressionActivityNode expressionActivityNode,
				TGGRule rule)
		{
			/*
			 * Get name of the created correspondence node of the rule
			 */
			String createdCorrNodeName = "";
			for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
			{
				if (ce.getModifier() == TGGModifierEnumeration.CREATE && ce instanceof CorrespondenceNode)
				{
					createdCorrNodeName = ce.getName();
					break;
				}
			}

			/*
			 * Create reference action for the correspondence node
			 */
			VariableReferenceAction corrNodeReferenceAction = SDGeneratorHelper.createVariableReferenceAction(createdCorrNodeName, null,
					createdCorrNodeName, MotePackage.eINSTANCE.getTGGNode());

			CallActionExpression corrNodeReferenceExpression = SDGeneratorHelper.createCallActionExpression(null, null,
					corrNodeReferenceAction);

			/*
			 * Create method call action to call getPrev() on the correspondence
			 * node
			 */
			MethodCallAction getPrevMethodCallAction = SDGeneratorHelper.createMethodCallAction("getPrevious()", null,
					corrNodeReferenceExpression, null, MotePackage.eNS_PREFIX + "." + MotePackage.eINSTANCE.getTGGNode().getName(),
					"getPrevious", EcorePackage.eINSTANCE.getEEList());

			CallActionExpression getPrevMethodCallExpression = SDGeneratorHelper.createCallActionExpression(null, null,
					getPrevMethodCallAction);

			/*
			 * Create method call action to call clear() on the list returned by
			 * getPrev().
			 */
			MethodCallAction clearMethodCallAction = SDGeneratorHelper.createMethodCallAction("clear()", null, getPrevMethodCallExpression,
					null, EcorePackage.eINSTANCE.getEEList().getInstanceClassName(), "clear", null);

			CallActionExpression clearMethodCallExpression = SDGeneratorHelper
					.createCallActionExpression(null, null, clearMethodCallAction);

			expressionActivityNode.setExpression(clearMethodCallExpression);
		}

		private void createDeleteTargetLinksToParentsStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EClass ruleClass) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.direction = direction;

			if (direction == TransformationDirection.FORWARD)
			{
				options.createRightCreatedNodes = true;
				options.createRightCreatedLinks = true;

				options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;

				options.createRightParentNodes = true;
				options.createRightParentLinks = true;
			}
			else if (direction == TransformationDirection.MAPPING)
			{
				options.createThisObject = true;
				options.thisEClass = ruleClass;
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.createLeftCreatedNodes = true;
				options.createLeftCreatedLinks = true;

				options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;

				options.createLeftParentNodes = true;
				options.createLeftParentLinks = true;
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			/*
			 * Append "old" to names of parent elements
			 */
			if (direction == TransformationDirection.FORWARD)
			{
				for (ModelObject mo : result.rightParentNodes)
				{
					StoryPatternObject spo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(mo);

					spo.setName(spo.getName() + OptimizedIncrRepairGenerationStrategy.OLD);
				}
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				for (ModelObject mo : result.leftParentNodes)
				{
					StoryPatternObject spo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(mo);

					spo.setName(spo.getName() + OptimizedIncrRepairGenerationStrategy.OLD);
				}
			}

			/*
			 * Change modifier of links between parent and child elements to
			 * destroy
			 */
			if (direction == TransformationDirection.FORWARD)
			{
				for (ModelLink ml : result.rightCreatedLinks)
				{
					/*
					 * Check if the link connects a parent and a created node.
					 */
					if (result.rightParentNodes.contains(ml.getSource()) && result.rightCreatedNodes.contains(ml.getTarget())
							|| result.rightParentNodes.contains(ml.getTarget()) && result.rightCreatedNodes.contains(ml.getSource()))
					{
						StoryPatternLink spl = (StoryPatternLink) result.ruleElementsToStoryPatternElementsMap.get(ml);

						spl.setModifier(StoryPatternModifierEnumeration.DESTROY);
					}
				}
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				for (ModelLink ml : result.leftCreatedLinks)
				{
					/*
					 * Check if the link connects a parent and a created node.
					 */
					if (result.leftParentNodes.contains(ml.getSource()) && result.leftCreatedNodes.contains(ml.getTarget())
							|| result.leftParentNodes.contains(ml.getTarget()) && result.leftCreatedNodes.contains(ml.getSource()))
					{
						StoryPatternLink spl = (StoryPatternLink) result.ruleElementsToStoryPatternElementsMap.get(ml);

						spl.setModifier(StoryPatternModifierEnumeration.DESTROY);
					}
				}

			}

			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
		}

		private void createLinksToNewParentsStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EClass ruleClass) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.direction = direction;

			options.createLeftCreatedNodes = true;
			options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;
			options.createLeftCreatedLinks = true;

			options.createRightCreatedNodes = true;
			options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;
			options.createRightCreatedLinks = true;

			options.createChildCorrespondenceNodes = true;
			options.childCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;
			options.createChildCorrespondenceNodesToInputCorrespondenceNode_inputNode_Link = true;

			options.createParentCorrespondenceNodesToChildCorrespondenceNodes_next_Links = true;
			options.createChildCorrespondenceNodesSourcesLinks = true;
			options.createChildCorrespondenceNodesTargetsLinks = true;

			options.childCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftParentNodes = true;
			options.leftParentNodesBinding = BindingTypeEnumeration.BOUND;
			options.createLeftParentLinks = true;

			options.createRightParentNodes = true;
			options.rightParentNodesBinding = BindingTypeEnumeration.BOUND;
			options.createRightParentLinks = true;

			options.createParentCorrespondenceNodes = true;
			options.parentCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;
			options.createParentCorrespondenceNodesSourcesLinks = true;
			options.createParentCorrespondenceNodesTargetsLinks = true;

			if (direction == TransformationDirection.FORWARD)
			{
				options.createRightAttributeAssignments = true;
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.createLeftAttributeAssignments = true;
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			/*
			 * Set the modifier of all links in the target model between parent
			 * and created nodes to CREATE
			 */
			if (direction == TransformationDirection.FORWARD)
			{
				for (ModelLink ml : result.rightCreatedLinks)
				{
					/*
					 * Check if the link connects a parent and a created node.
					 */
					if (result.rightParentNodes.contains(ml.getSource()) && result.rightCreatedNodes.contains(ml.getTarget())
							|| result.rightParentNodes.contains(ml.getTarget()) && result.rightCreatedNodes.contains(ml.getSource()))
					{
						StoryPatternLink spl = (StoryPatternLink) result.ruleElementsToStoryPatternElementsMap.get(ml);

						spl.setModifier(StoryPatternModifierEnumeration.CREATE);
					}
				}
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				for (ModelLink ml : result.leftCreatedLinks)
				{
					/*
					 * Check if the link connects a parent and a created node.
					 */
					if (result.leftParentNodes.contains(ml.getSource()) && result.leftCreatedNodes.contains(ml.getTarget())
							|| result.leftParentNodes.contains(ml.getTarget()) && result.leftCreatedNodes.contains(ml.getSource()))
					{
						StoryPatternLink spl = (StoryPatternLink) result.ruleElementsToStoryPatternElementsMap.get(ml);

						spl.setModifier(StoryPatternModifierEnumeration.CREATE);
					}
				}
			}

			/*
			 * Change modifier of next links to CREATE
			 */
			for (AbstractStoryPatternLink l : result.storyPatternLinks)
			{
				if (l instanceof StoryPatternLink)
				{
					StoryPatternLink spl = (StoryPatternLink) l;

					if (spl.getEStructuralFeature() == MotePackage.Literals.TGG_NODE__NEXT)
					{
						spl.setModifier(StoryPatternModifierEnumeration.CREATE);
					}
				}
			}

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

		private void createCreateNewCorrespondenceAndTargetElementsStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EClass ruleClass) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();
			
			options.direction = direction;

			options.createThisObject = true;
			options.thisEClass = ruleClass;

			options.createThisToRuleSet_ruleSet_Link = true;

			options.createRuleSetToChildCorrespondenceNodes_checkedCorrNodes_Link = true;
			options.checkedCorrNodes_LinkModifier = StoryPatternModifierEnumeration.CREATE;

			options.createRuleSetObject = true;

			options.createRuleSetToChildCorrespondenceNodes_checkedCorrNodes_Link = true;
			options.checkedCorrNodes_LinkModifier = StoryPatternModifierEnumeration.CREATE;

			options.createLeftParentNodes = true;
			options.createLeftParentLinks = true;

			options.leftParentNodesBinding = BindingTypeEnumeration.BOUND;

			options.createRightParentNodes = true;
			options.createRightParentLinks = true;

			options.rightParentNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftCreatedNodes = true;
			options.createLeftCreatedLinks = true;

			options.createRightCreatedNodes = true;
			options.createRightCreatedLinks = true;

			options.createParentCorrespondenceNodes = true;
			options.parentCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createChildCorrespondenceNodes = true;

			options.createParentCorrespondenceNodesSourcesLinks = true;
			options.createParentCorrespondenceNodesTargetsLinks = true;

			options.createChildCorrespondenceNodesSourcesLinks = true;
			options.createChildCorrespondenceNodesTargetsLinks = true;

			options.createParentCorrespondenceNodesToChildCorrespondenceNodes_next_Links = true;
			options.createChildCorrespondenceNodesToInputCorrespondenceNode_inputNode_Link = true;

			options.createThisToChildCorrespondenceNodes_createdCorrNodes_Link = true;

			options.createRuleSetToChildCorrespondenceNodes_correspondenceNodes_Link = true;
			options.createChildCorrespondenceNodesToRuleSet_ruleSet_Links = true;

			options.createRuleSetToCreatedNodes_sourceModelElementsLinks = true;
			options.createRuleSetToCreatedNodes_targetModelElementsLinks = true;

			options.childCorrespondenceNodesModifier = StoryPatternModifierEnumeration.CREATE;

			options.ruleSetToCreatedNodes_sourceModelElements_Modifier = StoryPatternModifierEnumeration.CREATE;
			options.ruleSetToCreatedNodes_targetModelElements_Modifier = StoryPatternModifierEnumeration.CREATE;

			if (direction == TransformationDirection.FORWARD)
			{
				options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;
				options.rightCreatedNodesModifier = StoryPatternModifierEnumeration.CREATE;

				options.createLeftConstraints = true;
				options.createRightAttributeAssignments = true;

				// DB added
				options.createRightAttAssignForPropValueConst = true;

				options.createRuleSetToCreatedNodes_uncoveredSourceElementsLinks = true;
				options.ruleSetToCreatedNodes_uncoveredSourceElements_Modifier = StoryPatternModifierEnumeration.DESTROY;
			}
			else if (direction == TransformationDirection.MAPPING)
			{
				options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;
				options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;

				options.createRuleSetToCreatedNodes_uncoveredSourceElementsLinks = true;
				options.ruleSetToCreatedNodes_uncoveredSourceElements_Modifier = StoryPatternModifierEnumeration.DESTROY;

				options.createRuleSetToCreatedNodes_uncoveredTargetElementsLinks = true;
				options.ruleSetToCreatedNodes_uncoveredTargetElements_Modifier = StoryPatternModifierEnumeration.DESTROY;
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;
				options.leftCreatedNodesModifier = StoryPatternModifierEnumeration.CREATE;

				options.createLeftAttributeAssignments = true;
				options.createRightConstraints = true;
				
				// DB added
				options.createLeftAttAssignForPropValueConst = true;

				options.createRuleSetToCreatedNodes_uncoveredTargetElementsLinks = true;
				options.ruleSetToCreatedNodes_uncoveredTargetElements_Modifier = StoryPatternModifierEnumeration.DESTROY;
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			/*
			 * Create objects for old child correspondence node and destroy old
			 * source-/targetModelElements and sources/targets links
			 */

			// // Create story pattern object for child correspondence node
			// StoryPatternObject spo =
			// SDGeneratorHelper.createStoryPatternObject(CORR_NODE, null,
			// MotePackage.eINSTANCE.getTGGNode(),
			// StoryPatternModifierEnumeration.NONE,
			// BindingTypeEnumeration.BOUND);
			//
			// result.storyPatternObjects.add(spo);
			//
			// if (direction == TransformationDirection.FORWARD || direction ==
			// TransformationDirection.MAPPING)
			// {
			// /*
			// * Create sourceModelElements links
			// */
			// for (ModelObject mo : result.leftCreatedNodes)
			// {
			// MapEntryStoryPatternLink mapEntrySpl =
			// SDGeneratorHelper.createMapEntryStoryPatternLink(null, null,
			// RulesPackage.eINSTANCE.getTGGRuleSet_SourceModelElements(),
			// StoryPatternModifierEnumeration.DESTROY,
			// result.ruleSetObject, (StoryPatternObject)
			// result.ruleElementsToStoryPatternElementsMap.get(mo), spo);
			//
			// storyActionNode.getStoryPatternLinks().add(mapEntrySpl);
			// }
			// }
			//
			// if (direction == TransformationDirection.REVERSE || direction ==
			// TransformationDirection.MAPPING)
			// {
			// /*
			// * Create targetModelElements links
			// */
			// for (ModelObject mo : result.rightCreatedNodes)
			// {
			// MapEntryStoryPatternLink mapEntrySpl =
			// SDGeneratorHelper.createMapEntryStoryPatternLink(null, null,
			// RulesPackage.eINSTANCE.getTGGRuleSet_TargetModelElements(),
			// StoryPatternModifierEnumeration.DESTROY,
			// result.ruleSetObject, (StoryPatternObject)
			// result.ruleElementsToStoryPatternElementsMap.get(mo), spo);
			//
			// storyActionNode.getStoryPatternLinks().add(mapEntrySpl);
			// }
			// }
			//
			// /*
			// * Create sources links
			// */
			// if (direction == TransformationDirection.FORWARD || direction ==
			// TransformationDirection.MAPPING)
			// {
			// for (ModelObject mo : result.leftCreatedNodes)
			// {
			// StoryPatternLink link =
			// SDGeneratorHelper.createStoryPatternLink(spo,
			// (AbstractStoryPatternObject)
			// result.ruleElementsToStoryPatternElementsMap.get(mo),
			// MotePackage.eINSTANCE
			// .getTGGNode_Sources(), StoryPatternModifierEnumeration.DESTROY,
			// null);
			//
			// storyActionNode.getStoryPatternLinks().add(link);
			// }
			// }
			//
			// /*
			// * Create targets links
			// */
			// if (direction == TransformationDirection.REVERSE || direction ==
			// TransformationDirection.MAPPING)
			// {
			// for (ModelObject mo : result.rightCreatedNodes)
			// {
			// StoryPatternLink link =
			// SDGeneratorHelper.createStoryPatternLink(spo,
			// (AbstractStoryPatternObject)
			// result.ruleElementsToStoryPatternElementsMap.get(mo),
			// MotePackage.eINSTANCE
			// .getTGGNode_Targets(), StoryPatternModifierEnumeration.DESTROY,
			// null);
			//
			// storyActionNode.getStoryPatternLinks().add(link);
			// }
			// }

			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
			
			if ( direction == TransformationDirection.FORWARD || direction == TransformationDirection.REVERSE ) {
				final boolean fromSource = TransformationDirection.REVERSE == direction;
			
				for ( final AbstractStoryPatternObject spo : result.storyPatternObjects ) {
					if ( needsModelObjectCall( spo, rule, fromSource ) ) {
					//if ( spo.getModifier() == StoryPatternModifierEnumeration.CREATE ) {
					//	spo.setExistingCorrNode( CORR_NODE );
					//}
					
						// DB: Create a call to TGGRuleSet to reuse the existing objects if it exists
						final Expression expr = createCreateModelObjectExpression( spo, rule, fromSource );
						
						if ( expr != null ) {
							spo.setCreateModelObjectExpression( expr );
						}
					}
				}
			}
			
			storyActionNode.getStoryPatternObjects().addAll( result.storyPatternObjects );
		}
		
		private Expression createCreateModelObjectExpression(	final AbstractStoryPatternObject p_spo,
																final TGGRule p_rule,
																final boolean pb_fromSource ) {
			// TODO: We need to find a better way to obtain the classifier, and actually to identify the 
			String spoClassVarName = null;
			final EClassifier spoClass = p_spo.getClassifier();

			if ( spoClass instanceof EClass && ( (EClass) spoClass ).isAbstract() ) {
				// Search for a rule variable of EClass type indicating the class to be instantiated.
				for ( final RuleVariable var : p_rule.getRuleVariables() ) {
					if ( var.getClassifier() == EcorePackage.Literals.ECLASS ) {
						spoClassVarName = var.getName();
						
						break;
					}
				}
			}

			if ( spoClassVarName != null ) {
				final Expression ruleSetExpression = SDGeneratorHelper.createOCLStringExpression( "ruleSet" );
				final MethodCallAction callAction = SDGeneratorHelper.createMethodCallAction( 	"Call existingModelObject", 
																								null, 
																								ruleSetExpression, 
																								existingModelObjectOper, 
																								null, 
																								null, 
																								EcorePackage.Literals.EOBJECT );
				final Expression corrNodeExpression = SDGeneratorHelper.createOCLStringExpression( CORR_NODE );
				final CallActionParameter corrNodeParam = SDGeneratorHelper.createCallActionParameter(	CORR_NODE, 
																										null, 
																										MotePackage.Literals.TGG_NODE, 
																										corrNodeExpression );
				callAction.getParameters().add( corrNodeParam );

				final Expression storyPatternExpression = SDGeneratorHelper.createOCLStringExpression( spoClassVarName );
				final CallActionParameter storyPatternParam = SDGeneratorHelper.createCallActionParameter(	spoClassVarName, 
																											null, 
																											EcorePackage.Literals.ECLASSIFIER, 
																											storyPatternExpression );
				callAction.getParameters().add( storyPatternParam );

				final Expression sourceExpression = SDGeneratorHelper.createOCLStringExpression( Boolean.toString( pb_fromSource ) );
				final CallActionParameter sourceParam = SDGeneratorHelper.createCallActionParameter(	"source", 
																										null, 
																										EcorePackage.Literals.EBOOLEAN, 
																										sourceExpression );
				callAction.getParameters().add( sourceParam );
				
				return SDGeneratorHelper.createCallActionExpression( "Existing Model Object", null, callAction );
			}
			
			return null;
		}
		
		private boolean needsModelObjectCall( 	final AbstractStoryPatternObject p_spo,
												final TGGRule p_rule,
												final boolean pb_fromSource ) {
			if ( p_spo.getModifier() == StoryPatternModifierEnumeration.CREATE ) {
				final String elemName = p_spo.getName();
				final Collection<ModelElement> searchedElem = pb_fromSource ?  p_rule.getSourceDomain().getModelElements() :  p_rule.getTargetDomain().getModelElements();
				
				for ( final ModelElement modelElem : searchedElem ) {
					if ( elemName.equals( modelElem.getName() ) ) {
						return true;
					}
				}
			}
			
			return false;
		}

		private void createRerouteNextLinksStoryActionNodeContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.direction = direction;

			options.createChildCorrespondenceNodes = true;

			options.childCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			StoryPatternObject spoNewCorrNode = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap
					.get(result.childCorrespondenceNodes.get(0));

			/*
			 * Create story pattern object for old correspondence node. THIS
			 * ASSUMES, THAT THERE IS ONLY ONE CHILD CORRESPONDENCE NODE
			 */
			// Create story pattern object for child correspondence node
			StoryPatternObject spoOldCorrNode = SDGeneratorHelper.createStoryPatternObject(OptimizedIncrRepairGenerationStrategy.CORR_NODE,
					null, MotePackage.eINSTANCE.getTGGNode(), StoryPatternModifierEnumeration.NONE, BindingTypeEnumeration.BOUND);

			result.storyPatternObjects.add(spoOldCorrNode);

			/*
			 * Create story pattern object for succeeding correspondence node
			 */
			StoryPatternObject spoNextNode = SDGeneratorHelper.createStoryPatternObject("nextNode", null,
					MotePackage.eINSTANCE.getTGGNode(), StoryPatternModifierEnumeration.NONE, BindingTypeEnumeration.UNBOUND);

			result.storyPatternObjects.add(spoNextNode);

			/*
			 * Create next links
			 */
			StoryPatternLink oldNextLink = SDGeneratorHelper.createStoryPatternLink(spoOldCorrNode, spoNextNode,
					MotePackage.eINSTANCE.getTGGNode_Next(), null, null, null, StoryPatternModifierEnumeration.DESTROY, spoOldCorrNode.getName()
							+ "->next to " + spoNextNode.getName());

			result.storyPatternLinks.add(oldNextLink);

			StoryPatternLink newNextLink = SDGeneratorHelper.createStoryPatternLink(spoNewCorrNode, spoNextNode,
					MotePackage.eINSTANCE.getTGGNode_Next(), null, null, null, StoryPatternModifierEnumeration.CREATE, spoNewCorrNode.getName()
							+ "->next to " + spoNextNode.getName());

			result.storyPatternLinks.add(newNextLink);

			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
		}
	}

	protected class RuntimeCheckActivityGenerator
	{
		protected void createRuntimeCheckActivity(TGGRule rule, TransformationDirection direction, EClass ruleClass, EOperation operation,
				Activity activity/*, Map<TGGRule, RuleInformationContainer> ruleInformationMap*/) throws RuleGenerationException
		{
			/*
			 * Set activity's attributes
			 */
			activity.setName(rule.getName() + "." + operation.getName());
			activity.setDescription(rule.getDescription());

			activity.setSpecification(operation);

			/*
			 * Create InitialNode
			 */
			InitialNode initialNode = NodesFactory.eINSTANCE.createInitialNode();
			initialNode.setName("start");

			activity.getNodes().add(initialNode);

			/*
			 * Create story action nodes that look for the LHS of the rule and
			 * check if the elements were transformed correctly.First, find out
			 * how many parent correspondence nodes are in the rule. Then,
			 * create the story action nodes for each of them.
			 */
			int count = 0;

			for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
			{
				if (ce.getModifier() == TGGModifierEnumeration.NONE && ce instanceof CorrespondenceNode)
				{
					count++;
				}
			}

			ActivityNode lastActivityNode = initialNode;

			for (int i = 0; i < count; i++)
			{
				ActivityNode n = this.createStoryActionNodes(activity, rule, direction, ruleClass, i/*, ruleInformationMap*/);

				if (i == 0)
				{
					activity.getEdges().add(
							SDGeneratorHelper.createActivityEdge(lastActivityNode, n, ActivityEdgeGuardEnumeration.NONE, null));
				}
				else
				{
					activity.getEdges().add(
							SDGeneratorHelper.createActivityEdge(lastActivityNode, n, ActivityEdgeGuardEnumeration.END, null));
				}

				lastActivityNode = n;
			}

			/*
			 * Create true final node
			 */
			ActivityFinalNode trueFinalNode = NodesFactory.eINSTANCE.createActivityFinalNode();
			trueFinalNode.setName("true");

			activity.getNodes().add(trueFinalNode);

			/*
			 * Create true literal declaration action
			 */
			LiteralDeclarationAction trueLiteral = SDGeneratorHelper.createLiteralDeclarationAction("true", null, "true",
					EcorePackage.Literals.EBOOLEAN_OBJECT);
			CallActionExpression trueCAE = SDGeneratorHelper.createCallActionExpression("true", null, trueLiteral);
			trueFinalNode.setReturnValue(trueCAE);

			/*
			 * Create activity edges
			 */
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(lastActivityNode, trueFinalNode, ActivityEdgeGuardEnumeration.END, null));
		}

		private ActivityNode createStoryActionNodes(Activity activity, TGGRule rule, TransformationDirection direction, EClass ruleClass,
				int parentCorrNodeNo/*, Map<TGGRule, RuleInformationContainer> ruleInformationMap*/) throws RuleGenerationException
		{
			/*
			 * Create node to find LHS of the rule starting from parent node
			 * with index parentCorrNodeNo.
			 */
			StoryActionNode findLHSSAN = NodesFactory.eINSTANCE.createStoryActionNode();
			findLHSSAN.setName("find LHS of rule (" + parentCorrNodeNo + ")");
			findLHSSAN.setForEach(true);

			activity.getNodes().add(findLHSSAN);

			/*
			 * Create content
			 */
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.direction = direction;

			options.createThisObject = true;
			options.thisEClass = ruleClass;

			options.createRuleSetObject = true;
			options.createThisToRuleSet_ruleSet_Link = true;

			options.createRuleSetToParentNodes_sourceModelElementsLinks = true;
			options.createRuleSetToParentNodes_targetModelElementsLinks = true;

			options.createParentCorrespondenceNodes = true;

			options.createParentCorrespondenceNodesSourcesLinks = true;
			options.createParentCorrespondenceNodesTargetsLinks = true;

			options.createLeftParentNodes = true;
			options.createLeftParentLinks = true;

			options.createRightParentNodes = true;
			options.createRightParentLinks = true;

			if (direction == TransformationDirection.FORWARD)
			{
				options.createLeftCreatedNodes = true;
				options.createLeftCreatedLinks = true;
				options.createLeftConstraints = true;
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.createRightCreatedNodes = true;
				options.createRightCreatedLinks = true;
				options.createRightConstraints = true;
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			/*
			 * Add a binding expression to the parent node with index
			 * parentCorrNodeNo
			 */
			int count = 0;

			for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
			{
				if (ce.getModifier() == TGGModifierEnumeration.NONE && ce instanceof CorrespondenceNode)
				{
					if (count == parentCorrNodeNo)
					{
						CorrespondenceNode cn = (CorrespondenceNode) ce;

						if (count == parentCorrNodeNo)
						{
							/*
							 * Create a direct assignment
							 */
							VariableReferenceAction variableReferenceAction = SDGeneratorHelper.createVariableReferenceAction(
									OptimizedIncrRepairGenerationStrategy.CORR_NODE, null, OptimizedIncrRepairGenerationStrategy.CORR_NODE,
									MotePackage.eINSTANCE.getTGGNode());

							((StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn))
									.setDirectAssignmentExpression(SDGeneratorHelper.createCallActionExpression(
											OptimizedIncrRepairGenerationStrategy.CORR_NODE, null, variableReferenceAction));

							((StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn))
									.setBindingType(BindingTypeEnumeration.BOUND);
						}
					}
					count++;
				}
			}

			findLHSSAN.getStoryPatternObjects().addAll(result.storyPatternObjects);
			findLHSSAN.getStoryPatternLinks().addAll(result.storyPatternLinks);

			/*
			 * Create story action node to check if the transformation was
			 * successful
			 */
			StoryActionNode checkIfSuccessfulSAN = NodesFactory.eINSTANCE.createStoryActionNode();
			checkIfSuccessfulSAN.setName("check if transformation was successful (" + parentCorrNodeNo + ")");

			activity.getNodes().add(checkIfSuccessfulSAN);

			this.createCheckIfTransformationSuccessfulSANContent(checkIfSuccessfulSAN, rule, direction, ruleClass);

			/*
			 * Create false final node
			 */
			ActivityFinalNode falseFinalNode = NodesFactory.eINSTANCE.createActivityFinalNode();
			falseFinalNode.setName("false");

			activity.getNodes().add(falseFinalNode);

			/*
			 * Create false literal declaration action
			 */
			LiteralDeclarationAction falseLiteral = SDGeneratorHelper.createLiteralDeclarationAction("false", null, "false",
					EcorePackage.Literals.EBOOLEAN_OBJECT);
			CallActionExpression falseCAE = SDGeneratorHelper.createCallActionExpression("false", null, falseLiteral);
			falseFinalNode.setReturnValue(falseCAE);

			/*
			 * Create edges
			 */
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(findLHSSAN, checkIfSuccessfulSAN, ActivityEdgeGuardEnumeration.FOR_EACH, null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(checkIfSuccessfulSAN, findLHSSAN, ActivityEdgeGuardEnumeration.SUCCESS, null));
			activity.getEdges().add(
					SDGeneratorHelper.createActivityEdge(checkIfSuccessfulSAN, falseFinalNode, ActivityEdgeGuardEnumeration.FAILURE, null));

			return findLHSSAN;
		}

		private void createCheckIfTransformationSuccessfulSANContent(StoryActionNode storyActionNode, TGGRule rule,
				TransformationDirection direction, EClass ruleClass) throws RuleGenerationException
		{
			StoryPatternGeneratorOptions options = new StoryPatternGeneratorOptions();

			options.direction = direction;

			options.createThisObject = true;
			options.thisEClass = ruleClass;

			options.createRuleSetObject = true;
			options.ruleSetObjectBinding = BindingTypeEnumeration.BOUND;

			options.createRuleSetToParentNodes_sourceModelElementsLinks = true;
			options.createRuleSetToParentNodes_targetModelElementsLinks = true;

			options.createParentCorrespondenceNodes = true;
			options.parentCorrespondenceNodesBinding = BindingTypeEnumeration.BOUND;

			options.createParentCorrespondenceNodesSourcesLinks = true;
			options.createParentCorrespondenceNodesTargetsLinks = true;

			options.createChildCorrespondenceNodesSourcesLinks = true;
			options.createChildCorrespondenceNodesTargetsLinks = true;

			options.createLeftParentNodes = true;
			options.createLeftParentLinks = true;
			options.leftParentNodesBinding = BindingTypeEnumeration.BOUND;

			options.createRightParentNodes = true;
			options.createRightParentLinks = true;
			options.rightParentNodesBinding = BindingTypeEnumeration.BOUND;

			options.createLeftCreatedNodes = true;
			options.createLeftCreatedLinks = true;

			options.createRightCreatedNodes = true;
			options.createRightCreatedLinks = true;

			options.createChildCorrespondenceNodes = true;
			options.createChildCorrespondenceNodesSourcesLinks = true;
			options.createChildCorrespondenceNodesTargetsLinks = true;

			options.createRuleSetToChildCorrespondenceNodes_correspondenceNodes_Link = true;

			options.createThisToChildCorrespondenceNodes_createdCorrNodes_Link = true;

			if (direction == TransformationDirection.FORWARD)
			{
				options.leftCreatedNodesBinding = BindingTypeEnumeration.BOUND;
				options.createRightAttributeChecks = true;
			}
			else if (direction == TransformationDirection.REVERSE)
			{
				options.rightCreatedNodesBinding = BindingTypeEnumeration.BOUND;
				options.createLeftAttributeChecks = true;
			}

			StoryPatternGeneratorResult result = SDGeneratorHelper.generateStoryPattern(options, rule);

			storyActionNode.getStoryPatternObjects().addAll(result.storyPatternObjects);
			storyActionNode.getStoryPatternLinks().addAll(result.storyPatternLinks);
		}

	}

	@Override
	public void createRules(String projectURI, String javaBasePackage, EPackage rulesPackage, TGGDiagram tggDiagram, EList<TGGRule> rules)
			throws RuleGenerationException
	{
		/*
		 * Add EClass for RuleSet.
		 */
		EClass ruleSetEClass = this.createRuleSetClass(tggDiagram, rulesPackage, javaBasePackage);

		/*
		 * Create classes for the rules along with their activities.
		 */
		this.createRuleClassesAndActivityDiagram(projectURI, javaBasePackage, rulesPackage, tggDiagram, ruleSetEClass, rules);
	}

	/**
	 * @param generatedRulesPackage
	 * @param tggDiagram
	 * @param requiredPackages
	 * @param projectUri
	 * @param ruleSetEClass
	 * @throws RuleGenerationException
	 */
	private void createRuleClassesAndActivityDiagram(	final String projectURI, 
														final String javaBasePackage, 
														final EPackage rulesPackage,
														final TGGDiagram tggDiagram,
														final EClass ruleSetEClass, 
														final EList<TGGRule> p_rulesToGenerate )
	throws RuleGenerationException {
		/*
		 * Create the activity diagram that will contain the activities for the
		 * rules
		 */
		//ResourceSet rs = rulesPackage.eResource().getResourceSet();

		/*
		 * Get the de.hpi.sam.mote.rules EPackage
		 */
		EPackage moteRulesPackage = this.getMoteRulesPackage(rulesPackage.eResource().getResourceSet());

		/*
		 * This map maps the TGGRules to their EClasses and activity diagrams
		 */
		final Map<TGGRule, RuleInformationContainer> ruleInformationMap = new HashMap<TGGRule, RuleInformationContainer>();
		/*
		 * Create a rule class and operations for each rule in the TGGDiagram
		 */
		System.out.println( new Date() + ": Create a rule class and operations for each rule in the TGGDiagram..." );
		
		for ( final TGGRule rule : tggDiagram.getTggRules() ) {
			final RuleInformationContainer ric = new RuleInformationContainer();
			ruleInformationMap.put(rule, ric);

			ric.isAxiom = SDGeneratorHelper.isAxiom(rule);
			ric.tggRule = rule;

			ric.ruleClass = EcoreFactory.eINSTANCE.createEClass();
			ric.ruleClass.setName(rule.getName());

			if (ric.isAxiom) {
				ric.ruleClass.getESuperTypes()
						.add((EClass) moteRulesPackage.getEClassifier(RulesPackage.eINSTANCE.getTGGAxiom().getName()));
			}
			else {
				ric.ruleClass.getESuperTypes().add((EClass) moteRulesPackage.getEClassifier(RulesPackage.eINSTANCE.getTGGRule().getName()));
			}

			/*
			 * Delete existing EClass
			 */
			if (rulesPackage.getEClassifier(ric.ruleClass.getName()) != null)
			{
				rulesPackage.getEClassifiers().remove(rulesPackage.getEClassifier(ric.ruleClass.getName()));
			}

			rulesPackage.getEClassifiers().add(ric.ruleClass);

			/*
			 * Add an annotation that contains the description of the rule
			 */
			EAnnotation annotation = EcoreFactory.eINSTANCE.createEAnnotation();
			annotation.setSource(GenerationStrategy.ECORE_GENMODEL_NSURI);
			annotation.getDetails().put(GenerationStrategy.ANNOTATION_DOCUMENTATION_KEY, rule.getDescription());

			ric.ruleClass.getEAnnotations().add(annotation);

			/*
			 * Create operations
			 */
			List<EOperation> operations = new LinkedList<EOperation>();
			operations
					.addAll(((EClass) moteRulesPackage.getEClassifier(RulesPackage.eINSTANCE.getTGGMapping().getName())).getEOperations());

			if (ric.isAxiom)
			{
				operations.addAll(((EClass) moteRulesPackage.getEClassifier(RulesPackage.eINSTANCE.getTGGAxiom().getName()))
						.getEOperations());
			}
			else
			{
				operations.addAll(((EClass) moteRulesPackage.getEClassifier(RulesPackage.eINSTANCE.getTGGRule().getName()))
						.getEOperations());
			}

			if (ric.isAxiom)
			{
				/*
				 * Transformation
				 */
				ric.transformationForwardOperation = this.createMainOperation(GenerationStrategy.FORWARD_TRANSFORMATION, ric.tggRule,
						operations, projectURI, ruleSetEClass.getName());
				ric.transformationMappingOperation = this.createMainOperation(GenerationStrategy.MAPPING_TRANSFORMATION, ric.tggRule,
						operations, projectURI, ruleSetEClass.getName());
				ric.transformationReverseOperation = this.createMainOperation(GenerationStrategy.REVERSE_TRANSFORMATION, ric.tggRule,
						operations, projectURI, ruleSetEClass.getName());

				ric.ruleClass.getEOperations().add(ric.transformationForwardOperation);
				ric.ruleClass.getEOperations().add(ric.transformationMappingOperation);
				ric.ruleClass.getEOperations().add(ric.transformationReverseOperation);

				/*
				 * Synchronization
				 */
				ric.synchronizationForwardOperation = this.createMainOperationSync(GenerationStrategy.FORWARD_SYNCHRONIZATION, ric.tggRule,
						operations, projectURI, ruleSetEClass.getName());
				ric.synchronizationMappingOperation = this.createMainOperationSync(GenerationStrategy.MAPPING_SYNCHRONIZATION, ric.tggRule,
						operations, projectURI, ruleSetEClass.getName());
				ric.synchronizationReverseOperation = this.createMainOperationSync(GenerationStrategy.REVERSE_SYNCHRONIZATION, ric.tggRule,
						operations, projectURI, ruleSetEClass.getName());

				ric.ruleClass.getEOperations().add(ric.synchronizationForwardOperation);
				ric.ruleClass.getEOperations().add(ric.synchronizationMappingOperation);
				ric.ruleClass.getEOperations().add(ric.synchronizationReverseOperation);
			}
			else
			{
				/*
				 * transformation
				 */
				ric.transformationForwardOperation = this.createMainOperation(GenerationStrategy.FORWARD_TRANSFORMATION, ric.tggRule,
						operations, projectURI, ruleSetEClass.getName());
				ric.transformationMappingOperation = this.createMainOperation(GenerationStrategy.MAPPING_TRANSFORMATION, ric.tggRule,
						operations, projectURI, ruleSetEClass.getName());
				ric.transformationReverseOperation = this.createMainOperation(GenerationStrategy.REVERSE_TRANSFORMATION, ric.tggRule,
						operations, projectURI, ruleSetEClass.getName());

				ric.ruleClass.getEOperations().add(ric.transformationForwardOperation);
				ric.ruleClass.getEOperations().add(ric.transformationMappingOperation);
				ric.ruleClass.getEOperations().add(ric.transformationReverseOperation);

				/*
				 * runtime checks
				 */
				// ric.runtimeCheckForwardOperation =
				// this.createMainOperation(IncrementalRepairGenerationStrategy.FORWARD_RUNTIME_CHECK,
				// ric.tggRule, operations, projectURI,
				// ruleSetEClass.getName());
				// ric.runtimeCheckReverseOperation =
				// this.createMainOperation(IncrementalRepairGenerationStrategy.REVERSE_RUNTIME_CHECK,
				// ric.tggRule, operations, projectURI,
				// ruleSetEClass.getName());
				//
				// ric.ruleClass.getEOperations().add(ric.runtimeCheckForwardOperation);
				// ric.ruleClass.getEOperations().add(ric.runtimeCheckReverseOperation);

				/*
				 * synchronization
				 */
				ric.synchronizationForwardOperation = this.createMainOperationSync(GenerationStrategy.FORWARD_SYNCHRONIZATION, ric.tggRule,
						operations, projectURI, ruleSetEClass.getName());
				ric.synchronizationMappingOperation = this.createMainOperationSync(GenerationStrategy.MAPPING_SYNCHRONIZATION, ric.tggRule,
						operations, projectURI, ruleSetEClass.getName());
				ric.synchronizationReverseOperation = this.createMainOperationSync(GenerationStrategy.REVERSE_SYNCHRONIZATION, ric.tggRule,
						operations, projectURI, ruleSetEClass.getName());

				ric.ruleClass.getEOperations().add(ric.synchronizationForwardOperation);
				ric.ruleClass.getEOperations().add(ric.synchronizationMappingOperation);
				ric.ruleClass.getEOperations().add(ric.synchronizationReverseOperation);

				/*
				 * acceptsParentCorrNode
				 */
				// DB: Replaced by initInputCorrNodeTypes
//				ric.acceptsParentCorrNodeOperation = this.createAcceptsParentCorrNodeOperation(rule,
//						javaBasePackage + "." + rulesPackage.getName());
//				ric.ruleClass.getEOperations().add(ric.acceptsParentCorrNodeOperation);

				/*
				 * initInputCorrNodeTypes
				 */
				final EOperation initInputCorrNodeTypesOp = createInitInputCorrNodeTypesOperation(	rule,
																									javaBasePackage + "." + rulesPackage.getName() );
				ric.ruleClass.getEOperations().add( initInputCorrNodeTypesOp );
			}
		}

		/*
		 * Create activities
		 */
		System.out.println( new Date() + ": Create activities..." );
		
		final boolean incremental = p_rulesToGenerate != null && !p_rulesToGenerate.isEmpty();
		
		for ( final TGGRule rule : tggDiagram.getTggRules() ) {
			final RuleInformationContainer ric = ruleInformationMap.get(rule);
			
			ric.transformationForwardActivity = createActivity( rule, rule.getName() + "_" + ric.transformationForwardOperation.getName(), incremental );
			ric.transformationMappingActivity = createActivity( rule, rule.getName() + "_" + ric.transformationMappingOperation.getName(), incremental );
			ric.transformationReverseActivity = createActivity( rule, rule.getName() + "_" + ric.transformationReverseOperation.getName(), incremental );

			ric.synchronizationForwardActivity = createActivity( rule, rule.getName() + "_" + ric.synchronizationForwardOperation.getName(), incremental );
			ric.synchronizationMappingActivity = createActivity( rule, rule.getName() + "_" + ric.synchronizationMappingOperation.getName(), incremental );
			ric.synchronizationReverseActivity = createActivity( rule, rule.getName() + "_" + ric.synchronizationReverseOperation.getName(), incremental );

			if ( !ric.isAxiom ) {
				/*
				 * transformation
				 */
//				ric.transformationForwardActivity = createActivity( rule, rule.getName() + "_" + ric.transformationForwardOperation.getName(), incremental );
//				ric.transformationMappingActivity = createActivity( rule, rule.getName() + "_" + ric.transformationMappingOperation.getName(), incremental );
//				ric.transformationReverseActivity = createActivity( rule, rule.getName() + "_" + ric.transformationReverseOperation.getName(), incremental );

				/*
				 * runtime checks
				 */
				// ric.runtimeCheckForwardActivity =
				// StoryDiagramEcoreFactory.eINSTANCE.createActivity();
				// ric.runtimeCheckReverseActivity =
				// StoryDiagramEcoreFactory.eINSTANCE.createActivity();

				/*
				 * synchronization
				 */
//				ric.synchronizationForwardActivity = StoryDiagramEcoreFactory.eINSTANCE.createActivity();
//				ric.synchronizationForwardActivity.setName(rule.getName() + "_" + ric.synchronizationForwardOperation.getName() );
//
//				ric.synchronizationMappingActivity = StoryDiagramEcoreFactory.eINSTANCE.createActivity();
//				ric.synchronizationMappingActivity.setName(rule.getName() + "_" + ric.synchronizationMappingOperation.getName() );
//
//				ric.synchronizationReverseActivity = StoryDiagramEcoreFactory.eINSTANCE.createActivity();
//				ric.synchronizationReverseActivity.setName(rule.getName() + "_" + ric.synchronizationReverseOperation.getName() );

				/*
				 * synchronize attributes
				 */
				ric.synchronizeAttributesForwardActivity = createActivity( rule, rule.getName() + "_" + FORWARD_SYNCHRONIZE_ATTRIBUTES, incremental );
				ric.synchronizeAttributesMappingActivity = createActivity( rule, rule.getName() + "_" + MAPPING_SYNCHRONIZE_ATTRIBUTES, incremental );
				ric.synchronizeAttributesReverseActivity = createActivity( rule, rule.getName() + "_" + REVERSE_SYNCHRONIZE_ATTRIBUTES, incremental );

				/*
				 * repair structure
				 */
				ric.repairForwardActivity = createActivity( rule, rule.getName() + "_" + FORWARD_REPAIR_STRUCTURE, incremental );
				ric.repairMappingActivity = createActivity( rule, rule.getName() + "_" + MAPPING_REPAIR_STRUCTURE, incremental );
				ric.repairReverseActivity = createActivity( rule, rule.getName() + "_" + REVERSE_REPAIR_STRUCTURE, incremental );
			}
		}
		
		addToResource( projectURI, ruleInformationMap, rulesPackage );

		/*
		 * Create activities' contents
		 */

		final Collection<TGGRule> diagRulesToGenerate;
		
		if ( incremental ) {
			diagRulesToGenerate = new ArrayList<TGGRule>();

			for ( final TGGRule ruleToGen : p_rulesToGenerate ) {
				for ( final TGGRule rule : tggDiagram.getTggRules() ) {
					if ( ruleToGen.getUuid().equals( rule.getUuid() ) ) {
						diagRulesToGenerate.add( rule );
						
						break;
					}
				}
			}
		}
		else {
			diagRulesToGenerate = tggDiagram.getTggRules();
		}

		createActivitiesContents( projectURI, diagRulesToGenerate, ruleInformationMap, rulesPackage );
	}
	
	private Activity createActivity( 	final TGGRule p_rule,
										final String p_activityName,
										final boolean pb_incremental ) {
		final Activity activity = StoryDiagramEcoreFactory.eINSTANCE.createActivity();
		activity.setName( p_activityName );
		
		if ( pb_incremental ) {
			// Preserve the UUID
			
			final Activity existAct = getExistActivity( p_rule, p_activityName );
			
			if ( existAct !=  null ) {
				activity.setUuid( existAct.getUuid() );
			}
		}
		
		return activity;
	}
	
	private void createActivitiesContents( 	final String projectURI,
											final Collection<TGGRule> p_rulesToGenerate,
											final Map<TGGRule, RuleInformationContainer> ruleInformationMap,
											final EPackage rulesPackage ) 
	throws RuleGenerationException {
		System.out.println( new Date() + ": Create activities' contents..." );
		
		for ( final TGGRule rule : p_rulesToGenerate /*tggDiagram.getTggRules()*/) {
			System.out.println( new Date() + ": Create activities' contents for " + rule.getName() + "..." );
			final RuleInformationContainer ric = copy( ruleInformationMap.get( rule ) );

			if (ric.isAxiom) {
				/*
				 * transformation
				 */
				AxtiomTransformationActivityGenerator axiomTransformationActivityGenerator = new AxtiomTransformationActivityGenerator();

				axiomTransformationActivityGenerator.createTransformationActivity(rule, TransformationDirection.FORWARD, ric.ruleClass,
						ric.transformationForwardOperation, ric.transformationForwardActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.transformationForwardActivity);

				axiomTransformationActivityGenerator.createTransformationActivity(rule, TransformationDirection.MAPPING, ric.ruleClass,
						ric.transformationMappingOperation, ric.transformationMappingActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.transformationMappingActivity);

				axiomTransformationActivityGenerator.createTransformationActivity(rule, TransformationDirection.REVERSE, ric.ruleClass,
						ric.transformationReverseOperation, ric.transformationReverseActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.transformationReverseActivity);

				/*
				 * synchronization
				 */
				AxtiomSynchronizationActivityGenerator axtiomSynchronizationActivityGenerator = new AxtiomSynchronizationActivityGenerator();

				axtiomSynchronizationActivityGenerator.createSynchronizationActivity(rule, TransformationDirection.FORWARD, ric.ruleClass,
						ric.synchronizationForwardOperation, ric.synchronizationForwardActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.synchronizationForwardActivity);

				axtiomSynchronizationActivityGenerator.createSynchronizationActivity(rule, TransformationDirection.MAPPING, ric.ruleClass,
						ric.synchronizationMappingOperation, ric.synchronizationMappingActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.synchronizationMappingActivity);

				axtiomSynchronizationActivityGenerator.createSynchronizationActivity(rule, TransformationDirection.REVERSE, ric.ruleClass,
						ric.synchronizationReverseOperation, ric.synchronizationReverseActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.synchronizationReverseActivity);
			}
			else
			{
				/*
				 * transformation
				 */
				TransformationActivityGenerator transformationActivityGenerator = new TransformationActivityGenerator();

				transformationActivityGenerator.createTransformationActivity(	rule,
																				TransformationDirection.FORWARD,
																				ric.ruleClass,
																				ric.transformationForwardOperation,
																				ric.transformationForwardActivity );

				//this.addToResource(projectURI, rs, ric.transformationForwardActivity);

				transformationActivityGenerator.createTransformationActivity(rule, TransformationDirection.MAPPING, ric.ruleClass,
						ric.transformationMappingOperation, ric.transformationMappingActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.transformationMappingActivity);

				transformationActivityGenerator.createTransformationActivity(rule, TransformationDirection.REVERSE, ric.ruleClass,
						ric.transformationReverseOperation, ric.transformationReverseActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.transformationReverseActivity);

				/*
				 * runtime checks
				 */
				// RuntimeCheckActivityGenerator runtimeCheckActivityGenerator =
				// new RuntimeCheckActivityGenerator();
				//
				// runtimeCheckActivityGenerator.createRuntimeCheckActivity(rule,
				// TransformationDirection.FORWARD, ric.ruleClass,
				// ric.runtimeCheckForwardOperation,
				// ric.runtimeCheckForwardActivity, ruleInformationMap);
				//
				// runtimeCheckActivityGenerator.createRuntimeCheckActivity(rule,
				// TransformationDirection.REVERSE, ric.ruleClass,
				// ric.runtimeCheckReverseOperation,
				// ric.runtimeCheckReverseActivity, ruleInformationMap);

				/*
				 * synchronization
				 */
				SynchronizationActivityGenerator synchronizationActivityGenerator = new SynchronizationActivityGenerator();

				synchronizationActivityGenerator.createSynchronizationActivity(rule, TransformationDirection.FORWARD, ric.ruleClass,
						ric.synchronizationForwardOperation, ric.synchronizationForwardActivity, ruleInformationMap );

				//this.addToResource(projectURI, rs, ric.synchronizationForwardActivity);

				synchronizationActivityGenerator.createSynchronizationActivity(rule, TransformationDirection.MAPPING, ric.ruleClass,
						ric.synchronizationMappingOperation, ric.synchronizationMappingActivity, ruleInformationMap );

				//this.addToResource(projectURI, rs, ric.synchronizationMappingActivity);

				synchronizationActivityGenerator.createSynchronizationActivity(rule, TransformationDirection.REVERSE, ric.ruleClass,
						ric.synchronizationReverseOperation, ric.synchronizationReverseActivity, ruleInformationMap );

				//this.addToResource(projectURI, rs, ric.synchronizationReverseActivity);

				/*
				 * Synchronize attributes
				 */
				SynchronizeAttributesActivityGenerator synchronizeAttributesActivityGenerator = new SynchronizeAttributesActivityGenerator();

				synchronizeAttributesActivityGenerator.createSynchronizeAttributesActivity(rule, TransformationDirection.FORWARD,
						ric.ruleClass, ric.synchronizeAttributesForwardActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.synchronizeAttributesForwardActivity);

				synchronizeAttributesActivityGenerator.createSynchronizeAttributesActivity(rule, TransformationDirection.MAPPING,
						ric.ruleClass, ric.synchronizeAttributesMappingActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.synchronizeAttributesMappingActivity);

				synchronizeAttributesActivityGenerator.createSynchronizeAttributesActivity(rule, TransformationDirection.REVERSE,
						ric.ruleClass, ric.synchronizeAttributesReverseActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.synchronizeAttributesReverseActivity);

				/*
				 * Repair structure
				 */
				RepairStructureActivityGenerator repairStructureActivityGenerator = new RepairStructureActivityGenerator();

				repairStructureActivityGenerator.createRepairStructureActivity(rule, TransformationDirection.FORWARD, ric.ruleClass,
						ric.repairForwardActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.repairForwardActivity);

				repairStructureActivityGenerator.createRepairStructureActivity(rule, TransformationDirection.MAPPING, ric.ruleClass,
						ric.repairMappingActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.repairMappingActivity);

				repairStructureActivityGenerator.createRepairStructureActivity(rule, TransformationDirection.REVERSE, ric.ruleClass,
						ric.repairReverseActivity/*, ruleInformationMap*/);

				//this.addToResource(projectURI, rs, ric.repairReverseActivity);
			}
			
			final Activity[] activities = new Activity[] { 	ric.transformationForwardActivity, 
															ric.transformationMappingActivity,
															ric.transformationReverseActivity,
															ric.synchronizationForwardActivity, 
															ric.synchronizationMappingActivity, 
															ric.synchronizationReverseActivity,
															ric.synchronizeAttributesForwardActivity, 
															ric.synchronizeAttributesMappingActivity,
															ric.synchronizeAttributesReverseActivity, 
															ric.repairForwardActivity,
															ric.repairMappingActivity,
															ric.repairReverseActivity };
			
			final ResourceSet resSet = new ResourceSetImpl();
			/*
			 * Add the generated rules package to the package registry and change
			 * the URI of its containing resource to the nsURI.
			 */
			System.out.println( new Date() + ": Add generated rules package to package registry..." );
			URI generatedPackageURI = rulesPackage.eResource().getURI();
			rulesPackage.eResource().setURI(URI.createURI(rulesPackage.getNsURI()));

			resSet.getPackageRegistry().put(rulesPackage.getNsURI(), rulesPackage);

			try {
				for ( final Activity activity : activities ) {
					if ( activity != null ) {
						System.out.println( new Date() + ": Saving activity " + activity.getName() + "..." );
	
						addToResource( projectURI, resSet, activity );
						activity.eResource().save( null );
					}
				}
			}
			catch( final IOException p_ex ) {
				throw new RuleGenerationException( "", p_ex );
			}
			finally {
				rulesPackage.eResource().setURI(generatedPackageURI);
			}
		}
	}
	
	private void addToResource( final String projectURI,
								final Map<TGGRule, RuleInformationContainer> ruleInformationMap,
								final EPackage rulesPackage ) {
		/*
		 * Add the generated rules package to the package registry and change
		 * the URI of its containing resource to the nsURI.
		 */
		System.out.println( new Date() + ": Add generated rules package to package registry..." );
		URI generatedPackageURI = rulesPackage.eResource().getURI();
		rulesPackage.eResource().setURI(URI.createURI(rulesPackage.getNsURI()));

		final ResourceSet resSet = rulesPackage.eResource().getResourceSet();
		resSet.getPackageRegistry().put(rulesPackage.getNsURI(), rulesPackage);

		for ( final RuleInformationContainer ric : ruleInformationMap.values())	{
			final Activity[] activities = new Activity[] {
					ric.transformationForwardActivity, ric.transformationMappingActivity, ric.transformationReverseActivity,
					ric.synchronizationForwardActivity, ric.synchronizationMappingActivity, ric.synchronizationReverseActivity,
					ric.synchronizeAttributesForwardActivity, ric.synchronizeAttributesMappingActivity,
					ric.synchronizeAttributesReverseActivity, ric.repairForwardActivity, ric.repairMappingActivity,
					ric.repairReverseActivity
			};

			for ( final Activity activity : activities ) {
				if (activity != null) {
					addToResource(projectURI, resSet, activity);
				}
			}
		}

		rulesPackage.eResource().setURI(generatedPackageURI);
	}

	private void addToResource(String projectURI, ResourceSet rs, Activity activity) {
		final ActivityDiagram ad = StoryDiagramEcoreFactory.eINSTANCE.createActivityDiagram();
		ad.getActivities().add(activity);

		final Resource r = rs.createResource(URI.createPlatformResourceURI(projectURI + "/model/story/" + activity.getName() + ".story", true));
		System.out.println( new Date() + ": Adding activity " + activity.getName() + " to resource " + r.getURI() + "..." );

		r.getContents().add(ad);
	}

	private EPackage getMoteRulesPackage(ResourceSet resourceSet)
	{
		return (EPackage) resourceSet.getEObject(URI.createURI("platform:/plugin/de.hpi.sam.mote/model/mote.ecore#//rules"), true);
	}

	private EOperation createMainOperation(String methodName, TGGRule rule, List<EOperation> operations, String projectName,
			String ruleSetName)
	{
		EOperation operation = EcoreUtil.copy(GenerationStrategy.getEOperation(operations, methodName));

		operation.getEAnnotations().clear();

		EAnnotation annotation = EcoreFactory.eINSTANCE.createEAnnotation();
		annotation.setSource(GenerationStrategy.ECORE_GENMODEL_NSURI);

		operation.getEAnnotations().add(annotation);

		List<String> parameterNames = new LinkedList<String>();

		for (EParameter p : operation.getEParameters())
		{
			parameterNames.add(p.getName());
		}

		final String paramName = getParamName( rule.isIsAxiom(), operation.getName() );
		
		/*
		 * Create source code
		 */
		annotation.getDetails().put(GenerationStrategy.ANNOTATION_BODY_KEY,
				RuleCallStoryDiagramTemplate.create(null).generate(projectName, ruleSetName, rule.getName() + "_" + methodName, operation, paramName ));

		return operation;
	}
	
	private String getParamName( 	final boolean isAxiom,
									final String opName ) {
		if ( isAxiom ) {
			if ( opName.startsWith( "reverse") ) {
				return "target";
			}
			
			return "source";
		}
		
		return "parentCorrNode";
	}

	private EOperation createMainOperationSync(String methodName, TGGRule rule, List<EOperation> operations, String projectName,
			String ruleSetName)
	{
		EOperation operation = EcoreUtil.copy(GenerationStrategy.getEOperation(operations, methodName));

		operation.getEAnnotations().clear();

		EAnnotation annotation = EcoreFactory.eINSTANCE.createEAnnotation();
		annotation.setSource(GenerationStrategy.ECORE_GENMODEL_NSURI);

		operation.getEAnnotations().add(annotation);

		List<String> parameterNames = new LinkedList<String>();

		for (EParameter p : operation.getEParameters())
		{
			parameterNames.add(p.getName());
		}

		final String paramName = getParamName( rule.isIsAxiom(), operation.getName() );

		/*
		 * Create source code
		 */
		annotation.getDetails().put(
				GenerationStrategy.ANNOTATION_BODY_KEY,
				RuleCallStoryDiagramTemplateSync.create(null).generate(projectName, ruleSetName, rule.getName() + "_" + methodName,
						operation, paramName));

		return operation;
	}

	private EOperation createInitInputCorrNodeTypesOperation(TGGRule rule, String basePackage) {
		/*
		 * Create operation
		 */
		EPackage moteRulesPackage = this.getMoteRulesPackage(rule.eResource().getResourceSet());

		EClass ruleClass = (EClass) moteRulesPackage.getEClassifier(RulesPackage.eINSTANCE.getTGGRule().getName());

		EOperation operation = EcoreUtil.copy(GenerationStrategy.getEOperation(ruleClass.getEOperations(),
				OptimizedIncrRepairGenerationStrategy.INIT_INPUT_CORR_NODE_TYPES));

		operation.getEAnnotations().clear();

		/*
		 * Create annotation
		 */

		EAnnotation annotation = EcoreFactory.eINSTANCE.createEAnnotation();
		annotation.setSource(GenerationStrategy.ECORE_GENMODEL_NSURI);

		operation.getEAnnotations().add(annotation);

		annotation.getDetails().put(GenerationStrategy.ANNOTATION_BODY_KEY,
				InitInputCorrNodeTypesOperationTemplate.create(null).generate(basePackage, rule));

		return operation;
	}
//
//	private EOperation createAcceptsParentCorrNodeOperation(TGGRule rule, String basePackage)
//	{
//		/*
//		 * Create operation
//		 */
//		EPackage moteRulesPackage = this.getMoteRulesPackage(rule.eResource().getResourceSet());
//
//		EClass ruleClass = (EClass) moteRulesPackage.getEClassifier(RulesPackage.eINSTANCE.getTGGRule().getName());
//
//		EOperation operation = EcoreUtil.copy(GenerationStrategy.getEOperation(ruleClass.getEOperations(),
//				OptimizedIncrRepairGenerationStrategy.ACCEPTS_PARENT_CORR_NODE));
//
//		operation.getEAnnotations().clear();
//
//		/*
//		 * Create annotation
//		 */
//
//		EAnnotation annotation = EcoreFactory.eINSTANCE.createEAnnotation();
//		annotation.setSource(GenerationStrategy.ECORE_GENMODEL_NSURI);
//
//		operation.getEAnnotations().add(annotation);
//
//		/*
//		 * Create source code
//		 */
//		Set<String> typeNames = new HashSet<String>();
//
//		int count = 0;
//
//		for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
//		{
//			if (ce.getModifier() == TGGModifierEnumeration.NONE && ce instanceof CorrespondenceNode)
//			{
//				if (this.checkIfTransformationPossible(rule, count))
//				{
//					typeNames.add(this.getFQN(((CorrespondenceNode) ce).getClassifier()));
//				}
//
//				count++;
//			}
//		}
//
//		annotation.getDetails().put(GenerationStrategy.ANNOTATION_BODY_KEY,
//				AcceptsParentCorrNodeOperationTemplate.create(null).generate(basePackage, rule));
//
//		return operation;
//	}

	protected EOperation createSynchronizeAttributesOperation(TransformationDirection direction, TGGRule rule,
			Map<String, EPackage> requiredPackages, EClass ruleSetEClass)
	{
		/*
		 * Create operation
		 */
		EOperation operation = EcoreFactory.eINSTANCE.createEOperation();

		/*
		 * Set return value
		 */
		operation.setEType(EcorePackage.eINSTANCE.getEBooleanObject());

		/*
		 * Set name depending on the direction
		 */
		switch (direction)
		{
			case FORWARD:
			{
				operation.setName(OptimizedIncrRepairGenerationStrategy.FORWARD_SYNCHRONIZE_ATTRIBUTES);
				break;
			}
			case MAPPING:
			{
				operation.setName(OptimizedIncrRepairGenerationStrategy.MAPPING_SYNCHRONIZE_ATTRIBUTES);
				break;
			}
			case REVERSE:
			{
				operation.setName(OptimizedIncrRepairGenerationStrategy.REVERSE_SYNCHRONIZE_ATTRIBUTES);
				break;
			}
		}

		List<String> parameterNames = new LinkedList<String>();

		/*
		 * Create parameters for the correspondence nodes
		 */
		for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
		{
			if (ce instanceof CorrespondenceNode)
			{
				CorrespondenceNode cn = (CorrespondenceNode) ce;

				EParameter parameter = EcoreFactory.eINSTANCE.createEParameter();
				parameter.setName(cn.getName());
				parameter.setEType(cn.getClassifier());

				operation.getEParameters().add(parameter);

				parameterNames.add(parameter.getName());
			}
		}

		/*
		 * Create parameters for the source domain elements
		 */
		for (ModelElement me : rule.getSourceDomain().getModelElements())
		{
			if (me instanceof ModelObject)
			{
				ModelObject mo = (ModelObject) me;

				EParameter parameter = EcoreFactory.eINSTANCE.createEParameter();
				parameter.setName(mo.getName() + OptimizedIncrRepairGenerationStrategy.PARAMETER);
				parameter.setEType(EcorePackage.eINSTANCE.getEObject());

				operation.getEParameters().add(parameter);

				parameterNames.add(parameter.getName());
			}
		}

		/*
		 * Create parameters for the target domain elements
		 */
		for (ModelElement me : rule.getTargetDomain().getModelElements())
		{
			if (me instanceof ModelObject)
			{
				ModelObject mo = (ModelObject) me;

				EParameter parameter = EcoreFactory.eINSTANCE.createEParameter();
				parameter.setName(mo.getName() + OptimizedIncrRepairGenerationStrategy.PARAMETER);
				parameter.setEType(EcorePackage.eINSTANCE.getEObject());

				operation.getEParameters().add(parameter);

				parameterNames.add(parameter.getName());
			}
		}

		/*
		 * Create annotation
		 */
		EAnnotation annotation = EcoreFactory.eINSTANCE.createEAnnotation();
		annotation.setSource(GenerationStrategy.ECORE_GENMODEL_NSURI);

		operation.getEAnnotations().add(annotation);

		/*
		 * Create source code
		 */
		// annotation.getDetails().put(
		// GenerationStrategy.ANNOTATION_BODY_KEY,
		// ExecuteActivityMethodTemplate.create(null).generate(rule.getName() +
		// "." + operation.getName(), parameterNames, "Boolean",
		// ruleSetEClass.getEPackage().getNsPrefix() + "." +
		// ruleSetEClass.getName()));

		return operation;
	}

	protected EOperation createRepairStructureOperation(TransformationDirection direction, TGGRule rule,
			Map<String, EPackage> requiredPackages, EClass ruleSetEClass)
	{
		/*
		 * Create operation
		 */
		EOperation operation = EcoreFactory.eINSTANCE.createEOperation();

		/*
		 * Set return type to Boolean
		 */
		operation.setEType(EcorePackage.eINSTANCE.getEBooleanObject());

		/*
		 * Set name depending on the direction
		 */
		switch (direction)
		{
			case FORWARD:
			{
				operation.setName(OptimizedIncrRepairGenerationStrategy.FORWARD_REPAIR_STRUCTURE);
				break;
			}
			case MAPPING:
			{
				operation.setName(OptimizedIncrRepairGenerationStrategy.MAPPING_REPAIR_STRUCTURE);
				break;
			}
			case REVERSE:
			{
				operation.setName(OptimizedIncrRepairGenerationStrategy.REVERSE_REPAIR_STRUCTURE);
				break;
			}
		}

		List<String> parameterNames = new LinkedList<String>();

		/*
		 * Create parameters for the child correspondence nodes.
		 */
		for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
		{
			if (ce.getModifier() == TGGModifierEnumeration.CREATE && ce instanceof CorrespondenceNode)
			{
				EParameter parameter = EcoreFactory.eINSTANCE.createEParameter();
				parameter.setName(OptimizedIncrRepairGenerationStrategy.CORR_NODE);
				parameter.setEType(MotePackage.eINSTANCE.getTGGNode());

				operation.getEParameters().add(parameter);

				parameterNames.add(parameter.getName());
			}
		}

		/*
		 * Create annotation
		 */
		EAnnotation annotation = EcoreFactory.eINSTANCE.createEAnnotation();
		annotation.setSource(GenerationStrategy.ECORE_GENMODEL_NSURI);

		operation.getEAnnotations().add(annotation);

		/*
		 * Create source code
		 */
		// annotation.getDetails().put(
		// GenerationStrategy.ANNOTATION_BODY_KEY,
		// ExecuteActivityMethodTemplate.create(null).generate(rule.getName() +
		// "." + operation.getName(), parameterNames, "Boolean",
		// ruleSetEClass.getEPackage().getNsPrefix() + "." +
		// ruleSetEClass.getName()));

		return operation;
	}

	protected void createAddModificationTagToQueueExpressionActivityNodeContent(ExpressionActivityNode expressionActivityNode,
			TGGRule rule, EClass ruleClass, /*Map<TGGRule, RuleInformationContainer> ruleInformationMap,*/ boolean transform,
			boolean synchronize, boolean synchronizeSelf, TransformationDirection direction)
	{
		String transformValue = String.valueOf(transform);
		String synchronizeValue = String.valueOf(synchronize);
		String synchronizeSelfValue = String.valueOf(synchronizeSelf);

		/*
		 * Get the created correspondence node
		 */
		CorrespondenceNode createdCorrNode = null;

		for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
		{
			if (ce.getModifier() == TGGModifierEnumeration.CREATE && ce instanceof CorrespondenceNode)
			{
				createdCorrNode = (CorrespondenceNode) ce;
				break;
			}
		}

		/*
		 * Create variable reference for correspondence node
		 */
		VariableReferenceAction corrNodeReferenceAction = SDGeneratorHelper.createVariableReferenceAction(createdCorrNode.getName(), null,
				createdCorrNode.getName(), createdCorrNode.getClassifier());

		CallActionExpression corrNodeReferenceExpression = SDGeneratorHelper.createCallActionExpression(createdCorrNode.getName(), null,
				corrNodeReferenceAction);

		CallActionParameter corrNodeParameter = SDGeneratorHelper.createCallActionParameter(createdCorrNode.getName(), null,
				MotePackage.eINSTANCE.getTGGNode(), corrNodeReferenceExpression);

		/*
		 * Create literal to perform transformation
		 */
		LiteralDeclarationAction transformLiteralAction = SDGeneratorHelper.createLiteralDeclarationAction(transformValue, null,
				transformValue, EcorePackage.Literals.EBOOLEAN);

		CallActionExpression transformLiteralExpression = SDGeneratorHelper.createCallActionExpression(transformValue, null,
				transformLiteralAction);

		CallActionParameter transformParameter = SDGeneratorHelper.createCallActionParameter(transformValue, null,
				EcorePackage.Literals.EBOOLEAN, transformLiteralExpression);

		/*
		 * Create literal to perform synchronization of children
		 */
		LiteralDeclarationAction synchronizeLiteralAction = SDGeneratorHelper.createLiteralDeclarationAction(synchronizeValue, null,
				synchronizeValue, EcorePackage.Literals.EBOOLEAN);

		CallActionExpression synchronizeLiteralExpression = SDGeneratorHelper.createCallActionExpression(synchronizeValue, null,
				synchronizeLiteralAction);

		CallActionParameter synchronizeParameter = SDGeneratorHelper.createCallActionParameter(synchronizeValue, null,
				EcorePackage.Literals.EBOOLEAN, synchronizeLiteralExpression);

		/*
		 * Create literal to perform synchronization of this correspondence node
		 */
		LiteralDeclarationAction synchronizeSelfLiteralAction = SDGeneratorHelper.createLiteralDeclarationAction(synchronizeSelfValue,
				null, synchronizeSelfValue, EcorePackage.Literals.EBOOLEAN);

		CallActionExpression synchronizeSelfLiteralExpression = SDGeneratorHelper.createCallActionExpression(synchronizeSelfValue, null,
				synchronizeSelfLiteralAction);

		CallActionParameter synchronizeSelfParameter = SDGeneratorHelper.createCallActionParameter(synchronizeSelfValue, null,
				EcorePackage.Literals.EBOOLEAN, synchronizeSelfLiteralExpression);

		/*
		 * Create variable reference for the ruleSet
		 */
		VariableReferenceAction ruleSetReferenceAction = SDGeneratorHelper.createVariableReferenceAction(GenerationStrategy.RULE_SET, null,
				GenerationStrategy.RULE_SET, RulesPackage.Literals.TGG_RULE_SET);

		CallActionExpression ruleSetReferenceExpression = SDGeneratorHelper.createCallActionExpression(GenerationStrategy.RULE_SET, null,
				ruleSetReferenceAction);

		/*
		 * Create method call action to call addModificationTagToQueue()
		 */
		MethodCallAction methodCallAction = SDGeneratorHelper.createMethodCallAction("add modification tag to queue", null,
				ruleSetReferenceExpression, null, "de.hpi.sam.mote.rules.impl.TGGRuleSetImpl", "addModificationTagToQueue", null);

		methodCallAction.getParameters().add(corrNodeParameter);
		methodCallAction.getParameters().add(transformParameter);
		methodCallAction.getParameters().add(synchronizeParameter);
		methodCallAction.getParameters().add(synchronizeSelfParameter);

		CallActionExpression methodCallExpression = SDGeneratorHelper.createCallActionExpression("add modification tag to queue", null,
				methodCallAction);

		expressionActivityNode.setExpression(methodCallExpression);
	}

	/**
	 * Creates a method call action, that invokes the XXXDeleteElements method.
	 * 
	 * @param rule
	 * @param direction
	 * @param isAxiom
	 * @param ruleClass
	 * @param operation
	 * @return
	 */
	protected void createCallDeleteElementsExpressionActivityNodeContent(TGGRule rule, TransformationDirection direction, EClass ruleClass,
			ExpressionActivityNode expressionActivityNode)
	{
		CallActionExpression expression = SDGeneratorHelper.createCallActionExpression("delete obsolete elements", null, null);

		expressionActivityNode.setExpression(expression);

		for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
		{
			if (ce.getModifier() == TGGModifierEnumeration.CREATE && ce instanceof CorrespondenceNode)
			{
				//CorrespondenceNode cn = (CorrespondenceNode) ce;

				/*
				 * Create method call action
				 */
				MethodCallAction methodCallAction = null;

				methodCallAction = SDGeneratorHelper.createMethodCallAction("invoke deleteElements", null, null, null,
						"de.hpi.sam.mote.rules.TGGRuleSetImpl", "deleteElements", null);

				expression.getCallActions().add(methodCallAction);

				/*
				 * Create variable reference action for the instance object that
				 * references the ruleSet object
				 */
				VariableReferenceAction variableReferenceAction = SDGeneratorHelper.createVariableReferenceAction(
						GenerationStrategy.RULE_SET, null, GenerationStrategy.RULE_SET, RulesPackage.Literals.TGG_RULE_SET);

				CallActionExpression variableReferenceExpression = SDGeneratorHelper.createCallActionExpression(null, null,
						variableReferenceAction);

				methodCallAction.setInstanceVariable(variableReferenceExpression);

				/*
				 * Create parameter that references the created correspondence
				 * node
				 */
				CallActionParameter variableReferenceParameter = CallActionsFactory.eINSTANCE.createCallActionParameter();
				variableReferenceParameter.setParameterClassfier(MotePackage.eINSTANCE.getTGGNode());

				methodCallAction.getParameters().add(variableReferenceParameter);

				/*
				 * Create variable reference action that references the created
				 * correspondence node
				 */
				// variableReferenceAction =
				// SDGeneratorHelper.createVariableReferenceAction(cn.getName(),
				// null, cn.getName(),
				// cn.getClassifier());

				variableReferenceAction = SDGeneratorHelper.createVariableReferenceAction("corrNode", null, "corrNode",
						MotePackage.Literals.TGG_NODE);

				variableReferenceExpression = SDGeneratorHelper.createCallActionExpression(null, null, variableReferenceAction);

				variableReferenceParameter.setParameterValueAction(variableReferenceExpression);

				/*
				 * Create parameter that references the appropriate
				 * TransformationDirection literal
				 */
				LiteralDeclarationAction directionLiteralAction = SDGeneratorHelper.createLiteralDeclarationAction(null, null,
						direction.getLiteral(), MotePackage.Literals.TRANSFORMATION_DIRECTION);

				CallActionExpression directionLiteralExpression = SDGeneratorHelper.createCallActionExpression(null, null,
						directionLiteralAction);

				CallActionParameter directionLiteralParameter = SDGeneratorHelper.createCallActionParameter(null, null,
						MotePackage.Literals.TRANSFORMATION_DIRECTION, directionLiteralExpression);

				methodCallAction.getParameters().add(directionLiteralParameter);
			}
		}
	}

	private void createPrintErrorMessageExpressionActivityNodeContent(ExpressionActivityNode expressionActivityNode, TGGRule rule,
			EClass ruleClass, String errorMsg)
	{
		/*
		 * Create literal declaration action for the error message
		 */
		LiteralDeclarationAction literalDeclarationAction = SDGeneratorHelper.createLiteralDeclarationAction(errorMsg, null, errorMsg,
				EcorePackage.eINSTANCE.getEString());

		CallActionExpression literalDeclarationExpression = SDGeneratorHelper.createCallActionExpression(errorMsg, null,
				literalDeclarationAction);

		CallActionParameter literalParameter = SDGeneratorHelper.createCallActionParameter(errorMsg, null,
				EcorePackage.eINSTANCE.getEString(), literalDeclarationExpression);

		/*
		 * Create variable reference for ruleSet
		 */
		VariableReferenceAction ruleSetReferenceAction = SDGeneratorHelper.createVariableReferenceAction(GenerationStrategy.RULE_SET, null,
				GenerationStrategy.RULE_SET, RulesPackage.Literals.TGG_RULE_SET);

		CallActionExpression ruleSetReferenceExpression = SDGeneratorHelper.createCallActionExpression(GenerationStrategy.RULE_SET, null,
				ruleSetReferenceAction);

		/*
		 * create method call action
		 */
		EOperation printMessageOperation = GenerationStrategy.getEOperation(RulesPackage.Literals.TGG_RULE_SET.getEOperations(),
				OptimizedIncrRepairGenerationStrategy.PRINT_MESSAGE);

		MethodCallAction printErrorMethodCallAction = SDGeneratorHelper.createMethodCallAction(
				OptimizedIncrRepairGenerationStrategy.PRINT_MESSAGE, null, ruleSetReferenceExpression, printMessageOperation, null, null,
				null);

		printErrorMethodCallAction.getParameters().add(literalParameter);

		CallActionExpression printMessageActionExpression = SDGeneratorHelper.createCallActionExpression(
				OptimizedIncrRepairGenerationStrategy.PRINT_MESSAGE, null, printErrorMethodCallAction);

		expressionActivityNode.setExpression(printMessageActionExpression);
	}

	/**
	 * This method checks if a transformation of elements is possible from the
	 * parent correspondence node with the given index.
	 * 
	 * @param rule
	 * @param direction
	 * @param i
	 * @return
	 */
	private boolean checkIfTransformationPossible(	TGGRule rule,
													CorrespondenceNode parentCorrNode )
	{
		//int count = 0;

//		for (CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
//		{
//			if (ce.getModifier() == TGGModifierEnumeration.NONE && ce instanceof CorrespondenceNode)
//			{
//				if ( ce == parentCorrNode )
//				{
//					CorrespondenceNode cn = (CorrespondenceNode) ce;
					/*
					 * Analyze
					 */

					/*
					 * The correspondence node must be connected to elements on
					 * both sides
					 */
					boolean connectedToLeftSide = false;
					boolean connectedToRightSide = false;

					for (CorrespondenceLink cl : parentCorrNode.getOutgoingCorrespondenceLinks())
					{
						if (rule.getSourceDomain().getModelElements().contains(cl.getTarget()))
						{
							connectedToLeftSide = true;
						}
						else if (rule.getTargetDomain().getModelElements().contains(cl.getTarget()))
						{
							connectedToRightSide = true;
						}
					}

					return connectedToLeftSide && connectedToRightSide;
//					{
//						return false;
//					}

					/*
					 * TODO Perform other checks, especially reachability.
					 */

					//return true;
			//	}

				//count++;
//			}
//		}

		//return false;
	}

	private void createEvaluateRuleVariablesExpressionActivityNodeContent(TGGRule rule, ExpressionActivityNode node,
			TransformationDirection direction)
	{
		/*
		 * Create a call action expression
		 */
		CallActionExpression cae = SDGeneratorHelper.createCallActionExpression("evaluate rule variables", null, null);

		node.setExpression(cae);

		if (rule.getRuleVariables().isEmpty())
		{
			CallAction trueLiteral = SDGeneratorHelper.createLiteralDeclarationAction("true", null, "true", EcorePackage.Literals.EBOOLEAN);

			cae.getCallActions().add(trueLiteral);
		}
		else
		{
			/*
			 * Create a call action for each rule variable
			 */
			for (RuleVariable ruleVariable : rule.getRuleVariables())
			{
				Expression valueAssignment = null;

				switch (direction)
				{
					case FORWARD:
					case MAPPING:
						// DB: Set new UUIDs
						valueAssignment = SDGeneratorHelper.copyWithNewUuids( ruleVariable.getForwardCalculationExpression() );
//						valueAssignment = EcoreUtil.copy(ruleVariable.getForwardCalculationExpression());
						break;

					case REVERSE:
						// DB: Set new UUIDs
						valueAssignment = SDGeneratorHelper.copyWithNewUuids(ruleVariable.getReverseCalculationExpression());
//						valueAssignment = EcoreUtil.copy(ruleVariable.getReverseCalculationExpression());
						break;
				}

				CallAction variableDeclarationAction = SDGeneratorHelper.createVariableDeclarationAction(null, null,
						ruleVariable.getName(), ruleVariable.getClassifier(), valueAssignment);

				cae.getCallActions().add(variableDeclarationAction);
			}
		}
	}
}
