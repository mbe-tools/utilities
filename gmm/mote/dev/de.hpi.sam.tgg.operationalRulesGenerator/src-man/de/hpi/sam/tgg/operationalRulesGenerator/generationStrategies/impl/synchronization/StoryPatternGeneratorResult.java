package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.synchronization;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;
import de.hpi.sam.tgg.CorrespondenceLink;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.RuleElement;

/**
 * Objects of this class contain the result of
 * StoryPatternGenerator.generateStoryPattern();
 * 
 * @author Stephan
 * 
 */
public class StoryPatternGeneratorResult
{
	public List<AbstractStoryPatternObject>			storyPatternObjects;
	public List<AbstractStoryPatternLink>			storyPatternLinks;

	public List<ModelObject>						leftParentNodes							= new LinkedList<ModelObject>();
	public List<ModelObject>						leftCreatedNodes						= new LinkedList<ModelObject>();
	public List<ModelLink>							leftParentLinks							= new LinkedList<ModelLink>();
	public List<ModelLink>							leftCreatedLinks						= new LinkedList<ModelLink>();

	public List<ModelObject>						rightParentNodes						= new LinkedList<ModelObject>();
	public List<ModelObject>						rightCreatedNodes						= new LinkedList<ModelObject>();
	public List<ModelLink>							rightParentLinks						= new LinkedList<ModelLink>();
	public List<ModelLink>							rightCreatedLinks						= new LinkedList<ModelLink>();

	public List<CorrespondenceNode>					parentCorrespondenceNodes				= new LinkedList<CorrespondenceNode>();
	public List<CorrespondenceNode>					childCorrespondenceNodes				= new LinkedList<CorrespondenceNode>();
	public List<CorrespondenceLink>					parentCorrespondenceLinksToSource		= new LinkedList<CorrespondenceLink>();
	public List<CorrespondenceLink>					parentCorrespondenceLinksToTarget		= new LinkedList<CorrespondenceLink>();
	public List<CorrespondenceLink>					childCorrespondenceLinksToSource		= new LinkedList<CorrespondenceLink>();
	public List<CorrespondenceLink>					childCorrespondenceLinksToTarget		= new LinkedList<CorrespondenceLink>();

	public CorrespondenceNode						inputCorrespondenceNode;

	public Map<RuleElement, StoryPatternElement>	ruleElementsToStoryPatternElementsMap	= new HashMap<RuleElement, StoryPatternElement>();

	public StoryPatternObject						thisObject								= null;
	public StoryPatternObject						ruleSetObject							= null;
	public StoryPatternObject						transformationQueueObject				= null;
	public StoryPatternObject						sourceModelElementsAdapter				= null;
	public StoryPatternObject						targetModelElementsAdapter				= null;
}
