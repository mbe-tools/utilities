package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.synchronization;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.hpi.sam.mote.MotePackage;
import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.callActions.AbstractComparator;
import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsFactory;
import de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction;
import de.hpi.sam.storyDiagramEcore.callActions.CompareAction;
import de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction;
import de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction;
import de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction;
import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsFactory;
import de.hpi.sam.storyDiagramEcore.expressions.StringExpression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesFactory;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.ExternalReference;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.SdmFactory;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;
import de.hpi.sam.tgg.CorrespondenceElement;
import de.hpi.sam.tgg.CorrespondenceLink;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelLinkPositionConstraint;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TGGModifierEnumeration;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.RuleGenerationException;

public class SDGeneratorHelper {
	protected static final String	MODIFICATION_TAG		= "modificationTag";
	protected static final String	TRANSFORMATION_QUEUE	= "transformationQueue";
	protected static final String	RULE_SET				= "ruleSet";
	protected static final String	THIS					= "this";
	protected static final String	OCL_EXPRESSION_LANGUAGE	= "OCL";
	public static final Set<String> OCL_KEYWORDS = new HashSet<String>(Arrays.asList("import", "include", "library", "package", "endpackage",
			"context", "static", "def", "inv", "body", "post", "pre", "init", "derive"));
	public static String OCL_ESCAPE_CHAR = "_";

	public static StoryPatternGeneratorResult generateStoryPattern(final StoryPatternGeneratorOptions options, final TGGRule rule)
	throws RuleGenerationException {
		/*
		 * Analyze TGG rule
		 */
		final StoryPatternGeneratorResult result = analyzeRule(rule);

		/*
		 * Create modelElements
		 */
		createModelElements(options, result);

		/*
		 * Create this, ruleSet and transformationQueue objects
		 */
		createThisAndRuleSetAndTransformationQueueObjects(options, result);

		/*
		 * Create correspondence nodes
		 */
		createCorrespondenceNodes(options, result);

		/*
		 * Create sources and targets links from correspondence nodes to model
		 * elements
		 */
		createSourcesAndTargetsLinks(options, result);

		/*
		 * Create modelElements links from ruleSet to model elements
		 */
		createModelElementsLinks(options, result);

		/*
		 * Create uncoveredElements links from ruleSet to model elements
		 */
		createUncoveredElementsLinks(options, result);

		return result;
	}

	private static void createUncoveredElementsLinks(final StoryPatternGeneratorOptions options, final StoryPatternGeneratorResult result)
			throws RuleGenerationException
	{
		/*
		 * Create uncoveredSourceElements links
		 */
		if (options.createRuleSetToCreatedNodes_uncoveredSourceElementsLinks)
		{
			if (!options.createLeftCreatedNodes || !options.createRuleSetObject)
			{
				throw new RuleGenerationException(
						"createRuleSetToCreatedNodes_uncoveredSourceElementsLinks is true but createLeftCreatedNodes is "
								+ options.createLeftCreatedNodes + " and createRuleSetObject is " + options.createRuleSetObject + ".");
			}

			/*
			 * The uncoveredSourceElements link consists of a MapEntry that is
			 * connected to the ruleSet via the uncoveredSourceElements link.
			 * Its key is the model element.
			 */
			for (final ModelObject mo : result.leftCreatedNodes)
			{
				result.storyPatternLinks.add(createMapEntryStoryPatternLink(null, null,
						RulesPackage.eINSTANCE.getTGGRuleSet_UncoveredSourceModelElements(),
						options.ruleSetToCreatedNodes_uncoveredSourceElements_Modifier, result.ruleSetObject,
						(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(mo), null));
			}
		}

		if (options.createRuleSetToCreatedNodes_uncoveredTargetElementsLinks)
		{
			if (!options.createRightCreatedNodes || !options.createRuleSetObject)
			{
				throw new RuleGenerationException(
						"createRuleSetToCreatedNodes_uncoveredSourceElementsLinks is true but createRightCreatedNodes is "
								+ options.createRightCreatedNodes + " and createRuleSetObject is " + options.createRuleSetObject + ".");
			}

			/*
			 * The uncoveredTargetElements link consists of a MapEntry that is
			 * connected to the ruleSet via the uncoveredSourceElements link.
			 * Its key is the model element.
			 */
			for (final ModelObject mo : result.rightCreatedNodes)
			{
				result.storyPatternLinks.add(createMapEntryStoryPatternLink(null, null,
						RulesPackage.eINSTANCE.getTGGRuleSet_UncoveredTargetModelElements(),
						options.ruleSetToCreatedNodes_uncoveredTargetElements_Modifier, result.ruleSetObject,
						(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(mo), null));
			}
		}
	}

	/**
	 * Creates the sourceModelElements and targetModelElements links of the
	 * ruleSet.
	 * 
	 * @param options
	 * @param result
	 * @throws RuleGenerationException
	 */
	private static void createModelElementsLinks(final StoryPatternGeneratorOptions options, final StoryPatternGeneratorResult result)
			throws RuleGenerationException
	{
		/*
		 * Create source model elements links
		 */
		if (options.createRuleSetToCreatedNodes_sourceModelElementsLinks)
		{
			if (!options.createLeftCreatedNodes || !options.createRuleSetObject)
			{
				throw new RuleGenerationException("createRuleSet_sourceModelElementsLinks is true but createLeftCreatedNodes is "
						+ options.createLeftCreatedNodes + " and createRuleSetObject is " + options.createRuleSetObject + ".");
			}
			else
			{
				/*
				 * The sourceModelElements and targetModelElements associations
				 * are implemented as map with the model elements keys and their
				 * associated correspondence nodes as values. Therefore, a
				 * MapEntry object must be created that has a key link to the
				 * model element and a value link to the correspondence node.
				 * The sourceModelElements links connects the ruleSet with the
				 * MapEntry object.
				 */
				for (final CorrespondenceNode cn : result.childCorrespondenceNodes)
				{
					for (final CorrespondenceLink cl : cn.getOutgoingCorrespondenceLinks())
					{
						if (result.leftCreatedNodes.contains(cl.getTarget()))
						{
							/*
							 * Only create the value link if child
							 * correspondence nodes were created in the story
							 * pattern.
							 */
							if (options.createChildCorrespondenceNodes)
							{
								result.storyPatternLinks.add(createMapEntryStoryPatternLink(null, null,
										RulesPackage.eINSTANCE.getTGGRuleSet_SourceModelElements(),
										options.ruleSetToCreatedNodes_sourceModelElements_Modifier, result.ruleSetObject,
										(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getTarget()),
										(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn)));

							}
							else
							{
								result.storyPatternLinks.add(SDGeneratorHelper
										.createMapEntryStoryPatternLink(null, null, RulesPackage.eINSTANCE
												.getTGGRuleSet_SourceModelElements(),
												options.ruleSetToCreatedNodes_sourceModelElements_Modifier, result.ruleSetObject,
												(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl
														.getTarget()), null));
							}
						}
					}
				}
			}
		}

		/*
		 * Create target model elements links
		 */
		if (options.createRuleSetToCreatedNodes_targetModelElementsLinks)
		{
			if (!options.createRightCreatedNodes || !options.createRuleSetObject)
			{
				throw new RuleGenerationException("createRuleSet_targetModelElementsLinks is true but createRightCreatedNodes is "
						+ options.createRightCreatedNodes + " and createRuleSetObject is " + options.createRuleSetObject + ".");
			}
			else
			{
				/*
				 * The sourceModelElements and targetModelElements associations
				 * are implemented as map with the model elements keys and their
				 * associated correspondence nodes as values. Therefore, a
				 * MapEntry object must be created that has a key link to the
				 * model element and a value link to the correspondence node.
				 * The sourceModelElements links connects the ruleSet with the
				 * MapEntry object.
				 */
				for (final CorrespondenceNode cn : result.childCorrespondenceNodes)
				{
					for (final CorrespondenceLink cl : cn.getOutgoingCorrespondenceLinks())
					{
						if (result.rightCreatedNodes.contains(cl.getTarget()))
						{
							/*
							 * Only create the value link if child
							 * correspondence nodes were created in the story
							 * pattern.
							 */
							if (options.createChildCorrespondenceNodes)
							{
								result.storyPatternLinks.add(createMapEntryStoryPatternLink(null, null,
										RulesPackage.eINSTANCE.getTGGRuleSet_TargetModelElements(),
										options.ruleSetToCreatedNodes_targetModelElements_Modifier, result.ruleSetObject,
										(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getTarget()),
										(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn)));

							}
							else
							{
								result.storyPatternLinks.add(SDGeneratorHelper
										.createMapEntryStoryPatternLink(null, null, RulesPackage.eINSTANCE
												.getTGGRuleSet_TargetModelElements(),
												options.ruleSetToCreatedNodes_targetModelElements_Modifier, result.ruleSetObject,
												(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl
														.getTarget()), null));
							}

						}
					}
				}
			}
		}

		/*
		 * Create source model elements links to parent nodes
		 */
		if (options.createRuleSetToParentNodes_sourceModelElementsLinks)
		{
			if (!options.createLeftParentNodes || !options.createRuleSetObject)
			{
				throw new RuleGenerationException(
						"createRuleSetToParentNodes_sourceModelElementsLinks is true but createLeftParentNodes is "
								+ options.createLeftParentNodes + " and createRuleSetObject is " + options.createRuleSetObject + ".");
			}
			else
			{
				/*
				 * The sourceModelElements and targetModelElements associations
				 * are implemented as map with the model elements keys and their
				 * associated correspondence nodes as values. Therefore, a
				 * MapEntry object must be created that has a key link to the
				 * model element and a value link to the correspondence node.
				 * The sourceModelElements links connects the ruleSet with the
				 * MapEntry object.
				 */
				for (final CorrespondenceNode cn : result.parentCorrespondenceNodes)
				{
					for (final CorrespondenceLink cl : cn.getOutgoingCorrespondenceLinks())
					{
						if (result.leftParentNodes.contains(cl.getTarget()))
						{
							/*
							 * Only create the value link if child
							 * correspondence nodes were created in the story
							 * pattern.
							 */
							if (options.createParentCorrespondenceNodes || options.createInputCorrespondenceNode
									&& result.ruleElementsToStoryPatternElementsMap.get(cn) != null)
							{
								result.storyPatternLinks.add(createMapEntryStoryPatternLink(null, null,
										RulesPackage.eINSTANCE.getTGGRuleSet_SourceModelElements(),
										options.ruleSetToParentNodes_sourceModelElements_Modifier, result.ruleSetObject,
										(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getTarget()),
										(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn)));

							}
							else
							{
								result.storyPatternLinks.add(SDGeneratorHelper
										.createMapEntryStoryPatternLink(null, null, RulesPackage.eINSTANCE
												.getTGGRuleSet_SourceModelElements(),
												options.ruleSetToParentNodes_sourceModelElements_Modifier, result.ruleSetObject,
												(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl
														.getTarget()), null));
							}
						}
					}
				}
			}
		}

		/*
		 * Create target model elements links
		 */
		if (options.createRuleSetToParentNodes_targetModelElementsLinks)
		{
			if (!options.createRightParentNodes || !options.createRuleSetObject)
			{
				throw new RuleGenerationException(
						"createRuleSetToParentNodes_targetModelElementsLinks is true but createRightParentNodes is "
								+ options.createRightParentNodes + " and createRuleSetObject is " + options.createRuleSetObject + ".");
			}
			else
			{
				/*
				 * The sourceModelElements and targetModelElements associations
				 * are implemented as map with the model elements keys and their
				 * associated correspondence nodes as values. Therefore, a
				 * MapEntry object must be created that has a key link to the
				 * model element and a value link to the correspondence node.
				 * The sourceModelElements links connects the ruleSet with the
				 * MapEntry object.
				 */
				for (final CorrespondenceNode cn : result.parentCorrespondenceNodes)
				{
					for (final CorrespondenceLink cl : cn.getOutgoingCorrespondenceLinks())
					{
						if (result.rightParentNodes.contains(cl.getTarget()))
						{
							/*
							 * Only create the value link if child
							 * correspondence nodes were created in the story
							 * pattern.
							 */
							if (options.createParentCorrespondenceNodes || options.createInputCorrespondenceNode
									&& result.ruleElementsToStoryPatternElementsMap.get(cn) != null)
							{
								result.storyPatternLinks.add(createMapEntryStoryPatternLink(null, null,
										RulesPackage.eINSTANCE.getTGGRuleSet_TargetModelElements(),
										options.ruleSetToParentNodes_targetModelElements_Modifier, result.ruleSetObject,
										(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getTarget()),
										(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn)));

							}
							else
							{
								result.storyPatternLinks.add(SDGeneratorHelper
										.createMapEntryStoryPatternLink(null, null, RulesPackage.eINSTANCE
												.getTGGRuleSet_TargetModelElements(),
												options.ruleSetToParentNodes_targetModelElements_Modifier, result.ruleSetObject,
												(AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl
														.getTarget()), null));
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Creates the sources and targets links from the correspondence nodes to
	 * the model elements.
	 * 
	 * @param options
	 * @param result
	 * @throws RuleGenerationException
	 */
	private static void createSourcesAndTargetsLinks(final StoryPatternGeneratorOptions options, final StoryPatternGeneratorResult result)
			throws RuleGenerationException
	{
		/*
		 * Create sources links of child correspondence nodes
		 */
		if (options.createChildCorrespondenceNodesSourcesLinks)
		{
			if (!options.createChildCorrespondenceNodes || !options.createLeftCreatedNodes)
			{
				throw new RuleGenerationException(
						"createChildCorrespondenceNodesSourcesLinks is true but createChildCorrespondenceNodes is "
								+ options.createChildCorrespondenceNodes + " and createLeftCreatedNodes is "
								+ options.createLeftCreatedNodes + ".");
			}
			else
			{
				for (final CorrespondenceLink cl : result.childCorrespondenceLinksToSource)
				{
					final StoryPatternLink spl = createStoryPatternLink(
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getSource()),
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getTarget()),
							MotePackage.Literals.TGG_NODE__SOURCES, null, null, null, options.childCorrespondenceNodesModifier, null);

					result.storyPatternLinks.add(spl);

					result.ruleElementsToStoryPatternElementsMap.put(cl, spl);
				}
			}
		}

		/*
		 * Create targets links of child correspondence nodes
		 */
		if (options.createChildCorrespondenceNodesTargetsLinks)
		{
			if (!options.createChildCorrespondenceNodes || !options.createRightCreatedNodes)
			{
				throw new RuleGenerationException(
						"createChildCorrespondenceNodesTargetsLinks is true but createChildCorrespondenceNodes is "
								+ options.createChildCorrespondenceNodes + " and createRightCreatedNodes is "
								+ options.createRightCreatedNodes + ".");
			}
			else
			{
				for (final CorrespondenceLink cl : result.childCorrespondenceLinksToTarget)
				{
					final StoryPatternLink spl = createStoryPatternLink(
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getSource()),
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getTarget()),
							MotePackage.Literals.TGG_NODE__TARGETS, null, null, null, options.childCorrespondenceNodesModifier, null);

					result.storyPatternLinks.add(spl);

					result.ruleElementsToStoryPatternElementsMap.put(cl, spl);
				}
			}
		}

		/*
		 * Create sources links of parent correspondence nodes
		 */
		if (options.createParentCorrespondenceNodesSourcesLinks)
		{
			if (!options.createLeftParentNodes)
			{
				throw new RuleGenerationException("createParentCorrespondenceNodesSourcesLinks is true but createLeftParentNodes is false");
			}

			/*
			 * Create sources links of all parent correspondence node
			 */
			if (options.createParentCorrespondenceNodes)
			{
				for (final CorrespondenceLink cl : result.parentCorrespondenceLinksToSource)
				{
					final StoryPatternLink spl = createStoryPatternLink(
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getSource()),
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getTarget()),
							MotePackage.Literals.TGG_NODE__SOURCES, null, null, null, options.parentCorrespondenceNodesModifier, null);

					result.storyPatternLinks.add(spl);

					result.ruleElementsToStoryPatternElementsMap.put(cl, spl);
				}
			}

			/*
			 * Create only sources links of input correspondence node
			 */
			else if (options.createInputCorrespondenceNode)
			{
				for (final CorrespondenceLink cl : result.parentCorrespondenceLinksToSource)
				{
					if (cl.getSource() == result.inputCorrespondenceNode)
					{
						final StoryPatternLink spl = createStoryPatternLink(
								(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(result.inputCorrespondenceNode),
								(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getTarget()),
								MotePackage.Literals.TGG_NODE__SOURCES, null, null, null, options.parentCorrespondenceNodesModifier, null);

						result.storyPatternLinks.add(spl);

						result.ruleElementsToStoryPatternElementsMap.put(cl, spl);
					}
				}
			}
			else
			{
				throw new RuleGenerationException(
						"createParentCorrespondenceNodesSourcesLinks is true but createParentCorrespondenceNodes and createInputCorrespondenceNode are both false.");
			}
		}

		/*
		 * Create targets links of parent correspondence nodes
		 */
		if (options.createParentCorrespondenceNodesTargetsLinks)
		{
			if (!options.createRightParentNodes)
			{
				throw new RuleGenerationException("createParentCorrespondenceNodesTargetsLinks is true but createRightParentNodes is false");
			}

			/*
			 * Create targets links of all parent correspondence node
			 */
			if (options.createParentCorrespondenceNodes)
			{
				for (final CorrespondenceLink cl : result.parentCorrespondenceLinksToTarget)
				{
					final StoryPatternLink spl = createStoryPatternLink(
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getSource()),
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getTarget()),
							MotePackage.Literals.TGG_NODE__TARGETS, null, null, null, options.parentCorrespondenceNodesModifier, null);

					result.storyPatternLinks.add(spl);

					result.ruleElementsToStoryPatternElementsMap.put(cl, spl);
				}
			}

			/*
			 * Create only targets links of input correspondence node
			 */
			else if (options.createInputCorrespondenceNode)
			{
				for (final CorrespondenceLink cl : result.parentCorrespondenceLinksToTarget)
				{
					if (cl.getSource() == result.inputCorrespondenceNode)
					{
						final StoryPatternLink spl = createStoryPatternLink(
								(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(result.inputCorrespondenceNode),
								(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cl.getTarget()),
								MotePackage.Literals.TGG_NODE__TARGETS, null, null, null, options.parentCorrespondenceNodesModifier, null);

						result.storyPatternLinks.add(spl);

						result.ruleElementsToStoryPatternElementsMap.put(cl, spl);
					}
				}
			}
			else
			{
				throw new RuleGenerationException(
						"createParentCorrespondenceNodesTargetsLinks is true but createParentCorrespondenceNodes and createInputCorrespondenceNode are both false.");
			}
		}
	}
	
	private static GetPropertyValueAction getGetPropertyValueAction( final Expression p_expression ) {
		if ( p_expression instanceof CallActionExpression ) {
			final CallActionExpression call = (CallActionExpression) p_expression;
			
			if ( call.getCallActions().size() == 1 &&  call.getCallActions().get( 0 ) instanceof GetPropertyValueAction ) {
				return (GetPropertyValueAction) call.getCallActions().get( 0 );
			}
		}
		
		return null;
	}

	private static void createModelObjectAttAssignmentsFromConstraints( final Collection<Expression> p_constraints,
																		final StoryPatternObject p_spo ) {
		for ( final Expression constr : p_constraints ) {
			if ( p_spo.getModifier() == StoryPatternModifierEnumeration.CREATE ) {
				final AttributeAssignment attAssignment;
				
				if ( constr instanceof CallActionExpression ) {
					attAssignment = convertToAttributeAssignment( (CallActionExpression) constr );
				}
				else if ( constr instanceof StringExpression ) {
					attAssignment = convertToAttributeAssignment( (StringExpression) constr, p_spo.getClassifier() );
				}
				else {
					attAssignment = null;
				}

				if ( attAssignment == null ) {
//					final ActivityDiagram root = (ActivityDiagram) EcoreUtil.getRootContainer( p_spo );
//					final String ruleName = root.getActivities().get( 0 ).getName();
					System.out.println( "Unable to convert constraint " + constr + " on pattern object " + p_spo.getName() + "." );
						//+ " of rule " + ruleName + "." );
				}
				else {
					p_spo.getAttributeAssignments().add( attAssignment );
				}
			}
		}
	}
	
	private static AttributeAssignment convertToAttributeAssignment( final CallActionExpression p_callExpression ) {
		if ( p_callExpression.getCallActions().size() == 1 ) {
			final CallAction action = p_callExpression.getCallActions().get( 0 );
			
			if ( action instanceof CompareAction ) {
				final CompareAction compAction = (CompareAction) action;
				final GetPropertyValueAction getPropAction1 = getGetPropertyValueAction( compAction.getExpression1() );
				final GetPropertyValueAction getPropAction2 = getGetPropertyValueAction( compAction.getExpression2() );
				final EStructuralFeature property;
				final Expression valueExpression;
				
				if ( getPropAction1 != null && getPropAction2 == null ) {
					property = getPropAction1.getProperty();
					valueExpression = compAction.getExpression2();
				}
				else if ( getPropAction2 != null && getPropAction1 == null ) {
					property = getPropAction2.getProperty();
					valueExpression = compAction.getExpression1();
				}
				else {
					property = null;
					valueExpression = null;
				}
				
				if ( property != null ) {
					final AttributeAssignment assignment = SdmFactory.eINSTANCE.createAttributeAssignment();
					assignment.setEStructuralFeature( property );
					assignment.setAssignmentExpression( copyWithNewUuids( valueExpression ) );

					return assignment;
				}
			}
		}
		
		return null;
	}

	
	private static AttributeAssignment convertToAttributeAssignment( 	final StringExpression p_expression,
																		final EClassifier p_classifier ) {
		if ( p_classifier instanceof EClass && OCL_EXPRESSION_LANGUAGE.equals( p_expression.getExpressionLanguage() ) ) {
			final String exprText = p_expression.getExpressionString().trim();
			final String delimiter;
			
			if ( exprText.endsWith( ".oclIsUndefined()" ) ) {
				delimiter = "\\.";
			}
			else if ( exprText.contains( "=" ) ) {
				delimiter = "=";
			}
			else if ( exprText.startsWith( "not" ) ) {
				delimiter = " ";
			}
			else {
				delimiter = null;
			}
			
			final String featureName;
			final String valueExpr;
	
			if ( delimiter != null ) {
				final String[] elements = exprText.split( delimiter );
				
				if ( elements.length == 2 ) {
					if ( "not".equals( elements [ 0 ].trim() ) ) {
						featureName = elements [ 1 ].trim();
						valueExpr = "false";
					}
					else {
						featureName = elements [ 0 ].trim(); 
	
						final String val = elements [ 1 ].trim();
						valueExpr = "oclIsUndefined()".equals( val ) ? "null" : val;
					}
				}
				else {
					featureName = null;
					valueExpr = null;
				}
			}
			else {
				featureName = exprText;
				valueExpr =  "true";
			}
				
			if ( featureName != null ) {
				final EStructuralFeature feature = ( (EClass) p_classifier ).getEStructuralFeature( removeOclEscapeChar( featureName ) );
				
				if ( feature != null ) {
					final AttributeAssignment assignment = SdmFactory.eINSTANCE.createAttributeAssignment();
					assignment.setEStructuralFeature( feature );
					
					final StringExpression strExpr = ExpressionsFactory.eINSTANCE.createStringExpression();
					strExpr.setExpressionString( valueExpr );
					assignment.setAssignmentExpression( strExpr );

					return assignment;
				}
			}
		}
		
		return null;
	}

	private static LinkPositionConstraintEnumeration getLinkPositionConstraintEnum( final ModelLink p_modelLink ) {
		final ModelLinkPositionConstraint positionCst = p_modelLink.getLinkPositionConstraint();
		
		if ( positionCst == null ) {
			return null;
		}

		return positionCst.getConstraintType();
	}
	
	/**
	 * Creates the source and target model elements
	 * 
	 * @param options
	 * @param result
	 * @throws RuleGenerationException
	 */
	private static void createModelElements(final StoryPatternGeneratorOptions options, final StoryPatternGeneratorResult result)
			throws RuleGenerationException
	{
		/*
		 * Source parent nodes
		 */
		if (options.createLeftParentNodes)
		{
			for (final ModelObject mo : result.leftParentNodes)
			{
				final StoryPatternObject spo = createStoryPatternObject(mo.getName(), mo.getDescription(),
						mo.getClassifier(), options.leftParentNodesModifier, options.leftParentNodesBinding);

				result.ruleElementsToStoryPatternElementsMap.put(mo, spo);

				result.storyPatternObjects.add(spo);

				if (options.createLeftConstraints && !mo.getConstraintExpressions().isEmpty())
				{
					spo.getConstraints().addAll(copyAllWithNewUuids(mo.getConstraintExpressions()));
				}
			}
		}

		/*
		 * Source created nodes
		 */
		if (options.createLeftCreatedNodes)
		{
			for (final ModelObject mo : result.leftCreatedNodes)
			{
				final StoryPatternObject spo = createStoryPatternObject(mo.getName(), mo.getDescription(),
						mo.getClassifier(), options.leftCreatedNodesModifier, options.leftCreatedNodesBinding);

				result.ruleElementsToStoryPatternElementsMap.put(mo, spo);

				result.storyPatternObjects.add(spo);

				if (options.createLeftConstraints && !mo.getConstraintExpressions().isEmpty())
				{
					spo.getConstraints().addAll(copyAllWithNewUuids(mo.getConstraintExpressions()));
				}

				if (options.createLeftAttributeAssignments && !mo.getAttributeAssignments().isEmpty())
				{
					spo.getAttributeAssignments().addAll(copyAllWithNewUuids(mo.getAttributeAssignments()));
				}

				if (options.createLeftAttributeChecks)
				{
					for (final AttributeAssignment attributeAssignment : mo.getAttributeAssignments())
					{
						final CompareAction compareAction = CallActionsFactory.eINSTANCE.createCompareAction();
						final AbstractComparator comparator = attributeAssignment.getCustomComparator();
						
						if ( comparator != null ) {
							compareAction.setComparator( copyWithNewUuids( comparator ) );
						}

						compareAction.setClassifier(EcorePackage.eINSTANCE.getEBooleanObject());

						compareAction.setExpression1( createOCLStringExpression( escapeOclKeyword(attributeAssignment.getEStructuralFeature().getName())));
						// DB: Set new UUIDs
						//compareAction.setExpression2(EcoreUtil.copy(attributeAssignment.getAssignmentExpression()));
						compareAction.setExpression2(copyWithNewUuids(attributeAssignment.getAssignmentExpression()));

						final CallActionExpression callActionExpression = createCallActionExpression(null, null,
								compareAction);

						spo.getConstraints().add(callActionExpression);
					}
				}
				
				// DB: Fixing the problem of model objects being re-instantiated during repair structures
				if ( options.createLeftAttAssignForPropValueConst ) {
					createModelObjectAttAssignmentsFromConstraints( mo.getConstraintExpressions(), spo );
				}
			}
		}

		/*
		 * source parent links
		 */
		if (options.createLeftParentLinks)
		{
			if (!options.createLeftParentNodes)
			{
				throw new RuleGenerationException("createLeftParentLinks is true but createLeftParentNodes is false.");
			}
			else
			{
				for (final ModelLink ml : result.leftParentLinks) {
					final StoryPatternLink spl = createStoryPatternLink(
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(ml.getSource()),
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(ml.getTarget()), ml.getEReference(),
							ml.getEOppositeReference(), ml.getExternalReference(), getLinkPositionConstraintEnum( ml ), options.leftParentNodesModifier, null);

					result.storyPatternLinks.add(spl);

					result.ruleElementsToStoryPatternElementsMap.put(ml, spl);
				}
			}
		}

		/*
		 * source created links
		 */
		if (options.createLeftCreatedLinks)
		{
			if (!options.createLeftCreatedNodes)
			{
				throw new RuleGenerationException("createLeftCreatedLinks is true but createLeftCreatedNodes is false.");
			}
			else
			{
				for (final ModelLink ml : result.leftCreatedLinks)
				{
					/*
					 * If left parent nodes are not created, create only those
					 * links, that are not connected to left parent nodes.
					 */
					if (options.createLeftParentNodes || !options.createLeftParentNodes
							&& ml.getSource().getModifier() == TGGModifierEnumeration.CREATE
							&& ml.getTarget().getModifier() == TGGModifierEnumeration.CREATE)
					{
						final StoryPatternLink spl = createStoryPatternLink(
								(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(ml.getSource()),
								(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(ml.getTarget()), ml.getEReference(),
								ml.getEOppositeReference(), ml.getExternalReference(), getLinkPositionConstraintEnum( ml ), options.leftCreatedNodesModifier, null);

						result.storyPatternLinks.add(spl);

						result.ruleElementsToStoryPatternElementsMap.put(ml, spl);
					}
				}
			}
		}

		/*
		 * target parent nodes
		 */
		if (options.createRightParentNodes)
		{
			for (final ModelObject mo : result.rightParentNodes)
			{
				final StoryPatternObject spo = createStoryPatternObject(mo.getName(), mo.getDescription(),
						mo.getClassifier(), options.rightParentNodesModifier, options.rightParentNodesBinding);

				result.ruleElementsToStoryPatternElementsMap.put(mo, spo);

				result.storyPatternObjects.add(spo);

				if (options.createRightConstraints && !mo.getConstraintExpressions().isEmpty())
				{
					spo.getConstraints().addAll(copyAllWithNewUuids(mo.getConstraintExpressions()));
				}
			}
		}

		/*
		 * target created nodes
		 */
		if (options.createRightCreatedNodes)
		{
			for (final ModelObject mo : result.rightCreatedNodes)
			{
				final StoryPatternObject spo = createStoryPatternObject(mo.getName(), mo.getDescription(),
						mo.getClassifier(), options.rightCreatedNodesModifier, options.rightCreatedNodesBinding);

				result.ruleElementsToStoryPatternElementsMap.put(mo, spo);

				result.storyPatternObjects.add(spo);

				if (options.createRightConstraints && !mo.getConstraintExpressions().isEmpty())
				{
					spo.getConstraints().addAll(copyAllWithNewUuids(mo.getConstraintExpressions()));
				}

				if (options.createRightAttributeAssignments && !mo.getAttributeAssignments().isEmpty())
				{
					spo.getAttributeAssignments().addAll(copyAllWithNewUuids(mo.getAttributeAssignments()));
				}

				if (options.createRightAttributeChecks)
				{
					for (final AttributeAssignment attributeAssignment : mo.getAttributeAssignments())
					{
						final CompareAction compareAction = CallActionsFactory.eINSTANCE.createCompareAction();
						final AbstractComparator comparator = attributeAssignment.getCustomComparator();
						
						if ( comparator != null ) {
							compareAction.setComparator( copyWithNewUuids( comparator ) );
						}

						compareAction.setClassifier(EcorePackage.eINSTANCE.getEBooleanObject());

						compareAction.setExpression1(createOCLStringExpression( escapeOclKeyword(attributeAssignment.getEStructuralFeature().getName())));
						compareAction.setExpression2(copyWithNewUuids( attributeAssignment.getAssignmentExpression() ) );

						final CallActionExpression callActionExpression = createCallActionExpression(null, null,
								compareAction);

						spo.getConstraints().add(callActionExpression);
					}
				}
				
				// DB: Fixing the problem of model objects being re-instantiated during repair structures
				if ( options.createRightAttAssignForPropValueConst ) {
					createModelObjectAttAssignmentsFromConstraints( mo.getConstraintExpressions(), spo );
				}
			}
		}

		/*
		 * target parent links
		 */
		if (options.createRightParentLinks)
		{
			if (!options.createRightParentNodes)
			{
				throw new RuleGenerationException("createRightParentLinks is true but createRightParentNodes is false.");
			}
			else
			{
				for (final ModelLink ml : result.rightParentLinks)
				{
					final StoryPatternLink spl = createStoryPatternLink(
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(ml.getSource()),
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(ml.getTarget()), ml.getEReference(),
							ml.getEOppositeReference(), ml.getExternalReference(), getLinkPositionConstraintEnum( ml ), options.rightParentNodesModifier, null);

					result.storyPatternLinks.add(spl);

					result.ruleElementsToStoryPatternElementsMap.put(ml, spl);
				}
			}
		}

		/*
		 * target created links
		 */
		if (options.createRightCreatedLinks)
		{
			if (!options.createRightCreatedNodes)
			{
				throw new RuleGenerationException("createRightCreatedLinks is true but createRightCreatedNodes is false.");
			}
			else
			{
				for (final ModelLink ml : result.rightCreatedLinks)
				{
					/*
					 * If left parent nodes are not created, create only those
					 * links, that are not connected to left parent nodes.
					 */
					if (options.createRightParentNodes || !options.createRightParentNodes
							&& ml.getSource().getModifier() == TGGModifierEnumeration.CREATE
							&& ml.getTarget().getModifier() == TGGModifierEnumeration.CREATE)
					{
						final StoryPatternLink spl = createStoryPatternLink(
								(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(ml.getSource()),
								(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(ml.getTarget()), ml.getEReference(),
								ml.getEOppositeReference(), ml.getExternalReference(), getLinkPositionConstraintEnum( ml ), options.rightCreatedNodesModifier, null);

						result.storyPatternLinks.add(spl);

						result.ruleElementsToStoryPatternElementsMap.put(ml, spl);
					}
				}
			}
		}
	}

	private static String removeOclEscapeChar( final String p_text ) {
		if ( p_text.startsWith( OCL_ESCAPE_CHAR ) ) {
			return p_text.substring( 1 );
		}
		
		return p_text;
	}
	
	private static String escapeOclKeyword( final String p_text ) {
		if ( OCL_KEYWORDS.contains( p_text ) ) {
			return OCL_ESCAPE_CHAR + p_text;
		}

		return p_text;
	}

	/**
	 * Creates the correspondence nodes and links connected to them.
	 * 
	 * @param options
	 * @param result
	 * @throws RuleGenerationException
	 */
	private static void createCorrespondenceNodes(final StoryPatternGeneratorOptions options, final StoryPatternGeneratorResult result)
			throws RuleGenerationException
	{
		/*
		 * Create parent correspondence nodes
		 */
		if (options.createParentCorrespondenceNodes)
		{
			for (final CorrespondenceNode cn : result.parentCorrespondenceNodes)
			{
				final StoryPatternObject spo = createStoryPatternObject(cn.getName(), cn.getDescription(),
						cn.getClassifier(), options.parentCorrespondenceNodesModifier, options.parentCorrespondenceNodesBinding);

				result.ruleElementsToStoryPatternElementsMap.put(cn, spo);

				result.storyPatternObjects.add(spo);
			}
		}

		/*
		 * Create input correspondence node if other parent correspondence nodes
		 * are not created.
		 */
		if (options.createInputCorrespondenceNode && !options.createParentCorrespondenceNodes)
		{
			if (result.inputCorrespondenceNode == null)
			{
				throw new RuleGenerationException("An input correspondence node could not be found.");
			}
			else
			{
				final StoryPatternObject spo = createStoryPatternObject(result.inputCorrespondenceNode.getName(),
						result.inputCorrespondenceNode.getDescription(), result.inputCorrespondenceNode.getClassifier(),
						options.parentCorrespondenceNodesModifier, options.parentCorrespondenceNodesBinding);

				result.ruleElementsToStoryPatternElementsMap.put(result.inputCorrespondenceNode, spo);

				result.storyPatternObjects.add(spo);

			}
		}

		/*
		 * Create assignment expression
		 */
		if (options.assignInputCorrespondenceNode)
		{
			if (!options.createInputCorrespondenceNode && !options.createParentCorrespondenceNodes)
			{
				throw new RuleGenerationException(
						"assignInputCorrespondenceNode is true but createInputCorrespondenceNode and createParentCorrespondenceNodes are both false.");
			}
			else if (options.inputCorrespondenceNodeAssignmentExpression == null
					|| "".equals(options.inputCorrespondenceNodeAssignmentExpression))
			{
				throw new RuleGenerationException(
						"assignInputCorrespondenceNode is true but inputCorrespondenceNodeAssignmentExpression is empty.");
			}
			else
			{
				final StoryPatternObject spo = (StoryPatternObject) result.ruleElementsToStoryPatternElementsMap
						.get(result.inputCorrespondenceNode);

				/*
				 * A story pattern object with a direct assignment is always
				 * bound.
				 */
				spo.setBindingType(BindingTypeEnumeration.BOUND);

				spo.setDirectAssignmentExpression(options.inputCorrespondenceNodeAssignmentExpression);
			}
		}

		/*
		 * create child correspondence nodes
		 */
		if (options.createChildCorrespondenceNodes)
		{
			for (final CorrespondenceNode cn : result.childCorrespondenceNodes)
			{
				final StoryPatternObject spo = createStoryPatternObject(cn.getName(), cn.getDescription(),
						cn.getClassifier(), options.childCorrespondenceNodesModifier, options.childCorrespondenceNodesBinding);

				result.ruleElementsToStoryPatternElementsMap.put(cn, spo);

				result.storyPatternObjects.add(spo);
			}
		}

		/*
		 * Create createdCorrNodes link from this to child correspondence nodes
		 */
		if (options.createThisToChildCorrespondenceNodes_createdCorrNodes_Link)
		{
			if (!options.createThisObject || !options.createChildCorrespondenceNodes)
			{
				throw new RuleGenerationException(
						"createThisToChildCorrespondenceNodes_createCorrNodes_Link is true but createThisObject is "
								+ options.createThisObject + " and createChildCorrespondenceNodes is "
								+ options.createChildCorrespondenceNodes);
			}
			else
			{
				for (final CorrespondenceNode cn : result.childCorrespondenceNodes)
				{
					result.storyPatternLinks.add(createStoryPatternLink(result.thisObject,
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn),
							RulesPackage.eINSTANCE.getTGGMapping_CreatedCorrNodes(), null, null, null, options.childCorrespondenceNodesModifier, null));
				}
			}
		}

		/*
		 * Create checkedCorrNodes link from ruleSet to child correspondence
		 * node
		 */
		// if
		// (options.createRuleSetToChildCorrespondenceNodes_checkedCorrNodes_Link)
		// {
		// if (!options.createRuleSetObject ||
		// !options.createChildCorrespondenceNodes) { throw new
		// RuleGenerationException(
		// "createRuleSetToChildCorrespondenceNodes_checkedCorrNodes_Link is true but createRuleSetObject is "
		// + options.createRuleSetObject +
		// " and createChildCorrespondenceNodes is "
		// + options.createChildCorrespondenceNodes); }
		//
		// for (CorrespondenceNode cn : result.childCorrespondenceNodes)
		// {
		// result.storyPatternLinks.add(createStoryPatternLink(result.ruleSetObject,
		// (StoryPatternObject)
		// result.ruleElementsToStoryPatternElementsMap.get(cn),
		// RulesPackage.eINSTANCE
		// .getTGGRuleSet_CheckedCorrNodes(),
		// options.checkedCorrNodes_LinkModifier, null));
		// }
		// }

		/*
		 * Create next links from parent to child correspondence nodes
		 */
		if (options.createParentCorrespondenceNodesToChildCorrespondenceNodes_next_Links)
		{
			if (!options.createChildCorrespondenceNodes || !options.createParentCorrespondenceNodes
					&& !options.createInputCorrespondenceNode)
			{
				throw new RuleGenerationException(
						"createParentCorrespondenceNodesToChildCorrespondenceNodes_next_Links is true but createChildCorrespondenceNodes is "
								+ options.createChildCorrespondenceNodes + ", createParentCorrespondenceNodes is "
								+ options.createParentCorrespondenceNodes + " and createInputCorrespondenceNode is "
								+ options.createInputCorrespondenceNode + ".");
			}
			else if (options.isAxiom)
			{
				throw new RuleGenerationException(
						"createParentCorrespondenceNodesToChildCorrespondenceNodes_next_Links is true but isAxiom is also true.");
			}
			else
			{
				if (options.createParentCorrespondenceNodes)
				{
					for (final CorrespondenceNode parentNode : result.parentCorrespondenceNodes)
					{
						for (final CorrespondenceNode childNode : result.childCorrespondenceNodes)
						{
							result.storyPatternLinks.add(createStoryPatternLink(
									(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(parentNode),
									(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(childNode),
									MotePackage.eINSTANCE.getTGGNode_Next(), null, null, null, options.childCorrespondenceNodesModifier, null));
						}
					}
				}
				else if (options.createInputCorrespondenceNode)
				{
					for (final CorrespondenceNode childNode : result.childCorrespondenceNodes)
					{
						result.storyPatternLinks.add(createStoryPatternLink(
								(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(result.inputCorrespondenceNode),
								(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(childNode),
								MotePackage.eINSTANCE.getTGGNode_Next(), null, null, null, options.childCorrespondenceNodesModifier, null));
					}

				}
			}
		}

		/*
		 * create input node links from child correspondence nodes to input
		 * correspondence node
		 */
		if (options.createChildCorrespondenceNodesToInputCorrespondenceNode_inputNode_Link)
		{
			// if (!options.createChildCorrespondenceNodes
			// || (!options.createParentCorrespondenceNodes &&
			// !options.createInputCorrespondenceNode))
			// {
			// throw new RuleGenerationException(
			// "createChildCorrespondenceNodesToInputCorrespondenceNode_inputNode_Link is true but createChildCorrespondenceNodes is "
			// + options.createChildCorrespondenceNodes +
			// ", createParentCorrespondenceNodes is "
			// + options.createParentCorrespondenceNodes +
			// " and createInputCorrespondenceNode is "
			// + options.createInputCorrespondenceNode + ".");
			// }
			// else if (options.isAxiom)
			// {
			// throw new RuleGenerationException(
			// "createChildCorrespondenceNodesToInputCorrespondenceNode_inputNode_Link is true but isAxiom is also true.");
			// }
			// else
			// {
			// StoryPatternObject inputNodeSpo = (StoryPatternObject)
			// result.ruleElementsToStoryPatternElementsMap
			// .get(result.inputCorrespondenceNode);
			//
			// for (CorrespondenceNode childNode :
			// result.childCorrespondenceNodes)
			// {
			// result.storyPatternLinks.add(createStoryPatternLink((StoryPatternObject)
			// result.ruleElementsToStoryPatternElementsMap
			// .get(childNode), inputNodeSpo,
			// MotePackage.eINSTANCE.getTGGNode_InputNode(),
			// options.childCorrespondenceNodesModifier, null));
			// }
			// }
		}

		/*
		 * Create ruleSet link from child correspondence nodes to ruleSet
		 */
		if (options.createChildCorrespondenceNodesToRuleSet_ruleSet_Links)
		{
			if (!options.createChildCorrespondenceNodes || !options.createRuleSetObject)
			{
				throw new RuleGenerationException(
						"createChildCorrespondenceNodesToRuleSet_ruleSet_Links is true but createChildCorrespondenceNodes is "
								+ options.createChildCorrespondenceNodes + " and createRuleSetObject is " + options.createRuleSetObject
								+ ".");
			}
			else
			{
				for (final CorrespondenceNode cn : result.childCorrespondenceNodes)
				{
					result.storyPatternLinks.add(createStoryPatternLink(
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn), result.ruleSetObject,
							MotePackage.eINSTANCE.getTGGNode_RuleSet(), null, null, null, options.childCorrespondenceNodesModifier, null));
				}
			}
		}

		/*
		 * Create correspondence nodes link from ruleSet to child correspondence
		 * nodes
		 */
		if (options.createRuleSetToChildCorrespondenceNodes_correspondenceNodes_Link)
		{
			if (!options.createChildCorrespondenceNodes || !options.createRuleSetObject)
			{
				throw new RuleGenerationException(
						"createRuleSetToChildCorrespondenceNodes_correspondenceNodes_Link is true but createChildCorrespondenceNodes is "
								+ options.createChildCorrespondenceNodes + " and createRuleSetObject is " + options.createRuleSetObject
								+ ".");
			}
			else
			{
				/*
				 * The correspondenceNodes association of the ruleSet is
				 * implemented as a map, that contains the correspondence nodes
				 * as keys. Conceptually, the map is a list of MapEntry objects.
				 * Each MapEntry has a key and a value link to a key and its
				 * corresponding value. Therefore, we need to create a MapEntry
				 * object, a link from the ruleSet to the MapEntry and a key
				 * link from the MapEntry to the correspondence node.
				 */
				for (final CorrespondenceNode cn : result.childCorrespondenceNodes)
				{
					result.storyPatternLinks.add(createMapEntryStoryPatternLink(null, null,
							RulesPackage.eINSTANCE.getTGGRuleSet_CorrespondenceNodes(), options.childCorrespondenceNodesModifier,
							result.ruleSetObject, (AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn), null));
				}
			}
		}

		/*
		 * Create correspondence nodes link from ruleSet to parent
		 * correspondence nodes
		 */
		if (options.createRuleSetToParentCorrespondenceNodes_correspondenceNodes_Link)
		{
			if (!options.createParentCorrespondenceNodes || !options.createRuleSetObject)
			{
				throw new RuleGenerationException(
						"createRuleSetToParentCorrespondenceNodes_correspondenceNodes_Link is true but createParentCorrespondenceNodes is "
								+ options.createParentCorrespondenceNodes + " and createRuleSetObject is " + options.createRuleSetObject
								+ ".");
			}
			else
			{
				/*
				 * The correspondenceNodes association of the ruleSet is
				 * implemented as a map, that contains the correspondence nodes
				 * as keys. Conceptually, the map is a list of MapEntry objects.
				 * Each MapEntry has a key and a value link to a key and its
				 * corresponding value. Therefore, we need to create a MapEntry
				 * object, a link from the ruleSet to the MapEntry and a key
				 * link from the MapEntry to the correspondence node.
				 */
				for (final CorrespondenceNode cn : result.parentCorrespondenceNodes)
				{
					result.storyPatternLinks.add(createMapEntryStoryPatternLink(null, null,
							RulesPackage.eINSTANCE.getTGGRuleSet_CorrespondenceNodes(), options.parentCorrespondenceNodesModifier,
							result.ruleSetObject, (AbstractStoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn), null));
				}
			}
		}

		/*
		 * Create root link from ruleSet to childCorrespondence node
		 */
		if (options.createRuleSetToChildCorrespondenceNode_root_Link)
		{
			if (!options.createChildCorrespondenceNodes || !options.createRuleSetObject)
			{
				throw new RuleGenerationException(
						"createRuleSetToChildCorrespondenceNode_root_Link is true but createChildCorrespondenceNodes is "
								+ options.createChildCorrespondenceNodes + " and createRuleSetObject is " + options.createRuleSetObject
								+ ".");
			}
			else
			{
				for (final CorrespondenceNode cn : result.childCorrespondenceNodes)
				{
					result.storyPatternLinks.add(createStoryPatternLink(result.ruleSetObject,
							(StoryPatternObject) result.ruleElementsToStoryPatternElementsMap.get(cn),
							RulesPackage.eINSTANCE.getTGGRuleSet_RootCorrNode(), null, null, null, options.childCorrespondenceNodesModifier, null));
				}
			}
		}

		/*
		 * Create modification tag for the first created correspondence node.
		 * Actually, there should be only one created correspondence node.
		 */
		// if (options.createModificationTag)
		// {
		// StoryPatternObject modificationTag =
		// createStoryPatternObject(MODIFICATION_TAG, null,
		// TagsPackage.eINSTANCE
		// .getModificationTag(), StoryPatternModifierEnumeration.CREATE,
		// BindingTypeEnumeration.UNBOUND);
		//
		// result.storyPatternObjects.add(modificationTag);
		//
		// StoryPatternLink spl = createStoryPatternLink(modificationTag,
		// (StoryPatternObject)
		// result.ruleElementsToStoryPatternElementsMap.get(result.childCorrespondenceNodes.get(0)),
		// TagsPackage.eINSTANCE.getModificationTag_CorrespondenceNode(),
		// StoryPatternModifierEnumeration.CREATE, null);
		//
		// result.storyPatternLinks.add(spl);
		// }
	}

	public static StringExpression createOCLStringExpression( final String p_expressionString ) {
		return createStringExpression( OCL_EXPRESSION_LANGUAGE, p_expressionString );
	}
	/**
	 * Creates a new constraint.
	 * 
	 * @param expressionLanguage
	 * @param expressionString
	 * @return
	 */
	public static StringExpression createStringExpression(final String expressionLanguage, final String expressionString)
	{
		final StringExpression stringExpression = ExpressionsFactory.eINSTANCE.createStringExpression();
		stringExpression.setExpressionLanguage(expressionLanguage);
		stringExpression.setExpressionString(expressionString);

		return stringExpression;
	}

	/**
	 * Creates a new CallActionExpression with one initial CallAction.
	 * 
	 * @param name
	 * @param description
	 * @param callAction
	 * @return
	 */
	public static CallActionExpression createCallActionExpression(final String name, final String description, final CallAction callAction)
	{
		final CallActionExpression callActionExpression = ExpressionsFactory.eINSTANCE.createCallActionExpression();
		callActionExpression.setName(name);
		callActionExpression.setDescription(description);

		if (callAction != null)
		{
			callActionExpression.getCallActions().add(callAction);
		}

		return callActionExpression;
	}

	/**
	 * Creates a new MethodCallAction without parameters.
	 * 
	 * @param name
	 * @param description
	 * @param instanceVariableExpression
	 * @param method
	 * @param methodClassName
	 * @param methodName
	 * @param classifier
	 * @return
	 */
	public static MethodCallAction createMethodCallAction(final String name, final String description,
			final Expression instanceVariableExpression, final EOperation method, final String methodClassName, final String methodName,
			final EClassifier classifier)
	{
		final MethodCallAction methodCallAction = CallActionsFactory.eINSTANCE.createMethodCallAction();
		methodCallAction.setName(name);
		methodCallAction.setDescription(description);
		methodCallAction.setInstanceVariable(instanceVariableExpression);
		methodCallAction.setMethod(method);
		methodCallAction.setMethodClassName(methodClassName);
		methodCallAction.setMethodName(methodName);
		methodCallAction.setClassifier(classifier);

		return methodCallAction;
	}

	/**
	 * Creates a new VariableReferenceAction.
	 * 
	 * @param name
	 * @param description
	 * @param variableName
	 * @param classifier
	 * @return
	 */
	public static VariableReferenceAction createVariableReferenceAction(final String name, final String description,
			final String variableName, final EClassifier classifier)
	{
		final VariableReferenceAction variableReferenceAction = CallActionsFactory.eINSTANCE.createVariableReferenceAction();
		variableReferenceAction.setName(name);
		variableReferenceAction.setDescription(description);
		variableReferenceAction.setVariableName(variableName);
		variableReferenceAction.setClassifier(classifier);

		return variableReferenceAction;
	}

	/**
	 * Creates a new VariableDeclarationAction.
	 * 
	 * @param name
	 * @param description
	 * @param variableName
	 * @param classifier
	 * @return
	 */
	public static VariableDeclarationAction createVariableDeclarationAction(final String name, final String description,
			final String variableName, final EClassifier classifier, final Expression valueAssignment)
	{
		final VariableDeclarationAction variableDeclarationAction = CallActionsFactory.eINSTANCE.createVariableDeclarationAction();
		variableDeclarationAction.setName(name);
		variableDeclarationAction.setDescription(description);
		variableDeclarationAction.setVariableName(variableName);
		variableDeclarationAction.setClassifier(classifier);

		variableDeclarationAction.setValueAssignment(valueAssignment);

		return variableDeclarationAction;
	}

	/**
	 * Creates a new LiteralDeclarationAction.
	 * 
	 * @param name
	 * @param description
	 * @param literal
	 * @param classifier
	 * @return
	 */
	public static LiteralDeclarationAction createLiteralDeclarationAction(final String name, final String description,
			final String literal, final EClassifier classifier)
	{
		final LiteralDeclarationAction literalDeclarationAction = CallActionsFactory.eINSTANCE.createLiteralDeclarationAction();
		literalDeclarationAction.setName(name);
		literalDeclarationAction.setDescription(description);
		literalDeclarationAction.setLiteral(literal);
		literalDeclarationAction.setClassifier(classifier);

		return literalDeclarationAction;
	}

	/**
	 * Creates a new CallActionParameter.
	 * 
	 * @param name
	 * @param description
	 * @param parameterClassifier
	 * @param parameterValueExpression
	 * @return
	 */
	public static CallActionParameter createCallActionParameter(final String name, final String description,
			final EClassifier parameterClassifier, final Expression parameterValueExpression)
	{
		final CallActionParameter callActionParameter = CallActionsFactory.eINSTANCE.createCallActionParameter();
		callActionParameter.setName(name);
		callActionParameter.setDescription(description);
		callActionParameter.setParameterClassfier(parameterClassifier);
		callActionParameter.setParameterValueAction(parameterValueExpression);

		return callActionParameter;
	}

	/**
	 * Creates the this, ruleSet and transformation queue objects and links
	 * between them.
	 * 
	 * @param options
	 * @param result
	 * @throws RuleGenerationException
	 */
	private static void createThisAndRuleSetAndTransformationQueueObjects(final StoryPatternGeneratorOptions options,
			final StoryPatternGeneratorResult result) throws RuleGenerationException
	{
		/*
		 * create this
		 */
		if (options.createThisObject)
		{
			if (options.thisEClass == null)
			{
				throw new RuleGenerationException("thisEClass is not set in the StoryPatternGeneratorOptions.");
			}

			result.thisObject = createStoryPatternObject(THIS, null, options.thisEClass,
					StoryPatternModifierEnumeration.NONE, BindingTypeEnumeration.BOUND);

			result.thisObject.setMatchType(StoryPatternMatchTypeEnumeration.THIS_OBJECT);

			result.storyPatternObjects.add(result.thisObject);
		}

		/*
		 * create ruleSet
		 */
		if (options.createRuleSetObject)
		{
			result.ruleSetObject = createStoryPatternObject(RULE_SET, null,
					RulesPackage.eINSTANCE.getTGGRuleSet(), StoryPatternModifierEnumeration.NONE, options.ruleSetObjectBinding);

			result.storyPatternObjects.add(result.ruleSetObject);
		}

		/*
		 * create transformation queue
		 */
		if (options.createTransformationQueueObject)
		{
			result.transformationQueueObject = createStoryPatternObject(TRANSFORMATION_QUEUE, null,
					RulesPackage.eINSTANCE.getTransformationQueue(), StoryPatternModifierEnumeration.NONE,
					options.transformationQueueBinding);

			result.storyPatternObjects.add(result.transformationQueueObject);
		}

		/*
		 * create ruleSet link from this to ruleSet
		 */
		if (options.createThisToRuleSet_ruleSet_Link)
		{
			if (!options.createThisObject || !options.createRuleSetObject)
			{
				throw new RuleGenerationException("createThisToRuleSetLinks is true but createThisObject is " + options.createThisObject
						+ " and createRuleSetObject is " + options.createRuleSetObject + ".");
			}
			else
			{
				if (options.isAxiom)
				{
					result.storyPatternLinks.add(createStoryPatternLink(result.thisObject, result.ruleSetObject,
							RulesPackage.eINSTANCE.getTGGAxiom_RuleSet(), null, null, null, StoryPatternModifierEnumeration.NONE, null));
				}
				else
				{
					result.storyPatternLinks.add(createStoryPatternLink(result.thisObject, result.ruleSetObject,
							RulesPackage.eINSTANCE.getTGGRule_RuleSet(), null, null, null, StoryPatternModifierEnumeration.NONE, null));
				}
			}
		}

		/*
		 * create transformationQueue link from ruleSet to transformation queue
		 */
		if (options.createRuleSetToTransformationQueueLink)
		{
			if (!options.createRuleSetObject || !options.createTransformationQueueObject)
			{
				throw new RuleGenerationException("createRuleSetToTransformationQueueLink is true but createRuleSetObject is "
						+ options.createRuleSetObject + " and createTransformationQueueObject is "
						+ options.createTransformationQueueObject + ".");
			}
			else
			{
				result.storyPatternLinks.add(createStoryPatternLink(result.ruleSetObject,
						result.transformationQueueObject, RulesPackage.eINSTANCE.getTGGRuleSet_TransformationQueue(),
						null, null, null, StoryPatternModifierEnumeration.NONE, null));
			}
		}
	}

	private static StoryPatternGeneratorResult analyzeRule(final TGGRule rule)
	{
		final StoryPatternGeneratorResult result = new StoryPatternGeneratorResult();

		/*
		 * Create lists for StoryPatternObjects and StoryPatternLinks
		 */
		result.storyPatternLinks = new LinkedList<AbstractStoryPatternLink>();
		result.storyPatternObjects = new LinkedList<AbstractStoryPatternObject>();

		/*
		 * Analyze source domain
		 */
		for (final ModelElement me : rule.getSourceDomain().getModelElements())
		{
			if (me instanceof ModelObject)
			{
				if (me.getModifier() == TGGModifierEnumeration.NONE)
				{
					result.leftParentNodes.add((ModelObject) me);
				}
				else if (me.getModifier() == TGGModifierEnumeration.CREATE)
				{
					result.leftCreatedNodes.add((ModelObject) me);
				}
			}
			else if (me instanceof ModelLink)
			{
				if (me.getModifier() == TGGModifierEnumeration.NONE)
				{
					result.leftParentLinks.add((ModelLink) me);
				}
				else if (me.getModifier() == TGGModifierEnumeration.CREATE)
				{
					result.leftCreatedLinks.add((ModelLink) me);
				}
			}
		}

		/*
		 * Analyze target domain
		 */
		for (final ModelElement me : rule.getTargetDomain().getModelElements())
		{
			if (me instanceof ModelObject)
			{
				if (me.getModifier() == TGGModifierEnumeration.NONE)
				{
					result.rightParentNodes.add((ModelObject) me);
				}
				else if (me.getModifier() == TGGModifierEnumeration.CREATE)
				{
					result.rightCreatedNodes.add((ModelObject) me);
				}
			}
			else if (me instanceof ModelLink)
			{
				if (me.getModifier() == TGGModifierEnumeration.NONE)
				{
					result.rightParentLinks.add((ModelLink) me);
				}
				else if (me.getModifier() == TGGModifierEnumeration.CREATE)
				{
					result.rightCreatedLinks.add((ModelLink) me);
				}
			}
		}

		/*
		 * Analyze correspondence domain
		 */
		for (final CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
		{
			if (ce instanceof CorrespondenceNode)
			{
				if (ce.getModifier() == TGGModifierEnumeration.NONE)
				{
					result.parentCorrespondenceNodes.add((CorrespondenceNode) ce);
				}
				else if (ce.getModifier() == TGGModifierEnumeration.CREATE)
				{
					result.childCorrespondenceNodes.add((CorrespondenceNode) ce);
				}
			}
			else if (ce instanceof CorrespondenceLink)
			{
				final CorrespondenceLink cl = (CorrespondenceLink) ce;

				if (cl.getModifier() == TGGModifierEnumeration.NONE)
				{
					if (rule.getSourceDomain().getModelElements().contains(cl.getTarget()))
					{
						result.parentCorrespondenceLinksToSource.add(cl);
					}
					else if (rule.getTargetDomain().getModelElements().contains(cl.getTarget()))
					{
						result.parentCorrespondenceLinksToTarget.add(cl);
					}
				}
				else if (cl.getModifier() == TGGModifierEnumeration.CREATE)
				{
					if (rule.getSourceDomain().getModelElements().contains(cl.getTarget()))
					{
						result.childCorrespondenceLinksToSource.add(cl);
					}
					else if (rule.getTargetDomain().getModelElements().contains(cl.getTarget()))
					{
						result.childCorrespondenceLinksToTarget.add(cl);
					}
				}
			}
		}

		if (!rule.getInputElements().isEmpty() && rule.getInputElements().get(0) instanceof CorrespondenceNode)
		{
			result.inputCorrespondenceNode = (CorrespondenceNode) rule.getInputElements().get(0);
		}

		return result;
	}

	/**
	 * Creates a story pattern object with the specified properties.
	 * 
	 * @param name
	 * @param description
	 * @param classifier
	 * @param modifier
	 * @param bound
	 * @return
	 */
	public static StoryPatternObject createStoryPatternObject(final String name, final String description, final EClassifier classifier,
			final StoryPatternModifierEnumeration modifier, final BindingTypeEnumeration bound)
	{
		final StoryPatternObject spo = SdmFactory.eINSTANCE.createStoryPatternObject();
		spo.setName(name);
		spo.setDescription(description);
		spo.setClassifier(classifier);
		spo.setModifier(modifier);
		spo.setBindingType(bound);

		return spo;
	}

	public static MapEntryStoryPatternLink createMapEntryStoryPatternLink(final String name, final String description,
			final EStructuralFeature feature, final StoryPatternModifierEnumeration modifier, final AbstractStoryPatternObject source,
			final AbstractStoryPatternObject keyTarget, final AbstractStoryPatternObject valueTarget)
	{
		final MapEntryStoryPatternLink spl = SdmFactory.eINSTANCE.createMapEntryStoryPatternLink();

		spl.setName(name);
		spl.setDescription(description);
		spl.setClassifier(HelpersPackage.eINSTANCE.getMapEntry());
		spl.setEStructuralFeature(feature);
		spl.setModifier(modifier);

		spl.setSource(source);
		spl.setTarget(keyTarget);
		spl.setValueTarget(valueTarget);

		return spl;
	}

	/**
	 * Creates a story pattern link that connects source and target. If the name
	 * is null or empty, the name of the link is constructed like this:
	 * "source.name_eStructuralFeature.name_target.name".
	 * 
	 * @param source
	 * @param target
	 * @param eStructuralFeature
	 * @param modifier
	 * @return
	 */
	public static StoryPatternLink createStoryPatternLink(	final AbstractStoryPatternObject source, 
															final AbstractStoryPatternObject target,
															final EStructuralFeature eStructuralFeature, 
															final EReference p_eOppositeReference,
															final ExternalReference p_extReference,
															final LinkPositionConstraintEnumeration p_positionConstraint, // DB
															final StoryPatternModifierEnumeration modifier, 
															final String name )
	{
		final StoryPatternLink spl = SdmFactory.eINSTANCE.createStoryPatternLink();
		spl.setSource(source);
		spl.setTarget(target);
		spl.setEStructuralFeature(eStructuralFeature);
		spl.setEOppositeReference( p_eOppositeReference );
		
		// DB: Handle link position constraints
		if ( p_positionConstraint != null ) {
			final LinkPositionConstraint sdeLinkPosConstraint = SdmFactory.eINSTANCE.createLinkPositionConstraint();
			sdeLinkPosConstraint.setConstraintType( p_positionConstraint );
			spl.setLinkPositionConstraint( sdeLinkPosConstraint );
		}
		
		// DB: Handle external references
		if ( p_extReference != null ) {
//			final MethodCallAction methodCallAction = createMethodCallAction( 	p_extReference.getName(),
//																				p_extReference.getDescription(),
//																				null,
//																				null, 
//																				p_extReference.getMethodClassName(),
//																				p_extReference.getMethodName(), 
//																				p_extReference.getClassifier() );
//			final StringExpression paramExpr = createOCLStringExpression( source.getName() );
//			final CallActionParameter parameter = createCallActionParameter( 	source.getName(),
//																				null,
//																				source.getClassifier(), 
//																				paramExpr );
//			methodCallAction.getParameters().add( parameter );
//
//			final CallActionExpression expr = createCallActionExpression( 	p_extReference.getName(),
//																			p_extReference.getDescription(), 
//																			methodCallAction );
			spl.setExternalReference( copyWithNewUuids( p_extReference ) );
		}

		if (name == null || "".equals(name))
		{
			final String featureName = p_extReference == null ? eStructuralFeature.getName() : p_extReference.getName();
			
			spl.setName(source.getName() + "->" + featureName + " to " + target.getName());
		}
		else
		{
			spl.setName(name);
		}
		spl.setModifier(modifier);

		return spl;
	}

	/**
	 * Creates an activity edge that connects the two ActivityNodes and contains
	 * an ActivityEdgeGuard with the specified guard.
	 * 
	 * @param source
	 * @param target
	 * @param guard
	 * @return
	 */
	public static ActivityEdge createActivityEdge(final ActivityNode source, final ActivityNode target,
			final ActivityEdgeGuardEnumeration guard, final Expression activityEdgeGuardExpression)
	{
		final ActivityEdge edge = NodesFactory.eINSTANCE.createActivityEdge();
		edge.setSource(source);
		edge.setTarget(target);
		edge.setName(source.getName() + " -> " + target.getName());
		edge.setGuardType(guard);
		edge.setGuardExpression(activityEdgeGuardExpression);

		return edge;
	}

	public static String firstLetterToUpperCase(final String s)
	{
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

	public static boolean isAxiom(final TGGRule rule)
	{
		for (final CorrespondenceElement ce : rule.getCorrespondenceDomain().getCorrespondenceElements())
		{
			if (ce.getModifier() != TGGModifierEnumeration.CREATE)
			{
				return false;
			}
		}

		for (final ModelElement me : rule.getSourceDomain().getModelElements())
		{
			if (me.getModifier() != TGGModifierEnumeration.CREATE)
			{
				return false;
			}
		}

		for (final ModelElement me : rule.getTargetDomain().getModelElements())
		{
			if (me.getModifier() != TGGModifierEnumeration.CREATE)
			{
				return false;
			}
		}

		return true;
	}

	public static CallStoryDiagramInterpreterAction createCallSDIAction(final String name, final String description,
			final Expression thisObjectExpression, final EClassifier thisClassifier, final EClassifier classifier, final Activity activity)
	{
		final CallStoryDiagramInterpreterAction callSDIAction = CallActionsFactory.eINSTANCE.createCallStoryDiagramInterpreterAction();
		callSDIAction.setName(name);
		callSDIAction.setDescription(description);
		callSDIAction.setClassifier(classifier);
		callSDIAction.setActivity(activity);

		final CallActionParameter callActionParameter = createCallActionParameter(THIS, null,
				thisClassifier, thisObjectExpression);

		callSDIAction.getParameters().add(callActionParameter);

		return callSDIAction;
	}

	public static <T extends EObject> T copyWithNewUuids( final T p_eObject ) {
		final T copy = EcoreUtil.copy( p_eObject );
		setAllUuids( copy );
		
		return copy;
	}

	public static <T extends EObject> Collection<T> copyAllWithNewUuids( final Collection<T> p_eObjects ) {
		final Collection<T> copies = EcoreUtil.copyAll( p_eObjects );
		
		for( final T element : copies ) {
			setAllUuids( element );
		}
		
		return copies;
	}
	
	private static void setAllUuids( final EObject p_eObject ) {
		if ( p_eObject instanceof NamedElement ) {
			( (NamedElement) p_eObject ).setUuid( EcoreUtil.generateUUID() );
		}

		final Iterator<EObject> iterator = p_eObject.eAllContents();
		
		while ( iterator.hasNext() ) {
			final EObject element = iterator.next();
			
			if ( element instanceof NamedElement ) {
				( (NamedElement) element ).setUuid( EcoreUtil.generateUUID() );
			}
		}
	}
}
