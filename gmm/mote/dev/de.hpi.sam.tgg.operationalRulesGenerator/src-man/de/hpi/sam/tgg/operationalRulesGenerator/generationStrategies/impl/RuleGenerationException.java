package de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl;

public class RuleGenerationException extends Exception
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

//	private String				message;
//	private Exception			innerException;

	public RuleGenerationException(String message)
	{
		super( message );
	}

	public RuleGenerationException(String message, Exception innerException)
	{
		super( message, innerException );
	}
//
//	@Override
//	public String toString()
//	{
//		if (innerException == null)
//		{
//			return message;
//		}
//		else
//		{
//			return message + "\nInnerException:\n" + innerException.toString();
//		}
//	}
//
//	public Exception getInnerException()
//	{
//		return innerException;
//	}
}
