package de.hpi.sam.tgg.operationalRulesGenerator.jet;

 public class InitInputCorrNodeTypesOperationTemplate
 {
  protected static String nl;
  public static synchronized InitInputCorrNodeTypesOperationTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    InitInputCorrNodeTypesOperationTemplate result = new InitInputCorrNodeTypesOperationTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "inputCorrNodeTypes.add( ";
  protected final String TEXT_3 = ".";
  protected final String TEXT_4 = ".class );";
  protected final String TEXT_5 = NL;

	public String generate(String basePackage, de.hpi.sam.tgg.TGGRule tggRule)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    final java.util.Set<org.eclipse.emf.ecore.EClassifier> types = new java.util.HashSet<org.eclipse.emf.ecore.EClassifier>();
for (de.hpi.sam.tgg.CorrespondenceElement ce : tggRule.getCorrespondenceDomain().getCorrespondenceElements())
{
	if (ce instanceof de.hpi.sam.tgg.CorrespondenceNode) {
		de.hpi.sam.tgg.CorrespondenceNode cn = (de.hpi.sam.tgg.CorrespondenceNode) ce;
		if (cn.getModifier() == de.hpi.sam.tgg.TGGModifierEnumeration.NONE)	{
			if ( !types.contains( cn.getClassifier() ) ) {
				types.add( cn.getClassifier() );
    stringBuffer.append(TEXT_2);
    stringBuffer.append(basePackage);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cn.getClassifier().getName());
    stringBuffer.append(TEXT_4);
    
			}
		}
	}
}
    stringBuffer.append(TEXT_5);
    return stringBuffer.toString();
  }
}
