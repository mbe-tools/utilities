package de.hpi.sam.tgg.operationalRulesGenerator.jet;

 public class RuleCallStoryDiagramTemplate
 {
  protected static String nl;
  public static synchronized RuleCallStoryDiagramTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    RuleCallStoryDiagramTemplate result = new RuleCallStoryDiagramTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t//System.out.println( new java.util.Date() + \": Starting to execute rule \" + getClass().getSimpleName() + \" on parent element \" + ";
  protected final String TEXT_2 = " );" + NL + "\torg.eclipse.emf.ecore.resource.Resource r = ((";
  protected final String TEXT_3 = ".";
  protected final String TEXT_4 = ") this.getRuleSet()).getResourceSet().getResource(" + NL + "\t\t\t\torg.eclipse.emf.common.util.URI.createPlatformPluginURI(\"/";
  protected final String TEXT_5 = "/model/story/";
  protected final String TEXT_6 = ".story\", true), true);" + NL + "\t\t" + NL + "\t\tde.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);" + NL + "\t\t" + NL + "\t\tde.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); " + NL + "\t\t" + NL + "\t\tde.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((";
  protected final String TEXT_7 = ") this.getRuleSet()).getSdmInterpreter();" + NL + "\t\t//final de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver receiver = new de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver( null, (de.mdelab.sdm.interpreter.sde.facade.SDEMetamodelFacadeFactory) sdi.getFacadeFactory() );" + NL + "\t\t//sdi.getNotificationEmitter().addNotificationReceiver( receiver );" + NL + "\t\t" + NL + "\t\tjava.util.List<Object> parameters = new java.util.ArrayList<Object>();" + NL + "\t\t";
  protected final String TEXT_8 = "\t\t" + NL + "\t\t\tparameters.add(";
  protected final String TEXT_9 = ");";
  protected final String TEXT_10 = NL + "\t\t" + NL + "\t\ttry {" + NL + "\t\t\tfinal de.hpi.sam.mote.rules.TransformationResult result = (de.hpi.sam.mote.rules.TransformationResult) sdi.executeActivity(a, this, parameters);" + NL + "\t\t\t//System.out.println( new java.util.Date() + \": Ended executing rule \" + getClass().getSimpleName() + \" with result \" + result.getName() + \".\" );" + NL + "\t\t\t" + NL + "\t\t\treturn result;" + NL + "\t\t}" + NL + "\t\tcatch (de.mdelab.sdm.interpreter.core.SDMException e) {" + NL + "\t\t\te.printStackTrace();" + NL + "\t\t\tthrow new de.hpi.sam.mote.impl.TransformationException(\"Error during execution of rule '";
  protected final String TEXT_11 = "'.\", e);" + NL + "\t\t}" + NL + "\t\t//finally {" + NL + "\t\t\t//sdi.getNotificationEmitter().removeNotificationReceiver( receiver );" + NL + "\t\t//}" + NL + "\t\t";

	public String generate(String projectName, String ruleSetName, String activityDiagramName, org.eclipse.emf.ecore.EOperation eOperation, String paramName)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(paramName);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(projectName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(ruleSetName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(projectName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(activityDiagramName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(projectName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(ruleSetName);
    stringBuffer.append(TEXT_7);
    		for (org.eclipse.emf.ecore.EParameter parameter : eOperation.getEParameters())
		{
    stringBuffer.append(TEXT_8);
    stringBuffer.append(parameter.getName());
    stringBuffer.append(TEXT_9);
    		}
    stringBuffer.append(TEXT_10);
    stringBuffer.append(activityDiagramName);
    stringBuffer.append(TEXT_11);
    return stringBuffer.toString();
  }
}
