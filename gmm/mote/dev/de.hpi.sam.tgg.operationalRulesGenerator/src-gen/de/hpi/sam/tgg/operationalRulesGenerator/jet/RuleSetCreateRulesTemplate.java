package de.hpi.sam.tgg.operationalRulesGenerator.jet;

 public class RuleSetCreateRulesTemplate
 {
  protected static String nl;
  public static synchronized RuleSetCreateRulesTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    RuleSetCreateRulesTemplate result = new RuleSetCreateRulesTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "try {" + NL + "\tde.mdelab.sdm.interpreter.sde.notifications.SDENotificationEmitter notificationEmitter = new de.mdelab.sdm.interpreter.sde.notifications.SDENotificationEmitter();" + NL + "" + NL + "\tthis.setSdmInterpreter(new de.mdelab.sdm.interpreter.sde.eclipse.SDEEclipseSDMInterpreter(getClass().getClassLoader(), notificationEmitter));" + NL + "} catch (de.mdelab.sdm.interpreter.core.SDMException e) {" + NL + "\te.printStackTrace();" + NL + "\tthrow new RuntimeException(e);" + NL + "}" + NL + "" + NL + "java.lang.reflect.Method method;" + NL + "de.hpi.sam.mote.rules.ReferencePattern refPattern;" + NL + "org.eclipse.emf.ecore.EPackage sourcePackage;" + NL + "org.eclipse.emf.ecore.EPackage targetPackage;" + NL;
  protected final String TEXT_2 = NL + "\t";
  protected final String TEXT_3 = NL + "\t\tthis.setAxiom(";
  protected final String TEXT_4 = ".";
  protected final String TEXT_5 = "Factory.eINSTANCE.create";
  protected final String TEXT_6 = "());" + NL + "\t";
  protected final String TEXT_7 = NL + "\t\tgetRules().add(";
  protected final String TEXT_8 = "());\t" + NL + "\t";
  protected final String TEXT_9 = NL + "\t\ttry {" + NL + "\t\t\tmethod = ";
  protected final String TEXT_10 = ".class.getMethod( \"";
  protected final String TEXT_11 = "\", new Class<?>[] { ";
  protected final String TEXT_12 = ".class } );" + NL + "\t\t\t" + NL + "\t\t\tif ( !getExternalReferences().contains( method ) ) {" + NL + "\t\t\t\tgetExternalReferences().add( method );" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\tcatch ( final NoSuchMethodException p_ex ) {" + NL + "\t\t\tthrow new RuntimeException( p_ex );" + NL + "\t\t}" + NL + "\t\tcatch ( final SecurityException p_ex ) {" + NL + "\t\t\tthrow new RuntimeException( p_ex );" + NL + "\t\t}" + NL + "\t\t\t";
  protected final String TEXT_13 = NL + "\t\trefPattern = de.hpi.sam.mote.rules.RulesFactory.eINSTANCE.createReferencePattern();" + NL + "\t\tsourcePackage = org.eclipse.emf.ecore.EPackage.Registry.INSTANCE.getEPackage( \"";
  protected final String TEXT_14 = "\" );" + NL + "\t\trefPattern.setSourceClassifier( sourcePackage.getEClassifier( \"";
  protected final String TEXT_15 = "\" ) );" + NL + "\t\ttargetPackage = org.eclipse.emf.ecore.EPackage.Registry.INSTANCE.getEPackage( \"";
  protected final String TEXT_16 = "\" );" + NL + "\t\trefPattern.setTargetClassifier( targetPackage.getEClassifier( \"";
  protected final String TEXT_17 = "\" ) );" + NL + "\t\trefPattern.setReference( (org.eclipse.emf.ecore.EReference) ( (org.eclipse.emf.ecore.EClass) refPattern.getSourceClassifier() ).getEStructuralFeature( \"";
  protected final String TEXT_18 = "\" ) );" + NL + "\t\t" + NL + "\t\tif ( !containsPattern( refPattern ) ) {" + NL + "\t\t\tgetExternalRefPatterns().add( refPattern );" + NL + "\t\t}" + NL + "\t\t\t";

	public String generate(de.hpi.sam.tgg.TGGDiagram tggDiagram, org.eclipse.emf.ecore.EPackage rulesPackage, String basePackage)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    for (de.hpi.sam.tgg.TGGRule rule : tggDiagram.getTggRules()) {
    stringBuffer.append(TEXT_2);
    if (rule.isIsAxiom()) {
    stringBuffer.append(TEXT_3);
    stringBuffer.append(basePackage);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(rulesPackage.getName());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationHelpers.firstLetterToUpperCase(rulesPackage.getName()));
    stringBuffer.append(TEXT_5);
    stringBuffer.append(rule.getName());
    stringBuffer.append(TEXT_6);
     } else { 
    stringBuffer.append(TEXT_7);
    stringBuffer.append(basePackage);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(rulesPackage.getName());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(de.hpi.sam.tgg.operationalRulesGenerator.generationStrategies.impl.GenerationHelpers.firstLetterToUpperCase(rulesPackage.getName()));
    stringBuffer.append(TEXT_5);
    stringBuffer.append(rule.getName());
    stringBuffer.append(TEXT_8);
    }
	final java.util.Collection<de.hpi.sam.tgg.ModelElement> allModelElements = new java.util.ArrayList<de.hpi.sam.tgg.ModelElement>( rule.getSourceDomain().getModelElements() );
	allModelElements.addAll(  rule.getTargetDomain().getModelElements() );
	for ( final de.hpi.sam.tgg.ModelElement modelElement : allModelElements ) {
		if ( modelElement instanceof de.hpi.sam.tgg.ModelLink ) {
			final de.hpi.sam.tgg.ModelLink modelLink = (de.hpi.sam.tgg.ModelLink) modelElement;
			final de.hpi.sam.storyDiagramEcore.sdm.ExternalReference extRef = modelLink.getExternalReference();
			
			if ( extRef != null ) {
				final de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression callExpr = (de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression) extRef.getExpression();
				final de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction methCallAct = (de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction) callExpr.getCallActions().get( 0 );
				final de.hpi.sam.tgg.ModelObject source = modelLink.getSource();
				final org.eclipse.emf.ecore.EClassifier classifier = source.getClassifier();
    stringBuffer.append(TEXT_9);
    stringBuffer.append(methCallAct.getMethodClassName());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(methCallAct.getMethodName());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(classifier.getInstanceClass().getName());
    stringBuffer.append(TEXT_12);
    }
			
			if ( modelLink.isExternal() ) {
    stringBuffer.append(TEXT_13);
    stringBuffer.append(modelLink.getSource().getClassifier().getEPackage().getNsURI());
    stringBuffer.append(TEXT_14);
    stringBuffer.append(modelLink.getSource().getClassifier().getName());
    stringBuffer.append(TEXT_15);
    stringBuffer.append(modelLink.getTarget().getClassifier().getEPackage().getNsURI());
    stringBuffer.append(TEXT_16);
    stringBuffer.append(modelLink.getTarget().getClassifier().getName());
    stringBuffer.append(TEXT_17);
    stringBuffer.append(modelLink.getEReference().getName());
    stringBuffer.append(TEXT_18);
    }
		}
	}
}
    return stringBuffer.toString();
  }
}
