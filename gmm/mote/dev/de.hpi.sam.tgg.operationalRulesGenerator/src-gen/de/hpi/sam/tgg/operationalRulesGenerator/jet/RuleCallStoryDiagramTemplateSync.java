package de.hpi.sam.tgg.operationalRulesGenerator.jet;

 public class RuleCallStoryDiagramTemplateSync
 {
  protected static String nl;
  public static synchronized RuleCallStoryDiagramTemplateSync create(String lineSeparator)
  {
    nl = lineSeparator;
    RuleCallStoryDiagramTemplateSync result = new RuleCallStoryDiagramTemplateSync();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "org.eclipse.emf.ecore.resource.Resource r = ((";
  protected final String TEXT_2 = ".";
  protected final String TEXT_3 = ") this.getRuleSet()).getResourceSet().getResource(" + NL + "\t\t\t\torg.eclipse.emf.common.util.URI.createPlatformPluginURI(\"/";
  protected final String TEXT_4 = "/model/story/";
  protected final String TEXT_5 = ".story\", true), true);" + NL + "\t\t" + NL + "\t\tde.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);" + NL + "\t\t" + NL + "\t\tde.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); " + NL + "\t\t" + NL + "\t\tde.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((";
  protected final String TEXT_6 = ") this.getRuleSet()).getSdmInterpreter();" + NL + "\t\t" + NL + "\t\tjava.util.List<Object> parameters = new java.util.ArrayList<Object>();" + NL + "\t\t";
  protected final String TEXT_7 = "\t\t" + NL + "\t\t\tparameters.add(";
  protected final String TEXT_8 = ");";
  protected final String TEXT_9 = NL + "\t\t" + NL + "\t\ttry {" + NL + "\t\t\treturn (Boolean) sdi.executeActivity(a, this, parameters);" + NL + "\t\t} catch (de.mdelab.sdm.interpreter.core.SDMException e) {" + NL + "\t\t\te.printStackTrace();" + NL + "\t\t\tthrow new de.hpi.sam.mote.impl.TransformationException(\"Error during execution of rule '";
  protected final String TEXT_10 = "'.\", e);" + NL + "\t\t}";

	public String generate(String projectName, String ruleSetName, String activityDiagramName, org.eclipse.emf.ecore.EOperation eOperation, String paramName)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(projectName);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(ruleSetName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(projectName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(activityDiagramName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(projectName);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(ruleSetName);
    stringBuffer.append(TEXT_6);
    		for (org.eclipse.emf.ecore.EParameter parameter : eOperation.getEParameters())
		{
    stringBuffer.append(TEXT_7);
    stringBuffer.append(parameter.getName());
    stringBuffer.append(TEXT_8);
    		}
    stringBuffer.append(TEXT_9);
    stringBuffer.append(activityDiagramName);
    stringBuffer.append(TEXT_10);
    return stringBuffer.toString();
  }
}
