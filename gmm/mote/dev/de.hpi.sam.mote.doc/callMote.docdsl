<sectionName>Invoking a Model Transformation From Java Code</sectionName>
<sectionID>callMote</sectionID>


<p>The model transformation system\'s main class is <emph>TGGEngine</emph>. It provides three transformation operations:</p>

<ul>

	<li><code>transformFromFiles(URI sourceFileUri, URI targetFileUri, TransformationDirection direction, String ruleSetID, boolean synchronize)</code><p>Performs a model transformation on the model files. The models are loaded from the files, the transformation or synchronization
	is performed, and the target model is written to file. If the synchronize flag is true, a mapping transformation is performed first
	to create the correspondence model.</p></li>

	<li><code>transformInMemory(URI sourceFileUri, URI targetFileUri, TransformationDirection direction, String ruleSetID, boolean synchronize)</code><p>Performs a model transformation in memory using appropriate model adapters. The diagram adapters a responsible for retrieving the model resource
	using the given URIs.</p></li>

	<li><code>transformInMemory(EResource sourceResource, EResource targetResource, TransformationDirection direction, String ruleSetID, boolean synchronize)</code><p>Performs a model transformation in memory on the specified resources. Use this operation, if you already have the resouces containing the models.</p></li>

</ul>

<p>If you want to use the transformation system from your own code, create a new <emph>TGGEngine</emph> using the <emph>MoteFactory</emph> class. Then, 
you would usually use the third operation. You only have to provide the source
and target resources, specify the transformation direction and the rule set to use, and state, whether a transformation or a synchronization should
be performed.</p>