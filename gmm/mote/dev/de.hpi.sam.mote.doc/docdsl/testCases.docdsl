<sectionName>Testing TGG Rules</sectionName>
<sectionID>testCases</sectionID>

<p>After creating a TGG, you have to be sure that it actually does what it is supposed to do. To help in testing
a model transformation, you can create a test project using the pre-generated <emph>generate test case project.workflow</emph>.
This creates a new project, which contains an initial workflow <emph>generate test case.workflow</emph>. Executing it
will create a new workflow, which contains the actual test case. Execute that test case workflow to test your transformation.</p>

<p><code>generate test case.workflow</code> has two parameters that you can change. Open the workflow file to modify them.
<code>modelSize</code> determines the size of the test models of the test case, <code>direction</code> specifies which
transformation direction should be tested by the generated test case. Possible values are <code>forward</code> and 
<code>reverse</code>.</p>

<p>A test case creates the source and an reference target model in parallel by applying the TGG rules. After that,
the source model is transformed by the TGG engine. The target model created by the engine is then compared to the
reference target model using <emph>EMFCompare</emph>. If both models are identical, the test has passed. The two
reference models and the created target model are saved to XMI files for later inspection.</p>

<p>The test models are created randomly. However, the randomization is done when the test case workflow is generated.
The test case itself contains a fixed plan based on which the test models are created. Therefore, the same test case
always creates the same test models so the test results can be reproduced.</p>

<p>Currently, only TGGs are supported that contain rule variables of type <code>string</code>.</p>

<p><emph>Note:</emph> The generated test case workflows have the file suffix <emph>workflowbin</emph>. These workflows
use a binary serialization format instead of XMI. Test case workflows that build large test models are itself very large
because they contain all the necessary building steps to create these test models. The binary serialization format leads
to smaller file sizes and shorter load times. However, they may not be readably anymore if EMF\'s implementation of the
serializer changes.</p>