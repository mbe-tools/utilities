<sectionName>Diagram Adapter</sectionName>
<sectionID>diagramAdapter</sectionID>


<p>A diagram adapter is responsible for opening a model using the appropriate editor making the editor\'s model available
to the transformation system. For an example, have a look at the <emph>de.hpi.sam.blockDiagram.diagramAdapter</emph>plug-in.</p>

<p>Every diagram adapter must extend the abstract <emph>de.hpi.sam.mote.DiagramAdapter</emph> class and implement the
<emph>getDiagramResource(URI)</emph> operation. This operation is called by the transformation system to open the model
with the given URI and return a resource that contains the model. You can also overwrite the <emph>load()</emph> and
<emph>save()</emph> operations if necessary. These operations are called before and after a transformation is executed.</p>

<image>
	<file>diagramAdapter/diagramAdapterMetaModel.png</file>
	<caption>Ecore metamodel of the block diagram adapter</caption>
</image>

<p>To create a new diagram adapter, create a new EMF project with a new ecore file. Load 
<emph>platform:/plugin/de.hpi.sam.mote/model.mote.ecore</emph> as an additional resource.
Create a new <emph>EClass</emph> that extends <emph>DiagramAdapter</emph>. Create an EMF genmodel and generate the model code.
Then, you can implement the missing operations.</p>

<p>Finally, you have to extend the <emph>de.hpi.sam.mote.diagramAdapterExtension</emph> extension point. There you have to provide
the ID of the plugin containing the metamodel code which the diagram adapter can handle, the nsURI of the package that contains
the diagram adapter, as well as the name of the factory method that creates the diagram adapter (this is a bit dirty, right now...).</p>