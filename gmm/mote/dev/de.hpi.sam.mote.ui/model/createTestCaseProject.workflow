<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflow.components.xpandComponent="http://mdelab/workflow/components/xpandComponent/1.0" name="workflow">
  <components xsi:type="workflow.components:ProjectCreator" name="projectCreator" projectName="${projectName}" deleteExistingProject="true"/>
  <components xsi:type="workflow.components.xpandComponent:XpandComponent" name="xpandComponent" templateDirURI="platform:/plugin/de.hpi.sam.mote.ui/model/templates" outputPathURI="platform:/resource/${projectName}/" templateName="createGenerateTestCasesWorkflow::main">
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${tggFileURI}"/>
  </components>
  <properties name="projectName"/>
  <properties name="tggFileURI"/>
</workflow:Workflow>
