<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflow.components.xpandComponent="http://mdelab/workflow/components/xpandComponent/1.0" name="workflow">
  <components xsi:type="workflow.components:ProjectCreator" name="projectCreator" projectName="${projectName}" deleteExistingProject="true"/>
  <components xsi:type="workflow.components.xpandComponent:XpandComponent" name="dotProject" modelSlot="" templateDirURI="templates" outputPathURI="platform:/resource/${projectName}/" templateName="createDotProject::main">
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${projectName}"/>
  </components>
  <components xsi:type="workflow.components.xpandComponent:XpandComponent" name="create .classpath" templateDirURI="templates" outputPathURI="platform:/resource/${projectName}/" templateName="createDotClasspath::main"/>
  <components xsi:type="workflow.components.xpandComponent:XpandComponent" name="Manifest" modelSlot="" templateDirURI="templates" outputPathURI="platform:/resource/${projectName}/META-INF/" templateName="createManifest::main">
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${projectName}"/>
  </components>
  <components xsi:type="workflow.components.xpandComponent:XpandComponent" name="Ecore" modelSlot="" templateDirURI="templates" outputPathURI="platform:/resource/${projectName}/model/" templateName="createEcore::main">
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${projectName}"/>
  </components>
  <components xsi:type="workflow.components.xpandComponent:XpandComponent" name="Tgg" modelSlot="" templateDirURI="templates" outputPathURI="platform:/resource/${projectName}/model/" templateName="createTgg::main">
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${projectName}"/>
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${nsURISource}"/>
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${nsURITarget}"/>
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${sourceRootNode}"/>
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${targetRootNode}"/>
  </components>
  <components xsi:type="workflow.components.xpandComponent:XpandComponent" name="opRules" modelSlot="" templateDirURI="templates" outputPathURI="platform:/resource/${projectName}/model/" templateName="createOpRules::main">
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${projectName}"/>
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${tggFileURI}"/>
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${correspondenceMetamodelURI}"/>
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${javaBasePackage}"/>
  </components>
  <components xsi:type="workflow.components.xpandComponent:XpandComponent" name="create build.properties" modelSlot="" templateDirURI="templates" outputPathURI="platform:/resource/${projectName}/" templateName="createBuildProperties::main">
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${projectName}"/>
  </components>
  <components xsi:type="workflow.components.xpandComponent:XpandComponent" name="create 'generate test case project.workflow'" templateDirURI="templates" outputPathURI="platform:/resource/${projectName}/model/" templateName="createGenerateTestCaseProjectWorkflow::main">
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${projectName}"/>
    <parameters xsi:type="workflow.components.xpandComponent:StringParameter" stringValue="${tggFileURI}"/>
  </components>
  <properties name="projectName" defaultValue="de.hpi.sam.mote.testRules"/>
  <properties name="tggFileURI" defaultValue="SDL2UML.tgg"/>
  <properties name="correspondenceMetamodelURI" defaultValue="sdl2uml.ecore"/>
  <properties name="javaBasePackage" defaultValue="de.hpi.sam.mote"/>
  <properties name="nsURISource" defaultValue=""/>
  <properties name="nsURITarget" defaultValue=""/>
  <properties name="sourceRootNode" defaultValue=""/>
  <properties name="targetRootNode" defaultValue=""/>
</workflow:Workflow>
