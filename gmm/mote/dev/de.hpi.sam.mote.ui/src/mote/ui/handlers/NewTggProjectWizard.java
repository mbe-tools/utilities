package mote.ui.handlers;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.presentation.EcoreActionBarContributor.ExtendedLoadResourceAction.RegisteredPackageDialog;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.console.IConsole;

import de.mdelab.workflow.Workflow;

public class NewTggProjectWizard extends Wizard implements INewWizard
{
	private String					chosenProjectName		= "de.hpi.sam.mote.x2y";
	private String					selectedBasepackage		= "de.hpi.sam.mote";
	private String					selectedSourceModel		= "";
	private String					selectedTargetModel		= "";
	private String					selectedSourceRootNode	= "";
	private String					selectedTargetRootNode	= "";

	private NewTggProjectWizardPage	newTggProjectWizardPage;

	private IWorkbenchWindow		window;

	public void init(IWorkbench workbench, IStructuredSelection selection)
	{
		this.window = workbench.getActiveWorkbenchWindow();
	}

	@Override
	public boolean performFinish()
	{
		// Get project name & base package
		this.chosenProjectName = this.newTggProjectWizardPage.projectName.getText();
		this.selectedBasepackage = this.newTggProjectWizardPage.basePackage.getText();

		// Check if all filenames have been entered
		if (this.chosenProjectName == null || this.chosenProjectName.equals(""))
		{
			MessageDialog.openError(this.getShell(), "Enter Project Name", "Please enter a project name.");
			return false;
		}
		else if (this.selectedBasepackage == null || this.selectedBasepackage.equals(""))
		{
			MessageDialog.openError(this.getShell(), "Enter Base Package", "Please chose a base package.");
			return false;
		}
		else if (this.selectedTargetModel == null)
		{
			MessageDialog.openError(this.getShell(), "Enter Target Model", "Please enter a target model.");
			return false;
		}
		else if (this.selectedSourceModel == null)
		{
			MessageDialog.openError(this.getShell(), "Enter Source Model", "Please enter a source model.");
			return false;
		}
		else if (this.selectedTargetRootNode == null)
		{
			MessageDialog.openError(this.getShell(), "Enter Target Root Node", "Please chose the target root node.");
			return false;
		}
		else if (this.selectedSourceRootNode == null)
		{
			MessageDialog.openError(this.getShell(), "Enter Source Root Node", "Please chose the source root node.");
			return false;
		}

		try
		{
			final Map<String, String> propertyValues = new HashMap<String, String>();

			URI workflowUri = URI.createPlatformPluginURI("/de.hpi.sam.mote.ui/model/createMoteProject.workflow", true);
			ResourceSet rs = new ResourceSetImpl();
			Resource r = rs.createResource(workflowUri);
			r.load(null);

			final Workflow workflow = (Workflow) r.getContents().get(0);

			// Map all variables gathered by the Wizard and pass them as
			// property values
			propertyValues.put("projectName", this.chosenProjectName);
			propertyValues.put("javaBasePackage", this.selectedBasepackage);
			propertyValues.put("nsURITarget", this.selectedTargetModel.toString());
			propertyValues.put("nsURISource", this.selectedSourceModel.toString());
			propertyValues.put("targetRootNode", this.selectedTargetRootNode);
			propertyValues.put("sourceRootNode", this.selectedSourceRootNode);
			propertyValues.put("tggFileURI",
					this.chosenProjectName.subSequence(this.chosenProjectName.lastIndexOf(".") + 1, this.chosenProjectName.length())
							+ ".tgg");
			propertyValues.put("correspondenceMetamodelURI",
					this.chosenProjectName.subSequence(this.chosenProjectName.lastIndexOf(".") + 1, this.chosenProjectName.length())
							+ ".ecore");

			ProgressMonitorDialog dialog = new ProgressMonitorDialog(this.getShell());

			dialog.run(true, false, new IRunnableWithProgress()
			{
				public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException
				{
					try
					{
						org.eclipse.ui.console.IConsoleManager consoleManager = org.eclipse.ui.console.ConsolePlugin.getDefault()
								.getConsoleManager();
						org.eclipse.ui.console.MessageConsole console = new org.eclipse.ui.console.MessageConsole("My Message Console",
								null);
						org.eclipse.ui.console.MessageConsoleStream consoleStream = new org.eclipse.ui.console.MessageConsoleStream(console);

						consoleManager.addConsoles(new IConsole[]
						{
							console
						});
						consoleManager.showConsoleView(console);

						workflow.execute(monitor, consoleStream, propertyValues);
					}
					catch (Exception e)
					{
						throw new InvocationTargetException(e);
					}
				}
			});
		}
		catch (Throwable ex)
		{
			ex.printStackTrace();
		}
		return true;
	}

	@Override
	public void addPages()
	{
		this.newTggProjectWizardPage = new NewTggProjectWizardPage("Create new TGG rule project", "Create new TGG rule project", null);
		this.addPage(this.newTggProjectWizardPage);
	}

	private class NewTggProjectWizardPage extends WizardPage
	{
		private Label	labelProjectName;
		public Text		projectName;
		private Label	labelBasePackage;
		public Text		basePackage;
		private Label	labelSourceModelUri;
		private Text	sourceModelUri;
		private Label	labelSourceRootNode;
		private Combo	sourceRootNode;
		private Label	labelTargetModelUri;
		private Text	targetModelUri;
		private Label	labelTargetRootNode;
		private Combo	targetRootNode;
		private Button	browseRegistry;

		protected NewTggProjectWizardPage(String pageName)
		{
			super(pageName);
		}

		protected NewTggProjectWizardPage(String pageName, String title, ImageDescriptor titleImage)
		{
			super(pageName, title, titleImage);
		}

		public void createControl(Composite parent)
		{

			Composite compositeMain = new Composite(parent, SWT.NONE);
			GridLayout gridLayout = new GridLayout(3, false);
			compositeMain.setLayout(gridLayout);

			// Project name label
			{
				this.labelProjectName = new Label(compositeMain, SWT.NONE);
				this.labelProjectName.setText("Project name");
				this.labelProjectName.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));
			}

			// Project name text field
			{
				this.projectName = new Text(compositeMain, SWT.SINGLE | SWT.BORDER);
				this.projectName.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
				this.projectName.setEditable(true);

				this.projectName.setToolTipText("The name of the project you wish to create.");

				this.projectName.setText(NewTggProjectWizard.this.chosenProjectName);
			}

			{
				Label emptyLabel = new Label(compositeMain, SWT.NONE);
				emptyLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));
			}

			// Base package label
			{
				this.labelBasePackage = new Label(compositeMain, SWT.NONE);
				this.labelBasePackage.setText("Java base package");
				this.labelBasePackage.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));
			}

			// Base package text field
			{
				this.basePackage = new Text(compositeMain, SWT.SINGLE | SWT.BORDER);
				this.basePackage.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
				this.basePackage.setEditable(true);

				this.basePackage.setToolTipText("Java base package to use.");

				this.basePackage.setText(NewTggProjectWizard.this.selectedBasepackage);
			}

			{
				Label emptyLabel = new Label(compositeMain, SWT.NONE);
				emptyLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));
			}

			// Source model URI label
			{
				this.labelSourceModelUri = new Label(compositeMain, SWT.HORIZONTAL | SWT.LEFT);
				this.labelSourceModelUri.setText("Source Metamodel Uri");
				this.labelSourceModelUri.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));
			}

			// Source model URI text field
			{
				this.sourceModelUri = new Text(compositeMain, SWT.SINGLE | SWT.BORDER);
				this.sourceModelUri.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
				this.sourceModelUri.setEditable(false);

				this.sourceModelUri.setToolTipText("URI of the source metamodel for the workflow.");

				this.sourceModelUri.setText(NewTggProjectWizard.this.selectedSourceModel);
			}

			// Browse registry
			{
				this.browseRegistry = new Button(compositeMain, SWT.PUSH | SWT.LEFT);
				this.browseRegistry.setText("Browse Package Registry");
				this.browseRegistry.setLayoutData(new GridData(SWT.WRAP, SWT.TOP, true, false, 1, 1));

				this.browseRegistry.setToolTipText("Browse the package registry...");

				this.browseRegistry.addSelectionListener(new SelectionAdapter()
				{
					@Override
					public void widgetSelected(SelectionEvent event)
					{
						RegisteredPackageDialog registeredPackageDialog = new RegisteredPackageDialog(NewTggProjectWizard.this.window
								.getShell());
						registeredPackageDialog.open();
						Object[] result = registeredPackageDialog.getResult();
						if (result != null)
						{
							List<?> nsURIs = Arrays.asList(result);
							NewTggProjectWizard.this.selectedSourceModel = nsURIs.get(0).toString();
							NewTggProjectWizard.this.selectedSourceModel = NewTggProjectWizard.this.selectedSourceModel.substring(0,
									NewTggProjectWizard.this.selectedSourceModel.length());

							NewTggProjectWizardPage.this.sourceModelUri.setText(NewTggProjectWizard.this.selectedSourceModel);
							EPackage ePackage = Registry.INSTANCE.getEPackage(NewTggProjectWizardPage.this.sourceModelUri.getText());
							NewTggProjectWizardPage.this.getPackages(ePackage, NewTggProjectWizardPage.this.sourceRootNode);
						}
					}
				});
			}

			// labelSourceRootNode
			{
				this.labelSourceRootNode = new Label(compositeMain, SWT.HORIZONTAL | SWT.LEFT);
				this.labelSourceRootNode.setText("Source Model Root Node");
				this.labelSourceRootNode.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));
			}

			// Source model root node drop down list
			{
				this.sourceRootNode = new Combo(compositeMain, SWT.DROP_DOWN | SWT.READ_ONLY);
				this.sourceRootNode.setBounds(135, 10, 220, 21);
				this.sourceRootNode.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

				this.sourceRootNode.addSelectionListener(new SelectionListener()
				{
					public void widgetSelected(SelectionEvent e)
					{
						NewTggProjectWizard.this.selectedSourceRootNode = NewTggProjectWizardPage.this.sourceRootNode.getText();
					}

					public void widgetDefaultSelected(SelectionEvent e)
					{
					}
				});
			}

			{
				Label emptyLabel = new Label(compositeMain, SWT.NONE);
				emptyLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));
			}

			// Target model URI label
			{
				this.labelTargetModelUri = new Label(compositeMain, SWT.NONE);
				this.labelTargetModelUri.setText("Target Metamodel URI");
				this.labelTargetModelUri.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));
			}

			// Target model URI text field
			{
				this.targetModelUri = new Text(compositeMain, SWT.SINGLE | SWT.BORDER);
				this.targetModelUri.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
				this.targetModelUri.setEditable(false);

				this.targetModelUri.setToolTipText("URI of the target metamodel for the workflow.");

				this.targetModelUri.setText(NewTggProjectWizard.this.selectedTargetModel);
			}

			// Browse registry
			{
				this.browseRegistry = new Button(compositeMain, SWT.PUSH | SWT.LEFT);
				this.browseRegistry.setText("Browse Package Registry");
				this.browseRegistry.setLayoutData(new GridData(SWT.WRAP, SWT.TOP, true, false, 1, 1));

				this.browseRegistry.setToolTipText("Browse the package registry...");

				this.browseRegistry.addSelectionListener(new SelectionAdapter()
				{
					@Override
					public void widgetSelected(SelectionEvent event)
					{
						RegisteredPackageDialog registeredPackageDialog = new RegisteredPackageDialog(NewTggProjectWizard.this.window
								.getShell());
						registeredPackageDialog.open();
						Object[] result = registeredPackageDialog.getResult();
						if (result != null)
						{
							List<?> nsURIs = Arrays.asList(result);
							NewTggProjectWizard.this.selectedTargetModel = nsURIs.get(0).toString();
							NewTggProjectWizard.this.selectedTargetModel = NewTggProjectWizard.this.selectedTargetModel.substring(0,
									NewTggProjectWizard.this.selectedTargetModel.length());

							NewTggProjectWizardPage.this.targetModelUri.setText(NewTggProjectWizard.this.selectedTargetModel);
							EPackage ePackage = Registry.INSTANCE.getEPackage(NewTggProjectWizardPage.this.targetModelUri.getText());
							NewTggProjectWizardPage.this.getPackages(ePackage, NewTggProjectWizardPage.this.targetRootNode);
						}
					}
				});
			}

			// labelTargetRootNode
			{
				this.labelTargetRootNode = new Label(compositeMain, SWT.HORIZONTAL | SWT.LEFT);
				this.labelTargetRootNode.setText("Target Model Root Node");
				this.labelTargetRootNode.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));
			}

			// Target model root node
			{
				this.targetRootNode = new Combo(compositeMain, SWT.DROP_DOWN | SWT.READ_ONLY);
				this.targetRootNode.setBounds(135, 10, 220, 21);
				this.targetRootNode.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

				this.targetRootNode.addSelectionListener(new SelectionListener()
				{
					public void widgetSelected(SelectionEvent e)
					{
						NewTggProjectWizard.this.selectedTargetRootNode = NewTggProjectWizardPage.this.targetRootNode.getText();
					}

					public void widgetDefaultSelected(SelectionEvent e)
					{
					}
				});
			}

			{
				Label emptyLabel = new Label(compositeMain, SWT.NONE);
				emptyLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));
			}
			this.setControl(compositeMain);
		}

		private void getPackages(EPackage e, Combo sourceRootNode)
		{
			if (e == null)
			{
				return;
			}

			EList<EClassifier> list = e.getEClassifiers();
			Iterator<EClassifier> listIterator = list.iterator();
			EClassifier currElement;

			// Process Classifiers
			while (listIterator.hasNext())
			{
				currElement = listIterator.next();
				if (currElement instanceof EClass)
				{
					if (!((EClass) currElement).isInterface() && !((EClass) currElement).isAbstract())
					{
						sourceRootNode.add(currElement.getName());
					}
				}
			}

			// Process sub-packages
			try
			{
				EList<EPackage> packageList = e.getESubpackages();
				for (EPackage p : packageList)
				{
					this.getPackages(p, sourceRootNode);
				}
			}
			catch (NullPointerException npe)
			{
				// If a nullpointer has been thrown, the recursion should
				// end here, because there are no more subpackages
				return;
			}
		}
	}
}
