package mote.ui.handlers;

import java.io.IOException;
import java.io.OutputStream;

import org.eclipse.emf.common.ui.dialogs.ResourceDialog;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class SaveLogWizard extends Wizard
{
	private class SaveLogWizardPage extends WizardPage
	{
		private Text	textUncoveredSourceElements;
		private Text	textUncoveredTargetElements;
		private Label	labelUncoveredSourceElements;
		private Label	labelUncoveredTargetElements;
		private Text	textLogFileName;
		private Button	buttonBrowseLogFileName;
		private Label	labelLogFileName;

		protected SaveLogWizardPage(String pageName)
		{
			super(pageName);
		}

		protected SaveLogWizardPage(String pageName, String title, ImageDescriptor titleImage)
		{
			super(pageName, title, titleImage);
		}

		public void createControl(Composite parent)
		{
			Composite compositeMain = new Composite(parent, SWT.NONE);

			GridLayout gridLayout = new GridLayout(3, false);
			compositeMain.setLayout(gridLayout);

			{
				labelUncoveredSourceElements = new Label(compositeMain, SWT.NONE);
				labelUncoveredSourceElements.setText("Model elements in " + sourceUri.lastSegment() + " without corresponding elements in "
						+ targetUri.lastSegment() + ":");
				labelUncoveredSourceElements.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 3, 1));
			}

			{
				textUncoveredSourceElements = new Text(compositeMain, SWT.MULTI | SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL);
				textUncoveredSourceElements.setText(sourceElementsLog);
				textUncoveredSourceElements.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
			}

			{
				labelUncoveredTargetElements = new Label(compositeMain, SWT.NONE);
				labelUncoveredTargetElements.setText("Model elements in " + targetUri.lastSegment() + " without corresponding elements in "
						+ sourceUri.lastSegment() + ":");
				labelUncoveredTargetElements.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 3, 1));
			}

			{
				textUncoveredTargetElements = new Text(compositeMain, SWT.MULTI | SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL);
				textUncoveredTargetElements.setText(targetElementsLog);
				textUncoveredTargetElements.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
			}

			{
				labelLogFileName = new Label(compositeMain, SWT.NONE);
				labelLogFileName.setText("Log File: ");
				labelLogFileName.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));
			}

			{
				textLogFileName = new Text(compositeMain, SWT.SINGLE | SWT.BORDER);
				if (logFileUri != null)
				{
					textLogFileName.setText(logFileUri.toString());
				}
				textLogFileName.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
			}

			{
				buttonBrowseLogFileName = new Button(compositeMain, SWT.PUSH | SWT.CENTER);
				buttonBrowseLogFileName.setText("Browse...");
				buttonBrowseLogFileName.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));

				buttonBrowseLogFileName.setToolTipText("Browse for a log file name.");

				buttonBrowseLogFileName.addSelectionListener(new SelectionListener()
				{

					public void widgetDefaultSelected(SelectionEvent e)
					{
						widgetSelected(e);
					}

					public void widgetSelected(SelectionEvent e)
					{
						ResourceDialog d = new ResourceDialog(getShell(), "Select Log File", SWT.OPEN | SWT.SINGLE);
						if (d.open() == ResourceDialog.OK)
						{
							if (d.getURIs().size() > 0)
							{
								logFileUri = d.getURIs().get(0);
								textLogFileName.setText(logFileUri.toString());
							}
							else
							{
								logFileUri = null;
								textLogFileName.setText("");
							}
						}
					}
				});
			}

			setControl(compositeMain);
		}
	}

	private String	message;
	private String	sourceElementsLog;
	private String	targetElementsLog;
	private URI		sourceUri;
	private URI		targetUri;
	private URI		logFileUri;

	public SaveLogWizard(String message, EList<EObject> sourceElements, EList<EObject> targetElements, URI sourceUri, URI targetUri)
	{
		this.message = message;
		this.sourceUri = sourceUri;
		this.targetUri = targetUri;

		String NL = System.getProperty("line.separator");

		StringBuilder stringBuilder = new StringBuilder();

		for (EObject e : sourceElements)
		{
			stringBuilder.append(e.toString());
			stringBuilder.append(NL);
		}

		sourceElementsLog = stringBuilder.toString();

		stringBuilder = new StringBuilder();

		for (EObject e : targetElements)
		{
			stringBuilder.append(e.toString());
			stringBuilder.append(NL);
		}

		targetElementsLog = stringBuilder.toString();
	}

	@Override
	public void addPages()
	{
		addPage(new SaveLogWizardPage("Save Log File", message, null));
	}

	@Override
	public boolean performFinish()
	{
		if (logFileUri != null)
		{
			ExtensibleURIConverterImpl uriConverter = new ExtensibleURIConverterImpl();

			try
			{
				OutputStream outputStream = uriConverter.createOutputStream(logFileUri);

				String NL = System.getProperty("line.separator");

				outputStream.write(message.getBytes());
				outputStream.write(NL.getBytes());
				outputStream.write(NL.getBytes());

				outputStream.write("Source model: ".getBytes());
				outputStream.write(sourceUri.toString().getBytes());
				outputStream.write(NL.getBytes());

				outputStream.write(sourceElementsLog.getBytes());
				outputStream.write(NL.getBytes());
				outputStream.write(NL.getBytes());

				outputStream.write("Target model: ".getBytes());
				outputStream.write(targetUri.toString().getBytes());
				outputStream.write(NL.getBytes());

				outputStream.write(targetElementsLog.getBytes());

				outputStream.close();
			}
			catch (IOException ex)
			{
				ex.printStackTrace();
				MessageDialog.openError(getShell(), "Error", "Could not write log: " + ex.getMessage());
			}

			return true;
		}
		else
		{
			MessageDialog.openError(getShell(), "Enter Log File Name", "Please select a log file.");

			return false;
		}
	}

}
