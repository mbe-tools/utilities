package de.hpi.sam.properties.storyDiagramEcore;

import org.eclipse.emf.ecore.EStructuralFeature;

public class ActivitySpecificationSection extends EnhancedChooserPropertySection
{

	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE.getActivity_Specification();
	}

	@Override
	protected String getLabelText()
	{
		return "Specification";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.Activity) getEObject()).getSpecification();
	}

}