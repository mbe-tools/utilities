package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EStructuralFeature;

public class MapEntryStoryPatternLinkEStructuralFeatureSection extends EnhancedChooserPropertySection
{

	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getMapEntryStoryPatternLink_EStructuralFeature();
	}

	@Override
	protected String getLabelText()
	{
		return "EStructuralFeature";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) getEObject()).getEStructuralFeature();
	}

}