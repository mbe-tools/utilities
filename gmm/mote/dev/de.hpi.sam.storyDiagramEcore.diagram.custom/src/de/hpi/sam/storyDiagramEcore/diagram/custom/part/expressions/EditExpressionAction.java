package de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.diagram.custom.EditExpressionDialog;

public abstract class EditExpressionAction extends CommonEditExpressionAction
{

	@Override
	protected void prepareDialog()
	{
		EditExpressionDialog dialog = new EditExpressionDialog(null);
		dialog.setActivity(getActivity());
		editExpressionDialog = dialog;
		super.prepareDialog();
	}

	/**
	 * Returns the activity that contains the expression.
	 * 
	 * @return
	 */
	protected abstract Activity getActivity();

}
