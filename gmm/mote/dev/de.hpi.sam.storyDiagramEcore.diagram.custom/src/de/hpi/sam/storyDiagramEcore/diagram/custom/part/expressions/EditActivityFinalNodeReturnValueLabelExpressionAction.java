package de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.ui.action.AbstractActionDelegate;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.ui.IObjectActionDelegate;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.diagram.custom.ChooseActivityOutParameterDialog;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeReturnValueLabelEditPart;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;

public class EditActivityFinalNodeReturnValueLabelExpressionAction extends AbstractActionDelegate implements IObjectActionDelegate
{

	ChooseActivityOutParameterDialog	parameterDialog;

	@Override
	protected void doRun(IProgressMonitor progressMonitor)
	{
		parameterDialog = new ChooseActivityOutParameterDialog(null);
		parameterDialog.setChangeCommandReceiver(this.getChangeCommandReceiver());
		parameterDialog.setNode(this.getModel());
		parameterDialog.setActivity(this.getActivity());
		parameterDialog.setEditPart(this.getEditPart());

		parameterDialog.open();
	}

	protected ActivityFinalNode getModel()
	{
		ActivityFinalNodeEditPart aaEditPart = this.getEditPart();
		return (ActivityFinalNode) ((View) aaEditPart.getModel()).getElement();
	}

	protected ActivityFinalNodeEditPart getEditPart()
	{
		return (ActivityFinalNodeEditPart) ((ActivityFinalNodeReturnValueLabelEditPart) getStructuredSelection().getFirstElement())
				.getParent();
	}

	protected TransactionalEditingDomain getChangeCommandReceiver()
	{
		return ((ActivityFinalNodeReturnValueLabelEditPart) getStructuredSelection().getFirstElement()).getEditingDomain();
	}

	protected Activity getActivity()
	{
		return (Activity) this.getExpressionOwner().eContainer();
	}

	protected EObject getExpressionOwner()
	{
		return ((View) ((ActivityFinalNodeReturnValueLabelEditPart) getStructuredSelection().getFirstElement()).getModel()).getElement();
	}

}
