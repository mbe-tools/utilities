package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart;
import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;

public class CustomCallActionExpression3EditPart extends CallActionExpression3EditPart
{

	public CustomCallActionExpression3EditPart(View view)
	{
		super(view);
	}

	/*
	 * should be similar to addNotationalListeners of:
	 * 
	 * CustomActivityEdgeGuardConstraintLabelEditPart
	 * CustomActivityFinalNodeReturnValueLabelEditPart
	 * CustomAttributeAssignmentEditPart CustomCallActionExpressionEditPart
	 * CustomCallActionExpression2EditPart CustomCallActionExpression3EditPart
	 * CustomStoryPatternExpressionLinkFeatureNameLabelEditPart
	 * CustomStoryPatternObjectClassifierLabelEditPart
	 */
	@Override
	protected void addNotationalListeners()
	{
		super.addNotationalListeners();
		CallActionExpression cae = (CallActionExpression) ((View) getModel()).getElement();
		TreeIterator<EObject> it = cae.eAllContents();

		for (EObject eObj; it.hasNext();)
		{
			eObj = it.next();
			this.addListenerFilter(Utility.getFilterId((NamedElement) eObj), this, eObj);
		}
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	protected void updateFigure()
	{
		CallActionExpression callActionExpression = (CallActionExpression) ((View) getModel()).getElement();

		if (callActionExpression != null)
		{
			setLabelText("[ " + callActionExpression.toString() + " ]");
		}
		else
		{
			setLabelText("");
		}

		// if (getLabelTextHelper(getFigure()).length() > 50)
		// {
		// setLabelText(getLabelTextHelper(getFigure()).substring(0, 50) +
		// "...");
		// }
		//
		// ((WrappingLabel) getFigure()).setTextWrap(true);
	}

	/*
	 * should be similar to handleNotificationEvent of:
	 * 
	 * CustomActivityEdgeGuardConstraintLabelEditPart
	 * CustomActivityFinalNodeReturnValueLabelEditPart
	 * CustomAttributeAssignmentEditPart CustomCallActionExpressionEditPart
	 * CustomCallActionExpression2EditPart CustomCallActionExpression3EditPart
	 * CustomStoryPatternExpressionLinkFeatureNameLabelEditPart
	 * CustomStoryPatternObjectClassifierLabelEditPart
	 */
	@Override
	protected void handleNotificationEvent(Notification event)
	{
		Object notifier = event.getNotifier();
		Object newValue = event.getNewValue();
		Object oldValue = event.getOldValue();
		int eventType = event.getEventType();

		if ((eventType == Notification.SET || eventType == Notification.ADD)
				&& (newValue instanceof CallAction || newValue instanceof Expression || newValue instanceof CallActionParameter))
		{
			this.addListenerFilter(Utility.getFilterId((NamedElement) newValue), this, (EObject) newValue);
			refreshVisuals();
		}
		else if (eventType == Notification.REMOVE
				&& (oldValue instanceof CallAction || oldValue instanceof Expression || oldValue instanceof CallActionParameter))
		{
			this.removeListenerFilter(Utility.getFilterId((NamedElement) oldValue));
			refreshVisuals();
		}
		else if (notifier instanceof CallAction || notifier instanceof Expression || notifier instanceof CallActionParameter)
		{
			refreshVisuals();
		}

		super.handleNotificationEvent(event);
	}
}
