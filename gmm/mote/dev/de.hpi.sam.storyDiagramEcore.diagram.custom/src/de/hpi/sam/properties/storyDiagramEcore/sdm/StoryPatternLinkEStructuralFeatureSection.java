package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EStructuralFeature;

public class StoryPatternLinkEStructuralFeatureSection extends EnhancedChooserPropertySection
{

	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getStoryPatternLink_EStructuralFeature();
	}

	@Override
	protected String getLabelText()
	{
		return "EStructuralFeature";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) getEObject()).getEStructuralFeature();
	}

}