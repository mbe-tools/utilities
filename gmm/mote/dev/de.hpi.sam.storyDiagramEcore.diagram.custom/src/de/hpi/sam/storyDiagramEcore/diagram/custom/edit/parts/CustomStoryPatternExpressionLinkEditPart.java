package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink;

public class CustomStoryPatternExpressionLinkEditPart extends StoryPatternExpressionLinkEditPart
{

	public CustomStoryPatternExpressionLinkEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification notification)
	{
		if (notification.getNotifier() instanceof StoryPatternExpressionLink)
		{
			// Update colors if necessary ------------------------------------
			Utility.adaptColor(getPrimaryShape(), ((StoryPatternElement) ((View) this.getModel()).getElement()).getModifier());
		}

		super.handleNotificationEvent(notification);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		Utility.adaptColor(getPrimaryShape(), ((StoryPatternElement) ((View) this.getModel()).getElement()).getModifier());
	}
}
