package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;

public class CustomStoryActionNodeEditPart extends StoryActionNodeEditPart
{

	public CustomStoryActionNodeEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification notification)
	{
		if (notification.getNotifier() instanceof StoryActionNode)
		{
			updateFigure();
			repaint();
			
			
		}

		super.handleNotificationEvent(notification);
	}

	protected void updateFigure()
	{
		boolean isForEach = ((StoryActionNode) ((View) getModel()).getElement()).isForEach();
		RectangleFigure frontRectangle = (RectangleFigure) getPrimaryShape().getChildren().get(1);

		Insets frontInsets = frontRectangle.getBorder().getInsets(frontRectangle);
	
		if (!isForEach)
		{
			frontInsets.top = 0;
			frontInsets.bottom = 0;
			frontInsets.left = 0;
			frontInsets.right = 0;
		}
		else
		{
			frontInsets.top = 0;
			frontInsets.bottom = 10;
			frontInsets.left = 0;
			frontInsets.right = 10;
		}
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();

	}
	private void repaint()
	{
		// Get the StoryPattern Compartment.
		IFigure rectangleFront = (IFigure) ((IFigure) getFigure().getChildren().get(0)).getChildren().get(1);
		rectangleFront =  (IFigure) rectangleFront.getChildren().get(0);
		IFigure rectangleContent = (IFigure) rectangleFront.getChildren().get(1);
		rectangleContent = (IFigure) rectangleContent.getChildren().get(1);
		ResizableCompartmentFigure upperCompartment = (ResizableCompartmentFigure) rectangleContent.getChildren().get(0);
		upperCompartment.setLayoutManager(new BorderLayout());
	}
}
