package de.hpi.sam.properties.storyDiagramEcore.callActions;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractStringPropertySection;

public class MethodCallActionMethodNameSection extends AbstractStringPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage.eINSTANCE.getMethodCallAction_MethodName();
	}

	@Override
	protected String getLabelText()
	{
		return "Method Name";
	}

}
