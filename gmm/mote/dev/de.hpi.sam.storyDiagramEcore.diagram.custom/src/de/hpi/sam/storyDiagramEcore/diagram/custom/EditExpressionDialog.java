package de.hpi.sam.storyDiagramEcore.diagram.custom;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.swt.widgets.Shell;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.ActivityParameter;
import de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum;
import de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

public class EditExpressionDialog extends CommonEditExpressionDialog
{

	public EditExpressionDialog(Shell parent)
	{
		super(parent);
	}

	private Activity	activity;

	public void setActivity(Activity activity)
	{
		this.activity = activity;
		this.setRootContainer(activity);
	}

	@Override
	protected Map<String, EClassifier> getContextInformation()
	{
		Map<String, EClassifier> contextInfos = new HashMap<String, EClassifier>();

		if (this.activity != null)
		{
			/*
			 * THIS and parameters
			 */
			if (this.activity.getSpecification() != null)
			{
				EClass thisClass = this.activity.getSpecification().getEContainingClass();

				if (thisClass != null)
				{
					contextInfos.put("this", thisClass);
				}

				for (EParameter parameter : this.activity.getSpecification().getEParameters())
				{
					if (parameter.getName() != null && !"".equals(parameter) && parameter.getEType() != null)
					{
						contextInfos.put(parameter.getName(), parameter.getEType());
					}
				}
			}
			else
			{
				for (ActivityParameter parameter : this.activity.getParameters())
				{
					if ((parameter.getDirection() == ActivityParameterDirectionEnum.IN || parameter.getDirection() == ActivityParameterDirectionEnum.OUT)
							&& parameter.getName() != null && !"".equals(parameter) && parameter.getType() != null)
					{
						contextInfos.put(parameter.getName(), parameter.getType());
					}
				}
			}

			/*
			 * Variables created in story patterns and
			 * VariableDeclarationActions
			 */
			TreeIterator<EObject> it = this.activity.eAllContents();

			while (it.hasNext())
			{
				EObject eObject = it.next();

				if (eObject instanceof StoryPatternObject)
				{
					StoryPatternObject spo = (StoryPatternObject) eObject;

					if (spo.getName() != null && !"".equals(spo.getName()) && spo.getClassifier() != null)
					{
						contextInfos.put(spo.getName(), spo.getClassifier());
					}
				}
				else if (eObject instanceof VariableDeclarationAction)
				{
					VariableDeclarationAction vda = (VariableDeclarationAction) eObject;

					if (vda.getVariableName() != null && !"".equals(vda.getVariableName()) && vda.getClassifier() != null)
					{
						contextInfos.put(vda.getVariableName(), vda.getClassifier());
					}
				}
			}
		}

		return contextInfos;
	}

}
