package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkFeatureNameLabelEditPart;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;

public class CustomStoryPatternLinkFeatureNameLabelEditPart extends StoryPatternLinkFeatureNameLabelEditPart
{

	public CustomStoryPatternLinkFeatureNameLabelEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof StoryPatternLink)
		{
			adaptColorAndLabelText();
		}

		super.handleNotificationEvent(event);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		adaptColorAndLabelText();
	}

	protected void adaptColorAndLabelText()
	{
		((WrappingLabel) getFigure()).setTextWrap(true);
		((WrappingLabel) getFigure()).setTextJustification(PositionConstants.CENTER);

		Utility.adaptColor(getFigure(), ((StoryPatternElement) ((View) this.getModel()).getElement()).getModifier());

		StoryPatternLink link = (StoryPatternLink) ((View) getModel()).getElement();

		String modifierString = Utility.adaptStoryPatternElementModifierText(this);

		if (!"".equals(modifierString))
		{
			modifierString += System.getProperty("line.separator");
		}

		if (link.getEStructuralFeature() == null)
		{
			setLabelText(modifierString + "[null]");
		}
		else
		{
			setLabelText(modifierString + link.getEStructuralFeature().getName());
		}
	}
}
