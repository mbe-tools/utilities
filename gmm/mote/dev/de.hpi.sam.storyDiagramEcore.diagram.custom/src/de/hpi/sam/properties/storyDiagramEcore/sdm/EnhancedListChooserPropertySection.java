package de.hpi.sam.properties.storyDiagramEcore.sdm;

import de.hpi.sam.properties.AbstractListChooserPropertySection;
import de.hpi.sam.properties.SectionHelper;
import de.hpi.sam.storyDiagramEcore.sdm.provider.SdmItemProviderAdapterFactory;

public abstract class EnhancedListChooserPropertySection extends AbstractListChooserPropertySection
{
	@Override
	final protected Object[] getAvailableObjects()
	{
		return SectionHelper.getAvailableObjects(this.getEObject(), this.getFeature(), SdmItemProviderAdapterFactory.class);
	}
}
