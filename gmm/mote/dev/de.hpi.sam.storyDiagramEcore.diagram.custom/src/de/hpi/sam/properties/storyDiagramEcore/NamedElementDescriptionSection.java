package de.hpi.sam.properties.storyDiagramEcore;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractStringPropertySection;

public class NamedElementDescriptionSection extends AbstractStringPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE.getNamedElement_Description();
	}

	@Override
	protected String getLabelText()
	{
		return "Description";
	}

}
