package de.hpi.sam.properties;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;

public class SectionHelper
{
	public static Object[] getAvailableObjects(EObject eObject, EStructuralFeature feature, Class<?> clazz)
	{
		for (Adapter adapter : eObject.eAdapters())
		{
			if (adapter instanceof ItemProviderAdapter)
			{
				ItemProviderAdapter iAdapter = (ItemProviderAdapter) adapter;
				if (clazz.isInstance(iAdapter.getAdapterFactory()))
				{
					IItemPropertyDescriptor descriptor = iAdapter.getPropertyDescriptor(eObject, feature.getName());
					if (descriptor != null)
					{
						return descriptor.getChoiceOfValues(eObject).toArray();
					}
				}
			}
		}
		return new String[]
		{
			""
		};
	}
}
