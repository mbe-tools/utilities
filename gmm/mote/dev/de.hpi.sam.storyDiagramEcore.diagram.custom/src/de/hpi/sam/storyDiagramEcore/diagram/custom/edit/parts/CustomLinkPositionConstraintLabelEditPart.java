package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkPositionConstraintLabelEditPart;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;

public class CustomLinkPositionConstraintLabelEditPart extends LinkPositionConstraintLabelEditPart
{

	public CustomLinkPositionConstraintLabelEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof StoryPatternLink)
		{
			adaptColorAndLabelText();
		}

		super.handleNotificationEvent(event);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		adaptColorAndLabelText();
	}

	protected void adaptColorAndLabelText()
	{
		Utility.adaptColor(getFigure(), ((StoryPatternElement) ((View) getModel()).getElement()).getModifier());

		StoryPatternLink link = (StoryPatternLink) ((View) getModel()).getElement();

		String string = "";

		if (link.getLinkPositionConstraint() != null)
		{
			switch (link.getLinkPositionConstraint().getConstraintType())
			{
				case FIRST:
				{
					string += "first";
					break;
				}
				case LAST:
				{
					string += "last";
					break;
				}
				default:
				{
					throw new UnsupportedOperationException();
				}
			}

			string += ", ";
		}

		if (string.length() > 0)
		{
			string = string.substring(0, string.length() - 2);
			string = "{" + string + "}";
		}

		setLabelText(string);
	}
}
