package de.hpi.sam.properties.storyDiagramEcore.callActions;

import org.eclipse.emf.ecore.EStructuralFeature;

public class CallActionParameterParameterClassfierSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage.eINSTANCE.getCallActionParameter_ParameterClassfier();
	}

	@Override
	protected String getLabelText()
	{
		return "Parameter Classfier";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter) getEObject()).getParameterClassfier();
	}
}
