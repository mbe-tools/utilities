package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.ExpressionImport;
import de.hpi.sam.storyDiagramEcore.Import;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeImportsLabelEditPart;
import de.hpi.sam.storyDiagramEcore.nodes.InitialNode;

public class CustomInitialNodeImportsLabelEditPart extends InitialNodeImportsLabelEditPart
{

	public CustomInitialNodeImportsLabelEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void addNotationalListeners()
	{
		super.addNotationalListeners();

		InitialNode initialNode = (InitialNode) ((View) getModel()).getElement();

		addListenerFilter("Activity", this, initialNode.getActivity());
	}

	protected void updateFigure()
	{
		InitialNode initialNode = (InitialNode) ((View) getModel()).getElement();

		((WrappingLabel) getFigure()).setTextWrap(true);
		((WrappingLabel) getFigure()).setTextJustification(PositionConstants.RIGHT);

		if (initialNode.getActivity() != null && initialNode.getActivity().getImports() != null)
		{
			String imports = "";

			String[] pathParts;
			String anImportFileName = "";
			String anImportLanguage = "";

			for (Import anImport : initialNode.getActivity().getImports())
			{

				pathParts = ((ExpressionImport) anImport).getImportedFileURI().split("\\/");

				anImportFileName = pathParts[pathParts.length - 1];
				anImportLanguage = ((ExpressionImport) anImport).getExpressionLanguage();

				if (anImportFileName != null && !anImportFileName.equals(""))
				{
					if (anImportLanguage != null && !anImportLanguage.equals(""))
					{
						imports = imports.concat(anImportLanguage.concat(":"));
					}
					imports = imports.concat(anImportFileName.concat("\n"));
				}
			}

			setLabelText(imports);

		}
		else
		{
			setLabelText("");
		}

	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		updateFigure();
		super.handleNotificationEvent(event);
	}
}
