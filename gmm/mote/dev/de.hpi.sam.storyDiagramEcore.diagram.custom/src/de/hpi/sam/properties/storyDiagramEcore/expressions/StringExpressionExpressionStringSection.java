package de.hpi.sam.properties.storyDiagramEcore.expressions;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractStringPropertySection;

public class StringExpressionExpressionStringSection extends AbstractStringPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression_ExpressionString();
	}

	@Override
	protected String getLabelText()
	{
		return "Expression String";
	}

}
