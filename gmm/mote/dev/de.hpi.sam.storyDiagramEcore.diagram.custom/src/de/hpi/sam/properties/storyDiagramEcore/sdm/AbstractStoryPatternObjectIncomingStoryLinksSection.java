package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.providers.TabbedPropertiesLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;

import de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;

public class AbstractStoryPatternObjectIncomingStoryLinksSection extends EnhancedListChooserPropertySection
{

	@Override
	protected IBaseLabelProvider getLabelProvider()
	{
		return new TabbedPropertiesLabelProvider(StoryDiagramEcoreDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory());
	}

	@Override
	protected Object getListValues()
	{
		return ((AbstractStoryPatternObject) this.getEObject()).getIncomingStoryLinks();
	}

	@Override
	protected EStructuralFeature getFeature()
	{
		return SdmPackage.eINSTANCE.getAbstractStoryPatternObject_IncomingStoryLinks();
	}

	@Override
	protected String getLabelText()
	{
		return "Incoming Links";
	}

}
