package de.hpi.sam.storyDiagramEcore.diagram.custom.part.sdm;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

import de.hpi.sam.storyDiagramEcore.diagram.custom.part.CustomAbstractActionDelegate;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.SdmFactory;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;

public class SetLinkPositionConstraintAction extends CustomAbstractActionDelegate
{

	private StoryPatternLink		storyPatternLink;

	private ConnectionNodeEditPart	storyPatternLinkEditPart;

	@Override
	public void selectionChanged(IAction action, ISelection selection)
	{
		this.storyPatternLinkEditPart = null;
		this.storyPatternLink = null;
		if (selection instanceof IStructuredSelection)
		{
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			if (structuredSelection.getFirstElement() instanceof ConnectionNodeEditPart)
			{
				this.storyPatternLinkEditPart = (ConnectionNodeEditPart) structuredSelection.getFirstElement();
				this.storyPatternLink = (StoryPatternLink) this.storyPatternLinkEditPart.getPrimaryView().getElement();
			}
		}
	}

	@Override
	protected void doRun(IProgressMonitor progressMonitor)
	{

		AbstractTransactionalCommand command = new AbstractTransactionalCommand(this.storyPatternLinkEditPart.getEditingDomain(),
				"Add Link Index Constraint", null)
		{
			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
			{
				String text = SetLinkPositionConstraintAction.this.getAction().getText();

				if (text.toUpperCase().equals("NONE"))
				{
					SetLinkPositionConstraintAction.this.storyPatternLink.setLinkPositionConstraint(null);
				}
				else
				{
					LinkPositionConstraint linkIndexConstraint = SdmFactory.eINSTANCE.createLinkPositionConstraint();

					SetLinkPositionConstraintAction.this.storyPatternLink.setLinkPositionConstraint(linkIndexConstraint);

					switch (LinkPositionConstraintEnumeration.get(text.toUpperCase()))
					{
						case FIRST:
						{
							linkIndexConstraint.setConstraintType(LinkPositionConstraintEnumeration.FIRST);
							break;
						}
						case LAST:
						{
							linkIndexConstraint.setConstraintType(LinkPositionConstraintEnumeration.LAST);
							break;
						}
						default:
						{
							throw new UnsupportedOperationException();
						}
					}
				}

				return CommandResult.newOKCommandResult();
			}
		};

		execute(command);
	}
}
