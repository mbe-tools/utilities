package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EStructuralFeature;

public class AbstractStoryPatternLinkOppositeStoryPatternLinkSection extends EnhancedChooserPropertySection
{

	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getAbstractStoryPatternLink_OppositeStoryPatternLink();
	}

	@Override
	protected String getLabelText()
	{
		return "Opposite Link";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink) getEObject()).getOppositeStoryPatternLink();
	}

}