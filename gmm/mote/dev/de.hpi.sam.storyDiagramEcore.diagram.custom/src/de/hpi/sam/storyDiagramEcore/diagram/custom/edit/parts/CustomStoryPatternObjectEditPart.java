package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.custom.edit.policies.CustomStoryPatternObjectItemSemanticEditPolicy;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

public class CustomStoryPatternObjectEditPart extends StoryPatternObjectEditPart
{

	public CustomStoryPatternObjectEditPart(View view)
	{
		super(view);
	}

	@Override
	protected IFigure createNodeShape()
	{
		StoryPatternObjectFigure figure = new StoryPatternObjectFigure();
		return primaryShape = figure;
	}

	@Override
	protected void handleNotificationEvent(Notification notification)
	{
		if (notification.getNotifier() instanceof StoryPatternObject)
		{
			updateFigure();
		}

		super.handleNotificationEvent(notification);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	protected void updateFigure()
	{
		StoryPatternObject spo = (StoryPatternObject) ((View) getModel()).getElement();
		if (spo.getMatchType() == StoryPatternMatchTypeEnumeration.NEGATIVE)
		{
			IFigure crossFigure = (IFigure) ((IFigure) getFigure().getChildren().get(0)).getChildren().get(1);
			crossFigure.setVisible(true);
		}
		else if (spo.getMatchType() == StoryPatternMatchTypeEnumeration.OPTIONAL)
		{
			IFigure crossFigure = (IFigure) ((IFigure) getFigure().getChildren().get(0)).getChildren().get(1);
			crossFigure.setVisible(false);
			RectangleFigure outerRectangle = ((RectangleFigure) ((IFigure) ((RectangleFigure) getFigure().getChildren().get(0))
					.getChildren().get(0)).getChildren().get(0));
			outerRectangle.setLineStyle(Graphics.LINE_DASH);
			((RectangleFigure) outerRectangle.getChildren().get(0)).setLineStyle(Graphics.LINE_DASH);
		}
		else
		{
			IFigure crossFigure = (IFigure) ((IFigure) getFigure().getChildren().get(0)).getChildren().get(1);
			crossFigure.setVisible(false);

			RectangleFigure outerRectangle = ((RectangleFigure) ((IFigure) ((RectangleFigure) getFigure().getChildren().get(0))
					.getChildren().get(0)).getChildren().get(0));
			outerRectangle.setLineStyle(Graphics.LINE_SOLID);
			((RectangleFigure) outerRectangle.getChildren().get(0)).setLineStyle(Graphics.LINE_SOLID);

		}

		Utility.adaptColor((IFigure) ((IFigure) ((IFigure) getFigure().getChildren().get(0)).getChildren().get(0)).getChildren().get(0),
				((StoryPatternElement) ((View) this.getModel()).getElement()).getModifier());

	}

	@Override
	protected void createDefaultEditPolicies()
	{
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new CustomStoryPatternObjectItemSemanticEditPolicy());
	}

	public class StoryPatternObjectFigure extends
			de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.StoryPatternObjectFigureDescriptor
	{

		public StoryPatternObjectFigure()
		{
			super();

			Utility.adaptColor(this,
					((StoryPatternElement) ((View) CustomStoryPatternObjectEditPart.this.getModel()).getElement()).getModifier());
		}
	}

}
