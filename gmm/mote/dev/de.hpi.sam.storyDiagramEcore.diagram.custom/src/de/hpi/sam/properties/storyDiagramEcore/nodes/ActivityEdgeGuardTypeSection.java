package de.hpi.sam.properties.storyDiagramEcore.nodes;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractEnumerationPropertySection;

public class ActivityEdgeGuardTypeSection extends AbstractEnumerationPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE.getActivityEdge_GuardType();
	}

	@Override
	protected String getLabelText()
	{
		return "Guard Type";
	}

	@Override
	protected String[] getEnumerationFeatureValues()
	{
		org.eclipse.emf.ecore.EEnum eenum = de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE.getActivityEdgeGuardEnumeration();
		org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EEnumLiteral> eLiteralList = eenum.getELiterals();
		String[] result = new String[eLiteralList.size()];

		for (int i = 0; i < eLiteralList.size(); i++)
		{
			result[i] = eLiteralList.get(i).getLiteral();
		}

		return result;
	}

	@Override
	protected String getFeatureAsText()
	{
		return ((de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration) getOldFeatureValue()).getLiteral();
	}

	@Override
	protected Object getFeatureValue(int index)
	{
		return de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration.get(index);
	}

	@Override
	protected Object getOldFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) this.getEObject()).getGuardType();
	}

}
