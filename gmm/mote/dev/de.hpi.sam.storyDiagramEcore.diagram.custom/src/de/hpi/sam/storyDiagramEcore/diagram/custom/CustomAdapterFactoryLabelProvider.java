package de.hpi.sam.storyDiagramEcore.diagram.custom;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;

import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;

public class CustomAdapterFactoryLabelProvider extends
		AdapterFactoryLabelProvider {

	public CustomAdapterFactoryLabelProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
		// TODO Auto-generated constructor stub
	}
	@Override
	public String getText(Object element) {
		String s = ((CallActionExpression) element).toString(); 
		return s;
	}

}
