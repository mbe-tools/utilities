package de.hpi.sam.properties;

import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractChooserPropertySection;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchPart;

import de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin;

public abstract class FixedChooserPropertySection extends AbstractChooserPropertySection
{
	@Override
	final protected void addListener()
	{
		super.addListener();

		if (this.getEObject() == null)
		{
			return;
		}

		StoryDiagramEcoreDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory()
				.adapt(this.getEObject(), IItemLabelProvider.class);
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
	}
}
