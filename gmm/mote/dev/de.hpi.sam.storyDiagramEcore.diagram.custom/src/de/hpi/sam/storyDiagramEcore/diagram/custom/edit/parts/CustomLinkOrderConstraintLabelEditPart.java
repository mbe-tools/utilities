package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintLabelEditPart;
import de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint;

public class CustomLinkOrderConstraintLabelEditPart extends LinkOrderConstraintLabelEditPart
{

	public CustomLinkOrderConstraintLabelEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof LinkOrderConstraint)
		{
			adaptLabelText();
		}

		super.handleNotificationEvent(event);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		adaptLabelText();
	}

	protected void adaptLabelText()
	{
		setForegroundColor(ColorConstants.gray);
		setFontColor(ColorConstants.gray);

		LinkOrderConstraint linkOderingConstraint = (LinkOrderConstraint) ((View) getModel()).getElement();

		switch (linkOderingConstraint.getConstraintType())
		{
			case DIRECT_SUCCESSOR:
			{
				setLabelText("{next}");
				break;
			}
			case SUCCESSOR:
			{
				setLabelText("{...}");
				break;
			}
			default:
			{
				throw new UnsupportedOperationException();
			}
		}
	}
}
