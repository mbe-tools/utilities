package de.hpi.sam.storyDiagramEcore.diagram.custom.layout;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.diagram.ui.services.layout.ILayoutNodeOperation;
import org.eclipse.gmf.runtime.notation.View;

public class TopDownProvider extends org.eclipse.gmf.runtime.diagram.ui.providers.TopDownProvider
{
	public static String	ID	= "TopDownProvider";

	@Override
	public boolean provides(IOperation operation)
	{
		Assert.isNotNull(operation);

		View cview = getContainer(operation);
		if (cview == null)
		{
			return false;
		}

		IAdaptable layoutHint = ((ILayoutNodeOperation) operation).getLayoutHint();
		String layoutType = (String) layoutHint.getAdapter(String.class);
		return ID.equals(layoutType);
	}
}