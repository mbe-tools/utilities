package de.hpi.sam.storyDiagramEcore.diagram.custom.providers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.UnexecutableCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.CopyToClipboardCommand;
import org.eclipse.emf.edit.command.CutToClipboardCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.EMFCommandOperation;
import org.eclipse.gef.editparts.AbstractEditPart;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.ui.action.global.GlobalActionId;
import org.eclipse.gmf.runtime.common.ui.services.action.global.IGlobalActionContext;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramWorkbenchPart;
import org.eclipse.gmf.runtime.diagram.ui.providers.DiagramGlobalActionHandler;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPart;

public class GlobalActionHandler extends DiagramGlobalActionHandler
{
	protected TransactionalEditingDomain	domain;

	protected IStructuredSelection			selection;

	protected Command						command;

	// Kopiert aus Superklasse, da Sichtbarkeit private
	protected TransactionalEditingDomain getEditingDomain(IDiagramWorkbenchPart part)
	{
		TransactionalEditingDomain result = null;
		IEditingDomainProvider provider = (IEditingDomainProvider) part.getAdapter(IEditingDomainProvider.class);

		if (provider != null)
		{
			EditingDomain domain = provider.getEditingDomain();

			if (domain != null && domain instanceof TransactionalEditingDomain)
			{
				result = (TransactionalEditingDomain) domain;
			}
		}

		return result;
	}

	public boolean canHandle(final IGlobalActionContext cntxt)
	{
		boolean result = false;

		/* Check if the active part is a IDiagramWorkbenchPart */
		IWorkbenchPart part = cntxt.getActivePart();
		if (!(part instanceof IDiagramWorkbenchPart))
		{
			return result;
		}

		this.domain = getEditingDomain((IDiagramWorkbenchPart) part);
		if (this.domain == null)
		{
			return result;
		}

		/* Check if the selection is a structured selection */
		ISelection sel = cntxt.getSelection();
		if (!(sel instanceof IStructuredSelection))
		{
			return result;
		}
		this.selection = (IStructuredSelection) sel;

		/* Check the action id */
		String actionId = cntxt.getActionId();
		if (actionId.equals(GlobalActionId.COPY))
		{
			result = canCopy(cntxt);
		}
		else if (actionId.equals(GlobalActionId.CUT))
		{
			result = canCut(cntxt);
		}
		else if (actionId.equals(GlobalActionId.PASTE))
		{
			result = canPaste(cntxt);
		}

		return result;
	}

	// strongly influenced by PasteFromClipboardCommand.optimizedCanExecute()
	protected boolean canPaste(IGlobalActionContext cntxt)
	{
		Object elem = this.selection.getFirstElement();
		if (!(elem instanceof AbstractEditPart))
		{
			return false;
		}

		this.command = AddCommand.create(this.domain, ((View) ((AbstractEditPart) elem).getModel()).getElement(), null,
				this.domain.getClipboard());
		if (this.command instanceof UnexecutableCommand)
		{
			return false;
		}

		return this.command.canExecute();
	}

	public ICommand getCommand(IGlobalActionContext cntxt)
	{
		/* Check if the active part is a IDiagramWorkbenchPart */
		IWorkbenchPart part = cntxt.getActivePart();
		if (!(part instanceof IDiagramWorkbenchPart))
		{
			return null;
		}

		/* Get the model operation context */
		IDiagramWorkbenchPart diagramPart = (IDiagramWorkbenchPart) part;

		/* Create a command */
		ICommand command = null;

		/* Check the action id */
		String actionId = cntxt.getActionId();
		if (actionId.equals(GlobalActionId.COPY))
		{
			command = getCopyCommand(cntxt, diagramPart, false);
		}
		else if (actionId.equals(GlobalActionId.CUT))
		{
			command = getCutCommand(cntxt, diagramPart);
		}
		else if (actionId.equals(GlobalActionId.PASTE))
		{
			command = getPasteCommand(cntxt, diagramPart);
		}

		return command;
	}

	// zurück geliefertes CompositeTransactionalCommand kann per se rückgängig
	// gemacht werden => isUndoable bedeutungslos
	protected ICommand getCopyCommand(IGlobalActionContext cntxt, IDiagramWorkbenchPart diagramPart, final boolean isUndoable)
	{
		Iterator<?> it = this.selection.iterator();
		List<EObject> objects2BeCopied = new ArrayList<EObject>();

		Object currObject;
		while (it.hasNext())
		{
			currObject = it.next();

			if (!(currObject instanceof AbstractEditPart))
			{
				continue;
			}

			objects2BeCopied.add(((View) ((AbstractEditPart) currObject).getModel()).getElement());
		}

		CompositeTransactionalCommand ctc = new CompositeTransactionalCommand(this.domain, "");
		ctc.add(new EMFCommandOperation(this.domain, CopyToClipboardCommand.create(this.domain, objects2BeCopied)));
		return ctc;
	}

	protected ICommand getCutCommand(IGlobalActionContext cntxt, IDiagramWorkbenchPart diagramPart)
	{
		Iterator<?> it = this.selection.iterator();
		List<EObject> objects2BeCutted = new ArrayList<EObject>();

		Object currObject;
		while (it.hasNext())
		{
			currObject = it.next();

			if (!(currObject instanceof AbstractEditPart))
			{
				continue;
			}

			objects2BeCutted.add(((View) ((AbstractEditPart) currObject).getModel()).getElement());
		}

		CompositeTransactionalCommand ctc = new CompositeTransactionalCommand(this.domain, "");
		ctc.add(new EMFCommandOperation(this.domain, CutToClipboardCommand.create(this.domain, objects2BeCutted)));
		return ctc;
	}

	protected ICommand getPasteCommand(IGlobalActionContext cntxt, IDiagramWorkbenchPart diagramPart)
	{
		CompositeTransactionalCommand ctc = new CompositeTransactionalCommand(this.domain, "");
		ctc.add(new EMFCommandOperation(this.domain, this.command));
		return ctc;
	}

}