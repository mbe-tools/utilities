package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreUtils;
import de.hpi.sam.storyDiagramEcore.diagram.edit.commands.StoryPatternLinkReorientCommand;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;

public class CustomStoryPatternLinkReorientCommand extends StoryPatternLinkReorientCommand
{

	public CustomStoryPatternLinkReorientCommand(ReorientRelationshipRequest request)
	{
		super(request);
	}

	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
	{
		if (!canExecute())
		{
			throw new ExecutionException("Invalid arguments in reorient link command"); //$NON-NLS-1$
		}

		CommandResult result;
		if (this.reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE)
		{
			result = this.reorientSource();
		}
		else if (this.reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET)
		{
			result = this.reorientTarget();
		}
		else
		{
			throw new IllegalStateException();
		}

		if (this.getLink().getOppositeStoryPatternLink() != null)
		{
			this.getLink().getOppositeStoryPatternLink().setOppositeStoryPatternLink(null);
			this.getLink().setOppositeStoryPatternLink(null);
		}

		EStructuralFeature feature = null;
		if (this.getLink().getSource().getClassifier() instanceof EClass)
		{
			feature = StoryDiagramEcoreUtils.getConnectingEStructuralFeature((EClass) this.getLink().getSource().getClassifier(), this
					.getLink().getTarget().getClassifier());
		}
		this.getLink().setEStructuralFeature(feature);

		if (feature instanceof EReference && ((EReference) feature).getEOpposite() != null)
		{
			StoryPatternLink opLink = StoryDiagramEcoreUtils.getStoryPatternLinkInCase(this.getLink().getTarget(), this.getLink()
					.getSource(), ((EReference) feature).getEOpposite());

			if (opLink != null)
			{
				this.getLink().setOppositeStoryPatternLink(opLink);
				opLink.setOppositeStoryPatternLink(this.getLink());
			}
		}

		return result;
	}
}
