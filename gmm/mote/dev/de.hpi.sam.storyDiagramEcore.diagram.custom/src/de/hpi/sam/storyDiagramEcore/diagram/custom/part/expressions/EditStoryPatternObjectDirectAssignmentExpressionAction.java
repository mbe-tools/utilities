package de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

public class EditStoryPatternObjectDirectAssignmentExpressionAction extends EditExpressionAction
{

	@Override
	protected EClassifier getExpectedClassifier()
	{
		return ((StoryPatternObject) this.getExpressionOwner()).getClassifier();
	}

	@Override
	protected Expression getExpression()
	{
		return ((StoryPatternObject) this.getExpressionOwner()).getDirectAssignmentExpression();
	}

	@Override
	protected TransactionalEditingDomain getEditingDomain()
	{
		return ((StoryPatternObjectEditPart) getStructuredSelection().getFirstElement()).getEditingDomain();
	}

	@Override
	protected void setNewExpression(Expression newExpression)
	{
		StoryPatternObject spo = (StoryPatternObject) this.getExpressionOwner();

		spo.setDirectAssignmentExpression(newExpression);
	}

	@Override
	protected EClassifier getContextClassifier()
	{
		return ((StoryPatternObject) this.getExpressionOwner()).getClassifier();
	}

	@Override
	protected Activity getActivity()
	{
		EObject container = (StoryActionNode) ((View) ((StoryPatternObjectEditPart) getStructuredSelection().getFirstElement()).getModel())
				.getElement().eContainer();

		while (!(container instanceof Activity))
		{
			container = container.eContainer();
		}

		return (Activity) container;
	}

	@Override
	protected EObject getExpressionOwner()
	{
		return ((View) ((StoryPatternObjectEditPart) getStructuredSelection().getFirstElement()).getModel()).getElement();
	}

	@Override
	protected void prepareDialog()
	{
		super.prepareDialog();
		editExpressionDialog.setStructuralFeature(SdmPackage.eINSTANCE.getStoryPatternObject_DirectAssignmentExpression());
	}
}
