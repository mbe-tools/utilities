package de.hpi.sam.storyDiagramEcore.diagram.custom;

import org.eclipse.draw2d.IFigure;

public interface ILineIsHideableFigure extends IFigure
{
	public boolean isHideLine();

	public void setHideLine(boolean hideLine);
}
