package de.hpi.sam.storyDiagramEcore.diagram.custom;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart;

/**
 * @author Patrick Rein
 * 
 */
public abstract class ActivityEditListFeatureDialog extends Dialog
{

	protected int		DIALOG_WIDTH			= 500;
	protected int		DIALOG_HEIGHT			= 200;

	protected int		TEXT_INPUT_CONFIG		= SWT.BORDER | SWT.SINGLE;
	protected int		COMBO_INPUT_CONFIG		= SWT.BORDER | SWT.SINGLE;
	protected int		TEXT_INPUT_HEIGHT		= 18;

	protected String	DIALOG_TITLE			= "Manage Items";
	protected String	ADD_BUTTON_TEXT			= "Add Item";
	protected String	ITEM_CONTAINER_TEXT		= "Items";
	protected String	DELETE_ITEM_BUTTON_TEXT	= "Remove";

	protected ActivityEditListFeatureDialog(Shell parentShell)
	{
		super(parentShell);
	}

	protected Activity				activity;
	protected InitialNodeEditPart	initialNodeEditPart;
	protected Group					itemContainer;

	/**
	 * In this method a group for every list item should be created. The parent
	 * widget of those groups should be the itemContainer.
	 */
	protected abstract void fillItemsContainer();

	/**
	 * This method should return a listener which implements the behavior of the
	 * dialog when the "add an item" button is pressed.
	 */
	protected abstract SelectionListener createAddButtonListener();

	protected Control createDialogArea(Composite parent)
	{
		this.getShell().setText(DIALOG_TITLE);

		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setSize(DIALOG_WIDTH, DIALOG_HEIGHT);
		composite.setLayout(new RowLayout(SWT.VERTICAL));

		itemContainer = new Group(composite, SWT.NONE);
		itemContainer.setText(ITEM_CONTAINER_TEXT);
		itemContainer.setLayout(new RowLayout(SWT.VERTICAL));
		this.fillItemsContainer();

		Button addImportButton = new Button(composite, SWT.NONE);
		addImportButton.setText(ADD_BUTTON_TEXT);
		addImportButton.addSelectionListener(this.createAddButtonListener());

		return composite;
	}

	/**
	 * refreshVisually() refreshes the dialog area so that is adjusts to new
	 * items.
	 */
	protected void refreshVisually()
	{
		((Composite) this.getDialogArea()).layout(true);
		this.getShell().pack();
		itemContainer.layout();
	}

	public void setActivity(Activity activity)
	{
		this.activity = activity;
	}

	public void setInitialNodeEditPart(InitialNodeEditPart initialNodeEditPart)
	{
		this.initialNodeEditPart = initialNodeEditPart;
	}

}
