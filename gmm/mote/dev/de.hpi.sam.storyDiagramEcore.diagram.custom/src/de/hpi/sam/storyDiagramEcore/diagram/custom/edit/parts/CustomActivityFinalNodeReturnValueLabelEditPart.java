package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.impl.EcoreFactoryImpl;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.ActivityParameter;
import de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum;
import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeReturnValueLabelEditPart;
import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;

public class CustomActivityFinalNodeReturnValueLabelEditPart extends ActivityFinalNodeReturnValueLabelEditPart implements IMLExpressionEditPart {

	public CustomActivityFinalNodeReturnValueLabelEditPart(View view)
	{
		super(view);
	}

	/*
	 * should be similar to addNotationalListeners of:
	 * 
	 * CustomActivityEdgeGuardConstraintLabelEditPart
	 * CustomActivityFinalNodeReturnValueLabelEditPart
	 * CustomAttributeAssignmentEditPart CustomCallActionExpressionEditPart
	 * CustomCallActionExpression2EditPart CustomCallActionExpression3EditPart
	 * CustomStoryPatternExpressionLinkFeatureNameLabelEditPart
	 * CustomStoryPatternObjectClassifierLabelEditPart
	 */
	@Override
	protected void addNotationalListeners()
	{
		super.addNotationalListeners();
		Expression expr = ((ActivityFinalNode) ((View) getModel()).getElement()).getReturnValue();

		if (expr != null)
		{
			this.addListenerFilter(Utility.getFilterId((NamedElement) expr), this, expr);

			if (expr instanceof CallActionExpression)
			{
				TreeIterator<EObject> it = ((CallActionExpression) expr).eAllContents();
				for (EObject eObj; it.hasNext();)
				{
					eObj = it.next();
					this.addListenerFilter(Utility.getFilterId((NamedElement) eObj), this, eObj);
				}
			}
		}
		ActivityFinalNode finalNode = (ActivityFinalNode) ((View) getModel()).getElement();
		addListenerFilter("Activity", this, finalNode.getActivity());
		this.addListener();
	}
	private void addListener(){
		ActivityFinalNode finalNode = (ActivityFinalNode) ((View) getModel()).getElement();
		if (finalNode != null)
			for (int i = 0;i <finalNode.getActivity().getParameters().size();i++)
				addListenerFilter("Paramter" + i, this, (EObject) finalNode.getActivity().getParameters().toArray()[i]);
	}

	/*
	 * should be similar to handleNotificationEvent of:
	 * 
	 * CustomActivityEdgeGuardConstraintLabelEditPart
	 * CustomActivityFinalNodeReturnValueLabelEditPart
	 * CustomAttributeAssignmentEditPart CustomCallActionExpressionEditPart
	 * CustomCallActionExpression2EditPart CustomCallActionExpression3EditPart
	 * CustomStoryPatternExpressionLinkFeatureNameLabelEditPart
	 * CustomStoryPatternObjectClassifierLabelEditPart
	 */
	@Override
	protected void handleNotificationEvent(Notification event)
	{
		Object notifier = event.getNotifier();
		Object newValue = event.getNewValue();
		Object oldValue = event.getOldValue();
		int eventType = event.getEventType();

		if ((eventType == Notification.SET || eventType == Notification.ADD)
				&& (newValue instanceof CallAction || newValue instanceof Expression || newValue instanceof CallActionParameter))
		{
			this.addListenerFilter(Utility.getFilterId((NamedElement) newValue), this, (EObject) newValue);
			updateFigure();
		}
		else if (eventType == Notification.REMOVE
				&& (oldValue instanceof CallAction || oldValue instanceof Expression || oldValue instanceof CallActionParameter))
		{
			this.removeListenerFilter(Utility.getFilterId((NamedElement) oldValue));
			updateFigure();
		}
		else if (notifier instanceof ActivityFinalNode || notifier instanceof CallAction || notifier instanceof Expression
				|| notifier instanceof CallActionParameter)
		{
			updateFigure();
		}
		updateFigure();
		super.handleNotificationEvent(event);
	}

	protected void updateFigure()
	{
		((WrappingLabel) getFigure()).setTextWrap(true);
		((WrappingLabel) getFigure()).setTextJustification(PositionConstants.CENTER);

		/*Expression returnValue = ((ActivityFinalNode) ((View) getModel()).getElement()).getReturnValue();

		String text = "";
		if (returnValue != null)
		{
			text = returnValue.toString();
		}
		else
		{
			text = "";
		}*/
		StringBuilder labelBuilder = new StringBuilder();

		EParameter aParameter;
		String s = "";
		for (int i = 0; i<this.getOutParameters().size();i++){
			aParameter = this.getOutParameters().get(i);
			if (aParameter != null && aParameter.getEType() != null)
			{
				s = aParameter.getEType().getName();
				s = s + " " + this.getOutParameters().get(i).getName();
			}

			labelBuilder.append(s);//exp[i].getName()/*((ActivityFinalNode) ((View) getModel()).getElement()).getName()*/ + "-> " + exp[i].toString());
			if (i<this.getOutParameters().size()-1) labelBuilder.append(System.getProperty("line.separator"));
		}

		setLabelText(labelBuilder.toString());
	}
	private List<EParameter> getOutParameters()
	{
		Activity activity = ((ActivityFinalNode) ((View) getModel()).getElement()).getActivity();
		List<EParameter> outgoingParameters = null;//new ArrayList<EParameter>();
		if (outgoingParameters == null)
		{
			EParameter tempParam;
			List<EParameter> resultParameters = new ArrayList<EParameter>();
			if (activity.getSpecification() != null)
			{
				tempParam = EcoreFactoryImpl.eINSTANCE.createEParameter();
				tempParam.setName(activity.getSpecification().getName());
				tempParam.setEType(activity.getSpecification().getEType());
				resultParameters.add(tempParam);
			}
			else
			{
				for (ActivityParameter aParameter : activity.getParameters())
				{
					if (aParameter.getDirection() == ActivityParameterDirectionEnum.INOUT
							|| aParameter.getDirection() == ActivityParameterDirectionEnum.OUT)
					{
						tempParam = EcoreFactoryImpl.eINSTANCE.createEParameter();
						tempParam.setEType(aParameter.getType());
						tempParam.setName(aParameter.getName());
						resultParameters.add(tempParam);
					}
				}
			}
			outgoingParameters = resultParameters;
		}

		return outgoingParameters;
	}
	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	@Override
	public Expression getExpression() {
		for ( final Expression expression : ( (ActivityFinalNode) ( (View) getModel() ).getElement() ).getOutParameterValues() ) {
			return expression;
		}
		
		
		return null;
	}
}
