package de.hpi.sam.properties.storyDiagramEcore.callActions;

import de.hpi.sam.properties.FixedChooserPropertySection;
import de.hpi.sam.properties.SectionHelper;
import de.hpi.sam.storyDiagramEcore.callActions.provider.CallActionsItemProviderAdapterFactory;

public abstract class EnhancedChooserPropertySection extends FixedChooserPropertySection
{
	@Override
	final protected Object[] getComboFeatureValues()
	{
		return SectionHelper.getAvailableObjects(this.getEObject(), this.getFeature(), CallActionsItemProviderAdapterFactory.class);
	}
}
