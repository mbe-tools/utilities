package de.hpi.sam.storyDiagramEcore.diagram.custom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.command.UnexecutableCommand;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.query.conditions.booleans.BooleanCondition;
import org.eclipse.emf.query.conditions.eobjects.EObjectCondition;
import org.eclipse.emf.query.conditions.eobjects.EObjectInstanceCondition;
import org.eclipse.emf.query.conditions.eobjects.structuralfeatures.EObjectAttributeValueCondition;
import org.eclipse.emf.query.conditions.eobjects.structuralfeatures.EObjectReferenceValueCondition;
import org.eclipse.emf.query.statements.FROM;
import org.eclipse.emf.query.statements.SELECT;
import org.eclipse.emf.query.statements.WHERE;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.widgets.Widget;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreUtils;
import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsFactory;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction;
import de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction;
import de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction;
import de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsFactory;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage;
import de.hpi.sam.storyDiagramEcore.expressions.StringExpression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

// ############################################################################
//
// If a new expression type or call action type was created, just create a new
// class implementing IView and add it to the VIEW_MAP.
//
// ############################################################################

public abstract class CommonEditExpressionDialog extends Dialog
{
	static final String	SEPARATOR	= " ";

	static interface IView
	{
		/**
		 * activates necessary input elements, such as text input fields,
		 * buttons or combos; called as a consequence of selection change
		 */
		public void adaptInputElements();

		/**
		 * wraps the appropriate generated factory, ie. method create of class
		 * XYView should use ABCPackage.eINSTANCE.createXY()
		 * 
		 * @return the newly created object
		 */
		public EObject create();

		/**
		 * If text input fields are necessary to edit the encapsulated types
		 * objects, this method provides the opportunity to save the text of a
		 * text input field before eg. the editor is closed or another object is
		 * selected
		 */
		public void processCurrentInput();

		public Command getExtraCommand4Addition(TransactionalEditingDomain domain);

		public Command getExtraCommand4Removal(TransactionalEditingDomain domain);
	}

	abstract class AbstractView implements IView
	{
		@Override
		public void adaptInputElements()
		{
			CommonEditExpressionDialog.this.adaptInputElementsDefault();
		}

		@Override
		public void processCurrentInput()
		{
		}

		@Override
		public Command getExtraCommand4Addition(TransactionalEditingDomain domain)
		{
			return null;
		}

		@Override
		public Command getExtraCommand4Removal(TransactionalEditingDomain domain)
		{
			return null;
		}
	}

	abstract class ExpressionView extends AbstractView
	{
		@Override
		public Command getExtraCommand4Addition(TransactionalEditingDomain domain)
		{
			if (CommonEditExpressionDialog.this.currentTreeValue instanceof ActivityEdge
					&& CommonEditExpressionDialog.this.feature.equals(NodesPackage.eINSTANCE.getActivityEdge_GuardExpression())
					&& ((ActivityEdge) CommonEditExpressionDialog.this.currentTreeValue).getGuardType() != ActivityEdgeGuardEnumeration.BOOLEAN)
			{
				return domain.createCommand(SetCommand.class, new CommandParameter(CommonEditExpressionDialog.this.currentTreeValue,
						NodesPackage.eINSTANCE.getActivityEdge_GuardType(), ActivityEdgeGuardEnumeration.BOOLEAN));
			}

			return null;
		}

		@Override
		public Command getExtraCommand4Removal(TransactionalEditingDomain domain)
		{
			EObject container = CommonEditExpressionDialog.this.currentTreeValue.eContainer();
			if (CommonEditExpressionDialog.this.currentTreeValue instanceof Expression && container instanceof ActivityEdge
					&& CommonEditExpressionDialog.this.feature.equals(NodesPackage.eINSTANCE.getActivityEdge_GuardExpression())
					&& ((ActivityEdge) container).getGuardType() == ActivityEdgeGuardEnumeration.BOOLEAN)
			{
				return domain.createCommand(SetCommand.class,
						new CommandParameter(container, NodesPackage.eINSTANCE.getActivityEdge_GuardType(),
								ActivityEdgeGuardEnumeration.NONE));
			}

			return null;
		}
	}

	class CallActionExpressionView extends ExpressionView
	{
		@Override
		public EObject create()
		{
			return ExpressionsFactory.eINSTANCE.createCallActionExpression();
		}
	}

	abstract class HasClassifierView extends AbstractView
	{
		abstract public EClassifier getClassifier();
	}

	class CallActionParameterView extends HasClassifierView
	{
		@Override
		public void adaptInputElements()
		{
			super.adaptInputElements();
			CommonEditExpressionDialog.this.addOrResetClassifierfield();

			String name = ((CallActionParameter) CommonEditExpressionDialog.this.currentTreeValue).getName();
			if (name != null)
			{
				CommonEditExpressionDialog.this.addOrResetExpressionField("Name:", name);
			}
			else
			{
				CommonEditExpressionDialog.this.addOrResetExpressionField("Name:", "");
			}

			CommonEditExpressionDialog.this.classifierCombo.addSelectionListener(CommonEditExpressionDialog.this
					.createComboSelectionListener(CallActionsPackage.eINSTANCE.getCallActionParameter_ParameterClassfier(),
							CommonEditExpressionDialog.this.classifierCombo, CommonEditExpressionDialog.this.classifierValues));
		}

		@Override
		public EObject create()
		{
			return CallActionsFactory.eINSTANCE.createCallActionParameter();
		}

		@Override
		public EClassifier getClassifier()
		{
			return ((CallActionParameter) CommonEditExpressionDialog.this.currentTreeValue).getParameterClassfier();
		}

		@Override
		public void processCurrentInput()
		{
			CallActionParameter cap = (CallActionParameter) CommonEditExpressionDialog.this.currentTreeValue;
			if (!CommonEditExpressionDialog.this.expressionText.getText().equals(cap.getName()))
			{
				TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(cap);
				Command command = domain.createCommand(SetCommand.class,
						new CommandParameter(cap, StoryDiagramEcorePackage.eINSTANCE.getNamedElement_Name(),
								CommonEditExpressionDialog.this.expressionText.getText()));
				domain.getCommandStack().execute(command);
			}
		}
	}

	abstract class CallActionView extends HasClassifierView
	{
		@Override
		public void adaptInputElements()
		{
			super.adaptInputElements();
			CommonEditExpressionDialog.this.addOrResetClassifierfield();
			CommonEditExpressionDialog.this.classifierCombo.addSelectionListener(CommonEditExpressionDialog.this
					.createComboSelectionListener(CallActionsPackage.eINSTANCE.getCallAction_Classifier(),
							CommonEditExpressionDialog.this.classifierCombo, CommonEditExpressionDialog.this.classifierValues));
		}

		@Override
		public EClassifier getClassifier()
		{
			return ((CallAction) CommonEditExpressionDialog.this.currentTreeValue).getClassifier();
		}
	}

	class CallSDInterpreterActionView extends CallActionView
	{
		@Override
		public void adaptInputElements()
		{
			super.adaptInputElements();

			if (((CallStoryDiagramInterpreterAction) CommonEditExpressionDialog.this.currentTreeValue).getActivity() != null)
			{
				int i;

				for (i = 0; i < CommonEditExpressionDialog.this.activityValues.toArray().length; i++)
				{
					if (((Activity) CommonEditExpressionDialog.this.activityValues.toArray()[i]).getName() != null
							&& ((Activity) CommonEditExpressionDialog.this.activityValues.toArray()[i]).getName().equals(
									((CallStoryDiagramInterpreterAction) CommonEditExpressionDialog.this.currentTreeValue).getActivity()
											.getName()))
					{
						break;
					}
				}

				CommonEditExpressionDialog.this.addOrResetActivityfield(i);
			}
			else
			{
				CommonEditExpressionDialog.this.addOrResetActivityfield(-1);
			}

			CommonEditExpressionDialog.this.activityCombo.addSelectionListener(new SelectionListener()
			{
				@Override
				public void widgetDefaultSelected(SelectionEvent e)
				{
					this.widgetSelected(e);
				}

				@Override
				public void widgetSelected(SelectionEvent e)
				{
					TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(CommonEditExpressionDialog.this.currentTreeValue);

					NamedElement nElem = null;
					Iterator<EObject> it = CommonEditExpressionDialog.this.activityValues.iterator();
					while (it.hasNext())
					{
						nElem = (NamedElement) it.next();

						if (nElem.getName() != null && nElem.getName().equals(CommonEditExpressionDialog.this.activityCombo.getText()))
						{
							break;
						}
					}

					Command command = domain.createCommand(
							SetCommand.class,
							new CommandParameter(CommonEditExpressionDialog.this.currentTreeValue, CallActionsPackage.eINSTANCE
									.getCallStoryDiagramInterpreterAction_Activity(), nElem));
					domain.getCommandStack().execute(command);
				}
			});
		}

		@Override
		public EObject create()
		{
			return CallActionsFactory.eINSTANCE.createCallStoryDiagramInterpreterAction();
		}
	}

	class CompareActionView extends CallActionView
	{
		@Override
		public EObject create()
		{
			return CallActionsFactory.eINSTANCE.createCompareAction();
		}
	}

	class LiteralDeclarationActionView extends CallActionView
	{
		@Override
		public void adaptInputElements()
		{
			super.adaptInputElements();

			if (((LiteralDeclarationAction) CommonEditExpressionDialog.this.currentTreeValue).getLiteral() != null)
			{
				CommonEditExpressionDialog.this.addOrResetExpressionField("Literal:",
						((LiteralDeclarationAction) CommonEditExpressionDialog.this.currentTreeValue).getLiteral());
			}
			else
			{
				CommonEditExpressionDialog.this.addOrResetExpressionField("Literal:", "");
			}
		}

		@Override
		public EObject create()
		{
			return CallActionsFactory.eINSTANCE.createLiteralDeclarationAction();
		}

		@Override
		public void processCurrentInput()
		{
			LiteralDeclarationAction lda = (LiteralDeclarationAction) CommonEditExpressionDialog.this.currentTreeValue;
			if (!CommonEditExpressionDialog.this.expressionText.getText().equals(lda.getLiteral()))
			{
				TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(lda);
				Command command = domain.createCommand(SetCommand.class,
						new CommandParameter(lda, CallActionsPackage.eINSTANCE.getLiteralDeclarationAction_Literal(),
								CommonEditExpressionDialog.this.expressionText.getText()));
				domain.getCommandStack().execute(command);
			}
		}
	}

	class MethodCallActionView extends CallActionView
	{
		@Override
		public void adaptInputElements()
		{
			super.adaptInputElements();

			if (((MethodCallAction) CommonEditExpressionDialog.this.currentTreeValue).getMethodClassName() != null)
			{
				CommonEditExpressionDialog.this.addOrResetExpressionField("Class Name:",
						((MethodCallAction) CommonEditExpressionDialog.this.currentTreeValue).getMethodClassName());
			}
			else
			{
				CommonEditExpressionDialog.this.addOrResetExpressionField("Class Name:", "");
			}

			if (((MethodCallAction) CommonEditExpressionDialog.this.currentTreeValue).getMethodName() != null)
			{
				CommonEditExpressionDialog.this.addOrResetVariableField("Method Name:",
						((MethodCallAction) CommonEditExpressionDialog.this.currentTreeValue).getMethodName());
			}
			else
			{
				CommonEditExpressionDialog.this.addOrResetVariableField("Method Name:", "");
			}

			if (((MethodCallAction) CommonEditExpressionDialog.this.currentTreeValue).getMethod() != null)
			{
				int i;
				
				for (i = 0; i < CommonEditExpressionDialog.this.operationValues.toArray().length; i++)
				{
					if (((EOperation) CommonEditExpressionDialog.this.operationValues.toArray()[i]).getName().equals(
							((MethodCallAction) CommonEditExpressionDialog.this.currentTreeValue).getMethod().getName()))
					{
						break;
					}
				}
				CommonEditExpressionDialog.this.addOrResetOperationField(i);
			}
			else
			{
				CommonEditExpressionDialog.this.addOrResetOperationField(-1);
			}

			CommonEditExpressionDialog.this.operationCombo.addSelectionListener(CommonEditExpressionDialog.this
					.createComboSelectionListener(CallActionsPackage.eINSTANCE.getMethodCallAction_Method(),
							CommonEditExpressionDialog.this.operationCombo, CommonEditExpressionDialog.this.operationValues));
		}

		@Override
		public EObject create()
		{
			return CallActionsFactory.eINSTANCE.createMethodCallAction();
		}

		@Override
		public void processCurrentInput()
		{
			CompoundCommand cc = new CompoundCommand();
			MethodCallAction mca = (MethodCallAction) CommonEditExpressionDialog.this.currentTreeValue;
			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(mca);

			if (!CommonEditExpressionDialog.this.expressionText.getText().equals(mca.getMethodClassName()))
			{
				cc.append(domain.createCommand(SetCommand.class,
						new CommandParameter(mca, CallActionsPackage.eINSTANCE.getMethodCallAction_MethodClassName(),
								CommonEditExpressionDialog.this.expressionText.getText())));
			}

			if (!CommonEditExpressionDialog.this.variableName.getText().equals(mca.getMethodName()))
			{
				cc.append(domain.createCommand(SetCommand.class,
						new CommandParameter(mca, CallActionsPackage.eINSTANCE.getMethodCallAction_MethodName(),
								CommonEditExpressionDialog.this.variableName.getText())));
			}

			Command command = cc.unwrap();
			if (!command.equals(UnexecutableCommand.INSTANCE))
			{
				domain.getCommandStack().execute(command);
			}
		}
	}

	class NewObjectActionView extends CallActionView
	{
		@Override
		public EObject create()
		{
			return CallActionsFactory.eINSTANCE.createNewObjectAction();
		}
	}

	class NullValueActionView extends CallActionView
	{
		@Override
		public EObject create()
		{
			return CallActionsFactory.eINSTANCE.createNullValueAction();
		}
	}

	class OperationActionView extends CallActionView
	{
		@Override
		public EObject create()
		{
			return CallActionsFactory.eINSTANCE.createOperationAction();
		}
	}

	class StringExpressionView extends ExpressionView
	{
		@Override
		public void adaptInputElements()
		{
			super.adaptInputElements();
			CommonEditExpressionDialog.this.addOrResetStringExpressionEditor(
					((StringExpression) CommonEditExpressionDialog.this.currentTreeValue).getExpressionLanguage(),
					((StringExpression) CommonEditExpressionDialog.this.currentTreeValue).getExpressionString());
		}

		@Override
		public EObject create()
		{
			return ExpressionsFactory.eINSTANCE.createStringExpression();
		}

		@Override
		public void processCurrentInput()
		{
			SourceViewerProvider svp = CommonEditExpressionDialog.this.sourceViewerProviders
					.get(((StringExpression) CommonEditExpressionDialog.this.currentTreeValue).getExpressionLanguage());
			if (CommonEditExpressionDialog.this.sourceViewerProviders.size() == 0 || svp == null)
			{
				return;
			}

			StringExpression se = (StringExpression) CommonEditExpressionDialog.this.currentTreeValue;
			String currExprString = svp.getText();
			if (!currExprString.equals(se.getExpressionString()))
			{
				final EditingDomain domain = getEditingDomain();
				
				// DB: Sometimes the editing domain is null, which prevents closing the dialog.
				if ( domain != null ) {
					Command command = domain.createCommand(SetCommand.class,
							new CommandParameter(se, ExpressionsPackage.eINSTANCE.getStringExpression_ExpressionString(), currExprString));
					domain.getCommandStack().execute(command);
				}
			}
		}
	}

	class VariableDeclarationActionView extends VariableReferenceActionView
	{
		@Override
		public EObject create()
		{
			return CallActionsFactory.eINSTANCE.createVariableDeclarationAction();
		}
	}

	class VariableReferenceActionView extends CallActionView
	{
		@Override
		public void adaptInputElements()
		{
			super.adaptInputElements();

			if (((VariableReferenceAction) CommonEditExpressionDialog.this.currentTreeValue).getVariableName() != null)
			{
				CommonEditExpressionDialog.this.addOrResetVariableField("Variable Name:",
						((VariableReferenceAction) CommonEditExpressionDialog.this.currentTreeValue).getVariableName());
			}
			else
			{
				CommonEditExpressionDialog.this.addOrResetVariableField("Variable Name:", "");
			}
		}

		@Override
		public EObject create()
		{
			return CallActionsFactory.eINSTANCE.createVariableReferenceAction();
		}

		@Override
		public void processCurrentInput()
		{
			VariableReferenceAction vra = (VariableReferenceAction) CommonEditExpressionDialog.this.currentTreeValue;
			if (!CommonEditExpressionDialog.this.variableName.getText().equals(vra.getVariableName()))
			{
				TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(vra);
				Command command = domain.createCommand(SetCommand.class,
						new CommandParameter(vra, CallActionsPackage.eINSTANCE.getVariableReferenceAction_VariableName(),
								CommonEditExpressionDialog.this.variableName.getText()));
				domain.getCommandStack().execute(command);
			}
		}
	}

	static final Map<String, Collection<EObject>>	SUBCLASS_MAP	= new HashMap<String, Collection<EObject>>();

	static
	{
		SUBCLASS_MAP.put(ExpressionsPackage.eINSTANCE.getExpression().getName(),
				getConcreteSubclasses(ExpressionsPackage.eINSTANCE.getExpression()));
		SUBCLASS_MAP.put(CallActionsPackage.eINSTANCE.getCallAction().getName(),
				getConcreteSubclasses(CallActionsPackage.eINSTANCE.getCallAction()));
	}

	public interface LanguageSelectionWidget
	{
		public abstract void addLanguageSelectedListener(LanguageSelectedListener listener);

		public abstract void removeLanguageSelectedListener(LanguageSelectedListener listener);

		public abstract void setSelectedLanguage(String language);

		public abstract void addLanguage(String language);

		public abstract String getSelectedLanguage();
	}

	public class LanguageSelectedEvent extends java.util.EventObject
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 5777557256338061090L;
		public String	language;

		public LanguageSelectedEvent(Object source, String language)
		{
			super(source);
			this.language = language;
		}
	}

	public interface LanguageSelectedListener extends java.util.EventListener
	{
		public void languageSelected(LanguageSelectedEvent event);
	}

	protected class LanguageSelectionComboWidget extends Composite implements LanguageSelectionWidget
	{

		Combo								languageCombo;

		Collection<String>					languageDirectory;

		Vector<LanguageSelectedListener>	languageSelectedListeners	= new Vector<LanguageSelectedListener>();

		public LanguageSelectionComboWidget(Composite parent, int style)
		{
			super(parent, style);
			this.setLayout(new GridLayout(2, false));

			this.languageDirectory = new Vector<String>();

			Label languageLabel = new Label(this, SWT.NONE);
			languageLabel.setText("Expression Language");
			this.createDialogLanguageCombo(this);
		}

		@Override
		public void addLanguageSelectedListener(LanguageSelectedListener listener)
		{

			this.languageSelectedListeners.addElement(listener);

		}

		@Override
		public void removeLanguageSelectedListener(LanguageSelectedListener listener)
		{

			this.languageSelectedListeners.removeElement(listener);

		}

		private void createDialogLanguageCombo(Composite mainArea)
		{
			this.languageCombo = new Combo(mainArea, SWT.DROP_DOWN);
			this.languageCombo.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, false, false));
			String[] items = {};
			this.languageCombo.setItems(items);

			this.languageCombo.addSelectionListener(new SelectionListener()
			{

				@Override
				public void widgetSelected(SelectionEvent e)
				{
					LanguageSelectionComboWidget.this.languageSelected(LanguageSelectionComboWidget.this.languageCombo
							.getItem(LanguageSelectionComboWidget.this.languageCombo.getSelectionIndex()));
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e)
				{
					this.widgetSelected(e);
				}

			});
		}

		protected void languageSelected(String language)
		{
			LanguageSelectedEvent e = new LanguageSelectedEvent(this, language);

			for (LanguageSelectedListener listener : this.languageSelectedListeners)
			{
				listener.languageSelected(e);
			}

		}

		@Override
		public void addLanguage(String language)
		{
			String[] items = {};
			this.languageDirectory.add(language);
			this.languageCombo.setItems(this.languageDirectory.toArray(items));
		}

		/**
		 * Sets the selected Language of the widget if it is already registered.
		 * If not nothing happens. Furthermore this does not trigger a
		 * languageSelectedEvent.
		 */
		@Override
		public void setSelectedLanguage(String language)
		{
			int index = this.searchStringInCombo(this.languageCombo, language);
			this.languageCombo.select(index);
		}

		private int searchStringInCombo(Combo aCombo, String searchString)
		{
			String[] items = aCombo.getItems();
			int numberOfItems = items.length;

			int searchedIndex = 0;
			for (int i = 0; i < numberOfItems && !items[searchedIndex].equals(searchString); i++)
			{
				searchedIndex++;
			}

			return searchedIndex;
		}

		@Override
		public String getSelectedLanguage()
		{
			return this.languageCombo.getItem(this.languageCombo.getSelectionIndex());
		}

	}

	protected class LanguageSelectionRadioWidget extends Composite implements LanguageSelectionWidget
	{

		private final Collection<String>	languageDirectory;
		Composite							languageRadioArea;

		Map<Button, String>					buttonList;

		Vector<LanguageSelectedListener>	languageSelectedListeners	= new Vector<LanguageSelectedListener>();

		public LanguageSelectionRadioWidget(Composite parent, int style)
		{
			super(parent, style);
			this.setLayout(new RowLayout(SWT.VERTICAL));

			this.languageDirectory = new Vector<String>();

			Label languageLabel = new Label(this, SWT.NONE);
			languageLabel.setText("Expression Language");
			this.createDialogLanguageRadioButtons(this);

		}

		@Override
		public void addLanguageSelectedListener(LanguageSelectedListener listener)
		{

			this.languageSelectedListeners.addElement(listener);

		}

		@Override
		public void removeLanguageSelectedListener(LanguageSelectedListener listener)
		{

			this.languageSelectedListeners.removeElement(listener);

		}

		public void createDialogLanguageRadioButtons(Composite mainArea)
		{
			this.languageRadioArea = new Composite(mainArea, SWT.NONE);
			this.languageRadioArea.setLayout(new RowLayout(SWT.HORIZONTAL));
			this.buttonList = new HashMap<Button, String>();
		}

		protected void languageSelected(String language)
		{
			LanguageSelectedEvent e = new LanguageSelectedEvent(this, language);

			for (LanguageSelectedListener listener : this.languageSelectedListeners)
			{
				listener.languageSelected(e);
			}

		}

		@Override
		public void addLanguage(String language)
		{
			if (!this.languageDirectory.contains(language))
			{
				this.languageDirectory.add(language);
			}

			Listener listener = new Listener()
			{
				@Override
				public void handleEvent(Event e)
				{
					for (Control aButton : LanguageSelectionRadioWidget.this.buttonList.keySet())
					{
						((Button) aButton).setSelection(false);
					}
					((Button) e.widget).setSelection(true);
					LanguageSelectionRadioWidget.this.languageSelected(LanguageSelectionRadioWidget.this.buttonList.get(e.widget));
				}
			};

			Button tempButton = new Button(this.languageRadioArea, SWT.RADIO);
			tempButton.setText(language);
			tempButton.addListener(SWT.Selection, listener);
			String buttonInfo = language;
			this.buttonList.put(tempButton, buttonInfo);

		}

		@Override
		public void setSelectedLanguage(String language)
		{
			Button searchedButton = this.buttonList.keySet().iterator().next();
			for (Button aButton : this.buttonList.keySet())
			{
				if (this.buttonList.get(aButton).equals(language))
				{
					searchedButton = aButton;
				}
				aButton.setSelection(false);
			}
			searchedButton.setSelection(true);
		}

		@Override
		public String getSelectedLanguage()
		{
			return this.getInfoOfSelectedButton();
		}

		private String getInfoOfSelectedButton()
		{
			String resultString = "none";
			for (Button aButton : this.buttonList.keySet())
			{
				if (aButton.getSelection())
				{
					resultString = this.buttonList.get(aButton);
				}
			}
			return resultString;
		}

	}

	static Collection<EObject> getConcreteSubclasses(EClass superClass)
	{
		// SELECT * FROM StoryDiagramEcorePackage WHERE
		// abstract == false AND
		// interface == false AND
		// eAllSuperTypes.contains(superClass)

		EObjectCondition _1stCond = new EObjectAttributeValueCondition(EcorePackage.eINSTANCE.getEClass_Abstract(), new BooleanCondition(
				false));
		EObjectCondition _2ndCond = new EObjectAttributeValueCondition(EcorePackage.eINSTANCE.getEClass_Interface(), new BooleanCondition(
				false));
		EObjectCondition _3rdCond = new EObjectReferenceValueCondition(EcorePackage.eINSTANCE.getEClass_EAllSuperTypes(),
				new EObjectInstanceCondition(superClass));
		SELECT statement = new SELECT(new FROM(superClass.getEPackage()), new WHERE(_1stCond.AND(_2ndCond).AND(_3rdCond)));
		List<EObject> list = new ArrayList<EObject>(statement.execute());
		Collections.sort(list, new Comparator<EObject>()
		{
			@Override
			public int compare(EObject arg0, EObject arg1)
			{
				EClass eClass0 = (EClass) arg0, eClass1 = (EClass) arg1;
				return eClass0.getName().compareTo(eClass1.getName());
			}
		});
		return list;
	}

	private Composite								activityField, operationField, stringexpField, variableField, expressionField,
			classifierField, upper;

	private Button									addButton, removeNodeButton, upButton, downButton;

	private TreeViewer								callActionHierarchie;

	private Combo									callActionTypeCombobox, classifierCombo, activityCombo, operationCombo;

	private LanguageSelectionWidget					expLanguageSelector;

	private Collection<EObject>						classifierValues, activityValues, operationValues;

	// widget Elements for the CallActionExpression Editor

	private EClassifier								contextClassifier, expectedClassifier;

	private TreeItem								currentTreeItem;

	private EObject									currentTreeValue, expressionOwner = null, rootContainer = null;

	private EStructuralFeature						feature;

	private EditingDomain				editingDomain;

	private Expression								expression;

	private Label									expressionLabel, expectedClassifierLabel, activityLabel, operationLabel,
			classifierLabel, variableLabel;

	private Text									expressionText, variableName;

	private Group									lower;

	private SourceViewerProvider					currSourceVP;

	private final Map<String, SourceViewerProvider>	sourceViewerProviders;

	private Map<String, ISourceViewer>				sourceViewers;

	final Map<String, IView>						VIEW_MAP	= new HashMap<String, IView>();

	public CommonEditExpressionDialog(Shell parent)
	{
		super(parent);

		this.VIEW_MAP.put(CallActionsPackage.Literals.CALL_STORY_DIAGRAM_INTERPRETER_ACTION.getName(), new CallSDInterpreterActionView());
		this.VIEW_MAP.put(CallActionsPackage.Literals.COMPARE_ACTION.getName(), new CompareActionView());
		this.VIEW_MAP.put(CallActionsPackage.Literals.LITERAL_DECLARATION_ACTION.getName(), new LiteralDeclarationActionView());
		this.VIEW_MAP.put(CallActionsPackage.Literals.METHOD_CALL_ACTION.getName(), new MethodCallActionView());
		this.VIEW_MAP.put(CallActionsPackage.Literals.NEW_OBJECT_ACTION.getName(), new NewObjectActionView());
		this.VIEW_MAP.put(CallActionsPackage.Literals.NULL_VALUE_ACTION.getName(), new NullValueActionView());
		this.VIEW_MAP.put(CallActionsPackage.Literals.OPERATION_ACTION.getName(), new OperationActionView());
		this.VIEW_MAP.put(CallActionsPackage.Literals.VARIABLE_DECLARATION_ACTION.getName(), new VariableDeclarationActionView());
		this.VIEW_MAP.put(CallActionsPackage.Literals.VARIABLE_REFERENCE_ACTION.getName(), new VariableReferenceActionView());
		this.VIEW_MAP.put(CallActionsPackage.Literals.CALL_ACTION_PARAMETER.getName(), new CallActionParameterView());
		this.VIEW_MAP.put(ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION.getName(), new CallActionExpressionView());
		this.VIEW_MAP.put(ExpressionsPackage.Literals.STRING_EXPRESSION.getName(), new StringExpressionView());

		/*
		 * Create the map of registered source viewers. This map maps the
		 * expression languages to their associated source viewers.
		 */
		this.sourceViewerProviders = new HashMap<String, SourceViewerProvider>();

		IConfigurationElement[] configurationElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
				StoryDiagramEcoreDiagramConstants.EXPRESSION_SOURCE_VIEWER_EXTENSION_POINT_ID);

		for (IConfigurationElement configurationElement : configurationElements)
		{
			String s = configurationElement.getAttribute(StoryDiagramEcoreDiagramConstants.EXPRESSION_LANGUAGE);

			if (s != null && !"".equals(s))
			{
				try
				{
					this.sourceViewerProviders.put(s, (SourceViewerProvider) configurationElement
							.createExecutableExtension(StoryDiagramEcoreDiagramConstants.SOURCE_VIEWER_PROVIDER));
				}
				catch (CoreException e)
				{
					// Skip it but show error message
					e.printStackTrace();
				}
			}
		}

		/*
		 * Add a default source viewer for all expression languages that do not
		 * have one.
		 */
		for (String expressionLanguage : StoryDiagramEcoreUtils.getAvailableExpressionLanguages())
		{
			if (!this.sourceViewerProviders.containsKey(expressionLanguage))
			{
				this.sourceViewerProviders.put(expressionLanguage, new SourceViewerProvider());
			}
		}

	}

	protected void adaptInputElementsDefault()
	{
		this.callActionTypeCombobox.removeAll();

		if (this.currentTreeValue == this.expressionOwner)
		{
			for (EObject subClass : SUBCLASS_MAP.get(this.feature.getEType().getName()))
			{
				this.callActionTypeCombobox.add(this.feature.getName() + SEPARATOR + ((EClass) subClass).getName());
			}

			if (this.feature.isMany() || this.expressionOwner.eGet(this.feature) == null)
			{
				this.callActionTypeCombobox.setEnabled(true);
				this.callActionTypeCombobox.select(0);
				this.addButton.setEnabled(true);
			}
			else
			{
				this.callActionTypeCombobox.setEnabled(false);
				this.addButton.setEnabled(false);
			}

			return;
		}

		EClass eClass = this.currentTreeValue.eClass();
		for (EReference eReference : eClass.getEAllReferences())
		{
			if (eReference.isContainment() && (eReference.isMany() || this.currentTreeValue.eGet(eReference) == null))
			{
				if (SUBCLASS_MAP.containsKey(eReference.getEReferenceType().getName()))
				{
					for (EObject subClass : SUBCLASS_MAP.get(eReference.getEReferenceType().getName()))
					{
						this.callActionTypeCombobox.add(eReference.getName() + SEPARATOR + ((EClass) subClass).getName());
					}
				}
				else if (this.VIEW_MAP.containsKey(eReference.getEReferenceType().getName()))
				{
					this.callActionTypeCombobox.add(eReference.getName() + SEPARATOR + eReference.getEReferenceType().getName());
				}
			}
		}

		if (this.callActionTypeCombobox.getItemCount() > 0)
		{
			this.callActionTypeCombobox.setEnabled(true);
			this.callActionTypeCombobox.select(0);
			this.addButton.setEnabled(true);
		}
		else
		{
			this.callActionTypeCombobox.setEnabled(false);
			this.addButton.setEnabled(false);
		}

	}

	protected void addOrResetActivityfield(int selection)
	{
		this.activityField = new Composite(this.lower, SWT.NONE);
		this.activityField.setLayout(new GridLayout(1, true));
		this.activityField.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

		this.activityLabel = new Label(this.activityField, SWT.NONE);
		this.activityLabel.setText("Activity:");
		this.activityLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false));

		this.activityCombo = new Combo(this.activityField, SWT.READ_ONLY);
		GridData gdTxtvarname = new GridData(GridData.FILL, GridData.FILL, true, true);
		gdTxtvarname.verticalSpan = 4;
		this.activityCombo.setLayoutData(gdTxtvarname);

		for (EObject a : this.activityValues)
		{
			if (((Activity) a).getName() != null)
			{
				this.activityCombo.add(((Activity) a).getName());
			}
		}

		this.activityCombo.select(selection);
	}

	protected void addOrResetClassifierfield()
	{
		this.classifierField = new Composite(this.lower, SWT.NONE);
		this.classifierField.setLayout(new GridLayout(1, false));
		this.classifierField.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

		this.classifierLabel = new Label(this.classifierField, SWT.NONE);
		this.classifierLabel.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));
		this.classifierLabel.setText("Classifier:");

		this.classifierCombo = new Combo(this.classifierField, SWT.READ_ONLY);
		this.classifierCombo.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

		// initialize Classifier Combo

		List<EObject> c = new ArrayList<EObject>(this.classifierValues);
		Collections.sort(c, new Comparator<EObject>()
		{

			@Override
			public int compare(EObject o1, EObject o2)
			{
				if (o1 instanceof EClassifier && o2 instanceof EClassifier)
				{
					return ((EClassifier) o1).getName().compareToIgnoreCase(((EClassifier) o2).getName());
				}
				else
				{
					return 0;
				}
			}

		});

		for (Object o : c)
		{
			if (o != null && ((EClassifier) o).getName() != null)
			{
				this.classifierCombo.add(((EClassifier) o).getName());
			}
		}

		EClassifier classifier = ((HasClassifierView) this.VIEW_MAP.get(this.currentTreeValue.eClass().getName())).getClassifier();
		// set the classifier in the combobox
		if (classifier != null)
		{
			for (int i = 0; i < this.classifierCombo.getItems().length; i++)
			{
				if (this.classifierCombo.getItem(i).equals(classifier.getName()))
				{
					this.classifierCombo.select(i);
					this.classifierCombo.setToolTipText(classifier.getInstanceTypeName());
					break;
				}
			}

		}
	}

	protected void addOrResetExpressionField(String labelText, String content)
	{
		this.expressionField = new Composite(this.lower, SWT.NONE);
		this.expressionField.setLayout(new GridLayout(1, true));
		this.expressionField.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

		this.expressionLabel = new Label(this.expressionField, SWT.NONE);
		this.expressionLabel.setText(labelText);
		GridData gdexpLabel = new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false);
		gdexpLabel.grabExcessHorizontalSpace = true;
		this.expressionLabel.setLayoutData(gdexpLabel);

		this.expressionText = new Text(this.expressionField, SWT.BORDER);
		GridData gdexpTxt = new GridData(GridData.FILL, GridData.FILL, true, true);
		this.expressionText.setLayoutData(gdexpTxt);
		this.expressionText.setText(content);
	}

	protected void addOrResetOperationField(int selection)
	{
		this.operationField = new Composite(this.lower, SWT.NONE);
		this.operationField.setLayout(new GridLayout(1, true));
		this.operationField.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

		this.operationLabel = new Label(this.operationField, SWT.NONE);
		this.operationLabel.setText("EOperation:");
		this.operationLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false));

		this.operationCombo = new Combo(this.operationField, SWT.READ_ONLY);
		GridData gdTxtvarname = new GridData(GridData.FILL, GridData.FILL, true, true);
		gdTxtvarname.verticalSpan = 4;
		this.operationCombo.setLayoutData(gdTxtvarname);

		this.operationCombo.add("");
		for (EObject a : this.operationValues)
		{
				this.operationCombo.add(((EOperation) a).getName());
		}
		if (selection>=0) selection++; else selection=0;
		this.operationCombo.select(selection);
	}

	protected void addOrResetStringExpressionEditor(String string, String content)
	{
		List<String> constraintLanguages = StoryDiagramEcoreUtils.getAvailableExpressionLanguages();
		this.currSourceVP = this.sourceViewerProviders.get(((StringExpression) this.currentTreeValue).getExpressionLanguage());

		if (constraintLanguages.size() == 0 || this.currSourceVP == null)
		{
			return;
		}

		this.stringexpField = new Composite(this.lower, SWT.NONE);
		this.stringexpField.setLayout(new GridLayout(1, false));
		this.stringexpField.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		if (constraintLanguages.size() <= StoryDiagramEcoreDiagramConstants.EXPRESSION_EDITOR_RADIO_BUTTON_THRESHOLD)
		{
			this.expLanguageSelector = new LanguageSelectionRadioWidget(this.stringexpField, SWT.NONE);
		}
		else
		{
			this.expLanguageSelector = new LanguageSelectionComboWidget(this.stringexpField, SWT.NONE);
		}
		for (String aLanguage : constraintLanguages)
		{
			this.expLanguageSelector.addLanguage(aLanguage);
		}

		this.expLanguageSelector.addLanguageSelectedListener(new LanguageSelectedListener()
		{

			@Override
			public void languageSelected(LanguageSelectedEvent event)
			{
				/*
				 * Get the text of the current source viewer
				 */
				// ((StringExpression)
				// currentTreeValue).setExpressionString(currSourceVP.getText());

				TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(CommonEditExpressionDialog.this.currentTreeValue);

				Command command = domain.createCommand(SetCommand.class, new CommandParameter(
						CommonEditExpressionDialog.this.currentTreeValue, ExpressionsPackage.Literals.STRING_EXPRESSION__EXPRESSION_STRING,
						CommonEditExpressionDialog.this.currSourceVP.getText()));
				domain.getCommandStack().execute(command);

				/*
				 * Make the current source viewer invisible
				 */
				CommonEditExpressionDialog.this.sourceViewers
						.get(((StringExpression) CommonEditExpressionDialog.this.currentTreeValue).getExpressionLanguage()).getTextWidget()
						.setVisible(false);
				((GridData) CommonEditExpressionDialog.this.sourceViewers
						.get(((StringExpression) CommonEditExpressionDialog.this.currentTreeValue).getExpressionLanguage()).getTextWidget()
						.getLayoutData()).exclude = true;

				/*
				 * Set the new expression language
				 */
				// ((StringExpression)
				// currentTreeValue).setExpressionLanguage(comboExpressionLanguages.getItem(comboExpressionLanguages
				// .getSelectionIndex()));

				command = domain.createCommand(SetCommand.class, new CommandParameter(CommonEditExpressionDialog.this.currentTreeValue,
						ExpressionsPackage.Literals.STRING_EXPRESSION__EXPRESSION_LANGUAGE,
						CommonEditExpressionDialog.this.expLanguageSelector.getSelectedLanguage()));
				domain.getCommandStack().execute(command);

				CommonEditExpressionDialog.this.currSourceVP = CommonEditExpressionDialog.this.sourceViewerProviders
						.get(((StringExpression) CommonEditExpressionDialog.this.currentTreeValue).getExpressionLanguage());

				/*
				 * Set the text of the new source viewer
				 */
				CommonEditExpressionDialog.this.currSourceVP.setText(((StringExpression) CommonEditExpressionDialog.this.currentTreeValue)
						.getExpressionString());

				/*
				 * Make the new source viewer visible
				 */
				CommonEditExpressionDialog.this.sourceViewers
						.get(((StringExpression) CommonEditExpressionDialog.this.currentTreeValue).getExpressionLanguage()).getTextWidget()
						.setVisible(true);
				((GridData) CommonEditExpressionDialog.this.sourceViewers
						.get(((StringExpression) CommonEditExpressionDialog.this.currentTreeValue).getExpressionLanguage()).getTextWidget()
						.getLayoutData()).exclude = false;

				CommonEditExpressionDialog.this.lower.layout();

			}
		});

		/*
		 * Create the source viewers for all source viewer providers
		 */

		this.sourceViewers = new HashMap<String, ISourceViewer>();

		for (Entry<String, SourceViewerProvider> svpEntry : this.sourceViewerProviders.entrySet())
		{
			ISourceViewer sv = svpEntry.getValue().createSourceViewer(this.lower, SWT.BORDER | SWT.MULTI, this.contextClassifier,
					this.getContextInformation(), "");
			this.sourceViewers.put(svpEntry.getKey(), sv);

			sv.getTextWidget().setVisible(false);

			GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
			gridData.exclude = true;

			sv.getTextWidget().setLayoutData(gridData);
		}

		// Select the expression language of the expression
		this.expLanguageSelector.setSelectedLanguage(((StringExpression) this.currentTreeValue).getExpressionLanguage());

		// Set the text of the source viewer
		this.currSourceVP.setText(((StringExpression) this.currentTreeValue).getExpressionString());

		StyledText st = this.sourceViewers.get(((StringExpression) this.currentTreeValue).getExpressionLanguage()).getTextWidget();

		// Make the source viewer visible
		st.setVisible(true);
		((GridData) st.getLayoutData()).exclude = false;

		this.lower.layout();
	}

	protected void addOrResetVariableField(String labelText, String content)
	{
		this.variableField = new Composite(this.lower, SWT.NONE);
		this.variableField.setLayout(new GridLayout(1, true));
		this.variableField.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

		this.variableLabel = new Label(this.variableField, SWT.NONE);
		this.variableLabel.setText(labelText);
		this.variableLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false));

		this.variableName = new Text(this.variableField, SWT.BORDER);
		this.variableName.setText(content);

		GridData gdTxtvarname = new GridData(GridData.FILL, GridData.FILL, true, true);
		gdTxtvarname.verticalSpan = 4;
		this.variableName.setLayoutData(gdTxtvarname);
	}

	@Override
	protected void buttonPressed(int buttonId)
	{
		if (IDialogConstants.CLOSE_ID == buttonId)
		{
			this.setReturnCode(OK);
			this.close();
		}
	}

	protected void changeLayout()
	{
		Widget[] controlElements =
		{
				this.expressionField, this.variableField, this.activityField, this.operationField, this.stringexpField,
				this.classifierField
		};

		// clear everything
		for (Widget w : controlElements)
		{
			if (w != null && !w.isDisposed())
			{
				w.dispose();
			}
		}

		if (this.sourceViewers != null)
		{
			for (ISourceViewer sv : this.sourceViewers.values())
			{
				if (sv.getTextWidget() != null)
				{
					sv.getTextWidget().dispose();
				}
			}
		}

		if (this.currentTreeValue == null || this.currentTreeValue.equals(this.expressionOwner))
		{
			this.removeNodeButton.setEnabled(false);
		}
		else
		{
			this.removeNodeButton.setEnabled(true);
		}

		if (this.currentTreeValue == null)
		{
			this.callActionTypeCombobox.setEnabled(false);
			this.addButton.setEnabled(false);
		}
		else
		{
			String name = this.currentTreeValue.eClass().getName();
			

			if (this.VIEW_MAP.containsKey(name))
			{
				this.VIEW_MAP.get(name).adaptInputElements();
			}
			else
			{
				this.adaptInputElementsDefault();
			}
		}

		this.lower.layout(true);
		this.upper.layout(true);
		this.callActionTypeCombobox.redraw();
	}

	@Override
	public boolean close() {
		processCurrentInput();
		disposeSourceViewers();
		
		return super.close();
	}

	@Override
	protected void configureShell(Shell newShell)
	{
		super.configureShell(newShell);
		newShell.setText("Edit Expression");
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent)
	{
		this.createButton(parent, IDialogConstants.CLOSE_ID, IDialogConstants.CLOSE_LABEL, true);
	}

	protected <T> SelectionListener createComboSelectionListener(final EReference eReference, final Combo combo,
			final Collection<EObject> eObjColl)
	{
		return new SelectionListener()
		{
			@Override
			public void widgetDefaultSelected(SelectionEvent e)
			{
				this.widgetSelected(e);
			}

			@Override
			public void widgetSelected(SelectionEvent e)
			{
				TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(CommonEditExpressionDialog.this.currentTreeValue);

				ENamedElement nElem = null;
				Iterator<EObject> it = eObjColl.iterator();
				while (it.hasNext())
				{
					nElem = (ENamedElement) it.next();
					if (combo.getText().equals("")){
						nElem = null;
						break;
					}
					if (nElem.getName().equals(combo.getText()))
					{
						break;
					}
				}

				Command command = domain.createCommand(SetCommand.class, new CommandParameter(
						CommonEditExpressionDialog.this.currentTreeValue, eReference, nElem));
				domain.getCommandStack().execute(command);
			}
		};
	}

	@Override
	protected Control createDialogArea(Composite parent)
	{
		Composite composite = (Composite) super.createDialogArea(parent);

		composite.setLayout(new GridLayout(1, false));

		Group main = new Group(composite, SWT.NONE);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		gridLayout.makeColumnsEqualWidth = false;
		main.setLayout(gridLayout);

		this.upper = new Composite(main, SWT.NONE);
		this.upper.setLayout(new GridLayout(3, false));
		this.upper.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, true, 2, 6));

		this.lower = new Group(main, SWT.EMBEDDED);
		this.lower.setLayout(new GridLayout(2, true));
		this.lower.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 1, 1));
		this.lower.setText("Options");
		((GridData) this.lower.getLayoutData()).minimumHeight = 200;
		
		// declare buttons and other widgets
		this.callActionHierarchie = new TreeViewer(this.upper, SWT.NONE); 

		GridData gridData = new GridData(GridData.FILL, GridData.FILL, true, true, 3, 1);
		gridData.minimumWidth = 500;
		gridData.minimumHeight = 200;
		gridData.widthHint = 500;
		gridData.heightHint = 200;
		this.callActionHierarchie.getTree().setLayoutData(gridData);
		this.callActionHierarchie.setContentProvider(new AdapterFactoryContentProvider(StoryDiagramEcoreDiagramEditorPlugin.getInstance()
				.getItemProvidersAdapterFactory()));
		this.callActionHierarchie.setLabelProvider(new AdapterFactoryLabelProvider.ColorProvider(StoryDiagramEcoreDiagramEditorPlugin
				.getInstance().getItemProvidersAdapterFactory(), this.callActionHierarchie));
		this.callActionHierarchie.setInput(this.expressionOwner.eContainer());

		// creating Listener for the TreeItems
		SelectionListener stdTreeItemlistener = new SelectionListener()
		{
			@Override
			public void widgetDefaultSelected(SelectionEvent event)
			{
				this.widgetSelected(event);
			}

			@Override
			public void widgetSelected(SelectionEvent event)
			{
				CommonEditExpressionDialog.this.processCurrentInput();
				CommonEditExpressionDialog.this.selectionChanged(event.item);
				upButton.setEnabled(false);
				downButton.setEnabled(false);		
				final TreeItem parentItem = ((TreeItem)event.item).getParentItem();
				
				if ( parentItem != null && parentItem.getData() instanceof StoryPatternObject){//.getClass().toString().equals("class de.hpi.sam.storyDiagramEcore.expressions.impl.CallActionExpressionImpl")){
					upButton.setEnabled(true);
					downButton.setEnabled(true);
					//for (int j = 0; j<((TreeItem)event.item).getParentItem().getItems().length;j++){
						//System.out.println(((TreeItem)event.item).getParentItem().getItems()[j].toString());
					//}
				}
			}
		};
		this.callActionHierarchie.getTree().addSelectionListener(stdTreeItemlistener);

		ViewerFilter[] viewerFilters = new ViewerFilter[1];
		viewerFilters[0] = new ViewerFilter()
		{

			@SuppressWarnings("unchecked")
			@Override
			public boolean select(Viewer viewer, Object parentElement, Object element)
			{
				if (element == CommonEditExpressionDialog.this.expressionOwner)
				{
					return true;
				}

				if (CommonEditExpressionDialog.this.feature.isMany())
				{
					for (Expression expr : (EList<Expression>) CommonEditExpressionDialog.this.expressionOwner
							.eGet(CommonEditExpressionDialog.this.feature))
					{
						if (this.checkSingleExpression(expr, element))
						{
							return true;
						}
					}

					return false;
				}
				else
				{
					return this.checkSingleExpression(
							(Expression) CommonEditExpressionDialog.this.expressionOwner.eGet(CommonEditExpressionDialog.this.feature),
							element);
				}
			}

			protected boolean checkSingleExpression(Expression expr, Object element)
			{
				if (expr == null)
				{
					return false;
				}

				if (expr == element)
				{
					return true;
				}

				TreeIterator<EObject> it = expr.eAllContents();
				while (it.hasNext())
				{
					if (it.next() == element)
					{
						return true;
					}
				}

				return false;
			}
		};
		this.callActionHierarchie.setFilters(viewerFilters);
		this.callActionHierarchie.expandAll();

		this.upButton = new Button(this.upper, SWT.NONE);
		this.upButton.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false, 3, 1));
		this.upButton.setText("Up");
		//this.upButton.setEnabled(false);
		this.downButton = new Button(this.upper, SWT.NONE);
		this.downButton.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false, 3, 1));
		this.downButton.setText("Down");
		//this.downButton.setEnabled(false);
		this.upButton.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetDefaultSelected(SelectionEvent e)
			{
				this.widgetSelected(e);
			}

			@Override
			public void widgetSelected(SelectionEvent e)
			{
				changeTreePositions(true);
			}
		});		
		this.downButton.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetDefaultSelected(SelectionEvent e)
			{
				this.widgetSelected(e);
			}

			@Override
			public void widgetSelected(SelectionEvent e)
			{
				changeTreePositions(false);
			}
		});		
		this.removeNodeButton = new Button(this.upper, SWT.NONE);
		this.removeNodeButton.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false, 3, 1));
		this.removeNodeButton.setText("Remove");

		this.callActionTypeCombobox = new Combo(this.upper, SWT.READ_ONLY);
		this.callActionTypeCombobox.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false, 1, 1));
		this.callActionTypeCombobox.setEnabled(false);

		this.removeNodeButton.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetDefaultSelected(SelectionEvent e)
			{
				this.widgetSelected(e);
			}

			@Override
			public void widgetSelected(SelectionEvent e)
			{
				TreeItem parentItem = CommonEditExpressionDialog.this.currentTreeItem.getParentItem();

				TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(CommonEditExpressionDialog.this.currentTreeValue);
				Command command = new DeleteCommand(domain, Arrays.asList(new EObject[]
				{
					CommonEditExpressionDialog.this.currentTreeValue
				}));
				command = command.chain(CommonEditExpressionDialog.this.VIEW_MAP.get(
						CommonEditExpressionDialog.this.currentTreeValue.eClass().getName()).getExtraCommand4Removal(domain));
				domain.getCommandStack().execute(command);

				CommonEditExpressionDialog.this.callActionHierarchie.getTree().select(parentItem);
				CommonEditExpressionDialog.this.selectionChanged(parentItem);
			}
		});

		this.addButton = new Button(this.upper, SWT.NONE);
		this.addButton.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, false, false, 1, 1));
		this.addButton.setText("Add");
		this.addButton.setEnabled(false);
		this.addButton.addSelectionListener(new SelectionListener()
		{

			@Override
			public void widgetDefaultSelected(SelectionEvent e)
			{
				this.widgetSelected(e);
			}

			@Override
			public void widgetSelected(SelectionEvent e)
			{
				// when the button is pressed a new node is added to the tree
				// the type of the node is determined
				// by the selection in the callActionTypeCombobox
				
				if (CommonEditExpressionDialog.this.callActionTypeCombobox.getSelectionIndex() == -1)
				{
					return;
				}
				
				String[] comboBoxText = CommonEditExpressionDialog.this.callActionTypeCombobox.getText().split(SEPARATOR);
				String featureName = comboBoxText[0], typeName = comboBoxText[1];
				EStructuralFeature feat = CommonEditExpressionDialog.this.currentTreeValue.eClass().getEStructuralFeature(featureName);
				IView view = CommonEditExpressionDialog.this.VIEW_MAP.get(typeName);
				EObject neweObj = view.create();

				TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(CommonEditExpressionDialog.this.currentTreeValue);

				Command command;
				if (feat.isMany())
				{

					command = domain.createCommand(AddCommand.class, new CommandParameter(CommonEditExpressionDialog.this.currentTreeValue,
							feat, Arrays.asList(new EObject[]
							{
								neweObj
							})));
				}
				else
				{
					command = domain.createCommand(SetCommand.class, new CommandParameter(CommonEditExpressionDialog.this.currentTreeValue,
							feat, neweObj));
				}
				command = command.chain(view.getExtraCommand4Addition(domain));
				domain.getCommandStack().execute(command);
				CommonEditExpressionDialog.this.changeLayout();

			}

		});

		this.expectedClassifierLabel = new Label(main, SWT.NONE);
		this.expectedClassifierLabel.setLayoutData(new GridData(GridData.CENTER, GridData.BEGINNING, true, false, 3, 1));

		if (this.expectedClassifier != null)
		{
			this.expectedClassifierLabel.setText("Expected Classifier: " + this.expectedClassifier.getName());
		}
		else
		{
			this.expectedClassifierLabel.setText("Expected Classifier: [null]");
		}

		this.classifierValues = ItemPropertyDescriptor.getReachableObjectsOfType(this.rootContainer,
				EcorePackage.eINSTANCE.getEClassifier());

		/*
		 * Add all classifiers from the Ecore meta model
		 */
		for (EClassifier classifier : EcorePackage.eINSTANCE.getEClassifiers())
		{
			if (!this.classifierValues.contains(classifier))
			{
				this.classifierValues.add(classifier);
			}
		}

		this.activityValues = ItemPropertyDescriptor.getReachableObjectsOfType(this.rootContainer,
				StoryDiagramEcorePackage.eINSTANCE.getActivity());

		this.operationValues = ItemPropertyDescriptor.getReachableObjectsOfType(this.rootContainer, EcorePackage.eINSTANCE.getEOperation());
		
		if (this.callActionHierarchie.getTree().getItem(0).getItemCount() > 0)
		{
			for (TreeItem treeItem : this.callActionHierarchie.getTree().getItem(0).getItems())
			{
				if (treeItem.getData().equals(this.expression))
				{
					this.callActionHierarchie.getTree().select(treeItem);
					this.selectionChanged(treeItem);
					break;
				}
			}
		}

		return composite;
	}
	
	protected void changeTreePositions(boolean direction){ //direction = true = "up"; direction = false = "down"
		if (CommonEditExpressionDialog.this.callActionTypeCombobox.getSelectionIndex() == -1)
		{
			return;
		}
		TreeItem ti = currentTreeItem;
		StoryPatternObject spo = (StoryPatternObject) ti.getParentItem().getData();
		TransactionalEditingDomain domain;
		int size = spo.getConstraints().size();
		int oldPos = -1;
		int newPos = -1;
		Expression[] expressions = new Expression[size];
		
		for (int i=0; i<size;i++){
			expressions[i] = spo.getConstraints().get(i);
			if (expressions[i].equals(ti.getData())){
				if (direction){
					newPos = (i-1+size)%size;
				}
				else
				{
					newPos = (i+1)%size;
				}
				oldPos = i;
			}
		}
		// remove old constraints
		for (int j = 0; j<size;j++){
			domain = TransactionUtil.getEditingDomain(spo);
			domain.getCommandStack().execute(
					new RemoveCommand(domain, spo, spo.eClass()
							.getEStructuralFeature("constraints"), expressions[j]));
		}
		
		// change positions
		Expression ex = expressions[oldPos];
		expressions[oldPos] = expressions[newPos];
		expressions[newPos]=ex;
		
		// add new constraints
		for (int j = 0; j<size;j++){
			domain = TransactionUtil.getEditingDomain(spo);
			domain.getCommandStack().execute(
					new AddCommand(domain, spo, spo.eClass()
							.getEStructuralFeature("constraints"), expressions[j]));
		}
		
		if (callActionHierarchie.getTree().getItem(0).getItemCount() > 0)
		{
			for (TreeItem treeItem : callActionHierarchie.getTree().getItem(0).getItems())
			{
				if (treeItem.getData().equals(expressions[oldPos]))
				{
					callActionHierarchie.getTree().select(treeItem);
					selectionChanged(treeItem);
					break;
				}
			}
		}
	}

	protected void disposeSourceViewers()
	{
		for (SourceViewerProvider svp : this.sourceViewerProviders.values())
		{
			svp.dispose();
		}
	}

	protected abstract Map<String, EClassifier> getContextInformation();

	public EditingDomain getEditingDomain()
	{
		return this.editingDomain;
	}

	public EClassifier getExpectedClassifier()
	{
		return this.expectedClassifier;
	}

	public Expression getExpression()
	{
		return this.expression;
	}

	protected void processCurrentInput()
	{
		if (this.currentTreeValue != null)
		{
			String name = this.currentTreeValue.eClass().getName();

			if (this.VIEW_MAP.containsKey(name))
			{
				this.VIEW_MAP.get(name).processCurrentInput();
			}
		}
	}

	protected void selectionChanged(Widget item)
	{
		if (item instanceof TreeItem)
		{
			this.currentTreeItem = (TreeItem) item;
			this.currentTreeValue = (EObject) this.currentTreeItem.getData();
			this.changeLayout();
		}
	}

	public void setContextClassifier(EClassifier contextClassifier)
	{
		this.contextClassifier = contextClassifier;
	}

	public void setEditingDomain( EditingDomain editingDomain)
	{
		this.editingDomain = editingDomain;
	}

	public void setExpectedClassifier(EClassifier classifier)
	{
		this.expectedClassifier = classifier;
	}

	public void setExpression(Expression expression)
	{
		this.expression = expression;
	}

	public void setExpressionOwner(EObject owner)
	{
		this.expressionOwner = owner;
	}

	/**
	 * Set the rootContainer of the model. This is required to gather
	 * EClassifiers to use in CallActionExpressions.
	 * 
	 * @param rootContainer
	 */
	public void setRootContainer(EObject rootContainer)
	{
		this.rootContainer = rootContainer;
	}

	/**
	 * The feature must be present at expression owner.
	 * 
	 * @param feature
	 */
	public void setStructuralFeature(EStructuralFeature feature)
	{
		this.feature = feature;
	}
}
