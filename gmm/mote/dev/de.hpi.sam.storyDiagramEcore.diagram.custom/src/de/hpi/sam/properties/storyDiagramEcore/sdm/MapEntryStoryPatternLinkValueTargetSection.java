package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EStructuralFeature;

public class MapEntryStoryPatternLinkValueTargetSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getMapEntryStoryPatternLink_ValueTarget();
	}

	@Override
	protected String getLabelText()
	{
		return "Value Target";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) getEObject()).getValueTarget();
	}
}
