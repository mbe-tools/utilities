package de.hpi.sam.storyDiagramEcore.diagram.custom;

import java.util.Map;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.eclipse.swt.widgets.Composite;

/**
 * This class can be used to provide custom implementations of ISourceViewer and
 * IDocument. Simply override the create* methods. IMPORTANT: Always provide a
 * parameterless public constructor.
 * 
 * @author Stephan
 * 
 */
public class SourceViewerProvider
{
	private IDocument	document;

	public ISourceViewer createSourceViewer(Composite parent, int styles, EClassifier contextClassifier,
			Map<String, EClassifier> contextInformation, String text)
	{
		ISourceViewer sourceViewer = new SourceViewer(parent, null, styles);

		sourceViewer.configure(new SourceViewerConfiguration());

		document = new Document(text);

		sourceViewer.setDocument(document);

		return sourceViewer;
	}

	public String getText()
	{
		return document.get();
	}

	public void setText(String text)
	{
		document.set(text);
	}

	public void dispose()
	{

	}
}
