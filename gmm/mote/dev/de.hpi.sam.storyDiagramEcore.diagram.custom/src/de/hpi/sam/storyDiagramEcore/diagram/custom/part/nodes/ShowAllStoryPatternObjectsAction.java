package de.hpi.sam.storyDiagramEcore.diagram.custom.part.nodes;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

import de.hpi.sam.storyDiagramEcore.diagram.custom.part.CustomAbstractActionDelegate;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart;

public class ShowAllStoryPatternObjectsAction extends CustomAbstractActionDelegate
{

	StoryActionNodeEditPart	storyActionNodeEditPart	= null;

	@Override
	public void selectionChanged(IAction act, ISelection selection)
	{
		if (selection instanceof IStructuredSelection)
		{
			storyActionNodeEditPart = (StoryActionNodeEditPart) ((IStructuredSelection) selection).getFirstElement();
		}
	}

	@Override
	protected void doRun(IProgressMonitor progressMonitor)
	{
		StoryActionNodeStoryActionNodeElementsCompartmentEditPart elementsCompartmentEditPart = null;

		for (Object object : storyActionNodeEditPart.getChildren())
		{
			if (object instanceof StoryActionNodeStoryActionNodeElementsCompartmentEditPart)
			{
				elementsCompartmentEditPart = (StoryActionNodeStoryActionNodeElementsCompartmentEditPart) object;
				break;
			}
		}

		for (Object object : elementsCompartmentEditPart.getChildren())
		{
			if (object instanceof GraphicalEditPart)
			{
				GraphicalEditPart editPart = (GraphicalEditPart) object;

				/*
				 * Show the figure
				 */
				editPart.getFigure().setVisible(true);

				/*
				 * Show all connected links
				 */
				for (Object object2 : editPart.getSourceConnections())
				{
					if (object2 instanceof GraphicalEditPart)
					{
						((GraphicalEditPart) object2).getFigure().setVisible(true);
					}
				}

				for (Object object2 : editPart.getTargetConnections())
				{
					if (object2 instanceof GraphicalEditPart)
					{
						((GraphicalEditPart) object2).getFigure().setVisible(true);
					}
				}

			}
		}
	}

}
