package de.hpi.sam.storyDiagramEcore.diagram.custom;

import java.util.Collection;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;

public class MyCommand extends AbstractTransactionalCommand {
	Collection <EOperation> operations = null;
	public MyCommand(TransactionalEditingDomain editingDomain){    
		super(editingDomain, "a message", null);} 
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info, Collection <EOperation> ops) throws ExecutionException{
		this.operations = ops;
		return this.doExecuteWithResult(monitor, info);
	}
	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException{
		// You can add here the line of code that is giving you the error!        
		// Below is an example line.   
		EOperation e = (EOperation)this.operations.toArray()[1];
		getEditingDomain().getCommandStack().execute(SetCommand.create(getEditingDomain(), e, EcorePackage.eINSTANCE.getName(), ""));    
		return CommandResult.newOKCommandResult();
	}
}
