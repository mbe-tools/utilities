package de.hpi.sam.storyDiagramEcore.diagram.custom;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.graphics.Color;

public class StoryDiagramEcoreDiagramConstants
{

	public static final Color	MODIFIER_CREATE_COLOR						= new Color(null, 4, 178, 4);

	public static final Color	MODIFIER_DESTROY_COLOR						= ColorConstants.red;

	public static final Color	MODIFIER_NONE_COLOR							= ColorConstants.black;

	public static String		EXPRESSION_SOURCE_VIEWER_EXTENSION_POINT_ID	= "de.hpi.sam.storyDiagramEcore.diagram.custom.expressionSourceViewerExtension";
	public static String		EXPRESSION_LANGUAGE							= "expressionLanguage";
	public static String		SOURCE_VIEWER_PROVIDER						= "sourceViewerProvider";

	public static Integer		EXPRESSION_EDITOR_RADIO_BUTTON_THRESHOLD	= 5;
}
