package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.provider.EDataTypeItemProvider;

public class CustomEDataTypeItemProvider extends EDataTypeItemProvider
{

	public CustomEDataTypeItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	@Override
	public String getText(Object object)
	{
		return getFullPackageName(((EDataType) object).getEPackage()) + "." + super.getText(object);
	}

	private String getFullPackageName(EPackage ePackage)
	{
		if (ePackage == null)
		{
			return "null";
		}

		String string = ePackage.getName();

		while (ePackage.getESuperPackage() != null)
		{
			ePackage = ePackage.getESuperPackage();
			string = ePackage.getName() + "." + string;
		}

		return string;
	}
}
