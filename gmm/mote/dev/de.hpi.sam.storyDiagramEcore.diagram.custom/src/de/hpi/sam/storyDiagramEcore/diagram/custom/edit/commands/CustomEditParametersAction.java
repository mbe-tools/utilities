package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.commands;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IObjectActionDelegate;

import de.hpi.sam.storyDiagramEcore.diagram.custom.CommonEditParametersDialog;
import de.hpi.sam.storyDiagramEcore.diagram.custom.part.CustomAbstractActionDelegate;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart;
import de.hpi.sam.storyDiagramEcore.nodes.InitialNode;

public class CustomEditParametersAction extends CustomAbstractActionDelegate implements IObjectActionDelegate
{

	protected CommonEditParametersDialog	editParamsDialog;

	@Override
	protected void doRun(IProgressMonitor progressMonitor)
	{
		if (this.getInitialNode().getActivity().getSpecification() == null)
		{
			editParamsDialog = new CommonEditParametersDialog(null);
			editParamsDialog.setActivity(this.getInitialNode().getActivity());
			editParamsDialog.setInitialNodeEditPart(this.getInitialNodeEditPart());
			editParamsDialog.open();
		}
		else
		{
			String[] labels =
			{
					"Ok", "Cancel"
			};

			(new MessageDialog(null, "Specification exists", null, "No parameters needed. The activity has got a specification.",
					MessageDialog.INFORMATION, labels, 0)).open();
		}
	}

	protected InitialNode getInitialNode()
	{
		return (InitialNode) ((View) ((InitialNodeEditPart) getStructuredSelection().getFirstElement()).getModel()).getElement();
	}

	protected InitialNodeEditPart getInitialNodeEditPart()
	{
		return (InitialNodeEditPart) getStructuredSelection().getFirstElement();
	}

}
