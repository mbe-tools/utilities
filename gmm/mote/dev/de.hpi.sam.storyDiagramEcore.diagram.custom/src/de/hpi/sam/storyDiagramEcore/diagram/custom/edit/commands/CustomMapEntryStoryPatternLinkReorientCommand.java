package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreUtils;
import de.hpi.sam.storyDiagramEcore.diagram.edit.commands.MapEntryStoryPatternLinkReorientCommand;

public class CustomMapEntryStoryPatternLinkReorientCommand extends MapEntryStoryPatternLinkReorientCommand
{

	public CustomMapEntryStoryPatternLinkReorientCommand(ReorientRelationshipRequest request)
	{
		super(request);
	}

	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
	{
		if (!canExecute())
		{
			throw new ExecutionException("Invalid arguments in reorient link command"); //$NON-NLS-1$
		}

		CommandResult result;
		if (this.reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE)
		{
			result = reorientSource();
		}
		else if (this.reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET)
		{
			result = reorientTarget();
		}
		else
		{
			throw new IllegalStateException();
		}

		EReference eReference = null;
		if (this.getLink().getSource().getClassifier() != null && this.getLink().getTarget().getClassifier() != null
				&& this.getLink().getSource().getClassifier() instanceof EClass)
		{
			eReference = StoryDiagramEcoreUtils.getMapTypedEReference((EClass) this.getLink().getSource().getClassifier(), this.getLink()
					.getTarget().getClassifier());
		}

		this.getLink().setEStructuralFeature(eReference);
		return result;
	}

}
