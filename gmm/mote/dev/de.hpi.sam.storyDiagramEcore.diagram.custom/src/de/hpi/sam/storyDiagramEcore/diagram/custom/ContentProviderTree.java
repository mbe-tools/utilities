package de.hpi.sam.storyDiagramEcore.diagram.custom;
import java.util.ArrayList;
import java.util.List;

import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;

public enum ContentProviderTree {
	INSTANCE;
	List<CallActionExpression> list = new ArrayList<CallActionExpression>();
	
	private ContentProviderTree() {
		//list.add("branch1");
		//list.add("branch2");
		CallActionExpression e1 = null;
		CallActionExpression e2 = null;
		list.add(e1);
		list.add(e2);
	}
	
	public List<CallActionExpression> getModel(){
		return list;
	}
}
