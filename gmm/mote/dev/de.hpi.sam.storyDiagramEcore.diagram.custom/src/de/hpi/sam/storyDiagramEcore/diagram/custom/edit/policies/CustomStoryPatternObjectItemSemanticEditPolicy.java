package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import de.hpi.sam.storyDiagramEcore.diagram.custom.edit.commands.CustomMapEntryStoryPatternLinkCreateCommand;
import de.hpi.sam.storyDiagramEcore.diagram.custom.edit.commands.CustomMapEntryStoryPatternLinkReorientCommand;
import de.hpi.sam.storyDiagramEcore.diagram.custom.edit.commands.CustomStoryPatternLinkCreateCommand;
import de.hpi.sam.storyDiagramEcore.diagram.custom.edit.commands.CustomStoryPatternLinkReorientCommand;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryPatternObjectItemSemanticEditPolicy;
import de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes;

public class CustomStoryPatternObjectItemSemanticEditPolicy extends StoryPatternObjectItemSemanticEditPolicy
{

	protected Command getReorientRelationshipCommand(ReorientRelationshipRequest req)
	{
		switch (getVisualID(req))
		{
			case StoryPatternLinkEditPart.VISUAL_ID:
				return getGEFWrapper(new CustomStoryPatternLinkReorientCommand(req));
			case MapEntryStoryPatternLinkEditPart.VISUAL_ID:
				return getGEFWrapper(new CustomMapEntryStoryPatternLinkReorientCommand(req));
		}

		return super.getReorientRelationshipCommand(req);
	}

	protected Command getStartCreateRelationshipCommand(CreateRelationshipRequest req)
	{
		if (StoryDiagramEcoreElementTypes.StoryPatternLink_4007 == req.getElementType())
		{
			return getGEFWrapper(new CustomStoryPatternLinkCreateCommand(req, req.getSource(), req.getTarget()));
		}
		else if (StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010 == req.getElementType())
		{
			return getGEFWrapper(new CustomMapEntryStoryPatternLinkCreateCommand(req, req.getSource(), req.getTarget()));
		}

		return super.getStartCreateRelationshipCommand(req);
	}

	protected Command getCompleteCreateRelationshipCommand(CreateRelationshipRequest req)
	{
		if (StoryDiagramEcoreElementTypes.StoryPatternLink_4007 == req.getElementType())
		{
			return getGEFWrapper(new CustomStoryPatternLinkCreateCommand(req, req.getSource(), req.getTarget()));
		}
		else if (StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010 == req.getElementType())
		{
			return getGEFWrapper(new CustomMapEntryStoryPatternLinkCreateCommand(req, req.getSource(), req.getTarget()));
		}

		return super.getCompleteCreateRelationshipCommand(req);
	}
}
