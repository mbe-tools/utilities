package de.hpi.sam.properties.storyDiagramEcore;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.PropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin;

public class GenericPropertySection extends AbstractPropertySection
{

	protected PropertySheetPage	page;

	public void createControls(Composite parent, TabbedPropertySheetPage tabbedPropertySheetPage)
	{
		super.createControls(parent, tabbedPropertySheetPage);
		Composite composite = getWidgetFactory().createFlatFormComposite(parent);
		page = new PropertySheetPage();
		page.setPropertySourceProvider(new AdapterFactoryContentProvider(StoryDiagramEcoreDiagramEditorPlugin.getInstance()
				.getItemProvidersAdapterFactory()));
		page.createControl(composite);
		FormData data = new FormData();
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(100, 0);
		data.top = new FormAttachment(0, 0);
		data.bottom = new FormAttachment(100, 0);
		page.getControl().setLayoutData(data);
	}

	public void setInput(IWorkbenchPart part, ISelection selection)
	{
		super.setInput(part, selection);
		page.selectionChanged(part, selection);
	}

	public void dispose()
	{
		super.dispose();

		if (page != null)
		{
			page.dispose();
			page = null;
		}
	}

	public void refresh()
	{
		page.refresh();
	}

	public boolean shouldUseExtraSpace()
	{
		return true;
	}

}