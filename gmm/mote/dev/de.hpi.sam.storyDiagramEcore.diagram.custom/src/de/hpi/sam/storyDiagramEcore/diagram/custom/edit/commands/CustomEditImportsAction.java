package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.commands;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.ui.IObjectActionDelegate;

import de.hpi.sam.storyDiagramEcore.diagram.custom.ActivityEditListFeatureDialog;
import de.hpi.sam.storyDiagramEcore.diagram.custom.CommonEditImportsDialog;
import de.hpi.sam.storyDiagramEcore.diagram.custom.part.CustomAbstractActionDelegate;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart;
import de.hpi.sam.storyDiagramEcore.nodes.InitialNode;

public class CustomEditImportsAction extends CustomAbstractActionDelegate implements IObjectActionDelegate
{

	protected ActivityEditListFeatureDialog	editImportsDialog;

	@Override
	protected void doRun(IProgressMonitor progressMonitor)
	{
		editImportsDialog = new CommonEditImportsDialog(null);
		editImportsDialog.setActivity(this.getInitialNode().getActivity());
		editImportsDialog.setInitialNodeEditPart(this.getInitialNodeEditPart());
		editImportsDialog.open();
	}

	protected InitialNode getInitialNode()
	{
		return (InitialNode) ((View) ((InitialNodeEditPart) getStructuredSelection().getFirstElement()).getModel()).getElement();
	}

	protected InitialNodeEditPart getInitialNodeEditPart()
	{
		return (InitialNodeEditPart) getStructuredSelection().getFirstElement();
	}

}
