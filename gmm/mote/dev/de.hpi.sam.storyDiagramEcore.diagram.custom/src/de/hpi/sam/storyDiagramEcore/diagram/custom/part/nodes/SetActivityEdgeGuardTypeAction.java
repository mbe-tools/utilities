package de.hpi.sam.storyDiagramEcore.diagram.custom.part.nodes;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;

import de.hpi.sam.storyDiagramEcore.diagram.custom.EditExpressionDialog;
import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.custom.part.CustomAbstractActionDelegate;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;

public class SetActivityEdgeGuardTypeAction extends CustomAbstractActionDelegate
{

	ActivityEdge			activityEdge;

	ActivityEdgeEditPart	activityEdgeEditPart;

	public void selectionChanged(IAction action, ISelection selection)
	{
		activityEdgeEditPart = null;
		activityEdge = null;
		if (selection instanceof IStructuredSelection)
		{
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			if (structuredSelection.getFirstElement() instanceof ActivityEdgeEditPart)
			{
				activityEdgeEditPart = (ActivityEdgeEditPart) structuredSelection.getFirstElement();
				activityEdge = (ActivityEdge) activityEdgeEditPart.getPrimaryView().getElement();
			}
		}

		if (activityEdge != null)
		{
			if (Utility.GuardEnum2String(activityEdge.getGuardType()).equals(action.getText()))
			{
				action.setChecked(true);
			}
			else
			{
				action.setChecked(false);
			}
		}
	}

	@Override
	protected void doRun(IProgressMonitor progressMonitor)
	{

		final String actEdgeGuardValue = getAction().getText();

		AbstractTransactionalCommand command = new AbstractTransactionalCommand(activityEdgeEditPart.getEditingDomain(), "Set GuardType",
				null)
		{
			protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
			{
				ActivityEdgeGuardEnumeration newGuard = Utility.String2GuardEnum(actEdgeGuardValue);

				if (newGuard != activityEdge.getGuardType())
				{
					activityEdge.setGuardType(newGuard);

					if (activityEdge.getGuardType() == ActivityEdgeGuardEnumeration.BOOLEAN)
					{
						EditExpressionDialog editExpressionDialog = new EditExpressionDialog(null);

						editExpressionDialog.setActivity(activityEdge.getActivity());
						editExpressionDialog.setExpectedClassifier(EcorePackage.eINSTANCE.getEBoolean());
						editExpressionDialog.setExpressionOwner(activityEdge);
						editExpressionDialog.setStructuralFeature(NodesPackage.eINSTANCE.getActivityEdge_GuardExpression());

						if (editExpressionDialog.open() == Window.OK)
						{
							activityEdge.setGuardExpression(editExpressionDialog.getExpression());
						}
						else
						{
							activityEdge.setGuardType(ActivityEdgeGuardEnumeration.NONE);
						}
					}
					else
					{
						activityEdge.setGuardExpression(null);
					}
				}

				return CommandResult.newOKCommandResult();
			}
		};

		execute(command);

	}
}
