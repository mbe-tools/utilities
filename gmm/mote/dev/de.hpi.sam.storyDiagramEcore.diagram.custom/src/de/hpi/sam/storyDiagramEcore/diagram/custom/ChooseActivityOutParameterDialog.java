package de.hpi.sam.storyDiagramEcore.diagram.custom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.impl.EcoreFactoryImpl;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.ActivityParameter;
import de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.expressions.StringExpression;
import de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsFactoryImpl;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;

/**
 * 
 * This dialog provides buttons to choose which Out Parameter should be edited.
 * It depends on information about the diagram and the stopNode which
 * expressions should be edited. It uses the EditExpressionDialog to edit the
 * expression. The mapping of expresssions and Out Parameters depends on the
 * order and number of expressions.
 * 
 * @author Patrick Rein (pre)
 */
public class ChooseActivityOutParameterDialog extends Dialog
{

	protected static final String		DIALOG_TITLE	= "Out Parameter";
	protected static final int			DIALOG_WIDTH	= 400;
	protected static final int			DIALOG_HEIGHT	= 400;
	protected static final int			DIALOG_PADDING	= 10;
	protected static final int			BUTTON_HEIGHT	= 35;
	protected String					ITEM_CONTAINER_TEXT		= "Out Parameters";
	protected Map<Button, Integer>		buttonDictionary;
	private List<EParameter>			outgoingParameters;
	private ActivityFinalNode			finalNode;
	private Activity					activity;
	private TransactionalEditingDomain	changeCommandReceiver;
	private ActivityFinalNodeEditPart	editPart;
	private EditExpressionDialog		editExpressionDialog;
	
	private Composite 					composite;
	protected Group						itemContainer;
	
	private Composite[] parameterRows = new Composite[256];
	private EParameter[] parameters= new EParameter[256];
	private ActivityParameter[] activityParameters= new ActivityParameter[256];
	private int numberOfParameter = 0;

	public ChooseActivityOutParameterDialog(Shell parent)
	{
		super(parent);
	}

	@Override
	public int open()
	{
		List<EParameter> outParameters = this.getOutParameters();
		Integer difference = outParameters.size() - this.finalNode.getOutParameterValues().size();
		Expression e = null;
		if (difference > 0)
		{
			int baseIndex = this.finalNode.getOutParameterValues().size();
			for (int i = 0; i < difference; i++)
			{
				e = ExpressionsFactoryImpl.eINSTANCE.createStringExpression();
				this.changeCommandReceiver.getCommandStack().execute(
						new AddCommand(this.editPart.getEditingDomain(), this.finalNode, this.finalNode.eClass().getEStructuralFeature(
								"outParameterValues"), e, baseIndex + i));
			}
		}
		else if (difference < 0)
		{
			//int baseIndex = outParameters.size();
			for (int i = 0; i < (-difference); i++)
			{
				e = ExpressionsFactoryImpl.eINSTANCE.createStringExpression();
				this.changeCommandReceiver.getCommandStack().execute(
						new RemoveCommand(this.editPart.getEditingDomain(), this.finalNode, this.finalNode.eClass().getEStructuralFeature(
								"outParameterValues"), this.finalNode.getOutParameterValues().get(i)));
			}
		}
		return super.open();
	}

	private List<EParameter> getOutParameters()
	{
		if (outgoingParameters == null)
		{
			EParameter tempParam;
			List<EParameter> resultParameters = new ArrayList<EParameter>();
			if (this.activity.getSpecification() != null)
			{
				tempParam = EcoreFactoryImpl.eINSTANCE.createEParameter();
				tempParam.setName(this.activity.getSpecification().getName());
				tempParam.setEType(this.activity.getSpecification().getEType());
				resultParameters.add(tempParam);
			}
			else
			{
				for (ActivityParameter aParameter : this.activity.getParameters())
				{
					if (aParameter.getDirection() == ActivityParameterDirectionEnum.INOUT
							|| aParameter.getDirection() == ActivityParameterDirectionEnum.OUT)
					{
						tempParam = EcoreFactoryImpl.eINSTANCE.createEParameter();
						tempParam.setEType(aParameter.getType());
						tempParam.setName(aParameter.getName());
						resultParameters.add(tempParam);
					}
				}
			}
			this.outgoingParameters = resultParameters;
		}

		return this.outgoingParameters;
	}

	@Override
	public void create()
	{
		super.create();
	}

	@Override
	public boolean close()
	{
		return super.close();
	}

	@Override
	protected void okPressed()
	{
		setReturnCode(OK);
		this.close();
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#
	 *      createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	protected Control createDialogArea(Composite parent)
	{

		this.getShell().setText(DIALOG_TITLE);

		composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new RowLayout(SWT.VERTICAL | SWT.CENTER));
		composite.setSize(DIALOG_WIDTH, DIALOG_HEIGHT);
		
		itemContainer = new Group(composite, SWT.NONE);
		itemContainer.setText(ITEM_CONTAINER_TEXT);
		itemContainer.setLayout(new RowLayout(SWT.VERTICAL));
		
		buttonDictionary = new HashMap<Button, Integer>();

		this.numberOfParameter = this.getOutParameters().size();
		this.activityParameters = (ActivityParameter[]) this.activity.getParameters().toArray();
		EParameter aParameter;
		for (int i = 0; i < this.getOutParameters().size(); i++)
		{
			aParameter = this.getOutParameters().get(i);
			this.createParameterRow(aParameter, i);
		}
		return composite;
	}
	private void parameterDown(int number){
		int n = numberOfParameter;
		EParameter actParTop = parameters[number];
		EParameter actParBottom = parameters[number+1];
		EParameter actParHelp = actParTop;
			Composite comT = parameterRows[number];
			Composite comB = parameterRows[number+1];
			Composite comHelp = comT;
			parameters[number]=actParBottom;
			parameters[number+1] = actParHelp;
			parameterRows[number]=comB;
			parameterRows[number+1] = comHelp;
			this.changeActivityParametersPosition(number, number+1);
			for (int j = number; j< n;j++){
				removeOldParameter(parameters[j], parameterRows[j]);
			}
			for (int j = number; j< n;j++){
				addOldParameter(parameters[j],j);
			}
			writeOldActivityParameters();
			this.refreshVisually();
	}
	
	private int findActivityParameter(EParameter aParameter){
		int j = 0;
		for (int i = 0; i< this.activity.getParameters().size();i++){
			if ((this.activityParameters[i].getName()).equals(aParameter.getName())){
				j=i;
			}
		}
		return j;
	}
	
	private void changeActivityParametersPosition(int numberA, int numberB){
		int a,b;
		a= findActivityParameter(parameters[numberA]);
		b= findActivityParameter(parameters[numberB]);
		ActivityParameter h = this.activityParameters[a];
		this.activityParameters[a]=this.activityParameters[b];
		this.activityParameters[b]=h;
	}
	
	private void parameterUp(int number){
		if (number > 0){
			EParameter actParTop = parameters[number-1];
			EParameter actParBottom = parameters[number];
			EParameter actParHelp = actParTop;
			Composite comT = parameterRows[number-1];
			Composite comB = parameterRows[number];
			Composite comH = comT;
	
			parameters[number -1 ]=actParBottom;
			parameters[number] = actParHelp;
			parameterRows[number -1 ]=comB;
			parameterRows[number] = comH;
			this.changeActivityParametersPosition(number -1, number);
			int n = numberOfParameter;
			for (int j = number-1; j< n;j++){
				removeOldParameter(parameters[j], parameterRows[j]);
						}
			for (int j = number-1; j< n;j++){
				addOldParameter(parameters[j],j);
			}
			writeOldActivityParameters();
			this.refreshVisually();
		}
	}
	
	protected void removeOldParameter(final EParameter aParameter, final Composite aParameterRow)
	{
//		Expression e = null;
//		e = ExpressionsFactoryImpl.eINSTANCE.createStringExpression();
		this.changeCommandReceiver.getCommandStack().execute(
				new RemoveCommand(this.editPart.getEditingDomain(), this.finalNode, this.finalNode.eClass().getEStructuralFeature(
						"outParameterValues"), aParameter));
		aParameterRow.dispose();
		this.refreshVisually();
	}
	
	protected void removeOldActivityParameter(final ActivityParameter aParameter)
	{
		final EditingDomain domain = TransactionUtil.getEditingDomain(this.activity);
		domain.getCommandStack().execute(
				new RemoveCommand(this.editPart.getEditingDomain(), this.activity, this.activity.eClass()
						.getEStructuralFeature("parameters"), aParameter));
		this.refreshVisually();
	}
	
	protected void writeOldActivityParameters(){
		for (ActivityParameter aParam : this.activityParameters){
			removeOldActivityParameter(aParam);
		}
		for (ActivityParameter aParam : this.activityParameters){
			addOldActivityParameter(aParam);
		}
	}

	protected void addOldParameter(EParameter aParameter, int number)
	{
		//addOldActivityParameter(aParameter, number);
		//Expression e = ExpressionsFactoryImpl.eINSTANCE.createStringExpression();
		this.changeCommandReceiver.getCommandStack().execute(
				new AddCommand(this.editPart.getEditingDomain(), this.finalNode, this.finalNode.eClass().getEStructuralFeature(
						"outParameterValues"), aParameter, numberOfParameter + 1));
		this.createParameterRow(aParameter, number);
		this.refreshVisually();
	}

	protected void addOldActivityParameter(ActivityParameter aParameter)
	{
		final EditingDomain domain = TransactionUtil.getEditingDomain(this.activity);

		domain.getCommandStack().execute(
				new AddCommand(this.editPart.getEditingDomain(), this.activity, this.activity.eClass().getEStructuralFeature(
						"parameters"), aParameter));

		//this.createParameterRow(aParameter, number);
		this.refreshVisually();
	}
	
	protected void editOutExpression(SelectionEvent e)
	{
		//EditExpressionDialog expressionDialog;
		int parameterIndex = this.buttonDictionary.get(e.getSource());
		EParameter aParameter = this.getOutParameters().get(parameterIndex);

		EditExpressionDialog dialog = new EditExpressionDialog(null);
		dialog.setActivity(this.activity);
		editExpressionDialog = dialog;
		editExpressionDialog.setExpression(this.getExpression(parameterIndex));
		editExpressionDialog.setExpectedClassifier(aParameter.getEType());
		editExpressionDialog.setContextClassifier(null);
		editExpressionDialog.setExpressionOwner(this.finalNode);
		editExpressionDialog.setEditingDomain(this.editPart.getEditingDomain());
		editExpressionDialog.setStructuralFeature(this.finalNode.eClass().getEStructuralFeature("outParameterValues"));

		dialog.open();
	}

	public void setChangeCommandReceiver(TransactionalEditingDomain changeCommandReceiver)
	{
		this.changeCommandReceiver = changeCommandReceiver;
	}

	public void setEditPart(ActivityFinalNodeEditPart stopNodeEP)
	{
		this.editPart = stopNodeEP;
	}

	protected StringExpression getExpression(int index)
	{
		EList<Expression> el = this.finalNode.getOutParameterValues();
		return (StringExpression) el.get(index);
	}

	public void setNode(ActivityFinalNode model)
	{
		this.finalNode = model;
	}

	public void setActivity(Activity activity)
	{
		this.activity = activity;
	}
	protected void createParameterRow(final EParameter aParameter, final int number)
	{
		/*String aString = "";

		final Composite aParameterRow = new Composite(this.itemContainer, SWT.NONE);
		aParameterRow.setLayout(new RowLayout());
		parameterRows[number]= aParameterRow;
		parameters[number]= aParameter;*/
		String s;
		Button b;
		final Composite aParameterRow = new Composite(itemContainer, SWT.NONE);
		aParameterRow.setLayout(new RowLayout());

		parameterRows[number]= aParameterRow;
		parameters[number]= aParameter;
		if (aParameter != null && aParameter.getEType() != null)
		{
			s = aParameter.getEType().getName();
			s = s + " " + aParameter.getName();

			b = new Button(aParameterRow, SWT.PUSH);
			b.setLayoutData(new RowData(DIALOG_WIDTH/3*2, BUTTON_HEIGHT));
			b.setText(s);
			b.addSelectionListener(new SelectionListener()
			{

				@Override
				public void widgetSelected(SelectionEvent e)
				{
					editOutExpression(e);
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e)
				{
					widgetSelected(e);
				}

			});
			buttonDictionary.put(b, number);
			
			
			Button upButton = new Button(aParameterRow, SWT.PUSH);
			upButton.setText("Up");
			upButton.setLayoutData(new RowData(DIALOG_WIDTH/6, BUTTON_HEIGHT));
			upButton.addSelectionListener(new SelectionListener() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					parameterUp(number);
				}
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {}
			});
			if (number <= 0){
				upButton.setEnabled(false);
			}
			Button downButton = new Button(aParameterRow, SWT.PUSH);
			downButton.setText("Down");
			downButton.setLayoutData(new RowData(DIALOG_WIDTH/6, BUTTON_HEIGHT));
			downButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					parameterDown(number);
				}
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {}
			});
			if (number > this.getOutParameters().size()-2){
				downButton.setEnabled(false);
			}
		}
	}
	protected void refreshVisually()
	{
		((Composite) this.getDialogArea()).layout(true);
		this.getShell().pack();
		itemContainer.layout();
	}

}
