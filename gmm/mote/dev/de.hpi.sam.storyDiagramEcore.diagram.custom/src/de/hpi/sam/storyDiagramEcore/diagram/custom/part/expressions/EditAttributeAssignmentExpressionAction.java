package de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.diagram.ui.editparts.CompartmentEditPart;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

public class EditAttributeAssignmentExpressionAction extends EditExpressionAction
{

	@Override
	protected EClassifier getExpectedClassifier()
	{
		EStructuralFeature feature = ((AttributeAssignment) this.getExpressionOwner()).getEStructuralFeature();

		if (feature != null)
		{
			return feature.getEType();
		}
		else
		{
			return null;
		}
	}

	@Override
	protected Expression getExpression()
	{
		return (Expression) ((AttributeAssignment) this.getExpressionOwner()).getAssignmentExpression();
	}

	@Override
	protected TransactionalEditingDomain getEditingDomain()
	{
		return ((CompartmentEditPart) getStructuredSelection().getFirstElement()).getEditingDomain();
	}

	@Override
	protected void setNewExpression(Expression newExpression)
	{
		AttributeAssignment aa = (AttributeAssignment) this.getExpressionOwner();

		aa.setAssignmentExpression(newExpression);
	}

	@Override
	protected EClassifier getContextClassifier()
	{
		return ((StoryPatternObject) ((View) ((CompartmentEditPart) getStructuredSelection().getFirstElement()).getParent().getParent()
				.getModel()).getElement()).getClassifier();
	}

	@Override
	protected Activity getActivity()
	{
		EObject container = ((StoryPatternObject) ((View) ((CompartmentEditPart) getStructuredSelection().getFirstElement()).getParent()
				.getParent().getModel()).getElement()).eContainer();

		while (!(container instanceof Activity))
		{
			container = container.eContainer();
		}

		return (Activity) container;
	}

	@Override
	protected EObject getExpressionOwner()
	{
		return ((View) ((CompartmentEditPart) getStructuredSelection().getFirstElement()).getModel()).getElement();
	}

	@Override
	protected void prepareDialog()
	{
		super.prepareDialog();
		editExpressionDialog.setStructuralFeature(SdmPackage.eINSTANCE.getAttributeAssignment_AssignmentExpression());
	}
}
