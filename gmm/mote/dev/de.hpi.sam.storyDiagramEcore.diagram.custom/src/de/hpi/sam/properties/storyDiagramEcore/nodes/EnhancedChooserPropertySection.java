package de.hpi.sam.properties.storyDiagramEcore.nodes;

import de.hpi.sam.properties.FixedChooserPropertySection;
import de.hpi.sam.properties.SectionHelper;
import de.hpi.sam.storyDiagramEcore.nodes.provider.NodesItemProviderAdapterFactory;

public abstract class EnhancedChooserPropertySection extends FixedChooserPropertySection
{
	@Override
	final protected Object[] getComboFeatureValues()
	{
		return SectionHelper.getAvailableObjects(this.getEObject(), this.getFeature(), NodesItemProviderAdapterFactory.class);
	}
}
