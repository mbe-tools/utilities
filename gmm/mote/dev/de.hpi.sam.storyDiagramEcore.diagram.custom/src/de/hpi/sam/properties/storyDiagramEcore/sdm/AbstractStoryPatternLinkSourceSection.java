package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EStructuralFeature;

public class AbstractStoryPatternLinkSourceSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getAbstractStoryPatternLink_Source();
	}

	@Override
	protected String getLabelText()
	{
		return "Source";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink) getEObject()).getSource();
	}
}
