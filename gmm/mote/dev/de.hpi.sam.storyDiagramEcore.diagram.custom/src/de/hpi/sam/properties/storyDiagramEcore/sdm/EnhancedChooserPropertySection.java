package de.hpi.sam.properties.storyDiagramEcore.sdm;

import de.hpi.sam.properties.FixedChooserPropertySection;
import de.hpi.sam.properties.SectionHelper;
import de.hpi.sam.storyDiagramEcore.sdm.provider.SdmItemProviderAdapterFactory;

public abstract class EnhancedChooserPropertySection extends FixedChooserPropertySection
{
	@Override
	final protected Object[] getComboFeatureValues()
	{
		return SectionHelper.getAvailableObjects(this.getEObject(), this.getFeature(), SdmItemProviderAdapterFactory.class);
	}
}
