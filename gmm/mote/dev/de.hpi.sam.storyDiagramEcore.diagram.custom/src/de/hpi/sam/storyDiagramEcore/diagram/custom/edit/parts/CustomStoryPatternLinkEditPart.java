package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.Graphics;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Anchor;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.RelativeBendpoints;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.ILineIsHideableFigure;
import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

public class CustomStoryPatternLinkEditPart extends StoryPatternLinkEditPart
{
	private boolean	selectable	= true;

	public CustomStoryPatternLinkEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification notification)
	{
		if (notification.getEventType() != Notification.REMOVE)
		{
			if (notification.getNotifier() instanceof StoryPatternLink || notification.getNotifier() instanceof StoryPatternObject)
			{
				adaptColor();
			}

			if (notification.getNotifier() instanceof StoryPatternLink)
			{
				switch (notification.getFeatureID(StoryPatternLink.class))
				{
					case SdmPackage.STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK: {
						refreshMergedStoryPatternLink();
						
						try {
							trackMergedStoryPatternLink(true);
						}
						catch ( final ExecutionException p_ex ) {
							p_ex.printStackTrace();
						}
						
						break;
					}
				}
			}
		}

		if (NotationPackage.Literals.ROUTING_STYLE__ROUTING.equals(notification.getFeature())
				|| notification.getNotifier() instanceof RelativeBendpoints || notification.getNotifier() instanceof Anchor) {
			try {
				trackMergedStoryPatternLink( true );
			}
			catch ( final ExecutionException p_ex ) {
				p_ex.printStackTrace();
			}
		}

		super.handleNotificationEvent(notification);
	}

	@Override
	protected void refreshVisuals()	{
		super.refreshVisuals();
		
		refreshMergedStoryPatternLink();
		
		try {
			this.trackMergedStoryPatternLink(false);
		}
		catch ( final ExecutionException p_ex ) {
			p_ex.printStackTrace();
		}
		
		adaptColor();
	}

	protected void adaptColor()
	{
		Utility.adaptColor(getPrimaryShape(), ((StoryPatternElement) ((View) this.getModel()).getElement()).getModifier());
		Utility.adaptLineStyle(getPrimaryShape(), ((StoryPatternLink) ((View) this.getModel()).getElement()).getMatchType());
	}

	@Override
	protected Connection createConnectionFigure()
	{
		return new StoryPatternLinkFigure();
	}

	/**
	 * @see org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionEditPart#isSelectable()
	 */
	@Override
	public boolean isSelectable()
	{
		return selectable;
	}

	/**
	 * @param selectable
	 *            the selectable to set
	 */
	public void setSelectable(boolean selectable)
	{
		this.selectable = selectable;
	}

	/**
	 * Refresh reference decoration and visibility
	 */
	protected void refreshMergedStoryPatternLink()
	{
		EObject semanticElement = resolveSemanticElement();
		if (!(semanticElement instanceof StoryPatternLink))
		{
			return;
		}

		StoryPatternLinkFigure fig = (StoryPatternLinkFigure) getPrimaryShape();
		if (((StoryPatternLink) semanticElement).getOppositeStoryPatternLink() != null)
		{
			fig.displayTargetDecoration(false);
		}
		else
		{
			fig.setHideLine(false);
			fig.displayTargetDecoration(true);
		}
	}

	protected void trackMergedStoryPatternLink(boolean trackFully)
	throws ExecutionException {
		EObject semanticElement = resolveSemanticElement();
		if (!(semanticElement instanceof StoryPatternLink))
		{
			return;
		}

		EObject opLink = ((StoryPatternLink) semanticElement).getOppositeStoryPatternLink();
		if (opLink != null)
		{
			Utility.trackMergedLinks(this, opLink, trackFully);
		}
	}

	public class StoryPatternLinkFigure extends StoryPatternLinkEditPart.StoryPatternLinkFigureDescriptor implements ILineIsHideableFigure
	{
		private boolean	hideLine;

		/**
		 * @return the hideLine
		 */
		public boolean isHideLine()
		{
			return hideLine;
		}

		/**
		 * @param hideLine
		 *            the hideLine to set
		 */
		public void setHideLine(boolean hideLine)
		{
			this.hideLine = hideLine;

			synchronized (CustomStoryPatternLinkEditPart.this)
			{
				if (hideLine)
				{
					getViewer().deselect(CustomStoryPatternLinkEditPart.this);
					CustomStoryPatternLinkEditPart.this.setSelected(SELECTED_NONE);
					CustomStoryPatternLinkEditPart.this.setSelectable(false);
				}
				else
				{
					CustomStoryPatternLinkEditPart.this.setSelectable(true);
				}
			}
		}

		public void displayTargetDecoration(boolean display)
		{
			if (display)
			{
				setTargetDecoration(this.createTargetDecoration());
			}
			else
			{
				if (getTargetDecoration() != null)
				{
					setTargetDecoration(null);
				}
			}
		}

		@Override
		public void paintFigure(Graphics graphics)
		{
			if (this.hideLine)
			{
				return;
			}
			super.paintFigure(graphics);
		}
	}
}
