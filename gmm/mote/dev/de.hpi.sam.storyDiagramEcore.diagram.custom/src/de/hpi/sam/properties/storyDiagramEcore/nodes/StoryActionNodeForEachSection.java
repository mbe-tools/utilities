package de.hpi.sam.properties.storyDiagramEcore.nodes;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractBooleanPropertySection;

public class StoryActionNodeForEachSection extends AbstractBooleanPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE.getStoryActionNode_ForEach();
	}

	@Override
	protected String getLabelText()
	{
		return "For Each";
	}

}
