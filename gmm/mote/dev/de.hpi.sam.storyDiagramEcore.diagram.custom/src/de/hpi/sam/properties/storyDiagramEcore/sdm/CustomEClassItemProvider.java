package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.provider.EClassItemProvider;

public class CustomEClassItemProvider extends EClassItemProvider
{

	public CustomEClassItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	@Override
	public String getText(Object object)
	{
		return getFullPackageName(((EClass) object).getEPackage()) + "." + super.getText(object) + getURI((EObject) object);
	}

	private String getURI(EObject object)
	{
		if (object.eResource() != null)
		{
			return "          (contained in " + object.eResource().getURI() + ")";
		}
		else
		{
			return "";
		}
	}

	private String getFullPackageName(EPackage ePackage)
	{
		if (ePackage == null)
		{
			return "null";
		}

		String string = ePackage.getName();

		while (ePackage.getESuperPackage() != null)
		{
			ePackage = ePackage.getESuperPackage();
			string = ePackage.getName() + "." + string;
		}

		return string;
	}
}
