package de.hpi.sam.storyDiagramEcore.diagram.custom.part.nodes;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

import de.hpi.sam.storyDiagramEcore.diagram.custom.part.CustomAbstractActionDelegate;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart;

public class HideStoryPatternObjectAction extends CustomAbstractActionDelegate
{
	StoryPatternObjectEditPart	storyPatternObjectEditPart	= null;

	@Override
	public void selectionChanged(IAction act, ISelection selection)
	{
		if (selection instanceof IStructuredSelection)
		{
			storyPatternObjectEditPart = (StoryPatternObjectEditPart) ((IStructuredSelection) selection).getFirstElement();
		}
	}

	@Override
	protected void doRun(IProgressMonitor progressMonitor)
	{
		/*
		 * Hide connected links
		 */
		for (Object object : storyPatternObjectEditPart.getSourceConnections())
		{
			if (object instanceof GraphicalEditPart)
			{
				((GraphicalEditPart) object).getFigure().setVisible(false);
			}
		}

		for (Object object : storyPatternObjectEditPart.getTargetConnections())
		{
			if (object instanceof GraphicalEditPart)
			{
				((GraphicalEditPart) object).getFigure().setVisible(false);
			}
		}

		/*
		 * Hide the figure
		 */
		storyPatternObjectEditPart.getFigure().setVisible(false);

		/*
		 * Make sure the selection shape disappears.
		 */
		storyPatternObjectEditPart.setSelected(org.eclipse.gef.EditPart.SELECTED_NONE);

	}

}
