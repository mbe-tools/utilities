package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectModifierLabelEditPart;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

public class CustomStoryPatternObjectModifierLabelEditPart extends StoryPatternObjectModifierLabelEditPart
{

	public CustomStoryPatternObjectModifierLabelEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof StoryPatternObject)
		{
			adjustColorAndModifierLabel();
		}

		super.handleNotificationEvent(event);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		adjustColorAndModifierLabel();
	}

	protected void adjustColorAndModifierLabel()
	{
		Utility.adaptColor(getFigure(), ((StoryPatternElement) ((View) this.getModel()).getElement()).getModifier());

		setLabelText(Utility.adaptStoryPatternElementModifierText(this));
	}

}
