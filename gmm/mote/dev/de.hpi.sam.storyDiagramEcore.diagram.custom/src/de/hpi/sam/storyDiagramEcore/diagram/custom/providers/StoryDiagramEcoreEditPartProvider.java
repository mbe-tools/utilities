package de.hpi.sam.storyDiagramEcore.diagram.custom.providers;

import de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts.CustomStoryDiagramEcoreEditPartFactory;

public class StoryDiagramEcoreEditPartProvider extends de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreEditPartProvider
{

	public StoryDiagramEcoreEditPartProvider()
	{
		super();
		setFactory(new CustomStoryDiagramEcoreEditPartFactory());
	}

}
