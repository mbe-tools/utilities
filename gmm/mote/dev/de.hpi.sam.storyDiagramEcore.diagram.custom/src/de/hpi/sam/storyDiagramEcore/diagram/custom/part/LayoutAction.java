package de.hpi.sam.storyDiagramEcore.diagram.custom.part;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.emf.workspace.AbstractEMFOperation;
import org.eclipse.gmf.runtime.diagram.ui.services.layout.LayoutService;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;

import de.hpi.sam.storyDiagramEcore.diagram.custom.layout.TopDownProvider;
import de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditor;

public class LayoutAction implements IEditorActionDelegate
{
	protected IEditorPart	targetEditor;

	public void run(IAction action)
	{
		StoryDiagramEcoreDiagramEditor editor = (StoryDiagramEcoreDiagramEditor) targetEditor;
		final Diagram diag = editor.getDiagram();
		TransactionalEditingDomain ted = TransactionUtil.getEditingDomain(diag);
		AbstractEMFOperation operation = new AbstractEMFOperation(ted, "Arrange Story Diagram", null)
		{
			protected IStatus doExecute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
			{
				LayoutService.getInstance().layout(diag, TopDownProvider.ID);
				return Status.OK_STATUS;
			}
		};

		try
		{
			operation.execute(new NullProgressMonitor(), null);
		}
		catch (ExecutionException e)
		{
			e.printStackTrace();
		}
	}

	public void selectionChanged(IAction action, ISelection selection)
	{
	}

	public void setActiveEditor(IAction action, IEditorPart targetEditor)
	{
		this.targetEditor = targetEditor;
	}

}