package de.hpi.sam.properties.storyDiagramEcore.nodes;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.providers.TabbedPropertiesLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;

import de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;

public class ActivityNodeIncomingSection extends EnhancedListChooserPropertySection
{

	@Override
	protected EStructuralFeature getFeature()
	{
		return NodesPackage.eINSTANCE.getActivityNode_Incoming();
	}

	@Override
	protected String getLabelText()
	{
		return "Incoming Edges";
	}

	@Override
	protected IBaseLabelProvider getLabelProvider()
	{
		return new TabbedPropertiesLabelProvider(StoryDiagramEcoreDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory());
	}

	@Override
	protected Object getListValues()
	{
		return ((ActivityNode) this.getEObject()).getIncoming();
	}

}
