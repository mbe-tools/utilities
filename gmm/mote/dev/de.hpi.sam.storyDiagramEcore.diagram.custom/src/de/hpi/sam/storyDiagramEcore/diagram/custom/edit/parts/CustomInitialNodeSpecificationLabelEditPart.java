package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.ActivityParameter;
import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeSpecificationLabelEditPart;
import de.hpi.sam.storyDiagramEcore.nodes.InitialNode;

public class CustomInitialNodeSpecificationLabelEditPart extends InitialNodeSpecificationLabelEditPart
{

	public CustomInitialNodeSpecificationLabelEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void addNotationalListeners()
	{
		super.addNotationalListeners();

		InitialNode initialNode = (InitialNode) ((View) getModel()).getElement();

		addListenerFilter("Activity", this, initialNode.getActivity());
		this.addListener();
	}

	protected void updateFigure()
	{
		((WrappingLabel) getFigure()).setTextWrap(true);
		((WrappingLabel) getFigure()).setTextJustification(PositionConstants.CENTER);

		InitialNode initialNode = (InitialNode) ((View) getModel()).getElement();

		assert initialNode.getActivity() != null;

		if (initialNode.getActivity().getSpecification() != null)
		{
			setLabelText(Utility.getSpecificationString(initialNode.getActivity().getSpecification()));
		}
		else if (!initialNode.getActivity().getParameters().isEmpty())
		{
			StringBuilder labelBuilder = new StringBuilder();
			
			this.removeListener();
			this.addListener();
			
			for (ActivityParameter param : initialNode.getActivity().getParameters())
			{
				switch (param.getDirection())
				{
					case IN:
						labelBuilder.append("in ");
						break;
					case OUT:
						labelBuilder.append("out ");
						break;
					case INOUT:
						labelBuilder.append("inout ");
						break;
				}

				labelBuilder.append(param.getName());

				labelBuilder.append(":");

				if (param.getType() != null)
				{
					labelBuilder.append(param.getType().getName());
				}
				else
				{
					labelBuilder.append("[NULL]");
				}

				labelBuilder.append(System.getProperty("line.separator"));
			}

			setLabelText(labelBuilder.toString());
		}
		else
		{
			setLabelText("");
		}
	}
	
	private void addListener(){
		InitialNode initialNode = (InitialNode) ((View) getModel()).getElement();
		if (initialNode != null)
			for (int i = 0;i <initialNode.getActivity().getParameters().size();i++)
				addListenerFilter("Paramter" + i, this, (EObject) initialNode.getActivity().getParameters().toArray()[i]);
	}

	private void removeListener(){
		InitialNode initialNode = (InitialNode) ((View) getModel()).getElement();
		if (initialNode != null)
			for (int i = 0;i <initialNode.getActivity().getParameters().size()-1;i++)
				removeListenerFilter("Paramter" + i);
	}
	
	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof ActivityParameter)
		{
			updateFigure();
		}

		if (event.getNotifier() instanceof Activity)
		{
			updateFigure();
		}
		super.handleNotificationEvent(event);
	}
}
