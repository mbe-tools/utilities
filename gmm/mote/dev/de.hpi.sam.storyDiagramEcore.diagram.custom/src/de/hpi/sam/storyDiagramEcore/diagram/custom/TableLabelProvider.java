package de.hpi.sam.storyDiagramEcore.diagram.custom;

import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;

public class TableLabelProvider extends BaseLabelProvider implements
		ITableLabelProvider {

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		CallActionExpression todo = (CallActionExpression) element;
		switch (columnIndex) {
		case 0:
			return todo.getName();
		case 1:
			return todo.getDescription();
		}
		return "Not supported";
	}

}

