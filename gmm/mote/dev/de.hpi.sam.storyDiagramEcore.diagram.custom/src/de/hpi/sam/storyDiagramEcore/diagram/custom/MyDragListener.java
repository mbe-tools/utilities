package de.hpi.sam.storyDiagramEcore.diagram.custom;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.TextTransfer;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
public class MyDragListener implements DragSourceListener {

	private final TreeViewer viewer;

	public MyDragListener(TreeViewer viewer) {
		this.viewer = viewer;
	}

	@Override
	public void dragFinished(DragSourceEvent event) {
		System.out.println("Finshed Drag");
	}

	@Override
	public void dragSetData(DragSourceEvent event) {
		// Here you do the convertion to the type which is expected.
		IStructuredSelection selection = (IStructuredSelection) viewer
		.getSelection();
		/*Object[] array = (Object[]) selection.toArray();
		for (int i = 0; i<array.length; i++)
		{
			System.out.println(i + (array[i]).toString());
		}*/
		Expression firstElement = (Expression) selection.getFirstElement();
		
		if (TextTransfer.getInstance().isSupportedType(event.dataType)) {
			event.data = firstElement.getName();//getShortDescription() + " " + firstElement.getLongDescription(); 
			//JOptionPane.showMessageDialog(null,firstElement.getName(),"Titel", JOptionPane.INFORMATION_MESSAGE);
		}

	}

	@Override
	public void dragStart(DragSourceEvent event) {
		System.out.println("Start Drag");
	}

}

