package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeGuardConstraintLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeReturnValueLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeImportsLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeSpecificationLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkPositionConstraintLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkFeatureNameLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkKeyLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueLinkLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryDiagramEcoreEditPartFactory;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkFeatureNameLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectClassifierLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectModifierLabelEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectNameLabelEditPart;

public class CustomStoryDiagramEcoreEditPartFactory extends StoryDiagramEcoreEditPartFactory
{

	@Override
	public EditPart createEditPart(EditPart context, Object model)
	{
		if (model instanceof View)
		{
			View view = (View) model;
			switch (de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry.getVisualID(view))
			{
				case ActivityEditPart.VISUAL_ID:
					return new CustomActivityEditPart(view);

				case ActivityEdgeGuardConstraintLabelEditPart.VISUAL_ID:
					return new CustomActivityEdgeGuardConstraintLabelEditPart(view);

				case ActivityFinalNodeReturnValueLabelEditPart.VISUAL_ID:
					return new CustomActivityFinalNodeReturnValueLabelEditPart(view);

				case AttributeAssignmentEditPart.VISUAL_ID:
					return new CustomAttributeAssignmentEditPart(view);

				case CallActionExpressionEditPart.VISUAL_ID:
					return new CustomCallActionExpressionEditPart(view);

				case CallActionExpression2EditPart.VISUAL_ID:
					return new CustomCallActionExpression2EditPart(view);

				case CallActionExpression3EditPart.VISUAL_ID:
					return new CustomCallActionExpression3EditPart(view);

				case InitialNodeSpecificationLabelEditPart.VISUAL_ID:
					return new CustomInitialNodeSpecificationLabelEditPart(view);

				case LinkOrderConstraintLabelEditPart.VISUAL_ID:
					return new CustomLinkOrderConstraintLabelEditPart(view);

				case StoryActionNodeEditPart.VISUAL_ID:
					return new CustomStoryActionNodeEditPart(view);

				case StoryActionNodeStoryActionNodeElementsCompartmentEditPart.VISUAL_ID:
					return new CustomStoryActionNodeStoryActionNodeElementsCompartmentEditPart(view);

				case StoryPatternContainmentLinkEditPart.VISUAL_ID:
					return new CustomStoryPatternContainmentLinkEditPart(view);

				case StoryPatternLinkEditPart.VISUAL_ID:
					return new CustomStoryPatternLinkEditPart(view);

				case StoryPatternLinkFeatureNameLabelEditPart.VISUAL_ID:
					return new CustomStoryPatternLinkFeatureNameLabelEditPart(view);

				case LinkPositionConstraintLabelEditPart.VISUAL_ID:
					return new CustomLinkPositionConstraintLabelEditPart(view);

				case StoryPatternObjectEditPart.VISUAL_ID:
					return new CustomStoryPatternObjectEditPart(view);

				case StoryPatternObjectNameLabelEditPart.VISUAL_ID:
					return new CustomStoryPatternObjectNameLabelEditPart(view);

				case StoryPatternObjectModifierLabelEditPart.VISUAL_ID:
					return new CustomStoryPatternObjectModifierLabelEditPart(view);

				case StoryPatternObjectClassifierLabelEditPart.VISUAL_ID:
					return new CustomStoryPatternObjectClassifierLabelEditPart(view);

				case StoryPatternExpressionLinkEditPart.VISUAL_ID:
					return new CustomStoryPatternExpressionLinkEditPart(view);

				case StoryPatternExpressionLinkLabelEditPart.VISUAL_ID:
					return new CustomStoryPatternExpressionLinkFeatureNameLabelEditPart(view);

				case MapEntryStoryPatternLinkEditPart.VISUAL_ID:
					return new CustomMapEntryStoryPatternLinkEditPart(view);

				case MapEntryStoryPatternLinkFeatureNameLabelEditPart.VISUAL_ID:
					return new CustomMapEntryStoryPatternLinkFeatureNameLabelEditPart(view);

				case MapEntryStoryPatternLinkValueTargetEditPart.VISUAL_ID:
					return new CustomMapEntryStoryPatternLinkValueTargetEditPart(view);

				case MapEntryStoryPatternLinkKeyLabelEditPart.VISUAL_ID:
					return new CustomMapEntryStoryPatternLinkKeyLabelEditPart(view);

				case MapEntryStoryPatternLinkValueLinkLabelEditPart.VISUAL_ID:
					return new CustomMapEntryStoryPatternLinkValueLinkLabelEditPart(view);

				case InitialNodeImportsLabelEditPart.VISUAL_ID:
					return new CustomInitialNodeImportsLabelEditPart(view);

				case SemaphoreEditPart.VISUAL_ID:
					return new CustomSemaphoreEditPart(view);
			}
		}
		return super.createEditPart(context, model);
	}
}
