package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.ecore.provider.EcoreItemProviderAdapterFactory;

public class CustomEcoreItemProviderAdapterFactory extends EcoreItemProviderAdapterFactory
{
	private CustomEClassItemProvider	customEClassItemProvider;

	@Override
	public Adapter createEClassAdapter()
	{
		if (customEClassItemProvider == null)
		{
			customEClassItemProvider = new CustomEClassItemProvider(this);
		}

		return customEClassItemProvider;
	}

	private CustomEDataTypeItemProvider	customEDataTypeItemProvider;

	@Override
	public Adapter createEDataTypeAdapter()
	{
		if (customEDataTypeItemProvider == null)
		{
			customEDataTypeItemProvider = new CustomEDataTypeItemProvider(this);
		}

		return customEDataTypeItemProvider;
	}
}
