package de.hpi.sam.properties.storyDiagramEcore.callActions;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractEnumerationPropertySection;

public class OperationActionOperatorSection extends AbstractEnumerationPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage.eINSTANCE.getOperationAction_Operator();
	}

	@Override
	protected String getLabelText()
	{
		return "Operator";
	}

	@Override
	protected String[] getEnumerationFeatureValues()
	{
		org.eclipse.emf.ecore.EEnum eenum = de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage.eINSTANCE.getOperators();
		org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EEnumLiteral> eLiteralList = eenum.getELiterals();
		String[] result = new String[eLiteralList.size()];

		for (int i = 0; i < eLiteralList.size(); i++)
		{
			result[i] = eLiteralList.get(i).getLiteral();
		}

		return result;
	}

	@Override
	protected String getFeatureAsText()
	{
		return ((de.hpi.sam.storyDiagramEcore.callActions.Operators) getOldFeatureValue()).getLiteral();
	}

	@Override
	protected Object getFeatureValue(int index)
	{
		return de.hpi.sam.storyDiagramEcore.callActions.Operators.get(index);
	}

	@Override
	protected Object getOldFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.callActions.OperationAction) this.getEObject()).getOperator();
	}

}
