package de.hpi.sam.properties;

import java.util.Arrays;

import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractListPropertySection;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.widgets.ChooseDialog;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

public abstract class AbstractListChooserPropertySection extends AbstractListPropertySection
{

	protected Object	selectedObject;

	protected Shell		shell;

	@Override
	protected void createWidgets(Composite composite)
	{
		super.createWidgets(composite);
		this.shell = composite.getShell();
	}

	@Override
	protected void hookListeners()
	{
		super.hookListeners();
		this.getTable().setRemoveListener(new SelectionAdapter()
		{

			public void widgetSelected(SelectionEvent event)
			{
				removeElement();
			}
		});
	}

	@Override
	protected void addElement()
	{
		ChooseDialog dialog = new ChooseDialog(this.shell, this.getAvailableObjects());
		dialog.setLabelProvider((ILabelProvider) this.getLabelProvider());

		if (dialog.open() == Window.OK)
		{
			EditingDomain editingDomain = this.getEditingDomain();
			editingDomain.getCommandStack().execute(
					AddCommand.create(editingDomain, this.getEObject(), this.getFeature(), Arrays.asList(dialog.getResult())));
		}
	}

	protected void removeElement()
	{
		EditingDomain editingDomain = this.getEditingDomain();
		editingDomain.getCommandStack().execute(
				RemoveCommand.create(editingDomain, this.getEObject(), this.getFeature(), this.selectedObject));
	}

	@Override
	public void updateSelection(Object obj)
	{
		this.selectedObject = obj;
	}

	protected abstract Object[] getAvailableObjects();

}