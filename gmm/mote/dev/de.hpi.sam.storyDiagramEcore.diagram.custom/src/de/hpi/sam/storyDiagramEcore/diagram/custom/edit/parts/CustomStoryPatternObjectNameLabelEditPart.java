package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectNameLabelEditPart;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

public class CustomStoryPatternObjectNameLabelEditPart extends StoryPatternObjectNameLabelEditPart
{

	public CustomStoryPatternObjectNameLabelEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof StoryPatternObject)
		{
			Utility.adaptColor(getFigure(), ((StoryPatternElement) ((View) this.getModel()).getElement()).getModifier());
		}

		super.handleNotificationEvent(event);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		Utility.adaptColor(getFigure(), ((StoryPatternElement) ((View) this.getModel()).getElement()).getModifier());

		((WrappingLabel) getFigure()).setTextUnderline(true);
	}
}
