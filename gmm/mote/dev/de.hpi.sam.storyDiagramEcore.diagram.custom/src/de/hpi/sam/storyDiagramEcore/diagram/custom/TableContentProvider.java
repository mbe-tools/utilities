package de.hpi.sam.storyDiagramEcore.diagram.custom;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;

public class TableContentProvider implements IStructuredContentProvider {

	@Override
	public Object[] getElements(Object inputElement) {
		List<CallActionExpression> list = (List<CallActionExpression>) inputElement;
		return list.toArray();
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

}
