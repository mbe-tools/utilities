package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreUtils;
import de.hpi.sam.storyDiagramEcore.diagram.edit.commands.MapEntryStoryPatternLinkCreateCommand;

public class CustomMapEntryStoryPatternLinkCreateCommand extends MapEntryStoryPatternLinkCreateCommand
{

	public CustomMapEntryStoryPatternLinkCreateCommand(CreateRelationshipRequest request, EObject source, EObject target)
	{
		super(request, source, target);
	}

	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
	{
		if (!canExecute())
		{
			throw new ExecutionException("Invalid arguments in create link command"); //$NON-NLS-1$
		}

		de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink newElement = de.hpi.sam.storyDiagramEcore.sdm.SdmFactory.eINSTANCE
				.createMapEntryStoryPatternLink();
		getContainer().getStoryPatternLinks().add(newElement);
		newElement.setSource(getSource());
		newElement.setTarget(getTarget());

		// inserted code here -------------------------------------------------
		if (this.getSource().getClassifier() != null && this.getTarget().getClassifier() != null
				&& this.getSource().getClassifier() instanceof EClass)
		{
			EReference eReference = StoryDiagramEcoreUtils.getMapTypedEReference((EClass) this.getSource().getClassifier(), this
					.getTarget().getClassifier());

			if (eReference != null)
			{
				newElement.setEStructuralFeature(eReference);
			}
		}
		// --------------------------------------------------------------------

		doConfigure(newElement, monitor, info);
		((CreateElementRequest) getRequest()).setNewElement(newElement);
		return CommandResult.newOKCommandResult(newElement);

	}

}
