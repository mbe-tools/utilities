package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EStructuralFeature;

public class AbstractStoryPatternLinkTargetSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getAbstractStoryPatternLink_Target();
	}

	@Override
	protected String getLabelText()
	{
		return "Target";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink) getEObject()).getTarget();
	}
}
