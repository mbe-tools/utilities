package de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.ui.action.AbstractActionDelegate;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.ui.IObjectActionDelegate;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.diagram.custom.ChooseActivityOutParameterDialog;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;

public class EditActivityFinalNodeReturnValueExpressionAction extends AbstractActionDelegate implements IObjectActionDelegate
{

	ChooseActivityOutParameterDialog	parameterDialog;

	@Override
	protected void doRun(IProgressMonitor progressMonitor)
	{
		parameterDialog = new ChooseActivityOutParameterDialog(null);
		parameterDialog.setChangeCommandReceiver(this.getChangeCommandReceiver());
		parameterDialog.setNode(this.getModel());
		parameterDialog.setActivity(this.getActivity());
		parameterDialog.setEditPart(this.getEditPart());

		parameterDialog.open();
	}

	protected Activity getActivity()
	{
		ActivityFinalNodeEditPart ep = this.getEditPart();
		return (Activity) ((View) ((GraphicalEditPart) ep.getRoot().getChildren().get(0)).getModel()).getElement();
	}

	protected ActivityFinalNodeEditPart getEditPart()
	{
		return (ActivityFinalNodeEditPart) this.getStructuredSelection().getFirstElement();
	}

	protected ActivityFinalNode getModel()
	{
		ActivityFinalNodeEditPart aaEditPart = this.getEditPart();
		return (ActivityFinalNode) ((View) aaEditPart.getModel()).getElement();
	}

	protected TransactionalEditingDomain getChangeCommandReceiver()
	{
		return this.getEditPart().getEditingDomain();
	}
}
