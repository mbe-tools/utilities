package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import java.util.List;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.emf.type.core.commands.SetValueCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.sdm.SdmFactory;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

public class CustomStoryActionNodeStoryActionNodeElementsCompartmentEditPart extends
		StoryActionNodeStoryActionNodeElementsCompartmentEditPart
{

	public CustomStoryActionNodeStoryActionNodeElementsCompartmentEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void createDefaultEditPolicies()
	{
		super.createDefaultEditPolicies();
		this.removeEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE);
		this.installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE, new StoryDiagramDnDEditPolicy());
	}

	class StoryDiagramDnDEditPolicy extends DragDropEditPolicy
	{
		@Override
		public Command getDropObjectsCommand(DropObjectsRequest request)
		{
			List<?> objects = request.getObjects();
			if (objects.size() != 1)
			{
				return super.getDropObjectsCommand(request);
			}

			Object object = objects.get(0);
			if (object == null || !(object instanceof EClassifier))
			{
				return super.getDropObjectsCommand(request);
			}
			EClassifier eClassifier = (EClassifier) object;

			StoryPatternObject spo = SdmFactory.eINSTANCE.createStoryPatternObject();
			CompoundCommand cc = new CompoundCommand();

			// zum SAN hinzu fügen
			EObject eObj = ((View) (getHost().getModel())).getElement();
			SetRequest sr = new SetRequest(eObj, NodesPackage.eINSTANCE.getStoryActionNode_StoryPatternObjects(), spo);
			TransactionalEditingDomain ted = sr.getEditingDomain();
			cc.add(new ICommandProxy(new SetValueCommand(sr)));

			// Classifier setzen
			sr = new SetRequest(ted, spo, SdmPackage.eINSTANCE.getAbstractStoryPatternObject_Classifier(), eClassifier);
			cc.add(new ICommandProxy(new SetValueCommand(sr)));

			// Namen setzen
			String name = eClassifier.getName();
			if (name.length() > 1)
			{
				name = name.substring(0, 1).toLowerCase() + name.substring(1);
			}
			else if (name.length() == 1)
			{
				name = name.toLowerCase();
			}

			sr = new SetRequest(ted, spo, StoryDiagramEcorePackage.eINSTANCE.getNamedElement_Name(), name);
			cc.add(new ICommandProxy(new SetValueCommand(sr)));

			// Node an entspr. Stelle anlegen
			ViewDescriptor vd = new ViewDescriptor(new EObjectAdapter(spo), Node.class,
					((IHintedType) StoryDiagramEcoreElementTypes.StoryPatternObject_3035).getSemanticHint(),
					CustomStoryActionNodeStoryActionNodeElementsCompartmentEditPart.this.getDiagramPreferencesHint());
			CreateViewRequest cvr = new CreateViewRequest(vd);
			cvr.setLocation(request.getLocation());
			cc.add(CustomStoryActionNodeStoryActionNodeElementsCompartmentEditPart.this.getCommand(cvr));

			return cc.unwrap();
		}
	}
}
