package de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.part.CustomAbstractActionDelegate;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

public class RemoveStoryPatternObjectDirectAssignmentExpressionAction extends CustomAbstractActionDelegate
{
	StoryPatternObject	storyPatternObject;

	@Override
	protected void doRun(IProgressMonitor progressMonitor)
	{
		StoryPatternObjectEditPart storyPatternObjectEditPart = ((StoryPatternObjectEditPart) getStructuredSelection().getFirstElement());

		storyPatternObject = (StoryPatternObject) ((View) storyPatternObjectEditPart.getModel()).getElement();

		AbstractTransactionalCommand command = new AbstractTransactionalCommand(storyPatternObjectEditPart.getEditingDomain(),
				"Edit Expression", null)
		{

			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
			{
				storyPatternObject.setDirectAssignmentExpression(null);

				return CommandResult.newOKCommandResult();
			}
		};

		execute(command);
	}
}
