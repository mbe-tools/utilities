package de.hpi.sam.properties.storyDiagramEcore.callActions;

import org.eclipse.emf.ecore.EStructuralFeature;

public class CallActionParameterParameterValueActionSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage.eINSTANCE.getCallActionParameter_ParameterValueAction();
	}

	@Override
	protected String getLabelText()
	{
		return "Parameter Value Action";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter) getEObject()).getParameterValueAction();
	}
}
