package de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

import de.hpi.sam.storyDiagramEcore.diagram.custom.CommonEditExpressionDialog;
import de.hpi.sam.storyDiagramEcore.diagram.custom.part.CustomAbstractActionDelegate;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;

public abstract class CommonEditExpressionAction extends CustomAbstractActionDelegate
{

	protected CommonEditExpressionDialog	editExpressionDialog;

	protected void prepareDialog()
	{
		editExpressionDialog.setExpression(getExpression());
		editExpressionDialog.setExpectedClassifier(getExpectedClassifier());
		editExpressionDialog.setContextClassifier(getContextClassifier());
		editExpressionDialog.setExpressionOwner(getExpressionOwner());
		editExpressionDialog.setEditingDomain(getEditingDomain());
	}

	@Override
	protected void doRun(IProgressMonitor progressMonitor)
	{
		prepareDialog();
		editExpressionDialog.open();
	}

	/**
	 * Returns the expression object that should be edited. Returns null if a
	 * new expression object should be created.
	 * 
	 * @return
	 */
	protected abstract Expression getExpression();

	/**
	 * Returns the expected classifier of the expression. The expression must
	 * evaluate to an instance of that classifier.
	 * 
	 * @return
	 */
	protected abstract EClassifier getExpectedClassifier();

	/**
	 * Returns the context classifier of the expression. The expression is valid
	 * in this context.
	 * 
	 * @return
	 */
	protected abstract EClassifier getContextClassifier();

	/**
	 * Returns the TransactionalEditingDomain.
	 * 
	 * @return
	 */
	protected abstract TransactionalEditingDomain getEditingDomain();

	protected abstract void setNewExpression(Expression newExpression);

	protected abstract EObject getExpressionOwner();
}
