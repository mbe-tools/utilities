package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart;

public class CustomActivityEditPart extends ActivityEditPart
{

	public CustomActivityEditPart(View view)
	{
		super(view);
	}

	@Override
	protected IFigure createFigure()
	{
		IFigure figure = super.createFigure();
		figure.setBackgroundColor(new Color(null, 240, 245, 255));
		figure.setOpaque(true);

		return figure;
	}
}
