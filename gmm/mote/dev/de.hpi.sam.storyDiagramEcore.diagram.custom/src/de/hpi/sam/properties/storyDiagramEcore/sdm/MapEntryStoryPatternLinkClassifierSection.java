package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EStructuralFeature;

public class MapEntryStoryPatternLinkClassifierSection extends EnhancedChooserPropertySection
{

	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getMapEntryStoryPatternLink_Classifier();
	}

	@Override
	protected String getLabelText()
	{
		return "Classifier";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) getEObject()).getClassifier();
	}

}