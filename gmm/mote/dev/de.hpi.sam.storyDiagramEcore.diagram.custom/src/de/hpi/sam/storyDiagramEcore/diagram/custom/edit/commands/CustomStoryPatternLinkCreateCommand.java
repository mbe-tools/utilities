package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreUtils;
import de.hpi.sam.storyDiagramEcore.diagram.edit.commands.StoryPatternLinkCreateCommand;
import de.hpi.sam.storyDiagramEcore.sdm.SdmFactory;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;

public class CustomStoryPatternLinkCreateCommand extends StoryPatternLinkCreateCommand
{

	public CustomStoryPatternLinkCreateCommand(CreateRelationshipRequest request, EObject source, EObject target)
	{
		super(request, source, target);
	}

	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
	{
		if (!canExecute())
		{
			throw new ExecutionException("Invalid arguments in create link command"); //$NON-NLS-1$
		}

		StoryPatternLink newElement = SdmFactory.eINSTANCE.createStoryPatternLink();
		getContainer().getStoryPatternLinks().add(newElement);
		newElement.setSource(getSource());
		newElement.setTarget(getTarget());

		// inserted code here -------------------------------------------------
		if (this.getSource().getClassifier() instanceof EClass)
		{
			EStructuralFeature feature = StoryDiagramEcoreUtils.getConnectingEStructuralFeature((EClass) getSource().getClassifier(),
					getTarget().getClassifier());

			if (feature != null)
			{
				newElement.setEStructuralFeature(feature);

				if (feature instanceof EReference && ((EReference) feature).getEOpposite() != null)
				{
					StoryPatternLink opLink = StoryDiagramEcoreUtils.getStoryPatternLinkInCase(this.getTarget(), this.getSource(),
							((EReference) feature).getEOpposite());

					if (opLink != null)
					{
						newElement.setOppositeStoryPatternLink(opLink);
						opLink.setOppositeStoryPatternLink(newElement);
					}
				}
			}

		}
		// --------------------------------------------------------------------

		this.doConfigure(newElement, monitor, info);
		((CreateElementRequest) this.getRequest()).setNewElement(newElement);
		return CommandResult.newOKCommandResult(newElement);
	}
}
