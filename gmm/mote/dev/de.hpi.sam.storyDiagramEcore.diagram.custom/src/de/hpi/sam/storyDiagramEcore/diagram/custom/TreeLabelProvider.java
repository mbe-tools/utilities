package de.hpi.sam.storyDiagramEcore.diagram.custom;

import org.eclipse.jface.viewers.LabelProvider;

import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;


public class TreeLabelProvider extends LabelProvider {
	@Override
	public String getText(Object element) {
		String s = ((CallActionExpression) element).toString(); 
		return s;
	}

}

