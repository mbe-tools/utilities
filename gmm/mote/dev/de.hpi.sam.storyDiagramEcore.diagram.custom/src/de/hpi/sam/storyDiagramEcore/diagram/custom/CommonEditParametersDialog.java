package de.hpi.sam.storyDiagramEcore.diagram.custom;

import java.util.Collection;
import java.util.Vector;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.widgets.ChooseDialog;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.hpi.sam.storyDiagramEcore.ActivityParameter;
import de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreFactory;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;

/**
 * @author Patrick Rein
 * 
 */
public class CommonEditParametersDialog extends ActivityEditListFeatureDialog
{

	private final String	REMOVAL_CONFIRMATION_TEXT		= "Do you really want to remove this parameter?";
	private Composite[] parameterRows = new Composite[256];
	private ActivityParameter[] parameters= new ActivityParameter[256];
	private int numberOfParameter = 0;
	/*
	 * Configuration of the fields of the form.
	 */
	private final int		CLASSIFIER_INPUT_WIDTH			= 100;
	private final int		NAME_INPUT_WIDTH				= 300;
	private final int		DIRECTION_INPUT_WIDTH			= 90;
	private final int		EDIT_CLASSIFIER_BUTTON_HEIGHT	= 27;
	private final int		EDIT_CLASSIFIER_BUTTON_WIDTH	= 20;

	public CommonEditParametersDialog(final Shell parentShell)
	{
		super(parentShell);
		this.ADD_BUTTON_TEXT = "New Parameter";
		this.DIALOG_TITLE = "Manage Parameters";
		this.ITEM_CONTAINER_TEXT = "Parameters";
	}

	protected void createParameterRow(final ActivityParameter aParameter, final int number)
	{
		String aString = "";

		final Composite aParameterRow = new Composite(this.itemContainer, SWT.NONE);
		aParameterRow.setLayout(new RowLayout());
		parameterRows[number]= aParameterRow;
		parameters[number]= aParameter;

		// Direction Input
		final Combo aParameterDirectionCombo = new Combo(aParameterRow, this.COMBO_INPUT_CONFIG);
		aParameterDirectionCombo.setItems(this.getDirections());

		final ActivityParameterDirectionEnum paramDirection = aParameter.getDirection();
		if (paramDirection != null)
		{
			aParameterDirectionCombo.setText(paramDirection.getName());
		}

		aParameterDirectionCombo.setLayoutData(new RowData(this.DIRECTION_INPUT_WIDTH, this.TEXT_INPUT_HEIGHT));
		aParameterDirectionCombo.addModifyListener(new ComboModifyListenerChangeDirection(aParameter));

		// Classifier Input
		this.createClassifierInput(aParameterRow, aParameter);

		// Name Input
		final Text aParameterNameInput = new Text(aParameterRow, this.TEXT_INPUT_CONFIG);
		aString = aParameter.getName();
		if (aString == null)
		{
			aString = "";
		}
		aParameterNameInput.setText(aString);
		aParameterNameInput.setLayoutData(new RowData(this.NAME_INPUT_WIDTH, this.TEXT_INPUT_HEIGHT));
		aParameterNameInput.addModifyListener(new TextModifyListenerChangeName(aParameter));

		// Buttons
		final Button removeParameterButton = new Button(aParameterRow, SWT.PUSH);
		removeParameterButton.setText(this.DELETE_ITEM_BUTTON_TEXT);
		removeParameterButton.addSelectionListener(new SelectionListenerRemoveRow(aParameter, aParameterRow));
		Button upButton = new Button(aParameterRow, SWT.PUSH);
		upButton.setText("Up");
		upButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				parameterUp(number);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		if (number <= 0){
			upButton.setEnabled(false);
		}
		Button downButton = new Button(aParameterRow, SWT.PUSH);
		downButton.setText("Down");
		downButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				parameterDown(number);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		if (number > numberOfParameter-2){
			downButton.setEnabled(false);
		}
	}

	private void parameterDown(int number){
		int n = numberOfParameter;
			ActivityParameter actParTop = parameters[number];
			ActivityParameter actParBottom = parameters[number+1];
			ActivityParameter actParHelp = actParTop;
			Composite comT = parameterRows[number];
			Composite comB = parameterRows[number+1];
			Composite comHelp = comT;
			parameters[number]=actParBottom;
			parameters[number+1] = actParHelp;
			parameterRows[number]=comB;
			parameterRows[number+1] = comHelp;
			for (int j = number; j< n;j++){
				removeOldParameter(parameters[j], parameterRows[j]);
			}
			for (int j = number; j< n;j++){
				addOldParameter(parameters[j],j);
			}
			this.refreshVisually();
	}
	
	private void parameterUp(int number){
		if (number > 0){
			ActivityParameter actParTop = parameters[number-1];
			ActivityParameter actParBottom = parameters[number];
			ActivityParameter actParHelp = actParTop;
			Composite comT = parameterRows[number-1];
			Composite comB = parameterRows[number];
			Composite comH = comT;
	
			parameters[number -1 ]=actParBottom;
			parameters[number] = actParHelp;
			parameterRows[number -1 ]=comB;
			parameterRows[number] = comH;
			int n = numberOfParameter;
			for (int j = number-1; j< n;j++){
				removeOldParameter(parameters[j], parameterRows[j]);
			}
			for (int j = number-1; j< n;j++){
				addOldParameter(parameters[j],j);
			}
			this.refreshVisually();
		}
	}
	
	protected String[] getDirections()
	{
		final String[] directionStrings = {};
		final Vector<String> directionElements = new Vector<String>();
		for (final ActivityParameterDirectionEnum aDirection : ActivityParameterDirectionEnum.values())
		{
			directionElements.add(aDirection.name());
		}
		return directionElements.toArray(directionStrings);
	}

	protected void createClassifierInput(final Composite aParameterRow, final ActivityParameter aParameter)
	{
		final Text aParameterClassifierText = new Text(aParameterRow, this.TEXT_INPUT_CONFIG | SWT.READ_ONLY);
		aParameterClassifierText.setLayoutData(new RowData(this.CLASSIFIER_INPUT_WIDTH, this.TEXT_INPUT_HEIGHT));

		if (aParameter.getType() != null)
		{
			aParameterClassifierText.setText(aParameter.getType().getName());
		}

		final Button editClassifierButton = new Button(aParameterRow, SWT.PUSH);
		editClassifierButton.setLayoutData(new RowData(this.EDIT_CLASSIFIER_BUTTON_WIDTH, this.EDIT_CLASSIFIER_BUTTON_HEIGHT));
		editClassifierButton.setText("...");
		editClassifierButton.addSelectionListener(new SelectionListenerEditClassifier(aParameter, aParameterClassifierText));
	}

	protected Collection<EObject> getParameterClassifierOptions(final ActivityParameter aParameter)
	{
		final Collection<EObject> collection = ItemPropertyDescriptor.getReachableObjectsOfType(this.activity,
				EcorePackage.eINSTANCE.getEClassifier());
		collection.addAll(EcorePackage.eINSTANCE.getEClassifiers());

		return collection;
	}

	@Override
	protected SelectionListener createAddButtonListener()
	{
		return new SelectionListener()
		{

			@Override
			public void widgetSelected(final SelectionEvent e)
			{
				CommonEditParametersDialog.this.addNewParameter();
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e)
			{
			}
		};
	}

	@Override
	protected void fillItemsContainer()
	{
		int i = 0;
		numberOfParameter=this.activity.getParameters().size();
		for (final ActivityParameter aParameter : this.activity.getParameters())
		{
			this.createParameterRow(aParameter, i);
			i++;	
		}
	}

	protected void addNewParameter()
	{
		final EditingDomain domain = TransactionUtil.getEditingDomain(this.activity);

		final ActivityParameter aParameter = StoryDiagramEcoreFactory.eINSTANCE.createActivityParameter();
		aParameter.setName("");

		domain.getCommandStack().execute(
				new AddCommand(this.initialNodeEditPart.getEditingDomain(), this.activity, this.activity.eClass().getEStructuralFeature(
						"parameters"), aParameter));
		numberOfParameter++;
		this.createParameterRow(aParameter, numberOfParameter);
		this.refreshVisually();
	}
	
	protected void addOldParameter(ActivityParameter aParameter, int number)
	{
		final EditingDomain domain = TransactionUtil.getEditingDomain(this.activity);

		domain.getCommandStack().execute(
				new AddCommand(this.initialNodeEditPart.getEditingDomain(), this.activity, this.activity.eClass().getEStructuralFeature(
						"parameters"), aParameter));

		this.createParameterRow(aParameter, number);
		this.refreshVisually();
	}

	protected class ComboModifyListenerChangeDirection implements ModifyListener
	{

		private final ActivityParameter	theParameter;

		public ComboModifyListenerChangeDirection(final ActivityParameter theParameter)
		{
			this.theParameter = theParameter;
		}

		@Override
		public void modifyText(final ModifyEvent e)
		{
			CommonEditParametersDialog.this.changeParameterDirection(this.theParameter, ((Combo) e.widget).getText());
		}

	}

	protected void changeParameterDirection(final ActivityParameter theParameter, final String directionName)
	{

		final EditingDomain domain = TransactionUtil.getEditingDomain(this.activity);

		domain.getCommandStack().execute(
				new SetCommand(this.initialNodeEditPart.getEditingDomain(), theParameter, theParameter.eClass().getEStructuralFeature(
						"direction"), ActivityParameterDirectionEnum.get(directionName)));
	}

	protected class TextModifyListenerChangeName implements ModifyListener
	{

		private final ActivityParameter	theParameter;

		public TextModifyListenerChangeName(final ActivityParameter theParameter)
		{
			this.theParameter = theParameter;
		}

		@Override
		public void modifyText(final ModifyEvent e)
		{
			CommonEditParametersDialog.this.changeParameterName(this.theParameter, ((Text) e.widget).getText());
		}

	}

	protected void changeParameterName(final ActivityParameter theParameter, final String name)
	{

		final EditingDomain domain = TransactionUtil.getEditingDomain(this.activity);

		domain.getCommandStack().execute(
				new SetCommand(this.initialNodeEditPart.getEditingDomain(), theParameter, theParameter.eClass().getEStructuralFeature(
						"name"), name));
	}

	protected class SelectionListenerEditClassifier implements SelectionListener
	{

		private final ActivityParameter	theParameter;
		private final Text				classifierInput;

		public SelectionListenerEditClassifier(final ActivityParameter aParameter, final Text classifierInput)
		{
			this.theParameter = aParameter;
			this.classifierInput = classifierInput;
		}

		@Override
		public void widgetSelected(final SelectionEvent e)
		{
			final String classifier = CommonEditParametersDialog.this.changeParameterClassifier(this.theParameter);
			if (classifier != null)
			{
				this.classifierInput.setText(classifier);
			}
		}

		@Override
		public void widgetDefaultSelected(final SelectionEvent e)
		{
		}

	}

	protected class SelectionListenerRemoveRow implements SelectionListener
	{

		private final ActivityParameter	theParameter;
		private final Composite			theParameterRow;

		public SelectionListenerRemoveRow(final ActivityParameter aParameter, final Composite aParameterRow)
		{
			this.theParameter = aParameter;
			this.theParameterRow = aParameterRow;
		}

		@Override
		public void widgetSelected(final SelectionEvent e)
		{
			CommonEditParametersDialog.this.removeAParameter(this.theParameter, this.theParameterRow);
		}

		@Override
		public void widgetDefaultSelected(final SelectionEvent e)
		{
		}
	}

	protected void removeAParameter(final ActivityParameter aParameter, final Composite aParameterRow)
	{
		final EditingDomain domain = TransactionUtil.getEditingDomain(this.activity);
		final MessageBox checkMessageBox = new MessageBox(this.getParentShell(), SWT.YES | SWT.NO);
		checkMessageBox.setMessage(this.REMOVAL_CONFIRMATION_TEXT);
		if (checkMessageBox.open() == SWT.YES)
		{
			domain.getCommandStack().execute(
					new RemoveCommand(this.initialNodeEditPart.getEditingDomain(), this.activity, this.activity.eClass()
							.getEStructuralFeature("parameters"), aParameter));
			aParameterRow.dispose();
			this.refreshVisually();
			numberOfParameter--;
		}
	}
	protected void removeOldParameter(final ActivityParameter aParameter, final Composite aParameterRow)
	{
		final EditingDomain domain = TransactionUtil.getEditingDomain(this.activity);
		domain.getCommandStack().execute(
				new RemoveCommand(this.initialNodeEditPart.getEditingDomain(), this.activity, this.activity.eClass()
						.getEStructuralFeature("parameters"), aParameter));
		aParameterRow.dispose();
		this.refreshVisually();
	}
	
	protected String changeParameterClassifier(final ActivityParameter aParameter)
	{

		Object classifierSelection;
		Object[] classifierOptions;

		classifierOptions = this.getParameterClassifierOptions(aParameter).toArray();

		final ChooseDialog cDialog = new ChooseDialog(this.getParentShell(), classifierOptions);
		cDialog.setBlockOnOpen(true);
		cDialog.setLabelProvider(new LabelProvider()
		{
			@Override
			public String getText(final Object objectToLabel)
			{
				return ((EClassifier) objectToLabel).getName();
			}
		});

		if (cDialog.open() == Window.OK)
		{
			classifierSelection = cDialog.getResult()[0];

			final EditingDomain domain = TransactionUtil.getEditingDomain(this.activity);

			domain.getCommandStack().execute(
					new SetCommand(this.initialNodeEditPart.getEditingDomain(), aParameter,
							StoryDiagramEcorePackage.Literals.ACTIVITY_PARAMETER__TYPE, classifierSelection));

			return ((EClassifier) classifierSelection).getName();
		}

		return "";

	}
}