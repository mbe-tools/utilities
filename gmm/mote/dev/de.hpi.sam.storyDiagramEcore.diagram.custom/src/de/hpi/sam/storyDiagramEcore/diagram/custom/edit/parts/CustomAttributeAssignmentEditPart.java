package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.diagram.custom.StoryDiagramEcoreDiagramConstants;
import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart;
import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;

public class CustomAttributeAssignmentEditPart extends AttributeAssignmentEditPart
{

	public CustomAttributeAssignmentEditPart(View view)
	{
		super(view);
	}

	/*
	 * should be similar to addNotationalListeners of:
	 * 
	 * CustomActivityEdgeGuardConstraintLabelEditPart
	 * CustomActivityFinalNodeReturnValueLabelEditPart
	 * CustomAttributeAssignmentEditPart CustomCallActionExpressionEditPart
	 * CustomCallActionExpression2EditPart CustomCallActionExpression3EditPart
	 * CustomStoryPatternExpressionLinkFeatureNameLabelEditPart
	 * CustomStoryPatternObjectClassifierLabelEditPart
	 */
	@Override
	protected void addNotationalListeners()
	{
		super.addNotationalListeners();
		Expression expr = ((AttributeAssignment) ((View) getModel()).getElement()).getAssignmentExpression();

		if (expr != null)
		{
			this.addListenerFilter(Utility.getFilterId((NamedElement) expr), this, expr);

			if (expr instanceof CallActionExpression)
			{
				TreeIterator<EObject> it = ((CallActionExpression) expr).eAllContents();
				for (EObject eObj; it.hasNext();)
				{
					eObj = it.next();
					this.addListenerFilter(Utility.getFilterId((NamedElement) eObj), this, eObj);
				}
			}
		}
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	protected void updateFigure()
	{
		AttributeAssignment aa = ((AttributeAssignment) ((View) getModel()).getElement());

		String text = "";

		if (aa.getEStructuralFeature() != null)
		{
			text = aa.getEStructuralFeature().getName() + " := ";
		}
		else
		{
			text = "[null] := ";
		}

		if (aa.getAssignmentExpression() != null)
		{
			text += aa.getAssignmentExpression().toString();
		}
		else
		{
			text += "[null]";
		}

		// if (text.length() > 50)
		// {
		// text = text.substring(0, 50);
		// text += "...";
		// }

		setLabelText(text);

		// ((WrappingLabel) getFigure()).setTextWrap(true);

		getFigure().setForegroundColor(StoryDiagramEcoreDiagramConstants.MODIFIER_CREATE_COLOR);
	}

	/*
	 * should be similar to handleNotificationEvent of:
	 * 
	 * CustomActivityEdgeGuardConstraintLabelEditPart
	 * CustomActivityFinalNodeReturnValueLabelEditPart
	 * CustomAttributeAssignmentEditPart CustomCallActionExpressionEditPart
	 * CustomCallActionExpression2EditPart CustomCallActionExpression3EditPart
	 * CustomStoryPatternExpressionLinkFeatureNameLabelEditPart
	 * CustomStoryPatternObjectClassifierLabelEditPart
	 */
	@Override
	protected void handleNotificationEvent(Notification event)
	{
		Object notifier = event.getNotifier();
		Object newValue = event.getNewValue();
		Object oldValue = event.getOldValue();
		int eventType = event.getEventType();

		if ((eventType == Notification.SET || eventType == Notification.ADD)
				&& (newValue instanceof CallAction || newValue instanceof Expression || newValue instanceof CallActionParameter))
		{
			this.addListenerFilter(Utility.getFilterId((NamedElement) newValue), this, (EObject) newValue);
			updateFigure();
		}
		else if (eventType == Notification.REMOVE
				&& (oldValue instanceof CallAction || oldValue instanceof Expression || oldValue instanceof CallActionParameter))
		{
			this.removeListenerFilter(Utility.getFilterId((NamedElement) oldValue));
			updateFigure();
		}
		else if (notifier instanceof AttributeAssignment || notifier instanceof CallAction || notifier instanceof Expression
				|| notifier instanceof CallActionParameter)
		{
			updateFigure();
		}

		super.handleNotificationEvent(event);
	}
}
