package de.hpi.sam.storyDiagramEcore.diagram.custom.providers;

import java.util.Hashtable;

import org.eclipse.gmf.runtime.common.ui.services.action.global.AbstractGlobalActionHandlerProvider;
import org.eclipse.gmf.runtime.common.ui.services.action.global.IGlobalActionHandler;
import org.eclipse.gmf.runtime.common.ui.services.action.global.IGlobalActionHandlerContext;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;

public class GlobalActionHandlerProvider extends AbstractGlobalActionHandlerProvider
{

	/**
	 * List for handlers.
	 */
	private Hashtable<IWorkbenchPart, IGlobalActionHandler>	handlerList	= new Hashtable<IWorkbenchPart, IGlobalActionHandler>();

	/**
	 * Returns a global action handler that supports global operations (cut,
	 * copy, and paste).
	 */
	public IGlobalActionHandler getGlobalActionHandler(final IGlobalActionHandlerContext context)
	{
		/* Create the handler */
		if (!getHandlerList().containsKey(context.getActivePart()))
		{
			getHandlerList().put(context.getActivePart(), new GlobalActionHandler());

			/*
			 * Register as a part listener so that the cache can be cleared when
			 * the part is disposed
			 */
			context.getActivePart().getSite().getPage().addPartListener(new IPartListener()
			{

				private IWorkbenchPart	localPart	= context.getActivePart();

				/**
				 * @see org.eclipse.ui.IPartListener#partActivated(IWorkbenchPart)
				 */
				public void partActivated(IWorkbenchPart part)
				{
					// Do nothing
				}

				/**
				 * @see org.eclipse.ui.IPartListener#partBroughtToTop(IWorkbenchPart)
				 */
				public void partBroughtToTop(IWorkbenchPart part)
				{
					// Do nothing
				}

				/**
				 * @see org.eclipse.ui.IPartListener#partClosed(IWorkbenchPart)
				 */
				public void partClosed(IWorkbenchPart part)
				{
					/* Remove the cache associated with the part */
					if (part != null && part == localPart && getHandlerList().containsKey(part))
					{
						getHandlerList().remove(part);
						localPart.getSite().getPage().removePartListener(this);
						localPart = null;
					}
				}

				/**
				 * @see org.eclipse.ui.IPartListener#partDeactivated(IWorkbenchPart)
				 */
				public void partDeactivated(IWorkbenchPart part)
				{
					// Do nothing
				}

				/**
				 * @see org.eclipse.ui.IPartListener#partOpened(IWorkbenchPart)
				 */
				public void partOpened(IWorkbenchPart part)
				{
					// Do nothing
				}
			});
		}

		return (GlobalActionHandler) getHandlerList().get(context.getActivePart());
	}

	/**
	 * Returns the handlerList.
	 * 
	 * @return Hashtable
	 */
	private Hashtable<IWorkbenchPart, IGlobalActionHandler> getHandlerList()
	{
		return handlerList;
	}

}