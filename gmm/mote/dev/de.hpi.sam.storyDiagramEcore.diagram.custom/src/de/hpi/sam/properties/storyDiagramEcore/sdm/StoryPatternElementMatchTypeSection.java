package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractEnumerationPropertySection;

public class StoryPatternElementMatchTypeSection extends AbstractEnumerationPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getStoryPatternElement_MatchType();
	}

	@Override
	protected String getLabelText()
	{
		return "Match Type";
	}

	@Override
	protected String[] getEnumerationFeatureValues()
	{
		org.eclipse.emf.ecore.EEnum eenum = de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getStoryPatternMatchTypeEnumeration();
		org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EEnumLiteral> eLiteralList = eenum.getELiterals();
		String[] result = new String[eLiteralList.size()];

		for (int i = 0; i < eLiteralList.size(); i++)
		{
			result[i] = eLiteralList.get(i).getLiteral();
		}

		return result;
	}

	@Override
	protected String getFeatureAsText()
	{
		return ((de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration) getOldFeatureValue()).getLiteral();
	}

	@Override
	protected Object getFeatureValue(int index)
	{
		return de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration.get(index);
	}

	@Override
	protected Object getOldFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement) this.getEObject()).getMatchType();
	}

}
