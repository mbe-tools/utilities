package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;

public class CustomStoryPatternContainmentLinkEditPart extends StoryPatternContainmentLinkEditPart
{

	public CustomStoryPatternContainmentLinkEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification notification)
	{
		if (notification.getNotifier() instanceof StoryPatternContainmentLink)
		{
			// Update colors if necessary ------------------------------------
			Utility.adaptColor(getPrimaryShape(), ((StoryPatternElement) ((View) this.getModel()).getElement()).getModifier());
		}

		super.handleNotificationEvent(notification);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		Utility.adaptColor(getPrimaryShape(), ((StoryPatternElement) ((View) this.getModel()).getElement()).getModifier());
	}
}
