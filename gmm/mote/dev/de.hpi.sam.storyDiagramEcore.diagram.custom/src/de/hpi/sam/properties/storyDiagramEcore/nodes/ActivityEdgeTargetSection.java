package de.hpi.sam.properties.storyDiagramEcore.nodes;

import org.eclipse.emf.ecore.EStructuralFeature;

public class ActivityEdgeTargetSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE.getActivityEdge_Target();
	}

	@Override
	protected String getLabelText()
	{
		return "Target";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) getEObject()).getTarget();
	}
}
