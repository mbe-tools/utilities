package de.hpi.sam.properties.storyDiagramEcore.nodes;

import de.hpi.sam.properties.AbstractListChooserPropertySection;
import de.hpi.sam.properties.SectionHelper;
import de.hpi.sam.storyDiagramEcore.nodes.provider.NodesItemProviderAdapterFactory;

public abstract class EnhancedListChooserPropertySection extends AbstractListChooserPropertySection
{
	@Override
	final protected Object[] getAvailableObjects()
	{
		return SectionHelper.getAvailableObjects(this.getEObject(), this.getFeature(), NodesItemProviderAdapterFactory.class);
	}
}
