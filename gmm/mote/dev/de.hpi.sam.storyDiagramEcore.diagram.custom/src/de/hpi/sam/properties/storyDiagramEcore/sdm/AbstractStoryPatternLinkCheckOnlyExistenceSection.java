package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractBooleanPropertySection;

public class AbstractStoryPatternLinkCheckOnlyExistenceSection extends AbstractBooleanPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getAbstractStoryPatternLink_CheckOnlyExistence();
	}

	@Override
	protected String getLabelText()
	{
		return "Check Only Existence";
	}

}
