package de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink;

public class EditStoryPatternExpressionLinkExpression extends EditExpressionAction
{

	@Override
	protected EClassifier getExpectedClassifier()
	{
		return ((StoryPatternExpressionLink) this.getExpressionOwner()).getTarget().getClassifier();
	}

	@Override
	protected Expression getExpression()
	{
		return ((StoryPatternExpressionLink) this.getExpressionOwner()).getExpression();
	}

	@Override
	protected TransactionalEditingDomain getEditingDomain()
	{
		return ((StoryPatternExpressionLinkEditPart) getStructuredSelection().getFirstElement()).getEditingDomain();
	}

	@Override
	protected void setNewExpression(Expression newExpression)
	{
		StoryPatternExpressionLink spl = (StoryPatternExpressionLink) this.getExpressionOwner();

		spl.setExpression(newExpression);
	}

	@Override
	protected EClassifier getContextClassifier()
	{
		return ((StoryPatternExpressionLink) this.getExpressionOwner()).getSource().getClassifier();
	}

	@Override
	protected Activity getActivity()
	{
		EObject container = this.getExpressionOwner();

		while (!(container instanceof Activity))
		{
			container = container.eContainer();
		}

		return (Activity) container;
	}

	@Override
	protected EObject getExpressionOwner()
	{
		return ((View) ((StoryPatternExpressionLinkEditPart) getStructuredSelection().getFirstElement()).getModel()).getElement();
	}

	@Override
	protected void prepareDialog()
	{
		super.prepareDialog();
		editExpressionDialog.setStructuralFeature(SdmPackage.eINSTANCE.getStoryPatternExpressionLink_Expression());
	}
}
