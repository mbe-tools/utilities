package de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.part.CustomAbstractActionDelegate;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeReturnValueLabelEditPart;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;

public class RemoveActivityFinalNodeReturnValueLabelExpressionAction extends CustomAbstractActionDelegate
{
	ActivityFinalNode	finalNode;

	@Override
	protected void doRun(IProgressMonitor progressMonitor)
	{
		ActivityFinalNodeReturnValueLabelEditPart finalNodeEditPart = ((ActivityFinalNodeReturnValueLabelEditPart) getStructuredSelection()
				.getFirstElement());

		finalNode = (ActivityFinalNode) ((View) finalNodeEditPart.getModel()).getElement();

		AbstractTransactionalCommand command = new AbstractTransactionalCommand(finalNodeEditPart.getEditingDomain(), "Edit Expression",
				null)
		{

			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
			{
				finalNode.setReturnValue(null);

				return CommandResult.newOKCommandResult();
			}
		};

		execute(command);
	}
}
