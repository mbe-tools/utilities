package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectClassifierLabelEditPart;
import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

public class CustomStoryPatternObjectClassifierLabelEditPart extends StoryPatternObjectClassifierLabelEditPart implements IMLExpressionEditPart {

	public CustomStoryPatternObjectClassifierLabelEditPart(View view)
	{
		super(view);
	}

	/*
	 * should be similar to addNotationalListeners of:
	 * 
	 * CustomActivityEdgeGuardConstraintLabelEditPart
	 * CustomActivityFinalNodeReturnValueLabelEditPart
	 * CustomAttributeAssignmentEditPart CustomCallActionExpressionEditPart
	 * CustomCallActionExpression2EditPart CustomCallActionExpression3EditPart
	 * CustomStoryPatternExpressionLinkFeatureNameLabelEditPart
	 * CustomStoryPatternObjectClassifierLabelEditPart
	 */
	@Override
	protected void addNotationalListeners()
	{
		super.addNotationalListeners();
		Expression expr = ((StoryPatternObject) ((View) getModel()).getElement()).getDirectAssignmentExpression();

		if (expr != null)
		{
			this.addListenerFilter(Utility.getFilterId((NamedElement) expr), this, expr);

			if (expr instanceof CallActionExpression)
			{
				TreeIterator<EObject> it = ((CallActionExpression) expr).eAllContents();
				for (EObject eObj; it.hasNext();)
				{
					eObj = it.next();
					this.addListenerFilter(Utility.getFilterId((NamedElement) eObj), this, eObj);
				}
			}
		}
	}

	/*
	 * should be similar to handleNotificationEvent of:
	 * 
	 * CustomActivityEdgeGuardConstraintLabelEditPart
	 * CustomActivityFinalNodeReturnValueLabelEditPart
	 * CustomAttributeAssignmentEditPart CustomCallActionExpressionEditPart
	 * CustomCallActionExpression2EditPart CustomCallActionExpression3EditPart
	 * CustomStoryPatternExpressionLinkFeatureNameLabelEditPart
	 * CustomStoryPatternObjectClassifierLabelEditPart
	 */
	@Override
	protected void handleNotificationEvent(Notification event)
	{
		Object notifier = event.getNotifier();
		Object newValue = event.getNewValue();
		Object oldValue = event.getOldValue();
		int eventType = event.getEventType();

		if ((eventType == Notification.SET || eventType == Notification.ADD)
				&& (newValue instanceof CallAction || newValue instanceof Expression || newValue instanceof CallActionParameter))
		{
			this.addListenerFilter(Utility.getFilterId((NamedElement) newValue), this, (EObject) newValue);
			updateFigure();
		}
		else if (eventType == Notification.REMOVE
				&& (oldValue instanceof CallAction || oldValue instanceof Expression || oldValue instanceof CallActionParameter))
		{
			this.removeListenerFilter(Utility.getFilterId((NamedElement) oldValue));
			updateFigure();
		}
		else if (notifier instanceof StoryPatternObject || notifier instanceof CallAction || notifier instanceof Expression
				|| notifier instanceof CallActionParameter)
		{
			updateFigure();
		}

		super.handleNotificationEvent(event);
	}

	protected void updateFigure()
	{
		Utility.adaptColor(getFigure(), ((StoryPatternElement) ((View) this.getModel()).getElement()).getModifier());

		StoryPatternObject spo = (StoryPatternObject) ((View) getModel()).getElement();

		String text = "";

		String eClassName = "[null]";
		if (spo.getClassifier() != null)
		{
			eClassName = spo.getClassifier().getName();
		}

		if (spo.getDirectAssignmentExpression() != null)
		{
			text = ":= (" + eClassName + ") " + spo.getDirectAssignmentExpression().toString();

		}
		else
		{
			text = ":" + eClassName;

			if (spo.getBindingType() == BindingTypeEnumeration.BOUND)
			{
				setForegroundColor(new Color(null, 160, 160, 160));
				((WrappingLabel) this.getFigure()).setOpaque(false);
			}
			else if (spo.getBindingType() == BindingTypeEnumeration.CAN_BE_BOUND)
			{
				setBackgroundColor(new Color(null, 200, 200, 200));
				((WrappingLabel) this.getFigure()).setOpaque(true);
			}
			else if (spo.getBindingType() == BindingTypeEnumeration.UNBOUND)
			{
				((WrappingLabel) this.getFigure()).setOpaque(false);
				/*
				 * Leave the color as is, so it is green in case of created
				 * objects, red for destroyed objects, and black otherwise.
				 */
			}
		}

		if (text.length() > 50)
		{
			text = text.substring(0, 50);
			text += "...";
		}

		setLabelText(text);

		((WrappingLabel) getFigure()).setTextUnderline(true);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	/* Temporary: until we have proper edit part, this represents story pattern object assignments
	 * (non-Javadoc)
	 * @see de.mdelab.mlstorypatterns.diagram.custom.edit.parts.IMLExpressionWrappingLabel#getWrappedExpression()
	 */
	@Override
	public Expression getExpression() {
		return ( (StoryPatternObject) ( (View) getModel() ).getElement() ).getDirectAssignmentExpression();
	}
}
