package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractIntegerPropertySection;

public class AbstractStoryPatternLinkMatchingPrioritySection extends AbstractIntegerPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getAbstractStoryPatternLink_MatchingPriority();
	}

	@Override
	protected String getLabelText()
	{
		return "Matching Priority";
	}

	@Override
	protected Integer getFeatureInteger()
	{
		return new Integer(((de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink) getEObject()).getMatchingPriority());
	}

}
