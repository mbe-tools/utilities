package de.hpi.sam.storyDiagramEcore.diagram.custom;

import org.eclipse.emf.common.ui.dialogs.ResourceDialog;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.hpi.sam.storyDiagramEcore.ExpressionImport;
import de.hpi.sam.storyDiagramEcore.Import;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreFactory;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreUtils;

/**
 * @author Patrick Rein
 * 
 */
public class CommonEditImportsDialog extends ActivityEditListFeatureDialog
{

	private final String	FILE_DIALOG_BUTTON_TEXT		= "Change File";
	private final String	REMOVAL_CONFIRMATION_TEXT	= "Do you really want to remove this import?";

	private final int		IMPORT_LANGUAGE_WIDTH		= 90;
	private final int		URI_INPUT_WIDTH				= 300;

	public CommonEditImportsDialog(Shell parentShell)
	{
		super(parentShell);
		ADD_BUTTON_TEXT = "New Import";
		DIALOG_TITLE = "Manage Imports";
		ITEM_CONTAINER_TEXT = "Imports";
	}

	protected SelectionListener createAddButtonListener()
	{
		return new SelectionListener()
		{

			@Override
			public void widgetSelected(SelectionEvent e)
			{
				addNewImport();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e)
			{

			}
		};
	}

	/**
	 * In this method the dispatching of Import Row generation is located. Here
	 * you can decide which create method to call for creating widgets for your
	 * import subclass.
	 */
	protected void fillItemsContainer()
	{
		for (Import anImport : activity.getImports())
		{
			createExpressionImportRow((ExpressionImport) anImport);
		}
	}

	protected void createExpressionImportRow(ExpressionImport anImport)
	{
		String aString = "";

		Composite anImportRow = new Composite(itemContainer, SWT.NONE);
		anImportRow.setLayout(new RowLayout());

		// Language Input
		String[] expressionLanguages = {};
		expressionLanguages = StoryDiagramEcoreUtils.getAvailableExpressionLanguages().toArray(expressionLanguages);

		Combo anImportLanguageInput = new Combo(anImportRow, COMBO_INPUT_CONFIG);
		anImportLanguageInput.setItems(expressionLanguages);
		aString = (anImport).getExpressionLanguage();
		if (aString != null && !aString.equals(""))
		{
			anImportLanguageInput.setText(aString);
		}
		anImportLanguageInput.setLayoutData(new RowData(IMPORT_LANGUAGE_WIDTH, TEXT_INPUT_HEIGHT));
		anImportLanguageInput.addModifyListener(new ComboModifyListenerChangeLanguage(anImport));

		// Filename Input
		Text anImportURIInput = new Text(anImportRow, TEXT_INPUT_CONFIG);
		aString = (anImport).getImportedFileURI();
		if (aString == null)
		{
			aString = "";
		}
		anImportURIInput.setText(aString);
		anImportURIInput.setLayoutData(new RowData(URI_INPUT_WIDTH, TEXT_INPUT_HEIGHT));

		// Buttons
		Button setFileButton = new Button(anImportRow, SWT.PUSH);
		setFileButton.setText(FILE_DIALOG_BUTTON_TEXT);
		setFileButton.addSelectionListener(new SelectionListenerSetFile(anImport, anImportURIInput));

		Button removeImportButton = new Button(anImportRow, SWT.PUSH);
		removeImportButton.setText(DELETE_ITEM_BUTTON_TEXT);
		removeImportButton.addSelectionListener(new SelectionListenerRemoveRow(anImport, anImportRow));

	}

	protected void addNewImport()
	{
		EditingDomain domain = TransactionUtil.getEditingDomain(activity);
		ExpressionImport anImport = StoryDiagramEcoreFactory.eINSTANCE.createExpressionImport();
		anImport.setExpressionLanguage("");
		anImport.setImportedFileURI("");

		domain.getCommandStack().execute(
				new AddCommand(initialNodeEditPart.getEditingDomain(), activity, activity.eClass().getEStructuralFeature("imports"),
						anImport));

		this.createExpressionImportRow(anImport);
		this.refreshVisually();
	}

	protected void removeAnImport(ExpressionImport anImport, Composite anImportRow)
	{
		EditingDomain domain = TransactionUtil.getEditingDomain(activity);
		MessageBox checkMessageBox = new MessageBox(this.getParentShell(), SWT.YES | SWT.NO);
		checkMessageBox.setMessage(REMOVAL_CONFIRMATION_TEXT);
		int check = checkMessageBox.open();
		if (check == SWT.YES)
		{
			domain.getCommandStack().execute(
					new RemoveCommand(initialNodeEditPart.getEditingDomain(), activity, activity.eClass().getEStructuralFeature("imports"),
							anImport));

			anImportRow.dispose();
			this.refreshVisually();
		}
	}

	protected String editImportedFileURI(ExpressionImport anExpressionImport)
	{
		EditingDomain domain = TransactionUtil.getEditingDomain(activity);
		ResourceDialog dialog = new ResourceDialog(this.getParentShell(), "Choose file to import", SWT.OPEN);
		dialog.open();
		String filePath = dialog.getURIText();

		domain.getCommandStack().execute(
				new SetCommand(initialNodeEditPart.getEditingDomain(), anExpressionImport, anExpressionImport.eClass()
						.getEStructuralFeature("importedFileURI"), filePath));

		return filePath;
	}

	protected void editImportLanguage(ExpressionImport anExpressionImport, String expressionLanguage)
	{
		EditingDomain domain = TransactionUtil.getEditingDomain(activity);

		domain.getCommandStack().execute(
				new SetCommand(initialNodeEditPart.getEditingDomain(), anExpressionImport, anExpressionImport.eClass()
						.getEStructuralFeature("expressionLanguage"), expressionLanguage));
	}

	protected class ComboModifyListenerChangeLanguage implements ModifyListener
	{

		ExpressionImport	anExpressionImport;

		public ComboModifyListenerChangeLanguage(ExpressionImport anExpressionImport)
		{
			this.anExpressionImport = anExpressionImport;
		}

		@Override
		public void modifyText(ModifyEvent e)
		{
			editImportLanguage(anExpressionImport, ((Combo) e.widget).getText());
		}

	}

	protected class SelectionListenerSetFile implements SelectionListener
	{

		ExpressionImport	theImport;
		Text				fileInput;

		public SelectionListenerSetFile(ExpressionImport anImport, Text anImportURIInput)
		{
			theImport = anImport;
			fileInput = anImportURIInput;
		}

		@Override
		public void widgetSelected(SelectionEvent e)
		{
			String filePath = editImportedFileURI(theImport);
			if (filePath != null)
			{
				fileInput.setText(filePath);
			}
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e)
		{
		}

	}

	protected class SelectionListenerRemoveRow implements SelectionListener
	{

		private ExpressionImport	theImport;
		private Composite			theImportRow;

		public SelectionListenerRemoveRow(ExpressionImport anImport, Composite anImportRow)
		{
			theImport = anImport;
			theImportRow = anImportRow;
		}

		@Override
		public void widgetSelected(SelectionEvent e)
		{
			removeAnImport(theImport, theImportRow);
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e)
		{
		}
	}
}
