package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.ui.action.AbstractActionDelegate;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.ui.IObjectActionDelegate;

import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart;
import de.hpi.sam.storyDiagramEcore.nodes.ForkNode;

public class CustomFlipForkNodeCommand extends AbstractActionDelegate implements IObjectActionDelegate
{

	ForkNode	aForkNode;

	@Override
	protected void doRun(IProgressMonitor progressMonitor)
	{
		final ForkNodeEditPart stopNodeEditPart = ((ForkNodeEditPart) getStructuredSelection().getFirstElement());

		AbstractTransactionalCommand command = new AbstractTransactionalCommand(stopNodeEditPart.getEditingDomain(),
				"Toggle StopNode FlowStopOnly Attribute", null)
		{

			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
			{

				IFigure forkNodeFigure = stopNodeEditPart.getFigure();
				// Dimension flippedDimension = new Dimension(
				// forkNodeFigure.getBounds().height,
				// forkNodeFigure.getMaximumSize().width);
				// forkNodeFigure.setMaximumSize(flippedDimension);
				// forkNodeFigure.setMinimumSize(flippedDimension);

				Rectangle flippedBounds = new Rectangle(forkNodeFigure.getBounds().x, forkNodeFigure.getBounds().y,
						forkNodeFigure.getBounds().height, forkNodeFigure.getBounds().width);
				forkNodeFigure.setBounds(flippedBounds);

				return CommandResult.newOKCommandResult();
			}
		};

		try
		{
			OperationHistoryFactory.getOperationHistory().execute(command, new NullProgressMonitor(), null);
		}
		catch (ExecutionException ee)
		{
			ee.printStackTrace();
		}
	}

}
