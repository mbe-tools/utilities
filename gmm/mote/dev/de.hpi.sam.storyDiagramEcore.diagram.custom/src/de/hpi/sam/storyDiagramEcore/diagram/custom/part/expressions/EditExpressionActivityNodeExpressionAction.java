package de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.diagram.ui.editparts.CompartmentEditPart;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;

public class EditExpressionActivityNodeExpressionAction extends EditExpressionAction
{

	@Override
	protected EClassifier getExpectedClassifier()
	{
		return EcorePackage.eINSTANCE.getEBoolean();
	}

	@Override
	protected Expression getExpression()
	{
		return (Expression) ((View) ((CompartmentEditPart) getStructuredSelection().getFirstElement()).getModel()).getElement();
	}

	@Override
	protected TransactionalEditingDomain getEditingDomain()
	{
		return ((CompartmentEditPart) getStructuredSelection().getFirstElement()).getEditingDomain();
	}

	@Override
	protected void setNewExpression(Expression newExpression)
	{
		Expression oldExpression = getExpression();
		ExpressionActivityNode ean = (ExpressionActivityNode) oldExpression.eContainer();

		ean.setExpression(newExpression);
	}

	@Override
	protected EClassifier getContextClassifier()
	{
		// For expression activity nodes, no context classifier can be
		// specified.
		return null;
	}

	@Override
	protected Activity getActivity()
	{
		EObject container = ((View) ((CompartmentEditPart) getStructuredSelection().getFirstElement()).getParent().getParent().getModel())
				.getElement();

		while (!(container instanceof Activity))
		{
			container = container.eContainer();
		}

		return (Activity) container;
	}

	@Override
	protected EObject getExpressionOwner()
	{
		return getExpression().eContainer();
	}

	@Override
	protected void prepareDialog()
	{
		super.prepareDialog();
		editExpressionDialog.setStructuralFeature(NodesPackage.eINSTANCE.getExpressionActivityNode_Expression());
	}
}
