package de.hpi.sam.storyDiagramEcore.diagram.custom;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TransferData;

import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;

public class MyDropListener extends ViewerDropAdapter {

	private final Viewer viewer;

	public MyDropListener(Viewer viewer) {
		super(viewer);
		this.viewer = viewer;
	}

	@Override
	public void drop(DropTargetEvent event) {
		int location = this.determineLocation(event);
		CallActionExpression target = (CallActionExpression) determineTarget(event);
		String translatedLocation ="";
		switch (location){
		case 1 :
			translatedLocation = "Dropped before the target ";
			break;
		case 2 :
			translatedLocation = "Dropped after the target ";
			break;
		case 3 :
			translatedLocation = "Dropped on the target ";
			break;
		case 4 :
			translatedLocation = "Dropped into nothing ";
			break;
		}
		System.out.println(translatedLocation);
		System.out.println("The drop was done on the element: " + target.getName() );
		super.drop(event);
	}

	// This method performs the actual drop
	// We simply add the String we receive to the model and trigger a refresh of the 
	// viewer by calling its setInput method.
	@Override
	public boolean performDrop(Object data) {

		ContentProviderTree.INSTANCE.getModel().add((CallActionExpression) data);
		viewer.setInput(ContentProviderTree.INSTANCE.getModel());
		return false;
	}

	@Override
	public boolean validateDrop(Object target, int operation,
			TransferData transferType) {
		return true;
		
	}

	

}
