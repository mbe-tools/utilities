package de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeGuardConstraintLabelEditPart;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;

public class EditActivityEdgeGuardLabelExpressionAction extends EditExpressionAction
{

	@Override
	protected EClassifier getExpectedClassifier()
	{
		return EcorePackage.eINSTANCE.getEBoolean();
	}

	@Override
	protected Expression getExpression()
	{
		return (Expression) ((ActivityEdge) this.getExpressionOwner()).getGuardExpression();
	}

	@Override
	protected TransactionalEditingDomain getEditingDomain()
	{
		return ((ActivityEdgeGuardConstraintLabelEditPart) getStructuredSelection().getFirstElement()).getEditingDomain();
	}

	@Override
	protected void setNewExpression(Expression newExpression)
	{
		ActivityEdge edge = (ActivityEdge) this.getExpressionOwner();

		edge.setGuardExpression(newExpression);
		edge.setGuardType(ActivityEdgeGuardEnumeration.BOOLEAN);
	}

	@Override
	protected EClassifier getContextClassifier()
	{
		// No context classifier can be specified.
		return null;
	}

	@Override
	protected Activity getActivity()
	{
		return (Activity) this.getExpressionOwner().eContainer();
	}

	@Override
	protected EObject getExpressionOwner()
	{
		return ((View) ((ActivityEdgeGuardConstraintLabelEditPart) getStructuredSelection().getFirstElement()).getModel()).getElement();
	}

	@Override
	protected void prepareDialog()
	{
		super.prepareDialog();
		editExpressionDialog.setStructuralFeature(NodesPackage.eINSTANCE.getActivityEdge_GuardExpression());
	}
}
