package de.hpi.sam.properties.storyDiagramEcore;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractStringPropertySection;

public class NamedElementNameSection extends AbstractStringPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE.getNamedElement_Name();
	}

	@Override
	protected String getLabelText()
	{
		return "Name";
	}

}
