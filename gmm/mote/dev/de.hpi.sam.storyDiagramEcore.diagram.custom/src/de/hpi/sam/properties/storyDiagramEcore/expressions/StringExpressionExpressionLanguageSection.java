package de.hpi.sam.properties.storyDiagramEcore.expressions;

import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractEnumerationPropertySection;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreUtils;
import de.hpi.sam.storyDiagramEcore.expressions.StringExpression;

public class StringExpressionExpressionLanguageSection extends AbstractEnumerationPropertySection
{

	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression_ExpressionLanguage();
	}

	@Override
	protected String getLabelText()
	{
		return "Expression Language";
	}

	@Override
	protected String[] getEnumerationFeatureValues()
	{
		List<String> langList = StoryDiagramEcoreUtils.getAvailableExpressionLanguages();
		String[] result = new String[langList.size()];
		return langList.toArray(result);
	}

	@Override
	protected String getFeatureAsText()
	{
		return (String) this.getOldFeatureValue();
	}

	@Override
	protected Object getFeatureValue(int index)
	{
		return StoryDiagramEcoreUtils.getAvailableExpressionLanguages().get(index);
	}

	@Override
	protected Object getOldFeatureValue()
	{
		return ((StringExpression) this.getEObject()).getExpressionLanguage();
	}

}
