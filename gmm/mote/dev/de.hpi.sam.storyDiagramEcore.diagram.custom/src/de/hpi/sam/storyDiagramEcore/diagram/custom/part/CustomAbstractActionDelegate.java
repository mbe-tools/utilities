package de.hpi.sam.storyDiagramEcore.diagram.custom.part;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.gmf.runtime.common.ui.action.AbstractActionDelegate;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.ui.IObjectActionDelegate;

public abstract class CustomAbstractActionDelegate extends AbstractActionDelegate implements IObjectActionDelegate
{

	protected void execute(AbstractTransactionalCommand command)
	{
		try
		{
			OperationHistoryFactory.getOperationHistory().execute(command, new NullProgressMonitor(), null);
		}
		catch (ExecutionException ee)
		{
			ee.printStackTrace();
		}
	}

}
