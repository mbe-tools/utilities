package de.hpi.sam.storyDiagramEcore.diagram.custom;

import java.util.Collections;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.draw2d.ConnectionLocator;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.transaction.Transaction;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.AbstractEMFOperation;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.util.StringStatics;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramGraphicalViewer;
import org.eclipse.gmf.runtime.diagram.ui.util.EditPartUtil;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.SWT;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.diagram.custom.edit.commands.UpdateMergedLinkDeferredCommand;
import de.hpi.sam.storyDiagramEcore.diagram.figures.CrossFigure;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;

public class Utility
{

	public static String GuardEnum2String(ActivityEdgeGuardEnumeration guardEnum)
	{
		return guardEnum.toString().toLowerCase().replaceAll("_", " ");
	}

	public static ActivityEdgeGuardEnumeration String2GuardEnum(String text)
	{
		return ActivityEdgeGuardEnumeration.valueOf(text.replaceAll(" ", "_").toUpperCase());
	}

	public static String adaptStoryPatternElementModifierText(IGraphicalEditPart editPart)
	{
		StoryPatternElement spo = (StoryPatternElement) ((View) editPart.getModel()).getElement();

		switch (spo.getModifier())
		{
			case CREATE:
			{
				return "<<create>>";
			}
			case DESTROY:
			{
				return "<<destroy>>";
			}
			default:
			{
				return "";
			}
		}

	}

	public static void adaptLineStyle(PolylineConnectionEx figure, StoryPatternMatchTypeEnumeration spMatch)
	{
		for (Object o : figure.getChildren())
		{
			if (o instanceof CrossFigure)
			{
				figure.getChildren().remove(o);
				break;
			}
		}

		switch (spMatch)
		{
			case NONE:
			{
				figure.setLineStyle(SWT.LINE_SOLID);
				break;
			}
			case OPTIONAL:
			{
				figure.setLineStyle(SWT.LINE_CUSTOM);
				figure.setLineDash(new int[]
				{
						15, 10
				});
				break;
			}
			case NEGATIVE:
			{
				CrossFigure crossFigure = new CrossFigure();

				Dimension size = new Dimension(20, 20);
				crossFigure.setMinimumSize(size);
				crossFigure.setMaximumSize(size);
				crossFigure.setPreferredSize(size);

				figure.add(crossFigure, new ConnectionLocator(figure, ConnectionLocator.MIDDLE));
			}
			default:
			{
				figure.setLineStyle(SWT.LINE_SOLID);
				break;
			}
		}

	}

	public static void adaptColor(IFigure figure, StoryPatternModifierEnumeration spMod)
	{
		// StoryPatternModifierEnumeration spMod = ((StoryPatternElement)
		// ((View) graphEditPart.getModel()).getElement()).getModifier();

		switch (spMod)
		{
			case CREATE:
			{
				figure.setForegroundColor(StoryDiagramEcoreDiagramConstants.MODIFIER_CREATE_COLOR);
				break;
			}
			case DESTROY:
			{
				figure.setForegroundColor(StoryDiagramEcoreDiagramConstants.MODIFIER_DESTROY_COLOR);
				break;
			}
			case NONE:
			{
				figure.setForegroundColor(StoryDiagramEcoreDiagramConstants.MODIFIER_NONE_COLOR);
				break;
			}
			default:
			{
				figure.setForegroundColor(StoryDiagramEcoreDiagramConstants.MODIFIER_NONE_COLOR);
				break;
			}
		}
	}

	public static void trackMergedLinks(ConnectionNodeEditPart linkEditPart, EObject opLink, boolean trackFully)
	throws ExecutionException {
		EditPart targetPart = linkEditPart.getTarget();
		if (!(targetPart instanceof IGraphicalEditPart))
		{
			return;
		}
		ConnectionNodeEditPart oppositeEditPart = null;
		for (Object part : ((IGraphicalEditPart) targetPart).getSourceConnections())
		{
			if (!(part instanceof ConnectionNodeEditPart))
			{
				continue;
			}
			if (((ConnectionNodeEditPart) part).resolveSemanticElement() == opLink) // ((StoryPatternLink)
																					// semanticElement).getOppositeStoryPatternLink()
			{
				oppositeEditPart = (ConnectionNodeEditPart) part;
				break;
			}
		}
		if (oppositeEditPart == null)
		{
			return;
		}
		// Hide line of the non selected reference
		ConnectionNodeEditPart masterPart;
		ConnectionNodeEditPart slavePart;
		if (oppositeEditPart.getSelected() != EditPart.SELECTED_NONE)
		{
			((ILineIsHideableFigure) linkEditPart.getFigure()).setHideLine(true);
			((ILineIsHideableFigure) oppositeEditPart.getFigure()).setHideLine(false);
			masterPart = oppositeEditPart;
			slavePart = linkEditPart;
		}
		else
		{
			((ILineIsHideableFigure) oppositeEditPart.getFigure()).setHideLine(true);
			((ILineIsHideableFigure) linkEditPart.getFigure()).setHideLine(false);
			masterPart = linkEditPart;
			slavePart = oppositeEditPart;
		}

		if (trackFully)
		{
			TransactionalEditingDomain editingDomain = masterPart.getEditingDomain();
			Command cmd = new ICommandProxy(new UpdateMergedLinkDeferredCommand(editingDomain, masterPart, slavePart));
			executeCommand(cmd, masterPart);
		}
	}
	

    /**
     * Executes the supplied command inside an <code>unchecked action</code>
     * Taken from the CanonicalEditPolicy class
     * 
     * @param cmd
     *            command that can be executed (i.e., cmd.canExecute() == true)
     * @param part
     */
	public static void executeCommand(final Command cmd, IGraphicalEditPart part) 
	throws ExecutionException {
		Map<String, Boolean> options = null;
		boolean isActivating = true;

		// use the viewer to determine if we are still initializing the diagram
		// do not use the DiagramEditPart.isActivating since
		// ConnectionEditPart's parent will not be a diagram edit part
		EditPartViewer viewer = part.getViewer();
		
		if (viewer instanceof DiagramGraphicalViewer) {
			isActivating = ((DiagramGraphicalViewer) viewer).isInitializing();
		}

		if (isActivating || !EditPartUtil.isWriteTransactionInProgress(part, false, false)) {
			options = Collections.singletonMap(Transaction.OPTION_UNPROTECTED, Boolean.TRUE);
		}

		final AbstractEMFOperation operation = new AbstractEMFOperation((part).getEditingDomain(), StringStatics.BLANK, options) {

			@Override
			protected IStatus doExecute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {

				cmd.execute();

				return Status.OK_STATUS;
			}
		};

		operation.execute(new NullProgressMonitor(), null);
	}

	public static String getSpecificationString(EOperation specification) {
		String text = "";
		if (specification != null && specification.getEContainingClass() != null
				&& specification.getEContainingClass().getEPackage() != null)
		{
			/*
			 * Operation name
			 */
			text = specification.getEContainingClass().getName() + "." + specification.getName();

			/*
			 * Parameters
			 */
			text += "(";

			if (!specification.getEParameters().isEmpty())
			{
				for (EParameter parameter : specification.getEParameters())
				{
					text += parameter.getEType().getName() + " " + parameter.getName() + ", ";
				}

				text = text.substring(0, text.length() - 2);
			}
			text += ")";

			/*
			 * Return value
			 */
			if (specification.getEType() != null)
			{
				text += ":" + specification.getEType().getName();
			}
		}
		else
		{
			text = "";
		}

		return text;
	}

	public static String getFilterId(NamedElement nElem)
	{
		return nElem.eClass().getName() + ": " + nElem.getUuid();
	}

	/**
	 * Returns the activity that contains this element.
	 * 
	 * @param storyPatternLink
	 * @return
	 */
	public static Activity getActivity(NamedElement element)
	{
		do
		{
			element = (NamedElement) element.eContainer();
		}
		while (element != null && !(element instanceof Activity));

		return (Activity) element;
	}
}
