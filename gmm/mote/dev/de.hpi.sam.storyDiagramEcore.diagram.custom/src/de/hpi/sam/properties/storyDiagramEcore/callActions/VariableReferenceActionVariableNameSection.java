package de.hpi.sam.properties.storyDiagramEcore.callActions;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractStringPropertySection;

public class VariableReferenceActionVariableNameSection extends AbstractStringPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage.eINSTANCE.getVariableReferenceAction_VariableName();
	}

	@Override
	protected String getLabelText()
	{
		return "Variable Name";
	}

}
