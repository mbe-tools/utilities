package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart;

public class CustomSemaphoreEditPart extends SemaphoreEditPart
{

	public CustomSemaphoreEditPart(View view)
	{
		super(view);
	}

	protected void handleNotificationEvent(Notification event)
	{
		super.handleNotificationEvent(event);
		this.updateFigure();
	}

	private void updateFigure()
	{
		((WrappingLabel) ((IFigure) this.getFigure().getChildren().get(0)).getChildren().get(0)).setAlignment(PositionConstants.CENTER);
	}

	@Override
	protected NodeFigure createNodeFigure()
	{
		NodeFigure rootFigure = super.createNodeFigure();
		IFigure figure = (IFigure) rootFigure.getChildren().get(0);
		figure.setLayoutManager(new StackLayout()
		{
			public void layout(IFigure figure)
			{
				Rectangle r = figure.getClientArea();
				List<?> children = figure.getChildren();
				IFigure child;
				Dimension d;
				for (int i = 0; i < children.size(); i++)
				{
					child = (IFigure) children.get(i);
					d = child.getPreferredSize(r.width, r.height);
					d.width = Math.min(d.width, r.width);
					d.height = Math.min(d.height, r.height);
					Rectangle childRect = new Rectangle(r.x + (r.width - d.width) / 2, r.y + (r.height - d.height) / 2, d.width, d.height);
					child.setBounds(childRect);
				}
			}
		});
		return rootFigure;
	}

	@Override
	public void refresh()
	{
		super.refresh();
		updateFigure();
	}

}
