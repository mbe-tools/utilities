package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;

public interface IMLExpressionEditPart {
	
	Expression getExpression();
}
