package de.hpi.sam.properties.storyDiagramEcore.callActions;

import org.eclipse.emf.ecore.EStructuralFeature;

public class CallStoryDiagramInterpreterActionActivitySection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage.eINSTANCE.getCallStoryDiagramInterpreterAction_Activity();
	}

	@Override
	protected String getLabelText()
	{
		return "Activity";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction) getEObject()).getActivity();
	}
}
