package de.hpi.sam.properties.storyDiagramEcore.nodes;

import org.eclipse.emf.ecore.EStructuralFeature;

public class ActivityEdgeSourceSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE.getActivityEdge_Source();
	}

	@Override
	protected String getLabelText()
	{
		return "Source";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) getEObject()).getSource();
	}
}