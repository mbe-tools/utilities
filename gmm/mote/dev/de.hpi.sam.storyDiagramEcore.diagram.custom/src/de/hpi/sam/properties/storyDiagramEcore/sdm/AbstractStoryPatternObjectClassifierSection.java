package de.hpi.sam.properties.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.providers.TabbedPropertiesLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;

public class AbstractStoryPatternObjectClassifierSection extends EnhancedChooserPropertySection
{

	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getAbstractStoryPatternObject_Classifier();
	}

	@Override
	protected String getLabelText()
	{
		return "Classifier";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject) getEObject()).getClassifier();
	}

	@Override
	protected String getItemLabelText(EObject object)
	{
		// TODO Auto-generated method stub
		return super.getItemLabelText(object);
	}

	@Override
	protected ILabelProvider getLabelProvider()
	{
		return new TabbedPropertiesLabelProvider(new CustomEcoreItemProviderAdapterFactory());
	}
}