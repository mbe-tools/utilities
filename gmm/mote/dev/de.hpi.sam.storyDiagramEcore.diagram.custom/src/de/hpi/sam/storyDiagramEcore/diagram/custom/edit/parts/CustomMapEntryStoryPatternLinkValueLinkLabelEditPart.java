package de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.Utility;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueLinkLabelEditPart;
import de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

public class CustomMapEntryStoryPatternLinkValueLinkLabelEditPart extends MapEntryStoryPatternLinkValueLinkLabelEditPart
{

	public CustomMapEntryStoryPatternLinkValueLinkLabelEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void addNotationalListeners()
	{
		super.addNotationalListeners();

		MapEntryStoryPatternLink mapEntryStoryPatternLink = (MapEntryStoryPatternLink) ((Edge) ((View) getModel()).eContainer())
				.getSource().getElement();

		addListenerFilter("MapEntryStoryPatternLink", this, mapEntryStoryPatternLink);
	}

	@Override
	protected void handleNotificationEvent(Notification notification)
	{
		if (notification.getEventType() != Notification.REMOVE
				&& (notification.getNotifier() instanceof MapEntryStoryPatternLink || notification.getNotifier() instanceof StoryPatternObject))
		{
			adaptColor();
		}

		super.handleNotificationEvent(notification);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		adaptColor();
	}

	protected void adaptColor()
	{
		if (((Edge) ((View) getModel()).eContainer()).getSource() != null)
		{
			StoryPatternModifierEnumeration modifier = ((StoryPatternElement) ((Edge) ((View) getModel()).eContainer()).getSource()
					.getElement()).getModifier();

			Utility.adaptColor(getFigure(), modifier);
		}
	}
}
