package de.hpi.sam.tgg.diagram.custom.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;

import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.TggFactory;
import de.hpi.sam.tgg.diagram.custom.Utility;
import de.hpi.sam.tgg.diagram.edit.commands.ModelLinkCreateCommand;

public class CustomModelLinkCreateCommand extends ModelLinkCreateCommand
{

	public CustomModelLinkCreateCommand(CreateRelationshipRequest request, EObject source, EObject target)
	{
		super(request, source, target);
	}

	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
	{
		if (!canExecute())
		{
			throw new ExecutionException("Invalid arguments in create link command"); //$NON-NLS-1$
		}

		ModelLink newElement = TggFactory.eINSTANCE.createModelLink();
		getContainer().getModelElements().add(newElement);
		newElement.setSource(getSource());
		newElement.setTarget(getTarget());

		// inserted code here -------------------------------------------------
		EClassifier srcClassifier = this.getSource().getClassifier();
		if (srcClassifier instanceof EClass)
		{
			EReference reference = Utility.getConnectingEReference((EClass) srcClassifier, this.getTarget().getClassifier());

			if (reference != null)
			{
				newElement.setEReference(reference);

				if (reference.getEOpposite() != null)
				{
					ModelLink opLink = Utility.getModelLinkInCase(this.getTarget(), this.getSource(), reference.getEOpposite());

					if (opLink != null)
					{
						newElement.setOppositeTGGLink(opLink);
						opLink.setOppositeTGGLink(newElement);
					}
				}
			}
		}
		// --------------------------------------------------------------------

		doConfigure(newElement, monitor, info);
		((CreateElementRequest) getRequest()).setNewElement(newElement);
		return CommandResult.newOKCommandResult(newElement);
	}

}
