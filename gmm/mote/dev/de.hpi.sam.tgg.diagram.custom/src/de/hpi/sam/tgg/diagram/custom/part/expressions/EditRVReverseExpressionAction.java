package de.hpi.sam.tgg.diagram.custom.part.expressions;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions.CommonEditExpressionAction;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.tgg.RuleVariable;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.diagram.custom.TggEditConstraintDialog;

public class EditRVReverseExpressionAction extends CommonEditExpressionAction
{

	@Override
	protected void prepareDialog()
	{
		TggEditConstraintDialog dialog = new TggEditConstraintDialog(null);
		dialog.setStructuralFeature(TggPackage.eINSTANCE.getRuleVariable_ReverseCalculationExpression());
		EObject rule = getExpressionOwner().eContainer();
		dialog.setRule((TGGRule) rule);
		this.editExpressionDialog = dialog;
		super.prepareDialog();
	}

	@Override
	protected EClassifier getContextClassifier()
	{
		return null;
	}

	@Override
	protected TransactionalEditingDomain getEditingDomain()
	{
		return getEditPart().getEditingDomain();
	}

	@Override
	protected EClassifier getExpectedClassifier()
	{
		return ((RuleVariable) getExpressionOwner()).getClassifier();
	}

	@Override
	protected Expression getExpression()
	{
		return (Expression) ((View) getEditPart().getModel()).getElement();
	}

	@Override
	protected EObject getExpressionOwner()
	{
		return getExpression().eContainer();
	}

	protected IGraphicalEditPart getEditPart()
	{
		return (IGraphicalEditPart) getStructuredSelection().getFirstElement();
	}

	@Override
	protected void setNewExpression(Expression newExpression)
	{
		RuleVariable rv = (RuleVariable) getExpressionOwner();
		rv.setReverseCalculationExpression(newExpression);
	}

}