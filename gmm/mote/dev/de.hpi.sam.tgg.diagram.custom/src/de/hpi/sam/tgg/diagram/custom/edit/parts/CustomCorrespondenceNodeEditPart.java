package de.hpi.sam.tgg.diagram.custom.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.diagram.custom.Utility;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeEditPart;

public class CustomCorrespondenceNodeEditPart extends CorrespondenceNodeEditPart
{

	public CustomCorrespondenceNodeEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification notification)
	{
		if (notification.getNotifier() instanceof CorrespondenceNode)
		{
			updateFigure();
		}

		super.handleNotificationEvent(notification);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	protected void updateFigure()
	{
		Utility.adaptColor(this, (IFigure) getFigure().getChildren().get(0));
	}

	public class CustomCorrespondenceNodeFigure extends CorrespondenceNodeFigure
	{

		public CustomCorrespondenceNodeFigure()
		{
			super();
			CustomCorrespondenceNodeEditPart.this.updateFigure();
		}
	}
}
