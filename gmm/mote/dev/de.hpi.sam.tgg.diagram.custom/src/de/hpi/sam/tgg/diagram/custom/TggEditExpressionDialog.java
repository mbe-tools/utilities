package de.hpi.sam.tgg.diagram.custom;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.swt.widgets.Shell;

import de.hpi.sam.storyDiagramEcore.diagram.custom.CommonEditExpressionDialog;
import de.hpi.sam.tgg.ModelDomain;
import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.SourceModelDomain;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TargetModelDomain;

public class TggEditExpressionDialog extends CommonEditExpressionDialog
{

	protected ModelDomain	modelDomain;

	public TggEditExpressionDialog(Shell parent)
	{
		super(parent);
	}

	public void setModelObject(ModelObject modelObject)
	{
		ModelDomain modelDomain = (ModelDomain) modelObject.eContainer();
		TGGRule tggRule = (TGGRule) modelDomain.eContainer();

		setRootContainer(tggRule);

		if (modelDomain instanceof SourceModelDomain)
		{
			this.modelDomain = tggRule.getTargetDomain();
		}
		else if (modelDomain instanceof TargetModelDomain)
		{
			this.modelDomain = tggRule.getSourceDomain();
		}
	}

	@Override
	protected Map<String, EClassifier> getContextInformation()
	{
		Map<String, EClassifier> contextInfos = new HashMap<String, EClassifier>();
		final EList<ModelElement> modelElements = this.modelDomain.getModelElements();

		String name;
		for (ModelElement modelElement : modelElements)
		{
			name = modelElement.getName();

			if (modelElement instanceof ModelLink || name == null || name.equals(""))
			{
				continue;
			}
			else if (modelElement instanceof ModelObject)
			{
				contextInfos.put(name, ((ModelObject) modelElement).getClassifier());
			}
		}

		return contextInfos;
	}
}
