package de.hpi.sam.tgg.diagram.custom;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.command.CopyCommand;
import org.eclipse.emf.edit.command.CreateCopyCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TggPackage;

public class TGGDiagramsDuplicator extends TGGRulesDuplicator {

	@Override
	protected void duplicateTggRule(	final EditingDomain p_editingDomain,
										final TGGRule p_rule ) {
		final ResourceSet resSet = p_rule.eResource().getResourceSet();
		final URI origDiagUri = DiagramsUtil.ruleDiagramFileUri( p_rule );
		
		if ( resSet.getURIConverter().exists( origDiagUri, null ) ) {
			final Resource origDiagRes = resSet.getResource( origDiagUri, true );
			final Diagram origDiag = (Diagram) origDiagRes.getContents().get( 0 );
			
			final Diagram copiedDiag = copy( p_editingDomain, origDiag );
			final CopyCommand copyCommand = copyCommand( p_editingDomain, p_rule );
			final TGGRule copiedRule = (TGGRule) copyCommand.getResult().iterator().next();
			copiedDiag.setElement( copiedRule );
			final Iterator<EObject> allContentIt = copiedDiag.eAllContents();
			
			while ( allContentIt.hasNext() ) {
				final EObject element = allContentIt.next();
				
				if ( element instanceof View ) {
					final View viewElem = (View) element;
					final EObject semanticModelElem = viewElem.getElement();
					
					if ( 	semanticModelElem != null && 
							TggPackage.eINSTANCE.equals( semanticModelElem.eClass().getEPackage() ) &&
							!contains( copiedRule, semanticModelElem ) ) {
						final EObject copiedNamedElem = findElement( copyCommand.getCommandList(), semanticModelElem );
						
						if ( copiedNamedElem == null ) {
							throw new IllegalStateException( "Copy of TGG element " + semanticModelElem + " not found.");
						}
						
						viewElem.setElement( copiedNamedElem );
					}
				}
			}
			
			try {
				copiedRule.setName( p_rule.getName() + "_copy"  );
				final TGGDiagram tggModel = (TGGDiagram) p_rule.eContainer();
				tggModel.getTggRules().add( copiedRule );
				tggModel.eResource().save( null );
				
				final URI copiedDiagUri = DiagramsUtil.ruleDiagramFileUri( copiedRule );
				final Resource copiedDiagRes = resSet.createResource( copiedDiagUri );
				copiedDiagRes.getContents().add( copiedDiag );
				copiedDiagRes.save( null );
				
				resSet.getResources().remove( origDiagRes );
				resSet.getResources().remove( copiedDiagRes );
			}
			catch( final IOException p_ex ) {
				throw new RuntimeException( p_ex );
			}
		}
	}
	
	private boolean contains( 	final EObject p_parent,
								final EObject p_eObject ) {
		for ( final EObject cont : p_parent.eContents() ) {
			if ( p_eObject == cont ) {
				return true;
			}
			
			final boolean contained = contains( cont, p_eObject );
			
			if ( contained ) {
				return true;
			}
		}
		
		return false;
	}
	
	private EObject findElement( 	final List<Command> p_commands,
									final EObject p_element ) {
		for ( final Command command : p_commands ) {
			if ( command instanceof CreateCopyCommand ) {
				final CreateCopyCommand cpyCmd = (CreateCopyCommand) command;

				if ( p_element == cpyCmd.getOwner() ) {
					return (EObject) cpyCmd.getResult().iterator().next();
				}
			}
			else if ( command instanceof CompoundCommand ) {
				final EObject element = findElement( ( (CompoundCommand) command ).getCommandList(), p_element );
				
				if ( element != null ) {
					return element;
				}
			}
		}
		
		return null;
	}
}
