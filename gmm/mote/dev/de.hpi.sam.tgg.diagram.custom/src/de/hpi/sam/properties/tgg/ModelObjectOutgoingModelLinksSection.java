package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.providers.TabbedPropertiesLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;

import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;

public class ModelObjectOutgoingModelLinksSection extends EnhancedListChooserPropertySection
{

	@Override
	protected IBaseLabelProvider getLabelProvider()
	{
		return new TabbedPropertiesLabelProvider(TggDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory());
	}

	@Override
	protected Object getListValues()
	{
		return ((ModelObject) this.getEObject()).getOutgoingModelLinks();
	}

	@Override
	protected EStructuralFeature getFeature()
	{
		return TggPackage.eINSTANCE.getModelObject_OutgoingModelLinks();
	}

	@Override
	protected String getLabelText()
	{
		return "Outgoing Links";
	}

}
