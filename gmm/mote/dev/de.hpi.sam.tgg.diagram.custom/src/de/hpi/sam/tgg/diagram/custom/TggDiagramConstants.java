package de.hpi.sam.tgg.diagram.custom;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.graphics.Color;

public class TggDiagramConstants
{

	public static final Color	MODIFIER_CREATE_COLOR	= new Color(null, 4, 178, 4);

	public static final Color	MODIFIER_NONE_COLOR		= ColorConstants.black;

	public static final int		PADDING					= 10;

	public static final int		DOMAIN_WIDTH			= 200;

}
