package de.hpi.sam.tgg.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.diagram.custom.Utility;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkReferenceEditPart;

public class CustomModelLinkReferenceEditPart extends ModelLinkReferenceEditPart
{

	public CustomModelLinkReferenceEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof ModelLink)
		{
			updateFigure();
		}

		super.handleNotificationEvent(event);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	protected void updateFigure()
	{
		Utility.adaptColor(this, getFigure());

		EReference reference = ((ModelLink) ((View) getModel()).getElement()).getEReference();

		String temp;
		if (reference == null)
		{
			temp = "";
		}
		else
		{
			temp = reference.getName();
		}
		setLabelText(temp);
	}

}
