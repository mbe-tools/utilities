package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;

public class ModelLinkSourceSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.tgg.TggPackage.eINSTANCE.getModelLink_Source();
	}

	@Override
	protected String getLabelText()
	{
		return "Source";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.tgg.ModelLink) getEObject()).getSource();
	}
}
