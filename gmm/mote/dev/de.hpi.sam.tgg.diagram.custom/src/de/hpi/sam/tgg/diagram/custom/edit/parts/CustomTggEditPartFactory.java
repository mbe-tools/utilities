package de.hpi.sam.tgg.diagram.custom.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.diagram.edit.parts.AttributeAssignmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression3EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression4EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression5EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceDomainCorrespondenceDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceLinkModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeClassifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkReferenceEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectClassifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableClassifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.SourceModelDomainModelDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TargetModelDomainModelDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TggEditPartFactory;
import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;

public class CustomTggEditPartFactory extends TggEditPartFactory
{

	@Override
	public EditPart createEditPart(EditPart context, Object model)
	{
		if (model instanceof View)
		{
			View view = (View) model;

			switch (TggVisualIDRegistry.getVisualID(view))
			{
				case AttributeAssignmentEditPart.VISUAL_ID:
					return new CustomAttributeAssignmentEditPart(view);

				case CallActionExpressionEditPart.VISUAL_ID:
					return new CustomCallActionExpressionEditPart(view);

				case CallActionExpression3EditPart.VISUAL_ID:
					return new CustomCallActionExpression3EditPart(view);

				case CallActionExpression4EditPart.VISUAL_ID:
					return new CustomCallActionExpression4EditPart(view);

				case CallActionExpression5EditPart.VISUAL_ID:
					return new CustomCallActionExpression5EditPart(view);

				case CorrespondenceDomainCorrespondenceDomainCompartmentEditPart.VISUAL_ID:
					return new CustomCorrespondenceDomainCorrespondenceDomainCompartmentEditPart(view);

				case CorrespondenceLinkEditPart.VISUAL_ID:
					return new CustomCorrespondenceLinkEditPart(view);

				case CorrespondenceLinkModifierEditPart.VISUAL_ID:
					return new CustomCorrespondenceLinkModifierEditPart(view);

				case CorrespondenceNodeEditPart.VISUAL_ID:
					return new CustomCorrespondenceNodeEditPart(view);

				case CorrespondenceNodeClassifierEditPart.VISUAL_ID:
					return new CustomCorrespondenceNodeClassifierEditPart(view);

					// case CorrespondenceNodeInputEditPart.VISUAL_ID:
					// return new CustomCorrespondenceNodeInputEditPart(view);

				case CorrespondenceNodeModifierEditPart.VISUAL_ID:
					return new CustomCorrespondenceNodeModifierEditPart(view);

				case CorrespondenceNodeNameEditPart.VISUAL_ID:
					return new CustomCorrespondenceNodeNameEditPart(view);

				case ModelLinkEditPart.VISUAL_ID:
					return new CustomModelLinkEditPart(view);

				case ModelLinkModifierEditPart.VISUAL_ID:
					return new CustomModelLinkModifierEditPart(view);

				case ModelLinkReferenceEditPart.VISUAL_ID:
					return new CustomModelLinkReferenceEditPart(view);

				case ModelObjectEditPart.VISUAL_ID:
					return new CustomModelObjectEditPart(view);

				case ModelObjectClassifierEditPart.VISUAL_ID:
					return new CustomModelObjectClassifierEditPart(view);

				case ModelObjectModifierEditPart.VISUAL_ID:
					return new CustomModelObjectModifierEditPart(view);

				case ModelObjectNameEditPart.VISUAL_ID:
					return new CustomModelObjectNameEditPart(view);

				case RuleVariableClassifierEditPart.VISUAL_ID:
					return new CustomRuleVariableClassifierEditPart(view);

				case SourceModelDomainModelDomainCompartmentEditPart.VISUAL_ID:
					return new CustomSourceModelDomainModelDomainCompartmentEditPart(view);

				case TargetModelDomainModelDomainCompartmentEditPart.VISUAL_ID:
					return new CustomTargetModelDomainModelDomainCompartmentEditPart(view);
			}
		}
		return super.createEditPart(context, model);
	}

}
