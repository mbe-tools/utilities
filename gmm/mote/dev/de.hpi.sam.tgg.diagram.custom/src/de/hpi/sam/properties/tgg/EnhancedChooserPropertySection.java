package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractChooserPropertySection;
import org.eclipse.emf.edit.provider.IItemLabelProvider;

import de.hpi.sam.properties.SectionHelper;
import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;
import de.hpi.sam.tgg.provider.TggItemProviderAdapterFactory;

public abstract class EnhancedChooserPropertySection extends AbstractChooserPropertySection
{
	@Override
	final protected Object[] getComboFeatureValues()
	{
		return SectionHelper.getAvailableObjects(this.getEObject(), this.getFeature(), TggItemProviderAdapterFactory.class);
	}

	@Override
	protected void addListener()
	{
		super.addListener();

		if (this.getEObject() == null)
		{
			return;
		}

		TggDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory().adapt(this.getEObject(), IItemLabelProvider.class);
	}
}
