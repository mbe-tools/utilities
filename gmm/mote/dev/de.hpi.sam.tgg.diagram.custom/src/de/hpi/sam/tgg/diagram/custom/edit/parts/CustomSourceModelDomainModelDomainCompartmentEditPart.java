package de.hpi.sam.tgg.diagram.custom.edit.parts;

import java.util.List;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.emf.type.core.commands.SetValueCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TggFactory;
import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.diagram.custom.Utility;
import de.hpi.sam.tgg.diagram.edit.parts.SourceModelDomainModelDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

public class CustomSourceModelDomainModelDomainCompartmentEditPart extends SourceModelDomainModelDomainCompartmentEditPart
{

	public CustomSourceModelDomainModelDomainCompartmentEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void createDefaultEditPolicies()
	{
		super.createDefaultEditPolicies();
		this.removeEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE);
		this.installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE, new SourceModelDnDEditPolicy());
	}

	class SourceModelDnDEditPolicy extends DragDropEditPolicy
	{
		@Override
		public Command getDropObjectsCommand(DropObjectsRequest request)
		{
			List<?> objects = request.getObjects();
			if (objects.size() != 1)
			{
				return super.getDropObjectsCommand(request);
			}

			Object object = objects.get(0);
			if (object == null || !(object instanceof EClassifier))
			{
				return super.getDropObjectsCommand(request);
			}
			EClassifier eClassifier = (EClassifier) object;

			ModelObject mo = TggFactory.eINSTANCE.createModelObject();
			CompoundCommand cc = new CompoundCommand();

			// zur Domain hinzu fügen
			EObject eObj = ((View) (getHost().getModel())).getElement();
			SetRequest sr = new SetRequest(eObj, TggPackage.eINSTANCE.getModelDomain_ModelElements(), mo);
			TransactionalEditingDomain ted = sr.getEditingDomain();
			cc.add(new ICommandProxy(new SetValueCommand(sr)));

			// Classifier setzen
			sr = new SetRequest(ted, mo, TggPackage.eINSTANCE.getModelObject_Classifier(), eClassifier);
			cc.add(new ICommandProxy(new SetValueCommand(sr)));

			// Namen setzen
			String name = Utility.firstToLowerCase(eClassifier.getName());
			sr = new SetRequest(ted, mo, StoryDiagramEcorePackage.eINSTANCE.getNamedElement_Name(), name);
			cc.add(new ICommandProxy(new SetValueCommand(sr)));

			// Node an entspr. Stelle anlegen
			ViewDescriptor vd = new ViewDescriptor(new EObjectAdapter(mo), Node.class,
					((IHintedType) TggElementTypes.ModelObject_3002).getSemanticHint(),
					CustomSourceModelDomainModelDomainCompartmentEditPart.this.getDiagramPreferencesHint());
			CreateViewRequest cvr = new CreateViewRequest(vd);
			cvr.setLocation(request.getLocation());
			cc.add(CustomSourceModelDomainModelDomainCompartmentEditPart.this.getCommand(cvr));

			return cc.unwrap();
		}
	}

}
