package de.hpi.sam.properties.tgg;

import de.hpi.sam.properties.AbstractListChooserPropertySection;
import de.hpi.sam.properties.SectionHelper;
import de.hpi.sam.tgg.provider.TggItemProviderAdapterFactory;

public abstract class EnhancedListChooserPropertySection extends AbstractListChooserPropertySection
{
	@Override
	final protected Object[] getAvailableObjects()
	{
		return SectionHelper.getAvailableObjects(this.getEObject(), this.getFeature(), TggItemProviderAdapterFactory.class);
	}
}
