package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;

public class ModelLinkEReferenceSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.tgg.TggPackage.eINSTANCE.getModelLink_EReference();
	}

	@Override
	protected String getLabelText()
	{
		return "EReference";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.tgg.ModelLink) getEObject()).getEReference();
	}
}
