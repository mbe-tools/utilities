package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;

public class CorrespondenceLinkSourceSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.tgg.TggPackage.eINSTANCE.getCorrespondenceLink_Source();
	}

	@Override
	protected String getLabelText()
	{
		return "Source";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.tgg.CorrespondenceLink) getEObject()).getSource();
	}
}
