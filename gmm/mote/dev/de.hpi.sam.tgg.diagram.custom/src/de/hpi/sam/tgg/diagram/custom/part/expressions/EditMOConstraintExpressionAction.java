package de.hpi.sam.tgg.diagram.custom.part.expressions;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions.CommonEditExpressionAction;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.diagram.custom.TggEditConstraintDialog;

public class EditMOConstraintExpressionAction extends CommonEditExpressionAction
{

	@Override
	protected void prepareDialog()
	{
		TggEditConstraintDialog dialog = new TggEditConstraintDialog(null);
		dialog.setStructuralFeature(TggPackage.eINSTANCE.getModelObject_ConstraintExpressions());
		EObject container = getExpressionOwner();
		dialog.setRule((TGGRule) container.eContainer().eContainer());
		this.editExpressionDialog = dialog;
		super.prepareDialog();
	}

	@Override
	protected EClassifier getContextClassifier()
	{
		EObject container = getExpressionOwner();
		return ((ModelObject) container).getClassifier();
	}

	@Override
	protected TransactionalEditingDomain getEditingDomain()
	{
		return getEditPart().getEditingDomain();
	}

	@Override
	protected EClassifier getExpectedClassifier()
	{
		return EcorePackage.eINSTANCE.getEBoolean();
	}

	@Override
	protected Expression getExpression()
	{
		return (Expression) ((View) getEditPart().getModel()).getElement();
	}

	@Override
	protected void setNewExpression(Expression newExpression)
	{
		Expression oldExpression = getExpression();
		EObject container = oldExpression.eContainer();
		EList<Expression> exprList = ((ModelObject) container).getConstraintExpressions();

		final int index = exprList.indexOf(oldExpression);
		exprList.remove(index);
		exprList.add(index, newExpression);
	}

	protected IGraphicalEditPart getEditPart()
	{
		return (IGraphicalEditPart) getStructuredSelection().getFirstElement();
	}

	@Override
	protected EObject getExpressionOwner()
	{
		return getExpression().eContainer();
	}

}