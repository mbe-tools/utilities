package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;

public class ModelObjectClassifierSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.tgg.TggPackage.eINSTANCE.getModelObject_Classifier();
	}

	@Override
	protected String getLabelText()
	{
		return "Classifier";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.tgg.ModelObject) getEObject()).getClassifier();
	}
}
