package de.hpi.sam.tgg.diagram.custom;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.handlers.HandlerUtil;

import de.hpi.sam.tgg.TGGRule;
import fr.labsticc.framework.emf.core.util.EMFUtil;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class DuplicateTggDiagramsHandler extends AbstractHandler {
	
	private final TGGDiagramsDuplicator rulesDuplicator;

	/**
	 * The constructor.
	 */
	public DuplicateTggDiagramsHandler() {
		rulesDuplicator = new TGGDiagramsDuplicator();
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	@Override
	public Object execute( final ExecutionEvent p_event )
	throws ExecutionException {
		final ISelection currentSelection = HandlerUtil.getCurrentSelection( p_event );
		final Collection<TGGRule> selectedRules = EMFUtil.selectedObjects( currentSelection, TGGRule.class );
		
		if ( !selectedRules.isEmpty() ) {
			final IEditorPart editorPart = HandlerUtil.getActiveEditor( p_event );
			final EditingDomain editingDomain;
			
			if ( editorPart instanceof IEditingDomainProvider ) {
				editingDomain = ( (IEditingDomainProvider) editorPart ).getEditingDomain();
			}
			else {
				editingDomain = null;
			}
			
			final WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {
				
				@Override
				public void execute( final IProgressMonitor monitor ) 
				throws InvocationTargetException {
					monitor.beginTask( "Duplicating diagrams...", selectedRules.size() );
					try {
						rulesDuplicator.duplicateTggRules( editingDomain, selectedRules );
					}
					catch( final Throwable p_th ) {
						throw new InvocationTargetException( p_th );
					}
				}
			};
			
			final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked( p_event );
			
			try {
				new ProgressMonitorDialog( window.getShell() ).run( true, true, operation );
			}
			catch( final InterruptedException p_ex ) {
			}
			catch( final InvocationTargetException p_ex ) {
				p_ex.printStackTrace();
			}
		}
		
		return null;
	}
}
