package de.hpi.sam.tgg.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.tgg.diagram.custom.Utility;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression3EditPart;

public class CustomCallActionExpression3EditPart extends CallActionExpression3EditPart
{

	public CustomCallActionExpression3EditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof CallActionExpression)
		{
			updateFigure();
		}

		super.handleNotificationEvent(event);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	protected void updateFigure()
	{
		Utility.adaptConstraintText(this);
	}
}
