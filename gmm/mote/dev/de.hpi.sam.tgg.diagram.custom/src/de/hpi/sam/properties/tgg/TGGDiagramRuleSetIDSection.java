package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractStringPropertySection;

public class TGGDiagramRuleSetIDSection extends AbstractStringPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.tgg.TggPackage.eINSTANCE.getTGGDiagram_RuleSetID();
	}

	@Override
	protected String getLabelText()
	{
		return "Rule Set ID";
	}

}
