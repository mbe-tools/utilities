package de.hpi.sam.tgg.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.diagram.custom.Utility;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectModifierEditPart;

public class CustomModelObjectModifierEditPart extends ModelObjectModifierEditPart
{

	public CustomModelObjectModifierEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof ModelObject)
		{
			updateFigure();
		}

		super.handleNotificationEvent(event);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	protected void updateFigure()
	{
		Utility.adaptColor(this, getFigure());
		Utility.adaptModifierText(this);
	}
}
