package de.hpi.sam.tgg.diagram.custom.edit.parts;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.emf.type.core.commands.SetValueCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.TggFactory;
import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.diagram.custom.Utility;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceDomainCorrespondenceDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

public class CustomCorrespondenceDomainCorrespondenceDomainCompartmentEditPart extends
		CorrespondenceDomainCorrespondenceDomainCompartmentEditPart
{

	public CustomCorrespondenceDomainCorrespondenceDomainCompartmentEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void createDefaultEditPolicies()
	{
		super.createDefaultEditPolicies();
		this.removeEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE);
		this.installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE, new CorrespondenceDnDEditPolicy());
	}

	class CorrespondenceDnDEditPolicy extends DragDropEditPolicy
	{
		@Override
		public Command getDropObjectsCommand(DropObjectsRequest request)
		{
			List<?> objects = request.getObjects();
			if (objects.size() != 1)
			{
				return super.getDropObjectsCommand(request);
			}

			Object object = objects.get(0);
			if (object == null || !(object instanceof EClass))
			{
				return super.getDropObjectsCommand(request);
			}
			EClass eClass = (EClass) object;

			CorrespondenceNode cn = TggFactory.eINSTANCE.createCorrespondenceNode();
			CompoundCommand cc = new CompoundCommand();

			// zur Domain hinzu fügen
			EObject eObj = ((View) (getHost().getModel())).getElement();
			SetRequest sr = new SetRequest(eObj, TggPackage.eINSTANCE.getCorrespondenceDomain_CorrespondenceElements(), cn);
			TransactionalEditingDomain ted = sr.getEditingDomain();
			cc.add(new ICommandProxy(new SetValueCommand(sr)));

			// Classifier setzen
			sr = new SetRequest(ted, cn, TggPackage.eINSTANCE.getCorrespondenceNode_Classifier(), eClass);
			cc.add(new ICommandProxy(new SetValueCommand(sr)));

			// Namen setzen
			String name = Utility.firstToLowerCase(eClass.getName());
			sr = new SetRequest(ted, cn, StoryDiagramEcorePackage.eINSTANCE.getNamedElement_Name(), name);
			cc.add(new ICommandProxy(new SetValueCommand(sr)));

			// Node an entspr. Stelle anlegen
			ViewDescriptor vd = new ViewDescriptor(new EObjectAdapter(cn), Node.class,
					((IHintedType) TggElementTypes.CorrespondenceNode_3001).getSemanticHint(),
					CustomCorrespondenceDomainCorrespondenceDomainCompartmentEditPart.this.getDiagramPreferencesHint());
			CreateViewRequest cvr = new CreateViewRequest(vd);
			cvr.setLocation(request.getLocation());
			cc.add(CustomCorrespondenceDomainCorrespondenceDomainCompartmentEditPart.this.getCommand(cvr));

			return cc.unwrap();
		}
	}

}
