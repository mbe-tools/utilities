package de.hpi.sam.tgg.diagram.custom.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.diagram.custom.Utility;
import de.hpi.sam.tgg.diagram.edit.commands.ModelLinkReorientCommand;

public class CustomModelLinkReorientCommand extends ModelLinkReorientCommand
{

	public CustomModelLinkReorientCommand(ReorientRelationshipRequest request)
	{
		super(request);
	}

	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
	{
		if (!canExecute())
		{
			throw new ExecutionException("Invalid arguments in reorient link command"); //$NON-NLS-1$
		}

		CommandResult result;
		if (this.reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE)
		{
			result = this.reorientSource();
		}
		else if (this.reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET)
		{
			result = this.reorientTarget();
		}
		else
		{
			throw new IllegalStateException();
		}

		if (this.getLink().getOppositeTGGLink() != null)
		{
			this.getLink().getOppositeTGGLink().setOppositeTGGLink(null);
			this.getLink().setOppositeTGGLink(null);
		}

		EReference reference = null;
		if (this.getLink().getSource().getClassifier() instanceof EClass)
		{
			reference = Utility.getConnectingEReference((EClass) this.getLink().getSource().getClassifier(), this.getLink().getTarget()
					.getClassifier());
		}
		this.getLink().setEReference(reference);

		if (reference.getEOpposite() != null)
		{
			ModelLink opLink = Utility.getModelLinkInCase(this.getLink().getTarget(), this.getLink().getSource(), reference.getEOpposite());

			if (opLink != null)
			{
				this.getLink().setOppositeTGGLink(opLink);
				opLink.setOppositeTGGLink(this.getLink());
			}
		}

		return result;
	}

}
