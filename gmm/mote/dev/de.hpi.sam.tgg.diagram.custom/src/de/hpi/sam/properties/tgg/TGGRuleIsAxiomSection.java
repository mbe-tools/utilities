package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractBooleanPropertySection;

import de.hpi.sam.tgg.TggPackage;

public class TGGRuleIsAxiomSection extends AbstractBooleanPropertySection
{

	@Override
	protected EStructuralFeature getFeature()
	{
		return TggPackage.eINSTANCE.getTGGRule_IsAxiom();
	}

	@Override
	protected String getLabelText()
	{
		return "Is Axiom";
	}

}
