package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.providers.TabbedPropertiesLabelProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;

import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;

public class TGGRuleInputElementsSection extends EnhancedListChooserPropertySection
{

	@Override
	protected IBaseLabelProvider getLabelProvider()
	{
		return new TabbedPropertiesLabelProvider(TggDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory());
	}

	@Override
	protected Object getListValues()
	{
		return ((TGGRule) this.getEObject()).getInputElements();
	}

	@Override
	protected EStructuralFeature getFeature()
	{
		return TggPackage.eINSTANCE.getTGGRule_InputElements();
	}

	@Override
	protected String getLabelText()
	{
		return "Input Elements";
	}

	@Override
	protected void addListener()
	{
		super.addListener();

		if (this.getEObject() == null)
		{
			return;
		}

		TggDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory().adapt(this.getEObject(), IItemLabelProvider.class);
	}
}
