package de.hpi.sam.tgg.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.RuleElement;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeInputEditPart;

public class CustomCorrespondenceNodeInputEditPart extends CorrespondenceNodeInputEditPart
{

	static final String	RULE_FILTER_ID	= "TggRule";

	public CustomCorrespondenceNodeInputEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void addNotationalListeners()
	{
		super.addNotationalListeners();
		final TGGRule tggRule = (TGGRule) ((CorrespondenceNode) ((View) getModel()).getElement()).eContainer().eContainer();
		addListenerFilter(RULE_FILTER_ID, this, tggRule, tggRule.eClass().getEStructuralFeature("inputElements"));
	}

	@Override
	protected void removeNotationalListeners()
	{
		super.removeNotationalListeners();
		removeListenerFilter(RULE_FILTER_ID);
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof TGGRule)
		{
			updateFigure();
		}

		super.handleNotificationEvent(event);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	protected void updateFigure()
	{
		final CorrespondenceNode cNode = (CorrespondenceNode) ((View) getModel()).getElement();
		final EList<RuleElement> inputElems = ((TGGRule) cNode.eContainer().eContainer()).getInputElements();

		String text = "";
		if (inputElems.contains(cNode))
		{
			text = "<<input>>";
		}
		setLabelText(text);
	}
}
