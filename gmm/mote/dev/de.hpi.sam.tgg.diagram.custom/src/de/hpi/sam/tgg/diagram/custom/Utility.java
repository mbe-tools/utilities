package de.hpi.sam.tgg.diagram.custom;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.ModelDomain;
import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.RuleElement;
import de.hpi.sam.tgg.RuleVariable;
import de.hpi.sam.tgg.TGGModifierEnumeration;

public class Utility
{

	public static void adaptColor(EditPart editPart, IFigure figure)
	{
		TGGModifierEnumeration tggMod = ((RuleElement) ((View) editPart.getModel()).getElement()).getModifier();

		Color temp = TggDiagramConstants.MODIFIER_NONE_COLOR;
		if (tggMod == TGGModifierEnumeration.CREATE)
		{
			temp = TggDiagramConstants.MODIFIER_CREATE_COLOR;
		}

		figure.setForegroundColor(temp);
	}

	public static void adaptModifierText(ITextAwareEditPart editPart)
	{
		TGGModifierEnumeration tggMod = ((RuleElement) ((View) editPart.getModel()).getElement()).getModifier();

		String temp = "";
		if (tggMod == TGGModifierEnumeration.CREATE)
		{
			temp = "<<create>>";
		}

		editPart.setLabelText(temp);
	}

	public static void adaptClassifierText(ITextAwareEditPart editPart)
	{
		EObject eObj = ((View) editPart.getModel()).getElement();
		EClassifier classifier = null;

		if (eObj instanceof CorrespondenceNode)
		{
			classifier = ((CorrespondenceNode) eObj).getClassifier();
		}
		else if (eObj instanceof ModelObject)
		{
			classifier = ((ModelObject) eObj).getClassifier();
		}
		else if (eObj instanceof RuleVariable)
		{
			classifier = ((RuleVariable) eObj).getClassifier();
		}

		String temp = "";
		if (classifier != null)
		{
			temp = ":" + classifier.getName();
		}

		editPart.setLabelText(temp);
	}

	public static void adaptConstraintText(ITextAwareEditPart editPart)
	{
		CallActionExpression callActionExpression = (CallActionExpression) ((View) editPart.getModel()).getElement();

		String text = "";
		if (callActionExpression != null)
		{
			text = callActionExpression.toString();
		}
		text = "[ " + text + " ]";

		editPart.setLabelText(text);
		((WrappingLabel) editPart.getFigure()).setTextWrap(true);
	}

	public static void adaptRVAssignmentText(ITextAwareEditPart editPart, String prefix)
	{
		CallActionExpression callActionExpression = (CallActionExpression) ((View) editPart.getModel()).getElement();

		String text = "";
		if (callActionExpression != null)
		{
			text = callActionExpression.toString();
		}
		text = prefix + text;
		editPart.setLabelText(text);
	}

	public static String firstToLowerCase(String text)
	{
		if (text.length() > 1)
		{
			return text.substring(0, 1).toLowerCase() + text.substring(1);
		}
		else
		{
			return text.toLowerCase();
		}
	}

	public static EReference getConnectingEReference(final EClass srcClass, final EClassifier dstClassifier)
	{
		EList<EClass> typeList;
		if (dstClassifier instanceof EClass)
		{
			typeList = ((EClass) dstClassifier).getEAllSuperTypes();
		}
		else
		{
			typeList = ECollections.emptyEList();
		}

		EList<EReference> referenceList = srcClass.getEAllReferences();
		EClassifier classifier;
		EReference resultReference = null;
		for (EReference reference : referenceList)
		{
			classifier = reference.getEType();
			if (classifier.equals(dstClassifier) || typeList.contains(classifier))
			{
				if (resultReference == null)
				{
					resultReference = reference;
				}
				else
				{
					return null;
				}
			}
		}

		return resultReference;
	}

	public static ModelLink getModelLinkInCase(ModelObject source, ModelObject target, EReference reference)
	{
		ModelDomain domain = (ModelDomain) source.eContainer();
		ModelLink link;
		for (ModelElement element : domain.getModelElements())
		{
			if (!(element instanceof ModelLink))
			{
				continue;
			}

			link = (ModelLink) element;

			if (link.getSource().equals(source) && link.getTarget().equals(target) && link.getEReference().equals(reference))
			{
				return link;
			}
		}

		return null;
	}
}
