package de.hpi.sam.tgg.diagram.custom;

import org.eclipse.emf.common.util.URI;

import de.hpi.sam.tgg.TGGRule;

public class DiagramsUtil {

	private DiagramsUtil() {
	}
	
	public static URI ruleDiagramFileUri( final TGGRule p_rule ) {
		URI newUri = p_rule.eResource().getURI().trimSegments( 1 );
		newUri = newUri.appendSegment( p_rule.getName().substring( 0, 1 ).toLowerCase() + p_rule.getName().substring( 1 ) );
		
		return newUri.appendFileExtension( "tggdiag" );
	}
}