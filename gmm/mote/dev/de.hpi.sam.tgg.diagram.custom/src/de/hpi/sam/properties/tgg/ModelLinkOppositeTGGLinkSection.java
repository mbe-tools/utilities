package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;

public class ModelLinkOppositeTGGLinkSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.tgg.TggPackage.eINSTANCE.getModelLink_OppositeTGGLink();
	}

	@Override
	protected String getLabelText()
	{
		return "Opposite TGGLink";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.tgg.ModelLink) getEObject()).getOppositeTGGLink();
	}
}
