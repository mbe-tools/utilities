package de.hpi.sam.tgg.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.tgg.diagram.custom.TggDiagramConstants;
import de.hpi.sam.tgg.diagram.edit.parts.AttributeAssignmentEditPart;

public class CustomAttributeAssignmentEditPart extends AttributeAssignmentEditPart
{

	public CustomAttributeAssignmentEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof AttributeAssignment)
		{
			updateFigure();
		}

		super.handleNotificationEvent(event);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	protected void updateFigure()
	{
		AttributeAssignment aa = ((AttributeAssignment) ((View) getModel()).getElement());

		EStructuralFeature strucFeat = aa.getEStructuralFeature();
		String text = (strucFeat != null) ? strucFeat.getName() : "null";

		text += " := ";

		Expression expr = aa.getAssignmentExpression();
		text += (expr != null) ? expr.toString() : "null";

		setLabelText(text);

		((WrappingLabel) getFigure()).setTextWrap(true);
		getFigure().setForegroundColor(TggDiagramConstants.MODIFIER_CREATE_COLOR);
	}
}
