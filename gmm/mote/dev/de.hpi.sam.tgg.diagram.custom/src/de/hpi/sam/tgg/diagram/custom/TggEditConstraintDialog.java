package de.hpi.sam.tgg.diagram.custom;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.swt.widgets.Shell;

import de.hpi.sam.storyDiagramEcore.diagram.custom.CommonEditExpressionDialog;
import de.hpi.sam.tgg.ModelDomain;
import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TGGRule;

public class TggEditConstraintDialog extends CommonEditExpressionDialog
{

	protected TGGRule	rule;

	public TggEditConstraintDialog(Shell parent)
	{
		super(parent);
	}

	public void setRule(TGGRule rule)
	{
		this.rule = rule;
		setRootContainer(rule);
	}

	@Override
	protected Map<String, EClassifier> getContextInformation()
	{
		Map<String, EClassifier> contextInfos = new HashMap<String, EClassifier>();

		ModelDomain[] modelDomains =
		{
				this.rule.getSourceDomain(), this.rule.getTargetDomain()
		};

		String name;
		for (ModelDomain modelDomain : modelDomains)
		{
			for (ModelElement modelElement : modelDomain.getModelElements())
			{
				name = modelElement.getName();

				if (modelElement instanceof ModelLink || name == null || name.equals(""))
				{
					continue;
				}
				else if (modelElement instanceof ModelObject)
				{
					contextInfos.put(name, ((ModelObject) modelElement).getClassifier());
				}
			}
		}

		return contextInfos;
	}

}
