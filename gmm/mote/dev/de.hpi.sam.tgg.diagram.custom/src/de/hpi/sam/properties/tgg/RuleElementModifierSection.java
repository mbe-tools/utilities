package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.sections.AbstractEnumerationPropertySection;

public class RuleElementModifierSection extends AbstractEnumerationPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.tgg.TggPackage.eINSTANCE.getRuleElement_Modifier();
	}

	@Override
	protected String getLabelText()
	{
		return "Modifier";
	}

	@Override
	protected String[] getEnumerationFeatureValues()
	{
		org.eclipse.emf.ecore.EEnum eenum = de.hpi.sam.tgg.TggPackage.eINSTANCE.getTGGModifierEnumeration();
		org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EEnumLiteral> eLiteralList = eenum.getELiterals();
		String[] result = new String[eLiteralList.size()];

		for (int i = 0; i < eLiteralList.size(); i++)
		{
			result[i] = eLiteralList.get(i).getLiteral();
		}

		return result;
	}

	@Override
	protected String getFeatureAsText()
	{
		return ((de.hpi.sam.tgg.TGGModifierEnumeration) getOldFeatureValue()).getLiteral();
	}

	@Override
	protected Object getFeatureValue(int index)
	{
		return de.hpi.sam.tgg.TGGModifierEnumeration.get(index);
	}

	@Override
	protected Object getOldFeatureValue()
	{
		return ((de.hpi.sam.tgg.RuleElement) this.getEObject()).getModifier();
	}

}
