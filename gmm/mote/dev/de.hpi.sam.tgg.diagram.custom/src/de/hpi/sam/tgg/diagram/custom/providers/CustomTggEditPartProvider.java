package de.hpi.sam.tgg.diagram.custom.providers;

import de.hpi.sam.tgg.diagram.custom.edit.parts.CustomTggEditPartFactory;
import de.hpi.sam.tgg.diagram.providers.TggEditPartProvider;

public class CustomTggEditPartProvider extends TggEditPartProvider
{

	public CustomTggEditPartProvider()
	{
		super();
		setFactory(new CustomTggEditPartFactory());
	}

}
