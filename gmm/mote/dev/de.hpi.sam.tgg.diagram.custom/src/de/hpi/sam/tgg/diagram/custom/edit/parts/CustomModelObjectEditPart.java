package de.hpi.sam.tgg.diagram.custom.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.diagram.custom.Utility;
import de.hpi.sam.tgg.diagram.custom.edit.policies.CustomModelObjectItemSemanticEditPolicy;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectEditPart;

public class CustomModelObjectEditPart extends ModelObjectEditPart
{

	public CustomModelObjectEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification notification)
	{
		if (notification.getNotifier() instanceof ModelObject)
		{
			updateFigure();
		}

		super.handleNotificationEvent(notification);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	@Override
	protected void createDefaultEditPolicies()
	{
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new CustomModelObjectItemSemanticEditPolicy());
	}

	protected void updateFigure()
	{
		Utility.adaptColor(this, (IFigure) getFigure().getChildren().get(0));
	}

	public class CustomModelObjectFigure extends ModelObjectFigure
	{

		public CustomModelObjectFigure()
		{
			super();
			CustomModelObjectEditPart.this.updateFigure();
		}

	}
}
