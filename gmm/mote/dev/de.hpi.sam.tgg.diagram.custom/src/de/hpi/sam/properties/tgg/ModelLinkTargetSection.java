package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;

public class ModelLinkTargetSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.tgg.TggPackage.eINSTANCE.getModelLink_Target();
	}

	@Override
	protected String getLabelText()
	{
		return "Target";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.tgg.ModelLink) getEObject()).getTarget();
	}
}
