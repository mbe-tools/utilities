package de.hpi.sam.tgg.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.tgg.diagram.custom.Utility;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpressionEditPart;

public class CustomCallActionExpressionEditPart extends CallActionExpressionEditPart
{

	public CustomCallActionExpressionEditPart(View view)
	{
		super(view);
	}

	protected void updateFigure()
	{
		Utility.adaptConstraintText(this);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof CallActionExpression)
		{
			updateFigure();
		}

		super.handleNotificationEvent(event);
	}

}
