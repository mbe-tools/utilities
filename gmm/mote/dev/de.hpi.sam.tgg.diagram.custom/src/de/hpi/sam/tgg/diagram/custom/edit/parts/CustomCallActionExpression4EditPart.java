package de.hpi.sam.tgg.diagram.custom.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.tgg.diagram.custom.Utility;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression4EditPart;

public class CustomCallActionExpression4EditPart extends CallActionExpression4EditPart
{

	public CustomCallActionExpression4EditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() instanceof CallActionExpression)
		{
			updateFigure();
		}

		super.handleNotificationEvent(event);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		updateFigure();
	}

	protected void updateFigure()
	{
		Utility.adaptRVAssignmentText(this, "r: ");
	}

}
