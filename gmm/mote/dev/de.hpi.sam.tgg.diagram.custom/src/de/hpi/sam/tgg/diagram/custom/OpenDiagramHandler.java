package de.hpi.sam.tgg.diagram.custom;

import java.util.Collection;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.IDE;

import de.hpi.sam.tgg.TGGRule;
import fr.labsticc.framework.emf.core.util.EMFUtil;

public class OpenDiagramHandler extends AbstractHandler {

	@Override
	public Object execute(final ExecutionEvent p_event ) throws ExecutionException {
		final ISelection currentSelection = HandlerUtil.getCurrentSelection( p_event );
		final Collection<TGGRule> selectedRules = EMFUtil.selectedObjects( currentSelection, TGGRule.class );
		
		for ( final TGGRule rule : selectedRules ) {
			try {
				IDE.openEditor( HandlerUtil.getActiveEditor( p_event ).getSite().getPage(), 
								(IFile) EMFUtil.convertToIDEResource( DiagramsUtil.ruleDiagramFileUri( rule ) ) );
			}
			catch( final PartInitException p_ex ) {
				p_ex.printStackTrace();
			}
		}

		return null;
	}
}
