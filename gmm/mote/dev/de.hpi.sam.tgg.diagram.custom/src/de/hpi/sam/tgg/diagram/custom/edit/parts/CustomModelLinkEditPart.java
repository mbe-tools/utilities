package de.hpi.sam.tgg.diagram.custom.edit.parts;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.Graphics;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Anchor;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.RelativeBendpoints;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.ILineIsHideableFigure;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.diagram.custom.Utility;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkEditPart;

public class CustomModelLinkEditPart extends ModelLinkEditPart
{
	private boolean	selectable	= true;

	public CustomModelLinkEditPart(View view)
	{
		super(view);
	}

	@Override
	protected void handleNotificationEvent(Notification notification)
	{
		if (notification.getNotifier() instanceof ModelLink)
		{
			switch (notification.getFeatureID(ModelLink.class))
			{
				case TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK:
					this.refreshMergedModelLink();
					this.trackMergedModelLink(true);
					break;
			}
			this.updateFigure();
		}

		if (NotationPackage.Literals.ROUTING_STYLE__ROUTING.equals(notification.getFeature())
				|| notification.getNotifier() instanceof RelativeBendpoints || notification.getNotifier() instanceof Anchor)
		{
			this.trackMergedModelLink(true);
		}

		super.handleNotificationEvent(notification);
	}

	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		this.refreshMergedModelLink();
		this.trackMergedModelLink(false);
		updateFigure();
	}

	protected void updateFigure()
	{
		Utility.adaptColor(this, getFigure());
	}

	@Override
	protected Connection createConnectionFigure()
	{
		return new ModelLinkFigure();
	}

	/**
	 * @see org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionEditPart#isSelectable()
	 */
	@Override
	public boolean isSelectable()
	{
		return selectable;
	}

	/**
	 * @param selectable
	 *            the selectable to set
	 */
	public void setSelectable(boolean selectable)
	{
		this.selectable = selectable;
	}

	/**
	 * Refresh reference decoration and visibility
	 */
	protected void refreshMergedModelLink()
	{
		EObject semanticElement = resolveSemanticElement();
		if (!(semanticElement instanceof ModelLink))
		{
			return;
		}

		ModelLinkFigure fig = (ModelLinkFigure) getPrimaryShape();
		if (((ModelLink) semanticElement).getOppositeTGGLink() != null)
		{
			fig.displayTargetDecoration(false);
		}
		else
		{
			fig.setHideLine(false);
			fig.displayTargetDecoration(true);
		}
	}

	protected void trackMergedModelLink(boolean trackFully)
	{
		EObject semanticElement = resolveSemanticElement();
		if (!(semanticElement instanceof ModelLink))
		{
			return;
		}

		EObject opLink = ((ModelLink) semanticElement).getOppositeTGGLink();
		
		if (opLink != null) {
			try {
				de.hpi.sam.storyDiagramEcore.diagram.custom.Utility.trackMergedLinks(this, opLink, trackFully);
			}
			catch ( final ExecutionException p_ex ) {
				p_ex.printStackTrace();
			}
		}
	}

	public class ModelLinkFigure extends ModelLinkEditPart.ModelLinkFigure implements ILineIsHideableFigure
	{
		private boolean	hideLine;

		/**
		 * @return the hideLine
		 */
		public boolean isHideLine()
		{
			return hideLine;
		}

		/**
		 * @param hideLine
		 *            the hideLine to set
		 */
		public void setHideLine(boolean hideLine)
		{
			this.hideLine = hideLine;

			synchronized (CustomModelLinkEditPart.this)
			{
				if (hideLine)
				{
					getViewer().deselect(CustomModelLinkEditPart.this);
					CustomModelLinkEditPart.this.setSelected(SELECTED_NONE);
					CustomModelLinkEditPart.this.setSelectable(false);
				}
				else
				{
					CustomModelLinkEditPart.this.setSelectable(true);
				}
			}
		}

		public void displayTargetDecoration(boolean display)
		{
			if (display)
			{
				setTargetDecoration(this.createTargetDecoration());
			}
			else
			{
				if (getTargetDecoration() != null)
				{
					setTargetDecoration(null);
				}
			}
		}

		@Override
		public void paintFigure(Graphics graphics)
		{
			if (this.hideLine)
			{
				return;
			}
			super.paintFigure(graphics);
		}
	}
}
