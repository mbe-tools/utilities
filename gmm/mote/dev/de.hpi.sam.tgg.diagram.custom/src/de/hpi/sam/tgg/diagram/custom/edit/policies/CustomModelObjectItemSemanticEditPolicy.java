package de.hpi.sam.tgg.diagram.custom.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import de.hpi.sam.tgg.diagram.custom.edit.commands.CustomModelLinkCreateCommand;
import de.hpi.sam.tgg.diagram.custom.edit.commands.CustomModelLinkReorientCommand;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.policies.ModelObjectItemSemanticEditPolicy;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

public class CustomModelObjectItemSemanticEditPolicy extends ModelObjectItemSemanticEditPolicy
{
	protected Command getReorientRelationshipCommand(ReorientRelationshipRequest req)
	{
		switch (getVisualID(req))
		{
			case ModelLinkEditPart.VISUAL_ID:
				return getGEFWrapper(new CustomModelLinkReorientCommand(req));
		}

		return super.getReorientRelationshipCommand(req);
	}

	protected Command getStartCreateRelationshipCommand(CreateRelationshipRequest req)
	{
		if (TggElementTypes.ModelLink_4002 == req.getElementType())
		{
			return getGEFWrapper(new CustomModelLinkCreateCommand(req, req.getSource(), req.getTarget()));
		}

		return super.getStartCreateRelationshipCommand(req);
	}

	protected Command getCompleteCreateRelationshipCommand(CreateRelationshipRequest req)
	{
		if (TggElementTypes.ModelLink_4002 == req.getElementType())
		{
			return getGEFWrapper(new CustomModelLinkCreateCommand(req, req.getSource(), req.getTarget()));
		}

		return super.getCompleteCreateRelationshipCommand(req);
	}
}
