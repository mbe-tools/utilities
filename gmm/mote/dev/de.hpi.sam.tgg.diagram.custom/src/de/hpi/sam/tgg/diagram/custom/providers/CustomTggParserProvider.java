package de.hpi.sam.tgg.diagram.custom.providers;

import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;

import de.hpi.sam.tgg.diagram.edit.parts.StringExpression3EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression4EditPart;
import de.hpi.sam.tgg.diagram.parsers.MessageFormatParser;
import de.hpi.sam.tgg.diagram.providers.TggParserProvider;

public class CustomTggParserProvider extends TggParserProvider
{

	protected IParser getParser(int visualID)
	{
		IParser parser = super.getParser(visualID);
		switch (visualID)
		{
			case StringExpression3EditPart.VISUAL_ID:
				processStringExpression3Parser((MessageFormatParser) parser);
				break;
			case StringExpression4EditPart.VISUAL_ID:
				processStringExpression4Parser((MessageFormatParser) parser);
				break;
		}
		return parser;
	}

	protected void processStringExpression3Parser(MessageFormatParser mfparser)
	{
		mfparser.setViewPattern("f: {0}");
	}

	protected void processStringExpression4Parser(MessageFormatParser mfparser)
	{
		mfparser.setViewPattern("r: {0}");
	}

}