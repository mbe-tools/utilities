package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecoretools.tabbedproperties.providers.TabbedPropertiesLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;

import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;

public class CorrespondenceNodeOutgoingCorrespondenceLinksSection extends EnhancedListChooserPropertySection
{

	@Override
	protected IBaseLabelProvider getLabelProvider()
	{
		return new TabbedPropertiesLabelProvider(TggDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory());
	}

	@Override
	protected Object getListValues()
	{
		return ((CorrespondenceNode) this.getEObject()).getOutgoingCorrespondenceLinks();
	}

	@Override
	protected EStructuralFeature getFeature()
	{
		return TggPackage.eINSTANCE.getCorrespondenceNode_OutgoingCorrespondenceLinks();
	}

	@Override
	protected String getLabelText()
	{
		return "Outgoing Links";
	}

}
