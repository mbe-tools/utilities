package de.hpi.sam.tgg.diagram.custom.part.expressions;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.custom.part.expressions.CommonEditExpressionAction;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.diagram.custom.TggEditExpressionDialog;
import de.hpi.sam.tgg.diagram.edit.parts.AttributeAssignmentEditPart;

public class EditAttributeAssignmentExpressionAction extends CommonEditExpressionAction
{

	@Override
	protected void prepareDialog()
	{
		TggEditExpressionDialog dialog = new TggEditExpressionDialog(null);
		dialog.setStructuralFeature(SdmPackage.eINSTANCE.getAttributeAssignment_AssignmentExpression());
		dialog.setModelObject((ModelObject) getExpressionOwner().eContainer());
		this.editExpressionDialog = dialog;
		super.prepareDialog();
	}

	@Override
	protected EClassifier getContextClassifier()
	{
		EStructuralFeature feature = ((AttributeAssignment) getExpressionOwner()).getEStructuralFeature();
		return (feature != null) ? feature.getEContainingClass() : null;
	}

	@Override
	protected TransactionalEditingDomain getEditingDomain()
	{
		return getEditPart().getEditingDomain();
	}

	@Override
	protected EClassifier getExpectedClassifier()
	{
		EStructuralFeature feature = ((AttributeAssignment) getExpressionOwner()).getEStructuralFeature();
		return (feature != null) ? feature.getEType() : null;
	}

	@Override
	protected Expression getExpression()
	{
		return ((AttributeAssignment) getExpressionOwner()).getAssignmentExpression();
	}

	@Override
	protected void setNewExpression(Expression newExpression)
	{
		((AttributeAssignment) getExpressionOwner()).setAssignmentExpression(newExpression);
	}

	protected AttributeAssignmentEditPart getEditPart()
	{
		return (AttributeAssignmentEditPart) getStructuredSelection().getFirstElement();
	}

	@Override
	protected EObject getExpressionOwner()
	{
		return ((View) getEditPart().getModel()).getElement();
	}

}