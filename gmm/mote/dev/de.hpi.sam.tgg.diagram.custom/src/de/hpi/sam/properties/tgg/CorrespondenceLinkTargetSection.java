package de.hpi.sam.properties.tgg;

import org.eclipse.emf.ecore.EStructuralFeature;

public class CorrespondenceLinkTargetSection extends EnhancedChooserPropertySection
{
	@Override
	protected EStructuralFeature getFeature()
	{
		return de.hpi.sam.tgg.TggPackage.eINSTANCE.getCorrespondenceLink_Target();
	}

	@Override
	protected String getLabelText()
	{
		return "Target";
	}

	@Override
	protected Object getFeatureValue()
	{
		return ((de.hpi.sam.tgg.CorrespondenceLink) getEObject()).getTarget();
	}
}
