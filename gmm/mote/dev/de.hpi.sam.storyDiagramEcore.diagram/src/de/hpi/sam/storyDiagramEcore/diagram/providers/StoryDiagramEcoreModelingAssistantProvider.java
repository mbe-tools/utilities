package de.hpi.sam.storyDiagramEcore.diagram.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.modelingassistant.ModelingAssistantProvider;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

/**
 * @generated
 */
public class StoryDiagramEcoreModelingAssistantProvider extends
		ModelingAssistantProvider {

	/**
	 * @generated
	 */
	public List getTypesForPopupBar(IAdaptable host) {
		IGraphicalEditPart editPart = (IGraphicalEditPart) host
				.getAdapter(IGraphicalEditPart.class);
		if (editPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(10);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityFinalNode_2018);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.DecisionNode_2019);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ExpressionActivityNode_2020);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.FlowFinalNode_2021);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ForkNode_2022);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.InitialNode_2023);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.JoinNode_2024);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.Semaphore_2027);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MergeNode_2025);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryActionNode_2026);
			return types;
		}
		if (editPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3031);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3032);
			return types;
		}
		if (editPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3033);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3034);
			return types;
		}
		if (editPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(3);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3036);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3037);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.AttributeAssignment_3038);
			return types;
		}
		if (editPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(1);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035);
			return types;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnSourceAndTarget(IAdaptable source,
			IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getTypesForTarget(IAdaptable source,
			IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) {
			return ((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForSource(IAdaptable target,
			IElementType relationshipType) {
		return selectExistingElement(target,
				getTypesForSource(target, relationshipType));
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForTarget(IAdaptable source,
			IElementType relationshipType) {
		return selectExistingElement(source,
				getTypesForTarget(source, relationshipType));
	}

	/**
	 * @generated
	 */
	protected EObject selectExistingElement(IAdaptable host, Collection types) {
		if (types.isEmpty()) {
			return null;
		}
		IGraphicalEditPart editPart = (IGraphicalEditPart) host
				.getAdapter(IGraphicalEditPart.class);
		if (editPart == null) {
			return null;
		}
		Diagram diagram = (Diagram) editPart.getRoot().getContents().getModel();
		HashSet<EObject> elements = new HashSet<EObject>();
		for (Iterator<EObject> it = diagram.getElement().eAllContents(); it
				.hasNext();) {
			EObject element = it.next();
			if (isApplicableElement(element, types)) {
				elements.add(element);
			}
		}
		if (elements.isEmpty()) {
			return null;
		}
		return selectElement((EObject[]) elements.toArray(new EObject[elements
				.size()]));
	}

	/**
	 * @generated
	 */
	protected boolean isApplicableElement(EObject element, Collection types) {
		IElementType type = ElementTypeRegistry.getInstance().getElementType(
				element);
		return types.contains(type);
	}

	/**
	 * @generated
	 */
	protected EObject selectElement(EObject[] elements) {
		Shell shell = Display.getCurrent().getActiveShell();
		ILabelProvider labelProvider = new AdapterFactoryLabelProvider(
				de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
						.getInstance().getItemProvidersAdapterFactory());
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(
				shell, labelProvider);
		dialog.setMessage(de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryDiagramEcoreModelingAssistantProviderMessage);
		dialog.setTitle(de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryDiagramEcoreModelingAssistantProviderTitle);
		dialog.setMultipleSelection(false);
		dialog.setElements(elements);
		EObject selected = null;
		if (dialog.open() == Window.OK) {
			selected = (EObject) dialog.getFirstResult();
		}
		return selected;
	}
}
