package de.hpi.sam.storyDiagramEcore.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientReferenceRelationshipRequest;

/**
 * @generated
 */
public class MapEntryStoryPatternLinkItemSemanticEditPolicy
		extends
		de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public MapEntryStoryPatternLinkItemSemanticEditPolicy() {
		super(
				de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		return getGEFWrapper(new DestroyElementCommand(req));
	}

	/**
	 * @generated
	 */
	protected Command getCreateRelationshipCommand(CreateRelationshipRequest req) {
		Command command = req.getTarget() == null ? getStartCreateRelationshipCommand(req)
				: getCompleteCreateRelationshipCommand(req);
		return command != null ? command : super
				.getCreateRelationshipCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getStartCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLinkValueTarget_4011 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.MapEntryStoryPatternLinkValueTargetCreateCommand(
					req, req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Command getCompleteCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLinkValueTarget_4011 == req
				.getElementType()) {
			return null;
		}
		return null;
	}

	/**
	 * Returns command to reorient EReference based link. New link target or
	 * source should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientReferenceRelationshipCommand(
			ReorientReferenceRelationshipRequest req) {
		switch (getVisualID(req)) {
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart.VISUAL_ID:
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.MapEntryStoryPatternLinkValueTargetReorientCommand(
					req));
		}
		return super.getReorientReferenceRelationshipCommand(req);
	}

}
