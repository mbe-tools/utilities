package de.hpi.sam.storyDiagramEcore.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

/**
 * @generated
 */
public class StoryDiagramEcoreElementTypes {

	/**
	 * @generated
	 */
	private StoryDiagramEcoreElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static ImageRegistry imageRegistry;

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType Activity_1000 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.Activity_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ActivityFinalNode_2018 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.ActivityFinalNode_2018"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType DecisionNode_2019 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.DecisionNode_2019"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ExpressionActivityNode_2020 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.ExpressionActivityNode_2020"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType FlowFinalNode_2021 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.FlowFinalNode_2021"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ForkNode_2022 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.ForkNode_2022"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType InitialNode_2023 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.InitialNode_2023"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType JoinNode_2024 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.JoinNode_2024"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Semaphore_2027 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.Semaphore_2027"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType MergeNode_2025 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.MergeNode_2025"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType StoryActionNode_2026 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.StoryActionNode_2026"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType StringExpression_3031 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.StringExpression_3031"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType CallActionExpression_3032 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.CallActionExpression_3032"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType StringExpression_3033 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.StringExpression_3033"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType CallActionExpression_3034 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.CallActionExpression_3034"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType StoryPatternObject_3035 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.StoryPatternObject_3035"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType StringExpression_3036 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.StringExpression_3036"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType CallActionExpression_3037 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.CallActionExpression_3037"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType AttributeAssignment_3038 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.AttributeAssignment_3038"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ActivityEdge_4006 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.ActivityEdge_4006"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType StoryPatternLink_4007 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.StoryPatternLink_4007"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType StoryPatternContainmentLink_4008 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.StoryPatternContainmentLink_4008"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType StoryPatternExpressionLink_4009 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.StoryPatternExpressionLink_4009"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType MapEntryStoryPatternLink_4010 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.MapEntryStoryPatternLink_4010"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType MapEntryStoryPatternLinkValueTarget_4011 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.MapEntryStoryPatternLinkValueTarget_4011"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ReleaseSemaphoreEdge_4012 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.ReleaseSemaphoreEdge_4012"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType AcquireSemaphoreEdge_4013 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.AcquireSemaphoreEdge_4013"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType LinkOrderConstraint_4015 = getElementType("de.hpi.sam.storyDiagramEcore.diagram.LinkOrderConstraint_4015"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	private static ImageRegistry getImageRegistry() {
		if (imageRegistry == null) {
			imageRegistry = new ImageRegistry();
		}
		return imageRegistry;
	}

	/**
	 * @generated
	 */
	private static String getImageRegistryKey(ENamedElement element) {
		return element.getName();
	}

	/**
	 * @generated
	 */
	private static ImageDescriptor getProvidedImageDescriptor(
			ENamedElement element) {
		if (element instanceof EStructuralFeature) {
			EStructuralFeature feature = ((EStructuralFeature) element);
			EClass eContainingClass = feature.getEContainingClass();
			EClassifier eType = feature.getEType();
			if (eContainingClass != null && !eContainingClass.isAbstract()) {
				element = eContainingClass;
			} else if (eType instanceof EClass
					&& !((EClass) eType).isAbstract()) {
				element = eType;
			}
		}
		if (element instanceof EClass) {
			EClass eClass = (EClass) element;
			if (!eClass.isAbstract()) {
				return de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
						.getInstance().getItemImageDescriptor(
								eClass.getEPackage().getEFactoryInstance()
										.create(eClass));
			}
		}
		// TODO : support structural features
		return null;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		String key = getImageRegistryKey(element);
		ImageDescriptor imageDescriptor = getImageRegistry().getDescriptor(key);
		if (imageDescriptor == null) {
			imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
		}
		return imageDescriptor;
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		String key = getImageRegistryKey(element);
		Image image = getImageRegistry().get(key);
		if (image == null) {
			ImageDescriptor imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
			image = getImageRegistry().get(key);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImage(element);
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(
					Activity_1000,
					de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE
							.getActivity());

			elements.put(ActivityFinalNode_2018,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getActivityFinalNode());

			elements.put(DecisionNode_2019,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getDecisionNode());

			elements.put(ExpressionActivityNode_2020,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getExpressionActivityNode());

			elements.put(FlowFinalNode_2021,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getFlowFinalNode());

			elements.put(ForkNode_2022,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getForkNode());

			elements.put(InitialNode_2023,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getInitialNode());

			elements.put(JoinNode_2024,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getJoinNode());

			elements.put(Semaphore_2027,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getSemaphore());

			elements.put(MergeNode_2025,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getMergeNode());

			elements.put(StoryActionNode_2026,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getStoryActionNode());

			elements.put(
					StringExpression_3031,
					de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
							.getStringExpression());

			elements.put(
					CallActionExpression_3032,
					de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
							.getCallActionExpression());

			elements.put(
					StringExpression_3033,
					de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
							.getStringExpression());

			elements.put(
					CallActionExpression_3034,
					de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
							.getCallActionExpression());

			elements.put(StoryPatternObject_3035,
					de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
							.getStoryPatternObject());

			elements.put(
					StringExpression_3036,
					de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
							.getStringExpression());

			elements.put(
					CallActionExpression_3037,
					de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
							.getCallActionExpression());

			elements.put(AttributeAssignment_3038,
					de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
							.getAttributeAssignment());

			elements.put(ActivityEdge_4006,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getActivityEdge());

			elements.put(StoryPatternLink_4007,
					de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
							.getStoryPatternLink());

			elements.put(StoryPatternContainmentLink_4008,
					de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
							.getStoryPatternContainmentLink());

			elements.put(StoryPatternExpressionLink_4009,
					de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
							.getStoryPatternExpressionLink());

			elements.put(MapEntryStoryPatternLink_4010,
					de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
							.getMapEntryStoryPatternLink());

			elements.put(MapEntryStoryPatternLinkValueTarget_4011,
					de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
							.getMapEntryStoryPatternLink_ValueTarget());

			elements.put(ReleaseSemaphoreEdge_4012,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getReleaseSemaphoreEdge());

			elements.put(AcquireSemaphoreEdge_4013,
					de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
							.getAcquireSemaphoreEdge());

			elements.put(LinkOrderConstraint_4015,
					de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
							.getLinkOrderConstraint());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(Activity_1000);
			KNOWN_ELEMENT_TYPES.add(ActivityFinalNode_2018);
			KNOWN_ELEMENT_TYPES.add(DecisionNode_2019);
			KNOWN_ELEMENT_TYPES.add(ExpressionActivityNode_2020);
			KNOWN_ELEMENT_TYPES.add(FlowFinalNode_2021);
			KNOWN_ELEMENT_TYPES.add(ForkNode_2022);
			KNOWN_ELEMENT_TYPES.add(InitialNode_2023);
			KNOWN_ELEMENT_TYPES.add(JoinNode_2024);
			KNOWN_ELEMENT_TYPES.add(Semaphore_2027);
			KNOWN_ELEMENT_TYPES.add(MergeNode_2025);
			KNOWN_ELEMENT_TYPES.add(StoryActionNode_2026);
			KNOWN_ELEMENT_TYPES.add(StringExpression_3031);
			KNOWN_ELEMENT_TYPES.add(CallActionExpression_3032);
			KNOWN_ELEMENT_TYPES.add(StringExpression_3033);
			KNOWN_ELEMENT_TYPES.add(CallActionExpression_3034);
			KNOWN_ELEMENT_TYPES.add(StoryPatternObject_3035);
			KNOWN_ELEMENT_TYPES.add(StringExpression_3036);
			KNOWN_ELEMENT_TYPES.add(CallActionExpression_3037);
			KNOWN_ELEMENT_TYPES.add(AttributeAssignment_3038);
			KNOWN_ELEMENT_TYPES.add(ActivityEdge_4006);
			KNOWN_ELEMENT_TYPES.add(StoryPatternLink_4007);
			KNOWN_ELEMENT_TYPES.add(StoryPatternContainmentLink_4008);
			KNOWN_ELEMENT_TYPES.add(StoryPatternExpressionLink_4009);
			KNOWN_ELEMENT_TYPES.add(MapEntryStoryPatternLink_4010);
			KNOWN_ELEMENT_TYPES.add(MapEntryStoryPatternLinkValueTarget_4011);
			KNOWN_ELEMENT_TYPES.add(ReleaseSemaphoreEdge_4012);
			KNOWN_ELEMENT_TYPES.add(AcquireSemaphoreEdge_4013);
			KNOWN_ELEMENT_TYPES.add(LinkOrderConstraint_4015);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID:
			return Activity_1000;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID:
			return ActivityFinalNode_2018;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID:
			return DecisionNode_2019;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID:
			return ExpressionActivityNode_2020;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID:
			return FlowFinalNode_2021;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID:
			return ForkNode_2022;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID:
			return InitialNode_2023;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID:
			return JoinNode_2024;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID:
			return Semaphore_2027;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID:
			return MergeNode_2025;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID:
			return StoryActionNode_2026;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID:
			return StringExpression_3031;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart.VISUAL_ID:
			return CallActionExpression_3032;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID:
			return StringExpression_3033;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart.VISUAL_ID:
			return CallActionExpression_3034;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID:
			return StoryPatternObject_3035;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID:
			return StringExpression_3036;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart.VISUAL_ID:
			return CallActionExpression_3037;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart.VISUAL_ID:
			return AttributeAssignment_3038;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID:
			return ActivityEdge_4006;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID:
			return StoryPatternLink_4007;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID:
			return StoryPatternContainmentLink_4008;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID:
			return StoryPatternExpressionLink_4009;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID:
			return MapEntryStoryPatternLink_4010;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart.VISUAL_ID:
			return MapEntryStoryPatternLinkValueTarget_4011;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID:
			return ReleaseSemaphoreEdge_4012;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID:
			return AcquireSemaphoreEdge_4013;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID:
			return LinkOrderConstraint_4015;
		}
		return null;
	}

}
