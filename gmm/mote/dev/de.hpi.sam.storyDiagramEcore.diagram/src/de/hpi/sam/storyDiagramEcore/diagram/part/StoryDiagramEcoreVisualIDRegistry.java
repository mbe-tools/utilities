package de.hpi.sam.storyDiagramEcore.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented by
 * a domain model object.
 * 
 * @generated
 */
public class StoryDiagramEcoreVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "de.hpi.sam.storyDiagramEcore.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.MODEL_ID
					.equals(view.getType())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
				.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
						.getInstance().logError(
								"Unable to parse view type as a visualID number: "
										+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE
				.getActivity().isSuperTypeOf(domainElement.eClass())
				&& isDiagram((de.hpi.sam.storyDiagramEcore.Activity) domainElement)) {
			return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
				.getModelID(containerView);
		if (!de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.MODEL_ID
				.equals(containerModelID)
				&& !"de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorID".equals(containerModelID)) { //$NON-NLS-1$
			return -1;
		}
		int containerVisualID;
		if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.MODEL_ID
				.equals(containerModelID)) {
			containerVisualID = de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getActivityFinalNode().isSuperTypeOf(
							domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID;
			}
			if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getDecisionNode().isSuperTypeOf(domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID;
			}
			if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getExpressionActivityNode().isSuperTypeOf(
							domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID;
			}
			if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getFlowFinalNode().isSuperTypeOf(domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID;
			}
			if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getForkNode().isSuperTypeOf(domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID;
			}
			if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getInitialNode().isSuperTypeOf(domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID;
			}
			if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getJoinNode().isSuperTypeOf(domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID;
			}
			if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getSemaphore().isSuperTypeOf(domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID;
			}
			if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getMergeNode().isSuperTypeOf(domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID;
			}
			if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getStoryActionNode().isSuperTypeOf(domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeExpressionFigureCompartmentEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
					.getStringExpression()
					.isSuperTypeOf(domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID;
			}
			if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
					.getCallActionExpression().isSuperTypeOf(
							domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart.VISUAL_ID;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
					.getStringExpression()
					.isSuperTypeOf(domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID;
			}
			if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
					.getCallActionExpression().isSuperTypeOf(
							domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart.VISUAL_ID;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
					.getStoryPatternObject().isSuperTypeOf(
							domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
					.getStringExpression()
					.isSuperTypeOf(domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID;
			}
			if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
					.getCallActionExpression().isSuperTypeOf(
							domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart.VISUAL_ID;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
					.getAttributeAssignment().isSuperTypeOf(
							domainElement.eClass())) {
				return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
				.getModelID(containerView);
		if (!de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.MODEL_ID
				.equals(containerModelID)
				&& !"de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorID".equals(containerModelID)) { //$NON-NLS-1$
			return false;
		}
		int containerVisualID;
		if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.MODEL_ID
				.equals(containerModelID)) {
			containerVisualID = de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeReturnValueLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeExpressionFigureCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeSpecificationLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeImportsLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreTokenCountEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectNameLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectModifierLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectClassifierLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeExpressionFigureCompartmentEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeGuardConstraintLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkFeatureNameLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkPositionConstraintLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkFeatureNameLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkKeyLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueLinkLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeWeightEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeWeightEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID:
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
				.getActivityEdge().isSuperTypeOf(domainElement.eClass())) {
			return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID;
		}
		if (de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
				.getStoryPatternLink().isSuperTypeOf(domainElement.eClass())) {
			return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID;
		}
		if (de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
				.getStoryPatternContainmentLink().isSuperTypeOf(
						domainElement.eClass())) {
			return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID;
		}
		if (de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
				.getStoryPatternExpressionLink().isSuperTypeOf(
						domainElement.eClass())) {
			return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID;
		}
		if (de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
				.getMapEntryStoryPatternLink().isSuperTypeOf(
						domainElement.eClass())) {
			return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID;
		}
		if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
				.getReleaseSemaphoreEdge()
				.isSuperTypeOf(domainElement.eClass())) {
			return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID;
		}
		if (de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
				.getAcquireSemaphoreEdge()
				.isSuperTypeOf(domainElement.eClass())) {
			return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID;
		}
		if (de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
				.getLinkOrderConstraint().isSuperTypeOf(domainElement.eClass())) {
			return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(
			de.hpi.sam.storyDiagramEcore.Activity element) {
		return true;
	}

}
