package de.hpi.sam.storyDiagramEcore.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

/**
 * @generated
 */
public class LinkOrderConstraintReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	protected final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public LinkOrderConstraintReorientCommand(
			ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink && newEnd instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink target = getLink()
				.getSuccessorLink();
		if (!(getLink().eContainer() instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink container = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) getLink()
				.eContainer();
		return de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy
				.getLinkConstraints().canExistLinkOrderConstraint_4015(
						container, getLink(), getNewSource(), target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink && newEnd instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink source = getLink()
				.getPredecessorLink();
		if (!(getLink().eContainer() instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink container = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) getLink()
				.eContainer();
		return de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy
				.getLinkConstraints().canExistLinkOrderConstraint_4015(
						container, getLink(), source, getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setPredecessorLink(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setSuccessorLink(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint getLink() {
		return (de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink getOldSource() {
		return (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) oldEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink getNewSource() {
		return (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) newEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink getOldTarget() {
		return (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) oldEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink getNewTarget() {
		return (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) newEnd;
	}
}
