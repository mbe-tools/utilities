package de.hpi.sam.storyDiagramEcore.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

/**
 * @generated
 */
public class StoryPatternObjectStoryPatternObjectConstraintsCompartmentItemSemanticEditPolicy
		extends
		de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public StoryPatternObjectStoryPatternObjectConstraintsCompartmentItemSemanticEditPolicy() {
		super(
				de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3036 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.StringExpression3CreateCommand(
					req));
		}
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3037 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.CallActionExpression3CreateCommand(
					req));
		}
		return super.getCreateCommand(req);
	}

}
