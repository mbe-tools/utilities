package de.hpi.sam.storyDiagramEcore.diagram.figures;

import org.eclipse.draw2d.ConnectionLocator;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;

public class LinkCrossFigure extends PolylineConnectionEx
{
	public LinkCrossFigure()
	{
		setLineWidth(1);

		createContents();
		// setTargetDecoration(createTargetDecoration());
	}

	private void createContents()
	{
		CrossFigure crossFigure = new CrossFigure();

		Dimension size = new Dimension(20, 20);
		crossFigure.setMinimumSize(size);
		crossFigure.setMaximumSize(size);
		crossFigure.setPreferredSize(size);

		this.add(crossFigure, new ConnectionLocator(this, ConnectionLocator.MIDDLE));
	}

	// private RotatableDecoration createTargetDecoration()
	// {
	// PolylineDecoration df = new PolylineDecoration();
	// df.setLineWidth(1);
	// df.setForegroundColor(ColorConstants.black);
	// return df;
	// }
}
