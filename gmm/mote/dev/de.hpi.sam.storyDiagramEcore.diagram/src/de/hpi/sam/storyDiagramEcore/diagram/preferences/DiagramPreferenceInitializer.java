package de.hpi.sam.storyDiagramEcore.diagram.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * @generated
 */
public class DiagramPreferenceInitializer extends AbstractPreferenceInitializer {

	/**
	 * @generated
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = getPreferenceStore();
		de.hpi.sam.storyDiagramEcore.diagram.preferences.DiagramGeneralPreferencePage
				.initDefaults(store);
		de.hpi.sam.storyDiagramEcore.diagram.preferences.DiagramAppearancePreferencePage
				.initDefaults(store);
		de.hpi.sam.storyDiagramEcore.diagram.preferences.DiagramConnectionsPreferencePage
				.initDefaults(store);
		de.hpi.sam.storyDiagramEcore.diagram.preferences.DiagramPrintingPreferencePage
				.initDefaults(store);
		de.hpi.sam.storyDiagramEcore.diagram.preferences.DiagramRulersAndGridPreferencePage
				.initDefaults(store);

	}

	/**
	 * @generated
	 */
	protected IPreferenceStore getPreferenceStore() {
		return de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.getInstance().getPreferenceStore();
	}
}
