package de.hpi.sam.storyDiagramEcore.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

/**
 * @generated
 */
public class StoryDiagramEcoreNavigatorSorter extends ViewerSorter {

	/**
	 * @generated
	 */
	private static final int GROUP_CATEGORY = 7020;

	/**
	 * @generated
	 */
	private static final int SHORTCUTS_CATEGORY = 7019;

	/**
	 * @generated
	 */
	public int category(Object element) {
		if (element instanceof de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem) {
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem item = (de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem) element;
			if (item.getView().getEAnnotation("Shortcut") != null) { //$NON-NLS-1$
				return SHORTCUTS_CATEGORY;
			}
			return de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
