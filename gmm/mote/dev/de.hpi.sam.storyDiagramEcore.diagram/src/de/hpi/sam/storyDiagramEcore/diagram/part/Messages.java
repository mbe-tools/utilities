package de.hpi.sam.storyDiagramEcore.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String ActivityNodesTools1Group_title;

	/**
	 * @generated
	 */
	public static String StoryPatternTools2Group_title;

	/**
	 * @generated
	 */
	public static String SemaphoreTools3Group_title;

	/**
	 * @generated
	 */
	public static String InitialNode1CreationTool_title;

	/**
	 * @generated
	 */
	public static String InitialNode1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ActivityFinalNode2CreationTool_title;

	/**
	 * @generated
	 */
	public static String ActivityFinalNode2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String FlowFinalNode3CreationTool_title;

	/**
	 * @generated
	 */
	public static String FlowFinalNode3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String DecisionNode4CreationTool_title;

	/**
	 * @generated
	 */
	public static String DecisionNode4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String MergeNode5CreationTool_title;

	/**
	 * @generated
	 */
	public static String MergeNode5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ForkNode6CreationTool_title;

	/**
	 * @generated
	 */
	public static String ForkNode6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String JoinNode7CreationTool_title;

	/**
	 * @generated
	 */
	public static String JoinNode7CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ExpressionActivityNode8CreationTool_title;

	/**
	 * @generated
	 */
	public static String ExpressionActivityNode8CreationTool_desc;

	/**
	 * @generated
	 */
	public static String StoryActionNode9CreationTool_title;

	/**
	 * @generated
	 */
	public static String StoryActionNode9CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ActivityEdge11CreationTool_title;

	/**
	 * @generated
	 */
	public static String ActivityEdge11CreationTool_desc;

	/**
	 * @generated
	 */
	public static String StringExpression13CreationTool_title;

	/**
	 * @generated
	 */
	public static String StringExpression13CreationTool_desc;

	/**
	 * @generated
	 */
	public static String CallActionExpression14CreationTool_title;

	/**
	 * @generated
	 */
	public static String CallActionExpression14CreationTool_desc;

	/**
	 * @generated
	 */
	public static String StoryPatternObject1CreationTool_title;

	/**
	 * @generated
	 */
	public static String StoryPatternObject1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String MapEntryStoryPatternLink2CreationTool_title;

	/**
	 * @generated
	 */
	public static String MapEntryStoryPatternLink2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String MapEntryStoryPatternLinkValueLink3CreationTool_title;

	/**
	 * @generated
	 */
	public static String MapEntryStoryPatternLinkValueLink3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AttributeAssignment4CreationTool_title;

	/**
	 * @generated
	 */
	public static String AttributeAssignment4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String StoryPatternLink5CreationTool_title;

	/**
	 * @generated
	 */
	public static String StoryPatternLink5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String StoryPatternContainmentLink6CreationTool_title;

	/**
	 * @generated
	 */
	public static String StoryPatternContainmentLink6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String StoryPatternExpressionLink7CreationTool_title;

	/**
	 * @generated
	 */
	public static String StoryPatternExpressionLink7CreationTool_desc;

	/**
	 * @generated
	 */
	public static String LinkOrderConstraint8CreationTool_title;

	/**
	 * @generated
	 */
	public static String LinkOrderConstraint8CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Semaphore1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Semaphore1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ReleaseEdge2CreationTool_title;

	/**
	 * @generated
	 */
	public static String ReleaseEdge2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AcquireEdge3CreationTool_title;

	/**
	 * @generated
	 */
	public static String AcquireEdge3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ExpressionActivityNodeExpressionFigureCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String StoryActionNodeStoryActionNodeElementsCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ForkNode_2022_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ForkNode_2022_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StoryPatternObject_3035_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StoryPatternObject_3035_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Semaphore_2027_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_JoinNode_2024_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_JoinNode_2024_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StoryPatternLink_4007_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StoryPatternLink_4007_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StoryPatternLink_4007_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StoryPatternLink_4007_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StoryPatternExpressionLink_4009_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StoryPatternExpressionLink_4009_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MergeNode_2025_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MergeNode_2025_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_FlowFinalNode_2021_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_FlowFinalNode_2021_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AcquireSemaphoreEdge_4013_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AcquireSemaphoreEdge_4013_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ReleaseSemaphoreEdge_4012_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ReleaseSemaphoreEdge_4012_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ActivityEdge_4006_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ActivityEdge_4006_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ActivityEdge_4006_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StoryPatternContainmentLink_4008_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StoryPatternContainmentLink_4008_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StoryActionNode_2026_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StoryActionNode_2026_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MapEntryStoryPatternLink_4010_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MapEntryStoryPatternLink_4010_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MapEntryStoryPatternLink_4010_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Activity_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ExpressionActivityNode_2020_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ExpressionActivityNode_2020_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ActivityFinalNode_2018_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ActivityFinalNode_2018_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_InitialNode_2023_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_InitialNode_2023_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_DecisionNode_2019_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_DecisionNode_2019_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LinkOrderConstraint_4015_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LinkOrderConstraint_4015_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MapEntryStoryPatternLinkValueTarget_4011_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MapEntryStoryPatternLinkValueTarget_4011_source;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnexpectedValueType;

	/**
	 * @generated
	 */
	public static String AbstractParser_WrongStringConversion;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnknownLiteral;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String StoryDiagramEcoreModelingAssistantProviderMessage;

	// TODO: put accessor fields manually
}
