package de.hpi.sam.storyDiagramEcore.diagram.part;

import org.eclipse.gef.KeyHandler;
import org.eclipse.gef.KeyStroke;
import org.eclipse.gmf.runtime.diagram.ui.actions.ActionIds;
import org.eclipse.swt.SWT;

public class CustomStoryDiagramEcoreDiagramEditor extends StoryDiagramEcoreDiagramEditor
{
	@Override
	protected KeyHandler getKeyHandler()
	{
		KeyHandler keyHandler = super.getKeyHandler();

		keyHandler.put(KeyStroke.getPressed(SWT.DEL, 127, 0), this.getActionRegistry().getAction(ActionIds.ACTION_DELETE_FROM_MODEL));
		keyHandler.put(KeyStroke.getPressed(SWT.BS, 8, 0), this.getActionRegistry().getAction(ActionIds.ACTION_DELETE_FROM_MODEL));

		return keyHandler;
	}
}
