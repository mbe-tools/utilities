package de.hpi.sam.storyDiagramEcore.diagram.edit.parts;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;

/**
 * @generated
 */
public class StoryPatternLinkEditPart extends ConnectionNodeEditPart implements
		ITreeBranchEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 4007;

	/**
	 * @generated
	 */
	public StoryPatternLinkEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(
				EditPolicyRoles.SEMANTIC_ROLE,
				new de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryPatternLinkItemSemanticEditPolicy());
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkFeatureNameLabelEditPart) {
			((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkFeatureNameLabelEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureStoryPatternLinkFeatureNameLabel());
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkPositionConstraintLabelEditPart) {
			((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkPositionConstraintLabelEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureLinkPositionConstraintLabel());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, index);
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkFeatureNameLabelEditPart) {
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkPositionConstraintLabelEditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */

	protected Connection createConnectionFigure() {
		return new StoryPatternLinkFigureDescriptor();
	}

	/**
	 * @generated
	 */
	public StoryPatternLinkFigureDescriptor getPrimaryShape() {
		return (StoryPatternLinkFigureDescriptor) getFigure();
	}

	/**
	 * @generated
	 */
	public class StoryPatternLinkFigureDescriptor extends PolylineConnectionEx {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureStoryPatternLinkFeatureNameLabel;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureLinkPositionConstraintLabel;

		/**
		 * @generated
		 */
		public StoryPatternLinkFigureDescriptor() {
			this.setForegroundColor(ColorConstants.black);

			createContents();
			setTargetDecoration(createTargetDecoration());
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureStoryPatternLinkFeatureNameLabel = new WrappingLabel();
			fFigureStoryPatternLinkFeatureNameLabel.setText("");

			this.add(fFigureStoryPatternLinkFeatureNameLabel);

			fFigureLinkPositionConstraintLabel = new WrappingLabel();
			fFigureLinkPositionConstraintLabel.setText("");

			this.add(fFigureLinkPositionConstraintLabel);

		}

		/**
		 * @generated
		 */
		protected RotatableDecoration createTargetDecoration() {
			PolylineDecoration df = new PolylineDecoration();
			return df;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureStoryPatternLinkFeatureNameLabel() {
			return fFigureStoryPatternLinkFeatureNameLabel;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureLinkPositionConstraintLabel() {
			return fFigureLinkPositionConstraintLabel;
		}

	}

}
