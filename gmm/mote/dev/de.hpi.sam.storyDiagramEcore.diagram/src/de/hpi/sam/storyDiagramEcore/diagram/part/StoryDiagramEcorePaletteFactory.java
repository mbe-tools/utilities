package de.hpi.sam.storyDiagramEcore.diagram.part;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteSeparator;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

/**
 * @generated
 */
public class StoryDiagramEcorePaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createActivityNodesTools1Group());
		paletteRoot.add(createStoryPatternTools2Group());
		paletteRoot.add(createSemaphoreTools3Group());
	}

	/**
	 * Creates "Activity Nodes Tools" palette tool group
	 * 
	 * @generated
	 */
	private PaletteContainer createActivityNodesTools1Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.ActivityNodesTools1Group_title);
		paletteContainer.setId("createActivityNodesTools1Group"); //$NON-NLS-1$
		paletteContainer.add(createInitialNode1CreationTool());
		paletteContainer.add(createActivityFinalNode2CreationTool());
		paletteContainer.add(createFlowFinalNode3CreationTool());
		paletteContainer.add(createDecisionNode4CreationTool());
		paletteContainer.add(createMergeNode5CreationTool());
		paletteContainer.add(createForkNode6CreationTool());
		paletteContainer.add(createJoinNode7CreationTool());
		paletteContainer.add(createExpressionActivityNode8CreationTool());
		paletteContainer.add(createStoryActionNode9CreationTool());
		paletteContainer.add(new PaletteSeparator());
		paletteContainer.add(createActivityEdge11CreationTool());
		paletteContainer.add(new PaletteSeparator());
		paletteContainer.add(createStringExpression13CreationTool());
		paletteContainer.add(createCallActionExpression14CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Story Pattern Tools" palette tool group
	 * 
	 * @generated
	 */
	private PaletteContainer createStoryPatternTools2Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryPatternTools2Group_title);
		paletteContainer.setId("createStoryPatternTools2Group"); //$NON-NLS-1$
		paletteContainer.add(createStoryPatternObject1CreationTool());
		paletteContainer.add(createMapEntryStoryPatternLink2CreationTool());
		paletteContainer
				.add(createMapEntryStoryPatternLinkValueLink3CreationTool());
		paletteContainer.add(createAttributeAssignment4CreationTool());
		paletteContainer.add(createStoryPatternLink5CreationTool());
		paletteContainer.add(createStoryPatternContainmentLink6CreationTool());
		paletteContainer.add(createStoryPatternExpressionLink7CreationTool());
		paletteContainer.add(createLinkOrderConstraint8CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Semaphore Tools" palette tool group
	 * 
	 * @generated
	 */
	private PaletteContainer createSemaphoreTools3Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.SemaphoreTools3Group_title);
		paletteContainer.setId("createSemaphoreTools3Group"); //$NON-NLS-1$
		paletteContainer.add(createSemaphore1CreationTool());
		paletteContainer.add(createReleaseEdge2CreationTool());
		paletteContainer.add(createAcquireEdge3CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createInitialNode1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.InitialNode1CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.InitialNode1CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.InitialNode_2023));
		entry.setId("createInitialNode1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/InitialNode.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createActivityFinalNode2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.ActivityFinalNode2CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.ActivityFinalNode2CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityFinalNode_2018));
		entry.setId("createActivityFinalNode2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/ActivityFinalNode.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createFlowFinalNode3CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.FlowFinalNode3CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.FlowFinalNode3CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.FlowFinalNode_2021));
		entry.setId("createFlowFinalNode3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/FlowFinalNode.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createDecisionNode4CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.DecisionNode4CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.DecisionNode4CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.DecisionNode_2019));
		entry.setId("createDecisionNode4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/DecisionNode.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createMergeNode5CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.MergeNode5CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.MergeNode5CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MergeNode_2025));
		entry.setId("createMergeNode5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/MergeNode.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createForkNode6CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.ForkNode6CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.ForkNode6CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ForkNode_2022));
		entry.setId("createForkNode6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/ForkNode.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createJoinNode7CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.JoinNode7CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.JoinNode7CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.JoinNode_2024));
		entry.setId("createJoinNode7CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/JoinNode.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createExpressionActivityNode8CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.ExpressionActivityNode8CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.ExpressionActivityNode8CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ExpressionActivityNode_2020));
		entry.setId("createExpressionActivityNode8CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/CallActionNode.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createStoryActionNode9CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryActionNode9CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryActionNode9CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryActionNode_2026));
		entry.setId("createStoryActionNode9CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/StoryActionNode.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createActivityEdge11CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.ActivityEdge11CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.ActivityEdge11CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006));
		entry.setId("createActivityEdge11CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/ActivityEdge.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createStringExpression13CreationTool() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(3);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3031);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3033);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3036);
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StringExpression13CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StringExpression13CreationTool_desc,
				types);
		entry.setId("createStringExpression13CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes
				.getImageDescriptor(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3031));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCallActionExpression14CreationTool() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(3);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3032);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3034);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3037);
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.CallActionExpression14CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.CallActionExpression14CreationTool_desc,
				types);
		entry.setId("createCallActionExpression14CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes
				.getImageDescriptor(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3032));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createStoryPatternObject1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryPatternObject1CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryPatternObject1CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035));
		entry.setId("createStoryPatternObject1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/StoryPatternObject.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createMapEntryStoryPatternLink2CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.MapEntryStoryPatternLink2CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.MapEntryStoryPatternLink2CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010));
		entry.setId("createMapEntryStoryPatternLink2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/MapEntryStoryPatternLink.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createMapEntryStoryPatternLinkValueLink3CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.MapEntryStoryPatternLinkValueLink3CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.MapEntryStoryPatternLinkValueLink3CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLinkValueTarget_4011));
		entry.setId("createMapEntryStoryPatternLinkValueLink3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/MapEntryStoryPatternLink.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAttributeAssignment4CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.AttributeAssignment4CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.AttributeAssignment4CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.AttributeAssignment_3038));
		entry.setId("createAttributeAssignment4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/AttributeAssignment.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createStoryPatternLink5CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryPatternLink5CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryPatternLink5CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternLink_4007));
		entry.setId("createStoryPatternLink5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/StoryPatternLink.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createStoryPatternContainmentLink6CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryPatternContainmentLink6CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryPatternContainmentLink6CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternContainmentLink_4008));
		entry.setId("createStoryPatternContainmentLink6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/StoryPatternContainmentLink.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createStoryPatternExpressionLink7CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryPatternExpressionLink7CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.StoryPatternExpressionLink7CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternExpressionLink_4009));
		entry.setId("createStoryPatternExpressionLink7CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/StoryPatternExpressionLink.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createLinkOrderConstraint8CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.LinkOrderConstraint8CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.LinkOrderConstraint8CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.LinkOrderConstraint_4015));
		entry.setId("createLinkOrderConstraint8CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/LinkOrderConstraint.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createSemaphore1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.Semaphore1CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.Semaphore1CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.Semaphore_2027));
		entry.setId("createSemaphore1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes
				.getImageDescriptor(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.Semaphore_2027));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createReleaseEdge2CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.ReleaseEdge2CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.ReleaseEdge2CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ReleaseSemaphoreEdge_4012));
		entry.setId("createReleaseEdge2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes
				.getImageDescriptor(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ReleaseSemaphoreEdge_4012));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAcquireEdge3CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.AcquireEdge3CreationTool_title,
				de.hpi.sam.storyDiagramEcore.diagram.part.Messages.AcquireEdge3CreationTool_desc,
				Collections
						.singletonList(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.AcquireSemaphoreEdge_4013));
		entry.setId("createAcquireEdge3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes
				.getImageDescriptor(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.AcquireSemaphoreEdge_4013));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List<IElementType> elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List<IElementType> relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
