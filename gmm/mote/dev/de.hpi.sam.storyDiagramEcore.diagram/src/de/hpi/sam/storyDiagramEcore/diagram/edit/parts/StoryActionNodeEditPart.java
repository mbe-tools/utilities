package de.hpi.sam.storyDiagramEcore.diagram.edit.parts;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

/**
 * @generated
 */
public class StoryActionNodeEditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2026;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public StoryActionNodeEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy());
		super.createDefaultEditPolicies();
		installEditPolicy(
				EditPolicyRoles.SEMANTIC_ROLE,
				new de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryActionNodeItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		return primaryShape = new StoryActionNodeFigureDescriptor();
	}

	/**
	 * @generated
	 */
	public StoryActionNodeFigureDescriptor getPrimaryShape() {
		return (StoryActionNodeFigureDescriptor) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeNameEditPart) {
			((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeNameEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureStoryActionNodeTitleLabel());
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureStoryActionNodeConstraintsRectangle();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.add(((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart) childEditPart)
					.getFigure());
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureStoryActionNodeElementsRectangle();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.add(((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart) childEditPart)
					.getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeNameEditPart) {
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureStoryActionNodeConstraintsRectangle();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.remove(((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart) childEditPart)
					.getFigure());
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureStoryActionNodeElementsRectangle();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.remove(((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart) childEditPart)
					.getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		if (editPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart) {
			return getPrimaryShape()
					.getFigureStoryActionNodeConstraintsRectangle();
		}
		if (editPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart) {
			return getPrimaryShape()
					.getFigureStoryActionNodeElementsRectangle();
		}
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(40, 40);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane. Respects
	 * layout one may have set for generated figure.
	 * 
	 * @param nodeShape
	 *            instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
				.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeNameEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSource() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(1);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006);
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSourceAndTarget(
			IGraphicalEditPart targetEditPart) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMATypesForTarget(IElementType relationshipType) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (relationshipType == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityFinalNode_2018);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.DecisionNode_2019);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ExpressionActivityNode_2020);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.FlowFinalNode_2021);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ForkNode_2022);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.InitialNode_2023);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.JoinNode_2024);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MergeNode_2025);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryActionNode_2026);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnTarget() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(1);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006);
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMATypesForSource(IElementType relationshipType) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (relationshipType == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityFinalNode_2018);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.DecisionNode_2019);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ExpressionActivityNode_2020);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.FlowFinalNode_2021);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ForkNode_2022);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.InitialNode_2023);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.JoinNode_2024);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MergeNode_2025);
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryActionNode_2026);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public EditPart getTargetEditPart(Request request) {
		if (request instanceof CreateViewAndElementRequest) {
			CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request)
					.getViewAndElementDescriptor()
					.getCreateElementRequestAdapter();
			IElementType type = (IElementType) adapter
					.getAdapter(IElementType.class);
			if (type == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3033) {
				return getChildBySemanticHint(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
						.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart.VISUAL_ID));
			}
			if (type == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3034) {
				return getChildBySemanticHint(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
						.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart.VISUAL_ID));
			}
		}
		return super.getTargetEditPart(request);
	}

	/**
	 * @generated
	 */
	protected void handleNotificationEvent(Notification event) {
		if (event.getNotifier() == getModel()
				&& EcorePackage.eINSTANCE.getEModelElement_EAnnotations()
						.equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {
			super.handleNotificationEvent(event);
		}
	}

	/**
	 * @generated
	 */
	public class StoryActionNodeFigureDescriptor extends RoundedRectangle {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureStoryActionNodeTitleLabel;
		/**
		 * @generated
		 */
		private RoundedRectangle fFigureStoryActionNodeConstraintsRectangle;
		/**
		 * @generated
		 */
		private RoundedRectangle fFigureStoryActionNodeElementsRectangle;

		/**
		 * @generated
		 */
		public StoryActionNodeFigureDescriptor() {
			this.setLayoutManager(new StackLayout());
			this.setCornerDimensions(new Dimension(getMapMode().DPtoLP(16),
					getMapMode().DPtoLP(16)));
			this.setFill(false);
			this.setOutline(false);
			this.setMinimumSize(new Dimension(getMapMode().DPtoLP(100),
					getMapMode().DPtoLP(50)));
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			RectangleFigure storyActionNodeContainerBack0 = new RectangleFigure();
			storyActionNodeContainerBack0.setFill(false);
			storyActionNodeContainerBack0.setOutline(false);

			storyActionNodeContainerBack0.setBorder(new MarginBorder(
					getMapMode().DPtoLP(10), getMapMode().DPtoLP(10),
					getMapMode().DPtoLP(0), getMapMode().DPtoLP(0)));

			this.add(storyActionNodeContainerBack0);
			storyActionNodeContainerBack0.setLayoutManager(new StackLayout());

			RoundedRectangle storyActionNodeFigureBackRectangle1 = new RoundedRectangle();
			storyActionNodeFigureBackRectangle1
					.setCornerDimensions(new Dimension(getMapMode().DPtoLP(16),
							getMapMode().DPtoLP(16)));
			storyActionNodeFigureBackRectangle1
					.setForegroundColor(ColorConstants.black);
			storyActionNodeFigureBackRectangle1
					.setBackgroundColor(STORYACTIONNODEFIGUREBACKRECTANGLE1_BACK);

			storyActionNodeContainerBack0
					.add(storyActionNodeFigureBackRectangle1);

			RectangleFigure storyActionNodeContainerFront0 = new RectangleFigure();
			storyActionNodeContainerFront0.setFill(false);
			storyActionNodeContainerFront0.setOutline(false);

			storyActionNodeContainerFront0.setBorder(new MarginBorder(
					getMapMode().DPtoLP(0), getMapMode().DPtoLP(0),
					getMapMode().DPtoLP(10), getMapMode().DPtoLP(10)));

			this.add(storyActionNodeContainerFront0);
			storyActionNodeContainerFront0.setLayoutManager(new StackLayout());

			RoundedRectangle storyActionNodeFigureFrontRectangle1 = new RoundedRectangle();
			storyActionNodeFigureFrontRectangle1
					.setCornerDimensions(new Dimension(getMapMode().DPtoLP(16),
							getMapMode().DPtoLP(16)));
			storyActionNodeFigureFrontRectangle1
					.setForegroundColor(ColorConstants.black);
			storyActionNodeFigureFrontRectangle1
					.setBackgroundColor(STORYACTIONNODEFIGUREFRONTRECTANGLE1_BACK);

			storyActionNodeContainerFront0
					.add(storyActionNodeFigureFrontRectangle1);

			BorderLayout layoutStoryActionNodeFigureFrontRectangle1 = new BorderLayout();
			storyActionNodeFigureFrontRectangle1
					.setLayoutManager(layoutStoryActionNodeFigureFrontRectangle1);

			RoundedRectangle storyActionNodeFigureTitleRectangle2 = new RoundedRectangle();
			storyActionNodeFigureTitleRectangle2
					.setCornerDimensions(new Dimension(getMapMode().DPtoLP(16),
							getMapMode().DPtoLP(16)));
			storyActionNodeFigureTitleRectangle2.setFill(false);
			storyActionNodeFigureTitleRectangle2.setOutline(false);
			storyActionNodeFigureTitleRectangle2
					.setPreferredSize(new Dimension(getMapMode().DPtoLP(100),
							getMapMode().DPtoLP(30)));

			storyActionNodeFigureFrontRectangle1.add(
					storyActionNodeFigureTitleRectangle2, BorderLayout.TOP);

			GridLayout layoutStoryActionNodeFigureTitleRectangle2 = new GridLayout();
			layoutStoryActionNodeFigureTitleRectangle2.numColumns = 1;
			layoutStoryActionNodeFigureTitleRectangle2.makeColumnsEqualWidth = true;
			storyActionNodeFigureTitleRectangle2
					.setLayoutManager(layoutStoryActionNodeFigureTitleRectangle2);

			fFigureStoryActionNodeTitleLabel = new WrappingLabel();
			fFigureStoryActionNodeTitleLabel.setText("");

			fFigureStoryActionNodeTitleLabel
					.setFont(FFIGURESTORYACTIONNODETITLELABEL_FONT);

			GridData constraintFFigureStoryActionNodeTitleLabel = new GridData();
			constraintFFigureStoryActionNodeTitleLabel.verticalAlignment = GridData.CENTER;
			constraintFFigureStoryActionNodeTitleLabel.horizontalAlignment = GridData.CENTER;
			constraintFFigureStoryActionNodeTitleLabel.horizontalIndent = 0;
			constraintFFigureStoryActionNodeTitleLabel.horizontalSpan = 1;
			constraintFFigureStoryActionNodeTitleLabel.verticalSpan = 1;
			constraintFFigureStoryActionNodeTitleLabel.grabExcessHorizontalSpace = true;
			constraintFFigureStoryActionNodeTitleLabel.grabExcessVerticalSpace = false;
			storyActionNodeFigureTitleRectangle2.add(
					fFigureStoryActionNodeTitleLabel,
					constraintFFigureStoryActionNodeTitleLabel);

			RoundedRectangle storyActionNodeContentRectangle2 = new RoundedRectangle();
			storyActionNodeContentRectangle2.setCornerDimensions(new Dimension(
					getMapMode().DPtoLP(16), getMapMode().DPtoLP(16)));
			storyActionNodeContentRectangle2.setFill(false);
			storyActionNodeContentRectangle2.setOutline(false);

			storyActionNodeFigureFrontRectangle1.add(
					storyActionNodeContentRectangle2, BorderLayout.CENTER);

			BorderLayout layoutStoryActionNodeContentRectangle2 = new BorderLayout();
			storyActionNodeContentRectangle2
					.setLayoutManager(layoutStoryActionNodeContentRectangle2);

			fFigureStoryActionNodeConstraintsRectangle = new RoundedRectangle();
			fFigureStoryActionNodeConstraintsRectangle
					.setCornerDimensions(new Dimension(getMapMode().DPtoLP(16),
							getMapMode().DPtoLP(16)));
			fFigureStoryActionNodeConstraintsRectangle.setFill(false);
			fFigureStoryActionNodeConstraintsRectangle.setOutline(false);

			storyActionNodeContentRectangle2.add(
					fFigureStoryActionNodeConstraintsRectangle,
					BorderLayout.TOP);

			fFigureStoryActionNodeElementsRectangle = new RoundedRectangle();
			fFigureStoryActionNodeElementsRectangle
					.setCornerDimensions(new Dimension(getMapMode().DPtoLP(16),
							getMapMode().DPtoLP(16)));
			fFigureStoryActionNodeElementsRectangle.setFill(false);
			fFigureStoryActionNodeElementsRectangle.setOutline(false);

			storyActionNodeContentRectangle2.add(
					fFigureStoryActionNodeElementsRectangle,
					BorderLayout.CENTER);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureStoryActionNodeTitleLabel() {
			return fFigureStoryActionNodeTitleLabel;
		}

		/**
		 * @generated
		 */
		public RoundedRectangle getFigureStoryActionNodeConstraintsRectangle() {
			return fFigureStoryActionNodeConstraintsRectangle;
		}

		/**
		 * @generated
		 */
		public RoundedRectangle getFigureStoryActionNodeElementsRectangle() {
			return fFigureStoryActionNodeElementsRectangle;
		}

	}

	/**
	 * @generated
	 */
	static final Color STORYACTIONNODEFIGUREBACKRECTANGLE1_BACK = new Color(
			null, 252, 254, 204);

	/**
	 * @generated
	 */
	static final Color STORYACTIONNODEFIGUREFRONTRECTANGLE1_BACK = new Color(
			null, 252, 254, 204);

	/**
	 * @generated
	 */
	static final Font FFIGURESTORYACTIONNODETITLELABEL_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 10, SWT.BOLD);

}
