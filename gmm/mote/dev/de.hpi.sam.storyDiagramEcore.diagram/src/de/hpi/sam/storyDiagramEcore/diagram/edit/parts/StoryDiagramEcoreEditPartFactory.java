package de.hpi.sam.storyDiagramEcore.diagram.edit.parts;

import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;

/**
 * @generated
 */
public class StoryDiagramEcoreEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getVisualID(view)) {

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeReturnValueLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeReturnValueLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeNameEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeNameEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeSpecificationLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeSpecificationLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeImportsLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeImportsLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreTokenCountEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreTokenCountEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeNameEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeNameEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectNameLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectNameLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectModifierLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectModifierLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectClassifierLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectClassifierLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeExpressionFigureCompartmentEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeExpressionFigureCompartmentEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeGuardConstraintLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeGuardConstraintLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkFeatureNameLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkFeatureNameLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkPositionConstraintLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkPositionConstraintLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkFeatureNameLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkFeatureNameLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkKeyLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkKeyLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueLinkLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueLinkLabelEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeWeightEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeWeightEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeWeightEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeWeightEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart(
						view);

			case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintLabelEditPart.VISUAL_ID:
				return new de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintLabelEditPart(
						view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		if (source.getFigure() instanceof WrappingLabel)
			return new TextCellEditorLocator((WrappingLabel) source.getFigure());
		else {
			return new LabelCellEditorLocator((Label) source.getFigure());
		}
	}

	/**
	 * @generated
	 */
	static private class TextCellEditorLocator implements CellEditorLocator {

		/**
		 * @generated
		 */
		private WrappingLabel wrapLabel;

		/**
		 * @generated
		 */
		public TextCellEditorLocator(WrappingLabel wrapLabel) {
			this.wrapLabel = wrapLabel;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getWrapLabel() {
			return wrapLabel;
		}

		/**
		 * @generated
		 */
		public void relocate(CellEditor celleditor) {
			Text text = (Text) celleditor.getControl();
			Rectangle rect = getWrapLabel().getTextBounds().getCopy();
			getWrapLabel().translateToAbsolute(rect);
			if (!text.getFont().isDisposed()) {
				if (getWrapLabel().isTextWrapOn()
						&& getWrapLabel().getText().length() > 0) {
					rect.setSize(new Dimension(text.computeSize(rect.width,
							SWT.DEFAULT)));
				} else {
					int avr = FigureUtilities.getFontMetrics(text.getFont())
							.getAverageCharWidth();
					rect.setSize(new Dimension(text.computeSize(SWT.DEFAULT,
							SWT.DEFAULT)).expand(avr * 2, 0));
				}
			}
			if (!rect.equals(new Rectangle(text.getBounds()))) {
				text.setBounds(rect.x, rect.y, rect.width, rect.height);
			}
		}
	}

	/**
	 * @generated
	 */
	private static class LabelCellEditorLocator implements CellEditorLocator {

		/**
		 * @generated
		 */
		private Label label;

		/**
		 * @generated
		 */
		public LabelCellEditorLocator(Label label) {
			this.label = label;
		}

		/**
		 * @generated
		 */
		public Label getLabel() {
			return label;
		}

		/**
		 * @generated
		 */
		public void relocate(CellEditor celleditor) {
			Text text = (Text) celleditor.getControl();
			Rectangle rect = getLabel().getTextBounds().getCopy();
			getLabel().translateToAbsolute(rect);
			if (!text.getFont().isDisposed()) {
				int avr = FigureUtilities.getFontMetrics(text.getFont())
						.getAverageCharWidth();
				rect.setSize(new Dimension(text.computeSize(SWT.DEFAULT,
						SWT.DEFAULT)).expand(avr * 2, 0));
			}
			if (!rect.equals(new Rectangle(text.getBounds()))) {
				text.setBounds(rect.x, rect.y, rect.width, rect.height);
			}
		}
	}
}
