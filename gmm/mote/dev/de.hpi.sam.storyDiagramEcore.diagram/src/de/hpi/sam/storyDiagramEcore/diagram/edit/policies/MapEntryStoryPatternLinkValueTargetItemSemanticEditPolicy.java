package de.hpi.sam.storyDiagramEcore.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;

/**
 * @generated
 */
public class MapEntryStoryPatternLinkValueTargetItemSemanticEditPolicy
		extends
		de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public MapEntryStoryPatternLinkValueTargetItemSemanticEditPolicy() {
		super(
				de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLinkValueTarget_4011);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyReferenceCommand(DestroyReferenceRequest req) {
		return getGEFWrapper(new DestroyReferenceCommand(req));
	}

}
