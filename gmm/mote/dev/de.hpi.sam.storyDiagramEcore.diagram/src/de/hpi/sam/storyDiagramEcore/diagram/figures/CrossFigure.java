package de.hpi.sam.storyDiagramEcore.diagram.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;

public class CrossFigure extends RectangleFigure
{
	@Override
	protected void fillShape(Graphics graphics)
	{
		// Do nothing
	}

	@Override
	protected void outlineShape(Graphics graphics)
	{
		graphics.pushState();

		Rectangle r = getBounds();

		graphics.setLineStyle(SWT.LINE_SOLID);
		graphics.setLineWidth(2);
		graphics.setForegroundColor(ColorConstants.black);

		graphics.drawLine(r.x, r.y, r.x + r.width, r.y + r.height);
		graphics.drawLine(r.x + r.width, r.y, r.x, r.y + r.height);

		graphics.popState();
	}
}
