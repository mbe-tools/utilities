package de.hpi.sam.storyDiagramEcore.diagram.edit.parts;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

/**
 * @generated
 */
public class StoryPatternObjectEditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 3035;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public StoryPatternObjectEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy());
		super.createDefaultEditPolicies();
		installEditPolicy(
				EditPolicyRoles.SEMANTIC_ROLE,
				new de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryPatternObjectItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		return primaryShape = new StoryPatternObjectFigureDescriptor();
	}

	/**
	 * @generated
	 */
	public StoryPatternObjectFigureDescriptor getPrimaryShape() {
		return (StoryPatternObjectFigureDescriptor) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectNameLabelEditPart) {
			((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectNameLabelEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureStoryPatternObjectNameLabel());
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectModifierLabelEditPart) {
			((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectModifierLabelEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureStoryPatternObjectModifierLabel());
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectClassifierLabelEditPart) {
			((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectClassifierLabelEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureStoryPatternObjectClassifierLabel());
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureStoryPatternObjectConstraintsRectangle();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.add(((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart) childEditPart)
					.getFigure());
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureStoryPatternObjectAttributeAssignmentsRectangle();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.add(((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart) childEditPart)
					.getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectNameLabelEditPart) {
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectModifierLabelEditPart) {
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectClassifierLabelEditPart) {
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureStoryPatternObjectConstraintsRectangle();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.remove(((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart) childEditPart)
					.getFigure());
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureStoryPatternObjectAttributeAssignmentsRectangle();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.remove(((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart) childEditPart)
					.getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		if (editPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart) {
			return getPrimaryShape()
					.getFigureStoryPatternObjectConstraintsRectangle();
		}
		if (editPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart) {
			return getPrimaryShape()
					.getFigureStoryPatternObjectAttributeAssignmentsRectangle();
		}
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(40, 40);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane. Respects
	 * layout one may have set for generated figure.
	 * 
	 * @param nodeShape
	 *            instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
				.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectNameLabelEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSource() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(4);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternLink_4007);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternContainmentLink_4008);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternExpressionLink_4009);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010);
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSourceAndTarget(
			IGraphicalEditPart targetEditPart) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternLink_4007);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternContainmentLink_4008);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternExpressionLink_4009);
		}
		if (targetEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMATypesForTarget(IElementType relationshipType) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (relationshipType == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternLink_4007) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035);
		} else if (relationshipType == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternContainmentLink_4008) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035);
		} else if (relationshipType == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternExpressionLink_4009) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035);
		} else if (relationshipType == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnTarget() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(4);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternLink_4007);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternContainmentLink_4008);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternExpressionLink_4009);
		types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010);
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMATypesForSource(IElementType relationshipType) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (relationshipType == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternLink_4007) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035);
		} else if (relationshipType == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternContainmentLink_4008) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035);
		} else if (relationshipType == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternExpressionLink_4009) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035);
		} else if (relationshipType == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010) {
			types.add(de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public EditPart getTargetEditPart(Request request) {
		if (request instanceof CreateViewAndElementRequest) {
			CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request)
					.getViewAndElementDescriptor()
					.getCreateElementRequestAdapter();
			IElementType type = (IElementType) adapter
					.getAdapter(IElementType.class);
			if (type == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3036) {
				return getChildBySemanticHint(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
						.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart.VISUAL_ID));
			}
			if (type == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3037) {
				return getChildBySemanticHint(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
						.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart.VISUAL_ID));
			}
			if (type == de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.AttributeAssignment_3038) {
				return getChildBySemanticHint(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
						.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID));
			}
		}
		return super.getTargetEditPart(request);
	}

	/**
	 * @generated
	 */
	public class StoryPatternObjectFigureDescriptor extends RectangleFigure {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureStoryPatternObjectNameLabel;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureStoryPatternObjectModifierLabel;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureStoryPatternObjectClassifierLabel;
		/**
		 * @generated
		 */
		private RectangleFigure fFigureStoryPatternObjectConstraintsRectangle;
		/**
		 * @generated
		 */
		private RectangleFigure fFigureStoryPatternObjectAttributeAssignmentsRectangle;

		/**
		 * @generated
		 */
		public StoryPatternObjectFigureDescriptor() {
			this.setLayoutManager(new StackLayout());
			this.setFill(false);
			this.setOutline(false);
			this.setMinimumSize(new Dimension(getMapMode().DPtoLP(0),
					getMapMode().DPtoLP(0)));
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			RectangleFigure storyPatternObjectContainerRectangle0 = new RectangleFigure();
			storyPatternObjectContainerRectangle0.setFill(false);
			storyPatternObjectContainerRectangle0.setOutline(false);

			storyPatternObjectContainerRectangle0.setBorder(new MarginBorder(
					getMapMode().DPtoLP(0), getMapMode().DPtoLP(0),
					getMapMode().DPtoLP(0), getMapMode().DPtoLP(0)));

			this.add(storyPatternObjectContainerRectangle0);
			storyPatternObjectContainerRectangle0
					.setLayoutManager(new StackLayout());

			RectangleFigure storyPatternObjectRectangle1 = new RectangleFigure();
			storyPatternObjectRectangle1
					.setForegroundColor(ColorConstants.black);

			storyPatternObjectContainerRectangle0
					.add(storyPatternObjectRectangle1);

			BorderLayout layoutStoryPatternObjectRectangle1 = new BorderLayout();
			storyPatternObjectRectangle1
					.setLayoutManager(layoutStoryPatternObjectRectangle1);

			RectangleFigure storyPatternObjectFigureNameRectangle2 = new RectangleFigure();

			storyPatternObjectRectangle1.add(
					storyPatternObjectFigureNameRectangle2, BorderLayout.TOP);

			GridLayout layoutStoryPatternObjectFigureNameRectangle2 = new GridLayout();
			layoutStoryPatternObjectFigureNameRectangle2.numColumns = 1;
			layoutStoryPatternObjectFigureNameRectangle2.makeColumnsEqualWidth = true;
			storyPatternObjectFigureNameRectangle2
					.setLayoutManager(layoutStoryPatternObjectFigureNameRectangle2);

			fFigureStoryPatternObjectModifierLabel = new WrappingLabel();
			fFigureStoryPatternObjectModifierLabel.setText("");

			fFigureStoryPatternObjectModifierLabel
					.setFont(FFIGURESTORYPATTERNOBJECTMODIFIERLABEL_FONT);

			GridData constraintFFigureStoryPatternObjectModifierLabel = new GridData();
			constraintFFigureStoryPatternObjectModifierLabel.verticalAlignment = GridData.BEGINNING;
			constraintFFigureStoryPatternObjectModifierLabel.horizontalAlignment = GridData.CENTER;
			constraintFFigureStoryPatternObjectModifierLabel.horizontalIndent = 0;
			constraintFFigureStoryPatternObjectModifierLabel.horizontalSpan = 1;
			constraintFFigureStoryPatternObjectModifierLabel.verticalSpan = 1;
			constraintFFigureStoryPatternObjectModifierLabel.grabExcessHorizontalSpace = true;
			constraintFFigureStoryPatternObjectModifierLabel.grabExcessVerticalSpace = false;
			storyPatternObjectFigureNameRectangle2.add(
					fFigureStoryPatternObjectModifierLabel,
					constraintFFigureStoryPatternObjectModifierLabel);

			fFigureStoryPatternObjectNameLabel = new WrappingLabel();
			fFigureStoryPatternObjectNameLabel.setText("");

			fFigureStoryPatternObjectNameLabel
					.setFont(FFIGURESTORYPATTERNOBJECTNAMELABEL_FONT);

			GridData constraintFFigureStoryPatternObjectNameLabel = new GridData();
			constraintFFigureStoryPatternObjectNameLabel.verticalAlignment = GridData.CENTER;
			constraintFFigureStoryPatternObjectNameLabel.horizontalAlignment = GridData.CENTER;
			constraintFFigureStoryPatternObjectNameLabel.horizontalIndent = 0;
			constraintFFigureStoryPatternObjectNameLabel.horizontalSpan = 1;
			constraintFFigureStoryPatternObjectNameLabel.verticalSpan = 1;
			constraintFFigureStoryPatternObjectNameLabel.grabExcessHorizontalSpace = true;
			constraintFFigureStoryPatternObjectNameLabel.grabExcessVerticalSpace = false;
			storyPatternObjectFigureNameRectangle2.add(
					fFigureStoryPatternObjectNameLabel,
					constraintFFigureStoryPatternObjectNameLabel);

			fFigureStoryPatternObjectClassifierLabel = new WrappingLabel();
			fFigureStoryPatternObjectClassifierLabel.setText("");

			fFigureStoryPatternObjectClassifierLabel
					.setFont(FFIGURESTORYPATTERNOBJECTCLASSIFIERLABEL_FONT);

			GridData constraintFFigureStoryPatternObjectClassifierLabel = new GridData();
			constraintFFigureStoryPatternObjectClassifierLabel.verticalAlignment = GridData.END;
			constraintFFigureStoryPatternObjectClassifierLabel.horizontalAlignment = GridData.CENTER;
			constraintFFigureStoryPatternObjectClassifierLabel.horizontalIndent = 0;
			constraintFFigureStoryPatternObjectClassifierLabel.horizontalSpan = 1;
			constraintFFigureStoryPatternObjectClassifierLabel.verticalSpan = 1;
			constraintFFigureStoryPatternObjectClassifierLabel.grabExcessHorizontalSpace = true;
			constraintFFigureStoryPatternObjectClassifierLabel.grabExcessVerticalSpace = false;
			storyPatternObjectFigureNameRectangle2.add(
					fFigureStoryPatternObjectClassifierLabel,
					constraintFFigureStoryPatternObjectClassifierLabel);

			RectangleFigure storyPatternObjectFigureBodyRectangle2 = new RectangleFigure();
			storyPatternObjectFigureBodyRectangle2.setFill(false);
			storyPatternObjectFigureBodyRectangle2.setOutline(false);

			storyPatternObjectRectangle1
					.add(storyPatternObjectFigureBodyRectangle2,
							BorderLayout.CENTER);

			BorderLayout layoutStoryPatternObjectFigureBodyRectangle2 = new BorderLayout();
			storyPatternObjectFigureBodyRectangle2
					.setLayoutManager(layoutStoryPatternObjectFigureBodyRectangle2);

			fFigureStoryPatternObjectConstraintsRectangle = new RectangleFigure();
			fFigureStoryPatternObjectConstraintsRectangle.setFill(false);
			fFigureStoryPatternObjectConstraintsRectangle.setOutline(false);

			storyPatternObjectFigureBodyRectangle2.add(
					fFigureStoryPatternObjectConstraintsRectangle,
					BorderLayout.TOP);

			fFigureStoryPatternObjectAttributeAssignmentsRectangle = new RectangleFigure();
			fFigureStoryPatternObjectAttributeAssignmentsRectangle
					.setFill(false);
			fFigureStoryPatternObjectAttributeAssignmentsRectangle
					.setOutline(false);

			storyPatternObjectFigureBodyRectangle2.add(
					fFigureStoryPatternObjectAttributeAssignmentsRectangle,
					BorderLayout.CENTER);

			RectangleFigure crossContainerRectangle0 = new RectangleFigure();
			crossContainerRectangle0.setFill(false);
			crossContainerRectangle0.setOutline(false);

			crossContainerRectangle0.setBorder(new MarginBorder(getMapMode()
					.DPtoLP(0), getMapMode().DPtoLP(0), getMapMode().DPtoLP(0),
					getMapMode().DPtoLP(0)));

			this.add(crossContainerRectangle0);
			crossContainerRectangle0.setLayoutManager(new StackLayout());

			de.hpi.sam.storyDiagramEcore.diagram.figures.CrossFigure crossFigure1 = new de.hpi.sam.storyDiagramEcore.diagram.figures.CrossFigure();

			crossContainerRectangle0.add(crossFigure1);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureStoryPatternObjectNameLabel() {
			return fFigureStoryPatternObjectNameLabel;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureStoryPatternObjectModifierLabel() {
			return fFigureStoryPatternObjectModifierLabel;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureStoryPatternObjectClassifierLabel() {
			return fFigureStoryPatternObjectClassifierLabel;
		}

		/**
		 * @generated
		 */
		public RectangleFigure getFigureStoryPatternObjectConstraintsRectangle() {
			return fFigureStoryPatternObjectConstraintsRectangle;
		}

		/**
		 * @generated
		 */
		public RectangleFigure getFigureStoryPatternObjectAttributeAssignmentsRectangle() {
			return fFigureStoryPatternObjectAttributeAssignmentsRectangle;
		}

	}

	/**
	 * @generated
	 */
	static final Font FFIGURESTORYPATTERNOBJECTMODIFIERLABEL_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.ITALIC);

	/**
	 * @generated
	 */
	static final Font FFIGURESTORYPATTERNOBJECTNAMELABEL_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.BOLD);

	/**
	 * @generated
	 */
	static final Font FFIGURESTORYPATTERNOBJECTCLASSIFIERLABEL_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.BOLD);

}
