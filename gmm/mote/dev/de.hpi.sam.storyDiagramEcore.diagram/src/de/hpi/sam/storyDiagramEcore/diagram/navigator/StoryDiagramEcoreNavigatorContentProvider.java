package de.hpi.sam.storyDiagramEcore.diagram.navigator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.emf.core.GMFEditingDomainFactory;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonContentProvider;

/**
 * @generated
 */
public class StoryDiagramEcoreNavigatorContentProvider implements
		ICommonContentProvider {

	/**
	 * @generated
	 */
	private static final Object[] EMPTY_ARRAY = new Object[0];

	/**
	 * @generated
	 */
	private Viewer myViewer;

	/**
	 * @generated
	 */
	private AdapterFactoryEditingDomain myEditingDomain;

	/**
	 * @generated
	 */
	private WorkspaceSynchronizer myWorkspaceSynchronizer;

	/**
	 * @generated
	 */
	private Runnable myViewerRefreshRunnable;

	/**
	 * @generated
	 */
	@SuppressWarnings({ "unchecked", "serial", "rawtypes" })
	public StoryDiagramEcoreNavigatorContentProvider() {
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		myEditingDomain = (AdapterFactoryEditingDomain) editingDomain;
		myEditingDomain.setResourceToReadOnlyMap(new HashMap() {
			public Object get(Object key) {
				if (!containsKey(key)) {
					put(key, Boolean.TRUE);
				}
				return super.get(key);
			}
		});
		myViewerRefreshRunnable = new Runnable() {
			public void run() {
				if (myViewer != null) {
					myViewer.refresh();
				}
			}
		};
		myWorkspaceSynchronizer = new WorkspaceSynchronizer(editingDomain,
				new WorkspaceSynchronizer.Delegate() {
					public void dispose() {
					}

					public boolean handleResourceChanged(final Resource resource) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}

					public boolean handleResourceDeleted(Resource resource) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}

					public boolean handleResourceMoved(Resource resource,
							final URI newURI) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}
				});
	}

	/**
	 * @generated
	 */
	public void dispose() {
		myWorkspaceSynchronizer.dispose();
		myWorkspaceSynchronizer = null;
		myViewerRefreshRunnable = null;
		myViewer = null;
		unloadAllResources();
		((TransactionalEditingDomain) myEditingDomain).dispose();
		myEditingDomain = null;
	}

	/**
	 * @generated
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		myViewer = viewer;
	}

	/**
	 * @generated
	 */
	void unloadAllResources() {
		for (Resource nextResource : myEditingDomain.getResourceSet()
				.getResources()) {
			nextResource.unload();
		}
	}

	/**
	 * @generated
	 */
	void asyncRefresh() {
		if (myViewer != null && !myViewer.getControl().isDisposed()) {
			myViewer.getControl().getDisplay()
					.asyncExec(myViewerRefreshRunnable);
		}
	}

	/**
	 * @generated
	 */
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof IFile) {
			IFile file = (IFile) parentElement;
			URI fileURI = URI.createPlatformResourceURI(file.getFullPath()
					.toString(), true);
			Resource resource = myEditingDomain.getResourceSet().getResource(
					fileURI, true);
			ArrayList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem> result = new ArrayList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem>();
			ArrayList<View> topViews = new ArrayList<View>(resource
					.getContents().size());
			for (EObject o : resource.getContents()) {
				if (o instanceof View) {
					topViews.add((View) o);
				}
			}
			result.addAll(createNavigatorItems(
					selectViewsByType(
							topViews,
							de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.MODEL_ID),
					file, false));
			return result.toArray();
		}

		if (parentElement instanceof de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup) {
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup group = (de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup) parentElement;
			return group.getChildren();
		}

		if (parentElement instanceof de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem) {
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem navigatorItem = (de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem) parentElement;
			if (navigatorItem.isLeaf() || !isOwnView(navigatorItem.getView())) {
				return EMPTY_ARRAY;
			}
			return getViewChildren(navigatorItem.getView(), parentElement);
		}

		/*
		 * Due to plugin.xml restrictions this code will be called only for views representing
		 * shortcuts to this diagram elements created on other diagrams. 
		 */
		if (parentElement instanceof IAdaptable) {
			View view = (View) ((IAdaptable) parentElement)
					.getAdapter(View.class);
			if (view != null) {
				return getViewChildren(view, parentElement);
			}
		}

		return EMPTY_ARRAY;
	}

	/**
	 * @generated
	 */
	private Object[] getViewChildren(View view, Object parentElement) {
		switch (de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
				.getVisualID(view)) {

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup target = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_MapEntryStoryPatternLink_4010_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup source = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_MapEntryStoryPatternLink_4010_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_MapEntryStoryPatternLink_4010_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Node sv = (Node) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup incominglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_FlowFinalNode_2021_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_FlowFinalNode_2021_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Node sv = (Node) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup incominglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_ActivityFinalNode_2018_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_ActivityFinalNode_2018_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Node sv = (Node) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup incominglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_ForkNode_2022_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_ForkNode_2022_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Node sv = (Node) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup incominglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_MergeNode_2025_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_MergeNode_2025_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Node sv = (Node) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_Semaphore_2027_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Node sv = (Node) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup incominglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_ExpressionActivityNode_2020_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_ExpressionActivityNode_2020_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeExpressionFigureCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeExpressionFigureCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Node sv = (Node) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup incominglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_InitialNode_2023_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_InitialNode_2023_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup target = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_StoryPatternExpressionLink_4009_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup source = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_StoryPatternExpressionLink_4009_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup target = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_StoryPatternLink_4007_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup source = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_StoryPatternLink_4007_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup incominglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_StoryPatternLink_4007_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_StoryPatternLink_4007_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Node sv = (Node) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup incominglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_JoinNode_2024_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_JoinNode_2024_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup target = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_ReleaseSemaphoreEdge_4012_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup source = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_ReleaseSemaphoreEdge_4012_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			result.addAll(getForeignShortcuts((Diagram) view, parentElement));
			Diagram sv = (Diagram) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup links = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_Activity_1000_links,
					"icons/linksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			if (!links.isEmpty()) {
				result.add(links);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup target = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_ActivityEdge_4006_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup source = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_ActivityEdge_4006_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup incominglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_ActivityEdge_4006_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Node sv = (Node) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup incominglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_StoryActionNode_2026_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_StoryActionNode_2026_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup target = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_LinkOrderConstraint_4015_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup source = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_LinkOrderConstraint_4015_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Node sv = (Node) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup incominglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_DecisionNode_2019_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_DecisionNode_2019_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup target = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_AcquireSemaphoreEdge_4013_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup source = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_AcquireSemaphoreEdge_4013_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Node sv = (Node) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup incominglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_StoryPatternObject_3035_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup outgoinglinks = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_StoryPatternObject_3035_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup target = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_MapEntryStoryPatternLinkValueTarget_4011_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup source = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_MapEntryStoryPatternLinkValueTarget_4011_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID: {
			LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup target = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_StoryPatternContainmentLink_4008_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup source = new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup(
					de.hpi.sam.storyDiagramEcore.diagram.part.Messages.NavigatorGroupName_StoryPatternContainmentLink_4008_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
							.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}
		}
		return EMPTY_ARRAY;
	}

	/**
	 * @generated
	 */
	private Collection<View> getLinksSourceByType(Collection<Edge> edges,
			String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (Edge nextEdge : edges) {
			View nextEdgeSource = nextEdge.getSource();
			if (type.equals(nextEdgeSource.getType())
					&& isOwnView(nextEdgeSource)) {
				result.add(nextEdgeSource);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getLinksTargetByType(Collection<Edge> edges,
			String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (Edge nextEdge : edges) {
			View nextEdgeTarget = nextEdge.getTarget();
			if (type.equals(nextEdgeTarget.getType())
					&& isOwnView(nextEdgeTarget)) {
				result.add(nextEdgeTarget);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getOutgoingLinksByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getSourceEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getIncomingLinksByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getTargetEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getChildrenByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getChildren(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getDiagramLinksByType(
			Collection<Diagram> diagrams, String type) {
		ArrayList<View> result = new ArrayList<View>();
		for (Diagram nextDiagram : diagrams) {
			result.addAll(selectViewsByType(nextDiagram.getEdges(), type));
		}
		return result;
	}

	// TODO refactor as static method
	/**
	 * @generated
	 */
	private Collection<View> selectViewsByType(Collection<View> views,
			String type) {
		ArrayList<View> result = new ArrayList<View>();
		for (View nextView : views) {
			if (type.equals(nextView.getType()) && isOwnView(nextView)) {
				result.add(nextView);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.MODEL_ID
				.equals(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
						.getModelID(view));
	}

	/**
	 * @generated
	 */
	private Collection<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem> createNavigatorItems(
			Collection<View> views, Object parent, boolean isLeafs) {
		ArrayList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem> result = new ArrayList<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem>(
				views.size());
		for (View nextView : views) {
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem(
					nextView, parent, isLeafs));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem> getForeignShortcuts(
			Diagram diagram, Object parent) {
		LinkedList<View> result = new LinkedList<View>();
		for (Iterator<View> it = diagram.getChildren().iterator(); it.hasNext();) {
			View nextView = it.next();
			if (!isOwnView(nextView)
					&& nextView.getEAnnotation("Shortcut") != null) { //$NON-NLS-1$
				result.add(nextView);
			}
		}
		return createNavigatorItems(result, parent, false);
	}

	/**
	 * @generated
	 */
	public Object getParent(Object element) {
		if (element instanceof de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem) {
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem abstractNavigatorItem = (de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreAbstractNavigatorItem) element;
			return abstractNavigatorItem.getParent();
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean hasChildren(Object element) {
		return element instanceof IFile || getChildren(element).length > 0;
	}

}
