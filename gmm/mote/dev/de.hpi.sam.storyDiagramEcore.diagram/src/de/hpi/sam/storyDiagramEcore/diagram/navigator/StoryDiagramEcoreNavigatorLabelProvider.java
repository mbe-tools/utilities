package de.hpi.sam.storyDiagramEcore.diagram.navigator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.ui.services.parser.CommonParserHint;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

/**
 * @generated
 */
public class StoryDiagramEcoreNavigatorLabelProvider extends LabelProvider
		implements ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem
				&& !isOwnView(((de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem) element)
						.getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup) {
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup group = (de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup) element;
			return de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().getBundledImage(group.getIcon());
		}

		if (element instanceof de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem) {
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem navigatorItem = (de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getImage(view);
			}
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
				.getVisualID(view)) {
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de/hpi/sam/storyDiagramEcore/sdm.ecore?MapEntryStoryPatternLink", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?FlowFinalNode", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.FlowFinalNode_2021); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?ActivityFinalNode", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityFinalNode_2018); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?CallActionExpression", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3034); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?ForkNode", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ForkNode_2022); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?MergeNode", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MergeNode_2025); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?Semaphore", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.Semaphore_2027); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?ExpressionActivityNode", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ExpressionActivityNode_2020); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?StringExpression", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3036); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?InitialNode", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.InitialNode_2023); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de/hpi/sam/storyDiagramEcore/sdm.ecore?StoryPatternExpressionLink", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternExpressionLink_4009); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?CallActionExpression", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3037); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de/hpi/sam/storyDiagramEcore/sdm.ecore?StoryPatternLink", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternLink_4007); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?JoinNode", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.JoinNode_2024); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?ReleaseSemaphoreEdge", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ReleaseSemaphoreEdge_4012); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://de/hpi/sam/storyDiagramEcore.ecore?Activity", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.Activity_1000); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?ActivityEdge", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?StoryActionNode", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryActionNode_2026); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de/hpi/sam/storyDiagramEcore/sdm.ecore?LinkOrderConstraint", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.LinkOrderConstraint_4015); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?CallActionExpression", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3032); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?StringExpression", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3033); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?DecisionNode", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.DecisionNode_2019); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?StringExpression", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3031); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de/hpi/sam/storyDiagramEcore/nodes.ecore?AcquireSemaphoreEdge", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.AcquireSemaphoreEdge_4013); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/sdm.ecore?StoryPatternObject", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/sdm.ecore?AttributeAssignment", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.AttributeAssignment_3038); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de/hpi/sam/storyDiagramEcore/sdm.ecore?MapEntryStoryPatternLink?valueTarget", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLinkValueTarget_4011); //$NON-NLS-1$
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://de/hpi/sam/storyDiagramEcore/sdm.ecore?StoryPatternContainmentLink", de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternContainmentLink_4008); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
				.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null
				&& elementType != null
				&& de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes
						.isKnownElementType(elementType)) {
			image = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes
					.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup) {
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup group = (de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem) {
			de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem navigatorItem = (de.hpi.sam.storyDiagramEcore.diagram.navigator.StoryDiagramEcoreNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getText(view);
			}
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
				.getVisualID(view)) {
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID:
			return getMapEntryStoryPatternLink_4010Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID:
			return getFlowFinalNode_2021Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID:
			return getActivityFinalNode_2018Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart.VISUAL_ID:
			return getCallActionExpression_3034Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID:
			return getForkNode_2022Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID:
			return getMergeNode_2025Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID:
			return getSemaphore_2027Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID:
			return getExpressionActivityNode_2020Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID:
			return getStringExpression_3036Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID:
			return getInitialNode_2023Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID:
			return getStoryPatternExpressionLink_4009Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart.VISUAL_ID:
			return getCallActionExpression_3037Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID:
			return getStoryPatternLink_4007Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID:
			return getJoinNode_2024Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID:
			return getReleaseSemaphoreEdge_4012Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID:
			return getActivity_1000Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID:
			return getActivityEdge_4006Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID:
			return getStoryActionNode_2026Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID:
			return getLinkOrderConstraint_4015Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart.VISUAL_ID:
			return getCallActionExpression_3032Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID:
			return getStringExpression_3033Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID:
			return getDecisionNode_2019Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID:
			return getStringExpression_3031Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID:
			return getAcquireSemaphoreEdge_4013Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID:
			return getStoryPatternObject_3035Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart.VISUAL_ID:
			return getAttributeAssignment_3038Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart.VISUAL_ID:
			return getMapEntryStoryPatternLinkValueTarget_4011Text(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID:
			return getStoryPatternContainmentLink_4008Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getForkNode_2022Text(View view) {
		de.hpi.sam.storyDiagramEcore.nodes.ForkNode domainModelElement = (de.hpi.sam.storyDiagramEcore.nodes.ForkNode) view
				.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance()
					.logError(
							"No domain element for view with visualID = " + 2022); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getStoryPatternObject_3035Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectNameLabelEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 5026); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getStringExpression_3031Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3031,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 3031); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSemaphore_2027Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.Semaphore_2027,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreTokenCountEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 5031); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getAttributeAssignment_3038Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.AttributeAssignment_3038,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 3038); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getJoinNode_2024Text(View view) {
		de.hpi.sam.storyDiagramEcore.nodes.JoinNode domainModelElement = (de.hpi.sam.storyDiagramEcore.nodes.JoinNode) view
				.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance()
					.logError(
							"No domain element for view with visualID = " + 2024); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getStoryPatternLink_4007Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternLink_4007,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkFeatureNameLabelEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 6010); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getStringExpression_3033Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3033,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 3033); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getStoryPatternExpressionLink_4009Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternExpressionLink_4009,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkLabelEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 6014); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getStringExpression_3036Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StringExpression_3036,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 3036); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getMergeNode_2025Text(View view) {
		de.hpi.sam.storyDiagramEcore.nodes.MergeNode domainModelElement = (de.hpi.sam.storyDiagramEcore.nodes.MergeNode) view
				.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance()
					.logError(
							"No domain element for view with visualID = " + 2025); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getFlowFinalNode_2021Text(View view) {
		de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode domainModelElement = (de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode) view
				.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance()
					.logError(
							"No domain element for view with visualID = " + 2021); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getAcquireSemaphoreEdge_4013Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.AcquireSemaphoreEdge_4013,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeWeightEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 6018); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getReleaseSemaphoreEdge_4012Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ReleaseSemaphoreEdge_4012,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeWeightEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 6016); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getActivityEdge_4006Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeGuardConstraintLabelEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 6009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getStoryPatternContainmentLink_4008Text(View view) {
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink domainModelElement = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink) view
				.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance()
					.logError(
							"No domain element for view with visualID = " + 4008); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCallActionExpression_3037Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3037,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 3037); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getStoryActionNode_2026Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryActionNode_2026,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 5030); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getMapEntryStoryPatternLink_4010Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkFeatureNameLabelEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 6015); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getActivity_1000Text(View view) {
		de.hpi.sam.storyDiagramEcore.Activity domainModelElement = (de.hpi.sam.storyDiagramEcore.Activity) view
				.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance()
					.logError(
							"No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getExpressionActivityNode_2020Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ExpressionActivityNode_2020,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 5024); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getActivityFinalNode_2018Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityFinalNode_2018,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeReturnValueLabelEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 5023); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getInitialNode_2023Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.InitialNode_2023,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeSpecificationLabelEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 5025); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getDecisionNode_2019Text(View view) {
		de.hpi.sam.storyDiagramEcore.nodes.DecisionNode domainModelElement = (de.hpi.sam.storyDiagramEcore.nodes.DecisionNode) view
				.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance()
					.logError(
							"No domain element for view with visualID = " + 2019); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCallActionExpression_3034Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3034,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 3034); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getLinkOrderConstraint_4015Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.LinkOrderConstraint_4015,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintLabelEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 6023); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCallActionExpression_3032Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.CallActionExpression_3032,
						view.getElement() != null ? view.getElement() : view,
						de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
								.getType(de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 3032); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getMapEntryStoryPatternLinkValueTarget_4011Text(View view) {
		IParser parser = de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreParserProvider
				.getParser(
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLinkValueTarget_4011,
						view.getElement() != null ? view.getElement() : view,
						CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreDiagramEditorPlugin
					.getInstance().logError(
							"Parser was not found for label " + 6019); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.MODEL_ID
				.equals(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
						.getModelID(view));
	}

}
