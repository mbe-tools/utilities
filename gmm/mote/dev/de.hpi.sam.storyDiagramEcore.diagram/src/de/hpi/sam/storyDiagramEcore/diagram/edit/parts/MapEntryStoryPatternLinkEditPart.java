package de.hpi.sam.storyDiagramEcore.diagram.edit.parts;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Connection;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;

/**
 * @generated
 */
public class MapEntryStoryPatternLinkEditPart extends ConnectionNodeEditPart
		implements ITreeBranchEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 4010;

	/**
	 * @generated
	 */
	public MapEntryStoryPatternLinkEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(
				EditPolicyRoles.SEMANTIC_ROLE,
				new de.hpi.sam.storyDiagramEcore.diagram.edit.policies.MapEntryStoryPatternLinkItemSemanticEditPolicy());
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkFeatureNameLabelEditPart) {
			((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkFeatureNameLabelEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureMapEntryStoryPatternLinkFeatureNameLabel());
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkKeyLabelEditPart) {
			((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkKeyLabelEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureMapEntryStoryPatternLinkKeyLabel());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, index);
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkFeatureNameLabelEditPart) {
			return true;
		}
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkKeyLabelEditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */

	protected Connection createConnectionFigure() {
		return new MapEntryStoryPatternLinkFigureDescriptor();
	}

	/**
	 * @generated
	 */
	public MapEntryStoryPatternLinkFigureDescriptor getPrimaryShape() {
		return (MapEntryStoryPatternLinkFigureDescriptor) getFigure();
	}

	/**
	 * @generated
	 */
	public class MapEntryStoryPatternLinkFigureDescriptor
			extends
			de.hpi.sam.storyDiagramEcore.diagram.figures.MapLinkConnectionFigure {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureMapEntryStoryPatternLinkFeatureNameLabel;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureMapEntryStoryPatternLinkKeyLabel;

		/**
		 * @generated
		 */
		public MapEntryStoryPatternLinkFigureDescriptor() {

			this.setForegroundColor(ColorConstants.black);
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureMapEntryStoryPatternLinkFeatureNameLabel = new WrappingLabel();
			fFigureMapEntryStoryPatternLinkFeatureNameLabel.setText("");

			this.add(fFigureMapEntryStoryPatternLinkFeatureNameLabel);

			fFigureMapEntryStoryPatternLinkKeyLabel = new WrappingLabel();
			fFigureMapEntryStoryPatternLinkKeyLabel.setText("key");

			this.add(fFigureMapEntryStoryPatternLinkKeyLabel);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureMapEntryStoryPatternLinkFeatureNameLabel() {
			return fFigureMapEntryStoryPatternLinkFeatureNameLabel;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureMapEntryStoryPatternLinkKeyLabel() {
			return fFigureMapEntryStoryPatternLinkKeyLabel;
		}

	}

}
