package de.hpi.sam.storyDiagramEcore.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

/**
 * @generated
 */
public class ReleaseSemaphoreEdgeReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	protected final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public ReleaseSemaphoreEdgeReorientCommand(
			ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof de.hpi.sam.storyDiagramEcore.nodes.Semaphore && newEnd instanceof de.hpi.sam.storyDiagramEcore.nodes.Semaphore)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge target = getLink()
				.getActivityEdge();
		if (!(getLink().eContainer() instanceof de.hpi.sam.storyDiagramEcore.nodes.Semaphore)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.nodes.Semaphore container = (de.hpi.sam.storyDiagramEcore.nodes.Semaphore) getLink()
				.eContainer();
		return de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy
				.getLinkConstraints().canExistReleaseSemaphoreEdge_4012(
						container, getLink(), getNewSource(), target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge && newEnd instanceof de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.nodes.Semaphore source = getLink()
				.getSemaphore();
		if (!(getLink().eContainer() instanceof de.hpi.sam.storyDiagramEcore.nodes.Semaphore)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.nodes.Semaphore container = (de.hpi.sam.storyDiagramEcore.nodes.Semaphore) getLink()
				.eContainer();
		return de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy
				.getLinkConstraints().canExistReleaseSemaphoreEdge_4012(
						container, getLink(), source, getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setSemaphore(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setActivityEdge(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge getLink() {
		return (de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.nodes.Semaphore getOldSource() {
		return (de.hpi.sam.storyDiagramEcore.nodes.Semaphore) oldEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.nodes.Semaphore getNewSource() {
		return (de.hpi.sam.storyDiagramEcore.nodes.Semaphore) newEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge getOldTarget() {
		return (de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) oldEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge getNewTarget() {
		return (de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) newEnd;
	}
}
