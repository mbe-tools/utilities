package de.hpi.sam.storyDiagramEcore.diagram.edit.parts;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;

/**
 * @generated
 */
public class StoryPatternExpressionLinkEditPart extends ConnectionNodeEditPart
		implements ITreeBranchEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 4009;

	/**
	 * @generated
	 */
	public StoryPatternExpressionLinkEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(
				EditPolicyRoles.SEMANTIC_ROLE,
				new de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryPatternExpressionLinkItemSemanticEditPolicy());
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkLabelEditPart) {
			((de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkLabelEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureStoryPatternExpressionLinkFeatureNameLabel());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, index);
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkLabelEditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */

	protected Connection createConnectionFigure() {
		return new StoryPatternExpressionLinkFigureDescriptor();
	}

	/**
	 * @generated
	 */
	public StoryPatternExpressionLinkFigureDescriptor getPrimaryShape() {
		return (StoryPatternExpressionLinkFigureDescriptor) getFigure();
	}

	/**
	 * @generated
	 */
	public class StoryPatternExpressionLinkFigureDescriptor extends
			PolylineConnectionEx {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureStoryPatternExpressionLinkFeatureNameLabel;

		/**
		 * @generated
		 */
		public StoryPatternExpressionLinkFigureDescriptor() {
			this.setLineWidth(2);
			this.setLineStyle(Graphics.LINE_DASH);
			this.setForegroundColor(ColorConstants.black);

			createContents();
			setTargetDecoration(createTargetDecoration());
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureStoryPatternExpressionLinkFeatureNameLabel = new WrappingLabel();
			fFigureStoryPatternExpressionLinkFeatureNameLabel.setText("");

			this.add(fFigureStoryPatternExpressionLinkFeatureNameLabel);

		}

		/**
		 * @generated
		 */
		private RotatableDecoration createTargetDecoration() {
			PolylineDecoration df = new PolylineDecoration();
			return df;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureStoryPatternExpressionLinkFeatureNameLabel() {
			return fFigureStoryPatternExpressionLinkFeatureNameLabel;
		}

	}

}
