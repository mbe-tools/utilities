package de.hpi.sam.storyDiagramEcore.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

/**
 * @generated
 */
public class ActivityItemSemanticEditPolicy
		extends
		de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public ActivityItemSemanticEditPolicy() {
		super(
				de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.Activity_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityFinalNode_2018 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.ActivityFinalNodeCreateCommand(
					req));
		}
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.DecisionNode_2019 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.DecisionNodeCreateCommand(
					req));
		}
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ExpressionActivityNode_2020 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.ExpressionActivityNodeCreateCommand(
					req));
		}
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.FlowFinalNode_2021 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.FlowFinalNodeCreateCommand(
					req));
		}
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ForkNode_2022 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.ForkNodeCreateCommand(
					req));
		}
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.InitialNode_2023 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.InitialNodeCreateCommand(
					req));
		}
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.JoinNode_2024 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.JoinNodeCreateCommand(
					req));
		}
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.Semaphore_2027 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.SemaphoreCreateCommand(
					req));
		}
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MergeNode_2025 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.MergeNodeCreateCommand(
					req));
		}
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryActionNode_2026 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.StoryActionNodeCreateCommand(
					req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost())
				.getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends
			DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(
				TransactionalEditingDomain editingDomain,
				DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req
					.getElementsToBeDuplicated(), req
					.getAllDuplicatedElementsMap());
		}

	}

}
