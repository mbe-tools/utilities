package de.hpi.sam.storyDiagramEcore.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

/**
 * @generated
 */
public class ActivityEdgeReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	protected final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public ActivityEdgeReorientCommand(ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof de.hpi.sam.storyDiagramEcore.nodes.ActivityNode && newEnd instanceof de.hpi.sam.storyDiagramEcore.nodes.ActivityNode)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.nodes.ActivityNode target = getLink()
				.getTarget();
		if (!(getLink().eContainer() instanceof de.hpi.sam.storyDiagramEcore.Activity)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.Activity container = (de.hpi.sam.storyDiagramEcore.Activity) getLink()
				.eContainer();
		return de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy
				.getLinkConstraints().canExistActivityEdge_4006(container,
						getLink(), getNewSource(), target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof de.hpi.sam.storyDiagramEcore.nodes.ActivityNode && newEnd instanceof de.hpi.sam.storyDiagramEcore.nodes.ActivityNode)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.nodes.ActivityNode source = getLink()
				.getSource();
		if (!(getLink().eContainer() instanceof de.hpi.sam.storyDiagramEcore.Activity)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.Activity container = (de.hpi.sam.storyDiagramEcore.Activity) getLink()
				.eContainer();
		return de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy
				.getLinkConstraints().canExistActivityEdge_4006(container,
						getLink(), source, getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setSource(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setTarget(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge getLink() {
		return (de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.nodes.ActivityNode getOldSource() {
		return (de.hpi.sam.storyDiagramEcore.nodes.ActivityNode) oldEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.nodes.ActivityNode getNewSource() {
		return (de.hpi.sam.storyDiagramEcore.nodes.ActivityNode) newEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.nodes.ActivityNode getOldTarget() {
		return (de.hpi.sam.storyDiagramEcore.nodes.ActivityNode) oldEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.nodes.ActivityNode getNewTarget() {
		return (de.hpi.sam.storyDiagramEcore.nodes.ActivityNode) newEnd;
	}
}
