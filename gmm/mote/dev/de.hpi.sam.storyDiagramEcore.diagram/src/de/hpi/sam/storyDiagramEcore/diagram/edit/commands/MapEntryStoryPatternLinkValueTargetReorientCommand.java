package de.hpi.sam.storyDiagramEcore.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientReferenceRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

/**
 * @generated
 */
public class MapEntryStoryPatternLinkValueTargetReorientCommand extends
		EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject referenceOwner;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public MapEntryStoryPatternLinkValueTargetReorientCommand(
			ReorientReferenceRelationshipRequest request) {
		super(request.getLabel(), null, request);
		reorientDirection = request.getDirection();
		referenceOwner = request.getReferenceOwner();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == referenceOwner instanceof de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject && newEnd instanceof de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink)) {
			return false;
		}
		return de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy
				.getLinkConstraints()
				.canExistMapEntryStoryPatternLinkValueTarget_4011(
						getNewSource(), getOldTarget());
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject && newEnd instanceof de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject)) {
			return false;
		}
		return de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy
				.getLinkConstraints()
				.canExistMapEntryStoryPatternLinkValueTarget_4011(
						getOldSource(), getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getOldSource().setValueTarget(null);
		getNewSource().setValueTarget(getOldTarget());
		return CommandResult.newOKCommandResult(referenceOwner);
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getOldSource().setValueTarget(getNewTarget());
		return CommandResult.newOKCommandResult(referenceOwner);
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink getOldSource() {
		return (de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) referenceOwner;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink getNewSource() {
		return (de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) newEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject getOldTarget() {
		return (de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject) oldEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject getNewTarget() {
		return (de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject) newEnd;
	}
}
