package de.hpi.sam.storyDiagramEcore.diagram.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionLocator;
import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;

public class MapLinkConnectionFigure extends PolylineConnectionEx
{

	public MapLinkConnectionFigure()
	{
		this.setLineWidth(1);

		createContents();
		setTargetDecoration(createTargetDecoration());
	}

	private void createContents()
	{
		Ellipse ellipse = new Ellipse();

		Dimension size = new Dimension(20, 20);
		ellipse.setMinimumSize(size);
		ellipse.setMaximumSize(size);
		ellipse.setPreferredSize(size);

		this.add(ellipse, new ConnectionLocator(this, ConnectionLocator.MIDDLE));
	}

	private RotatableDecoration createTargetDecoration()
	{
		PolylineDecoration df = new PolylineDecoration();
		df.setLineWidth(1);
		df.setForegroundColor(ColorConstants.black);
		return df;
	}

}
