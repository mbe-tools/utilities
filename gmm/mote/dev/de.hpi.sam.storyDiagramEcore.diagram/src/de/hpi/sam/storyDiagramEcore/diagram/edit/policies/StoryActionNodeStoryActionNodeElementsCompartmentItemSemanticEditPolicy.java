package de.hpi.sam.storyDiagramEcore.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

/**
 * @generated
 */
public class StoryActionNodeStoryActionNodeElementsCompartmentItemSemanticEditPolicy
		extends
		de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public StoryActionNodeStoryActionNodeElementsCompartmentItemSemanticEditPolicy() {
		super(
				de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryActionNode_2026);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternObject_3035 == req
				.getElementType()) {
			return getGEFWrapper(new de.hpi.sam.storyDiagramEcore.diagram.edit.commands.StoryPatternObjectCreateCommand(
					req));
		}
		return super.getCreateCommand(req);
	}

}
