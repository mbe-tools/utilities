package de.hpi.sam.storyDiagramEcore.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;

/**
 * @generated
 */
public class StoryDiagramEcoreDiagramUpdater {

	/**
	 * @generated
	 */
	public static boolean isShortcutOrphaned(View view) {
		return !view.isSetElement() || view.getElement() == null
				|| view.getElement().eIsProxy();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> getSemanticChildren(
			View view) {
		switch (de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
				.getVisualID(view)) {
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID:
			return getActivity_1000SemanticChildren(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeExpressionFigureCompartmentEditPart.VISUAL_ID:
			return getExpressionActivityNodeExpressionFigureCompartment_7014SemanticChildren(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart.VISUAL_ID:
			return getStoryActionNodeStoryActionNodeConstraintsCompartment_7015SemanticChildren(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeElementsCompartmentEditPart.VISUAL_ID:
			return getStoryActionNodeStoryActionNodeElementsCompartment_7016SemanticChildren(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart.VISUAL_ID:
			return getStoryPatternObjectStoryPatternObjectConstraintsCompartment_7017SemanticChildren(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID:
			return getStoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartment_7018SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> getActivity_1000SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		de.hpi.sam.storyDiagramEcore.Activity modelElement = (de.hpi.sam.storyDiagramEcore.Activity) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor>();
		for (Iterator<?> it = modelElement.getNodes().iterator(); it.hasNext();) {
			de.hpi.sam.storyDiagramEcore.nodes.ActivityNode childElement = (de.hpi.sam.storyDiagramEcore.nodes.ActivityNode) it
					.next();
			int visualID = de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getSemaphores().iterator(); it
				.hasNext();) {
			de.hpi.sam.storyDiagramEcore.nodes.Semaphore childElement = (de.hpi.sam.storyDiagramEcore.nodes.Semaphore) it
					.next();
			int visualID = de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> getExpressionActivityNodeExpressionFigureCompartment_7014SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode) containerView
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor>();
		{
			de.hpi.sam.storyDiagramEcore.expressions.Expression childElement = modelElement
					.getExpression();
			int visualID = de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
			}
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> getStoryActionNodeStoryActionNodeConstraintsCompartment_7015SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) containerView
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor>();
		for (Iterator<?> it = modelElement.getConstraints().iterator(); it
				.hasNext();) {
			de.hpi.sam.storyDiagramEcore.expressions.Expression childElement = (de.hpi.sam.storyDiagramEcore.expressions.Expression) it
					.next();
			int visualID = de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> getStoryActionNodeStoryActionNodeElementsCompartment_7016SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) containerView
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor>();
		for (Iterator<?> it = modelElement.getStoryPatternObjects().iterator(); it
				.hasNext();) {
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject childElement = (de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject) it
					.next();
			int visualID = de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> getStoryPatternObjectStoryPatternObjectConstraintsCompartment_7017SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject modelElement = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject) containerView
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor>();
		for (Iterator<?> it = modelElement.getConstraints().iterator(); it
				.hasNext();) {
			de.hpi.sam.storyDiagramEcore.expressions.Expression childElement = (de.hpi.sam.storyDiagramEcore.expressions.Expression) it
					.next();
			int visualID = de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> getStoryPatternObjectStoryPatternObjectAttributeAssignmentsCompartment_7018SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject modelElement = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject) containerView
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor>();
		for (Iterator<?> it = modelElement.getAttributeAssignments().iterator(); it
				.hasNext();) {
			de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment childElement = (de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment) it
					.next();
			int visualID = de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart.VISUAL_ID) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getContainedLinks(
			View view) {
		switch (de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
				.getVisualID(view)) {
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart.VISUAL_ID:
			return getActivity_1000ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID:
			return getActivityFinalNode_2018ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID:
			return getDecisionNode_2019ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID:
			return getExpressionActivityNode_2020ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID:
			return getFlowFinalNode_2021ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID:
			return getForkNode_2022ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID:
			return getInitialNode_2023ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID:
			return getJoinNode_2024ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID:
			return getSemaphore_2027ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID:
			return getMergeNode_2025ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID:
			return getStoryActionNode_2026ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID:
			return getStringExpression_3031ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart.VISUAL_ID:
			return getCallActionExpression_3032ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID:
			return getStringExpression_3033ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart.VISUAL_ID:
			return getCallActionExpression_3034ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID:
			return getStoryPatternObject_3035ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID:
			return getStringExpression_3036ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart.VISUAL_ID:
			return getCallActionExpression_3037ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart.VISUAL_ID:
			return getAttributeAssignment_3038ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID:
			return getActivityEdge_4006ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID:
			return getStoryPatternLink_4007ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID:
			return getStoryPatternContainmentLink_4008ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID:
			return getStoryPatternExpressionLink_4009ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID:
			return getMapEntryStoryPatternLink_4010ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID:
			return getReleaseSemaphoreEdge_4012ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID:
			return getAcquireSemaphoreEdge_4013ContainedLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID:
			return getLinkOrderConstraint_4015ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getIncomingLinks(
			View view) {
		switch (de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
				.getVisualID(view)) {
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID:
			return getActivityFinalNode_2018IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID:
			return getDecisionNode_2019IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID:
			return getExpressionActivityNode_2020IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID:
			return getFlowFinalNode_2021IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID:
			return getForkNode_2022IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID:
			return getInitialNode_2023IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID:
			return getJoinNode_2024IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID:
			return getSemaphore_2027IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID:
			return getMergeNode_2025IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID:
			return getStoryActionNode_2026IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID:
			return getStringExpression_3031IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart.VISUAL_ID:
			return getCallActionExpression_3032IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID:
			return getStringExpression_3033IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart.VISUAL_ID:
			return getCallActionExpression_3034IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID:
			return getStoryPatternObject_3035IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID:
			return getStringExpression_3036IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart.VISUAL_ID:
			return getCallActionExpression_3037IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart.VISUAL_ID:
			return getAttributeAssignment_3038IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID:
			return getActivityEdge_4006IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID:
			return getStoryPatternLink_4007IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID:
			return getStoryPatternContainmentLink_4008IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID:
			return getStoryPatternExpressionLink_4009IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID:
			return getMapEntryStoryPatternLink_4010IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID:
			return getReleaseSemaphoreEdge_4012IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID:
			return getAcquireSemaphoreEdge_4013IncomingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID:
			return getLinkOrderConstraint_4015IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getOutgoingLinks(
			View view) {
		switch (de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
				.getVisualID(view)) {
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityFinalNodeEditPart.VISUAL_ID:
			return getActivityFinalNode_2018OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.DecisionNodeEditPart.VISUAL_ID:
			return getDecisionNode_2019OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeEditPart.VISUAL_ID:
			return getExpressionActivityNode_2020OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.FlowFinalNodeEditPart.VISUAL_ID:
			return getFlowFinalNode_2021OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ForkNodeEditPart.VISUAL_ID:
			return getForkNode_2022OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.InitialNodeEditPart.VISUAL_ID:
			return getInitialNode_2023OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.JoinNodeEditPart.VISUAL_ID:
			return getJoinNode_2024OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreEditPart.VISUAL_ID:
			return getSemaphore_2027OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MergeNodeEditPart.VISUAL_ID:
			return getMergeNode_2025OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeEditPart.VISUAL_ID:
			return getStoryActionNode_2026OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID:
			return getStringExpression_3031OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpressionEditPart.VISUAL_ID:
			return getCallActionExpression_3032OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID:
			return getStringExpression_3033OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression2EditPart.VISUAL_ID:
			return getCallActionExpression_3034OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectEditPart.VISUAL_ID:
			return getStoryPatternObject_3035OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID:
			return getStringExpression_3036OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.CallActionExpression3EditPart.VISUAL_ID:
			return getCallActionExpression_3037OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AttributeAssignmentEditPart.VISUAL_ID:
			return getAttributeAssignment_3038OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID:
			return getActivityEdge_4006OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID:
			return getStoryPatternLink_4007OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID:
			return getStoryPatternContainmentLink_4008OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID:
			return getStoryPatternExpressionLink_4009OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID:
			return getMapEntryStoryPatternLink_4010OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID:
			return getReleaseSemaphoreEdge_4012OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID:
			return getAcquireSemaphoreEdge_4013OutgoingLinks(view);
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID:
			return getLinkOrderConstraint_4015OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getActivity_1000ContainedLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.Activity modelElement = (de.hpi.sam.storyDiagramEcore.Activity) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_ActivityEdge_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getActivityFinalNode_2018ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getDecisionNode_2019ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getExpressionActivityNode_2020ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getFlowFinalNode_2021ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getForkNode_2022ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getInitialNode_2023ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getJoinNode_2024ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getSemaphore_2027ContainedLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.Semaphore modelElement = (de.hpi.sam.storyDiagramEcore.nodes.Semaphore) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_ReleaseSemaphoreEdge_4012(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_AcquireSemaphoreEdge_4013(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getMergeNode_2025ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryActionNode_2026ContainedLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_StoryPatternLink_4007(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_StoryPatternContainmentLink_4008(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_StoryPatternExpressionLink_4009(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MapEntryStoryPatternLink_4010(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStringExpression_3031ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getCallActionExpression_3032ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStringExpression_3033ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getCallActionExpression_3034ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryPatternObject_3035ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStringExpression_3036ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getCallActionExpression_3037ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getAttributeAssignment_3038ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getActivityEdge_4006ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryPatternLink_4007ContainedLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink modelElement = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_LinkOrderConstraint_4015(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryPatternContainmentLink_4008ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryPatternExpressionLink_4009ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getMapEntryStoryPatternLink_4010ContainedLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink modelElement = (de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_MapEntryStoryPatternLink_ValueTarget_4011(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getReleaseSemaphoreEdge_4012ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getAcquireSemaphoreEdge_4013ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getLinkOrderConstraint_4015ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getActivityFinalNode_2018IncomingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ActivityEdge_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getDecisionNode_2019IncomingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.DecisionNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.DecisionNode) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ActivityEdge_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getExpressionActivityNode_2020IncomingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ActivityEdge_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getFlowFinalNode_2021IncomingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ActivityEdge_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getForkNode_2022IncomingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.ForkNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.ForkNode) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ActivityEdge_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getInitialNode_2023IncomingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.InitialNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.InitialNode) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ActivityEdge_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getJoinNode_2024IncomingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.JoinNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.JoinNode) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ActivityEdge_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getSemaphore_2027IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getMergeNode_2025IncomingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.MergeNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.MergeNode) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ActivityEdge_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryActionNode_2026IncomingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ActivityEdge_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStringExpression_3031IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getCallActionExpression_3032IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStringExpression_3033IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getCallActionExpression_3034IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryPatternObject_3035IncomingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject modelElement = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_StoryPatternLink_4007(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_StoryPatternContainmentLink_4008(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_StoryPatternExpressionLink_4009(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_MapEntryStoryPatternLink_4010(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_MapEntryStoryPatternLink_ValueTarget_4011(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStringExpression_3036IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getCallActionExpression_3037IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getAttributeAssignment_3038IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getActivityEdge_4006IncomingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge modelElement = (de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ReleaseSemaphoreEdge_4012(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_AcquireSemaphoreEdge_4013(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryPatternLink_4007IncomingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink modelElement = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_LinkOrderConstraint_4015(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryPatternContainmentLink_4008IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryPatternExpressionLink_4009IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getMapEntryStoryPatternLink_4010IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getReleaseSemaphoreEdge_4012IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getAcquireSemaphoreEdge_4013IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getLinkOrderConstraint_4015IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getActivityFinalNode_2018OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ActivityEdge_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getDecisionNode_2019OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.DecisionNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.DecisionNode) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ActivityEdge_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getExpressionActivityNode_2020OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ActivityEdge_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getFlowFinalNode_2021OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ActivityEdge_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getForkNode_2022OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.ForkNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.ForkNode) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ActivityEdge_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getInitialNode_2023OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.InitialNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.InitialNode) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ActivityEdge_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getJoinNode_2024OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.JoinNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.JoinNode) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ActivityEdge_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getSemaphore_2027OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.Semaphore modelElement = (de.hpi.sam.storyDiagramEcore.nodes.Semaphore) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ReleaseSemaphoreEdge_4012(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_AcquireSemaphoreEdge_4013(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getMergeNode_2025OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.MergeNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.MergeNode) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ActivityEdge_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryActionNode_2026OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode modelElement = (de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ActivityEdge_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStringExpression_3031OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getCallActionExpression_3032OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStringExpression_3033OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getCallActionExpression_3034OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryPatternObject_3035OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject modelElement = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_StoryPatternLink_4007(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_StoryPatternContainmentLink_4008(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_StoryPatternExpressionLink_4009(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_MapEntryStoryPatternLink_4010(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStringExpression_3036OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getCallActionExpression_3037OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getAttributeAssignment_3038OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getActivityEdge_4006OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryPatternLink_4007OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink modelElement = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_LinkOrderConstraint_4015(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryPatternContainmentLink_4008OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getStoryPatternExpressionLink_4009OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getMapEntryStoryPatternLink_4010OutgoingLinks(
			View view) {
		de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink modelElement = (de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) view
				.getElement();
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_MapEntryStoryPatternLink_ValueTarget_4011(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getReleaseSemaphoreEdge_4012OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getAcquireSemaphoreEdge_4013OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getLinkOrderConstraint_4015OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getContainedTypeModelFacetLinks_ActivityEdge_4006(
			de.hpi.sam.storyDiagramEcore.Activity container) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getEdges().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge link = (de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ActivityNode dst = link
					.getTarget();
			de.hpi.sam.storyDiagramEcore.nodes.ActivityNode src = link
					.getSource();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getContainedTypeModelFacetLinks_StoryPatternLink_4007(
			de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode container) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getStoryPatternLinks().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink link = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject dst = link
					.getTarget();
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject src = link
					.getSource();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternLink_4007,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getContainedTypeModelFacetLinks_StoryPatternContainmentLink_4008(
			de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode container) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getStoryPatternLinks().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink link = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject dst = link
					.getTarget();
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject src = link
					.getSource();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternContainmentLink_4008,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getContainedTypeModelFacetLinks_StoryPatternExpressionLink_4009(
			de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode container) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getStoryPatternLinks().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink link = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject dst = link
					.getTarget();
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject src = link
					.getSource();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternExpressionLink_4009,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getContainedTypeModelFacetLinks_MapEntryStoryPatternLink_4010(
			de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode container) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getStoryPatternLinks().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink link = (de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject dst = link
					.getTarget();
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject src = link
					.getSource();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getContainedTypeModelFacetLinks_ReleaseSemaphoreEdge_4012(
			de.hpi.sam.storyDiagramEcore.nodes.Semaphore container) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getSynchronizationEdges().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge link = (de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge dst = link
					.getActivityEdge();
			de.hpi.sam.storyDiagramEcore.nodes.Semaphore src = link
					.getSemaphore();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ReleaseSemaphoreEdge_4012,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getContainedTypeModelFacetLinks_AcquireSemaphoreEdge_4013(
			de.hpi.sam.storyDiagramEcore.nodes.Semaphore container) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getSynchronizationEdges().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge link = (de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge dst = link
					.getActivityEdge();
			de.hpi.sam.storyDiagramEcore.nodes.Semaphore src = link
					.getSemaphore();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.AcquireSemaphoreEdge_4013,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getContainedTypeModelFacetLinks_LinkOrderConstraint_4015(
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink container) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getOutgoingLinkOrderConstraints()
				.iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint link = (de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink dst = link
					.getSuccessorLink();
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink src = link
					.getPredecessorLink();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.LinkOrderConstraint_4015,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getIncomingTypeModelFacetLinks_ActivityEdge_4006(
			de.hpi.sam.storyDiagramEcore.nodes.ActivityNode target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getActivityEdge_Target()
					|| false == setting.getEObject() instanceof de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge link = (de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) setting
					.getEObject();
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ActivityNode src = link
					.getSource();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					target,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getIncomingTypeModelFacetLinks_StoryPatternLink_4007(
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
					.getAbstractStoryPatternLink_Target()
					|| false == setting.getEObject() instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink link = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) setting
					.getEObject();
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject src = link
					.getSource();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					target,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternLink_4007,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getIncomingTypeModelFacetLinks_StoryPatternContainmentLink_4008(
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
					.getAbstractStoryPatternLink_Target()
					|| false == setting.getEObject() instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink link = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink) setting
					.getEObject();
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject src = link
					.getSource();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					target,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternContainmentLink_4008,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getIncomingTypeModelFacetLinks_StoryPatternExpressionLink_4009(
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
					.getAbstractStoryPatternLink_Target()
					|| false == setting.getEObject() instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink link = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink) setting
					.getEObject();
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject src = link
					.getSource();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					target,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternExpressionLink_4009,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getIncomingTypeModelFacetLinks_MapEntryStoryPatternLink_4010(
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
					.getAbstractStoryPatternLink_Target()
					|| false == setting.getEObject() instanceof de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink link = (de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) setting
					.getEObject();
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject src = link
					.getSource();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					target,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getIncomingFeatureModelFacetLinks_MapEntryStoryPatternLink_ValueTarget_4011(
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
					.getMapEntryStoryPatternLink_ValueTarget()) {
				result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
						setting.getEObject(),
						target,
						de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLinkValueTarget_4011,
						de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getIncomingTypeModelFacetLinks_ReleaseSemaphoreEdge_4012(
			de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getReleaseSemaphoreEdge_ActivityEdge()
					|| false == setting.getEObject() instanceof de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge link = (de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge) setting
					.getEObject();
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.Semaphore src = link
					.getSemaphore();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					target,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ReleaseSemaphoreEdge_4012,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getIncomingTypeModelFacetLinks_AcquireSemaphoreEdge_4013(
			de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getAcquireSemaphoreEdge_ActivityEdge()
					|| false == setting.getEObject() instanceof de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge link = (de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge) setting
					.getEObject();
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.Semaphore src = link
					.getSemaphore();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					target,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.AcquireSemaphoreEdge_4013,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getIncomingTypeModelFacetLinks_LinkOrderConstraint_4015(
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE
					.getLinkOrderConstraint_SuccessorLink()
					|| false == setting.getEObject() instanceof de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint link = (de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint) setting
					.getEObject();
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink src = link
					.getPredecessorLink();
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					target,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.LinkOrderConstraint_4015,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getOutgoingTypeModelFacetLinks_ActivityEdge_4006(
			de.hpi.sam.storyDiagramEcore.nodes.ActivityNode source) {
		de.hpi.sam.storyDiagramEcore.Activity container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof de.hpi.sam.storyDiagramEcore.Activity) {
				container = (de.hpi.sam.storyDiagramEcore.Activity) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getEdges().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge link = (de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ActivityNode dst = link
					.getTarget();
			de.hpi.sam.storyDiagramEcore.nodes.ActivityNode src = link
					.getSource();
			if (src != source) {
				continue;
			}
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ActivityEdge_4006,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getOutgoingTypeModelFacetLinks_StoryPatternLink_4007(
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject source) {
		de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) {
				container = (de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getStoryPatternLinks().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink link = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject dst = link
					.getTarget();
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject src = link
					.getSource();
			if (src != source) {
				continue;
			}
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternLink_4007,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getOutgoingTypeModelFacetLinks_StoryPatternContainmentLink_4008(
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject source) {
		de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) {
				container = (de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getStoryPatternLinks().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink link = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject dst = link
					.getTarget();
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject src = link
					.getSource();
			if (src != source) {
				continue;
			}
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternContainmentLink_4008,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternContainmentLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getOutgoingTypeModelFacetLinks_StoryPatternExpressionLink_4009(
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject source) {
		de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) {
				container = (de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getStoryPatternLinks().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink link = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject dst = link
					.getTarget();
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject src = link
					.getSource();
			if (src != source) {
				continue;
			}
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.StoryPatternExpressionLink_4009,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternExpressionLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getOutgoingTypeModelFacetLinks_MapEntryStoryPatternLink_4010(
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject source) {
		de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) {
				container = (de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getStoryPatternLinks().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink link = (de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject dst = link
					.getTarget();
			de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject src = link
					.getSource();
			if (src != source) {
				continue;
			}
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLink_4010,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getOutgoingFeatureModelFacetLinks_MapEntryStoryPatternLink_ValueTarget_4011(
			de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink source) {
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject destination = source
				.getValueTarget();
		if (destination == null) {
			return result;
		}
		result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
				source,
				destination,
				de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.MapEntryStoryPatternLinkValueTarget_4011,
				de.hpi.sam.storyDiagramEcore.diagram.edit.parts.MapEntryStoryPatternLinkValueTargetEditPart.VISUAL_ID));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getOutgoingTypeModelFacetLinks_ReleaseSemaphoreEdge_4012(
			de.hpi.sam.storyDiagramEcore.nodes.Semaphore source) {
		de.hpi.sam.storyDiagramEcore.nodes.Semaphore container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof de.hpi.sam.storyDiagramEcore.nodes.Semaphore) {
				container = (de.hpi.sam.storyDiagramEcore.nodes.Semaphore) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getSynchronizationEdges().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge link = (de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge dst = link
					.getActivityEdge();
			de.hpi.sam.storyDiagramEcore.nodes.Semaphore src = link
					.getSemaphore();
			if (src != source) {
				continue;
			}
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.ReleaseSemaphoreEdge_4012,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getOutgoingTypeModelFacetLinks_AcquireSemaphoreEdge_4013(
			de.hpi.sam.storyDiagramEcore.nodes.Semaphore source) {
		de.hpi.sam.storyDiagramEcore.nodes.Semaphore container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof de.hpi.sam.storyDiagramEcore.nodes.Semaphore) {
				container = (de.hpi.sam.storyDiagramEcore.nodes.Semaphore) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getSynchronizationEdges().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge link = (de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge dst = link
					.getActivityEdge();
			de.hpi.sam.storyDiagramEcore.nodes.Semaphore src = link
					.getSemaphore();
			if (src != source) {
				continue;
			}
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.AcquireSemaphoreEdge_4013,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> getOutgoingTypeModelFacetLinks_LinkOrderConstraint_4015(
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink source) {
		de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) {
				container = (de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor> result = new LinkedList<de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor>();
		for (Iterator<?> links = container.getOutgoingLinkOrderConstraints()
				.iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint link = (de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint) linkObject;
			if (de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID != de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink dst = link
					.getSuccessorLink();
			de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink src = link
					.getPredecessorLink();
			if (src != source) {
				continue;
			}
			result.add(new de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreLinkDescriptor(
					src,
					dst,
					link,
					de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes.LinkOrderConstraint_4015,
					de.hpi.sam.storyDiagramEcore.diagram.edit.parts.LinkOrderConstraintEditPart.VISUAL_ID));
		}
		return result;
	}

}
