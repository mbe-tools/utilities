package de.hpi.sam.storyDiagramEcore.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;

/**
 * @generated
 */
public class StoryDiagramEcoreParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser expressionActivityNodeName_5024Parser;

	/**
	 * @generated
	 */
	private IParser getExpressionActivityNodeName_5024Parser() {
		if (expressionActivityNodeName_5024Parser == null) {
			EAttribute[] features = new EAttribute[] { de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE
					.getNamedElement_Name() };
			de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser parser = new de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser(
					features);
			parser.setViewPattern("{0}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			expressionActivityNodeName_5024Parser = parser;
		}
		return expressionActivityNodeName_5024Parser;
	}

	/**
	 * @generated
	 */
	private IParser semaphoreTokenCount_5031Parser;

	/**
	 * @generated
	 */
	private IParser getSemaphoreTokenCount_5031Parser() {
		if (semaphoreTokenCount_5031Parser == null) {
			EAttribute[] features = new EAttribute[] { de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getSemaphore_TokenCount() };
			EAttribute[] editableFeatures = new EAttribute[] { de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getSemaphore_TokenCount() };
			de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser parser = new de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser(
					features, editableFeatures);
			semaphoreTokenCount_5031Parser = parser;
		}
		return semaphoreTokenCount_5031Parser;
	}

	/**
	 * @generated
	 */
	private IParser storyActionNodeName_5030Parser;

	/**
	 * @generated
	 */
	private IParser getStoryActionNodeName_5030Parser() {
		if (storyActionNodeName_5030Parser == null) {
			EAttribute[] features = new EAttribute[] { de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE
					.getNamedElement_Name() };
			de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser parser = new de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser(
					features);
			parser.setViewPattern("{0}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			storyActionNodeName_5030Parser = parser;
		}
		return storyActionNodeName_5030Parser;
	}

	/**
	 * @generated
	 */
	private IParser stringExpression_3031Parser;

	/**
	 * @generated
	 */
	private IParser getStringExpression_3031Parser() {
		if (stringExpression_3031Parser == null) {
			EAttribute[] features = new EAttribute[] { de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
					.getStringExpression_ExpressionString() };
			de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser parser = new de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser(
					features);
			parser.setViewPattern("[ {0} ]"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			stringExpression_3031Parser = parser;
		}
		return stringExpression_3031Parser;
	}

	/**
	 * @generated
	 */
	private IParser stringExpression_3033Parser;

	/**
	 * @generated
	 */
	private IParser getStringExpression_3033Parser() {
		if (stringExpression_3033Parser == null) {
			EAttribute[] features = new EAttribute[] { de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
					.getStringExpression_ExpressionString() };
			de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser parser = new de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser(
					features);
			parser.setViewPattern("[ {0} ]"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			stringExpression_3033Parser = parser;
		}
		return stringExpression_3033Parser;
	}

	/**
	 * @generated
	 */
	private IParser storyPatternObjectName_5026Parser;

	/**
	 * @generated
	 */
	private IParser getStoryPatternObjectName_5026Parser() {
		if (storyPatternObjectName_5026Parser == null) {
			EAttribute[] features = new EAttribute[] { de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE
					.getNamedElement_Name() };
			de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser parser = new de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser(
					features);
			parser.setViewPattern("{0}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			storyPatternObjectName_5026Parser = parser;
		}
		return storyPatternObjectName_5026Parser;
	}

	/**
	 * @generated
	 */
	private IParser stringExpression_3036Parser;

	/**
	 * @generated
	 */
	private IParser getStringExpression_3036Parser() {
		if (stringExpression_3036Parser == null) {
			EAttribute[] features = new EAttribute[] { de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE
					.getStringExpression_ExpressionString() };
			de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser parser = new de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser(
					features);
			parser.setViewPattern("[ {0} ]"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			stringExpression_3036Parser = parser;
		}
		return stringExpression_3036Parser;
	}

	/**
	 * @generated
	 */
	private IParser releaseSemaphoreEdgeWeight_6016Parser;

	/**
	 * @generated
	 */
	private IParser getReleaseSemaphoreEdgeWeight_6016Parser() {
		if (releaseSemaphoreEdgeWeight_6016Parser == null) {
			EAttribute[] features = new EAttribute[] { de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getSynchronizationEdge_Weight() };
			EAttribute[] editableFeatures = new EAttribute[] { de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getSynchronizationEdge_Weight() };
			de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser parser = new de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser(
					features, editableFeatures);
			parser.setViewPattern("release: {0}"); //$NON-NLS-1$
			parser.setEditorPattern("release: {0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			releaseSemaphoreEdgeWeight_6016Parser = parser;
		}
		return releaseSemaphoreEdgeWeight_6016Parser;
	}

	/**
	 * @generated
	 */
	private IParser acquireSemaphoreEdgeWeight_6018Parser;

	/**
	 * @generated
	 */
	private IParser getAcquireSemaphoreEdgeWeight_6018Parser() {
		if (acquireSemaphoreEdgeWeight_6018Parser == null) {
			EAttribute[] features = new EAttribute[] { de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getSynchronizationEdge_Weight() };
			EAttribute[] editableFeatures = new EAttribute[] { de.hpi.sam.storyDiagramEcore.nodes.NodesPackage.eINSTANCE
					.getSynchronizationEdge_Weight() };
			de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser parser = new de.hpi.sam.storyDiagramEcore.diagram.parsers.MessageFormatParser(
					features, editableFeatures);
			parser.setViewPattern("acquire: {0}"); //$NON-NLS-1$
			parser.setEditorPattern("acquire: {0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			acquireSemaphoreEdgeWeight_6018Parser = parser;
		}
		return acquireSemaphoreEdgeWeight_6018Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ExpressionActivityNodeNameEditPart.VISUAL_ID:
			return getExpressionActivityNodeName_5024Parser();
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.SemaphoreTokenCountEditPart.VISUAL_ID:
			return getSemaphoreTokenCount_5031Parser();
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeNameEditPart.VISUAL_ID:
			return getStoryActionNodeName_5030Parser();
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart.VISUAL_ID:
			return getStringExpression_3031Parser();
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression2EditPart.VISUAL_ID:
			return getStringExpression_3033Parser();
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectNameLabelEditPart.VISUAL_ID:
			return getStoryPatternObjectName_5026Parser();
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpression3EditPart.VISUAL_ID:
			return getStringExpression_3036Parser();
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ReleaseSemaphoreEdgeWeightEditPart.VISUAL_ID:
			return getReleaseSemaphoreEdgeWeight_6016Parser();
		case de.hpi.sam.storyDiagramEcore.diagram.edit.parts.AcquireSemaphoreEdgeWeightEditPart.VISUAL_ID:
			return getAcquireSemaphoreEdgeWeight_6018Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * 
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(de.hpi.sam.storyDiagramEcore.diagram.part.StoryDiagramEcoreVisualIDRegistry
					.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (de.hpi.sam.storyDiagramEcore.diagram.providers.StoryDiagramEcoreElementTypes
					.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
