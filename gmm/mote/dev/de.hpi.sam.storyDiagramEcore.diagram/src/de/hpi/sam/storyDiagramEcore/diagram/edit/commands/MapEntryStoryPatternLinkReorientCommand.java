package de.hpi.sam.storyDiagramEcore.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

/**
 * @generated
 */
public class MapEntryStoryPatternLinkReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	protected final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public MapEntryStoryPatternLinkReorientCommand(
			ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject && newEnd instanceof de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject target = getLink()
				.getTarget();
		if (!(getLink().eContainer() instanceof de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode container = (de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) getLink()
				.eContainer();
		return de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy
				.getLinkConstraints().canExistMapEntryStoryPatternLink_4010(
						container, getLink(), getNewSource(), target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject && newEnd instanceof de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject source = getLink()
				.getSource();
		if (!(getLink().eContainer() instanceof de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode)) {
			return false;
		}
		de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode container = (de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode) getLink()
				.eContainer();
		return de.hpi.sam.storyDiagramEcore.diagram.edit.policies.StoryDiagramEcoreBaseItemSemanticEditPolicy
				.getLinkConstraints().canExistMapEntryStoryPatternLink_4010(
						container, getLink(), source, getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setSource(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setTarget(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink getLink() {
		return (de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject getOldSource() {
		return (de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject) oldEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject getNewSource() {
		return (de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject) newEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject getOldTarget() {
		return (de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject) oldEnd;
	}

	/**
	 * @generated
	 */
	protected de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject getNewTarget() {
		return (de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject) newEnd;
	}
}
