package de.hpi.sam.storyDiagramEcore.presentation;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.ui.celleditor.ExtendedComboBoxCellEditor;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.provider.PropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;

import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage;

public class StoryDiagramEcorePropertyDescriptor extends PropertyDescriptor
{

	public StoryDiagramEcorePropertyDescriptor(Object object, IItemPropertyDescriptor itemPropertyDescriptor)
	{
		super(object, itemPropertyDescriptor);
	}

	@Override
	public CellEditor createPropertyEditor(Composite composite)
	{
		if (itemPropertyDescriptor.getFeature(object) == ExpressionsPackage.eINSTANCE.getStringExpression_ExpressionLanguage())
		{
			Collection<?> choiceOfValues = itemPropertyDescriptor.getChoiceOfValues(object);

			if (choiceOfValues == null)
			{
				choiceOfValues = new ArrayList<Object>();
			}

			return new ExtendedComboBoxCellEditor(composite, new ArrayList<Object>(choiceOfValues), getEditLabelProvider(),
					itemPropertyDescriptor.isSortChoices(object));
		}
		else
		{
			return super.createPropertyEditor(composite);
		}
	}
}
