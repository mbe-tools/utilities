package de.hpi.sam.tgg.presentation;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.RuleVariable;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.diagram.custom.TggEditConstraintDialog;
import fr.labsticc.framework.emf.core.util.EMFUtil;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class EditExpressionHandler extends AbstractHandler {
	
	/**
	 * The constructor.
	 */
	public EditExpressionHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	@Override
	public Object execute( final ExecutionEvent p_event )
	throws ExecutionException {
		final ISelection currentSelection = HandlerUtil.getCurrentSelection( p_event );
		final Expression selectedExpr = EMFUtil.selectedObject( currentSelection, Expression.class );
		
		if ( selectedExpr != null ) {
			final IEditorPart editorPart = HandlerUtil.getActiveEditor( p_event );
			final TggEditConstraintDialog dialog = new TggEditConstraintDialog( editorPart.getSite().getShell() );
			dialog.setStructuralFeature( selectedExpr.eContainingFeature() );
			dialog.setRule( findRule( selectedExpr ) );
			dialog.setExpression( selectedExpr );
			
			final EObject parent = selectedExpr.eContainer();
			final EClassifier expectedClass;
			
			if ( parent instanceof TGGRule ) {
				expectedClass = EcorePackage.eINSTANCE.getEBoolean();
			}
			else if ( parent instanceof RuleVariable ) {
				expectedClass = ( (RuleVariable) parent ).getClassifier();
			}
			else if ( parent instanceof ModelObject ) {
				expectedClass = ( (ModelObject) parent ).getClassifier();
			}
			else if ( parent instanceof AttributeAssignment ) {
				final EStructuralFeature attAssFeature = ( (AttributeAssignment) parent ).getEStructuralFeature();
				expectedClass = attAssFeature == null ? null : attAssFeature.getEType();
			}
			else {
				expectedClass = null;
			}

			dialog.setExpectedClassifier( expectedClass );
			//dialog.setContextClassifier( null );
			dialog.setExpressionOwner( selectedExpr.eContainer() );

			final EditingDomain editingDomain;
			if ( editorPart instanceof IEditingDomainProvider ) {
				editingDomain = ( (IEditingDomainProvider) editorPart ).getEditingDomain();
			}
			else {
				editingDomain = null;
			}
			
			dialog.setEditingDomain( editingDomain );
			dialog.open();
		}
		
		return null;
	}
	
	private TGGRule findRule( final Expression p_expression ) {
		EObject parent = p_expression.eContainer();
		
		while ( parent != null && !( parent instanceof TGGRule ) ) {
			parent = parent.eContainer();
		}
		
		return (TGGRule) parent;
	}
}
