/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.provider;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.SdmFactory;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;

/*
 * * This is the item provider adapter for a {@link
 * de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class StoryPatternLinkItemProvider extends
		AbstractStoryPatternLinkItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StoryPatternLinkItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addEStructuralFeaturePropertyDescriptor(object);
			addIncomingLinkOrderConstraintsPropertyDescriptor(object);
			addEOppositeReferencePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the EStructural Feature feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addEStructuralFeaturePropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(),
		// getString("_UI_StoryPatternLink_eStructuralFeature_feature"),
		// getString(
		// "_UI_PropertyDescriptor_description",
		// "_UI_StoryPatternLink_eStructuralFeature_feature",
		// "_UI_StoryPatternLink_type"),
		// SdmPackage.Literals.STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE, true,
		// false, true, null,
		// null, null));

		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_StoryPatternLink_eStructuralFeature_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_StoryPatternLink_eStructuralFeature_feature",
						"_UI_StoryPatternLink_type"),
				SdmPackage.Literals.STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE,
				true, false, true, null, null, null) {
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				StoryPatternLink storyPatternLink = (StoryPatternLink) object;

				// Return an empty list if the EClass of the source or target
				// are not set
				if (storyPatternLink.getSource() == null
						|| storyPatternLink.getTarget() == null) {
					return Collections.EMPTY_LIST;
				} else if (storyPatternLink.getSource().getClassifier() == null
						|| storyPatternLink.getTarget().getClassifier() == null) {
					return Collections.EMPTY_LIST;
				} else {
					EClassifier sourceClassifier = storyPatternLink.getSource()
							.getClassifier();
					EClassifier targetClassifier = storyPatternLink.getTarget()
							.getClassifier();

					Collection<Object> result = new LinkedList<Object>();

					if (sourceClassifier instanceof EClass) {
						EClass sourceEClass = (EClass) sourceClassifier;

						for (EStructuralFeature feat : sourceEClass
								.getEAllStructuralFeatures()) {
							if (targetClassifier instanceof EClass) {
								EClass targetEClass = (EClass) targetClassifier;

								/*
								 * If the type of the feature is the
								 * targetEClass or one of its supertypes
								 */
								if (targetEClass == feat.getEType()
										|| targetEClass.getEAllSuperTypes()
												.contains(feat.getEType())
										|| feat.getEType() == EcorePackage.eINSTANCE
												.getEJavaObject()
										|| feat.getEType() == EcorePackage.eINSTANCE
												.getEObject()) {
									// Add the feature to the result list.
									result.add(feat);
								}
							} else if (targetClassifier instanceof EDataType) {
								// TODO Hier muss noch Unterst�tzung f�r
								// primitive
								// Datentypen rein.

								if (targetClassifier == feat.getEType()) {
									result.add(feat);
								}
							}
						}
					}
					return result;
				}
			}
		});
	}

	/**
	 * This adds a property descriptor for the Incoming Link Order Constraints feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIncomingLinkOrderConstraintsPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_StoryPatternLink_incomingLinkOrderConstraints_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_StoryPatternLink_incomingLinkOrderConstraints_feature",
								"_UI_StoryPatternLink_type"),
						SdmPackage.Literals.STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the EOpposite Reference feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEOppositeReferencePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_StoryPatternLink_eOppositeReference_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_StoryPatternLink_eOppositeReference_feature",
						"_UI_StoryPatternLink_type"),
				SdmPackage.Literals.STORY_PATTERN_LINK__EOPPOSITE_REFERENCE,
				true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(SdmPackage.Literals.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT);
			childrenFeatures
					.add(SdmPackage.Literals.STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS);
			childrenFeatures
					.add(SdmPackage.Literals.STORY_PATTERN_LINK__EXTERNAL_REFERENCE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns StoryPatternLink.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/StoryPatternLink"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public String getText(Object object) {
		// String label = ((StoryPatternLink) object).getName();
		// return label == null || label.length() == 0 ?
		// getString("_UI_StoryPatternLink_type") :
		// getString("_UI_StoryPatternLink_type") + " "
		// + label;

		String label = ((AbstractStoryPatternLink) object).getName();

		if (((AbstractStoryPatternLink) object).getModifier() == StoryPatternModifierEnumeration.CREATE) {
			label = "<<CREATE>> " + label;
		} else if (((AbstractStoryPatternLink) object).getModifier() == StoryPatternModifierEnumeration.DESTROY) {
			label = "<<DESTROY>> " + label;
		}

		return label == null || label.length() == 0 ? getString("_UI_StoryPatternLink_type")
				: getString("_UI_StoryPatternLink_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(StoryPatternLink.class)) {
		case SdmPackage.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT:
		case SdmPackage.STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS:
		case SdmPackage.STORY_PATTERN_LINK__EXTERNAL_REFERENCE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						SdmPackage.Literals.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT,
						SdmFactory.eINSTANCE.createLinkPositionConstraint()));

		newChildDescriptors
				.add(createChildParameter(
						SdmPackage.Literals.STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS,
						SdmFactory.eINSTANCE.createLinkOrderConstraint()));

		newChildDescriptors.add(createChildParameter(
				SdmPackage.Literals.STORY_PATTERN_LINK__EXTERNAL_REFERENCE,
				SdmFactory.eINSTANCE.createExternalReference()));
	}

}
