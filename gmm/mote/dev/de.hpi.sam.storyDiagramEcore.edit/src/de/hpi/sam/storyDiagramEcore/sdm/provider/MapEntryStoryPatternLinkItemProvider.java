/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

import de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;

/**
 * This is the item provider adapter for a {@link de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class MapEntryStoryPatternLinkItemProvider extends
		AbstractStoryPatternLinkItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MapEntryStoryPatternLinkItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addValueTargetPropertyDescriptor(object);
			addEStructuralFeaturePropertyDescriptor(object);
			addClassifierPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Value Target feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addValueTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MapEntryStoryPatternLink_valueTarget_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MapEntryStoryPatternLink_valueTarget_feature",
						"_UI_MapEntryStoryPatternLink_type"),
				SdmPackage.Literals.MAP_ENTRY_STORY_PATTERN_LINK__VALUE_TARGET,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the EStructural Feature feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addEStructuralFeaturePropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(),
		// getString("_UI_MapEntryStoryPatternLink_eStructuralFeature_feature"),
		// getString(
		// "_UI_PropertyDescriptor_description",
		// "_UI_MapEntryStoryPatternLink_eStructuralFeature_feature",
		// "_UI_MapEntryStoryPatternLink_type"),
		// SdmPackage.Literals.MAP_ENTRY_STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE,
		// true,
		// false, true, null, null, null));

		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_MapEntryStoryPatternLink_eStructuralFeature_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_MapEntryStoryPatternLink_eStructuralFeature_feature",
								"_UI_MapEntryStoryPatternLink_type"),
						SdmPackage.Literals.MAP_ENTRY_STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE,
						true, false, true, null, null, null) {

					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						MapEntryStoryPatternLink storyPatternLink = (MapEntryStoryPatternLink) object;

						// Return an empty list if the EClass of the source or target
						// are not set
						if (storyPatternLink.getClassifier() == null
								|| storyPatternLink.getSource() == null
								|| storyPatternLink.getSource().getClassifier() == null) {
							return Collections.EMPTY_LIST;
						} else {
							EClassifier sourceClassifier = storyPatternLink
									.getSource().getClassifier();
							EClassifier targetClassifier = storyPatternLink
									.getClassifier();

							Collection<Object> result = new LinkedList<Object>();

							if (sourceClassifier instanceof EClass) {
								EClass sourceEClass = (EClass) sourceClassifier;

								for (EStructuralFeature feat : sourceEClass
										.getEAllStructuralFeatures()) {
									if (targetClassifier instanceof EClass) {
										EClass targetEClass = (EClass) targetClassifier;

										// If the type of the feature is the
										// targetEClass or
										// one
										// of its supertypes
										if (targetEClass == feat.getEType()
												|| targetEClass
														.getEAllSuperTypes()
														.contains(
																feat.getEType())
												|| feat.getEType() == EcorePackage.eINSTANCE
														.getEJavaObject()
												|| feat.getEType() == EcorePackage.eINSTANCE
														.getEObject()) {
											// Add the feature to the result list.
											result.add(feat);
										}
									} else if (targetClassifier instanceof EDataType) {
										// TODO Hier muss noch Unterst�tzung f�r
										// primitive
										// Datentypen rein.

										if (targetClassifier == feat.getEType()) {
											result.add(feat);
										}
									}
								}
							}
							return result;
						}
					}
				});
	}

	/**
	 * This adds a property descriptor for the Classifier feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addClassifierPropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(
		// ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(),
		// getString("_UI_MapEntryStoryPatternLink_classifier_feature"),
		// getString("_UI_PropertyDescriptor_description",
		// "_UI_MapEntryStoryPatternLink_classifier_feature",
		// "_UI_MapEntryStoryPatternLink_type"),
		// SdmPackage.Literals.MAP_ENTRY_STORY_PATTERN_LINK__CLASSIFIER, true,
		// false,
		// true, null, null, null));

		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MapEntryStoryPatternLink_classifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MapEntryStoryPatternLink_classifier_feature",
						"_UI_MapEntryStoryPatternLink_type"),
				SdmPackage.Literals.MAP_ENTRY_STORY_PATTERN_LINK__CLASSIFIER,
				true, false, true, null, null, null) {
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				MapEntryStoryPatternLink link = (MapEntryStoryPatternLink) object;

				if (link.getSource() != null
						&& link.getSource().getClassifier() != null
						&& link.getSource().getClassifier() instanceof EClass) {
					EClass sourceEClass = (EClass) link.getSource()
							.getClassifier();

					Collection<Object> returnList = new ArrayList<Object>();

					for (EReference eReference : sourceEClass
							.getEAllReferences()) {
						if (EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY
								.getInstanceTypeName().equals(
										eReference.getEType()
												.getInstanceTypeName())) {
							returnList.add(eReference.getEType());
						}
					}

					return returnList;
				} else {
					return Collections.EMPTY_LIST;
				}
			}
		});
	}

	/**
	 * This returns MapEntryStoryPatternLink.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(
				object,
				getResourceLocator().getImage(
						"full/obj16/MapEntryStoryPatternLink"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((MapEntryStoryPatternLink) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_MapEntryStoryPatternLink_type")
				: getString("_UI_MapEntryStoryPatternLink_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
