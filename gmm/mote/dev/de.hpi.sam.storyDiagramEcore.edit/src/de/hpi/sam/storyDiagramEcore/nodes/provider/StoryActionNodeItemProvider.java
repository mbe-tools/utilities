/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsFactory;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.SdmFactory;

/*
 * * This is the item provider adapter for a {@link
 * de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class StoryActionNodeItemProvider extends ActivityNodeItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StoryActionNodeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addForEachPropertyDescriptor(object);
			addNacSemanticPropertyDescriptor(object);
			addForEachSemanticsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the For Each feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addForEachPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_StoryActionNode_forEach_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_StoryActionNode_forEach_feature",
						"_UI_StoryActionNode_type"),
				NodesPackage.Literals.STORY_ACTION_NODE__FOR_EACH, true, false,
				false, ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Nac Semantic feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addNacSemanticPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_StoryActionNode_nacSemantic_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_StoryActionNode_nacSemantic_feature",
						"_UI_StoryActionNode_type"),
				NodesPackage.Literals.STORY_ACTION_NODE__NAC_SEMANTIC, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the For Each Semantics feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addForEachSemanticsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_StoryActionNode_forEachSemantics_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_StoryActionNode_forEachSemantics_feature",
						"_UI_StoryActionNode_type"),
				NodesPackage.Literals.STORY_ACTION_NODE__FOR_EACH_SEMANTICS,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(NodesPackage.Literals.STORY_ACTION_NODE__STORY_PATTERN_OBJECTS);
			childrenFeatures
					.add(NodesPackage.Literals.STORY_ACTION_NODE__STORY_PATTERN_LINKS);
			childrenFeatures
					.add(NodesPackage.Literals.STORY_ACTION_NODE__CONSTRAINTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns StoryActionNode.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/StoryActionNode"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((StoryActionNode) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_StoryActionNode_type")
				: getString("_UI_StoryActionNode_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(StoryActionNode.class)) {
		case NodesPackage.STORY_ACTION_NODE__FOR_EACH:
		case NodesPackage.STORY_ACTION_NODE__NAC_SEMANTIC:
		case NodesPackage.STORY_ACTION_NODE__FOR_EACH_SEMANTICS:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_OBJECTS:
		case NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_LINKS:
		case NodesPackage.STORY_ACTION_NODE__CONSTRAINTS:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				NodesPackage.Literals.STORY_ACTION_NODE__STORY_PATTERN_OBJECTS,
				SdmFactory.eINSTANCE.createStoryPatternObject()));

		newChildDescriptors.add(createChildParameter(
				NodesPackage.Literals.STORY_ACTION_NODE__STORY_PATTERN_LINKS,
				SdmFactory.eINSTANCE.createStoryPatternLink()));

		newChildDescriptors.add(createChildParameter(
				NodesPackage.Literals.STORY_ACTION_NODE__STORY_PATTERN_LINKS,
				SdmFactory.eINSTANCE.createStoryPatternContainmentLink()));

		newChildDescriptors.add(createChildParameter(
				NodesPackage.Literals.STORY_ACTION_NODE__STORY_PATTERN_LINKS,
				SdmFactory.eINSTANCE.createStoryPatternExpressionLink()));

		newChildDescriptors.add(createChildParameter(
				NodesPackage.Literals.STORY_ACTION_NODE__STORY_PATTERN_LINKS,
				SdmFactory.eINSTANCE.createMapEntryStoryPatternLink()));

		newChildDescriptors.add(createChildParameter(
				NodesPackage.Literals.STORY_ACTION_NODE__STORY_PATTERN_LINKS,
				SdmFactory.eINSTANCE.createEContainerStoryPatternLink()));

		newChildDescriptors.add(createChildParameter(
				NodesPackage.Literals.STORY_ACTION_NODE__CONSTRAINTS,
				ExpressionsFactory.eINSTANCE.createCallActionExpression()));

		newChildDescriptors.add(createChildParameter(
				NodesPackage.Literals.STORY_ACTION_NODE__CONSTRAINTS,
				ExpressionsFactory.eINSTANCE.createStringExpression()));
	}

}
