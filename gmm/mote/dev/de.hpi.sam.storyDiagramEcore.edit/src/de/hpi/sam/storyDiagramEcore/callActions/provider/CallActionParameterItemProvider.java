/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.provider;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsFactory;
import de.hpi.sam.storyDiagramEcore.provider.NamedElementItemProvider;
import de.hpi.sam.storyDiagramEcore.provider.StoryDiagramEcoreEditPlugin;

/*
 * * This is the item provider adapter for a {@link
 * de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class CallActionParameterItemProvider extends NamedElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CallActionParameterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addParameterClassfierPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Parameter Classfier feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addParameterClassfierPropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(),
		// getString("_UI_CallActionParameter_parameterClassfier_feature"),
		// getString(
		// "_UI_PropertyDescriptor_description",
		// "_UI_CallActionParameter_parameterClassfier_feature",
		// "_UI_CallActionParameter_type"),
		// CallActionsPackage.Literals.CALL_ACTION_PARAMETER__PARAMETER_CLASSFIER,
		// true,
		// false, true, null, null, null));

		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_CallActionParameter_parameterClassfier_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_CallActionParameter_parameterClassfier_feature",
								"_UI_CallActionParameter_type"),
						CallActionsPackage.Literals.CALL_ACTION_PARAMETER__PARAMETER_CLASSFIER,
						true, false, true, null, null, null) {
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						Collection<?> result = super.getChoiceOfValues(object);
						if (feature instanceof EReference
								&& object instanceof EObject) {
							@SuppressWarnings("unchecked")
							List<EObject> eObjects = (List<EObject>) (List<?>) new LinkedList<Object>(
									result);
							Resource resource = ((EObject) object).eResource();
							if (resource != null) {
								ResourceSet resourceSet = resource
										.getResourceSet();
								if (resourceSet != null) {
									Collection<EObject> visited = new HashSet<EObject>(
											eObjects);
									Registry packageRegistry = resourceSet
											.getPackageRegistry();
									for (String nsURI : packageRegistry
											.keySet()) {
										collectReachableObjectsOfType(visited,
												eObjects, packageRegistry
														.getEPackage(nsURI),
												feature.getEType());
									}
								}
							}
							result = eObjects;
						}
						return result;
					}
				});
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(CallActionsPackage.Literals.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns CallActionParameter.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/CallActionParameter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((CallActionParameter) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_CallActionParameter_type")
				: getString("_UI_CallActionParameter_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CallActionParameter.class)) {
		case CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						CallActionsPackage.Literals.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION,
						ExpressionsFactory.eINSTANCE
								.createCallActionExpression()));

		newChildDescriptors
				.add(createChildParameter(
						CallActionsPackage.Literals.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION,
						ExpressionsFactory.eINSTANCE.createStringExpression()));
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return StoryDiagramEcoreEditPlugin.INSTANCE;
	}

}
