/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsFactory;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;

/**
 * This is the item provider adapter for a {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class AbstractStoryPatternObjectItemProvider extends
		StoryPatternElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource, IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public AbstractStoryPatternObjectItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addClassifierPropertyDescriptor(object);
			addOutgoingStoryLinksPropertyDescriptor(object);
			addIncomingStoryLinksPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Classifier feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addClassifierPropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(),
		// getString("_UI_AbstractStoryPatternObject_classifier_feature"),
		// getString(
		// "_UI_PropertyDescriptor_description",
		// "_UI_AbstractStoryPatternObject_classifier_feature",
		// "_UI_AbstractStoryPatternObject_type"),
		// SdmPackage.Literals.ABSTRACT_STORY_PATTERN_OBJECT__CLASSIFIER, true,
		// false,
		// true, null, null, null));

		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AbstractStoryPatternObject_classifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AbstractStoryPatternObject_classifier_feature",
						"_UI_AbstractStoryPatternObject_type"),
				SdmPackage.Literals.ABSTRACT_STORY_PATTERN_OBJECT__CLASSIFIER,
				true, false, true, null, null, null) {
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				Collection<?> result = super.getChoiceOfValues(object);
				if (feature instanceof EReference && object instanceof EObject) {
					@SuppressWarnings("unchecked")
					List<EObject> eObjects = (List<EObject>) (List<?>) new LinkedList<Object>(
							result);
					Resource resource = ((EObject) object).eResource();
					if (resource != null) {
						ResourceSet resourceSet = resource.getResourceSet();
						if (resourceSet != null) {
							Collection<EObject> visited = new HashSet<EObject>(
									eObjects);
							Registry packageRegistry = resourceSet
									.getPackageRegistry();
							for (String nsURI : packageRegistry.keySet()) {
								collectReachableObjectsOfType(visited,
										eObjects,
										packageRegistry.getEPackage(nsURI),
										feature.getEType());
							}
						}
					}
					result = eObjects;
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Outgoing Story Links feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addOutgoingStoryLinksPropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(),
		// getString("_UI_AbstractStoryPatternObject_outgoingStoryLinks_feature"),
		// getString(
		// "_UI_PropertyDescriptor_description",
		// "_UI_AbstractStoryPatternObject_outgoingStoryLinks_feature",
		// "_UI_AbstractStoryPatternObject_type"),
		// SdmPackage.Literals.ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS,
		// true, false, true, null, null, null));

		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AbstractStoryPatternObject_outgoingStoryLinks_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AbstractStoryPatternObject_outgoingStoryLinks_feature",
								"_UI_AbstractStoryPatternObject_type"),
						SdmPackage.Literals.ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS,
						true, false, true, null, null, null) {
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						AbstractStoryPatternObject aspo = (AbstractStoryPatternObject) object;
						StoryActionNode storyActionNode = (StoryActionNode) aspo
								.eContainer();

						Collection<Object> returnValues = new ArrayList<Object>(
								storyActionNode.getStoryPatternLinks());

						Object o = null;
						for (Iterator<?> it = returnValues.iterator(); it
								.hasNext();) {
							o = it.next();

							if (aspo.getOutgoingStoryLinks().contains(o)) {
								it.remove();
							}
						}

						return returnValues;

					}
				});
	}

	/**
	 * This adds a property descriptor for the Incoming Story Links feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addIncomingStoryLinksPropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(),
		// getString("_UI_AbstractStoryPatternObject_incomingStoryLinks_feature"),
		// getString(
		// "_UI_PropertyDescriptor_description",
		// "_UI_AbstractStoryPatternObject_incomingStoryLinks_feature",
		// "_UI_AbstractStoryPatternObject_type"),
		// SdmPackage.Literals.ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS,
		// true, false, true, null, null, null));

		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AbstractStoryPatternObject_incomingStoryLinks_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AbstractStoryPatternObject_incomingStoryLinks_feature",
								"_UI_AbstractStoryPatternObject_type"),
						SdmPackage.Literals.ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS,
						true, false, true, null, null, null) {
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						AbstractStoryPatternObject aspo = (AbstractStoryPatternObject) object;
						StoryActionNode storyActionNode = (StoryActionNode) aspo
								.eContainer();

						Collection<Object> returnValues = new ArrayList<Object>(
								storyActionNode.getStoryPatternLinks());

						Object o = null;
						for (Iterator<?> it = returnValues.iterator(); it
								.hasNext();) {
							o = it.next();

							if (aspo.getIncomingStoryLinks().contains(o)) {
								it.remove();
							}
						}

						return returnValues;
					}
				});
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(SdmPackage.Literals.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public String getText(Object object) {
		// String label = ((AbstractStoryPatternObject) object).getName();
		// return label == null || label.length() == 0 ?
		// getString("_UI_AbstractStoryPatternObject_type")
		// : getString("_UI_AbstractStoryPatternObject_type") + " " + label;

		String label = ((AbstractStoryPatternObject) object).getName();

		if (((AbstractStoryPatternObject) object).getModifier() == StoryPatternModifierEnumeration.CREATE) {
			label = "<<CREATE>> " + label;
		} else if (((AbstractStoryPatternObject) object).getModifier() == StoryPatternModifierEnumeration.DESTROY) {
			label = "<<DESTROY>> " + label;
		}

		return label == null || label.length() == 0 ? getString("_UI_StoryPatternObject_type")
				: getString("_UI_StoryPatternObject_type") + " " + label;

	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AbstractStoryPatternObject.class)) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						SdmPackage.Literals.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION,
						ExpressionsFactory.eINSTANCE
								.createCallActionExpression()));

		newChildDescriptors
				.add(createChildParameter(
						SdmPackage.Literals.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION,
						ExpressionsFactory.eINSTANCE.createStringExpression()));
	}

}
