/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;

/**
 * This is the item provider adapter for a {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class AbstractStoryPatternLinkItemProvider extends
		StoryPatternElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource, IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public AbstractStoryPatternLinkItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSourcePropertyDescriptor(object);
			addTargetPropertyDescriptor(object);
			addCheckOnlyExistencePropertyDescriptor(object);
			addMatchingPriorityPropertyDescriptor(object);
			addOppositeStoryPatternLinkPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Source feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addSourcePropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(),
		// getString("_UI_AbstractStoryPatternLink_source_feature"), getString(
		// "_UI_PropertyDescriptor_description",
		// "_UI_AbstractStoryPatternLink_source_feature",
		// "_UI_AbstractStoryPatternLink_type"),
		// SdmPackage.Literals.ABSTRACT_STORY_PATTERN_LINK__SOURCE, true, false,
		// true,
		// null, null, null));

		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AbstractStoryPatternLink_source_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AbstractStoryPatternLink_source_feature",
						"_UI_AbstractStoryPatternLink_type"),
				SdmPackage.Literals.ABSTRACT_STORY_PATTERN_LINK__SOURCE, true,
				false, true, null, null, null) {
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				StoryActionNode storyActionNode = (StoryActionNode) ((AbstractStoryPatternLink) object)
						.eContainer();

				Collection<Object> returnValues = new ArrayList<Object>();

				returnValues.addAll(storyActionNode.getStoryPatternObjects());

				return returnValues;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Target feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addTargetPropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(),
		// getString("_UI_AbstractStoryPatternLink_target_feature"), getString(
		// "_UI_PropertyDescriptor_description",
		// "_UI_AbstractStoryPatternLink_target_feature",
		// "_UI_AbstractStoryPatternLink_type"),
		// SdmPackage.Literals.ABSTRACT_STORY_PATTERN_LINK__TARGET, true, false,
		// true,
		// null, null, null));

		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AbstractStoryPatternLink_target_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AbstractStoryPatternLink_target_feature",
						"_UI_AbstractStoryPatternLink_type"),
				SdmPackage.Literals.ABSTRACT_STORY_PATTERN_LINK__TARGET, true,
				false, true, null, null, null) {
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				StoryActionNode storyActionNode = (StoryActionNode) ((AbstractStoryPatternLink) object)
						.eContainer();

				Collection<Object> returnValues = new ArrayList<Object>();

				returnValues.addAll(storyActionNode.getStoryPatternObjects());

				return returnValues;
			}

		});

	}

	/**
	 * This adds a property descriptor for the Check Only Existence feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCheckOnlyExistencePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AbstractStoryPatternLink_checkOnlyExistence_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AbstractStoryPatternLink_checkOnlyExistence_feature",
								"_UI_AbstractStoryPatternLink_type"),
						SdmPackage.Literals.ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE,
						true, false, false,
						ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Matching Priority feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addMatchingPriorityPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AbstractStoryPatternLink_matchingPriority_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AbstractStoryPatternLink_matchingPriority_feature",
								"_UI_AbstractStoryPatternLink_type"),
						SdmPackage.Literals.ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY,
						true, false, false,
						ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Opposite Story Pattern Link feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOppositeStoryPatternLinkPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AbstractStoryPatternLink_oppositeStoryPatternLink_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AbstractStoryPatternLink_oppositeStoryPatternLink_feature",
								"_UI_AbstractStoryPatternLink_type"),
						SdmPackage.Literals.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK,
						true, false, true, null, null, null));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public String getText(Object object) {
		// String label = ((AbstractStoryPatternLink) object).getName();
		// return label == null || label.length() == 0 ?
		// getString("_UI_AbstractStoryPatternLink_type")
		// : getString("_UI_AbstractStoryPatternLink_type") + " " + label;
		//
		String label = ((StoryPatternLink) object).getName();

		if (((AbstractStoryPatternLink) object).getModifier() == StoryPatternModifierEnumeration.CREATE) {
			label = "<<CREATE>> " + label;
		} else if (((AbstractStoryPatternLink) object).getModifier() == StoryPatternModifierEnumeration.DESTROY) {
			label = "<<DESTROY>> " + label;
		}

		return label == null || label.length() == 0 ? getString("_UI_AbstractStoryPatternLink_type")
				: getString("_UI_AbstractStoryPatternLink_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AbstractStoryPatternLink.class)) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE:
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
