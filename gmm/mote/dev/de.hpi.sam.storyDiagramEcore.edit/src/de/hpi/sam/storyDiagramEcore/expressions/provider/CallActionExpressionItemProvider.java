/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.expressions.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionsFactory;
import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage;

/*
 * * This is the item provider adapter for a {@link
 * de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class CallActionExpressionItemProvider extends ExpressionItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CallActionExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION__CALL_ACTIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns CallActionExpression.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator()
						.getImage("full/obj16/CallActionExpression"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((CallActionExpression) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_CallActionExpression_type")
				: getString("_UI_CallActionExpression_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CallActionExpression.class)) {
		case ExpressionsPackage.CALL_ACTION_EXPRESSION__CALL_ACTIONS:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION__CALL_ACTIONS,
						CallActionsFactory.eINSTANCE
								.createCallStoryDiagramInterpreterAction()));

		newChildDescriptors
				.add(createChildParameter(
						ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION__CALL_ACTIONS,
						CallActionsFactory.eINSTANCE
								.createLiteralDeclarationAction()));

		newChildDescriptors
				.add(createChildParameter(
						ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION__CALL_ACTIONS,
						CallActionsFactory.eINSTANCE.createMethodCallAction()));

		newChildDescriptors
				.add(createChildParameter(
						ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION__CALL_ACTIONS,
						CallActionsFactory.eINSTANCE.createNewObjectAction()));

		newChildDescriptors
				.add(createChildParameter(
						ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION__CALL_ACTIONS,
						CallActionsFactory.eINSTANCE
								.createVariableReferenceAction()));

		newChildDescriptors
				.add(createChildParameter(
						ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION__CALL_ACTIONS,
						CallActionsFactory.eINSTANCE
								.createVariableDeclarationAction()));

		newChildDescriptors
				.add(createChildParameter(
						ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION__CALL_ACTIONS,
						CallActionsFactory.eINSTANCE.createCompareAction()));

		newChildDescriptors
				.add(createChildParameter(
						ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION__CALL_ACTIONS,
						CallActionsFactory.eINSTANCE.createNullValueAction()));

		newChildDescriptors
				.add(createChildParameter(
						ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION__CALL_ACTIONS,
						CallActionsFactory.eINSTANCE.createOperationAction()));

		newChildDescriptors
				.add(createChildParameter(
						ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION__CALL_ACTIONS,
						CallActionsFactory.eINSTANCE
								.createGetPropertyValueAction()));
	}

}
