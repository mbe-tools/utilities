package de.hpi.sam.storyDiagramEcore.edit.command;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.CopyCommand.Helper;
import org.eclipse.emf.edit.domain.EditingDomain;

import de.hpi.sam.storyDiagramEcore.NamedElement;

public class InitializeCopyCommand extends org.eclipse.emf.edit.command.InitializeCopyCommand
{

	public InitializeCopyCommand(EditingDomain domain, EObject owner, Helper copyHelper)
	{
		super(domain, owner, copyHelper);
	}

	protected void copyAttributes()
	{
		super.copyAttributes();

		if (this.copy instanceof NamedElement)
		{
			((NamedElement) this.copy).setUuid(EcoreUtil.generateUUID());
		}
	}

}
