/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.provider;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.storyDiagramEcore.ActivityParameter;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;

/**
 * This is the item provider adapter for a
 * {@link de.hpi.sam.storyDiagramEcore.ActivityParameter} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class ActivityParameterItemProvider extends NamedElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ActivityParameterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTypePropertyDescriptor(object);
			addDirectionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Type feature. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addTypePropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(),
		// getString("_UI_ActivityParameter_type_feature"),
		// getString("_UI_PropertyDescriptor_description",
		// "_UI_ActivityParameter_type_feature", "_UI_ActivityParameter_type"),
		// StoryDiagramEcorePackage.Literals.ACTIVITY_PARAMETER__TYPE, true,
		// false, true, null, null, null));

		this.itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) this.adapterFactory)
						.getRootAdapterFactory(), this.getResourceLocator(),
				this.getString("_UI_ActivityParameter_type_feature"), this
						.getString("_UI_PropertyDescriptor_description",
								"_UI_ActivityParameter_type_feature",
								"_UI_ActivityParameter_type"),
				StoryDiagramEcorePackage.Literals.ACTIVITY_PARAMETER__TYPE,
				true, false, true, null, null, null) {

			@Override
			protected Collection<?> getComboBoxObjects(Object object) {
				Collection<?> result = super.getComboBoxObjects(object);
				if (this.feature instanceof EReference
						&& object instanceof EObject) {
					@SuppressWarnings("unchecked")
					List<EObject> eObjects = (List<EObject>) (List<?>) new LinkedList<Object>(
							result);
					Resource resource = ((EObject) object).eResource();
					if (resource != null) {
						ResourceSet resourceSet = resource.getResourceSet();
						if (resourceSet != null) {
							Collection<EObject> visited = new HashSet<EObject>(
									eObjects);
							Registry packageRegistry = resourceSet
									.getPackageRegistry();
							for (String nsURI : packageRegistry.keySet()) {
								collectReachableObjectsOfType(visited,
										eObjects,
										packageRegistry.getEPackage(nsURI),
										this.feature.getEType());
							}
						}
					}
					result = eObjects;
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Direction feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDirectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ActivityParameter_direction_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_ActivityParameter_direction_feature",
								"_UI_ActivityParameter_type"),
						StoryDiagramEcorePackage.Literals.ACTIVITY_PARAMETER__DIRECTION,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This returns ActivityParameter.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/ActivityParameter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ActivityParameter) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_ActivityParameter_type")
				: getString("_UI_ActivityParameter_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ActivityParameter.class)) {
		case StoryDiagramEcorePackage.ACTIVITY_PARAMETER__DIRECTION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
