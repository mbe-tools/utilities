/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.expressions.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreFactory;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreUtils;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsFactory;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsFactory;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage;
import de.hpi.sam.storyDiagramEcore.expressions.StringExpression;
import de.hpi.sam.storyDiagramEcore.helpers.HelpersFactory;
import de.hpi.sam.storyDiagramEcore.helpers.HelpersPackage;
import de.hpi.sam.storyDiagramEcore.nodes.NodesFactory;
import de.hpi.sam.storyDiagramEcore.sdm.SdmFactory;

/*
 * * This is the item provider adapter for a {@link
 * de.hpi.sam.storyDiagramEcore.expressions.StringExpression} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class StringExpressionItemProvider extends ExpressionItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StringExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addExpressionStringPropertyDescriptor(object);
			addExpressionLanguagePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Expression String feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addExpressionStringPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_StringExpression_expressionString_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_StringExpression_expressionString_feature",
								"_UI_StringExpression_type"),
						ExpressionsPackage.Literals.STRING_EXPRESSION__EXPRESSION_STRING,
						true, true, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Expression Language feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addExpressionLanguagePropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory)
		// .getRootAdapterFactory(), getResourceLocator(),
		// getString("_UI_StringExpression_expressionLanguage_feature"),
		// getString(
		// "_UI_PropertyDescriptor_description",
		// "_UI_StringExpression_expressionLanguage_feature",
		// "_UI_StringExpression_type"),
		// ExpressionsPackage.Literals.STRING_EXPRESSION__EXPRESSION_LANGUAGE,
		// true, false, false,
		// ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));

		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_StringExpression_expressionLanguage_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_StringExpression_expressionLanguage_feature",
								"_UI_StringExpression_type"),
						ExpressionsPackage.Literals.STRING_EXPRESSION__EXPRESSION_LANGUAGE,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null) {
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						return StoryDiagramEcoreUtils
								.getAvailableExpressionLanguages();
					}
				});
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(ExpressionsPackage.Literals.STRING_EXPRESSION__AST);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns StringExpression.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/StringExpression"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public String getText(Object object) {
		String label = getString("_UI_StringExpression_type");

		StringExpression se = (StringExpression) object;

		if (se.getExpressionLanguage() != null
				&& !"".equals(se.getExpressionLanguage())) {
			label += " " + se.getExpressionLanguage() + ": ";
		} else {
			label += " [null]: ";
		}

		if (se.getExpressionString() != null
				&& !"".equals(se.getExpressionString())) {
			label += se.getExpressionString();
		} else {
			label += "[null]";
		}

		return label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(StringExpression.class)) {
		case ExpressionsPackage.STRING_EXPRESSION__EXPRESSION_STRING:
		case ExpressionsPackage.STRING_EXPRESSION__EXPRESSION_LANGUAGE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case ExpressionsPackage.STRING_EXPRESSION__AST:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				ExpressionsFactory.eINSTANCE.createCallActionExpression()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				ExpressionsFactory.eINSTANCE.createStringExpression()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				StoryDiagramEcoreFactory.eINSTANCE.createActivity()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				StoryDiagramEcoreFactory.eINSTANCE.createActivityDiagram()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				StoryDiagramEcoreFactory.eINSTANCE.createExpressionImport()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				StoryDiagramEcoreFactory.eINSTANCE.createActivityParameter()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				CallActionsFactory.eINSTANCE.createCallActionParameter()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				CallActionsFactory.eINSTANCE
						.createCallStoryDiagramInterpreterAction()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				CallActionsFactory.eINSTANCE.createLiteralDeclarationAction()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				CallActionsFactory.eINSTANCE.createMethodCallAction()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				CallActionsFactory.eINSTANCE.createNewObjectAction()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				CallActionsFactory.eINSTANCE.createVariableReferenceAction()));

		newChildDescriptors
				.add(createChildParameter(
						ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
						CallActionsFactory.eINSTANCE
								.createVariableDeclarationAction()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				CallActionsFactory.eINSTANCE.createCompareAction()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				CallActionsFactory.eINSTANCE.createNullValueAction()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				CallActionsFactory.eINSTANCE.createOperationAction()));

		newChildDescriptors
				.add(createChildParameter(
						ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
						CallActionsFactory.eINSTANCE
								.createCaseInsensitiveComparator()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				CallActionsFactory.eINSTANCE.createGetPropertyValueAction()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createActivityEdge()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createActivityFinalNode()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createFlowFinalNode()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createExpressionActivityNode()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createDecisionNode()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createInitialNode()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createMergeNode()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createStoryActionNode()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createForkNode()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createJoinNode()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createSemaphore()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createReleaseSemaphoreEdge()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				NodesFactory.eINSTANCE.createAcquireSemaphoreEdge()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				SdmFactory.eINSTANCE.createAttributeAssignment()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				SdmFactory.eINSTANCE.createStoryPatternLink()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				SdmFactory.eINSTANCE.createStoryPatternContainmentLink()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				SdmFactory.eINSTANCE.createStoryPatternExpressionLink()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				SdmFactory.eINSTANCE.createStoryPatternObject()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				SdmFactory.eINSTANCE.createMapEntryStoryPatternLink()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				SdmFactory.eINSTANCE.createEContainerStoryPatternLink()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				SdmFactory.eINSTANCE.createLinkPositionConstraint()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				SdmFactory.eINSTANCE.createLinkOrderConstraint()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.STRING_EXPRESSION__AST,
				HelpersFactory.eINSTANCE
						.create(HelpersPackage.Literals.MAP_ENTRY)));
	}

}
