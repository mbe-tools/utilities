/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;

/*
 * * This is the item provider adapter for a {@link
 * de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class StoryPatternContainmentLinkItemProvider extends
		AbstractStoryPatternLinkItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StoryPatternContainmentLinkItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This returns StoryPatternContainmentLink.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(
				object,
				getResourceLocator().getImage(
						"full/obj16/StoryPatternContainmentLink"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public String getText(Object object) {
		// String label = ((StoryPatternContainmentLink) object).getName();
		// return label == null || label.length() == 0 ?
		// getString("_UI_StoryPatternContainmentLink_type")
		// : getString("_UI_StoryPatternContainmentLink_type") + " " + label;

		String label = ((AbstractStoryPatternLink) object).getName();

		if (((AbstractStoryPatternLink) object).getModifier() == StoryPatternModifierEnumeration.CREATE) {
			label = "<<CREATE>> " + label;
		} else if (((AbstractStoryPatternLink) object).getModifier() == StoryPatternModifierEnumeration.DESTROY) {
			label = "<<DESTROY>> " + label;
		}

		return label == null || label.length() == 0 ? getString("_UI_StoryPatternContainmentLink_type")
				: getString("_UI_StoryPatternContainmentLink_type") + " "
						+ label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
