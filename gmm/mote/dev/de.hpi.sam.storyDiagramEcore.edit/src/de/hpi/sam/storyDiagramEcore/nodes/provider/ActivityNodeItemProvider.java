/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.provider;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.provider.NamedElementItemProvider;
import de.hpi.sam.storyDiagramEcore.provider.StoryDiagramEcoreEditPlugin;

/*
 * * This is the item provider adapter for a {@link
 * de.hpi.sam.storyDiagramEcore.nodes.ActivityNode} object. <!-- begin-user-doc
 * --> <!-- end-user-doc -->
 * @generated
 */
public class ActivityNodeItemProvider extends NamedElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ActivityNodeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addIncomingPropertyDescriptor(object);
			addOutgoingPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Incoming feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	protected void addIncomingPropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(
		// ((ComposeableAdapterFactory) adapterFactory)
		// .getRootAdapterFactory(), getResourceLocator(),
		// getString("_UI_ActivityNode_incoming_feature"), getString(
		// "_UI_PropertyDescriptor_description",
		// "_UI_ActivityNode_incoming_feature",
		// "_UI_ActivityNode_type"),
		// NodesPackage.Literals.ACTIVITY_NODE__INCOMING, true, false,
		// true, null, null, null));

		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ActivityNode_incoming_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_ActivityNode_incoming_feature",
						"_UI_ActivityNode_type"),
				NodesPackage.Literals.ACTIVITY_NODE__INCOMING, true, false,
				true, null, null, null) {
			public Collection<?> getChoiceOfValues(Object object) {
				Collection<?> values = super.getChoiceOfValues(object);
				ActivityNode node = (ActivityNode) object;

				Object o = null;
				for (Iterator<?> it = values.iterator(); it.hasNext();) {
					o = it.next();

					if (node.getIncoming().contains(o)) {
						it.remove();
					}
				}

				return values;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Outgoing feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	protected void addOutgoingPropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(
		// ((ComposeableAdapterFactory) adapterFactory)
		// .getRootAdapterFactory(), getResourceLocator(),
		// getString("_UI_ActivityNode_outgoing_feature"), getString(
		// "_UI_PropertyDescriptor_description",
		// "_UI_ActivityNode_outgoing_feature",
		// "_UI_ActivityNode_type"),
		// NodesPackage.Literals.ACTIVITY_NODE__OUTGOING, true, false,
		// true, null, null, null));

		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ActivityNode_outgoing_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_ActivityNode_outgoing_feature",
						"_UI_ActivityNode_type"),
				NodesPackage.Literals.ACTIVITY_NODE__OUTGOING, true, false,
				true, null, null, null) {
			public Collection<?> getChoiceOfValues(Object object) {
				Collection<?> values = super.getChoiceOfValues(object);
				ActivityNode node = (ActivityNode) object;

				Object o = null;
				for (Iterator<?> it = values.iterator(); it.hasNext();) {
					o = it.next();

					if (node.getOutgoing().contains(o)) {
						it.remove();
					}
				}

				return values;
			}
		});
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ActivityNode) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_ActivityNode_type")
				: getString("_UI_ActivityNode_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return StoryDiagramEcoreEditPlugin.INSTANCE;
	}

}
