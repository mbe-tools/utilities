/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.provider;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsFactory;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.provider.NamedElementItemProvider;
import de.hpi.sam.storyDiagramEcore.provider.StoryDiagramEcoreEditPlugin;

/*
 * * This is the item provider adapter for a {@link
 * de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge} object. <!-- begin-user-doc
 * --> <!-- end-user-doc -->
 * @generated
 */
public class ActivityEdgeItemProvider extends NamedElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ActivityEdgeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSourcePropertyDescriptor(object);
			addTargetPropertyDescriptor(object);
			addGuardTypePropertyDescriptor(object);
			addReleaseEdgesPropertyDescriptor(object);
			addAcquireEdgesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Source feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addSourcePropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(), getString("_UI_ActivityEdge_source_feature"),
		// getString("_UI_PropertyDescriptor_description",
		// "_UI_ActivityEdge_source_feature", "_UI_ActivityEdge_type"),
		// NodesPackage.Literals.ACTIVITY_EDGE__SOURCE, true,
		// false, true, null, null, null));

		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ActivityEdge_source_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_ActivityEdge_source_feature",
						"_UI_ActivityEdge_type"),
				NodesPackage.Literals.ACTIVITY_EDGE__SOURCE, true, false, true,
				null, null, null) {
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				Collection<?> values = super.getChoiceOfValues(object);
				ActivityEdge edge = (ActivityEdge) object;

				Object o = null;
				for (Iterator<?> it = values.iterator(); it.hasNext();) {
					o = it.next();

					if (o instanceof ActivityNode) {
						ActivityNode an = (ActivityNode) o;
						if (an.eContainer() != edge.eContainer()) {
							it.remove();
						}
					} else {
						it.remove();
					}
				}

				return values;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Target feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addTargetPropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(), getString("_UI_ActivityEdge_target_feature"),
		// getString("_UI_PropertyDescriptor_description",
		// "_UI_ActivityEdge_target_feature", "_UI_ActivityEdge_type"),
		// NodesPackage.Literals.ACTIVITY_EDGE__TARGET, true,
		// false, true, null, null, null));

		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ActivityEdge_target_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_ActivityEdge_target_feature",
						"_UI_ActivityEdge_type"),
				NodesPackage.Literals.ACTIVITY_EDGE__TARGET, true, false, true,
				null, null, null) {
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				Collection<?> values = super.getChoiceOfValues(object);
				ActivityEdge edge = (ActivityEdge) object;

				Object o = null;

				for (Iterator<?> it = values.iterator(); it.hasNext();) {
					o = it.next();

					if (o instanceof ActivityNode) {
						ActivityNode an = (ActivityNode) o;
						if (an.eContainer() != edge.eContainer()) {
							it.remove();
						}
					} else {
						it.remove();
					}
				}

				return values;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Guard Type feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addGuardTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ActivityEdge_guardType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ActivityEdge_guardType_feature",
						"_UI_ActivityEdge_type"),
				NodesPackage.Literals.ACTIVITY_EDGE__GUARD_TYPE, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Release Edges feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addReleaseEdgesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ActivityEdge_releaseEdges_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ActivityEdge_releaseEdges_feature",
						"_UI_ActivityEdge_type"),
				NodesPackage.Literals.ACTIVITY_EDGE__RELEASE_EDGES, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Acquire Edges feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addAcquireEdgesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ActivityEdge_acquireEdges_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ActivityEdge_acquireEdges_feature",
						"_UI_ActivityEdge_type"),
				NodesPackage.Literals.ACTIVITY_EDGE__ACQUIRE_EDGES, true,
				false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(NodesPackage.Literals.ACTIVITY_EDGE__GUARD_EXPRESSION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ActivityEdge.gif. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/ActivityEdge"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ActivityEdge) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_ActivityEdge_type")
				: getString("_UI_ActivityEdge_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ActivityEdge.class)) {
		case NodesPackage.ACTIVITY_EDGE__GUARD_TYPE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case NodesPackage.ACTIVITY_EDGE__GUARD_EXPRESSION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				NodesPackage.Literals.ACTIVITY_EDGE__GUARD_EXPRESSION,
				ExpressionsFactory.eINSTANCE.createCallActionExpression()));

		newChildDescriptors.add(createChildParameter(
				NodesPackage.Literals.ACTIVITY_EDGE__GUARD_EXPRESSION,
				ExpressionsFactory.eINSTANCE.createStringExpression()));
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return StoryDiagramEcoreEditPlugin.INSTANCE;
	}

}
