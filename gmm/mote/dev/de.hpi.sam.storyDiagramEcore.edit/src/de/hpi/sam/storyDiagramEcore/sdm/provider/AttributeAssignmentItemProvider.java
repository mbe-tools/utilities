/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.provider;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionsFactory;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsFactory;
import de.hpi.sam.storyDiagramEcore.provider.NamedElementItemProvider;
import de.hpi.sam.storyDiagramEcore.provider.StoryDiagramEcoreEditPlugin;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;

/*
 * * This is the item provider adapter for a {@link
 * de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class AttributeAssignmentItemProvider extends NamedElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public AttributeAssignmentItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addEStructuralFeaturePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the EStructural Feature feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addEStructuralFeaturePropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(),
		// getString("_UI_AttributeAssignment_eStructuralFeature_feature"),
		// getString(
		// "_UI_PropertyDescriptor_description",
		// "_UI_AttributeAssignment_eStructuralFeature_feature",
		// "_UI_AttributeAssignment_type"),
		// SdmPackage.Literals.ATTRIBUTE_ASSIGNMENT__ESTRUCTURAL_FEATURE, true,
		// false, true,
		// null, null, null));

		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AttributeAssignment_eStructuralFeature_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AttributeAssignment_eStructuralFeature_feature",
								"_UI_AttributeAssignment_type"),
						SdmPackage.Literals.ATTRIBUTE_ASSIGNMENT__ESTRUCTURAL_FEATURE,
						true, false, true, null, null, null) {

					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						// Get all possible values
						Collection<?> result = super.getChoiceOfValues(object);

						EObject parent = ((AttributeAssignment) object)
								.eContainer();

						EStructuralFeature classifierFeature = parent.eClass()
								.getEStructuralFeature("classifier");

						if (classifierFeature == null
								|| classifierFeature != null
								&& parent.eGet(classifierFeature) == null
								|| !(parent.eGet(classifierFeature) instanceof EClass)) {
							return Collections.EMPTY_LIST;
						}

						EClass owner = (EClass) parent.eGet(classifierFeature);

						// Remove those values that are not structural features of the
						// owner of this attribute assignment
						for (Iterator<?> it = result.iterator(); it.hasNext();) {
							Object o = it.next();

							if (o instanceof EStructuralFeature) {
								if (!owner.getEAllStructuralFeatures()
										.contains(o)) {
									it.remove();
								}
							} else {
								it.remove();
							}
						}

						return result;
					}
				});
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(SdmPackage.Literals.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION);
			childrenFeatures
					.add(SdmPackage.Literals.ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns AttributeAssignment.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/AttributeAssignment"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public String getText(Object object) {
		// return getString("_UI_AttributeAssignment_type");

		String label = getString("_UI_AttributeAssignment_type");

		AttributeAssignment attributeAssignment = (AttributeAssignment) object;

		if (attributeAssignment.getEStructuralFeature() != null) {
			label += " "
					+ attributeAssignment.getEStructuralFeature().getName()
					+ " := ";

			if (attributeAssignment.getAssignmentExpression() != null) {
				label += attributeAssignment.getAssignmentExpression()
						.toString();
			} else {
				label += "?";
			}
		}

		return label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AttributeAssignment.class)) {
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION:
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						SdmPackage.Literals.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION,
						ExpressionsFactory.eINSTANCE
								.createCallActionExpression()));

		newChildDescriptors
				.add(createChildParameter(
						SdmPackage.Literals.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION,
						ExpressionsFactory.eINSTANCE.createStringExpression()));

		newChildDescriptors
				.add(createChildParameter(
						SdmPackage.Literals.ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR,
						CallActionsFactory.eINSTANCE
								.createCaseInsensitiveComparator()));
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return StoryDiagramEcoreEditPlugin.INSTANCE;
	}

}
