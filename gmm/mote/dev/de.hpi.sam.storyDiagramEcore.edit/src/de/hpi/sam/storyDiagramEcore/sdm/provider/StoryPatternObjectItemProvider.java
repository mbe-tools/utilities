/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsFactory;
import de.hpi.sam.storyDiagramEcore.sdm.SdmFactory;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

/**
 * This is the item provider adapter for a
 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class StoryPatternObjectItemProvider extends
		AbstractStoryPatternObjectItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StoryPatternObjectItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addBindingTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Binding Type feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addBindingTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_StoryPatternObject_bindingType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_StoryPatternObject_bindingType_feature",
						"_UI_StoryPatternObject_type"),
				SdmPackage.Literals.STORY_PATTERN_OBJECT__BINDING_TYPE, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(SdmPackage.Literals.STORY_PATTERN_OBJECT__ATTRIBUTE_ASSIGNMENTS);
			childrenFeatures
					.add(SdmPackage.Literals.STORY_PATTERN_OBJECT__CONSTRAINTS);
			childrenFeatures
					.add(SdmPackage.Literals.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns StoryPatternObject.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/StoryPatternObject"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public String getText(Object object) {
		// String label = ((StoryPatternObject) object).getName();
		// return label == null || label.length() == 0 ?
		// getString("_UI_StoryPatternObject_type") :
		// getString("_UI_StoryPatternObject_type")
		// + " " + label;

		return super.getText(object);
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(StoryPatternObject.class)) {
		case SdmPackage.STORY_PATTERN_OBJECT__BINDING_TYPE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case SdmPackage.STORY_PATTERN_OBJECT__ATTRIBUTE_ASSIGNMENTS:
		case SdmPackage.STORY_PATTERN_OBJECT__CONSTRAINTS:
		case SdmPackage.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						SdmPackage.Literals.STORY_PATTERN_OBJECT__ATTRIBUTE_ASSIGNMENTS,
						SdmFactory.eINSTANCE.createAttributeAssignment()));

		newChildDescriptors.add(createChildParameter(
				SdmPackage.Literals.STORY_PATTERN_OBJECT__CONSTRAINTS,
				ExpressionsFactory.eINSTANCE.createCallActionExpression()));

		newChildDescriptors.add(createChildParameter(
				SdmPackage.Literals.STORY_PATTERN_OBJECT__CONSTRAINTS,
				ExpressionsFactory.eINSTANCE.createStringExpression()));

		newChildDescriptors
				.add(createChildParameter(
						SdmPackage.Literals.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION,
						ExpressionsFactory.eINSTANCE
								.createCallActionExpression()));

		newChildDescriptors
				.add(createChildParameter(
						SdmPackage.Literals.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION,
						ExpressionsFactory.eINSTANCE.createStringExpression()));
	}

	/**
	 * This returns the label text for
	 * {@link org.eclipse.emf.edit.command.CreateChildCommand}. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature,
			Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == SdmPackage.Literals.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION
				|| childFeature == SdmPackage.Literals.STORY_PATTERN_OBJECT__CONSTRAINTS
				|| childFeature == SdmPackage.Literals.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION;

		if (qualify) {
			return getString("_UI_CreateChild_text2", new Object[] {
					getTypeText(childObject), getFeatureText(childFeature),
					getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
