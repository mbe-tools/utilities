/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.provider;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreFactory;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.storyDiagramEcore.nodes.NodesFactory;

/*
 * * This is the item provider adapter for a {@link
 * de.hpi.sam.storyDiagramEcore.Activity} object. <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * @generated
 */
public class ActivityItemProvider extends NamedElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ActivityItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSpecificationPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Specification feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addSpecificationPropertyDescriptor(Object object) {
		// itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// adapterFactory).getRootAdapterFactory(),
		// getResourceLocator(),
		// getString("_UI_Activity_specification_feature"),
		// getString("_UI_PropertyDescriptor_description",
		// "_UI_Activity_specification_feature", "_UI_Activity_type"),
		// StoryDiagramEcorePackage.Literals.ACTIVITY__SPECIFICATION, true,
		// false, true, null, null, null));

		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_Activity_specification_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_Activity_specification_feature",
						"_UI_Activity_type"),
				StoryDiagramEcorePackage.Literals.ACTIVITY__SPECIFICATION,
				true, false, true, null, null, null) {

			@Override
			public IItemLabelProvider getLabelProvider(Object object) {
				return new ItemDelegator(adapterFactory, resourceLocator) {
					@Override
					public String getText(Object object) {
						if (object != null && object instanceof EOperation) {
							EClass c = ((EOperation) object)
									.getEContainingClass();

							if (c != null) {
								return c.getEPackage().getNsPrefix() + "."
										+ c.getName() + "."
										+ super.getText(object);
							} else {
								return super.getText(object);
							}
						} else {
							return super.getText(object);
						}
					}
				};
			}

			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				Collection<?> result = super.getChoiceOfValues(object);
				if (feature instanceof EReference && object instanceof EObject) {
					@SuppressWarnings("unchecked")
					List<EObject> eObjects = (List<EObject>) (List<?>) new LinkedList<Object>(
							result);
					Resource resource = ((EObject) object).eResource();
					if (resource != null) {
						ResourceSet resourceSet = resource.getResourceSet();
						if (resourceSet != null) {
							Collection<EObject> visited = new HashSet<EObject>(
									eObjects);
							Registry packageRegistry = resourceSet
									.getPackageRegistry();
							for (String nsURI : packageRegistry.keySet()) {
								collectReachableObjectsOfType(visited,
										eObjects,
										packageRegistry.getEPackage(nsURI),
										feature.getEType());
							}
						}
					}
					result = eObjects;
				}
				return result;
			}
		});
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(StoryDiagramEcorePackage.Literals.ACTIVITY__NODES);
			childrenFeatures
					.add(StoryDiagramEcorePackage.Literals.ACTIVITY__EDGES);
			childrenFeatures
					.add(StoryDiagramEcorePackage.Literals.ACTIVITY__SEMAPHORES);
			childrenFeatures
					.add(StoryDiagramEcorePackage.Literals.ACTIVITY__IMPORTS);
			childrenFeatures
					.add(StoryDiagramEcorePackage.Literals.ACTIVITY__PARAMETERS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Activity.gif.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/Activity"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public String getText(Object object) {
		// String label = ((Activity) object).getName();
		// return label == null || label.length() == 0 ?
		// getString("_UI_Activity_type") : getString("_UI_Activity_type") + " "
		// + label;

		Activity a = (Activity) object;
		String label = "";
		if (a.getName() != null && a.getName().length() > 0) {
			label = a.getName();
		}

		if (a.getSpecification() != null) {
			if (label != null && label.length() != 0) {
				label += " - ";
			}

			if (a.getSpecification().getEContainingClass() != null
					&& a.getSpecification().getEContainingClass().getEPackage() != null) {
				label += a.getSpecification().getEContainingClass()
						.getEPackage().getNsPrefix()
						+ "."
						+ a.getSpecification().getEContainingClass().getName()
						+ "." + a.getSpecification().getName();
			}
		}

		return label == null || label.length() == 0 ? getString("_UI_Activity_type")
				: getString("_UI_Activity_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Activity.class)) {
		case StoryDiagramEcorePackage.ACTIVITY__NODES:
		case StoryDiagramEcorePackage.ACTIVITY__EDGES:
		case StoryDiagramEcorePackage.ACTIVITY__SEMAPHORES:
		case StoryDiagramEcorePackage.ACTIVITY__IMPORTS:
		case StoryDiagramEcorePackage.ACTIVITY__PARAMETERS:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__NODES,
				NodesFactory.eINSTANCE.createActivityFinalNode()));

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__NODES,
				NodesFactory.eINSTANCE.createFlowFinalNode()));

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__NODES,
				NodesFactory.eINSTANCE.createExpressionActivityNode()));

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__NODES,
				NodesFactory.eINSTANCE.createDecisionNode()));

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__NODES,
				NodesFactory.eINSTANCE.createInitialNode()));

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__NODES,
				NodesFactory.eINSTANCE.createMergeNode()));

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__NODES,
				NodesFactory.eINSTANCE.createStoryActionNode()));

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__NODES,
				NodesFactory.eINSTANCE.createForkNode()));

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__NODES,
				NodesFactory.eINSTANCE.createJoinNode()));

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__EDGES,
				NodesFactory.eINSTANCE.createActivityEdge()));

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__SEMAPHORES,
				NodesFactory.eINSTANCE.createSemaphore()));

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__IMPORTS,
				StoryDiagramEcoreFactory.eINSTANCE.createExpressionImport()));

		newChildDescriptors.add(createChildParameter(
				StoryDiagramEcorePackage.Literals.ACTIVITY__PARAMETERS,
				StoryDiagramEcoreFactory.eINSTANCE.createActivityParameter()));
	}

}
