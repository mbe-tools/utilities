/**
 */
package de.hpi.sam.tgg.provider;

import de.hpi.sam.storyDiagramEcore.sdm.SdmFactory;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TggFactory;
import de.hpi.sam.tgg.TggPackage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link de.hpi.sam.tgg.ModelLink} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ModelLinkItemProvider extends ModelElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelLinkItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addEReferencePropertyDescriptor(object);
			addSourcePropertyDescriptor(object);
			addTargetPropertyDescriptor(object);
			addOppositeTGGLinkPropertyDescriptor(object);
			addEOppositeReferencePropertyDescriptor(object);
			addExternalPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the EReference feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEReferencePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ModelLink_eReference_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ModelLink_eReference_feature",
						"_UI_ModelLink_type"),
				TggPackage.Literals.MODEL_LINK__EREFERENCE, true, false, true,
				null, null, null));
	}

	/**
	 * This adds a property descriptor for the Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourcePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ModelLink_source_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ModelLink_source_feature", "_UI_ModelLink_type"),
				TggPackage.Literals.MODEL_LINK__SOURCE, true, false, true,
				null, null, null));
	}

	/**
	 * This adds a property descriptor for the Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ModelLink_target_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ModelLink_target_feature", "_UI_ModelLink_type"),
				TggPackage.Literals.MODEL_LINK__TARGET, true, false, true,
				null, null, null));
	}

	/**
	 * This adds a property descriptor for the Opposite TGG Link feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOppositeTGGLinkPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ModelLink_oppositeTGGLink_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ModelLink_oppositeTGGLink_feature",
						"_UI_ModelLink_type"),
				TggPackage.Literals.MODEL_LINK__OPPOSITE_TGG_LINK, true, false,
				true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the EOpposite Reference feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addEOppositeReferencePropertyDescriptor(final Object object) {
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ModelLink_eOppositeReference_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ModelLink_eOppositeReference_feature",
						"_UI_ModelLink_type"),
				TggPackage.Literals.MODEL_LINK__EOPPOSITE_REFERENCE, true,
				false, true, null, null, null) {

			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(final Object p_object) {
				final Collection<EObject> elementsChoice = new ArrayList<EObject>();
				final Collection<EObject> currentElements = new ArrayList<EObject>();
				final PropertyValueWrapper wrapper = (PropertyValueWrapper) getPropertyValue(p_object);
				final Object value = wrapper.getEditableValue(p_object);

				// Test so that it works also for single cardinality features
				if (value instanceof Collection<?>) {
					currentElements.addAll((Collection<EObject>) value);
				}

				final ModelLink modelLink = (ModelLink) object;
				final ModelObject target = modelLink.getTarget();

				if (target != null) {
					final EClassifier targetClass = target.getClassifier();

					if (targetClass instanceof EClass) {
						final Collection<EReference> targetClassRefs = ((EClass) targetClass)
								.getEAllReferences();
						final ModelObject source = modelLink.getSource();
						final EClassifier sourceClass = source.getClassifier();

						if (sourceClass instanceof EClass) {
							for (final Object ref : super
									.getChoiceOfValues(p_object)) {
								if (ref instanceof EReference) {
									final EReference eRef = (EReference) ref;

									if (targetClassRefs.contains(eRef)
											&& eRef.getEType() instanceof EClass
											&& ((EClass) eRef.getEType())
													.isSuperTypeOf((EClass) sourceClass)) {
										elementsChoice.add(eRef);
									}
								}
							}
						}
					}
				}

				elementsChoice.removeAll(currentElements);
				elementsChoice.remove(object);

				return elementsChoice;
			}
		});
		//		itemPropertyDescriptors.add(createItemPropertyDescriptor(
		//				((ComposeableAdapterFactory) adapterFactory)
		//						.getRootAdapterFactory(),
		//				getResourceLocator(),
		//				getString("_UI_ModelLink_eOppositeReference_feature"),
		//				getString("_UI_PropertyDescriptor_description",
		//						"_UI_ModelLink_eOppositeReference_feature",
		//						"_UI_ModelLink_type"),
		//				TggPackage.Literals.MODEL_LINK__EOPPOSITE_REFERENCE, true,
		//				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the External feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExternalPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ModelLink_external_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_ModelLink_external_feature",
								"_UI_ModelLink_type"),
						TggPackage.Literals.MODEL_LINK__EXTERNAL, true, false,
						false, ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
						null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(TggPackage.Literals.MODEL_LINK__LINK_POSITION_CONSTRAINT);
			childrenFeatures
					.add(TggPackage.Literals.MODEL_LINK__EXTERNAL_REFERENCE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ModelLink.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/ModelLink"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ModelLink) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_ModelLink_type")
				: getString("_UI_ModelLink_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ModelLink.class)) {
		case TggPackage.MODEL_LINK__EXTERNAL:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case TggPackage.MODEL_LINK__LINK_POSITION_CONSTRAINT:
		case TggPackage.MODEL_LINK__EXTERNAL_REFERENCE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				TggPackage.Literals.MODEL_LINK__LINK_POSITION_CONSTRAINT,
				TggFactory.eINSTANCE.createModelLinkPositionConstraint()));

		newChildDescriptors.add(createChildParameter(
				TggPackage.Literals.MODEL_LINK__EXTERNAL_REFERENCE,
				SdmFactory.eINSTANCE.createExternalReference()));
	}

}
