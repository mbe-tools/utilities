/**
 */
package de.hpi.sam.tgg.provider;

import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsFactory;

import de.hpi.sam.storyDiagramEcore.sdm.SdmFactory;

import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TggPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link de.hpi.sam.tgg.ModelObject} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ModelObjectItemProvider extends ModelElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelObjectItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addClassifierPropertyDescriptor(object);
			addOutgoingModelLinksPropertyDescriptor(object);
			addIncomingModelLinksPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClassifierPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ModelObject_classifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ModelObject_classifier_feature",
						"_UI_ModelObject_type"),
				TggPackage.Literals.MODEL_OBJECT__CLASSIFIER, true, false,
				true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Outgoing Model Links feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOutgoingModelLinksPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ModelObject_outgoingModelLinks_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ModelObject_outgoingModelLinks_feature",
						"_UI_ModelObject_type"),
				TggPackage.Literals.MODEL_OBJECT__OUTGOING_MODEL_LINKS, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Incoming Model Links feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIncomingModelLinksPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ModelObject_incomingModelLinks_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ModelObject_incomingModelLinks_feature",
						"_UI_ModelObject_type"),
				TggPackage.Literals.MODEL_OBJECT__INCOMING_MODEL_LINKS, true,
				false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(TggPackage.Literals.MODEL_OBJECT__CONSTRAINT_EXPRESSIONS);
			childrenFeatures
					.add(TggPackage.Literals.MODEL_OBJECT__ATTRIBUTE_ASSIGNMENTS);
			childrenFeatures
					.add(TggPackage.Literals.MODEL_OBJECT__POST_CREATION_EXPRESSIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ModelObject.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/ModelObject"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ModelObject) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_ModelObject_type")
				: getString("_UI_ModelObject_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ModelObject.class)) {
		case TggPackage.MODEL_OBJECT__CONSTRAINT_EXPRESSIONS:
		case TggPackage.MODEL_OBJECT__ATTRIBUTE_ASSIGNMENTS:
		case TggPackage.MODEL_OBJECT__POST_CREATION_EXPRESSIONS:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				TggPackage.Literals.MODEL_OBJECT__CONSTRAINT_EXPRESSIONS,
				ExpressionsFactory.eINSTANCE.createCallActionExpression()));

		newChildDescriptors.add(createChildParameter(
				TggPackage.Literals.MODEL_OBJECT__CONSTRAINT_EXPRESSIONS,
				ExpressionsFactory.eINSTANCE.createStringExpression()));

		newChildDescriptors.add(createChildParameter(
				TggPackage.Literals.MODEL_OBJECT__ATTRIBUTE_ASSIGNMENTS,
				SdmFactory.eINSTANCE.createAttributeAssignment()));

		newChildDescriptors.add(createChildParameter(
				TggPackage.Literals.MODEL_OBJECT__POST_CREATION_EXPRESSIONS,
				ExpressionsFactory.eINSTANCE.createCallActionExpression()));

		newChildDescriptors.add(createChildParameter(
				TggPackage.Literals.MODEL_OBJECT__POST_CREATION_EXPRESSIONS,
				ExpressionsFactory.eINSTANCE.createStringExpression()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature,
			Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == TggPackage.Literals.MODEL_OBJECT__CONSTRAINT_EXPRESSIONS
				|| childFeature == TggPackage.Literals.MODEL_OBJECT__POST_CREATION_EXPRESSIONS;

		if (qualify) {
			return getString("_UI_CreateChild_text2", new Object[] {
					getTypeText(childObject), getFeatureText(childFeature),
					getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
