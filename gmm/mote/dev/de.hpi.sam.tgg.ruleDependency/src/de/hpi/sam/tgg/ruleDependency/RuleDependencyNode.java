/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.ruleDependency;

import de.hpi.sam.tgg.TGGRule;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.ruleDependency.RuleDependencyNode#getCorrNodeDependencies <em>Corr Node Dependencies</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ruleDependency.RuleDependencyNode#getTggRule <em>Tgg Rule</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage#getRuleDependencyNode()
 * @model
 * @generated
 */
public interface RuleDependencyNode extends EObject {
	/**
	 * Returns the value of the '<em><b>Corr Node Dependencies</b></em>' containment reference list.
	 * The list contents are of type {@link de.hpi.sam.tgg.ruleDependency.CorrNodeDependency}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corr Node Dependencies</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Corr Node Dependencies</em>' containment reference list.
	 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage#getRuleDependencyNode_CorrNodeDependencies()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<CorrNodeDependency> getCorrNodeDependencies();

	/**
	 * Returns the value of the '<em><b>Tgg Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgg Rule</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tgg Rule</em>' reference.
	 * @see #setTggRule(TGGRule)
	 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage#getRuleDependencyNode_TggRule()
	 * @model required="true"
	 * @generated
	 */
	TGGRule getTggRule();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ruleDependency.RuleDependencyNode#getTggRule <em>Tgg Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tgg Rule</em>' reference.
	 * @see #getTggRule()
	 * @generated
	 */
	void setTggRule(TGGRule value);

} // RuleDependencyNode
