/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.ruleDependency.impl;

import de.hpi.sam.tgg.TGGRule;

import de.hpi.sam.tgg.ruleDependency.CorrNodeDependency;
import de.hpi.sam.tgg.ruleDependency.RuleDependencyNode;
import de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyNodeImpl#getCorrNodeDependencies <em>Corr Node Dependencies</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyNodeImpl#getTggRule <em>Tgg Rule</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RuleDependencyNodeImpl extends EObjectImpl implements RuleDependencyNode {
	/**
	 * The cached value of the '{@link #getCorrNodeDependencies() <em>Corr Node Dependencies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrNodeDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<CorrNodeDependency> corrNodeDependencies;

	/**
	 * The cached value of the '{@link #getTggRule() <em>Tgg Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTggRule()
	 * @generated
	 * @ordered
	 */
	protected TGGRule tggRule;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuleDependencyNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuleDependencyPackage.Literals.RULE_DEPENDENCY_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CorrNodeDependency> getCorrNodeDependencies() {
		if (corrNodeDependencies == null)
		{
			corrNodeDependencies = new EObjectContainmentEList<CorrNodeDependency>(CorrNodeDependency.class, this, RuleDependencyPackage.RULE_DEPENDENCY_NODE__CORR_NODE_DEPENDENCIES);
		}
		return corrNodeDependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TGGRule getTggRule() {
		if (tggRule != null && tggRule.eIsProxy())
		{
			InternalEObject oldTggRule = (InternalEObject)tggRule;
			tggRule = (TGGRule)eResolveProxy(oldTggRule);
			if (tggRule != oldTggRule)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuleDependencyPackage.RULE_DEPENDENCY_NODE__TGG_RULE, oldTggRule, tggRule));
			}
		}
		return tggRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TGGRule basicGetTggRule() {
		return tggRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTggRule(TGGRule newTggRule) {
		TGGRule oldTggRule = tggRule;
		tggRule = newTggRule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuleDependencyPackage.RULE_DEPENDENCY_NODE__TGG_RULE, oldTggRule, tggRule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID)
		{
			case RuleDependencyPackage.RULE_DEPENDENCY_NODE__CORR_NODE_DEPENDENCIES:
				return ((InternalEList<?>)getCorrNodeDependencies()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID)
		{
			case RuleDependencyPackage.RULE_DEPENDENCY_NODE__CORR_NODE_DEPENDENCIES:
				return getCorrNodeDependencies();
			case RuleDependencyPackage.RULE_DEPENDENCY_NODE__TGG_RULE:
				if (resolve) return getTggRule();
				return basicGetTggRule();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID)
		{
			case RuleDependencyPackage.RULE_DEPENDENCY_NODE__CORR_NODE_DEPENDENCIES:
				getCorrNodeDependencies().clear();
				getCorrNodeDependencies().addAll((Collection<? extends CorrNodeDependency>)newValue);
				return;
			case RuleDependencyPackage.RULE_DEPENDENCY_NODE__TGG_RULE:
				setTggRule((TGGRule)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID)
		{
			case RuleDependencyPackage.RULE_DEPENDENCY_NODE__CORR_NODE_DEPENDENCIES:
				getCorrNodeDependencies().clear();
				return;
			case RuleDependencyPackage.RULE_DEPENDENCY_NODE__TGG_RULE:
				setTggRule((TGGRule)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID)
		{
			case RuleDependencyPackage.RULE_DEPENDENCY_NODE__CORR_NODE_DEPENDENCIES:
				return corrNodeDependencies != null && !corrNodeDependencies.isEmpty();
			case RuleDependencyPackage.RULE_DEPENDENCY_NODE__TGG_RULE:
				return tggRule != null;
		}
		return super.eIsSet(featureID);
	}

} //RuleDependencyNodeImpl
