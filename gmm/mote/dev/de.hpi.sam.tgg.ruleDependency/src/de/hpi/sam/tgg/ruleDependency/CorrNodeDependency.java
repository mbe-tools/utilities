/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.ruleDependency;

import de.hpi.sam.tgg.CorrespondenceNode;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Node Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.ruleDependency.CorrNodeDependency#getRequiredDependencies <em>Required Dependencies</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ruleDependency.CorrNodeDependency#getCorrNode <em>Corr Node</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage#getCorrNodeDependency()
 * @model
 * @generated
 */
public interface CorrNodeDependency extends EObject {
	/**
	 * Returns the value of the '<em><b>Required Dependencies</b></em>' reference list.
	 * The list contents are of type {@link de.hpi.sam.tgg.ruleDependency.RuleDependencyNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Dependencies</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Dependencies</em>' reference list.
	 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage#getCorrNodeDependency_RequiredDependencies()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<RuleDependencyNode> getRequiredDependencies();

	/**
	 * Returns the value of the '<em><b>Corr Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corr Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Corr Node</em>' reference.
	 * @see #setCorrNode(CorrespondenceNode)
	 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage#getCorrNodeDependency_CorrNode()
	 * @model required="true"
	 * @generated
	 */
	CorrespondenceNode getCorrNode();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ruleDependency.CorrNodeDependency#getCorrNode <em>Corr Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Corr Node</em>' reference.
	 * @see #getCorrNode()
	 * @generated
	 */
	void setCorrNode(CorrespondenceNode value);

} // CorrNodeDependency
