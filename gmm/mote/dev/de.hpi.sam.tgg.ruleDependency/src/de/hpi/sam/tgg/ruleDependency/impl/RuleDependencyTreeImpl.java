/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.ruleDependency.impl;

import de.hpi.sam.tgg.ruleDependency.RuleDependencyNode;
import de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage;
import de.hpi.sam.tgg.ruleDependency.RuleDependencyTree;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tree</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyTreeImpl#getDependencyNodes <em>Dependency Nodes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RuleDependencyTreeImpl extends EObjectImpl implements RuleDependencyTree {
	/**
	 * The cached value of the '{@link #getDependencyNodes() <em>Dependency Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencyNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<RuleDependencyNode> dependencyNodes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuleDependencyTreeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuleDependencyPackage.Literals.RULE_DEPENDENCY_TREE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RuleDependencyNode> getDependencyNodes() {
		if (dependencyNodes == null)
		{
			dependencyNodes = new EObjectContainmentEList<RuleDependencyNode>(RuleDependencyNode.class, this, RuleDependencyPackage.RULE_DEPENDENCY_TREE__DEPENDENCY_NODES);
		}
		return dependencyNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID)
		{
			case RuleDependencyPackage.RULE_DEPENDENCY_TREE__DEPENDENCY_NODES:
				return ((InternalEList<?>)getDependencyNodes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID)
		{
			case RuleDependencyPackage.RULE_DEPENDENCY_TREE__DEPENDENCY_NODES:
				return getDependencyNodes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID)
		{
			case RuleDependencyPackage.RULE_DEPENDENCY_TREE__DEPENDENCY_NODES:
				getDependencyNodes().clear();
				getDependencyNodes().addAll((Collection<? extends RuleDependencyNode>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID)
		{
			case RuleDependencyPackage.RULE_DEPENDENCY_TREE__DEPENDENCY_NODES:
				getDependencyNodes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID)
		{
			case RuleDependencyPackage.RULE_DEPENDENCY_TREE__DEPENDENCY_NODES:
				return dependencyNodes != null && !dependencyNodes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RuleDependencyTreeImpl
