/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.ruleDependency;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyFactory
 * @model kind="package"
 * @generated
 */
public interface RuleDependencyPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ruleDependency";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de/hpi/sam/tgg/ruleDependency.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ruleDependency";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RuleDependencyPackage eINSTANCE = de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyTreeImpl <em>Tree</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyTreeImpl
	 * @see de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyPackageImpl#getRuleDependencyTree()
	 * @generated
	 */
	int RULE_DEPENDENCY_TREE = 0;

	/**
	 * The feature id for the '<em><b>Dependency Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DEPENDENCY_TREE__DEPENDENCY_NODES = 0;

	/**
	 * The number of structural features of the '<em>Tree</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DEPENDENCY_TREE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyNodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyNodeImpl
	 * @see de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyPackageImpl#getRuleDependencyNode()
	 * @generated
	 */
	int RULE_DEPENDENCY_NODE = 1;

	/**
	 * The feature id for the '<em><b>Corr Node Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DEPENDENCY_NODE__CORR_NODE_DEPENDENCIES = 0;

	/**
	 * The feature id for the '<em><b>Tgg Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DEPENDENCY_NODE__TGG_RULE = 1;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DEPENDENCY_NODE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.ruleDependency.impl.CorrNodeDependencyImpl <em>Corr Node Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.tgg.ruleDependency.impl.CorrNodeDependencyImpl
	 * @see de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyPackageImpl#getCorrNodeDependency()
	 * @generated
	 */
	int CORR_NODE_DEPENDENCY = 2;

	/**
	 * The feature id for the '<em><b>Required Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_NODE_DEPENDENCY__REQUIRED_DEPENDENCIES = 0;

	/**
	 * The feature id for the '<em><b>Corr Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_NODE_DEPENDENCY__CORR_NODE = 1;

	/**
	 * The number of structural features of the '<em>Corr Node Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_NODE_DEPENDENCY_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.ruleDependency.RuleDependencyTree <em>Tree</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tree</em>'.
	 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyTree
	 * @generated
	 */
	EClass getRuleDependencyTree();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.tgg.ruleDependency.RuleDependencyTree#getDependencyNodes <em>Dependency Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dependency Nodes</em>'.
	 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyTree#getDependencyNodes()
	 * @see #getRuleDependencyTree()
	 * @generated
	 */
	EReference getRuleDependencyTree_DependencyNodes();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.ruleDependency.RuleDependencyNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyNode
	 * @generated
	 */
	EClass getRuleDependencyNode();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.tgg.ruleDependency.RuleDependencyNode#getCorrNodeDependencies <em>Corr Node Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Corr Node Dependencies</em>'.
	 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyNode#getCorrNodeDependencies()
	 * @see #getRuleDependencyNode()
	 * @generated
	 */
	EReference getRuleDependencyNode_CorrNodeDependencies();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.ruleDependency.RuleDependencyNode#getTggRule <em>Tgg Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tgg Rule</em>'.
	 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyNode#getTggRule()
	 * @see #getRuleDependencyNode()
	 * @generated
	 */
	EReference getRuleDependencyNode_TggRule();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.ruleDependency.CorrNodeDependency <em>Corr Node Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Corr Node Dependency</em>'.
	 * @see de.hpi.sam.tgg.ruleDependency.CorrNodeDependency
	 * @generated
	 */
	EClass getCorrNodeDependency();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.tgg.ruleDependency.CorrNodeDependency#getRequiredDependencies <em>Required Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Dependencies</em>'.
	 * @see de.hpi.sam.tgg.ruleDependency.CorrNodeDependency#getRequiredDependencies()
	 * @see #getCorrNodeDependency()
	 * @generated
	 */
	EReference getCorrNodeDependency_RequiredDependencies();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.ruleDependency.CorrNodeDependency#getCorrNode <em>Corr Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Corr Node</em>'.
	 * @see de.hpi.sam.tgg.ruleDependency.CorrNodeDependency#getCorrNode()
	 * @see #getCorrNodeDependency()
	 * @generated
	 */
	EReference getCorrNodeDependency_CorrNode();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RuleDependencyFactory getRuleDependencyFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyTreeImpl <em>Tree</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyTreeImpl
		 * @see de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyPackageImpl#getRuleDependencyTree()
		 * @generated
		 */
		EClass RULE_DEPENDENCY_TREE = eINSTANCE.getRuleDependencyTree();

		/**
		 * The meta object literal for the '<em><b>Dependency Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_DEPENDENCY_TREE__DEPENDENCY_NODES = eINSTANCE.getRuleDependencyTree_DependencyNodes();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyNodeImpl <em>Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyNodeImpl
		 * @see de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyPackageImpl#getRuleDependencyNode()
		 * @generated
		 */
		EClass RULE_DEPENDENCY_NODE = eINSTANCE.getRuleDependencyNode();

		/**
		 * The meta object literal for the '<em><b>Corr Node Dependencies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_DEPENDENCY_NODE__CORR_NODE_DEPENDENCIES = eINSTANCE.getRuleDependencyNode_CorrNodeDependencies();

		/**
		 * The meta object literal for the '<em><b>Tgg Rule</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_DEPENDENCY_NODE__TGG_RULE = eINSTANCE.getRuleDependencyNode_TggRule();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.ruleDependency.impl.CorrNodeDependencyImpl <em>Corr Node Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.ruleDependency.impl.CorrNodeDependencyImpl
		 * @see de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyPackageImpl#getCorrNodeDependency()
		 * @generated
		 */
		EClass CORR_NODE_DEPENDENCY = eINSTANCE.getCorrNodeDependency();

		/**
		 * The meta object literal for the '<em><b>Required Dependencies</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CORR_NODE_DEPENDENCY__REQUIRED_DEPENDENCIES = eINSTANCE.getCorrNodeDependency_RequiredDependencies();

		/**
		 * The meta object literal for the '<em><b>Corr Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CORR_NODE_DEPENDENCY__CORR_NODE = eINSTANCE.getCorrNodeDependency_CorrNode();

	}

} //RuleDependencyPackage
