/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.ruleDependency;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tree</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.ruleDependency.RuleDependencyTree#getDependencyNodes <em>Dependency Nodes</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage#getRuleDependencyTree()
 * @model
 * @generated
 */
public interface RuleDependencyTree extends EObject {
	/**
	 * Returns the value of the '<em><b>Dependency Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link de.hpi.sam.tgg.ruleDependency.RuleDependencyNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependency Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependency Nodes</em>' containment reference list.
	 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage#getRuleDependencyTree_DependencyNodes()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<RuleDependencyNode> getDependencyNodes();

} // RuleDependencyTree
