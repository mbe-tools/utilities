/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.ruleDependency;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage
 * @generated
 */
public interface RuleDependencyFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RuleDependencyFactory eINSTANCE = de.hpi.sam.tgg.ruleDependency.impl.RuleDependencyFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Tree</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tree</em>'.
	 * @generated
	 */
	RuleDependencyTree createRuleDependencyTree();

	/**
	 * Returns a new object of class '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Node</em>'.
	 * @generated
	 */
	RuleDependencyNode createRuleDependencyNode();

	/**
	 * Returns a new object of class '<em>Corr Node Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Corr Node Dependency</em>'.
	 * @generated
	 */
	CorrNodeDependency createCorrNodeDependency();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	RuleDependencyPackage getRuleDependencyPackage();

} //RuleDependencyFactory
