/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.ruleDependency.impl;

import de.hpi.sam.tgg.ruleDependency.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RuleDependencyFactoryImpl extends EFactoryImpl implements RuleDependencyFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RuleDependencyFactory init() {
		try
		{
			RuleDependencyFactory theRuleDependencyFactory = (RuleDependencyFactory)EPackage.Registry.INSTANCE.getEFactory("http://de/hpi/sam/tgg/ruleDependency.ecore"); 
			if (theRuleDependencyFactory != null)
			{
				return theRuleDependencyFactory;
			}
		}
		catch (Exception exception)
		{
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RuleDependencyFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDependencyFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID())
		{
			case RuleDependencyPackage.RULE_DEPENDENCY_TREE: return createRuleDependencyTree();
			case RuleDependencyPackage.RULE_DEPENDENCY_NODE: return createRuleDependencyNode();
			case RuleDependencyPackage.CORR_NODE_DEPENDENCY: return createCorrNodeDependency();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDependencyTree createRuleDependencyTree() {
		RuleDependencyTreeImpl ruleDependencyTree = new RuleDependencyTreeImpl();
		return ruleDependencyTree;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDependencyNode createRuleDependencyNode() {
		RuleDependencyNodeImpl ruleDependencyNode = new RuleDependencyNodeImpl();
		return ruleDependencyNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrNodeDependency createCorrNodeDependency() {
		CorrNodeDependencyImpl corrNodeDependency = new CorrNodeDependencyImpl();
		return corrNodeDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDependencyPackage getRuleDependencyPackage() {
		return (RuleDependencyPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RuleDependencyPackage getPackage() {
		return RuleDependencyPackage.eINSTANCE;
	}

} //RuleDependencyFactoryImpl
