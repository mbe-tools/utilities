/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.ruleDependency.impl;

import de.hpi.sam.tgg.TggPackage;

import de.hpi.sam.tgg.ruleDependency.CorrNodeDependency;
import de.hpi.sam.tgg.ruleDependency.RuleDependencyFactory;
import de.hpi.sam.tgg.ruleDependency.RuleDependencyNode;
import de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage;
import de.hpi.sam.tgg.ruleDependency.RuleDependencyTree;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RuleDependencyPackageImpl extends EPackageImpl implements RuleDependencyPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDependencyTreeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDependencyNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrNodeDependencyEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RuleDependencyPackageImpl() {
		super(eNS_URI, RuleDependencyFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RuleDependencyPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RuleDependencyPackage init() {
		if (isInited) return (RuleDependencyPackage)EPackage.Registry.INSTANCE.getEPackage(RuleDependencyPackage.eNS_URI);

		// Obtain or create and register package
		RuleDependencyPackageImpl theRuleDependencyPackage = (RuleDependencyPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RuleDependencyPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RuleDependencyPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		TggPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theRuleDependencyPackage.createPackageContents();

		// Initialize created meta-data
		theRuleDependencyPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRuleDependencyPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RuleDependencyPackage.eNS_URI, theRuleDependencyPackage);
		return theRuleDependencyPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDependencyTree() {
		return ruleDependencyTreeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleDependencyTree_DependencyNodes() {
		return (EReference)ruleDependencyTreeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDependencyNode() {
		return ruleDependencyNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleDependencyNode_CorrNodeDependencies() {
		return (EReference)ruleDependencyNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleDependencyNode_TggRule() {
		return (EReference)ruleDependencyNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrNodeDependency() {
		return corrNodeDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCorrNodeDependency_RequiredDependencies() {
		return (EReference)corrNodeDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCorrNodeDependency_CorrNode() {
		return (EReference)corrNodeDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDependencyFactory getRuleDependencyFactory() {
		return (RuleDependencyFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		ruleDependencyTreeEClass = createEClass(RULE_DEPENDENCY_TREE);
		createEReference(ruleDependencyTreeEClass, RULE_DEPENDENCY_TREE__DEPENDENCY_NODES);

		ruleDependencyNodeEClass = createEClass(RULE_DEPENDENCY_NODE);
		createEReference(ruleDependencyNodeEClass, RULE_DEPENDENCY_NODE__CORR_NODE_DEPENDENCIES);
		createEReference(ruleDependencyNodeEClass, RULE_DEPENDENCY_NODE__TGG_RULE);

		corrNodeDependencyEClass = createEClass(CORR_NODE_DEPENDENCY);
		createEReference(corrNodeDependencyEClass, CORR_NODE_DEPENDENCY__REQUIRED_DEPENDENCIES);
		createEReference(corrNodeDependencyEClass, CORR_NODE_DEPENDENCY__CORR_NODE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TggPackage theTggPackage = (TggPackage)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(ruleDependencyTreeEClass, RuleDependencyTree.class, "RuleDependencyTree", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuleDependencyTree_DependencyNodes(), this.getRuleDependencyNode(), null, "dependencyNodes", null, 0, -1, RuleDependencyTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(ruleDependencyNodeEClass, RuleDependencyNode.class, "RuleDependencyNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuleDependencyNode_CorrNodeDependencies(), this.getCorrNodeDependency(), null, "corrNodeDependencies", null, 0, -1, RuleDependencyNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRuleDependencyNode_TggRule(), theTggPackage.getTGGRule(), null, "tggRule", null, 1, 1, RuleDependencyNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(corrNodeDependencyEClass, CorrNodeDependency.class, "CorrNodeDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCorrNodeDependency_RequiredDependencies(), this.getRuleDependencyNode(), null, "requiredDependencies", null, 1, -1, CorrNodeDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getCorrNodeDependency_CorrNode(), theTggPackage.getCorrespondenceNode(), null, "corrNode", null, 1, 1, CorrNodeDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //RuleDependencyPackageImpl
