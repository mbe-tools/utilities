/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.ruleDependency.impl;

import de.hpi.sam.tgg.CorrespondenceNode;

import de.hpi.sam.tgg.ruleDependency.CorrNodeDependency;
import de.hpi.sam.tgg.ruleDependency.RuleDependencyNode;
import de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Node Dependency</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.ruleDependency.impl.CorrNodeDependencyImpl#getRequiredDependencies <em>Required Dependencies</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ruleDependency.impl.CorrNodeDependencyImpl#getCorrNode <em>Corr Node</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CorrNodeDependencyImpl extends EObjectImpl implements CorrNodeDependency {
	/**
	 * The cached value of the '{@link #getRequiredDependencies() <em>Required Dependencies</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<RuleDependencyNode> requiredDependencies;

	/**
	 * The cached value of the '{@link #getCorrNode() <em>Corr Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrNode()
	 * @generated
	 * @ordered
	 */
	protected CorrespondenceNode corrNode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrNodeDependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuleDependencyPackage.Literals.CORR_NODE_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RuleDependencyNode> getRequiredDependencies() {
		if (requiredDependencies == null)
		{
			requiredDependencies = new EObjectResolvingEList<RuleDependencyNode>(RuleDependencyNode.class, this, RuleDependencyPackage.CORR_NODE_DEPENDENCY__REQUIRED_DEPENDENCIES);
		}
		return requiredDependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrespondenceNode getCorrNode() {
		if (corrNode != null && corrNode.eIsProxy())
		{
			InternalEObject oldCorrNode = (InternalEObject)corrNode;
			corrNode = (CorrespondenceNode)eResolveProxy(oldCorrNode);
			if (corrNode != oldCorrNode)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuleDependencyPackage.CORR_NODE_DEPENDENCY__CORR_NODE, oldCorrNode, corrNode));
			}
		}
		return corrNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrespondenceNode basicGetCorrNode() {
		return corrNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCorrNode(CorrespondenceNode newCorrNode) {
		CorrespondenceNode oldCorrNode = corrNode;
		corrNode = newCorrNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuleDependencyPackage.CORR_NODE_DEPENDENCY__CORR_NODE, oldCorrNode, corrNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID)
		{
			case RuleDependencyPackage.CORR_NODE_DEPENDENCY__REQUIRED_DEPENDENCIES:
				return getRequiredDependencies();
			case RuleDependencyPackage.CORR_NODE_DEPENDENCY__CORR_NODE:
				if (resolve) return getCorrNode();
				return basicGetCorrNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID)
		{
			case RuleDependencyPackage.CORR_NODE_DEPENDENCY__REQUIRED_DEPENDENCIES:
				getRequiredDependencies().clear();
				getRequiredDependencies().addAll((Collection<? extends RuleDependencyNode>)newValue);
				return;
			case RuleDependencyPackage.CORR_NODE_DEPENDENCY__CORR_NODE:
				setCorrNode((CorrespondenceNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID)
		{
			case RuleDependencyPackage.CORR_NODE_DEPENDENCY__REQUIRED_DEPENDENCIES:
				getRequiredDependencies().clear();
				return;
			case RuleDependencyPackage.CORR_NODE_DEPENDENCY__CORR_NODE:
				setCorrNode((CorrespondenceNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID)
		{
			case RuleDependencyPackage.CORR_NODE_DEPENDENCY__REQUIRED_DEPENDENCIES:
				return requiredDependencies != null && !requiredDependencies.isEmpty();
			case RuleDependencyPackage.CORR_NODE_DEPENDENCY__CORR_NODE:
				return corrNode != null;
		}
		return super.eIsSet(featureID);
	}

} //CorrNodeDependencyImpl
