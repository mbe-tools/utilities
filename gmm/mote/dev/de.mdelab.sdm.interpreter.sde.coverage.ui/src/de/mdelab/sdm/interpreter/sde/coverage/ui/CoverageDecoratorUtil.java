package de.mdelab.sdm.interpreter.sde.coverage.ui;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.AbstractDecorator;

import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.ActivityEditPart;
import de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport;
import de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry;
import de.mdelab.sdm.interpreter.sde.coverage.CoverageReport;

/**
 * @author Patrick Rein CoverageDecoratorUtil organizes the coverage entries of
 *         a Coverage Report and their decorators. It needs the resource set of
 *         the corresponding diagrams.
 * 
 */
public class CoverageDecoratorUtil
{

	protected final static String										expressionCoveredSymbol	= "icons/expressionCovered.png";

	protected static ResourceSet										rs						= null;
	protected static Resource											r						= null;
	protected static Map<DiagramEditPart, Vector<AbstractDecorator>>	coverageDecorators		= new HashMap<DiagramEditPart, Vector<AbstractDecorator>>();

	public static Map<String, Integer> mapResource(Resource r)
	{
		Map<String, Integer> result = new HashMap<String, Integer>();

		EList<EObject> coverageReports = r.getContents();
		CoverageReport reportRoot = (CoverageReport) coverageReports.get(0);
		for (ActivityCoverageReport activityReport : reportRoot.getActivityCoverageReports())
		{
			for (CoverageEntry anEntry : activityReport.getCoverageEntries())
			{
				result.put(anEntry.getElement().getUuid(), (anEntry).getExecutions());
			}
		}

		return result;
	}

	public static void registerDecoratorInActivity(AbstractDecorator ad, DiagramEditPart activityEp)
	{
		if (!coverageDecorators.keySet().contains(activityEp))
		{
			coverageDecorators.put(activityEp, new Vector<AbstractDecorator>());
		}
		coverageDecorators.get(activityEp).add(ad);
	}

	public static Collection<AbstractDecorator> getAllDecoratorsForActivity(ActivityEditPart activityEp)
	{
		return coverageDecorators.get(activityEp);
	}
}
