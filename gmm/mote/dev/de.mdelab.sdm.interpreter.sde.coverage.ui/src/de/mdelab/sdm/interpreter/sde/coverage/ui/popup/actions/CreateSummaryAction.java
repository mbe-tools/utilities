package de.mdelab.sdm.interpreter.sde.coverage.ui.popup.actions;

import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

public class CreateSummaryAction implements IObjectActionDelegate
{
	private URI	uri;

	@Override
	public void run(IAction action)
	{
		new CreateSummaryJob("Create summary for '" + this.uri + "'...", this.uri).schedule();
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection)
	{
		action.setEnabled(false);

		if (!(selection instanceof IStructuredSelection) || selection.isEmpty())
		{
			return;
		}

		@SuppressWarnings("rawtypes")
		Iterator it = ((IStructuredSelection) selection).iterator();

		while (it.hasNext())
		{
			Object o = it.next();

			if (o instanceof IFile)
			{
				this.uri = URI.createPlatformResourceURI(((IFile) o).getFullPath().toString(), true);
			}
		}

		action.setEnabled(true);
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart)
	{
		// Do nothing
	}

}
