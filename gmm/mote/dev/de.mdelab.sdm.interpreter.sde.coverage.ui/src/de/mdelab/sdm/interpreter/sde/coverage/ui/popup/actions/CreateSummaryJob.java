package de.mdelab.sdm.interpreter.sde.coverage.ui.popup.actions;

import java.io.IOException;
import java.io.PrintStream;

import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import de.mdelab.sdm.interpreter.sde.coverage.CoverageReport;
import de.mdelab.sdm.interpreter.sde.coverage.CoverageSummaryGenerator;

public class CreateSummaryJob extends WorkspaceJob
{
	private static final String	PLUGIN_ID	= "de.hpi.sam.storyDiagramIntepreter.coverageAnalyzer.editor";
	private final URI			coverageReportURI;

	public CreateSummaryJob(String name, URI coverageReportURI)
	{
		super(name);

		this.coverageReportURI = coverageReportURI;
	}

	@Override
	public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException
	{
		/*
		 * Load the coverage report
		 */
		ResourceSet rs = new ResourceSetImpl();

		Resource r = rs.getResource(this.coverageReportURI, true);

		if (r == null)
		{
			return new Status(IStatus.ERROR, PLUGIN_ID, "Resource '" + this.coverageReportURI + "' does not exist.");
		}
		else if (r.getContents().isEmpty())
		{
			return new Status(IStatus.ERROR, PLUGIN_ID, "Resource '" + this.coverageReportURI + "' is empty.");
		}
		else if (!(r.getContents().get(0) instanceof CoverageReport))
		{
			return new Status(IStatus.ERROR, PLUGIN_ID, "Resource '" + this.coverageReportURI + "' contains no coverage reports.");
		}

		CoverageReport report = (CoverageReport) r.getContents().get(0);

		try
		{
			PrintStream printStream;
			printStream = new PrintStream(ExtensibleURIConverterImpl.INSTANCE.createOutputStream(this.coverageReportURI
					.appendFileExtension("txt")));

			printStream.print(CoverageSummaryGenerator.generateSummary(report));

			printStream.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return new Status(IStatus.ERROR, PLUGIN_ID, "Could not save coverage report summary.");
		}

		return new Status(IStatus.OK, PLUGIN_ID, null);
	}

}
