package de.mdelab.sdm.interpreter.sde.coverage.ui;

import java.net.URL;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Platform;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DecorationEditPolicy.DecoratorTarget;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.AbstractDecorator;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.IDecoratorTarget;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.IDecoratorTarget.Direction;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.draw2d.ui.mapmode.MapModeUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart;

/**
 * @author Patrick Rein A CoverageDecorator visualizes an CoverageReportEntry by
 *         showing the count of executions on the top right corner of a node.
 * 
 */
public class CoverageDecorator extends AbstractDecorator
{

	private Map<String, Integer>	resourceMap;

	public CoverageDecorator(IDecoratorTarget decoratorTarget)
	{
		super(decoratorTarget);
		this.resourceMap = null;
	}

	@Override
	public void activate()
	{
		this.refresh();
	}

	public void setResourceMap(Map<String, Integer> rm)
	{
		this.resourceMap = rm;
	}

	@Override
	public void refresh()
	{

		this.removeDecoration();
		EditPart editPart = (EditPart) this.getDecoratorTarget().getAdapter(EditPart.class);

		if (this.resourceMap == null)
		{
			return;
		}

		if (editPart instanceof StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart
				|| editPart instanceof StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart)
		{

			this.setDecorationForConstraints(editPart);

		}
		else
		{

			this.decorate(editPart, this.getDecoratorTarget());

		}
	}

	private void decorate(EditPart editPart, DecoratorTarget dt)
	{
		String uuid = this.determineUuid(editPart);

		if (this.resourceMap.containsKey(uuid))
		{
			int coverage = this.resourceMap.get(uuid);

			Ellipse decoration = (Ellipse) this.prepareFigure(coverage);
			int marginOffset = 0;
			Direction decorationDirection = IDecoratorTarget.Direction.NORTH_EAST;

			if (editPart instanceof AbstractConnectionEditPart)
			{
				marginOffset = 0;
				decorationDirection = IDecoratorTarget.Direction.CENTER;
			}
			else
			{
				marginOffset = -2;
			}

			this.setDecoration(dt.addShapeDecoration(decoration, decorationDirection,
					MapModeUtil.getMapMode(((AbstractGraphicalEditPart) editPart).getFigure()).DPtoLP(marginOffset), false));
		}
	}

	/**
	 * @param editPart
	 *            This method decorates the contraints of an element. It can
	 *            check e.g. all elements of a covered compartment even if those
	 *            elements have no decorators themself.
	 */
	private void setDecorationForConstraints(EditPart editPart)
	{
		EditPart constraintEditPart;
		String uuid = "";
		IFigure constraintFigure;

		for (Object anObject : editPart.getChildren())
		{
			constraintEditPart = (EditPart) anObject;
			uuid = this.determineUuid(constraintEditPart);
			if (this.resourceMap.containsKey(uuid))
			{
				constraintFigure = ((GraphicalEditPart) constraintEditPart).getFigure();

				URL url = null;
				Bundle bundle = Platform.getBundle("de.hpi.sam.storyDiagramInterpreter.coverageAnalyzer");

				url = bundle.getEntry(CoverageDecoratorUtil.expressionCoveredSymbol);
				ImageDescriptor imageDesc = ImageDescriptor.createFromURL(url);
				Image image = imageDesc.createImage();

				((WrappingLabel) constraintFigure).setIcon(image);
			}
		}
	}

	private String determineUuid(EditPart editPart)
	{
		return ((NamedElement) ((View) editPart.getModel()).getElement()).getUuid();
	}

	private IFigure prepareFigure(int coverage)
	{
		Label coverageLabel = new Label();
		coverageLabel.setText(Integer.toString(coverage));

		Ellipse decoration = new Ellipse();
		decoration.setSize(25, 25);
		decoration.setLayoutManager(new StackLayout()
		{
			@Override
			public void layout(IFigure figure)
			{
				Rectangle r = figure.getClientArea();
				@SuppressWarnings("rawtypes")
				List children = figure.getChildren();
				IFigure child;
				Dimension d;
				for (int i = 0; i < children.size(); i++)
				{
					child = (IFigure) children.get(i);
					d = child.getPreferredSize(r.width, r.height);
					d.width = Math.min(d.width, r.width);
					d.height = Math.min(d.height, r.height);
					Rectangle childRect = new Rectangle(r.x + (r.width - d.width) / 2, r.y + (r.height - d.height) / 2, d.width, d.height);
					child.setBounds(childRect);
				}
			}
		});

		if (coverage == 0)
		{
			decoration.setBackgroundColor(ColorConstants.red);
		}
		else
		{
			decoration.setBackgroundColor(ColorConstants.green);
		}

		decoration.add(coverageLabel);
		return decoration;
	}

}
