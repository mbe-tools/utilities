package de.mdelab.sdm.interpreter.sde.coverage.ui;

import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.EvaluationContext;
import org.eclipse.emf.common.ui.dialogs.ResourceDialog;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.AbstractDecorator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

import de.hpi.sam.storyDiagramEcore.diagram.custom.edit.parts.CustomActivityEditPart;

public class LoadCoverageReportAction extends AbstractHandler
{

	@SuppressWarnings("unchecked")
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		IEditorPart diagramEditor = HandlerUtil.getActiveEditorChecked(event);
		Shell shell = diagramEditor.getEditorSite().getShell();

		ResourceDialog loadResourceDialog = new ResourceDialog(shell, "Select the coverage report", SWT.SINGLE);

		if (loadResourceDialog.open() == ResourceDialog.OK && !loadResourceDialog.getURIs().isEmpty())
		{

			if (!loadResourceDialog.getURIs().get(0).fileExtension().equals("coverage"))
			{
				return null;
			}

			@SuppressWarnings("rawtypes")
			List list = (List) ((EvaluationContext) event.getApplicationContext()).getDefaultVariable();
			CustomActivityEditPart activityEditPart = (CustomActivityEditPart) list.get(0);

			Resource coverageResource = activityEditPart.getEditingDomain().getResourceSet()
					.getResource(loadResourceDialog.getURIs().get(0), true);

			if (coverageResource != null)
			{
				@SuppressWarnings("rawtypes")
				Map resourceMap = CoverageDecoratorUtil.mapResource(coverageResource);
				for (AbstractDecorator a : CoverageDecoratorUtil.getAllDecoratorsForActivity(activityEditPart))
				{
					((CoverageDecorator) a).setResourceMap(resourceMap);
					a.refresh();
				}
			}
		}
		return null;
	}

}
