package de.mdelab.sdm.interpreter.sde.coverage.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IPrimaryEditPart;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.CreateDecoratorsOperation;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.IDecoratorProvider;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.IDecoratorTarget;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart;
import de.hpi.sam.storyDiagramEcore.diagram.edit.parts.StringExpressionEditPart;
import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.storyDiagramEcore.expressions.StringExpression;
import de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.DecisionNode;
import de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.ForkNode;
import de.hpi.sam.storyDiagramEcore.nodes.InitialNode;
import de.hpi.sam.storyDiagramEcore.nodes.JoinNode;
import de.hpi.sam.storyDiagramEcore.nodes.MergeNode;
import de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge;
import de.hpi.sam.storyDiagramEcore.nodes.Semaphore;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

/**
 * @author Patrick Rein A CoverageDecoratorProvider produces CoverageDecorators
 *         to visualize a CoverageReport.
 * 
 */
public class CoverageDecoratorProvider extends AbstractProvider implements IDecoratorProvider
{

	@Override
	public boolean provides(IOperation operation)
	{
		if (!(operation instanceof CreateDecoratorsOperation))
		{
			return false;
		}

		IDecoratorTarget decoratorTarget = ((CreateDecoratorsOperation) operation).getDecoratorTarget();
		EditPart ep = (EditPart) decoratorTarget.getAdapter(EditPart.class);
		final Object model = ep.getModel();
		
		// DB: This may be called from non GMF based editors and the model may not be a view.
		if ( model instanceof View ) {
			final View view = (View) model;
			EObject element = view.getElement();

			return this.checkType(element);
		}
		
		return false;
	}

	@Override
	public void createDecorators(IDecoratorTarget decoratorTarget)
	{

		EditPart ep = (EditPart) decoratorTarget.getAdapter(EditPart.class);
		DiagramEditPart activityEp = (DiagramEditPart) ep.getRoot().getChildren().get(0);
		View view = (View) ep.getModel();
		EObject element = view.getElement();

		if (ep instanceof AbstractGraphicalEditPart
				&& (ep instanceof IPrimaryEditPart || ep instanceof StringExpressionEditPart
						|| ep instanceof StoryPatternObjectStoryPatternObjectConstraintsCompartmentEditPart || ep instanceof StoryActionNodeStoryActionNodeConstraintsCompartmentEditPart))
		{
			if (this.checkType(element))
			{

				CoverageDecorator cd = new CoverageDecorator(decoratorTarget);
				CoverageDecoratorUtil.registerDecoratorInActivity(cd, activityEp);

				decoratorTarget.installDecorator("COVERAGE_DECORATOR", cd);
			}
		}

	}

	protected boolean checkType(EObject element)
	{
		return element instanceof StoryActionNode || element instanceof StoryPatternObject || element instanceof ActivityEdge
				|| element instanceof StoryPatternLink || element instanceof MergeNode || element instanceof DecisionNode
				|| element instanceof InitialNode || element instanceof FlowFinalNode || element instanceof ActivityFinalNode
				|| element instanceof ExpressionActivityNode || element instanceof AcquireSemaphoreEdge
				|| element instanceof ReleaseSemaphoreEdge || element instanceof Semaphore || element instanceof SynchronizationEdge
				|| element instanceof JoinNode || element instanceof ForkNode || element instanceof CallActionExpression
				|| element instanceof StringExpression || element instanceof AttributeAssignment;
	}

}
