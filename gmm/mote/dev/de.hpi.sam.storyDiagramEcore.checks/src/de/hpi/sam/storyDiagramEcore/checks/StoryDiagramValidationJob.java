package de.hpi.sam.storyDiagramEcore.checks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.edit.ui.action.ValidateAction.EclipseResourcesUtil;

public class StoryDiagramValidationJob extends WorkspaceJob
{
	private static final String	PLUGIN_ID	= "de.hpi.sam.storyDiagramEcore.checks";
	private URI					uri			= null;

	public StoryDiagramValidationJob(String name, URI uri)
	{
		super(name);
		this.uri = uri;
	}

	@Override
	public IStatus runInWorkspace(final IProgressMonitor monitor) throws CoreException
	{
		/*
		 * Load model into memory.
		 */
		List<EObject> objectsToValidate = new ArrayList<EObject>();
		int count = 0;

		ResourceSet rs = new ResourceSetImpl();
		Resource resource;
		try
		{
			resource = rs.getResource(uri, true);
			objectsToValidate.addAll(resource.getContents());

			/*
			 * Count the number of objects to provide an accurate progress
			 * estimate.
			 */

			Iterator<EObject> it = resource.getAllContents();

			while (it.hasNext())
			{
				it.next();
				count++;
			}
		}
		catch (Exception ex)
		{
			return new Status(IStatus.ERROR, PLUGIN_ID, "Could not load file '" + uri + "'.");
		}

		monitor.beginTask("Validating model '" + uri.lastSegment() + "'...", count);

		/*
		 * Create a diagnostician that performs the validation
		 */
		Diagnostician diagnostician = new Diagnostician()
		{

			@Override
			public boolean validate(EClass eClass, EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context)
			{
				/*
				 * Overwrite validate to react on cancel requests and update the
				 * progress bar.
				 */
				if (monitor.isCanceled())
				{
					return false;
				}

				monitor.worked(1);
				super.validate(eClass, eObject, diagnostics, context);

				return true;
			}

		};

		/*
		 * The diagnostic contains the validation results.
		 */
		BasicDiagnostic diagnostic;

		if (objectsToValidate.size() == 1)
		{
			diagnostic = diagnostician.createDefaultDiagnostic(objectsToValidate.get(0));
		}
		else
		{
			diagnostic = new BasicDiagnostic(EObjectValidator.DIAGNOSTIC_SOURCE, 0, "Validating...", objectsToValidate.toArray());
		}

		Map<Object, Object> context = diagnostician.createDefaultContext();

		/*
		 * Perform the validation
		 */
		for (EObject eObject : objectsToValidate)
		{
			monitor.setTaskName("Validating '" + eObject + "'...");

			if (monitor.isCanceled() || !diagnostician.validate(eObject, diagnostic, context))
			{
				return new Status(IStatus.CANCEL, PLUGIN_ID, "");
			}
		}

		/*
		 * Create error and warning markers
		 */
		EclipseResourcesUtil eclipseResourcesUtil = new EclipseResourcesUtil();

		/*
		 * Delete old markers for this resource
		 */
		eclipseResourcesUtil.deleteMarkers(resource);

		/*
		 * Create new markers
		 */
		for (Diagnostic childDiagnostic : diagnostic.getChildren())
		{
			eclipseResourcesUtil.createMarkers(resource, childDiagnostic);
		}

		return new Status(IStatus.OK, PLUGIN_ID, "");
	}

}
