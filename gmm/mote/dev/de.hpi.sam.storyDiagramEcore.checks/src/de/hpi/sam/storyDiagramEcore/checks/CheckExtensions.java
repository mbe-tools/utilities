package de.hpi.sam.storyDiagramEcore.checks;

import java.util.Collection;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import de.hpi.sam.storyDiagramEcore.ActivityDiagram;

public class CheckExtensions
{
	/**
	 * Returns true if there is a proxy in the activity diagram that cannot be
	 * resolved.
	 * 
	 * @param e
	 * @return
	 */
	public static boolean findProxies(ActivityDiagram e)
	{
		if (checkReferencesForProxies(e))
		{
			return true;
		}

		TreeIterator<EObject> it = e.eAllContents();

		while (it.hasNext())
		{
			if (checkReferencesForProxies(it.next()))
			{
				return true;
			}
		}

		return false;
	}

	private static boolean checkReferencesForProxies(EObject e)
	{
		for (EReference ref : e.eClass().getEAllReferences())
		{
			Object o = e.eGet(ref);

			if (o == null)
			{
				return false;
			}

			if (!ref.isMany())
			{
				if (((EObject) o).eIsProxy())
				{
					return true;
				}
			}
			else
			{
				for (EObject eObject : (Collection<EObject>) o)
				{
					if (eObject.eIsProxy())
					{
						return true;
					}
				}
			}
		}
		return false;
	}
}
