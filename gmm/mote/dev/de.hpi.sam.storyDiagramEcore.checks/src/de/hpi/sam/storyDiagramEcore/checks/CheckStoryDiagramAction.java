package de.hpi.sam.storyDiagramEcore.checks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

public class CheckStoryDiagramAction implements IObjectActionDelegate
{
	private List<URI>	uris	= new ArrayList<URI>();

	@Override
	public void run(IAction action)
	{
		for (URI uri : uris)
		{
			new StoryDiagramValidationJob("Validate '" + uri.lastSegment() + "'...", uri).schedule();
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection)
	{
		action.setEnabled(false);

		uris.clear();

		if (!(selection instanceof IStructuredSelection) || selection.isEmpty())
		{
			return;
		}

		@SuppressWarnings("rawtypes")
		Iterator it = ((IStructuredSelection) selection).iterator();

		while (it.hasNext())
		{
			Object o = it.next();

			if (o instanceof IFile)
			{
				uris.add(URI.createPlatformResourceURI(((IFile) o).getFullPath().toString(), true));
			}
		}

		action.setEnabled(true);

	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart)
	{
	}
}
