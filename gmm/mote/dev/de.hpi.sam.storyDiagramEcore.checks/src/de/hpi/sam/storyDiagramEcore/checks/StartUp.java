package de.hpi.sam.storyDiagramEcore.checks;

import org.eclipse.ui.IStartup;
import org.eclipse.xtend.typesystem.emf.check.CheckRegistry;

public class StartUp implements IStartup
{
	public void earlyStartup()
	{
		CheckRegistry.getInstance();
	}
}
