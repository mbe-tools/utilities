package de.hpi.sam.storyDiagramEcore.checks;

public class OCLStoryDiagramChecker
{
	// static private String error = "";
	// static private Map<ActivityNode, EClassifier> checkedNodes = new
	// HashMap<ActivityNode, EClassifier>();
	// static private Vector<ActivityNode> nodesToVisit = new
	// Vector<ActivityNode>();
	//
	// static private OCL ocl = OCL.newInstance();
	// static private Helper oclHelper = ocl.createOCLHelper();
	//
	// public static String getErrorMessage()
	// {
	// return error;
	// }
	//
	// public static boolean checkOCLExpression(ActivityDiagram ad)
	// {
	// // Check OCL constraint for each activity in the diagram.
	// for (Activity activity : ad.getActivities())
	// {
	// // start at initial node
	// ActivityNode node = null;
	// for (EObject ob : activity.getNodes())
	// {
	// if (ob instanceof InitialNode)
	// {
	// node = (InitialNode) ob;
	// break;
	// }
	// }
	// if (node == null)
	// {
	// error = "No initial node found!";
	// return false;
	// }
	//
	// //traverse the activity element
	// while (node != null)
	// {
	// // process current node
	// if (node instanceof StoryActionNode)
	// {
	// StoryActionNode storyActionNode = (StoryActionNode) node;
	//
	// if (!checkOCLContraintOnNode(storyActionNode))
	// {
	// error = "Invalid OCL constraint in story action node " +
	// storyActionNode.getName() + ".\n" + error;
	// return false;
	// }
	//
	// }
	// else if (node instanceof CallActionNode)
	// {
	// CallActionNode callActionNode = (CallActionNode) node;
	//
	// if (!checkOCLContraintOnNode(callActionNode))
	// {
	// error = "Invalid OCL constraint in call action node " +
	// callActionNode.getName() + ".\n" + error;
	// return false;
	// }
	// }
	//
	// // process all outgoing edges
	// if (node.getOutgoing().isEmpty())
	// {
	// node = getNextNode(node);
	// continue;
	// }
	//
	// for (ActivityEdge edge : node.getOutgoing())
	// if (!checkOCLContraintOnEdge(edge))
	// {
	// error = "Invalid OCL constraint in edge " + edge.getName() + ".\n" +
	// error;
	// return false;
	// }
	//
	// node = getNextNode(node);
	// }
	//
	// }
	// return true;
	// }
	//
	// private static ActivityNode getNextNode(ActivityNode node)
	// {
	// checkedNodes.put(node, node.eClass());
	//
	// for (ActivityEdge edge : node.getOutgoing())
	// {
	// ActivityNode n = edge.getTarget();
	// if (!checkedNodes.containsKey(n) && !nodesToVisit.contains(n))
	// nodesToVisit.add(n);
	// }
	//
	// if (nodesToVisit.isEmpty())
	// return null;
	//
	// ActivityNode result = nodesToVisit.lastElement();
	// nodesToVisit.remove(nodesToVisit.size()-1);
	//
	// return result;
	// }
	//
	// private static boolean checkOCLContraintOnNode(CallActionNode
	// callActionNode)
	// {
	// boolean success = true;
	//
	// //TODO only OCL at first hierarchy level will be found!!!
	//
	// //only ValueExpressionActions could have one constraint
	// for (CallAction action : callActionNode.getCallActions())
	// {
	// if (action instanceof ValueExpressionAction)
	// {
	// ValueExpressionAction eAction = (ValueExpressionAction) action;
	// Constraint constraint = eAction.getConstraint();
	// if (constraint != null && constraint.getConstraintLanguage() ==
	// ConstraintLanguagesEnumeration.OCL)
	// success = checkOclExpression(eAction,
	// constraint.getConstraintExpression());
	// if (!success)
	// return success;
	// }
	// }
	//
	// return success;
	// }
	//
	// private static boolean checkOCLContraintOnNode(StoryActionNode
	// storyActionNode)
	// {
	// boolean success = true;
	//
	// // check all OCL constraints on StoryActionNode
	// EList<Constraint> list = storyActionNode.getConstraints();
	// if (list != null && list.size() != 0)
	// {
	// for (Constraint c : list)
	// {
	// if (c != null && c.getConstraintLanguage() ==
	// ConstraintLanguagesEnumeration.OCL)
	// success = checkOclExpression(storyActionNode,
	// c.getConstraintExpression());
	// if (!success)
	// return success;
	// }
	//
	// }
	//
	// //check each story pattern object
	// EList<StoryPatternObject> objects =
	// storyActionNode.getStoryPatternObjects();
	// if (objects != null && objects.size() != 0)
	// {
	// for (StoryPatternObject o : objects)
	// {
	// list = o.getConstraints();
	// if (list != null && list.size() != 0)
	// {
	// for (Constraint c : list)
	// {
	// if (c != null && c.getConstraintLanguage() ==
	// ConstraintLanguagesEnumeration.OCL)
	// success = checkOclExpression(o, c.getConstraintExpression());
	// if (!success)
	// return success;
	// }
	//
	// }
	// }
	// }
	//
	// return success;
	// }
	//
	// private static boolean checkOCLContraintOnEdge(ActivityEdge edge)
	// {
	// boolean success = true;
	// ActivityEdgeGuard guard = edge.getGuard();
	//
	// // if no guard is found, all OCL constraints are syntactically correct
	// if (guard == null)
	// return success;
	//
	// //only boolean guards can have an OCL constraint
	// if (guard.getGuardType() != ActivityEdgeGuardEnumeration.BOOLEAN)
	// return success;
	//
	// Constraint constraint = guard.getConstraint();
	//
	// if (constraint != null && constraint.getConstraintLanguage() ==
	// ConstraintLanguagesEnumeration.OCL)
	// success = checkOclExpression(edge, constraint.getConstraintExpression());
	//
	// return success;
	// }
	//
	// private static boolean checkOclExpression(NamedElement context, String
	// expression)
	// {
	// /*
	// // Create a new OCL instance
	// oclHelper.setContext(context.eClass());
	//
	// // Return true if there are no constraints or only assignments
	// if (expression != null && !expression.equals(""))
	// {
	// try
	// {
	// ocl.createQuery(oclHelper.createQuery(expression));
	// }
	// catch (ParserException e)
	// {
	// error = "Error message: " + e.getMessage();
	// return false;
	// }
	// }
	// */
	// return true;
	// }
	//
	// private static void setNewVariable(String name, EClassifier classifier,
	// EObject value)
	// {
	// /*
	// // Add the variable the OCL environment and evaluation environment.
	// Variable<EClassifier, EParameter> v =
	// ocl.getEnvironment().getOCLFactory().createVariable();
	// v.setName(name);
	// v.setType(getOclTypeFrom(classifier));
	//
	// ocl.getEnvironment().addElement(v.getName(), v, true);
	//
	//
	// */
	// }
	//
	// private static void removeExistingVariable(String name)
	// {
	// ocl.getEnvironment().deleteElement(name);
	// ocl.getEvaluationEnvironment().remove(name);
	// }
	//
	// private static EClassifier getOclTypeFrom(EClassifier classifier)
	// {
	// //TODO all primitive DataTypes EDouble EDoubleObject, EFloat....
	// if
	// (EcorePackage.eINSTANCE.getEInt().getInstanceTypeName().equals(classifier.getInstanceTypeName())
	// ||
	// EcorePackage.eINSTANCE.getEIntegerObject().getInstanceTypeName().equals(classifier.getInstanceTypeName()))
	// {
	// return ocl.getEnvironment().getOCLStandardLibrary().getInteger();
	// }
	// else
	// {
	// return classifier;
	// }
	// }
}
