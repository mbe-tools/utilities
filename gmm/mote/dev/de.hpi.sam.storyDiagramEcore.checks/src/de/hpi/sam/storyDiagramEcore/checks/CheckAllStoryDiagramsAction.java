package de.hpi.sam.storyDiagramEcore.checks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

public class CheckAllStoryDiagramsAction implements IObjectActionDelegate
{
	private final List<IContainer>	projects	= new ArrayList<IContainer>();

	@Override
	public void run(IAction action)
	{
		for (IContainer project : this.projects)
		{
			try
			{
				validate(project);
			}
			catch (CoreException e)
			{
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
	}

	private void validate(IResource resource) throws CoreException
	{
		if (resource instanceof IFile)
		{
			IFile file = (IFile) resource;

			if ("story".equals(file.getFileExtension()))
			{
				URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
				new StoryDiagramValidationJob("Validate '" + uri.lastSegment() + "'...", uri).schedule();
			}

		}
		else if (resource instanceof IContainer)
		{
			for (IResource childResource : ((IContainer) resource).members())
			{
				validate(childResource);
			}
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection)
	{
		action.setEnabled(false);

		this.projects.clear();

		if (!(selection instanceof IStructuredSelection) || selection.isEmpty())
		{
			return;
		}

		@SuppressWarnings("rawtypes")
		Iterator it = ((IStructuredSelection) selection).iterator();

		while (it.hasNext())
		{
			Object o = it.next();

			if (o instanceof IContainer)
			{
				this.projects.add((IContainer) o);
			}
		}

		action.setEnabled(true);

	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart)
	{
	}
}
