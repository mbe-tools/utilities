import ecore;

import storyDiagramEcore;
import storyDiagramEcore::nodes;
import storyDiagramEcore::sdm;
import storyDiagramEcore::expressions;

// Story Action Node
context StoryActionNode ERROR
	"This story action node does not contain any story pattern objects." : 
		storyPatternObjects.size >= 1;

context StoryActionNode ERROR
	"This story action node must contain at least one story pattern object that is bound or that can be bound." : 
		this.storyPatternObjects
			.select(e|e.metaType == StoryPatternObject)
			.exists(e|((StoryPatternObject)e).bindingType == BindingTypeEnumeration::BOUND ||
			((StoryPatternObject)e).bindingType == BindingTypeEnumeration::CAN_BE_BOUND) ||
		this.storyPatternObjects.forAll(e|e.modifier == StoryPatternModifierEnumeration::CREATE);

context StoryActionNode if this.forEach WARNING
	"Caution: This for-each node contains story pattern objects with binding expressions. These are only evaluated during the first iteration of this for-each node. Move these objects to a separate story node in the for-each node's loop body if you want it to be evaluated in each iteration.":
	!this.storyPatternObjects.select(e|e.matchType == StoryPatternObject).exists(e|((StoryPatternObject)e).directAssignmentExpression != null);

//Abstract Story Pattern Object
context AbstractStoryPatternObject ERROR
	"The classifier (i.e. the type) of this story pattern object is not set." : 
		classifier != null ;//&& !classifier.eIsProxy();

context AbstractStoryPatternObject if this.modifier == StoryPatternModifierEnumeration::CREATE && this.classifier.metaType == EClass ERROR
	"The classifier of this story pattern object is abstract. No instances of this classifier can be created.":
		!((EClass)this.classifier).abstract;

context AbstractStoryPatternObject if this.classifier.metaType != ecore::EClass ERROR
	"A story pattern object whose type is not an EClass may not be the source of a story pattern link.":
		this.outgoingStoryLinks.isEmpty;
		
//StoryPatternObject		
context StoryPatternObject ERROR
	"A story pattern object must have a name.":
	this.name != null && this.name.trim() != "";

context StoryPatternObject ERROR
	"A Story Pattern may not contain multiple objects with the same name.":
		((StoryActionNode) this.eContainer).storyPatternObjects.select(e|e.metaType == StoryPatternObject && e.name == this.name).size == 1; 

context StoryPatternObject if this.name == "this" ERROR
	"The 'this' object must be bound." :
		bindingType == BindingTypeEnumeration::BOUND;
	
context StoryPatternObject if this.name == "this" ERROR
	"The THIS object cannot be created or destroyed." :
		this.modifier == StoryPatternModifierEnumeration::NONE; 	
		
context StoryPatternObject if this.modifier == StoryPatternModifierEnumeration::CREATE && this.matchType != StoryPatternMatchTypeEnumeration::OPTIONAL ERROR
	"Objects that are created cannot have constraints." : 
		this.constraints.isEmpty;

context StoryPatternObject if this.modifier == StoryPatternModifierEnumeration::DESTROY WARNING
	"This object has attribute assignments although it is destroyed by the story pattern." : 
		attributeAssignments.isEmpty;
		
context StoryPatternObject if this.directAssignmentExpression != null ERROR
	"A story pattern object with a direct assignment must be bound." :
		this.bindingType ==  BindingTypeEnumeration::BOUND;
		
//Every story pattern object (that is not created and not bound) must have an incoming story pattern link, 
//or one of the outgoing story pattern links must have an EReference that has an EOpposite
context StoryPatternObject if this.bindingType != BindingTypeEnumeration::BOUND && this.modifier != StoryPatternModifierEnumeration::CREATE ERROR
	"This story pattern object is not reachable from another bound story pattern object." :
		!this.incomingStoryLinks.isEmpty || //there is an incoming link
		this.outgoingStoryLinks.exists(e| //or there is a link
			e.metaType == StoryPatternLink && //that is a StoryPatternLink (the type of outgoingStoryLinks is AbstractStoryPatternLink)
			((StoryPatternLink)e).eStructuralFeature.metaType == EReference && //whose structural feature is an EReference
			(((EReference)((StoryPatternLink)e).eStructuralFeature).eOpposite != null || //that has an eOpposite
			((EReference)((StoryPatternLink)e).eStructuralFeature).containment) || //or is a containment reference.
			e.metaType == StoryPatternContainmentLink) || // or is a containment link
		//or there is a MapEntryStoryPatternLink, whose valueTarget points at the object
		((StoryActionNode) this.eContainer).storyPatternLinks.exists(e|e.metaType == MapEntryStoryPatternLink && ((MapEntryStoryPatternLink) e).valueTarget == this);

context StoryPatternObject ERROR
	"'__RETURN_VALUE__' is a reserved name. Please use a different name for this story pattern object.":
	this.name != "__RETURN_VALUE__"; 
		
//Abstract Story Pattern Link
context AbstractStoryPatternLink ERROR
	"The source of the story pattern link is not set." :
		this.source != null;
		
context AbstractStoryPatternLink ERROR
	"The target of the story pattern link is not set." :
		this.target != null;

context AbstractStoryPatternLink if this.source.modifier == StoryPatternModifierEnumeration::CREATE ERROR
	"The modifier of this link must be set to CREATE because the source object is also created." : 
		this.modifier == StoryPatternModifierEnumeration::CREATE;
		
context AbstractStoryPatternLink if this.target.modifier == StoryPatternModifierEnumeration::CREATE ERROR
	"The modifier of this link must be set to CREATE because the target object is also created." : 
		this.modifier == StoryPatternModifierEnumeration::CREATE;
		
/*context AbstractStoryPatternLink if this.source.modifier == StoryPatternModifierEnumeration::DESTROY ERROR
	"The modifier of this link must be set to DESTROY because the source object is also destroyed." : 
		this.modifier == StoryPatternModifierEnumeration::DESTROY;
		
context AbstractStoryPatternLink if this.target.modifier == StoryPatternModifierEnumeration::DESTROY ERROR
	"The modifier of this link must be set to DESTROY because the target object is also destroyed." : 
		this.modifier == StoryPatternModifierEnumeration::DESTROY;
*/
context AbstractStoryPatternLink ERROR
	"The story pattern link, the source and the target story pattern object must reside in the same story action node." :
		this.eContainer == this.source.eContainer && this.eContainer == target.eContainer;

context AbstractStoryPatternLink if this.oppositeStoryPatternLink != null ERROR
	"The modifier and the match type of this link an the opposite link must be equal." :
		this.modifier == oppositeStoryPatternLink.modifier && this.matchType == oppositeStoryPatternLink.matchType;

context AbstractStoryPatternLink ERROR
	"The match type THIS_OBJECT is not allowed for story pattern links." : 
		this.matchType != StoryPatternMatchTypeEnumeration::THIS_OBJECT;

context AbstractStoryPatternLink if this.oppositeStoryPatternLink != null ERROR
	"The source and target objects of the opposite story pattern link do not match this link's target and source objects." : 
		this.source == this.oppositeStoryPatternLink.target && this.target == this.oppositeStoryPatternLink.source;

//MapEntryStoryPatternLink
context MapEntryStoryPatternLink ERROR
	"The EStructuralFeature of this link must be a to-many containment reference. The instance type name of this link's classifier must be 'java.util.Map$Entry'.":
	this.eStructuralFeature.metaType == EReference && //The structural feature must be an EReference
	((EReference)this.eStructuralFeature).many && //The reference must be a to many reference
	((EReference)this.eStructuralFeature).containment && //and a containment
	((EReference)this.eStructuralFeature).eType.instanceTypeName == "java.util.Map$Entry"; //The instance type name of the type of the reference must be java.util.Map$Entry

context MapEntryStoryPatternLink if this.valueTarget != null && this.valueTarget.modifier != StoryPatternModifierEnumeration::NONE ERROR
	"The modifier of this link must be NONE, because the value object's modifier is " + this.valueTarget.modifier + ".":
	this.modifier == this.valueTarget.modifier;
	
context MapEntryStoryPatternLink ERROR
	"The classifier must be set.":
	this.classifier != null;
	
context MapEntryStoryPatternLink if this.classifier != null && this.eStructuralFeature != null ERROR
	"The classifier of this MapEntryStoryPatternLink does not match the structural feature's type.":
	this.classifier == this.eStructuralFeature.eType;

//Story Pattern Link
context StoryPatternLink ERROR
	"The EReference of the story pattern link is not set." :
		this.eStructuralFeature != null;
		
context StoryPatternLink if oppositeStoryPatternLink != null && oppositeStoryPatternLink.metaType == StoryPatternLink ERROR
	"The opposite reference from the meta model does not match the eReference of the opposite story pattern link." : 
		((ecore::EReference) this.eStructuralFeature).eOpposite == ((StoryPatternLink) this.oppositeStoryPatternLink).eStructuralFeature;
		
context StoryPatternLink if this.source.classifier.metaType == EClass ERROR
	"The EReference of this link does not belong to the source story pattern object's EClass." :
		((EClass) this.source.classifier).eAllStructuralFeatures.contains(this.eStructuralFeature); 

context StoryPatternLink if this.eStructuralFeature != null && this.eStructuralFeature.eType.metaType == EClass && this.target != null && 
		this.target.classifier != null && this.target.classifier.metaType == EClass ERROR
	"The type of the target object of this link is not compatible to the type of this link. Expected type is '" + this.eStructuralFeature.eType.name  +".":
	this.target.classifier == this.eStructuralFeature.eType || ((EClass) this.eStructuralFeature.eType).isSuperTypeOf((EClass) this.target.classifier) ||
	this.eStructuralFeature.eType.name == "EObject"; //This is a hack. For some reason, a direct comparison with ecore::EObject always fails.

//StoryPatternExpressionLink
context StoryPatternExpressionLink ERROR
	"This story pattern expression link does not have an expression." :
	this.expression != null;

context StoryPatternExpressionLink if this.expression.metaType == CallActionExpression ERROR
	"The type of the target story pattern object of this link (" + this.target.classifier.name +
	") is not a supertype of the return type of the expression of this link (" + ((CallActionExpression)this.expression).callActions.last().classifier.name + ").":
	let callActionClassifier = ((CallActionExpression)this.expression).callActions.last().classifier :
		callActionClassifier.metaType == EClass && this.target.classifier.metaType == EClass ?
			((EClass)this.target.classifier).isSuperTypeOf((EClass)callActionClassifier)
			:true;
			
context StoryPatternExpressionLink ERROR
	"A StoryPatternExpressionLink cannot be created or destroyed.":
	this.modifier == StoryPatternModifierEnumeration::NONE;

//StoryPatternContainmentLink
context StoryPatternContainmentLink ERROR
	"A StoryPatternContainmentLink cannot be created.":
	this.modifier != StoryPatternModifierEnumeration::CREATE;

//Attribute Assignment
context AttributeAssignment ERROR
	"The structural feature is not set." : 
		eStructuralFeature != null;
		
context AttributeAssignment if ((AbstractStoryPatternObject) this.eContainer).classifier.metaType == EClass ERROR
	"The classifier '" + ((AbstractStoryPatternObject) this.eContainer).classifier.name + "' does not contain structural feature '" + this.eStructuralFeature.eContainingClass.name + "::" + this.eStructuralFeature.name +"'." : 
		((EClass)((AbstractStoryPatternObject)this.eContainer).classifier).eAllStructuralFeatures.contains(this.eStructuralFeature);
		
context AttributeAssignment ERROR
	"The assignment expression must be set.":
	this.assignmentExpression != null;

//Unsupported features
context StoryPatternElement WARNING
	"Negative application conditions are currently not supported by the Story Diagram Interpreter. Use constraints instead.":
	this.matchType != StoryPatternMatchTypeEnumeration::NEGATIVE;
	
context StoryPatternElement WARNING
	"The match type SET is currently not supported by the Story Diagram Interpreter.":
	this.matchType != StoryPatternMatchTypeEnumeration::SET;
		
context StoryPatternLink WARNING
	"Matching priorities are currently not supported by the Story Diagram Interpreter.":
	this.matchingPriority == 0;

context StoryPatternLink if !this.eStructuralFeature.many ERROR
	"The feature of this link is not a many-valued feature. Link constraints do not make sense, here.":
	this.linkPositionConstraint == null && this.incomingLinkOrderConstraints.isEmpty && this.outgoingLinkOrderConstraints.isEmpty;
	
context StoryPatternLink if this.linkPositionConstraint.constraintType == LinkPositionConstraintEnumeration::FIRST ERROR
	"This link already has the FIRST position constraint. It cannot have a predecessor.":
	this.incomingLinkOrderConstraints.isEmpty;
	
context StoryPatternLink if this.linkPositionConstraint.constraintType == LinkPositionConstraintEnumeration::LAST ERROR
	"This link already has the LAST position constraint. It cannot have a sucessor.":
	this.outgoingLinkOrderConstraints.isEmpty;
	
	
//NACs
context StoryPatternObject if this.matchType == StoryPatternMatchTypeEnumeration::NEGATIVE ERROR
	"A negative object must be unbound and cannot be created or destroyed.":
	this.bindingType == BindingTypeEnumeration::UNBOUND && this.modifier == StoryPatternModifierEnumeration::NONE;

context AbstractStoryPatternLink if this.matchType == StoryPatternMatchTypeEnumeration::NEGATIVE ERROR
	"A negative link must cannot be created or destroyed.":
	this.modifier == StoryPatternModifierEnumeration::NONE;

context AbstractStoryPatternLink if this.source.matchType == StoryPatternMatchTypeEnumeration::NEGATIVE || this.target.matchType == StoryPatternMatchTypeEnumeration::NEGATIVE ERROR
	"This link must be negative because it is connected a negative object.":
	this.modifier == StoryPatternMatchTypeEnumeration::NEGATIVE;

context StoryActionNode ERROR
	"AND NAC semantics are not supported.":
	this.nacSemantic == StoryPatternNACSematicEnumeration::OR;

//Alle Links zu einem NEGATIVE Objekt m�ssen auch NEGATIVE sein
//AND und OR Semantik Schalter bezieht sich nur auf die StoryPatternObjects nicht auf die SPLinks.

