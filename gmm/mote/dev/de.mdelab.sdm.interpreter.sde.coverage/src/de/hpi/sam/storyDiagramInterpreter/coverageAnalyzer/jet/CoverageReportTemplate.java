package de.hpi.sam.storyDiagramInterpreter.coverageAnalyzer.jet;

 public class CoverageReportTemplate
 {
  protected static String nl;
  public static synchronized CoverageReportTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    CoverageReportTemplate result = new CoverageReportTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "Coverage report for ";
  protected final String TEXT_2 = ":" + NL + "" + NL + "Overall coverage: ";
  protected final String TEXT_3 = " %" + NL;
  protected final String TEXT_4 = NL + "Activity '";
  protected final String TEXT_5 = "' (";
  protected final String TEXT_6 = "#";
  protected final String TEXT_7 = ")" + NL + "Coverage: ";
  protected final String TEXT_8 = " %" + NL + "" + NL + "Uncovered Elements:";
  protected final String TEXT_9 = NL;
  protected final String TEXT_10 = " ";
  protected final String TEXT_11 = NL + NL + "------------------------------------------------------" + NL;

	public String generate(de.mdelab.sdm.interpreter.sde.coverage.CoverageReport coverageReport)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(coverageReport.eResource().getURI());
    stringBuffer.append(TEXT_2);
    stringBuffer.append((int) (de.mdelab.sdm.interpreter.sde.coverage.CoverageSummaryGenerator.calculateOverallCoverage(coverageReport) * 100));
    stringBuffer.append(TEXT_3);
    for (de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport activityCoverageReport : coverageReport.getActivityCoverageReports())
{
    stringBuffer.append(TEXT_4);
    stringBuffer.append(activityCoverageReport.getActivity().getName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(activityCoverageReport.getActivity().eResource().getURI());
    stringBuffer.append(TEXT_6);
    stringBuffer.append(activityCoverageReport.getActivity().eResource().getURIFragment(activityCoverageReport.getActivity()));
    stringBuffer.append(TEXT_7);
    stringBuffer.append((int) (de.mdelab.sdm.interpreter.sde.coverage.CoverageSummaryGenerator.calculateActivityCoverage(activityCoverageReport) * 100));
    stringBuffer.append(TEXT_8);
    for (String string : de.mdelab.sdm.interpreter.sde.coverage.CoverageSummaryGenerator.getUncoveredElementNames(activityCoverageReport)) {
    stringBuffer.append(TEXT_9);
    stringBuffer.append(string);
    stringBuffer.append(TEXT_10);
    }
    stringBuffer.append(TEXT_11);
    }
    return stringBuffer.toString();
  }
}
