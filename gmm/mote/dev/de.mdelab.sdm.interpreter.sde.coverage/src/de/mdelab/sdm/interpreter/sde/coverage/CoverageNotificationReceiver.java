package de.mdelab.sdm.interpreter.sde.coverage;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.mdelab.sdm.interpreter.core.notifications.ActivityExecutionStartedNotification;
import de.mdelab.sdm.interpreter.core.notifications.InterpreterNotification;
import de.mdelab.sdm.interpreter.sde.notifications.SDENotificationReceiver;

public class CoverageNotificationReceiver implements SDENotificationReceiver
{

	private final CoverageReport						coverageReport;

	private final HashMap<NamedElement, CoverageEntry>	coverageEntryMap;

	private final Map<Activity, ActivityCoverageReport>	activityCoverageReports;

	public CoverageNotificationReceiver()
	{
		this.coverageReport = CoverageFactory.eINSTANCE.createCoverageReport();

		this.coverageEntryMap = new HashMap<NamedElement, CoverageEntry>();

		this.activityCoverageReports = new HashMap<Activity, ActivityCoverageReport>();
	}

	@Override
	public void notifyChanged(InterpreterNotification<EClassifier> notification)
	{
		if (!(notification.getMainStoryDiagramElement() instanceof NamedElement))
		{
			return;
		}

		switch (notification.getNotificationType())
		{
			case ACTIVITY_EXECUTION_STARTED:
			{
				/*
				 * make sure that there is an entry for the activity and all its
				 * elements
				 */
				@SuppressWarnings("unchecked")
				Activity activity = ((ActivityExecutionStartedNotification<Activity, EClassifier>) notification).getActivity();

				ActivityCoverageReport activityCoverageReport = this.activityCoverageReports.get(activity);

				if (activityCoverageReport == null)
				{
					/*
					 * Create an activity coverage report for the activity
					 */
					activityCoverageReport = CoverageFactory.eINSTANCE.createActivityCoverageReport();
					activityCoverageReport.setActivity(activity);

					this.activityCoverageReports.put(activity, activityCoverageReport);

					this.coverageReport.getActivityCoverageReports().add(activityCoverageReport);

					/*
					 * Create coverage entries for the activity's contents
					 */
					TreeIterator<EObject> it = activity.eAllContents();

					while (it.hasNext())
					{
						EObject eObject = it.next();

						assert eObject instanceof NamedElement;

						if (eObject instanceof CallAction || eObject instanceof CallActionParameter)
						{
							/*
							 * Skip call actions
							 */
						}
						else
						{
							CoverageEntry entry = CoverageFactory.eINSTANCE.createCoverageEntry();
							entry.setElement((NamedElement) eObject);

							this.coverageEntryMap.put((NamedElement) eObject, entry);

							activityCoverageReport.getCoverageEntries().add(entry);
						}
					}
				}

				activityCoverageReport.setExecutions(activityCoverageReport.getExecutions() + 1);
				break;
			}

			case ACTIVITY_NODE_EXECUTION_STARTED:
			case EVALUATING_EXPRESSION:
			case INSTANCE_LINK_CREATED:
			case INSTANCE_LINK_DESTROYED:
			case INSTANCE_OBJECT_CREATED:
			case INSTANCE_OBJECT_DESTROYED:
			case STORY_PATTERN_OBJECT_BOUND:
			case STORY_PATTERN_OBJECT_NOT_BOUND:
			case STORY_PATTERN_OBJECT_BINDING_REVOKED:
			case TRAVERSING_ACTIVITY_EDGE:
			case TRAVERSING_LINK:
			{
				CoverageEntry entry = this.coverageEntryMap.get(notification.getMainStoryDiagramElement());

				assert entry != null;

				entry.setExecutions(entry.getExecutions() + 1);
			}
		}
	}

	public CoverageReport getCoverageReport()
	{
		return this.coverageReport;
	}
}
