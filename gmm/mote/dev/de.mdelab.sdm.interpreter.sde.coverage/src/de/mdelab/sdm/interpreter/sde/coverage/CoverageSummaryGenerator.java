package de.mdelab.sdm.interpreter.sde.coverage;

import java.util.LinkedList;
import java.util.List;

import de.hpi.sam.storyDiagramInterpreter.coverageAnalyzer.jet.CoverageReportTemplate;

public class CoverageSummaryGenerator
{
	public static String generateSummary(CoverageReport coverageReport)
	{
		return CoverageReportTemplate.create(null).generate(coverageReport);
	}

	/**
	 * Calculates the fraction of covered elements of the activity, i.e.
	 * elements which were executed at least once.
	 * 
	 * @param activityCoverageReport
	 * @return
	 */
	public static double calculateActivityCoverage(ActivityCoverageReport activityCoverageReport)
	{
		int count = 0;

		for (CoverageEntry entry : activityCoverageReport.getCoverageEntries())
		{
			if (entry.getExecutions() > 0)
			{
				count++;
			}
		}

		if (activityCoverageReport.getCoverageEntries().size() == 0)
		{
			return 1.0;
		}
		else
		{
			return (double) count / (double) activityCoverageReport.getCoverageEntries().size();
		}
	}

	public static List<String> getUncoveredElementNames(ActivityCoverageReport activityCoverageReport)
	{
		List<String> list = new LinkedList<String>();

		for (CoverageEntry entry : activityCoverageReport.getCoverageEntries())
		{
			if (entry.getExecutions() == 0)
			{
				list.add(entry.getElement().eClass().getName() + " '" + entry.getElement().getName() + "' (" + entry.getElement().getUuid()
						+ ")");
			}
		}

		return list;
	}

	public static double calculateOverallCoverage(CoverageReport coverageReport)
	{
		int count = 0;
		int noElements = 0;

		for (ActivityCoverageReport activityCoverageReport : coverageReport.getActivityCoverageReports())
		{
			for (CoverageEntry entry : activityCoverageReport.getCoverageEntries())
			{
				if (entry.getExecutions() > 0)
				{
					count++;
				}
			}

			noElements += activityCoverageReport.getCoverageEntries().size();
		}

		if (noElements == 0)
		{
			return 1.0;
		}
		else
		{
			return count / (double) noElements;
		}

	}
}
