/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.mdelab.sdm.interpreter.sde.coverage.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport;
import de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage;
import de.mdelab.sdm.interpreter.sde.coverage.CoverageReport;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Report</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link de.mdelab.sdm.interpreter.sde.coverage.impl.CoverageReportImpl#getActivityCoverageReports
 * <em>Activity Coverage Reports</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class CoverageReportImpl extends EObjectImpl implements CoverageReport
{
	/**
	 * The cached value of the '{@link #getActivityCoverageReports()
	 * <em>Activity Coverage Reports</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getActivityCoverageReports()
	 * @generated
	 * @ordered
	 */
	protected EList<ActivityCoverageReport>	activityCoverageReports;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected CoverageReportImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return CoveragePackage.Literals.COVERAGE_REPORT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ActivityCoverageReport> getActivityCoverageReports()
	{
		if (activityCoverageReports == null)
		{
			activityCoverageReports = new EObjectContainmentEList<ActivityCoverageReport>(ActivityCoverageReport.class, this,
					CoveragePackage.COVERAGE_REPORT__ACTIVITY_COVERAGE_REPORTS);
		}
		return activityCoverageReports;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
			case CoveragePackage.COVERAGE_REPORT__ACTIVITY_COVERAGE_REPORTS:
				return ((InternalEList<?>) getActivityCoverageReports()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case CoveragePackage.COVERAGE_REPORT__ACTIVITY_COVERAGE_REPORTS:
				return getActivityCoverageReports();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case CoveragePackage.COVERAGE_REPORT__ACTIVITY_COVERAGE_REPORTS:
				getActivityCoverageReports().clear();
				getActivityCoverageReports().addAll((Collection<? extends ActivityCoverageReport>) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case CoveragePackage.COVERAGE_REPORT__ACTIVITY_COVERAGE_REPORTS:
				getActivityCoverageReports().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case CoveragePackage.COVERAGE_REPORT__ACTIVITY_COVERAGE_REPORTS:
				return activityCoverageReports != null && !activityCoverageReports.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // CoverageReportImpl
