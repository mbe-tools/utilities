/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.mdelab.sdm.interpreter.sde.coverage.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport;
import de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry;
import de.mdelab.sdm.interpreter.sde.coverage.CoverageFactory;
import de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage;
import de.mdelab.sdm.interpreter.sde.coverage.CoverageReport;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class CoverageFactoryImpl extends EFactoryImpl implements CoverageFactory
{
	/**
	 * Creates the default factory implementation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public static CoverageFactory init()
	{
		try
		{
			CoverageFactory theCoverageFactory = (CoverageFactory) EPackage.Registry.INSTANCE
					.getEFactory("http://www.mdelab.de/coverage/1.0");
			if (theCoverageFactory != null)
			{
				return theCoverageFactory;
			}
		}
		catch (Exception exception)
		{
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CoverageFactoryImpl();
	}

	/**
	 * Creates an instance of the factory. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public CoverageFactoryImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass)
	{
		switch (eClass.getClassifierID())
		{
			case CoveragePackage.COVERAGE_REPORT:
				return createCoverageReport();
			case CoveragePackage.COVERAGE_ENTRY:
				return createCoverageEntry();
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT:
				return createActivityCoverageReport();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CoverageReport createCoverageReport()
	{
		CoverageReportImpl coverageReport = new CoverageReportImpl();
		return coverageReport;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CoverageEntry createCoverageEntry()
	{
		CoverageEntryImpl coverageEntry = new CoverageEntryImpl();
		return coverageEntry;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ActivityCoverageReport createActivityCoverageReport()
	{
		ActivityCoverageReportImpl activityCoverageReport = new ActivityCoverageReportImpl();
		return activityCoverageReport;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CoveragePackage getCoveragePackage()
	{
		return (CoveragePackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CoveragePackage getPackage()
	{
		return CoveragePackage.eINSTANCE;
	}

} // CoverageFactoryImpl
