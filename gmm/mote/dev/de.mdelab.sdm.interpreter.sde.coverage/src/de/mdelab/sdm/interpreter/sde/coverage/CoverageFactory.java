/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.mdelab.sdm.interpreter.sde.coverage;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * 
 * @see de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage
 * @generated
 */
public interface CoverageFactory extends EFactory
{
	/**
	 * The singleton instance of the factory. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	CoverageFactory	eINSTANCE	= de.mdelab.sdm.interpreter.sde.coverage.impl.CoverageFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Report</em>'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Report</em>'.
	 * @generated
	 */
	CoverageReport createCoverageReport();

	/**
	 * Returns a new object of class '<em>Entry</em>'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Entry</em>'.
	 * @generated
	 */
	CoverageEntry createCoverageEntry();

	/**
	 * Returns a new object of class '<em>Activity Coverage Report</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Activity Coverage Report</em>'.
	 * @generated
	 */
	ActivityCoverageReport createActivityCoverageReport();

	/**
	 * Returns the package supported by this factory. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the package supported by this factory.
	 * @generated
	 */
	CoveragePackage getCoveragePackage();

} // CoverageFactory
