/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.mdelab.sdm.interpreter.sde.coverage.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry;
import de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Entry</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link de.mdelab.sdm.interpreter.sde.coverage.impl.CoverageEntryImpl#getExecutions
 * <em>Executions</em>}</li>
 * <li>
 * {@link de.mdelab.sdm.interpreter.sde.coverage.impl.CoverageEntryImpl#getElement
 * <em>Element</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class CoverageEntryImpl extends EObjectImpl implements CoverageEntry
{
	/**
	 * The default value of the '{@link #getExecutions() <em>Executions</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getExecutions()
	 * @generated
	 * @ordered
	 */
	protected static final int	EXECUTIONS_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getExecutions() <em>Executions</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getExecutions()
	 * @generated
	 * @ordered
	 */
	protected int				executions			= EXECUTIONS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getElement() <em>Element</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getElement()
	 * @generated
	 * @ordered
	 */
	protected NamedElement		element;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected CoverageEntryImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return CoveragePackage.Literals.COVERAGE_ENTRY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getExecutions()
	{
		return executions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setExecutions(int newExecutions)
	{
		int oldExecutions = executions;
		executions = newExecutions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoveragePackage.COVERAGE_ENTRY__EXECUTIONS, oldExecutions, executions));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NamedElement getElement()
	{
		if (element != null && element.eIsProxy())
		{
			InternalEObject oldElement = (InternalEObject) element;
			element = (NamedElement) eResolveProxy(oldElement);
			if (element != oldElement)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoveragePackage.COVERAGE_ENTRY__ELEMENT, oldElement, element));
			}
		}
		return element;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NamedElement basicGetElement()
	{
		return element;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setElement(NamedElement newElement)
	{
		NamedElement oldElement = element;
		element = newElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoveragePackage.COVERAGE_ENTRY__ELEMENT, oldElement, element));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case CoveragePackage.COVERAGE_ENTRY__EXECUTIONS:
				return getExecutions();
			case CoveragePackage.COVERAGE_ENTRY__ELEMENT:
				if (resolve)
					return getElement();
				return basicGetElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case CoveragePackage.COVERAGE_ENTRY__EXECUTIONS:
				setExecutions((Integer) newValue);
				return;
			case CoveragePackage.COVERAGE_ENTRY__ELEMENT:
				setElement((NamedElement) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case CoveragePackage.COVERAGE_ENTRY__EXECUTIONS:
				setExecutions(EXECUTIONS_EDEFAULT);
				return;
			case CoveragePackage.COVERAGE_ENTRY__ELEMENT:
				setElement((NamedElement) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case CoveragePackage.COVERAGE_ENTRY__EXECUTIONS:
				return executions != EXECUTIONS_EDEFAULT;
			case CoveragePackage.COVERAGE_ENTRY__ELEMENT:
				return element != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (executions: ");
		result.append(executions);
		result.append(')');
		return result.toString();
	}

} // CoverageEntryImpl
