/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.mdelab.sdm.interpreter.sde.coverage.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport;
import de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry;
import de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Activity Coverage Report</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link de.mdelab.sdm.interpreter.sde.coverage.impl.ActivityCoverageReportImpl#getCoverageEntries
 * <em>Coverage Entries</em>}</li>
 * <li>
 * {@link de.mdelab.sdm.interpreter.sde.coverage.impl.ActivityCoverageReportImpl#getExecutions
 * <em>Executions</em>}</li>
 * <li>
 * {@link de.mdelab.sdm.interpreter.sde.coverage.impl.ActivityCoverageReportImpl#getActivity
 * <em>Activity</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ActivityCoverageReportImpl extends EObjectImpl implements ActivityCoverageReport
{
	/**
	 * The cached value of the '{@link #getCoverageEntries()
	 * <em>Coverage Entries</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCoverageEntries()
	 * @generated
	 * @ordered
	 */
	protected EList<CoverageEntry>	coverageEntries;

	/**
	 * The default value of the '{@link #getExecutions() <em>Executions</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getExecutions()
	 * @generated
	 * @ordered
	 */
	protected static final int		EXECUTIONS_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getExecutions() <em>Executions</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getExecutions()
	 * @generated
	 * @ordered
	 */
	protected int					executions			= EXECUTIONS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getActivity() <em>Activity</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getActivity()
	 * @generated
	 * @ordered
	 */
	protected Activity				activity;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ActivityCoverageReportImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return CoveragePackage.Literals.ACTIVITY_COVERAGE_REPORT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<CoverageEntry> getCoverageEntries()
	{
		if (coverageEntries == null)
		{
			coverageEntries = new EObjectContainmentEList<CoverageEntry>(CoverageEntry.class, this,
					CoveragePackage.ACTIVITY_COVERAGE_REPORT__COVERAGE_ENTRIES);
		}
		return coverageEntries;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getExecutions()
	{
		return executions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setExecutions(int newExecutions)
	{
		int oldExecutions = executions;
		executions = newExecutions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoveragePackage.ACTIVITY_COVERAGE_REPORT__EXECUTIONS, oldExecutions,
					executions));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Activity getActivity()
	{
		if (activity != null && activity.eIsProxy())
		{
			InternalEObject oldActivity = (InternalEObject) activity;
			activity = (Activity) eResolveProxy(oldActivity);
			if (activity != oldActivity)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoveragePackage.ACTIVITY_COVERAGE_REPORT__ACTIVITY,
							oldActivity, activity));
			}
		}
		return activity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Activity basicGetActivity()
	{
		return activity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setActivity(Activity newActivity)
	{
		Activity oldActivity = activity;
		activity = newActivity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoveragePackage.ACTIVITY_COVERAGE_REPORT__ACTIVITY, oldActivity, activity));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__COVERAGE_ENTRIES:
				return ((InternalEList<?>) getCoverageEntries()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__COVERAGE_ENTRIES:
				return getCoverageEntries();
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__EXECUTIONS:
				return getExecutions();
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__ACTIVITY:
				if (resolve)
					return getActivity();
				return basicGetActivity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__COVERAGE_ENTRIES:
				getCoverageEntries().clear();
				getCoverageEntries().addAll((Collection<? extends CoverageEntry>) newValue);
				return;
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__EXECUTIONS:
				setExecutions((Integer) newValue);
				return;
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__ACTIVITY:
				setActivity((Activity) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__COVERAGE_ENTRIES:
				getCoverageEntries().clear();
				return;
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__EXECUTIONS:
				setExecutions(EXECUTIONS_EDEFAULT);
				return;
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__ACTIVITY:
				setActivity((Activity) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__COVERAGE_ENTRIES:
				return coverageEntries != null && !coverageEntries.isEmpty();
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__EXECUTIONS:
				return executions != EXECUTIONS_EDEFAULT;
			case CoveragePackage.ACTIVITY_COVERAGE_REPORT__ACTIVITY:
				return activity != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (executions: ");
		result.append(executions);
		result.append(')');
		return result.toString();
	}

} // ActivityCoverageReportImpl
