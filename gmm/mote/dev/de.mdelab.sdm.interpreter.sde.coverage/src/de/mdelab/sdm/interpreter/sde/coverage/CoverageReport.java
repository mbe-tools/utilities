/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.mdelab.sdm.interpreter.sde.coverage;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Report</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link de.mdelab.sdm.interpreter.sde.coverage.CoverageReport#getActivityCoverageReports
 * <em>Activity Coverage Reports</em>}</li>
 * </ul>
 * </p>
 * 
 * @see de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage#getCoverageReport()
 * @model
 * @generated
 */
public interface CoverageReport extends EObject
{
	/**
	 * Returns the value of the '<em><b>Activity Coverage Reports</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity Coverage Reports</em>' containment
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Activity Coverage Reports</em>' containment
	 *         reference list.
	 * @see de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage#getCoverageReport_ActivityCoverageReports()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActivityCoverageReport> getActivityCoverageReports();

} // CoverageReport
