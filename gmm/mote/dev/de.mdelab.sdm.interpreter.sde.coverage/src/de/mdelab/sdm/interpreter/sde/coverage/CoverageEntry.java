/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.mdelab.sdm.interpreter.sde.coverage;

import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.storyDiagramEcore.NamedElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Entry</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry#getExecutions
 * <em>Executions</em>}</li>
 * <li>{@link de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry#getElement
 * <em>Element</em>}</li>
 * </ul>
 * </p>
 * 
 * @see de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage#getCoverageEntry()
 * @model
 * @generated
 */
public interface CoverageEntry extends EObject
{
	/**
	 * Returns the value of the '<em><b>Executions</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Executions</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Executions</em>' attribute.
	 * @see #setExecutions(int)
	 * @see de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage#getCoverageEntry_Executions()
	 * @model
	 * @generated
	 */
	int getExecutions();

	/**
	 * Sets the value of the '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry#getExecutions
	 * <em>Executions</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Executions</em>' attribute.
	 * @see #getExecutions()
	 * @generated
	 */
	void setExecutions(int value);

	/**
	 * Returns the value of the '<em><b>Element</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Element</em>' reference.
	 * @see #setElement(NamedElement)
	 * @see de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage#getCoverageEntry_Element()
	 * @model required="true"
	 * @generated
	 */
	NamedElement getElement();

	/**
	 * Sets the value of the '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry#getElement
	 * <em>Element</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Element</em>' reference.
	 * @see #getElement()
	 * @generated
	 */
	void setElement(NamedElement value);

} // CoverageEntry
