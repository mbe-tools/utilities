/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.mdelab.sdm.interpreter.sde.coverage;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.storyDiagramEcore.Activity;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Activity Coverage Report</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport#getCoverageEntries
 * <em>Coverage Entries</em>}</li>
 * <li>
 * {@link de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport#getExecutions
 * <em>Executions</em>}</li>
 * <li>
 * {@link de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport#getActivity
 * <em>Activity</em>}</li>
 * </ul>
 * </p>
 * 
 * @see de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage#getActivityCoverageReport()
 * @model
 * @generated
 */
public interface ActivityCoverageReport extends EObject
{
	/**
	 * Returns the value of the '<em><b>Coverage Entries</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coverage Entries</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Coverage Entries</em>' containment
	 *         reference list.
	 * @see de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage#getActivityCoverageReport_CoverageEntries()
	 * @model containment="true"
	 * @generated
	 */
	EList<CoverageEntry> getCoverageEntries();

	/**
	 * Returns the value of the '<em><b>Executions</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Executions</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Executions</em>' attribute.
	 * @see #setExecutions(int)
	 * @see de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage#getActivityCoverageReport_Executions()
	 * @model
	 * @generated
	 */
	int getExecutions();

	/**
	 * Sets the value of the '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport#getExecutions
	 * <em>Executions</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Executions</em>' attribute.
	 * @see #getExecutions()
	 * @generated
	 */
	void setExecutions(int value);

	/**
	 * Returns the value of the '<em><b>Activity</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Activity</em>' reference.
	 * @see #setActivity(Activity)
	 * @see de.mdelab.sdm.interpreter.sde.coverage.CoveragePackage#getActivityCoverageReport_Activity()
	 * @model required="true"
	 * @generated
	 */
	Activity getActivity();

	/**
	 * Sets the value of the '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport#getActivity
	 * <em>Activity</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Activity</em>' reference.
	 * @see #getActivity()
	 * @generated
	 */
	void setActivity(Activity value);

} // ActivityCoverageReport
