/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.mdelab.sdm.interpreter.sde.coverage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see de.mdelab.sdm.interpreter.sde.coverage.CoverageFactory
 * @model kind="package"
 * @generated
 */
public interface CoveragePackage extends EPackage
{
	/**
	 * The package name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String			eNAME										= "coverage";

	/**
	 * The package namespace URI. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String			eNS_URI										= "http://www.mdelab.de/coverage/1.0";

	/**
	 * The package namespace name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String			eNS_PREFIX									= "coverage";

	/**
	 * The singleton instance of the package. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	CoveragePackage	eINSTANCE									= de.mdelab.sdm.interpreter.sde.coverage.impl.CoveragePackageImpl.init();

	/**
	 * The meta object id for the '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.impl.CoverageReportImpl
	 * <em>Report</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see de.mdelab.sdm.interpreter.sde.coverage.impl.CoverageReportImpl
	 * @see de.mdelab.sdm.interpreter.sde.coverage.impl.CoveragePackageImpl#getCoverageReport()
	 * @generated
	 */
	int				COVERAGE_REPORT								= 0;

	/**
	 * The feature id for the '<em><b>Activity Coverage Reports</b></em>'
	 * containment reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COVERAGE_REPORT__ACTIVITY_COVERAGE_REPORTS	= 0;

	/**
	 * The number of structural features of the '<em>Report</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COVERAGE_REPORT_FEATURE_COUNT				= 1;

	/**
	 * The meta object id for the '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.impl.CoverageEntryImpl
	 * <em>Entry</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see de.mdelab.sdm.interpreter.sde.coverage.impl.CoverageEntryImpl
	 * @see de.mdelab.sdm.interpreter.sde.coverage.impl.CoveragePackageImpl#getCoverageEntry()
	 * @generated
	 */
	int				COVERAGE_ENTRY								= 1;

	/**
	 * The feature id for the '<em><b>Executions</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COVERAGE_ENTRY__EXECUTIONS					= 0;

	/**
	 * The feature id for the '<em><b>Element</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COVERAGE_ENTRY__ELEMENT						= 1;

	/**
	 * The number of structural features of the '<em>Entry</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COVERAGE_ENTRY_FEATURE_COUNT				= 2;

	/**
	 * The meta object id for the '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.impl.ActivityCoverageReportImpl
	 * <em>Activity Coverage Report</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see de.mdelab.sdm.interpreter.sde.coverage.impl.ActivityCoverageReportImpl
	 * @see de.mdelab.sdm.interpreter.sde.coverage.impl.CoveragePackageImpl#getActivityCoverageReport()
	 * @generated
	 */
	int				ACTIVITY_COVERAGE_REPORT					= 2;

	/**
	 * The feature id for the '<em><b>Coverage Entries</b></em>' containment
	 * reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ACTIVITY_COVERAGE_REPORT__COVERAGE_ENTRIES	= 0;

	/**
	 * The feature id for the '<em><b>Executions</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ACTIVITY_COVERAGE_REPORT__EXECUTIONS		= 1;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ACTIVITY_COVERAGE_REPORT__ACTIVITY			= 2;

	/**
	 * The number of structural features of the '
	 * <em>Activity Coverage Report</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ACTIVITY_COVERAGE_REPORT_FEATURE_COUNT		= 3;

	/**
	 * Returns the meta object for class '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.CoverageReport
	 * <em>Report</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Report</em>'.
	 * @see de.mdelab.sdm.interpreter.sde.coverage.CoverageReport
	 * @generated
	 */
	EClass getCoverageReport();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.CoverageReport#getActivityCoverageReports
	 * <em>Activity Coverage Reports</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '
	 *         <em>Activity Coverage Reports</em>'.
	 * @see de.mdelab.sdm.interpreter.sde.coverage.CoverageReport#getActivityCoverageReports()
	 * @see #getCoverageReport()
	 * @generated
	 */
	EReference getCoverageReport_ActivityCoverageReports();

	/**
	 * Returns the meta object for class '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry
	 * <em>Entry</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Entry</em>'.
	 * @see de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry
	 * @generated
	 */
	EClass getCoverageEntry();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry#getExecutions
	 * <em>Executions</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Executions</em>'.
	 * @see de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry#getExecutions()
	 * @see #getCoverageEntry()
	 * @generated
	 */
	EAttribute getCoverageEntry_Executions();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry#getElement
	 * <em>Element</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Element</em>'.
	 * @see de.mdelab.sdm.interpreter.sde.coverage.CoverageEntry#getElement()
	 * @see #getCoverageEntry()
	 * @generated
	 */
	EReference getCoverageEntry_Element();

	/**
	 * Returns the meta object for class '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport
	 * <em>Activity Coverage Report</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Activity Coverage Report</em>'.
	 * @see de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport
	 * @generated
	 */
	EClass getActivityCoverageReport();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport#getCoverageEntries
	 * <em>Coverage Entries</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the containment reference list '
	 *         <em>Coverage Entries</em>'.
	 * @see de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport#getCoverageEntries()
	 * @see #getActivityCoverageReport()
	 * @generated
	 */
	EReference getActivityCoverageReport_CoverageEntries();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport#getExecutions
	 * <em>Executions</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Executions</em>'.
	 * @see de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport#getExecutions()
	 * @see #getActivityCoverageReport()
	 * @generated
	 */
	EAttribute getActivityCoverageReport_Executions();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport#getActivity
	 * <em>Activity</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Activity</em>'.
	 * @see de.mdelab.sdm.interpreter.sde.coverage.ActivityCoverageReport#getActivity()
	 * @see #getActivityCoverageReport()
	 * @generated
	 */
	EReference getActivityCoverageReport_Activity();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoverageFactory getCoverageFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	interface Literals
	{
		/**
		 * The meta object literal for the '
		 * {@link de.mdelab.sdm.interpreter.sde.coverage.impl.CoverageReportImpl
		 * <em>Report</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see de.mdelab.sdm.interpreter.sde.coverage.impl.CoverageReportImpl
		 * @see de.mdelab.sdm.interpreter.sde.coverage.impl.CoveragePackageImpl#getCoverageReport()
		 * @generated
		 */
		EClass		COVERAGE_REPORT								= eINSTANCE.getCoverageReport();

		/**
		 * The meta object literal for the '
		 * <em><b>Activity Coverage Reports</b></em>' containment reference list
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	COVERAGE_REPORT__ACTIVITY_COVERAGE_REPORTS	= eINSTANCE.getCoverageReport_ActivityCoverageReports();

		/**
		 * The meta object literal for the '
		 * {@link de.mdelab.sdm.interpreter.sde.coverage.impl.CoverageEntryImpl
		 * <em>Entry</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see de.mdelab.sdm.interpreter.sde.coverage.impl.CoverageEntryImpl
		 * @see de.mdelab.sdm.interpreter.sde.coverage.impl.CoveragePackageImpl#getCoverageEntry()
		 * @generated
		 */
		EClass		COVERAGE_ENTRY								= eINSTANCE.getCoverageEntry();

		/**
		 * The meta object literal for the '<em><b>Executions</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	COVERAGE_ENTRY__EXECUTIONS					= eINSTANCE.getCoverageEntry_Executions();

		/**
		 * The meta object literal for the '<em><b>Element</b></em>' reference
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	COVERAGE_ENTRY__ELEMENT						= eINSTANCE.getCoverageEntry_Element();

		/**
		 * The meta object literal for the '
		 * {@link de.mdelab.sdm.interpreter.sde.coverage.impl.ActivityCoverageReportImpl
		 * <em>Activity Coverage Report</em>}' class. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see de.mdelab.sdm.interpreter.sde.coverage.impl.ActivityCoverageReportImpl
		 * @see de.mdelab.sdm.interpreter.sde.coverage.impl.CoveragePackageImpl#getActivityCoverageReport()
		 * @generated
		 */
		EClass		ACTIVITY_COVERAGE_REPORT					= eINSTANCE.getActivityCoverageReport();

		/**
		 * The meta object literal for the '<em><b>Coverage Entries</b></em>'
		 * containment reference list feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ACTIVITY_COVERAGE_REPORT__COVERAGE_ENTRIES	= eINSTANCE.getActivityCoverageReport_CoverageEntries();

		/**
		 * The meta object literal for the '<em><b>Executions</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ACTIVITY_COVERAGE_REPORT__EXECUTIONS		= eINSTANCE.getActivityCoverageReport_Executions();

		/**
		 * The meta object literal for the '<em><b>Activity</b></em>' reference
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ACTIVITY_COVERAGE_REPORT__ACTIVITY			= eINSTANCE.getActivityCoverageReport_Activity();

	}

} // CoveragePackage
