/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import de.mdelab.workflow.components.ComponentsPackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsFactory
 * @model kind="package"
 * @generated
 */
public interface WorkflowComponentsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workflowComponents";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mote/workflowComponents/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mote.workflowComponents";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	WorkflowComponentsPackage eINSTANCE = de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.workflowComponents.impl.ModelGeneratorImpl <em>Model Generator</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.mote.workflowComponents.impl.ModelGeneratorImpl
	 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getModelGenerator()
	 * @generated
	 */
	int MODEL_GENERATOR = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_GENERATOR__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_GENERATOR__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Axiom Generator Activity</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_GENERATOR__AXIOM_GENERATOR_ACTIVITY = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rule Generator Activities</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_GENERATOR__RULE_GENERATOR_ACTIVITIES = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Correspondence Model Size</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_GENERATOR__CORRESPONDENCE_MODEL_SIZE = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Left Model Slot</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_GENERATOR__LEFT_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Right Model Slot</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_GENERATOR__RIGHT_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Left Model URI</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_GENERATOR__LEFT_MODEL_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Right Model URI</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_GENERATOR__RIGHT_MODEL_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Corr Node Type Restrictions</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_GENERATOR__CORR_NODE_TYPE_RESTRICTIONS = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Model Generator</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_GENERATOR_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.workflowComponents.impl.GeneratorActivityImpl <em>Generator Activity</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.mote.workflowComponents.impl.GeneratorActivityImpl
	 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getGeneratorActivity()
	 * @generated
	 */
	int GENERATOR_ACTIVITY = 1;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int GENERATOR_ACTIVITY__ACTIVITY = 0;

	/**
	 * The feature id for the '<em><b>Rule Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_ACTIVITY__RULE_PARAMETERS = 1;

	/**
	 * The feature id for the '<em><b>Parent Corr Node Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_ACTIVITY__PARENT_CORR_NODE_PARAMETERS = 2;

	/**
	 * The feature id for the '<em><b>Created Corr Node Type</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_ACTIVITY__CREATED_CORR_NODE_TYPE = 3;

	/**
	 * The number of structural features of the '<em>Generator Activity</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_ACTIVITY_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.workflowComponents.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.workflowComponents.impl.ParameterImpl
	 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int PARAMETER__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int PARAMETER__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Base Value</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int PARAMETER__BASE_VALUE = 2;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.workflowComponents.impl.CorrNodeTypeRestrictionImpl <em>Corr Node Type Restriction</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.mote.workflowComponents.impl.CorrNodeTypeRestrictionImpl
	 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getCorrNodeTypeRestriction()
	 * @generated
	 */
	int CORR_NODE_TYPE_RESTRICTION = 3;

	/**
	 * The feature id for the '<em><b>Corr Node Type</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORR_NODE_TYPE_RESTRICTION__CORR_NODE_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Max Number</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORR_NODE_TYPE_RESTRICTION__MAX_NUMBER = 1;

	/**
	 * The feature id for the '<em><b>Max Child Nodes</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORR_NODE_TYPE_RESTRICTION__MAX_CHILD_NODES = 2;

	/**
	 * The number of structural features of the '<em>Corr Node Type Restriction</em>' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_NODE_TYPE_RESTRICTION_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.workflowComponents.impl.MoteTransformerImpl <em>Mote Transformer</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.mote.workflowComponents.impl.MoteTransformerImpl
	 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getMoteTransformer()
	 * @generated
	 */
	int MOTE_TRANSFORMER = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Source Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER__SOURCE_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER__TARGET_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Rule Set ID</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER__RULE_SET_ID = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Synchronize</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER__SYNCHRONIZE = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Transformation Direction</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER__TRANSFORMATION_DIRECTION = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Tgg Engine Slot Name</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER__TGG_ENGINE_SLOT_NAME = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Mote Transformer</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.workflowComponents.impl.ModelComparerImpl <em>Model Comparer</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.mote.workflowComponents.impl.ModelComparerImpl
	 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getModelComparer()
	 * @generated
	 */
	int MODEL_COMPARER = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_COMPARER__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_COMPARER__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Model Slot1</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_COMPARER__MODEL_SLOT1 = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Model Slot2</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_COMPARER__MODEL_SLOT2 = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Comparison Result Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_COMPARER__COMPARISON_RESULT_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Diff Model Slot</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_COMPARER__DIFF_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Model Comparer</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_COMPARER_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.workflowComponents.impl.MoteTransformerMappingImpl <em>Mote Transformer Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.workflowComponents.impl.MoteTransformerMappingImpl
	 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getMoteTransformerMapping()
	 * @generated
	 */
	int MOTE_TRANSFORMER_MAPPING = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER_MAPPING__NAME = MOTE_TRANSFORMER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER_MAPPING__DESCRIPTION = MOTE_TRANSFORMER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Source Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER_MAPPING__SOURCE_MODEL_SLOT = MOTE_TRANSFORMER__SOURCE_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Target Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER_MAPPING__TARGET_MODEL_SLOT = MOTE_TRANSFORMER__TARGET_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Rule Set ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER_MAPPING__RULE_SET_ID = MOTE_TRANSFORMER__RULE_SET_ID;

	/**
	 * The feature id for the '<em><b>Synchronize</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER_MAPPING__SYNCHRONIZE = MOTE_TRANSFORMER__SYNCHRONIZE;

	/**
	 * The feature id for the '<em><b>Transformation Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER_MAPPING__TRANSFORMATION_DIRECTION = MOTE_TRANSFORMER__TRANSFORMATION_DIRECTION;

	/**
	 * The feature id for the '<em><b>Tgg Engine Slot Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER_MAPPING__TGG_ENGINE_SLOT_NAME = MOTE_TRANSFORMER__TGG_ENGINE_SLOT_NAME;

	/**
	 * The feature id for the '<em><b>Correspondence Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER_MAPPING__CORRESPONDENCE_MODEL_SLOT = MOTE_TRANSFORMER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Mote Transformer Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_TRANSFORMER_MAPPING_FEATURE_COUNT = MOTE_TRANSFORMER_FEATURE_COUNT + 1;

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.workflowComponents.ModelGenerator <em>Model Generator</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Generator</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelGenerator
	 * @generated
	 */
	EClass getModelGenerator();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getAxiomGeneratorActivity <em>Axiom Generator Activity</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference '<em>Axiom Generator Activity</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelGenerator#getAxiomGeneratorActivity()
	 * @see #getModelGenerator()
	 * @generated
	 */
	EReference getModelGenerator_AxiomGeneratorActivity();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getRuleGeneratorActivities <em>Rule Generator Activities</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rule Generator Activities</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelGenerator#getRuleGeneratorActivities()
	 * @see #getModelGenerator()
	 * @generated
	 */
	EReference getModelGenerator_RuleGeneratorActivities();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getCorrespondenceModelSize <em>Correspondence Model Size</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Correspondence Model Size</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelGenerator#getCorrespondenceModelSize()
	 * @see #getModelGenerator()
	 * @generated
	 */
	EAttribute getModelGenerator_CorrespondenceModelSize();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getLeftModelSlot <em>Left Model Slot</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Left Model Slot</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelGenerator#getLeftModelSlot()
	 * @see #getModelGenerator()
	 * @generated
	 */
	EAttribute getModelGenerator_LeftModelSlot();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getRightModelSlot
	 * <em>Right Model Slot</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Right Model Slot</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelGenerator#getRightModelSlot()
	 * @see #getModelGenerator()
	 * @generated
	 */
	EAttribute getModelGenerator_RightModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getLeftModelURI <em>Left Model URI</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Left Model URI</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelGenerator#getLeftModelURI()
	 * @see #getModelGenerator()
	 * @generated
	 */
	EAttribute getModelGenerator_LeftModelURI();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getRightModelURI <em>Right Model URI</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Right Model URI</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelGenerator#getRightModelURI()
	 * @see #getModelGenerator()
	 * @generated
	 */
	EAttribute getModelGenerator_RightModelURI();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getCorrNodeTypeRestrictions <em>Corr Node Type Restrictions</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Corr Node Type Restrictions</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelGenerator#getCorrNodeTypeRestrictions()
	 * @see #getModelGenerator()
	 * @generated
	 */
	EReference getModelGenerator_CorrNodeTypeRestrictions();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.mote.workflowComponents.GeneratorActivity
	 * <em>Generator Activity</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Generator Activity</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.GeneratorActivity
	 * @generated
	 */
	EClass getGeneratorActivity();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.workflowComponents.GeneratorActivity#getActivity <em>Activity</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Activity</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.GeneratorActivity#getActivity()
	 * @see #getGeneratorActivity()
	 * @generated
	 */
	EReference getGeneratorActivity_Activity();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.mote.workflowComponents.GeneratorActivity#getRuleParameters <em>Rule Parameters</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rule Parameters</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.GeneratorActivity#getRuleParameters()
	 * @see #getGeneratorActivity()
	 * @generated
	 */
	EReference getGeneratorActivity_RuleParameters();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.mote.workflowComponents.GeneratorActivity#getParentCorrNodeParameters <em>Parent Corr Node Parameters</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parent Corr Node Parameters</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.GeneratorActivity#getParentCorrNodeParameters()
	 * @see #getGeneratorActivity()
	 * @generated
	 */
	EReference getGeneratorActivity_ParentCorrNodeParameters();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.workflowComponents.GeneratorActivity#getCreatedCorrNodeType <em>Created Corr Node Type</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference '<em>Created Corr Node Type</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.GeneratorActivity#getCreatedCorrNodeType()
	 * @see #getGeneratorActivity()
	 * @generated
	 */
	EReference getGeneratorActivity_CreatedCorrNodeType();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.workflowComponents.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.Parameter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.Parameter#getName()
	 * @see #getParameter()
	 * @generated
	 */
	EAttribute getParameter_Name();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.workflowComponents.Parameter#getType <em>Type</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.Parameter#getType()
	 * @see #getParameter()
	 * @generated
	 */
	EReference getParameter_Type();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.Parameter#getBaseValue <em>Base Value</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base Value</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.Parameter#getBaseValue()
	 * @see #getParameter()
	 * @generated
	 */
	EAttribute getParameter_BaseValue();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction <em>Corr Node Type Restriction</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Corr Node Type Restriction</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction
	 * @generated
	 */
	EClass getCorrNodeTypeRestriction();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction#getCorrNodeType <em>Corr Node Type</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Corr Node Type</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction#getCorrNodeType()
	 * @see #getCorrNodeTypeRestriction()
	 * @generated
	 */
	EReference getCorrNodeTypeRestriction_CorrNodeType();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction#getMaxNumber <em>Max Number</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Number</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction#getMaxNumber()
	 * @see #getCorrNodeTypeRestriction()
	 * @generated
	 */
	EAttribute getCorrNodeTypeRestriction_MaxNumber();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction#getMaxChildNodes <em>Max Child Nodes</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Child Nodes</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction#getMaxChildNodes()
	 * @see #getCorrNodeTypeRestriction()
	 * @generated
	 */
	EAttribute getCorrNodeTypeRestriction_MaxChildNodes();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer
	 * <em>Mote Transformer</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Mote Transformer</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.MoteTransformer
	 * @generated
	 */
	EClass getMoteTransformer();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getSourceModelSlot
	 * <em>Source Model Slot</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Source Model Slot</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.MoteTransformer#getSourceModelSlot()
	 * @see #getMoteTransformer()
	 * @generated
	 */
	EAttribute getMoteTransformer_SourceModelSlot();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getTargetModelSlot
	 * <em>Target Model Slot</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Target Model Slot</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.MoteTransformer#getTargetModelSlot()
	 * @see #getMoteTransformer()
	 * @generated
	 */
	EAttribute getMoteTransformer_TargetModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getRuleSetID <em>Rule Set ID</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rule Set ID</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.MoteTransformer#getRuleSetID()
	 * @see #getMoteTransformer()
	 * @generated
	 */
	EAttribute getMoteTransformer_RuleSetID();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getSynchronize <em>Synchronize</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Synchronize</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.MoteTransformer#getSynchronize()
	 * @see #getMoteTransformer()
	 * @generated
	 */
	EAttribute getMoteTransformer_Synchronize();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getTransformationDirection <em>Transformation Direction</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Transformation Direction</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.MoteTransformer#getTransformationDirection()
	 * @see #getMoteTransformer()
	 * @generated
	 */
	EAttribute getMoteTransformer_TransformationDirection();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getTggEngineSlotName <em>Tgg Engine Slot Name</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Tgg Engine Slot Name</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.MoteTransformer#getTggEngineSlotName()
	 * @see #getMoteTransformer()
	 * @generated
	 */
	EAttribute getMoteTransformer_TggEngineSlotName();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.workflowComponents.ModelComparer <em>Model Comparer</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Comparer</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelComparer
	 * @generated
	 */
	EClass getModelComparer();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.ModelComparer#getModelSlot1 <em>Model Slot1</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Slot1</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelComparer#getModelSlot1()
	 * @see #getModelComparer()
	 * @generated
	 */
	EAttribute getModelComparer_ModelSlot1();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.ModelComparer#getModelSlot2 <em>Model Slot2</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Slot2</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelComparer#getModelSlot2()
	 * @see #getModelComparer()
	 * @generated
	 */
	EAttribute getModelComparer_ModelSlot2();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.ModelComparer#getComparisonResultModelSlot <em>Comparison Result Model Slot</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Comparison Result Model Slot</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelComparer#getComparisonResultModelSlot()
	 * @see #getModelComparer()
	 * @generated
	 */
	EAttribute getModelComparer_ComparisonResultModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.ModelComparer#getDiffModelSlot <em>Diff Model Slot</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Diff Model Slot</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.ModelComparer#getDiffModelSlot()
	 * @see #getModelComparer()
	 * @generated
	 */
	EAttribute getModelComparer_DiffModelSlot();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.workflowComponents.MoteTransformerMapping <em>Mote Transformer Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mote Transformer Mapping</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.MoteTransformerMapping
	 * @generated
	 */
	EClass getMoteTransformerMapping();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.workflowComponents.MoteTransformerMapping#getCorrespondenceModelSlot <em>Correspondence Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Correspondence Model Slot</em>'.
	 * @see de.hpi.sam.mote.workflowComponents.MoteTransformerMapping#getCorrespondenceModelSlot()
	 * @see #getMoteTransformerMapping()
	 * @generated
	 */
	EAttribute getMoteTransformerMapping_CorrespondenceModelSlot();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkflowComponentsFactory getWorkflowComponentsFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.workflowComponents.impl.ModelGeneratorImpl <em>Model Generator</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.mote.workflowComponents.impl.ModelGeneratorImpl
		 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getModelGenerator()
		 * @generated
		 */
		EClass MODEL_GENERATOR = eINSTANCE.getModelGenerator();

		/**
		 * The meta object literal for the '<em><b>Axiom Generator Activity</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_GENERATOR__AXIOM_GENERATOR_ACTIVITY = eINSTANCE
				.getModelGenerator_AxiomGeneratorActivity();

		/**
		 * The meta object literal for the '<em><b>Rule Generator Activities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_GENERATOR__RULE_GENERATOR_ACTIVITIES = eINSTANCE
				.getModelGenerator_RuleGeneratorActivities();

		/**
		 * The meta object literal for the '
		 * <em><b>Correspondence Model Size</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute MODEL_GENERATOR__CORRESPONDENCE_MODEL_SIZE = eINSTANCE
				.getModelGenerator_CorrespondenceModelSize();

		/**
		 * The meta object literal for the '<em><b>Left Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_GENERATOR__LEFT_MODEL_SLOT = eINSTANCE
				.getModelGenerator_LeftModelSlot();

		/**
		 * The meta object literal for the '<em><b>Right Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_GENERATOR__RIGHT_MODEL_SLOT = eINSTANCE
				.getModelGenerator_RightModelSlot();

		/**
		 * The meta object literal for the '<em><b>Left Model URI</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_GENERATOR__LEFT_MODEL_URI = eINSTANCE
				.getModelGenerator_LeftModelURI();

		/**
		 * The meta object literal for the '<em><b>Right Model URI</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_GENERATOR__RIGHT_MODEL_URI = eINSTANCE
				.getModelGenerator_RightModelURI();

		/**
		 * The meta object literal for the '<em><b>Corr Node Type Restrictions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_GENERATOR__CORR_NODE_TYPE_RESTRICTIONS = eINSTANCE
				.getModelGenerator_CorrNodeTypeRestrictions();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.workflowComponents.impl.GeneratorActivityImpl <em>Generator Activity</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.mote.workflowComponents.impl.GeneratorActivityImpl
		 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getGeneratorActivity()
		 * @generated
		 */
		EClass GENERATOR_ACTIVITY = eINSTANCE.getGeneratorActivity();

		/**
		 * The meta object literal for the '<em><b>Activity</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERATOR_ACTIVITY__ACTIVITY = eINSTANCE
				.getGeneratorActivity_Activity();

		/**
		 * The meta object literal for the '<em><b>Rule Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference GENERATOR_ACTIVITY__RULE_PARAMETERS = eINSTANCE
				.getGeneratorActivity_RuleParameters();

		/**
		 * The meta object literal for the '<em><b>Parent Corr Node Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERATOR_ACTIVITY__PARENT_CORR_NODE_PARAMETERS = eINSTANCE
				.getGeneratorActivity_ParentCorrNodeParameters();

		/**
		 * The meta object literal for the '
		 * <em><b>Created Corr Node Type</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference GENERATOR_ACTIVITY__CREATED_CORR_NODE_TYPE = eINSTANCE
				.getGeneratorActivity_CreatedCorrNodeType();

		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.mote.workflowComponents.impl.ParameterImpl
		 * <em>Parameter</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see de.hpi.sam.mote.workflowComponents.impl.ParameterImpl
		 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER__NAME = eINSTANCE.getParameter_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER__TYPE = eINSTANCE.getParameter_Type();

		/**
		 * The meta object literal for the '<em><b>Base Value</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER__BASE_VALUE = eINSTANCE.getParameter_BaseValue();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.workflowComponents.impl.CorrNodeTypeRestrictionImpl <em>Corr Node Type Restriction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.workflowComponents.impl.CorrNodeTypeRestrictionImpl
		 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getCorrNodeTypeRestriction()
		 * @generated
		 */
		EClass CORR_NODE_TYPE_RESTRICTION = eINSTANCE
				.getCorrNodeTypeRestriction();

		/**
		 * The meta object literal for the '<em><b>Corr Node Type</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference CORR_NODE_TYPE_RESTRICTION__CORR_NODE_TYPE = eINSTANCE
				.getCorrNodeTypeRestriction_CorrNodeType();

		/**
		 * The meta object literal for the '<em><b>Max Number</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CORR_NODE_TYPE_RESTRICTION__MAX_NUMBER = eINSTANCE
				.getCorrNodeTypeRestriction_MaxNumber();

		/**
		 * The meta object literal for the '<em><b>Max Child Nodes</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CORR_NODE_TYPE_RESTRICTION__MAX_CHILD_NODES = eINSTANCE
				.getCorrNodeTypeRestriction_MaxChildNodes();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.workflowComponents.impl.MoteTransformerImpl <em>Mote Transformer</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.mote.workflowComponents.impl.MoteTransformerImpl
		 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getMoteTransformer()
		 * @generated
		 */
		EClass MOTE_TRANSFORMER = eINSTANCE.getMoteTransformer();

		/**
		 * The meta object literal for the '<em><b>Source Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOTE_TRANSFORMER__SOURCE_MODEL_SLOT = eINSTANCE
				.getMoteTransformer_SourceModelSlot();

		/**
		 * The meta object literal for the '<em><b>Target Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOTE_TRANSFORMER__TARGET_MODEL_SLOT = eINSTANCE
				.getMoteTransformer_TargetModelSlot();

		/**
		 * The meta object literal for the '<em><b>Rule Set ID</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOTE_TRANSFORMER__RULE_SET_ID = eINSTANCE
				.getMoteTransformer_RuleSetID();

		/**
		 * The meta object literal for the '<em><b>Synchronize</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOTE_TRANSFORMER__SYNCHRONIZE = eINSTANCE
				.getMoteTransformer_Synchronize();

		/**
		 * The meta object literal for the '
		 * <em><b>Transformation Direction</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute MOTE_TRANSFORMER__TRANSFORMATION_DIRECTION = eINSTANCE
				.getMoteTransformer_TransformationDirection();

		/**
		 * The meta object literal for the '<em><b>Tgg Engine Slot Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOTE_TRANSFORMER__TGG_ENGINE_SLOT_NAME = eINSTANCE
				.getMoteTransformer_TggEngineSlotName();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.workflowComponents.impl.ModelComparerImpl <em>Model Comparer</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.mote.workflowComponents.impl.ModelComparerImpl
		 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getModelComparer()
		 * @generated
		 */
		EClass MODEL_COMPARER = eINSTANCE.getModelComparer();

		/**
		 * The meta object literal for the '<em><b>Model Slot1</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_COMPARER__MODEL_SLOT1 = eINSTANCE
				.getModelComparer_ModelSlot1();

		/**
		 * The meta object literal for the '<em><b>Model Slot2</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_COMPARER__MODEL_SLOT2 = eINSTANCE
				.getModelComparer_ModelSlot2();

		/**
		 * The meta object literal for the '
		 * <em><b>Comparison Result Model Slot</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute MODEL_COMPARER__COMPARISON_RESULT_MODEL_SLOT = eINSTANCE
				.getModelComparer_ComparisonResultModelSlot();

		/**
		 * The meta object literal for the '<em><b>Diff Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_COMPARER__DIFF_MODEL_SLOT = eINSTANCE
				.getModelComparer_DiffModelSlot();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.workflowComponents.impl.MoteTransformerMappingImpl <em>Mote Transformer Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.workflowComponents.impl.MoteTransformerMappingImpl
		 * @see de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsPackageImpl#getMoteTransformerMapping()
		 * @generated
		 */
		EClass MOTE_TRANSFORMER_MAPPING = eINSTANCE.getMoteTransformerMapping();

		/**
		 * The meta object literal for the '<em><b>Correspondence Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOTE_TRANSFORMER_MAPPING__CORRESPONDENCE_MODEL_SLOT = eINSTANCE
				.getMoteTransformerMapping_CorrespondenceModelSlot();

	}

} // WorkflowComponentsPackage
