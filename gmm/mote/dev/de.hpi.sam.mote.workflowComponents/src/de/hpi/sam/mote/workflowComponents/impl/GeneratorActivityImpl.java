/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.mote.workflowComponents.GeneratorActivity;
import de.hpi.sam.mote.workflowComponents.Parameter;
import de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage;
import de.hpi.sam.storyDiagramEcore.Activity;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Generator Activity</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.GeneratorActivityImpl#getActivity
 * <em>Activity</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.GeneratorActivityImpl#getRuleParameters
 * <em>Rule Parameters</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.GeneratorActivityImpl#getParentCorrNodeParameters
 * <em>Parent Corr Node Parameters</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.GeneratorActivityImpl#getCreatedCorrNodeType
 * <em>Created Corr Node Type</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class GeneratorActivityImpl extends EObjectImpl implements GeneratorActivity
{
	/**
	 * The cached value of the '{@link #getActivity() <em>Activity</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getActivity()
	 * @generated
	 * @ordered
	 */
	protected Activity			activity;

	/**
	 * The cached value of the '{@link #getRuleParameters()
	 * <em>Rule Parameters</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRuleParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter>	ruleParameters;

	/**
	 * The cached value of the '{@link #getParentCorrNodeParameters()
	 * <em>Parent Corr Node Parameters</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getParentCorrNodeParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter>	parentCorrNodeParameters;

	/**
	 * The cached value of the '{@link #getCreatedCorrNodeType()
	 * <em>Created Corr Node Type</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getCreatedCorrNodeType()
	 * @generated
	 * @ordered
	 */
	protected EClass			createdCorrNodeType;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected GeneratorActivityImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WorkflowComponentsPackage.Literals.GENERATOR_ACTIVITY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Activity getActivity()
	{
		if (activity != null && activity.eIsProxy())
		{
			InternalEObject oldActivity = (InternalEObject) activity;
			activity = (Activity) eResolveProxy(oldActivity);
			if (activity != oldActivity)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WorkflowComponentsPackage.GENERATOR_ACTIVITY__ACTIVITY,
							oldActivity, activity));
			}
		}
		return activity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Activity basicGetActivity()
	{
		return activity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setActivity(Activity newActivity)
	{
		Activity oldActivity = activity;
		activity = newActivity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.GENERATOR_ACTIVITY__ACTIVITY, oldActivity,
					activity));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Parameter> getRuleParameters()
	{
		if (ruleParameters == null)
		{
			ruleParameters = new EObjectContainmentEList<Parameter>(Parameter.class, this,
					WorkflowComponentsPackage.GENERATOR_ACTIVITY__RULE_PARAMETERS);
		}
		return ruleParameters;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Parameter> getParentCorrNodeParameters()
	{
		if (parentCorrNodeParameters == null)
		{
			parentCorrNodeParameters = new EObjectContainmentEList<Parameter>(Parameter.class, this,
					WorkflowComponentsPackage.GENERATOR_ACTIVITY__PARENT_CORR_NODE_PARAMETERS);
		}
		return parentCorrNodeParameters;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getCreatedCorrNodeType()
	{
		if (createdCorrNodeType != null && createdCorrNodeType.eIsProxy())
		{
			InternalEObject oldCreatedCorrNodeType = (InternalEObject) createdCorrNodeType;
			createdCorrNodeType = (EClass) eResolveProxy(oldCreatedCorrNodeType);
			if (createdCorrNodeType != oldCreatedCorrNodeType)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							WorkflowComponentsPackage.GENERATOR_ACTIVITY__CREATED_CORR_NODE_TYPE, oldCreatedCorrNodeType,
							createdCorrNodeType));
			}
		}
		return createdCorrNodeType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass basicGetCreatedCorrNodeType()
	{
		return createdCorrNodeType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setCreatedCorrNodeType(EClass newCreatedCorrNodeType)
	{
		EClass oldCreatedCorrNodeType = createdCorrNodeType;
		createdCorrNodeType = newCreatedCorrNodeType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.GENERATOR_ACTIVITY__CREATED_CORR_NODE_TYPE,
					oldCreatedCorrNodeType, createdCorrNodeType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__RULE_PARAMETERS:
				return ((InternalEList<?>) getRuleParameters()).basicRemove(otherEnd, msgs);
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__PARENT_CORR_NODE_PARAMETERS:
				return ((InternalEList<?>) getParentCorrNodeParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__ACTIVITY:
				if (resolve)
					return getActivity();
				return basicGetActivity();
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__RULE_PARAMETERS:
				return getRuleParameters();
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__PARENT_CORR_NODE_PARAMETERS:
				return getParentCorrNodeParameters();
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__CREATED_CORR_NODE_TYPE:
				if (resolve)
					return getCreatedCorrNodeType();
				return basicGetCreatedCorrNodeType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__ACTIVITY:
				setActivity((Activity) newValue);
				return;
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__RULE_PARAMETERS:
				getRuleParameters().clear();
				getRuleParameters().addAll((Collection<? extends Parameter>) newValue);
				return;
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__PARENT_CORR_NODE_PARAMETERS:
				getParentCorrNodeParameters().clear();
				getParentCorrNodeParameters().addAll((Collection<? extends Parameter>) newValue);
				return;
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__CREATED_CORR_NODE_TYPE:
				setCreatedCorrNodeType((EClass) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__ACTIVITY:
				setActivity((Activity) null);
				return;
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__RULE_PARAMETERS:
				getRuleParameters().clear();
				return;
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__PARENT_CORR_NODE_PARAMETERS:
				getParentCorrNodeParameters().clear();
				return;
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__CREATED_CORR_NODE_TYPE:
				setCreatedCorrNodeType((EClass) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__ACTIVITY:
				return activity != null;
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__RULE_PARAMETERS:
				return ruleParameters != null && !ruleParameters.isEmpty();
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__PARENT_CORR_NODE_PARAMETERS:
				return parentCorrNodeParameters != null && !parentCorrNodeParameters.isEmpty();
			case WorkflowComponentsPackage.GENERATOR_ACTIVITY__CREATED_CORR_NODE_TYPE:
				return createdCorrNodeType != null;
		}
		return super.eIsSet(featureID);
	}

} // GeneratorActivityImpl
