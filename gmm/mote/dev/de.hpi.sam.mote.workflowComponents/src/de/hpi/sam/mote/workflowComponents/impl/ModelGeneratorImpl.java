/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction;
import de.hpi.sam.mote.workflowComponents.GeneratorActivity;
import de.hpi.sam.mote.workflowComponents.ModelGenerator;
import de.hpi.sam.mote.workflowComponents.Parameter;
import de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage;
import de.hpi.sam.storyDiagramEcore.Activity;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.SDMInterpreterConstants;
import de.mdelab.sdm.interpreter.core.variables.Variable;
import de.mdelab.sdm.interpreter.sde.eclipse.SDEEclipseSDMInterpreter;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import de.mdelab.workflow.util.WorkflowUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Model Generator</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.ModelGeneratorImpl#getAxiomGeneratorActivity
 * <em>Axiom Generator Activity</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.ModelGeneratorImpl#getRuleGeneratorActivities
 * <em>Rule Generator Activities</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.ModelGeneratorImpl#getCorrespondenceModelSize
 * <em>Correspondence Model Size</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.ModelGeneratorImpl#getLeftModelSlot
 * <em>Left Model Slot</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.ModelGeneratorImpl#getRightModelSlot
 * <em>Right Model Slot</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.ModelGeneratorImpl#getLeftModelURI
 * <em>Left Model URI</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.ModelGeneratorImpl#getRightModelURI
 * <em>Right Model URI</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.ModelGeneratorImpl#getCorrNodeTypeRestrictions
 * <em>Corr Node Type Restrictions</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ModelGeneratorImpl extends WorkflowComponentImpl implements ModelGenerator
{
	/**
	 * The cached value of the '{@link #getAxiomGeneratorActivity()
	 * <em>Axiom Generator Activity</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getAxiomGeneratorActivity()
	 * @generated
	 * @ordered
	 */
	protected GeneratorActivity					axiomGeneratorActivity;

	/**
	 * The cached value of the '{@link #getRuleGeneratorActivities()
	 * <em>Rule Generator Activities</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRuleGeneratorActivities()
	 * @generated
	 * @ordered
	 */
	protected EList<GeneratorActivity>			ruleGeneratorActivities;

	/**
	 * The default value of the '{@link #getCorrespondenceModelSize()
	 * <em>Correspondence Model Size</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getCorrespondenceModelSize()
	 * @generated
	 * @ordered
	 */
	protected static final String				CORRESPONDENCE_MODEL_SIZE_EDEFAULT	= "100";

	/**
	 * The cached value of the '{@link #getCorrespondenceModelSize()
	 * <em>Correspondence Model Size</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getCorrespondenceModelSize()
	 * @generated
	 * @ordered
	 */
	protected String							correspondenceModelSize				= CORRESPONDENCE_MODEL_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLeftModelSlot()
	 * <em>Left Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getLeftModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String				LEFT_MODEL_SLOT_EDEFAULT			= null;

	/**
	 * The cached value of the '{@link #getLeftModelSlot()
	 * <em>Left Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getLeftModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String							leftModelSlot						= LEFT_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getRightModelSlot()
	 * <em>Right Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRightModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String				RIGHT_MODEL_SLOT_EDEFAULT			= null;

	/**
	 * The cached value of the '{@link #getRightModelSlot()
	 * <em>Right Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRightModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String							rightModelSlot						= RIGHT_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getLeftModelURI()
	 * <em>Left Model URI</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getLeftModelURI()
	 * @generated
	 * @ordered
	 */
	protected static final String				LEFT_MODEL_URI_EDEFAULT				= null;

	/**
	 * The cached value of the '{@link #getLeftModelURI()
	 * <em>Left Model URI</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getLeftModelURI()
	 * @generated
	 * @ordered
	 */
	protected String							leftModelURI						= LEFT_MODEL_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getRightModelURI()
	 * <em>Right Model URI</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRightModelURI()
	 * @generated
	 * @ordered
	 */
	protected static final String				RIGHT_MODEL_URI_EDEFAULT			= null;

	/**
	 * The cached value of the '{@link #getRightModelURI()
	 * <em>Right Model URI</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRightModelURI()
	 * @generated
	 * @ordered
	 */
	protected String							rightModelURI						= RIGHT_MODEL_URI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCorrNodeTypeRestrictions()
	 * <em>Corr Node Type Restrictions</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCorrNodeTypeRestrictions()
	 * @generated
	 * @ordered
	 */
	protected EList<CorrNodeTypeRestriction>	corrNodeTypeRestrictions;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ModelGeneratorImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WorkflowComponentsPackage.Literals.MODEL_GENERATOR;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public GeneratorActivity getAxiomGeneratorActivity()
	{
		return this.axiomGeneratorActivity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetAxiomGeneratorActivity(GeneratorActivity newAxiomGeneratorActivity, NotificationChain msgs)
	{
		GeneratorActivity oldAxiomGeneratorActivity = this.axiomGeneratorActivity;
		this.axiomGeneratorActivity = newAxiomGeneratorActivity;
		if (this.eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					WorkflowComponentsPackage.MODEL_GENERATOR__AXIOM_GENERATOR_ACTIVITY, oldAxiomGeneratorActivity,
					newAxiomGeneratorActivity);
			if (msgs == null)
			{
				msgs = notification;
			}
			else
			{
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setAxiomGeneratorActivity(GeneratorActivity newAxiomGeneratorActivity)
	{
		if (newAxiomGeneratorActivity != this.axiomGeneratorActivity)
		{
			NotificationChain msgs = null;
			if (this.axiomGeneratorActivity != null)
			{
				msgs = ((InternalEObject) this.axiomGeneratorActivity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- WorkflowComponentsPackage.MODEL_GENERATOR__AXIOM_GENERATOR_ACTIVITY, null, msgs);
			}
			if (newAxiomGeneratorActivity != null)
			{
				msgs = ((InternalEObject) newAxiomGeneratorActivity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE
						- WorkflowComponentsPackage.MODEL_GENERATOR__AXIOM_GENERATOR_ACTIVITY, null, msgs);
			}
			msgs = this.basicSetAxiomGeneratorActivity(newAxiomGeneratorActivity, msgs);
			if (msgs != null)
			{
				msgs.dispatch();
			}
		}
		else if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MODEL_GENERATOR__AXIOM_GENERATOR_ACTIVITY,
					newAxiomGeneratorActivity, newAxiomGeneratorActivity));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<GeneratorActivity> getRuleGeneratorActivities()
	{
		if (this.ruleGeneratorActivities == null)
		{
			this.ruleGeneratorActivities = new EObjectContainmentEList<GeneratorActivity>(GeneratorActivity.class, this,
					WorkflowComponentsPackage.MODEL_GENERATOR__RULE_GENERATOR_ACTIVITIES);
		}
		return this.ruleGeneratorActivities;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getCorrespondenceModelSize()
	{
		return this.correspondenceModelSize;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setCorrespondenceModelSize(String newCorrespondenceModelSize)
	{
		String oldCorrespondenceModelSize = this.correspondenceModelSize;
		this.correspondenceModelSize = newCorrespondenceModelSize;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET,
					WorkflowComponentsPackage.MODEL_GENERATOR__CORRESPONDENCE_MODEL_SIZE, oldCorrespondenceModelSize,
					this.correspondenceModelSize));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getLeftModelSlot()
	{
		return this.leftModelSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setLeftModelSlot(String newLeftModelSlot)
	{
		String oldLeftModelSlot = this.leftModelSlot;
		this.leftModelSlot = newLeftModelSlot;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MODEL_GENERATOR__LEFT_MODEL_SLOT,
					oldLeftModelSlot, this.leftModelSlot));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getRightModelSlot()
	{
		return this.rightModelSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setRightModelSlot(String newRightModelSlot)
	{
		String oldRightModelSlot = this.rightModelSlot;
		this.rightModelSlot = newRightModelSlot;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MODEL_GENERATOR__RIGHT_MODEL_SLOT,
					oldRightModelSlot, this.rightModelSlot));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getLeftModelURI()
	{
		return this.leftModelURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setLeftModelURI(String newLeftModelURI)
	{
		String oldLeftModelURI = this.leftModelURI;
		this.leftModelURI = newLeftModelURI;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MODEL_GENERATOR__LEFT_MODEL_URI,
					oldLeftModelURI, this.leftModelURI));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getRightModelURI()
	{
		return this.rightModelURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setRightModelURI(String newRightModelURI)
	{
		String oldRightModelURI = this.rightModelURI;
		this.rightModelURI = newRightModelURI;
		if (this.eNotificationRequired())
		{
			this.eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MODEL_GENERATOR__RIGHT_MODEL_URI,
					oldRightModelURI, this.rightModelURI));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<CorrNodeTypeRestriction> getCorrNodeTypeRestrictions()
	{
		if (this.corrNodeTypeRestrictions == null)
		{
			this.corrNodeTypeRestrictions = new EObjectContainmentEList<CorrNodeTypeRestriction>(CorrNodeTypeRestriction.class, this,
					WorkflowComponentsPackage.MODEL_GENERATOR__CORR_NODE_TYPE_RESTRICTIONS);
		}
		return this.corrNodeTypeRestrictions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MODEL_GENERATOR__AXIOM_GENERATOR_ACTIVITY:
				return this.basicSetAxiomGeneratorActivity(null, msgs);
			case WorkflowComponentsPackage.MODEL_GENERATOR__RULE_GENERATOR_ACTIVITIES:
				return ((InternalEList<?>) this.getRuleGeneratorActivities()).basicRemove(otherEnd, msgs);
			case WorkflowComponentsPackage.MODEL_GENERATOR__CORR_NODE_TYPE_RESTRICTIONS:
				return ((InternalEList<?>) this.getCorrNodeTypeRestrictions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MODEL_GENERATOR__AXIOM_GENERATOR_ACTIVITY:
				return this.getAxiomGeneratorActivity();
			case WorkflowComponentsPackage.MODEL_GENERATOR__RULE_GENERATOR_ACTIVITIES:
				return this.getRuleGeneratorActivities();
			case WorkflowComponentsPackage.MODEL_GENERATOR__CORRESPONDENCE_MODEL_SIZE:
				return this.getCorrespondenceModelSize();
			case WorkflowComponentsPackage.MODEL_GENERATOR__LEFT_MODEL_SLOT:
				return this.getLeftModelSlot();
			case WorkflowComponentsPackage.MODEL_GENERATOR__RIGHT_MODEL_SLOT:
				return this.getRightModelSlot();
			case WorkflowComponentsPackage.MODEL_GENERATOR__LEFT_MODEL_URI:
				return this.getLeftModelURI();
			case WorkflowComponentsPackage.MODEL_GENERATOR__RIGHT_MODEL_URI:
				return this.getRightModelURI();
			case WorkflowComponentsPackage.MODEL_GENERATOR__CORR_NODE_TYPE_RESTRICTIONS:
				return this.getCorrNodeTypeRestrictions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MODEL_GENERATOR__AXIOM_GENERATOR_ACTIVITY:
				this.setAxiomGeneratorActivity((GeneratorActivity) newValue);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__RULE_GENERATOR_ACTIVITIES:
				this.getRuleGeneratorActivities().clear();
				this.getRuleGeneratorActivities().addAll((Collection<? extends GeneratorActivity>) newValue);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__CORRESPONDENCE_MODEL_SIZE:
				this.setCorrespondenceModelSize((String) newValue);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__LEFT_MODEL_SLOT:
				this.setLeftModelSlot((String) newValue);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__RIGHT_MODEL_SLOT:
				this.setRightModelSlot((String) newValue);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__LEFT_MODEL_URI:
				this.setLeftModelURI((String) newValue);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__RIGHT_MODEL_URI:
				this.setRightModelURI((String) newValue);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__CORR_NODE_TYPE_RESTRICTIONS:
				this.getCorrNodeTypeRestrictions().clear();
				this.getCorrNodeTypeRestrictions().addAll((Collection<? extends CorrNodeTypeRestriction>) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MODEL_GENERATOR__AXIOM_GENERATOR_ACTIVITY:
				this.setAxiomGeneratorActivity((GeneratorActivity) null);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__RULE_GENERATOR_ACTIVITIES:
				this.getRuleGeneratorActivities().clear();
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__CORRESPONDENCE_MODEL_SIZE:
				this.setCorrespondenceModelSize(CORRESPONDENCE_MODEL_SIZE_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__LEFT_MODEL_SLOT:
				this.setLeftModelSlot(LEFT_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__RIGHT_MODEL_SLOT:
				this.setRightModelSlot(RIGHT_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__LEFT_MODEL_URI:
				this.setLeftModelURI(LEFT_MODEL_URI_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__RIGHT_MODEL_URI:
				this.setRightModelURI(RIGHT_MODEL_URI_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MODEL_GENERATOR__CORR_NODE_TYPE_RESTRICTIONS:
				this.getCorrNodeTypeRestrictions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MODEL_GENERATOR__AXIOM_GENERATOR_ACTIVITY:
				return this.axiomGeneratorActivity != null;
			case WorkflowComponentsPackage.MODEL_GENERATOR__RULE_GENERATOR_ACTIVITIES:
				return this.ruleGeneratorActivities != null && !this.ruleGeneratorActivities.isEmpty();
			case WorkflowComponentsPackage.MODEL_GENERATOR__CORRESPONDENCE_MODEL_SIZE:
				return CORRESPONDENCE_MODEL_SIZE_EDEFAULT == null ? this.correspondenceModelSize != null
						: !CORRESPONDENCE_MODEL_SIZE_EDEFAULT.equals(this.correspondenceModelSize);
			case WorkflowComponentsPackage.MODEL_GENERATOR__LEFT_MODEL_SLOT:
				return LEFT_MODEL_SLOT_EDEFAULT == null ? this.leftModelSlot != null : !LEFT_MODEL_SLOT_EDEFAULT.equals(this.leftModelSlot);
			case WorkflowComponentsPackage.MODEL_GENERATOR__RIGHT_MODEL_SLOT:
				return RIGHT_MODEL_SLOT_EDEFAULT == null ? this.rightModelSlot != null : !RIGHT_MODEL_SLOT_EDEFAULT
						.equals(this.rightModelSlot);
			case WorkflowComponentsPackage.MODEL_GENERATOR__LEFT_MODEL_URI:
				return LEFT_MODEL_URI_EDEFAULT == null ? this.leftModelURI != null : !LEFT_MODEL_URI_EDEFAULT.equals(this.leftModelURI);
			case WorkflowComponentsPackage.MODEL_GENERATOR__RIGHT_MODEL_URI:
				return RIGHT_MODEL_URI_EDEFAULT == null ? this.rightModelURI != null : !RIGHT_MODEL_URI_EDEFAULT.equals(this.rightModelURI);
			case WorkflowComponentsPackage.MODEL_GENERATOR__CORR_NODE_TYPE_RESTRICTIONS:
				return this.corrNodeTypeRestrictions != null && !this.corrNodeTypeRestrictions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (this.eIsProxy())
		{
			return super.toString();
		}

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (correspondenceModelSize: ");
		result.append(this.correspondenceModelSize);
		result.append(", leftModelSlot: ");
		result.append(this.leftModelSlot);
		result.append(", rightModelSlot: ");
		result.append(this.rightModelSlot);
		result.append(", leftModelURI: ");
		result.append(this.leftModelURI);
		result.append(", rightModelURI: ");
		result.append(this.rightModelURI);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean checkConfiguration(WorkflowExecutionContext context) throws IOException
	{
		boolean success = true;

		if (this.getAxiomGeneratorActivity() == null)
		{
			context.getLogger().addError("axiom generator activity is null", null, this);
			success = false;
		}
		else
		{
			if (this.getAxiomGeneratorActivity().getActivity() == null)
			{
				context.getLogger().addError("activity is null", null, this);
				success = false;
			}

			if (!this.getAxiomGeneratorActivity().getParentCorrNodeParameters().isEmpty())
			{
				context.getLogger().addError(
						"An axiom cannot have parent correspondence nodes. There parentCorrNodeParameters must be empty.", null, this);
				success = false;
			}
		}

		return success;
	}

	@Override
	public void execute(WorkflowExecutionContext context) throws WorkflowExecutionException, IOException
	{
		try
		{
			this.counters = new HashMap<String, Integer>();

			/*
			 * Analyze restrictions
			 */
			Map<EClass, CorrNodeTypeRestriction> corrNodeTypeRestrictions = new HashMap<EClass, CorrNodeTypeRestriction>();

			// Count the number of instances of a corr node type
			Map<EClass, Integer> noNodes = new HashMap<EClass, Integer>();

			for (CorrNodeTypeRestriction r : this.getCorrNodeTypeRestrictions())
			{
				corrNodeTypeRestrictions.put(r.getCorrNodeType(), r);
				noNodes.put(r.getCorrNodeType(), 0);
			}

			/*
			 * Execute axiom
			 */
			context.getLogger().addInfo("Executing axiom '" + this.getAxiomGeneratorActivity().getActivity().getName() + "'...", this);

			TGGNode corrAxiom = this.executeAxiom(context, this.getAxiomGeneratorActivity());

			// Increase the counter for this node type
			noNodes.put(corrAxiom.eClass(), 1);

			/*
			 * Create resources for the models
			 */
			Resource sourceResource = context.getGlobalResourceSet().createResource(
					WorkflowUtil.getResolvedURI(URI.createURI(this.getLeftModelURI()), context.getWorkflowFileURI()));
			sourceResource.getContents().add(corrAxiom.getSources().get(0));

			Resource targetResource = context.getGlobalResourceSet().createResource(
					WorkflowUtil.getResolvedURI(URI.createURI(this.getRightModelURI()), context.getWorkflowFileURI()));
			targetResource.getContents().add(corrAxiom.getTargets().get(0));

			/*
			 * Add models to model slots
			 */
			context.getModelSlots().put(this.getLeftModelSlot(), corrAxiom.getSources().get(0));
			context.getModelSlots().put(this.getRightModelSlot(), corrAxiom.getTargets().get(0));

			/*
			 * Create list for the correspondence nodes
			 */
			List<TGGNode> corrNodes = new ArrayList<TGGNode>();
			corrNodes.add(corrAxiom);

			/*
			 * Execute rules until - the model size has been reached - the
			 * maximum applications for each rule have been reached - no rule
			 * can be applied anymore
			 */
			boolean ruleApplied = true;

			int modelSize = Integer.parseInt(this.getCorrespondenceModelSize());

			while (ruleApplied && corrNodes.size() < modelSize)
			{
				ruleApplied = false;

				for (GeneratorActivity generatorActivity : this.getRuleGeneratorActivities())
				{
					if (this.executeRule(context, generatorActivity, corrNodes, noNodes, corrNodeTypeRestrictions))
					{
						ruleApplied = true;
					}
				}
			}

			context.getLogger().addInfo("Successfully generated reference models", this);
			context.getLogger().addInfo("Number of correspondence nodes: " + corrNodes.size(), this);
		}
		catch (SDMException e)
		{
			context.getLogger().addError("Error during story diagram execution.", e, this);
			throw new WorkflowExecutionException("Error during story diagram execution.", e);
		}
	}

	private TGGNode executeAxiom(WorkflowExecutionContext context, GeneratorActivity generatorActivity) throws SDMException
	{
		List<Variable<EClassifier>> variableBindings = new ArrayList<Variable<EClassifier>>();

		for (Parameter ruleParameter : generatorActivity.getRuleParameters())
		{
			if (ruleParameter.getType() == EcorePackage.Literals.ESTRING)
			{
				Variable<EClassifier> iv = new Variable<EClassifier>(ruleParameter.getName(), ruleParameter.getType(),
						this.getNextName(ruleParameter.getBaseValue()));

				variableBindings.add(iv);
			}
			else
			{
				context.getLogger().addError("Generating non-string values is currently not supported.", null, this);
				throw new UnsupportedOperationException("Generating non-string values is currently not supported.");
			}
		}

		context.getLogger().addInfo("Executing axiom '" + generatorActivity.getActivity().getName() + "'...", this);

		return (TGGNode) this.executeActivity(generatorActivity.getActivity(), variableBindings);
	}

	private boolean executeRule(WorkflowExecutionContext context, GeneratorActivity generatorActivity, List<TGGNode> corrNodes,
			Map<EClass, Integer> noNodes, Map<EClass, CorrNodeTypeRestriction> corrNodeTypeRestrictions) throws SDMException
	{
		/*
		 * Check if this rule creates a correspondence node that has a maximum
		 * number restriction. If this maximum is reached, abort.
		 */
		if (corrNodeTypeRestrictions.get(generatorActivity.getCreatedCorrNodeType()) != null)
		{
			int maxNumber = Integer.parseInt(corrNodeTypeRestrictions.get(generatorActivity.getCreatedCorrNodeType()).getMaxNumber());

			if (noNodes.containsKey(generatorActivity.getCreatedCorrNodeType())
					&& noNodes.get(generatorActivity.getCreatedCorrNodeType()) >= maxNumber)
			{
				return false;
			}
		}
		/*
		 * Find suitable TGGNodes for the required parent corr node parameters
		 */
		List<Variable<EClassifier>> variableBindings = new ArrayList<Variable<EClassifier>>();

		Set<TGGNode> usedNodes = new HashSet<TGGNode>();

		for (Parameter param : generatorActivity.getParentCorrNodeParameters())
		{
			for (TGGNode n : corrNodes)
			{
				/*
				 * Check the correspondence node is not already used as a
				 * different parameter and that it is an instance of the
				 * required type.
				 */
				if (!usedNodes.contains(n) && param.getType().isInstance(n))
				{
					CorrNodeTypeRestriction r = corrNodeTypeRestrictions.get(n.eClass());

					int maxChildNodes = Integer.MAX_VALUE;

					if (r != null)
					{
						maxChildNodes = Integer.parseInt(r.getMaxChildNodes());
					}

					/*
					 * Check the the maximum number of nodes for this type of
					 * correspondence node and the maximum number of children
					 * have not been reached
					 */
					if (n.getNext().size() < maxChildNodes)
					{
						Variable<EClassifier> iv = new Variable<EClassifier>(param.getName(), param.getType(), n);

						variableBindings.add(iv);

						usedNodes.add(n);

						break;
					}
				}
			}
		}

		if (variableBindings.size() != generatorActivity.getParentCorrNodeParameters().size())
		{
			/*
			 * Could not find proper values for all required parameters. Return
			 * immediately.
			 */
			return false;
		}

		/*
		 * Set the rule parameters
		 */
		for (Parameter ruleParameter : generatorActivity.getRuleParameters())
		{

			if (ruleParameter.getType() == EcorePackage.Literals.ESTRING)
			{
				Variable<EClassifier> iv = new Variable<EClassifier>(ruleParameter.getName(), ruleParameter.getType(),
						this.getNextName(ruleParameter.getBaseValue()));

				variableBindings.add(iv);
			}
			else
			{
				context.getLogger().addError("Generating non-string values is currently not supported.", null, this);
				throw new UnsupportedOperationException("Generating non-string values is currently not supported.");
			}
		}

		/*
		 * Execute the rule
		 */
		context.getLogger().addInfo("Executing rule '" + generatorActivity.getActivity().getName() + "'...", this);

		TGGNode corrNode = (TGGNode) this.executeActivity(generatorActivity.getActivity(), variableBindings);

		if (corrNode != null)
		{
			if (!noNodes.containsKey(corrNode.eClass()))
			{
				noNodes.put(corrNode.eClass(), 0);
			}

			noNodes.put(corrNode.eClass(), noNodes.get(corrNode.eClass()) + 1);

			corrNodes.add(corrNode);

			return true;
		}
		else
		{
			return false;
		}
	}

	private SDEEclipseSDMInterpreter	interpreter;

	private Object executeActivity(Activity activity, List<Variable<EClassifier>> variableBindings) throws SDMException
	{
		if (this.interpreter == null)
		{
			this.interpreter = new SDEEclipseSDMInterpreter(this.getClass().getClassLoader());
		}

		return this.interpreter.executeActivity(activity, variableBindings).get(SDMInterpreterConstants.RETURN_VALUE_VAR_NAME).getValue();
	}

	Map<String, Integer>	counters;

	private String getNextName(String baseName)
	{
		Integer counter = this.counters.get(baseName);

		if (counter == null)
		{
			counter = 0;
		}

		counter++;

		this.counters.put(baseName, counter);

		return new String(baseName + counter);
	}
} // ModelGeneratorImpl
