/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage
 * @generated
 */
public interface WorkflowComponentsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	WorkflowComponentsFactory eINSTANCE = de.hpi.sam.mote.workflowComponents.impl.WorkflowComponentsFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>Model Generator</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Model Generator</em>'.
	 * @generated
	 */
	ModelGenerator createModelGenerator();

	/**
	 * Returns a new object of class '<em>Generator Activity</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Generator Activity</em>'.
	 * @generated
	 */
	GeneratorActivity createGeneratorActivity();

	/**
	 * Returns a new object of class '<em>Parameter</em>'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter</em>'.
	 * @generated
	 */
	Parameter createParameter();

	/**
	 * Returns a new object of class '<em>Corr Node Type Restriction</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Corr Node Type Restriction</em>'.
	 * @generated
	 */
	CorrNodeTypeRestriction createCorrNodeTypeRestriction();

	/**
	 * Returns a new object of class '<em>Mote Transformer</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Mote Transformer</em>'.
	 * @generated
	 */
	MoteTransformer createMoteTransformer();

	/**
	 * Returns a new object of class '<em>Model Comparer</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Model Comparer</em>'.
	 * @generated
	 */
	ModelComparer createModelComparer();

	/**
	 * Returns a new object of class '<em>Mote Transformer Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mote Transformer Mapping</em>'.
	 * @generated
	 */
	MoteTransformerMapping createMoteTransformerMapping();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WorkflowComponentsPackage getWorkflowComponentsPackage();

} // WorkflowComponentsFactory
