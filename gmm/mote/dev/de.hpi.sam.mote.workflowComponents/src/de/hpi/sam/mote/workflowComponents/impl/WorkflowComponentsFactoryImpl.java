/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents.impl;

import de.hpi.sam.mote.workflowComponents.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction;
import de.hpi.sam.mote.workflowComponents.GeneratorActivity;
import de.hpi.sam.mote.workflowComponents.ModelComparer;
import de.hpi.sam.mote.workflowComponents.ModelGenerator;
import de.hpi.sam.mote.workflowComponents.MoteTransformer;
import de.hpi.sam.mote.workflowComponents.Parameter;
import de.hpi.sam.mote.workflowComponents.WorkflowComponentsFactory;
import de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class WorkflowComponentsFactoryImpl extends EFactoryImpl implements
		WorkflowComponentsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static WorkflowComponentsFactory init() {
		try {
			WorkflowComponentsFactory theWorkflowComponentsFactory = (WorkflowComponentsFactory) EPackage.Registry.INSTANCE
					.getEFactory("http://mote/workflowComponents/1.0");
			if (theWorkflowComponentsFactory != null) {
				return theWorkflowComponentsFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WorkflowComponentsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public WorkflowComponentsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case WorkflowComponentsPackage.MODEL_GENERATOR:
			return createModelGenerator();
		case WorkflowComponentsPackage.GENERATOR_ACTIVITY:
			return createGeneratorActivity();
		case WorkflowComponentsPackage.PARAMETER:
			return createParameter();
		case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION:
			return createCorrNodeTypeRestriction();
		case WorkflowComponentsPackage.MOTE_TRANSFORMER:
			return createMoteTransformer();
		case WorkflowComponentsPackage.MODEL_COMPARER:
			return createModelComparer();
		case WorkflowComponentsPackage.MOTE_TRANSFORMER_MAPPING:
			return createMoteTransformerMapping();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModelGenerator createModelGenerator() {
		ModelGeneratorImpl modelGenerator = new ModelGeneratorImpl();
		return modelGenerator;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public GeneratorActivity createGeneratorActivity() {
		GeneratorActivityImpl generatorActivity = new GeneratorActivityImpl();
		return generatorActivity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CorrNodeTypeRestriction createCorrNodeTypeRestriction() {
		CorrNodeTypeRestrictionImpl corrNodeTypeRestriction = new CorrNodeTypeRestrictionImpl();
		return corrNodeTypeRestriction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MoteTransformer createMoteTransformer() {
		MoteTransformerImpl moteTransformer = new MoteTransformerImpl();
		return moteTransformer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModelComparer createModelComparer() {
		ModelComparerImpl modelComparer = new ModelComparerImpl();
		return modelComparer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MoteTransformerMapping createMoteTransformerMapping() {
		MoteTransformerMappingImpl moteTransformerMapping = new MoteTransformerMappingImpl();
		return moteTransformerMapping;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowComponentsPackage getWorkflowComponentsPackage() {
		return (WorkflowComponentsPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WorkflowComponentsPackage getPackage() {
		return WorkflowComponentsPackage.eINSTANCE;
	}

} // WorkflowComponentsFactoryImpl
