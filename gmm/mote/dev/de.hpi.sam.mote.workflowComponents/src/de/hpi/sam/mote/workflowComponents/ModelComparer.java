/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Model Comparer</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link de.hpi.sam.mote.workflowComponents.ModelComparer#getModelSlot1
 * <em>Model Slot1</em>}</li>
 * <li>{@link de.hpi.sam.mote.workflowComponents.ModelComparer#getModelSlot2
 * <em>Model Slot2</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.ModelComparer#getComparisonResultModelSlot
 * <em>Comparison Result Model Slot</em>}</li>
 * <li>{@link de.hpi.sam.mote.workflowComponents.ModelComparer#getDiffModelSlot
 * <em>Diff Model Slot</em>}</li>
 * </ul>
 * </p>
 * 
 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelComparer()
 * @model
 * @generated
 */
public interface ModelComparer extends WorkflowComponent
{
	/**
	 * Returns the value of the '<em><b>Model Slot1</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Slot1</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Model Slot1</em>' attribute.
	 * @see #setModelSlot1(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelComparer_ModelSlot1()
	 * @model required="true"
	 * @generated
	 */
	String getModelSlot1();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.ModelComparer#getModelSlot1
	 * <em>Model Slot1</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Model Slot1</em>' attribute.
	 * @see #getModelSlot1()
	 * @generated
	 */
	void setModelSlot1(String value);

	/**
	 * Returns the value of the '<em><b>Model Slot2</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Slot2</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Model Slot2</em>' attribute.
	 * @see #setModelSlot2(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelComparer_ModelSlot2()
	 * @model required="true"
	 * @generated
	 */
	String getModelSlot2();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.ModelComparer#getModelSlot2
	 * <em>Model Slot2</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Model Slot2</em>' attribute.
	 * @see #getModelSlot2()
	 * @generated
	 */
	void setModelSlot2(String value);

	/**
	 * Returns the value of the '<em><b>Comparison Result Model Slot</b></em>'
	 * attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comparison Result Model Slot</em>' attribute
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Comparison Result Model Slot</em>'
	 *         attribute.
	 * @see #setComparisonResultModelSlot(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelComparer_ComparisonResultModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getComparisonResultModelSlot();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.ModelComparer#getComparisonResultModelSlot
	 * <em>Comparison Result Model Slot</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Comparison Result Model Slot</em>'
	 *            attribute.
	 * @see #getComparisonResultModelSlot()
	 * @generated
	 */
	void setComparisonResultModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Diff Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diff Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Diff Model Slot</em>' attribute.
	 * @see #setDiffModelSlot(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelComparer_DiffModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getDiffModelSlot();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.ModelComparer#getDiffModelSlot
	 * <em>Diff Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Diff Model Slot</em>' attribute.
	 * @see #getDiffModelSlot()
	 * @generated
	 */
	void setDiffModelSlot(String value);

} // ModelComparer
