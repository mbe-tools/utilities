/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents.impl;

import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;

import de.hpi.sam.mote.MoteFactory;
import de.hpi.sam.mote.TGGEngine;
import de.hpi.sam.mote.TransformationDirection;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.workflowComponents.MoteTransformer;
import de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Mote Transformer</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.MoteTransformerImpl#getSourceModelSlot
 * <em>Source Model Slot</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.MoteTransformerImpl#getTargetModelSlot
 * <em>Target Model Slot</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.MoteTransformerImpl#getRuleSetID
 * <em>Rule Set ID</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.MoteTransformerImpl#getSynchronize
 * <em>Synchronize</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.MoteTransformerImpl#getTransformationDirection
 * <em>Transformation Direction</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.MoteTransformerImpl#getTggEngineSlotName
 * <em>Tgg Engine Slot Name</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class MoteTransformerImpl extends WorkflowComponentImpl implements MoteTransformer
{
	protected static final String	FORWARD								= "forward";
	protected static final String	REVERSE								= "reverse";
	protected static final String	MAPPING								= "mapping";
	// private static final String Compare = "compare";

	/**
	 * The default value of the '{@link #getSourceModelSlot()
	 * <em>Source Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getSourceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String	SOURCE_MODEL_SLOT_EDEFAULT			= null;

	/**
	 * The cached value of the '{@link #getSourceModelSlot()
	 * <em>Source Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getSourceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String				sourceModelSlot						= SOURCE_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetModelSlot()
	 * <em>Target Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTargetModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String	TARGET_MODEL_SLOT_EDEFAULT			= null;

	/**
	 * The cached value of the '{@link #getTargetModelSlot()
	 * <em>Target Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTargetModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String				targetModelSlot						= TARGET_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getRuleSetID() <em>Rule Set ID</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRuleSetID()
	 * @generated
	 * @ordered
	 */
	protected static final String	RULE_SET_ID_EDEFAULT				= null;

	/**
	 * The cached value of the '{@link #getRuleSetID() <em>Rule Set ID</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRuleSetID()
	 * @generated
	 * @ordered
	 */
	protected String				ruleSetID							= RULE_SET_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSynchronize() <em>Synchronize</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSynchronize()
	 * @generated
	 * @ordered
	 */
	protected static final String	SYNCHRONIZE_EDEFAULT				= null;

	/**
	 * The cached value of the '{@link #getSynchronize() <em>Synchronize</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSynchronize()
	 * @generated
	 * @ordered
	 */
	protected String				synchronize							= SYNCHRONIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTransformationDirection()
	 * <em>Transformation Direction</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getTransformationDirection()
	 * @generated
	 * @ordered
	 */
	protected static final String	TRANSFORMATION_DIRECTION_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getTransformationDirection()
	 * <em>Transformation Direction</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getTransformationDirection()
	 * @generated
	 * @ordered
	 */
	protected String				transformationDirection				= TRANSFORMATION_DIRECTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getTggEngineSlotName()
	 * <em>Tgg Engine Slot Name</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTggEngineSlotName()
	 * @generated
	 * @ordered
	 */
	protected static final String	TGG_ENGINE_SLOT_NAME_EDEFAULT		= null;
	/**
	 * The cached value of the '{@link #getTggEngineSlotName()
	 * <em>Tgg Engine Slot Name</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTggEngineSlotName()
	 * @generated
	 * @ordered
	 */
	protected String				tggEngineSlotName					= TGG_ENGINE_SLOT_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected MoteTransformerImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WorkflowComponentsPackage.Literals.MOTE_TRANSFORMER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getSourceModelSlot()
	{
		return sourceModelSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSourceModelSlot(String newSourceModelSlot)
	{
		String oldSourceModelSlot = sourceModelSlot;
		sourceModelSlot = newSourceModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MOTE_TRANSFORMER__SOURCE_MODEL_SLOT,
					oldSourceModelSlot, sourceModelSlot));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getTargetModelSlot()
	{
		return targetModelSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTargetModelSlot(String newTargetModelSlot)
	{
		String oldTargetModelSlot = targetModelSlot;
		targetModelSlot = newTargetModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MOTE_TRANSFORMER__TARGET_MODEL_SLOT,
					oldTargetModelSlot, targetModelSlot));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getRuleSetID()
	{
		return ruleSetID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setRuleSetID(String newRuleSetID)
	{
		String oldRuleSetID = ruleSetID;
		ruleSetID = newRuleSetID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MOTE_TRANSFORMER__RULE_SET_ID, oldRuleSetID,
					ruleSetID));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getSynchronize()
	{
		return synchronize;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSynchronize(String newSynchronize)
	{
		String oldSynchronize = synchronize;
		synchronize = newSynchronize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MOTE_TRANSFORMER__SYNCHRONIZE, oldSynchronize,
					synchronize));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getTransformationDirection()
	{
		return transformationDirection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTransformationDirection(String newTransformationDirection)
	{
		String oldTransformationDirection = transformationDirection;
		transformationDirection = newTransformationDirection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MOTE_TRANSFORMER__TRANSFORMATION_DIRECTION,
					oldTransformationDirection, transformationDirection));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getTggEngineSlotName()
	{
		return tggEngineSlotName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTggEngineSlotName(String newTggEngineSlotName)
	{
		String oldTggEngineSlotName = tggEngineSlotName;
		tggEngineSlotName = newTggEngineSlotName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MOTE_TRANSFORMER__TGG_ENGINE_SLOT_NAME,
					oldTggEngineSlotName, tggEngineSlotName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__SOURCE_MODEL_SLOT:
				return getSourceModelSlot();
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__TARGET_MODEL_SLOT:
				return getTargetModelSlot();
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__RULE_SET_ID:
				return getRuleSetID();
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__SYNCHRONIZE:
				return getSynchronize();
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__TRANSFORMATION_DIRECTION:
				return getTransformationDirection();
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__TGG_ENGINE_SLOT_NAME:
				return getTggEngineSlotName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__SOURCE_MODEL_SLOT:
				setSourceModelSlot((String) newValue);
				return;
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__TARGET_MODEL_SLOT:
				setTargetModelSlot((String) newValue);
				return;
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__RULE_SET_ID:
				setRuleSetID((String) newValue);
				return;
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__SYNCHRONIZE:
				setSynchronize((String) newValue);
				return;
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__TRANSFORMATION_DIRECTION:
				setTransformationDirection((String) newValue);
				return;
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__TGG_ENGINE_SLOT_NAME:
				setTggEngineSlotName((String) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__SOURCE_MODEL_SLOT:
				setSourceModelSlot(SOURCE_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__TARGET_MODEL_SLOT:
				setTargetModelSlot(TARGET_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__RULE_SET_ID:
				setRuleSetID(RULE_SET_ID_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__SYNCHRONIZE:
				setSynchronize(SYNCHRONIZE_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__TRANSFORMATION_DIRECTION:
				setTransformationDirection(TRANSFORMATION_DIRECTION_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__TGG_ENGINE_SLOT_NAME:
				setTggEngineSlotName(TGG_ENGINE_SLOT_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__SOURCE_MODEL_SLOT:
				return SOURCE_MODEL_SLOT_EDEFAULT == null ? sourceModelSlot != null : !SOURCE_MODEL_SLOT_EDEFAULT.equals(sourceModelSlot);
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__TARGET_MODEL_SLOT:
				return TARGET_MODEL_SLOT_EDEFAULT == null ? targetModelSlot != null : !TARGET_MODEL_SLOT_EDEFAULT.equals(targetModelSlot);
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__RULE_SET_ID:
				return RULE_SET_ID_EDEFAULT == null ? ruleSetID != null : !RULE_SET_ID_EDEFAULT.equals(ruleSetID);
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__SYNCHRONIZE:
				return SYNCHRONIZE_EDEFAULT == null ? synchronize != null : !SYNCHRONIZE_EDEFAULT.equals(synchronize);
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__TRANSFORMATION_DIRECTION:
				return TRANSFORMATION_DIRECTION_EDEFAULT == null ? transformationDirection != null : !TRANSFORMATION_DIRECTION_EDEFAULT
						.equals(transformationDirection);
			case WorkflowComponentsPackage.MOTE_TRANSFORMER__TGG_ENGINE_SLOT_NAME:
				return TGG_ENGINE_SLOT_NAME_EDEFAULT == null ? tggEngineSlotName != null : !TGG_ENGINE_SLOT_NAME_EDEFAULT
						.equals(tggEngineSlotName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sourceModelSlot: ");
		result.append(sourceModelSlot);
		result.append(", targetModelSlot: ");
		result.append(targetModelSlot);
		result.append(", ruleSetID: ");
		result.append(ruleSetID);
		result.append(", synchronize: ");
		result.append(synchronize);
		result.append(", transformationDirection: ");
		result.append(transformationDirection);
		result.append(", tggEngineSlotName: ");
		result.append(tggEngineSlotName);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean checkConfiguration(WorkflowExecutionContext context) throws IOException
	{
		boolean success = true;

		// modelSlots einf�gen

		if (getRuleSetID() == null || "".equals(getRuleSetID()))
		{
			context.getLogger().addError("ruleSetID is null", null, this);
			success = false;
		}

		return success;
	}

	@Override
	public void execute(WorkflowExecutionContext context) throws WorkflowExecutionException, IOException
	{
		context.getLogger().addInfo("Executing model transformation...", this);

		/*
		 * Get a TGGEngine. If TGG Engine slot name is provided, look if it
		 * contains a TGGEngine. If not, create a new one and put it into that
		 * slot.
		 */
		TGGEngine tggEngine = null;

		if (getTggEngineSlotName() != null && !"".equals(getTggEngineSlotName()))
		{
			Object o = context.getModelSlots().get(getTggEngineSlotName());

			if (o != null)
			{
				if (o instanceof TGGEngine)
				{
					tggEngine = (TGGEngine) o;
				}
				else
				{
					throw new WorkflowExecutionException("TGGEngine model slot '" + getTggEngineSlotName()
							+ "' does not contain a TGGEngine, but '" + o + "'.");
				}
			}
			else
			{
				tggEngine = MoteFactory.eINSTANCE.createTGGEngine();
				context.getModelSlots().put(getTggEngineSlotName(), tggEngine);
			}
		}
		else
		{
			tggEngine = MoteFactory.eINSTANCE.createTGGEngine();
		}

		/*
		 * Get source and target model resources
		 */
		EObject sourceModel = (null);

		Resource sourceResource = null;

		Resource targetResource = null;

		EObject targetModel = null;

		/*
		 * Get transformation direction
		 */
		TransformationDirection direction = null;

		if (FORWARD.equals(getTransformationDirection()))
		{
			direction = TransformationDirection.FORWARD;
			sourceModel = (EObject) context.getModelSlots().get(getSourceModelSlot());
			targetResource = context.getGlobalResourceSet().createResource(URI.createURI(getTargetModelSlot()));

			context.getLogger().addInfo(
					"Transforming '" + getSourceModelSlot() + "' to '" + getTargetModelSlot() + "' (forward transformation)...", this);
		}
		else if (REVERSE.equals(getTransformationDirection()))
		{
			direction = TransformationDirection.REVERSE;
			targetModel = (EObject) context.getModelSlots().get(getTargetModelSlot());
			sourceResource = context.getGlobalResourceSet().createResource(URI.createURI(getSourceModelSlot()));

			context.getLogger().addInfo(
					"Transforming '" + getTargetModelSlot() + "' to '" + getSourceModelSlot() + "' (reverse transformation)...", this);
		}
		else if (MAPPING.equals(getTransformationDirection()))
		{
			direction = TransformationDirection.MAPPING;
			
			targetModel = (EObject) context.getModelSlots().get(getTargetModelSlot());
			sourceModel = (EObject) context.getModelSlots().get(getSourceModelSlot());

			context.getLogger().addInfo("Executing mapping (integration) transformation with source model slot'" + getTargetModelSlot() + "' and target model slot '" + getSourceModelSlot() + "'...", this);
		}
		else
		{
			context.getLogger().addError("Unknown transformation direction :'" + getTransformationDirection() + "'.", null, this);

			throw new WorkflowExecutionException("Unknown transformation direction :'" + getTransformationDirection() + "'.");
		}

		/*
		 * Execute transformation
		 */
		try
		{
			if (FORWARD.equals(getTransformationDirection()))
			{

				executeTransformation(tggEngine, sourceModel.eResource(), targetResource, direction, getRuleSetID(),
						Boolean.parseBoolean(getSynchronize()), context.getLogger().getIProgressMonitor());

				context.getModelSlots().put(getTargetModelSlot(), targetResource.getContents().get(0));

			}
			else if (REVERSE.equals(getTransformationDirection()))
			{
				executeTransformation(tggEngine, sourceResource, targetModel.eResource(), direction, getRuleSetID(),
						Boolean.parseBoolean(getSynchronize()), context.getLogger().getIProgressMonitor());

				context.getModelSlots().put(getSourceModelSlot(), sourceResource.getContents().get(0));
			}
			else if (MAPPING.equals(getTransformationDirection()))
			{
				executeTransformation(tggEngine, sourceModel.eResource(), targetModel.eResource(), direction, getRuleSetID(),
						Boolean.parseBoolean(getSynchronize()), context.getLogger().getIProgressMonitor());
			}
			else
			{
				context.getLogger().addError("Unknown transformation direction :'" + getTransformationDirection() + "'.", null, this);
				throw new WorkflowExecutionException("Unknown transformation direction :'" + getTransformationDirection() + "'.");
			}
		}
		catch (TransformationException e)
		{
			context.getLogger().addError("Error during transformation.", e, this);

			throw new WorkflowExecutionException("Error during transformation.", e);
		}
	}

	/**
	 * Executes the model transformation
	 * 
	 * @param tggEngine
	 * @param sourceResource
	 * @param targetResource
	 * @param direction
	 * @param ruleSetID
	 * @param synchronize
	 * @throws TransformationException
	 */
	protected void executeTransformation(TGGEngine tggEngine, Resource sourceResource, Resource targetResource,
			TransformationDirection direction, String ruleSetID, boolean synchronize, IProgressMonitor monitor)
			throws TransformationException
	{
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		
														// TODO: Handle created dependencies
		tggEngine.transform(sourceResource, targetResource, new BasicEMap<Resource, EList<EObject>>(), direction, ruleSetID, synchronize, subMonitor);

	}
} // MoteTransformerImpl
