/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents;

import org.eclipse.emf.common.util.EList;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Model Generator</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getAxiomGeneratorActivity
 * <em>Axiom Generator Activity</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getRuleGeneratorActivities
 * <em>Rule Generator Activities</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getCorrespondenceModelSize
 * <em>Correspondence Model Size</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getLeftModelSlot
 * <em>Left Model Slot</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getRightModelSlot
 * <em>Right Model Slot</em>}</li>
 * <li>{@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getLeftModelURI
 * <em>Left Model URI</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getRightModelURI
 * <em>Right Model URI</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getCorrNodeTypeRestrictions
 * <em>Corr Node Type Restrictions</em>}</li>
 * </ul>
 * </p>
 * 
 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelGenerator()
 * @model
 * @generated
 */
public interface ModelGenerator extends WorkflowComponent
{
	/**
	 * Returns the value of the '<em><b>Axiom Generator Activity</b></em>'
	 * containment reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Axiom Generator Activity</em>' containment
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Axiom Generator Activity</em>' containment
	 *         reference.
	 * @see #setAxiomGeneratorActivity(GeneratorActivity)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelGenerator_AxiomGeneratorActivity()
	 * @model containment="true" required="true"
	 * @generated
	 */
	GeneratorActivity getAxiomGeneratorActivity();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getAxiomGeneratorActivity
	 * <em>Axiom Generator Activity</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Axiom Generator Activity</em>'
	 *            containment reference.
	 * @see #getAxiomGeneratorActivity()
	 * @generated
	 */
	void setAxiomGeneratorActivity(GeneratorActivity value);

	/**
	 * Returns the value of the '<em><b>Rule Generator Activities</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.mote.workflowComponents.GeneratorActivity}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Generator Activities</em>' reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Rule Generator Activities</em>' containment
	 *         reference list.
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelGenerator_RuleGeneratorActivities()
	 * @model containment="true"
	 * @generated
	 */
	EList<GeneratorActivity> getRuleGeneratorActivities();

	/**
	 * Returns the value of the '<em><b>Correspondence Model Size</b></em>'
	 * attribute. The default value is <code>"100"</code>. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Correspondence Model Size</em>' attribute
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Correspondence Model Size</em>' attribute.
	 * @see #setCorrespondenceModelSize(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelGenerator_CorrespondenceModelSize()
	 * @model default="100" required="true"
	 * @generated
	 */
	String getCorrespondenceModelSize();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getCorrespondenceModelSize
	 * <em>Correspondence Model Size</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Correspondence Model Size</em>'
	 *            attribute.
	 * @see #getCorrespondenceModelSize()
	 * @generated
	 */
	void setCorrespondenceModelSize(String value);

	/**
	 * Returns the value of the '<em><b>Left Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Left Model Slot</em>' attribute.
	 * @see #setLeftModelSlot(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelGenerator_LeftModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getLeftModelSlot();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getLeftModelSlot
	 * <em>Left Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Left Model Slot</em>' attribute.
	 * @see #getLeftModelSlot()
	 * @generated
	 */
	void setLeftModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Right Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Right Model Slot</em>' attribute.
	 * @see #setRightModelSlot(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelGenerator_RightModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getRightModelSlot();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getRightModelSlot
	 * <em>Right Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Right Model Slot</em>' attribute.
	 * @see #getRightModelSlot()
	 * @generated
	 */
	void setRightModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Left Model URI</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Model URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Left Model URI</em>' attribute.
	 * @see #setLeftModelURI(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelGenerator_LeftModelURI()
	 * @model required="true"
	 * @generated
	 */
	String getLeftModelURI();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getLeftModelURI
	 * <em>Left Model URI</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Left Model URI</em>' attribute.
	 * @see #getLeftModelURI()
	 * @generated
	 */
	void setLeftModelURI(String value);

	/**
	 * Returns the value of the '<em><b>Right Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Model URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Right Model URI</em>' attribute.
	 * @see #setRightModelURI(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelGenerator_RightModelURI()
	 * @model required="true"
	 * @generated
	 */
	String getRightModelURI();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.ModelGenerator#getRightModelURI
	 * <em>Right Model URI</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Right Model URI</em>' attribute.
	 * @see #getRightModelURI()
	 * @generated
	 */
	void setRightModelURI(String value);

	/**
	 * Returns the value of the '<em><b>Corr Node Type Restrictions</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corr Node Type Restrictions</em>' containment
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Corr Node Type Restrictions</em>'
	 *         containment reference list.
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getModelGenerator_CorrNodeTypeRestrictions()
	 * @model containment="true"
	 * @generated
	 */
	EList<CorrNodeTypeRestriction> getCorrNodeTypeRestrictions();

} // ModelGenerator
