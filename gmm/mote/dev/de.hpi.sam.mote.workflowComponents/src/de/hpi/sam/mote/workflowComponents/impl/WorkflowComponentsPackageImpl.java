/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction;
import de.hpi.sam.mote.workflowComponents.GeneratorActivity;
import de.hpi.sam.mote.workflowComponents.ModelComparer;
import de.hpi.sam.mote.workflowComponents.ModelGenerator;
import de.hpi.sam.mote.workflowComponents.MoteTransformer;
import de.hpi.sam.mote.workflowComponents.MoteTransformerMapping;
import de.hpi.sam.mote.workflowComponents.Parameter;
import de.hpi.sam.mote.workflowComponents.WorkflowComponentsFactory;
import de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.mdelab.workflow.WorkflowPackage;
import de.mdelab.workflow.components.ComponentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class WorkflowComponentsPackageImpl extends EPackageImpl implements
		WorkflowComponentsPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelGeneratorEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass generatorActivityEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrNodeTypeRestrictionEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moteTransformerEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelComparerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moteTransformerMappingEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorkflowComponentsPackageImpl() {
		super(eNS_URI, WorkflowComponentsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link WorkflowComponentsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorkflowComponentsPackage init() {
		if (isInited)
			return (WorkflowComponentsPackage) EPackage.Registry.INSTANCE
					.getEPackage(WorkflowComponentsPackage.eNS_URI);

		// Obtain or create and register package
		WorkflowComponentsPackageImpl theWorkflowComponentsPackage = (WorkflowComponentsPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof WorkflowComponentsPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new WorkflowComponentsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		StoryDiagramEcorePackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorkflowComponentsPackage.createPackageContents();

		// Initialize created meta-data
		theWorkflowComponentsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWorkflowComponentsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorkflowComponentsPackage.eNS_URI,
				theWorkflowComponentsPackage);
		return theWorkflowComponentsPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelGenerator() {
		return modelGeneratorEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelGenerator_AxiomGeneratorActivity() {
		return (EReference) modelGeneratorEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelGenerator_RuleGeneratorActivities() {
		return (EReference) modelGeneratorEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelGenerator_CorrespondenceModelSize() {
		return (EAttribute) modelGeneratorEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelGenerator_LeftModelSlot() {
		return (EAttribute) modelGeneratorEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelGenerator_RightModelSlot() {
		return (EAttribute) modelGeneratorEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelGenerator_LeftModelURI() {
		return (EAttribute) modelGeneratorEClass.getEStructuralFeatures()
				.get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelGenerator_RightModelURI() {
		return (EAttribute) modelGeneratorEClass.getEStructuralFeatures()
				.get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelGenerator_CorrNodeTypeRestrictions() {
		return (EReference) modelGeneratorEClass.getEStructuralFeatures()
				.get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGeneratorActivity() {
		return generatorActivityEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGeneratorActivity_Activity() {
		return (EReference) generatorActivityEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGeneratorActivity_RuleParameters() {
		return (EReference) generatorActivityEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGeneratorActivity_ParentCorrNodeParameters() {
		return (EReference) generatorActivityEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGeneratorActivity_CreatedCorrNodeType() {
		return (EReference) generatorActivityEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameter() {
		return parameterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameter_Name() {
		return (EAttribute) parameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameter_Type() {
		return (EReference) parameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameter_BaseValue() {
		return (EAttribute) parameterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrNodeTypeRestriction() {
		return corrNodeTypeRestrictionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCorrNodeTypeRestriction_CorrNodeType() {
		return (EReference) corrNodeTypeRestrictionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCorrNodeTypeRestriction_MaxNumber() {
		return (EAttribute) corrNodeTypeRestrictionEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCorrNodeTypeRestriction_MaxChildNodes() {
		return (EAttribute) corrNodeTypeRestrictionEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMoteTransformer() {
		return moteTransformerEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMoteTransformer_SourceModelSlot() {
		return (EAttribute) moteTransformerEClass.getEStructuralFeatures().get(
				0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMoteTransformer_TargetModelSlot() {
		return (EAttribute) moteTransformerEClass.getEStructuralFeatures().get(
				1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMoteTransformer_RuleSetID() {
		return (EAttribute) moteTransformerEClass.getEStructuralFeatures().get(
				2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMoteTransformer_Synchronize() {
		return (EAttribute) moteTransformerEClass.getEStructuralFeatures().get(
				3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMoteTransformer_TransformationDirection() {
		return (EAttribute) moteTransformerEClass.getEStructuralFeatures().get(
				4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMoteTransformer_TggEngineSlotName() {
		return (EAttribute) moteTransformerEClass.getEStructuralFeatures().get(
				5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelComparer() {
		return modelComparerEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelComparer_ModelSlot1() {
		return (EAttribute) modelComparerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelComparer_ModelSlot2() {
		return (EAttribute) modelComparerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelComparer_ComparisonResultModelSlot() {
		return (EAttribute) modelComparerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelComparer_DiffModelSlot() {
		return (EAttribute) modelComparerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMoteTransformerMapping() {
		return moteTransformerMappingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMoteTransformerMapping_CorrespondenceModelSlot() {
		return (EAttribute) moteTransformerMappingEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowComponentsFactory getWorkflowComponentsFactory() {
		return (WorkflowComponentsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		modelGeneratorEClass = createEClass(MODEL_GENERATOR);
		createEReference(modelGeneratorEClass,
				MODEL_GENERATOR__AXIOM_GENERATOR_ACTIVITY);
		createEReference(modelGeneratorEClass,
				MODEL_GENERATOR__RULE_GENERATOR_ACTIVITIES);
		createEAttribute(modelGeneratorEClass,
				MODEL_GENERATOR__CORRESPONDENCE_MODEL_SIZE);
		createEAttribute(modelGeneratorEClass, MODEL_GENERATOR__LEFT_MODEL_SLOT);
		createEAttribute(modelGeneratorEClass,
				MODEL_GENERATOR__RIGHT_MODEL_SLOT);
		createEAttribute(modelGeneratorEClass, MODEL_GENERATOR__LEFT_MODEL_URI);
		createEAttribute(modelGeneratorEClass, MODEL_GENERATOR__RIGHT_MODEL_URI);
		createEReference(modelGeneratorEClass,
				MODEL_GENERATOR__CORR_NODE_TYPE_RESTRICTIONS);

		generatorActivityEClass = createEClass(GENERATOR_ACTIVITY);
		createEReference(generatorActivityEClass, GENERATOR_ACTIVITY__ACTIVITY);
		createEReference(generatorActivityEClass,
				GENERATOR_ACTIVITY__RULE_PARAMETERS);
		createEReference(generatorActivityEClass,
				GENERATOR_ACTIVITY__PARENT_CORR_NODE_PARAMETERS);
		createEReference(generatorActivityEClass,
				GENERATOR_ACTIVITY__CREATED_CORR_NODE_TYPE);

		parameterEClass = createEClass(PARAMETER);
		createEAttribute(parameterEClass, PARAMETER__NAME);
		createEReference(parameterEClass, PARAMETER__TYPE);
		createEAttribute(parameterEClass, PARAMETER__BASE_VALUE);

		corrNodeTypeRestrictionEClass = createEClass(CORR_NODE_TYPE_RESTRICTION);
		createEReference(corrNodeTypeRestrictionEClass,
				CORR_NODE_TYPE_RESTRICTION__CORR_NODE_TYPE);
		createEAttribute(corrNodeTypeRestrictionEClass,
				CORR_NODE_TYPE_RESTRICTION__MAX_NUMBER);
		createEAttribute(corrNodeTypeRestrictionEClass,
				CORR_NODE_TYPE_RESTRICTION__MAX_CHILD_NODES);

		moteTransformerEClass = createEClass(MOTE_TRANSFORMER);
		createEAttribute(moteTransformerEClass,
				MOTE_TRANSFORMER__SOURCE_MODEL_SLOT);
		createEAttribute(moteTransformerEClass,
				MOTE_TRANSFORMER__TARGET_MODEL_SLOT);
		createEAttribute(moteTransformerEClass, MOTE_TRANSFORMER__RULE_SET_ID);
		createEAttribute(moteTransformerEClass, MOTE_TRANSFORMER__SYNCHRONIZE);
		createEAttribute(moteTransformerEClass,
				MOTE_TRANSFORMER__TRANSFORMATION_DIRECTION);
		createEAttribute(moteTransformerEClass,
				MOTE_TRANSFORMER__TGG_ENGINE_SLOT_NAME);

		modelComparerEClass = createEClass(MODEL_COMPARER);
		createEAttribute(modelComparerEClass, MODEL_COMPARER__MODEL_SLOT1);
		createEAttribute(modelComparerEClass, MODEL_COMPARER__MODEL_SLOT2);
		createEAttribute(modelComparerEClass,
				MODEL_COMPARER__COMPARISON_RESULT_MODEL_SLOT);
		createEAttribute(modelComparerEClass, MODEL_COMPARER__DIFF_MODEL_SLOT);

		moteTransformerMappingEClass = createEClass(MOTE_TRANSFORMER_MAPPING);
		createEAttribute(moteTransformerMappingEClass,
				MOTE_TRANSFORMER_MAPPING__CORRESPONDENCE_MODEL_SLOT);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ComponentsPackage theComponentsPackage = (ComponentsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ComponentsPackage.eNS_URI);
		StoryDiagramEcorePackage theStoryDiagramEcorePackage = (StoryDiagramEcorePackage) EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		modelGeneratorEClass.getESuperTypes().add(
				theComponentsPackage.getWorkflowComponent());
		moteTransformerEClass.getESuperTypes().add(
				theComponentsPackage.getWorkflowComponent());
		modelComparerEClass.getESuperTypes().add(
				theComponentsPackage.getWorkflowComponent());
		moteTransformerMappingEClass.getESuperTypes().add(
				this.getMoteTransformer());

		// Initialize classes and features; add operations and parameters
		initEClass(modelGeneratorEClass, ModelGenerator.class,
				"ModelGenerator", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModelGenerator_AxiomGeneratorActivity(),
				this.getGeneratorActivity(), null, "axiomGeneratorActivity",
				null, 1, 1, ModelGenerator.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModelGenerator_RuleGeneratorActivities(),
				this.getGeneratorActivity(), null, "ruleGeneratorActivities",
				null, 0, -1, ModelGenerator.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModelGenerator_CorrespondenceModelSize(),
				ecorePackage.getEString(), "correspondenceModelSize", "100", 1,
				1, ModelGenerator.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getModelGenerator_LeftModelSlot(),
				ecorePackage.getEString(), "leftModelSlot", null, 1, 1,
				ModelGenerator.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getModelGenerator_RightModelSlot(),
				ecorePackage.getEString(), "rightModelSlot", null, 1, 1,
				ModelGenerator.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getModelGenerator_LeftModelURI(),
				ecorePackage.getEString(), "leftModelURI", null, 1, 1,
				ModelGenerator.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getModelGenerator_RightModelURI(),
				ecorePackage.getEString(), "rightModelURI", null, 1, 1,
				ModelGenerator.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getModelGenerator_CorrNodeTypeRestrictions(),
				this.getCorrNodeTypeRestriction(), null,
				"corrNodeTypeRestrictions", null, 0, -1, ModelGenerator.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(generatorActivityEClass, GeneratorActivity.class,
				"GeneratorActivity", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGeneratorActivity_Activity(),
				theStoryDiagramEcorePackage.getActivity(), null, "activity",
				null, 1, 1, GeneratorActivity.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGeneratorActivity_RuleParameters(),
				this.getParameter(), null, "ruleParameters", null, 0, -1,
				GeneratorActivity.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGeneratorActivity_ParentCorrNodeParameters(),
				this.getParameter(), null, "parentCorrNodeParameters", null, 0,
				-1, GeneratorActivity.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGeneratorActivity_CreatedCorrNodeType(),
				ecorePackage.getEClass(), null, "createdCorrNodeType", null, 1,
				1, GeneratorActivity.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(parameterEClass, Parameter.class, "Parameter", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getParameter_Name(), ecorePackage.getEString(), "name",
				null, 1, 1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getParameter_Type(), ecorePackage.getEClassifier(),
				null, "type", null, 1, 1, Parameter.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getParameter_BaseValue(), ecorePackage.getEString(),
				"baseValue", null, 0, 1, Parameter.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(corrNodeTypeRestrictionEClass,
				CorrNodeTypeRestriction.class, "CorrNodeTypeRestriction",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCorrNodeTypeRestriction_CorrNodeType(),
				ecorePackage.getEClass(), null, "corrNodeType", null, 1, 1,
				CorrNodeTypeRestriction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCorrNodeTypeRestriction_MaxNumber(),
				ecorePackage.getEString(), "maxNumber", "2147483647", 0, 1,
				CorrNodeTypeRestriction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getCorrNodeTypeRestriction_MaxChildNodes(),
				ecorePackage.getEString(), "maxChildNodes", "2147483647", 0, 1,
				CorrNodeTypeRestriction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(moteTransformerEClass, MoteTransformer.class,
				"MoteTransformer", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMoteTransformer_SourceModelSlot(),
				ecorePackage.getEString(), "sourceModelSlot", null, 1, 1,
				MoteTransformer.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMoteTransformer_TargetModelSlot(),
				ecorePackage.getEString(), "targetModelSlot", null, 1, 1,
				MoteTransformer.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMoteTransformer_RuleSetID(),
				ecorePackage.getEString(), "ruleSetID", null, 1, 1,
				MoteTransformer.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMoteTransformer_Synchronize(),
				ecorePackage.getEString(), "synchronize", null, 1, 1,
				MoteTransformer.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMoteTransformer_TransformationDirection(),
				ecorePackage.getEString(), "transformationDirection", null, 1,
				1, MoteTransformer.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMoteTransformer_TggEngineSlotName(),
				ecorePackage.getEString(), "tggEngineSlotName", null, 0, 1,
				MoteTransformer.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(modelComparerEClass, ModelComparer.class, "ModelComparer",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getModelComparer_ModelSlot1(),
				ecorePackage.getEString(), "modelSlot1", null, 1, 1,
				ModelComparer.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getModelComparer_ModelSlot2(),
				ecorePackage.getEString(), "modelSlot2", null, 1, 1,
				ModelComparer.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getModelComparer_ComparisonResultModelSlot(),
				ecorePackage.getEString(), "comparisonResultModelSlot", null,
				1, 1, ModelComparer.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getModelComparer_DiffModelSlot(),
				ecorePackage.getEString(), "diffModelSlot", null, 1, 1,
				ModelComparer.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(moteTransformerMappingEClass, MoteTransformerMapping.class,
				"MoteTransformerMapping", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMoteTransformerMapping_CorrespondenceModelSlot(),
				ecorePackage.getEString(), "correspondenceModelSlot", null, 1,
				1, MoteTransformerMapping.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} // WorkflowComponentsPackageImpl
