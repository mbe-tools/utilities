/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents.impl;

import java.io.IOException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.hpi.sam.mote.TGGEngine;
import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.helpers.RuleSetTag;
import de.hpi.sam.mote.workflowComponents.MoteTransformerMapping;
import de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mote Transformer Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.mote.workflowComponents.impl.MoteTransformerMappingImpl#getCorrespondenceModelSlot <em>Correspondence Model Slot</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MoteTransformerMappingImpl extends MoteTransformerImpl implements
		MoteTransformerMapping {
	/**
	 * The default value of the '{@link #getCorrespondenceModelSlot() <em>Correspondence Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondenceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String CORRESPONDENCE_MODEL_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCorrespondenceModelSlot() <em>Correspondence Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondenceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String correspondenceModelSlot = CORRESPONDENCE_MODEL_SLOT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MoteTransformerMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowComponentsPackage.Literals.MOTE_TRANSFORMER_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCorrespondenceModelSlot() {
		return correspondenceModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCorrespondenceModelSlot(String newCorrespondenceModelSlot) {
		String oldCorrespondenceModelSlot = correspondenceModelSlot;
		correspondenceModelSlot = newCorrespondenceModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					WorkflowComponentsPackage.MOTE_TRANSFORMER_MAPPING__CORRESPONDENCE_MODEL_SLOT,
					oldCorrespondenceModelSlot, correspondenceModelSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case WorkflowComponentsPackage.MOTE_TRANSFORMER_MAPPING__CORRESPONDENCE_MODEL_SLOT:
			return getCorrespondenceModelSlot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case WorkflowComponentsPackage.MOTE_TRANSFORMER_MAPPING__CORRESPONDENCE_MODEL_SLOT:
			setCorrespondenceModelSlot((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case WorkflowComponentsPackage.MOTE_TRANSFORMER_MAPPING__CORRESPONDENCE_MODEL_SLOT:
			setCorrespondenceModelSlot(CORRESPONDENCE_MODEL_SLOT_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case WorkflowComponentsPackage.MOTE_TRANSFORMER_MAPPING__CORRESPONDENCE_MODEL_SLOT:
			return CORRESPONDENCE_MODEL_SLOT_EDEFAULT == null ? correspondenceModelSlot != null
					: !CORRESPONDENCE_MODEL_SLOT_EDEFAULT
							.equals(correspondenceModelSlot);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (correspondenceModelSlot: ");
		result.append(correspondenceModelSlot);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public void execute(WorkflowExecutionContext context) throws WorkflowExecutionException, IOException {
		TGGEngine engine = null;
		
		//If TGG engine slot is not set, set it
		if (!(getTggEngineSlotName() != null && !"".equals(getTggEngineSlotName()))) {
			this.setTggEngineSlotName("tggEngineSlot");
		}
		
		super.execute(context);
		
		//Get TGG engine
		Object o = context.getModelSlots().get(this.getTggEngineSlotName());
		
		if (engine instanceof TGGEngine) 
			engine = (TGGEngine) o;
		else 
			throw new WorkflowExecutionException("No TGG engine found in modelslot " + this.getTggEngineSlotName());
		
		//Find adequate RuleSetTag list
		EMap<String, EList<RuleSetTag>> usedRuleSets = engine.getUsedRuleSets();
		EList<RuleSetTag> list = usedRuleSets.get(this.getRuleSetID());
		
		Object sourceModel = context.getModelSlots().get(this.sourceModelSlot);
		Object targetModel = context.getModelSlots().get(this.targetModelSlot);
		
		RuleSetTag tag = null;
		
		for (RuleSetTag currTag : list) {
			if (currTag.getRuleSet().getSourceModelRootNode() == sourceModel 
					&& currTag.getRuleSet().getTargetModelRootNode() == targetModel) {
				tag = currTag;
				break;
			}
		}
		
		if (tag == null) {
			throw new WorkflowExecutionException("No corresponding RuleSetTag found.");
		}

		//Deep copy the correspondence node list
		Collection<TGGNode> corrNodes = EcoreUtil.copyAll(tag.getRuleSet().getCorrespondenceNodes().keySet());

		for (TGGNode corrNode : corrNodes) {
			corrNode.setRuleSet(null);
			corrNode.setCreationRule(null);
		}		

		context.getModelSlots().put(this.correspondenceModelSlot, corrNodes);
	}

} //MoteTransformerMappingImpl
