/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents.util;

import de.hpi.sam.mote.workflowComponents.*;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction;
import de.hpi.sam.mote.workflowComponents.GeneratorActivity;
import de.hpi.sam.mote.workflowComponents.ModelComparer;
import de.hpi.sam.mote.workflowComponents.ModelGenerator;
import de.hpi.sam.mote.workflowComponents.MoteTransformer;
import de.hpi.sam.mote.workflowComponents.Parameter;
import de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage;
import de.mdelab.workflow.NamedComponent;
import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides
 * an adapter <code>createXXX</code> method for each class of the model. <!--
 * end-user-doc -->
 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage
 * @generated
 */
public class WorkflowComponentsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static WorkflowComponentsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public WorkflowComponentsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = WorkflowComponentsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc --> This implementation returns <code>true</code> if
	 * the object is either the model's package or is an instance object of the
	 * model. <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected WorkflowComponentsSwitch<Adapter> modelSwitch = new WorkflowComponentsSwitch<Adapter>() {
		@Override
		public Adapter caseModelGenerator(ModelGenerator object) {
			return createModelGeneratorAdapter();
		}

		@Override
		public Adapter caseGeneratorActivity(GeneratorActivity object) {
			return createGeneratorActivityAdapter();
		}

		@Override
		public Adapter caseParameter(Parameter object) {
			return createParameterAdapter();
		}

		@Override
		public Adapter caseCorrNodeTypeRestriction(
				CorrNodeTypeRestriction object) {
			return createCorrNodeTypeRestrictionAdapter();
		}

		@Override
		public Adapter caseMoteTransformer(MoteTransformer object) {
			return createMoteTransformerAdapter();
		}

		@Override
		public Adapter caseModelComparer(ModelComparer object) {
			return createModelComparerAdapter();
		}

		@Override
		public Adapter caseMoteTransformerMapping(MoteTransformerMapping object) {
			return createMoteTransformerMappingAdapter();
		}

		@Override
		public Adapter caseNamedComponent(NamedComponent object) {
			return createNamedComponentAdapter();
		}

		@Override
		public Adapter caseWorkflowComponent(WorkflowComponent object) {
			return createWorkflowComponentAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.mote.workflowComponents.ModelGenerator <em>Model Generator</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.mote.workflowComponents.ModelGenerator
	 * @generated
	 */
	public Adapter createModelGeneratorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.mote.workflowComponents.GeneratorActivity <em>Generator Activity</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.mote.workflowComponents.GeneratorActivity
	 * @generated
	 */
	public Adapter createGeneratorActivityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.mote.workflowComponents.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that
	 * we can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.mote.workflowComponents.Parameter
	 * @generated
	 */
	public Adapter createParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction <em>Corr Node Type Restriction</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction
	 * @generated
	 */
	public Adapter createCorrNodeTypeRestrictionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.mote.workflowComponents.MoteTransformer <em>Mote Transformer</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.mote.workflowComponents.MoteTransformer
	 * @generated
	 */
	public Adapter createMoteTransformerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.mote.workflowComponents.ModelComparer <em>Model Comparer</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.mote.workflowComponents.ModelComparer
	 * @generated
	 */
	public Adapter createModelComparerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.mote.workflowComponents.MoteTransformerMapping <em>Mote Transformer Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.mote.workflowComponents.MoteTransformerMapping
	 * @generated
	 */
	public Adapter createMoteTransformerMappingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link de.mdelab.workflow.NamedComponent <em>Named Component</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we
	 * can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see de.mdelab.workflow.NamedComponent
	 * @generated
	 */
	public Adapter createNamedComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.workflow.components.WorkflowComponent <em>Workflow Component</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.workflow.components.WorkflowComponent
	 * @generated
	 */
	public Adapter createWorkflowComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} // WorkflowComponentsAdapterFactory
