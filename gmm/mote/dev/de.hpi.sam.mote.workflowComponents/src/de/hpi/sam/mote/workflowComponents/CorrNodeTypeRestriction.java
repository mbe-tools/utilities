/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Corr Node Type Restriction</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction#getCorrNodeType
 * <em>Corr Node Type</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction#getMaxNumber
 * <em>Max Number</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction#getMaxChildNodes
 * <em>Max Child Nodes</em>}</li>
 * </ul>
 * </p>
 * 
 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getCorrNodeTypeRestriction()
 * @model
 * @generated
 */
public interface CorrNodeTypeRestriction extends EObject
{
	/**
	 * Returns the value of the '<em><b>Corr Node Type</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corr Node Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Corr Node Type</em>' reference.
	 * @see #setCorrNodeType(EClass)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getCorrNodeTypeRestriction_CorrNodeType()
	 * @model required="true"
	 * @generated
	 */
	EClass getCorrNodeType();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction#getCorrNodeType
	 * <em>Corr Node Type</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Corr Node Type</em>' reference.
	 * @see #getCorrNodeType()
	 * @generated
	 */
	void setCorrNodeType(EClass value);

	/**
	 * Returns the value of the '<em><b>Max Number</b></em>' attribute. The
	 * default value is <code>"2147483647"</code>. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Number</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Max Number</em>' attribute.
	 * @see #setMaxNumber(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getCorrNodeTypeRestriction_MaxNumber()
	 * @model default="2147483647"
	 * @generated
	 */
	String getMaxNumber();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction#getMaxNumber
	 * <em>Max Number</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Max Number</em>' attribute.
	 * @see #getMaxNumber()
	 * @generated
	 */
	void setMaxNumber(String value);

	/**
	 * Returns the value of the '<em><b>Max Child Nodes</b></em>' attribute. The
	 * default value is <code>"2147483647"</code>. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Child Nodes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Max Child Nodes</em>' attribute.
	 * @see #setMaxChildNodes(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getCorrNodeTypeRestriction_MaxChildNodes()
	 * @model default="2147483647"
	 * @generated
	 */
	String getMaxChildNodes();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction#getMaxChildNodes
	 * <em>Max Child Nodes</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Max Child Nodes</em>' attribute.
	 * @see #getMaxChildNodes()
	 * @generated
	 */
	void setMaxChildNodes(String value);

} // CorrNodeTypeRestriction
