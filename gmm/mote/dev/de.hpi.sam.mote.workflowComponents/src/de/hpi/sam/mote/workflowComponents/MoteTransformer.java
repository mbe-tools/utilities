/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Mote Transformer</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getSourceModelSlot
 * <em>Source Model Slot</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getTargetModelSlot
 * <em>Target Model Slot</em>}</li>
 * <li>{@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getRuleSetID
 * <em>Rule Set ID</em>}</li>
 * <li>{@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getSynchronize
 * <em>Synchronize</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getTransformationDirection
 * <em>Transformation Direction</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getTggEngineSlotName
 * <em>Tgg Engine Slot Name</em>}</li>
 * </ul>
 * </p>
 * 
 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getMoteTransformer()
 * @model
 * @generated
 */
public interface MoteTransformer extends WorkflowComponent
{
	/**
	 * Returns the value of the '<em><b>Source Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Source Model Slot</em>' attribute.
	 * @see #setSourceModelSlot(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getMoteTransformer_SourceModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getSourceModelSlot();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getSourceModelSlot
	 * <em>Source Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Source Model Slot</em>' attribute.
	 * @see #getSourceModelSlot()
	 * @generated
	 */
	void setSourceModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Target Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target Model Slot</em>' attribute.
	 * @see #setTargetModelSlot(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getMoteTransformer_TargetModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getTargetModelSlot();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getTargetModelSlot
	 * <em>Target Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Target Model Slot</em>' attribute.
	 * @see #getTargetModelSlot()
	 * @generated
	 */
	void setTargetModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Rule Set ID</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Set ID</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Rule Set ID</em>' attribute.
	 * @see #setRuleSetID(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getMoteTransformer_RuleSetID()
	 * @model required="true"
	 * @generated
	 */
	String getRuleSetID();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getRuleSetID
	 * <em>Rule Set ID</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Rule Set ID</em>' attribute.
	 * @see #getRuleSetID()
	 * @generated
	 */
	void setRuleSetID(String value);

	/**
	 * Returns the value of the '<em><b>Synchronize</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synchronize</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Synchronize</em>' attribute.
	 * @see #setSynchronize(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getMoteTransformer_Synchronize()
	 * @model required="true"
	 * @generated
	 */
	String getSynchronize();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getSynchronize
	 * <em>Synchronize</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Synchronize</em>' attribute.
	 * @see #getSynchronize()
	 * @generated
	 */
	void setSynchronize(String value);

	/**
	 * Returns the value of the '<em><b>Transformation Direction</b></em>'
	 * attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transformation Direction</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Transformation Direction</em>' attribute.
	 * @see #setTransformationDirection(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getMoteTransformer_TransformationDirection()
	 * @model required="true"
	 * @generated
	 */
	String getTransformationDirection();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getTransformationDirection
	 * <em>Transformation Direction</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Transformation Direction</em>'
	 *            attribute.
	 * @see #getTransformationDirection()
	 * @generated
	 */
	void setTransformationDirection(String value);

	/**
	 * Returns the value of the '<em><b>Tgg Engine Slot Name</b></em>'
	 * attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgg Engine Slot Name</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Tgg Engine Slot Name</em>' attribute.
	 * @see #setTggEngineSlotName(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getMoteTransformer_TggEngineSlotName()
	 * @model
	 * @generated
	 */
	String getTggEngineSlotName();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.MoteTransformer#getTggEngineSlotName
	 * <em>Tgg Engine Slot Name</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Tgg Engine Slot Name</em>'
	 *            attribute.
	 * @see #getTggEngineSlotName()
	 * @generated
	 */
	void setTggEngineSlotName(String value);

} // MoteTransformer
