/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.storyDiagramEcore.Activity;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Generator Activity</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link de.hpi.sam.mote.workflowComponents.GeneratorActivity#getActivity
 * <em>Activity</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.GeneratorActivity#getRuleParameters
 * <em>Rule Parameters</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.GeneratorActivity#getParentCorrNodeParameters
 * <em>Parent Corr Node Parameters</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.GeneratorActivity#getCreatedCorrNodeType
 * <em>Created Corr Node Type</em>}</li>
 * </ul>
 * </p>
 * 
 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getGeneratorActivity()
 * @model
 * @generated
 */
public interface GeneratorActivity extends EObject
{
	/**
	 * Returns the value of the '<em><b>Activity</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Activity</em>' reference.
	 * @see #setActivity(Activity)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getGeneratorActivity_Activity()
	 * @model required="true"
	 * @generated
	 */
	Activity getActivity();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.GeneratorActivity#getActivity
	 * <em>Activity</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Activity</em>' reference.
	 * @see #getActivity()
	 * @generated
	 */
	void setActivity(Activity value);

	/**
	 * Returns the value of the '<em><b>Rule Parameters</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link de.hpi.sam.mote.workflowComponents.Parameter}. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Rule Parameters</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Rule Parameters</em>' containment reference
	 *         list.
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getGeneratorActivity_RuleParameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Parameter> getRuleParameters();

	/**
	 * Returns the value of the '<em><b>Parent Corr Node Parameters</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.mote.workflowComponents.Parameter}. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Parent Corr Node Parameters</em>' containment
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Corr Node Parameters</em>'
	 *         containment reference list.
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getGeneratorActivity_ParentCorrNodeParameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Parameter> getParentCorrNodeParameters();

	/**
	 * Returns the value of the '<em><b>Created Corr Node Type</b></em>'
	 * reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Created Corr Node Type</em>' reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Created Corr Node Type</em>' reference.
	 * @see #setCreatedCorrNodeType(EClass)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getGeneratorActivity_CreatedCorrNodeType()
	 * @model required="true"
	 * @generated
	 */
	EClass getCreatedCorrNodeType();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.GeneratorActivity#getCreatedCorrNodeType
	 * <em>Created Corr Node Type</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Created Corr Node Type</em>'
	 *            reference.
	 * @see #getCreatedCorrNodeType()
	 * @generated
	 */
	void setCreatedCorrNodeType(EClass value);

} // GeneratorActivity
