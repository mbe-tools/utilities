/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents.util;

import de.hpi.sam.mote.workflowComponents.*;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction;
import de.hpi.sam.mote.workflowComponents.GeneratorActivity;
import de.hpi.sam.mote.workflowComponents.ModelComparer;
import de.hpi.sam.mote.workflowComponents.ModelGenerator;
import de.hpi.sam.mote.workflowComponents.MoteTransformer;
import de.hpi.sam.mote.workflowComponents.Parameter;
import de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage;
import de.mdelab.workflow.NamedComponent;
import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance
 * hierarchy. It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the
 * inheritance hierarchy until a non-null result is returned, which is the
 * result of the switch. <!-- end-user-doc -->
 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage
 * @generated
 */
public class WorkflowComponentsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static WorkflowComponentsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public WorkflowComponentsSwitch() {
		if (modelPackage == null) {
			modelPackage = WorkflowComponentsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case WorkflowComponentsPackage.MODEL_GENERATOR: {
			ModelGenerator modelGenerator = (ModelGenerator) theEObject;
			T result = caseModelGenerator(modelGenerator);
			if (result == null)
				result = caseWorkflowComponent(modelGenerator);
			if (result == null)
				result = caseNamedComponent(modelGenerator);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WorkflowComponentsPackage.GENERATOR_ACTIVITY: {
			GeneratorActivity generatorActivity = (GeneratorActivity) theEObject;
			T result = caseGeneratorActivity(generatorActivity);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WorkflowComponentsPackage.PARAMETER: {
			Parameter parameter = (Parameter) theEObject;
			T result = caseParameter(parameter);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION: {
			CorrNodeTypeRestriction corrNodeTypeRestriction = (CorrNodeTypeRestriction) theEObject;
			T result = caseCorrNodeTypeRestriction(corrNodeTypeRestriction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WorkflowComponentsPackage.MOTE_TRANSFORMER: {
			MoteTransformer moteTransformer = (MoteTransformer) theEObject;
			T result = caseMoteTransformer(moteTransformer);
			if (result == null)
				result = caseWorkflowComponent(moteTransformer);
			if (result == null)
				result = caseNamedComponent(moteTransformer);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WorkflowComponentsPackage.MODEL_COMPARER: {
			ModelComparer modelComparer = (ModelComparer) theEObject;
			T result = caseModelComparer(modelComparer);
			if (result == null)
				result = caseWorkflowComponent(modelComparer);
			if (result == null)
				result = caseNamedComponent(modelComparer);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WorkflowComponentsPackage.MOTE_TRANSFORMER_MAPPING: {
			MoteTransformerMapping moteTransformerMapping = (MoteTransformerMapping) theEObject;
			T result = caseMoteTransformerMapping(moteTransformerMapping);
			if (result == null)
				result = caseMoteTransformer(moteTransformerMapping);
			if (result == null)
				result = caseWorkflowComponent(moteTransformerMapping);
			if (result == null)
				result = caseNamedComponent(moteTransformerMapping);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Generator</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Generator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelGenerator(ModelGenerator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generator Activity</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generator Activity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGeneratorActivity(GeneratorActivity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameter(Parameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Corr Node Type Restriction</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Corr Node Type Restriction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrNodeTypeRestriction(CorrNodeTypeRestriction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mote Transformer</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mote Transformer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMoteTransformer(MoteTransformer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Comparer</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Comparer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelComparer(ModelComparer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mote Transformer Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mote Transformer Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMoteTransformerMapping(MoteTransformerMapping object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Component</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedComponent(NamedComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Workflow Component</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Workflow Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorkflowComponent(WorkflowComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch, but this is
	 * the last case anyway. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} // WorkflowComponentsSwitch
