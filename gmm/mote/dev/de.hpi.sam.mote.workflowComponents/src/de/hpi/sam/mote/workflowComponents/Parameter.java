/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Parameter</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link de.hpi.sam.mote.workflowComponents.Parameter#getName <em>Name
 * </em>}</li>
 * <li>{@link de.hpi.sam.mote.workflowComponents.Parameter#getType <em>Type
 * </em>}</li>
 * <li>{@link de.hpi.sam.mote.workflowComponents.Parameter#getBaseValue <em>Base
 * Value</em>}</li>
 * </ul>
 * </p>
 * 
 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends EObject
{
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getParameter_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.Parameter#getName
	 * <em>Name</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(EClassifier)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getParameter_Type()
	 * @model required="true"
	 * @generated
	 */
	EClassifier getType();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.Parameter#getType
	 * <em>Type</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(EClassifier value);

	/**
	 * Returns the value of the '<em><b>Base Value</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Value</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Value</em>' attribute.
	 * @see #setBaseValue(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getParameter_BaseValue()
	 * @model
	 * @generated
	 */
	String getBaseValue();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.mote.workflowComponents.Parameter#getBaseValue
	 * <em>Base Value</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Value</em>' attribute.
	 * @see #getBaseValue()
	 * @generated
	 */
	void setBaseValue(String value);

} // Parameter
