/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import de.hpi.sam.mote.workflowComponents.CorrNodeTypeRestriction;
import de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Corr Node Type Restriction</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.CorrNodeTypeRestrictionImpl#getCorrNodeType
 * <em>Corr Node Type</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.CorrNodeTypeRestrictionImpl#getMaxNumber
 * <em>Max Number</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.CorrNodeTypeRestrictionImpl#getMaxChildNodes
 * <em>Max Child Nodes</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class CorrNodeTypeRestrictionImpl extends EObjectImpl implements CorrNodeTypeRestriction
{
	/**
	 * The cached value of the '{@link #getCorrNodeType()
	 * <em>Corr Node Type</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getCorrNodeType()
	 * @generated
	 * @ordered
	 */
	protected EClass				corrNodeType;

	/**
	 * The default value of the '{@link #getMaxNumber() <em>Max Number</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getMaxNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String	MAX_NUMBER_EDEFAULT			= "2147483647";

	/**
	 * The cached value of the '{@link #getMaxNumber() <em>Max Number</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getMaxNumber()
	 * @generated
	 * @ordered
	 */
	protected String				maxNumber					= MAX_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxChildNodes()
	 * <em>Max Child Nodes</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getMaxChildNodes()
	 * @generated
	 * @ordered
	 */
	protected static final String	MAX_CHILD_NODES_EDEFAULT	= "2147483647";

	/**
	 * The cached value of the '{@link #getMaxChildNodes()
	 * <em>Max Child Nodes</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getMaxChildNodes()
	 * @generated
	 * @ordered
	 */
	protected String				maxChildNodes				= MAX_CHILD_NODES_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected CorrNodeTypeRestrictionImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WorkflowComponentsPackage.Literals.CORR_NODE_TYPE_RESTRICTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getCorrNodeType()
	{
		if (corrNodeType != null && corrNodeType.eIsProxy())
		{
			InternalEObject oldCorrNodeType = (InternalEObject) corrNodeType;
			corrNodeType = (EClass) eResolveProxy(oldCorrNodeType);
			if (corrNodeType != oldCorrNodeType)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__CORR_NODE_TYPE, oldCorrNodeType, corrNodeType));
			}
		}
		return corrNodeType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass basicGetCorrNodeType()
	{
		return corrNodeType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setCorrNodeType(EClass newCorrNodeType)
	{
		EClass oldCorrNodeType = corrNodeType;
		corrNodeType = newCorrNodeType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__CORR_NODE_TYPE,
					oldCorrNodeType, corrNodeType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getMaxNumber()
	{
		return maxNumber;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setMaxNumber(String newMaxNumber)
	{
		String oldMaxNumber = maxNumber;
		maxNumber = newMaxNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__MAX_NUMBER,
					oldMaxNumber, maxNumber));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getMaxChildNodes()
	{
		return maxChildNodes;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setMaxChildNodes(String newMaxChildNodes)
	{
		String oldMaxChildNodes = maxChildNodes;
		maxChildNodes = newMaxChildNodes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__MAX_CHILD_NODES,
					oldMaxChildNodes, maxChildNodes));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__CORR_NODE_TYPE:
				if (resolve)
					return getCorrNodeType();
				return basicGetCorrNodeType();
			case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__MAX_NUMBER:
				return getMaxNumber();
			case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__MAX_CHILD_NODES:
				return getMaxChildNodes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__CORR_NODE_TYPE:
				setCorrNodeType((EClass) newValue);
				return;
			case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__MAX_NUMBER:
				setMaxNumber((String) newValue);
				return;
			case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__MAX_CHILD_NODES:
				setMaxChildNodes((String) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__CORR_NODE_TYPE:
				setCorrNodeType((EClass) null);
				return;
			case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__MAX_NUMBER:
				setMaxNumber(MAX_NUMBER_EDEFAULT);
				return;
			case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__MAX_CHILD_NODES:
				setMaxChildNodes(MAX_CHILD_NODES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__CORR_NODE_TYPE:
				return corrNodeType != null;
			case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__MAX_NUMBER:
				return MAX_NUMBER_EDEFAULT == null ? maxNumber != null : !MAX_NUMBER_EDEFAULT.equals(maxNumber);
			case WorkflowComponentsPackage.CORR_NODE_TYPE_RESTRICTION__MAX_CHILD_NODES:
				return MAX_CHILD_NODES_EDEFAULT == null ? maxChildNodes != null : !MAX_CHILD_NODES_EDEFAULT.equals(maxChildNodes);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (maxNumber: ");
		result.append(maxNumber);
		result.append(", maxChildNodes: ");
		result.append(maxChildNodes);
		result.append(')');
		return result.toString();
	}

} // CorrNodeTypeRestrictionImpl
