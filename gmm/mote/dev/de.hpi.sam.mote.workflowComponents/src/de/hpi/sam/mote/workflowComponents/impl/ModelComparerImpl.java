/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents.impl;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.mote.workflowComponents.ModelComparer;
import de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.labsticc.gmm.merge.service.GmmMergeUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Model Comparer</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.ModelComparerImpl#getModelSlot1
 * <em>Model Slot1</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.ModelComparerImpl#getModelSlot2
 * <em>Model Slot2</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.ModelComparerImpl#getComparisonResultModelSlot
 * <em>Comparison Result Model Slot</em>}</li>
 * <li>
 * {@link de.hpi.sam.mote.workflowComponents.impl.ModelComparerImpl#getDiffModelSlot
 * <em>Diff Model Slot</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ModelComparerImpl extends WorkflowComponentImpl implements ModelComparer
{
	/**
	 * The default value of the '{@link #getModelSlot1() <em>Model Slot1</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getModelSlot1()
	 * @generated
	 * @ordered
	 */
	protected static final String	MODEL_SLOT1_EDEFAULT					= null;

	/**
	 * The cached value of the '{@link #getModelSlot1() <em>Model Slot1</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getModelSlot1()
	 * @generated
	 * @ordered
	 */
	protected String				modelSlot1								= MODEL_SLOT1_EDEFAULT;

	/**
	 * The default value of the '{@link #getModelSlot2() <em>Model Slot2</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getModelSlot2()
	 * @generated
	 * @ordered
	 */
	protected static final String	MODEL_SLOT2_EDEFAULT					= null;

	/**
	 * The cached value of the '{@link #getModelSlot2() <em>Model Slot2</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getModelSlot2()
	 * @generated
	 * @ordered
	 */
	protected String				modelSlot2								= MODEL_SLOT2_EDEFAULT;

	/**
	 * The default value of the '{@link #getComparisonResultModelSlot()
	 * <em>Comparison Result Model Slot</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getComparisonResultModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String	COMPARISON_RESULT_MODEL_SLOT_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getComparisonResultModelSlot()
	 * <em>Comparison Result Model Slot</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getComparisonResultModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String				comparisonResultModelSlot				= COMPARISON_RESULT_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getDiffModelSlot()
	 * <em>Diff Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDiffModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String	DIFF_MODEL_SLOT_EDEFAULT				= null;

	/**
	 * The cached value of the '{@link #getDiffModelSlot()
	 * <em>Diff Model Slot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDiffModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String				diffModelSlot							= DIFF_MODEL_SLOT_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ModelComparerImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WorkflowComponentsPackage.Literals.MODEL_COMPARER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getModelSlot1()
	{
		return modelSlot1;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setModelSlot1(String newModelSlot1)
	{
		String oldModelSlot1 = modelSlot1;
		modelSlot1 = newModelSlot1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MODEL_COMPARER__MODEL_SLOT1, oldModelSlot1,
					modelSlot1));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getModelSlot2()
	{
		return modelSlot2;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setModelSlot2(String newModelSlot2)
	{
		String oldModelSlot2 = modelSlot2;
		modelSlot2 = newModelSlot2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MODEL_COMPARER__MODEL_SLOT2, oldModelSlot2,
					modelSlot2));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getComparisonResultModelSlot()
	{
		return comparisonResultModelSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setComparisonResultModelSlot(String newComparisonResultModelSlot)
	{
		String oldComparisonResultModelSlot = comparisonResultModelSlot;
		comparisonResultModelSlot = newComparisonResultModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MODEL_COMPARER__COMPARISON_RESULT_MODEL_SLOT,
					oldComparisonResultModelSlot, comparisonResultModelSlot));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getDiffModelSlot()
	{
		return diffModelSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDiffModelSlot(String newDiffModelSlot)
	{
		String oldDiffModelSlot = diffModelSlot;
		diffModelSlot = newDiffModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowComponentsPackage.MODEL_COMPARER__DIFF_MODEL_SLOT,
					oldDiffModelSlot, diffModelSlot));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MODEL_COMPARER__MODEL_SLOT1:
				return getModelSlot1();
			case WorkflowComponentsPackage.MODEL_COMPARER__MODEL_SLOT2:
				return getModelSlot2();
			case WorkflowComponentsPackage.MODEL_COMPARER__COMPARISON_RESULT_MODEL_SLOT:
				return getComparisonResultModelSlot();
			case WorkflowComponentsPackage.MODEL_COMPARER__DIFF_MODEL_SLOT:
				return getDiffModelSlot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MODEL_COMPARER__MODEL_SLOT1:
				setModelSlot1((String) newValue);
				return;
			case WorkflowComponentsPackage.MODEL_COMPARER__MODEL_SLOT2:
				setModelSlot2((String) newValue);
				return;
			case WorkflowComponentsPackage.MODEL_COMPARER__COMPARISON_RESULT_MODEL_SLOT:
				setComparisonResultModelSlot((String) newValue);
				return;
			case WorkflowComponentsPackage.MODEL_COMPARER__DIFF_MODEL_SLOT:
				setDiffModelSlot((String) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MODEL_COMPARER__MODEL_SLOT1:
				setModelSlot1(MODEL_SLOT1_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MODEL_COMPARER__MODEL_SLOT2:
				setModelSlot2(MODEL_SLOT2_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MODEL_COMPARER__COMPARISON_RESULT_MODEL_SLOT:
				setComparisonResultModelSlot(COMPARISON_RESULT_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowComponentsPackage.MODEL_COMPARER__DIFF_MODEL_SLOT:
				setDiffModelSlot(DIFF_MODEL_SLOT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case WorkflowComponentsPackage.MODEL_COMPARER__MODEL_SLOT1:
				return MODEL_SLOT1_EDEFAULT == null ? modelSlot1 != null : !MODEL_SLOT1_EDEFAULT.equals(modelSlot1);
			case WorkflowComponentsPackage.MODEL_COMPARER__MODEL_SLOT2:
				return MODEL_SLOT2_EDEFAULT == null ? modelSlot2 != null : !MODEL_SLOT2_EDEFAULT.equals(modelSlot2);
			case WorkflowComponentsPackage.MODEL_COMPARER__COMPARISON_RESULT_MODEL_SLOT:
				return COMPARISON_RESULT_MODEL_SLOT_EDEFAULT == null ? comparisonResultModelSlot != null
						: !COMPARISON_RESULT_MODEL_SLOT_EDEFAULT.equals(comparisonResultModelSlot);
			case WorkflowComponentsPackage.MODEL_COMPARER__DIFF_MODEL_SLOT:
				return DIFF_MODEL_SLOT_EDEFAULT == null ? diffModelSlot != null : !DIFF_MODEL_SLOT_EDEFAULT.equals(diffModelSlot);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (modelSlot1: ");
		result.append(modelSlot1);
		result.append(", modelSlot2: ");
		result.append(modelSlot2);
		result.append(", comparisonResultModelSlot: ");
		result.append(comparisonResultModelSlot);
		result.append(", diffModelSlot: ");
		result.append(diffModelSlot);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean checkConfiguration(WorkflowExecutionContext context) throws IOException
	{
		boolean success = true;

		if (getModelSlot1() == null || "".equals(getModelSlot1()))
		{
			context.getLogger().addError("getModelSlot1 is null", null, this);
			success = false;
		}

		if (getModelSlot2() == null || "".equals(getModelSlot2()))
		{
			context.getLogger().addError("getModelSlot2 is null", null, this);
			success = false;
		}

		if (getComparisonResultModelSlot() == null || "".equals(getComparisonResultModelSlot()))
		{
			context.getLogger().addError("getComparisonResultModelSlot is null", null, this);
			success = false;
		}

		if (getDiffModelSlot() == null || "".equals(getDiffModelSlot()))
		{
			context.getLogger().addError("getDiffModelSlot is null", null, this);
			success = false;
		}

		return success;
	}

	// ergaenzungen siehe MoteTransformerImpl, weiter anpassen siehe .ecore

	@Override
	public void execute(WorkflowExecutionContext context) throws WorkflowExecutionException, IOException
	{
		try
		{
			EObject model1 = (EObject) context.getModelSlots().get(getModelSlot1());

			EObject model2 = (EObject) context.getModelSlots().get(getModelSlot2());

//			Map<String, Object> options = new HashMap<String, Object>();
//			options.put(MatchOptions.OPTION_SEARCH_WINDOW, Integer.MAX_VALUE);
//
//			MatchModel matchModel = MatchService.doResourceMatch(model1.eResource(), model2.eResource(), options);
//
//			DiffModel diffModel = DiffService.doDiff(matchModel);
//
//			context.getModelSlots().put(getDiffModelSlot(), diffModel);
//
//			List<DiffElement> diffs = diffModel.getOwnedElements();
			// DB Migrate to EMF Compare 2. To be tested.
//		 	final IEObjectMatcher matcher = DefaultMatchEngine.createDefaultEObjectMatcher( UseIdentifiers.NEVER, 1 );
//			final IComparisonFactory comparisonFactory = new DefaultComparisonFactory( new DefaultEqualityHelperFactory() );
//			final IMatchEngine.Factory matchEngineFactory = new MatchEngineFactoryImpl( matcher, comparisonFactory );
//		    //matchEngineFactory.setRanking( 20 );
//		    final IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
//		    matchEngineRegistry.add(matchEngineFactory);
//			EMFCompare.Builder builder = EMFCompare.builder().setMatchEngineFactoryRegistry( matchEngineRegistry );
//			final EMFCompare comparator = builder.build();
//		 
//			// Compare the two models
//			final IComparisonScope scope = EMFCompare.createDefaultScope( model1.eResource(), model2.eResource() );
//			final Comparison comp = comparator.compare( scope );
//			
//			List<Diff> diffs = comp.getDifferences();
									// TODO: DB: Fix and test with latest EMF compare
//			if ( diffs.size() == 1 && diffs.get(0).getSubDiffElements().isEmpty())
			if ( GmmMergeUtil.getMergeService().equals( model1.eResource(), model2.eResource(), true, null, null ) )
			{
				context.getModelSlots().put(getComparisonResultModelSlot(), true);
				context.getLogger().addInfo("### OK - Models in '" + getModelSlot1() + "' and '" + getModelSlot2() + "' are equal.", this);
			}
			else
			{
				context.getModelSlots().put(getComparisonResultModelSlot(), false);
				context.getLogger().addInfo("### FAIL - Models in '" + getModelSlot1() + "' and '" + getModelSlot2() + "' are different.",
						this);
			}
		}
		catch (CoreException exc)
		{
			exc.printStackTrace();
			
			throw new RuntimeException( exc );
		}
	}

} // ModelComparerImpl
