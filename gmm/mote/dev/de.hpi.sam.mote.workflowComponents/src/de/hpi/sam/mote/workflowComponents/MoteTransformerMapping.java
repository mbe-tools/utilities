/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.workflowComponents;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mote Transformer Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.mote.workflowComponents.MoteTransformerMapping#getCorrespondenceModelSlot <em>Correspondence Model Slot</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getMoteTransformerMapping()
 * @model
 * @generated
 */
public interface MoteTransformerMapping extends MoteTransformer {
	/**
	 * Returns the value of the '<em><b>Correspondence Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correspondence Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correspondence Model Slot</em>' attribute.
	 * @see #setCorrespondenceModelSlot(String)
	 * @see de.hpi.sam.mote.workflowComponents.WorkflowComponentsPackage#getMoteTransformerMapping_CorrespondenceModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getCorrespondenceModelSlot();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.workflowComponents.MoteTransformerMapping#getCorrespondenceModelSlot <em>Correspondence Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Correspondence Model Slot</em>' attribute.
	 * @see #getCorrespondenceModelSlot()
	 * @generated
	 */
	void setCorrespondenceModelSlot(String value);

} // MoteTransformerMapping
