package de.hpi.sam.tgg.checks;

public class Utility
{

	public static boolean isJavaIdentifier(String name)
	{
		if (name == null || name.length() == 0)
		{
			return false;
		}

		if (!Character.isJavaIdentifierStart(name.charAt(0)))
		{
			return false;
		}

		for (int i = 1; i < name.length(); i++)
		{
			if (!Character.isJavaIdentifierPart(name.charAt(i)))
			{
				return false;
			}
		}

		return true;
	}

}
