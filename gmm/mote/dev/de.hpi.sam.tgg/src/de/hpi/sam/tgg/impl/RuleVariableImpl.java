/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl;
import de.hpi.sam.tgg.RuleVariable;
import de.hpi.sam.tgg.TggPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Rule Variable</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.impl.RuleVariableImpl#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.RuleVariableImpl#getForwardCalculationExpression <em>Forward Calculation Expression</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.RuleVariableImpl#getReverseCalculationExpression <em>Reverse Calculation Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RuleVariableImpl extends NamedElementImpl implements RuleVariable {
	/**
	 * The cached value of the '{@link #getClassifier() <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClassifier()
	 * @generated
	 * @ordered
	 */
	protected EClassifier classifier;

	/**
	 * The cached value of the '{@link #getForwardCalculationExpression()
	 * <em>Forward Calculation Expression</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getForwardCalculationExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression forwardCalculationExpression;

	/**
	 * The cached value of the '{@link #getReverseCalculationExpression()
	 * <em>Reverse Calculation Expression</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getReverseCalculationExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression reverseCalculationExpression;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected RuleVariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.RULE_VARIABLE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier getClassifier() {
		if (classifier != null && classifier.eIsProxy()) {
			InternalEObject oldClassifier = (InternalEObject) classifier;
			classifier = (EClassifier) eResolveProxy(oldClassifier);
			if (classifier != oldClassifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TggPackage.RULE_VARIABLE__CLASSIFIER,
							oldClassifier, classifier));
			}
		}
		return classifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier basicGetClassifier() {
		return classifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassifier(EClassifier newClassifier) {
		EClassifier oldClassifier = classifier;
		classifier = newClassifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.RULE_VARIABLE__CLASSIFIER, oldClassifier,
					classifier));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getForwardCalculationExpression() {
		return forwardCalculationExpression;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetForwardCalculationExpression(
			Expression newForwardCalculationExpression, NotificationChain msgs) {
		Expression oldForwardCalculationExpression = forwardCalculationExpression;
		forwardCalculationExpression = newForwardCalculationExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					TggPackage.RULE_VARIABLE__FORWARD_CALCULATION_EXPRESSION,
					oldForwardCalculationExpression,
					newForwardCalculationExpression);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setForwardCalculationExpression(
			Expression newForwardCalculationExpression) {
		if (newForwardCalculationExpression != forwardCalculationExpression) {
			NotificationChain msgs = null;
			if (forwardCalculationExpression != null)
				msgs = ((InternalEObject) forwardCalculationExpression)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- TggPackage.RULE_VARIABLE__FORWARD_CALCULATION_EXPRESSION,
								null, msgs);
			if (newForwardCalculationExpression != null)
				msgs = ((InternalEObject) newForwardCalculationExpression)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- TggPackage.RULE_VARIABLE__FORWARD_CALCULATION_EXPRESSION,
								null, msgs);
			msgs = basicSetForwardCalculationExpression(
					newForwardCalculationExpression, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.RULE_VARIABLE__FORWARD_CALCULATION_EXPRESSION,
					newForwardCalculationExpression,
					newForwardCalculationExpression));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getReverseCalculationExpression() {
		return reverseCalculationExpression;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReverseCalculationExpression(
			Expression newReverseCalculationExpression, NotificationChain msgs) {
		Expression oldReverseCalculationExpression = reverseCalculationExpression;
		reverseCalculationExpression = newReverseCalculationExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					TggPackage.RULE_VARIABLE__REVERSE_CALCULATION_EXPRESSION,
					oldReverseCalculationExpression,
					newReverseCalculationExpression);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setReverseCalculationExpression(
			Expression newReverseCalculationExpression) {
		if (newReverseCalculationExpression != reverseCalculationExpression) {
			NotificationChain msgs = null;
			if (reverseCalculationExpression != null)
				msgs = ((InternalEObject) reverseCalculationExpression)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- TggPackage.RULE_VARIABLE__REVERSE_CALCULATION_EXPRESSION,
								null, msgs);
			if (newReverseCalculationExpression != null)
				msgs = ((InternalEObject) newReverseCalculationExpression)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- TggPackage.RULE_VARIABLE__REVERSE_CALCULATION_EXPRESSION,
								null, msgs);
			msgs = basicSetReverseCalculationExpression(
					newReverseCalculationExpression, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.RULE_VARIABLE__REVERSE_CALCULATION_EXPRESSION,
					newReverseCalculationExpression,
					newReverseCalculationExpression));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.RULE_VARIABLE__FORWARD_CALCULATION_EXPRESSION:
			return basicSetForwardCalculationExpression(null, msgs);
		case TggPackage.RULE_VARIABLE__REVERSE_CALCULATION_EXPRESSION:
			return basicSetReverseCalculationExpression(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TggPackage.RULE_VARIABLE__CLASSIFIER:
			if (resolve)
				return getClassifier();
			return basicGetClassifier();
		case TggPackage.RULE_VARIABLE__FORWARD_CALCULATION_EXPRESSION:
			return getForwardCalculationExpression();
		case TggPackage.RULE_VARIABLE__REVERSE_CALCULATION_EXPRESSION:
			return getReverseCalculationExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TggPackage.RULE_VARIABLE__CLASSIFIER:
			setClassifier((EClassifier) newValue);
			return;
		case TggPackage.RULE_VARIABLE__FORWARD_CALCULATION_EXPRESSION:
			setForwardCalculationExpression((Expression) newValue);
			return;
		case TggPackage.RULE_VARIABLE__REVERSE_CALCULATION_EXPRESSION:
			setReverseCalculationExpression((Expression) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TggPackage.RULE_VARIABLE__CLASSIFIER:
			setClassifier((EClassifier) null);
			return;
		case TggPackage.RULE_VARIABLE__FORWARD_CALCULATION_EXPRESSION:
			setForwardCalculationExpression((Expression) null);
			return;
		case TggPackage.RULE_VARIABLE__REVERSE_CALCULATION_EXPRESSION:
			setReverseCalculationExpression((Expression) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TggPackage.RULE_VARIABLE__CLASSIFIER:
			return classifier != null;
		case TggPackage.RULE_VARIABLE__FORWARD_CALCULATION_EXPRESSION:
			return forwardCalculationExpression != null;
		case TggPackage.RULE_VARIABLE__REVERSE_CALCULATION_EXPRESSION:
			return reverseCalculationExpression != null;
		}
		return super.eIsSet(featureID);
	}

} // RuleVariableImpl
