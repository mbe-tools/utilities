/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Correspondence Element</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see de.hpi.sam.tgg.TggPackage#getCorrespondenceElement()
 * @model abstract="true"
 * @generated
 */
public interface CorrespondenceElement extends RuleElement {

} // CorrespondenceElement
