/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Correspondence Node</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A CorrespondenceNode belongs to the CorrespondenceDomain. It is connected to several ModelElements from different ModelDomains via CorrespondenceLinks. A CorrespondenceNode must have a nodeTypeName. By default, it is TGGNode, which means, that the superclass of all correspondence nodes is used as the classfier for this CorrespondenceNode when the TGGRule is translated to a Story Diagram. Otherwise, a new Class is created in the Ecore meta model that is created along with the story diagram.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.CorrespondenceNode#getOutgoingCorrespondenceLinks <em>Outgoing Correspondence Links</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.CorrespondenceNode#getClassifier <em>Classifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.TggPackage#getCorrespondenceNode()
 * @model
 * @generated
 */
public interface CorrespondenceNode extends CorrespondenceElement {
	/**
	 * Returns the value of the '<em><b>Outgoing Correspondence Links</b></em>'
	 * reference list. The list contents are of type
	 * {@link de.hpi.sam.tgg.CorrespondenceLink}. It is bidirectional and its
	 * opposite is '{@link de.hpi.sam.tgg.CorrespondenceLink#getSource
	 * <em>Source</em>}'. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> A list of all CorrespondenceLinks going out from this
	 * CorrespondenceNode. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Outgoing Correspondence Links</em>'
	 *         reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getCorrespondenceNode_OutgoingCorrespondenceLinks()
	 * @see de.hpi.sam.tgg.CorrespondenceLink#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<CorrespondenceLink> getOutgoingCorrespondenceLinks();

	/**
	 * Returns the value of the '<em><b>Classifier</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * EClass of this correspondence node. This must be the class TGGNode from
	 * the MoTE plugin or a subclass thereof. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Classifier</em>' reference.
	 * @see #setClassifier(EClass)
	 * @see de.hpi.sam.tgg.TggPackage#getCorrespondenceNode_Classifier()
	 * @model
	 * @generated
	 */
	EClass getClassifier();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.CorrespondenceNode#getClassifier <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Classifier</em>' reference.
	 * @see #getClassifier()
	 * @generated
	 */
	void setClassifier(EClass value);

} // CorrespondenceNode
