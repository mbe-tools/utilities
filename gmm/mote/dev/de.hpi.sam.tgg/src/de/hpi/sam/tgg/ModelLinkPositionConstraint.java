/**
 */
package de.hpi.sam.tgg;

import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Link Position Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.ModelLinkPositionConstraint#getConstraintType <em>Constraint Type</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelLinkPositionConstraint#getModelLink <em>Model Link</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.TggPackage#getModelLinkPositionConstraint()
 * @model
 * @generated
 */
public interface ModelLinkPositionConstraint extends EObject {
	/**
	 * Returns the value of the '<em><b>Constraint Type</b></em>' attribute.
	 * The default value is <code>"FIRST"</code>.
	 * The literals are from the enumeration {@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The type of the link constraint: first, last, or index. In case of index, 
	 * indexExpression has to specify the exact index.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Constraint Type</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration
	 * @see #setConstraintType(LinkPositionConstraintEnumeration)
	 * @see de.hpi.sam.tgg.TggPackage#getModelLinkPositionConstraint_ConstraintType()
	 * @model default="FIRST" required="true"
	 * @generated
	 */
	LinkPositionConstraintEnumeration getConstraintType();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ModelLinkPositionConstraint#getConstraintType <em>Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraint Type</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration
	 * @see #getConstraintType()
	 * @generated
	 */
	void setConstraintType(LinkPositionConstraintEnumeration value);

	/**
	 * Returns the value of the '<em><b>Model Link</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.tgg.ModelLink#getLinkPositionConstraint <em>Link Position Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Link</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Link</em>' container reference.
	 * @see #setModelLink(ModelLink)
	 * @see de.hpi.sam.tgg.TggPackage#getModelLinkPositionConstraint_ModelLink()
	 * @see de.hpi.sam.tgg.ModelLink#getLinkPositionConstraint
	 * @model opposite="linkPositionConstraint" required="true" transient="false"
	 * @generated
	 */
	ModelLink getModelLink();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ModelLinkPositionConstraint#getModelLink <em>Model Link</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Link</em>' container reference.
	 * @see #getModelLink()
	 * @generated
	 */
	void setModelLink(ModelLink value);

} // ModelLinkPositionConstraint
