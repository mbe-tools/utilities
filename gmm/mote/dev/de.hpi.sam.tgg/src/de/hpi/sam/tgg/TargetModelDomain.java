/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Target Model Domain</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see de.hpi.sam.tgg.TggPackage#getTargetModelDomain()
 * @model
 * @generated
 */
public interface TargetModelDomain extends ModelDomain {
} // TargetModelDomain
