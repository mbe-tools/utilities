/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

import de.hpi.sam.storyDiagramEcore.NamedElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Rule Element</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.RuleElement#getModifier <em>Modifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.TggPackage#getRuleElement()
 * @model
 * @generated
 */
public interface RuleElement extends NamedElement {

	/**
	 * Returns the value of the '<em><b>Modifier</b></em>' attribute. The
	 * literals are from the enumeration
	 * {@link de.hpi.sam.tgg.TGGModifierEnumeration}. <!-- begin-user-doc -->
	 * <!-- end-user-doc --> <!-- begin-model-doc --> The modifier of this
	 * ModelElement. Denotes whether the element is created or left unchanged.
	 * <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Modifier</em>' attribute.
	 * @see de.hpi.sam.tgg.TGGModifierEnumeration
	 * @see #setModifier(TGGModifierEnumeration)
	 * @see de.hpi.sam.tgg.TggPackage#getRuleElement_Modifier()
	 * @model required="true"
	 * @generated
	 */
	TGGModifierEnumeration getModifier();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.RuleElement#getModifier
	 * <em>Modifier</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Modifier</em>' attribute.
	 * @see de.hpi.sam.tgg.TGGModifierEnumeration
	 * @see #getModifier()
	 * @generated
	 */
	void setModifier(TGGModifierEnumeration value);
} // RuleElement
