/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.impl;

import de.hpi.sam.storyDiagramEcore.sdm.ExternalReference;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelLinkPositionConstraint;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TggPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Model Link</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelLinkImpl#getEReference <em>EReference</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelLinkImpl#getSource <em>Source</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelLinkImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelLinkImpl#getOppositeTGGLink <em>Opposite TGG Link</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelLinkImpl#getEOppositeReference <em>EOpposite Reference</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelLinkImpl#getLinkPositionConstraint <em>Link Position Constraint</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelLinkImpl#getExternalReference <em>External Reference</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelLinkImpl#isExternal <em>External</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ModelLinkImpl extends ModelElementImpl implements ModelLink {
	/**
	 * The cached value of the '{@link #getEReference() <em>EReference</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEReference()
	 * @generated
	 * @ordered
	 */
	protected EReference eReference;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected ModelObject source;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected ModelObject target;

	/**
	 * The cached value of the '{@link #getOppositeTGGLink() <em>Opposite TGG Link</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getOppositeTGGLink()
	 * @generated
	 * @ordered
	 */
	protected ModelLink oppositeTGGLink;

	/**
	 * The cached value of the '{@link #getEOppositeReference() <em>EOpposite Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEOppositeReference()
	 * @generated
	 * @ordered
	 */
	protected EReference eOppositeReference;

	/**
	 * The cached value of the '{@link #getLinkPositionConstraint() <em>Link Position Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkPositionConstraint()
	 * @generated
	 * @ordered
	 */
	protected ModelLinkPositionConstraint linkPositionConstraint;

	/**
	 * The cached value of the '{@link #getExternalReference() <em>External Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalReference()
	 * @generated
	 * @ordered
	 */
	protected ExternalReference externalReference;

	/**
	 * The default value of the '{@link #isExternal() <em>External</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExternal()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXTERNAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExternal() <em>External</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExternal()
	 * @generated
	 * @ordered
	 */
	protected boolean external = EXTERNAL_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.MODEL_LINK;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEReference() {
		if (eReference != null && eReference.eIsProxy()) {
			InternalEObject oldEReference = (InternalEObject) eReference;
			eReference = (EReference) eResolveProxy(oldEReference);
			if (eReference != oldEReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TggPackage.MODEL_LINK__EREFERENCE, oldEReference,
							eReference));
			}
		}
		return eReference;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetEReference() {
		return eReference;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setEReference(EReference newEReference) {
		EReference oldEReference = eReference;
		eReference = newEReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.MODEL_LINK__EREFERENCE, oldEReference,
					eReference));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModelObject getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject) source;
			source = (ModelObject) eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TggPackage.MODEL_LINK__SOURCE, oldSource, source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModelObject basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSource(ModelObject newSource,
			NotificationChain msgs) {
		ModelObject oldSource = source;
		source = newSource;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, TggPackage.MODEL_LINK__SOURCE, oldSource,
					newSource);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(ModelObject newSource) {
		if (newSource != source) {
			NotificationChain msgs = null;
			if (source != null)
				msgs = ((InternalEObject) source).eInverseRemove(this,
						TggPackage.MODEL_OBJECT__OUTGOING_MODEL_LINKS,
						ModelObject.class, msgs);
			if (newSource != null)
				msgs = ((InternalEObject) newSource).eInverseAdd(this,
						TggPackage.MODEL_OBJECT__OUTGOING_MODEL_LINKS,
						ModelObject.class, msgs);
			msgs = basicSetSource(newSource, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.MODEL_LINK__SOURCE, newSource, newSource));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModelObject getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject) target;
			target = (ModelObject) eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TggPackage.MODEL_LINK__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModelObject basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTarget(ModelObject newTarget,
			NotificationChain msgs) {
		ModelObject oldTarget = target;
		target = newTarget;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, TggPackage.MODEL_LINK__TARGET, oldTarget,
					newTarget);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(ModelObject newTarget) {
		if (newTarget != target) {
			NotificationChain msgs = null;
			if (target != null)
				msgs = ((InternalEObject) target).eInverseRemove(this,
						TggPackage.MODEL_OBJECT__INCOMING_MODEL_LINKS,
						ModelObject.class, msgs);
			if (newTarget != null)
				msgs = ((InternalEObject) newTarget).eInverseAdd(this,
						TggPackage.MODEL_OBJECT__INCOMING_MODEL_LINKS,
						ModelObject.class, msgs);
			msgs = basicSetTarget(newTarget, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.MODEL_LINK__TARGET, newTarget, newTarget));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModelLink getOppositeTGGLink() {
		if (oppositeTGGLink != null && oppositeTGGLink.eIsProxy()) {
			InternalEObject oldOppositeTGGLink = (InternalEObject) oppositeTGGLink;
			oppositeTGGLink = (ModelLink) eResolveProxy(oldOppositeTGGLink);
			if (oppositeTGGLink != oldOppositeTGGLink) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK,
							oldOppositeTGGLink, oppositeTGGLink));
			}
		}
		return oppositeTGGLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModelLink basicGetOppositeTGGLink() {
		return oppositeTGGLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOppositeTGGLink(
			ModelLink newOppositeTGGLink, NotificationChain msgs) {
		ModelLink oldOppositeTGGLink = oppositeTGGLink;
		oppositeTGGLink = newOppositeTGGLink;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK,
					oldOppositeTGGLink, newOppositeTGGLink);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setOppositeTGGLink(ModelLink newOppositeTGGLink) {
		if (newOppositeTGGLink != oppositeTGGLink) {
			NotificationChain msgs = null;
			if (oppositeTGGLink != null)
				msgs = ((InternalEObject) oppositeTGGLink).eInverseRemove(this,
						TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK,
						ModelLink.class, msgs);
			if (newOppositeTGGLink != null)
				msgs = ((InternalEObject) newOppositeTGGLink).eInverseAdd(this,
						TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK,
						ModelLink.class, msgs);
			msgs = basicSetOppositeTGGLink(newOppositeTGGLink, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK,
					newOppositeTGGLink, newOppositeTGGLink));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEOppositeReference() {
		if (eOppositeReference != null && eOppositeReference.eIsProxy()) {
			InternalEObject oldEOppositeReference = (InternalEObject) eOppositeReference;
			eOppositeReference = (EReference) eResolveProxy(oldEOppositeReference);
			if (eOppositeReference != oldEOppositeReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TggPackage.MODEL_LINK__EOPPOSITE_REFERENCE,
							oldEOppositeReference, eOppositeReference));
			}
		}
		return eOppositeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetEOppositeReference() {
		return eOppositeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEOppositeReference(EReference newEOppositeReference) {
		EReference oldEOppositeReference = eOppositeReference;
		eOppositeReference = newEOppositeReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.MODEL_LINK__EOPPOSITE_REFERENCE,
					oldEOppositeReference, eOppositeReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelLinkPositionConstraint getLinkPositionConstraint() {
		return linkPositionConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLinkPositionConstraint(
			ModelLinkPositionConstraint newLinkPositionConstraint,
			NotificationChain msgs) {
		ModelLinkPositionConstraint oldLinkPositionConstraint = linkPositionConstraint;
		linkPositionConstraint = newLinkPositionConstraint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					TggPackage.MODEL_LINK__LINK_POSITION_CONSTRAINT,
					oldLinkPositionConstraint, newLinkPositionConstraint);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkPositionConstraint(
			ModelLinkPositionConstraint newLinkPositionConstraint) {
		if (newLinkPositionConstraint != linkPositionConstraint) {
			NotificationChain msgs = null;
			if (linkPositionConstraint != null)
				msgs = ((InternalEObject) linkPositionConstraint)
						.eInverseRemove(
								this,
								TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK,
								ModelLinkPositionConstraint.class, msgs);
			if (newLinkPositionConstraint != null)
				msgs = ((InternalEObject) newLinkPositionConstraint)
						.eInverseAdd(
								this,
								TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK,
								ModelLinkPositionConstraint.class, msgs);
			msgs = basicSetLinkPositionConstraint(newLinkPositionConstraint,
					msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.MODEL_LINK__LINK_POSITION_CONSTRAINT,
					newLinkPositionConstraint, newLinkPositionConstraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalReference getExternalReference() {
		return externalReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExternalReference(
			ExternalReference newExternalReference, NotificationChain msgs) {
		ExternalReference oldExternalReference = externalReference;
		externalReference = newExternalReference;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					TggPackage.MODEL_LINK__EXTERNAL_REFERENCE,
					oldExternalReference, newExternalReference);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalReference(ExternalReference newExternalReference) {
		if (newExternalReference != externalReference) {
			NotificationChain msgs = null;
			if (externalReference != null)
				msgs = ((InternalEObject) externalReference).eInverseRemove(
						this, EOPPOSITE_FEATURE_BASE
								- TggPackage.MODEL_LINK__EXTERNAL_REFERENCE,
						null, msgs);
			if (newExternalReference != null)
				msgs = ((InternalEObject) newExternalReference).eInverseAdd(
						this, EOPPOSITE_FEATURE_BASE
								- TggPackage.MODEL_LINK__EXTERNAL_REFERENCE,
						null, msgs);
			msgs = basicSetExternalReference(newExternalReference, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.MODEL_LINK__EXTERNAL_REFERENCE,
					newExternalReference, newExternalReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExternal() {
		return external;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternal(boolean newExternal) {
		boolean oldExternal = external;
		external = newExternal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.MODEL_LINK__EXTERNAL, oldExternal, external));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.MODEL_LINK__SOURCE:
			if (source != null)
				msgs = ((InternalEObject) source).eInverseRemove(this,
						TggPackage.MODEL_OBJECT__OUTGOING_MODEL_LINKS,
						ModelObject.class, msgs);
			return basicSetSource((ModelObject) otherEnd, msgs);
		case TggPackage.MODEL_LINK__TARGET:
			if (target != null)
				msgs = ((InternalEObject) target).eInverseRemove(this,
						TggPackage.MODEL_OBJECT__INCOMING_MODEL_LINKS,
						ModelObject.class, msgs);
			return basicSetTarget((ModelObject) otherEnd, msgs);
		case TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK:
			if (oppositeTGGLink != null)
				msgs = ((InternalEObject) oppositeTGGLink).eInverseRemove(this,
						TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK,
						ModelLink.class, msgs);
			return basicSetOppositeTGGLink((ModelLink) otherEnd, msgs);
		case TggPackage.MODEL_LINK__LINK_POSITION_CONSTRAINT:
			if (linkPositionConstraint != null)
				msgs = ((InternalEObject) linkPositionConstraint)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- TggPackage.MODEL_LINK__LINK_POSITION_CONSTRAINT,
								null, msgs);
			return basicSetLinkPositionConstraint(
					(ModelLinkPositionConstraint) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.MODEL_LINK__SOURCE:
			return basicSetSource(null, msgs);
		case TggPackage.MODEL_LINK__TARGET:
			return basicSetTarget(null, msgs);
		case TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK:
			return basicSetOppositeTGGLink(null, msgs);
		case TggPackage.MODEL_LINK__LINK_POSITION_CONSTRAINT:
			return basicSetLinkPositionConstraint(null, msgs);
		case TggPackage.MODEL_LINK__EXTERNAL_REFERENCE:
			return basicSetExternalReference(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TggPackage.MODEL_LINK__EREFERENCE:
			if (resolve)
				return getEReference();
			return basicGetEReference();
		case TggPackage.MODEL_LINK__SOURCE:
			if (resolve)
				return getSource();
			return basicGetSource();
		case TggPackage.MODEL_LINK__TARGET:
			if (resolve)
				return getTarget();
			return basicGetTarget();
		case TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK:
			if (resolve)
				return getOppositeTGGLink();
			return basicGetOppositeTGGLink();
		case TggPackage.MODEL_LINK__EOPPOSITE_REFERENCE:
			if (resolve)
				return getEOppositeReference();
			return basicGetEOppositeReference();
		case TggPackage.MODEL_LINK__LINK_POSITION_CONSTRAINT:
			return getLinkPositionConstraint();
		case TggPackage.MODEL_LINK__EXTERNAL_REFERENCE:
			return getExternalReference();
		case TggPackage.MODEL_LINK__EXTERNAL:
			return isExternal();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TggPackage.MODEL_LINK__EREFERENCE:
			setEReference((EReference) newValue);
			return;
		case TggPackage.MODEL_LINK__SOURCE:
			setSource((ModelObject) newValue);
			return;
		case TggPackage.MODEL_LINK__TARGET:
			setTarget((ModelObject) newValue);
			return;
		case TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK:
			setOppositeTGGLink((ModelLink) newValue);
			return;
		case TggPackage.MODEL_LINK__EOPPOSITE_REFERENCE:
			setEOppositeReference((EReference) newValue);
			return;
		case TggPackage.MODEL_LINK__LINK_POSITION_CONSTRAINT:
			setLinkPositionConstraint((ModelLinkPositionConstraint) newValue);
			return;
		case TggPackage.MODEL_LINK__EXTERNAL_REFERENCE:
			setExternalReference((ExternalReference) newValue);
			return;
		case TggPackage.MODEL_LINK__EXTERNAL:
			setExternal((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TggPackage.MODEL_LINK__EREFERENCE:
			setEReference((EReference) null);
			return;
		case TggPackage.MODEL_LINK__SOURCE:
			setSource((ModelObject) null);
			return;
		case TggPackage.MODEL_LINK__TARGET:
			setTarget((ModelObject) null);
			return;
		case TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK:
			setOppositeTGGLink((ModelLink) null);
			return;
		case TggPackage.MODEL_LINK__EOPPOSITE_REFERENCE:
			setEOppositeReference((EReference) null);
			return;
		case TggPackage.MODEL_LINK__LINK_POSITION_CONSTRAINT:
			setLinkPositionConstraint((ModelLinkPositionConstraint) null);
			return;
		case TggPackage.MODEL_LINK__EXTERNAL_REFERENCE:
			setExternalReference((ExternalReference) null);
			return;
		case TggPackage.MODEL_LINK__EXTERNAL:
			setExternal(EXTERNAL_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TggPackage.MODEL_LINK__EREFERENCE:
			return eReference != null;
		case TggPackage.MODEL_LINK__SOURCE:
			return source != null;
		case TggPackage.MODEL_LINK__TARGET:
			return target != null;
		case TggPackage.MODEL_LINK__OPPOSITE_TGG_LINK:
			return oppositeTGGLink != null;
		case TggPackage.MODEL_LINK__EOPPOSITE_REFERENCE:
			return eOppositeReference != null;
		case TggPackage.MODEL_LINK__LINK_POSITION_CONSTRAINT:
			return linkPositionConstraint != null;
		case TggPackage.MODEL_LINK__EXTERNAL_REFERENCE:
			return externalReference != null;
		case TggPackage.MODEL_LINK__EXTERNAL:
			return external != EXTERNAL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (external: ");
		result.append(external);
		result.append(')');
		return result.toString();
	}

} // ModelLinkImpl
