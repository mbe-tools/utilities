/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.impl;

import de.hpi.sam.tgg.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.hpi.sam.tgg.CorrespondenceDomain;
import de.hpi.sam.tgg.CorrespondenceLink;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.RuleElement;
import de.hpi.sam.tgg.RuleVariable;
import de.hpi.sam.tgg.SourceModelDomain;
import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGModifierEnumeration;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TargetModelDomain;
import de.hpi.sam.tgg.TggFactory;
import de.hpi.sam.tgg.TggPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class TggFactoryImpl extends EFactoryImpl implements TggFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static TggFactory init() {
		try {
			TggFactory theTggFactory = (TggFactory) EPackage.Registry.INSTANCE
					.getEFactory(TggPackage.eNS_URI);
			if (theTggFactory != null) {
				return theTggFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TggFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public TggFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case TggPackage.TGG_DIAGRAM:
			return createTGGDiagram();
		case TggPackage.CORRESPONDENCE_DOMAIN:
			return createCorrespondenceDomain();
		case TggPackage.MODEL_LINK:
			return createModelLink();
		case TggPackage.MODEL_OBJECT:
			return createModelObject();
		case TggPackage.TGG_RULE:
			return createTGGRule();
		case TggPackage.CORRESPONDENCE_NODE:
			return createCorrespondenceNode();
		case TggPackage.CORRESPONDENCE_LINK:
			return createCorrespondenceLink();
		case TggPackage.RULE_ELEMENT:
			return createRuleElement();
		case TggPackage.SOURCE_MODEL_DOMAIN:
			return createSourceModelDomain();
		case TggPackage.TARGET_MODEL_DOMAIN:
			return createTargetModelDomain();
		case TggPackage.RULE_VARIABLE:
			return createRuleVariable();
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT:
			return createModelLinkPositionConstraint();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case TggPackage.TGG_MODIFIER_ENUMERATION:
			return createTGGModifierEnumerationFromString(eDataType,
					initialValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case TggPackage.TGG_MODIFIER_ENUMERATION:
			return convertTGGModifierEnumerationToString(eDataType,
					instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGDiagram createTGGDiagram() {
		TGGDiagramImpl tggDiagram = new TGGDiagramImpl();
		return tggDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CorrespondenceDomain createCorrespondenceDomain() {
		CorrespondenceDomainImpl correspondenceDomain = new CorrespondenceDomainImpl();
		return correspondenceDomain;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModelLink createModelLink() {
		ModelLinkImpl modelLink = new ModelLinkImpl();
		return modelLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModelObject createModelObject() {
		ModelObjectImpl modelObject = new ModelObjectImpl();
		return modelObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGRule createTGGRule() {
		TGGRuleImpl tggRule = new TGGRuleImpl();
		return tggRule;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CorrespondenceNode createCorrespondenceNode() {
		CorrespondenceNodeImpl correspondenceNode = new CorrespondenceNodeImpl();
		return correspondenceNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CorrespondenceLink createCorrespondenceLink() {
		CorrespondenceLinkImpl correspondenceLink = new CorrespondenceLinkImpl();
		return correspondenceLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RuleElement createRuleElement() {
		RuleElementImpl ruleElement = new RuleElementImpl();
		return ruleElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SourceModelDomain createSourceModelDomain() {
		SourceModelDomainImpl sourceModelDomain = new SourceModelDomainImpl();
		return sourceModelDomain;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TargetModelDomain createTargetModelDomain() {
		TargetModelDomainImpl targetModelDomain = new TargetModelDomainImpl();
		return targetModelDomain;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RuleVariable createRuleVariable() {
		RuleVariableImpl ruleVariable = new RuleVariableImpl();
		return ruleVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelLinkPositionConstraint createModelLinkPositionConstraint() {
		ModelLinkPositionConstraintImpl modelLinkPositionConstraint = new ModelLinkPositionConstraintImpl();
		return modelLinkPositionConstraint;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGModifierEnumeration createTGGModifierEnumerationFromString(
			EDataType eDataType, String initialValue) {
		TGGModifierEnumeration result = TGGModifierEnumeration
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTGGModifierEnumerationToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TggPackage getTggPackage() {
		return (TggPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TggPackage getPackage() {
		return TggPackage.eINSTANCE;
	}

} // TggFactoryImpl
