/**
 */
package de.hpi.sam.tgg.impl;

import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration;

import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelLinkPositionConstraint;
import de.hpi.sam.tgg.TggPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model Link Position Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelLinkPositionConstraintImpl#getConstraintType <em>Constraint Type</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelLinkPositionConstraintImpl#getModelLink <em>Model Link</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ModelLinkPositionConstraintImpl extends EObjectImpl implements
		ModelLinkPositionConstraint {
	/**
	 * The default value of the '{@link #getConstraintType() <em>Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraintType()
	 * @generated
	 * @ordered
	 */
	protected static final LinkPositionConstraintEnumeration CONSTRAINT_TYPE_EDEFAULT = LinkPositionConstraintEnumeration.FIRST;

	/**
	 * The cached value of the '{@link #getConstraintType() <em>Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraintType()
	 * @generated
	 * @ordered
	 */
	protected LinkPositionConstraintEnumeration constraintType = CONSTRAINT_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelLinkPositionConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.MODEL_LINK_POSITION_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkPositionConstraintEnumeration getConstraintType() {
		return constraintType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraintType(
			LinkPositionConstraintEnumeration newConstraintType) {
		LinkPositionConstraintEnumeration oldConstraintType = constraintType;
		constraintType = newConstraintType == null ? CONSTRAINT_TYPE_EDEFAULT
				: newConstraintType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.MODEL_LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE,
					oldConstraintType, constraintType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelLink getModelLink() {
		if (eContainerFeatureID() != TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK)
			return null;
		return (ModelLink) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModelLink(ModelLink newModelLink,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newModelLink,
				TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelLink(ModelLink newModelLink) {
		if (newModelLink != eInternalContainer()
				|| (eContainerFeatureID() != TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK && newModelLink != null)) {
			if (EcoreUtil.isAncestor(this, newModelLink))
				throw new IllegalArgumentException(
						"Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newModelLink != null)
				msgs = ((InternalEObject) newModelLink).eInverseAdd(this,
						TggPackage.MODEL_LINK__LINK_POSITION_CONSTRAINT,
						ModelLink.class, msgs);
			msgs = basicSetModelLink(newModelLink, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK,
					newModelLink, newModelLink));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetModelLink((ModelLink) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK:
			return basicSetModelLink(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(
			NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK:
			return eInternalContainer().eInverseRemove(this,
					TggPackage.MODEL_LINK__LINK_POSITION_CONSTRAINT,
					ModelLink.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE:
			return getConstraintType();
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK:
			return getModelLink();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE:
			setConstraintType((LinkPositionConstraintEnumeration) newValue);
			return;
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK:
			setModelLink((ModelLink) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE:
			setConstraintType(CONSTRAINT_TYPE_EDEFAULT);
			return;
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK:
			setModelLink((ModelLink) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE:
			return constraintType != CONSTRAINT_TYPE_EDEFAULT;
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK:
			return getModelLink() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (constraintType: ");
		result.append(constraintType);
		result.append(')');
		return result.toString();
	}

} //ModelLinkPositionConstraintImpl
