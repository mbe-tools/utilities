/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Model Object</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The ModelObject represents an object.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.ModelObject#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelObject#getConstraintExpressions <em>Constraint Expressions</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelObject#getAttributeAssignments <em>Attribute Assignments</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelObject#getOutgoingModelLinks <em>Outgoing Model Links</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelObject#getIncomingModelLinks <em>Incoming Model Links</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelObject#getPostCreationExpressions <em>Post Creation Expressions</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.TggPackage#getModelObject()
 * @model
 * @generated
 */
public interface ModelObject extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Classifier</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * classifier represented by this ModelObject. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Classifier</em>' reference.
	 * @see #setClassifier(EClassifier)
	 * @see de.hpi.sam.tgg.TggPackage#getModelObject_Classifier()
	 * @model
	 * @generated
	 */
	EClassifier getClassifier();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ModelObject#getClassifier <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Classifier</em>' reference.
	 * @see #getClassifier()
	 * @generated
	 */
	void setClassifier(EClassifier value);

	/**
	 * Returns the value of the '<em><b>Constraint Expressions</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.expressions.Expression}. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> A
	 * constraint that must be fulfilled by an instance object before this
	 * ModelObject can be matched to it. No references to other ModelObjects or
	 * their attributes should be made in this constraint because they may not
	 * be matched when this constraint is evaluated. The constraint of the
	 * TGGRule should be used instead. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Constraint Expressions</em>' containment
	 *         reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getModelObject_ConstraintExpressions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getConstraintExpressions();

	/**
	 * Returns the value of the '<em><b>Attribute Assignments</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment}. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> A list
	 * of AttributeAssignments. When the TGGRule is executed, these assignments
	 * are executed, as well. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Attribute Assignments</em>' containment
	 *         reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getModelObject_AttributeAssignments()
	 * @model containment="true"
	 * @generated
	 */
	EList<AttributeAssignment> getAttributeAssignments();

	/**
	 * Returns the value of the '<em><b>Outgoing Model Links</b></em>' reference
	 * list. The list contents are of type {@link de.hpi.sam.tgg.ModelLink}. It
	 * is bidirectional and its opposite is '
	 * {@link de.hpi.sam.tgg.ModelLink#getSource <em>Source</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> A list
	 * of all ModelLinks going out from this ModelObject. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Outgoing Model Links</em>' reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getModelObject_OutgoingModelLinks()
	 * @see de.hpi.sam.tgg.ModelLink#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<ModelLink> getOutgoingModelLinks();

	/**
	 * Returns the value of the '<em><b>Incoming Model Links</b></em>' reference
	 * list. The list contents are of type {@link de.hpi.sam.tgg.ModelLink}. It
	 * is bidirectional and its opposite is '
	 * {@link de.hpi.sam.tgg.ModelLink#getTarget <em>Target</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> A list
	 * of all ModelLinks ending at this ModelObject. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Incoming Model Links</em>' reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getModelObject_IncomingModelLinks()
	 * @see de.hpi.sam.tgg.ModelLink#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<ModelLink> getIncomingModelLinks();

	/**
	 * Returns the value of the '<em><b>Post Creation Expressions</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.expressions.Expression}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Post Creation Expressions</em>' containment
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Post Creation Expressions</em>' containment
	 *         reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getModelObject_PostCreationExpressions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getPostCreationExpressions();

} // ModelObject
