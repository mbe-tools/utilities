/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Model Element</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * ModelElement is the abstract superclass of ModelObject and ModelLink.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.tgg.TggPackage#getModelElement()
 * @model abstract="true"
 * @generated
 */
public interface ModelElement extends RuleElement {

} // ModelElement
