/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.TggPackage
 * @generated
 */
public interface TggFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	TggFactory eINSTANCE = de.hpi.sam.tgg.impl.TggFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>TGG Diagram</em>'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return a new object of class '<em>TGG Diagram</em>'.
	 * @generated
	 */
	TGGDiagram createTGGDiagram();

	/**
	 * Returns a new object of class '<em>Correspondence Domain</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Correspondence Domain</em>'.
	 * @generated
	 */
	CorrespondenceDomain createCorrespondenceDomain();

	/**
	 * Returns a new object of class '<em>Model Link</em>'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Link</em>'.
	 * @generated
	 */
	ModelLink createModelLink();

	/**
	 * Returns a new object of class '<em>Model Object</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Model Object</em>'.
	 * @generated
	 */
	ModelObject createModelObject();

	/**
	 * Returns a new object of class '<em>TGG Rule</em>'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return a new object of class '<em>TGG Rule</em>'.
	 * @generated
	 */
	TGGRule createTGGRule();

	/**
	 * Returns a new object of class '<em>Correspondence Node</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Correspondence Node</em>'.
	 * @generated
	 */
	CorrespondenceNode createCorrespondenceNode();

	/**
	 * Returns a new object of class '<em>Correspondence Link</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Correspondence Link</em>'.
	 * @generated
	 */
	CorrespondenceLink createCorrespondenceLink();

	/**
	 * Returns a new object of class '<em>Rule Element</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Rule Element</em>'.
	 * @generated
	 */
	RuleElement createRuleElement();

	/**
	 * Returns a new object of class '<em>Source Model Domain</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Source Model Domain</em>'.
	 * @generated
	 */
	SourceModelDomain createSourceModelDomain();

	/**
	 * Returns a new object of class '<em>Target Model Domain</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Target Model Domain</em>'.
	 * @generated
	 */
	TargetModelDomain createTargetModelDomain();

	/**
	 * Returns a new object of class '<em>Rule Variable</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Rule Variable</em>'.
	 * @generated
	 */
	RuleVariable createRuleVariable();

	/**
	 * Returns a new object of class '<em>Model Link Position Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Link Position Constraint</em>'.
	 * @generated
	 */
	ModelLinkPositionConstraint createModelLinkPositionConstraint();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TggPackage getTggPackage();

} // TggFactory
