/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

import org.eclipse.emf.common.util.EList;

import de.hpi.sam.storyDiagramEcore.NamedElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>TGG Diagram</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A TGGDiagram contains several TGGRules. Each TGGRule has a CorrespondenceDomain, that contains CorrespondenceNodes and CorrespondenceLinks, and several ModelDomains, that contain the ModelElements that should be tranformed by the rule.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.TGGDiagram#getTggRules <em>Tgg Rules</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.TGGDiagram#getRuleSetID <em>Rule Set ID</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.TggPackage#getTGGDiagram()
 * @model
 * @generated
 */
public interface TGGDiagram extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Tgg Rules</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link de.hpi.sam.tgg.TGGRule}. <!-- begin-user-doc --> <!-- end-user-doc
	 * --> <!-- begin-model-doc --> The rules contained in this TGGDiagram. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Tgg Rules</em>' containment reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getTGGDiagram_TggRules()
	 * @model containment="true"
	 * @generated
	 */
	EList<TGGRule> getTggRules();

	/**
	 * Returns the value of the '<em><b>Rule Set ID</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The ID
	 * of this rule set. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Rule Set ID</em>' attribute.
	 * @see #setRuleSetID(String)
	 * @see de.hpi.sam.tgg.TggPackage#getTGGDiagram_RuleSetID()
	 * @model
	 * @generated
	 */
	String getRuleSetID();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.TGGDiagram#getRuleSetID <em>Rule Set ID</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Rule Set ID</em>' attribute.
	 * @see #getRuleSetID()
	 * @generated
	 */
	void setRuleSetID(String value);

} // TGGDiagram
