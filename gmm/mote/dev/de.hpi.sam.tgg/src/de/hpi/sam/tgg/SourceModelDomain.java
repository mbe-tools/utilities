/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Source Model Domain</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see de.hpi.sam.tgg.TggPackage#getSourceModelDomain()
 * @model
 * @generated
 */
public interface SourceModelDomain extends ModelDomain {
} // SourceModelDomain
