/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl;
import de.hpi.sam.tgg.CorrespondenceDomain;
import de.hpi.sam.tgg.CorrespondenceElement;
import de.hpi.sam.tgg.TggPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Correspondence Domain</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.impl.CorrespondenceDomainImpl#getCorrespondenceElements <em>Correspondence Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CorrespondenceDomainImpl extends NamedElementImpl implements
		CorrespondenceDomain {
	/**
	 * The cached value of the '{@link #getCorrespondenceElements()
	 * <em>Correspondence Elements</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCorrespondenceElements()
	 * @generated
	 * @ordered
	 */
	protected EList<CorrespondenceElement> correspondenceElements;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrespondenceDomainImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.CORRESPONDENCE_DOMAIN;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CorrespondenceElement> getCorrespondenceElements() {
		if (correspondenceElements == null) {
			correspondenceElements = new EObjectContainmentEList<CorrespondenceElement>(
					CorrespondenceElement.class, this,
					TggPackage.CORRESPONDENCE_DOMAIN__CORRESPONDENCE_ELEMENTS);
		}
		return correspondenceElements;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_DOMAIN__CORRESPONDENCE_ELEMENTS:
			return ((InternalEList<?>) getCorrespondenceElements())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_DOMAIN__CORRESPONDENCE_ELEMENTS:
			return getCorrespondenceElements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_DOMAIN__CORRESPONDENCE_ELEMENTS:
			getCorrespondenceElements().clear();
			getCorrespondenceElements().addAll(
					(Collection<? extends CorrespondenceElement>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_DOMAIN__CORRESPONDENCE_ELEMENTS:
			getCorrespondenceElements().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_DOMAIN__CORRESPONDENCE_ELEMENTS:
			return correspondenceElements != null
					&& !correspondenceElements.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // CorrespondenceDomainImpl
