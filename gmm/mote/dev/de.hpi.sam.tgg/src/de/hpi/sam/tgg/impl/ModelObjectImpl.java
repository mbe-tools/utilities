/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TggPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Model Object</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelObjectImpl#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelObjectImpl#getConstraintExpressions <em>Constraint Expressions</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelObjectImpl#getAttributeAssignments <em>Attribute Assignments</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelObjectImpl#getOutgoingModelLinks <em>Outgoing Model Links</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelObjectImpl#getIncomingModelLinks <em>Incoming Model Links</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.ModelObjectImpl#getPostCreationExpressions <em>Post Creation Expressions</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ModelObjectImpl extends ModelElementImpl implements ModelObject {
	/**
	 * The cached value of the '{@link #getClassifier() <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClassifier()
	 * @generated
	 * @ordered
	 */
	protected EClassifier classifier;

	/**
	 * The cached value of the '{@link #getConstraintExpressions()
	 * <em>Constraint Expressions</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getConstraintExpressions()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> constraintExpressions;

	/**
	 * The cached value of the '{@link #getAttributeAssignments()
	 * <em>Attribute Assignments</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getAttributeAssignments()
	 * @generated
	 * @ordered
	 */
	protected EList<AttributeAssignment> attributeAssignments;

	/**
	 * The cached value of the '{@link #getOutgoingModelLinks() <em>Outgoing Model Links</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutgoingModelLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<ModelLink> outgoingModelLinks;

	/**
	 * The cached value of the '{@link #getIncomingModelLinks() <em>Incoming Model Links</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncomingModelLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<ModelLink> incomingModelLinks;

	/**
	 * The cached value of the '{@link #getPostCreationExpressions()
	 * <em>Post Creation Expressions</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getPostCreationExpressions()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> postCreationExpressions;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.MODEL_OBJECT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier getClassifier() {
		if (classifier != null && classifier.eIsProxy()) {
			InternalEObject oldClassifier = (InternalEObject) classifier;
			classifier = (EClassifier) eResolveProxy(oldClassifier);
			if (classifier != oldClassifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TggPackage.MODEL_OBJECT__CLASSIFIER, oldClassifier,
							classifier));
			}
		}
		return classifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier basicGetClassifier() {
		return classifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassifier(EClassifier newClassifier) {
		EClassifier oldClassifier = classifier;
		classifier = newClassifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.MODEL_OBJECT__CLASSIFIER, oldClassifier,
					classifier));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getConstraintExpressions() {
		if (constraintExpressions == null) {
			constraintExpressions = new EObjectContainmentEList<Expression>(
					Expression.class, this,
					TggPackage.MODEL_OBJECT__CONSTRAINT_EXPRESSIONS);
		}
		return constraintExpressions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeAssignment> getAttributeAssignments() {
		if (attributeAssignments == null) {
			attributeAssignments = new EObjectContainmentEList<AttributeAssignment>(
					AttributeAssignment.class, this,
					TggPackage.MODEL_OBJECT__ATTRIBUTE_ASSIGNMENTS);
		}
		return attributeAssignments;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelLink> getOutgoingModelLinks() {
		if (outgoingModelLinks == null) {
			outgoingModelLinks = new EObjectWithInverseResolvingEList<ModelLink>(
					ModelLink.class, this,
					TggPackage.MODEL_OBJECT__OUTGOING_MODEL_LINKS,
					TggPackage.MODEL_LINK__SOURCE);
		}
		return outgoingModelLinks;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelLink> getIncomingModelLinks() {
		if (incomingModelLinks == null) {
			incomingModelLinks = new EObjectWithInverseResolvingEList<ModelLink>(
					ModelLink.class, this,
					TggPackage.MODEL_OBJECT__INCOMING_MODEL_LINKS,
					TggPackage.MODEL_LINK__TARGET);
		}
		return incomingModelLinks;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getPostCreationExpressions() {
		if (postCreationExpressions == null) {
			postCreationExpressions = new EObjectContainmentEList<Expression>(
					Expression.class, this,
					TggPackage.MODEL_OBJECT__POST_CREATION_EXPRESSIONS);
		}
		return postCreationExpressions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.MODEL_OBJECT__OUTGOING_MODEL_LINKS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOutgoingModelLinks())
					.basicAdd(otherEnd, msgs);
		case TggPackage.MODEL_OBJECT__INCOMING_MODEL_LINKS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getIncomingModelLinks())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.MODEL_OBJECT__CONSTRAINT_EXPRESSIONS:
			return ((InternalEList<?>) getConstraintExpressions()).basicRemove(
					otherEnd, msgs);
		case TggPackage.MODEL_OBJECT__ATTRIBUTE_ASSIGNMENTS:
			return ((InternalEList<?>) getAttributeAssignments()).basicRemove(
					otherEnd, msgs);
		case TggPackage.MODEL_OBJECT__OUTGOING_MODEL_LINKS:
			return ((InternalEList<?>) getOutgoingModelLinks()).basicRemove(
					otherEnd, msgs);
		case TggPackage.MODEL_OBJECT__INCOMING_MODEL_LINKS:
			return ((InternalEList<?>) getIncomingModelLinks()).basicRemove(
					otherEnd, msgs);
		case TggPackage.MODEL_OBJECT__POST_CREATION_EXPRESSIONS:
			return ((InternalEList<?>) getPostCreationExpressions())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TggPackage.MODEL_OBJECT__CLASSIFIER:
			if (resolve)
				return getClassifier();
			return basicGetClassifier();
		case TggPackage.MODEL_OBJECT__CONSTRAINT_EXPRESSIONS:
			return getConstraintExpressions();
		case TggPackage.MODEL_OBJECT__ATTRIBUTE_ASSIGNMENTS:
			return getAttributeAssignments();
		case TggPackage.MODEL_OBJECT__OUTGOING_MODEL_LINKS:
			return getOutgoingModelLinks();
		case TggPackage.MODEL_OBJECT__INCOMING_MODEL_LINKS:
			return getIncomingModelLinks();
		case TggPackage.MODEL_OBJECT__POST_CREATION_EXPRESSIONS:
			return getPostCreationExpressions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TggPackage.MODEL_OBJECT__CLASSIFIER:
			setClassifier((EClassifier) newValue);
			return;
		case TggPackage.MODEL_OBJECT__CONSTRAINT_EXPRESSIONS:
			getConstraintExpressions().clear();
			getConstraintExpressions().addAll(
					(Collection<? extends Expression>) newValue);
			return;
		case TggPackage.MODEL_OBJECT__ATTRIBUTE_ASSIGNMENTS:
			getAttributeAssignments().clear();
			getAttributeAssignments().addAll(
					(Collection<? extends AttributeAssignment>) newValue);
			return;
		case TggPackage.MODEL_OBJECT__OUTGOING_MODEL_LINKS:
			getOutgoingModelLinks().clear();
			getOutgoingModelLinks().addAll(
					(Collection<? extends ModelLink>) newValue);
			return;
		case TggPackage.MODEL_OBJECT__INCOMING_MODEL_LINKS:
			getIncomingModelLinks().clear();
			getIncomingModelLinks().addAll(
					(Collection<? extends ModelLink>) newValue);
			return;
		case TggPackage.MODEL_OBJECT__POST_CREATION_EXPRESSIONS:
			getPostCreationExpressions().clear();
			getPostCreationExpressions().addAll(
					(Collection<? extends Expression>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TggPackage.MODEL_OBJECT__CLASSIFIER:
			setClassifier((EClassifier) null);
			return;
		case TggPackage.MODEL_OBJECT__CONSTRAINT_EXPRESSIONS:
			getConstraintExpressions().clear();
			return;
		case TggPackage.MODEL_OBJECT__ATTRIBUTE_ASSIGNMENTS:
			getAttributeAssignments().clear();
			return;
		case TggPackage.MODEL_OBJECT__OUTGOING_MODEL_LINKS:
			getOutgoingModelLinks().clear();
			return;
		case TggPackage.MODEL_OBJECT__INCOMING_MODEL_LINKS:
			getIncomingModelLinks().clear();
			return;
		case TggPackage.MODEL_OBJECT__POST_CREATION_EXPRESSIONS:
			getPostCreationExpressions().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TggPackage.MODEL_OBJECT__CLASSIFIER:
			return classifier != null;
		case TggPackage.MODEL_OBJECT__CONSTRAINT_EXPRESSIONS:
			return constraintExpressions != null
					&& !constraintExpressions.isEmpty();
		case TggPackage.MODEL_OBJECT__ATTRIBUTE_ASSIGNMENTS:
			return attributeAssignments != null
					&& !attributeAssignments.isEmpty();
		case TggPackage.MODEL_OBJECT__OUTGOING_MODEL_LINKS:
			return outgoingModelLinks != null && !outgoingModelLinks.isEmpty();
		case TggPackage.MODEL_OBJECT__INCOMING_MODEL_LINKS:
			return incomingModelLinks != null && !incomingModelLinks.isEmpty();
		case TggPackage.MODEL_OBJECT__POST_CREATION_EXPRESSIONS:
			return postCreationExpressions != null
					&& !postCreationExpressions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // ModelObjectImpl
