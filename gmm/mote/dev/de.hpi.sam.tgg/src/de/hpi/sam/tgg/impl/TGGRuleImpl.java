package de.hpi.sam.tgg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl;
import de.hpi.sam.tgg.CorrespondenceDomain;
import de.hpi.sam.tgg.RuleElement;
import de.hpi.sam.tgg.RuleVariable;
import de.hpi.sam.tgg.SourceModelDomain;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TargetModelDomain;
import de.hpi.sam.tgg.TggFactory;
import de.hpi.sam.tgg.TggPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>TGG Rule</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.impl.TGGRuleImpl#getCorrespondenceDomain <em>Correspondence Domain</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.TGGRuleImpl#getConstraintExpressions <em>Constraint Expressions</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.TGGRuleImpl#getSourceDomain <em>Source Domain</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.TGGRuleImpl#getTargetDomain <em>Target Domain</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.TGGRuleImpl#getInputElements <em>Input Elements</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.TGGRuleImpl#getRuleVariables <em>Rule Variables</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.TGGRuleImpl#isIsAxiom <em>Is Axiom</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.TGGRuleImpl#getForwardConstraintExpressions <em>Forward Constraint Expressions</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.TGGRuleImpl#getReverseConstraintExpressions <em>Reverse Constraint Expressions</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.TGGRuleImpl#isDisabled <em>Disabled</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TGGRuleImpl extends NamedElementImpl implements TGGRule {
	/**
	 * The cached value of the '{@link #getCorrespondenceDomain()
	 * <em>Correspondence Domain</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCorrespondenceDomain()
	 * @generated
	 * @ordered
	 */
	protected CorrespondenceDomain correspondenceDomain;

	/**
	 * The cached value of the '{@link #getConstraintExpressions()
	 * <em>Constraint Expressions</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getConstraintExpressions()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> constraintExpressions;

	/**
	 * The cached value of the '{@link #getSourceDomain() <em>Source Domain</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceDomain()
	 * @generated
	 * @ordered
	 */
	protected SourceModelDomain sourceDomain;

	/**
	 * The cached value of the '{@link #getTargetDomain() <em>Target Domain</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetDomain()
	 * @generated
	 * @ordered
	 */
	protected TargetModelDomain targetDomain;

	/**
	 * The cached value of the '{@link #getInputElements() <em>Input Elements</em>}' reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getInputElements()
	 * @generated
	 * @ordered
	 */
	protected EList<RuleElement> inputElements;

	/**
	 * The cached value of the '{@link #getRuleVariables() <em>Rule Variables</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getRuleVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<RuleVariable> ruleVariables;

	/**
	 * The default value of the '{@link #isIsAxiom() <em>Is Axiom</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isIsAxiom()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_AXIOM_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsAxiom() <em>Is Axiom</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isIsAxiom()
	 * @generated
	 * @ordered
	 */
	protected boolean isAxiom = IS_AXIOM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getForwardConstraintExpressions() <em>Forward Constraint Expressions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getForwardConstraintExpressions()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> forwardConstraintExpressions;

	/**
	 * The cached value of the '{@link #getReverseConstraintExpressions() <em>Reverse Constraint Expressions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReverseConstraintExpressions()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> reverseConstraintExpressions;

	/**
	 * The default value of the '{@link #isDisabled() <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisabled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DISABLED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDisabled() <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisabled()
	 * @generated
	 * @ordered
	 */
	protected boolean disabled = DISABLED_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	protected TGGRuleImpl() {
		super();
		setSourceDomain(TggFactory.eINSTANCE.createSourceModelDomain());
		setCorrespondenceDomain(TggFactory.eINSTANCE
				.createCorrespondenceDomain());
		setTargetDomain(TggFactory.eINSTANCE.createTargetModelDomain());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.TGG_RULE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CorrespondenceDomain getCorrespondenceDomain() {
		return correspondenceDomain;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCorrespondenceDomain(
			CorrespondenceDomain newCorrespondenceDomain, NotificationChain msgs) {
		CorrespondenceDomain oldCorrespondenceDomain = correspondenceDomain;
		correspondenceDomain = newCorrespondenceDomain;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					TggPackage.TGG_RULE__CORRESPONDENCE_DOMAIN,
					oldCorrespondenceDomain, newCorrespondenceDomain);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCorrespondenceDomain(
			CorrespondenceDomain newCorrespondenceDomain) {
		if (newCorrespondenceDomain != correspondenceDomain) {
			NotificationChain msgs = null;
			if (correspondenceDomain != null)
				msgs = ((InternalEObject) correspondenceDomain).eInverseRemove(
						this, EOPPOSITE_FEATURE_BASE
								- TggPackage.TGG_RULE__CORRESPONDENCE_DOMAIN,
						null, msgs);
			if (newCorrespondenceDomain != null)
				msgs = ((InternalEObject) newCorrespondenceDomain).eInverseAdd(
						this, EOPPOSITE_FEATURE_BASE
								- TggPackage.TGG_RULE__CORRESPONDENCE_DOMAIN,
						null, msgs);
			msgs = basicSetCorrespondenceDomain(newCorrespondenceDomain, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.TGG_RULE__CORRESPONDENCE_DOMAIN,
					newCorrespondenceDomain, newCorrespondenceDomain));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getConstraintExpressions() {
		if (constraintExpressions == null) {
			constraintExpressions = new EObjectContainmentEList<Expression>(
					Expression.class, this,
					TggPackage.TGG_RULE__CONSTRAINT_EXPRESSIONS);
		}
		return constraintExpressions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SourceModelDomain getSourceDomain() {
		return sourceDomain;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceDomain(
			SourceModelDomain newSourceDomain, NotificationChain msgs) {
		SourceModelDomain oldSourceDomain = sourceDomain;
		sourceDomain = newSourceDomain;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, TggPackage.TGG_RULE__SOURCE_DOMAIN,
					oldSourceDomain, newSourceDomain);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceDomain(SourceModelDomain newSourceDomain) {
		if (newSourceDomain != sourceDomain) {
			NotificationChain msgs = null;
			if (sourceDomain != null)
				msgs = ((InternalEObject) sourceDomain).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- TggPackage.TGG_RULE__SOURCE_DOMAIN, null,
						msgs);
			if (newSourceDomain != null)
				msgs = ((InternalEObject) newSourceDomain).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- TggPackage.TGG_RULE__SOURCE_DOMAIN, null,
						msgs);
			msgs = basicSetSourceDomain(newSourceDomain, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.TGG_RULE__SOURCE_DOMAIN, newSourceDomain,
					newSourceDomain));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TargetModelDomain getTargetDomain() {
		return targetDomain;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTargetDomain(
			TargetModelDomain newTargetDomain, NotificationChain msgs) {
		TargetModelDomain oldTargetDomain = targetDomain;
		targetDomain = newTargetDomain;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, TggPackage.TGG_RULE__TARGET_DOMAIN,
					oldTargetDomain, newTargetDomain);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetDomain(TargetModelDomain newTargetDomain) {
		if (newTargetDomain != targetDomain) {
			NotificationChain msgs = null;
			if (targetDomain != null)
				msgs = ((InternalEObject) targetDomain).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- TggPackage.TGG_RULE__TARGET_DOMAIN, null,
						msgs);
			if (newTargetDomain != null)
				msgs = ((InternalEObject) newTargetDomain).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- TggPackage.TGG_RULE__TARGET_DOMAIN, null,
						msgs);
			msgs = basicSetTargetDomain(newTargetDomain, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.TGG_RULE__TARGET_DOMAIN, newTargetDomain,
					newTargetDomain));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RuleElement> getInputElements() {
		if (inputElements == null) {
			inputElements = new EObjectResolvingEList<RuleElement>(
					RuleElement.class, this,
					TggPackage.TGG_RULE__INPUT_ELEMENTS);
		}
		return inputElements;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RuleVariable> getRuleVariables() {
		if (ruleVariables == null) {
			ruleVariables = new EObjectContainmentEList<RuleVariable>(
					RuleVariable.class, this,
					TggPackage.TGG_RULE__RULE_VARIABLES);
		}
		return ruleVariables;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsAxiom() {
		return isAxiom;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsAxiom(boolean newIsAxiom) {
		boolean oldIsAxiom = isAxiom;
		isAxiom = newIsAxiom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.TGG_RULE__IS_AXIOM, oldIsAxiom, isAxiom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getForwardConstraintExpressions() {
		if (forwardConstraintExpressions == null) {
			forwardConstraintExpressions = new EObjectContainmentEList<Expression>(
					Expression.class, this,
					TggPackage.TGG_RULE__FORWARD_CONSTRAINT_EXPRESSIONS);
		}
		return forwardConstraintExpressions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getReverseConstraintExpressions() {
		if (reverseConstraintExpressions == null) {
			reverseConstraintExpressions = new EObjectContainmentEList<Expression>(
					Expression.class, this,
					TggPackage.TGG_RULE__REVERSE_CONSTRAINT_EXPRESSIONS);
		}
		return reverseConstraintExpressions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisabled(boolean newDisabled) {
		boolean oldDisabled = disabled;
		disabled = newDisabled;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.TGG_RULE__DISABLED, oldDisabled, disabled));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.TGG_RULE__CORRESPONDENCE_DOMAIN:
			return basicSetCorrespondenceDomain(null, msgs);
		case TggPackage.TGG_RULE__CONSTRAINT_EXPRESSIONS:
			return ((InternalEList<?>) getConstraintExpressions()).basicRemove(
					otherEnd, msgs);
		case TggPackage.TGG_RULE__SOURCE_DOMAIN:
			return basicSetSourceDomain(null, msgs);
		case TggPackage.TGG_RULE__TARGET_DOMAIN:
			return basicSetTargetDomain(null, msgs);
		case TggPackage.TGG_RULE__RULE_VARIABLES:
			return ((InternalEList<?>) getRuleVariables()).basicRemove(
					otherEnd, msgs);
		case TggPackage.TGG_RULE__FORWARD_CONSTRAINT_EXPRESSIONS:
			return ((InternalEList<?>) getForwardConstraintExpressions())
					.basicRemove(otherEnd, msgs);
		case TggPackage.TGG_RULE__REVERSE_CONSTRAINT_EXPRESSIONS:
			return ((InternalEList<?>) getReverseConstraintExpressions())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TggPackage.TGG_RULE__CORRESPONDENCE_DOMAIN:
			return getCorrespondenceDomain();
		case TggPackage.TGG_RULE__CONSTRAINT_EXPRESSIONS:
			return getConstraintExpressions();
		case TggPackage.TGG_RULE__SOURCE_DOMAIN:
			return getSourceDomain();
		case TggPackage.TGG_RULE__TARGET_DOMAIN:
			return getTargetDomain();
		case TggPackage.TGG_RULE__INPUT_ELEMENTS:
			return getInputElements();
		case TggPackage.TGG_RULE__RULE_VARIABLES:
			return getRuleVariables();
		case TggPackage.TGG_RULE__IS_AXIOM:
			return isIsAxiom();
		case TggPackage.TGG_RULE__FORWARD_CONSTRAINT_EXPRESSIONS:
			return getForwardConstraintExpressions();
		case TggPackage.TGG_RULE__REVERSE_CONSTRAINT_EXPRESSIONS:
			return getReverseConstraintExpressions();
		case TggPackage.TGG_RULE__DISABLED:
			return isDisabled();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TggPackage.TGG_RULE__CORRESPONDENCE_DOMAIN:
			setCorrespondenceDomain((CorrespondenceDomain) newValue);
			return;
		case TggPackage.TGG_RULE__CONSTRAINT_EXPRESSIONS:
			getConstraintExpressions().clear();
			getConstraintExpressions().addAll(
					(Collection<? extends Expression>) newValue);
			return;
		case TggPackage.TGG_RULE__SOURCE_DOMAIN:
			setSourceDomain((SourceModelDomain) newValue);
			return;
		case TggPackage.TGG_RULE__TARGET_DOMAIN:
			setTargetDomain((TargetModelDomain) newValue);
			return;
		case TggPackage.TGG_RULE__INPUT_ELEMENTS:
			getInputElements().clear();
			getInputElements().addAll(
					(Collection<? extends RuleElement>) newValue);
			return;
		case TggPackage.TGG_RULE__RULE_VARIABLES:
			getRuleVariables().clear();
			getRuleVariables().addAll(
					(Collection<? extends RuleVariable>) newValue);
			return;
		case TggPackage.TGG_RULE__IS_AXIOM:
			setIsAxiom((Boolean) newValue);
			return;
		case TggPackage.TGG_RULE__FORWARD_CONSTRAINT_EXPRESSIONS:
			getForwardConstraintExpressions().clear();
			getForwardConstraintExpressions().addAll(
					(Collection<? extends Expression>) newValue);
			return;
		case TggPackage.TGG_RULE__REVERSE_CONSTRAINT_EXPRESSIONS:
			getReverseConstraintExpressions().clear();
			getReverseConstraintExpressions().addAll(
					(Collection<? extends Expression>) newValue);
			return;
		case TggPackage.TGG_RULE__DISABLED:
			setDisabled((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TggPackage.TGG_RULE__CORRESPONDENCE_DOMAIN:
			setCorrespondenceDomain((CorrespondenceDomain) null);
			return;
		case TggPackage.TGG_RULE__CONSTRAINT_EXPRESSIONS:
			getConstraintExpressions().clear();
			return;
		case TggPackage.TGG_RULE__SOURCE_DOMAIN:
			setSourceDomain((SourceModelDomain) null);
			return;
		case TggPackage.TGG_RULE__TARGET_DOMAIN:
			setTargetDomain((TargetModelDomain) null);
			return;
		case TggPackage.TGG_RULE__INPUT_ELEMENTS:
			getInputElements().clear();
			return;
		case TggPackage.TGG_RULE__RULE_VARIABLES:
			getRuleVariables().clear();
			return;
		case TggPackage.TGG_RULE__IS_AXIOM:
			setIsAxiom(IS_AXIOM_EDEFAULT);
			return;
		case TggPackage.TGG_RULE__FORWARD_CONSTRAINT_EXPRESSIONS:
			getForwardConstraintExpressions().clear();
			return;
		case TggPackage.TGG_RULE__REVERSE_CONSTRAINT_EXPRESSIONS:
			getReverseConstraintExpressions().clear();
			return;
		case TggPackage.TGG_RULE__DISABLED:
			setDisabled(DISABLED_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TggPackage.TGG_RULE__CORRESPONDENCE_DOMAIN:
			return correspondenceDomain != null;
		case TggPackage.TGG_RULE__CONSTRAINT_EXPRESSIONS:
			return constraintExpressions != null
					&& !constraintExpressions.isEmpty();
		case TggPackage.TGG_RULE__SOURCE_DOMAIN:
			return sourceDomain != null;
		case TggPackage.TGG_RULE__TARGET_DOMAIN:
			return targetDomain != null;
		case TggPackage.TGG_RULE__INPUT_ELEMENTS:
			return inputElements != null && !inputElements.isEmpty();
		case TggPackage.TGG_RULE__RULE_VARIABLES:
			return ruleVariables != null && !ruleVariables.isEmpty();
		case TggPackage.TGG_RULE__IS_AXIOM:
			return isAxiom != IS_AXIOM_EDEFAULT;
		case TggPackage.TGG_RULE__FORWARD_CONSTRAINT_EXPRESSIONS:
			return forwardConstraintExpressions != null
					&& !forwardConstraintExpressions.isEmpty();
		case TggPackage.TGG_RULE__REVERSE_CONSTRAINT_EXPRESSIONS:
			return reverseConstraintExpressions != null
					&& !reverseConstraintExpressions.isEmpty();
		case TggPackage.TGG_RULE__DISABLED:
			return disabled != DISABLED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isAxiom: ");
		result.append(isAxiom);
		result.append(", disabled: ");
		result.append(disabled);
		result.append(')');
		return result.toString();
	}

} // TGGRuleImpl