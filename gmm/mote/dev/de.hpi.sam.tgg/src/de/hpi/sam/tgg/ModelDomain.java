/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

import org.eclipse.emf.common.util.EList;

import de.hpi.sam.storyDiagramEcore.NamedElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Model Domain</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The ModelDomain contains those elements of a TGGRule that belong to the same model domain.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.ModelDomain#getModelElements <em>Model Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.TggPackage#getModelDomain()
 * @model abstract="true"
 * @generated
 */
public interface ModelDomain extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Model Elements</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link de.hpi.sam.tgg.ModelElement}. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The TGGElements contained in
	 * this domain. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Model Elements</em>' containment reference
	 *         list.
	 * @see de.hpi.sam.tgg.TggPackage#getModelDomain_ModelElements()
	 * @model containment="true"
	 * @generated
	 */
	EList<ModelElement> getModelElements();

} // ModelDomain
