/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl;
import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TggPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>TGG Diagram</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.impl.TGGDiagramImpl#getTggRules <em>Tgg Rules</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.TGGDiagramImpl#getRuleSetID <em>Rule Set ID</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TGGDiagramImpl extends NamedElementImpl implements TGGDiagram {
	/**
	 * The cached value of the '{@link #getTggRules() <em>Tgg Rules</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTggRules()
	 * @generated
	 * @ordered
	 */
	protected EList<TGGRule> tggRules;

	/**
	 * The default value of the '{@link #getRuleSetID() <em>Rule Set ID</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuleSetID()
	 * @generated
	 * @ordered
	 */
	protected static final String RULE_SET_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRuleSetID() <em>Rule Set ID</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuleSetID()
	 * @generated
	 * @ordered
	 */
	protected String ruleSetID = RULE_SET_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected TGGDiagramImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.TGG_DIAGRAM;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TGGRule> getTggRules() {
		if (tggRules == null) {
			tggRules = new EObjectContainmentEList<TGGRule>(TGGRule.class,
					this, TggPackage.TGG_DIAGRAM__TGG_RULES);
		}
		return tggRules;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getRuleSetID() {
		return ruleSetID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleSetID(String newRuleSetID) {
		String oldRuleSetID = ruleSetID;
		ruleSetID = newRuleSetID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.TGG_DIAGRAM__RULE_SET_ID, oldRuleSetID,
					ruleSetID));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.TGG_DIAGRAM__TGG_RULES:
			return ((InternalEList<?>) getTggRules()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TggPackage.TGG_DIAGRAM__TGG_RULES:
			return getTggRules();
		case TggPackage.TGG_DIAGRAM__RULE_SET_ID:
			return getRuleSetID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TggPackage.TGG_DIAGRAM__TGG_RULES:
			getTggRules().clear();
			getTggRules().addAll((Collection<? extends TGGRule>) newValue);
			return;
		case TggPackage.TGG_DIAGRAM__RULE_SET_ID:
			setRuleSetID((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TggPackage.TGG_DIAGRAM__TGG_RULES:
			getTggRules().clear();
			return;
		case TggPackage.TGG_DIAGRAM__RULE_SET_ID:
			setRuleSetID(RULE_SET_ID_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TggPackage.TGG_DIAGRAM__TGG_RULES:
			return tggRules != null && !tggRules.isEmpty();
		case TggPackage.TGG_DIAGRAM__RULE_SET_ID:
			return RULE_SET_ID_EDEFAULT == null ? ruleSetID != null
					: !RULE_SET_ID_EDEFAULT.equals(ruleSetID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ruleSetID: ");
		result.append(ruleSetID);
		result.append(')');
		return result.toString();
	}

} // TGGDiagramImpl
