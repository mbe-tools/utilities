/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

import de.hpi.sam.storyDiagramEcore.sdm.ExternalReference;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Model Link</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * ModelLink represents links in a TGGRule. It has a reference to the EReference it is representing, references to the source and target ModelObjects, and a reference to the opposite ModelLink, if applicable.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.ModelLink#getEReference <em>EReference</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelLink#getSource <em>Source</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelLink#getTarget <em>Target</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelLink#getOppositeTGGLink <em>Opposite TGG Link</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelLink#getEOppositeReference <em>EOpposite Reference</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelLink#getLinkPositionConstraint <em>Link Position Constraint</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelLink#getExternalReference <em>External Reference</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.ModelLink#isExternal <em>External</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.TggPackage#getModelLink()
 * @model
 * @generated
 */
public interface ModelLink extends ModelElement {
	/**
	 * Returns the value of the '<em><b>EReference</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * EReference that is represented by this ModelLink. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>EReference</em>' reference.
	 * @see #setEReference(EReference)
	 * @see de.hpi.sam.tgg.TggPackage#getModelLink_EReference()
	 * @model
	 * @generated
	 */
	EReference getEReference();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ModelLink#getEReference <em>EReference</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>EReference</em>' reference.
	 * @see #getEReference()
	 * @generated
	 */
	void setEReference(EReference value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.tgg.ModelObject#getOutgoingModelLinks
	 * <em>Outgoing Model Links</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The source ModelObject of this
	 * ModelLink. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(ModelObject)
	 * @see de.hpi.sam.tgg.TggPackage#getModelLink_Source()
	 * @see de.hpi.sam.tgg.ModelObject#getOutgoingModelLinks
	 * @model opposite="outgoingModelLinks"
	 * @generated
	 */
	ModelObject getSource();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ModelLink#getSource
	 * <em>Source</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(ModelObject value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.tgg.ModelObject#getIncomingModelLinks
	 * <em>Incoming Model Links</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The target ModelObject of this
	 * ModelLink. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(ModelObject)
	 * @see de.hpi.sam.tgg.TggPackage#getModelLink_Target()
	 * @see de.hpi.sam.tgg.ModelObject#getIncomingModelLinks
	 * @model opposite="incomingModelLinks"
	 * @generated
	 */
	ModelObject getTarget();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ModelLink#getTarget
	 * <em>Target</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ModelObject value);

	/**
	 * Returns the value of the '<em><b>Opposite TGG Link</b></em>' reference.
	 * It is bidirectional and its opposite is '
	 * {@link de.hpi.sam.tgg.ModelLink#getOppositeTGGLink
	 * <em>Opposite TGG Link</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * --> <!-- begin-model-doc --> The opposite ModelLink if applicable. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Opposite TGG Link</em>' reference.
	 * @see #setOppositeTGGLink(ModelLink)
	 * @see de.hpi.sam.tgg.TggPackage#getModelLink_OppositeTGGLink()
	 * @see de.hpi.sam.tgg.ModelLink#getOppositeTGGLink
	 * @model opposite="oppositeTGGLink"
	 * @generated
	 */
	ModelLink getOppositeTGGLink();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ModelLink#getOppositeTGGLink <em>Opposite TGG Link</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Opposite TGG Link</em>' reference.
	 * @see #getOppositeTGGLink()
	 * @generated
	 */
	void setOppositeTGGLink(ModelLink value);

	/**
	 * Returns the value of the '<em><b>EOpposite Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The EReference that is represented by this ModelLink.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>EOpposite Reference</em>' reference.
	 * @see #setEOppositeReference(EReference)
	 * @see de.hpi.sam.tgg.TggPackage#getModelLink_EOppositeReference()
	 * @model
	 * @generated
	 */
	EReference getEOppositeReference();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ModelLink#getEOppositeReference <em>EOpposite Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EOpposite Reference</em>' reference.
	 * @see #getEOppositeReference()
	 * @generated
	 */
	void setEOppositeReference(EReference value);

	/**
	 * Returns the value of the '<em><b>Link Position Constraint</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.tgg.ModelLinkPositionConstraint#getModelLink <em>Model Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link Position Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link Position Constraint</em>' containment reference.
	 * @see #setLinkPositionConstraint(ModelLinkPositionConstraint)
	 * @see de.hpi.sam.tgg.TggPackage#getModelLink_LinkPositionConstraint()
	 * @see de.hpi.sam.tgg.ModelLinkPositionConstraint#getModelLink
	 * @model opposite="modelLink" containment="true"
	 * @generated
	 */
	ModelLinkPositionConstraint getLinkPositionConstraint();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ModelLink#getLinkPositionConstraint <em>Link Position Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Link Position Constraint</em>' containment reference.
	 * @see #getLinkPositionConstraint()
	 * @generated
	 */
	void setLinkPositionConstraint(ModelLinkPositionConstraint value);

	/**
	 * Returns the value of the '<em><b>External Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Reference</em>' containment reference.
	 * @see #setExternalReference(ExternalReference)
	 * @see de.hpi.sam.tgg.TggPackage#getModelLink_ExternalReference()
	 * @model containment="true"
	 * @generated
	 */
	ExternalReference getExternalReference();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ModelLink#getExternalReference <em>External Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Reference</em>' containment reference.
	 * @see #getExternalReference()
	 * @generated
	 */
	void setExternalReference(ExternalReference value);

	/**
	 * Returns the value of the '<em><b>External</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External</em>' attribute.
	 * @see #setExternal(boolean)
	 * @see de.hpi.sam.tgg.TggPackage#getModelLink_External()
	 * @model required="true"
	 * @generated
	 */
	boolean isExternal();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.ModelLink#isExternal <em>External</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External</em>' attribute.
	 * @see #isExternal()
	 * @generated
	 */
	void setExternal(boolean value);

} // ModelLink
