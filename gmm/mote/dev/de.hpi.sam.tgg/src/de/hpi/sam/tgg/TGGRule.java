/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

import org.eclipse.emf.common.util.EList;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>TGG Rule</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A TGGRule contains at least two model domains and a correspondence domain.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.TGGRule#getCorrespondenceDomain <em>Correspondence Domain</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.TGGRule#getConstraintExpressions <em>Constraint Expressions</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.TGGRule#getSourceDomain <em>Source Domain</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.TGGRule#getTargetDomain <em>Target Domain</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.TGGRule#getInputElements <em>Input Elements</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.TGGRule#getRuleVariables <em>Rule Variables</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.TGGRule#isIsAxiom <em>Is Axiom</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.TGGRule#getForwardConstraintExpressions <em>Forward Constraint Expressions</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.TGGRule#getReverseConstraintExpressions <em>Reverse Constraint Expressions</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.TGGRule#isDisabled <em>Disabled</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.TggPackage#getTGGRule()
 * @model
 * @generated
 */
public interface TGGRule extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Correspondence Domain</b></em>'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> The correspondence domain of this TGGRule. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Correspondence Domain</em>' containment
	 *         reference.
	 * @see #setCorrespondenceDomain(CorrespondenceDomain)
	 * @see de.hpi.sam.tgg.TggPackage#getTGGRule_CorrespondenceDomain()
	 * @model containment="true" required="true"
	 * @generated
	 */
	CorrespondenceDomain getCorrespondenceDomain();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.TGGRule#getCorrespondenceDomain
	 * <em>Correspondence Domain</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Correspondence Domain</em>'
	 *            containment reference.
	 * @see #getCorrespondenceDomain()
	 * @generated
	 */
	void setCorrespondenceDomain(CorrespondenceDomain value);

	/**
	 * Returns the value of the '<em><b>Constraint Expressions</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.expressions.Expression}. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> A
	 * constraint that must be fulfilled before the TGGRule can be executed.
	 * This is required if, e.g. the attribute values of two different
	 * TGGObjects are compared. Using the constraint of the TGGObject directly
	 * is not possible because not all TGGObjects may be matched when the
	 * constraint is evaluated. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Constraint Expressions</em>' containment
	 *         reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getTGGRule_ConstraintExpressions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getConstraintExpressions();

	/**
	 * Returns the value of the '<em><b>Source Domain</b></em>' containment
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> The source domain of the TGG rule. <!-- end-model-doc
	 * -->
	 * 
	 * @return the value of the '<em>Source Domain</em>' containment reference.
	 * @see #setSourceDomain(SourceModelDomain)
	 * @see de.hpi.sam.tgg.TggPackage#getTGGRule_SourceDomain()
	 * @model containment="true" required="true"
	 * @generated
	 */
	SourceModelDomain getSourceDomain();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.TGGRule#getSourceDomain <em>Source Domain</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Domain</em>' containment reference.
	 * @see #getSourceDomain()
	 * @generated
	 */
	void setSourceDomain(SourceModelDomain value);

	/**
	 * Returns the value of the '<em><b>Target Domain</b></em>' containment
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> The target domain of the TGG rule. <!-- end-model-doc
	 * -->
	 * 
	 * @return the value of the '<em>Target Domain</em>' containment reference.
	 * @see #setTargetDomain(TargetModelDomain)
	 * @see de.hpi.sam.tgg.TggPackage#getTGGRule_TargetDomain()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TargetModelDomain getTargetDomain();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.TGGRule#getTargetDomain <em>Target Domain</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Domain</em>' containment reference.
	 * @see #getTargetDomain()
	 * @generated
	 */
	void setTargetDomain(TargetModelDomain value);

	/**
	 * Returns the value of the '<em><b>Input Elements</b></em>' reference list.
	 * The list contents are of type {@link de.hpi.sam.tgg.RuleElement}. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Specifies which elements of the TGG rule will be bound to the input
	 * parameter of the transformation rule. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Input Elements</em>' reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getTGGRule_InputElements()
	 * @model upper="2"
	 * @generated
	 */
	EList<RuleElement> getInputElements();

	/**
	 * Returns the value of the '<em><b>Rule Variables</b></em>' containment reference list.
	 * The list contents are of type {@link de.hpi.sam.tgg.RuleVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Variables</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Variables</em>' containment reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getTGGRule_RuleVariables()
	 * @model containment="true"
	 * @generated
	 */
	EList<RuleVariable> getRuleVariables();

	/**
	 * Returns the value of the '<em><b>Is Axiom</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Axiom</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Is Axiom</em>' attribute.
	 * @see #setIsAxiom(boolean)
	 * @see de.hpi.sam.tgg.TggPackage#getTGGRule_IsAxiom()
	 * @model required="true"
	 * @generated
	 */
	boolean isIsAxiom();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.TGGRule#isIsAxiom
	 * <em>Is Axiom</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Is Axiom</em>' attribute.
	 * @see #isIsAxiom()
	 * @generated
	 */
	void setIsAxiom(boolean value);

	/**
	 * Returns the value of the '<em><b>Forward Constraint Expressions</b></em>' containment reference list.
	 * The list contents are of type {@link de.hpi.sam.storyDiagramEcore.expressions.Expression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A constraint that must be fulfilled before the TGGRule can be executed. This is required if, e.g. the attribute values of two different TGGObjects are compared. Using the constraint of the TGGObject directly is not possible because not all TGGObjects may be matched when the constraint is evaluated.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Forward Constraint Expressions</em>' containment reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getTGGRule_ForwardConstraintExpressions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getForwardConstraintExpressions();

	/**
	 * Returns the value of the '<em><b>Reverse Constraint Expressions</b></em>' containment reference list.
	 * The list contents are of type {@link de.hpi.sam.storyDiagramEcore.expressions.Expression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A constraint that must be fulfilled before the TGGRule can be executed. This is required if, e.g. the attribute values of two different TGGObjects are compared. Using the constraint of the TGGObject directly is not possible because not all TGGObjects may be matched when the constraint is evaluated.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Reverse Constraint Expressions</em>' containment reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getTGGRule_ReverseConstraintExpressions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getReverseConstraintExpressions();

	/**
	 * Returns the value of the '<em><b>Disabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disabled</em>' attribute.
	 * @see #setDisabled(boolean)
	 * @see de.hpi.sam.tgg.TggPackage#getTGGRule_Disabled()
	 * @model required="true"
	 * @generated
	 */
	boolean isDisabled();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.TGGRule#isDisabled <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disabled</em>' attribute.
	 * @see #isDisabled()
	 * @generated
	 */
	void setDisabled(boolean value);

} // TGGRule
