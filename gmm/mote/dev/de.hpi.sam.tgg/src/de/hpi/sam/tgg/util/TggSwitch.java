/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.tgg.*;
import de.hpi.sam.tgg.CorrespondenceDomain;
import de.hpi.sam.tgg.CorrespondenceElement;
import de.hpi.sam.tgg.CorrespondenceLink;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.ModelDomain;
import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.RuleElement;
import de.hpi.sam.tgg.RuleVariable;
import de.hpi.sam.tgg.SourceModelDomain;
import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TargetModelDomain;
import de.hpi.sam.tgg.TggPackage;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance
 * hierarchy. It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the
 * inheritance hierarchy until a non-null result is returned, which is the
 * result of the switch. <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.TggPackage
 * @generated
 */
public class TggSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static TggPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public TggSwitch() {
		if (modelPackage == null) {
			modelPackage = TggPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case TggPackage.TGG_DIAGRAM: {
			TGGDiagram tggDiagram = (TGGDiagram) theEObject;
			T result = caseTGGDiagram(tggDiagram);
			if (result == null)
				result = caseNamedElement(tggDiagram);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.MODEL_DOMAIN: {
			ModelDomain modelDomain = (ModelDomain) theEObject;
			T result = caseModelDomain(modelDomain);
			if (result == null)
				result = caseNamedElement(modelDomain);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.CORRESPONDENCE_DOMAIN: {
			CorrespondenceDomain correspondenceDomain = (CorrespondenceDomain) theEObject;
			T result = caseCorrespondenceDomain(correspondenceDomain);
			if (result == null)
				result = caseNamedElement(correspondenceDomain);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.MODEL_ELEMENT: {
			ModelElement modelElement = (ModelElement) theEObject;
			T result = caseModelElement(modelElement);
			if (result == null)
				result = caseRuleElement(modelElement);
			if (result == null)
				result = caseNamedElement(modelElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.MODEL_LINK: {
			ModelLink modelLink = (ModelLink) theEObject;
			T result = caseModelLink(modelLink);
			if (result == null)
				result = caseModelElement(modelLink);
			if (result == null)
				result = caseRuleElement(modelLink);
			if (result == null)
				result = caseNamedElement(modelLink);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.MODEL_OBJECT: {
			ModelObject modelObject = (ModelObject) theEObject;
			T result = caseModelObject(modelObject);
			if (result == null)
				result = caseModelElement(modelObject);
			if (result == null)
				result = caseRuleElement(modelObject);
			if (result == null)
				result = caseNamedElement(modelObject);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.TGG_RULE: {
			TGGRule tggRule = (TGGRule) theEObject;
			T result = caseTGGRule(tggRule);
			if (result == null)
				result = caseNamedElement(tggRule);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.CORRESPONDENCE_NODE: {
			CorrespondenceNode correspondenceNode = (CorrespondenceNode) theEObject;
			T result = caseCorrespondenceNode(correspondenceNode);
			if (result == null)
				result = caseCorrespondenceElement(correspondenceNode);
			if (result == null)
				result = caseRuleElement(correspondenceNode);
			if (result == null)
				result = caseNamedElement(correspondenceNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.CORRESPONDENCE_LINK: {
			CorrespondenceLink correspondenceLink = (CorrespondenceLink) theEObject;
			T result = caseCorrespondenceLink(correspondenceLink);
			if (result == null)
				result = caseCorrespondenceElement(correspondenceLink);
			if (result == null)
				result = caseRuleElement(correspondenceLink);
			if (result == null)
				result = caseNamedElement(correspondenceLink);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.CORRESPONDENCE_ELEMENT: {
			CorrespondenceElement correspondenceElement = (CorrespondenceElement) theEObject;
			T result = caseCorrespondenceElement(correspondenceElement);
			if (result == null)
				result = caseRuleElement(correspondenceElement);
			if (result == null)
				result = caseNamedElement(correspondenceElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.RULE_ELEMENT: {
			RuleElement ruleElement = (RuleElement) theEObject;
			T result = caseRuleElement(ruleElement);
			if (result == null)
				result = caseNamedElement(ruleElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.SOURCE_MODEL_DOMAIN: {
			SourceModelDomain sourceModelDomain = (SourceModelDomain) theEObject;
			T result = caseSourceModelDomain(sourceModelDomain);
			if (result == null)
				result = caseModelDomain(sourceModelDomain);
			if (result == null)
				result = caseNamedElement(sourceModelDomain);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.TARGET_MODEL_DOMAIN: {
			TargetModelDomain targetModelDomain = (TargetModelDomain) theEObject;
			T result = caseTargetModelDomain(targetModelDomain);
			if (result == null)
				result = caseModelDomain(targetModelDomain);
			if (result == null)
				result = caseNamedElement(targetModelDomain);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.RULE_VARIABLE: {
			RuleVariable ruleVariable = (RuleVariable) theEObject;
			T result = caseRuleVariable(ruleVariable);
			if (result == null)
				result = caseNamedElement(ruleVariable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TggPackage.MODEL_LINK_POSITION_CONSTRAINT: {
			ModelLinkPositionConstraint modelLinkPositionConstraint = (ModelLinkPositionConstraint) theEObject;
			T result = caseModelLinkPositionConstraint(modelLinkPositionConstraint);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TGG Diagram</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TGG Diagram</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTGGDiagram(TGGDiagram object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Domain</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Domain</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelDomain(ModelDomain object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Correspondence Domain</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Correspondence Domain</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrespondenceDomain(CorrespondenceDomain object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Element</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelElement(ModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Link</em>'.
	 * <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelLink(ModelLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Object</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelObject(ModelObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TGG Rule</em>'.
	 * <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TGG Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTGGRule(TGGRule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Correspondence Node</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Correspondence Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrespondenceNode(CorrespondenceNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Correspondence Link</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Correspondence Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrespondenceLink(CorrespondenceLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Correspondence Element</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Correspondence Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrespondenceElement(CorrespondenceElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Element</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleElement(RuleElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Source Model Domain</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Source Model Domain</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSourceModelDomain(SourceModelDomain object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Target Model Domain</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Target Model Domain</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTargetModelDomain(TargetModelDomain object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Variable</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleVariable(RuleVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Link Position Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Link Position Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelLinkPositionConstraint(ModelLinkPositionConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch, but this is
	 * the last case anyway. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} // TggSwitch
