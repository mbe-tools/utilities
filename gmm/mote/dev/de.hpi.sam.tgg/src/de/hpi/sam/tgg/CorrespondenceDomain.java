/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

import org.eclipse.emf.common.util.EList;

import de.hpi.sam.storyDiagramEcore.NamedElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Correspondence Domain</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The CorrespondenceDomain contains CorrespondenceNodes and CorrespondenceLinks to elements in model domains.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.CorrespondenceDomain#getCorrespondenceElements <em>Correspondence Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.TggPackage#getCorrespondenceDomain()
 * @model
 * @generated
 */
public interface CorrespondenceDomain extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Correspondence Elements</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.tgg.CorrespondenceElement}. <!-- begin-user-doc -->
	 * <!-- end-user-doc --> <!-- begin-model-doc --> The CorrespondenceElements
	 * in this CorrespondenceDomain. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Correspondence Elements</em>' containment
	 *         reference list.
	 * @see de.hpi.sam.tgg.TggPackage#getCorrespondenceDomain_CorrespondenceElements()
	 * @model containment="true"
	 * @generated
	 */
	EList<CorrespondenceElement> getCorrespondenceElements();

} // CorrespondenceDomain
