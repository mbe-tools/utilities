package de.hpi.sam.tgg.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.tgg.CorrespondenceDomain;
import de.hpi.sam.tgg.CorrespondenceElement;
import de.hpi.sam.tgg.CorrespondenceLink;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.ModelDomain;
import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelLinkPositionConstraint;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.RuleElement;
import de.hpi.sam.tgg.RuleVariable;
import de.hpi.sam.tgg.SourceModelDomain;
import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGModifierEnumeration;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TargetModelDomain;
import de.hpi.sam.tgg.TggFactory;
import de.hpi.sam.tgg.TggPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class TggPackageImpl extends EPackageImpl implements TggPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tggDiagramEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelDomainEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass correspondenceDomainEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelElementEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelLinkEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelObjectEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tggRuleEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass correspondenceNodeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass correspondenceLinkEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass correspondenceElementEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleElementEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sourceModelDomainEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass targetModelDomainEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelLinkPositionConstraintEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum tggModifierEnumerationEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.tgg.TggPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TggPackageImpl() {
		super(eNS_URI, TggFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link TggPackage#eINSTANCE} when that
	 * field is accessed. Clients should not invoke it directly. Instead, they
	 * should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TggPackage init() {
		if (isInited)
			return (TggPackage) EPackage.Registry.INSTANCE
					.getEPackage(TggPackage.eNS_URI);

		// Obtain or create and register package
		TggPackageImpl theTggPackage = (TggPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof TggPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new TggPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		StoryDiagramEcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theTggPackage.createPackageContents();

		// Initialize created meta-data
		theTggPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTggPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TggPackage.eNS_URI, theTggPackage);
		return theTggPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTGGDiagram() {
		return tggDiagramEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGDiagram_TggRules() {
		return (EReference) tggDiagramEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGDiagram_RuleSetID() {
		return (EAttribute) tggDiagramEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelDomain() {
		return modelDomainEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelDomain_ModelElements() {
		return (EReference) modelDomainEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrespondenceDomain() {
		return correspondenceDomainEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCorrespondenceDomain_CorrespondenceElements() {
		return (EReference) correspondenceDomainEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelElement() {
		return modelElementEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelLink() {
		return modelLinkEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelLink_EReference() {
		return (EReference) modelLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelLink_Source() {
		return (EReference) modelLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelLink_Target() {
		return (EReference) modelLinkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelLink_OppositeTGGLink() {
		return (EReference) modelLinkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelLink_EOppositeReference() {
		return (EReference) modelLinkEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelLink_LinkPositionConstraint() {
		return (EReference) modelLinkEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelLink_ExternalReference() {
		return (EReference) modelLinkEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelLink_External() {
		return (EAttribute) modelLinkEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelObject() {
		return modelObjectEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelObject_Classifier() {
		return (EReference) modelObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelObject_ConstraintExpressions() {
		return (EReference) modelObjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelObject_AttributeAssignments() {
		return (EReference) modelObjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelObject_OutgoingModelLinks() {
		return (EReference) modelObjectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelObject_IncomingModelLinks() {
		return (EReference) modelObjectEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelObject_PostCreationExpressions() {
		return (EReference) modelObjectEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTGGRule() {
		return tggRuleEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRule_CorrespondenceDomain() {
		return (EReference) tggRuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRule_ConstraintExpressions() {
		return (EReference) tggRuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRule_SourceDomain() {
		return (EReference) tggRuleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRule_TargetDomain() {
		return (EReference) tggRuleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRule_InputElements() {
		return (EReference) tggRuleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRule_RuleVariables() {
		return (EReference) tggRuleEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGRule_IsAxiom() {
		return (EAttribute) tggRuleEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRule_ForwardConstraintExpressions() {
		return (EReference) tggRuleEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRule_ReverseConstraintExpressions() {
		return (EReference) tggRuleEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGRule_Disabled() {
		return (EAttribute) tggRuleEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrespondenceNode() {
		return correspondenceNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCorrespondenceNode_OutgoingCorrespondenceLinks() {
		return (EReference) correspondenceNodeEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCorrespondenceNode_Classifier() {
		return (EReference) correspondenceNodeEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrespondenceLink() {
		return correspondenceLinkEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCorrespondenceLink_Source() {
		return (EReference) correspondenceLinkEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCorrespondenceLink_Target() {
		return (EReference) correspondenceLinkEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrespondenceElement() {
		return correspondenceElementEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleElement() {
		return ruleElementEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRuleElement_Modifier() {
		return (EAttribute) ruleElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSourceModelDomain() {
		return sourceModelDomainEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTargetModelDomain() {
		return targetModelDomainEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleVariable() {
		return ruleVariableEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleVariable_Classifier() {
		return (EReference) ruleVariableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleVariable_ForwardCalculationExpression() {
		return (EReference) ruleVariableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleVariable_ReverseCalculationExpression() {
		return (EReference) ruleVariableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelLinkPositionConstraint() {
		return modelLinkPositionConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelLinkPositionConstraint_ConstraintType() {
		return (EAttribute) modelLinkPositionConstraintEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelLinkPositionConstraint_ModelLink() {
		return (EReference) modelLinkPositionConstraintEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTGGModifierEnumeration() {
		return tggModifierEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TggFactory getTggFactory() {
		return (TggFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		tggDiagramEClass = createEClass(TGG_DIAGRAM);
		createEReference(tggDiagramEClass, TGG_DIAGRAM__TGG_RULES);
		createEAttribute(tggDiagramEClass, TGG_DIAGRAM__RULE_SET_ID);

		modelDomainEClass = createEClass(MODEL_DOMAIN);
		createEReference(modelDomainEClass, MODEL_DOMAIN__MODEL_ELEMENTS);

		correspondenceDomainEClass = createEClass(CORRESPONDENCE_DOMAIN);
		createEReference(correspondenceDomainEClass,
				CORRESPONDENCE_DOMAIN__CORRESPONDENCE_ELEMENTS);

		modelElementEClass = createEClass(MODEL_ELEMENT);

		modelLinkEClass = createEClass(MODEL_LINK);
		createEReference(modelLinkEClass, MODEL_LINK__EREFERENCE);
		createEReference(modelLinkEClass, MODEL_LINK__SOURCE);
		createEReference(modelLinkEClass, MODEL_LINK__TARGET);
		createEReference(modelLinkEClass, MODEL_LINK__OPPOSITE_TGG_LINK);
		createEReference(modelLinkEClass, MODEL_LINK__EOPPOSITE_REFERENCE);
		createEReference(modelLinkEClass, MODEL_LINK__LINK_POSITION_CONSTRAINT);
		createEReference(modelLinkEClass, MODEL_LINK__EXTERNAL_REFERENCE);
		createEAttribute(modelLinkEClass, MODEL_LINK__EXTERNAL);

		modelObjectEClass = createEClass(MODEL_OBJECT);
		createEReference(modelObjectEClass, MODEL_OBJECT__CLASSIFIER);
		createEReference(modelObjectEClass,
				MODEL_OBJECT__CONSTRAINT_EXPRESSIONS);
		createEReference(modelObjectEClass, MODEL_OBJECT__ATTRIBUTE_ASSIGNMENTS);
		createEReference(modelObjectEClass, MODEL_OBJECT__OUTGOING_MODEL_LINKS);
		createEReference(modelObjectEClass, MODEL_OBJECT__INCOMING_MODEL_LINKS);
		createEReference(modelObjectEClass,
				MODEL_OBJECT__POST_CREATION_EXPRESSIONS);

		tggRuleEClass = createEClass(TGG_RULE);
		createEReference(tggRuleEClass, TGG_RULE__CORRESPONDENCE_DOMAIN);
		createEReference(tggRuleEClass, TGG_RULE__CONSTRAINT_EXPRESSIONS);
		createEReference(tggRuleEClass, TGG_RULE__SOURCE_DOMAIN);
		createEReference(tggRuleEClass, TGG_RULE__TARGET_DOMAIN);
		createEReference(tggRuleEClass, TGG_RULE__INPUT_ELEMENTS);
		createEReference(tggRuleEClass, TGG_RULE__RULE_VARIABLES);
		createEAttribute(tggRuleEClass, TGG_RULE__IS_AXIOM);
		createEReference(tggRuleEClass,
				TGG_RULE__FORWARD_CONSTRAINT_EXPRESSIONS);
		createEReference(tggRuleEClass,
				TGG_RULE__REVERSE_CONSTRAINT_EXPRESSIONS);
		createEAttribute(tggRuleEClass, TGG_RULE__DISABLED);

		correspondenceNodeEClass = createEClass(CORRESPONDENCE_NODE);
		createEReference(correspondenceNodeEClass,
				CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS);
		createEReference(correspondenceNodeEClass,
				CORRESPONDENCE_NODE__CLASSIFIER);

		correspondenceLinkEClass = createEClass(CORRESPONDENCE_LINK);
		createEReference(correspondenceLinkEClass, CORRESPONDENCE_LINK__SOURCE);
		createEReference(correspondenceLinkEClass, CORRESPONDENCE_LINK__TARGET);

		correspondenceElementEClass = createEClass(CORRESPONDENCE_ELEMENT);

		ruleElementEClass = createEClass(RULE_ELEMENT);
		createEAttribute(ruleElementEClass, RULE_ELEMENT__MODIFIER);

		sourceModelDomainEClass = createEClass(SOURCE_MODEL_DOMAIN);

		targetModelDomainEClass = createEClass(TARGET_MODEL_DOMAIN);

		ruleVariableEClass = createEClass(RULE_VARIABLE);
		createEReference(ruleVariableEClass, RULE_VARIABLE__CLASSIFIER);
		createEReference(ruleVariableEClass,
				RULE_VARIABLE__FORWARD_CALCULATION_EXPRESSION);
		createEReference(ruleVariableEClass,
				RULE_VARIABLE__REVERSE_CALCULATION_EXPRESSION);

		modelLinkPositionConstraintEClass = createEClass(MODEL_LINK_POSITION_CONSTRAINT);
		createEAttribute(modelLinkPositionConstraintEClass,
				MODEL_LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE);
		createEReference(modelLinkPositionConstraintEClass,
				MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK);

		// Create enums
		tggModifierEnumerationEEnum = createEEnum(TGG_MODIFIER_ENUMERATION);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StoryDiagramEcorePackage theStoryDiagramEcorePackage = (StoryDiagramEcorePackage) EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI);
		SdmPackage theSdmPackage = (SdmPackage) EPackage.Registry.INSTANCE
				.getEPackage(SdmPackage.eNS_URI);
		ExpressionsPackage theExpressionsPackage = (ExpressionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		tggDiagramEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		modelDomainEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		correspondenceDomainEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		modelElementEClass.getESuperTypes().add(this.getRuleElement());
		modelLinkEClass.getESuperTypes().add(this.getModelElement());
		modelObjectEClass.getESuperTypes().add(this.getModelElement());
		tggRuleEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		correspondenceNodeEClass.getESuperTypes().add(
				this.getCorrespondenceElement());
		correspondenceLinkEClass.getESuperTypes().add(
				this.getCorrespondenceElement());
		correspondenceElementEClass.getESuperTypes().add(this.getRuleElement());
		ruleElementEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		sourceModelDomainEClass.getESuperTypes().add(this.getModelDomain());
		targetModelDomainEClass.getESuperTypes().add(this.getModelDomain());
		ruleVariableEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());

		// Initialize classes and features; add operations and parameters
		initEClass(tggDiagramEClass, TGGDiagram.class, "TGGDiagram",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTGGDiagram_TggRules(), this.getTGGRule(), null,
				"tggRules", null, 0, -1, TGGDiagram.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGDiagram_RuleSetID(), ecorePackage.getEString(),
				"ruleSetID", null, 0, 1, TGGDiagram.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(modelDomainEClass, ModelDomain.class, "ModelDomain",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModelDomain_ModelElements(), this.getModelElement(),
				null, "modelElements", null, 0, -1, ModelDomain.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(correspondenceDomainEClass, CorrespondenceDomain.class,
				"CorrespondenceDomain", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCorrespondenceDomain_CorrespondenceElements(),
				this.getCorrespondenceElement(), null,
				"correspondenceElements", null, 0, -1,
				CorrespondenceDomain.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(modelElementEClass, ModelElement.class, "ModelElement",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(modelLinkEClass, ModelLink.class, "ModelLink", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModelLink_EReference(), ecorePackage.getEReference(),
				null, "eReference", null, 0, 1, ModelLink.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModelLink_Source(), this.getModelObject(),
				this.getModelObject_OutgoingModelLinks(), "source", null, 1, 1,
				ModelLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getModelLink_Target(), this.getModelObject(),
				this.getModelObject_IncomingModelLinks(), "target", null, 1, 1,
				ModelLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getModelLink_OppositeTGGLink(), this.getModelLink(),
				this.getModelLink_OppositeTGGLink(), "oppositeTGGLink", null,
				0, 1, ModelLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModelLink_EOppositeReference(),
				ecorePackage.getEReference(), null, "eOppositeReference", null,
				0, 1, ModelLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModelLink_LinkPositionConstraint(),
				this.getModelLinkPositionConstraint(),
				this.getModelLinkPositionConstraint_ModelLink(),
				"linkPositionConstraint", null, 0, 1, ModelLink.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getModelLink_ExternalReference(),
				theSdmPackage.getExternalReference(), null,
				"externalReference", null, 0, 1, ModelLink.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getModelLink_External(), ecorePackage.getEBoolean(),
				"external", null, 1, 1, ModelLink.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(modelObjectEClass, ModelObject.class, "ModelObject",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModelObject_Classifier(),
				ecorePackage.getEClassifier(), null, "classifier", null, 1, 1,
				ModelObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getModelObject_ConstraintExpressions(),
				theExpressionsPackage.getExpression(), null,
				"constraintExpressions", null, 0, -1, ModelObject.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getModelObject_AttributeAssignments(),
				theSdmPackage.getAttributeAssignment(), null,
				"attributeAssignments", null, 0, -1, ModelObject.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getModelObject_OutgoingModelLinks(),
				this.getModelLink(), this.getModelLink_Source(),
				"outgoingModelLinks", null, 0, -1, ModelObject.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getModelObject_IncomingModelLinks(),
				this.getModelLink(), this.getModelLink_Target(),
				"incomingModelLinks", null, 0, -1, ModelObject.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getModelObject_PostCreationExpressions(),
				theExpressionsPackage.getExpression(), null,
				"postCreationExpressions", null, 0, -1, ModelObject.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(tggRuleEClass, TGGRule.class, "TGGRule", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTGGRule_CorrespondenceDomain(),
				this.getCorrespondenceDomain(), null, "correspondenceDomain",
				null, 1, 1, TGGRule.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTGGRule_ConstraintExpressions(),
				theExpressionsPackage.getExpression(), null,
				"constraintExpressions", null, 0, -1, TGGRule.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getTGGRule_SourceDomain(), this.getSourceModelDomain(),
				null, "sourceDomain", null, 1, 1, TGGRule.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTGGRule_TargetDomain(), this.getTargetModelDomain(),
				null, "targetDomain", null, 1, 1, TGGRule.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTGGRule_InputElements(), this.getRuleElement(), null,
				"inputElements", null, 0, 2, TGGRule.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTGGRule_RuleVariables(), this.getRuleVariable(),
				null, "ruleVariables", null, 0, -1, TGGRule.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getTGGRule_IsAxiom(), ecorePackage.getEBoolean(),
				"isAxiom", null, 1, 1, TGGRule.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getTGGRule_ForwardConstraintExpressions(),
				theExpressionsPackage.getExpression(), null,
				"forwardConstraintExpressions", null, 0, -1, TGGRule.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getTGGRule_ReverseConstraintExpressions(),
				theExpressionsPackage.getExpression(), null,
				"reverseConstraintExpressions", null, 0, -1, TGGRule.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getTGGRule_Disabled(), ecorePackage.getEBoolean(),
				"disabled", null, 1, 1, TGGRule.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(correspondenceNodeEClass, CorrespondenceNode.class,
				"CorrespondenceNode", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCorrespondenceNode_OutgoingCorrespondenceLinks(),
				this.getCorrespondenceLink(),
				this.getCorrespondenceLink_Source(),
				"outgoingCorrespondenceLinks", null, 0, -1,
				CorrespondenceNode.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCorrespondenceNode_Classifier(),
				ecorePackage.getEClass(), null, "classifier", null, 1, 1,
				CorrespondenceNode.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(correspondenceLinkEClass, CorrespondenceLink.class,
				"CorrespondenceLink", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCorrespondenceLink_Source(),
				this.getCorrespondenceNode(),
				this.getCorrespondenceNode_OutgoingCorrespondenceLinks(),
				"source", null, 0, 1, CorrespondenceLink.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCorrespondenceLink_Target(), this.getModelObject(),
				null, "target", null, 0, 1, CorrespondenceLink.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(correspondenceElementEClass, CorrespondenceElement.class,
				"CorrespondenceElement", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(ruleElementEClass, RuleElement.class, "RuleElement",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRuleElement_Modifier(),
				this.getTGGModifierEnumeration(), "modifier", null, 1, 1,
				RuleElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sourceModelDomainEClass, SourceModelDomain.class,
				"SourceModelDomain", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(targetModelDomainEClass, TargetModelDomain.class,
				"TargetModelDomain", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(ruleVariableEClass, RuleVariable.class, "RuleVariable",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuleVariable_Classifier(),
				ecorePackage.getEClassifier(), null, "classifier", null, 1, 1,
				RuleVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getRuleVariable_ForwardCalculationExpression(),
				theExpressionsPackage.getExpression(), null,
				"forwardCalculationExpression", null, 1, 1, RuleVariable.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getRuleVariable_ReverseCalculationExpression(),
				theExpressionsPackage.getExpression(), null,
				"reverseCalculationExpression", null, 1, 1, RuleVariable.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(modelLinkPositionConstraintEClass,
				ModelLinkPositionConstraint.class,
				"ModelLinkPositionConstraint", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getModelLinkPositionConstraint_ConstraintType(),
				theSdmPackage.getLinkPositionConstraintEnumeration(),
				"constraintType", "FIRST", 1, 1,
				ModelLinkPositionConstraint.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getModelLinkPositionConstraint_ModelLink(),
				this.getModelLink(),
				this.getModelLink_LinkPositionConstraint(), "modelLink", null,
				1, 1, ModelLinkPositionConstraint.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(tggModifierEnumerationEEnum, TGGModifierEnumeration.class,
				"TGGModifierEnumeration");
		addEEnumLiteral(tggModifierEnumerationEEnum,
				TGGModifierEnumeration.NONE);
		addEEnumLiteral(tggModifierEnumerationEEnum,
				TGGModifierEnumeration.CREATE);
		addEEnumLiteral(tggModifierEnumerationEEnum,
				TGGModifierEnumeration.CREATE_NOT_EXISTS);

		// Create resource
		createResource(eNS_URI);
	}

} // TggPackageImpl