/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hpi.sam.tgg.TggFactory
 * @model kind="package"
 * @generated
 */
public interface TggPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "tgg";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///de/hpi/sam/tgg.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.hpi.sam.tgg";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	TggPackage eINSTANCE = de.hpi.sam.tgg.impl.TggPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.TGGDiagramImpl
	 * <em>TGG Diagram</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.tgg.impl.TGGDiagramImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getTGGDiagram()
	 * @generated
	 */
	int TGG_DIAGRAM = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_DIAGRAM__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_DIAGRAM__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_DIAGRAM__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Tgg Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_DIAGRAM__TGG_RULES = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rule Set ID</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_DIAGRAM__RULE_SET_ID = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>TGG Diagram</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_DIAGRAM_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.ModelDomainImpl
	 * <em>Model Domain</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.tgg.impl.ModelDomainImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getModelDomain()
	 * @generated
	 */
	int MODEL_DOMAIN = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_DOMAIN__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_DOMAIN__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_DOMAIN__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Model Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_DOMAIN__MODEL_ELEMENTS = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Model Domain</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_DOMAIN_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.CorrespondenceDomainImpl <em>Correspondence Domain</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.impl.CorrespondenceDomainImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getCorrespondenceDomain()
	 * @generated
	 */
	int CORRESPONDENCE_DOMAIN = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_DOMAIN__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_DOMAIN__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_DOMAIN__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Correspondence Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_DOMAIN__CORRESPONDENCE_ELEMENTS = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Correspondence Domain</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_DOMAIN_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.RuleElementImpl
	 * <em>Rule Element</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.tgg.impl.RuleElementImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getRuleElement()
	 * @generated
	 */
	int RULE_ELEMENT = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_ELEMENT__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_ELEMENT__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_ELEMENT__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_ELEMENT__MODIFIER = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Rule Element</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ELEMENT_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.ModelElementImpl
	 * <em>Model Element</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.tgg.impl.ModelElementImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getModelElement()
	 * @generated
	 */
	int MODEL_ELEMENT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT__NAME = RULE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT__DESCRIPTION = RULE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT__UUID = RULE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT__MODIFIER = RULE_ELEMENT__MODIFIER;

	/**
	 * The number of structural features of the '<em>Model Element</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT_FEATURE_COUNT = RULE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.ModelLinkImpl
	 * <em>Model Link</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.tgg.impl.ModelLinkImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getModelLink()
	 * @generated
	 */
	int MODEL_LINK = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK__NAME = MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK__DESCRIPTION = MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK__UUID = MODEL_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK__MODIFIER = MODEL_ELEMENT__MODIFIER;

	/**
	 * The feature id for the '<em><b>EReference</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK__EREFERENCE = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK__SOURCE = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK__TARGET = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Opposite TGG Link</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK__OPPOSITE_TGG_LINK = MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>EOpposite Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK__EOPPOSITE_REFERENCE = MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Link Position Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK__LINK_POSITION_CONSTRAINT = MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>External Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK__EXTERNAL_REFERENCE = MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>External</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK__EXTERNAL = MODEL_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Model Link</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.ModelObjectImpl
	 * <em>Model Object</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.tgg.impl.ModelObjectImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getModelObject()
	 * @generated
	 */
	int MODEL_OBJECT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_OBJECT__NAME = MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_OBJECT__DESCRIPTION = MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_OBJECT__UUID = MODEL_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_OBJECT__MODIFIER = MODEL_ELEMENT__MODIFIER;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODEL_OBJECT__CLASSIFIER = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constraint Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_OBJECT__CONSTRAINT_EXPRESSIONS = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Attribute Assignments</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_OBJECT__ATTRIBUTE_ASSIGNMENTS = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Outgoing Model Links</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_OBJECT__OUTGOING_MODEL_LINKS = MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Incoming Model Links</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_OBJECT__INCOMING_MODEL_LINKS = MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Post Creation Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_OBJECT__POST_CREATION_EXPRESSIONS = MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Model Object</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_OBJECT_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.TGGRuleImpl <em>TGG Rule</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.tgg.impl.TGGRuleImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getTGGRule()
	 * @generated
	 */
	int TGG_RULE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Correspondence Domain</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__CORRESPONDENCE_DOMAIN = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constraint Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__CONSTRAINT_EXPRESSIONS = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source Domain</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__SOURCE_DOMAIN = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Target Domain</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__TARGET_DOMAIN = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Input Elements</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__INPUT_ELEMENTS = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Rule Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__RULE_VARIABLES = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Is Axiom</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__IS_AXIOM = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Forward Constraint Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__FORWARD_CONSTRAINT_EXPRESSIONS = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Reverse Constraint Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__REVERSE_CONSTRAINT_EXPRESSIONS = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Disabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__DISABLED = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>TGG Rule</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.CorrespondenceElementImpl <em>Correspondence Element</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.impl.CorrespondenceElementImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getCorrespondenceElement()
	 * @generated
	 */
	int CORRESPONDENCE_ELEMENT = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_ELEMENT__NAME = RULE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_ELEMENT__DESCRIPTION = RULE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_ELEMENT__UUID = RULE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_ELEMENT__MODIFIER = RULE_ELEMENT__MODIFIER;

	/**
	 * The number of structural features of the '<em>Correspondence Element</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_ELEMENT_FEATURE_COUNT = RULE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.CorrespondenceNodeImpl <em>Correspondence Node</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.impl.CorrespondenceNodeImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getCorrespondenceNode()
	 * @generated
	 */
	int CORRESPONDENCE_NODE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_NODE__NAME = CORRESPONDENCE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_NODE__DESCRIPTION = CORRESPONDENCE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_NODE__UUID = CORRESPONDENCE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_NODE__MODIFIER = CORRESPONDENCE_ELEMENT__MODIFIER;

	/**
	 * The feature id for the '<em><b>Outgoing Correspondence Links</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS = CORRESPONDENCE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_NODE__CLASSIFIER = CORRESPONDENCE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Correspondence Node</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_NODE_FEATURE_COUNT = CORRESPONDENCE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.CorrespondenceLinkImpl <em>Correspondence Link</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.impl.CorrespondenceLinkImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getCorrespondenceLink()
	 * @generated
	 */
	int CORRESPONDENCE_LINK = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_LINK__NAME = CORRESPONDENCE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_LINK__DESCRIPTION = CORRESPONDENCE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_LINK__UUID = CORRESPONDENCE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_LINK__MODIFIER = CORRESPONDENCE_ELEMENT__MODIFIER;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_LINK__SOURCE = CORRESPONDENCE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_LINK__TARGET = CORRESPONDENCE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Correspondence Link</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRESPONDENCE_LINK_FEATURE_COUNT = CORRESPONDENCE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.SourceModelDomainImpl <em>Source Model Domain</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.impl.SourceModelDomainImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getSourceModelDomain()
	 * @generated
	 */
	int SOURCE_MODEL_DOMAIN = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOURCE_MODEL_DOMAIN__NAME = MODEL_DOMAIN__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOURCE_MODEL_DOMAIN__DESCRIPTION = MODEL_DOMAIN__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SOURCE_MODEL_DOMAIN__UUID = MODEL_DOMAIN__UUID;

	/**
	 * The feature id for the '<em><b>Model Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_MODEL_DOMAIN__MODEL_ELEMENTS = MODEL_DOMAIN__MODEL_ELEMENTS;

	/**
	 * The number of structural features of the '<em>Source Model Domain</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_MODEL_DOMAIN_FEATURE_COUNT = MODEL_DOMAIN_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.TargetModelDomainImpl <em>Target Model Domain</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.impl.TargetModelDomainImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getTargetModelDomain()
	 * @generated
	 */
	int TARGET_MODEL_DOMAIN = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TARGET_MODEL_DOMAIN__NAME = MODEL_DOMAIN__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TARGET_MODEL_DOMAIN__DESCRIPTION = MODEL_DOMAIN__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TARGET_MODEL_DOMAIN__UUID = MODEL_DOMAIN__UUID;

	/**
	 * The feature id for the '<em><b>Model Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_MODEL_DOMAIN__MODEL_ELEMENTS = MODEL_DOMAIN__MODEL_ELEMENTS;

	/**
	 * The number of structural features of the '<em>Target Model Domain</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_MODEL_DOMAIN_FEATURE_COUNT = MODEL_DOMAIN_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.RuleVariableImpl
	 * <em>Rule Variable</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.tgg.impl.RuleVariableImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getRuleVariable()
	 * @generated
	 */
	int RULE_VARIABLE = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_VARIABLE__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_VARIABLE__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_VARIABLE__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_VARIABLE__CLASSIFIER = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Forward Calculation Expression</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_VARIABLE__FORWARD_CALCULATION_EXPRESSION = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Reverse Calculation Expression</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_VARIABLE__REVERSE_CALCULATION_EXPRESSION = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Rule Variable</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_VARIABLE_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.impl.ModelLinkPositionConstraintImpl <em>Model Link Position Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.tgg.impl.ModelLinkPositionConstraintImpl
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getModelLinkPositionConstraint()
	 * @generated
	 */
	int MODEL_LINK_POSITION_CONSTRAINT = 14;

	/**
	 * The feature id for the '<em><b>Constraint Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Model Link</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK = 1;

	/**
	 * The number of structural features of the '<em>Model Link Position Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_LINK_POSITION_CONSTRAINT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.tgg.TGGModifierEnumeration <em>TGG Modifier Enumeration</em>}' enum.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.tgg.TGGModifierEnumeration
	 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getTGGModifierEnumeration()
	 * @generated
	 */
	int TGG_MODIFIER_ENUMERATION = 15;

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.TGGDiagram <em>TGG Diagram</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>TGG Diagram</em>'.
	 * @see de.hpi.sam.tgg.TGGDiagram
	 * @generated
	 */
	EClass getTGGDiagram();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link de.hpi.sam.tgg.TGGDiagram#getTggRules <em>Tgg Rules</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '
	 *         <em>Tgg Rules</em>'.
	 * @see de.hpi.sam.tgg.TGGDiagram#getTggRules()
	 * @see #getTGGDiagram()
	 * @generated
	 */
	EReference getTGGDiagram_TggRules();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.TGGDiagram#getRuleSetID <em>Rule Set ID</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rule Set ID</em>'.
	 * @see de.hpi.sam.tgg.TGGDiagram#getRuleSetID()
	 * @see #getTGGDiagram()
	 * @generated
	 */
	EAttribute getTGGDiagram_RuleSetID();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.ModelDomain <em>Model Domain</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Domain</em>'.
	 * @see de.hpi.sam.tgg.ModelDomain
	 * @generated
	 */
	EClass getModelDomain();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.tgg.ModelDomain#getModelElements <em>Model Elements</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Model Elements</em>'.
	 * @see de.hpi.sam.tgg.ModelDomain#getModelElements()
	 * @see #getModelDomain()
	 * @generated
	 */
	EReference getModelDomain_ModelElements();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.CorrespondenceDomain <em>Correspondence Domain</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Correspondence Domain</em>'.
	 * @see de.hpi.sam.tgg.CorrespondenceDomain
	 * @generated
	 */
	EClass getCorrespondenceDomain();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.tgg.CorrespondenceDomain#getCorrespondenceElements <em>Correspondence Elements</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Correspondence Elements</em>'.
	 * @see de.hpi.sam.tgg.CorrespondenceDomain#getCorrespondenceElements()
	 * @see #getCorrespondenceDomain()
	 * @generated
	 */
	EReference getCorrespondenceDomain_CorrespondenceElements();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.ModelElement <em>Model Element</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Element</em>'.
	 * @see de.hpi.sam.tgg.ModelElement
	 * @generated
	 */
	EClass getModelElement();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.ModelLink <em>Model Link</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Link</em>'.
	 * @see de.hpi.sam.tgg.ModelLink
	 * @generated
	 */
	EClass getModelLink();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.hpi.sam.tgg.ModelLink#getEReference <em>EReference</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>EReference</em>'.
	 * @see de.hpi.sam.tgg.ModelLink#getEReference()
	 * @see #getModelLink()
	 * @generated
	 */
	EReference getModelLink_EReference();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.hpi.sam.tgg.ModelLink#getSource <em>Source</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see de.hpi.sam.tgg.ModelLink#getSource()
	 * @see #getModelLink()
	 * @generated
	 */
	EReference getModelLink_Source();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.hpi.sam.tgg.ModelLink#getTarget <em>Target</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see de.hpi.sam.tgg.ModelLink#getTarget()
	 * @see #getModelLink()
	 * @generated
	 */
	EReference getModelLink_Target();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.hpi.sam.tgg.ModelLink#getOppositeTGGLink
	 * <em>Opposite TGG Link</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Opposite TGG Link</em>'.
	 * @see de.hpi.sam.tgg.ModelLink#getOppositeTGGLink()
	 * @see #getModelLink()
	 * @generated
	 */
	EReference getModelLink_OppositeTGGLink();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.ModelLink#getEOppositeReference <em>EOpposite Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EOpposite Reference</em>'.
	 * @see de.hpi.sam.tgg.ModelLink#getEOppositeReference()
	 * @see #getModelLink()
	 * @generated
	 */
	EReference getModelLink_EOppositeReference();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.tgg.ModelLink#getLinkPositionConstraint <em>Link Position Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Link Position Constraint</em>'.
	 * @see de.hpi.sam.tgg.ModelLink#getLinkPositionConstraint()
	 * @see #getModelLink()
	 * @generated
	 */
	EReference getModelLink_LinkPositionConstraint();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.tgg.ModelLink#getExternalReference <em>External Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>External Reference</em>'.
	 * @see de.hpi.sam.tgg.ModelLink#getExternalReference()
	 * @see #getModelLink()
	 * @generated
	 */
	EReference getModelLink_ExternalReference();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.ModelLink#isExternal <em>External</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>External</em>'.
	 * @see de.hpi.sam.tgg.ModelLink#isExternal()
	 * @see #getModelLink()
	 * @generated
	 */
	EAttribute getModelLink_External();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.ModelObject <em>Model Object</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Object</em>'.
	 * @see de.hpi.sam.tgg.ModelObject
	 * @generated
	 */
	EClass getModelObject();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.ModelObject#getClassifier <em>Classifier</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Classifier</em>'.
	 * @see de.hpi.sam.tgg.ModelObject#getClassifier()
	 * @see #getModelObject()
	 * @generated
	 */
	EReference getModelObject_Classifier();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.tgg.ModelObject#getConstraintExpressions <em>Constraint Expressions</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraint Expressions</em>'.
	 * @see de.hpi.sam.tgg.ModelObject#getConstraintExpressions()
	 * @see #getModelObject()
	 * @generated
	 */
	EReference getModelObject_ConstraintExpressions();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.tgg.ModelObject#getAttributeAssignments <em>Attribute Assignments</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attribute Assignments</em>'.
	 * @see de.hpi.sam.tgg.ModelObject#getAttributeAssignments()
	 * @see #getModelObject()
	 * @generated
	 */
	EReference getModelObject_AttributeAssignments();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.tgg.ModelObject#getOutgoingModelLinks <em>Outgoing Model Links</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing Model Links</em>'.
	 * @see de.hpi.sam.tgg.ModelObject#getOutgoingModelLinks()
	 * @see #getModelObject()
	 * @generated
	 */
	EReference getModelObject_OutgoingModelLinks();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.tgg.ModelObject#getIncomingModelLinks <em>Incoming Model Links</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming Model Links</em>'.
	 * @see de.hpi.sam.tgg.ModelObject#getIncomingModelLinks()
	 * @see #getModelObject()
	 * @generated
	 */
	EReference getModelObject_IncomingModelLinks();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.tgg.ModelObject#getPostCreationExpressions <em>Post Creation Expressions</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Post Creation Expressions</em>'.
	 * @see de.hpi.sam.tgg.ModelObject#getPostCreationExpressions()
	 * @see #getModelObject()
	 * @generated
	 */
	EReference getModelObject_PostCreationExpressions();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.TGGRule <em>TGG Rule</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>TGG Rule</em>'.
	 * @see de.hpi.sam.tgg.TGGRule
	 * @generated
	 */
	EClass getTGGRule();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.tgg.TGGRule#getCorrespondenceDomain <em>Correspondence Domain</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference '<em>Correspondence Domain</em>'.
	 * @see de.hpi.sam.tgg.TGGRule#getCorrespondenceDomain()
	 * @see #getTGGRule()
	 * @generated
	 */
	EReference getTGGRule_CorrespondenceDomain();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.tgg.TGGRule#getConstraintExpressions <em>Constraint Expressions</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraint Expressions</em>'.
	 * @see de.hpi.sam.tgg.TGGRule#getConstraintExpressions()
	 * @see #getTGGRule()
	 * @generated
	 */
	EReference getTGGRule_ConstraintExpressions();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.tgg.TGGRule#getSourceDomain <em>Source Domain</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source Domain</em>'.
	 * @see de.hpi.sam.tgg.TGGRule#getSourceDomain()
	 * @see #getTGGRule()
	 * @generated
	 */
	EReference getTGGRule_SourceDomain();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.tgg.TGGRule#getTargetDomain <em>Target Domain</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target Domain</em>'.
	 * @see de.hpi.sam.tgg.TGGRule#getTargetDomain()
	 * @see #getTGGRule()
	 * @generated
	 */
	EReference getTGGRule_TargetDomain();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.tgg.TGGRule#getInputElements <em>Input Elements</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Input Elements</em>'.
	 * @see de.hpi.sam.tgg.TGGRule#getInputElements()
	 * @see #getTGGRule()
	 * @generated
	 */
	EReference getTGGRule_InputElements();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.tgg.TGGRule#getRuleVariables <em>Rule Variables</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rule Variables</em>'.
	 * @see de.hpi.sam.tgg.TGGRule#getRuleVariables()
	 * @see #getTGGRule()
	 * @generated
	 */
	EReference getTGGRule_RuleVariables();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.tgg.TGGRule#isIsAxiom <em>Is Axiom</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Is Axiom</em>'.
	 * @see de.hpi.sam.tgg.TGGRule#isIsAxiom()
	 * @see #getTGGRule()
	 * @generated
	 */
	EAttribute getTGGRule_IsAxiom();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.tgg.TGGRule#getForwardConstraintExpressions <em>Forward Constraint Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Forward Constraint Expressions</em>'.
	 * @see de.hpi.sam.tgg.TGGRule#getForwardConstraintExpressions()
	 * @see #getTGGRule()
	 * @generated
	 */
	EReference getTGGRule_ForwardConstraintExpressions();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.tgg.TGGRule#getReverseConstraintExpressions <em>Reverse Constraint Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reverse Constraint Expressions</em>'.
	 * @see de.hpi.sam.tgg.TGGRule#getReverseConstraintExpressions()
	 * @see #getTGGRule()
	 * @generated
	 */
	EReference getTGGRule_ReverseConstraintExpressions();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.TGGRule#isDisabled <em>Disabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Disabled</em>'.
	 * @see de.hpi.sam.tgg.TGGRule#isDisabled()
	 * @see #getTGGRule()
	 * @generated
	 */
	EAttribute getTGGRule_Disabled();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.CorrespondenceNode <em>Correspondence Node</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Correspondence Node</em>'.
	 * @see de.hpi.sam.tgg.CorrespondenceNode
	 * @generated
	 */
	EClass getCorrespondenceNode();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.tgg.CorrespondenceNode#getOutgoingCorrespondenceLinks <em>Outgoing Correspondence Links</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing Correspondence Links</em>'.
	 * @see de.hpi.sam.tgg.CorrespondenceNode#getOutgoingCorrespondenceLinks()
	 * @see #getCorrespondenceNode()
	 * @generated
	 */
	EReference getCorrespondenceNode_OutgoingCorrespondenceLinks();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.CorrespondenceNode#getClassifier <em>Classifier</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Classifier</em>'.
	 * @see de.hpi.sam.tgg.CorrespondenceNode#getClassifier()
	 * @see #getCorrespondenceNode()
	 * @generated
	 */
	EReference getCorrespondenceNode_Classifier();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.CorrespondenceLink <em>Correspondence Link</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Correspondence Link</em>'.
	 * @see de.hpi.sam.tgg.CorrespondenceLink
	 * @generated
	 */
	EClass getCorrespondenceLink();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.CorrespondenceLink#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see de.hpi.sam.tgg.CorrespondenceLink#getSource()
	 * @see #getCorrespondenceLink()
	 * @generated
	 */
	EReference getCorrespondenceLink_Source();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.CorrespondenceLink#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see de.hpi.sam.tgg.CorrespondenceLink#getTarget()
	 * @see #getCorrespondenceLink()
	 * @generated
	 */
	EReference getCorrespondenceLink_Target();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.CorrespondenceElement <em>Correspondence Element</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Correspondence Element</em>'.
	 * @see de.hpi.sam.tgg.CorrespondenceElement
	 * @generated
	 */
	EClass getCorrespondenceElement();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.RuleElement <em>Rule Element</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Element</em>'.
	 * @see de.hpi.sam.tgg.RuleElement
	 * @generated
	 */
	EClass getRuleElement();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.tgg.RuleElement#getModifier <em>Modifier</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Modifier</em>'.
	 * @see de.hpi.sam.tgg.RuleElement#getModifier()
	 * @see #getRuleElement()
	 * @generated
	 */
	EAttribute getRuleElement_Modifier();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.SourceModelDomain <em>Source Model Domain</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Source Model Domain</em>'.
	 * @see de.hpi.sam.tgg.SourceModelDomain
	 * @generated
	 */
	EClass getSourceModelDomain();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.TargetModelDomain <em>Target Model Domain</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Target Model Domain</em>'.
	 * @see de.hpi.sam.tgg.TargetModelDomain
	 * @generated
	 */
	EClass getTargetModelDomain();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.RuleVariable <em>Rule Variable</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Variable</em>'.
	 * @see de.hpi.sam.tgg.RuleVariable
	 * @generated
	 */
	EClass getRuleVariable();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.tgg.RuleVariable#getClassifier <em>Classifier</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Classifier</em>'.
	 * @see de.hpi.sam.tgg.RuleVariable#getClassifier()
	 * @see #getRuleVariable()
	 * @generated
	 */
	EReference getRuleVariable_Classifier();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.tgg.RuleVariable#getForwardCalculationExpression <em>Forward Calculation Expression</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference '<em>Forward Calculation Expression</em>'.
	 * @see de.hpi.sam.tgg.RuleVariable#getForwardCalculationExpression()
	 * @see #getRuleVariable()
	 * @generated
	 */
	EReference getRuleVariable_ForwardCalculationExpression();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.tgg.RuleVariable#getReverseCalculationExpression <em>Reverse Calculation Expression</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reverse Calculation Expression</em>'.
	 * @see de.hpi.sam.tgg.RuleVariable#getReverseCalculationExpression()
	 * @see #getRuleVariable()
	 * @generated
	 */
	EReference getRuleVariable_ReverseCalculationExpression();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.tgg.ModelLinkPositionConstraint <em>Model Link Position Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Link Position Constraint</em>'.
	 * @see de.hpi.sam.tgg.ModelLinkPositionConstraint
	 * @generated
	 */
	EClass getModelLinkPositionConstraint();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.tgg.ModelLinkPositionConstraint#getConstraintType <em>Constraint Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constraint Type</em>'.
	 * @see de.hpi.sam.tgg.ModelLinkPositionConstraint#getConstraintType()
	 * @see #getModelLinkPositionConstraint()
	 * @generated
	 */
	EAttribute getModelLinkPositionConstraint_ConstraintType();

	/**
	 * Returns the meta object for the container reference '{@link de.hpi.sam.tgg.ModelLinkPositionConstraint#getModelLink <em>Model Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Model Link</em>'.
	 * @see de.hpi.sam.tgg.ModelLinkPositionConstraint#getModelLink()
	 * @see #getModelLinkPositionConstraint()
	 * @generated
	 */
	EReference getModelLinkPositionConstraint_ModelLink();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.tgg.TGGModifierEnumeration <em>TGG Modifier Enumeration</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for enum '<em>TGG Modifier Enumeration</em>'.
	 * @see de.hpi.sam.tgg.TGGModifierEnumeration
	 * @generated
	 */
	EEnum getTGGModifierEnumeration();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TggFactory getTggFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.TGGDiagramImpl <em>TGG Diagram</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.TGGDiagramImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getTGGDiagram()
		 * @generated
		 */
		EClass TGG_DIAGRAM = eINSTANCE.getTGGDiagram();

		/**
		 * The meta object literal for the '<em><b>Tgg Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference TGG_DIAGRAM__TGG_RULES = eINSTANCE.getTGGDiagram_TggRules();

		/**
		 * The meta object literal for the '<em><b>Rule Set ID</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_DIAGRAM__RULE_SET_ID = eINSTANCE
				.getTGGDiagram_RuleSetID();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.ModelDomainImpl <em>Model Domain</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.ModelDomainImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getModelDomain()
		 * @generated
		 */
		EClass MODEL_DOMAIN = eINSTANCE.getModelDomain();

		/**
		 * The meta object literal for the '<em><b>Model Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference MODEL_DOMAIN__MODEL_ELEMENTS = eINSTANCE
				.getModelDomain_ModelElements();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.CorrespondenceDomainImpl <em>Correspondence Domain</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.CorrespondenceDomainImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getCorrespondenceDomain()
		 * @generated
		 */
		EClass CORRESPONDENCE_DOMAIN = eINSTANCE.getCorrespondenceDomain();

		/**
		 * The meta object literal for the '<em><b>Correspondence Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference CORRESPONDENCE_DOMAIN__CORRESPONDENCE_ELEMENTS = eINSTANCE
				.getCorrespondenceDomain_CorrespondenceElements();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.ModelElementImpl <em>Model Element</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.ModelElementImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getModelElement()
		 * @generated
		 */
		EClass MODEL_ELEMENT = eINSTANCE.getModelElement();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.ModelLinkImpl <em>Model Link</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.ModelLinkImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getModelLink()
		 * @generated
		 */
		EClass MODEL_LINK = eINSTANCE.getModelLink();

		/**
		 * The meta object literal for the '<em><b>EReference</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_LINK__EREFERENCE = eINSTANCE.getModelLink_EReference();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_LINK__SOURCE = eINSTANCE.getModelLink_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_LINK__TARGET = eINSTANCE.getModelLink_Target();

		/**
		 * The meta object literal for the '<em><b>Opposite TGG Link</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_LINK__OPPOSITE_TGG_LINK = eINSTANCE
				.getModelLink_OppositeTGGLink();

		/**
		 * The meta object literal for the '<em><b>EOpposite Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_LINK__EOPPOSITE_REFERENCE = eINSTANCE
				.getModelLink_EOppositeReference();

		/**
		 * The meta object literal for the '<em><b>Link Position Constraint</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_LINK__LINK_POSITION_CONSTRAINT = eINSTANCE
				.getModelLink_LinkPositionConstraint();

		/**
		 * The meta object literal for the '<em><b>External Reference</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_LINK__EXTERNAL_REFERENCE = eINSTANCE
				.getModelLink_ExternalReference();

		/**
		 * The meta object literal for the '<em><b>External</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_LINK__EXTERNAL = eINSTANCE.getModelLink_External();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.ModelObjectImpl <em>Model Object</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.ModelObjectImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getModelObject()
		 * @generated
		 */
		EClass MODEL_OBJECT = eINSTANCE.getModelObject();

		/**
		 * The meta object literal for the '<em><b>Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_OBJECT__CLASSIFIER = eINSTANCE
				.getModelObject_Classifier();

		/**
		 * The meta object literal for the '<em><b>Constraint Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_OBJECT__CONSTRAINT_EXPRESSIONS = eINSTANCE
				.getModelObject_ConstraintExpressions();

		/**
		 * The meta object literal for the '<em><b>Attribute Assignments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_OBJECT__ATTRIBUTE_ASSIGNMENTS = eINSTANCE
				.getModelObject_AttributeAssignments();

		/**
		 * The meta object literal for the '<em><b>Outgoing Model Links</b></em>
		 * ' reference list feature. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @generated
		 */
		EReference MODEL_OBJECT__OUTGOING_MODEL_LINKS = eINSTANCE
				.getModelObject_OutgoingModelLinks();

		/**
		 * The meta object literal for the '<em><b>Incoming Model Links</b></em>
		 * ' reference list feature. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @generated
		 */
		EReference MODEL_OBJECT__INCOMING_MODEL_LINKS = eINSTANCE
				.getModelObject_IncomingModelLinks();

		/**
		 * The meta object literal for the '<em><b>Post Creation Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_OBJECT__POST_CREATION_EXPRESSIONS = eINSTANCE
				.getModelObject_PostCreationExpressions();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.TGGRuleImpl <em>TGG Rule</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.TGGRuleImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getTGGRule()
		 * @generated
		 */
		EClass TGG_RULE = eINSTANCE.getTGGRule();

		/**
		 * The meta object literal for the '<em><b>Correspondence Domain</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE__CORRESPONDENCE_DOMAIN = eINSTANCE
				.getTGGRule_CorrespondenceDomain();

		/**
		 * The meta object literal for the '<em><b>Constraint Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE__CONSTRAINT_EXPRESSIONS = eINSTANCE
				.getTGGRule_ConstraintExpressions();

		/**
		 * The meta object literal for the '<em><b>Source Domain</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE__SOURCE_DOMAIN = eINSTANCE
				.getTGGRule_SourceDomain();

		/**
		 * The meta object literal for the '<em><b>Target Domain</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE__TARGET_DOMAIN = eINSTANCE
				.getTGGRule_TargetDomain();

		/**
		 * The meta object literal for the '<em><b>Input Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE__INPUT_ELEMENTS = eINSTANCE
				.getTGGRule_InputElements();

		/**
		 * The meta object literal for the '<em><b>Rule Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE__RULE_VARIABLES = eINSTANCE
				.getTGGRule_RuleVariables();

		/**
		 * The meta object literal for the '<em><b>Is Axiom</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_RULE__IS_AXIOM = eINSTANCE.getTGGRule_IsAxiom();

		/**
		 * The meta object literal for the '<em><b>Forward Constraint Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE__FORWARD_CONSTRAINT_EXPRESSIONS = eINSTANCE
				.getTGGRule_ForwardConstraintExpressions();

		/**
		 * The meta object literal for the '<em><b>Reverse Constraint Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE__REVERSE_CONSTRAINT_EXPRESSIONS = eINSTANCE
				.getTGGRule_ReverseConstraintExpressions();

		/**
		 * The meta object literal for the '<em><b>Disabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_RULE__DISABLED = eINSTANCE.getTGGRule_Disabled();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.CorrespondenceNodeImpl <em>Correspondence Node</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.CorrespondenceNodeImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getCorrespondenceNode()
		 * @generated
		 */
		EClass CORRESPONDENCE_NODE = eINSTANCE.getCorrespondenceNode();

		/**
		 * The meta object literal for the '<em><b>Outgoing Correspondence Links</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS = eINSTANCE
				.getCorrespondenceNode_OutgoingCorrespondenceLinks();

		/**
		 * The meta object literal for the '<em><b>Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference CORRESPONDENCE_NODE__CLASSIFIER = eINSTANCE
				.getCorrespondenceNode_Classifier();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.CorrespondenceLinkImpl <em>Correspondence Link</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.CorrespondenceLinkImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getCorrespondenceLink()
		 * @generated
		 */
		EClass CORRESPONDENCE_LINK = eINSTANCE.getCorrespondenceLink();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference CORRESPONDENCE_LINK__SOURCE = eINSTANCE
				.getCorrespondenceLink_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference CORRESPONDENCE_LINK__TARGET = eINSTANCE
				.getCorrespondenceLink_Target();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.CorrespondenceElementImpl <em>Correspondence Element</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.CorrespondenceElementImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getCorrespondenceElement()
		 * @generated
		 */
		EClass CORRESPONDENCE_ELEMENT = eINSTANCE.getCorrespondenceElement();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.RuleElementImpl <em>Rule Element</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.RuleElementImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getRuleElement()
		 * @generated
		 */
		EClass RULE_ELEMENT = eINSTANCE.getRuleElement();

		/**
		 * The meta object literal for the '<em><b>Modifier</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE_ELEMENT__MODIFIER = eINSTANCE.getRuleElement_Modifier();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.SourceModelDomainImpl <em>Source Model Domain</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.SourceModelDomainImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getSourceModelDomain()
		 * @generated
		 */
		EClass SOURCE_MODEL_DOMAIN = eINSTANCE.getSourceModelDomain();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.TargetModelDomainImpl <em>Target Model Domain</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.TargetModelDomainImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getTargetModelDomain()
		 * @generated
		 */
		EClass TARGET_MODEL_DOMAIN = eINSTANCE.getTargetModelDomain();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.RuleVariableImpl <em>Rule Variable</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.RuleVariableImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getRuleVariable()
		 * @generated
		 */
		EClass RULE_VARIABLE = eINSTANCE.getRuleVariable();

		/**
		 * The meta object literal for the '<em><b>Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_VARIABLE__CLASSIFIER = eINSTANCE
				.getRuleVariable_Classifier();

		/**
		 * The meta object literal for the '<em><b>Forward Calculation Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_VARIABLE__FORWARD_CALCULATION_EXPRESSION = eINSTANCE
				.getRuleVariable_ForwardCalculationExpression();

		/**
		 * The meta object literal for the '<em><b>Reverse Calculation Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_VARIABLE__REVERSE_CALCULATION_EXPRESSION = eINSTANCE
				.getRuleVariable_ReverseCalculationExpression();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.impl.ModelLinkPositionConstraintImpl <em>Model Link Position Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.impl.ModelLinkPositionConstraintImpl
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getModelLinkPositionConstraint()
		 * @generated
		 */
		EClass MODEL_LINK_POSITION_CONSTRAINT = eINSTANCE
				.getModelLinkPositionConstraint();

		/**
		 * The meta object literal for the '<em><b>Constraint Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE = eINSTANCE
				.getModelLinkPositionConstraint_ConstraintType();

		/**
		 * The meta object literal for the '<em><b>Model Link</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_LINK_POSITION_CONSTRAINT__MODEL_LINK = eINSTANCE
				.getModelLinkPositionConstraint_ModelLink();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.tgg.TGGModifierEnumeration <em>TGG Modifier Enumeration</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.tgg.TGGModifierEnumeration
		 * @see de.hpi.sam.tgg.impl.TggPackageImpl#getTGGModifierEnumeration()
		 * @generated
		 */
		EEnum TGG_MODIFIER_ENUMERATION = eINSTANCE.getTGGModifierEnumeration();

	}

} // TggPackage
