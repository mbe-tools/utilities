/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

import org.eclipse.emf.ecore.EClassifier;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Rule Variable</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.RuleVariable#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.RuleVariable#getForwardCalculationExpression <em>Forward Calculation Expression</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.RuleVariable#getReverseCalculationExpression <em>Reverse Calculation Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.TggPackage#getRuleVariable()
 * @model
 * @generated
 */
public interface RuleVariable extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Classifier</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classifier</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Classifier</em>' reference.
	 * @see #setClassifier(EClassifier)
	 * @see de.hpi.sam.tgg.TggPackage#getRuleVariable_Classifier()
	 * @model required="true"
	 * @generated
	 */
	EClassifier getClassifier();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.RuleVariable#getClassifier <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Classifier</em>' reference.
	 * @see #getClassifier()
	 * @generated
	 */
	void setClassifier(EClassifier value);

	/**
	 * Returns the value of the '<em><b>Forward Calculation Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Forward Calculation Expression</em>'
	 * containment reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Forward Calculation Expression</em>' containment reference.
	 * @see #setForwardCalculationExpression(Expression)
	 * @see de.hpi.sam.tgg.TggPackage#getRuleVariable_ForwardCalculationExpression()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getForwardCalculationExpression();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.RuleVariable#getForwardCalculationExpression
	 * <em>Forward Calculation Expression</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Forward Calculation Expression</em>'
	 *            containment reference.
	 * @see #getForwardCalculationExpression()
	 * @generated
	 */
	void setForwardCalculationExpression(Expression value);

	/**
	 * Returns the value of the '<em><b>Reverse Calculation Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reverse Calculation Expression</em>'
	 * containment reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reverse Calculation Expression</em>' containment reference.
	 * @see #setReverseCalculationExpression(Expression)
	 * @see de.hpi.sam.tgg.TggPackage#getRuleVariable_ReverseCalculationExpression()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getReverseCalculationExpression();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.tgg.RuleVariable#getReverseCalculationExpression
	 * <em>Reverse Calculation Expression</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Reverse Calculation Expression</em>'
	 *            containment reference.
	 * @see #getReverseCalculationExpression()
	 * @generated
	 */
	void setReverseCalculationExpression(Expression value);

} // RuleVariable
