package de.hpi.sam.tgg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.tgg.CorrespondenceLink;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.TggPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Correspondence Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.impl.CorrespondenceNodeImpl#getOutgoingCorrespondenceLinks <em>Outgoing Correspondence Links</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.CorrespondenceNodeImpl#getClassifier <em>Classifier</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CorrespondenceNodeImpl extends CorrespondenceElementImpl implements
		CorrespondenceNode {
	/**
	 * The cached value of the '{@link #getOutgoingCorrespondenceLinks()
	 * <em>Outgoing Correspondence Links</em>}' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getOutgoingCorrespondenceLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<CorrespondenceLink> outgoingCorrespondenceLinks;

	/**
	 * The cached value of the '{@link #getClassifier() <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClassifier()
	 * @generated
	 * @ordered
	 */
	protected EClass classifier;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrespondenceNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.CORRESPONDENCE_NODE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CorrespondenceLink> getOutgoingCorrespondenceLinks() {
		if (outgoingCorrespondenceLinks == null) {
			outgoingCorrespondenceLinks = new EObjectWithInverseResolvingEList<CorrespondenceLink>(
					CorrespondenceLink.class,
					this,
					TggPackage.CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS,
					TggPackage.CORRESPONDENCE_LINK__SOURCE);
		}
		return outgoingCorrespondenceLinks;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassifier() {
		if (classifier != null && classifier.eIsProxy()) {
			InternalEObject oldClassifier = (InternalEObject) classifier;
			classifier = (EClass) eResolveProxy(oldClassifier);
			if (classifier != oldClassifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TggPackage.CORRESPONDENCE_NODE__CLASSIFIER,
							oldClassifier, classifier));
			}
		}
		return classifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass basicGetClassifier() {
		return classifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassifier(EClass newClassifier) {
		EClass oldClassifier = classifier;
		classifier = newClassifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.CORRESPONDENCE_NODE__CLASSIFIER, oldClassifier,
					classifier));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOutgoingCorrespondenceLinks())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS:
			return ((InternalEList<?>) getOutgoingCorrespondenceLinks())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS:
			return getOutgoingCorrespondenceLinks();
		case TggPackage.CORRESPONDENCE_NODE__CLASSIFIER:
			if (resolve)
				return getClassifier();
			return basicGetClassifier();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS:
			getOutgoingCorrespondenceLinks().clear();
			getOutgoingCorrespondenceLinks().addAll(
					(Collection<? extends CorrespondenceLink>) newValue);
			return;
		case TggPackage.CORRESPONDENCE_NODE__CLASSIFIER:
			setClassifier((EClass) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS:
			getOutgoingCorrespondenceLinks().clear();
			return;
		case TggPackage.CORRESPONDENCE_NODE__CLASSIFIER:
			setClassifier((EClass) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS:
			return outgoingCorrespondenceLinks != null
					&& !outgoingCorrespondenceLinks.isEmpty();
		case TggPackage.CORRESPONDENCE_NODE__CLASSIFIER:
			return classifier != null;
		}
		return super.eIsSet(featureID);
	}

} // CorrespondenceNodeImpl