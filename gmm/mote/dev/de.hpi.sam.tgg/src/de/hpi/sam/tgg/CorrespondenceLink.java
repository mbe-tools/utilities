/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Correspondence Link</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.CorrespondenceLink#getSource <em>Source</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.CorrespondenceLink#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.tgg.TggPackage#getCorrespondenceLink()
 * @model
 * @generated
 */
public interface CorrespondenceLink extends CorrespondenceElement {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.tgg.CorrespondenceNode#getOutgoingCorrespondenceLinks
	 * <em>Outgoing Correspondence Links</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The source CorrespondenceNode
	 * of this CorrespondenceLink. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(CorrespondenceNode)
	 * @see de.hpi.sam.tgg.TggPackage#getCorrespondenceLink_Source()
	 * @see de.hpi.sam.tgg.CorrespondenceNode#getOutgoingCorrespondenceLinks
	 * @model opposite="outgoingCorrespondenceLinks"
	 * @generated
	 */
	CorrespondenceNode getSource();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.CorrespondenceLink#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(CorrespondenceNode value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * target ModelObject of this CorrespondenceLink. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(ModelObject)
	 * @see de.hpi.sam.tgg.TggPackage#getCorrespondenceLink_Target()
	 * @model
	 * @generated
	 */
	ModelObject getTarget();

	/**
	 * Sets the value of the '{@link de.hpi.sam.tgg.CorrespondenceLink#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ModelObject value);

} // CorrespondenceLink
