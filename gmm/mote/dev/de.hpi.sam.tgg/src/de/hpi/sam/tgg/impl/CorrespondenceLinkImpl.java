/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.tgg.CorrespondenceLink;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TggPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Correspondence Link</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.tgg.impl.CorrespondenceLinkImpl#getSource <em>Source</em>}</li>
 *   <li>{@link de.hpi.sam.tgg.impl.CorrespondenceLinkImpl#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CorrespondenceLinkImpl extends CorrespondenceElementImpl implements
		CorrespondenceLink {
	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected CorrespondenceNode source;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected ModelObject target;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrespondenceLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.Literals.CORRESPONDENCE_LINK;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CorrespondenceNode getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject) source;
			source = (CorrespondenceNode) eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TggPackage.CORRESPONDENCE_LINK__SOURCE, oldSource,
							source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CorrespondenceNode basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSource(CorrespondenceNode newSource,
			NotificationChain msgs) {
		CorrespondenceNode oldSource = source;
		source = newSource;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, TggPackage.CORRESPONDENCE_LINK__SOURCE,
					oldSource, newSource);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(CorrespondenceNode newSource) {
		if (newSource != source) {
			NotificationChain msgs = null;
			if (source != null)
				msgs = ((InternalEObject) source)
						.eInverseRemove(
								this,
								TggPackage.CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS,
								CorrespondenceNode.class, msgs);
			if (newSource != null)
				msgs = ((InternalEObject) newSource)
						.eInverseAdd(
								this,
								TggPackage.CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS,
								CorrespondenceNode.class, msgs);
			msgs = basicSetSource(newSource, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.CORRESPONDENCE_LINK__SOURCE, newSource,
					newSource));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModelObject getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject) target;
			target = (ModelObject) eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TggPackage.CORRESPONDENCE_LINK__TARGET, oldTarget,
							target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModelObject basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(ModelObject newTarget) {
		ModelObject oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TggPackage.CORRESPONDENCE_LINK__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_LINK__SOURCE:
			if (source != null)
				msgs = ((InternalEObject) source)
						.eInverseRemove(
								this,
								TggPackage.CORRESPONDENCE_NODE__OUTGOING_CORRESPONDENCE_LINKS,
								CorrespondenceNode.class, msgs);
			return basicSetSource((CorrespondenceNode) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_LINK__SOURCE:
			return basicSetSource(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_LINK__SOURCE:
			if (resolve)
				return getSource();
			return basicGetSource();
		case TggPackage.CORRESPONDENCE_LINK__TARGET:
			if (resolve)
				return getTarget();
			return basicGetTarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_LINK__SOURCE:
			setSource((CorrespondenceNode) newValue);
			return;
		case TggPackage.CORRESPONDENCE_LINK__TARGET:
			setTarget((ModelObject) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_LINK__SOURCE:
			setSource((CorrespondenceNode) null);
			return;
		case TggPackage.CORRESPONDENCE_LINK__TARGET:
			setTarget((ModelObject) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TggPackage.CORRESPONDENCE_LINK__SOURCE:
			return source != null;
		case TggPackage.CORRESPONDENCE_LINK__TARGET:
			return target != null;
		}
		return super.eIsSet(featureID);
	}

} // CorrespondenceLinkImpl
