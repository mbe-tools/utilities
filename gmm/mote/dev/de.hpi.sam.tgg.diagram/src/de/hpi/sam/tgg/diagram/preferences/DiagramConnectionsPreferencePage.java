package de.hpi.sam.tgg.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.ConnectionsPreferencePage;

import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramConnectionsPreferencePage extends ConnectionsPreferencePage
{

	/**
	 * @generated
	 */
	public DiagramConnectionsPreferencePage()
	{
		setPreferenceStore(TggDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
