package de.hpi.sam.tgg.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import de.hpi.sam.tgg.diagram.edit.commands.CallActionExpression5CreateCommand;
import de.hpi.sam.tgg.diagram.edit.commands.StringExpression3CreateCommand;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class RuleVariableRuleVariableForExpressionCompartmentItemSemanticEditPolicy extends TggBaseItemSemanticEditPolicy
{

	/**
	 * @generated
	 */
	public RuleVariableRuleVariableForExpressionCompartmentItemSemanticEditPolicy()
	{
		super(TggElementTypes.RuleVariable_2008);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req)
	{
		if (TggElementTypes.StringExpression_3009 == req.getElementType())
		{
			return getGEFWrapper(new StringExpression3CreateCommand(req));
		}
		if (TggElementTypes.CallActionExpression_3010 == req.getElementType())
		{
			return getGEFWrapper(new CallActionExpression5CreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
