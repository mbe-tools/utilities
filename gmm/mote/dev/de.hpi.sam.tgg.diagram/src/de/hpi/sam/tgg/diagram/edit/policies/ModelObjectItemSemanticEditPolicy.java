package de.hpi.sam.tgg.diagram.edit.policies;

import java.util.Iterator;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.ICompositeCommand;
import org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.diagram.edit.commands.CorrespondenceLinkCreateCommand;
import de.hpi.sam.tgg.diagram.edit.commands.CorrespondenceLinkReorientCommand;
import de.hpi.sam.tgg.diagram.edit.commands.ModelLinkCreateCommand;
import de.hpi.sam.tgg.diagram.edit.commands.ModelLinkReorientCommand;
import de.hpi.sam.tgg.diagram.edit.parts.AttributeAssignmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectModelObjectConstraintsCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpressionEditPart;
import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class ModelObjectItemSemanticEditPolicy extends TggBaseItemSemanticEditPolicy
{

	/**
	 * @generated
	 */
	public ModelObjectItemSemanticEditPolicy()
	{
		super(TggElementTypes.ModelObject_3002);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req)
	{
		View view = (View) getHost().getModel();
		CompositeTransactionalCommand cmd = new CompositeTransactionalCommand(getEditingDomain(), null);
		cmd.setTransactionNestingEnabled(false);
		for (Iterator it = view.getTargetEdges().iterator(); it.hasNext();)
		{
			Edge incomingLink = (Edge) it.next();
			if (TggVisualIDRegistry.getVisualID(incomingLink) == CorrespondenceLinkEditPart.VISUAL_ID)
			{
				DestroyElementRequest r = new DestroyElementRequest(incomingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
			if (TggVisualIDRegistry.getVisualID(incomingLink) == ModelLinkEditPart.VISUAL_ID)
			{
				DestroyElementRequest r = new DestroyElementRequest(incomingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
		}
		for (Iterator it = view.getSourceEdges().iterator(); it.hasNext();)
		{
			Edge outgoingLink = (Edge) it.next();
			if (TggVisualIDRegistry.getVisualID(outgoingLink) == ModelLinkEditPart.VISUAL_ID)
			{
				DestroyElementRequest r = new DestroyElementRequest(outgoingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
		}
		EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
		if (annotation == null)
		{
			// there are indirectly referenced children, need extra commands:
			// false
			addDestroyChildNodesCommand(cmd);
			addDestroyShortcutsCommand(cmd, view);
			// delete host element
			cmd.add(new DestroyElementCommand(req));
		}
		else
		{
			cmd.add(new DeleteCommand(getEditingDomain(), view));
		}
		return getGEFWrapper(cmd.reduce());
	}

	/**
	 * @generated
	 */
	private void addDestroyChildNodesCommand(ICompositeCommand cmd)
	{
		View view = (View) getHost().getModel();
		for (Iterator nit = view.getChildren().iterator(); nit.hasNext();)
		{
			Node node = (Node) nit.next();
			switch (TggVisualIDRegistry.getVisualID(node))
			{
				case ModelObjectModelObjectConstraintsCompartmentEditPart.VISUAL_ID:
					for (Iterator cit = node.getChildren().iterator(); cit.hasNext();)
					{
						Node cnode = (Node) cit.next();
						switch (TggVisualIDRegistry.getVisualID(cnode))
						{
							case StringExpressionEditPart.VISUAL_ID:
								cmd.add(new DestroyElementCommand(new DestroyElementRequest(getEditingDomain(), cnode.getElement(), false))); // directlyOwned:
																																				// true
								// don't need explicit deletion of cnode as
								// parent's view deletion would clean child
								// views as well
								// cmd.add(new
								// org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand(getEditingDomain(),
								// cnode));
								break;
							case CallActionExpressionEditPart.VISUAL_ID:
								cmd.add(new DestroyElementCommand(new DestroyElementRequest(getEditingDomain(), cnode.getElement(), false))); // directlyOwned:
																																				// true
								// don't need explicit deletion of cnode as
								// parent's view deletion would clean child
								// views as well
								// cmd.add(new
								// org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand(getEditingDomain(),
								// cnode));
								break;
						}
					}
					break;
				case ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID:
					for (Iterator cit = node.getChildren().iterator(); cit.hasNext();)
					{
						Node cnode = (Node) cit.next();
						switch (TggVisualIDRegistry.getVisualID(cnode))
						{
							case AttributeAssignmentEditPart.VISUAL_ID:
								cmd.add(new DestroyElementCommand(new DestroyElementRequest(getEditingDomain(), cnode.getElement(), false))); // directlyOwned:
																																				// true
								// don't need explicit deletion of cnode as
								// parent's view deletion would clean child
								// views as well
								// cmd.add(new
								// org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand(getEditingDomain(),
								// cnode));
								break;
						}
					}
					break;
			}
		}
	}

	/**
	 * @generated
	 */
	protected Command getCreateRelationshipCommand(CreateRelationshipRequest req)
	{
		Command command = req.getTarget() == null ? getStartCreateRelationshipCommand(req) : getCompleteCreateRelationshipCommand(req);
		return command != null ? command : super.getCreateRelationshipCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getStartCreateRelationshipCommand(CreateRelationshipRequest req)
	{
		if (TggElementTypes.CorrespondenceLink_4001 == req.getElementType())
		{
			return null;
		}
		if (TggElementTypes.ModelLink_4002 == req.getElementType())
		{
			return getGEFWrapper(new ModelLinkCreateCommand(req, req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Command getCompleteCreateRelationshipCommand(CreateRelationshipRequest req)
	{
		if (TggElementTypes.CorrespondenceLink_4001 == req.getElementType())
		{
			return getGEFWrapper(new CorrespondenceLinkCreateCommand(req, req.getSource(), req.getTarget()));
		}
		if (TggElementTypes.ModelLink_4002 == req.getElementType())
		{
			return getGEFWrapper(new ModelLinkCreateCommand(req, req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * Returns command to reorient EClass based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientRelationshipCommand(ReorientRelationshipRequest req)
	{
		switch (getVisualID(req))
		{
			case CorrespondenceLinkEditPart.VISUAL_ID:
				return getGEFWrapper(new CorrespondenceLinkReorientCommand(req));
			case ModelLinkEditPart.VISUAL_ID:
				return getGEFWrapper(new ModelLinkReorientCommand(req));
		}
		return super.getReorientRelationshipCommand(req);
	}

}
