package de.hpi.sam.tgg.diagram.edit.parts;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.diagram.edit.policies.CorrespondenceLinkItemSemanticEditPolicy;

/**
 * @generated
 */
public class CorrespondenceLinkEditPart extends ConnectionNodeEditPart implements ITreeBranchEditPart
{

	/**
	 * @generated
	 */
	public static final int	VISUAL_ID	= 4001;

	/**
	 * @generated
	 */
	public CorrespondenceLinkEditPart(View view)
	{
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies()
	{
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new CorrespondenceLinkItemSemanticEditPolicy());
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart)
	{
		if (childEditPart instanceof CorrespondenceLinkModifierEditPart)
		{
			((CorrespondenceLinkModifierEditPart) childEditPart).setLabel(getPrimaryShape().getFigureCorrespondenceLinkModifier());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index)
	{
		if (addFixedChild(childEditPart))
		{
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart)
	{
		if (childEditPart instanceof CorrespondenceLinkModifierEditPart)
		{
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart)
	{
		if (removeFixedChild(childEditPart))
		{
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */

	protected Connection createConnectionFigure()
	{
		return new CorrespondenceLinkFigure();
	}

	/**
	 * @generated
	 */
	public CorrespondenceLinkFigure getPrimaryShape()
	{
		return (CorrespondenceLinkFigure) getFigure();
	}

	/**
	 * @generated
	 */
	public class CorrespondenceLinkFigure extends PolylineConnectionEx
	{

		/**
		 * @generated
		 */
		private WrappingLabel	fFigureCorrespondenceLinkModifier;

		/**
		 * @generated
		 */
		public CorrespondenceLinkFigure()
		{
			this.setLineWidth(1);
			this.setForegroundColor(ColorConstants.black);

			createContents();
			setTargetDecoration(createTargetDecoration());
		}

		/**
		 * @generated
		 */
		private void createContents()
		{

			fFigureCorrespondenceLinkModifier = new WrappingLabel();
			fFigureCorrespondenceLinkModifier.setText("");

			this.add(fFigureCorrespondenceLinkModifier);

		}

		/**
		 * @generated
		 */
		private RotatableDecoration createTargetDecoration()
		{
			PolylineDecoration df = new PolylineDecoration();
			df.setLineWidth(1);
			df.setForegroundColor(ColorConstants.black);
			return df;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureCorrespondenceLinkModifier()
		{
			return fFigureCorrespondenceLinkModifier;
		}

	}

}
