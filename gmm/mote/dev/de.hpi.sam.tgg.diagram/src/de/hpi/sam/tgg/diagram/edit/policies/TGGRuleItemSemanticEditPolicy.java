package de.hpi.sam.tgg.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

import de.hpi.sam.tgg.diagram.edit.commands.CallActionExpression2CreateCommand;
import de.hpi.sam.tgg.diagram.edit.commands.CorrespondenceDomainCreateCommand;
import de.hpi.sam.tgg.diagram.edit.commands.RuleVariableCreateCommand;
import de.hpi.sam.tgg.diagram.edit.commands.SourceModelDomainCreateCommand;
import de.hpi.sam.tgg.diagram.edit.commands.StringExpression2CreateCommand;
import de.hpi.sam.tgg.diagram.edit.commands.TargetModelDomainCreateCommand;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class TGGRuleItemSemanticEditPolicy extends TggBaseItemSemanticEditPolicy
{

	/**
	 * @generated
	 */
	public TGGRuleItemSemanticEditPolicy()
	{
		super(TggElementTypes.TGGRule_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req)
	{
		if (TggElementTypes.CorrespondenceDomain_2001 == req.getElementType())
		{
			return getGEFWrapper(new CorrespondenceDomainCreateCommand(req));
		}
		if (TggElementTypes.SourceModelDomain_2002 == req.getElementType())
		{
			return getGEFWrapper(new SourceModelDomainCreateCommand(req));
		}
		if (TggElementTypes.TargetModelDomain_2003 == req.getElementType())
		{
			return getGEFWrapper(new TargetModelDomainCreateCommand(req));
		}
		if (TggElementTypes.StringExpression_2004 == req.getElementType())
		{
			return getGEFWrapper(new StringExpression2CreateCommand(req));
		}
		if (TggElementTypes.CallActionExpression_2005 == req.getElementType())
		{
			return getGEFWrapper(new CallActionExpression2CreateCommand(req));
		}
		if (TggElementTypes.RuleVariable_2008 == req.getElementType())
		{
			return getGEFWrapper(new RuleVariableCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req)
	{
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost()).getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends DuplicateEObjectsCommand
	{

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(TransactionalEditingDomain editingDomain, DuplicateElementsRequest req)
		{
			super(editingDomain, req.getLabel(), req.getElementsToBeDuplicated(), req.getAllDuplicatedElementsMap());
		}

	}

}
