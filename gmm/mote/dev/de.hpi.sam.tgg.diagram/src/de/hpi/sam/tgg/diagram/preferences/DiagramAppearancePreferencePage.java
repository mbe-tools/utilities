package de.hpi.sam.tgg.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.AppearancePreferencePage;

import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramAppearancePreferencePage extends AppearancePreferencePage
{

	/**
	 * @generated
	 */
	public DiagramAppearancePreferencePage()
	{
		setPreferenceStore(TggDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
