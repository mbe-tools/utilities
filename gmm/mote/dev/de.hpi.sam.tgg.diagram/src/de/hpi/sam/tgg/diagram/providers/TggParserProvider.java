package de.hpi.sam.tgg.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression3EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression4EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpressionExpressionStringEditPart;
import de.hpi.sam.tgg.diagram.parsers.MessageFormatParser;
import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;

/**
 * @generated
 */
public class TggParserProvider extends AbstractProvider implements IParserProvider
{

	/**
	 * @generated
	 */
	private IParser	stringExpressionExpressionString_5008Parser;

	/**
	 * @generated
	 */
	private IParser getStringExpressionExpressionString_5008Parser()
	{
		if (stringExpressionExpressionString_5008Parser == null)
		{
			EAttribute[] features = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression_ExpressionString()
			};
			EAttribute[] editableFeatures = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression_ExpressionString()
			};
			MessageFormatParser parser = new MessageFormatParser(features, editableFeatures);
			parser.setViewPattern("[ {0} ]"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			stringExpressionExpressionString_5008Parser = parser;
		}
		return stringExpressionExpressionString_5008Parser;
	}

	/**
	 * @generated
	 */
	private IParser	ruleVariableName_5012Parser;

	/**
	 * @generated
	 */
	private IParser getRuleVariableName_5012Parser()
	{
		if (ruleVariableName_5012Parser == null)
		{
			EAttribute[] features = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE.getNamedElement_Name()
			};
			EAttribute[] editableFeatures = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE.getNamedElement_Name()
			};
			MessageFormatParser parser = new MessageFormatParser(features, editableFeatures);
			ruleVariableName_5012Parser = parser;
		}
		return ruleVariableName_5012Parser;
	}

	/**
	 * @generated
	 */
	private IParser	correspondenceNodeName_5001Parser;

	/**
	 * @generated
	 */
	private IParser getCorrespondenceNodeName_5001Parser()
	{
		if (correspondenceNodeName_5001Parser == null)
		{
			EAttribute[] features = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE.getNamedElement_Name()
			};
			EAttribute[] editableFeatures = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE.getNamedElement_Name()
			};
			MessageFormatParser parser = new MessageFormatParser(features, editableFeatures);
			correspondenceNodeName_5001Parser = parser;
		}
		return correspondenceNodeName_5001Parser;
	}

	/**
	 * @generated
	 */
	private IParser	modelObjectName_5004Parser;

	/**
	 * @generated
	 */
	private IParser getModelObjectName_5004Parser()
	{
		if (modelObjectName_5004Parser == null)
		{
			EAttribute[] features = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE.getNamedElement_Name()
			};
			EAttribute[] editableFeatures = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage.eINSTANCE.getNamedElement_Name()
			};
			MessageFormatParser parser = new MessageFormatParser(features, editableFeatures);
			modelObjectName_5004Parser = parser;
		}
		return modelObjectName_5004Parser;
	}

	/**
	 * @generated
	 */
	private IParser	stringExpression_3003Parser;

	/**
	 * @generated
	 */
	private IParser getStringExpression_3003Parser()
	{
		if (stringExpression_3003Parser == null)
		{
			EAttribute[] features = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression_ExpressionString()
			};
			EAttribute[] editableFeatures = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression_ExpressionString()
			};
			MessageFormatParser parser = new MessageFormatParser(features, editableFeatures);
			parser.setViewPattern("[ {0} ]"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			stringExpression_3003Parser = parser;
		}
		return stringExpression_3003Parser;
	}

	/**
	 * @generated
	 */
	private IParser	stringExpression_3009Parser;

	/**
	 * @generated
	 */
	private IParser getStringExpression_3009Parser()
	{
		if (stringExpression_3009Parser == null)
		{
			EAttribute[] features = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression_ExpressionString()
			};
			EAttribute[] editableFeatures = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression_ExpressionString()
			};
			MessageFormatParser parser = new MessageFormatParser(features, editableFeatures);
			parser.setViewPattern("[ {0} ]"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			stringExpression_3009Parser = parser;
		}
		return stringExpression_3009Parser;
	}

	/**
	 * @generated
	 */
	private IParser	stringExpression_3011Parser;

	/**
	 * @generated
	 */
	private IParser getStringExpression_3011Parser()
	{
		if (stringExpression_3011Parser == null)
		{
			EAttribute[] features = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression_ExpressionString()
			};
			EAttribute[] editableFeatures = new EAttribute[]
			{
				de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression_ExpressionString()
			};
			MessageFormatParser parser = new MessageFormatParser(features, editableFeatures);
			parser.setViewPattern("[ {0} ]"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			stringExpression_3011Parser = parser;
		}
		return stringExpression_3011Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID)
	{
		switch (visualID)
		{
			case StringExpressionExpressionStringEditPart.VISUAL_ID:
				return getStringExpressionExpressionString_5008Parser();
			case RuleVariableNameEditPart.VISUAL_ID:
				return getRuleVariableName_5012Parser();
			case CorrespondenceNodeNameEditPart.VISUAL_ID:
				return getCorrespondenceNodeName_5001Parser();
			case ModelObjectNameEditPart.VISUAL_ID:
				return getModelObjectName_5004Parser();
			case StringExpressionEditPart.VISUAL_ID:
				return getStringExpression_3003Parser();
			case StringExpression3EditPart.VISUAL_ID:
				return getStringExpression_3009Parser();
			case StringExpression4EditPart.VISUAL_ID:
				return getStringExpression_3011Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * 
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object, String parserHint)
	{
		return ParserService.getInstance().getParser(new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint)
	{
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null)
		{
			return getParser(TggVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null)
		{
			return getParser(TggVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation)
	{
		if (operation instanceof GetParserOperation)
		{
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (TggElementTypes.getElement(hint) == null)
			{
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter
	{

		/**
		 * @generated
		 */
		private final IElementType	elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint)
		{
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter)
		{
			if (IElementType.class.equals(adapter))
			{
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
