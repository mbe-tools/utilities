package de.hpi.sam.tgg.diagram.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.modelingassistant.ModelingAssistantProvider;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceDomainCorrespondenceDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.SourceModelDomainModelDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TGGRuleEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TargetModelDomainModelDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.part.Messages;
import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;

/**
 * @generated
 */
public class TggModelingAssistantProvider extends ModelingAssistantProvider
{

	/**
	 * @generated
	 */
	public List getTypesForPopupBar(IAdaptable host)
	{
		IGraphicalEditPart editPart = (IGraphicalEditPart) host.getAdapter(IGraphicalEditPart.class);
		if (editPart instanceof RuleVariableEditPart)
		{
			ArrayList types = new ArrayList(4);
			types.add(TggElementTypes.StringExpression_3009);
			types.add(TggElementTypes.CallActionExpression_3010);
			types.add(TggElementTypes.StringExpression_3011);
			types.add(TggElementTypes.CallActionExpression_3012);
			return types;
		}
		if (editPart instanceof ModelObjectEditPart)
		{
			ArrayList types = new ArrayList(3);
			types.add(TggElementTypes.StringExpression_3003);
			types.add(TggElementTypes.CallActionExpression_3004);
			types.add(TggElementTypes.AttributeAssignment_3005);
			return types;
		}
		if (editPart instanceof CorrespondenceDomainCorrespondenceDomainCompartmentEditPart)
		{
			ArrayList types = new ArrayList(1);
			types.add(TggElementTypes.CorrespondenceNode_3001);
			return types;
		}
		if (editPart instanceof SourceModelDomainModelDomainCompartmentEditPart)
		{
			ArrayList types = new ArrayList(1);
			types.add(TggElementTypes.ModelObject_3002);
			return types;
		}
		if (editPart instanceof TargetModelDomainModelDomainCompartmentEditPart)
		{
			ArrayList types = new ArrayList(1);
			types.add(TggElementTypes.ModelObject_3002);
			return types;
		}
		if (editPart instanceof TGGRuleEditPart)
		{
			ArrayList types = new ArrayList(6);
			types.add(TggElementTypes.CorrespondenceDomain_2001);
			types.add(TggElementTypes.SourceModelDomain_2002);
			types.add(TggElementTypes.TargetModelDomain_2003);
			types.add(TggElementTypes.StringExpression_2004);
			types.add(TggElementTypes.CallActionExpression_2005);
			types.add(TggElementTypes.RuleVariable_2008);
			return types;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnSource(IAdaptable source)
	{
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof CorrespondenceNodeEditPart)
		{
			return ((CorrespondenceNodeEditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof ModelObjectEditPart)
		{
			return ((ModelObjectEditPart) sourceEditPart).getMARelTypesOnSource();
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnTarget(IAdaptable target)
	{
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof ModelObjectEditPart)
		{
			return ((ModelObjectEditPart) targetEditPart).getMARelTypesOnTarget();
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnSourceAndTarget(IAdaptable source, IAdaptable target)
	{
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof CorrespondenceNodeEditPart)
		{
			return ((CorrespondenceNodeEditPart) sourceEditPart).getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof ModelObjectEditPart)
		{
			return ((ModelObjectEditPart) sourceEditPart).getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getTypesForSource(IAdaptable target, IElementType relationshipType)
	{
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof ModelObjectEditPart)
		{
			return ((ModelObjectEditPart) targetEditPart).getMATypesForSource(relationshipType);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getTypesForTarget(IAdaptable source, IElementType relationshipType)
	{
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof CorrespondenceNodeEditPart)
		{
			return ((CorrespondenceNodeEditPart) sourceEditPart).getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof ModelObjectEditPart)
		{
			return ((ModelObjectEditPart) sourceEditPart).getMATypesForTarget(relationshipType);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForSource(IAdaptable target, IElementType relationshipType)
	{
		return selectExistingElement(target, getTypesForSource(target, relationshipType));
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForTarget(IAdaptable source, IElementType relationshipType)
	{
		return selectExistingElement(source, getTypesForTarget(source, relationshipType));
	}

	/**
	 * @generated
	 */
	protected EObject selectExistingElement(IAdaptable host, Collection types)
	{
		if (types.isEmpty())
		{
			return null;
		}
		IGraphicalEditPart editPart = (IGraphicalEditPart) host.getAdapter(IGraphicalEditPart.class);
		if (editPart == null)
		{
			return null;
		}
		Diagram diagram = (Diagram) editPart.getRoot().getContents().getModel();
		Collection elements = new HashSet();
		for (Iterator it = diagram.getElement().eAllContents(); it.hasNext();)
		{
			EObject element = (EObject) it.next();
			if (isApplicableElement(element, types))
			{
				elements.add(element);
			}
		}
		if (elements.isEmpty())
		{
			return null;
		}
		return selectElement((EObject[]) elements.toArray(new EObject[elements.size()]));
	}

	/**
	 * @generated
	 */
	protected boolean isApplicableElement(EObject element, Collection types)
	{
		IElementType type = ElementTypeRegistry.getInstance().getElementType(element);
		return types.contains(type);
	}

	/**
	 * @generated
	 */
	protected EObject selectElement(EObject[] elements)
	{
		Shell shell = Display.getCurrent().getActiveShell();
		ILabelProvider labelProvider = new AdapterFactoryLabelProvider(TggDiagramEditorPlugin.getInstance()
				.getItemProvidersAdapterFactory());
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(shell, labelProvider);
		dialog.setMessage(Messages.TggModelingAssistantProviderMessage);
		dialog.setTitle(Messages.TggModelingAssistantProviderTitle);
		dialog.setMultipleSelection(false);
		dialog.setElements(elements);
		EObject selected = null;
		if (dialog.open() == Window.OK)
		{
			selected = (EObject) dialog.getFirstResult();
		}
		return selected;
	}
}
