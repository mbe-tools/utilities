package de.hpi.sam.tgg.diagram.part;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.core.services.view.CreateDiagramViewOperation;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.notation.Bounds;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.SourceModelDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TGGRuleEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TargetModelDomainEditPart;

/**
 * @generated
 */
public class TggNewDiagramFileWizard extends Wizard
{

	/**
	 * @generated
	 */
	private WizardNewFileCreationPage	myFileCreationPage;

	/**
	 * @generated
	 */
	private ModelElementSelectionPage	diagramRootElementSelectionPage;

	/**
	 * @generated
	 */
	private TransactionalEditingDomain	myEditingDomain;

	/**
	 * @generated
	 */
	public TggNewDiagramFileWizard(URI domainModelURI, EObject diagramRoot, TransactionalEditingDomain editingDomain)
	{
		assert domainModelURI != null : "Domain model uri must be specified"; //$NON-NLS-1$
		assert diagramRoot != null : "Doagram root element must be specified"; //$NON-NLS-1$
		assert editingDomain != null : "Editing domain must be specified"; //$NON-NLS-1$

		myFileCreationPage = new WizardNewFileCreationPage(Messages.TggNewDiagramFileWizard_CreationPageName, StructuredSelection.EMPTY);
		myFileCreationPage.setTitle(Messages.TggNewDiagramFileWizard_CreationPageTitle);
		myFileCreationPage.setDescription(NLS.bind(Messages.TggNewDiagramFileWizard_CreationPageDescription, TGGRuleEditPart.MODEL_ID));
		IPath filePath;
		String fileName = URI.decode(domainModelURI.trimFileExtension().lastSegment());
		if (domainModelURI.isPlatformResource())
		{
			filePath = new Path(domainModelURI.trimSegments(1).toPlatformString(true));
		}
		else if (domainModelURI.isFile())
		{
			filePath = new Path(domainModelURI.trimSegments(1).toFileString());
		}
		else
		{
			// TODO : use some default path
			throw new IllegalArgumentException("Unsupported URI: " + domainModelURI); //$NON-NLS-1$
		}
		myFileCreationPage.setContainerFullPath(filePath);
		myFileCreationPage.setFileName(TggDiagramEditorUtil.getUniqueFileName(filePath, fileName, "tggdiag")); //$NON-NLS-1$

		diagramRootElementSelectionPage = new DiagramRootElementSelectionPage(Messages.TggNewDiagramFileWizard_RootSelectionPageName);
		diagramRootElementSelectionPage.setTitle(Messages.TggNewDiagramFileWizard_RootSelectionPageTitle);
		diagramRootElementSelectionPage.setDescription(Messages.TggNewDiagramFileWizard_RootSelectionPageDescription);
		diagramRootElementSelectionPage.setModelElement(diagramRoot);

		myEditingDomain = editingDomain;
	}

	/**
	 * @generated
	 */
	public void addPages()
	{
		addPage(myFileCreationPage);
		addPage(diagramRootElementSelectionPage);
	}

	/**
	 * @generated
	 */
	public boolean performFinish()
	{
		List affectedFiles = new LinkedList();
		IFile diagramFile = myFileCreationPage.createNewFile();
		TggDiagramEditorUtil.setCharset(diagramFile);
		affectedFiles.add(diagramFile);
		URI diagramModelURI = URI.createPlatformResourceURI(diagramFile.getFullPath().toString(), true);
		ResourceSet resourceSet = myEditingDomain.getResourceSet();
		final Resource diagramResource = resourceSet.createResource(diagramModelURI);
		AbstractTransactionalCommand command = new AbstractTransactionalCommand(myEditingDomain,
				Messages.TggNewDiagramFileWizard_InitDiagramCommand, affectedFiles)
		{

			protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
			{
				int diagramVID = TggVisualIDRegistry.getDiagramVisualID(diagramRootElementSelectionPage.getModelElement());
				if (diagramVID != TGGRuleEditPart.VISUAL_ID)
				{
					return CommandResult.newErrorCommandResult(Messages.TggNewDiagramFileWizard_IncorrectRootError);
				}
				Diagram diagram = ViewService.createDiagram(diagramRootElementSelectionPage.getModelElement(), TGGRuleEditPart.MODEL_ID,
						TggDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);

				final int PADDING = 15;
				final int WIDTH = 200;
				TGGRule rule = (TGGRule) diagram.getElement();

				Node n1 = ViewService.createNode(diagram, rule.getSourceDomain(), SourceModelDomainEditPart.VISUAL_ID + "",
						TggDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);

				Bounds bounds = (Bounds) n1.getLayoutConstraint();
				bounds.setX(PADDING);
				bounds.setY(PADDING);
				bounds.setWidth(WIDTH);

				Node n2 = ViewService.createNode(diagram, rule.getCorrespondenceDomain(), CorrespondenceDomainEditPart.VISUAL_ID + "",
						TggDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);

				bounds = (Bounds) n2.getLayoutConstraint();
				bounds.setX(2 * PADDING + WIDTH);
				bounds.setY(PADDING);
				bounds.setWidth(WIDTH);

				Node n3 = ViewService.createNode(diagram, rule.getTargetDomain(), TargetModelDomainEditPart.VISUAL_ID + "",
						TggDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);

				bounds = (Bounds) n3.getLayoutConstraint();
				bounds.setX(3 * PADDING + 2 * WIDTH);
				bounds.setY(PADDING);
				bounds.setWidth(WIDTH);

				diagramResource.getContents().add(diagram);
				return CommandResult.newOKCommandResult();
			}
		};
		try
		{
			OperationHistoryFactory.getOperationHistory().execute(command, new NullProgressMonitor(), null);
			diagramResource.save(TggDiagramEditorUtil.getSaveOptions());
			TggDiagramEditorUtil.openDiagram(diagramResource);
		}
		catch (ExecutionException e)
		{
			TggDiagramEditorPlugin.getInstance().logError("Unable to create model and diagram", e); //$NON-NLS-1$
		}
		catch (IOException ex)
		{
			TggDiagramEditorPlugin.getInstance().logError("Save operation failed for: " + diagramModelURI, ex); //$NON-NLS-1$
		}
		catch (PartInitException ex)
		{
			TggDiagramEditorPlugin.getInstance().logError("Unable to open editor", ex); //$NON-NLS-1$
		}
		return true;
	}

	/**
	 * @generated
	 */
	private static class DiagramRootElementSelectionPage extends ModelElementSelectionPage
	{

		/**
		 * @generated
		 */
		protected DiagramRootElementSelectionPage(String pageName)
		{
			super(pageName);
		}

		/**
		 * @generated
		 */
		protected String getSelectionTitle()
		{
			return Messages.TggNewDiagramFileWizard_RootSelectionPageSelectionTitle;
		}

		/**
		 * @generated
		 */
		protected boolean validatePage()
		{
			if (selectedModelElement == null)
			{
				setErrorMessage(Messages.TggNewDiagramFileWizard_RootSelectionPageNoSelectionMessage);
				return false;
			}
			boolean result = ViewService.getInstance().provides(
					new CreateDiagramViewOperation(new EObjectAdapter(selectedModelElement), TGGRuleEditPart.MODEL_ID,
							TggDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT));
			setErrorMessage(result ? null : Messages.TggNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage);
			return result;
		}
	}
}
