package de.hpi.sam.tgg.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import de.hpi.sam.tgg.diagram.edit.commands.CorrespondenceNodeCreateCommand;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class CorrespondenceDomainCorrespondenceDomainCompartmentItemSemanticEditPolicy extends TggBaseItemSemanticEditPolicy
{

	/**
	 * @generated
	 */
	public CorrespondenceDomainCorrespondenceDomainCompartmentItemSemanticEditPolicy()
	{
		super(TggElementTypes.CorrespondenceDomain_2001);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req)
	{
		if (TggElementTypes.CorrespondenceNode_3001 == req.getElementType())
		{
			return getGEFWrapper(new CorrespondenceNodeCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
