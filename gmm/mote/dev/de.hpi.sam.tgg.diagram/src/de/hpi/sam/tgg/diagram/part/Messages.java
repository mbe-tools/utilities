package de.hpi.sam.tgg.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS
{

	/**
	 * @generated
	 */
	static
	{
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages()
	{
	}

	/**
	 * @generated
	 */
	public static String	TggCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String	TggCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String	TggCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String	TggCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String	TggCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String	TggCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String	TggCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String	TggCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String	TggDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String	TggDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String	TggDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String	TggDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String	TggDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String	TggDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String	TggDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String	TggDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String	TggDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String	TggDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String	TggDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String	TggDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String	TggDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String	InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String	InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String	InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String	InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String	TggNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String	TggNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String	TggNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String	TggNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String	TggNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String	TggNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String	TggNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String	TggNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String	TggNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String	TggNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String	TggNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String	TggDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String	TggDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String	TggDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String	TggDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String	TggDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String	TggElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String	ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String	ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String	Elements1Group_title;

	/**
	 * @generated
	 */
	public static String	Domains2Group_title;

	/**
	 * @generated
	 */
	public static String	ModelObject1CreationTool_title;

	/**
	 * @generated
	 */
	public static String	ModelObject1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String	ModelLink2CreationTool_title;

	/**
	 * @generated
	 */
	public static String	ModelLink2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String	CorrespondenceNode3CreationTool_title;

	/**
	 * @generated
	 */
	public static String	CorrespondenceNode3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String	CorrespondenceLink4CreationTool_title;

	/**
	 * @generated
	 */
	public static String	CorrespondenceLink4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String	AttributeAssignment5CreationTool_title;

	/**
	 * @generated
	 */
	public static String	AttributeAssignment5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String	StringExpression6CreationTool_title;

	/**
	 * @generated
	 */
	public static String	CallActionExpression7CreationTool_title;

	/**
	 * @generated
	 */
	public static String	RuleVariable8CreationTool_title;

	/**
	 * @generated
	 */
	public static String	SourceModelDomain1CreationTool_title;

	/**
	 * @generated
	 */
	public static String	SourceModelDomain1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String	CorrespondenceDomain2CreationTool_title;

	/**
	 * @generated
	 */
	public static String	CorrespondenceDomain2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String	TargetModelDomain3CreationTool_title;

	/**
	 * @generated
	 */
	public static String	TargetModelDomain3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String	CorrespondenceDomainCorrespondenceDomainCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String	SourceModelDomainModelDomainCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String	ModelObjectModelObjectConstraintsCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String	ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String	TargetModelDomainModelDomainCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String	RuleVariableRuleVariableForExpressionCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String	RuleVariableRuleVariableRevExpressionCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String	CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String	NavigatorGroupName_TGGRule_1000_links;

	/**
	 * @generated
	 */
	public static String	NavigatorGroupName_CorrespondenceNode_3001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String	NavigatorGroupName_ModelObject_3002_incominglinks;

	/**
	 * @generated
	 */
	public static String	NavigatorGroupName_ModelObject_3002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String	NavigatorGroupName_CorrespondenceLink_4001_target;

	/**
	 * @generated
	 */
	public static String	NavigatorGroupName_CorrespondenceLink_4001_source;

	/**
	 * @generated
	 */
	public static String	NavigatorGroupName_ModelLink_4002_target;

	/**
	 * @generated
	 */
	public static String	NavigatorGroupName_ModelLink_4002_source;

	/**
	 * @generated
	 */
	public static String	NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String	AbstractParser_UnexpectedValueType;

	/**
	 * @generated
	 */
	public static String	AbstractParser_WrongStringConversion;

	/**
	 * @generated
	 */
	public static String	AbstractParser_UnknownLiteral;

	/**
	 * @generated
	 */
	public static String	MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String	TggModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String	TggModelingAssistantProviderMessage;

	// TODO: put accessor fields manually
}
