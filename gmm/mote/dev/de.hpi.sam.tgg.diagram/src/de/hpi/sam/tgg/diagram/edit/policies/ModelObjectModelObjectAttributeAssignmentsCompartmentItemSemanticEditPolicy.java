package de.hpi.sam.tgg.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import de.hpi.sam.tgg.diagram.edit.commands.AttributeAssignmentCreateCommand;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class ModelObjectModelObjectAttributeAssignmentsCompartmentItemSemanticEditPolicy extends TggBaseItemSemanticEditPolicy
{

	/**
	 * @generated
	 */
	public ModelObjectModelObjectAttributeAssignmentsCompartmentItemSemanticEditPolicy()
	{
		super(TggElementTypes.ModelObject_3002);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req)
	{
		if (TggElementTypes.AttributeAssignment_3005 == req.getElementType())
		{
			return getGEFWrapper(new AttributeAssignmentCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
