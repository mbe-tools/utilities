package de.hpi.sam.tgg.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.DiagramsPreferencePage;

import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramGeneralPreferencePage extends DiagramsPreferencePage
{

	/**
	 * @generated
	 */
	public DiagramGeneralPreferencePage()
	{
		setPreferenceStore(TggDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
