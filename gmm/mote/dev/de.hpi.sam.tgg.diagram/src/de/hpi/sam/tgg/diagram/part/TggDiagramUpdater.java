package de.hpi.sam.tgg.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.CorrespondenceDomain;
import de.hpi.sam.tgg.CorrespondenceElement;
import de.hpi.sam.tgg.CorrespondenceLink;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.ModelDomain;
import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.RuleVariable;
import de.hpi.sam.tgg.SourceModelDomain;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TargetModelDomain;
import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.diagram.edit.parts.AttributeAssignmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression2EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression4EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression5EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceDomainCorrespondenceDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectModelObjectConstraintsCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableRuleVariableForExpressionCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableRuleVariableRevExpressionCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.SourceModelDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.SourceModelDomainModelDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression2EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression3EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression4EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TGGRuleEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TargetModelDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TargetModelDomainModelDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class TggDiagramUpdater
{

	/**
	 * @generated
	 */
	public static boolean isShortcutOrphaned(View view)
	{
		return !view.isSetElement() || view.getElement() == null || view.getElement().eIsProxy();
	}

	/**
	 * @generated
	 */
	public static List getSemanticChildren(View view)
	{
		switch (TggVisualIDRegistry.getVisualID(view))
		{
			case CorrespondenceDomainCorrespondenceDomainCompartmentEditPart.VISUAL_ID:
				return getCorrespondenceDomainCorrespondenceDomainCompartment_7001SemanticChildren(view);
			case SourceModelDomainModelDomainCompartmentEditPart.VISUAL_ID:
				return getSourceModelDomainModelDomainCompartment_7002SemanticChildren(view);
			case ModelObjectModelObjectConstraintsCompartmentEditPart.VISUAL_ID:
				return getModelObjectModelObjectConstraintsCompartment_7004SemanticChildren(view);
			case ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID:
				return getModelObjectModelObjectAttributeAssignmentsCompartment_7005SemanticChildren(view);
			case TargetModelDomainModelDomainCompartmentEditPart.VISUAL_ID:
				return getTargetModelDomainModelDomainCompartment_7003SemanticChildren(view);
			case RuleVariableRuleVariableForExpressionCompartmentEditPart.VISUAL_ID:
				return getRuleVariableRuleVariableForExpressionCompartment_7006SemanticChildren(view);
			case RuleVariableRuleVariableRevExpressionCompartmentEditPart.VISUAL_ID:
				return getRuleVariableRuleVariableRevExpressionCompartment_7007SemanticChildren(view);
			case TGGRuleEditPart.VISUAL_ID:
				return getTGGRule_1000SemanticChildren(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCorrespondenceDomainCorrespondenceDomainCompartment_7001SemanticChildren(View view)
	{
		if (false == view.eContainer() instanceof View)
		{
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement())
		{
			return Collections.EMPTY_LIST;
		}
		CorrespondenceDomain modelElement = (CorrespondenceDomain) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getCorrespondenceElements().iterator(); it.hasNext();)
		{
			CorrespondenceElement childElement = (CorrespondenceElement) it.next();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == CorrespondenceNodeEditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSourceModelDomainModelDomainCompartment_7002SemanticChildren(View view)
	{
		if (false == view.eContainer() instanceof View)
		{
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement())
		{
			return Collections.EMPTY_LIST;
		}
		SourceModelDomain modelElement = (SourceModelDomain) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getModelElements().iterator(); it.hasNext();)
		{
			ModelElement childElement = (ModelElement) it.next();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == ModelObjectEditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getModelObjectModelObjectConstraintsCompartment_7004SemanticChildren(View view)
	{
		if (false == view.eContainer() instanceof View)
		{
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement())
		{
			return Collections.EMPTY_LIST;
		}
		ModelObject modelElement = (ModelObject) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getConstraintExpressions().iterator(); it.hasNext();)
		{
			de.hpi.sam.storyDiagramEcore.expressions.Expression childElement = (de.hpi.sam.storyDiagramEcore.expressions.Expression) it
					.next();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == StringExpressionEditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == CallActionExpressionEditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getModelObjectModelObjectAttributeAssignmentsCompartment_7005SemanticChildren(View view)
	{
		if (false == view.eContainer() instanceof View)
		{
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement())
		{
			return Collections.EMPTY_LIST;
		}
		ModelObject modelElement = (ModelObject) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getAttributeAssignments().iterator(); it.hasNext();)
		{
			de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment childElement = (de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment) it
					.next();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == AttributeAssignmentEditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getTargetModelDomainModelDomainCompartment_7003SemanticChildren(View view)
	{
		if (false == view.eContainer() instanceof View)
		{
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement())
		{
			return Collections.EMPTY_LIST;
		}
		TargetModelDomain modelElement = (TargetModelDomain) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getModelElements().iterator(); it.hasNext();)
		{
			ModelElement childElement = (ModelElement) it.next();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == ModelObjectEditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRuleVariableRuleVariableForExpressionCompartment_7006SemanticChildren(View view)
	{
		if (false == view.eContainer() instanceof View)
		{
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement())
		{
			return Collections.EMPTY_LIST;
		}
		RuleVariable modelElement = (RuleVariable) containerView.getElement();
		List result = new LinkedList();
		{
			de.hpi.sam.storyDiagramEcore.expressions.Expression childElement = modelElement.getForwardCalculationExpression();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == StringExpression3EditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
			}
			if (visualID == CallActionExpression5EditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRuleVariableRuleVariableRevExpressionCompartment_7007SemanticChildren(View view)
	{
		if (false == view.eContainer() instanceof View)
		{
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement())
		{
			return Collections.EMPTY_LIST;
		}
		RuleVariable modelElement = (RuleVariable) containerView.getElement();
		List result = new LinkedList();
		{
			de.hpi.sam.storyDiagramEcore.expressions.Expression childElement = modelElement.getReverseCalculationExpression();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == StringExpression4EditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
			}
			if (visualID == CallActionExpression4EditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getTGGRule_1000SemanticChildren(View view)
	{
		if (!view.isSetElement())
		{
			return Collections.EMPTY_LIST;
		}
		TGGRule modelElement = (TGGRule) view.getElement();
		List result = new LinkedList();
		{
			CorrespondenceDomain childElement = modelElement.getCorrespondenceDomain();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == CorrespondenceDomainEditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
			}
		}
		{
			SourceModelDomain childElement = modelElement.getSourceDomain();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == SourceModelDomainEditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
			}
		}
		{
			TargetModelDomain childElement = modelElement.getTargetDomain();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == TargetModelDomainEditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
			}
		}
		for (Iterator it = modelElement.getConstraintExpressions().iterator(); it.hasNext();)
		{
			de.hpi.sam.storyDiagramEcore.expressions.Expression childElement = (de.hpi.sam.storyDiagramEcore.expressions.Expression) it
					.next();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == StringExpression2EditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == CallActionExpression2EditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getRuleVariables().iterator(); it.hasNext();)
		{
			RuleVariable childElement = (RuleVariable) it.next();
			int visualID = TggVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == RuleVariableEditPart.VISUAL_ID)
			{
				result.add(new TggNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getContainedLinks(View view)
	{
		switch (TggVisualIDRegistry.getVisualID(view))
		{
			case TGGRuleEditPart.VISUAL_ID:
				return getTGGRule_1000ContainedLinks(view);
			case CorrespondenceDomainEditPart.VISUAL_ID:
				return getCorrespondenceDomain_2001ContainedLinks(view);
			case SourceModelDomainEditPart.VISUAL_ID:
				return getSourceModelDomain_2002ContainedLinks(view);
			case TargetModelDomainEditPart.VISUAL_ID:
				return getTargetModelDomain_2003ContainedLinks(view);
			case StringExpression2EditPart.VISUAL_ID:
				return getStringExpression_2004ContainedLinks(view);
			case CallActionExpression2EditPart.VISUAL_ID:
				return getCallActionExpression_2005ContainedLinks(view);
			case RuleVariableEditPart.VISUAL_ID:
				return getRuleVariable_2008ContainedLinks(view);
			case CorrespondenceNodeEditPart.VISUAL_ID:
				return getCorrespondenceNode_3001ContainedLinks(view);
			case ModelObjectEditPart.VISUAL_ID:
				return getModelObject_3002ContainedLinks(view);
			case StringExpressionEditPart.VISUAL_ID:
				return getStringExpression_3003ContainedLinks(view);
			case CallActionExpressionEditPart.VISUAL_ID:
				return getCallActionExpression_3004ContainedLinks(view);
			case AttributeAssignmentEditPart.VISUAL_ID:
				return getAttributeAssignment_3005ContainedLinks(view);
			case StringExpression3EditPart.VISUAL_ID:
				return getStringExpression_3009ContainedLinks(view);
			case CallActionExpression5EditPart.VISUAL_ID:
				return getCallActionExpression_3010ContainedLinks(view);
			case StringExpression4EditPart.VISUAL_ID:
				return getStringExpression_3011ContainedLinks(view);
			case CallActionExpression4EditPart.VISUAL_ID:
				return getCallActionExpression_3012ContainedLinks(view);
			case CorrespondenceLinkEditPart.VISUAL_ID:
				return getCorrespondenceLink_4001ContainedLinks(view);
			case ModelLinkEditPart.VISUAL_ID:
				return getModelLink_4002ContainedLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getIncomingLinks(View view)
	{
		switch (TggVisualIDRegistry.getVisualID(view))
		{
			case CorrespondenceDomainEditPart.VISUAL_ID:
				return getCorrespondenceDomain_2001IncomingLinks(view);
			case SourceModelDomainEditPart.VISUAL_ID:
				return getSourceModelDomain_2002IncomingLinks(view);
			case TargetModelDomainEditPart.VISUAL_ID:
				return getTargetModelDomain_2003IncomingLinks(view);
			case StringExpression2EditPart.VISUAL_ID:
				return getStringExpression_2004IncomingLinks(view);
			case CallActionExpression2EditPart.VISUAL_ID:
				return getCallActionExpression_2005IncomingLinks(view);
			case RuleVariableEditPart.VISUAL_ID:
				return getRuleVariable_2008IncomingLinks(view);
			case CorrespondenceNodeEditPart.VISUAL_ID:
				return getCorrespondenceNode_3001IncomingLinks(view);
			case ModelObjectEditPart.VISUAL_ID:
				return getModelObject_3002IncomingLinks(view);
			case StringExpressionEditPart.VISUAL_ID:
				return getStringExpression_3003IncomingLinks(view);
			case CallActionExpressionEditPart.VISUAL_ID:
				return getCallActionExpression_3004IncomingLinks(view);
			case AttributeAssignmentEditPart.VISUAL_ID:
				return getAttributeAssignment_3005IncomingLinks(view);
			case StringExpression3EditPart.VISUAL_ID:
				return getStringExpression_3009IncomingLinks(view);
			case CallActionExpression5EditPart.VISUAL_ID:
				return getCallActionExpression_3010IncomingLinks(view);
			case StringExpression4EditPart.VISUAL_ID:
				return getStringExpression_3011IncomingLinks(view);
			case CallActionExpression4EditPart.VISUAL_ID:
				return getCallActionExpression_3012IncomingLinks(view);
			case CorrespondenceLinkEditPart.VISUAL_ID:
				return getCorrespondenceLink_4001IncomingLinks(view);
			case ModelLinkEditPart.VISUAL_ID:
				return getModelLink_4002IncomingLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getOutgoingLinks(View view)
	{
		switch (TggVisualIDRegistry.getVisualID(view))
		{
			case CorrespondenceDomainEditPart.VISUAL_ID:
				return getCorrespondenceDomain_2001OutgoingLinks(view);
			case SourceModelDomainEditPart.VISUAL_ID:
				return getSourceModelDomain_2002OutgoingLinks(view);
			case TargetModelDomainEditPart.VISUAL_ID:
				return getTargetModelDomain_2003OutgoingLinks(view);
			case StringExpression2EditPart.VISUAL_ID:
				return getStringExpression_2004OutgoingLinks(view);
			case CallActionExpression2EditPart.VISUAL_ID:
				return getCallActionExpression_2005OutgoingLinks(view);
			case RuleVariableEditPart.VISUAL_ID:
				return getRuleVariable_2008OutgoingLinks(view);
			case CorrespondenceNodeEditPart.VISUAL_ID:
				return getCorrespondenceNode_3001OutgoingLinks(view);
			case ModelObjectEditPart.VISUAL_ID:
				return getModelObject_3002OutgoingLinks(view);
			case StringExpressionEditPart.VISUAL_ID:
				return getStringExpression_3003OutgoingLinks(view);
			case CallActionExpressionEditPart.VISUAL_ID:
				return getCallActionExpression_3004OutgoingLinks(view);
			case AttributeAssignmentEditPart.VISUAL_ID:
				return getAttributeAssignment_3005OutgoingLinks(view);
			case StringExpression3EditPart.VISUAL_ID:
				return getStringExpression_3009OutgoingLinks(view);
			case CallActionExpression5EditPart.VISUAL_ID:
				return getCallActionExpression_3010OutgoingLinks(view);
			case StringExpression4EditPart.VISUAL_ID:
				return getStringExpression_3011OutgoingLinks(view);
			case CallActionExpression4EditPart.VISUAL_ID:
				return getCallActionExpression_3012OutgoingLinks(view);
			case CorrespondenceLinkEditPart.VISUAL_ID:
				return getCorrespondenceLink_4001OutgoingLinks(view);
			case ModelLinkEditPart.VISUAL_ID:
				return getModelLink_4002OutgoingLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getTGGRule_1000ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCorrespondenceDomain_2001ContainedLinks(View view)
	{
		CorrespondenceDomain modelElement = (CorrespondenceDomain) view.getElement();
		List result = new LinkedList();
		result.addAll(getContainedTypeModelFacetLinks_CorrespondenceLink_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSourceModelDomain_2002ContainedLinks(View view)
	{
		SourceModelDomain modelElement = (SourceModelDomain) view.getElement();
		List result = new LinkedList();
		result.addAll(getContainedTypeModelFacetLinks_ModelLink_4002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getTargetModelDomain_2003ContainedLinks(View view)
	{
		TargetModelDomain modelElement = (TargetModelDomain) view.getElement();
		List result = new LinkedList();
		result.addAll(getContainedTypeModelFacetLinks_ModelLink_4002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getStringExpression_2004ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCallActionExpression_2005ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRuleVariable_2008ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCorrespondenceNode_3001ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModelObject_3002ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getStringExpression_3003ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCallActionExpression_3004ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttributeAssignment_3005ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getStringExpression_3009ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCallActionExpression_3010ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getStringExpression_3011ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCallActionExpression_3012ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCorrespondenceLink_4001ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModelLink_4002ContainedLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCorrespondenceDomain_2001IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getSourceModelDomain_2002IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getTargetModelDomain_2003IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getStringExpression_2004IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCallActionExpression_2005IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRuleVariable_2008IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCorrespondenceNode_3001IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModelObject_3002IncomingLinks(View view)
	{
		ModelObject modelElement = (ModelObject) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource().getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_CorrespondenceLink_4001(modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ModelLink_4002(modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getStringExpression_3003IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCallActionExpression_3004IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttributeAssignment_3005IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getStringExpression_3009IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCallActionExpression_3010IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getStringExpression_3011IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCallActionExpression_3012IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCorrespondenceLink_4001IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModelLink_4002IncomingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCorrespondenceDomain_2001OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getSourceModelDomain_2002OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getTargetModelDomain_2003OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getStringExpression_2004OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCallActionExpression_2005OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRuleVariable_2008OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCorrespondenceNode_3001OutgoingLinks(View view)
	{
		CorrespondenceNode modelElement = (CorrespondenceNode) view.getElement();
		List result = new LinkedList();
		result.addAll(getOutgoingTypeModelFacetLinks_CorrespondenceLink_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getModelObject_3002OutgoingLinks(View view)
	{
		ModelObject modelElement = (ModelObject) view.getElement();
		List result = new LinkedList();
		result.addAll(getOutgoingTypeModelFacetLinks_ModelLink_4002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getStringExpression_3003OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCallActionExpression_3004OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttributeAssignment_3005OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getStringExpression_3009OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCallActionExpression_3010OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getStringExpression_3011OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCallActionExpression_3012OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCorrespondenceLink_4001OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModelLink_4002OutgoingLinks(View view)
	{
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	private static Collection getContainedTypeModelFacetLinks_CorrespondenceLink_4001(CorrespondenceDomain container)
	{
		Collection result = new LinkedList();
		for (Iterator links = container.getCorrespondenceElements().iterator(); links.hasNext();)
		{
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof CorrespondenceLink)
			{
				continue;
			}
			CorrespondenceLink link = (CorrespondenceLink) linkObject;
			if (CorrespondenceLinkEditPart.VISUAL_ID != TggVisualIDRegistry.getLinkWithClassVisualID(link))
			{
				continue;
			}
			ModelObject dst = link.getTarget();
			CorrespondenceNode src = link.getSource();
			result.add(new TggLinkDescriptor(src, dst, link, TggElementTypes.CorrespondenceLink_4001, CorrespondenceLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getContainedTypeModelFacetLinks_ModelLink_4002(ModelDomain container)
	{
		Collection result = new LinkedList();
		for (Iterator links = container.getModelElements().iterator(); links.hasNext();)
		{
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof ModelLink)
			{
				continue;
			}
			ModelLink link = (ModelLink) linkObject;
			if (ModelLinkEditPart.VISUAL_ID != TggVisualIDRegistry.getLinkWithClassVisualID(link))
			{
				continue;
			}
			ModelObject dst = link.getTarget();
			ModelObject src = link.getSource();
			result.add(new TggLinkDescriptor(src, dst, link, TggElementTypes.ModelLink_4002, ModelLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getIncomingTypeModelFacetLinks_CorrespondenceLink_4001(ModelObject target, Map crossReferences)
	{
		Collection result = new LinkedList();
		Collection settings = (Collection) crossReferences.get(target);
		for (Iterator it = settings.iterator(); it.hasNext();)
		{
			EStructuralFeature.Setting setting = (EStructuralFeature.Setting) it.next();
			if (setting.getEStructuralFeature() != TggPackage.eINSTANCE.getCorrespondenceLink_Target()
					|| false == setting.getEObject() instanceof CorrespondenceLink)
			{
				continue;
			}
			CorrespondenceLink link = (CorrespondenceLink) setting.getEObject();
			if (CorrespondenceLinkEditPart.VISUAL_ID != TggVisualIDRegistry.getLinkWithClassVisualID(link))
			{
				continue;
			}
			CorrespondenceNode src = link.getSource();
			result.add(new TggLinkDescriptor(src, target, link, TggElementTypes.CorrespondenceLink_4001,
					CorrespondenceLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getIncomingTypeModelFacetLinks_ModelLink_4002(ModelObject target, Map crossReferences)
	{
		Collection result = new LinkedList();
		Collection settings = (Collection) crossReferences.get(target);
		for (Iterator it = settings.iterator(); it.hasNext();)
		{
			EStructuralFeature.Setting setting = (EStructuralFeature.Setting) it.next();
			if (setting.getEStructuralFeature() != TggPackage.eINSTANCE.getModelLink_Target()
					|| false == setting.getEObject() instanceof ModelLink)
			{
				continue;
			}
			ModelLink link = (ModelLink) setting.getEObject();
			if (ModelLinkEditPart.VISUAL_ID != TggVisualIDRegistry.getLinkWithClassVisualID(link))
			{
				continue;
			}
			ModelObject src = link.getSource();
			result.add(new TggLinkDescriptor(src, target, link, TggElementTypes.ModelLink_4002, ModelLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getOutgoingTypeModelFacetLinks_CorrespondenceLink_4001(CorrespondenceNode source)
	{
		CorrespondenceDomain container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element.eContainer())
		{
			if (element instanceof CorrespondenceDomain)
			{
				container = (CorrespondenceDomain) element;
			}
		}
		if (container == null)
		{
			return Collections.EMPTY_LIST;
		}
		Collection result = new LinkedList();
		for (Iterator links = container.getCorrespondenceElements().iterator(); links.hasNext();)
		{
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof CorrespondenceLink)
			{
				continue;
			}
			CorrespondenceLink link = (CorrespondenceLink) linkObject;
			if (CorrespondenceLinkEditPart.VISUAL_ID != TggVisualIDRegistry.getLinkWithClassVisualID(link))
			{
				continue;
			}
			ModelObject dst = link.getTarget();
			CorrespondenceNode src = link.getSource();
			if (src != source)
			{
				continue;
			}
			result.add(new TggLinkDescriptor(src, dst, link, TggElementTypes.CorrespondenceLink_4001, CorrespondenceLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getOutgoingTypeModelFacetLinks_ModelLink_4002(ModelObject source)
	{
		ModelDomain container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element.eContainer())
		{
			if (element instanceof ModelDomain)
			{
				container = (ModelDomain) element;
			}
		}
		if (container == null)
		{
			return Collections.EMPTY_LIST;
		}
		Collection result = new LinkedList();
		for (Iterator links = container.getModelElements().iterator(); links.hasNext();)
		{
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof ModelLink)
			{
				continue;
			}
			ModelLink link = (ModelLink) linkObject;
			if (ModelLinkEditPart.VISUAL_ID != TggVisualIDRegistry.getLinkWithClassVisualID(link))
			{
				continue;
			}
			ModelObject dst = link.getTarget();
			ModelObject src = link.getSource();
			if (src != source)
			{
				continue;
			}
			result.add(new TggLinkDescriptor(src, dst, link, TggElementTypes.ModelLink_4002, ModelLinkEditPart.VISUAL_ID));
		}
		return result;
	}

}
