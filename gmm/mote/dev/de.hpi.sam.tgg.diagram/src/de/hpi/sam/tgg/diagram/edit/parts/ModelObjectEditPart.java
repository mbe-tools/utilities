package de.hpi.sam.tgg.diagram.edit.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import de.hpi.sam.tgg.diagram.edit.policies.ModelObjectItemSemanticEditPolicy;
import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class ModelObjectEditPart extends ShapeNodeEditPart
{

	/**
	 * @generated
	 */
	public static final int	VISUAL_ID	= 3002;

	/**
	 * @generated
	 */
	protected IFigure		contentPane;

	/**
	 * @generated
	 */
	protected IFigure		primaryShape;

	/**
	 * @generated
	 */
	public ModelObjectEditPart(View view)
	{
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies()
	{
		installEditPolicy(EditPolicyRoles.CREATION_ROLE, new CreationEditPolicy());
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new ModelObjectItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that
		// would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy()
	{
		LayoutEditPolicy lep = new LayoutEditPolicy()
		{

			protected EditPolicy createChildEditPolicy(EditPart child)
			{
				EditPolicy result = child.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null)
				{
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request)
			{
				return null;
			}

			protected Command getCreateCommand(CreateRequest request)
			{
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape()
	{
		ModelObjectFigure figure = new ModelObjectFigure();
		return primaryShape = figure;
	}

	/**
	 * @generated
	 */
	public ModelObjectFigure getPrimaryShape()
	{
		return (ModelObjectFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart)
	{
		if (childEditPart instanceof ModelObjectNameEditPart)
		{
			((ModelObjectNameEditPart) childEditPart).setLabel(getPrimaryShape().getFigureModelObjectNameFigure());
			return true;
		}
		if (childEditPart instanceof ModelObjectClassifierEditPart)
		{
			((ModelObjectClassifierEditPart) childEditPart).setLabel(getPrimaryShape().getFigureModelObjectClassifierFigure());
			return true;
		}
		if (childEditPart instanceof ModelObjectModifierEditPart)
		{
			((ModelObjectModifierEditPart) childEditPart).setLabel(getPrimaryShape().getFigureModelObjectModifierFigure());
			return true;
		}
		if (childEditPart instanceof ModelObjectModelObjectConstraintsCompartmentEditPart)
		{
			IFigure pane = getPrimaryShape().getFigureModelObjectFigureConstraints();
			setupContentPane(pane); // FIXME each comparment should handle his
			// content pane in his own way
			pane.add(((ModelObjectModelObjectConstraintsCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		if (childEditPart instanceof ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart)
		{
			IFigure pane = getPrimaryShape().getFigureModelObjectFigureAttributeAssignments();
			setupContentPane(pane); // FIXME each comparment should handle his
			// content pane in his own way
			pane.add(((ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart)
	{
		if (childEditPart instanceof ModelObjectNameEditPart)
		{
			return true;
		}
		if (childEditPart instanceof ModelObjectClassifierEditPart)
		{
			return true;
		}
		if (childEditPart instanceof ModelObjectModifierEditPart)
		{
			return true;
		}
		if (childEditPart instanceof ModelObjectModelObjectConstraintsCompartmentEditPart)
		{
			IFigure pane = getPrimaryShape().getFigureModelObjectFigureConstraints();
			setupContentPane(pane); // FIXME each comparment should handle his
			// content pane in his own way
			pane.remove(((ModelObjectModelObjectConstraintsCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		if (childEditPart instanceof ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart)
		{
			IFigure pane = getPrimaryShape().getFigureModelObjectFigureAttributeAssignments();
			setupContentPane(pane); // FIXME each comparment should handle his
			// content pane in his own way
			pane.remove(((ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index)
	{
		if (addFixedChild(childEditPart))
		{
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart)
	{
		if (removeFixedChild(childEditPart))
		{
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart)
	{
		if (editPart instanceof ModelObjectModelObjectConstraintsCompartmentEditPart)
		{
			return getPrimaryShape().getFigureModelObjectFigureConstraints();
		}
		if (editPart instanceof ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart)
		{
			return getPrimaryShape().getFigureModelObjectFigureAttributeAssignments();
		}
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate()
	{
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(100, 86);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure()
	{
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane. Respects
	 * layout one may have set for generated figure.
	 * 
	 * @param nodeShape
	 *            instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape)
	{
		if (nodeShape.getLayoutManager() == null)
		{
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane()
	{
		if (contentPane != null)
		{
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color)
	{
		if (primaryShape != null)
		{
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color)
	{
		if (primaryShape != null)
		{
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width)
	{
		if (primaryShape instanceof Shape)
		{
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style)
	{
		if (primaryShape instanceof Shape)
		{
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart()
	{
		return getChildBySemanticHint(TggVisualIDRegistry.getType(ModelObjectNameEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */getMARelTypesOnSource()
	{
		List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */types = new ArrayList/*
																							 * <
																							 * org
																							 * .
																							 * eclipse
																							 * .
																							 * gmf
																							 * .
																							 * runtime
																							 * .
																							 * emf
																							 * .
																							 * type
																							 * .
																							 * core
																							 * .
																							 * IElementType
																							 * >
																							 */();
		types.add(TggElementTypes.ModelLink_4002);
		return types;
	}

	/**
	 * @generated
	 */
	public List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */getMARelTypesOnSourceAndTarget(IGraphicalEditPart targetEditPart)
	{
		List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */types = new ArrayList/*
																							 * <
																							 * org
																							 * .
																							 * eclipse
																							 * .
																							 * gmf
																							 * .
																							 * runtime
																							 * .
																							 * emf
																							 * .
																							 * type
																							 * .
																							 * core
																							 * .
																							 * IElementType
																							 * >
																							 */();
		if (targetEditPart instanceof de.hpi.sam.tgg.diagram.edit.parts.ModelObjectEditPart)
		{
			types.add(TggElementTypes.ModelLink_4002);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */getMATypesForTarget(IElementType relationshipType)
	{
		List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */types = new ArrayList/*
																							 * <
																							 * org
																							 * .
																							 * eclipse
																							 * .
																							 * gmf
																							 * .
																							 * runtime
																							 * .
																							 * emf
																							 * .
																							 * type
																							 * .
																							 * core
																							 * .
																							 * IElementType
																							 * >
																							 */();
		if (relationshipType == TggElementTypes.ModelLink_4002)
		{
			types.add(TggElementTypes.ModelObject_3002);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */getMARelTypesOnTarget()
	{
		List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */types = new ArrayList/*
																							 * <
																							 * org
																							 * .
																							 * eclipse
																							 * .
																							 * gmf
																							 * .
																							 * runtime
																							 * .
																							 * emf
																							 * .
																							 * type
																							 * .
																							 * core
																							 * .
																							 * IElementType
																							 * >
																							 */();
		types.add(TggElementTypes.CorrespondenceLink_4001);
		types.add(TggElementTypes.ModelLink_4002);
		return types;
	}

	/**
	 * @generated
	 */
	public List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */getMATypesForSource(IElementType relationshipType)
	{
		List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */types = new ArrayList/*
																							 * <
																							 * org
																							 * .
																							 * eclipse
																							 * .
																							 * gmf
																							 * .
																							 * runtime
																							 * .
																							 * emf
																							 * .
																							 * type
																							 * .
																							 * core
																							 * .
																							 * IElementType
																							 * >
																							 */();
		if (relationshipType == TggElementTypes.CorrespondenceLink_4001)
		{
			types.add(TggElementTypes.CorrespondenceNode_3001);
		}
		if (relationshipType == TggElementTypes.ModelLink_4002)
		{
			types.add(TggElementTypes.ModelObject_3002);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public EditPart getTargetEditPart(Request request)
	{
		if (request instanceof CreateViewAndElementRequest)
		{
			CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request).getViewAndElementDescriptor()
					.getCreateElementRequestAdapter();
			IElementType type = (IElementType) adapter.getAdapter(IElementType.class);
			if (type == TggElementTypes.StringExpression_3003)
			{
				return getChildBySemanticHint(TggVisualIDRegistry.getType(ModelObjectModelObjectConstraintsCompartmentEditPart.VISUAL_ID));
			}
			if (type == TggElementTypes.CallActionExpression_3004)
			{
				return getChildBySemanticHint(TggVisualIDRegistry.getType(ModelObjectModelObjectConstraintsCompartmentEditPart.VISUAL_ID));
			}
			if (type == TggElementTypes.AttributeAssignment_3005)
			{
				return getChildBySemanticHint(TggVisualIDRegistry
						.getType(ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID));
			}
		}
		return super.getTargetEditPart(request);
	}

	/**
	 * @generated
	 */
	public class ModelObjectFigure extends RectangleFigure
	{

		/**
		 * @generated
		 */
		private WrappingLabel	fFigureModelObjectModifierFigure;
		/**
		 * @generated
		 */
		private WrappingLabel	fFigureModelObjectNameFigure;
		/**
		 * @generated
		 */
		private WrappingLabel	fFigureModelObjectClassifierFigure;
		/**
		 * @generated
		 */
		private RectangleFigure	fFigureModelObjectFigureConstraints;
		/**
		 * @generated
		 */
		private RectangleFigure	fFigureModelObjectFigureAttributeAssignments;

		/**
		 * @generated
		 */
		public ModelObjectFigure()
		{

			BorderLayout layoutThis = new BorderLayout();
			this.setLayoutManager(layoutThis);

			this.setLineWidth(1);
			this.setForegroundColor(ColorConstants.black);
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents()
		{

			RectangleFigure modelObjectFigureHead0 = new RectangleFigure();
			modelObjectFigureHead0.setFill(false);
			modelObjectFigureHead0.setOutline(false);
			modelObjectFigureHead0.setLineWidth(0);

			this.add(modelObjectFigureHead0, BorderLayout.TOP);

			GridLayout layoutModelObjectFigureHead0 = new GridLayout();
			layoutModelObjectFigureHead0.numColumns = 1;
			layoutModelObjectFigureHead0.makeColumnsEqualWidth = true;
			modelObjectFigureHead0.setLayoutManager(layoutModelObjectFigureHead0);

			fFigureModelObjectModifierFigure = new WrappingLabel();
			fFigureModelObjectModifierFigure.setText("");

			GridData constraintFFigureModelObjectModifierFigure = new GridData();
			constraintFFigureModelObjectModifierFigure.verticalAlignment = GridData.BEGINNING;
			constraintFFigureModelObjectModifierFigure.horizontalAlignment = GridData.CENTER;
			constraintFFigureModelObjectModifierFigure.horizontalIndent = 0;
			constraintFFigureModelObjectModifierFigure.horizontalSpan = 1;
			constraintFFigureModelObjectModifierFigure.verticalSpan = 1;
			constraintFFigureModelObjectModifierFigure.grabExcessHorizontalSpace = true;
			constraintFFigureModelObjectModifierFigure.grabExcessVerticalSpace = false;
			modelObjectFigureHead0.add(fFigureModelObjectModifierFigure, constraintFFigureModelObjectModifierFigure);

			fFigureModelObjectNameFigure = new WrappingLabel();
			fFigureModelObjectNameFigure.setText("");

			GridData constraintFFigureModelObjectNameFigure = new GridData();
			constraintFFigureModelObjectNameFigure.verticalAlignment = GridData.CENTER;
			constraintFFigureModelObjectNameFigure.horizontalAlignment = GridData.CENTER;
			constraintFFigureModelObjectNameFigure.horizontalIndent = 0;
			constraintFFigureModelObjectNameFigure.horizontalSpan = 1;
			constraintFFigureModelObjectNameFigure.verticalSpan = 1;
			constraintFFigureModelObjectNameFigure.grabExcessHorizontalSpace = true;
			constraintFFigureModelObjectNameFigure.grabExcessVerticalSpace = false;
			modelObjectFigureHead0.add(fFigureModelObjectNameFigure, constraintFFigureModelObjectNameFigure);

			fFigureModelObjectClassifierFigure = new WrappingLabel();
			fFigureModelObjectClassifierFigure.setText("");

			GridData constraintFFigureModelObjectClassifierFigure = new GridData();
			constraintFFigureModelObjectClassifierFigure.verticalAlignment = GridData.END;
			constraintFFigureModelObjectClassifierFigure.horizontalAlignment = GridData.CENTER;
			constraintFFigureModelObjectClassifierFigure.horizontalIndent = 0;
			constraintFFigureModelObjectClassifierFigure.horizontalSpan = 1;
			constraintFFigureModelObjectClassifierFigure.verticalSpan = 1;
			constraintFFigureModelObjectClassifierFigure.grabExcessHorizontalSpace = true;
			constraintFFigureModelObjectClassifierFigure.grabExcessVerticalSpace = false;
			modelObjectFigureHead0.add(fFigureModelObjectClassifierFigure, constraintFFigureModelObjectClassifierFigure);

			RectangleFigure modelObjectFigureBody0 = new RectangleFigure();
			modelObjectFigureBody0.setFill(false);
			modelObjectFigureBody0.setOutline(false);
			modelObjectFigureBody0.setLineWidth(0);

			this.add(modelObjectFigureBody0, BorderLayout.CENTER);

			BorderLayout layoutModelObjectFigureBody0 = new BorderLayout();
			modelObjectFigureBody0.setLayoutManager(layoutModelObjectFigureBody0);

			fFigureModelObjectFigureConstraints = new RectangleFigure();
			fFigureModelObjectFigureConstraints.setFill(false);
			fFigureModelObjectFigureConstraints.setOutline(false);
			fFigureModelObjectFigureConstraints.setLineWidth(0);

			modelObjectFigureBody0.add(fFigureModelObjectFigureConstraints, BorderLayout.TOP);

			fFigureModelObjectFigureAttributeAssignments = new RectangleFigure();
			fFigureModelObjectFigureAttributeAssignments.setFill(false);
			fFigureModelObjectFigureAttributeAssignments.setOutline(false);
			fFigureModelObjectFigureAttributeAssignments.setLineWidth(0);

			modelObjectFigureBody0.add(fFigureModelObjectFigureAttributeAssignments, BorderLayout.CENTER);

		}

		/**
		 * @generated
		 */
		private boolean	myUseLocalCoordinates	= false;

		/**
		 * @generated
		 */
		protected boolean useLocalCoordinates()
		{
			return myUseLocalCoordinates;
		}

		/**
		 * @generated
		 */
		protected void setUseLocalCoordinates(boolean useLocalCoordinates)
		{
			myUseLocalCoordinates = useLocalCoordinates;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureModelObjectModifierFigure()
		{
			return fFigureModelObjectModifierFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureModelObjectNameFigure()
		{
			return fFigureModelObjectNameFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureModelObjectClassifierFigure()
		{
			return fFigureModelObjectClassifierFigure;
		}

		/**
		 * @generated
		 */
		public RectangleFigure getFigureModelObjectFigureConstraints()
		{
			return fFigureModelObjectFigureConstraints;
		}

		/**
		 * @generated
		 */
		public RectangleFigure getFigureModelObjectFigureAttributeAssignments()
		{
			return fFigureModelObjectFigureAttributeAssignments;
		}

	}

}
