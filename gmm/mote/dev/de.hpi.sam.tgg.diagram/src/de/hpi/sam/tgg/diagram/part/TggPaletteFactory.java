package de.hpi.sam.tgg.diagram.part;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;

import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class TggPaletteFactory
{

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot)
	{
		paletteRoot.add(createElements1Group());
		paletteRoot.add(createDomains2Group());
	}

	/**
	 * Creates "Elements" palette tool group
	 * 
	 * @generated
	 */
	private PaletteContainer createElements1Group()
	{
		PaletteDrawer paletteContainer = new PaletteDrawer(Messages.Elements1Group_title);
		paletteContainer.setId("createElements1Group"); //$NON-NLS-1$
		paletteContainer.add(createModelObject1CreationTool());
		paletteContainer.add(createModelLink2CreationTool());
		paletteContainer.add(createCorrespondenceNode3CreationTool());
		paletteContainer.add(createCorrespondenceLink4CreationTool());
		paletteContainer.add(createAttributeAssignment5CreationTool());
		paletteContainer.add(createStringExpression6CreationTool());
		paletteContainer.add(createCallActionExpression7CreationTool());
		paletteContainer.add(createRuleVariable8CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Domains" palette tool group
	 * 
	 * @generated
	 */
	private PaletteContainer createDomains2Group()
	{
		PaletteDrawer paletteContainer = new PaletteDrawer(Messages.Domains2Group_title);
		paletteContainer.setId("createDomains2Group"); //$NON-NLS-1$
		paletteContainer.add(createSourceModelDomain1CreationTool());
		paletteContainer.add(createCorrespondenceDomain2CreationTool());
		paletteContainer.add(createTargetModelDomain3CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createModelObject1CreationTool()
	{
		List/* <IElementType> */types = new ArrayList/* <IElementType> */(1);
		types.add(TggElementTypes.ModelObject_3002);
		NodeToolEntry entry = new NodeToolEntry(Messages.ModelObject1CreationTool_title, Messages.ModelObject1CreationTool_desc, types);
		entry.setId("createModelObject1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggElementTypes.getImageDescriptor(TggElementTypes.ModelObject_3002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createModelLink2CreationTool()
	{
		List/* <IElementType> */types = new ArrayList/* <IElementType> */(1);
		types.add(TggElementTypes.ModelLink_4002);
		LinkToolEntry entry = new LinkToolEntry(Messages.ModelLink2CreationTool_title, Messages.ModelLink2CreationTool_desc, types);
		entry.setId("createModelLink2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggElementTypes.getImageDescriptor(TggElementTypes.ModelLink_4002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCorrespondenceNode3CreationTool()
	{
		List/* <IElementType> */types = new ArrayList/* <IElementType> */(1);
		types.add(TggElementTypes.CorrespondenceNode_3001);
		NodeToolEntry entry = new NodeToolEntry(Messages.CorrespondenceNode3CreationTool_title,
				Messages.CorrespondenceNode3CreationTool_desc, types);
		entry.setId("createCorrespondenceNode3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggElementTypes.getImageDescriptor(TggElementTypes.CorrespondenceNode_3001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCorrespondenceLink4CreationTool()
	{
		List/* <IElementType> */types = new ArrayList/* <IElementType> */(1);
		types.add(TggElementTypes.CorrespondenceLink_4001);
		LinkToolEntry entry = new LinkToolEntry(Messages.CorrespondenceLink4CreationTool_title,
				Messages.CorrespondenceLink4CreationTool_desc, types);
		entry.setId("createCorrespondenceLink4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggElementTypes.getImageDescriptor(TggElementTypes.CorrespondenceLink_4001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAttributeAssignment5CreationTool()
	{
		List/* <IElementType> */types = new ArrayList/* <IElementType> */(1);
		types.add(TggElementTypes.AttributeAssignment_3005);
		NodeToolEntry entry = new NodeToolEntry(Messages.AttributeAssignment5CreationTool_title,
				Messages.AttributeAssignment5CreationTool_desc, types);
		entry.setId("createAttributeAssignment5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggElementTypes.getImageDescriptor(TggElementTypes.AttributeAssignment_3005));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createStringExpression6CreationTool()
	{
		List/* <IElementType> */types = new ArrayList/* <IElementType> */(4);
		types.add(TggElementTypes.StringExpression_3003);
		types.add(TggElementTypes.StringExpression_2004);
		types.add(TggElementTypes.StringExpression_3009);
		types.add(TggElementTypes.StringExpression_3011);
		NodeToolEntry entry = new NodeToolEntry(Messages.StringExpression6CreationTool_title, null, types);
		entry.setId("createStringExpression6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/StringExpression.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCallActionExpression7CreationTool()
	{
		List/* <IElementType> */types = new ArrayList/* <IElementType> */(4);
		types.add(TggElementTypes.CallActionExpression_3004);
		types.add(TggElementTypes.CallActionExpression_2005);
		types.add(TggElementTypes.CallActionExpression_3010);
		types.add(TggElementTypes.CallActionExpression_3012);
		NodeToolEntry entry = new NodeToolEntry(Messages.CallActionExpression7CreationTool_title, null, types);
		entry.setId("createCallActionExpression7CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggDiagramEditorPlugin
				.findImageDescriptor("/de.hpi.sam.storyDiagramEcore.edit/icons/full/obj16/CallActionExpression.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createRuleVariable8CreationTool()
	{
		List/* <IElementType> */types = new ArrayList/* <IElementType> */(1);
		types.add(TggElementTypes.RuleVariable_2008);
		NodeToolEntry entry = new NodeToolEntry(Messages.RuleVariable8CreationTool_title, null, types);
		entry.setId("createRuleVariable8CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggElementTypes.getImageDescriptor(TggElementTypes.RuleVariable_2008));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createSourceModelDomain1CreationTool()
	{
		List/* <IElementType> */types = new ArrayList/* <IElementType> */(1);
		types.add(TggElementTypes.SourceModelDomain_2002);
		NodeToolEntry entry = new NodeToolEntry(Messages.SourceModelDomain1CreationTool_title,
				Messages.SourceModelDomain1CreationTool_desc, types);
		entry.setId("createSourceModelDomain1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggElementTypes.getImageDescriptor(TggElementTypes.SourceModelDomain_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCorrespondenceDomain2CreationTool()
	{
		List/* <IElementType> */types = new ArrayList/* <IElementType> */(1);
		types.add(TggElementTypes.CorrespondenceDomain_2001);
		NodeToolEntry entry = new NodeToolEntry(Messages.CorrespondenceDomain2CreationTool_title,
				Messages.CorrespondenceDomain2CreationTool_desc, types);
		entry.setId("createCorrespondenceDomain2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggElementTypes.getImageDescriptor(TggElementTypes.CorrespondenceDomain_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createTargetModelDomain3CreationTool()
	{
		List/* <IElementType> */types = new ArrayList/* <IElementType> */(1);
		types.add(TggElementTypes.TargetModelDomain_2003);
		NodeToolEntry entry = new NodeToolEntry(Messages.TargetModelDomain3CreationTool_title,
				Messages.TargetModelDomain3CreationTool_desc, types);
		entry.setId("createTargetModelDomain3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TggElementTypes.getImageDescriptor(TggElementTypes.TargetModelDomain_2003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry
	{

		/**
		 * @generated
		 */
		private final List	elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description, List elementTypes)
		{
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool()
		{
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry
	{

		/**
		 * @generated
		 */
		private final List	relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description, List relationshipTypes)
		{
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool()
		{
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
