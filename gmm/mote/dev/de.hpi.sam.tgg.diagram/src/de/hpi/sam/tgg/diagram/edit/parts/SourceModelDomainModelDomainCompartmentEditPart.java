package de.hpi.sam.tgg.diagram.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.diagram.edit.policies.SourceModelDomainModelDomainCompartmentCanonicalEditPolicy;
import de.hpi.sam.tgg.diagram.edit.policies.SourceModelDomainModelDomainCompartmentItemSemanticEditPolicy;
import de.hpi.sam.tgg.diagram.part.Messages;

/**
 * @generated
 */
public class SourceModelDomainModelDomainCompartmentEditPart extends ShapeCompartmentEditPart
{

	/**
	 * @generated
	 */
	public static final int	VISUAL_ID	= 7002;

	/**
	 * @generated
	 */
	public SourceModelDomainModelDomainCompartmentEditPart(View view)
	{
		super(view);
	}

	/**
	 * @generated
	 */
	public String getCompartmentName()
	{
		return Messages.SourceModelDomainModelDomainCompartmentEditPart_title;
	}

	/**
	 * @generated
	 */
	public IFigure createFigure()
	{
		ResizableCompartmentFigure result = (ResizableCompartmentFigure) super.createFigure();
		result.setTitleVisibility(false);
		return result;
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies()
	{
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new SourceModelDomainModelDomainCompartmentItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.CREATION_ROLE, new CreationEditPolicy());
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE, new DragDropEditPolicy());
		installEditPolicy(EditPolicyRoles.CANONICAL_ROLE, new SourceModelDomainModelDomainCompartmentCanonicalEditPolicy());
	}

	/**
	 * @generated
	 */
	protected void setRatio(Double ratio)
	{
		if (getFigure().getParent().getLayoutManager() instanceof ConstrainedToolbarLayout)
		{
			super.setRatio(ratio);
		}
	}

}
