package de.hpi.sam.tgg.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.diagram.edit.parts.AttributeAssignmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression2EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression3EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression4EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression5EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceDomainCorrespondenceDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceLinkModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeClassifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeInputEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkReferenceEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectClassifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectModelObjectConstraintsCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableClassifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableRuleVariableForExpressionCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableRuleVariableRevExpressionCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.SourceModelDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.SourceModelDomainModelDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression2EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression3EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression4EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpressionExpressionStringEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TGGRuleEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TargetModelDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TargetModelDomainModelDomainCompartmentEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented by
 * a domain model object.
 * 
 * @generated
 */
public class TggVisualIDRegistry
{

	/**
	 * @generated
	 */
	private static final String	DEBUG_KEY	= "de.hpi.sam.tgg.diagram/debug/visualID";	//$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view)
	{
		if (view instanceof Diagram)
		{
			if (TGGRuleEditPart.MODEL_ID.equals(view.getType()))
			{
				return TGGRuleEditPart.VISUAL_ID;
			}
			else
			{
				return -1;
			}
		}
		return de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view)
	{
		View diagram = view.getDiagram();
		while (view != diagram)
		{
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null)
			{
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type)
	{
		try
		{
			return Integer.parseInt(type);
		}
		catch (NumberFormatException e)
		{
			if (Boolean.TRUE.toString().equalsIgnoreCase(Platform.getDebugOption(DEBUG_KEY)))
			{
				TggDiagramEditorPlugin.getInstance().logError("Unable to parse view type as a visualID number: " + type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID)
	{
		return String.valueOf(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement)
	{
		if (domainElement == null)
		{
			return -1;
		}
		if (TggPackage.eINSTANCE.getTGGRule().isSuperTypeOf(domainElement.eClass()) && isDiagram((TGGRule) domainElement))
		{
			return TGGRuleEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement)
	{
		if (domainElement == null)
		{
			return -1;
		}
		String containerModelID = de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry.getModelID(containerView);
		if (!TGGRuleEditPart.MODEL_ID.equals(containerModelID)
				&& !"de.hpi.sam.tgg.diagram.part.TggDiagramEditorID".equals(containerModelID)) { //$NON-NLS-1$
			return -1;
		}
		int containerVisualID;
		if (TGGRuleEditPart.MODEL_ID.equals(containerModelID))
		{
			containerVisualID = de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry.getVisualID(containerView);
		}
		else
		{
			if (containerView instanceof Diagram)
			{
				containerVisualID = TGGRuleEditPart.VISUAL_ID;
			}
			else
			{
				return -1;
			}
		}
		switch (containerVisualID)
		{
			case CorrespondenceDomainCorrespondenceDomainCompartmentEditPart.VISUAL_ID:
				if (TggPackage.eINSTANCE.getCorrespondenceNode().isSuperTypeOf(domainElement.eClass()))
				{
					return CorrespondenceNodeEditPart.VISUAL_ID;
				}
				break;
			case SourceModelDomainModelDomainCompartmentEditPart.VISUAL_ID:
				if (TggPackage.eINSTANCE.getModelObject().isSuperTypeOf(domainElement.eClass()))
				{
					return ModelObjectEditPart.VISUAL_ID;
				}
				break;
			case ModelObjectModelObjectConstraintsCompartmentEditPart.VISUAL_ID:
				if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression().isSuperTypeOf(
						domainElement.eClass()))
				{
					return StringExpressionEditPart.VISUAL_ID;
				}
				if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getCallActionExpression().isSuperTypeOf(
						domainElement.eClass()))
				{
					return CallActionExpressionEditPart.VISUAL_ID;
				}
				break;
			case ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID:
				if (de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getAttributeAssignment().isSuperTypeOf(domainElement.eClass()))
				{
					return AttributeAssignmentEditPart.VISUAL_ID;
				}
				break;
			case TargetModelDomainModelDomainCompartmentEditPart.VISUAL_ID:
				if (TggPackage.eINSTANCE.getModelObject().isSuperTypeOf(domainElement.eClass()))
				{
					return ModelObjectEditPart.VISUAL_ID;
				}
				break;
			case RuleVariableRuleVariableForExpressionCompartmentEditPart.VISUAL_ID:
				if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression().isSuperTypeOf(
						domainElement.eClass()))
				{
					return StringExpression3EditPart.VISUAL_ID;
				}
				if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getCallActionExpression().isSuperTypeOf(
						domainElement.eClass()))
				{
					return CallActionExpression5EditPart.VISUAL_ID;
				}
				break;
			case RuleVariableRuleVariableRevExpressionCompartmentEditPart.VISUAL_ID:
				if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression().isSuperTypeOf(
						domainElement.eClass()))
				{
					return StringExpression4EditPart.VISUAL_ID;
				}
				if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getCallActionExpression().isSuperTypeOf(
						domainElement.eClass()))
				{
					return CallActionExpression4EditPart.VISUAL_ID;
				}
				break;
			case TGGRuleEditPart.VISUAL_ID:
				if (TggPackage.eINSTANCE.getCorrespondenceDomain().isSuperTypeOf(domainElement.eClass()))
				{
					return CorrespondenceDomainEditPart.VISUAL_ID;
				}
				if (TggPackage.eINSTANCE.getSourceModelDomain().isSuperTypeOf(domainElement.eClass()))
				{
					return SourceModelDomainEditPart.VISUAL_ID;
				}
				if (TggPackage.eINSTANCE.getTargetModelDomain().isSuperTypeOf(domainElement.eClass()))
				{
					return TargetModelDomainEditPart.VISUAL_ID;
				}
				if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression().isSuperTypeOf(
						domainElement.eClass()))
				{
					return StringExpression2EditPart.VISUAL_ID;
				}
				if (de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getCallActionExpression().isSuperTypeOf(
						domainElement.eClass()))
				{
					return CallActionExpression2EditPart.VISUAL_ID;
				}
				if (TggPackage.eINSTANCE.getRuleVariable().isSuperTypeOf(domainElement.eClass()))
				{
					return RuleVariableEditPart.VISUAL_ID;
				}
				break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID)
	{
		String containerModelID = de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry.getModelID(containerView);
		if (!TGGRuleEditPart.MODEL_ID.equals(containerModelID)
				&& !"de.hpi.sam.tgg.diagram.part.TggDiagramEditorID".equals(containerModelID)) { //$NON-NLS-1$
			return false;
		}
		int containerVisualID;
		if (TGGRuleEditPart.MODEL_ID.equals(containerModelID))
		{
			containerVisualID = de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry.getVisualID(containerView);
		}
		else
		{
			if (containerView instanceof Diagram)
			{
				containerVisualID = TGGRuleEditPart.VISUAL_ID;
			}
			else
			{
				return false;
			}
		}
		switch (containerVisualID)
		{
			case CorrespondenceDomainEditPart.VISUAL_ID:
				if (CorrespondenceDomainCorrespondenceDomainCompartmentEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case SourceModelDomainEditPart.VISUAL_ID:
				if (SourceModelDomainModelDomainCompartmentEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case TargetModelDomainEditPart.VISUAL_ID:
				if (TargetModelDomainModelDomainCompartmentEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case StringExpression2EditPart.VISUAL_ID:
				if (StringExpressionExpressionStringEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case CallActionExpression2EditPart.VISUAL_ID:
				if (CallActionExpression3EditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case RuleVariableEditPart.VISUAL_ID:
				if (RuleVariableNameEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (RuleVariableClassifierEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (RuleVariableRuleVariableForExpressionCompartmentEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (RuleVariableRuleVariableRevExpressionCompartmentEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case CorrespondenceNodeEditPart.VISUAL_ID:
				if (CorrespondenceNodeNameEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (CorrespondenceNodeClassifierEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (CorrespondenceNodeModifierEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (CorrespondenceNodeInputEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case ModelObjectEditPart.VISUAL_ID:
				if (ModelObjectNameEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (ModelObjectClassifierEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (ModelObjectModifierEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (ModelObjectModelObjectConstraintsCompartmentEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case CorrespondenceDomainCorrespondenceDomainCompartmentEditPart.VISUAL_ID:
				if (CorrespondenceNodeEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case SourceModelDomainModelDomainCompartmentEditPart.VISUAL_ID:
				if (ModelObjectEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case ModelObjectModelObjectConstraintsCompartmentEditPart.VISUAL_ID:
				if (StringExpressionEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (CallActionExpressionEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID:
				if (AttributeAssignmentEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case TargetModelDomainModelDomainCompartmentEditPart.VISUAL_ID:
				if (ModelObjectEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case RuleVariableRuleVariableForExpressionCompartmentEditPart.VISUAL_ID:
				if (StringExpression3EditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (CallActionExpression5EditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case RuleVariableRuleVariableRevExpressionCompartmentEditPart.VISUAL_ID:
				if (StringExpression4EditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (CallActionExpression4EditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case TGGRuleEditPart.VISUAL_ID:
				if (CorrespondenceDomainEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (SourceModelDomainEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (TargetModelDomainEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (StringExpression2EditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (CallActionExpression2EditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (RuleVariableEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case CorrespondenceLinkEditPart.VISUAL_ID:
				if (CorrespondenceLinkModifierEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
			case ModelLinkEditPart.VISUAL_ID:
				if (ModelLinkReferenceEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				if (ModelLinkModifierEditPart.VISUAL_ID == nodeVisualID)
				{
					return true;
				}
				break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement)
	{
		if (domainElement == null)
		{
			return -1;
		}
		if (TggPackage.eINSTANCE.getCorrespondenceLink().isSuperTypeOf(domainElement.eClass()))
		{
			return CorrespondenceLinkEditPart.VISUAL_ID;
		}
		if (TggPackage.eINSTANCE.getModelLink().isSuperTypeOf(domainElement.eClass()))
		{
			return ModelLinkEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(TGGRule element)
	{
		return true;
	}

}
