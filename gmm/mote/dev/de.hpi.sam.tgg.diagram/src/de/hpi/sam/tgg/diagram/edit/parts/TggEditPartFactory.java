package de.hpi.sam.tgg.diagram.edit.parts;

import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;

import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;

/**
 * @generated
 */
public class TggEditPartFactory implements EditPartFactory
{

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model)
	{
		if (model instanceof View)
		{
			View view = (View) model;
			switch (TggVisualIDRegistry.getVisualID(view))
			{

				case TGGRuleEditPart.VISUAL_ID:
					return new TGGRuleEditPart(view);

				case CorrespondenceDomainEditPart.VISUAL_ID:
					return new CorrespondenceDomainEditPart(view);

				case SourceModelDomainEditPart.VISUAL_ID:
					return new SourceModelDomainEditPart(view);

				case TargetModelDomainEditPart.VISUAL_ID:
					return new TargetModelDomainEditPart(view);

				case StringExpression2EditPart.VISUAL_ID:
					return new StringExpression2EditPart(view);

				case StringExpressionExpressionStringEditPart.VISUAL_ID:
					return new StringExpressionExpressionStringEditPart(view);

				case CallActionExpression2EditPart.VISUAL_ID:
					return new CallActionExpression2EditPart(view);

				case CallActionExpression3EditPart.VISUAL_ID:
					return new CallActionExpression3EditPart(view);

				case RuleVariableEditPart.VISUAL_ID:
					return new RuleVariableEditPart(view);

				case RuleVariableNameEditPart.VISUAL_ID:
					return new RuleVariableNameEditPart(view);

				case RuleVariableClassifierEditPart.VISUAL_ID:
					return new RuleVariableClassifierEditPart(view);

				case CorrespondenceNodeEditPart.VISUAL_ID:
					return new CorrespondenceNodeEditPart(view);

				case CorrespondenceNodeNameEditPart.VISUAL_ID:
					return new CorrespondenceNodeNameEditPart(view);

				case CorrespondenceNodeClassifierEditPart.VISUAL_ID:
					return new CorrespondenceNodeClassifierEditPart(view);

				case CorrespondenceNodeModifierEditPart.VISUAL_ID:
					return new CorrespondenceNodeModifierEditPart(view);

				case CorrespondenceNodeInputEditPart.VISUAL_ID:
					return new CorrespondenceNodeInputEditPart(view);

				case ModelObjectEditPart.VISUAL_ID:
					return new ModelObjectEditPart(view);

				case ModelObjectNameEditPart.VISUAL_ID:
					return new ModelObjectNameEditPart(view);

				case ModelObjectClassifierEditPart.VISUAL_ID:
					return new ModelObjectClassifierEditPart(view);

				case ModelObjectModifierEditPart.VISUAL_ID:
					return new ModelObjectModifierEditPart(view);

				case StringExpressionEditPart.VISUAL_ID:
					return new StringExpressionEditPart(view);

				case CallActionExpressionEditPart.VISUAL_ID:
					return new CallActionExpressionEditPart(view);

				case AttributeAssignmentEditPart.VISUAL_ID:
					return new AttributeAssignmentEditPart(view);

				case StringExpression3EditPart.VISUAL_ID:
					return new StringExpression3EditPart(view);

				case CallActionExpression5EditPart.VISUAL_ID:
					return new CallActionExpression5EditPart(view);

				case StringExpression4EditPart.VISUAL_ID:
					return new StringExpression4EditPart(view);

				case CallActionExpression4EditPart.VISUAL_ID:
					return new CallActionExpression4EditPart(view);

				case CorrespondenceDomainCorrespondenceDomainCompartmentEditPart.VISUAL_ID:
					return new CorrespondenceDomainCorrespondenceDomainCompartmentEditPart(view);

				case SourceModelDomainModelDomainCompartmentEditPart.VISUAL_ID:
					return new SourceModelDomainModelDomainCompartmentEditPart(view);

				case ModelObjectModelObjectConstraintsCompartmentEditPart.VISUAL_ID:
					return new ModelObjectModelObjectConstraintsCompartmentEditPart(view);

				case ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID:
					return new ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart(view);

				case TargetModelDomainModelDomainCompartmentEditPart.VISUAL_ID:
					return new TargetModelDomainModelDomainCompartmentEditPart(view);

				case RuleVariableRuleVariableForExpressionCompartmentEditPart.VISUAL_ID:
					return new RuleVariableRuleVariableForExpressionCompartmentEditPart(view);

				case RuleVariableRuleVariableRevExpressionCompartmentEditPart.VISUAL_ID:
					return new RuleVariableRuleVariableRevExpressionCompartmentEditPart(view);

				case CorrespondenceLinkEditPart.VISUAL_ID:
					return new CorrespondenceLinkEditPart(view);

				case CorrespondenceLinkModifierEditPart.VISUAL_ID:
					return new CorrespondenceLinkModifierEditPart(view);

				case ModelLinkEditPart.VISUAL_ID:
					return new ModelLinkEditPart(view);

				case ModelLinkReferenceEditPart.VISUAL_ID:
					return new ModelLinkReferenceEditPart(view);

				case ModelLinkModifierEditPart.VISUAL_ID:
					return new ModelLinkModifierEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model)
	{
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(ITextAwareEditPart source)
	{
		if (source.getFigure() instanceof WrappingLabel)
			return new TextCellEditorLocator((WrappingLabel) source.getFigure());
		else
		{
			return new LabelCellEditorLocator((Label) source.getFigure());
		}
	}

	/**
	 * @generated
	 */
	static private class TextCellEditorLocator implements CellEditorLocator
	{

		/**
		 * @generated
		 */
		private WrappingLabel	wrapLabel;

		/**
		 * @generated
		 */
		public TextCellEditorLocator(WrappingLabel wrapLabel)
		{
			this.wrapLabel = wrapLabel;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getWrapLabel()
		{
			return wrapLabel;
		}

		/**
		 * @generated
		 */
		public void relocate(CellEditor celleditor)
		{
			Text text = (Text) celleditor.getControl();
			Rectangle rect = getWrapLabel().getTextBounds().getCopy();
			getWrapLabel().translateToAbsolute(rect);
			if (getWrapLabel().isTextWrapOn() && getWrapLabel().getText().length() > 0)
			{
				rect.setSize(new Dimension(text.computeSize(rect.width, SWT.DEFAULT)));
			}
			else
			{
				int avr = FigureUtilities.getFontMetrics(text.getFont()).getAverageCharWidth();
				rect.setSize(new Dimension(text.computeSize(SWT.DEFAULT, SWT.DEFAULT)).expand(avr * 2, 0));
			}
			if (!rect.equals(new Rectangle(text.getBounds())))
			{
				text.setBounds(rect.x, rect.y, rect.width, rect.height);
			}
		}
	}

	/**
	 * @generated
	 */
	private static class LabelCellEditorLocator implements CellEditorLocator
	{

		/**
		 * @generated
		 */
		private Label	label;

		/**
		 * @generated
		 */
		public LabelCellEditorLocator(Label label)
		{
			this.label = label;
		}

		/**
		 * @generated
		 */
		public Label getLabel()
		{
			return label;
		}

		/**
		 * @generated
		 */
		public void relocate(CellEditor celleditor)
		{
			Text text = (Text) celleditor.getControl();
			Rectangle rect = getLabel().getTextBounds().getCopy();
			getLabel().translateToAbsolute(rect);
			int avr = FigureUtilities.getFontMetrics(text.getFont()).getAverageCharWidth();
			rect.setSize(new Dimension(text.computeSize(SWT.DEFAULT, SWT.DEFAULT)).expand(avr * 2, 0));
			if (!rect.equals(new Rectangle(text.getBounds())))
			{
				text.setBounds(rect.x, rect.y, rect.width, rect.height);
			}
		}
	}
}
