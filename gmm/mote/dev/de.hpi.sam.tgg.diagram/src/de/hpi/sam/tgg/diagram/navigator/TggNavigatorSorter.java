package de.hpi.sam.tgg.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;

/**
 * @generated
 */
public class TggNavigatorSorter extends ViewerSorter
{

	/**
	 * @generated
	 */
	private static final int	GROUP_CATEGORY		= 7009;

	/**
	 * @generated
	 */
	private static final int	SHORTCUTS_CATEGORY	= 7008;

	/**
	 * @generated
	 */
	public int category(Object element)
	{
		if (element instanceof TggNavigatorItem)
		{
			TggNavigatorItem item = (TggNavigatorItem) element;
			if (item.getView().getEAnnotation("Shortcut") != null) { //$NON-NLS-1$
				return SHORTCUTS_CATEGORY;
			}
			return TggVisualIDRegistry.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
