package de.hpi.sam.tgg.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import de.hpi.sam.tgg.ModelDomain;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.diagram.edit.policies.TggBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class ModelLinkReorientCommand extends EditElementCommand
{

	/**
	 * @generated
	 */
	protected final int		reorientDirection;

	/**
	 * @generated
	 */
	private final EObject	oldEnd;

	/**
	 * @generated
	 */
	private final EObject	newEnd;

	/**
	 * @generated
	 */
	public ModelLinkReorientCommand(ReorientRelationshipRequest request)
	{
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute()
	{
		if (false == getElementToEdit() instanceof ModelLink)
		{
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE)
		{
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET)
		{
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource()
	{
		if (!(oldEnd instanceof ModelObject && newEnd instanceof ModelObject))
		{
			return false;
		}
		ModelObject target = getLink().getTarget();
		if (!(getLink().eContainer() instanceof ModelDomain))
		{
			return false;
		}
		ModelDomain container = (ModelDomain) getLink().eContainer();
		return TggBaseItemSemanticEditPolicy.LinkConstraints.canExistModelLink_4002(container, getNewSource(), target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget()
	{
		if (!(oldEnd instanceof ModelObject && newEnd instanceof ModelObject))
		{
			return false;
		}
		ModelObject source = getLink().getSource();
		if (!(getLink().eContainer() instanceof ModelDomain))
		{
			return false;
		}
		ModelDomain container = (ModelDomain) getLink().eContainer();
		return TggBaseItemSemanticEditPolicy.LinkConstraints.canExistModelLink_4002(container, source, getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException
	{
		if (!canExecute())
		{
			throw new ExecutionException("Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE)
		{
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET)
		{
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException
	{
		getLink().setSource(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException
	{
		getLink().setTarget(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected ModelLink getLink()
	{
		return (ModelLink) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected ModelObject getOldSource()
	{
		return (ModelObject) oldEnd;
	}

	/**
	 * @generated
	 */
	protected ModelObject getNewSource()
	{
		return (ModelObject) newEnd;
	}

	/**
	 * @generated
	 */
	protected ModelObject getOldTarget()
	{
		return (ModelObject) oldEnd;
	}

	/**
	 * @generated
	 */
	protected ModelObject getNewTarget()
	{
		return (ModelObject) newEnd;
	}
}
