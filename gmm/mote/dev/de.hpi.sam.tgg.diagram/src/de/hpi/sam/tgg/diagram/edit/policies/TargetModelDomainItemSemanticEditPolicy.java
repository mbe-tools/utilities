package de.hpi.sam.tgg.diagram.edit.policies;

import java.util.Iterator;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.ICompositeCommand;
import org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TargetModelDomainModelDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class TargetModelDomainItemSemanticEditPolicy extends TggBaseItemSemanticEditPolicy
{

	/**
	 * @generated
	 */
	public TargetModelDomainItemSemanticEditPolicy()
	{
		super(TggElementTypes.TargetModelDomain_2003);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req)
	{
		View view = (View) getHost().getModel();
		CompositeTransactionalCommand cmd = new CompositeTransactionalCommand(getEditingDomain(), null);
		cmd.setTransactionNestingEnabled(false);
		EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
		if (annotation == null)
		{
			// there are indirectly referenced children, need extra commands:
			// false
			addDestroyChildNodesCommand(cmd);
			addDestroyShortcutsCommand(cmd, view);
			// delete host element
			cmd.add(new DestroyElementCommand(req));
		}
		else
		{
			cmd.add(new DeleteCommand(getEditingDomain(), view));
		}
		return getGEFWrapper(cmd.reduce());
	}

	/**
	 * @generated
	 */
	private void addDestroyChildNodesCommand(ICompositeCommand cmd)
	{
		View view = (View) getHost().getModel();
		for (Iterator nit = view.getChildren().iterator(); nit.hasNext();)
		{
			Node node = (Node) nit.next();
			switch (TggVisualIDRegistry.getVisualID(node))
			{
				case TargetModelDomainModelDomainCompartmentEditPart.VISUAL_ID:
					for (Iterator cit = node.getChildren().iterator(); cit.hasNext();)
					{
						Node cnode = (Node) cit.next();
						switch (TggVisualIDRegistry.getVisualID(cnode))
						{
							case ModelObjectEditPart.VISUAL_ID:
								for (Iterator it = cnode.getTargetEdges().iterator(); it.hasNext();)
								{
									Edge incomingLink = (Edge) it.next();
									if (TggVisualIDRegistry.getVisualID(incomingLink) == CorrespondenceLinkEditPart.VISUAL_ID)
									{
										DestroyElementRequest r = new DestroyElementRequest(incomingLink.getElement(), false);
										cmd.add(new DestroyElementCommand(r));
										cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
										continue;
									}
									if (TggVisualIDRegistry.getVisualID(incomingLink) == ModelLinkEditPart.VISUAL_ID)
									{
										DestroyElementRequest r = new DestroyElementRequest(incomingLink.getElement(), false);
										cmd.add(new DestroyElementCommand(r));
										cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
										continue;
									}
								}
								for (Iterator it = cnode.getSourceEdges().iterator(); it.hasNext();)
								{
									Edge outgoingLink = (Edge) it.next();
									if (TggVisualIDRegistry.getVisualID(outgoingLink) == ModelLinkEditPart.VISUAL_ID)
									{
										DestroyElementRequest r = new DestroyElementRequest(outgoingLink.getElement(), false);
										cmd.add(new DestroyElementCommand(r));
										cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
										continue;
									}
								}
								cmd.add(new DestroyElementCommand(new DestroyElementRequest(getEditingDomain(), cnode.getElement(), false))); // directlyOwned:
																																				// true
								// don't need explicit deletion of cnode as
								// parent's view deletion would clean child
								// views as well
								// cmd.add(new
								// org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand(getEditingDomain(),
								// cnode));
								break;
						}
					}
					break;
			}
		}
	}

}
