package de.hpi.sam.tgg.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import de.hpi.sam.tgg.diagram.edit.commands.CallActionExpressionCreateCommand;
import de.hpi.sam.tgg.diagram.edit.commands.StringExpressionCreateCommand;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class ModelObjectModelObjectConstraintsCompartmentItemSemanticEditPolicy extends TggBaseItemSemanticEditPolicy
{

	/**
	 * @generated
	 */
	public ModelObjectModelObjectConstraintsCompartmentItemSemanticEditPolicy()
	{
		super(TggElementTypes.ModelObject_3002);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req)
	{
		if (TggElementTypes.StringExpression_3003 == req.getElementType())
		{
			return getGEFWrapper(new StringExpressionCreateCommand(req));
		}
		if (TggElementTypes.CallActionExpression_3004 == req.getElementType())
		{
			return getGEFWrapper(new CallActionExpressionCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
