package de.hpi.sam.tgg.diagram.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ListCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.diagram.edit.policies.RuleVariableRuleVariableForExpressionCompartmentCanonicalEditPolicy;
import de.hpi.sam.tgg.diagram.edit.policies.RuleVariableRuleVariableForExpressionCompartmentItemSemanticEditPolicy;
import de.hpi.sam.tgg.diagram.part.Messages;

/**
 * @generated
 */
public class RuleVariableRuleVariableForExpressionCompartmentEditPart extends ListCompartmentEditPart
{

	/**
	 * @generated
	 */
	public static final int	VISUAL_ID	= 7006;

	/**
	 * @generated
	 */
	public RuleVariableRuleVariableForExpressionCompartmentEditPart(View view)
	{
		super(view);
	}

	/**
	 * @generated
	 */
	protected boolean hasModelChildrenChanged(Notification evt)
	{
		return false;
	}

	/**
	 * @generated
	 */
	public String getCompartmentName()
	{
		return Messages.RuleVariableRuleVariableForExpressionCompartmentEditPart_title;
	}

	/**
	 * @generated
	 */
	public IFigure createFigure()
	{
		ResizableCompartmentFigure result = (ResizableCompartmentFigure) super.createFigure();
		result.setTitleVisibility(false);
		return result;
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies()
	{
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new RuleVariableRuleVariableForExpressionCompartmentItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.CREATION_ROLE, new CreationEditPolicy());
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE, new DragDropEditPolicy());
		installEditPolicy(EditPolicyRoles.CANONICAL_ROLE, new RuleVariableRuleVariableForExpressionCompartmentCanonicalEditPolicy());
	}

	/**
	 * @generated
	 */
	protected void setRatio(Double ratio)
	{
		if (getFigure().getParent().getLayoutManager() instanceof ConstrainedToolbarLayout)
		{
			super.setRatio(ratio);
		}
	}

}
