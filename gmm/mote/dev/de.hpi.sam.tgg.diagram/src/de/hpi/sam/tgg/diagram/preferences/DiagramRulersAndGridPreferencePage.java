package de.hpi.sam.tgg.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage;

import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramRulersAndGridPreferencePage extends RulerGridPreferencePage
{

	/**
	 * @generated
	 */
	public DiagramRulersAndGridPreferencePage()
	{
		setPreferenceStore(TggDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
