package de.hpi.sam.tgg.diagram.providers;

import java.util.ArrayList;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.core.providers.IViewProvider;
import org.eclipse.gmf.runtime.diagram.core.services.view.CreateDiagramViewOperation;
import org.eclipse.gmf.runtime.diagram.core.services.view.CreateEdgeViewOperation;
import org.eclipse.gmf.runtime.diagram.core.services.view.CreateNodeViewOperation;
import org.eclipse.gmf.runtime.diagram.core.services.view.CreateViewForKindOperation;
import org.eclipse.gmf.runtime.diagram.core.services.view.CreateViewOperation;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.preferences.IPreferenceConstants;
import org.eclipse.gmf.runtime.draw2d.ui.figures.FigureUtilities;
import org.eclipse.gmf.runtime.emf.core.util.EMFCoreUtil;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.DecorationNode;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.FontStyle;
import org.eclipse.gmf.runtime.notation.Location;
import org.eclipse.gmf.runtime.notation.MeasurementUnit;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.NotationFactory;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.RelativeBendpoints;
import org.eclipse.gmf.runtime.notation.Routing;
import org.eclipse.gmf.runtime.notation.Shape;
import org.eclipse.gmf.runtime.notation.TitleStyle;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.datatype.RelativeBendpoint;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;

import de.hpi.sam.tgg.diagram.edit.parts.AttributeAssignmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression2EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression3EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression4EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression5EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceDomainCorrespondenceDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceLinkModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeClassifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeInputEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkReferenceEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectClassifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectModelObjectConstraintsCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableClassifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableRuleVariableForExpressionCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableRuleVariableRevExpressionCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.SourceModelDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.SourceModelDomainModelDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression2EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression3EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression4EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpressionExpressionStringEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TGGRuleEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TargetModelDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TargetModelDomainModelDomainCompartmentEditPart;
import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;

/**
 * @generated
 */
public class TggViewProvider extends AbstractProvider implements IViewProvider
{

	/**
	 * @generated
	 */
	public final boolean provides(IOperation operation)
	{
		if (operation instanceof CreateViewForKindOperation)
		{
			return provides((CreateViewForKindOperation) operation);
		}
		assert operation instanceof CreateViewOperation;
		if (operation instanceof CreateDiagramViewOperation)
		{
			return provides((CreateDiagramViewOperation) operation);
		}
		else if (operation instanceof CreateEdgeViewOperation)
		{
			return provides((CreateEdgeViewOperation) operation);
		}
		else if (operation instanceof CreateNodeViewOperation)
		{
			return provides((CreateNodeViewOperation) operation);
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean provides(CreateViewForKindOperation op)
	{
		/*
		 * if (op.getViewKind() == Node.class) return
		 * getNodeViewClass(op.getSemanticAdapter(), op.getContainerView(),
		 * op.getSemanticHint()) != null; if (op.getViewKind() == Edge.class)
		 * return getEdgeViewClass(op.getSemanticAdapter(),
		 * op.getContainerView(), op.getSemanticHint()) != null;
		 */
		return true;
	}

	/**
	 * @generated
	 */
	protected boolean provides(CreateDiagramViewOperation op)
	{
		return TGGRuleEditPart.MODEL_ID.equals(op.getSemanticHint())
				&& TggVisualIDRegistry.getDiagramVisualID(getSemanticElement(op.getSemanticAdapter())) != -1;
	}

	/**
	 * @generated
	 */
	protected boolean provides(CreateNodeViewOperation op)
	{
		if (op.getContainerView() == null)
		{
			return false;
		}
		IElementType elementType = getSemanticElementType(op.getSemanticAdapter());
		EObject domainElement = getSemanticElement(op.getSemanticAdapter());
		int visualID;
		if (op.getSemanticHint() == null)
		{
			// Semantic hint is not specified. Can be a result of call from
			// CanonicalEditPolicy.
			// In this situation there should be NO elementType, visualID will
			// be determined
			// by VisualIDRegistry.getNodeVisualID() for domainElement.
			if (elementType != null || domainElement == null)
			{
				return false;
			}
			visualID = TggVisualIDRegistry.getNodeVisualID(op.getContainerView(), domainElement);
		}
		else
		{
			visualID = TggVisualIDRegistry.getVisualID(op.getSemanticHint());
			if (elementType != null)
			{
				if (!TggElementTypes.isKnownElementType(elementType) || (!(elementType instanceof IHintedType)))
				{
					return false; // foreign element type
				}
				String elementTypeHint = ((IHintedType) elementType).getSemanticHint();
				if (!op.getSemanticHint().equals(elementTypeHint))
				{
					return false; // if semantic hint is specified it should be
					// the same as in element type
				}
				if (domainElement != null && visualID != TggVisualIDRegistry.getNodeVisualID(op.getContainerView(), domainElement))
				{
					return false; // visual id for node EClass should match
					// visual id from element type
				}
			}
			else
			{
				if (!TGGRuleEditPart.MODEL_ID.equals(TggVisualIDRegistry.getModelID(op.getContainerView())))
				{
					return false; // foreign diagram
				}
				switch (visualID)
				{
					case CorrespondenceDomainEditPart.VISUAL_ID:
					case SourceModelDomainEditPart.VISUAL_ID:
					case TargetModelDomainEditPart.VISUAL_ID:
					case RuleVariableEditPart.VISUAL_ID:
					case CorrespondenceNodeEditPart.VISUAL_ID:
					case ModelObjectEditPart.VISUAL_ID:
					case StringExpressionEditPart.VISUAL_ID:
					case CallActionExpressionEditPart.VISUAL_ID:
					case AttributeAssignmentEditPart.VISUAL_ID:
					case StringExpression2EditPart.VISUAL_ID:
					case CallActionExpression2EditPart.VISUAL_ID:
					case StringExpression3EditPart.VISUAL_ID:
					case CallActionExpression5EditPart.VISUAL_ID:
					case StringExpression4EditPart.VISUAL_ID:
					case CallActionExpression4EditPart.VISUAL_ID:
						if (domainElement == null || visualID != TggVisualIDRegistry.getNodeVisualID(op.getContainerView(), domainElement))
						{
							return false; // visual id in semantic hint should
							// match visual id for domain
							// element
						}
						break;
					default:
						return false;
				}
			}
		}
		return CorrespondenceDomainEditPart.VISUAL_ID == visualID || SourceModelDomainEditPart.VISUAL_ID == visualID
				|| TargetModelDomainEditPart.VISUAL_ID == visualID || StringExpression2EditPart.VISUAL_ID == visualID
				|| CallActionExpression2EditPart.VISUAL_ID == visualID || RuleVariableEditPart.VISUAL_ID == visualID
				|| CorrespondenceNodeEditPart.VISUAL_ID == visualID || ModelObjectEditPart.VISUAL_ID == visualID
				|| StringExpressionEditPart.VISUAL_ID == visualID || CallActionExpressionEditPart.VISUAL_ID == visualID
				|| AttributeAssignmentEditPart.VISUAL_ID == visualID || StringExpression3EditPart.VISUAL_ID == visualID
				|| CallActionExpression5EditPart.VISUAL_ID == visualID || StringExpression4EditPart.VISUAL_ID == visualID
				|| CallActionExpression4EditPart.VISUAL_ID == visualID;
	}

	/**
	 * @generated
	 */
	protected boolean provides(CreateEdgeViewOperation op)
	{
		IElementType elementType = getSemanticElementType(op.getSemanticAdapter());
		if (!TggElementTypes.isKnownElementType(elementType) || (!(elementType instanceof IHintedType)))
		{
			return false; // foreign element type
		}
		String elementTypeHint = ((IHintedType) elementType).getSemanticHint();
		if (elementTypeHint == null || (op.getSemanticHint() != null && !elementTypeHint.equals(op.getSemanticHint())))
		{
			return false; // our hint is visual id and must be specified, and it
			// should be the same as in element type
		}
		int visualID = TggVisualIDRegistry.getVisualID(elementTypeHint);
		EObject domainElement = getSemanticElement(op.getSemanticAdapter());
		if (domainElement != null && visualID != TggVisualIDRegistry.getLinkWithClassVisualID(domainElement))
		{
			return false; // visual id for link EClass should match visual id
			// from element type
		}
		return true;
	}

	/**
	 * @generated
	 */
	public Diagram createDiagram(IAdaptable semanticAdapter, String diagramKind, PreferencesHint preferencesHint)
	{
		Diagram diagram = NotationFactory.eINSTANCE.createDiagram();
		diagram.getStyles().add(NotationFactory.eINSTANCE.createDiagramStyle());
		diagram.setType(TGGRuleEditPart.MODEL_ID);
		diagram.setElement(getSemanticElement(semanticAdapter));
		diagram.setMeasurementUnit(MeasurementUnit.PIXEL_LITERAL);
		return diagram;
	}

	/**
	 * @generated
	 */
	public Node createNode(IAdaptable semanticAdapter, View containerView, String semanticHint, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		final EObject domainElement = getSemanticElement(semanticAdapter);
		final int visualID;
		if (semanticHint == null)
		{
			visualID = TggVisualIDRegistry.getNodeVisualID(containerView, domainElement);
		}
		else
		{
			visualID = TggVisualIDRegistry.getVisualID(semanticHint);
		}
		switch (visualID)
		{
			case CorrespondenceDomainEditPart.VISUAL_ID:
				return createCorrespondenceDomain_2001(domainElement, containerView, index, persisted, preferencesHint);
			case SourceModelDomainEditPart.VISUAL_ID:
				return createSourceModelDomain_2002(domainElement, containerView, index, persisted, preferencesHint);
			case TargetModelDomainEditPart.VISUAL_ID:
				return createTargetModelDomain_2003(domainElement, containerView, index, persisted, preferencesHint);
			case StringExpression2EditPart.VISUAL_ID:
				return createStringExpression_2004(domainElement, containerView, index, persisted, preferencesHint);
			case CallActionExpression2EditPart.VISUAL_ID:
				return createCallActionExpression_2005(domainElement, containerView, index, persisted, preferencesHint);
			case RuleVariableEditPart.VISUAL_ID:
				return createRuleVariable_2008(domainElement, containerView, index, persisted, preferencesHint);
			case CorrespondenceNodeEditPart.VISUAL_ID:
				return createCorrespondenceNode_3001(domainElement, containerView, index, persisted, preferencesHint);
			case ModelObjectEditPart.VISUAL_ID:
				return createModelObject_3002(domainElement, containerView, index, persisted, preferencesHint);
			case StringExpressionEditPart.VISUAL_ID:
				return createStringExpression_3003(domainElement, containerView, index, persisted, preferencesHint);
			case CallActionExpressionEditPart.VISUAL_ID:
				return createCallActionExpression_3004(domainElement, containerView, index, persisted, preferencesHint);
			case AttributeAssignmentEditPart.VISUAL_ID:
				return createAttributeAssignment_3005(domainElement, containerView, index, persisted, preferencesHint);
			case StringExpression3EditPart.VISUAL_ID:
				return createStringExpression_3009(domainElement, containerView, index, persisted, preferencesHint);
			case CallActionExpression5EditPart.VISUAL_ID:
				return createCallActionExpression_3010(domainElement, containerView, index, persisted, preferencesHint);
			case StringExpression4EditPart.VISUAL_ID:
				return createStringExpression_3011(domainElement, containerView, index, persisted, preferencesHint);
			case CallActionExpression4EditPart.VISUAL_ID:
				return createCallActionExpression_3012(domainElement, containerView, index, persisted, preferencesHint);
		}
		// can't happen, provided #provides(CreateNodeViewOperation) is correct
		return null;
	}

	/**
	 * @generated
	 */
	public Edge createEdge(IAdaptable semanticAdapter, View containerView, String semanticHint, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		IElementType elementType = getSemanticElementType(semanticAdapter);
		String elementTypeHint = ((IHintedType) elementType).getSemanticHint();
		switch (TggVisualIDRegistry.getVisualID(elementTypeHint))
		{
			case CorrespondenceLinkEditPart.VISUAL_ID:
				return createCorrespondenceLink_4001(getSemanticElement(semanticAdapter), containerView, index, persisted, preferencesHint);
			case ModelLinkEditPart.VISUAL_ID:
				return createModelLink_4002(getSemanticElement(semanticAdapter), containerView, index, persisted, preferencesHint);
		}
		// can never happen, provided #provides(CreateEdgeViewOperation) is
		// correct
		return null;
	}

	/**
	 * @generated
	 */
	public Node createCorrespondenceDomain_2001(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Shape node = NotationFactory.eINSTANCE.createShape();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createBounds());
		node.setType(TggVisualIDRegistry.getType(CorrespondenceDomainEditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		stampShortcut(containerView, node);
		// initializeFromPreferences
		final IPreferenceStore prefStore = (IPreferenceStore) preferencesHint.getPreferenceStore();

		org.eclipse.swt.graphics.RGB lineRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_LINE_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getLineStyle_LineColor(), FigureUtilities.RGBToInteger(lineRGB));
		FontStyle nodeFontStyle = (FontStyle) node.getStyle(NotationPackage.Literals.FONT_STYLE);
		if (nodeFontStyle != null)
		{
			FontData fontData = PreferenceConverter.getFontData(prefStore, IPreferenceConstants.PREF_DEFAULT_FONT);
			nodeFontStyle.setFontName(fontData.getName());
			nodeFontStyle.setFontHeight(fontData.getHeight());
			nodeFontStyle.setBold((fontData.getStyle() & SWT.BOLD) != 0);
			nodeFontStyle.setItalic((fontData.getStyle() & SWT.ITALIC) != 0);
			org.eclipse.swt.graphics.RGB fontRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FONT_COLOR);
			nodeFontStyle.setFontColor(FigureUtilities.RGBToInteger(fontRGB).intValue());
		}
		org.eclipse.swt.graphics.RGB fillRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FILL_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getFillStyle_FillColor(), FigureUtilities.RGBToInteger(fillRGB));
		createCompartment(node, TggVisualIDRegistry.getType(CorrespondenceDomainCorrespondenceDomainCompartmentEditPart.VISUAL_ID), false,
				false, false, false);
		return node;
	}

	/**
	 * @generated
	 */
	public Node createSourceModelDomain_2002(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Shape node = NotationFactory.eINSTANCE.createShape();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createBounds());
		node.setType(TggVisualIDRegistry.getType(SourceModelDomainEditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		stampShortcut(containerView, node);
		// initializeFromPreferences
		final IPreferenceStore prefStore = (IPreferenceStore) preferencesHint.getPreferenceStore();

		org.eclipse.swt.graphics.RGB lineRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_LINE_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getLineStyle_LineColor(), FigureUtilities.RGBToInteger(lineRGB));
		FontStyle nodeFontStyle = (FontStyle) node.getStyle(NotationPackage.Literals.FONT_STYLE);
		if (nodeFontStyle != null)
		{
			FontData fontData = PreferenceConverter.getFontData(prefStore, IPreferenceConstants.PREF_DEFAULT_FONT);
			nodeFontStyle.setFontName(fontData.getName());
			nodeFontStyle.setFontHeight(fontData.getHeight());
			nodeFontStyle.setBold((fontData.getStyle() & SWT.BOLD) != 0);
			nodeFontStyle.setItalic((fontData.getStyle() & SWT.ITALIC) != 0);
			org.eclipse.swt.graphics.RGB fontRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FONT_COLOR);
			nodeFontStyle.setFontColor(FigureUtilities.RGBToInteger(fontRGB).intValue());
		}
		org.eclipse.swt.graphics.RGB fillRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FILL_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getFillStyle_FillColor(), FigureUtilities.RGBToInteger(fillRGB));
		createCompartment(node, TggVisualIDRegistry.getType(SourceModelDomainModelDomainCompartmentEditPart.VISUAL_ID), false, false,
				false, false);
		return node;
	}

	/**
	 * @generated
	 */
	public Node createTargetModelDomain_2003(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Shape node = NotationFactory.eINSTANCE.createShape();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createBounds());
		node.setType(TggVisualIDRegistry.getType(TargetModelDomainEditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		stampShortcut(containerView, node);
		// initializeFromPreferences
		final IPreferenceStore prefStore = (IPreferenceStore) preferencesHint.getPreferenceStore();

		org.eclipse.swt.graphics.RGB lineRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_LINE_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getLineStyle_LineColor(), FigureUtilities.RGBToInteger(lineRGB));
		FontStyle nodeFontStyle = (FontStyle) node.getStyle(NotationPackage.Literals.FONT_STYLE);
		if (nodeFontStyle != null)
		{
			FontData fontData = PreferenceConverter.getFontData(prefStore, IPreferenceConstants.PREF_DEFAULT_FONT);
			nodeFontStyle.setFontName(fontData.getName());
			nodeFontStyle.setFontHeight(fontData.getHeight());
			nodeFontStyle.setBold((fontData.getStyle() & SWT.BOLD) != 0);
			nodeFontStyle.setItalic((fontData.getStyle() & SWT.ITALIC) != 0);
			org.eclipse.swt.graphics.RGB fontRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FONT_COLOR);
			nodeFontStyle.setFontColor(FigureUtilities.RGBToInteger(fontRGB).intValue());
		}
		org.eclipse.swt.graphics.RGB fillRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FILL_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getFillStyle_FillColor(), FigureUtilities.RGBToInteger(fillRGB));
		createCompartment(node, TggVisualIDRegistry.getType(TargetModelDomainModelDomainCompartmentEditPart.VISUAL_ID), false, false,
				false, false);
		return node;
	}

	/**
	 * @generated
	 */
	public Node createStringExpression_2004(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Shape node = NotationFactory.eINSTANCE.createShape();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createBounds());
		node.setType(TggVisualIDRegistry.getType(StringExpression2EditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		stampShortcut(containerView, node);
		// initializeFromPreferences
		final IPreferenceStore prefStore = (IPreferenceStore) preferencesHint.getPreferenceStore();

		org.eclipse.swt.graphics.RGB lineRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_LINE_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getLineStyle_LineColor(), FigureUtilities.RGBToInteger(lineRGB));
		FontStyle nodeFontStyle = (FontStyle) node.getStyle(NotationPackage.Literals.FONT_STYLE);
		if (nodeFontStyle != null)
		{
			FontData fontData = PreferenceConverter.getFontData(prefStore, IPreferenceConstants.PREF_DEFAULT_FONT);
			nodeFontStyle.setFontName(fontData.getName());
			nodeFontStyle.setFontHeight(fontData.getHeight());
			nodeFontStyle.setBold((fontData.getStyle() & SWT.BOLD) != 0);
			nodeFontStyle.setItalic((fontData.getStyle() & SWT.ITALIC) != 0);
			org.eclipse.swt.graphics.RGB fontRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FONT_COLOR);
			nodeFontStyle.setFontColor(FigureUtilities.RGBToInteger(fontRGB).intValue());
		}
		org.eclipse.swt.graphics.RGB fillRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FILL_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getFillStyle_FillColor(), FigureUtilities.RGBToInteger(fillRGB));
		Node label5008 = createLabel(node, TggVisualIDRegistry.getType(StringExpressionExpressionStringEditPart.VISUAL_ID));
		return node;
	}

	/**
	 * @generated
	 */
	public Node createCallActionExpression_2005(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Shape node = NotationFactory.eINSTANCE.createShape();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createBounds());
		node.setType(TggVisualIDRegistry.getType(CallActionExpression2EditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		stampShortcut(containerView, node);
		// initializeFromPreferences
		final IPreferenceStore prefStore = (IPreferenceStore) preferencesHint.getPreferenceStore();

		org.eclipse.swt.graphics.RGB lineRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_LINE_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getLineStyle_LineColor(), FigureUtilities.RGBToInteger(lineRGB));
		FontStyle nodeFontStyle = (FontStyle) node.getStyle(NotationPackage.Literals.FONT_STYLE);
		if (nodeFontStyle != null)
		{
			FontData fontData = PreferenceConverter.getFontData(prefStore, IPreferenceConstants.PREF_DEFAULT_FONT);
			nodeFontStyle.setFontName(fontData.getName());
			nodeFontStyle.setFontHeight(fontData.getHeight());
			nodeFontStyle.setBold((fontData.getStyle() & SWT.BOLD) != 0);
			nodeFontStyle.setItalic((fontData.getStyle() & SWT.ITALIC) != 0);
			org.eclipse.swt.graphics.RGB fontRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FONT_COLOR);
			nodeFontStyle.setFontColor(FigureUtilities.RGBToInteger(fontRGB).intValue());
		}
		org.eclipse.swt.graphics.RGB fillRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FILL_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getFillStyle_FillColor(), FigureUtilities.RGBToInteger(fillRGB));
		Node label5009 = createLabel(node, TggVisualIDRegistry.getType(CallActionExpression3EditPart.VISUAL_ID));
		return node;
	}

	/**
	 * @generated
	 */
	public Node createRuleVariable_2008(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Shape node = NotationFactory.eINSTANCE.createShape();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createBounds());
		node.setType(TggVisualIDRegistry.getType(RuleVariableEditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		stampShortcut(containerView, node);
		// initializeFromPreferences
		final IPreferenceStore prefStore = (IPreferenceStore) preferencesHint.getPreferenceStore();

		org.eclipse.swt.graphics.RGB lineRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_LINE_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getLineStyle_LineColor(), FigureUtilities.RGBToInteger(lineRGB));
		FontStyle nodeFontStyle = (FontStyle) node.getStyle(NotationPackage.Literals.FONT_STYLE);
		if (nodeFontStyle != null)
		{
			FontData fontData = PreferenceConverter.getFontData(prefStore, IPreferenceConstants.PREF_DEFAULT_FONT);
			nodeFontStyle.setFontName(fontData.getName());
			nodeFontStyle.setFontHeight(fontData.getHeight());
			nodeFontStyle.setBold((fontData.getStyle() & SWT.BOLD) != 0);
			nodeFontStyle.setItalic((fontData.getStyle() & SWT.ITALIC) != 0);
			org.eclipse.swt.graphics.RGB fontRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FONT_COLOR);
			nodeFontStyle.setFontColor(FigureUtilities.RGBToInteger(fontRGB).intValue());
		}
		org.eclipse.swt.graphics.RGB fillRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FILL_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getFillStyle_FillColor(), FigureUtilities.RGBToInteger(fillRGB));
		Node label5012 = createLabel(node, TggVisualIDRegistry.getType(RuleVariableNameEditPart.VISUAL_ID));
		Node label5013 = createLabel(node, TggVisualIDRegistry.getType(RuleVariableClassifierEditPart.VISUAL_ID));
		createCompartment(node, TggVisualIDRegistry.getType(RuleVariableRuleVariableForExpressionCompartmentEditPart.VISUAL_ID), false,
				false, true, true);
		createCompartment(node, TggVisualIDRegistry.getType(RuleVariableRuleVariableRevExpressionCompartmentEditPart.VISUAL_ID), false,
				false, true, true);
		return node;
	}

	/**
	 * @generated
	 */
	public Node createCorrespondenceNode_3001(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Node node = NotationFactory.eINSTANCE.createNode();
		node.getStyles().add(NotationFactory.eINSTANCE.createDescriptionStyle());
		node.getStyles().add(NotationFactory.eINSTANCE.createFontStyle());
		node.getStyles().add(NotationFactory.eINSTANCE.createFillStyle());
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createBounds());
		node.setType(TggVisualIDRegistry.getType(CorrespondenceNodeEditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		// initializeFromPreferences
		final IPreferenceStore prefStore = (IPreferenceStore) preferencesHint.getPreferenceStore();
		FontStyle nodeFontStyle = (FontStyle) node.getStyle(NotationPackage.Literals.FONT_STYLE);
		if (nodeFontStyle != null)
		{
			FontData fontData = PreferenceConverter.getFontData(prefStore, IPreferenceConstants.PREF_DEFAULT_FONT);
			nodeFontStyle.setFontName(fontData.getName());
			nodeFontStyle.setFontHeight(fontData.getHeight());
			nodeFontStyle.setBold((fontData.getStyle() & SWT.BOLD) != 0);
			nodeFontStyle.setItalic((fontData.getStyle() & SWT.ITALIC) != 0);
			org.eclipse.swt.graphics.RGB fontRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FONT_COLOR);
			nodeFontStyle.setFontColor(FigureUtilities.RGBToInteger(fontRGB).intValue());
		}
		org.eclipse.swt.graphics.RGB fillRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FILL_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getFillStyle_FillColor(), FigureUtilities.RGBToInteger(fillRGB));
		Node label5001 = createLabel(node, TggVisualIDRegistry.getType(CorrespondenceNodeNameEditPart.VISUAL_ID));
		Node label5002 = createLabel(node, TggVisualIDRegistry.getType(CorrespondenceNodeClassifierEditPart.VISUAL_ID));
		Node label5003 = createLabel(node, TggVisualIDRegistry.getType(CorrespondenceNodeModifierEditPart.VISUAL_ID));
		Node label5007 = createLabel(node, TggVisualIDRegistry.getType(CorrespondenceNodeInputEditPart.VISUAL_ID));
		return node;
	}

	/**
	 * @generated
	 */
	public Node createModelObject_3002(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Node node = NotationFactory.eINSTANCE.createNode();
		node.getStyles().add(NotationFactory.eINSTANCE.createDescriptionStyle());
		node.getStyles().add(NotationFactory.eINSTANCE.createFontStyle());
		node.getStyles().add(NotationFactory.eINSTANCE.createFillStyle());
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createBounds());
		node.setType(TggVisualIDRegistry.getType(ModelObjectEditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		// initializeFromPreferences
		final IPreferenceStore prefStore = (IPreferenceStore) preferencesHint.getPreferenceStore();
		FontStyle nodeFontStyle = (FontStyle) node.getStyle(NotationPackage.Literals.FONT_STYLE);
		if (nodeFontStyle != null)
		{
			FontData fontData = PreferenceConverter.getFontData(prefStore, IPreferenceConstants.PREF_DEFAULT_FONT);
			nodeFontStyle.setFontName(fontData.getName());
			nodeFontStyle.setFontHeight(fontData.getHeight());
			nodeFontStyle.setBold((fontData.getStyle() & SWT.BOLD) != 0);
			nodeFontStyle.setItalic((fontData.getStyle() & SWT.ITALIC) != 0);
			org.eclipse.swt.graphics.RGB fontRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FONT_COLOR);
			nodeFontStyle.setFontColor(FigureUtilities.RGBToInteger(fontRGB).intValue());
		}
		org.eclipse.swt.graphics.RGB fillRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FILL_COLOR);
		ViewUtil.setStructuralFeatureValue(node, NotationPackage.eINSTANCE.getFillStyle_FillColor(), FigureUtilities.RGBToInteger(fillRGB));
		Node label5004 = createLabel(node, TggVisualIDRegistry.getType(ModelObjectNameEditPart.VISUAL_ID));
		Node label5005 = createLabel(node, TggVisualIDRegistry.getType(ModelObjectClassifierEditPart.VISUAL_ID));
		Node label5006 = createLabel(node, TggVisualIDRegistry.getType(ModelObjectModifierEditPart.VISUAL_ID));
		createCompartment(node, TggVisualIDRegistry.getType(ModelObjectModelObjectConstraintsCompartmentEditPart.VISUAL_ID), false, false,
				true, true);
		createCompartment(node, TggVisualIDRegistry.getType(ModelObjectModelObjectAttributeAssignmentsCompartmentEditPart.VISUAL_ID),
				false, false, true, true);
		return node;
	}

	/**
	 * @generated
	 */
	public Node createStringExpression_3003(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Node node = NotationFactory.eINSTANCE.createNode();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createLocation());
		node.setType(TggVisualIDRegistry.getType(StringExpressionEditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		return node;
	}

	/**
	 * @generated
	 */
	public Node createCallActionExpression_3004(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Node node = NotationFactory.eINSTANCE.createNode();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createLocation());
		node.setType(TggVisualIDRegistry.getType(CallActionExpressionEditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		return node;
	}

	/**
	 * @generated
	 */
	public Node createAttributeAssignment_3005(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Node node = NotationFactory.eINSTANCE.createNode();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createLocation());
		node.setType(TggVisualIDRegistry.getType(AttributeAssignmentEditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		return node;
	}

	/**
	 * @generated
	 */
	public Node createStringExpression_3009(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Node node = NotationFactory.eINSTANCE.createNode();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createLocation());
		node.setType(TggVisualIDRegistry.getType(StringExpression3EditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		return node;
	}

	/**
	 * @generated
	 */
	public Node createCallActionExpression_3010(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Node node = NotationFactory.eINSTANCE.createNode();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createLocation());
		node.setType(TggVisualIDRegistry.getType(CallActionExpression5EditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		return node;
	}

	/**
	 * @generated
	 */
	public Node createStringExpression_3011(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Node node = NotationFactory.eINSTANCE.createNode();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createLocation());
		node.setType(TggVisualIDRegistry.getType(StringExpression4EditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		return node;
	}

	/**
	 * @generated
	 */
	public Node createCallActionExpression_3012(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Node node = NotationFactory.eINSTANCE.createNode();
		node.setLayoutConstraint(NotationFactory.eINSTANCE.createLocation());
		node.setType(TggVisualIDRegistry.getType(CallActionExpression4EditPart.VISUAL_ID));
		ViewUtil.insertChildView(containerView, node, index, persisted);
		node.setElement(domainElement);
		return node;
	}

	/**
	 * @generated
	 */
	public Edge createCorrespondenceLink_4001(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Edge edge = NotationFactory.eINSTANCE.createEdge();
		edge.getStyles().add(NotationFactory.eINSTANCE.createRoutingStyle());
		edge.getStyles().add(NotationFactory.eINSTANCE.createFontStyle());
		RelativeBendpoints bendpoints = NotationFactory.eINSTANCE.createRelativeBendpoints();
		ArrayList points = new ArrayList(2);
		points.add(new RelativeBendpoint());
		points.add(new RelativeBendpoint());
		bendpoints.setPoints(points);
		edge.setBendpoints(bendpoints);
		ViewUtil.insertChildView(containerView, edge, index, persisted);
		edge.setType(TggVisualIDRegistry.getType(CorrespondenceLinkEditPart.VISUAL_ID));
		edge.setElement(domainElement);
		// initializePreferences
		final IPreferenceStore prefStore = (IPreferenceStore) preferencesHint.getPreferenceStore();
		FontStyle edgeFontStyle = (FontStyle) edge.getStyle(NotationPackage.Literals.FONT_STYLE);
		if (edgeFontStyle != null)
		{
			FontData fontData = PreferenceConverter.getFontData(prefStore, IPreferenceConstants.PREF_DEFAULT_FONT);
			edgeFontStyle.setFontName(fontData.getName());
			edgeFontStyle.setFontHeight(fontData.getHeight());
			edgeFontStyle.setBold((fontData.getStyle() & SWT.BOLD) != 0);
			edgeFontStyle.setItalic((fontData.getStyle() & SWT.ITALIC) != 0);
			org.eclipse.swt.graphics.RGB fontRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FONT_COLOR);
			edgeFontStyle.setFontColor(FigureUtilities.RGBToInteger(fontRGB).intValue());
		}
		Routing routing = Routing.get(prefStore.getInt(IPreferenceConstants.PREF_LINE_STYLE));
		if (routing != null)
		{
			ViewUtil.setStructuralFeatureValue(edge, NotationPackage.eINSTANCE.getRoutingStyle_Routing(), routing);
		}
		Node label6002 = createLabel(edge, TggVisualIDRegistry.getType(CorrespondenceLinkModifierEditPart.VISUAL_ID));
		label6002.setLayoutConstraint(NotationFactory.eINSTANCE.createLocation());
		Location location6002 = (Location) label6002.getLayoutConstraint();
		location6002.setX(0);
		location6002.setY(40);
		return edge;
	}

	/**
	 * @generated
	 */
	public Edge createModelLink_4002(EObject domainElement, View containerView, int index, boolean persisted,
			PreferencesHint preferencesHint)
	{
		Edge edge = NotationFactory.eINSTANCE.createEdge();
		edge.getStyles().add(NotationFactory.eINSTANCE.createRoutingStyle());
		edge.getStyles().add(NotationFactory.eINSTANCE.createFontStyle());
		RelativeBendpoints bendpoints = NotationFactory.eINSTANCE.createRelativeBendpoints();
		ArrayList points = new ArrayList(2);
		points.add(new RelativeBendpoint());
		points.add(new RelativeBendpoint());
		bendpoints.setPoints(points);
		edge.setBendpoints(bendpoints);
		ViewUtil.insertChildView(containerView, edge, index, persisted);
		edge.setType(TggVisualIDRegistry.getType(ModelLinkEditPart.VISUAL_ID));
		edge.setElement(domainElement);
		// initializePreferences
		final IPreferenceStore prefStore = (IPreferenceStore) preferencesHint.getPreferenceStore();
		FontStyle edgeFontStyle = (FontStyle) edge.getStyle(NotationPackage.Literals.FONT_STYLE);
		if (edgeFontStyle != null)
		{
			FontData fontData = PreferenceConverter.getFontData(prefStore, IPreferenceConstants.PREF_DEFAULT_FONT);
			edgeFontStyle.setFontName(fontData.getName());
			edgeFontStyle.setFontHeight(fontData.getHeight());
			edgeFontStyle.setBold((fontData.getStyle() & SWT.BOLD) != 0);
			edgeFontStyle.setItalic((fontData.getStyle() & SWT.ITALIC) != 0);
			org.eclipse.swt.graphics.RGB fontRGB = PreferenceConverter.getColor(prefStore, IPreferenceConstants.PREF_FONT_COLOR);
			edgeFontStyle.setFontColor(FigureUtilities.RGBToInteger(fontRGB).intValue());
		}
		Routing routing = Routing.get(prefStore.getInt(IPreferenceConstants.PREF_LINE_STYLE));
		if (routing != null)
		{
			ViewUtil.setStructuralFeatureValue(edge, NotationPackage.eINSTANCE.getRoutingStyle_Routing(), routing);
		}
		Node label6001 = createLabel(edge, TggVisualIDRegistry.getType(ModelLinkReferenceEditPart.VISUAL_ID));
		label6001.setLayoutConstraint(NotationFactory.eINSTANCE.createLocation());
		Location location6001 = (Location) label6001.getLayoutConstraint();
		location6001.setX(0);
		location6001.setY(40);
		Node label6003 = createLabel(edge, TggVisualIDRegistry.getType(ModelLinkModifierEditPart.VISUAL_ID));
		label6003.setLayoutConstraint(NotationFactory.eINSTANCE.createLocation());
		Location location6003 = (Location) label6003.getLayoutConstraint();
		location6003.setX(0);
		location6003.setY(60);
		return edge;
	}

	/**
	 * @generated
	 */
	private void stampShortcut(View containerView, Node target)
	{
		if (!TGGRuleEditPart.MODEL_ID.equals(TggVisualIDRegistry.getModelID(containerView)))
		{
			EAnnotation shortcutAnnotation = EcoreFactory.eINSTANCE.createEAnnotation();
			shortcutAnnotation.setSource("Shortcut"); //$NON-NLS-1$
			shortcutAnnotation.getDetails().put("modelID", TGGRuleEditPart.MODEL_ID); //$NON-NLS-1$
			target.getEAnnotations().add(shortcutAnnotation);
		}
	}

	/**
	 * @generated
	 */
	private Node createLabel(View owner, String hint)
	{
		DecorationNode rv = NotationFactory.eINSTANCE.createDecorationNode();
		rv.setType(hint);
		ViewUtil.insertChildView(owner, rv, ViewUtil.APPEND, true);
		return rv;
	}

	/**
	 * @generated
	 */
	private Node createCompartment(View owner, String hint, boolean canCollapse, boolean hasTitle, boolean canSort, boolean canFilter)
	{
		// SemanticListCompartment rv =
		// NotationFactory.eINSTANCE.createSemanticListCompartment();
		// rv.setShowTitle(showTitle);
		// rv.setCollapsed(isCollapsed);
		Node rv;
		if (canCollapse)
		{
			rv = NotationFactory.eINSTANCE.createBasicCompartment();
		}
		else
		{
			rv = NotationFactory.eINSTANCE.createDecorationNode();
		}
		if (hasTitle)
		{
			TitleStyle ts = NotationFactory.eINSTANCE.createTitleStyle();
			ts.setShowTitle(true);
			rv.getStyles().add(ts);
		}
		if (canSort)
		{
			rv.getStyles().add(NotationFactory.eINSTANCE.createSortingStyle());
		}
		if (canFilter)
		{
			rv.getStyles().add(NotationFactory.eINSTANCE.createFilteringStyle());
		}
		rv.setType(hint);
		ViewUtil.insertChildView(owner, rv, ViewUtil.APPEND, true);
		return rv;
	}

	/**
	 * @generated
	 */
	private EObject getSemanticElement(IAdaptable semanticAdapter)
	{
		if (semanticAdapter == null)
		{
			return null;
		}
		EObject eObject = (EObject) semanticAdapter.getAdapter(EObject.class);
		if (eObject != null)
		{
			return EMFCoreUtil.resolve(TransactionUtil.getEditingDomain(eObject), eObject);
		}
		return null;
	}

	/**
	 * @generated
	 */
	private IElementType getSemanticElementType(IAdaptable semanticAdapter)
	{
		if (semanticAdapter == null)
		{
			return null;
		}
		return (IElementType) semanticAdapter.getAdapter(IElementType.class);
	}
}
