package de.hpi.sam.tgg.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.diagram.edit.parts.AttributeAssignmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression2EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression4EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression5EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.SourceModelDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression2EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression3EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression4EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TGGRuleEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TargetModelDomainEditPart;
import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;

/**
 * @generated
 */
public class TggElementTypes extends ElementInitializers
{

	/**
	 * @generated
	 */
	private TggElementTypes()
	{
	}

	/**
	 * @generated
	 */
	private static Map					elements;

	/**
	 * @generated
	 */
	private static ImageRegistry		imageRegistry;

	/**
	 * @generated
	 */
	private static Set					KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType	TGGRule_1000				= getElementType("de.hpi.sam.tgg.diagram.TGGRule_1000");				//$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType	CorrespondenceDomain_2001	= getElementType("de.hpi.sam.tgg.diagram.CorrespondenceDomain_2001");	//$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType	SourceModelDomain_2002		= getElementType("de.hpi.sam.tgg.diagram.SourceModelDomain_2002");		//$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType	TargetModelDomain_2003		= getElementType("de.hpi.sam.tgg.diagram.TargetModelDomain_2003");		//$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType	StringExpression_2004		= getElementType("de.hpi.sam.tgg.diagram.StringExpression_2004");		//$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType	CallActionExpression_2005	= getElementType("de.hpi.sam.tgg.diagram.CallActionExpression_2005");	//$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType	RuleVariable_2008			= getElementType("de.hpi.sam.tgg.diagram.RuleVariable_2008");			//$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType	CorrespondenceNode_3001		= getElementType("de.hpi.sam.tgg.diagram.CorrespondenceNode_3001");	//$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType	ModelObject_3002			= getElementType("de.hpi.sam.tgg.diagram.ModelObject_3002");			//$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType	StringExpression_3003		= getElementType("de.hpi.sam.tgg.diagram.StringExpression_3003");		//$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType	CallActionExpression_3004	= getElementType("de.hpi.sam.tgg.diagram.CallActionExpression_3004");	//$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType	AttributeAssignment_3005	= getElementType("de.hpi.sam.tgg.diagram.AttributeAssignment_3005");	//$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType	StringExpression_3009		= getElementType("de.hpi.sam.tgg.diagram.StringExpression_3009");		//$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType	CallActionExpression_3010	= getElementType("de.hpi.sam.tgg.diagram.CallActionExpression_3010");	//$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType	StringExpression_3011		= getElementType("de.hpi.sam.tgg.diagram.StringExpression_3011");		//$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType	CallActionExpression_3012	= getElementType("de.hpi.sam.tgg.diagram.CallActionExpression_3012");	//$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType	CorrespondenceLink_4001		= getElementType("de.hpi.sam.tgg.diagram.CorrespondenceLink_4001");	//$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType	ModelLink_4002				= getElementType("de.hpi.sam.tgg.diagram.ModelLink_4002");				//$NON-NLS-1$

	/**
	 * @generated
	 */
	private static ImageRegistry getImageRegistry()
	{
		if (imageRegistry == null)
		{
			imageRegistry = new ImageRegistry();
		}
		return imageRegistry;
	}

	/**
	 * @generated
	 */
	private static String getImageRegistryKey(ENamedElement element)
	{
		return element.getName();
	}

	/**
	 * @generated
	 */
	private static ImageDescriptor getProvidedImageDescriptor(ENamedElement element)
	{
		if (element instanceof EStructuralFeature)
		{
			EStructuralFeature feature = ((EStructuralFeature) element);
			EClass eContainingClass = feature.getEContainingClass();
			EClassifier eType = feature.getEType();
			if (eContainingClass != null && !eContainingClass.isAbstract())
			{
				element = eContainingClass;
			}
			else if (eType instanceof EClass && !((EClass) eType).isAbstract())
			{
				element = eType;
			}
		}
		if (element instanceof EClass)
		{
			EClass eClass = (EClass) element;
			if (!eClass.isAbstract())
			{
				return TggDiagramEditorPlugin.getInstance().getItemImageDescriptor(
						eClass.getEPackage().getEFactoryInstance().create(eClass));
			}
		}
		// TODO : support structural features
		return null;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element)
	{
		String key = getImageRegistryKey(element);
		ImageDescriptor imageDescriptor = getImageRegistry().getDescriptor(key);
		if (imageDescriptor == null)
		{
			imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null)
			{
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
		}
		return imageDescriptor;
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element)
	{
		String key = getImageRegistryKey(element);
		Image image = getImageRegistry().get(key);
		if (image == null)
		{
			ImageDescriptor imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null)
			{
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
			image = getImageRegistry().get(key);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint)
	{
		ENamedElement element = getElement(hint);
		if (element == null)
		{
			return null;
		}
		return getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint)
	{
		ENamedElement element = getElement(hint);
		if (element == null)
		{
			return null;
		}
		return getImage(element);
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint)
	{
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null)
		{
			elements = new IdentityHashMap();

			elements.put(TGGRule_1000, TggPackage.eINSTANCE.getTGGRule());

			elements.put(CorrespondenceDomain_2001, TggPackage.eINSTANCE.getCorrespondenceDomain());

			elements.put(SourceModelDomain_2002, TggPackage.eINSTANCE.getSourceModelDomain());

			elements.put(TargetModelDomain_2003, TggPackage.eINSTANCE.getTargetModelDomain());

			elements.put(StringExpression_2004, de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression());

			elements.put(CallActionExpression_2005,
					de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getCallActionExpression());

			elements.put(RuleVariable_2008, TggPackage.eINSTANCE.getRuleVariable());

			elements.put(CorrespondenceNode_3001, TggPackage.eINSTANCE.getCorrespondenceNode());

			elements.put(ModelObject_3002, TggPackage.eINSTANCE.getModelObject());

			elements.put(StringExpression_3003, de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression());

			elements.put(CallActionExpression_3004,
					de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getCallActionExpression());

			elements.put(AttributeAssignment_3005, de.hpi.sam.storyDiagramEcore.sdm.SdmPackage.eINSTANCE.getAttributeAssignment());

			elements.put(StringExpression_3009, de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression());

			elements.put(CallActionExpression_3010,
					de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getCallActionExpression());

			elements.put(StringExpression_3011, de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getStringExpression());

			elements.put(CallActionExpression_3012,
					de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage.eINSTANCE.getCallActionExpression());

			elements.put(CorrespondenceLink_4001, TggPackage.eINSTANCE.getCorrespondenceLink());

			elements.put(ModelLink_4002, TggPackage.eINSTANCE.getModelLink());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id)
	{
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType)
	{
		if (KNOWN_ELEMENT_TYPES == null)
		{
			KNOWN_ELEMENT_TYPES = new HashSet();
			KNOWN_ELEMENT_TYPES.add(TGGRule_1000);
			KNOWN_ELEMENT_TYPES.add(CorrespondenceDomain_2001);
			KNOWN_ELEMENT_TYPES.add(SourceModelDomain_2002);
			KNOWN_ELEMENT_TYPES.add(TargetModelDomain_2003);
			KNOWN_ELEMENT_TYPES.add(StringExpression_2004);
			KNOWN_ELEMENT_TYPES.add(CallActionExpression_2005);
			KNOWN_ELEMENT_TYPES.add(RuleVariable_2008);
			KNOWN_ELEMENT_TYPES.add(CorrespondenceNode_3001);
			KNOWN_ELEMENT_TYPES.add(ModelObject_3002);
			KNOWN_ELEMENT_TYPES.add(StringExpression_3003);
			KNOWN_ELEMENT_TYPES.add(CallActionExpression_3004);
			KNOWN_ELEMENT_TYPES.add(AttributeAssignment_3005);
			KNOWN_ELEMENT_TYPES.add(StringExpression_3009);
			KNOWN_ELEMENT_TYPES.add(CallActionExpression_3010);
			KNOWN_ELEMENT_TYPES.add(StringExpression_3011);
			KNOWN_ELEMENT_TYPES.add(CallActionExpression_3012);
			KNOWN_ELEMENT_TYPES.add(CorrespondenceLink_4001);
			KNOWN_ELEMENT_TYPES.add(ModelLink_4002);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID)
	{
		switch (visualID)
		{
			case TGGRuleEditPart.VISUAL_ID:
				return TGGRule_1000;
			case CorrespondenceDomainEditPart.VISUAL_ID:
				return CorrespondenceDomain_2001;
			case SourceModelDomainEditPart.VISUAL_ID:
				return SourceModelDomain_2002;
			case TargetModelDomainEditPart.VISUAL_ID:
				return TargetModelDomain_2003;
			case StringExpression2EditPart.VISUAL_ID:
				return StringExpression_2004;
			case CallActionExpression2EditPart.VISUAL_ID:
				return CallActionExpression_2005;
			case RuleVariableEditPart.VISUAL_ID:
				return RuleVariable_2008;
			case CorrespondenceNodeEditPart.VISUAL_ID:
				return CorrespondenceNode_3001;
			case ModelObjectEditPart.VISUAL_ID:
				return ModelObject_3002;
			case StringExpressionEditPart.VISUAL_ID:
				return StringExpression_3003;
			case CallActionExpressionEditPart.VISUAL_ID:
				return CallActionExpression_3004;
			case AttributeAssignmentEditPart.VISUAL_ID:
				return AttributeAssignment_3005;
			case StringExpression3EditPart.VISUAL_ID:
				return StringExpression_3009;
			case CallActionExpression5EditPart.VISUAL_ID:
				return CallActionExpression_3010;
			case StringExpression4EditPart.VISUAL_ID:
				return StringExpression_3011;
			case CallActionExpression4EditPart.VISUAL_ID:
				return CallActionExpression_3012;
			case CorrespondenceLinkEditPart.VISUAL_ID:
				return CorrespondenceLink_4001;
			case ModelLinkEditPart.VISUAL_ID:
				return ModelLink_4002;
		}
		return null;
	}

}
