package de.hpi.sam.tgg.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import de.hpi.sam.tgg.diagram.edit.commands.CallActionExpression4CreateCommand;
import de.hpi.sam.tgg.diagram.edit.commands.StringExpression4CreateCommand;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class RuleVariableRuleVariableRevExpressionCompartmentItemSemanticEditPolicy extends TggBaseItemSemanticEditPolicy
{

	/**
	 * @generated
	 */
	public RuleVariableRuleVariableRevExpressionCompartmentItemSemanticEditPolicy()
	{
		super(TggElementTypes.RuleVariable_2008);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req)
	{
		if (TggElementTypes.StringExpression_3011 == req.getElementType())
		{
			return getGEFWrapper(new StringExpression4CreateCommand(req));
		}
		if (TggElementTypes.CallActionExpression_3012 == req.getElementType())
		{
			return getGEFWrapper(new CallActionExpression4CreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
