package de.hpi.sam.tgg.diagram.edit.parts;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import de.hpi.sam.tgg.diagram.edit.policies.RuleVariableItemSemanticEditPolicy;
import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class RuleVariableEditPart extends ShapeNodeEditPart
{

	/**
	 * @generated
	 */
	public static final int	VISUAL_ID	= 2008;

	/**
	 * @generated
	 */
	protected IFigure		contentPane;

	/**
	 * @generated
	 */
	protected IFigure		primaryShape;

	/**
	 * @generated
	 */
	public RuleVariableEditPart(View view)
	{
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies()
	{
		installEditPolicy(EditPolicyRoles.CREATION_ROLE, new CreationEditPolicy());
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new RuleVariableItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that
		// would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy()
	{
		LayoutEditPolicy lep = new LayoutEditPolicy()
		{

			protected EditPolicy createChildEditPolicy(EditPart child)
			{
				EditPolicy result = child.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null)
				{
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request)
			{
				return null;
			}

			protected Command getCreateCommand(CreateRequest request)
			{
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape()
	{
		RuleVariableFigure figure = new RuleVariableFigure();
		return primaryShape = figure;
	}

	/**
	 * @generated
	 */
	public RuleVariableFigure getPrimaryShape()
	{
		return (RuleVariableFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart)
	{
		if (childEditPart instanceof RuleVariableClassifierEditPart)
		{
			((RuleVariableClassifierEditPart) childEditPart).setLabel(getPrimaryShape().getFigureRuleVariableClassifierFigure());
			return true;
		}
		if (childEditPart instanceof RuleVariableNameEditPart)
		{
			((RuleVariableNameEditPart) childEditPart).setLabel(getPrimaryShape().getFigureRuleVariableNameFigure());
			return true;
		}
		if (childEditPart instanceof RuleVariableRuleVariableForExpressionCompartmentEditPart)
		{
			IFigure pane = getPrimaryShape().getFigureRuleVariableForExpressionFigure();
			setupContentPane(pane); // FIXME each comparment should handle his
			// content pane in his own way
			pane.add(((RuleVariableRuleVariableForExpressionCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		if (childEditPart instanceof RuleVariableRuleVariableRevExpressionCompartmentEditPart)
		{
			IFigure pane = getPrimaryShape().getFigureRuleVariableRevExpressionFigure();
			setupContentPane(pane); // FIXME each comparment should handle his
			// content pane in his own way
			pane.add(((RuleVariableRuleVariableRevExpressionCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart)
	{
		if (childEditPart instanceof RuleVariableClassifierEditPart)
		{
			return true;
		}
		if (childEditPart instanceof RuleVariableNameEditPart)
		{
			return true;
		}
		if (childEditPart instanceof RuleVariableRuleVariableForExpressionCompartmentEditPart)
		{
			IFigure pane = getPrimaryShape().getFigureRuleVariableForExpressionFigure();
			setupContentPane(pane); // FIXME each comparment should handle his
			// content pane in his own way
			pane.remove(((RuleVariableRuleVariableForExpressionCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		if (childEditPart instanceof RuleVariableRuleVariableRevExpressionCompartmentEditPart)
		{
			IFigure pane = getPrimaryShape().getFigureRuleVariableRevExpressionFigure();
			setupContentPane(pane); // FIXME each comparment should handle his
			// content pane in his own way
			pane.remove(((RuleVariableRuleVariableRevExpressionCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index)
	{
		if (addFixedChild(childEditPart))
		{
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart)
	{
		if (removeFixedChild(childEditPart))
		{
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart)
	{
		if (editPart instanceof RuleVariableRuleVariableForExpressionCompartmentEditPart)
		{
			return getPrimaryShape().getFigureRuleVariableForExpressionFigure();
		}
		if (editPart instanceof RuleVariableRuleVariableRevExpressionCompartmentEditPart)
		{
			return getPrimaryShape().getFigureRuleVariableRevExpressionFigure();
		}
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate()
	{
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(40, 40);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure()
	{
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane. Respects
	 * layout one may have set for generated figure.
	 * 
	 * @param nodeShape
	 *            instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape)
	{
		if (nodeShape.getLayoutManager() == null)
		{
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane()
	{
		if (contentPane != null)
		{
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color)
	{
		if (primaryShape != null)
		{
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color)
	{
		if (primaryShape != null)
		{
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width)
	{
		if (primaryShape instanceof Shape)
		{
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style)
	{
		if (primaryShape instanceof Shape)
		{
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart()
	{
		return getChildBySemanticHint(TggVisualIDRegistry.getType(RuleVariableNameEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public EditPart getTargetEditPart(Request request)
	{
		if (request instanceof CreateViewAndElementRequest)
		{
			CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request).getViewAndElementDescriptor()
					.getCreateElementRequestAdapter();
			IElementType type = (IElementType) adapter.getAdapter(IElementType.class);
			if (type == TggElementTypes.StringExpression_3009)
			{
				return getChildBySemanticHint(TggVisualIDRegistry
						.getType(RuleVariableRuleVariableForExpressionCompartmentEditPart.VISUAL_ID));
			}
			if (type == TggElementTypes.CallActionExpression_3010)
			{
				return getChildBySemanticHint(TggVisualIDRegistry
						.getType(RuleVariableRuleVariableForExpressionCompartmentEditPart.VISUAL_ID));
			}
			if (type == TggElementTypes.StringExpression_3011)
			{
				return getChildBySemanticHint(TggVisualIDRegistry
						.getType(RuleVariableRuleVariableRevExpressionCompartmentEditPart.VISUAL_ID));
			}
			if (type == TggElementTypes.CallActionExpression_3012)
			{
				return getChildBySemanticHint(TggVisualIDRegistry
						.getType(RuleVariableRuleVariableRevExpressionCompartmentEditPart.VISUAL_ID));
			}
		}
		return super.getTargetEditPart(request);
	}

	/**
	 * @generated
	 */
	protected void handleNotificationEvent(Notification event)
	{
		if (event.getNotifier() == getModel() && EcorePackage.eINSTANCE.getEModelElement_EAnnotations().equals(event.getFeature()))
		{
			handleMajorSemanticChange();
		}
		else
		{
			super.handleNotificationEvent(event);
		}
	}

	/**
	 * @generated
	 */
	public class RuleVariableFigure extends RoundedRectangle
	{

		/**
		 * @generated
		 */
		private WrappingLabel		fFigureRuleVariableNameFigure;
		/**
		 * @generated
		 */
		private WrappingLabel		fFigureRuleVariableClassifierFigure;
		/**
		 * @generated
		 */
		private RoundedRectangle	fFigureRuleVariableForExpressionFigure;
		/**
		 * @generated
		 */
		private RoundedRectangle	fFigureRuleVariableRevExpressionFigure;

		/**
		 * @generated
		 */
		public RuleVariableFigure()
		{

			BorderLayout layoutThis = new BorderLayout();
			this.setLayoutManager(layoutThis);

			this.setCornerDimensions(new Dimension(getMapMode().DPtoLP(8), getMapMode().DPtoLP(8)));
			this.setLineWidth(1);
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents()
		{

			RoundedRectangle ruleVariableFigureHead0 = new RoundedRectangle();
			ruleVariableFigureHead0.setCornerDimensions(new Dimension(getMapMode().DPtoLP(8), getMapMode().DPtoLP(8)));
			ruleVariableFigureHead0.setFill(false);
			ruleVariableFigureHead0.setOutline(false);
			ruleVariableFigureHead0.setLineWidth(0);

			this.add(ruleVariableFigureHead0, BorderLayout.TOP);

			GridLayout layoutRuleVariableFigureHead0 = new GridLayout();
			layoutRuleVariableFigureHead0.numColumns = 1;
			layoutRuleVariableFigureHead0.makeColumnsEqualWidth = true;
			ruleVariableFigureHead0.setLayoutManager(layoutRuleVariableFigureHead0);

			fFigureRuleVariableNameFigure = new WrappingLabel();
			fFigureRuleVariableNameFigure.setText("");

			GridData constraintFFigureRuleVariableNameFigure = new GridData();
			constraintFFigureRuleVariableNameFigure.verticalAlignment = GridData.BEGINNING;
			constraintFFigureRuleVariableNameFigure.horizontalAlignment = GridData.CENTER;
			constraintFFigureRuleVariableNameFigure.horizontalIndent = 0;
			constraintFFigureRuleVariableNameFigure.horizontalSpan = 1;
			constraintFFigureRuleVariableNameFigure.verticalSpan = 1;
			constraintFFigureRuleVariableNameFigure.grabExcessHorizontalSpace = true;
			constraintFFigureRuleVariableNameFigure.grabExcessVerticalSpace = false;
			ruleVariableFigureHead0.add(fFigureRuleVariableNameFigure, constraintFFigureRuleVariableNameFigure);

			fFigureRuleVariableClassifierFigure = new WrappingLabel();
			fFigureRuleVariableClassifierFigure.setText("");

			GridData constraintFFigureRuleVariableClassifierFigure = new GridData();
			constraintFFigureRuleVariableClassifierFigure.verticalAlignment = GridData.CENTER;
			constraintFFigureRuleVariableClassifierFigure.horizontalAlignment = GridData.CENTER;
			constraintFFigureRuleVariableClassifierFigure.horizontalIndent = 0;
			constraintFFigureRuleVariableClassifierFigure.horizontalSpan = 1;
			constraintFFigureRuleVariableClassifierFigure.verticalSpan = 1;
			constraintFFigureRuleVariableClassifierFigure.grabExcessHorizontalSpace = true;
			constraintFFigureRuleVariableClassifierFigure.grabExcessVerticalSpace = false;
			ruleVariableFigureHead0.add(fFigureRuleVariableClassifierFigure, constraintFFigureRuleVariableClassifierFigure);

			RoundedRectangle ruleVariableFigureBody0 = new RoundedRectangle();
			ruleVariableFigureBody0.setCornerDimensions(new Dimension(getMapMode().DPtoLP(8), getMapMode().DPtoLP(8)));
			ruleVariableFigureBody0.setFill(false);
			ruleVariableFigureBody0.setOutline(false);
			ruleVariableFigureBody0.setLineWidth(0);

			this.add(ruleVariableFigureBody0, BorderLayout.CENTER);

			BorderLayout layoutRuleVariableFigureBody0 = new BorderLayout();
			ruleVariableFigureBody0.setLayoutManager(layoutRuleVariableFigureBody0);

			fFigureRuleVariableForExpressionFigure = new RoundedRectangle();
			fFigureRuleVariableForExpressionFigure.setCornerDimensions(new Dimension(getMapMode().DPtoLP(8), getMapMode().DPtoLP(8)));
			fFigureRuleVariableForExpressionFigure.setFill(false);
			fFigureRuleVariableForExpressionFigure.setOutline(false);
			fFigureRuleVariableForExpressionFigure.setLineWidth(0);

			ruleVariableFigureBody0.add(fFigureRuleVariableForExpressionFigure, BorderLayout.TOP);

			fFigureRuleVariableRevExpressionFigure = new RoundedRectangle();
			fFigureRuleVariableRevExpressionFigure.setCornerDimensions(new Dimension(getMapMode().DPtoLP(8), getMapMode().DPtoLP(8)));
			fFigureRuleVariableRevExpressionFigure.setFill(false);
			fFigureRuleVariableRevExpressionFigure.setOutline(false);
			fFigureRuleVariableRevExpressionFigure.setLineWidth(0);

			ruleVariableFigureBody0.add(fFigureRuleVariableRevExpressionFigure, BorderLayout.CENTER);

		}

		/**
		 * @generated
		 */
		private boolean	myUseLocalCoordinates	= false;

		/**
		 * @generated
		 */
		protected boolean useLocalCoordinates()
		{
			return myUseLocalCoordinates;
		}

		/**
		 * @generated
		 */
		protected void setUseLocalCoordinates(boolean useLocalCoordinates)
		{
			myUseLocalCoordinates = useLocalCoordinates;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureRuleVariableNameFigure()
		{
			return fFigureRuleVariableNameFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureRuleVariableClassifierFigure()
		{
			return fFigureRuleVariableClassifierFigure;
		}

		/**
		 * @generated
		 */
		public RoundedRectangle getFigureRuleVariableForExpressionFigure()
		{
			return fFigureRuleVariableForExpressionFigure;
		}

		/**
		 * @generated
		 */
		public RoundedRectangle getFigureRuleVariableRevExpressionFigure()
		{
			return fFigureRuleVariableRevExpressionFigure;
		}

	}

}
