package de.hpi.sam.tgg.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.PrintingPreferencePage;

import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramPrintingPreferencePage extends PrintingPreferencePage
{

	/**
	 * @generated
	 */
	public DiagramPrintingPreferencePage()
	{
		setPreferenceStore(TggDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
