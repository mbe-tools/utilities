package de.hpi.sam.tgg.diagram.providers;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.diagram.edit.parts.TGGRuleEditPart;
import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;
import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;

/**
 * @generated
 */
public class TggValidationProvider
{

	/**
	 * @generated
	 */
	private static boolean	constraintsActive	= false;

	/**
	 * @generated
	 */
	public static boolean shouldConstraintsBePrivate()
	{
		return false;
	}

	/**
	 * @generated
	 */
	public static void runWithConstraints(TransactionalEditingDomain editingDomain, Runnable operation)
	{
		final Runnable op = operation;
		Runnable task = new Runnable()
		{
			public void run()
			{
				try
				{
					constraintsActive = true;
					op.run();
				}
				finally
				{
					constraintsActive = false;
				}
			}
		};
		if (editingDomain != null)
		{
			try
			{
				editingDomain.runExclusive(task);
			}
			catch (Exception e)
			{
				TggDiagramEditorPlugin.getInstance().logError("Validation failed", e); //$NON-NLS-1$
			}
		}
		else
		{
			task.run();
		}
	}

	/**
	 * @generated
	 */
	static boolean isInDefaultEditorContext(Object object)
	{
		if (shouldConstraintsBePrivate() && !constraintsActive)
		{
			return false;
		}
		if (object instanceof View)
		{
			return constraintsActive && TGGRuleEditPart.MODEL_ID.equals(TggVisualIDRegistry.getModelID((View) object));
		}
		return true;
	}

}
