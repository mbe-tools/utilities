package de.hpi.sam.tgg.diagram.edit.policies;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.notation.View;

import de.hpi.sam.tgg.TggPackage;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectEditPart;
import de.hpi.sam.tgg.diagram.part.TggDiagramUpdater;
import de.hpi.sam.tgg.diagram.part.TggNodeDescriptor;
import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;

/**
 * @generated
 */
public class TargetModelDomainModelDomainCompartmentCanonicalEditPolicy extends CanonicalEditPolicy
{

	/**
	 * @generated
	 */
	Set	myFeaturesToSynchronize;

	/**
	 * @generated
	 */
	protected List getSemanticChildrenList()
	{
		View viewObject = (View) getHost().getModel();
		List result = new LinkedList();
		for (Iterator it = TggDiagramUpdater.getTargetModelDomainModelDomainCompartment_7003SemanticChildren(viewObject).iterator(); it
				.hasNext();)
		{
			result.add(((TggNodeDescriptor) it.next()).getModelElement());
		}
		return result;
	}

	/**
	 * @generated
	 */
	protected boolean isOrphaned(Collection semanticChildren, final View view)
	{
		int visualID = TggVisualIDRegistry.getVisualID(view);
		switch (visualID)
		{
			case ModelObjectEditPart.VISUAL_ID:
				if (!semanticChildren.contains(view.getElement()))
				{
					return true;
				}
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected String getDefaultFactoryHint()
	{
		return null;
	}

	/**
	 * @generated
	 */
	protected Set getFeaturesToSynchronize()
	{
		if (myFeaturesToSynchronize == null)
		{
			myFeaturesToSynchronize = new HashSet();
			myFeaturesToSynchronize.add(TggPackage.eINSTANCE.getModelDomain_ModelElements());
		}
		return myFeaturesToSynchronize;
	}

}
