package de.hpi.sam.tgg.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;

import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class ModelLinkItemSemanticEditPolicy extends TggBaseItemSemanticEditPolicy
{

	/**
	 * @generated
	 */
	public ModelLinkItemSemanticEditPolicy()
	{
		super(TggElementTypes.ModelLink_4002);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req)
	{
		return getGEFWrapper(new DestroyElementCommand(req));
	}

}
