package de.hpi.sam.tgg.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import de.hpi.sam.tgg.diagram.edit.commands.ModelObjectCreateCommand;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class TargetModelDomainModelDomainCompartmentItemSemanticEditPolicy extends TggBaseItemSemanticEditPolicy
{

	/**
	 * @generated
	 */
	public TargetModelDomainModelDomainCompartmentItemSemanticEditPolicy()
	{
		super(TggElementTypes.TargetModelDomain_2003);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req)
	{
		if (TggElementTypes.ModelObject_3002 == req.getElementType())
		{
			return getGEFWrapper(new ModelObjectCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
