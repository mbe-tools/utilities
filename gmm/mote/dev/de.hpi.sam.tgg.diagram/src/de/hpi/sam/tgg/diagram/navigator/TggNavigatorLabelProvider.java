package de.hpi.sam.tgg.diagram.navigator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

import de.hpi.sam.tgg.CorrespondenceDomain;
import de.hpi.sam.tgg.SourceModelDomain;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TargetModelDomain;
import de.hpi.sam.tgg.diagram.edit.parts.AttributeAssignmentEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression2EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression3EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression4EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpression5EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CallActionExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceLinkModifierEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.CorrespondenceNodeNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelLinkReferenceEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.ModelObjectNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.RuleVariableNameEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.SourceModelDomainEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression2EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression3EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpression4EditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpressionEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.StringExpressionExpressionStringEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TGGRuleEditPart;
import de.hpi.sam.tgg.diagram.edit.parts.TargetModelDomainEditPart;
import de.hpi.sam.tgg.diagram.part.TggDiagramEditorPlugin;
import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;
import de.hpi.sam.tgg.diagram.providers.TggParserProvider;

/**
 * @generated
 */
public class TggNavigatorLabelProvider extends LabelProvider implements ICommonLabelProvider, ITreePathLabelProvider
{

	/**
	 * @generated
	 */
	static
	{
		TggDiagramEditorPlugin.getInstance().getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		TggDiagramEditorPlugin.getInstance().getImageRegistry().put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath)
	{
		Object element = elementPath.getLastSegment();
		if (element instanceof TggNavigatorItem && !isOwnView(((TggNavigatorItem) element).getView()))
		{
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element)
	{
		if (element instanceof TggNavigatorGroup)
		{
			TggNavigatorGroup group = (TggNavigatorGroup) element;
			return TggDiagramEditorPlugin.getInstance().getBundledImage(group.getIcon());
		}

		if (element instanceof TggNavigatorItem)
		{
			TggNavigatorItem navigatorItem = (TggNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView()))
			{
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable)
		{
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view))
			{
				return getImage(view);
			}
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view)
	{
		switch (TggVisualIDRegistry.getVisualID(view))
		{
			case TGGRuleEditPart.VISUAL_ID:
				return getImage("Navigator?Diagram?http:///de/hpi/sam/tgg.ecore?TGGRule", TggElementTypes.TGGRule_1000); //$NON-NLS-1$
			case CorrespondenceDomainEditPart.VISUAL_ID:
				return getImage(
						"Navigator?TopLevelNode?http:///de/hpi/sam/tgg.ecore?CorrespondenceDomain", TggElementTypes.CorrespondenceDomain_2001); //$NON-NLS-1$
			case SourceModelDomainEditPart.VISUAL_ID:
				return getImage(
						"Navigator?TopLevelNode?http:///de/hpi/sam/tgg.ecore?SourceModelDomain", TggElementTypes.SourceModelDomain_2002); //$NON-NLS-1$
			case TargetModelDomainEditPart.VISUAL_ID:
				return getImage(
						"Navigator?TopLevelNode?http:///de/hpi/sam/tgg.ecore?TargetModelDomain", TggElementTypes.TargetModelDomain_2003); //$NON-NLS-1$
			case StringExpression2EditPart.VISUAL_ID:
				return getImage(
						"Navigator?TopLevelNode?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?StringExpression", TggElementTypes.StringExpression_2004); //$NON-NLS-1$
			case CallActionExpression2EditPart.VISUAL_ID:
				return getImage(
						"Navigator?TopLevelNode?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?CallActionExpression", TggElementTypes.CallActionExpression_2005); //$NON-NLS-1$
			case RuleVariableEditPart.VISUAL_ID:
				return getImage("Navigator?TopLevelNode?http:///de/hpi/sam/tgg.ecore?RuleVariable", TggElementTypes.RuleVariable_2008); //$NON-NLS-1$
			case CorrespondenceNodeEditPart.VISUAL_ID:
				return getImage("Navigator?Node?http:///de/hpi/sam/tgg.ecore?CorrespondenceNode", TggElementTypes.CorrespondenceNode_3001); //$NON-NLS-1$
			case ModelObjectEditPart.VISUAL_ID:
				return getImage("Navigator?Node?http:///de/hpi/sam/tgg.ecore?ModelObject", TggElementTypes.ModelObject_3002); //$NON-NLS-1$
			case StringExpressionEditPart.VISUAL_ID:
				return getImage(
						"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?StringExpression", TggElementTypes.StringExpression_3003); //$NON-NLS-1$
			case CallActionExpressionEditPart.VISUAL_ID:
				return getImage(
						"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?CallActionExpression", TggElementTypes.CallActionExpression_3004); //$NON-NLS-1$
			case AttributeAssignmentEditPart.VISUAL_ID:
				return getImage(
						"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/sdm.ecore?AttributeAssignment", TggElementTypes.AttributeAssignment_3005); //$NON-NLS-1$
			case StringExpression3EditPart.VISUAL_ID:
				return getImage(
						"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?StringExpression", TggElementTypes.StringExpression_3009); //$NON-NLS-1$
			case CallActionExpression5EditPart.VISUAL_ID:
				return getImage(
						"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?CallActionExpression", TggElementTypes.CallActionExpression_3010); //$NON-NLS-1$
			case StringExpression4EditPart.VISUAL_ID:
				return getImage(
						"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?StringExpression", TggElementTypes.StringExpression_3011); //$NON-NLS-1$
			case CallActionExpression4EditPart.VISUAL_ID:
				return getImage(
						"Navigator?Node?http://de/hpi/sam/storyDiagramEcore/expressions.ecore?CallActionExpression", TggElementTypes.CallActionExpression_3012); //$NON-NLS-1$
			case CorrespondenceLinkEditPart.VISUAL_ID:
				return getImage("Navigator?Link?http:///de/hpi/sam/tgg.ecore?CorrespondenceLink", TggElementTypes.CorrespondenceLink_4001); //$NON-NLS-1$
			case ModelLinkEditPart.VISUAL_ID:
				return getImage("Navigator?Link?http:///de/hpi/sam/tgg.ecore?ModelLink", TggElementTypes.ModelLink_4002); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType)
	{
		ImageRegistry imageRegistry = TggDiagramEditorPlugin.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null && TggElementTypes.isKnownElementType(elementType))
		{
			image = TggElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null)
		{
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element)
	{
		if (element instanceof TggNavigatorGroup)
		{
			TggNavigatorGroup group = (TggNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof TggNavigatorItem)
		{
			TggNavigatorItem navigatorItem = (TggNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView()))
			{
				return null;
			}
			return getText(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable)
		{
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view))
			{
				return getText(view);
			}
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view)
	{
		if (view.getElement() != null && view.getElement().eIsProxy())
		{
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (TggVisualIDRegistry.getVisualID(view))
		{
			case TGGRuleEditPart.VISUAL_ID:
				return getTGGRule_1000Text(view);
			case CorrespondenceDomainEditPart.VISUAL_ID:
				return getCorrespondenceDomain_2001Text(view);
			case SourceModelDomainEditPart.VISUAL_ID:
				return getSourceModelDomain_2002Text(view);
			case TargetModelDomainEditPart.VISUAL_ID:
				return getTargetModelDomain_2003Text(view);
			case StringExpression2EditPart.VISUAL_ID:
				return getStringExpression_2004Text(view);
			case CallActionExpression2EditPart.VISUAL_ID:
				return getCallActionExpression_2005Text(view);
			case RuleVariableEditPart.VISUAL_ID:
				return getRuleVariable_2008Text(view);
			case CorrespondenceNodeEditPart.VISUAL_ID:
				return getCorrespondenceNode_3001Text(view);
			case ModelObjectEditPart.VISUAL_ID:
				return getModelObject_3002Text(view);
			case StringExpressionEditPart.VISUAL_ID:
				return getStringExpression_3003Text(view);
			case CallActionExpressionEditPart.VISUAL_ID:
				return getCallActionExpression_3004Text(view);
			case AttributeAssignmentEditPart.VISUAL_ID:
				return getAttributeAssignment_3005Text(view);
			case StringExpression3EditPart.VISUAL_ID:
				return getStringExpression_3009Text(view);
			case CallActionExpression5EditPart.VISUAL_ID:
				return getCallActionExpression_3010Text(view);
			case StringExpression4EditPart.VISUAL_ID:
				return getStringExpression_3011Text(view);
			case CallActionExpression4EditPart.VISUAL_ID:
				return getCallActionExpression_3012Text(view);
			case CorrespondenceLinkEditPart.VISUAL_ID:
				return getCorrespondenceLink_4001Text(view);
			case ModelLinkEditPart.VISUAL_ID:
				return getModelLink_4002Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getTGGRule_1000Text(View view)
	{
		TGGRule domainModelElement = (TGGRule) view.getElement();
		if (domainModelElement != null)
		{
			return domainModelElement.getName();
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCorrespondenceDomain_2001Text(View view)
	{
		CorrespondenceDomain domainModelElement = (CorrespondenceDomain) view.getElement();
		if (domainModelElement != null)
		{
			return domainModelElement.getName();
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("No domain element for view with visualID = " + 2001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSourceModelDomain_2002Text(View view)
	{
		SourceModelDomain domainModelElement = (SourceModelDomain) view.getElement();
		if (domainModelElement != null)
		{
			return domainModelElement.getName();
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("No domain element for view with visualID = " + 2002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTargetModelDomain_2003Text(View view)
	{
		TargetModelDomain domainModelElement = (TargetModelDomain) view.getElement();
		if (domainModelElement != null)
		{
			return domainModelElement.getName();
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("No domain element for view with visualID = " + 2003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getStringExpression_2004Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.StringExpression_2004, view.getElement() != null ? view.getElement()
				: view, TggVisualIDRegistry.getType(StringExpressionExpressionStringEditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5008); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCallActionExpression_2005Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.CallActionExpression_2005,
				view.getElement() != null ? view.getElement() : view, TggVisualIDRegistry.getType(CallActionExpression3EditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRuleVariable_2008Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.RuleVariable_2008, view.getElement() != null ? view.getElement()
				: view, TggVisualIDRegistry.getType(RuleVariableNameEditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5012); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCorrespondenceNode_3001Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.CorrespondenceNode_3001, view.getElement() != null ? view.getElement()
				: view, TggVisualIDRegistry.getType(CorrespondenceNodeNameEditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getModelObject_3002Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.ModelObject_3002,
				view.getElement() != null ? view.getElement() : view, TggVisualIDRegistry.getType(ModelObjectNameEditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getStringExpression_3003Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.StringExpression_3003, view.getElement() != null ? view.getElement()
				: view, TggVisualIDRegistry.getType(StringExpressionEditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 3003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCallActionExpression_3004Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.CallActionExpression_3004,
				view.getElement() != null ? view.getElement() : view, TggVisualIDRegistry.getType(CallActionExpressionEditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 3004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getAttributeAssignment_3005Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.AttributeAssignment_3005,
				view.getElement() != null ? view.getElement() : view, TggVisualIDRegistry.getType(AttributeAssignmentEditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 3005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getStringExpression_3009Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.StringExpression_3009, view.getElement() != null ? view.getElement()
				: view, TggVisualIDRegistry.getType(StringExpression3EditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 3009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCallActionExpression_3010Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.CallActionExpression_3010,
				view.getElement() != null ? view.getElement() : view, TggVisualIDRegistry.getType(CallActionExpression5EditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 3010); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getStringExpression_3011Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.StringExpression_3011, view.getElement() != null ? view.getElement()
				: view, TggVisualIDRegistry.getType(StringExpression4EditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 3011); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCallActionExpression_3012Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.CallActionExpression_3012,
				view.getElement() != null ? view.getElement() : view, TggVisualIDRegistry.getType(CallActionExpression4EditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 3012); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCorrespondenceLink_4001Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.CorrespondenceLink_4001, view.getElement() != null ? view.getElement()
				: view, TggVisualIDRegistry.getType(CorrespondenceLinkModifierEditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 6002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getModelLink_4002Text(View view)
	{
		IParser parser = TggParserProvider.getParser(TggElementTypes.ModelLink_4002, view.getElement() != null ? view.getElement() : view,
				TggVisualIDRegistry.getType(ModelLinkReferenceEditPart.VISUAL_ID));
		if (parser != null)
		{
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		}
		else
		{
			TggDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 6001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view)
	{
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view)
	{
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig)
	{
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento)
	{
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento)
	{
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement)
	{
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view)
	{
		return TGGRuleEditPart.MODEL_ID.equals(TggVisualIDRegistry.getModelID(view));
	}

}
