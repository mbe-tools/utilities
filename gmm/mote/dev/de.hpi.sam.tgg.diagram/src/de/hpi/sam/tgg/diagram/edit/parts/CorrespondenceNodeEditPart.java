package de.hpi.sam.tgg.diagram.edit.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import de.hpi.sam.tgg.diagram.edit.policies.CorrespondenceNodeItemSemanticEditPolicy;
import de.hpi.sam.tgg.diagram.part.TggVisualIDRegistry;
import de.hpi.sam.tgg.diagram.providers.TggElementTypes;

/**
 * @generated
 */
public class CorrespondenceNodeEditPart extends ShapeNodeEditPart
{

	/**
	 * @generated
	 */
	public static final int	VISUAL_ID	= 3001;

	/**
	 * @generated
	 */
	protected IFigure		contentPane;

	/**
	 * @generated
	 */
	protected IFigure		primaryShape;

	/**
	 * @generated
	 */
	public CorrespondenceNodeEditPart(View view)
	{
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies()
	{
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new CorrespondenceNodeItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that
		// would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy()
	{
		LayoutEditPolicy lep = new LayoutEditPolicy()
		{

			protected EditPolicy createChildEditPolicy(EditPart child)
			{
				EditPolicy result = child.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null)
				{
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request)
			{
				return null;
			}

			protected Command getCreateCommand(CreateRequest request)
			{
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape()
	{
		CorrespondenceNodeFigure figure = new CorrespondenceNodeFigure();
		return primaryShape = figure;
	}

	/**
	 * @generated
	 */
	public CorrespondenceNodeFigure getPrimaryShape()
	{
		return (CorrespondenceNodeFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart)
	{
		if (childEditPart instanceof CorrespondenceNodeInputEditPart)
		{
			((CorrespondenceNodeInputEditPart) childEditPart).setLabel(getPrimaryShape().getFigureCorrespondenceNodeInputFigure());
			return true;
		}
		if (childEditPart instanceof CorrespondenceNodeModifierEditPart)
		{
			((CorrespondenceNodeModifierEditPart) childEditPart).setLabel(getPrimaryShape().getFigureCorrespondenceNodeModifierFigure());
			return true;
		}
		if (childEditPart instanceof CorrespondenceNodeClassifierEditPart)
		{
			((CorrespondenceNodeClassifierEditPart) childEditPart)
					.setLabel(getPrimaryShape().getFigureCorrespondenceNodeClassifierFigure());
			return true;
		}
		if (childEditPart instanceof CorrespondenceNodeNameEditPart)
		{
			((CorrespondenceNodeNameEditPart) childEditPart).setLabel(getPrimaryShape().getFigureCorrespondenceNodeNameFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart)
	{
		if (childEditPart instanceof CorrespondenceNodeInputEditPart)
		{
			return true;
		}
		if (childEditPart instanceof CorrespondenceNodeModifierEditPart)
		{
			return true;
		}
		if (childEditPart instanceof CorrespondenceNodeClassifierEditPart)
		{
			return true;
		}
		if (childEditPart instanceof CorrespondenceNodeNameEditPart)
		{
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index)
	{
		if (addFixedChild(childEditPart))
		{
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart)
	{
		if (removeFixedChild(childEditPart))
		{
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart)
	{
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate()
	{
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(100, 65);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure()
	{
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane. Respects
	 * layout one may have set for generated figure.
	 * 
	 * @param nodeShape
	 *            instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape)
	{
		if (nodeShape.getLayoutManager() == null)
		{
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane()
	{
		if (contentPane != null)
		{
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color)
	{
		if (primaryShape != null)
		{
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color)
	{
		if (primaryShape != null)
		{
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width)
	{
		if (primaryShape instanceof Shape)
		{
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style)
	{
		if (primaryShape instanceof Shape)
		{
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart()
	{
		return getChildBySemanticHint(TggVisualIDRegistry.getType(CorrespondenceNodeNameEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */getMARelTypesOnSource()
	{
		List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */types = new ArrayList/*
																							 * <
																							 * org
																							 * .
																							 * eclipse
																							 * .
																							 * gmf
																							 * .
																							 * runtime
																							 * .
																							 * emf
																							 * .
																							 * type
																							 * .
																							 * core
																							 * .
																							 * IElementType
																							 * >
																							 */();
		types.add(TggElementTypes.CorrespondenceLink_4001);
		return types;
	}

	/**
	 * @generated
	 */
	public List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */getMARelTypesOnSourceAndTarget(IGraphicalEditPart targetEditPart)
	{
		List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */types = new ArrayList/*
																							 * <
																							 * org
																							 * .
																							 * eclipse
																							 * .
																							 * gmf
																							 * .
																							 * runtime
																							 * .
																							 * emf
																							 * .
																							 * type
																							 * .
																							 * core
																							 * .
																							 * IElementType
																							 * >
																							 */();
		if (targetEditPart instanceof ModelObjectEditPart)
		{
			types.add(TggElementTypes.CorrespondenceLink_4001);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */getMATypesForTarget(IElementType relationshipType)
	{
		List/* <org.eclipse.gmf.runtime.emf.type.core.IElementType> */types = new ArrayList/*
																							 * <
																							 * org
																							 * .
																							 * eclipse
																							 * .
																							 * gmf
																							 * .
																							 * runtime
																							 * .
																							 * emf
																							 * .
																							 * type
																							 * .
																							 * core
																							 * .
																							 * IElementType
																							 * >
																							 */();
		if (relationshipType == TggElementTypes.CorrespondenceLink_4001)
		{
			types.add(TggElementTypes.ModelObject_3002);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public class CorrespondenceNodeFigure extends RectangleFigure
	{

		/**
		 * @generated
		 */
		private WrappingLabel	fFigureCorrespondenceNodeNameFigure;
		/**
		 * @generated
		 */
		private WrappingLabel	fFigureCorrespondenceNodeModifierFigure;
		/**
		 * @generated
		 */
		private WrappingLabel	fFigureCorrespondenceNodeClassifierFigure;

		/**
		 * @generated
		 */
		private WrappingLabel	fFigureCorrespondenceNodeInputFigure;

		/**
		 * @generated
		 */
		public CorrespondenceNodeFigure()
		{

			GridLayout layoutThis = new GridLayout();
			layoutThis.numColumns = 1;
			layoutThis.makeColumnsEqualWidth = true;
			this.setLayoutManager(layoutThis);

			this.setLineWidth(1);
			this.setForegroundColor(ColorConstants.black);
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents()
		{

			fFigureCorrespondenceNodeModifierFigure = new WrappingLabel();
			fFigureCorrespondenceNodeModifierFigure.setText("");

			GridData constraintFFigureCorrespondenceNodeModifierFigure = new GridData();
			constraintFFigureCorrespondenceNodeModifierFigure.verticalAlignment = GridData.BEGINNING;
			constraintFFigureCorrespondenceNodeModifierFigure.horizontalAlignment = GridData.CENTER;
			constraintFFigureCorrespondenceNodeModifierFigure.horizontalIndent = 0;
			constraintFFigureCorrespondenceNodeModifierFigure.horizontalSpan = 1;
			constraintFFigureCorrespondenceNodeModifierFigure.verticalSpan = 1;
			constraintFFigureCorrespondenceNodeModifierFigure.grabExcessHorizontalSpace = true;
			constraintFFigureCorrespondenceNodeModifierFigure.grabExcessVerticalSpace = false;
			this.add(fFigureCorrespondenceNodeModifierFigure, constraintFFigureCorrespondenceNodeModifierFigure);

			fFigureCorrespondenceNodeInputFigure = new WrappingLabel();
			fFigureCorrespondenceNodeInputFigure.setText("");

			GridData constraintFFigureCorrespondenceNodeInputFigure = new GridData();
			constraintFFigureCorrespondenceNodeInputFigure.verticalAlignment = GridData.CENTER;
			constraintFFigureCorrespondenceNodeInputFigure.horizontalAlignment = GridData.CENTER;
			constraintFFigureCorrespondenceNodeInputFigure.horizontalIndent = 0;
			constraintFFigureCorrespondenceNodeInputFigure.horizontalSpan = 1;
			constraintFFigureCorrespondenceNodeInputFigure.verticalSpan = 1;
			constraintFFigureCorrespondenceNodeInputFigure.grabExcessHorizontalSpace = true;
			constraintFFigureCorrespondenceNodeInputFigure.grabExcessVerticalSpace = false;
			this.add(fFigureCorrespondenceNodeInputFigure, constraintFFigureCorrespondenceNodeInputFigure);

			fFigureCorrespondenceNodeNameFigure = new WrappingLabel();
			fFigureCorrespondenceNodeNameFigure.setText("");

			GridData constraintFFigureCorrespondenceNodeNameFigure = new GridData();
			constraintFFigureCorrespondenceNodeNameFigure.verticalAlignment = GridData.END;
			constraintFFigureCorrespondenceNodeNameFigure.horizontalAlignment = GridData.CENTER;
			constraintFFigureCorrespondenceNodeNameFigure.horizontalIndent = 0;
			constraintFFigureCorrespondenceNodeNameFigure.horizontalSpan = 1;
			constraintFFigureCorrespondenceNodeNameFigure.verticalSpan = 1;
			constraintFFigureCorrespondenceNodeNameFigure.grabExcessHorizontalSpace = true;
			constraintFFigureCorrespondenceNodeNameFigure.grabExcessVerticalSpace = false;
			this.add(fFigureCorrespondenceNodeNameFigure, constraintFFigureCorrespondenceNodeNameFigure);

			fFigureCorrespondenceNodeClassifierFigure = new WrappingLabel();
			fFigureCorrespondenceNodeClassifierFigure.setText("");

			GridData constraintFFigureCorrespondenceNodeClassifierFigure = new GridData();
			constraintFFigureCorrespondenceNodeClassifierFigure.verticalAlignment = GridData.END;
			constraintFFigureCorrespondenceNodeClassifierFigure.horizontalAlignment = GridData.CENTER;
			constraintFFigureCorrespondenceNodeClassifierFigure.horizontalIndent = 0;
			constraintFFigureCorrespondenceNodeClassifierFigure.horizontalSpan = 1;
			constraintFFigureCorrespondenceNodeClassifierFigure.verticalSpan = 1;
			constraintFFigureCorrespondenceNodeClassifierFigure.grabExcessHorizontalSpace = true;
			constraintFFigureCorrespondenceNodeClassifierFigure.grabExcessVerticalSpace = false;
			this.add(fFigureCorrespondenceNodeClassifierFigure, constraintFFigureCorrespondenceNodeClassifierFigure);

		}

		/**
		 * @generated
		 */
		private boolean	myUseLocalCoordinates	= false;

		/**
		 * @generated
		 */
		protected boolean useLocalCoordinates()
		{
			return myUseLocalCoordinates;
		}

		/**
		 * @generated
		 */
		protected void setUseLocalCoordinates(boolean useLocalCoordinates)
		{
			myUseLocalCoordinates = useLocalCoordinates;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureCorrespondenceNodeNameFigure()
		{
			return fFigureCorrespondenceNodeNameFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureCorrespondenceNodeModifierFigure()
		{
			return fFigureCorrespondenceNodeModifierFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureCorrespondenceNodeClassifierFigure()
		{
			return fFigureCorrespondenceNodeClassifierFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureCorrespondenceNodeInputFigure()
		{
			return fFigureCorrespondenceNodeInputFigure;
		}

	}

}
