/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.tgg.ruleDependency.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;

import de.hpi.sam.tgg.CorrespondenceElement;
import de.hpi.sam.tgg.CorrespondenceNode;
import de.hpi.sam.tgg.TGGModifierEnumeration;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.ruleDependency.CorrNodeDependency;
import de.hpi.sam.tgg.ruleDependency.RuleDependencyNode;
import de.hpi.sam.tgg.ruleDependency.RuleDependencyPackage;

/**
 * This is the item provider adapter for a
 * {@link de.hpi.sam.tgg.ruleDependency.CorrNodeDependency} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class CorrNodeDependencyItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CorrNodeDependencyItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (this.itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);

			this.addRequiredDependenciesPropertyDescriptor(object);
			this.addCorrNodePropertyDescriptor(object);
		}
		return this.itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Required Dependencies feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addRequiredDependenciesPropertyDescriptor(Object object)
	{
		this.itemPropertyDescriptors.add(this.createItemPropertyDescriptor(((ComposeableAdapterFactory) this.adapterFactory)
				.getRootAdapterFactory(), this.getResourceLocator(), this.getString("_UI_CorrNodeDependency_requiredDependencies_feature"),
				this.getString("_UI_PropertyDescriptor_description", "_UI_CorrNodeDependency_requiredDependencies_feature",
						"_UI_CorrNodeDependency_type"), RuleDependencyPackage.Literals.CORR_NODE_DEPENDENCY__REQUIRED_DEPENDENCIES, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Corr Node feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected void addCorrNodePropertyDescriptor(Object object)
	{

		// this.itemPropertyDescriptors.add(this.createItemPropertyDescriptor(((ComposeableAdapterFactory)
		// this.adapterFactory)
		// .getRootAdapterFactory(), this.getResourceLocator(),
		// this.getString("_UI_CorrNodeDependency_corrNode_feature"), this
		// .getString("_UI_PropertyDescriptor_description",
		// "_UI_CorrNodeDependency_corrNode_feature",
		// "_UI_CorrNodeDependency_type"),
		// RuleDependencyPackage.Literals.CORR_NODE_DEPENDENCY__CORR_NODE, true,
		// false, true, null, null, null));

		this.itemPropertyDescriptors.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) this.adapterFactory)
				.getRootAdapterFactory(), this.getResourceLocator(), this.getString("_UI_CorrNodeDependency_corrNode_feature"), this
				.getString("_UI_PropertyDescriptor_description", "_UI_CorrNodeDependency_corrNode_feature", "_UI_CorrNodeDependency_type"),
				RuleDependencyPackage.Literals.CORR_NODE_DEPENDENCY__CORR_NODE, true, false, true, null, null, null)
		{
			@Override
			public Collection<?> getChoiceOfValues(Object object)
			{
				CorrNodeDependency dependency = (CorrNodeDependency) object;
				ArrayList<CorrespondenceNode> values = new ArrayList<CorrespondenceNode>();

				TGGRule rule = ((RuleDependencyNode) dependency.eContainer()).getTggRule();
				for (CorrespondenceElement element : rule.getCorrespondenceDomain().getCorrespondenceElements())
				{
					if (element instanceof CorrespondenceNode && element.getModifier() == TGGModifierEnumeration.NONE)
					{
						values.add((CorrespondenceNode) element);
					}
				}

				return values;
			}
		});
	}

	/**
	 * This returns CorrNodeDependency.gif. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object)
	{
		return this.overlayImage(object, this.getResourceLocator().getImage("full/obj16/CorrNodeDependency"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object)
	{
		return this.getString("_UI_CorrNodeDependency_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to
	 * update any cached children and by creating a viewer notification, which
	 * it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		this.updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator()
	{
		return RuleDependencyEditPlugin.INSTANCE;
	}

}
