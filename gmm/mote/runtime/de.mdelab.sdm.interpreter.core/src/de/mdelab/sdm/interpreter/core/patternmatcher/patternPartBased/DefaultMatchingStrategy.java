package de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased;

import java.util.LinkedHashSet;
import java.util.Set;

import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.facade.MetamodelFacadeFactory;
import de.mdelab.sdm.interpreter.core.patternmatcher.MatchingStrategy;

/**
 * The default matching strategy of the story pattern matcher. It searches
 * matches in the order of their cost estimates, starting with the cheapest
 * ones.
 * 
 * DB: Important to change the matching strategy. The tricky point is that the order in which the pattern parts are returned has
 * to be reproducible. Otherwise the match algorithm for a given activity may not terminate.
 * 
 * @author Stephan Hildebrandt
 * 
 * @param <StoryPattern>
 * @param <StoryPatternObject>
 * @param <StoryPatternLink>
 * @param <Classifier>
 * @param <Feature>
 * @param <Expression>
 */
public class DefaultMatchingStrategy<StoryPattern, StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> extends
		MatchingStrategy<StoryPattern, StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> {

	private final LinkedHashSet<PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>>	log;
	
	public DefaultMatchingStrategy(
			final MetamodelFacadeFactory<?, ?, ?, StoryPattern, StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> facadeFactory) {
		super(facadeFactory);

		log = new LinkedHashSet<PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>>();
	}

	/**
	 * Return the pattern part for which the next match should be sought.
	 * 
	 * @param uncheckedPatternParts
	 * @return
	 */
	public PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> getNextPatternPartForMatching(
			PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> p_previouslyMatchedPatternPart,
			Set<PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>> p_uncheckedPatternParts )
	throws SDMException {
		final PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> patternPart = determineNextPatternPartForMatching( p_previouslyMatchedPatternPart,
																																					p_uncheckedPatternParts );
		
		if ( patternPart != null && !log.contains( patternPart ) ) {
			log.add( patternPart );
		}
		
		return patternPart;
	}
	
	protected PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> determineNextPatternPartForMatching( PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> p_previouslyMatchedPatternPart,
			Set<PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>> uncheckedPatternParts )
	throws SDMException {
		int lowestCost = PatternPart.MATCHING_NOT_POSSIBLE;
		int lowestCostOptional = PatternPart.MATCHING_NOT_POSSIBLE;
		int cost = 0;

		PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> cheapestPatternPart = null;
		PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> cheapestPatternPartOptional = null;

		/*
		 * Look at all unchecked pattern parts.
		 */
		for ( final PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> patternPart : uncheckedPatternParts ) {
			
			/*
			 * Calculate matching cost. Return MATCHING_NOT_POSSIBLE for every story pattern link that does not have any of its end bound.
			 */
			cost = patternPart.calculateMatchingCost();

			if ( cost == PatternPart.MATCHING_NOT_POSSIBLE ) {
				// Skip
			}
			else {
				switch ( patternPart.getMatchType() ) {
					case MANDATORY:
						// case NEGATIVE:
						if ( cost == 1 ) {
							/*
							 * The cost can hardly be lower, return immediately.
							 */
							return patternPart;
						}
						
						if ((cost < lowestCost) || (cheapestPatternPart == null)) {
							/*
							 * keep this pattern part if it is cheaper
							 */
							lowestCost = cost;
							cheapestPatternPart = patternPart;
						}
						
						break;
					case OPTIONAL:
						if ((cost < lowestCostOptional) || (cheapestPatternPartOptional == null))
						{
							lowestCostOptional = cost;
							cheapestPatternPartOptional = patternPart;
						}
						
						break;
				}
			}
		}

		if ( cheapestPatternPart != null ) {
			return cheapestPatternPart;
		}

		return cheapestPatternPartOptional;
	}

	public LinkedHashSet<PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>> getLog() {
		return log;
	}
}
