package de.mdelab.sdm.interpreter.core;


/**
 * General container exception for all kinds of exceptions occurring during the
 * execution of a story diagram.
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDMException extends Exception
{
	private static final long	serialVersionUID	= 4051161944125615443L;

	// DB: Better use expression nesting. As is, the trace of innerException is not seen.
	//private String				message				= "";
	//private Exception			innerException		= null;

	public SDMException( final String p_message ) {
		super( p_message );
	}

	public SDMException(	final String p_message, 
							final Throwable p_throwable ) {
		super( p_message, p_throwable );
	}

	public SDMException( final Throwable p_th )	{
		super( p_th );
	}
}
