package de.mdelab.sdm.interpreter.core.notifications;

import de.mdelab.sdm.interpreter.core.variables.VariablesScope;

/**
 * Execution of an activity node has started.
 * 
 * @author Stephan Hildebrandt
 * 
 * @param <ActivityNode>
 * @param <Classifier>
 */
public class ActivityNodeExecutionStartedNotification<ActivityNode, Classifier> extends InterpreterNotification<Classifier> {
	
	private final ActivityNode	activityNode;

	public ActivityNodeExecutionStartedNotification(	final VariablesScope<?, ActivityNode, ?, ?, ?, ?, Classifier, ?, ?> variablesScope,
														final Notifier<?, ActivityNode, ?, ?, ?, ?, Classifier, ?, ?> notifier, 
														final ActivityNode activityNode ) {
		super( NotificationTypeEnum.ACTIVITY_NODE_EXECUTION_STARTED, variablesScope, notifier );

		assert activityNode != null;

		this.activityNode = activityNode;
	}

	public ActivityNode getActivityNode() {
		return activityNode;
	}

	@Override
	public ActivityNode getMainStoryDiagramElement() {
		return getActivityNode();
	}
}
