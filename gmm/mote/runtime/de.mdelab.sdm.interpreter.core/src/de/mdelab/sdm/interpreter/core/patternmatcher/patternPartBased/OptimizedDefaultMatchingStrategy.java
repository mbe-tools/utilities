package de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.facade.MetamodelFacadeFactory;

/**
 * Improves the default matching strategy by returning story pattern parts that are linked to the previously matched
 * pattern part. This reduces the matching time of complex patterns by avoiding to match several branches of
 * an object graph in parallel.
 * 
 * @author dblouin
 *
 * @param <StoryPattern>
 * @param <StoryPatternObject>
 * @param <StoryPatternLink>
 * @param <Classifier>
 * @param <Feature>
 * @param <Expression>
 */
public class OptimizedDefaultMatchingStrategy<StoryPattern, StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>
		extends	DefaultMatchingStrategy<StoryPattern, StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> {

	public OptimizedDefaultMatchingStrategy( final MetamodelFacadeFactory<?, ?, ?, StoryPattern, StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> facadeFactory) {
		super(facadeFactory);
	}
	
	private boolean areDirectlyLinked( PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> p_pattern1,
			PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> p_pattern2 ) {
		if ( 	p_pattern1 == null || p_pattern1.getStoryPatternObjects().length < 2 ||
				p_pattern2 == null || p_pattern2.getStoryPatternObjects().length < 2 ) {
			return false;
		}
		
		return p_pattern1.getStoryPatternObjects()[ 1 ] == p_pattern2.getStoryPatternObjects()[ 0 ];
	}
	
	@Override
	protected PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> determineNextPatternPartForMatching(
			final PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> p_prevMatchedPatternPart,
			final Set<PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>> p_uncheckedPatternParts)
	throws SDMException {
		final Set<PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>> unLinkedPatternParts = new LinkedHashSet<PatternPart<StoryPatternObject,StoryPatternLink,Classifier,Feature,Expression>>( p_uncheckedPatternParts );
		final Set<PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>> linkedPatternParts = new LinkedHashSet<PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>>();
		final Iterator<PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>> partsIt = unLinkedPatternParts.iterator();
		
		while ( partsIt.hasNext() ) {
			final PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> part = partsIt.next();
			
			if ( areDirectlyLinked( p_prevMatchedPatternPart, part ) ) {
				linkedPatternParts.add( part );
				partsIt.remove();
			}
		}
		
		PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> nextPart = super.determineNextPatternPartForMatching( 	p_prevMatchedPatternPart,
																																					linkedPatternParts );
		
		if ( nextPart == null ) {
			nextPart = super.determineNextPatternPartForMatching( p_prevMatchedPatternPart, unLinkedPatternParts  );
		}
		
		return nextPart;
	}
}
