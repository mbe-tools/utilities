package de.mdelab.sdm.interpreter.core.facade;

import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.expressions.ExpressionInterpreterManager;
import de.mdelab.sdm.interpreter.core.variables.VariablesScope;

/**
 * This facade provides access to properties of story pattern links.
 * 
 * @author Stephan Hildebrandt
 * 
 * @param <StoryPatternObject>
 * @param <StoryPatternLink>
 * @param <Feature>
 */
public interface IStoryPatternLinkFacade<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> extends IMetamodelFacade
{

	String getName(StoryPatternLink spl);
	
	boolean isMany( StoryPatternLink spl );

	boolean isDerived( StoryPatternLink spl );

	int matchingCostPenalty( StoryPatternLink spl );
	
	Object getFeatureValue( StoryPatternLink spl,
							Object instanceObject,
							ExpressionInterpreterManager<?, ?, ?, ?, StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> p_exprInterpreter,
							VariablesScope<?, ?, ?, ?, StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> p_variableScope )
	throws SDMException;
	
	boolean isReference( StoryPatternLink spl );

	boolean isContainment( StoryPatternLink spl );
	
	boolean hasReverseReference( StoryPatternLink spl );

	boolean reverseReferenceIsMany( StoryPatternLink spl );
	
	Object getReverseFeatureValue( 	StoryPatternLink spl,
									Object instanceObject );
}
