package de.mdelab.sdm.interpreter.core;

public class UnresolvedReferenceException extends SDMException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8721451268556554243L;
	
	private final String refObjectUri;

	public UnresolvedReferenceException( 	final String p_message,
											final String p_refObjectUri ) {
		super( p_message );
		
		refObjectUri = p_refObjectUri;
	}

	public String getRefObjectUri() {
		return refObjectUri;
	}
}
