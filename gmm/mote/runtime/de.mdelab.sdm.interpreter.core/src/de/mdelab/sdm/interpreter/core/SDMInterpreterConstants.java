package de.mdelab.sdm.interpreter.core;

/**
 * Some constants that are used through the interpreter.
 * 
 * @author Stephan Hildebrandt
 * 
 */
public interface SDMInterpreterConstants
{
	/*
	 * Name of the return variable of the activity.
	 */
	public static final String	RETURN_VALUE_VAR_NAME				= "##return_value";

	/*
	 * Default name of internal variables.
	 */
	public static final String	INTERNAL_VAR_NAME					= "##internal_variable";

	public static final String	DEFAULT_EXPRESSION_LANGUAGE_VERSION	= "1.0";

}
