package de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased;

import java.util.Set;

public class CheckPatternPartTransaction<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> extends
		PatternPartTransaction<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>
{

	public CheckPatternPartTransaction(PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression> patternPart,
			Set<PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>> uncheckedPatternParts,
			Set<PatternPart<StoryPatternObject, StoryPatternLink, Classifier, Feature, Expression>> checkedPatternParts)
	{
		super(patternPart, uncheckedPatternParts, checkedPatternParts);
	}
}
