/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.helpers.provider;

import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.helpers.RuleSetTag;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemFontProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link de.hpi.sam.mote.helpers.RuleSetTag} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RuleSetTagItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider, IItemFontProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleSetTagItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addRuleSetIDPropertyDescriptor(object);
			addRuleSetNamePropertyDescriptor(object);
			addSourceModelIDPropertyDescriptor(object);
			addTargetModelIDPropertyDescriptor(object);
			addRuleSetPackageNsURIPropertyDescriptor(object);
			addSourceModelUriPropertyDescriptor(object);
			addTargetModelUriPropertyDescriptor(object);
			addRuleSetPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Rule Set ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRuleSetIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_RuleSetTag_ruleSetID_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_RuleSetTag_ruleSetID_feature",
						"_UI_RuleSetTag_type"),
				HelpersPackage.Literals.RULE_SET_TAG__RULE_SET_ID, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Rule Set Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRuleSetNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_RuleSetTag_ruleSetName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_RuleSetTag_ruleSetName_feature",
						"_UI_RuleSetTag_type"),
				HelpersPackage.Literals.RULE_SET_TAG__RULE_SET_NAME, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Source Model ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceModelIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_RuleSetTag_sourceModelID_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_RuleSetTag_sourceModelID_feature",
						"_UI_RuleSetTag_type"),
				HelpersPackage.Literals.RULE_SET_TAG__SOURCE_MODEL_ID, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Target Model ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetModelIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_RuleSetTag_targetModelID_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_RuleSetTag_targetModelID_feature",
						"_UI_RuleSetTag_type"),
				HelpersPackage.Literals.RULE_SET_TAG__TARGET_MODEL_ID, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Rule Set Package Ns URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRuleSetPackageNsURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_RuleSetTag_ruleSetPackageNsURI_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_RuleSetTag_ruleSetPackageNsURI_feature",
						"_UI_RuleSetTag_type"),
				HelpersPackage.Literals.RULE_SET_TAG__RULE_SET_PACKAGE_NS_URI,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Source Model Uri feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceModelUriPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_RuleSetTag_sourceModelUri_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_RuleSetTag_sourceModelUri_feature",
						"_UI_RuleSetTag_type"),
				HelpersPackage.Literals.RULE_SET_TAG__SOURCE_MODEL_URI, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Target Model Uri feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetModelUriPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_RuleSetTag_targetModelUri_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_RuleSetTag_targetModelUri_feature",
						"_UI_RuleSetTag_type"),
				HelpersPackage.Literals.RULE_SET_TAG__TARGET_MODEL_URI, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Rule Set feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRuleSetPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_RuleSetTag_ruleSet_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_RuleSetTag_ruleSet_feature",
								"_UI_RuleSetTag_type"),
						HelpersPackage.Literals.RULE_SET_TAG__RULE_SET, true,
						false, true, null, null, null));
	}

	/**
	 * This returns RuleSetTag.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/RuleSetTag"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((RuleSetTag) object).getRuleSetName();
		return label == null || label.length() == 0 ? getString("_UI_RuleSetTag_type")
				: getString("_UI_RuleSetTag_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(RuleSetTag.class)) {
		case HelpersPackage.RULE_SET_TAG__RULE_SET_ID:
		case HelpersPackage.RULE_SET_TAG__RULE_SET_NAME:
		case HelpersPackage.RULE_SET_TAG__SOURCE_MODEL_ID:
		case HelpersPackage.RULE_SET_TAG__TARGET_MODEL_ID:
		case HelpersPackage.RULE_SET_TAG__RULE_SET_PACKAGE_NS_URI:
		case HelpersPackage.RULE_SET_TAG__SOURCE_MODEL_URI:
		case HelpersPackage.RULE_SET_TAG__TARGET_MODEL_URI:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender) adapterFactory).getResourceLocator();
	}

}
