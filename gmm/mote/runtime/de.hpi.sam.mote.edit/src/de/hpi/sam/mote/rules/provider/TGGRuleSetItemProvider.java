/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemFontProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.hpi.sam.mote.helpers.HelpersFactory;
import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.provider.MoteEditPlugin;
import de.hpi.sam.mote.rules.RulesFactory;
import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.mote.rules.TGGRuleSet;

/**
 * This is the item provider adapter for a {@link de.hpi.sam.mote.rules.TGGRuleSet} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TGGRuleSetItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider, IItemFontProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TGGRuleSetItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addRootCorrNodePropertyDescriptor(object);
			addAxiomPropertyDescriptor(object);
			addRulesPropertyDescriptor(object);
			addProcessNotificationsPropertyDescriptor(object);
			addSourceModelElementsAdapterPropertyDescriptor(object);
			addTargetModelElementsAdapterPropertyDescriptor(object);
			addTransformationQueuePropertyDescriptor(object);
			addSourceReverseNavigationStorePropertyDescriptor(object);
			addTargetReverseNavigationStorePropertyDescriptor(object);
			addSourceModelRootNodePropertyDescriptor(object);
			addTargetModelRootNodePropertyDescriptor(object);
			addCorrNodesToDeletePropertyDescriptor(object);
			addResourceSetPropertyDescriptor(object);
			addIdPropertyDescriptor(object);
			addExternalReferencesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Root Corr Node feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRootCorrNodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_rootCorrNode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_rootCorrNode_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__ROOT_CORR_NODE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Axiom feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAxiomPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_axiom_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_axiom_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__AXIOM,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Rules feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRulesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_rules_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_rules_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__RULES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Process Notifications feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProcessNotificationsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_processNotifications_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_processNotifications_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__PROCESS_NOTIFICATIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source Model Elements Adapter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceModelElementsAdapterPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_sourceModelElementsAdapter_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_sourceModelElementsAdapter_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS_ADAPTER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Model Elements Adapter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetModelElementsAdapterPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_targetModelElementsAdapter_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_targetModelElementsAdapter_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__TARGET_MODEL_ELEMENTS_ADAPTER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Transformation Queue feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransformationQueuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_transformationQueue_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_transformationQueue_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__TRANSFORMATION_QUEUE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source Reverse Navigation Store feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceReverseNavigationStorePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_sourceReverseNavigationStore_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_sourceReverseNavigationStore_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__SOURCE_REVERSE_NAVIGATION_STORE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Reverse Navigation Store feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetReverseNavigationStorePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_targetReverseNavigationStore_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_targetReverseNavigationStore_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__TARGET_REVERSE_NAVIGATION_STORE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source Model Root Node feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceModelRootNodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_sourceModelRootNode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_sourceModelRootNode_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__SOURCE_MODEL_ROOT_NODE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Model Root Node feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetModelRootNodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_targetModelRootNode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_targetModelRootNode_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__TARGET_MODEL_ROOT_NODE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Corr Nodes To Delete feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrNodesToDeletePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_corrNodesToDelete_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_corrNodesToDelete_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__CORR_NODES_TO_DELETE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Resource Set feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addResourceSetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_resourceSet_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_resourceSet_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__RESOURCE_SET,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_id_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the External References feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExternalReferencesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TGGRuleSet_externalReferences_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TGGRuleSet_externalReferences_feature", "_UI_TGGRuleSet_type"),
				 RulesPackage.Literals.TGG_RULE_SET__EXTERNAL_REFERENCES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(RulesPackage.Literals.TGG_RULE_SET__CORRESPONDENCE_NODES);
			childrenFeatures.add(RulesPackage.Literals.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS);
			childrenFeatures.add(RulesPackage.Literals.TGG_RULE_SET__TARGET_MODEL_ELEMENTS);
			childrenFeatures.add(RulesPackage.Literals.TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS);
			childrenFeatures.add(RulesPackage.Literals.TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS);
			childrenFeatures.add(RulesPackage.Literals.TGG_RULE_SET__EXTERNAL_REF_PATTERNS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns TGGRuleSet.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/TGGRuleSet"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((TGGRuleSet)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_TGGRuleSet_type") :
			getString("_UI_TGGRuleSet_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TGGRuleSet.class)) {
			case RulesPackage.TGG_RULE_SET__PROCESS_NOTIFICATIONS:
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS_ADAPTER:
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS_ADAPTER:
			case RulesPackage.TGG_RULE_SET__RESOURCE_SET:
			case RulesPackage.TGG_RULE_SET__ID:
			case RulesPackage.TGG_RULE_SET__EXTERNAL_REFERENCES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case RulesPackage.TGG_RULE_SET__CORRESPONDENCE_NODES:
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS:
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS:
			case RulesPackage.TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS:
			case RulesPackage.TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS:
			case RulesPackage.TGG_RULE_SET__EXTERNAL_REF_PATTERNS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(RulesPackage.Literals.TGG_RULE_SET__CORRESPONDENCE_NODES,
				 HelpersFactory.eINSTANCE.create(HelpersPackage.Literals.MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RulesPackage.Literals.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS,
				 HelpersFactory.eINSTANCE.create(HelpersPackage.Literals.MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RulesPackage.Literals.TGG_RULE_SET__TARGET_MODEL_ELEMENTS,
				 HelpersFactory.eINSTANCE.create(HelpersPackage.Literals.MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RulesPackage.Literals.TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS,
				 HelpersFactory.eINSTANCE.create(HelpersPackage.Literals.MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RulesPackage.Literals.TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS,
				 HelpersFactory.eINSTANCE.create(HelpersPackage.Literals.MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RulesPackage.Literals.TGG_RULE_SET__EXTERNAL_REF_PATTERNS,
				 RulesFactory.eINSTANCE.createReferencePattern()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature,
			Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == RulesPackage.Literals.TGG_RULE_SET__CORRESPONDENCE_NODES ||
			childFeature == RulesPackage.Literals.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS ||
			childFeature == RulesPackage.Literals.TGG_RULE_SET__TARGET_MODEL_ELEMENTS ||
			childFeature == RulesPackage.Literals.TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS ||
			childFeature == RulesPackage.Literals.TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MoteEditPlugin.INSTANCE;
	}

}
