/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage
 * @generated
 */
public interface SdmFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	SdmFactory eINSTANCE = de.hpi.sam.storyDiagramEcore.sdm.impl.SdmFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>Attribute Assignment</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Attribute Assignment</em>'.
	 * @generated
	 */
	AttributeAssignment createAttributeAssignment();

	/**
	 * Returns a new object of class '<em>Story Pattern Link</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Story Pattern Link</em>'.
	 * @generated
	 */
	StoryPatternLink createStoryPatternLink();

	/**
	 * Returns a new object of class '<em>Story Pattern Containment Link</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return a new object of class '<em>Story Pattern Containment Link</em>'.
	 * @generated
	 */
	StoryPatternContainmentLink createStoryPatternContainmentLink();

	/**
	 * Returns a new object of class '<em>Story Pattern Expression Link</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return a new object of class '<em>Story Pattern Expression Link</em>'.
	 * @generated
	 */
	StoryPatternExpressionLink createStoryPatternExpressionLink();

	/**
	 * Returns a new object of class '<em>Story Pattern Object</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Story Pattern Object</em>'.
	 * @generated
	 */
	StoryPatternObject createStoryPatternObject();

	/**
	 * Returns a new object of class '<em>Map Entry Story Pattern Link</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return a new object of class '<em>Map Entry Story Pattern Link</em>'.
	 * @generated
	 */
	MapEntryStoryPatternLink createMapEntryStoryPatternLink();

	/**
	 * Returns a new object of class '<em>EContainer Story Pattern Link</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return a new object of class '<em>EContainer Story Pattern Link</em>'.
	 * @generated
	 */
	EContainerStoryPatternLink createEContainerStoryPatternLink();

	/**
	 * Returns a new object of class '<em>Link Position Constraint</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Link Position Constraint</em>'.
	 * @generated
	 */
	LinkPositionConstraint createLinkPositionConstraint();

	/**
	 * Returns a new object of class '<em>Link Order Constraint</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Link Order Constraint</em>'.
	 * @generated
	 */
	LinkOrderConstraint createLinkOrderConstraint();

	/**
	 * Returns a new object of class '<em>External Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>External Reference</em>'.
	 * @generated
	 */
	ExternalReference createExternalReference();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SdmPackage getSdmPackage();

} // SdmFactory
