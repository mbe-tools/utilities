/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreFactory
 * @model kind="package"
 * @generated
 */
public interface StoryDiagramEcorePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "storyDiagramEcore";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de/hpi/sam/storyDiagramEcore.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.hpi.sam.storyDiagramEcore";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	StoryDiagramEcorePackage eINSTANCE = de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl
			.init();

	/**
	 * The meta object id for the '
	 * {@link de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl
	 * <em>Named Element</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl
	 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__UUID = 2;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.impl.ActivityImpl <em>Activity</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.impl.ActivityImpl
	 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getActivity()
	 * @generated
	 */
	int ACTIVITY = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__UUID = NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__NODES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__SPECIFICATION = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__EDGES = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Semaphores</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__SEMAPHORES = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__IMPORTS = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__PARAMETERS = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Activity</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.impl.ActivityDiagramImpl <em>Activity Diagram</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.impl.ActivityDiagramImpl
	 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getActivityDiagram()
	 * @generated
	 */
	int ACTIVITY_DIAGRAM = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DIAGRAM__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DIAGRAM__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DIAGRAM__UUID = NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Activities</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DIAGRAM__ACTIVITIES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Activity Diagram</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DIAGRAM_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.impl.ImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.impl.ImportImpl
	 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getImport()
	 * @generated
	 */
	int IMPORT = 3;

	/**
	 * The number of structural features of the '<em>Import</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int IMPORT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.impl.ExpressionImportImpl <em>Expression Import</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.impl.ExpressionImportImpl
	 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getExpressionImport()
	 * @generated
	 */
	int EXPRESSION_IMPORT = 4;

	/**
	 * The feature id for the '<em><b>Imported File URI</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_IMPORT__IMPORTED_FILE_URI = IMPORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_IMPORT__EXPRESSION_LANGUAGE = IMPORT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Resolved Imported File URI</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_IMPORT__RESOLVED_IMPORTED_FILE_URI = IMPORT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Expression Import</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_IMPORT_FEATURE_COUNT = IMPORT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.impl.ActivityParameterImpl <em>Activity Parameter</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.impl.ActivityParameterImpl
	 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getActivityParameter()
	 * @generated
	 */
	int ACTIVITY_PARAMETER = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_PARAMETER__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_PARAMETER__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_PARAMETER__UUID = NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_PARAMETER__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_PARAMETER__DIRECTION = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Activity Parameter</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_PARAMETER_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum <em>Activity Parameter Direction Enum</em>}' enum.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum
	 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getActivityParameterDirectionEnum()
	 * @generated
	 */
	int ACTIVITY_PARAMETER_DIRECTION_ENUM = 6;

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.storyDiagramEcore.Activity <em>Activity</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Activity</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.Activity
	 * @generated
	 */
	EClass getActivity();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.Activity#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Nodes</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.Activity#getNodes()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_Nodes();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.Activity#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Specification</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.Activity#getSpecification()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_Specification();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.Activity#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Edges</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.Activity#getEdges()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_Edges();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.Activity#getSemaphores <em>Semaphores</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Semaphores</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.Activity#getSemaphores()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_Semaphores();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.Activity#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imports</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.Activity#getImports()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_Imports();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.Activity#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.Activity#getParameters()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_Parameters();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.storyDiagramEcore.ActivityDiagram
	 * <em>Activity Diagram</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Activity Diagram</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.ActivityDiagram
	 * @generated
	 */
	EClass getActivityDiagram();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.ActivityDiagram#getActivities <em>Activities</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Activities</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.ActivityDiagram#getActivities()
	 * @see #getActivityDiagram()
	 * @generated
	 */
	EReference getActivityDiagram_Activities();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.NamedElement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.NamedElement#getDescription()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Description();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.NamedElement#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.NamedElement#getUuid()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Uuid();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.storyDiagramEcore.Import <em>Import</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Import</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.Import
	 * @generated
	 */
	EClass getImport();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.storyDiagramEcore.ExpressionImport
	 * <em>Expression Import</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Expression Import</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.ExpressionImport
	 * @generated
	 */
	EClass getExpressionImport();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.storyDiagramEcore.ExpressionImport#getImportedFileURI
	 * <em>Imported File URI</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Imported File URI</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.ExpressionImport#getImportedFileURI()
	 * @see #getExpressionImport()
	 * @generated
	 */
	EAttribute getExpressionImport_ImportedFileURI();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.storyDiagramEcore.ExpressionImport#getExpressionLanguage
	 * <em>Expression Language</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Expression Language</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.ExpressionImport#getExpressionLanguage()
	 * @see #getExpressionImport()
	 * @generated
	 */
	EAttribute getExpressionImport_ExpressionLanguage();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.ExpressionImport#getResolvedImportedFileURI <em>Resolved Imported File URI</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Resolved Imported File URI</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.ExpressionImport#getResolvedImportedFileURI()
	 * @see #getExpressionImport()
	 * @generated
	 */
	EAttribute getExpressionImport_ResolvedImportedFileURI();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.storyDiagramEcore.ActivityParameter
	 * <em>Activity Parameter</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Activity Parameter</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.ActivityParameter
	 * @generated
	 */
	EClass getActivityParameter();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.ActivityParameter#getType <em>Type</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.ActivityParameter#getType()
	 * @see #getActivityParameter()
	 * @generated
	 */
	EReference getActivityParameter_Type();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.ActivityParameter#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.ActivityParameter#getDirection()
	 * @see #getActivityParameter()
	 * @generated
	 */
	EAttribute getActivityParameter_Direction();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum <em>Activity Parameter Direction Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Activity Parameter Direction Enum</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum
	 * @generated
	 */
	EEnum getActivityParameterDirectionEnum();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StoryDiagramEcoreFactory getStoryDiagramEcoreFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.storyDiagramEcore.impl.ActivityImpl
		 * <em>Activity</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see de.hpi.sam.storyDiagramEcore.impl.ActivityImpl
		 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getActivity()
		 * @generated
		 */
		EClass ACTIVITY = eINSTANCE.getActivity();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__NODES = eINSTANCE.getActivity_Nodes();

		/**
		 * The meta object literal for the '<em><b>Specification</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__SPECIFICATION = eINSTANCE
				.getActivity_Specification();

		/**
		 * The meta object literal for the '<em><b>Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__EDGES = eINSTANCE.getActivity_Edges();

		/**
		 * The meta object literal for the '<em><b>Semaphores</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__SEMAPHORES = eINSTANCE.getActivity_Semaphores();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__IMPORTS = eINSTANCE.getActivity_Imports();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__PARAMETERS = eINSTANCE.getActivity_Parameters();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.impl.ActivityDiagramImpl <em>Activity Diagram</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.impl.ActivityDiagramImpl
		 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getActivityDiagram()
		 * @generated
		 */
		EClass ACTIVITY_DIAGRAM = eINSTANCE.getActivityDiagram();

		/**
		 * The meta object literal for the '<em><b>Activities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_DIAGRAM__ACTIVITIES = eINSTANCE
				.getActivityDiagram_Activities();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl
		 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__DESCRIPTION = eINSTANCE
				.getNamedElement_Description();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__UUID = eINSTANCE.getNamedElement_Uuid();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.impl.ImportImpl <em>Import</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.impl.ImportImpl
		 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getImport()
		 * @generated
		 */
		EClass IMPORT = eINSTANCE.getImport();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.impl.ExpressionImportImpl <em>Expression Import</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.impl.ExpressionImportImpl
		 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getExpressionImport()
		 * @generated
		 */
		EClass EXPRESSION_IMPORT = eINSTANCE.getExpressionImport();

		/**
		 * The meta object literal for the '<em><b>Imported File URI</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPRESSION_IMPORT__IMPORTED_FILE_URI = eINSTANCE
				.getExpressionImport_ImportedFileURI();

		/**
		 * The meta object literal for the '<em><b>Expression Language</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPRESSION_IMPORT__EXPRESSION_LANGUAGE = eINSTANCE
				.getExpressionImport_ExpressionLanguage();

		/**
		 * The meta object literal for the '
		 * <em><b>Resolved Imported File URI</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute EXPRESSION_IMPORT__RESOLVED_IMPORTED_FILE_URI = eINSTANCE
				.getExpressionImport_ResolvedImportedFileURI();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.impl.ActivityParameterImpl <em>Activity Parameter</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.impl.ActivityParameterImpl
		 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getActivityParameter()
		 * @generated
		 */
		EClass ACTIVITY_PARAMETER = eINSTANCE.getActivityParameter();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_PARAMETER__TYPE = eINSTANCE
				.getActivityParameter_Type();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY_PARAMETER__DIRECTION = eINSTANCE
				.getActivityParameter_Direction();

		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum
		 * <em>Activity Parameter Direction Enum</em>}' enum. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum
		 * @see de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl#getActivityParameterDirectionEnum()
		 * @generated
		 */
		EEnum ACTIVITY_PARAMETER_DIRECTION_ENUM = eINSTANCE
				.getActivityParameterDirectionEnum();

	}

} // StoryDiagramEcorePackage
