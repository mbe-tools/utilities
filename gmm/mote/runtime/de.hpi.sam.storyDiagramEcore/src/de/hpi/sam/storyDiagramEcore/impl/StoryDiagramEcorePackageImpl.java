/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.ActivityDiagram;
import de.hpi.sam.storyDiagramEcore.ActivityParameter;
import de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum;
import de.hpi.sam.storyDiagramEcore.ExpressionImport;
import de.hpi.sam.storyDiagramEcore.Import;
import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreFactory;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage;
import de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsPackageImpl;
import de.hpi.sam.storyDiagramEcore.helpers.HelpersPackage;
import de.hpi.sam.storyDiagramEcore.helpers.impl.HelpersPackageImpl;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class StoryDiagramEcorePackageImpl extends EPackageImpl implements
		StoryDiagramEcorePackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activityEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activityDiagramEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass importEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expressionImportEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activityParameterEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum activityParameterDirectionEnumEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private StoryDiagramEcorePackageImpl() {
		super(eNS_URI, StoryDiagramEcoreFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link StoryDiagramEcorePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static StoryDiagramEcorePackage init() {
		if (isInited)
			return (StoryDiagramEcorePackage) EPackage.Registry.INSTANCE
					.getEPackage(StoryDiagramEcorePackage.eNS_URI);

		// Obtain or create and register package
		StoryDiagramEcorePackageImpl theStoryDiagramEcorePackage = (StoryDiagramEcorePackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof StoryDiagramEcorePackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new StoryDiagramEcorePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		CallActionsPackageImpl theCallActionsPackage = (CallActionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(CallActionsPackage.eNS_URI) instanceof CallActionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(CallActionsPackage.eNS_URI)
				: CallActionsPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI)
				: ExpressionsPackage.eINSTANCE);
		NodesPackageImpl theNodesPackage = (NodesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(NodesPackage.eNS_URI) instanceof NodesPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(NodesPackage.eNS_URI) : NodesPackage.eINSTANCE);
		SdmPackageImpl theSdmPackage = (SdmPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(SdmPackage.eNS_URI) instanceof SdmPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(SdmPackage.eNS_URI) : SdmPackage.eINSTANCE);
		HelpersPackageImpl theHelpersPackage = (HelpersPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(HelpersPackage.eNS_URI) instanceof HelpersPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(HelpersPackage.eNS_URI) : HelpersPackage.eINSTANCE);

		// Create package meta-data objects
		theStoryDiagramEcorePackage.createPackageContents();
		theCallActionsPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theNodesPackage.createPackageContents();
		theSdmPackage.createPackageContents();
		theHelpersPackage.createPackageContents();

		// Initialize created meta-data
		theStoryDiagramEcorePackage.initializePackageContents();
		theCallActionsPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theNodesPackage.initializePackageContents();
		theSdmPackage.initializePackageContents();
		theHelpersPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theStoryDiagramEcorePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(StoryDiagramEcorePackage.eNS_URI,
				theStoryDiagramEcorePackage);
		return theStoryDiagramEcorePackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActivity() {
		return activityEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivity_Nodes() {
		return (EReference) activityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivity_Specification() {
		return (EReference) activityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivity_Edges() {
		return (EReference) activityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivity_Semaphores() {
		return (EReference) activityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivity_Imports() {
		return (EReference) activityEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivity_Parameters() {
		return (EReference) activityEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActivityDiagram() {
		return activityDiagramEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityDiagram_Activities() {
		return (EReference) activityDiagramEClass.getEStructuralFeatures().get(
				0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute) namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Description() {
		return (EAttribute) namedElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Uuid() {
		return (EAttribute) namedElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImport() {
		return importEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpressionImport() {
		return expressionImportEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExpressionImport_ImportedFileURI() {
		return (EAttribute) expressionImportEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExpressionImport_ExpressionLanguage() {
		return (EAttribute) expressionImportEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExpressionImport_ResolvedImportedFileURI() {
		return (EAttribute) expressionImportEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActivityParameter() {
		return activityParameterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityParameter_Type() {
		return (EReference) activityParameterEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivityParameter_Direction() {
		return (EAttribute) activityParameterEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getActivityParameterDirectionEnum() {
		return activityParameterDirectionEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryDiagramEcoreFactory getStoryDiagramEcoreFactory() {
		return (StoryDiagramEcoreFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		activityEClass = createEClass(ACTIVITY);
		createEReference(activityEClass, ACTIVITY__NODES);
		createEReference(activityEClass, ACTIVITY__SPECIFICATION);
		createEReference(activityEClass, ACTIVITY__EDGES);
		createEReference(activityEClass, ACTIVITY__SEMAPHORES);
		createEReference(activityEClass, ACTIVITY__IMPORTS);
		createEReference(activityEClass, ACTIVITY__PARAMETERS);

		activityDiagramEClass = createEClass(ACTIVITY_DIAGRAM);
		createEReference(activityDiagramEClass, ACTIVITY_DIAGRAM__ACTIVITIES);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__DESCRIPTION);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__UUID);

		importEClass = createEClass(IMPORT);

		expressionImportEClass = createEClass(EXPRESSION_IMPORT);
		createEAttribute(expressionImportEClass,
				EXPRESSION_IMPORT__IMPORTED_FILE_URI);
		createEAttribute(expressionImportEClass,
				EXPRESSION_IMPORT__EXPRESSION_LANGUAGE);
		createEAttribute(expressionImportEClass,
				EXPRESSION_IMPORT__RESOLVED_IMPORTED_FILE_URI);

		activityParameterEClass = createEClass(ACTIVITY_PARAMETER);
		createEReference(activityParameterEClass, ACTIVITY_PARAMETER__TYPE);
		createEAttribute(activityParameterEClass, ACTIVITY_PARAMETER__DIRECTION);

		// Create enums
		activityParameterDirectionEnumEEnum = createEEnum(ACTIVITY_PARAMETER_DIRECTION_ENUM);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CallActionsPackage theCallActionsPackage = (CallActionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(CallActionsPackage.eNS_URI);
		ExpressionsPackage theExpressionsPackage = (ExpressionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI);
		NodesPackage theNodesPackage = (NodesPackage) EPackage.Registry.INSTANCE
				.getEPackage(NodesPackage.eNS_URI);
		SdmPackage theSdmPackage = (SdmPackage) EPackage.Registry.INSTANCE
				.getEPackage(SdmPackage.eNS_URI);
		HelpersPackage theHelpersPackage = (HelpersPackage) EPackage.Registry.INSTANCE
				.getEPackage(HelpersPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theCallActionsPackage);
		getESubpackages().add(theExpressionsPackage);
		getESubpackages().add(theNodesPackage);
		getESubpackages().add(theSdmPackage);
		getESubpackages().add(theHelpersPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		activityEClass.getESuperTypes().add(this.getNamedElement());
		activityDiagramEClass.getESuperTypes().add(this.getNamedElement());
		expressionImportEClass.getESuperTypes().add(this.getImport());
		activityParameterEClass.getESuperTypes().add(this.getNamedElement());

		// Initialize classes and features; add operations and parameters
		initEClass(activityEClass, Activity.class, "Activity", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActivity_Nodes(), theNodesPackage.getActivityNode(),
				theNodesPackage.getActivityNode_Activity(), "nodes", null, 0,
				-1, Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getActivity_Specification(),
				ecorePackage.getEOperation(), null, "specification", null, 0,
				1, Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getActivity_Edges(), theNodesPackage.getActivityEdge(),
				theNodesPackage.getActivityEdge_Activity(), "edges", null, 0,
				-1, Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getActivity_Semaphores(),
				theNodesPackage.getSemaphore(), null, "semaphores", null, 0,
				-1, Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getActivity_Imports(), this.getImport(), null,
				"imports", null, 0, -1, Activity.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivity_Parameters(), this.getActivityParameter(),
				null, "parameters", null, 0, -1, Activity.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activityDiagramEClass, ActivityDiagram.class,
				"ActivityDiagram", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActivityDiagram_Activities(), this.getActivity(),
				null, "activities", null, 0, -1, ActivityDiagram.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(),
				"name", null, 0, 1, NamedElement.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedElement_Description(),
				ecorePackage.getEString(), "description", null, 0, 1,
				NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedElement_Uuid(), ecorePackage.getEString(),
				"uuid", null, 1, 1, NamedElement.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(importEClass, Import.class, "Import", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(expressionImportEClass, ExpressionImport.class,
				"ExpressionImport", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExpressionImport_ImportedFileURI(),
				ecorePackage.getEString(), "importedFileURI", null, 1, 1,
				ExpressionImport.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getExpressionImport_ExpressionLanguage(),
				ecorePackage.getEString(), "expressionLanguage", null, 1, 1,
				ExpressionImport.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getExpressionImport_ResolvedImportedFileURI(),
				ecorePackage.getEString(), "resolvedImportedFileURI", null, 0,
				1, ExpressionImport.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);

		initEClass(activityParameterEClass, ActivityParameter.class,
				"ActivityParameter", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActivityParameter_Type(),
				ecorePackage.getEClassifier(), null, "type", null, 1, 1,
				ActivityParameter.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActivityParameter_Direction(),
				this.getActivityParameterDirectionEnum(), "direction", "IN", 1,
				1, ActivityParameter.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(activityParameterDirectionEnumEEnum,
				ActivityParameterDirectionEnum.class,
				"ActivityParameterDirectionEnum");
		addEEnumLiteral(activityParameterDirectionEnumEEnum,
				ActivityParameterDirectionEnum.IN);
		addEEnumLiteral(activityParameterDirectionEnumEEnum,
				ActivityParameterDirectionEnum.OUT);
		addEEnumLiteral(activityParameterDirectionEnumEEnum,
				ActivityParameterDirectionEnum.INOUT);

		// Create resource
		createResource(eNS_URI);
	}

} // StoryDiagramEcorePackageImpl
