/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage
 * @generated
 */
public interface StoryDiagramEcoreFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	StoryDiagramEcoreFactory eINSTANCE = de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcoreFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>Activity</em>'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return a new object of class '<em>Activity</em>'.
	 * @generated
	 */
	Activity createActivity();

	/**
	 * Returns a new object of class '<em>Activity Diagram</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Activity Diagram</em>'.
	 * @generated
	 */
	ActivityDiagram createActivityDiagram();

	/**
	 * Returns a new object of class '<em>Expression Import</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Expression Import</em>'.
	 * @generated
	 */
	ExpressionImport createExpressionImport();

	/**
	 * Returns a new object of class '<em>Activity Parameter</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Activity Parameter</em>'.
	 * @generated
	 */
	ActivityParameter createActivityParameter();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	StoryDiagramEcorePackage getStoryDiagramEcorePackage();

} // StoryDiagramEcoreFactory
