/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

import org.eclipse.emf.common.util.EList;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.NamedElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Activity Node</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is the abstract super class of all ActivityNodes. It holds lists of all incoming and outgoing ActivityEdges and a reference to the containing activity.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getIncoming <em>Incoming</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getOutgoing <em>Outgoing</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getActivity <em>Activity</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityNode()
 * @model abstract="true"
 * @generated
 */
public interface ActivityNode extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Incoming</b></em>' reference list. The
	 * list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge}. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getTarget
	 * <em>Target</em>}'. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> This list contains all ActivityEdges that use this
	 * ActivityNode as target. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Incoming</em>' reference list.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityNode_Incoming()
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<ActivityEdge> getIncoming();

	/**
	 * Returns the value of the '<em><b>Outgoing</b></em>' reference list. The
	 * list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge}. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getSource
	 * <em>Source</em>}'. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> This list contains all ActivityEdges that use this
	 * ActivityNode as source. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Outgoing</em>' reference list.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityNode_Outgoing()
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<ActivityEdge> getOutgoing();

	/**
	 * Returns the value of the '<em><b>Activity</b></em>' container reference.
	 * It is bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.Activity#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The activity that contains this activity node. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Activity</em>' container reference.
	 * @see #setActivity(Activity)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityNode_Activity()
	 * @see de.hpi.sam.storyDiagramEcore.Activity#getNodes
	 * @model opposite="nodes" required="true" transient="false"
	 * @generated
	 */
	Activity getActivity();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getActivity <em>Activity</em>}' container reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Activity</em>' container reference.
	 * @see #getActivity()
	 * @generated
	 */
	void setActivity(Activity value);

} // ActivityNode
