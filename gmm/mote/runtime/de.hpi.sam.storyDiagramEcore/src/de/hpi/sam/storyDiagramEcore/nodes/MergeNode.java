/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Merge Node</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A merge node merge several control flows that were split by a branch node. A merge node may have several incoming edges and must have one outgoing edge.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getMergeNode()
 * @model
 * @generated
 */
public interface MergeNode extends ActivityNode {
} // MergeNode
