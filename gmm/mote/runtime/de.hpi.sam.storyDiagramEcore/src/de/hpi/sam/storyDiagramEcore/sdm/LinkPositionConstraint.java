/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Link Position Constraint</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Constraints the position of the target object of the associated link. The target
 * object must be either the first or last element of the ordered reference, or
 * located at the specified index. If the link is created, the target object is 
 * inserted at the specified position. The indexExpression has to return an 
 * integer that specifies the index.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint#getConstraintType <em>Constraint Type</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint#getStoryPatternLink <em>Story Pattern Link</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getLinkPositionConstraint()
 * @model
 * @generated
 */
public interface LinkPositionConstraint extends EObject {
	/**
	 * Returns the value of the '<em><b>Constraint Type</b></em>' attribute. The
	 * default value is <code>"FIRST"</code>. The literals are from the
	 * enumeration
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration}
	 * . <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The type of the link constraint: first, last, or index. In case of index,
	 * indexExpression has to specify the exact index. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Constraint Type</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration
	 * @see #setConstraintType(LinkPositionConstraintEnumeration)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getLinkPositionConstraint_ConstraintType()
	 * @model default="FIRST"
	 * @generated
	 */
	LinkPositionConstraintEnumeration getConstraintType();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint#getConstraintType <em>Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Constraint Type</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration
	 * @see #getConstraintType()
	 * @generated
	 */
	void setConstraintType(LinkPositionConstraintEnumeration value);

	/**
	 * Returns the value of the '<em><b>Story Pattern Link</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The story pattern link to which this link constraint is associated. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Story Pattern Link</em>' reference.
	 * @see #setStoryPatternLink(StoryPatternLink)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getLinkPositionConstraint_StoryPatternLink()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	StoryPatternLink getStoryPatternLink();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint#getStoryPatternLink <em>Story Pattern Link</em>}' container reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Story Pattern Link</em>' container reference.
	 * @see #getStoryPatternLink()
	 * @generated
	 */
	void setStoryPatternLink(StoryPatternLink value);

} // LinkPositionConstraint
