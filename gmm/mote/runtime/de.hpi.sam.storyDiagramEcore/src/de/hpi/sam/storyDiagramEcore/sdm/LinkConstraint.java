/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import de.hpi.sam.storyDiagramEcore.NamedElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Link Constraint</b></em>'. <!-- end-user-doc -->
 * 
 * 
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getLinkConstraint()
 * @model abstract="true"
 * @generated
 */
public interface LinkConstraint extends NamedElement
{

} // LinkConstraint
