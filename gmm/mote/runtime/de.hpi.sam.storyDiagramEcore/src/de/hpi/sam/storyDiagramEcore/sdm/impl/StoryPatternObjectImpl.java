/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Story Pattern Object</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternObjectImpl#getBindingType <em>Binding Type</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternObjectImpl#getAttributeAssignments <em>Attribute Assignments</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternObjectImpl#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternObjectImpl#getDirectAssignmentExpression <em>Direct Assignment Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StoryPatternObjectImpl extends AbstractStoryPatternObjectImpl
		implements StoryPatternObject {
	/**
	 * The default value of the '{@link #getBindingType() <em>Binding Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBindingType()
	 * @generated
	 * @ordered
	 */
	protected static final BindingTypeEnumeration BINDING_TYPE_EDEFAULT = BindingTypeEnumeration.UNBOUND;

	/**
	 * The cached value of the '{@link #getBindingType() <em>Binding Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBindingType()
	 * @generated
	 * @ordered
	 */
	protected BindingTypeEnumeration bindingType = BINDING_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAttributeAssignments()
	 * <em>Attribute Assignments</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getAttributeAssignments()
	 * @generated
	 * @ordered
	 */
	protected EList<AttributeAssignment> attributeAssignments;

	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> constraints;

	/**
	 * The cached value of the '{@link #getDirectAssignmentExpression()
	 * <em>Direct Assignment Expression</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDirectAssignmentExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression directAssignmentExpression;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected StoryPatternObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SdmPackage.Literals.STORY_PATTERN_OBJECT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public BindingTypeEnumeration getBindingType() {
		return bindingType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setBindingType(BindingTypeEnumeration newBindingType) {
		BindingTypeEnumeration oldBindingType = bindingType;
		bindingType = newBindingType == null ? BINDING_TYPE_EDEFAULT
				: newBindingType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.STORY_PATTERN_OBJECT__BINDING_TYPE,
					oldBindingType, bindingType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeAssignment> getAttributeAssignments() {
		if (attributeAssignments == null) {
			attributeAssignments = new EObjectContainmentEList<AttributeAssignment>(
					AttributeAssignment.class, this,
					SdmPackage.STORY_PATTERN_OBJECT__ATTRIBUTE_ASSIGNMENTS);
		}
		return attributeAssignments;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getConstraints() {
		if (constraints == null) {
			constraints = new EObjectContainmentEList<Expression>(
					Expression.class, this,
					SdmPackage.STORY_PATTERN_OBJECT__CONSTRAINTS);
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getDirectAssignmentExpression() {
		return directAssignmentExpression;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDirectAssignmentExpression(
			Expression newDirectAssignmentExpression, NotificationChain msgs) {
		Expression oldDirectAssignmentExpression = directAssignmentExpression;
		directAssignmentExpression = newDirectAssignmentExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
					this,
					Notification.SET,
					SdmPackage.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION,
					oldDirectAssignmentExpression,
					newDirectAssignmentExpression);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirectAssignmentExpression(
			Expression newDirectAssignmentExpression) {
		if (newDirectAssignmentExpression != directAssignmentExpression) {
			NotificationChain msgs = null;
			if (directAssignmentExpression != null)
				msgs = ((InternalEObject) directAssignmentExpression)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- SdmPackage.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION,
								null, msgs);
			if (newDirectAssignmentExpression != null)
				msgs = ((InternalEObject) newDirectAssignmentExpression)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- SdmPackage.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION,
								null, msgs);
			msgs = basicSetDirectAssignmentExpression(
					newDirectAssignmentExpression, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					SdmPackage.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION,
					newDirectAssignmentExpression,
					newDirectAssignmentExpression));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_OBJECT__ATTRIBUTE_ASSIGNMENTS:
			return ((InternalEList<?>) getAttributeAssignments()).basicRemove(
					otherEnd, msgs);
		case SdmPackage.STORY_PATTERN_OBJECT__CONSTRAINTS:
			return ((InternalEList<?>) getConstraints()).basicRemove(otherEnd,
					msgs);
		case SdmPackage.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION:
			return basicSetDirectAssignmentExpression(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_OBJECT__BINDING_TYPE:
			return getBindingType();
		case SdmPackage.STORY_PATTERN_OBJECT__ATTRIBUTE_ASSIGNMENTS:
			return getAttributeAssignments();
		case SdmPackage.STORY_PATTERN_OBJECT__CONSTRAINTS:
			return getConstraints();
		case SdmPackage.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION:
			return getDirectAssignmentExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_OBJECT__BINDING_TYPE:
			setBindingType((BindingTypeEnumeration) newValue);
			return;
		case SdmPackage.STORY_PATTERN_OBJECT__ATTRIBUTE_ASSIGNMENTS:
			getAttributeAssignments().clear();
			getAttributeAssignments().addAll(
					(Collection<? extends AttributeAssignment>) newValue);
			return;
		case SdmPackage.STORY_PATTERN_OBJECT__CONSTRAINTS:
			getConstraints().clear();
			getConstraints()
					.addAll((Collection<? extends Expression>) newValue);
			return;
		case SdmPackage.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION:
			setDirectAssignmentExpression((Expression) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_OBJECT__BINDING_TYPE:
			setBindingType(BINDING_TYPE_EDEFAULT);
			return;
		case SdmPackage.STORY_PATTERN_OBJECT__ATTRIBUTE_ASSIGNMENTS:
			getAttributeAssignments().clear();
			return;
		case SdmPackage.STORY_PATTERN_OBJECT__CONSTRAINTS:
			getConstraints().clear();
			return;
		case SdmPackage.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION:
			setDirectAssignmentExpression((Expression) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_OBJECT__BINDING_TYPE:
			return bindingType != BINDING_TYPE_EDEFAULT;
		case SdmPackage.STORY_PATTERN_OBJECT__ATTRIBUTE_ASSIGNMENTS:
			return attributeAssignments != null
					&& !attributeAssignments.isEmpty();
		case SdmPackage.STORY_PATTERN_OBJECT__CONSTRAINTS:
			return constraints != null && !constraints.isEmpty();
		case SdmPackage.STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION:
			return directAssignmentExpression != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (bindingType: ");
		result.append(bindingType);
		result.append(')');
		return result.toString();
	}

} // StoryPatternObjectImpl
