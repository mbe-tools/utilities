/**
 */
package de.hpi.sam.storyDiagramEcore.callActions.impl;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Get Property Value Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.GetPropertyValueActionImpl#getInstanceVariable <em>Instance Variable</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.GetPropertyValueActionImpl#getProperty <em>Property</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GetPropertyValueActionImpl extends CallActionImpl implements
		GetPropertyValueAction {
	/**
	 * The cached value of the '{@link #getInstanceVariable() <em>Instance Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceVariable()
	 * @generated
	 * @ordered
	 */
	protected Expression instanceVariable;

	/**
	 * The cached value of the '{@link #getProperty() <em>Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature property;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GetPropertyValueActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CallActionsPackage.Literals.GET_PROPERTY_VALUE_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getInstanceVariable() {
		return instanceVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInstanceVariable(
			Expression newInstanceVariable, NotificationChain msgs) {
		Expression oldInstanceVariable = instanceVariable;
		instanceVariable = newInstanceVariable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
					this,
					Notification.SET,
					CallActionsPackage.GET_PROPERTY_VALUE_ACTION__INSTANCE_VARIABLE,
					oldInstanceVariable, newInstanceVariable);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstanceVariable(Expression newInstanceVariable) {
		if (newInstanceVariable != instanceVariable) {
			NotificationChain msgs = null;
			if (instanceVariable != null)
				msgs = ((InternalEObject) instanceVariable)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.GET_PROPERTY_VALUE_ACTION__INSTANCE_VARIABLE,
								null, msgs);
			if (newInstanceVariable != null)
				msgs = ((InternalEObject) newInstanceVariable)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.GET_PROPERTY_VALUE_ACTION__INSTANCE_VARIABLE,
								null, msgs);
			msgs = basicSetInstanceVariable(newInstanceVariable, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					CallActionsPackage.GET_PROPERTY_VALUE_ACTION__INSTANCE_VARIABLE,
					newInstanceVariable, newInstanceVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature getProperty() {
		if (property != null && property.eIsProxy()) {
			InternalEObject oldProperty = (InternalEObject) property;
			property = (EStructuralFeature) eResolveProxy(oldProperty);
			if (property != oldProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							CallActionsPackage.GET_PROPERTY_VALUE_ACTION__PROPERTY,
							oldProperty, property));
			}
		}
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature basicGetProperty() {
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProperty(EStructuralFeature newProperty) {
		EStructuralFeature oldProperty = property;
		property = newProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CallActionsPackage.GET_PROPERTY_VALUE_ACTION__PROPERTY,
					oldProperty, property));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CallActionsPackage.GET_PROPERTY_VALUE_ACTION__INSTANCE_VARIABLE:
			return basicSetInstanceVariable(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CallActionsPackage.GET_PROPERTY_VALUE_ACTION__INSTANCE_VARIABLE:
			return getInstanceVariable();
		case CallActionsPackage.GET_PROPERTY_VALUE_ACTION__PROPERTY:
			if (resolve)
				return getProperty();
			return basicGetProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CallActionsPackage.GET_PROPERTY_VALUE_ACTION__INSTANCE_VARIABLE:
			setInstanceVariable((Expression) newValue);
			return;
		case CallActionsPackage.GET_PROPERTY_VALUE_ACTION__PROPERTY:
			setProperty((EStructuralFeature) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CallActionsPackage.GET_PROPERTY_VALUE_ACTION__INSTANCE_VARIABLE:
			setInstanceVariable((Expression) null);
			return;
		case CallActionsPackage.GET_PROPERTY_VALUE_ACTION__PROPERTY:
			setProperty((EStructuralFeature) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CallActionsPackage.GET_PROPERTY_VALUE_ACTION__INSTANCE_VARIABLE:
			return instanceVariable != null;
		case CallActionsPackage.GET_PROPERTY_VALUE_ACTION__PROPERTY:
			return property != null;
		}
		return super.eIsSet(featureID);
	}

} //GetPropertyValueActionImpl
