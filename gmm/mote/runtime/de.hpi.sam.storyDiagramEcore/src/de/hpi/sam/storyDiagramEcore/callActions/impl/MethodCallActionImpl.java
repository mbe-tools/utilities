/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Method Call Action</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.MethodCallActionImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.MethodCallActionImpl#getMethod <em>Method</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.MethodCallActionImpl#getMethodClassName <em>Method Class Name</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.MethodCallActionImpl#getMethodName <em>Method Name</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.MethodCallActionImpl#getInstanceVariable <em>Instance Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MethodCallActionImpl extends CallActionImpl implements
		MethodCallAction {
	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<CallActionParameter> parameters;

	/**
	 * The cached value of the '{@link #getMethod() <em>Method</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMethod()
	 * @generated
	 * @ordered
	 */
	protected EOperation method;

	/**
	 * The default value of the '{@link #getMethodClassName() <em>Method Class Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getMethodClassName()
	 * @generated
	 * @ordered
	 */
	protected static final String METHOD_CLASS_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMethodClassName() <em>Method Class Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getMethodClassName()
	 * @generated
	 * @ordered
	 */
	protected String methodClassName = METHOD_CLASS_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMethodName() <em>Method Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMethodName()
	 * @generated
	 * @ordered
	 */
	protected static final String METHOD_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMethodName() <em>Method Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMethodName()
	 * @generated
	 * @ordered
	 */
	protected String methodName = METHOD_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInstanceVariable() <em>Instance Variable</em>}' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getInstanceVariable()
	 * @generated
	 * @ordered
	 */
	protected Expression instanceVariable;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MethodCallActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CallActionsPackage.Literals.METHOD_CALL_ACTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CallActionParameter> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<CallActionParameter>(
					CallActionParameter.class, this,
					CallActionsPackage.METHOD_CALL_ACTION__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMethod() {
		if (method != null && method.eIsProxy()) {
			InternalEObject oldMethod = (InternalEObject) method;
			method = (EOperation) eResolveProxy(oldMethod);
			if (method != oldMethod) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							CallActionsPackage.METHOD_CALL_ACTION__METHOD,
							oldMethod, method));
			}
		}
		return method;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation basicGetMethod() {
		return method;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethod(EOperation newMethod) {
		EOperation oldMethod = method;
		method = newMethod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CallActionsPackage.METHOD_CALL_ACTION__METHOD, oldMethod,
					method));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getMethodClassName() {
		return methodClassName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethodClassName(String newMethodClassName) {
		String oldMethodClassName = methodClassName;
		methodClassName = newMethodClassName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CallActionsPackage.METHOD_CALL_ACTION__METHOD_CLASS_NAME,
					oldMethodClassName, methodClassName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethodName(String newMethodName) {
		String oldMethodName = methodName;
		methodName = newMethodName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CallActionsPackage.METHOD_CALL_ACTION__METHOD_NAME,
					oldMethodName, methodName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getInstanceVariable() {
		return instanceVariable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInstanceVariable(
			Expression newInstanceVariable, NotificationChain msgs) {
		Expression oldInstanceVariable = instanceVariable;
		instanceVariable = newInstanceVariable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					CallActionsPackage.METHOD_CALL_ACTION__INSTANCE_VARIABLE,
					oldInstanceVariable, newInstanceVariable);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstanceVariable(Expression newInstanceVariable) {
		if (newInstanceVariable != instanceVariable) {
			NotificationChain msgs = null;
			if (instanceVariable != null)
				msgs = ((InternalEObject) instanceVariable)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.METHOD_CALL_ACTION__INSTANCE_VARIABLE,
								null, msgs);
			if (newInstanceVariable != null)
				msgs = ((InternalEObject) newInstanceVariable)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.METHOD_CALL_ACTION__INSTANCE_VARIABLE,
								null, msgs);
			msgs = basicSetInstanceVariable(newInstanceVariable, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CallActionsPackage.METHOD_CALL_ACTION__INSTANCE_VARIABLE,
					newInstanceVariable, newInstanceVariable));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CallActionsPackage.METHOD_CALL_ACTION__PARAMETERS:
			return ((InternalEList<?>) getParameters()).basicRemove(otherEnd,
					msgs);
		case CallActionsPackage.METHOD_CALL_ACTION__INSTANCE_VARIABLE:
			return basicSetInstanceVariable(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CallActionsPackage.METHOD_CALL_ACTION__PARAMETERS:
			return getParameters();
		case CallActionsPackage.METHOD_CALL_ACTION__METHOD:
			if (resolve)
				return getMethod();
			return basicGetMethod();
		case CallActionsPackage.METHOD_CALL_ACTION__METHOD_CLASS_NAME:
			return getMethodClassName();
		case CallActionsPackage.METHOD_CALL_ACTION__METHOD_NAME:
			return getMethodName();
		case CallActionsPackage.METHOD_CALL_ACTION__INSTANCE_VARIABLE:
			return getInstanceVariable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CallActionsPackage.METHOD_CALL_ACTION__PARAMETERS:
			getParameters().clear();
			getParameters().addAll(
					(Collection<? extends CallActionParameter>) newValue);
			return;
		case CallActionsPackage.METHOD_CALL_ACTION__METHOD:
			setMethod((EOperation) newValue);
			return;
		case CallActionsPackage.METHOD_CALL_ACTION__METHOD_CLASS_NAME:
			setMethodClassName((String) newValue);
			return;
		case CallActionsPackage.METHOD_CALL_ACTION__METHOD_NAME:
			setMethodName((String) newValue);
			return;
		case CallActionsPackage.METHOD_CALL_ACTION__INSTANCE_VARIABLE:
			setInstanceVariable((Expression) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CallActionsPackage.METHOD_CALL_ACTION__PARAMETERS:
			getParameters().clear();
			return;
		case CallActionsPackage.METHOD_CALL_ACTION__METHOD:
			setMethod((EOperation) null);
			return;
		case CallActionsPackage.METHOD_CALL_ACTION__METHOD_CLASS_NAME:
			setMethodClassName(METHOD_CLASS_NAME_EDEFAULT);
			return;
		case CallActionsPackage.METHOD_CALL_ACTION__METHOD_NAME:
			setMethodName(METHOD_NAME_EDEFAULT);
			return;
		case CallActionsPackage.METHOD_CALL_ACTION__INSTANCE_VARIABLE:
			setInstanceVariable((Expression) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CallActionsPackage.METHOD_CALL_ACTION__PARAMETERS:
			return parameters != null && !parameters.isEmpty();
		case CallActionsPackage.METHOD_CALL_ACTION__METHOD:
			return method != null;
		case CallActionsPackage.METHOD_CALL_ACTION__METHOD_CLASS_NAME:
			return METHOD_CLASS_NAME_EDEFAULT == null ? methodClassName != null
					: !METHOD_CLASS_NAME_EDEFAULT.equals(methodClassName);
		case CallActionsPackage.METHOD_CALL_ACTION__METHOD_NAME:
			return METHOD_NAME_EDEFAULT == null ? methodName != null
					: !METHOD_NAME_EDEFAULT.equals(methodName);
		case CallActionsPackage.METHOD_CALL_ACTION__INSTANCE_VARIABLE:
			return instanceVariable != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer buffer = new StringBuffer();

		if (getInstanceVariable() != null) {
			buffer.append(getInstanceVariable().toString());
		} else {
			//This may be a static method call
			if (getMethod() != null
					&& getMethod().getEContainingClass() != null) {
				buffer.append(getMethod().getEContainingClass().getName());
			} else if (getMethodClassName() != null
					&& !"".equals(getMethodClassName())) {
				buffer.append(getMethodClassName());
			} else {
				buffer.append("[null]");
			}
		}

		buffer.append(".");

		if (getMethod() != null) {
			if (getMethod().getName() != null
					&& !"".equals(getMethod().getName())) {
				buffer.append(getMethod().getName());
			} else {
				buffer.append("[null]");
			}
		} else {
			if (getMethodName() != null && !"".equals(getMethodName())) {
				buffer.append(getMethodName());
			} else {
				buffer.append("[null]");
			}
		}

		buffer.append("(");

		if (!getParameters().isEmpty()) {
			for (CallActionParameter param : getParameters()) {
				buffer.append(param.toString());

				buffer.append(", ");
			}

			buffer.delete(buffer.length() - 2, buffer.length());
		}

		buffer.append(")");

		return buffer.toString().trim();
	}

} // MethodCallActionImpl
