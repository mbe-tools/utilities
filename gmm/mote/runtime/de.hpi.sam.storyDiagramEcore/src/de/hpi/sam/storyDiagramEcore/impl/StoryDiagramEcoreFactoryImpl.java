/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.ActivityDiagram;
import de.hpi.sam.storyDiagramEcore.ActivityParameter;
import de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum;
import de.hpi.sam.storyDiagramEcore.ExpressionImport;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreFactory;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class StoryDiagramEcoreFactoryImpl extends EFactoryImpl implements
		StoryDiagramEcoreFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static StoryDiagramEcoreFactory init() {
		try {
			StoryDiagramEcoreFactory theStoryDiagramEcoreFactory = (StoryDiagramEcoreFactory) EPackage.Registry.INSTANCE
					.getEFactory(StoryDiagramEcorePackage.eNS_URI);
			if (theStoryDiagramEcoreFactory != null) {
				return theStoryDiagramEcoreFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StoryDiagramEcoreFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public StoryDiagramEcoreFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case StoryDiagramEcorePackage.ACTIVITY:
			return createActivity();
		case StoryDiagramEcorePackage.ACTIVITY_DIAGRAM:
			return createActivityDiagram();
		case StoryDiagramEcorePackage.EXPRESSION_IMPORT:
			return createExpressionImport();
		case StoryDiagramEcorePackage.ACTIVITY_PARAMETER:
			return createActivityParameter();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case StoryDiagramEcorePackage.ACTIVITY_PARAMETER_DIRECTION_ENUM:
			return createActivityParameterDirectionEnumFromString(eDataType,
					initialValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case StoryDiagramEcorePackage.ACTIVITY_PARAMETER_DIRECTION_ENUM:
			return convertActivityParameterDirectionEnumToString(eDataType,
					instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Activity createActivity() {
		ActivityImpl activity = new ActivityImpl();
		return activity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDiagram createActivityDiagram() {
		ActivityDiagramImpl activityDiagram = new ActivityDiagramImpl();
		return activityDiagram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionImport createExpressionImport() {
		ExpressionImportImpl expressionImport = new ExpressionImportImpl();
		return expressionImport;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityParameter createActivityParameter() {
		ActivityParameterImpl activityParameter = new ActivityParameterImpl();
		return activityParameter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityParameterDirectionEnum createActivityParameterDirectionEnumFromString(
			EDataType eDataType, String initialValue) {
		ActivityParameterDirectionEnum result = ActivityParameterDirectionEnum
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActivityParameterDirectionEnumToString(
			EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryDiagramEcorePackage getStoryDiagramEcorePackage() {
		return (StoryDiagramEcorePackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StoryDiagramEcorePackage getPackage() {
		return StoryDiagramEcorePackage.eINSTANCE;
	}

} // StoryDiagramEcoreFactoryImpl
