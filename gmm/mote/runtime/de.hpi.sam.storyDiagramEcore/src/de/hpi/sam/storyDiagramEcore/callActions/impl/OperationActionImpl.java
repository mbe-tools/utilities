/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.OperationAction;
import de.hpi.sam.storyDiagramEcore.callActions.Operators;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Operation Action</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.OperationActionImpl#getOperand1 <em>Operand1</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.OperationActionImpl#getOperand2 <em>Operand2</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.OperationActionImpl#getOperator <em>Operator</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OperationActionImpl extends CallActionImpl implements
		OperationAction {
	/**
	 * The cached value of the '{@link #getOperand1() <em>Operand1</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOperand1()
	 * @generated
	 * @ordered
	 */
	protected Expression operand1;

	/**
	 * The cached value of the '{@link #getOperand2() <em>Operand2</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOperand2()
	 * @generated
	 * @ordered
	 */
	protected Expression operand2;

	/**
	 * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected static final Operators OPERATOR_EDEFAULT = Operators.EQUALS;

	/**
	 * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected Operators operator = OPERATOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CallActionsPackage.Literals.OPERATION_ACTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getOperand1() {
		return operand1;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperand1(Expression newOperand1,
			NotificationChain msgs) {
		Expression oldOperand1 = operand1;
		operand1 = newOperand1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					CallActionsPackage.OPERATION_ACTION__OPERAND1, oldOperand1,
					newOperand1);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperand1(Expression newOperand1) {
		if (newOperand1 != operand1) {
			NotificationChain msgs = null;
			if (operand1 != null)
				msgs = ((InternalEObject) operand1)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.OPERATION_ACTION__OPERAND1,
								null, msgs);
			if (newOperand1 != null)
				msgs = ((InternalEObject) newOperand1)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.OPERATION_ACTION__OPERAND1,
								null, msgs);
			msgs = basicSetOperand1(newOperand1, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CallActionsPackage.OPERATION_ACTION__OPERAND1, newOperand1,
					newOperand1));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getOperand2() {
		return operand2;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperand2(Expression newOperand2,
			NotificationChain msgs) {
		Expression oldOperand2 = operand2;
		operand2 = newOperand2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					CallActionsPackage.OPERATION_ACTION__OPERAND2, oldOperand2,
					newOperand2);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperand2(Expression newOperand2) {
		if (newOperand2 != operand2) {
			NotificationChain msgs = null;
			if (operand2 != null)
				msgs = ((InternalEObject) operand2)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.OPERATION_ACTION__OPERAND2,
								null, msgs);
			if (newOperand2 != null)
				msgs = ((InternalEObject) newOperand2)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.OPERATION_ACTION__OPERAND2,
								null, msgs);
			msgs = basicSetOperand2(newOperand2, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CallActionsPackage.OPERATION_ACTION__OPERAND2, newOperand2,
					newOperand2));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Operators getOperator() {
		return operator;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperator(Operators newOperator) {
		Operators oldOperator = operator;
		operator = newOperator == null ? OPERATOR_EDEFAULT : newOperator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CallActionsPackage.OPERATION_ACTION__OPERATOR, oldOperator,
					operator));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer buffer = new StringBuffer();

		if (this.getOperator() == Operators.NOT) {
			buffer.append(this.getOperator().toString());
		}

		if (this.getOperand1() != null) {
			buffer.append(this.getOperand1().toString());
		} else {
			buffer.append("[null]");
		}

		if (this.getOperator() != Operators.NOT) {
			buffer.append(" ");
			buffer.append(this.getOperator().toString());
			buffer.append(" ");

			if (this.getOperand2() != null) {
				buffer.append(this.getOperand2().toString());
			} else {
				buffer.append("[null]");
			}
		}
		return buffer.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CallActionsPackage.OPERATION_ACTION__OPERAND1:
			return basicSetOperand1(null, msgs);
		case CallActionsPackage.OPERATION_ACTION__OPERAND2:
			return basicSetOperand2(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CallActionsPackage.OPERATION_ACTION__OPERAND1:
			return getOperand1();
		case CallActionsPackage.OPERATION_ACTION__OPERAND2:
			return getOperand2();
		case CallActionsPackage.OPERATION_ACTION__OPERATOR:
			return getOperator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CallActionsPackage.OPERATION_ACTION__OPERAND1:
			setOperand1((Expression) newValue);
			return;
		case CallActionsPackage.OPERATION_ACTION__OPERAND2:
			setOperand2((Expression) newValue);
			return;
		case CallActionsPackage.OPERATION_ACTION__OPERATOR:
			setOperator((Operators) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CallActionsPackage.OPERATION_ACTION__OPERAND1:
			setOperand1((Expression) null);
			return;
		case CallActionsPackage.OPERATION_ACTION__OPERAND2:
			setOperand2((Expression) null);
			return;
		case CallActionsPackage.OPERATION_ACTION__OPERATOR:
			setOperator(OPERATOR_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CallActionsPackage.OPERATION_ACTION__OPERAND1:
			return operand1 != null;
		case CallActionsPackage.OPERATION_ACTION__OPERAND2:
			return operand2 != null;
		case CallActionsPackage.OPERATION_ACTION__OPERATOR:
			return operator != OPERATOR_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} // OperationActionImpl
