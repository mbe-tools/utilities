/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.callActions.AbstractComparator;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Attribute Assignment</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The attribute assignment contains an assignment expression that is evaluated and the resulting value is assigned to the attribute.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment#getEStructuralFeature <em>EStructural Feature</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment#getAssignmentExpression <em>Assignment Expression</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment#getCustomComparator <em>Custom Comparator</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAttributeAssignment()
 * @model
 * @generated
 */
public interface AttributeAssignment extends NamedElement {
	/**
	 * Returns the value of the '<em><b>EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The EAttribute in the meta model. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>EStructural Feature</em>' reference.
	 * @see #setEStructuralFeature(EStructuralFeature)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAttributeAssignment_EStructuralFeature()
	 * @model required="true"
	 * @generated
	 */
	EStructuralFeature getEStructuralFeature();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment#getEStructuralFeature <em>EStructural Feature</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>EStructural Feature</em>' reference.
	 * @see #getEStructuralFeature()
	 * @generated
	 */
	void setEStructuralFeature(EStructuralFeature value);

	/**
	 * Returns the value of the '<em><b>Assignment Expression</b></em>'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> The assignment expression that is evaluated to
	 * compute a value for this attribute. The expression must return a value of
	 * the type of this attribute. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Assignment Expression</em>' containment
	 *         reference.
	 * @see #setAssignmentExpression(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAttributeAssignment_AssignmentExpression()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getAssignmentExpression();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment#getAssignmentExpression
	 * <em>Assignment Expression</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Assignment Expression</em>'
	 *            containment reference.
	 * @see #getAssignmentExpression()
	 * @generated
	 */
	void setAssignmentExpression(Expression value);

	/**
	 * Returns the value of the '<em><b>Custom Comparator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Custom Comparator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Custom Comparator</em>' containment reference.
	 * @see #setCustomComparator(AbstractComparator)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAttributeAssignment_CustomComparator()
	 * @model containment="true"
	 * @generated
	 */
	AbstractComparator getCustomComparator();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment#getCustomComparator <em>Custom Comparator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Custom Comparator</em>' containment reference.
	 * @see #getCustomComparator()
	 * @generated
	 */
	void setCustomComparator(AbstractComparator value);

} // AttributeAssignment
