/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Story Pattern Expression Link</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The StoryPatternExpressionLink can be used to match the target story pattern object to the return value of an expression. This implies, that that StoryPatternObject may not be modified, i.e. its modifier must be NONE.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternExpressionLink()
 * @model
 * @generated
 */
public interface StoryPatternExpressionLink extends AbstractStoryPatternLink {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> The EOperation that is executed to yield the target
	 * story pattern object. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #setExpression(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternExpressionLink_Expression()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getExpression();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(Expression value);

} // StoryPatternExpressionLink
