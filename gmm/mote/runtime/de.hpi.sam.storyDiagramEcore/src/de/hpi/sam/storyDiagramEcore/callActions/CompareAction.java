/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Compare Action</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The CompareAction compares the results of two Expressions and returns true if both Expressions yield the same value.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.CompareAction#getExpression1 <em>Expression1</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.CompareAction#getExpression2 <em>Expression2</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.CompareAction#getComparator <em>Comparator</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCompareAction()
 * @model
 * @generated
 */
public interface CompareAction extends CallAction {
	/**
	 * Returns the value of the '<em><b>Expression1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression1</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression1</em>' containment reference.
	 * @see #setExpression1(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCompareAction_Expression1()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getExpression1();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.CompareAction#getExpression1 <em>Expression1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression1</em>' containment reference.
	 * @see #getExpression1()
	 * @generated
	 */
	void setExpression1(Expression value);

	/**
	 * Returns the value of the '<em><b>Expression2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression2</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression2</em>' containment reference.
	 * @see #setExpression2(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCompareAction_Expression2()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getExpression2();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.CompareAction#getExpression2 <em>Expression2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression2</em>' containment reference.
	 * @see #getExpression2()
	 * @generated
	 */
	void setExpression2(Expression value);

	/**
	 * Returns the value of the '<em><b>Comparator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comparator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comparator</em>' containment reference.
	 * @see #setComparator(AbstractComparator)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCompareAction_Comparator()
	 * @model containment="true"
	 * @generated
	 */
	AbstractComparator getComparator();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.CompareAction#getComparator <em>Comparator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comparator</em>' containment reference.
	 * @see #getComparator()
	 * @generated
	 */
	void setComparator(AbstractComparator value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (eIsProxy()) return super.toString();\r\n\r\nStringBuffer buffer = new StringBuffer();\r\n\r\nif (getExpression1() != null)\r\n{\r\n\tbuffer.append(getExpression1().toString());\r\n}\r\nelse\r\n{\r\n\tbuffer.append(\"[null]\");\r\n}\r\n\r\nbuffer.append(\" = \");\r\n\r\nif (getExpression2() != null)\r\n{\r\n\tbuffer.append(getExpression2().toString());\r\n}\r\nelse\r\n{\r\n\tbuffer.append(\"[null]\");\r\n}\r\n      \r\nreturn buffer.toString();'"
	 * @generated
	 */
	String toString();

} // CompareAction
