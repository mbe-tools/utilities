/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Abstract Story Pattern Link</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternLinkImpl#getSource <em>Source</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternLinkImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternLinkImpl#isCheckOnlyExistence <em>Check Only Existence</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternLinkImpl#getMatchingPriority <em>Matching Priority</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternLinkImpl#getOppositeStoryPatternLink <em>Opposite Story Pattern Link</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class AbstractStoryPatternLinkImpl extends
		StoryPatternElementImpl implements AbstractStoryPatternLink {
	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected AbstractStoryPatternObject source;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected AbstractStoryPatternObject target;

	/**
	 * The default value of the '{@link #isCheckOnlyExistence() <em>Check Only Existence</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isCheckOnlyExistence()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CHECK_ONLY_EXISTENCE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCheckOnlyExistence() <em>Check Only Existence</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isCheckOnlyExistence()
	 * @generated
	 * @ordered
	 */
	protected boolean checkOnlyExistence = CHECK_ONLY_EXISTENCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMatchingPriority() <em>Matching Priority</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getMatchingPriority()
	 * @generated
	 * @ordered
	 */
	protected static final int MATCHING_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMatchingPriority() <em>Matching Priority</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getMatchingPriority()
	 * @generated
	 * @ordered
	 */
	protected int matchingPriority = MATCHING_PRIORITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOppositeStoryPatternLink() <em>Opposite Story Pattern Link</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOppositeStoryPatternLink()
	 * @generated
	 * @ordered
	 */
	protected AbstractStoryPatternLink oppositeStoryPatternLink;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractStoryPatternLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SdmPackage.Literals.ABSTRACT_STORY_PATTERN_LINK;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractStoryPatternObject getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject) source;
			source = (AbstractStoryPatternObject) eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SdmPackage.ABSTRACT_STORY_PATTERN_LINK__SOURCE,
							oldSource, source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractStoryPatternObject basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSource(
			AbstractStoryPatternObject newSource, NotificationChain msgs) {
		AbstractStoryPatternObject oldSource = source;
		source = newSource;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					SdmPackage.ABSTRACT_STORY_PATTERN_LINK__SOURCE, oldSource,
					newSource);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(AbstractStoryPatternObject newSource) {
		if (newSource != source) {
			NotificationChain msgs = null;
			if (source != null)
				msgs = ((InternalEObject) source)
						.eInverseRemove(
								this,
								SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS,
								AbstractStoryPatternObject.class, msgs);
			if (newSource != null)
				msgs = ((InternalEObject) newSource)
						.eInverseAdd(
								this,
								SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS,
								AbstractStoryPatternObject.class, msgs);
			msgs = basicSetSource(newSource, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.ABSTRACT_STORY_PATTERN_LINK__SOURCE, newSource,
					newSource));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractStoryPatternObject getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject) target;
			target = (AbstractStoryPatternObject) eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SdmPackage.ABSTRACT_STORY_PATTERN_LINK__TARGET,
							oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractStoryPatternObject basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTarget(
			AbstractStoryPatternObject newTarget, NotificationChain msgs) {
		AbstractStoryPatternObject oldTarget = target;
		target = newTarget;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					SdmPackage.ABSTRACT_STORY_PATTERN_LINK__TARGET, oldTarget,
					newTarget);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(AbstractStoryPatternObject newTarget) {
		if (newTarget != target) {
			NotificationChain msgs = null;
			if (target != null)
				msgs = ((InternalEObject) target)
						.eInverseRemove(
								this,
								SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS,
								AbstractStoryPatternObject.class, msgs);
			if (newTarget != null)
				msgs = ((InternalEObject) newTarget)
						.eInverseAdd(
								this,
								SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS,
								AbstractStoryPatternObject.class, msgs);
			msgs = basicSetTarget(newTarget, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.ABSTRACT_STORY_PATTERN_LINK__TARGET, newTarget,
					newTarget));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCheckOnlyExistence() {
		return checkOnlyExistence;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckOnlyExistence(boolean newCheckOnlyExistence) {
		boolean oldCheckOnlyExistence = checkOnlyExistence;
		checkOnlyExistence = newCheckOnlyExistence;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					SdmPackage.ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE,
					oldCheckOnlyExistence, checkOnlyExistence));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getMatchingPriority() {
		return matchingPriority;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMatchingPriority(int newMatchingPriority) {
		int oldMatchingPriority = matchingPriority;
		matchingPriority = newMatchingPriority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY,
					oldMatchingPriority, matchingPriority));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractStoryPatternLink getOppositeStoryPatternLink() {
		if (oppositeStoryPatternLink != null
				&& oppositeStoryPatternLink.eIsProxy()) {
			InternalEObject oldOppositeStoryPatternLink = (InternalEObject) oppositeStoryPatternLink;
			oppositeStoryPatternLink = (AbstractStoryPatternLink) eResolveProxy(oldOppositeStoryPatternLink);
			if (oppositeStoryPatternLink != oldOppositeStoryPatternLink) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							SdmPackage.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK,
							oldOppositeStoryPatternLink,
							oppositeStoryPatternLink));
			}
		}
		return oppositeStoryPatternLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractStoryPatternLink basicGetOppositeStoryPatternLink() {
		return oppositeStoryPatternLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOppositeStoryPatternLink(
			AbstractStoryPatternLink newOppositeStoryPatternLink,
			NotificationChain msgs) {
		AbstractStoryPatternLink oldOppositeStoryPatternLink = oppositeStoryPatternLink;
		oppositeStoryPatternLink = newOppositeStoryPatternLink;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
					this,
					Notification.SET,
					SdmPackage.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK,
					oldOppositeStoryPatternLink, newOppositeStoryPatternLink);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setOppositeStoryPatternLink(
			AbstractStoryPatternLink newOppositeStoryPatternLink) {
		if (newOppositeStoryPatternLink != oppositeStoryPatternLink) {
			NotificationChain msgs = null;
			if (oppositeStoryPatternLink != null)
				msgs = ((InternalEObject) oppositeStoryPatternLink)
						.eInverseRemove(
								this,
								SdmPackage.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK,
								AbstractStoryPatternLink.class, msgs);
			if (newOppositeStoryPatternLink != null)
				msgs = ((InternalEObject) newOppositeStoryPatternLink)
						.eInverseAdd(
								this,
								SdmPackage.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK,
								AbstractStoryPatternLink.class, msgs);
			msgs = basicSetOppositeStoryPatternLink(
					newOppositeStoryPatternLink, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					SdmPackage.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK,
					newOppositeStoryPatternLink, newOppositeStoryPatternLink));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__SOURCE:
			if (source != null)
				msgs = ((InternalEObject) source)
						.eInverseRemove(
								this,
								SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS,
								AbstractStoryPatternObject.class, msgs);
			return basicSetSource((AbstractStoryPatternObject) otherEnd, msgs);
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__TARGET:
			if (target != null)
				msgs = ((InternalEObject) target)
						.eInverseRemove(
								this,
								SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS,
								AbstractStoryPatternObject.class, msgs);
			return basicSetTarget((AbstractStoryPatternObject) otherEnd, msgs);
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK:
			if (oppositeStoryPatternLink != null)
				msgs = ((InternalEObject) oppositeStoryPatternLink)
						.eInverseRemove(
								this,
								SdmPackage.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK,
								AbstractStoryPatternLink.class, msgs);
			return basicSetOppositeStoryPatternLink(
					(AbstractStoryPatternLink) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__SOURCE:
			return basicSetSource(null, msgs);
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__TARGET:
			return basicSetTarget(null, msgs);
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK:
			return basicSetOppositeStoryPatternLink(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__SOURCE:
			if (resolve)
				return getSource();
			return basicGetSource();
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__TARGET:
			if (resolve)
				return getTarget();
			return basicGetTarget();
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE:
			return isCheckOnlyExistence();
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY:
			return getMatchingPriority();
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK:
			if (resolve)
				return getOppositeStoryPatternLink();
			return basicGetOppositeStoryPatternLink();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__SOURCE:
			setSource((AbstractStoryPatternObject) newValue);
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__TARGET:
			setTarget((AbstractStoryPatternObject) newValue);
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE:
			setCheckOnlyExistence((Boolean) newValue);
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY:
			setMatchingPriority((Integer) newValue);
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK:
			setOppositeStoryPatternLink((AbstractStoryPatternLink) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__SOURCE:
			setSource((AbstractStoryPatternObject) null);
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__TARGET:
			setTarget((AbstractStoryPatternObject) null);
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE:
			setCheckOnlyExistence(CHECK_ONLY_EXISTENCE_EDEFAULT);
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY:
			setMatchingPriority(MATCHING_PRIORITY_EDEFAULT);
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK:
			setOppositeStoryPatternLink((AbstractStoryPatternLink) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__SOURCE:
			return source != null;
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__TARGET:
			return target != null;
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE:
			return checkOnlyExistence != CHECK_ONLY_EXISTENCE_EDEFAULT;
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY:
			return matchingPriority != MATCHING_PRIORITY_EDEFAULT;
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK:
			return oppositeStoryPatternLink != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checkOnlyExistence: ");
		result.append(checkOnlyExistence);
		result.append(", matchingPriority: ");
		result.append(matchingPriority);
		result.append(')');
		return result.toString();
	}

} // AbstractStoryPatternLinkImpl
