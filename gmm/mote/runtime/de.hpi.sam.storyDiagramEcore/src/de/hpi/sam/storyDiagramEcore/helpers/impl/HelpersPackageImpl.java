/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.helpers.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage;
import de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsPackageImpl;
import de.hpi.sam.storyDiagramEcore.helpers.HelpersFactory;
import de.hpi.sam.storyDiagramEcore.helpers.HelpersPackage;
import de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class HelpersPackageImpl extends EPackageImpl implements HelpersPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mapEntryEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.storyDiagramEcore.helpers.HelpersPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private HelpersPackageImpl() {
		super(eNS_URI, HelpersFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link HelpersPackage#eINSTANCE} when
	 * that field is accessed. Clients should not invoke it directly. Instead,
	 * they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static HelpersPackage init() {
		if (isInited)
			return (HelpersPackage) EPackage.Registry.INSTANCE
					.getEPackage(HelpersPackage.eNS_URI);

		// Obtain or create and register package
		HelpersPackageImpl theHelpersPackage = (HelpersPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof HelpersPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new HelpersPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		StoryDiagramEcorePackageImpl theStoryDiagramEcorePackage = (StoryDiagramEcorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI) instanceof StoryDiagramEcorePackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI)
				: StoryDiagramEcorePackage.eINSTANCE);
		CallActionsPackageImpl theCallActionsPackage = (CallActionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(CallActionsPackage.eNS_URI) instanceof CallActionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(CallActionsPackage.eNS_URI)
				: CallActionsPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI)
				: ExpressionsPackage.eINSTANCE);
		NodesPackageImpl theNodesPackage = (NodesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(NodesPackage.eNS_URI) instanceof NodesPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(NodesPackage.eNS_URI) : NodesPackage.eINSTANCE);
		SdmPackageImpl theSdmPackage = (SdmPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(SdmPackage.eNS_URI) instanceof SdmPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(SdmPackage.eNS_URI) : SdmPackage.eINSTANCE);

		// Create package meta-data objects
		theHelpersPackage.createPackageContents();
		theStoryDiagramEcorePackage.createPackageContents();
		theCallActionsPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theNodesPackage.createPackageContents();
		theSdmPackage.createPackageContents();

		// Initialize created meta-data
		theHelpersPackage.initializePackageContents();
		theStoryDiagramEcorePackage.initializePackageContents();
		theCallActionsPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theNodesPackage.initializePackageContents();
		theSdmPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theHelpersPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(HelpersPackage.eNS_URI,
				theHelpersPackage);
		return theHelpersPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMapEntry() {
		return mapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMapEntry_Key() {
		return (EAttribute) mapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMapEntry_Value() {
		return (EAttribute) mapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public HelpersFactory getHelpersFactory() {
		return (HelpersFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		mapEntryEClass = createEClass(MAP_ENTRY);
		createEAttribute(mapEntryEClass, MAP_ENTRY__KEY);
		createEAttribute(mapEntryEClass, MAP_ENTRY__VALUE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters
		ETypeParameter mapEntryEClass_KeyType = addETypeParameter(
				mapEntryEClass, "KeyType");
		ETypeParameter mapEntryEClass_ValueType = addETypeParameter(
				mapEntryEClass, "ValueType");

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(mapEntryEClass, Map.Entry.class, "MapEntry", !IS_ABSTRACT,
				!IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		EGenericType g1 = createEGenericType(mapEntryEClass_KeyType);
		initEAttribute(getMapEntry_Key(), g1, "key", null, 1, 1,
				Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(mapEntryEClass_ValueType);
		initEAttribute(getMapEntry_Value(), g1, "value", null, 0, 1,
				Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} // HelpersPackageImpl
