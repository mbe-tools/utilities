/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Initial Node</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An initial node marks the starting point of the control flow of an activity. There must be exactly one initial node in an activity. An Initial node must have one outgoing edge and no incoming edges.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getInitialNode()
 * @model
 * @generated
 */
public interface InitialNode extends ActivityNode {
} // InitialNode
