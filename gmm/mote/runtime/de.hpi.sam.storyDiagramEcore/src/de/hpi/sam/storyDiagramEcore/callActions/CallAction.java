/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions;

import org.eclipse.emf.ecore.EClassifier;

import de.hpi.sam.storyDiagramEcore.NamedElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Call Action</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * CallAction is the base interface for all call actions, i.e. variable declarations, literal statements and method calls.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.CallAction#getClassifier <em>Classifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCallAction()
 * @model abstract="true"
 * @generated
 */
public interface CallAction extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Classifier</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * EClassifier of the call action. This can be a variable's or literal's
	 * type or the return type of a method call. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Classifier</em>' reference.
	 * @see #setClassifier(EClassifier)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCallAction_Classifier()
	 * @model
	 * @generated
	 */
	EClassifier getClassifier();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.CallAction#getClassifier <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Classifier</em>' reference.
	 * @see #getClassifier()
	 * @generated
	 */
	void setClassifier(EClassifier value);

} // CallAction
