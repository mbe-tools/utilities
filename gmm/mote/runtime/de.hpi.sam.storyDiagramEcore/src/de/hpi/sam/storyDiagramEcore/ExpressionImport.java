/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Expression Import</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.ExpressionImport#getImportedFileURI <em>Imported File URI</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.ExpressionImport#getExpressionLanguage <em>Expression Language</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.ExpressionImport#getResolvedImportedFileURI <em>Resolved Imported File URI</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getExpressionImport()
 * @model
 * @generated
 */
public interface ExpressionImport extends Import {
	/**
	 * Returns the value of the '<em><b>Imported File URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imported File URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imported File URI</em>' attribute.
	 * @see #setImportedFileURI(String)
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getExpressionImport_ImportedFileURI()
	 * @model required="true"
	 * @generated
	 */
	String getImportedFileURI();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.ExpressionImport#getImportedFileURI <em>Imported File URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Imported File URI</em>' attribute.
	 * @see #getImportedFileURI()
	 * @generated
	 */
	void setImportedFileURI(String value);

	/**
	 * Returns the value of the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression Language</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression Language</em>' attribute.
	 * @see #setExpressionLanguage(String)
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getExpressionImport_ExpressionLanguage()
	 * @model required="true"
	 * @generated
	 */
	String getExpressionLanguage();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.ExpressionImport#getExpressionLanguage <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Expression Language</em>' attribute.
	 * @see #getExpressionLanguage()
	 * @generated
	 */
	void setExpressionLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Resolved Imported File URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resolved Imported File URI</em>' attribute
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resolved Imported File URI</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getExpressionImport_ResolvedImportedFileURI()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getResolvedImportedFileURI();

} // ExpressionImport
