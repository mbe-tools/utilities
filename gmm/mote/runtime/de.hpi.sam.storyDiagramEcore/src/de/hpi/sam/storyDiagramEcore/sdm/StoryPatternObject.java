/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import org.eclipse.emf.common.util.EList;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Story Pattern Object</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Story pattern objects can be bound to instance objects during the execution of the story pattern.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getBindingType <em>Binding Type</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getAttributeAssignments <em>Attribute Assignments</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getDirectAssignmentExpression <em>Direct Assignment Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternObject()
 * @model
 * @generated
 */
public interface StoryPatternObject extends AbstractStoryPatternObject {
	/**
	 * Returns the value of the '<em><b>Binding Type</b></em>' attribute. The
	 * literals are from the enumeration
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration}. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Indicates, whether this story pattern object should bound to a specific
	 * object in advance or if a valid binding has to be found during the
	 * execution of the story pattern. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Binding Type</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration
	 * @see #setBindingType(BindingTypeEnumeration)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternObject_BindingType()
	 * @model required="true"
	 * @generated
	 */
	BindingTypeEnumeration getBindingType();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getBindingType <em>Binding Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Binding Type</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration
	 * @see #getBindingType()
	 * @generated
	 */
	void setBindingType(BindingTypeEnumeration value);

	/**
	 * Returns the value of the '<em><b>Attribute Assignments</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment}. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> A list
	 * of AttributeAssignments, which specify the new values of the attributes
	 * after a successful match of the story pattern. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Attribute Assignments</em>' containment
	 *         reference list.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternObject_AttributeAssignments()
	 * @model containment="true"
	 * @generated
	 */
	EList<AttributeAssignment> getAttributeAssignments();

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.expressions.Expression}. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> A list
	 * of constraint expressions that must hold for the instance object bound to
	 * this story pattern object. Each constraint expression must return a
	 * boolean value. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Constraints</em>' containment reference
	 *         list.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternObject_Constraints()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getConstraints();

	/**
	 * Returns the value of the '<em><b>Direct Assignment Expression</b></em>'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> A direct assignment expression can be used to return
	 * an object to which this StoryPatternObject is bound to. The expression
	 * must return a value of the type of the story pattern object. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Direct Assignment Expression</em>'
	 *         containment reference.
	 * @see #setDirectAssignmentExpression(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternObject_DirectAssignmentExpression()
	 * @model containment="true"
	 * @generated
	 */
	Expression getDirectAssignmentExpression();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getDirectAssignmentExpression
	 * <em>Direct Assignment Expression</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Direct Assignment Expression</em>'
	 *            containment reference.
	 * @see #getDirectAssignmentExpression()
	 * @generated
	 */
	void setDirectAssignmentExpression(Expression value);

} // StoryPatternObject
