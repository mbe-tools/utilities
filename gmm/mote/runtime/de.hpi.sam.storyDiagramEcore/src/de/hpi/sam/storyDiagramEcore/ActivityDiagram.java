/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Activity Diagram</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The activity diagram is the model root of all activity diagrams. It contains several activities.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.ActivityDiagram#getActivities <em>Activities</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getActivityDiagram()
 * @model
 * @generated
 */
public interface ActivityDiagram extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Activities</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.Activity}. <!-- begin-user-doc -->
	 * <!-- end-user-doc --> <!-- begin-model-doc --> The activities contained
	 * in this story diagram. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Activities</em>' containment reference
	 *         list.
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getActivityDiagram_Activities()
	 * @model containment="true"
	 * @generated
	 */
	EList<Activity> getActivities();

} // ActivityDiagram
