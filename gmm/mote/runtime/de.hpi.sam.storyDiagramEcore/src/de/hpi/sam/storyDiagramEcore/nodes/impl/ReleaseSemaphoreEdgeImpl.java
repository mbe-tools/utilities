/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Release Semaphore Edge</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ReleaseSemaphoreEdgeImpl#getActivityEdge <em>Activity Edge</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReleaseSemaphoreEdgeImpl extends SynchronizationEdgeImpl implements
		ReleaseSemaphoreEdge {
	/**
	 * The cached value of the '{@link #getActivityEdge() <em>Activity Edge</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getActivityEdge()
	 * @generated
	 * @ordered
	 */
	protected ActivityEdge activityEdge;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ReleaseSemaphoreEdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NodesPackage.Literals.RELEASE_SEMAPHORE_EDGE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityEdge getActivityEdge() {
		if (activityEdge != null && activityEdge.eIsProxy()) {
			InternalEObject oldActivityEdge = (InternalEObject) activityEdge;
			activityEdge = (ActivityEdge) eResolveProxy(oldActivityEdge);
			if (activityEdge != oldActivityEdge) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							NodesPackage.RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE,
							oldActivityEdge, activityEdge));
			}
		}
		return activityEdge;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityEdge basicGetActivityEdge() {
		return activityEdge;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActivityEdge(ActivityEdge newActivityEdge,
			NotificationChain msgs) {
		ActivityEdge oldActivityEdge = activityEdge;
		activityEdge = newActivityEdge;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					NodesPackage.RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE,
					oldActivityEdge, newActivityEdge);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivityEdge(ActivityEdge newActivityEdge) {
		if (newActivityEdge != activityEdge) {
			NotificationChain msgs = null;
			if (activityEdge != null)
				msgs = ((InternalEObject) activityEdge).eInverseRemove(this,
						NodesPackage.ACTIVITY_EDGE__RELEASE_EDGES,
						ActivityEdge.class, msgs);
			if (newActivityEdge != null)
				msgs = ((InternalEObject) newActivityEdge).eInverseAdd(this,
						NodesPackage.ACTIVITY_EDGE__RELEASE_EDGES,
						ActivityEdge.class, msgs);
			msgs = basicSetActivityEdge(newActivityEdge, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					NodesPackage.RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE,
					newActivityEdge, newActivityEdge));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case NodesPackage.RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE:
			if (activityEdge != null)
				msgs = ((InternalEObject) activityEdge).eInverseRemove(this,
						NodesPackage.ACTIVITY_EDGE__RELEASE_EDGES,
						ActivityEdge.class, msgs);
			return basicSetActivityEdge((ActivityEdge) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case NodesPackage.RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE:
			return basicSetActivityEdge(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case NodesPackage.RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE:
			if (resolve)
				return getActivityEdge();
			return basicGetActivityEdge();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case NodesPackage.RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE:
			setActivityEdge((ActivityEdge) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case NodesPackage.RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE:
			setActivityEdge((ActivityEdge) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case NodesPackage.RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE:
			return activityEdge != null;
		}
		return super.eIsSet(featureID);
	}

} // ReleaseSemaphoreEdgeImpl
