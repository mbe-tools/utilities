/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions;

import org.eclipse.emf.ecore.EClassifier;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Call Action Parameter</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A parameter definition that is used by method call actions, for example.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter#getParameterValueAction <em>Parameter Value Action</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter#getParameterClassfier <em>Parameter Classfier</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCallActionParameter()
 * @model
 * @generated
 */
public interface CallActionParameter extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Parameter Value Action</b></em>'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> An expression that provides a value when it is
	 * evaluated. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Parameter Value Action</em>' containment
	 *         reference.
	 * @see #setParameterValueAction(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCallActionParameter_ParameterValueAction()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getParameterValueAction();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter#getParameterValueAction
	 * <em>Parameter Value Action</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Parameter Value Action</em>'
	 *            containment reference.
	 * @see #getParameterValueAction()
	 * @generated
	 */
	void setParameterValueAction(Expression value);

	/**
	 * Returns the value of the '<em><b>Parameter Classfier</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The EClassifier of the parameter. IMPORTANT: The parameter's type is the
	 * type expected by the method for which the parameter is used. It does not
	 * need to be the type of the parameterValueAction. Of course, the type of
	 * the parameterValueAction must be a subtype of the parameter's type. If
	 * the parameterClassifier is not available, the parameterTypeName is used
	 * instead. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Parameter Classfier</em>' reference.
	 * @see #setParameterClassfier(EClassifier)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCallActionParameter_ParameterClassfier()
	 * @model required="true"
	 * @generated
	 */
	EClassifier getParameterClassfier();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter#getParameterClassfier <em>Parameter Classfier</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Parameter Classfier</em>' reference.
	 * @see #getParameterClassfier()
	 * @generated
	 */
	void setParameterClassfier(EClassifier value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (eIsProxy()) return super.toString();\r\n\r\nStringBuffer buffer = new StringBuffer();\r\n\r\nif (getParameterValueAction() == null)\r\n{\r\n\tbuffer.append(\"[null]\");\r\n}\r\nelse\r\n{\r\n\tbuffer.append(getParameterValueAction().toString());\r\n}\r\n\r\nreturn buffer.toString().trim();'"
	 * @generated
	 */
	String toString();

} // CallActionParameter
