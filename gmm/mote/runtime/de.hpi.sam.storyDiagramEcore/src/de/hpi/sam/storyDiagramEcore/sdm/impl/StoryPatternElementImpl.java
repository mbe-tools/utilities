/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Story Pattern Element</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternElementImpl#getModifier <em>Modifier</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternElementImpl#getMatchType <em>Match Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class StoryPatternElementImpl extends NamedElementImpl
		implements StoryPatternElement {
	/**
	 * The default value of the '{@link #getModifier() <em>Modifier</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getModifier()
	 * @generated
	 * @ordered
	 */
	protected static final StoryPatternModifierEnumeration MODIFIER_EDEFAULT = StoryPatternModifierEnumeration.NONE;

	/**
	 * The cached value of the '{@link #getModifier() <em>Modifier</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getModifier()
	 * @generated
	 * @ordered
	 */
	protected StoryPatternModifierEnumeration modifier = MODIFIER_EDEFAULT;

	/**
	 * The default value of the '{@link #getMatchType() <em>Match Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMatchType()
	 * @generated
	 * @ordered
	 */
	protected static final StoryPatternMatchTypeEnumeration MATCH_TYPE_EDEFAULT = StoryPatternMatchTypeEnumeration.NONE;

	/**
	 * The cached value of the '{@link #getMatchType() <em>Match Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMatchType()
	 * @generated
	 * @ordered
	 */
	protected StoryPatternMatchTypeEnumeration matchType = MATCH_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected StoryPatternElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SdmPackage.Literals.STORY_PATTERN_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternModifierEnumeration getModifier() {
		return modifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setModifier(StoryPatternModifierEnumeration newModifier) {
		StoryPatternModifierEnumeration oldModifier = modifier;
		modifier = newModifier == null ? MODIFIER_EDEFAULT : newModifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.STORY_PATTERN_ELEMENT__MODIFIER, oldModifier,
					modifier));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternMatchTypeEnumeration getMatchType() {
		return matchType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMatchType(StoryPatternMatchTypeEnumeration newMatchType) {
		StoryPatternMatchTypeEnumeration oldMatchType = matchType;
		matchType = newMatchType == null ? MATCH_TYPE_EDEFAULT : newMatchType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.STORY_PATTERN_ELEMENT__MATCH_TYPE, oldMatchType,
					matchType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_ELEMENT__MODIFIER:
			return getModifier();
		case SdmPackage.STORY_PATTERN_ELEMENT__MATCH_TYPE:
			return getMatchType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_ELEMENT__MODIFIER:
			setModifier((StoryPatternModifierEnumeration) newValue);
			return;
		case SdmPackage.STORY_PATTERN_ELEMENT__MATCH_TYPE:
			setMatchType((StoryPatternMatchTypeEnumeration) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_ELEMENT__MODIFIER:
			setModifier(MODIFIER_EDEFAULT);
			return;
		case SdmPackage.STORY_PATTERN_ELEMENT__MATCH_TYPE:
			setMatchType(MATCH_TYPE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_ELEMENT__MODIFIER:
			return modifier != MODIFIER_EDEFAULT;
		case SdmPackage.STORY_PATTERN_ELEMENT__MATCH_TYPE:
			return matchType != MATCH_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (modifier: ");
		result.append(modifier);
		result.append(", matchType: ");
		result.append(matchType);
		result.append(')');
		return result.toString();
	}

} // StoryPatternElementImpl
