/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Import</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getImport()
 * @model abstract="true"
 * @generated
 */
public interface Import extends EObject {
} // Import
