/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage
 * @generated
 */
public interface CallActionsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	CallActionsFactory eINSTANCE = de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>Call Action Parameter</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Call Action Parameter</em>'.
	 * @generated
	 */
	CallActionParameter createCallActionParameter();

	/**
	 * Returns a new object of class '<em>Call Story Diagram Interpreter Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Call Story Diagram Interpreter Action</em>'.
	 * @generated
	 */
	CallStoryDiagramInterpreterAction createCallStoryDiagramInterpreterAction();

	/**
	 * Returns a new object of class '<em>Literal Declaration Action</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Literal Declaration Action</em>'.
	 * @generated
	 */
	LiteralDeclarationAction createLiteralDeclarationAction();

	/**
	 * Returns a new object of class '<em>Method Call Action</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Method Call Action</em>'.
	 * @generated
	 */
	MethodCallAction createMethodCallAction();

	/**
	 * Returns a new object of class '<em>New Object Action</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>New Object Action</em>'.
	 * @generated
	 */
	NewObjectAction createNewObjectAction();

	/**
	 * Returns a new object of class '<em>Variable Declaration Action</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return a new object of class '<em>Variable Declaration Action</em>'.
	 * @generated
	 */
	VariableDeclarationAction createVariableDeclarationAction();

	/**
	 * Returns a new object of class '<em>Variable Reference Action</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Variable Reference Action</em>'.
	 * @generated
	 */
	VariableReferenceAction createVariableReferenceAction();

	/**
	 * Returns a new object of class '<em>Compare Action</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Compare Action</em>'.
	 * @generated
	 */
	CompareAction createCompareAction();

	/**
	 * Returns a new object of class '<em>Null Value Action</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Null Value Action</em>'.
	 * @generated
	 */
	NullValueAction createNullValueAction();

	/**
	 * Returns a new object of class '<em>Operation Action</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Operation Action</em>'.
	 * @generated
	 */
	OperationAction createOperationAction();

	/**
	 * Returns a new object of class '<em>Case Insensitive Comparator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Case Insensitive Comparator</em>'.
	 * @generated
	 */
	CaseInsensitiveComparator createCaseInsensitiveComparator();

	/**
	 * Returns a new object of class '<em>Get Property Value Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Property Value Action</em>'.
	 * @generated
	 */
	GetPropertyValueAction createGetPropertyValueAction();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CallActionsPackage getCallActionsPackage();

} // CallActionsFactory
