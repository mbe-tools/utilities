/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Abstract Story Pattern Object</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternObjectImpl#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternObjectImpl#getOutgoingStoryLinks <em>Outgoing Story Links</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternObjectImpl#getIncomingStoryLinks <em>Incoming Story Links</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternObjectImpl#getCreateModelObjectExpression <em>Create Model Object Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class AbstractStoryPatternObjectImpl extends
		StoryPatternElementImpl implements AbstractStoryPatternObject {
	/**
	 * The cached value of the '{@link #getClassifier() <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClassifier()
	 * @generated
	 * @ordered
	 */
	protected EClassifier classifier;

	/**
	 * The cached value of the '{@link #getOutgoingStoryLinks() <em>Outgoing Story Links</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutgoingStoryLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractStoryPatternLink> outgoingStoryLinks;

	/**
	 * The cached value of the '{@link #getIncomingStoryLinks() <em>Incoming Story Links</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncomingStoryLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractStoryPatternLink> incomingStoryLinks;

	/**
	 * The cached value of the '{@link #getCreateModelObjectExpression() <em>Create Model Object Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreateModelObjectExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression createModelObjectExpression;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractStoryPatternObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SdmPackage.Literals.ABSTRACT_STORY_PATTERN_OBJECT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier getClassifier() {
		if (classifier != null && classifier.eIsProxy()) {
			InternalEObject oldClassifier = (InternalEObject) classifier;
			classifier = (EClassifier) eResolveProxy(oldClassifier);
			if (classifier != oldClassifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CLASSIFIER,
							oldClassifier, classifier));
			}
		}
		return classifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier basicGetClassifier() {
		return classifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassifier(EClassifier newClassifier) {
		EClassifier oldClassifier = classifier;
		classifier = newClassifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CLASSIFIER,
					oldClassifier, classifier));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractStoryPatternLink> getOutgoingStoryLinks() {
		if (outgoingStoryLinks == null) {
			outgoingStoryLinks = new EObjectWithInverseResolvingEList<AbstractStoryPatternLink>(
					AbstractStoryPatternLink.class,
					this,
					SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS,
					SdmPackage.ABSTRACT_STORY_PATTERN_LINK__SOURCE);
		}
		return outgoingStoryLinks;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractStoryPatternLink> getIncomingStoryLinks() {
		if (incomingStoryLinks == null) {
			incomingStoryLinks = new EObjectWithInverseResolvingEList<AbstractStoryPatternLink>(
					AbstractStoryPatternLink.class,
					this,
					SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS,
					SdmPackage.ABSTRACT_STORY_PATTERN_LINK__TARGET);
		}
		return incomingStoryLinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getCreateModelObjectExpression() {
		return createModelObjectExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCreateModelObjectExpression(
			Expression newCreateModelObjectExpression, NotificationChain msgs) {
		Expression oldCreateModelObjectExpression = createModelObjectExpression;
		createModelObjectExpression = newCreateModelObjectExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
					this,
					Notification.SET,
					SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION,
					oldCreateModelObjectExpression,
					newCreateModelObjectExpression);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreateModelObjectExpression(
			Expression newCreateModelObjectExpression) {
		if (newCreateModelObjectExpression != createModelObjectExpression) {
			NotificationChain msgs = null;
			if (createModelObjectExpression != null)
				msgs = ((InternalEObject) createModelObjectExpression)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION,
								null, msgs);
			if (newCreateModelObjectExpression != null)
				msgs = ((InternalEObject) newCreateModelObjectExpression)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION,
								null, msgs);
			msgs = basicSetCreateModelObjectExpression(
					newCreateModelObjectExpression, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION,
					newCreateModelObjectExpression,
					newCreateModelObjectExpression));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOutgoingStoryLinks())
					.basicAdd(otherEnd, msgs);
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getIncomingStoryLinks())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS:
			return ((InternalEList<?>) getOutgoingStoryLinks()).basicRemove(
					otherEnd, msgs);
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS:
			return ((InternalEList<?>) getIncomingStoryLinks()).basicRemove(
					otherEnd, msgs);
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION:
			return basicSetCreateModelObjectExpression(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CLASSIFIER:
			if (resolve)
				return getClassifier();
			return basicGetClassifier();
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS:
			return getOutgoingStoryLinks();
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS:
			return getIncomingStoryLinks();
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION:
			return getCreateModelObjectExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CLASSIFIER:
			setClassifier((EClassifier) newValue);
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS:
			getOutgoingStoryLinks().clear();
			getOutgoingStoryLinks().addAll(
					(Collection<? extends AbstractStoryPatternLink>) newValue);
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS:
			getIncomingStoryLinks().clear();
			getIncomingStoryLinks().addAll(
					(Collection<? extends AbstractStoryPatternLink>) newValue);
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION:
			setCreateModelObjectExpression((Expression) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CLASSIFIER:
			setClassifier((EClassifier) null);
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS:
			getOutgoingStoryLinks().clear();
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS:
			getIncomingStoryLinks().clear();
			return;
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION:
			setCreateModelObjectExpression((Expression) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CLASSIFIER:
			return classifier != null;
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS:
			return outgoingStoryLinks != null && !outgoingStoryLinks.isEmpty();
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS:
			return incomingStoryLinks != null && !incomingStoryLinks.isEmpty();
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION:
			return createModelObjectExpression != null;
		}
		return super.eIsSet(featureID);
	}

} // AbstractStoryPatternObjectImpl
