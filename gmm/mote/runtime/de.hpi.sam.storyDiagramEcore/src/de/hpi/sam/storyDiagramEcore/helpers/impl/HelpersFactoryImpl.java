/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.helpers.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.hpi.sam.storyDiagramEcore.helpers.HelpersFactory;
import de.hpi.sam.storyDiagramEcore.helpers.HelpersPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class HelpersFactoryImpl extends EFactoryImpl implements HelpersFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static HelpersFactory init() {
		try {
			HelpersFactory theHelpersFactory = (HelpersFactory) EPackage.Registry.INSTANCE
					.getEFactory(HelpersPackage.eNS_URI);
			if (theHelpersFactory != null) {
				return theHelpersFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new HelpersFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public HelpersFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case HelpersPackage.MAP_ENTRY:
			return (EObject) createMapEntry();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public <KeyType, ValueType> Map.Entry<KeyType, ValueType> createMapEntry() {
		MapEntryImpl<KeyType, ValueType> mapEntry = new MapEntryImpl<KeyType, ValueType>();
		return mapEntry;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public HelpersPackage getHelpersPackage() {
		return (HelpersPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static HelpersPackage getPackage() {
		return HelpersPackage.eINSTANCE;
	}

} // HelpersFactoryImpl
