/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EOperation;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Method Call Action</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The MethodCallAction can be used to invoke an arbitrary method. Some CallActionParameters can be used for the methods parameters. The instanceVariable CallAction returns the instance object in whose context the method will be executed.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getParameters <em>Parameters</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getMethod <em>Method</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getMethodClassName <em>Method Class Name</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getMethodName <em>Method Name</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getInstanceVariable <em>Instance Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getMethodCallAction()
 * @model
 * @generated
 */
public interface MethodCallAction extends CallAction {
	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter}.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> A
	 * list of parameters that are supplied to the method. <!-- end-model-doc
	 * -->
	 * 
	 * @return the value of the '<em>Parameters</em>' containment reference
	 *         list.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getMethodCallAction_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<CallActionParameter> getParameters();

	/**
	 * Returns the value of the '<em><b>Method</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * EOperation that will be executed. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Method</em>' reference.
	 * @see #setMethod(EOperation)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getMethodCallAction_Method()
	 * @model
	 * @generated
	 */
	EOperation getMethod();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getMethod
	 * <em>Method</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Method</em>' reference.
	 * @see #getMethod()
	 * @generated
	 */
	void setMethod(EOperation value);

	/**
	 * Returns the value of the '<em><b>Method Class Name</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The fully qualified name of the class that contains the method. This is
	 * used if no EOperation is available. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Method Class Name</em>' attribute.
	 * @see #setMethodClassName(String)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getMethodCallAction_MethodClassName()
	 * @model
	 * @generated
	 */
	String getMethodClassName();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getMethodClassName <em>Method Class Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Method Class Name</em>' attribute.
	 * @see #getMethodClassName()
	 * @generated
	 */
	void setMethodClassName(String value);

	/**
	 * Returns the value of the '<em><b>Method Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * name of the method. This is used if no EOperation is available. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Method Name</em>' attribute.
	 * @see #setMethodName(String)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getMethodCallAction_MethodName()
	 * @model
	 * @generated
	 */
	String getMethodName();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getMethodName <em>Method Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Method Name</em>' attribute.
	 * @see #getMethodName()
	 * @generated
	 */
	void setMethodName(String value);

	/**
	 * Returns the value of the '<em><b>Instance Variable</b></em>' containment
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> An Expression that must return an object in whose
	 * context the method will be executed. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Instance Variable</em>' containment
	 *         reference.
	 * @see #setInstanceVariable(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getMethodCallAction_InstanceVariable()
	 * @model containment="true"
	 * @generated
	 */
	Expression getInstanceVariable();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getInstanceVariable <em>Instance Variable</em>}' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance Variable</em>' containment reference.
	 * @see #getInstanceVariable()
	 * @generated
	 */
	void setInstanceVariable(Expression value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (eIsProxy()) return super.toString();\r\n\r\nStringBuffer buffer = new StringBuffer();\r\n\r\nif (getInstanceVariable() != null)\r\n{\r\n\tbuffer.append(getInstanceVariable().toString());\r\n}\r\nelse\r\n{\r\n\t//This may be a static method call\r\n\tif (getMethod() != null && getMethod().getEContainingClass() != null)\r\n\t{\r\n\t\tbuffer.append(getMethod().getEContainingClass().getName());\r\n\t}\r\n\telse if (getMethodClassName() != null && !\"\".equals(getMethodClassName()))\r\n\t{\r\n\t\tbuffer.append(getMethodClassName());\r\n\t}\r\n\telse\r\n\t{\r\n\t\tbuffer.append(\"[null]\");\r\n\t}\r\n}\r\n\r\nbuffer.append(\".\");\r\n\r\nif (getMethod() != null)\r\n{\r\n\tif (getMethod().getName() != null && !\"\".equals(getMethod().getName()))\r\n\t{\r\n\t\tbuffer.append(getMethod().getName());\r\n\t}\r\n\telse\r\n\t{\r\n\t\tbuffer.append(\"[null]\");\r\n\t}\r\n}\r\nelse\r\n{\r\n\tif (getMethodName() != null && !\"\".equals(getMethodName()))\r\n\t{\r\n\t\tbuffer.append(getMethodName());\r\n\t}\r\n\telse\r\n\t{\r\n\t\tbuffer.append(\"[null]\");\r\n\t}\r\n}\r\n\r\nbuffer.append(\"(\");\r\n\r\nif (!getParameters().isEmpty())\r\n{\r\n\tfor (CallActionParameter param : getParameters())\r\n\t{\r\n\t\tbuffer.append(param.toString());\r\n\r\n\t\tbuffer.append(\", \");\r\n\t}\r\n\r\n\tbuffer.delete(buffer.length() - 2, buffer.length());\r\n}\r\n\r\nbuffer.append(\")\");\r\n\r\nreturn buffer.toString().trim();'"
	 * @generated
	 */
	String toString();

} // MethodCallAction
