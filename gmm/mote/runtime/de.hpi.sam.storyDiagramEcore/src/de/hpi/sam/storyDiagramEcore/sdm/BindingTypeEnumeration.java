/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '
 * <em><b>Binding Type Enumeration</b></em>', and utility methods for working
 * with them. <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getBindingTypeEnumeration()
 * @model
 * @generated
 */
public enum BindingTypeEnumeration implements Enumerator {
	/**
	 * The '<em><b>UNBOUND</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNBOUND_VALUE
	 * @generated
	 * @ordered
	 */
	UNBOUND(0, "UNBOUND", "UNBOUND"),

	/**
	 * The '<em><b>BOUND</b></em>' literal object.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #BOUND_VALUE
	 * @generated
	 * @ordered
	 */
	BOUND(1, "BOUND", "BOUND"),

	/**
	 * The '<em><b>CAN BE BOUND</b></em>' literal object.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #CAN_BE_BOUND_VALUE
	 * @generated
	 * @ordered
	 */
	CAN_BE_BOUND(2, "CAN_BE_BOUND", "CAN_BE_BOUND");

	/**
	 * The '<em><b>UNBOUND</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> Indicates that a story pattern
	 * object has to be bound to an instance object during the execution of the
	 * story pattern. <!-- end-model-doc -->
	 * 
	 * @see #UNBOUND
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNBOUND_VALUE = 0;

	/**
	 * The '<em><b>BOUND</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> Indicates that a story pattern
	 * object is bound to an instance object before the execution of the story
	 * pattern. <!-- end-model-doc -->
	 * 
	 * @see #BOUND
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BOUND_VALUE = 1;

	/**
	 * The '<em><b>CAN BE BOUND</b></em>' literal value. <!-- begin-user-doc -->
	 * <!-- end-user-doc --> <!-- begin-model-doc --> Indicates that a story
	 * pattern object may be bound to an instance object before the execution of
	 * the story pattern. If it is not bound in advance a binding has to be
	 * found. <!-- end-model-doc -->
	 * 
	 * @see #CAN_BE_BOUND
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CAN_BE_BOUND_VALUE = 2;

	/**
	 * An array of all the '<em><b>Binding Type Enumeration</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static final BindingTypeEnumeration[] VALUES_ARRAY = new BindingTypeEnumeration[] {
			UNBOUND, BOUND, CAN_BE_BOUND, };

	/**
	 * A public read-only list of all the '
	 * <em><b>Binding Type Enumeration</b></em>' enumerators. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final List<BindingTypeEnumeration> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Binding Type Enumeration</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static BindingTypeEnumeration get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			BindingTypeEnumeration result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Binding Type Enumeration</b></em>' literal with the specified name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static BindingTypeEnumeration getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			BindingTypeEnumeration result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Binding Type Enumeration</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static BindingTypeEnumeration get(int value) {
		switch (value) {
		case UNBOUND_VALUE:
			return UNBOUND;
		case BOUND_VALUE:
			return BOUND;
		case CAN_BE_BOUND_VALUE:
			return CAN_BE_BOUND;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	private BindingTypeEnumeration(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} // BindingTypeEnumeration
