/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Variable Reference Action</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Refers to an existing variable. The type of the existing variable must match the type given in this VariableReferenceAction.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction#getVariableName <em>Variable Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getVariableReferenceAction()
 * @model
 * @generated
 */
public interface VariableReferenceAction extends CallAction {
	/**
	 * Returns the value of the '<em><b>Variable Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * name of the variable. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Variable Name</em>' attribute.
	 * @see #setVariableName(String)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getVariableReferenceAction_VariableName()
	 * @model required="true"
	 * @generated
	 */
	String getVariableName();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction#getVariableName <em>Variable Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Variable Name</em>' attribute.
	 * @see #getVariableName()
	 * @generated
	 */
	void setVariableName(String value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (eIsProxy()) return super.toString();\r\n\r\nif (getVariableName() != null) return getVariableName();\r\nelse return \"[null]\";'"
	 * @generated
	 */
	String toString();

} // VariableReferenceAction
