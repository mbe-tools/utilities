/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '
 * <em><b>Link Order Constraint Enumeration</b></em>', and utility methods for
 * working with them. <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getLinkOrderConstraintEnumeration()
 * @model
 * @generated
 */
public enum LinkOrderConstraintEnumeration implements Enumerator {
	/**
	 * The '<em><b>DIRECT SUCCESSOR</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #DIRECT_SUCCESSOR_VALUE
	 * @generated
	 * @ordered
	 */
	DIRECT_SUCCESSOR(0, "DIRECT_SUCCESSOR", "DIRECT_SUCCESSOR"),

	/**
	 * The '<em><b>SUCCESSOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUCCESSOR_VALUE
	 * @generated
	 * @ordered
	 */
	SUCCESSOR(1, "SUCCESSOR", "SUCCESSOR");

	/**
	 * The '<em><b>DIRECT SUCCESSOR</b></em>' literal value.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>DIRECT SUCCESSOR</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIRECT_SUCCESSOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DIRECT_SUCCESSOR_VALUE = 0;

	/**
	 * The '<em><b>SUCCESSOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SUCCESSOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SUCCESSOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SUCCESSOR_VALUE = 1;

	/**
	 * An array of all the '<em><b>Link Order Constraint Enumeration</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static final LinkOrderConstraintEnumeration[] VALUES_ARRAY = new LinkOrderConstraintEnumeration[] {
			DIRECT_SUCCESSOR, SUCCESSOR, };

	/**
	 * A public read-only list of all the '
	 * <em><b>Link Order Constraint Enumeration</b></em>' enumerators. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final List<LinkOrderConstraintEnumeration> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Link Order Constraint Enumeration</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static LinkOrderConstraintEnumeration get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			LinkOrderConstraintEnumeration result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Link Order Constraint Enumeration</b></em>' literal with the specified name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static LinkOrderConstraintEnumeration getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			LinkOrderConstraintEnumeration result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Link Order Constraint Enumeration</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static LinkOrderConstraintEnumeration get(int value) {
		switch (value) {
		case DIRECT_SUCCESSOR_VALUE:
			return DIRECT_SUCCESSOR;
		case SUCCESSOR_VALUE:
			return SUCCESSOR;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	private LinkOrderConstraintEnumeration(int value, String name,
			String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} // LinkOrderConstraintEnumeration
