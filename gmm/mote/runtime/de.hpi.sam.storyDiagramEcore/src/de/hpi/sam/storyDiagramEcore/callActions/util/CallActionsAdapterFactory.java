/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.callActions.*;
import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction;
import de.hpi.sam.storyDiagramEcore.callActions.CompareAction;
import de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction;
import de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction;
import de.hpi.sam.storyDiagramEcore.callActions.NullValueAction;
import de.hpi.sam.storyDiagramEcore.callActions.OperationAction;
import de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides
 * an adapter <code>createXXX</code> method for each class of the model. <!--
 * end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage
 * @generated
 */
public class CallActionsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static CallActionsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public CallActionsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = CallActionsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc --> This implementation returns <code>true</code> if
	 * the object is either the model's package or is an instance object of the
	 * model. <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected CallActionsSwitch<Adapter> modelSwitch = new CallActionsSwitch<Adapter>() {
		@Override
		public Adapter caseCallAction(CallAction object) {
			return createCallActionAdapter();
		}

		@Override
		public Adapter caseCallActionParameter(CallActionParameter object) {
			return createCallActionParameterAdapter();
		}

		@Override
		public Adapter caseCallStoryDiagramInterpreterAction(
				CallStoryDiagramInterpreterAction object) {
			return createCallStoryDiagramInterpreterActionAdapter();
		}

		@Override
		public Adapter caseLiteralDeclarationAction(
				LiteralDeclarationAction object) {
			return createLiteralDeclarationActionAdapter();
		}

		@Override
		public Adapter caseMethodCallAction(MethodCallAction object) {
			return createMethodCallActionAdapter();
		}

		@Override
		public Adapter caseNewObjectAction(NewObjectAction object) {
			return createNewObjectActionAdapter();
		}

		@Override
		public Adapter caseVariableDeclarationAction(
				VariableDeclarationAction object) {
			return createVariableDeclarationActionAdapter();
		}

		@Override
		public Adapter caseVariableReferenceAction(
				VariableReferenceAction object) {
			return createVariableReferenceActionAdapter();
		}

		@Override
		public Adapter caseCompareAction(CompareAction object) {
			return createCompareActionAdapter();
		}

		@Override
		public Adapter caseNullValueAction(NullValueAction object) {
			return createNullValueActionAdapter();
		}

		@Override
		public Adapter caseOperationAction(OperationAction object) {
			return createOperationActionAdapter();
		}

		@Override
		public Adapter caseAbstractComparator(AbstractComparator object) {
			return createAbstractComparatorAdapter();
		}

		@Override
		public Adapter caseCaseInsensitiveComparator(
				CaseInsensitiveComparator object) {
			return createCaseInsensitiveComparatorAdapter();
		}

		@Override
		public Adapter caseGetPropertyValueAction(GetPropertyValueAction object) {
			return createGetPropertyValueActionAdapter();
		}

		@Override
		public Adapter caseNamedElement(NamedElement object) {
			return createNamedElementAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.CallAction <em>Call Action</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallAction
	 * @generated
	 */
	public Adapter createCallActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter <em>Call Action Parameter</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter
	 * @generated
	 */
	public Adapter createCallActionParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction <em>Call Story Diagram Interpreter Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore
	 * cases; it's useful to ignore a case when inheritance will catch all the
	 * cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction
	 * @generated
	 */
	public Adapter createCallStoryDiagramInterpreterActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction <em>Literal Declaration Action</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction
	 * @generated
	 */
	public Adapter createLiteralDeclarationActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction <em>Method Call Action</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction
	 * @generated
	 */
	public Adapter createMethodCallActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction <em>New Object Action</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction
	 * @generated
	 */
	public Adapter createNewObjectActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction <em>Variable Declaration Action</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction
	 * @generated
	 */
	public Adapter createVariableDeclarationActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction <em>Variable Reference Action</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction
	 * @generated
	 */
	public Adapter createVariableReferenceActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.CompareAction <em>Compare Action</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CompareAction
	 * @generated
	 */
	public Adapter createCompareActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.NullValueAction <em>Null Value Action</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.NullValueAction
	 * @generated
	 */
	public Adapter createNullValueActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.OperationAction <em>Operation Action</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.OperationAction
	 * @generated
	 */
	public Adapter createOperationActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.AbstractComparator <em>Abstract Comparator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.AbstractComparator
	 * @generated
	 */
	public Adapter createAbstractComparatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.CaseInsensitiveComparator <em>Case Insensitive Comparator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CaseInsensitiveComparator
	 * @generated
	 */
	public Adapter createCaseInsensitiveComparatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction <em>Get Property Value Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction
	 * @generated
	 */
	public Adapter createGetPropertyValueActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so
	 * that we can easily ignore cases; it's useful to ignore a case when
	 * inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} // CallActionsAdapterFactory
