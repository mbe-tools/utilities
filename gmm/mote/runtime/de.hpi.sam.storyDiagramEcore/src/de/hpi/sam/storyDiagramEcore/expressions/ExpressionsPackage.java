/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.expressions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.expressions.ExpressionsFactory
 * @model kind="package"
 * @generated
 */
public interface ExpressionsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "expressions";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de/hpi/sam/storyDiagramEcore/expressions.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.hpi.sam.storyDiagramEcore.expressions";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	ExpressionsPackage eINSTANCE = de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsPackageImpl
			.init();

	/**
	 * The meta object id for the '
	 * {@link de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionImpl
	 * <em>Expression</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionImpl
	 * @see de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsPackageImpl#getExpression()
	 * @generated
	 */
	int EXPRESSION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.expressions.impl.CallActionExpressionImpl <em>Call Action Expression</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.expressions.impl.CallActionExpressionImpl
	 * @see de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsPackageImpl#getCallActionExpression()
	 * @generated
	 */
	int CALL_ACTION_EXPRESSION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_EXPRESSION__UUID = EXPRESSION__UUID;

	/**
	 * The feature id for the '<em><b>Call Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_EXPRESSION__CALL_ACTIONS = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Call Action Expression</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.expressions.impl.StringExpressionImpl <em>String Expression</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.expressions.impl.StringExpressionImpl
	 * @see de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsPackageImpl#getStringExpression()
	 * @generated
	 */
	int STRING_EXPRESSION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__UUID = EXPRESSION__UUID;

	/**
	 * The feature id for the '<em><b>Expression String</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__EXPRESSION_STRING = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__EXPRESSION_LANGUAGE = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ast</b></em>' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__AST = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>String Expression</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.expressions.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.expressions.Expression
	 * @generated
	 */
	EClass getExpression();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression <em>Call Action Expression</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Call Action Expression</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression
	 * @generated
	 */
	EClass getCallActionExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression#getCallActions <em>Call Actions</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Call Actions</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression#getCallActions()
	 * @see #getCallActionExpression()
	 * @generated
	 */
	EReference getCallActionExpression_CallActions();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.storyDiagramEcore.expressions.StringExpression
	 * <em>String Expression</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>String Expression</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.expressions.StringExpression
	 * @generated
	 */
	EClass getStringExpression();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.storyDiagramEcore.expressions.StringExpression#getExpressionString
	 * <em>Expression String</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Expression String</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.expressions.StringExpression#getExpressionString()
	 * @see #getStringExpression()
	 * @generated
	 */
	EAttribute getStringExpression_ExpressionString();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.storyDiagramEcore.expressions.StringExpression#getExpressionLanguage
	 * <em>Expression Language</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Expression Language</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.expressions.StringExpression#getExpressionLanguage()
	 * @see #getStringExpression()
	 * @generated
	 */
	EAttribute getStringExpression_ExpressionLanguage();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.expressions.StringExpression#getAst <em>Ast</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Ast</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.expressions.StringExpression#getAst()
	 * @see #getStringExpression()
	 * @generated
	 */
	EReference getStringExpression_Ast();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ExpressionsFactory getExpressionsFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionImpl <em>Expression</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionImpl
		 * @see de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsPackageImpl#getExpression()
		 * @generated
		 */
		EClass EXPRESSION = eINSTANCE.getExpression();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.expressions.impl.CallActionExpressionImpl <em>Call Action Expression</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.expressions.impl.CallActionExpressionImpl
		 * @see de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsPackageImpl#getCallActionExpression()
		 * @generated
		 */
		EClass CALL_ACTION_EXPRESSION = eINSTANCE.getCallActionExpression();

		/**
		 * The meta object literal for the '<em><b>Call Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference CALL_ACTION_EXPRESSION__CALL_ACTIONS = eINSTANCE
				.getCallActionExpression_CallActions();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.expressions.impl.StringExpressionImpl <em>String Expression</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.expressions.impl.StringExpressionImpl
		 * @see de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsPackageImpl#getStringExpression()
		 * @generated
		 */
		EClass STRING_EXPRESSION = eINSTANCE.getStringExpression();

		/**
		 * The meta object literal for the '<em><b>Expression String</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_EXPRESSION__EXPRESSION_STRING = eINSTANCE
				.getStringExpression_ExpressionString();

		/**
		 * The meta object literal for the '<em><b>Expression Language</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_EXPRESSION__EXPRESSION_LANGUAGE = eINSTANCE
				.getStringExpression_ExpressionLanguage();

		/**
		 * The meta object literal for the '<em><b>Ast</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRING_EXPRESSION__AST = eINSTANCE.getStringExpression_Ast();

	}

} // ExpressionsPackage
