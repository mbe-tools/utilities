/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Join Node</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A JoinNode joins several control flows to one control flow. It continues when all control flows to join have reached the JoinNode.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getJoinNode()
 * @model
 * @generated
 */
public interface JoinNode extends ActivityNode {
} // JoinNode
