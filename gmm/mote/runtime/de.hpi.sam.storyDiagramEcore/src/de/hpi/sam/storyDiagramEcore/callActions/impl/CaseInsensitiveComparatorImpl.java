/**
 */
package de.hpi.sam.storyDiagramEcore.callActions.impl;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.CaseInsensitiveComparator;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Case Insensitive Comparator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class CaseInsensitiveComparatorImpl extends AbstractComparatorImpl
		implements CaseInsensitiveComparator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaseInsensitiveComparatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CallActionsPackage.Literals.CASE_INSENSITIVE_COMPARATOR;
	}

} //CaseInsensitiveComparatorImpl
