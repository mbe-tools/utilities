/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc --> <!-- begin-model-doc --> This package contains the node
 * types available in an activity. <!-- end-model-doc -->
 * 
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesFactory
 * @model kind="package"
 * @generated
 */
public interface NodesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "nodes";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de/hpi/sam/storyDiagramEcore/nodes.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.hpi.sam.storyDiagramEcore.nodes";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	NodesPackage eINSTANCE = de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl
			.init();

	/**
	 * The meta object id for the '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityEdgeImpl
	 * <em>Activity Edge</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityEdgeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getActivityEdge()
	 * @generated
	 */
	int ACTIVITY_EDGE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_EDGE__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_EDGE__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_EDGE__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_EDGE__SOURCE = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_EDGE__TARGET = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_EDGE__ACTIVITY = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Guard Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_EDGE__GUARD_TYPE = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Guard Expression</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_EDGE__GUARD_EXPRESSION = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Release Edges</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_EDGE__RELEASE_EDGES = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Acquire Edges</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_EDGE__ACQUIRE_EDGES = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Activity Edge</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_EDGE_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityNodeImpl
	 * <em>Activity Node</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityNodeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getActivityNode()
	 * @generated
	 */
	int ACTIVITY_NODE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_NODE__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_NODE__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_NODE__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_NODE__INCOMING = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_NODE__OUTGOING = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_NODE__ACTIVITY = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Activity Node</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_NODE_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityFinalNodeImpl <em>Activity Final Node</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityFinalNodeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getActivityFinalNode()
	 * @generated
	 */
	int ACTIVITY_FINAL_NODE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FINAL_NODE__NAME = ACTIVITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FINAL_NODE__DESCRIPTION = ACTIVITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FINAL_NODE__UUID = ACTIVITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FINAL_NODE__INCOMING = ACTIVITY_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FINAL_NODE__OUTGOING = ACTIVITY_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FINAL_NODE__ACTIVITY = ACTIVITY_NODE__ACTIVITY;

	/**
	 * The feature id for the '<em><b>Return Value</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FINAL_NODE__RETURN_VALUE = ACTIVITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Out Parameter Values</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FINAL_NODE__OUT_PARAMETER_VALUES = ACTIVITY_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Activity Final Node</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FINAL_NODE_FEATURE_COUNT = ACTIVITY_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.FlowFinalNodeImpl <em>Flow Final Node</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.FlowFinalNodeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getFlowFinalNode()
	 * @generated
	 */
	int FLOW_FINAL_NODE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int FLOW_FINAL_NODE__NAME = ACTIVITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int FLOW_FINAL_NODE__DESCRIPTION = ACTIVITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int FLOW_FINAL_NODE__UUID = ACTIVITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int FLOW_FINAL_NODE__INCOMING = ACTIVITY_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int FLOW_FINAL_NODE__OUTGOING = ACTIVITY_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_FINAL_NODE__ACTIVITY = ACTIVITY_NODE__ACTIVITY;

	/**
	 * The number of structural features of the '<em>Flow Final Node</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_FINAL_NODE_FEATURE_COUNT = ACTIVITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ExpressionActivityNodeImpl <em>Expression Activity Node</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.ExpressionActivityNodeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getExpressionActivityNode()
	 * @generated
	 */
	int EXPRESSION_ACTIVITY_NODE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_ACTIVITY_NODE__NAME = ACTIVITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_ACTIVITY_NODE__DESCRIPTION = ACTIVITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_ACTIVITY_NODE__UUID = ACTIVITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_ACTIVITY_NODE__INCOMING = ACTIVITY_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_ACTIVITY_NODE__OUTGOING = ACTIVITY_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_ACTIVITY_NODE__ACTIVITY = ACTIVITY_NODE__ACTIVITY;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_ACTIVITY_NODE__EXPRESSION = ACTIVITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Expression Activity Node</em>' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_ACTIVITY_NODE_FEATURE_COUNT = ACTIVITY_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.impl.DecisionNodeImpl
	 * <em>Decision Node</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.DecisionNodeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getDecisionNode()
	 * @generated
	 */
	int DECISION_NODE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__NAME = ACTIVITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__DESCRIPTION = ACTIVITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__UUID = ACTIVITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__INCOMING = ACTIVITY_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__OUTGOING = ACTIVITY_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__ACTIVITY = ACTIVITY_NODE__ACTIVITY;

	/**
	 * The number of structural features of the '<em>Decision Node</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE_FEATURE_COUNT = ACTIVITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.impl.InitialNodeImpl
	 * <em>Initial Node</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.InitialNodeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getInitialNode()
	 * @generated
	 */
	int INITIAL_NODE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE__NAME = ACTIVITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE__DESCRIPTION = ACTIVITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE__UUID = ACTIVITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE__INCOMING = ACTIVITY_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE__OUTGOING = ACTIVITY_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE__ACTIVITY = ACTIVITY_NODE__ACTIVITY;

	/**
	 * The number of structural features of the '<em>Initial Node</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_FEATURE_COUNT = ACTIVITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.impl.MergeNodeImpl
	 * <em>Merge Node</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.MergeNodeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getMergeNode()
	 * @generated
	 */
	int MERGE_NODE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MERGE_NODE__NAME = ACTIVITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MERGE_NODE__DESCRIPTION = ACTIVITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MERGE_NODE__UUID = ACTIVITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MERGE_NODE__INCOMING = ACTIVITY_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MERGE_NODE__OUTGOING = ACTIVITY_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_NODE__ACTIVITY = ACTIVITY_NODE__ACTIVITY;

	/**
	 * The number of structural features of the '<em>Merge Node</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_NODE_FEATURE_COUNT = ACTIVITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.StoryActionNodeImpl <em>Story Action Node</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.StoryActionNodeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getStoryActionNode()
	 * @generated
	 */
	int STORY_ACTION_NODE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE__NAME = ACTIVITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE__DESCRIPTION = ACTIVITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE__UUID = ACTIVITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE__INCOMING = ACTIVITY_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE__OUTGOING = ACTIVITY_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE__ACTIVITY = ACTIVITY_NODE__ACTIVITY;

	/**
	 * The feature id for the '<em><b>For Each</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE__FOR_EACH = ACTIVITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Story Pattern Objects</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE__STORY_PATTERN_OBJECTS = ACTIVITY_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Story Pattern Links</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE__STORY_PATTERN_LINKS = ACTIVITY_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE__CONSTRAINTS = ACTIVITY_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Nac Semantic</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE__NAC_SEMANTIC = ACTIVITY_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>For Each Semantics</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE__FOR_EACH_SEMANTICS = ACTIVITY_NODE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Story Action Node</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_ACTION_NODE_FEATURE_COUNT = ACTIVITY_NODE_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ForkNodeImpl <em>Fork Node</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.ForkNodeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getForkNode()
	 * @generated
	 */
	int FORK_NODE = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int FORK_NODE__NAME = ACTIVITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int FORK_NODE__DESCRIPTION = ACTIVITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int FORK_NODE__UUID = ACTIVITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int FORK_NODE__INCOMING = ACTIVITY_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int FORK_NODE__OUTGOING = ACTIVITY_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORK_NODE__ACTIVITY = ACTIVITY_NODE__ACTIVITY;

	/**
	 * The number of structural features of the '<em>Fork Node</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int FORK_NODE_FEATURE_COUNT = ACTIVITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.JoinNodeImpl <em>Join Node</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.JoinNodeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getJoinNode()
	 * @generated
	 */
	int JOIN_NODE = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int JOIN_NODE__NAME = ACTIVITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int JOIN_NODE__DESCRIPTION = ACTIVITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int JOIN_NODE__UUID = ACTIVITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int JOIN_NODE__INCOMING = ACTIVITY_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int JOIN_NODE__OUTGOING = ACTIVITY_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_NODE__ACTIVITY = ACTIVITY_NODE__ACTIVITY;

	/**
	 * The number of structural features of the '<em>Join Node</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int JOIN_NODE_FEATURE_COUNT = ACTIVITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.SemaphoreImpl <em>Semaphore</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.SemaphoreImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getSemaphore()
	 * @generated
	 */
	int SEMAPHORE = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SEMAPHORE__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SEMAPHORE__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SEMAPHORE__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Synchronization Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMAPHORE__SYNCHRONIZATION_EDGES = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Token Count</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SEMAPHORE__TOKEN_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Semaphore</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SEMAPHORE_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.SynchronizationEdgeImpl <em>Synchronization Edge</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.SynchronizationEdgeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getSynchronizationEdge()
	 * @generated
	 */
	int SYNCHRONIZATION_EDGE = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION_EDGE__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION_EDGE__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION_EDGE__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Semaphore</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION_EDGE__SEMAPHORE = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION_EDGE__WEIGHT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Synchronization Edge</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION_EDGE_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ReleaseSemaphoreEdgeImpl <em>Release Semaphore Edge</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.ReleaseSemaphoreEdgeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getReleaseSemaphoreEdge()
	 * @generated
	 */
	int RELEASE_SEMAPHORE_EDGE = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RELEASE_SEMAPHORE_EDGE__NAME = SYNCHRONIZATION_EDGE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RELEASE_SEMAPHORE_EDGE__DESCRIPTION = SYNCHRONIZATION_EDGE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RELEASE_SEMAPHORE_EDGE__UUID = SYNCHRONIZATION_EDGE__UUID;

	/**
	 * The feature id for the '<em><b>Semaphore</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELEASE_SEMAPHORE_EDGE__SEMAPHORE = SYNCHRONIZATION_EDGE__SEMAPHORE;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RELEASE_SEMAPHORE_EDGE__WEIGHT = SYNCHRONIZATION_EDGE__WEIGHT;

	/**
	 * The feature id for the '<em><b>Activity Edge</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE = SYNCHRONIZATION_EDGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Release Semaphore Edge</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELEASE_SEMAPHORE_EDGE_FEATURE_COUNT = SYNCHRONIZATION_EDGE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.AcquireSemaphoreEdgeImpl <em>Acquire Semaphore Edge</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.AcquireSemaphoreEdgeImpl
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getAcquireSemaphoreEdge()
	 * @generated
	 */
	int ACQUIRE_SEMAPHORE_EDGE = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACQUIRE_SEMAPHORE_EDGE__NAME = SYNCHRONIZATION_EDGE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACQUIRE_SEMAPHORE_EDGE__DESCRIPTION = SYNCHRONIZATION_EDGE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACQUIRE_SEMAPHORE_EDGE__UUID = SYNCHRONIZATION_EDGE__UUID;

	/**
	 * The feature id for the '<em><b>Semaphore</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACQUIRE_SEMAPHORE_EDGE__SEMAPHORE = SYNCHRONIZATION_EDGE__SEMAPHORE;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACQUIRE_SEMAPHORE_EDGE__WEIGHT = SYNCHRONIZATION_EDGE__WEIGHT;

	/**
	 * The feature id for the '<em><b>Activity Edge</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ACQUIRE_SEMAPHORE_EDGE__ACTIVITY_EDGE = SYNCHRONIZATION_EDGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Acquire Semaphore Edge</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACQUIRE_SEMAPHORE_EDGE_FEATURE_COUNT = SYNCHRONIZATION_EDGE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration <em>Activity Edge Guard Enumeration</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getActivityEdgeGuardEnumeration()
	 * @generated
	 */
	int ACTIVITY_EDGE_GUARD_ENUMERATION = 15;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.nodes.ForEachSemanticsEnumeration <em>For Each Semantics Enumeration</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ForEachSemanticsEnumeration
	 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getForEachSemanticsEnumeration()
	 * @generated
	 */
	int FOR_EACH_SEMANTICS_ENUMERATION = 16;

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge <em>Activity Edge</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Activity Edge</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge
	 * @generated
	 */
	EClass getActivityEdge();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getSource()
	 * @see #getActivityEdge()
	 * @generated
	 */
	EReference getActivityEdge_Source();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getTarget()
	 * @see #getActivityEdge()
	 * @generated
	 */
	EReference getActivityEdge_Target();

	/**
	 * Returns the meta object for the container reference '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getActivity <em>Activity</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Activity</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getActivity()
	 * @see #getActivityEdge()
	 * @generated
	 */
	EReference getActivityEdge_Activity();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getGuardType <em>Guard Type</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Guard Type</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getGuardType()
	 * @see #getActivityEdge()
	 * @generated
	 */
	EAttribute getActivityEdge_GuardType();

	/**
	 * Returns the meta object for the containment reference '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getGuardExpression
	 * <em>Guard Expression</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the containment reference '
	 *         <em>Guard Expression</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getGuardExpression()
	 * @see #getActivityEdge()
	 * @generated
	 */
	EReference getActivityEdge_GuardExpression();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getReleaseEdges <em>Release Edges</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Release Edges</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getReleaseEdges()
	 * @see #getActivityEdge()
	 * @generated
	 */
	EReference getActivityEdge_ReleaseEdges();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getAcquireEdges <em>Acquire Edges</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Acquire Edges</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getAcquireEdges()
	 * @see #getActivityEdge()
	 * @generated
	 */
	EReference getActivityEdge_AcquireEdges();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode
	 * <em>Activity Final Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Activity Final Node</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode
	 * @generated
	 */
	EClass getActivityFinalNode();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode#getReturnValue <em>Return Value</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Return Value</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode#getReturnValue()
	 * @see #getActivityFinalNode()
	 * @generated
	 */
	EReference getActivityFinalNode_ReturnValue();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode#getOutParameterValues <em>Out Parameter Values</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Out Parameter Values</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode#getOutParameterValues()
	 * @see #getActivityFinalNode()
	 * @generated
	 */
	EReference getActivityFinalNode_OutParameterValues();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode <em>Flow Final Node</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flow Final Node</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode
	 * @generated
	 */
	EClass getFlowFinalNode();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode <em>Activity Node</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Activity Node</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityNode
	 * @generated
	 */
	EClass getActivityNode();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getIncoming <em>Incoming</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getIncoming()
	 * @see #getActivityNode()
	 * @generated
	 */
	EReference getActivityNode_Incoming();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getOutgoing <em>Outgoing</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getOutgoing()
	 * @see #getActivityNode()
	 * @generated
	 */
	EReference getActivityNode_Outgoing();

	/**
	 * Returns the meta object for the container reference '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getActivity <em>Activity</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Activity</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getActivity()
	 * @see #getActivityNode()
	 * @generated
	 */
	EReference getActivityNode_Activity();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode <em>Expression Activity Node</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Expression Activity Node</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode
	 * @generated
	 */
	EClass getExpressionActivityNode();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode#getExpression()
	 * @see #getExpressionActivityNode()
	 * @generated
	 */
	EReference getExpressionActivityNode_Expression();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.DecisionNode <em>Decision Node</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decision Node</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.DecisionNode
	 * @generated
	 */
	EClass getDecisionNode();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.InitialNode <em>Initial Node</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Initial Node</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.InitialNode
	 * @generated
	 */
	EClass getInitialNode();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.MergeNode <em>Merge Node</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Merge Node</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.MergeNode
	 * @generated
	 */
	EClass getMergeNode();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode
	 * <em>Story Action Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Story Action Node</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode
	 * @generated
	 */
	EClass getStoryActionNode();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#isForEach <em>For Each</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>For Each</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#isForEach()
	 * @see #getStoryActionNode()
	 * @generated
	 */
	EAttribute getStoryActionNode_ForEach();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getStoryPatternObjects <em>Story Pattern Objects</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Story Pattern Objects</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getStoryPatternObjects()
	 * @see #getStoryActionNode()
	 * @generated
	 */
	EReference getStoryActionNode_StoryPatternObjects();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getStoryPatternLinks
	 * <em>Story Pattern Links</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the containment reference list '
	 *         <em>Story Pattern Links</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getStoryPatternLinks()
	 * @see #getStoryActionNode()
	 * @generated
	 */
	EReference getStoryActionNode_StoryPatternLinks();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getConstraints()
	 * @see #getStoryActionNode()
	 * @generated
	 */
	EReference getStoryActionNode_Constraints();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getNacSemantic <em>Nac Semantic</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nac Semantic</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getNacSemantic()
	 * @see #getStoryActionNode()
	 * @generated
	 */
	EAttribute getStoryActionNode_NacSemantic();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getForEachSemantics
	 * <em>For Each Semantics</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>For Each Semantics</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getForEachSemantics()
	 * @see #getStoryActionNode()
	 * @generated
	 */
	EAttribute getStoryActionNode_ForEachSemantics();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.ForkNode <em>Fork Node</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fork Node</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ForkNode
	 * @generated
	 */
	EClass getForkNode();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.JoinNode <em>Join Node</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Join Node</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.JoinNode
	 * @generated
	 */
	EClass getJoinNode();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.Semaphore <em>Semaphore</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Semaphore</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.Semaphore
	 * @generated
	 */
	EClass getSemaphore();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.nodes.Semaphore#getSynchronizationEdges <em>Synchronization Edges</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Synchronization Edges</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.Semaphore#getSynchronizationEdges()
	 * @see #getSemaphore()
	 * @generated
	 */
	EReference getSemaphore_SynchronizationEdges();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.nodes.Semaphore#getTokenCount <em>Token Count</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Token Count</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.Semaphore#getTokenCount()
	 * @see #getSemaphore()
	 * @generated
	 */
	EAttribute getSemaphore_TokenCount();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge <em>Synchronization Edge</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Synchronization Edge</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge
	 * @generated
	 */
	EClass getSynchronizationEdge();

	/**
	 * Returns the meta object for the container reference '{@link de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge#getSemaphore <em>Semaphore</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Semaphore</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge#getSemaphore()
	 * @see #getSynchronizationEdge()
	 * @generated
	 */
	EReference getSynchronizationEdge_Semaphore();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge#getWeight <em>Weight</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Weight</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge#getWeight()
	 * @see #getSynchronizationEdge()
	 * @generated
	 */
	EAttribute getSynchronizationEdge_Weight();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge <em>Release Semaphore Edge</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Release Semaphore Edge</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge
	 * @generated
	 */
	EClass getReleaseSemaphoreEdge();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge#getActivityEdge <em>Activity Edge</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Activity Edge</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge#getActivityEdge()
	 * @see #getReleaseSemaphoreEdge()
	 * @generated
	 */
	EReference getReleaseSemaphoreEdge_ActivityEdge();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge <em>Acquire Semaphore Edge</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Acquire Semaphore Edge</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge
	 * @generated
	 */
	EClass getAcquireSemaphoreEdge();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge#getActivityEdge <em>Activity Edge</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Activity Edge</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge#getActivityEdge()
	 * @see #getAcquireSemaphoreEdge()
	 * @generated
	 */
	EReference getAcquireSemaphoreEdge_ActivityEdge();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration <em>Activity Edge Guard Enumeration</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for enum '<em>Activity Edge Guard Enumeration</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration
	 * @generated
	 */
	EEnum getActivityEdgeGuardEnumeration();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.storyDiagramEcore.nodes.ForEachSemanticsEnumeration <em>For Each Semantics Enumeration</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for enum '<em>For Each Semantics Enumeration</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ForEachSemanticsEnumeration
	 * @generated
	 */
	EEnum getForEachSemanticsEnumeration();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	NodesFactory getNodesFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityEdgeImpl <em>Activity Edge</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityEdgeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getActivityEdge()
		 * @generated
		 */
		EClass ACTIVITY_EDGE = eINSTANCE.getActivityEdge();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_EDGE__SOURCE = eINSTANCE.getActivityEdge_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_EDGE__TARGET = eINSTANCE.getActivityEdge_Target();

		/**
		 * The meta object literal for the '<em><b>Activity</b></em>' container reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_EDGE__ACTIVITY = eINSTANCE
				.getActivityEdge_Activity();

		/**
		 * The meta object literal for the '<em><b>Guard Type</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY_EDGE__GUARD_TYPE = eINSTANCE
				.getActivityEdge_GuardType();

		/**
		 * The meta object literal for the '<em><b>Guard Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_EDGE__GUARD_EXPRESSION = eINSTANCE
				.getActivityEdge_GuardExpression();

		/**
		 * The meta object literal for the '<em><b>Release Edges</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_EDGE__RELEASE_EDGES = eINSTANCE
				.getActivityEdge_ReleaseEdges();

		/**
		 * The meta object literal for the '<em><b>Acquire Edges</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_EDGE__ACQUIRE_EDGES = eINSTANCE
				.getActivityEdge_AcquireEdges();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityFinalNodeImpl <em>Activity Final Node</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityFinalNodeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getActivityFinalNode()
		 * @generated
		 */
		EClass ACTIVITY_FINAL_NODE = eINSTANCE.getActivityFinalNode();

		/**
		 * The meta object literal for the '<em><b>Return Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_FINAL_NODE__RETURN_VALUE = eINSTANCE
				.getActivityFinalNode_ReturnValue();

		/**
		 * The meta object literal for the '<em><b>Out Parameter Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_FINAL_NODE__OUT_PARAMETER_VALUES = eINSTANCE
				.getActivityFinalNode_OutParameterValues();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.FlowFinalNodeImpl <em>Flow Final Node</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.FlowFinalNodeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getFlowFinalNode()
		 * @generated
		 */
		EClass FLOW_FINAL_NODE = eINSTANCE.getFlowFinalNode();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityNodeImpl <em>Activity Node</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityNodeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getActivityNode()
		 * @generated
		 */
		EClass ACTIVITY_NODE = eINSTANCE.getActivityNode();

		/**
		 * The meta object literal for the '<em><b>Incoming</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_NODE__INCOMING = eINSTANCE
				.getActivityNode_Incoming();

		/**
		 * The meta object literal for the '<em><b>Outgoing</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_NODE__OUTGOING = eINSTANCE
				.getActivityNode_Outgoing();

		/**
		 * The meta object literal for the '<em><b>Activity</b></em>' container reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_NODE__ACTIVITY = eINSTANCE
				.getActivityNode_Activity();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ExpressionActivityNodeImpl <em>Expression Activity Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.ExpressionActivityNodeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getExpressionActivityNode()
		 * @generated
		 */
		EClass EXPRESSION_ACTIVITY_NODE = eINSTANCE.getExpressionActivityNode();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference EXPRESSION_ACTIVITY_NODE__EXPRESSION = eINSTANCE
				.getExpressionActivityNode_Expression();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.DecisionNodeImpl <em>Decision Node</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.DecisionNodeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getDecisionNode()
		 * @generated
		 */
		EClass DECISION_NODE = eINSTANCE.getDecisionNode();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.InitialNodeImpl <em>Initial Node</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.InitialNodeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getInitialNode()
		 * @generated
		 */
		EClass INITIAL_NODE = eINSTANCE.getInitialNode();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.MergeNodeImpl <em>Merge Node</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.MergeNodeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getMergeNode()
		 * @generated
		 */
		EClass MERGE_NODE = eINSTANCE.getMergeNode();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.StoryActionNodeImpl <em>Story Action Node</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.StoryActionNodeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getStoryActionNode()
		 * @generated
		 */
		EClass STORY_ACTION_NODE = eINSTANCE.getStoryActionNode();

		/**
		 * The meta object literal for the '<em><b>For Each</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STORY_ACTION_NODE__FOR_EACH = eINSTANCE
				.getStoryActionNode_ForEach();

		/**
		 * The meta object literal for the '<em><b>Story Pattern Objects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference STORY_ACTION_NODE__STORY_PATTERN_OBJECTS = eINSTANCE
				.getStoryActionNode_StoryPatternObjects();

		/**
		 * The meta object literal for the '<em><b>Story Pattern Links</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference STORY_ACTION_NODE__STORY_PATTERN_LINKS = eINSTANCE
				.getStoryActionNode_StoryPatternLinks();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference STORY_ACTION_NODE__CONSTRAINTS = eINSTANCE
				.getStoryActionNode_Constraints();

		/**
		 * The meta object literal for the '<em><b>Nac Semantic</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STORY_ACTION_NODE__NAC_SEMANTIC = eINSTANCE
				.getStoryActionNode_NacSemantic();

		/**
		 * The meta object literal for the '<em><b>For Each Semantics</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STORY_ACTION_NODE__FOR_EACH_SEMANTICS = eINSTANCE
				.getStoryActionNode_ForEachSemantics();

		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.storyDiagramEcore.nodes.impl.ForkNodeImpl
		 * <em>Fork Node</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.ForkNodeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getForkNode()
		 * @generated
		 */
		EClass FORK_NODE = eINSTANCE.getForkNode();

		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.storyDiagramEcore.nodes.impl.JoinNodeImpl
		 * <em>Join Node</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.JoinNodeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getJoinNode()
		 * @generated
		 */
		EClass JOIN_NODE = eINSTANCE.getJoinNode();

		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.storyDiagramEcore.nodes.impl.SemaphoreImpl
		 * <em>Semaphore</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.SemaphoreImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getSemaphore()
		 * @generated
		 */
		EClass SEMAPHORE = eINSTANCE.getSemaphore();

		/**
		 * The meta object literal for the '<em><b>Synchronization Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEMAPHORE__SYNCHRONIZATION_EDGES = eINSTANCE
				.getSemaphore_SynchronizationEdges();

		/**
		 * The meta object literal for the '<em><b>Token Count</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEMAPHORE__TOKEN_COUNT = eINSTANCE.getSemaphore_TokenCount();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.SynchronizationEdgeImpl <em>Synchronization Edge</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.SynchronizationEdgeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getSynchronizationEdge()
		 * @generated
		 */
		EClass SYNCHRONIZATION_EDGE = eINSTANCE.getSynchronizationEdge();

		/**
		 * The meta object literal for the '<em><b>Semaphore</b></em>' container reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNCHRONIZATION_EDGE__SEMAPHORE = eINSTANCE
				.getSynchronizationEdge_Semaphore();

		/**
		 * The meta object literal for the '<em><b>Weight</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYNCHRONIZATION_EDGE__WEIGHT = eINSTANCE
				.getSynchronizationEdge_Weight();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ReleaseSemaphoreEdgeImpl <em>Release Semaphore Edge</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.ReleaseSemaphoreEdgeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getReleaseSemaphoreEdge()
		 * @generated
		 */
		EClass RELEASE_SEMAPHORE_EDGE = eINSTANCE.getReleaseSemaphoreEdge();

		/**
		 * The meta object literal for the '<em><b>Activity Edge</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE = eINSTANCE
				.getReleaseSemaphoreEdge_ActivityEdge();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.impl.AcquireSemaphoreEdgeImpl <em>Acquire Semaphore Edge</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.AcquireSemaphoreEdgeImpl
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getAcquireSemaphoreEdge()
		 * @generated
		 */
		EClass ACQUIRE_SEMAPHORE_EDGE = eINSTANCE.getAcquireSemaphoreEdge();

		/**
		 * The meta object literal for the '<em><b>Activity Edge</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACQUIRE_SEMAPHORE_EDGE__ACTIVITY_EDGE = eINSTANCE
				.getAcquireSemaphoreEdge_ActivityEdge();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration <em>Activity Edge Guard Enumeration</em>}' enum.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getActivityEdgeGuardEnumeration()
		 * @generated
		 */
		EEnum ACTIVITY_EDGE_GUARD_ENUMERATION = eINSTANCE
				.getActivityEdgeGuardEnumeration();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.nodes.ForEachSemanticsEnumeration <em>For Each Semantics Enumeration</em>}' enum.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.nodes.ForEachSemanticsEnumeration
		 * @see de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl#getForEachSemanticsEnumeration()
		 * @generated
		 */
		EEnum FOR_EACH_SEMANTICS_ENUMERATION = eINSTANCE
				.getForEachSemanticsEnumeration();

	}

} // NodesPackage
