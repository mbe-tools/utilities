/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ForEachSemanticsEnumeration;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Story Action Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.StoryActionNodeImpl#isForEach <em>For Each</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.StoryActionNodeImpl#getStoryPatternObjects <em>Story Pattern Objects</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.StoryActionNodeImpl#getStoryPatternLinks <em>Story Pattern Links</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.StoryActionNodeImpl#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.StoryActionNodeImpl#getNacSemantic <em>Nac Semantic</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.StoryActionNodeImpl#getForEachSemantics <em>For Each Semantics</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StoryActionNodeImpl extends ActivityNodeImpl implements
		StoryActionNode {
	/**
	 * The default value of the '{@link #isForEach() <em>For Each</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isForEach()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FOR_EACH_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isForEach() <em>For Each</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isForEach()
	 * @generated
	 * @ordered
	 */
	protected boolean forEach = FOR_EACH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStoryPatternObjects()
	 * <em>Story Pattern Objects</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getStoryPatternObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractStoryPatternObject> storyPatternObjects;

	/**
	 * The cached value of the '{@link #getStoryPatternLinks()
	 * <em>Story Pattern Links</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getStoryPatternLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractStoryPatternLink> storyPatternLinks;

	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> constraints;

	/**
	 * The default value of the '{@link #getNacSemantic() <em>Nac Semantic</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNacSemantic()
	 * @generated
	 * @ordered
	 */
	protected static final StoryPatternNACSematicEnumeration NAC_SEMANTIC_EDEFAULT = StoryPatternNACSematicEnumeration.OR;

	/**
	 * The cached value of the '{@link #getNacSemantic() <em>Nac Semantic</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNacSemantic()
	 * @generated
	 * @ordered
	 */
	protected StoryPatternNACSematicEnumeration nacSemantic = NAC_SEMANTIC_EDEFAULT;

	/**
	 * The default value of the '{@link #getForEachSemantics() <em>For Each Semantics</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getForEachSemantics()
	 * @generated
	 * @ordered
	 */
	protected static final ForEachSemanticsEnumeration FOR_EACH_SEMANTICS_EDEFAULT = ForEachSemanticsEnumeration.FRESH_MATCH;

	/**
	 * The cached value of the '{@link #getForEachSemantics() <em>For Each Semantics</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getForEachSemantics()
	 * @generated
	 * @ordered
	 */
	protected ForEachSemanticsEnumeration forEachSemantics = FOR_EACH_SEMANTICS_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected StoryActionNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NodesPackage.Literals.STORY_ACTION_NODE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isForEach() {
		return forEach;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setForEach(boolean newForEach) {
		boolean oldForEach = forEach;
		forEach = newForEach;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					NodesPackage.STORY_ACTION_NODE__FOR_EACH, oldForEach,
					forEach));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractStoryPatternObject> getStoryPatternObjects() {
		if (storyPatternObjects == null) {
			storyPatternObjects = new EObjectContainmentEList<AbstractStoryPatternObject>(
					AbstractStoryPatternObject.class, this,
					NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_OBJECTS);
		}
		return storyPatternObjects;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractStoryPatternLink> getStoryPatternLinks() {
		if (storyPatternLinks == null) {
			storyPatternLinks = new EObjectContainmentEList<AbstractStoryPatternLink>(
					AbstractStoryPatternLink.class, this,
					NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_LINKS);
		}
		return storyPatternLinks;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getConstraints() {
		if (constraints == null) {
			constraints = new EObjectContainmentEList<Expression>(
					Expression.class, this,
					NodesPackage.STORY_ACTION_NODE__CONSTRAINTS);
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternNACSematicEnumeration getNacSemantic() {
		return nacSemantic;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setNacSemantic(StoryPatternNACSematicEnumeration newNacSemantic) {
		StoryPatternNACSematicEnumeration oldNacSemantic = nacSemantic;
		nacSemantic = newNacSemantic == null ? NAC_SEMANTIC_EDEFAULT
				: newNacSemantic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					NodesPackage.STORY_ACTION_NODE__NAC_SEMANTIC,
					oldNacSemantic, nacSemantic));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ForEachSemanticsEnumeration getForEachSemantics() {
		return forEachSemantics;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setForEachSemantics(
			ForEachSemanticsEnumeration newForEachSemantics) {
		ForEachSemanticsEnumeration oldForEachSemantics = forEachSemantics;
		forEachSemantics = newForEachSemantics == null ? FOR_EACH_SEMANTICS_EDEFAULT
				: newForEachSemantics;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					NodesPackage.STORY_ACTION_NODE__FOR_EACH_SEMANTICS,
					oldForEachSemantics, forEachSemantics));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_OBJECTS:
			return ((InternalEList<?>) getStoryPatternObjects()).basicRemove(
					otherEnd, msgs);
		case NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_LINKS:
			return ((InternalEList<?>) getStoryPatternLinks()).basicRemove(
					otherEnd, msgs);
		case NodesPackage.STORY_ACTION_NODE__CONSTRAINTS:
			return ((InternalEList<?>) getConstraints()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case NodesPackage.STORY_ACTION_NODE__FOR_EACH:
			return isForEach();
		case NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_OBJECTS:
			return getStoryPatternObjects();
		case NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_LINKS:
			return getStoryPatternLinks();
		case NodesPackage.STORY_ACTION_NODE__CONSTRAINTS:
			return getConstraints();
		case NodesPackage.STORY_ACTION_NODE__NAC_SEMANTIC:
			return getNacSemantic();
		case NodesPackage.STORY_ACTION_NODE__FOR_EACH_SEMANTICS:
			return getForEachSemantics();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case NodesPackage.STORY_ACTION_NODE__FOR_EACH:
			setForEach((Boolean) newValue);
			return;
		case NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_OBJECTS:
			getStoryPatternObjects().clear();
			getStoryPatternObjects()
					.addAll((Collection<? extends AbstractStoryPatternObject>) newValue);
			return;
		case NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_LINKS:
			getStoryPatternLinks().clear();
			getStoryPatternLinks().addAll(
					(Collection<? extends AbstractStoryPatternLink>) newValue);
			return;
		case NodesPackage.STORY_ACTION_NODE__CONSTRAINTS:
			getConstraints().clear();
			getConstraints()
					.addAll((Collection<? extends Expression>) newValue);
			return;
		case NodesPackage.STORY_ACTION_NODE__NAC_SEMANTIC:
			setNacSemantic((StoryPatternNACSematicEnumeration) newValue);
			return;
		case NodesPackage.STORY_ACTION_NODE__FOR_EACH_SEMANTICS:
			setForEachSemantics((ForEachSemanticsEnumeration) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case NodesPackage.STORY_ACTION_NODE__FOR_EACH:
			setForEach(FOR_EACH_EDEFAULT);
			return;
		case NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_OBJECTS:
			getStoryPatternObjects().clear();
			return;
		case NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_LINKS:
			getStoryPatternLinks().clear();
			return;
		case NodesPackage.STORY_ACTION_NODE__CONSTRAINTS:
			getConstraints().clear();
			return;
		case NodesPackage.STORY_ACTION_NODE__NAC_SEMANTIC:
			setNacSemantic(NAC_SEMANTIC_EDEFAULT);
			return;
		case NodesPackage.STORY_ACTION_NODE__FOR_EACH_SEMANTICS:
			setForEachSemantics(FOR_EACH_SEMANTICS_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case NodesPackage.STORY_ACTION_NODE__FOR_EACH:
			return forEach != FOR_EACH_EDEFAULT;
		case NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_OBJECTS:
			return storyPatternObjects != null
					&& !storyPatternObjects.isEmpty();
		case NodesPackage.STORY_ACTION_NODE__STORY_PATTERN_LINKS:
			return storyPatternLinks != null && !storyPatternLinks.isEmpty();
		case NodesPackage.STORY_ACTION_NODE__CONSTRAINTS:
			return constraints != null && !constraints.isEmpty();
		case NodesPackage.STORY_ACTION_NODE__NAC_SEMANTIC:
			return nacSemantic != NAC_SEMANTIC_EDEFAULT;
		case NodesPackage.STORY_ACTION_NODE__FOR_EACH_SEMANTICS:
			return forEachSemantics != FOR_EACH_SEMANTICS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (forEach: ");
		result.append(forEach);
		result.append(", nacSemantic: ");
		result.append(nacSemantic);
		result.append(", forEachSemantics: ");
		result.append(forEachSemantics);
		result.append(')');
		return result.toString();
	}

} // StoryActionNodeImpl
