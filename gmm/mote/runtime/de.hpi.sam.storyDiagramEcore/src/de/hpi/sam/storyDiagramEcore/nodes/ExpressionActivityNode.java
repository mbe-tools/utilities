/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Expression Activity Node</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This node contains expressions which model imperative calls.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getExpressionActivityNode()
 * @model
 * @generated
 */
public interface ExpressionActivityNode extends ActivityNode {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> The expression of this ExpressionActivityNode. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #setExpression(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getExpressionActivityNode_Expression()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getExpression();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(Expression value);

} // ExpressionActivityNode
