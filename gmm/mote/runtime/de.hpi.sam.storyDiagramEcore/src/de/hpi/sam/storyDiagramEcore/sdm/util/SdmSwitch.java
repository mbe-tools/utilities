/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.ExternalReference;
import de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 <<<<<<< .mine
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage
 * @generated
 */
public class SdmSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SdmPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SdmSwitch() {
		if (modelPackage == null) {
			modelPackage = SdmPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case SdmPackage.ATTRIBUTE_ASSIGNMENT: {
			AttributeAssignment attributeAssignment = (AttributeAssignment) theEObject;
			T result = caseAttributeAssignment(attributeAssignment);
			if (result == null)
				result = caseNamedElement(attributeAssignment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SdmPackage.STORY_PATTERN_ELEMENT: {
			StoryPatternElement storyPatternElement = (StoryPatternElement) theEObject;
			T result = caseStoryPatternElement(storyPatternElement);
			if (result == null)
				result = caseNamedElement(storyPatternElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SdmPackage.ABSTRACT_STORY_PATTERN_LINK: {
			AbstractStoryPatternLink abstractStoryPatternLink = (AbstractStoryPatternLink) theEObject;
			T result = caseAbstractStoryPatternLink(abstractStoryPatternLink);
			if (result == null)
				result = caseStoryPatternElement(abstractStoryPatternLink);
			if (result == null)
				result = caseNamedElement(abstractStoryPatternLink);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SdmPackage.STORY_PATTERN_LINK: {
			StoryPatternLink storyPatternLink = (StoryPatternLink) theEObject;
			T result = caseStoryPatternLink(storyPatternLink);
			if (result == null)
				result = caseAbstractStoryPatternLink(storyPatternLink);
			if (result == null)
				result = caseStoryPatternElement(storyPatternLink);
			if (result == null)
				result = caseNamedElement(storyPatternLink);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SdmPackage.STORY_PATTERN_CONTAINMENT_LINK: {
			StoryPatternContainmentLink storyPatternContainmentLink = (StoryPatternContainmentLink) theEObject;
			T result = caseStoryPatternContainmentLink(storyPatternContainmentLink);
			if (result == null)
				result = caseAbstractStoryPatternLink(storyPatternContainmentLink);
			if (result == null)
				result = caseStoryPatternElement(storyPatternContainmentLink);
			if (result == null)
				result = caseNamedElement(storyPatternContainmentLink);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SdmPackage.STORY_PATTERN_EXPRESSION_LINK: {
			StoryPatternExpressionLink storyPatternExpressionLink = (StoryPatternExpressionLink) theEObject;
			T result = caseStoryPatternExpressionLink(storyPatternExpressionLink);
			if (result == null)
				result = caseAbstractStoryPatternLink(storyPatternExpressionLink);
			if (result == null)
				result = caseStoryPatternElement(storyPatternExpressionLink);
			if (result == null)
				result = caseNamedElement(storyPatternExpressionLink);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SdmPackage.STORY_PATTERN_OBJECT: {
			StoryPatternObject storyPatternObject = (StoryPatternObject) theEObject;
			T result = caseStoryPatternObject(storyPatternObject);
			if (result == null)
				result = caseAbstractStoryPatternObject(storyPatternObject);
			if (result == null)
				result = caseStoryPatternElement(storyPatternObject);
			if (result == null)
				result = caseNamedElement(storyPatternObject);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SdmPackage.ABSTRACT_STORY_PATTERN_OBJECT: {
			AbstractStoryPatternObject abstractStoryPatternObject = (AbstractStoryPatternObject) theEObject;
			T result = caseAbstractStoryPatternObject(abstractStoryPatternObject);
			if (result == null)
				result = caseStoryPatternElement(abstractStoryPatternObject);
			if (result == null)
				result = caseNamedElement(abstractStoryPatternObject);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK: {
			MapEntryStoryPatternLink mapEntryStoryPatternLink = (MapEntryStoryPatternLink) theEObject;
			T result = caseMapEntryStoryPatternLink(mapEntryStoryPatternLink);
			if (result == null)
				result = caseAbstractStoryPatternLink(mapEntryStoryPatternLink);
			if (result == null)
				result = caseStoryPatternElement(mapEntryStoryPatternLink);
			if (result == null)
				result = caseNamedElement(mapEntryStoryPatternLink);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SdmPackage.ECONTAINER_STORY_PATTERN_LINK: {
			EContainerStoryPatternLink eContainerStoryPatternLink = (EContainerStoryPatternLink) theEObject;
			T result = caseEContainerStoryPatternLink(eContainerStoryPatternLink);
			if (result == null)
				result = caseAbstractStoryPatternLink(eContainerStoryPatternLink);
			if (result == null)
				result = caseStoryPatternElement(eContainerStoryPatternLink);
			if (result == null)
				result = caseNamedElement(eContainerStoryPatternLink);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SdmPackage.LINK_POSITION_CONSTRAINT: {
			LinkPositionConstraint linkPositionConstraint = (LinkPositionConstraint) theEObject;
			T result = caseLinkPositionConstraint(linkPositionConstraint);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SdmPackage.LINK_ORDER_CONSTRAINT: {
			LinkOrderConstraint linkOrderConstraint = (LinkOrderConstraint) theEObject;
			T result = caseLinkOrderConstraint(linkOrderConstraint);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SdmPackage.EXTERNAL_REFERENCE: {
			ExternalReference externalReference = (ExternalReference) theEObject;
			T result = caseExternalReference(externalReference);
			if (result == null)
				result = caseNamedElement(externalReference);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttributeAssignment(AttributeAssignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Story Pattern Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Story Pattern Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStoryPatternElement(StoryPatternElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Story Pattern Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Story Pattern Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractStoryPatternLink(AbstractStoryPatternLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Story Pattern Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Story Pattern Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStoryPatternLink(StoryPatternLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Story Pattern Containment Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Story Pattern Containment Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStoryPatternContainmentLink(StoryPatternContainmentLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Story Pattern Expression Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Story Pattern Expression Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStoryPatternExpressionLink(StoryPatternExpressionLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Story Pattern Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Story Pattern Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStoryPatternObject(StoryPatternObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Story Pattern Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Story Pattern Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractStoryPatternObject(AbstractStoryPatternObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Map Entry Story Pattern Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Map Entry Story Pattern Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMapEntryStoryPatternLink(MapEntryStoryPatternLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EContainer Story Pattern Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EContainer Story Pattern Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEContainerStoryPatternLink(EContainerStoryPatternLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Link Position Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Link Position Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLinkPositionConstraint(LinkPositionConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Link Order Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Link Order Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLinkOrderConstraint(LinkOrderConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>External Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>External Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExternalReference(ExternalReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SdmSwitch
