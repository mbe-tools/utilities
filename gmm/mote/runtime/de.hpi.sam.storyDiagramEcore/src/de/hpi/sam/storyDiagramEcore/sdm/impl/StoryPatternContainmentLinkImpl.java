/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import org.eclipse.emf.ecore.EClass;

import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Story Pattern Containment Link</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class StoryPatternContainmentLinkImpl extends
		AbstractStoryPatternLinkImpl implements StoryPatternContainmentLink {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected StoryPatternContainmentLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SdmPackage.Literals.STORY_PATTERN_CONTAINMENT_LINK;
	}

} // StoryPatternContainmentLinkImpl
