/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Link Order Constraint</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Specifies an ordering constraint between two links. The target object of the
 * second link must be a direct or indirect successor of the first link (the link
 * associated to this link constraint). If the pattern is matched and
 * constraintType is INDIRECT_SUCCESSOR, the target object of the second
 * link is matched to the closest successeeding object in the reference. An
 * important condition is that both story pattern links refer to the same 
 * collection of elements, i.e. the same reference in case of StoryPatternLinks.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getConstraintType <em>Constraint Type</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getPredecessorLink <em>Predecessor Link</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getSuccessorLink <em>Successor Link</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getLinkOrderConstraint()
 * @model
 * @generated
 */
public interface LinkOrderConstraint extends EObject {
	/**
	 * Returns the value of the '<em><b>Constraint Type</b></em>' attribute. The
	 * literals are from the enumeration
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration}.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The type of ordering constraint, either direct or indirect successor.
	 * <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Constraint Type</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration
	 * @see #setConstraintType(LinkOrderConstraintEnumeration)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getLinkOrderConstraint_ConstraintType()
	 * @model
	 * @generated
	 */
	LinkOrderConstraintEnumeration getConstraintType();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getConstraintType <em>Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Constraint Type</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration
	 * @see #getConstraintType()
	 * @generated
	 */
	void setConstraintType(LinkOrderConstraintEnumeration value);

	/**
	 * Returns the value of the '<em><b>Predecessor Link</b></em>' container
	 * reference. It is bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getOutgoingLinkOrderConstraints
	 * <em>Outgoing Link Order Constraints</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The link whose target object
	 * must be a direct or indirect successor of the target object of this
	 * constraint's associated link. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Predecessor Link</em>' container reference.
	 * @see #setPredecessorLink(StoryPatternLink)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getLinkOrderConstraint_PredecessorLink()
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getOutgoingLinkOrderConstraints
	 * @model opposite="outgoingLinkOrderConstraints" required="true"
	 *        transient="false" ordered="false"
	 * @generated
	 */
	StoryPatternLink getPredecessorLink();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getPredecessorLink <em>Predecessor Link</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Predecessor Link</em>' container reference.
	 * @see #getPredecessorLink()
	 * @generated
	 */
	void setPredecessorLink(StoryPatternLink value);

	/**
	 * Returns the value of the '<em><b>Successor Link</b></em>' reference. It
	 * is bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getIncomingLinkOrderConstraints
	 * <em>Incoming Link Order Constraints</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The story pattern link to which
	 * this link constraint is associated. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Successor Link</em>' reference.
	 * @see #setSuccessorLink(StoryPatternLink)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getLinkOrderConstraint_SuccessorLink()
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getIncomingLinkOrderConstraints
	 * @model opposite="incomingLinkOrderConstraints" required="true"
	 *        ordered="false"
	 * @generated
	 */
	StoryPatternLink getSuccessorLink();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getSuccessorLink <em>Successor Link</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Successor Link</em>' reference.
	 * @see #getSuccessorLink()
	 * @generated
	 */
	void setSuccessorLink(StoryPatternLink value);

} // LinkOrderConstraint
