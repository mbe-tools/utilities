/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.DecisionNode;
import de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.ForEachSemanticsEnumeration;
import de.hpi.sam.storyDiagramEcore.nodes.ForkNode;
import de.hpi.sam.storyDiagramEcore.nodes.InitialNode;
import de.hpi.sam.storyDiagramEcore.nodes.JoinNode;
import de.hpi.sam.storyDiagramEcore.nodes.MergeNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesFactory;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge;
import de.hpi.sam.storyDiagramEcore.nodes.Semaphore;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class NodesFactoryImpl extends EFactoryImpl implements NodesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static NodesFactory init() {
		try {
			NodesFactory theNodesFactory = (NodesFactory) EPackage.Registry.INSTANCE
					.getEFactory(NodesPackage.eNS_URI);
			if (theNodesFactory != null) {
				return theNodesFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new NodesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public NodesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case NodesPackage.ACTIVITY_EDGE:
			return createActivityEdge();
		case NodesPackage.ACTIVITY_FINAL_NODE:
			return createActivityFinalNode();
		case NodesPackage.FLOW_FINAL_NODE:
			return createFlowFinalNode();
		case NodesPackage.EXPRESSION_ACTIVITY_NODE:
			return createExpressionActivityNode();
		case NodesPackage.DECISION_NODE:
			return createDecisionNode();
		case NodesPackage.INITIAL_NODE:
			return createInitialNode();
		case NodesPackage.MERGE_NODE:
			return createMergeNode();
		case NodesPackage.STORY_ACTION_NODE:
			return createStoryActionNode();
		case NodesPackage.FORK_NODE:
			return createForkNode();
		case NodesPackage.JOIN_NODE:
			return createJoinNode();
		case NodesPackage.SEMAPHORE:
			return createSemaphore();
		case NodesPackage.RELEASE_SEMAPHORE_EDGE:
			return createReleaseSemaphoreEdge();
		case NodesPackage.ACQUIRE_SEMAPHORE_EDGE:
			return createAcquireSemaphoreEdge();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case NodesPackage.ACTIVITY_EDGE_GUARD_ENUMERATION:
			return createActivityEdgeGuardEnumerationFromString(eDataType,
					initialValue);
		case NodesPackage.FOR_EACH_SEMANTICS_ENUMERATION:
			return createForEachSemanticsEnumerationFromString(eDataType,
					initialValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case NodesPackage.ACTIVITY_EDGE_GUARD_ENUMERATION:
			return convertActivityEdgeGuardEnumerationToString(eDataType,
					instanceValue);
		case NodesPackage.FOR_EACH_SEMANTICS_ENUMERATION:
			return convertForEachSemanticsEnumerationToString(eDataType,
					instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityEdge createActivityEdge() {
		ActivityEdgeImpl activityEdge = new ActivityEdgeImpl();
		return activityEdge;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityFinalNode createActivityFinalNode() {
		ActivityFinalNodeImpl activityFinalNode = new ActivityFinalNodeImpl();
		return activityFinalNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public FlowFinalNode createFlowFinalNode() {
		FlowFinalNodeImpl flowFinalNode = new FlowFinalNodeImpl();
		return flowFinalNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionActivityNode createExpressionActivityNode() {
		ExpressionActivityNodeImpl expressionActivityNode = new ExpressionActivityNodeImpl();
		return expressionActivityNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public DecisionNode createDecisionNode() {
		DecisionNodeImpl decisionNode = new DecisionNodeImpl();
		return decisionNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public InitialNode createInitialNode() {
		InitialNodeImpl initialNode = new InitialNodeImpl();
		return initialNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MergeNode createMergeNode() {
		MergeNodeImpl mergeNode = new MergeNodeImpl();
		return mergeNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryActionNode createStoryActionNode() {
		StoryActionNodeImpl storyActionNode = new StoryActionNodeImpl();
		return storyActionNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ForkNode createForkNode() {
		ForkNodeImpl forkNode = new ForkNodeImpl();
		return forkNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public JoinNode createJoinNode() {
		JoinNodeImpl joinNode = new JoinNodeImpl();
		return joinNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Semaphore createSemaphore() {
		SemaphoreImpl semaphore = new SemaphoreImpl();
		return semaphore;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ReleaseSemaphoreEdge createReleaseSemaphoreEdge() {
		ReleaseSemaphoreEdgeImpl releaseSemaphoreEdge = new ReleaseSemaphoreEdgeImpl();
		return releaseSemaphoreEdge;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AcquireSemaphoreEdge createAcquireSemaphoreEdge() {
		AcquireSemaphoreEdgeImpl acquireSemaphoreEdge = new AcquireSemaphoreEdgeImpl();
		return acquireSemaphoreEdge;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityEdgeGuardEnumeration createActivityEdgeGuardEnumerationFromString(
			EDataType eDataType, String initialValue) {
		ActivityEdgeGuardEnumeration result = ActivityEdgeGuardEnumeration
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActivityEdgeGuardEnumerationToString(
			EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ForEachSemanticsEnumeration createForEachSemanticsEnumerationFromString(
			EDataType eDataType, String initialValue) {
		ForEachSemanticsEnumeration result = ForEachSemanticsEnumeration
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertForEachSemanticsEnumerationToString(
			EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NodesPackage getNodesPackage() {
		return (NodesPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static NodesPackage getPackage() {
		return NodesPackage.eINSTANCE;
	}

} // NodesFactoryImpl
