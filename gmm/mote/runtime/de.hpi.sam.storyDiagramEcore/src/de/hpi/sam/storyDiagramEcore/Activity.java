/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EOperation;

import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.Semaphore;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Activity</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The activity describes the behaviour of a method (pointed to by specification). It contains several ActivityNodes that are connected by ActivityEdges. There must be exactly one InitialNode and at least one ActivityFinalNode.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.Activity#getNodes <em>Nodes</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.Activity#getSpecification <em>Specification</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.Activity#getEdges <em>Edges</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.Activity#getSemaphores <em>Semaphores</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.Activity#getImports <em>Imports</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.Activity#getParameters <em>Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getActivity()
 * @model
 * @generated
 */
public interface Activity extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' containment reference
	 * list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode}. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getActivity
	 * <em>Activity</em>}'. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> The action nodes, that are contained in this
	 * activity. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Nodes</em>' containment reference list.
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getActivity_Nodes()
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getActivity
	 * @model opposite="activity" containment="true"
	 * @generated
	 */
	EList<ActivityNode> getNodes();

	/**
	 * Returns the value of the '<em><b>Specification</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * EOperation whose behavior is modeled by this activity. <!-- end-model-doc
	 * -->
	 * 
	 * @return the value of the '<em>Specification</em>' reference.
	 * @see #setSpecification(EOperation)
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getActivity_Specification()
	 * @model
	 * @generated
	 */
	EOperation getSpecification();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.Activity#getSpecification <em>Specification</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Specification</em>' reference.
	 * @see #getSpecification()
	 * @generated
	 */
	void setSpecification(EOperation value);

	/**
	 * Returns the value of the '<em><b>Edges</b></em>' containment reference
	 * list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge}. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getActivity
	 * <em>Activity</em>}'. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> Edges, that connect the activity nodes of the
	 * activity. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Edges</em>' containment reference list.
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getActivity_Edges()
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getActivity
	 * @model opposite="activity" containment="true"
	 * @generated
	 */
	EList<ActivityEdge> getEdges();

	/**
	 * Returns the value of the '<em><b>Semaphores</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.Semaphore}. <!-- begin-user-doc
	 * --> <!-- end-user-doc --> <!-- begin-model-doc --> The semaphores
	 * contained in this activity. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Semaphores</em>' containment reference
	 *         list.
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getActivity_Semaphores()
	 * @model containment="true"
	 * @generated
	 */
	EList<Semaphore> getSemaphores();

	/**
	 * Returns the value of the '<em><b>Imports</b></em>' containment reference list.
	 * The list contents are of type {@link de.hpi.sam.storyDiagramEcore.Import}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imports</em>' containment reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imports</em>' containment reference list.
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getActivity_Imports()
	 * @model containment="true"
	 * @generated
	 */
	EList<Import> getImports();

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.ActivityParameter}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parameters</em>' containment reference
	 *         list.
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getActivity_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActivityParameter> getParameters();

} // Activity
