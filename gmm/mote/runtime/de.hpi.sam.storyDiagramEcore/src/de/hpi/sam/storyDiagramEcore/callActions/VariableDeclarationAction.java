/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Variable Declaration Action</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The VariableDeclarationAction declares a variable along with its type. A CallAction can be used to assign a value to the variable. If a variable with that name is already in use, it is overwritten.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction#getValueAssignment <em>Value Assignment</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getVariableDeclarationAction()
 * @model
 * @generated
 */
public interface VariableDeclarationAction extends VariableReferenceAction {
	/**
	 * Returns the value of the '<em><b>Value Assignment</b></em>' containment
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> A CallAction that is evaluated. The value will be
	 * assigned to the declared variable name. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Value Assignment</em>' containment
	 *         reference.
	 * @see #setValueAssignment(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getVariableDeclarationAction_ValueAssignment()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getValueAssignment();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction#getValueAssignment <em>Value Assignment</em>}' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Assignment</em>' containment reference.
	 * @see #getValueAssignment()
	 * @generated
	 */
	void setValueAssignment(Expression value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (eIsProxy()) return super.toString();\r\n\r\nStringBuffer buffer = new StringBuffer();\r\n\r\nif (getClassifier() != null && getClassifier().getName() != null && !\"\".equals(getClassifier().getName()))\r\n{\r\n\tbuffer.append(getClassifier().getName());\r\n}\r\nelse\r\n{\r\n\tbuffer.append(\"[null]\");\r\n}\r\n\r\nbuffer.append(\" \");\r\nbuffer.append(getVariableName());\r\nbuffer.append(\" := \");\r\n\r\nif (getValueAssignment() != null)\r\n{\r\n\tbuffer.append(getValueAssignment().toString());\r\n}\r\nelse\r\n{\r\n\tbuffer.append(\"[null]\");\r\n}\r\n\r\nreturn buffer.toString().trim();'"
	 * @generated
	 */
	String toString();

} // VariableDeclarationAction
