/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.impl;

import de.hpi.sam.storyDiagramEcore.callActions.AbstractComparator;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.CompareAction;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Compare Action</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CompareActionImpl#getExpression1 <em>Expression1</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CompareActionImpl#getExpression2 <em>Expression2</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CompareActionImpl#getComparator <em>Comparator</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CompareActionImpl extends CallActionImpl implements CompareAction {
	/**
	 * The cached value of the '{@link #getExpression1() <em>Expression1</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getExpression1()
	 * @generated
	 * @ordered
	 */
	protected Expression expression1;

	/**
	 * The cached value of the '{@link #getExpression2() <em>Expression2</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getExpression2()
	 * @generated
	 * @ordered
	 */
	protected Expression expression2;

	/**
	 * The cached value of the '{@link #getComparator() <em>Comparator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComparator()
	 * @generated
	 * @ordered
	 */
	protected AbstractComparator comparator;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected CompareActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CallActionsPackage.Literals.COMPARE_ACTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getExpression1() {
		return expression1;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpression1(Expression newExpression1,
			NotificationChain msgs) {
		Expression oldExpression1 = expression1;
		expression1 = newExpression1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					CallActionsPackage.COMPARE_ACTION__EXPRESSION1,
					oldExpression1, newExpression1);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpression1(Expression newExpression1) {
		if (newExpression1 != expression1) {
			NotificationChain msgs = null;
			if (expression1 != null)
				msgs = ((InternalEObject) expression1)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.COMPARE_ACTION__EXPRESSION1,
								null, msgs);
			if (newExpression1 != null)
				msgs = ((InternalEObject) newExpression1)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.COMPARE_ACTION__EXPRESSION1,
								null, msgs);
			msgs = basicSetExpression1(newExpression1, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CallActionsPackage.COMPARE_ACTION__EXPRESSION1,
					newExpression1, newExpression1));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getExpression2() {
		return expression2;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpression2(Expression newExpression2,
			NotificationChain msgs) {
		Expression oldExpression2 = expression2;
		expression2 = newExpression2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					CallActionsPackage.COMPARE_ACTION__EXPRESSION2,
					oldExpression2, newExpression2);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpression2(Expression newExpression2) {
		if (newExpression2 != expression2) {
			NotificationChain msgs = null;
			if (expression2 != null)
				msgs = ((InternalEObject) expression2)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.COMPARE_ACTION__EXPRESSION2,
								null, msgs);
			if (newExpression2 != null)
				msgs = ((InternalEObject) newExpression2)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.COMPARE_ACTION__EXPRESSION2,
								null, msgs);
			msgs = basicSetExpression2(newExpression2, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CallActionsPackage.COMPARE_ACTION__EXPRESSION2,
					newExpression2, newExpression2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractComparator getComparator() {
		return comparator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComparator(
			AbstractComparator newComparator, NotificationChain msgs) {
		AbstractComparator oldComparator = comparator;
		comparator = newComparator;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					CallActionsPackage.COMPARE_ACTION__COMPARATOR,
					oldComparator, newComparator);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComparator(AbstractComparator newComparator) {
		if (newComparator != comparator) {
			NotificationChain msgs = null;
			if (comparator != null)
				msgs = ((InternalEObject) comparator)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.COMPARE_ACTION__COMPARATOR,
								null, msgs);
			if (newComparator != null)
				msgs = ((InternalEObject) newComparator)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.COMPARE_ACTION__COMPARATOR,
								null, msgs);
			msgs = basicSetComparator(newComparator, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CallActionsPackage.COMPARE_ACTION__COMPARATOR,
					newComparator, newComparator));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer buffer = new StringBuffer();

		if (getExpression1() != null) {
			buffer.append(getExpression1().toString());
		} else {
			buffer.append("[null]");
		}

		buffer.append(" = ");

		if (getExpression2() != null) {
			buffer.append(getExpression2().toString());
		} else {
			buffer.append("[null]");
		}

		return buffer.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CallActionsPackage.COMPARE_ACTION__EXPRESSION1:
			return basicSetExpression1(null, msgs);
		case CallActionsPackage.COMPARE_ACTION__EXPRESSION2:
			return basicSetExpression2(null, msgs);
		case CallActionsPackage.COMPARE_ACTION__COMPARATOR:
			return basicSetComparator(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CallActionsPackage.COMPARE_ACTION__EXPRESSION1:
			return getExpression1();
		case CallActionsPackage.COMPARE_ACTION__EXPRESSION2:
			return getExpression2();
		case CallActionsPackage.COMPARE_ACTION__COMPARATOR:
			return getComparator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CallActionsPackage.COMPARE_ACTION__EXPRESSION1:
			setExpression1((Expression) newValue);
			return;
		case CallActionsPackage.COMPARE_ACTION__EXPRESSION2:
			setExpression2((Expression) newValue);
			return;
		case CallActionsPackage.COMPARE_ACTION__COMPARATOR:
			setComparator((AbstractComparator) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CallActionsPackage.COMPARE_ACTION__EXPRESSION1:
			setExpression1((Expression) null);
			return;
		case CallActionsPackage.COMPARE_ACTION__EXPRESSION2:
			setExpression2((Expression) null);
			return;
		case CallActionsPackage.COMPARE_ACTION__COMPARATOR:
			setComparator((AbstractComparator) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CallActionsPackage.COMPARE_ACTION__EXPRESSION1:
			return expression1 != null;
		case CallActionsPackage.COMPARE_ACTION__EXPRESSION2:
			return expression2 != null;
		case CallActionsPackage.COMPARE_ACTION__COMPARATOR:
			return comparator != null;
		}
		return super.eIsSet(featureID);
	}

} // CompareActionImpl
