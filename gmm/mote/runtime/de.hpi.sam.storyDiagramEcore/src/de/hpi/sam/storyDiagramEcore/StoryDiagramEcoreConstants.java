package de.hpi.sam.storyDiagramEcore;

public interface StoryDiagramEcoreConstants
{
	public static final String	EXPRESSION_LANGUAGES_EXTENSION_POINT_ID					= "de.hpi.sam.storyDiagramEcore.expressionLanguagesExtension";
	public static final String	EXPRESSION_LANGUAGES_EXPRESSION_LANGUAGE_ATTRIBUTE_NAME	= "expressionLanguage";
}
