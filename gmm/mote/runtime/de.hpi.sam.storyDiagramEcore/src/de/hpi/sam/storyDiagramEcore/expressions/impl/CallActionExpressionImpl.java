/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.expressions.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Call Action Expression</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.expressions.impl.CallActionExpressionImpl#getCallActions <em>Call Actions</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CallActionExpressionImpl extends ExpressionImpl implements
		CallActionExpression {
	/**
	 * The cached value of the '{@link #getCallActions() <em>Call Actions</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCallActions()
	 * @generated
	 * @ordered
	 */
	protected EList<CallAction> callActions;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected CallActionExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.CALL_ACTION_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CallAction> getCallActions() {
		if (callActions == null) {
			callActions = new EObjectContainmentEList<CallAction>(
					CallAction.class, this,
					ExpressionsPackage.CALL_ACTION_EXPRESSION__CALL_ACTIONS);
		}
		return callActions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		if (getCallActions().isEmpty()) {
			return "[null]";
		} else {
			StringBuffer buffer = new StringBuffer();

			String NL = System.getProperty("line.separator");

			for (CallAction c : getCallActions()) {
				buffer.append(c.toString());
				buffer.append(NL);
			}

			return buffer.toString().trim();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.CALL_ACTION_EXPRESSION__CALL_ACTIONS:
			return ((InternalEList<?>) getCallActions()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.CALL_ACTION_EXPRESSION__CALL_ACTIONS:
			return getCallActions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.CALL_ACTION_EXPRESSION__CALL_ACTIONS:
			getCallActions().clear();
			getCallActions()
					.addAll((Collection<? extends CallAction>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.CALL_ACTION_EXPRESSION__CALL_ACTIONS:
			getCallActions().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.CALL_ACTION_EXPRESSION__CALL_ACTIONS:
			return callActions != null && !callActions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // CallActionExpressionImpl
