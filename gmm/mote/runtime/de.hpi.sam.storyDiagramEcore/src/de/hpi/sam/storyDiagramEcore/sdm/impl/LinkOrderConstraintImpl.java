/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Link Order Constraint</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.LinkOrderConstraintImpl#getConstraintType <em>Constraint Type</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.LinkOrderConstraintImpl#getPredecessorLink <em>Predecessor Link</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.LinkOrderConstraintImpl#getSuccessorLink <em>Successor Link</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LinkOrderConstraintImpl extends EObjectImpl implements
		LinkOrderConstraint {
	/**
	 * The default value of the '{@link #getConstraintType() <em>Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getConstraintType()
	 * @generated
	 * @ordered
	 */
	protected static final LinkOrderConstraintEnumeration CONSTRAINT_TYPE_EDEFAULT = LinkOrderConstraintEnumeration.DIRECT_SUCCESSOR;

	/**
	 * The cached value of the '{@link #getConstraintType() <em>Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getConstraintType()
	 * @generated
	 * @ordered
	 */
	protected LinkOrderConstraintEnumeration constraintType = CONSTRAINT_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSuccessorLink() <em>Successor Link</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSuccessorLink()
	 * @generated
	 * @ordered
	 */
	protected StoryPatternLink successorLink;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkOrderConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SdmPackage.Literals.LINK_ORDER_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LinkOrderConstraintEnumeration getConstraintType() {
		return constraintType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraintType(
			LinkOrderConstraintEnumeration newConstraintType) {
		LinkOrderConstraintEnumeration oldConstraintType = constraintType;
		constraintType = newConstraintType == null ? CONSTRAINT_TYPE_EDEFAULT
				: newConstraintType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.LINK_ORDER_CONSTRAINT__CONSTRAINT_TYPE,
					oldConstraintType, constraintType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternLink getPredecessorLink() {
		if (eContainerFeatureID() != SdmPackage.LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK)
			return null;
		return (StoryPatternLink) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPredecessorLink(
			StoryPatternLink newPredecessorLink, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newPredecessorLink,
				SdmPackage.LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPredecessorLink(StoryPatternLink newPredecessorLink) {
		if (newPredecessorLink != eInternalContainer()
				|| (eContainerFeatureID() != SdmPackage.LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK && newPredecessorLink != null)) {
			if (EcoreUtil.isAncestor(this, newPredecessorLink))
				throw new IllegalArgumentException(
						"Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newPredecessorLink != null)
				msgs = ((InternalEObject) newPredecessorLink)
						.eInverseAdd(
								this,
								SdmPackage.STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS,
								StoryPatternLink.class, msgs);
			msgs = basicSetPredecessorLink(newPredecessorLink, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK,
					newPredecessorLink, newPredecessorLink));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternLink getSuccessorLink() {
		if (successorLink != null && successorLink.eIsProxy()) {
			InternalEObject oldSuccessorLink = (InternalEObject) successorLink;
			successorLink = (StoryPatternLink) eResolveProxy(oldSuccessorLink);
			if (successorLink != oldSuccessorLink) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SdmPackage.LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK,
							oldSuccessorLink, successorLink));
			}
		}
		return successorLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternLink basicGetSuccessorLink() {
		return successorLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuccessorLink(
			StoryPatternLink newSuccessorLink, NotificationChain msgs) {
		StoryPatternLink oldSuccessorLink = successorLink;
		successorLink = newSuccessorLink;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					SdmPackage.LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK,
					oldSuccessorLink, newSuccessorLink);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuccessorLink(StoryPatternLink newSuccessorLink) {
		if (newSuccessorLink != successorLink) {
			NotificationChain msgs = null;
			if (successorLink != null)
				msgs = ((InternalEObject) successorLink)
						.eInverseRemove(
								this,
								SdmPackage.STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS,
								StoryPatternLink.class, msgs);
			if (newSuccessorLink != null)
				msgs = ((InternalEObject) newSuccessorLink)
						.eInverseAdd(
								this,
								SdmPackage.STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS,
								StoryPatternLink.class, msgs);
			msgs = basicSetSuccessorLink(newSuccessorLink, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK,
					newSuccessorLink, newSuccessorLink));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SdmPackage.LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetPredecessorLink((StoryPatternLink) otherEnd, msgs);
		case SdmPackage.LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK:
			if (successorLink != null)
				msgs = ((InternalEObject) successorLink)
						.eInverseRemove(
								this,
								SdmPackage.STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS,
								StoryPatternLink.class, msgs);
			return basicSetSuccessorLink((StoryPatternLink) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SdmPackage.LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK:
			return basicSetPredecessorLink(null, msgs);
		case SdmPackage.LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK:
			return basicSetSuccessorLink(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(
			NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case SdmPackage.LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK:
			return eInternalContainer()
					.eInverseRemove(
							this,
							SdmPackage.STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS,
							StoryPatternLink.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SdmPackage.LINK_ORDER_CONSTRAINT__CONSTRAINT_TYPE:
			return getConstraintType();
		case SdmPackage.LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK:
			return getPredecessorLink();
		case SdmPackage.LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK:
			if (resolve)
				return getSuccessorLink();
			return basicGetSuccessorLink();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SdmPackage.LINK_ORDER_CONSTRAINT__CONSTRAINT_TYPE:
			setConstraintType((LinkOrderConstraintEnumeration) newValue);
			return;
		case SdmPackage.LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK:
			setPredecessorLink((StoryPatternLink) newValue);
			return;
		case SdmPackage.LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK:
			setSuccessorLink((StoryPatternLink) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SdmPackage.LINK_ORDER_CONSTRAINT__CONSTRAINT_TYPE:
			setConstraintType(CONSTRAINT_TYPE_EDEFAULT);
			return;
		case SdmPackage.LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK:
			setPredecessorLink((StoryPatternLink) null);
			return;
		case SdmPackage.LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK:
			setSuccessorLink((StoryPatternLink) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SdmPackage.LINK_ORDER_CONSTRAINT__CONSTRAINT_TYPE:
			return constraintType != CONSTRAINT_TYPE_EDEFAULT;
		case SdmPackage.LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK:
			return getPredecessorLink() != null;
		case SdmPackage.LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK:
			return successorLink != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (constraintType: ");
		result.append(constraintType);
		result.append(')');
		return result.toString();
	}

} // LinkOrderConstraintImpl
