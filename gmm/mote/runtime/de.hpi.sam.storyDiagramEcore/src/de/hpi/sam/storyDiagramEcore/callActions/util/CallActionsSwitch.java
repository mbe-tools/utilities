/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.callActions.*;
import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction;
import de.hpi.sam.storyDiagramEcore.callActions.CompareAction;
import de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction;
import de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction;
import de.hpi.sam.storyDiagramEcore.callActions.NullValueAction;
import de.hpi.sam.storyDiagramEcore.callActions.OperationAction;
import de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 <<<<<<< .mine
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage
 * @generated
 */
public class CallActionsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CallActionsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallActionsSwitch() {
		if (modelPackage == null) {
			modelPackage = CallActionsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case CallActionsPackage.CALL_ACTION: {
			CallAction callAction = (CallAction) theEObject;
			T result = caseCallAction(callAction);
			if (result == null)
				result = caseNamedElement(callAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.CALL_ACTION_PARAMETER: {
			CallActionParameter callActionParameter = (CallActionParameter) theEObject;
			T result = caseCallActionParameter(callActionParameter);
			if (result == null)
				result = caseNamedElement(callActionParameter);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION: {
			CallStoryDiagramInterpreterAction callStoryDiagramInterpreterAction = (CallStoryDiagramInterpreterAction) theEObject;
			T result = caseCallStoryDiagramInterpreterAction(callStoryDiagramInterpreterAction);
			if (result == null)
				result = caseCallAction(callStoryDiagramInterpreterAction);
			if (result == null)
				result = caseNamedElement(callStoryDiagramInterpreterAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.LITERAL_DECLARATION_ACTION: {
			LiteralDeclarationAction literalDeclarationAction = (LiteralDeclarationAction) theEObject;
			T result = caseLiteralDeclarationAction(literalDeclarationAction);
			if (result == null)
				result = caseCallAction(literalDeclarationAction);
			if (result == null)
				result = caseNamedElement(literalDeclarationAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.METHOD_CALL_ACTION: {
			MethodCallAction methodCallAction = (MethodCallAction) theEObject;
			T result = caseMethodCallAction(methodCallAction);
			if (result == null)
				result = caseCallAction(methodCallAction);
			if (result == null)
				result = caseNamedElement(methodCallAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.NEW_OBJECT_ACTION: {
			NewObjectAction newObjectAction = (NewObjectAction) theEObject;
			T result = caseNewObjectAction(newObjectAction);
			if (result == null)
				result = caseCallAction(newObjectAction);
			if (result == null)
				result = caseNamedElement(newObjectAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.VARIABLE_DECLARATION_ACTION: {
			VariableDeclarationAction variableDeclarationAction = (VariableDeclarationAction) theEObject;
			T result = caseVariableDeclarationAction(variableDeclarationAction);
			if (result == null)
				result = caseVariableReferenceAction(variableDeclarationAction);
			if (result == null)
				result = caseCallAction(variableDeclarationAction);
			if (result == null)
				result = caseNamedElement(variableDeclarationAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.VARIABLE_REFERENCE_ACTION: {
			VariableReferenceAction variableReferenceAction = (VariableReferenceAction) theEObject;
			T result = caseVariableReferenceAction(variableReferenceAction);
			if (result == null)
				result = caseCallAction(variableReferenceAction);
			if (result == null)
				result = caseNamedElement(variableReferenceAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.COMPARE_ACTION: {
			CompareAction compareAction = (CompareAction) theEObject;
			T result = caseCompareAction(compareAction);
			if (result == null)
				result = caseCallAction(compareAction);
			if (result == null)
				result = caseNamedElement(compareAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.NULL_VALUE_ACTION: {
			NullValueAction nullValueAction = (NullValueAction) theEObject;
			T result = caseNullValueAction(nullValueAction);
			if (result == null)
				result = caseCallAction(nullValueAction);
			if (result == null)
				result = caseNamedElement(nullValueAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.OPERATION_ACTION: {
			OperationAction operationAction = (OperationAction) theEObject;
			T result = caseOperationAction(operationAction);
			if (result == null)
				result = caseCallAction(operationAction);
			if (result == null)
				result = caseNamedElement(operationAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.ABSTRACT_COMPARATOR: {
			AbstractComparator abstractComparator = (AbstractComparator) theEObject;
			T result = caseAbstractComparator(abstractComparator);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.CASE_INSENSITIVE_COMPARATOR: {
			CaseInsensitiveComparator caseInsensitiveComparator = (CaseInsensitiveComparator) theEObject;
			T result = caseCaseInsensitiveComparator(caseInsensitiveComparator);
			if (result == null)
				result = caseAbstractComparator(caseInsensitiveComparator);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case CallActionsPackage.GET_PROPERTY_VALUE_ACTION: {
			GetPropertyValueAction getPropertyValueAction = (GetPropertyValueAction) theEObject;
			T result = caseGetPropertyValueAction(getPropertyValueAction);
			if (result == null)
				result = caseCallAction(getPropertyValueAction);
			if (result == null)
				result = caseNamedElement(getPropertyValueAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCallAction(CallAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Action Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Action Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCallActionParameter(CallActionParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Story Diagram Interpreter Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Story Diagram Interpreter Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCallStoryDiagramInterpreterAction(
			CallStoryDiagramInterpreterAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Literal Declaration Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Literal Declaration Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLiteralDeclarationAction(LiteralDeclarationAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Method Call Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Method Call Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMethodCallAction(MethodCallAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>New Object Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>New Object Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNewObjectAction(NewObjectAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Declaration Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Declaration Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableDeclarationAction(VariableDeclarationAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Reference Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Reference Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableReferenceAction(VariableReferenceAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Compare Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Compare Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompareAction(CompareAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null Value Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null Value Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNullValueAction(NullValueAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationAction(OperationAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Comparator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Comparator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractComparator(AbstractComparator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Case Insensitive Comparator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Case Insensitive Comparator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaseInsensitiveComparator(CaseInsensitiveComparator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Property Value Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Property Value Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetPropertyValueAction(GetPropertyValueAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //CallActionsSwitch
