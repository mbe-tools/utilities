/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.util;

import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc --> The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.util.StoryDiagramEcoreResourceFactoryImpl
 * @generated
 */
public class StoryDiagramEcoreResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param uri
	 *            the URI of the new resource.
	 * @generated NOT
	 */
	public StoryDiagramEcoreResourceImpl(URI uri) {
		super(uri);
		this.getDefaultLoadOptions().put(OPTION_DEFER_IDREF_RESOLUTION,
				Boolean.TRUE);
		this.setIntrinsicIDToEObjectMap(new HashMap<String, EObject>());
	}

} // StoryDiagramEcoreResourceImpl
