/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.impl;

import org.eclipse.emf.ecore.EClass;

import de.hpi.sam.storyDiagramEcore.nodes.ForkNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Fork Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ForkNodeImpl extends ActivityNodeImpl implements ForkNode {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ForkNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NodesPackage.Literals.FORK_NODE;
	}

} // ForkNodeImpl
