/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc --> <!-- begin-model-doc --> This package contains all
 * classes specific for story driven modeling. <!-- end-model-doc -->
 * 
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmFactory
 * @model kind="package"
 * @generated
 */
public interface SdmPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "sdm";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de/hpi/sam/storyDiagramEcore/sdm.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.hpi.sam.storyDiagramEcore.sdm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	SdmPackage eINSTANCE = de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AttributeAssignmentImpl <em>Attribute Assignment</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.AttributeAssignmentImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getAttributeAssignment()
	 * @generated
	 */
	int ATTRIBUTE_ASSIGNMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__ESTRUCTURAL_FEATURE = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Assignment Expression</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Custom Comparator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Attribute Assignment</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternElementImpl <em>Story Pattern Element</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternElementImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternElement()
	 * @generated
	 */
	int STORY_PATTERN_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_ELEMENT__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_ELEMENT__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_ELEMENT__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_ELEMENT__MODIFIER = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Match Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_ELEMENT__MATCH_TYPE = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Story Pattern Element</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_ELEMENT_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternLinkImpl <em>Abstract Story Pattern Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternLinkImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getAbstractStoryPatternLink()
	 * @generated
	 */
	int ABSTRACT_STORY_PATTERN_LINK = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_LINK__NAME = STORY_PATTERN_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_LINK__DESCRIPTION = STORY_PATTERN_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_LINK__UUID = STORY_PATTERN_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_LINK__MODIFIER = STORY_PATTERN_ELEMENT__MODIFIER;

	/**
	 * The feature id for the '<em><b>Match Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_LINK__MATCH_TYPE = STORY_PATTERN_ELEMENT__MATCH_TYPE;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_LINK__SOURCE = STORY_PATTERN_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_LINK__TARGET = STORY_PATTERN_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Check Only Existence</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE = STORY_PATTERN_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Matching Priority</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY = STORY_PATTERN_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Opposite Story Pattern Link</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK = STORY_PATTERN_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Abstract Story Pattern Link</em>' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT = STORY_PATTERN_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternLinkImpl <em>Story Pattern Link</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternLinkImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternLink()
	 * @generated
	 */
	int STORY_PATTERN_LINK = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__NAME = ABSTRACT_STORY_PATTERN_LINK__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__DESCRIPTION = ABSTRACT_STORY_PATTERN_LINK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__UUID = ABSTRACT_STORY_PATTERN_LINK__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__MODIFIER = ABSTRACT_STORY_PATTERN_LINK__MODIFIER;

	/**
	 * The feature id for the '<em><b>Match Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__MATCH_TYPE = ABSTRACT_STORY_PATTERN_LINK__MATCH_TYPE;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__SOURCE = ABSTRACT_STORY_PATTERN_LINK__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__TARGET = ABSTRACT_STORY_PATTERN_LINK__TARGET;

	/**
	 * The feature id for the '<em><b>Check Only Existence</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE = ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE;

	/**
	 * The feature id for the '<em><b>Matching Priority</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__MATCHING_PRIORITY = ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY;

	/**
	 * The feature id for the '<em><b>Opposite Story Pattern Link</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK = ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK;

	/**
	 * The feature id for the '<em><b>EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Link Position Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Outgoing Link Order Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Incoming Link Order Constraints</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>EOpposite Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__EOPPOSITE_REFERENCE = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>External Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK__EXTERNAL_REFERENCE = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Story Pattern Link</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_LINK_FEATURE_COUNT = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternContainmentLinkImpl <em>Story Pattern Containment Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternContainmentLinkImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternContainmentLink()
	 * @generated
	 */
	int STORY_PATTERN_CONTAINMENT_LINK = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_CONTAINMENT_LINK__NAME = ABSTRACT_STORY_PATTERN_LINK__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_CONTAINMENT_LINK__DESCRIPTION = ABSTRACT_STORY_PATTERN_LINK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_CONTAINMENT_LINK__UUID = ABSTRACT_STORY_PATTERN_LINK__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_CONTAINMENT_LINK__MODIFIER = ABSTRACT_STORY_PATTERN_LINK__MODIFIER;

	/**
	 * The feature id for the '<em><b>Match Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_CONTAINMENT_LINK__MATCH_TYPE = ABSTRACT_STORY_PATTERN_LINK__MATCH_TYPE;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_CONTAINMENT_LINK__SOURCE = ABSTRACT_STORY_PATTERN_LINK__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_CONTAINMENT_LINK__TARGET = ABSTRACT_STORY_PATTERN_LINK__TARGET;

	/**
	 * The feature id for the '<em><b>Check Only Existence</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_CONTAINMENT_LINK__CHECK_ONLY_EXISTENCE = ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE;

	/**
	 * The feature id for the '<em><b>Matching Priority</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_CONTAINMENT_LINK__MATCHING_PRIORITY = ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY;

	/**
	 * The feature id for the '<em><b>Opposite Story Pattern Link</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_CONTAINMENT_LINK__OPPOSITE_STORY_PATTERN_LINK = ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK;

	/**
	 * The number of structural features of the '<em>Story Pattern Containment Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_CONTAINMENT_LINK_FEATURE_COUNT = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternExpressionLinkImpl <em>Story Pattern Expression Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternExpressionLinkImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternExpressionLink()
	 * @generated
	 */
	int STORY_PATTERN_EXPRESSION_LINK = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_EXPRESSION_LINK__NAME = ABSTRACT_STORY_PATTERN_LINK__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_EXPRESSION_LINK__DESCRIPTION = ABSTRACT_STORY_PATTERN_LINK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_EXPRESSION_LINK__UUID = ABSTRACT_STORY_PATTERN_LINK__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_EXPRESSION_LINK__MODIFIER = ABSTRACT_STORY_PATTERN_LINK__MODIFIER;

	/**
	 * The feature id for the '<em><b>Match Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_EXPRESSION_LINK__MATCH_TYPE = ABSTRACT_STORY_PATTERN_LINK__MATCH_TYPE;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_EXPRESSION_LINK__SOURCE = ABSTRACT_STORY_PATTERN_LINK__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_EXPRESSION_LINK__TARGET = ABSTRACT_STORY_PATTERN_LINK__TARGET;

	/**
	 * The feature id for the '<em><b>Check Only Existence</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_EXPRESSION_LINK__CHECK_ONLY_EXISTENCE = ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE;

	/**
	 * The feature id for the '<em><b>Matching Priority</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_EXPRESSION_LINK__MATCHING_PRIORITY = ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY;

	/**
	 * The feature id for the '<em><b>Opposite Story Pattern Link</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_EXPRESSION_LINK__OPPOSITE_STORY_PATTERN_LINK = ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_EXPRESSION_LINK__EXPRESSION = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Story Pattern Expression Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_EXPRESSION_LINK_FEATURE_COUNT = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternObjectImpl <em>Abstract Story Pattern Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternObjectImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getAbstractStoryPatternObject()
	 * @generated
	 */
	int ABSTRACT_STORY_PATTERN_OBJECT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_OBJECT__NAME = STORY_PATTERN_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_OBJECT__DESCRIPTION = STORY_PATTERN_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_OBJECT__UUID = STORY_PATTERN_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_OBJECT__MODIFIER = STORY_PATTERN_ELEMENT__MODIFIER;

	/**
	 * The feature id for the '<em><b>Match Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_OBJECT__MATCH_TYPE = STORY_PATTERN_ELEMENT__MATCH_TYPE;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_OBJECT__CLASSIFIER = STORY_PATTERN_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Outgoing Story Links</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS = STORY_PATTERN_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Incoming Story Links</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS = STORY_PATTERN_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Create Model Object Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION = STORY_PATTERN_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Abstract Story Pattern Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STORY_PATTERN_OBJECT_FEATURE_COUNT = STORY_PATTERN_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternObjectImpl <em>Story Pattern Object</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternObjectImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternObject()
	 * @generated
	 */
	int STORY_PATTERN_OBJECT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__NAME = ABSTRACT_STORY_PATTERN_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__DESCRIPTION = ABSTRACT_STORY_PATTERN_OBJECT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__UUID = ABSTRACT_STORY_PATTERN_OBJECT__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__MODIFIER = ABSTRACT_STORY_PATTERN_OBJECT__MODIFIER;

	/**
	 * The feature id for the '<em><b>Match Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__MATCH_TYPE = ABSTRACT_STORY_PATTERN_OBJECT__MATCH_TYPE;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__CLASSIFIER = ABSTRACT_STORY_PATTERN_OBJECT__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Outgoing Story Links</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS = ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS;

	/**
	 * The feature id for the '<em><b>Incoming Story Links</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS = ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS;

	/**
	 * The feature id for the '<em><b>Create Model Object Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION = ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Binding Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__BINDING_TYPE = ABSTRACT_STORY_PATTERN_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute Assignments</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__ATTRIBUTE_ASSIGNMENTS = ABSTRACT_STORY_PATTERN_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__CONSTRAINTS = ABSTRACT_STORY_PATTERN_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Direct Assignment Expression</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION = ABSTRACT_STORY_PATTERN_OBJECT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Story Pattern Object</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STORY_PATTERN_OBJECT_FEATURE_COUNT = ABSTRACT_STORY_PATTERN_OBJECT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.MapEntryStoryPatternLinkImpl <em>Map Entry Story Pattern Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.MapEntryStoryPatternLinkImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getMapEntryStoryPatternLink()
	 * @generated
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__NAME = ABSTRACT_STORY_PATTERN_LINK__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__DESCRIPTION = ABSTRACT_STORY_PATTERN_LINK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__UUID = ABSTRACT_STORY_PATTERN_LINK__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__MODIFIER = ABSTRACT_STORY_PATTERN_LINK__MODIFIER;

	/**
	 * The feature id for the '<em><b>Match Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__MATCH_TYPE = ABSTRACT_STORY_PATTERN_LINK__MATCH_TYPE;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__SOURCE = ABSTRACT_STORY_PATTERN_LINK__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__TARGET = ABSTRACT_STORY_PATTERN_LINK__TARGET;

	/**
	 * The feature id for the '<em><b>Check Only Existence</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE = ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE;

	/**
	 * The feature id for the '<em><b>Matching Priority</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__MATCHING_PRIORITY = ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY;

	/**
	 * The feature id for the '<em><b>Opposite Story Pattern Link</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK = ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK;

	/**
	 * The feature id for the '<em><b>Value Target</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__VALUE_TARGET = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK__CLASSIFIER = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Map Entry Story Pattern Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_STORY_PATTERN_LINK_FEATURE_COUNT = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.EContainerStoryPatternLinkImpl <em>EContainer Story Pattern Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.EContainerStoryPatternLinkImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getEContainerStoryPatternLink()
	 * @generated
	 */
	int ECONTAINER_STORY_PATTERN_LINK = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ECONTAINER_STORY_PATTERN_LINK__NAME = ABSTRACT_STORY_PATTERN_LINK__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ECONTAINER_STORY_PATTERN_LINK__DESCRIPTION = ABSTRACT_STORY_PATTERN_LINK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ECONTAINER_STORY_PATTERN_LINK__UUID = ABSTRACT_STORY_PATTERN_LINK__UUID;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ECONTAINER_STORY_PATTERN_LINK__MODIFIER = ABSTRACT_STORY_PATTERN_LINK__MODIFIER;

	/**
	 * The feature id for the '<em><b>Match Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ECONTAINER_STORY_PATTERN_LINK__MATCH_TYPE = ABSTRACT_STORY_PATTERN_LINK__MATCH_TYPE;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ECONTAINER_STORY_PATTERN_LINK__SOURCE = ABSTRACT_STORY_PATTERN_LINK__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ECONTAINER_STORY_PATTERN_LINK__TARGET = ABSTRACT_STORY_PATTERN_LINK__TARGET;

	/**
	 * The feature id for the '<em><b>Check Only Existence</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECONTAINER_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE = ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE;

	/**
	 * The feature id for the '<em><b>Matching Priority</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECONTAINER_STORY_PATTERN_LINK__MATCHING_PRIORITY = ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY;

	/**
	 * The feature id for the '<em><b>Opposite Story Pattern Link</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECONTAINER_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK = ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK;

	/**
	 * The feature id for the '<em><b>Allow Indirect Containment</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECONTAINER_STORY_PATTERN_LINK__ALLOW_INDIRECT_CONTAINMENT = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>EContainer Story Pattern Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECONTAINER_STORY_PATTERN_LINK_FEATURE_COUNT = ABSTRACT_STORY_PATTERN_LINK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.LinkPositionConstraintImpl <em>Link Position Constraint</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.LinkPositionConstraintImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getLinkPositionConstraint()
	 * @generated
	 */
	int LINK_POSITION_CONSTRAINT = 10;

	/**
	 * The feature id for the '<em><b>Constraint Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Story Pattern Link</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK = 1;

	/**
	 * The number of structural features of the '<em>Link Position Constraint</em>' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_POSITION_CONSTRAINT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.LinkOrderConstraintImpl <em>Link Order Constraint</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.LinkOrderConstraintImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getLinkOrderConstraint()
	 * @generated
	 */
	int LINK_ORDER_CONSTRAINT = 11;

	/**
	 * The feature id for the '<em><b>Constraint Type</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int LINK_ORDER_CONSTRAINT__CONSTRAINT_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Predecessor Link</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK = 1;

	/**
	 * The feature id for the '<em><b>Successor Link</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK = 2;

	/**
	 * The number of structural features of the '<em>Link Order Constraint</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_ORDER_CONSTRAINT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.ExternalReferenceImpl <em>External Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.ExternalReferenceImpl
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getExternalReference()
	 * @generated
	 */
	int EXTERNAL_REFERENCE = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_REFERENCE__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_REFERENCE__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_REFERENCE__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_REFERENCE__EXPRESSION = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Many</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_REFERENCE__MANY = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>External Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_REFERENCE_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration <em>Binding Type Enumeration</em>}' enum.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getBindingTypeEnumeration()
	 * @generated
	 */
	int BINDING_TYPE_ENUMERATION = 13;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration <em>Story Pattern Match Type Enumeration</em>}' enum.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternMatchTypeEnumeration()
	 * @generated
	 */
	int STORY_PATTERN_MATCH_TYPE_ENUMERATION = 14;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration <em>Story Pattern Modifier Enumeration</em>}' enum.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternModifierEnumeration()
	 * @generated
	 */
	int STORY_PATTERN_MODIFIER_ENUMERATION = 15;

	/**
	 * The meta object id for the '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration
	 * <em>Story Pattern NAC Sematic Enumeration</em>}' enum. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternNACSematicEnumeration()
	 * @generated
	 */
	int STORY_PATTERN_NAC_SEMATIC_ENUMERATION = 16;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration <em>Link Position Constraint Enumeration</em>}' enum.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getLinkPositionConstraintEnumeration()
	 * @generated
	 */
	int LINK_POSITION_CONSTRAINT_ENUMERATION = 17;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration <em>Link Order Constraint Enumeration</em>}' enum.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration
	 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getLinkOrderConstraintEnumeration()
	 * @generated
	 */
	int LINK_ORDER_CONSTRAINT_ENUMERATION = 18;

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment <em>Attribute Assignment</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Attribute Assignment</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment
	 * @generated
	 */
	EClass getAttributeAssignment();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment#getEStructuralFeature
	 * <em>EStructural Feature</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>EStructural Feature</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment#getEStructuralFeature()
	 * @see #getAttributeAssignment()
	 * @generated
	 */
	EReference getAttributeAssignment_EStructuralFeature();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment#getAssignmentExpression <em>Assignment Expression</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference '<em>Assignment Expression</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment#getAssignmentExpression()
	 * @see #getAttributeAssignment()
	 * @generated
	 */
	EReference getAttributeAssignment_AssignmentExpression();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment#getCustomComparator <em>Custom Comparator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Custom Comparator</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment#getCustomComparator()
	 * @see #getAttributeAssignment()
	 * @generated
	 */
	EReference getAttributeAssignment_CustomComparator();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement <em>Story Pattern Element</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Story Pattern Element</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement
	 * @generated
	 */
	EClass getStoryPatternElement();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement#getModifier <em>Modifier</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Modifier</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement#getModifier()
	 * @see #getStoryPatternElement()
	 * @generated
	 */
	EAttribute getStoryPatternElement_Modifier();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement#getMatchType <em>Match Type</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Match Type</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement#getMatchType()
	 * @see #getStoryPatternElement()
	 * @generated
	 */
	EAttribute getStoryPatternElement_MatchType();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink <em>Abstract Story Pattern Link</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Abstract Story Pattern Link</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink
	 * @generated
	 */
	EClass getAbstractStoryPatternLink();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getSource()
	 * @see #getAbstractStoryPatternLink()
	 * @generated
	 */
	EReference getAbstractStoryPatternLink_Source();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getTarget()
	 * @see #getAbstractStoryPatternLink()
	 * @generated
	 */
	EReference getAbstractStoryPatternLink_Target();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#isCheckOnlyExistence <em>Check Only Existence</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Check Only Existence</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#isCheckOnlyExistence()
	 * @see #getAbstractStoryPatternLink()
	 * @generated
	 */
	EAttribute getAbstractStoryPatternLink_CheckOnlyExistence();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getMatchingPriority
	 * <em>Matching Priority</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Matching Priority</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getMatchingPriority()
	 * @see #getAbstractStoryPatternLink()
	 * @generated
	 */
	EAttribute getAbstractStoryPatternLink_MatchingPriority();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getOppositeStoryPatternLink <em>Opposite Story Pattern Link</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference '<em>Opposite Story Pattern Link</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getOppositeStoryPatternLink()
	 * @see #getAbstractStoryPatternLink()
	 * @generated
	 */
	EReference getAbstractStoryPatternLink_OppositeStoryPatternLink();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink
	 * <em>Story Pattern Link</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Story Pattern Link</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink
	 * @generated
	 */
	EClass getStoryPatternLink();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getEStructuralFeature
	 * <em>EStructural Feature</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>EStructural Feature</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getEStructuralFeature()
	 * @see #getStoryPatternLink()
	 * @generated
	 */
	EReference getStoryPatternLink_EStructuralFeature();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getLinkPositionConstraint <em>Link Position Constraint</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference '<em>Link Position Constraint</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getLinkPositionConstraint()
	 * @see #getStoryPatternLink()
	 * @generated
	 */
	EReference getStoryPatternLink_LinkPositionConstraint();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getOutgoingLinkOrderConstraints <em>Outgoing Link Order Constraints</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Outgoing Link Order Constraints</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getOutgoingLinkOrderConstraints()
	 * @see #getStoryPatternLink()
	 * @generated
	 */
	EReference getStoryPatternLink_OutgoingLinkOrderConstraints();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getIncomingLinkOrderConstraints <em>Incoming Link Order Constraints</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming Link Order Constraints</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getIncomingLinkOrderConstraints()
	 * @see #getStoryPatternLink()
	 * @generated
	 */
	EReference getStoryPatternLink_IncomingLinkOrderConstraints();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getEOppositeReference <em>EOpposite Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EOpposite Reference</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getEOppositeReference()
	 * @see #getStoryPatternLink()
	 * @generated
	 */
	EReference getStoryPatternLink_EOppositeReference();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getExternalReference <em>External Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>External Reference</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getExternalReference()
	 * @see #getStoryPatternLink()
	 * @generated
	 */
	EReference getStoryPatternLink_ExternalReference();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink <em>Story Pattern Containment Link</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Story Pattern Containment Link</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink
	 * @generated
	 */
	EClass getStoryPatternContainmentLink();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink <em>Story Pattern Expression Link</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Story Pattern Expression Link</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink
	 * @generated
	 */
	EClass getStoryPatternExpressionLink();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink#getExpression()
	 * @see #getStoryPatternExpressionLink()
	 * @generated
	 */
	EReference getStoryPatternExpressionLink_Expression();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject <em>Story Pattern Object</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Story Pattern Object</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject
	 * @generated
	 */
	EClass getStoryPatternObject();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getBindingType <em>Binding Type</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Binding Type</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getBindingType()
	 * @see #getStoryPatternObject()
	 * @generated
	 */
	EAttribute getStoryPatternObject_BindingType();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getAttributeAssignments <em>Attribute Assignments</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attribute Assignments</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getAttributeAssignments()
	 * @see #getStoryPatternObject()
	 * @generated
	 */
	EReference getStoryPatternObject_AttributeAssignments();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getConstraints()
	 * @see #getStoryPatternObject()
	 * @generated
	 */
	EReference getStoryPatternObject_Constraints();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getDirectAssignmentExpression <em>Direct Assignment Expression</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference '<em>Direct Assignment Expression</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject#getDirectAssignmentExpression()
	 * @see #getStoryPatternObject()
	 * @generated
	 */
	EReference getStoryPatternObject_DirectAssignmentExpression();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject <em>Abstract Story Pattern Object</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Abstract Story Pattern Object</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject
	 * @generated
	 */
	EClass getAbstractStoryPatternObject();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getClassifier <em>Classifier</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Classifier</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getClassifier()
	 * @see #getAbstractStoryPatternObject()
	 * @generated
	 */
	EReference getAbstractStoryPatternObject_Classifier();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getOutgoingStoryLinks <em>Outgoing Story Links</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing Story Links</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getOutgoingStoryLinks()
	 * @see #getAbstractStoryPatternObject()
	 * @generated
	 */
	EReference getAbstractStoryPatternObject_OutgoingStoryLinks();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getIncomingStoryLinks <em>Incoming Story Links</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming Story Links</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getIncomingStoryLinks()
	 * @see #getAbstractStoryPatternObject()
	 * @generated
	 */
	EReference getAbstractStoryPatternObject_IncomingStoryLinks();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getCreateModelObjectExpression <em>Create Model Object Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Create Model Object Expression</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getCreateModelObjectExpression()
	 * @see #getAbstractStoryPatternObject()
	 * @generated
	 */
	EReference getAbstractStoryPatternObject_CreateModelObjectExpression();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink <em>Map Entry Story Pattern Link</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Map Entry Story Pattern Link</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink
	 * @generated
	 */
	EClass getMapEntryStoryPatternLink();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink#getValueTarget <em>Value Target</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Target</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink#getValueTarget()
	 * @see #getMapEntryStoryPatternLink()
	 * @generated
	 */
	EReference getMapEntryStoryPatternLink_ValueTarget();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink#getEStructuralFeature
	 * <em>EStructural Feature</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>EStructural Feature</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink#getEStructuralFeature()
	 * @see #getMapEntryStoryPatternLink()
	 * @generated
	 */
	EReference getMapEntryStoryPatternLink_EStructuralFeature();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink#getClassifier <em>Classifier</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Classifier</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink#getClassifier()
	 * @see #getMapEntryStoryPatternLink()
	 * @generated
	 */
	EReference getMapEntryStoryPatternLink_Classifier();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink <em>EContainer Story Pattern Link</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>EContainer Story Pattern Link</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink
	 * @generated
	 */
	EClass getEContainerStoryPatternLink();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink#isAllowIndirectContainment <em>Allow Indirect Containment</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Allow Indirect Containment</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink#isAllowIndirectContainment()
	 * @see #getEContainerStoryPatternLink()
	 * @generated
	 */
	EAttribute getEContainerStoryPatternLink_AllowIndirectContainment();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint <em>Link Position Constraint</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Link Position Constraint</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint
	 * @generated
	 */
	EClass getLinkPositionConstraint();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint#getConstraintType <em>Constraint Type</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constraint Type</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint#getConstraintType()
	 * @see #getLinkPositionConstraint()
	 * @generated
	 */
	EAttribute getLinkPositionConstraint_ConstraintType();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint#getStoryPatternLink
	 * <em>Story Pattern Link</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Story Pattern Link</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint#getStoryPatternLink()
	 * @see #getLinkPositionConstraint()
	 * @generated
	 */
	EReference getLinkPositionConstraint_StoryPatternLink();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint <em>Link Order Constraint</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Link Order Constraint</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint
	 * @generated
	 */
	EClass getLinkOrderConstraint();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getConstraintType <em>Constraint Type</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constraint Type</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getConstraintType()
	 * @see #getLinkOrderConstraint()
	 * @generated
	 */
	EAttribute getLinkOrderConstraint_ConstraintType();

	/**
	 * Returns the meta object for the container reference '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getPredecessorLink
	 * <em>Predecessor Link</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the container reference '
	 *         <em>Predecessor Link</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getPredecessorLink()
	 * @see #getLinkOrderConstraint()
	 * @generated
	 */
	EReference getLinkOrderConstraint_PredecessorLink();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getSuccessorLink <em>Successor Link</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Successor Link</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getSuccessorLink()
	 * @see #getLinkOrderConstraint()
	 * @generated
	 */
	EReference getLinkOrderConstraint_SuccessorLink();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.sdm.ExternalReference <em>External Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Reference</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.ExternalReference
	 * @generated
	 */
	EClass getExternalReference();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.sdm.ExternalReference#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.ExternalReference#getExpression()
	 * @see #getExternalReference()
	 * @generated
	 */
	EReference getExternalReference_Expression();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.sdm.ExternalReference#isMany <em>Many</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Many</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.ExternalReference#isMany()
	 * @see #getExternalReference()
	 * @generated
	 */
	EAttribute getExternalReference_Many();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration <em>Binding Type Enumeration</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for enum '<em>Binding Type Enumeration</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration
	 * @generated
	 */
	EEnum getBindingTypeEnumeration();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration <em>Story Pattern Match Type Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Story Pattern Match Type Enumeration</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration
	 * @generated
	 */
	EEnum getStoryPatternMatchTypeEnumeration();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration <em>Story Pattern Modifier Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Story Pattern Modifier Enumeration</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration
	 * @generated
	 */
	EEnum getStoryPatternModifierEnumeration();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration <em>Story Pattern NAC Sematic Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Story Pattern NAC Sematic Enumeration</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration
	 * @generated
	 */
	EEnum getStoryPatternNACSematicEnumeration();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration <em>Link Position Constraint Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Link Position Constraint Enumeration</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration
	 * @generated
	 */
	EEnum getLinkPositionConstraintEnumeration();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration <em>Link Order Constraint Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Link Order Constraint Enumeration</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration
	 * @generated
	 */
	EEnum getLinkOrderConstraintEnumeration();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SdmFactory getSdmFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AttributeAssignmentImpl <em>Attribute Assignment</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.AttributeAssignmentImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getAttributeAssignment()
		 * @generated
		 */
		EClass ATTRIBUTE_ASSIGNMENT = eINSTANCE.getAttributeAssignment();

		/**
		 * The meta object literal for the '<em><b>EStructural Feature</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_ASSIGNMENT__ESTRUCTURAL_FEATURE = eINSTANCE
				.getAttributeAssignment_EStructuralFeature();

		/**
		 * The meta object literal for the '<em><b>Assignment Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION = eINSTANCE
				.getAttributeAssignment_AssignmentExpression();

		/**
		 * The meta object literal for the '<em><b>Custom Comparator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR = eINSTANCE
				.getAttributeAssignment_CustomComparator();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternElementImpl <em>Story Pattern Element</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternElementImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternElement()
		 * @generated
		 */
		EClass STORY_PATTERN_ELEMENT = eINSTANCE.getStoryPatternElement();

		/**
		 * The meta object literal for the '<em><b>Modifier</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STORY_PATTERN_ELEMENT__MODIFIER = eINSTANCE
				.getStoryPatternElement_Modifier();

		/**
		 * The meta object literal for the '<em><b>Match Type</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STORY_PATTERN_ELEMENT__MATCH_TYPE = eINSTANCE
				.getStoryPatternElement_MatchType();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternLinkImpl <em>Abstract Story Pattern Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternLinkImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getAbstractStoryPatternLink()
		 * @generated
		 */
		EClass ABSTRACT_STORY_PATTERN_LINK = eINSTANCE
				.getAbstractStoryPatternLink();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_STORY_PATTERN_LINK__SOURCE = eINSTANCE
				.getAbstractStoryPatternLink_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_STORY_PATTERN_LINK__TARGET = eINSTANCE
				.getAbstractStoryPatternLink_Target();

		/**
		 * The meta object literal for the '<em><b>Check Only Existence</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE = eINSTANCE
				.getAbstractStoryPatternLink_CheckOnlyExistence();

		/**
		 * The meta object literal for the '<em><b>Matching Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY = eINSTANCE
				.getAbstractStoryPatternLink_MatchingPriority();

		/**
		 * The meta object literal for the '
		 * <em><b>Opposite Story Pattern Link</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK = eINSTANCE
				.getAbstractStoryPatternLink_OppositeStoryPatternLink();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternLinkImpl <em>Story Pattern Link</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternLinkImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternLink()
		 * @generated
		 */
		EClass STORY_PATTERN_LINK = eINSTANCE.getStoryPatternLink();

		/**
		 * The meta object literal for the '<em><b>EStructural Feature</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE = eINSTANCE
				.getStoryPatternLink_EStructuralFeature();

		/**
		 * The meta object literal for the '<em><b>Link Position Constraint</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT = eINSTANCE
				.getStoryPatternLink_LinkPositionConstraint();

		/**
		 * The meta object literal for the '<em><b>Outgoing Link Order Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS = eINSTANCE
				.getStoryPatternLink_OutgoingLinkOrderConstraints();

		/**
		 * The meta object literal for the '<em><b>Incoming Link Order Constraints</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS = eINSTANCE
				.getStoryPatternLink_IncomingLinkOrderConstraints();

		/**
		 * The meta object literal for the '<em><b>EOpposite Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STORY_PATTERN_LINK__EOPPOSITE_REFERENCE = eINSTANCE
				.getStoryPatternLink_EOppositeReference();

		/**
		 * The meta object literal for the '<em><b>External Reference</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STORY_PATTERN_LINK__EXTERNAL_REFERENCE = eINSTANCE
				.getStoryPatternLink_ExternalReference();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternContainmentLinkImpl <em>Story Pattern Containment Link</em>}' class.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternContainmentLinkImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternContainmentLink()
		 * @generated
		 */
		EClass STORY_PATTERN_CONTAINMENT_LINK = eINSTANCE
				.getStoryPatternContainmentLink();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternExpressionLinkImpl <em>Story Pattern Expression Link</em>}' class.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternExpressionLinkImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternExpressionLink()
		 * @generated
		 */
		EClass STORY_PATTERN_EXPRESSION_LINK = eINSTANCE
				.getStoryPatternExpressionLink();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference STORY_PATTERN_EXPRESSION_LINK__EXPRESSION = eINSTANCE
				.getStoryPatternExpressionLink_Expression();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternObjectImpl <em>Story Pattern Object</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternObjectImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternObject()
		 * @generated
		 */
		EClass STORY_PATTERN_OBJECT = eINSTANCE.getStoryPatternObject();

		/**
		 * The meta object literal for the '<em><b>Binding Type</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STORY_PATTERN_OBJECT__BINDING_TYPE = eINSTANCE
				.getStoryPatternObject_BindingType();

		/**
		 * The meta object literal for the '<em><b>Attribute Assignments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference STORY_PATTERN_OBJECT__ATTRIBUTE_ASSIGNMENTS = eINSTANCE
				.getStoryPatternObject_AttributeAssignments();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference STORY_PATTERN_OBJECT__CONSTRAINTS = eINSTANCE
				.getStoryPatternObject_Constraints();

		/**
		 * The meta object literal for the '<em><b>Direct Assignment Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION = eINSTANCE
				.getStoryPatternObject_DirectAssignmentExpression();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternObjectImpl <em>Abstract Story Pattern Object</em>}' class.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.AbstractStoryPatternObjectImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getAbstractStoryPatternObject()
		 * @generated
		 */
		EClass ABSTRACT_STORY_PATTERN_OBJECT = eINSTANCE
				.getAbstractStoryPatternObject();

		/**
		 * The meta object literal for the '<em><b>Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_STORY_PATTERN_OBJECT__CLASSIFIER = eINSTANCE
				.getAbstractStoryPatternObject_Classifier();

		/**
		 * The meta object literal for the '<em><b>Outgoing Story Links</b></em>
		 * ' reference list feature. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS = eINSTANCE
				.getAbstractStoryPatternObject_OutgoingStoryLinks();

		/**
		 * The meta object literal for the '<em><b>Incoming Story Links</b></em>
		 * ' reference list feature. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS = eINSTANCE
				.getAbstractStoryPatternObject_IncomingStoryLinks();

		/**
		 * The meta object literal for the '<em><b>Create Model Object Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION = eINSTANCE
				.getAbstractStoryPatternObject_CreateModelObjectExpression();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.MapEntryStoryPatternLinkImpl <em>Map Entry Story Pattern Link</em>}' class.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.MapEntryStoryPatternLinkImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getMapEntryStoryPatternLink()
		 * @generated
		 */
		EClass MAP_ENTRY_STORY_PATTERN_LINK = eINSTANCE
				.getMapEntryStoryPatternLink();

		/**
		 * The meta object literal for the '<em><b>Value Target</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP_ENTRY_STORY_PATTERN_LINK__VALUE_TARGET = eINSTANCE
				.getMapEntryStoryPatternLink_ValueTarget();

		/**
		 * The meta object literal for the '<em><b>EStructural Feature</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP_ENTRY_STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE = eINSTANCE
				.getMapEntryStoryPatternLink_EStructuralFeature();

		/**
		 * The meta object literal for the '<em><b>Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP_ENTRY_STORY_PATTERN_LINK__CLASSIFIER = eINSTANCE
				.getMapEntryStoryPatternLink_Classifier();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.EContainerStoryPatternLinkImpl <em>EContainer Story Pattern Link</em>}' class.
		 * <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.EContainerStoryPatternLinkImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getEContainerStoryPatternLink()
		 * @generated
		 */
		EClass ECONTAINER_STORY_PATTERN_LINK = eINSTANCE
				.getEContainerStoryPatternLink();

		/**
		 * The meta object literal for the '
		 * <em><b>Allow Indirect Containment</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute ECONTAINER_STORY_PATTERN_LINK__ALLOW_INDIRECT_CONTAINMENT = eINSTANCE
				.getEContainerStoryPatternLink_AllowIndirectContainment();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.LinkPositionConstraintImpl <em>Link Position Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.LinkPositionConstraintImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getLinkPositionConstraint()
		 * @generated
		 */
		EClass LINK_POSITION_CONSTRAINT = eINSTANCE.getLinkPositionConstraint();

		/**
		 * The meta object literal for the '<em><b>Constraint Type</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE = eINSTANCE
				.getLinkPositionConstraint_ConstraintType();

		/**
		 * The meta object literal for the '<em><b>Story Pattern Link</b></em>' container reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK = eINSTANCE
				.getLinkPositionConstraint_StoryPatternLink();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.LinkOrderConstraintImpl <em>Link Order Constraint</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.LinkOrderConstraintImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getLinkOrderConstraint()
		 * @generated
		 */
		EClass LINK_ORDER_CONSTRAINT = eINSTANCE.getLinkOrderConstraint();

		/**
		 * The meta object literal for the '<em><b>Constraint Type</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LINK_ORDER_CONSTRAINT__CONSTRAINT_TYPE = eINSTANCE
				.getLinkOrderConstraint_ConstraintType();

		/**
		 * The meta object literal for the '<em><b>Predecessor Link</b></em>' container reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK = eINSTANCE
				.getLinkOrderConstraint_PredecessorLink();

		/**
		 * The meta object literal for the '<em><b>Successor Link</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK = eINSTANCE
				.getLinkOrderConstraint_SuccessorLink();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.impl.ExternalReferenceImpl <em>External Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.ExternalReferenceImpl
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getExternalReference()
		 * @generated
		 */
		EClass EXTERNAL_REFERENCE = eINSTANCE.getExternalReference();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTERNAL_REFERENCE__EXPRESSION = eINSTANCE
				.getExternalReference_Expression();

		/**
		 * The meta object literal for the '<em><b>Many</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTERNAL_REFERENCE__MANY = eINSTANCE
				.getExternalReference_Many();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration <em>Binding Type Enumeration</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getBindingTypeEnumeration()
		 * @generated
		 */
		EEnum BINDING_TYPE_ENUMERATION = eINSTANCE.getBindingTypeEnumeration();

		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration
		 * <em>Story Pattern Match Type Enumeration</em>}' enum. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternMatchTypeEnumeration()
		 * @generated
		 */
		EEnum STORY_PATTERN_MATCH_TYPE_ENUMERATION = eINSTANCE
				.getStoryPatternMatchTypeEnumeration();

		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration
		 * <em>Story Pattern Modifier Enumeration</em>}' enum. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternModifierEnumeration()
		 * @generated
		 */
		EEnum STORY_PATTERN_MODIFIER_ENUMERATION = eINSTANCE
				.getStoryPatternModifierEnumeration();

		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration
		 * <em>Story Pattern NAC Sematic Enumeration</em>}' enum. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getStoryPatternNACSematicEnumeration()
		 * @generated
		 */
		EEnum STORY_PATTERN_NAC_SEMATIC_ENUMERATION = eINSTANCE
				.getStoryPatternNACSematicEnumeration();

		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration
		 * <em>Link Position Constraint Enumeration</em>}' enum. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getLinkPositionConstraintEnumeration()
		 * @generated
		 */
		EEnum LINK_POSITION_CONSTRAINT_ENUMERATION = eINSTANCE
				.getLinkPositionConstraintEnumeration();

		/**
		 * The meta object literal for the '
		 * {@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration
		 * <em>Link Order Constraint Enumeration</em>}' enum. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration
		 * @see de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl#getLinkOrderConstraintEnumeration()
		 * @generated
		 */
		EEnum LINK_ORDER_CONSTRAINT_ENUMERATION = eINSTANCE
				.getLinkOrderConstraintEnumeration();

	}

} // SdmPackage
