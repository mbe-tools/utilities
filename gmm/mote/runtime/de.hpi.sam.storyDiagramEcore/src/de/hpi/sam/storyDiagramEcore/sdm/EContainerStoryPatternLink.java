/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>EContainer Story Pattern Link</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This story pattern links of this type are added automatically by the interpreter during analysis of a story pattern. They correspond to the eContainer() call to get the container of an object.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink#isAllowIndirectContainment <em>Allow Indirect Containment</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getEContainerStoryPatternLink()
 * @model
 * @generated
 */
public interface EContainerStoryPatternLink extends AbstractStoryPatternLink {
	/**
	 * Returns the value of the '<em><b>Allow Indirect Containment</b></em>'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> If allowIndirectContainment is true, the interpreter
	 * will follow the containment hierarchy upwards until it finds a matching
	 * object or an element without a container is reached. <!-- end-model-doc
	 * -->
	 * 
	 * @return the value of the '<em>Allow Indirect Containment</em>' attribute.
	 * @see #setAllowIndirectContainment(boolean)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getEContainerStoryPatternLink_AllowIndirectContainment()
	 * @model
	 * @generated
	 */
	boolean isAllowIndirectContainment();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink#isAllowIndirectContainment <em>Allow Indirect Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allow Indirect Containment</em>' attribute.
	 * @see #isAllowIndirectContainment()
	 * @generated
	 */
	void setAllowIndirectContainment(boolean value);

} // EContainerStoryPatternLink
