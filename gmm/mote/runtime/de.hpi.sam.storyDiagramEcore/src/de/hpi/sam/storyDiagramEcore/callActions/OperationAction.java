/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Operation Action</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The operation action executes an operation on the two operands and returns the result.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.OperationAction#getOperand1 <em>Operand1</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.OperationAction#getOperand2 <em>Operand2</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.OperationAction#getOperator <em>Operator</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getOperationAction()
 * @model
 * @generated
 */
public interface OperationAction extends CallAction {
	/**
	 * Returns the value of the '<em><b>Operand1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operand1</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operand1</em>' containment reference.
	 * @see #setOperand1(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getOperationAction_Operand1()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getOperand1();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.OperationAction#getOperand1 <em>Operand1</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Operand1</em>' containment reference.
	 * @see #getOperand1()
	 * @generated
	 */
	void setOperand1(Expression value);

	/**
	 * Returns the value of the '<em><b>Operand2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operand2</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operand2</em>' containment reference.
	 * @see #setOperand2(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getOperationAction_Operand2()
	 * @model containment="true"
	 * @generated
	 */
	Expression getOperand2();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.OperationAction#getOperand2 <em>Operand2</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Operand2</em>' containment reference.
	 * @see #getOperand2()
	 * @generated
	 */
	void setOperand2(Expression value);

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute. The
	 * literals are from the enumeration
	 * {@link de.hpi.sam.storyDiagramEcore.callActions.Operators}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.Operators
	 * @see #setOperator(Operators)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getOperationAction_Operator()
	 * @model required="true"
	 * @generated
	 */
	Operators getOperator();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.callActions.OperationAction#getOperator
	 * <em>Operator</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Operator</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.Operators
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(Operators value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (eIsProxy())\r\n\treturn super.toString();\r\n\r\nStringBuffer buffer = new StringBuffer();\r\n\r\nif (this.getOperator() == Operators.NOT)\r\n{\r\n\tbuffer.append(this.getOperator().toString());\r\n}\r\n\r\nif (this.getOperand1() != null) {\r\n\tbuffer.append(this.getOperand1().toString());\r\n} else {\r\n\tbuffer.append(\"[null]\");\r\n}\r\n\r\nif (this.getOperator() != Operators.NOT)\r\n{\r\n\tbuffer.append(\" \");\r\n\tbuffer.append(this.getOperator().toString());\r\n\tbuffer.append(\" \");\r\n\r\n\tif (this.getOperand2() != null) {\r\n\t\tbuffer.append(this.getOperand2().toString());\r\n\t} else {\r\n\t\tbuffer.append(\"[null]\");\r\n\t}\r\n}\r\nreturn buffer.toString();'"
	 * @generated
	 */
	String toString();

} // OperationAction
