/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.Semaphore;
import de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Semaphore</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.SemaphoreImpl#getSynchronizationEdges <em>Synchronization Edges</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.SemaphoreImpl#getTokenCount <em>Token Count</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SemaphoreImpl extends NamedElementImpl implements Semaphore {
	/**
	 * The cached value of the '{@link #getSynchronizationEdges()
	 * <em>Synchronization Edges</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSynchronizationEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<SynchronizationEdge> synchronizationEdges;

	/**
	 * The default value of the '{@link #getTokenCount() <em>Token Count</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTokenCount()
	 * @generated
	 * @ordered
	 */
	protected static final int TOKEN_COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTokenCount() <em>Token Count</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTokenCount()
	 * @generated
	 * @ordered
	 */
	protected int tokenCount = TOKEN_COUNT_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected SemaphoreImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NodesPackage.Literals.SEMAPHORE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SynchronizationEdge> getSynchronizationEdges() {
		if (synchronizationEdges == null) {
			synchronizationEdges = new EObjectContainmentWithInverseEList<SynchronizationEdge>(
					SynchronizationEdge.class, this,
					NodesPackage.SEMAPHORE__SYNCHRONIZATION_EDGES,
					NodesPackage.SYNCHRONIZATION_EDGE__SEMAPHORE);
		}
		return synchronizationEdges;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getTokenCount() {
		return tokenCount;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTokenCount(int newTokenCount) {
		int oldTokenCount = tokenCount;
		tokenCount = newTokenCount;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					NodesPackage.SEMAPHORE__TOKEN_COUNT, oldTokenCount,
					tokenCount));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case NodesPackage.SEMAPHORE__SYNCHRONIZATION_EDGES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getSynchronizationEdges())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case NodesPackage.SEMAPHORE__SYNCHRONIZATION_EDGES:
			return ((InternalEList<?>) getSynchronizationEdges()).basicRemove(
					otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case NodesPackage.SEMAPHORE__SYNCHRONIZATION_EDGES:
			return getSynchronizationEdges();
		case NodesPackage.SEMAPHORE__TOKEN_COUNT:
			return getTokenCount();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case NodesPackage.SEMAPHORE__SYNCHRONIZATION_EDGES:
			getSynchronizationEdges().clear();
			getSynchronizationEdges().addAll(
					(Collection<? extends SynchronizationEdge>) newValue);
			return;
		case NodesPackage.SEMAPHORE__TOKEN_COUNT:
			setTokenCount((Integer) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case NodesPackage.SEMAPHORE__SYNCHRONIZATION_EDGES:
			getSynchronizationEdges().clear();
			return;
		case NodesPackage.SEMAPHORE__TOKEN_COUNT:
			setTokenCount(TOKEN_COUNT_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case NodesPackage.SEMAPHORE__SYNCHRONIZATION_EDGES:
			return synchronizationEdges != null
					&& !synchronizationEdges.isEmpty();
		case NodesPackage.SEMAPHORE__TOKEN_COUNT:
			return tokenCount != TOKEN_COUNT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tokenCount: ");
		result.append(tokenCount);
		result.append(')');
		return result.toString();
	}

} // SemaphoreImpl
