/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

import org.eclipse.emf.common.util.EList;

import de.hpi.sam.storyDiagramEcore.NamedElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Semaphore</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The semaphore controls access to shared resources. It contains a certain amount
 * of tokens, which are acquired and released when an activity edge is traversed.
 * If not enough tokens are available, the execution of an activity edge is blocked
 * until enough tokens are available again.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.Semaphore#getSynchronizationEdges <em>Synchronization Edges</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.Semaphore#getTokenCount <em>Token Count</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getSemaphore()
 * @model
 * @generated
 */
public interface Semaphore extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Synchronization Edges</b></em>' containment reference list.
	 * The list contents are of type {@link de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge}.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge#getSemaphore <em>Semaphore</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synchronization Edges</em>' containment
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synchronization Edges</em>' containment reference list.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getSemaphore_SynchronizationEdges()
	 * @see de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge#getSemaphore
	 * @model opposite="semaphore" containment="true"
	 * @generated
	 */
	EList<SynchronizationEdge> getSynchronizationEdges();

	/**
	 * Returns the value of the '<em><b>Token Count</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Token Count</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Token Count</em>' attribute.
	 * @see #setTokenCount(int)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getSemaphore_TokenCount()
	 * @model required="true"
	 * @generated
	 */
	int getTokenCount();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.nodes.Semaphore#getTokenCount <em>Token Count</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Token Count</em>' attribute.
	 * @see #getTokenCount()
	 * @generated
	 */
	void setTokenCount(int value);

} // Semaphore
