/**
 */
package de.hpi.sam.storyDiagramEcore.callActions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Case Insensitive Comparator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCaseInsensitiveComparator()
 * @model
 * @generated
 */
public interface CaseInsensitiveComparator extends AbstractComparator {
} // CaseInsensitiveComparator
