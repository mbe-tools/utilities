/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Flow Final Node</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A FlowFinalNode marks the end of a single control flow inside an activity. If a FlowFinalNode is reached by a control flow, that control flow terminates but all other control flows still continue.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getFlowFinalNode()
 * @model
 * @generated
 */
public interface FlowFinalNode extends ActivityNode {
} // FlowFinalNode
