/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;

import de.hpi.sam.storyDiagramEcore.ExpressionImport;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Expression Import</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.impl.ExpressionImportImpl#getImportedFileURI <em>Imported File URI</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.impl.ExpressionImportImpl#getExpressionLanguage <em>Expression Language</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.impl.ExpressionImportImpl#getResolvedImportedFileURI <em>Resolved Imported File URI</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ExpressionImportImpl extends ImportImpl implements
		ExpressionImport {
	/**
	 * The default value of the '{@link #getImportedFileURI() <em>Imported File URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getImportedFileURI()
	 * @generated
	 * @ordered
	 */
	protected static final String IMPORTED_FILE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImportedFileURI() <em>Imported File URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getImportedFileURI()
	 * @generated
	 * @ordered
	 */
	protected String importedFileURI = IMPORTED_FILE_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getExpressionLanguage() <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getExpressionLanguage()
	 * @generated
	 * @ordered
	 */
	protected static final String EXPRESSION_LANGUAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExpressionLanguage() <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getExpressionLanguage()
	 * @generated
	 * @ordered
	 */
	protected String expressionLanguage = EXPRESSION_LANGUAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getResolvedImportedFileURI() <em>Resolved Imported File URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResolvedImportedFileURI()
	 * @generated
	 * @ordered
	 */
	protected static final String RESOLVED_IMPORTED_FILE_URI_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpressionImportImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StoryDiagramEcorePackage.Literals.EXPRESSION_IMPORT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getImportedFileURI() {
		return importedFileURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setImportedFileURI(String newImportedFileURI) {
		String oldImportedFileURI = importedFileURI;
		importedFileURI = newImportedFileURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					StoryDiagramEcorePackage.EXPRESSION_IMPORT__IMPORTED_FILE_URI,
					oldImportedFileURI, importedFileURI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getExpressionLanguage() {
		return expressionLanguage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExpressionLanguage(String newExpressionLanguage) {
		String oldExpressionLanguage = expressionLanguage;
		expressionLanguage = newExpressionLanguage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					StoryDiagramEcorePackage.EXPRESSION_IMPORT__EXPRESSION_LANGUAGE,
					oldExpressionLanguage, expressionLanguage));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public String getResolvedImportedFileURI() {
		if (this.getImportedFileURI() == null
				|| "".equals(this.getImportedFileURI())) {
			return "";
		} else {
			Resource resource = this.eResource();

			if (resource != null) {
				try {
					URI unresolvedURI = URI
							.createURI(this.getImportedFileURI());

					URI baseURI = resource.getURI();
					if (baseURI != null && baseURI.isHierarchical()
							&& !baseURI.isRelative()) {
						return unresolvedURI.resolve(baseURI).toString();
					} else {
						return unresolvedURI.toString();
					}
				} catch (IllegalArgumentException ex) {
					return "";
				}
			} else {
				return "";
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case StoryDiagramEcorePackage.EXPRESSION_IMPORT__IMPORTED_FILE_URI:
			return getImportedFileURI();
		case StoryDiagramEcorePackage.EXPRESSION_IMPORT__EXPRESSION_LANGUAGE:
			return getExpressionLanguage();
		case StoryDiagramEcorePackage.EXPRESSION_IMPORT__RESOLVED_IMPORTED_FILE_URI:
			return getResolvedImportedFileURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case StoryDiagramEcorePackage.EXPRESSION_IMPORT__IMPORTED_FILE_URI:
			setImportedFileURI((String) newValue);
			return;
		case StoryDiagramEcorePackage.EXPRESSION_IMPORT__EXPRESSION_LANGUAGE:
			setExpressionLanguage((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case StoryDiagramEcorePackage.EXPRESSION_IMPORT__IMPORTED_FILE_URI:
			setImportedFileURI(IMPORTED_FILE_URI_EDEFAULT);
			return;
		case StoryDiagramEcorePackage.EXPRESSION_IMPORT__EXPRESSION_LANGUAGE:
			setExpressionLanguage(EXPRESSION_LANGUAGE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case StoryDiagramEcorePackage.EXPRESSION_IMPORT__IMPORTED_FILE_URI:
			return IMPORTED_FILE_URI_EDEFAULT == null ? importedFileURI != null
					: !IMPORTED_FILE_URI_EDEFAULT.equals(importedFileURI);
		case StoryDiagramEcorePackage.EXPRESSION_IMPORT__EXPRESSION_LANGUAGE:
			return EXPRESSION_LANGUAGE_EDEFAULT == null ? expressionLanguage != null
					: !EXPRESSION_LANGUAGE_EDEFAULT.equals(expressionLanguage);
		case StoryDiagramEcorePackage.EXPRESSION_IMPORT__RESOLVED_IMPORTED_FILE_URI:
			return RESOLVED_IMPORTED_FILE_URI_EDEFAULT == null ? getResolvedImportedFileURI() != null
					: !RESOLVED_IMPORTED_FILE_URI_EDEFAULT
							.equals(getResolvedImportedFileURI());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (importedFileURI: ");
		result.append(importedFileURI);
		result.append(", expressionLanguage: ");
		result.append(expressionLanguage);
		result.append(')');
		return result.toString();
	}

} // ExpressionImportImpl
