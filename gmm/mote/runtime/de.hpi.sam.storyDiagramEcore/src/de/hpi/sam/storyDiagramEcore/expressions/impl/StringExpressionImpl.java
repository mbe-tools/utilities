/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.expressions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage;
import de.hpi.sam.storyDiagramEcore.expressions.StringExpression;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>String Expression</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.expressions.impl.StringExpressionImpl#getExpressionString <em>Expression String</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.expressions.impl.StringExpressionImpl#getExpressionLanguage <em>Expression Language</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.expressions.impl.StringExpressionImpl#getAst <em>Ast</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StringExpressionImpl extends ExpressionImpl implements
		StringExpression {
	/**
	 * The default value of the '{@link #getExpressionString() <em>Expression String</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getExpressionString()
	 * @generated
	 * @ordered
	 */
	protected static final String EXPRESSION_STRING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExpressionString() <em>Expression String</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getExpressionString()
	 * @generated
	 * @ordered
	 */
	protected String expressionString = EXPRESSION_STRING_EDEFAULT;

	/**
	 * The default value of the '{@link #getExpressionLanguage() <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getExpressionLanguage()
	 * @generated
	 * @ordered
	 */
	protected static final String EXPRESSION_LANGUAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExpressionLanguage() <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getExpressionLanguage()
	 * @generated
	 * @ordered
	 */
	protected String expressionLanguage = EXPRESSION_LANGUAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAst() <em>Ast</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getAst()
	 * @generated
	 * @ordered
	 */
	protected EObject ast;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	protected StringExpressionImpl() {
		super();

		if (!de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreUtils
				.getAvailableExpressionLanguages().isEmpty()) {
			this.setExpressionLanguage(de.hpi.sam.storyDiagramEcore.StoryDiagramEcoreUtils
					.getAvailableExpressionLanguages().get(0));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.STRING_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getExpressionString() {
		return expressionString;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpressionString(String newExpressionString) {
		String oldExpressionString = expressionString;
		expressionString = newExpressionString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.STRING_EXPRESSION__EXPRESSION_STRING,
					oldExpressionString, expressionString));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getExpressionLanguage() {
		return expressionLanguage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpressionLanguage(String newExpressionLanguage) {
		String oldExpressionLanguage = expressionLanguage;
		expressionLanguage = newExpressionLanguage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.STRING_EXPRESSION__EXPRESSION_LANGUAGE,
					oldExpressionLanguage, expressionLanguage));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getAst() {
		return ast;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAst(EObject newAst, NotificationChain msgs) {
		EObject oldAst = ast;
		ast = newAst;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					ExpressionsPackage.STRING_EXPRESSION__AST, oldAst, newAst);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setAst(EObject newAst) {
		if (newAst != ast) {
			NotificationChain msgs = null;
			if (ast != null)
				msgs = ((InternalEObject) ast).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.STRING_EXPRESSION__AST,
						null, msgs);
			if (newAst != null)
				msgs = ((InternalEObject) newAst).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.STRING_EXPRESSION__AST,
						null, msgs);
			msgs = basicSetAst(newAst, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.STRING_EXPRESSION__AST, newAst, newAst));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.STRING_EXPRESSION__EXPRESSION_STRING:
			return getExpressionString();
		case ExpressionsPackage.STRING_EXPRESSION__EXPRESSION_LANGUAGE:
			return getExpressionLanguage();
		case ExpressionsPackage.STRING_EXPRESSION__AST:
			return getAst();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.STRING_EXPRESSION__EXPRESSION_STRING:
			setExpressionString((String) newValue);
			return;
		case ExpressionsPackage.STRING_EXPRESSION__EXPRESSION_LANGUAGE:
			setExpressionLanguage((String) newValue);
			return;
		case ExpressionsPackage.STRING_EXPRESSION__AST:
			setAst((EObject) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.STRING_EXPRESSION__EXPRESSION_STRING:
			setExpressionString(EXPRESSION_STRING_EDEFAULT);
			return;
		case ExpressionsPackage.STRING_EXPRESSION__EXPRESSION_LANGUAGE:
			setExpressionLanguage(EXPRESSION_LANGUAGE_EDEFAULT);
			return;
		case ExpressionsPackage.STRING_EXPRESSION__AST:
			setAst((EObject) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.STRING_EXPRESSION__EXPRESSION_STRING:
			return EXPRESSION_STRING_EDEFAULT == null ? expressionString != null
					: !EXPRESSION_STRING_EDEFAULT.equals(expressionString);
		case ExpressionsPackage.STRING_EXPRESSION__EXPRESSION_LANGUAGE:
			return EXPRESSION_LANGUAGE_EDEFAULT == null ? expressionLanguage != null
					: !EXPRESSION_LANGUAGE_EDEFAULT.equals(expressionLanguage);
		case ExpressionsPackage.STRING_EXPRESSION__AST:
			return ast != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		} else {
			if (getExpressionString() == null
					|| "".equals(getExpressionString())) {
				return "[null]";
			} else {
				return getExpressionString();
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.STRING_EXPRESSION__AST:
			return basicSetAst(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

} // StringExpressionImpl
