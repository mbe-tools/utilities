/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.DecisionNode;
import de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.ForkNode;
import de.hpi.sam.storyDiagramEcore.nodes.InitialNode;
import de.hpi.sam.storyDiagramEcore.nodes.JoinNode;
import de.hpi.sam.storyDiagramEcore.nodes.MergeNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge;
import de.hpi.sam.storyDiagramEcore.nodes.Semaphore;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 <<<<<<< .mine
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage
 * @generated
 */
public class NodesSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static NodesPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodesSwitch() {
		if (modelPackage == null) {
			modelPackage = NodesPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case NodesPackage.ACTIVITY_EDGE: {
			ActivityEdge activityEdge = (ActivityEdge) theEObject;
			T result = caseActivityEdge(activityEdge);
			if (result == null)
				result = caseNamedElement(activityEdge);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.ACTIVITY_FINAL_NODE: {
			ActivityFinalNode activityFinalNode = (ActivityFinalNode) theEObject;
			T result = caseActivityFinalNode(activityFinalNode);
			if (result == null)
				result = caseActivityNode(activityFinalNode);
			if (result == null)
				result = caseNamedElement(activityFinalNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.FLOW_FINAL_NODE: {
			FlowFinalNode flowFinalNode = (FlowFinalNode) theEObject;
			T result = caseFlowFinalNode(flowFinalNode);
			if (result == null)
				result = caseActivityNode(flowFinalNode);
			if (result == null)
				result = caseNamedElement(flowFinalNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.ACTIVITY_NODE: {
			ActivityNode activityNode = (ActivityNode) theEObject;
			T result = caseActivityNode(activityNode);
			if (result == null)
				result = caseNamedElement(activityNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.EXPRESSION_ACTIVITY_NODE: {
			ExpressionActivityNode expressionActivityNode = (ExpressionActivityNode) theEObject;
			T result = caseExpressionActivityNode(expressionActivityNode);
			if (result == null)
				result = caseActivityNode(expressionActivityNode);
			if (result == null)
				result = caseNamedElement(expressionActivityNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.DECISION_NODE: {
			DecisionNode decisionNode = (DecisionNode) theEObject;
			T result = caseDecisionNode(decisionNode);
			if (result == null)
				result = caseActivityNode(decisionNode);
			if (result == null)
				result = caseNamedElement(decisionNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.INITIAL_NODE: {
			InitialNode initialNode = (InitialNode) theEObject;
			T result = caseInitialNode(initialNode);
			if (result == null)
				result = caseActivityNode(initialNode);
			if (result == null)
				result = caseNamedElement(initialNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.MERGE_NODE: {
			MergeNode mergeNode = (MergeNode) theEObject;
			T result = caseMergeNode(mergeNode);
			if (result == null)
				result = caseActivityNode(mergeNode);
			if (result == null)
				result = caseNamedElement(mergeNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.STORY_ACTION_NODE: {
			StoryActionNode storyActionNode = (StoryActionNode) theEObject;
			T result = caseStoryActionNode(storyActionNode);
			if (result == null)
				result = caseActivityNode(storyActionNode);
			if (result == null)
				result = caseNamedElement(storyActionNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.FORK_NODE: {
			ForkNode forkNode = (ForkNode) theEObject;
			T result = caseForkNode(forkNode);
			if (result == null)
				result = caseActivityNode(forkNode);
			if (result == null)
				result = caseNamedElement(forkNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.JOIN_NODE: {
			JoinNode joinNode = (JoinNode) theEObject;
			T result = caseJoinNode(joinNode);
			if (result == null)
				result = caseActivityNode(joinNode);
			if (result == null)
				result = caseNamedElement(joinNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.SEMAPHORE: {
			Semaphore semaphore = (Semaphore) theEObject;
			T result = caseSemaphore(semaphore);
			if (result == null)
				result = caseNamedElement(semaphore);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.SYNCHRONIZATION_EDGE: {
			SynchronizationEdge synchronizationEdge = (SynchronizationEdge) theEObject;
			T result = caseSynchronizationEdge(synchronizationEdge);
			if (result == null)
				result = caseNamedElement(synchronizationEdge);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.RELEASE_SEMAPHORE_EDGE: {
			ReleaseSemaphoreEdge releaseSemaphoreEdge = (ReleaseSemaphoreEdge) theEObject;
			T result = caseReleaseSemaphoreEdge(releaseSemaphoreEdge);
			if (result == null)
				result = caseSynchronizationEdge(releaseSemaphoreEdge);
			if (result == null)
				result = caseNamedElement(releaseSemaphoreEdge);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case NodesPackage.ACQUIRE_SEMAPHORE_EDGE: {
			AcquireSemaphoreEdge acquireSemaphoreEdge = (AcquireSemaphoreEdge) theEObject;
			T result = caseAcquireSemaphoreEdge(acquireSemaphoreEdge);
			if (result == null)
				result = caseSynchronizationEdge(acquireSemaphoreEdge);
			if (result == null)
				result = caseNamedElement(acquireSemaphoreEdge);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activity Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activity Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivityEdge(ActivityEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activity Final Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activity Final Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivityFinalNode(ActivityFinalNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Flow Final Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Flow Final Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFlowFinalNode(FlowFinalNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activity Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activity Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivityNode(ActivityNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression Activity Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression Activity Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpressionActivityNode(ExpressionActivityNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Decision Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Decision Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDecisionNode(DecisionNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Initial Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Initial Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInitialNode(InitialNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Merge Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Merge Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMergeNode(MergeNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Story Action Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Story Action Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStoryActionNode(StoryActionNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fork Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fork Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForkNode(ForkNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Join Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Join Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJoinNode(JoinNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Semaphore</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Semaphore</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSemaphore(Semaphore object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Synchronization Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Synchronization Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSynchronizationEdge(SynchronizationEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Release Semaphore Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Release Semaphore Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReleaseSemaphoreEdge(ReleaseSemaphoreEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acquire Semaphore Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acquire Semaphore Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcquireSemaphoreEdge(AcquireSemaphoreEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //NodesSwitch
