/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.expressions;

import de.hpi.sam.storyDiagramEcore.NamedElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Expression</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Expression is the abstract super type of all expression.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage#getExpression()
 * @model abstract="true"
 * @generated
 */
public interface Expression extends NamedElement {
} // Expression
