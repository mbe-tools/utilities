/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '
 * <em><b>Activity Edge Guard Enumeration</b></em>', and utility methods for
 * working with them. <!-- end-user-doc --> <!-- begin-model-doc --> This
 * enumeration contains the guard types an activity edge guard can have. <!--
 * end-model-doc -->
 * 
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityEdgeGuardEnumeration()
 * @model
 * @generated
 */
public enum ActivityEdgeGuardEnumeration implements Enumerator {
	/**
	 * The '<em><b>NONE</b></em>' literal object.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #NONE_VALUE
	 * @generated
	 * @ordered
	 */
	NONE(0, "NONE", "NONE"),

	/**
	 * The '<em><b>FOR EACH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FOR_EACH_VALUE
	 * @generated
	 * @ordered
	 */
	FOR_EACH(1, "FOR_EACH", "FOR_EACH"),

	/**
	 * The '<em><b>END</b></em>' literal object.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #END_VALUE
	 * @generated
	 * @ordered
	 */
	END(2, "END", "END"),

	/**
	 * The '<em><b>SUCCESS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUCCESS_VALUE
	 * @generated
	 * @ordered
	 */
	SUCCESS(3, "SUCCESS", "SUCCESS"),

	/**
	 * The '<em><b>FAILURE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FAILURE_VALUE
	 * @generated
	 * @ordered
	 */
	FAILURE(4, "FAILURE", "FAILURE"),

	/**
	 * The '<em><b>BOOLEAN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BOOLEAN_VALUE
	 * @generated
	 * @ordered
	 */
	BOOLEAN(5, "BOOLEAN", "BOOLEAN"),

	/**
	 * The '<em><b>ELSE</b></em>' literal object.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #ELSE_VALUE
	 * @generated
	 * @ordered
	 */
	ELSE(6, "ELSE", "ELSE");

	/**
	 * The '<em><b>NONE</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> No guard. <!-- end-model-doc
	 * -->
	 * 
	 * @see #NONE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NONE_VALUE = 0;

	/**
	 * The '<em><b>FOR EACH</b></em>' literal value. <!-- begin-user-doc -->
	 * <!-- end-user-doc --> <!-- begin-model-doc --> If the story action node
	 * that is used as the source of the activity edge, the edge is followed
	 * each time a valid match was found for the story pattern of the story
	 * action node. <!-- end-model-doc -->
	 * 
	 * @see #FOR_EACH
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FOR_EACH_VALUE = 1;

	/**
	 * The '<em><b>END</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> If the story action node that
	 * is used as the source of the activity edge, the edge is followed when no
	 * valid match could be found for the story pattern of the story action
	 * node. <!-- end-model-doc -->
	 * 
	 * @see #END
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int END_VALUE = 2;

	/**
	 * The '<em><b>SUCCESS</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The activity edge is followed
	 * if a valid match was found for the story pattern of the story action node
	 * where the activity edge starts. <!-- end-model-doc -->
	 * 
	 * @see #SUCCESS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SUCCESS_VALUE = 3;

	/**
	 * The '<em><b>FAILURE</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The activity edge is followed
	 * if no valid match was found for the story pattern of the story action
	 * node where the activity edge starts. <!-- end-model-doc -->
	 * 
	 * @see #FAILURE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FAILURE_VALUE = 4;

	/**
	 * The '<em><b>BOOLEAN</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The activity edge is followed
	 * if the Boolean condition holds. Used in conjunction with ELSE. <!--
	 * end-model-doc -->
	 * 
	 * @see #BOOLEAN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BOOLEAN_VALUE = 5;

	/**
	 * The '<em><b>ELSE</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The activity edge is followed
	 * if the Boolean condition does not hold. Used in conjunction with BOOLEAN.
	 * <!-- end-model-doc -->
	 * 
	 * @see #ELSE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ELSE_VALUE = 6;

	/**
	 * An array of all the '<em><b>Activity Edge Guard Enumeration</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static final ActivityEdgeGuardEnumeration[] VALUES_ARRAY = new ActivityEdgeGuardEnumeration[] {
			NONE, FOR_EACH, END, SUCCESS, FAILURE, BOOLEAN, ELSE, };

	/**
	 * A public read-only list of all the '
	 * <em><b>Activity Edge Guard Enumeration</b></em>' enumerators. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final List<ActivityEdgeGuardEnumeration> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Activity Edge Guard Enumeration</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static ActivityEdgeGuardEnumeration get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ActivityEdgeGuardEnumeration result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Activity Edge Guard Enumeration</b></em>' literal with the specified name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static ActivityEdgeGuardEnumeration getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ActivityEdgeGuardEnumeration result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Activity Edge Guard Enumeration</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static ActivityEdgeGuardEnumeration get(int value) {
		switch (value) {
		case NONE_VALUE:
			return NONE;
		case FOR_EACH_VALUE:
			return FOR_EACH;
		case END_VALUE:
			return END;
		case SUCCESS_VALUE:
			return SUCCESS;
		case FAILURE_VALUE:
			return FAILURE;
		case BOOLEAN_VALUE:
			return BOOLEAN;
		case ELSE_VALUE:
			return ELSE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	private ActivityEdgeGuardEnumeration(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} // ActivityEdgeGuardEnumeration
