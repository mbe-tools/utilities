/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Abstract Story Pattern Link</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * AbstractStoryPatternLink is the super class of all StoryPatternLinks.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getSource <em>Source</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getTarget <em>Target</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#isCheckOnlyExistence <em>Check Only Existence</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getMatchingPriority <em>Matching Priority</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getOppositeStoryPatternLink <em>Opposite Story Pattern Link</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAbstractStoryPatternLink()
 * @model abstract="true"
 * @generated
 */
public interface AbstractStoryPatternLink extends StoryPatternElement {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getOutgoingStoryLinks
	 * <em>Outgoing Story Links</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The source story pattern object
	 * of this link. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(AbstractStoryPatternObject)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAbstractStoryPatternLink_Source()
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getOutgoingStoryLinks
	 * @model opposite="outgoingStoryLinks" required="true"
	 * @generated
	 */
	AbstractStoryPatternObject getSource();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getSource
	 * <em>Source</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(AbstractStoryPatternObject value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getIncomingStoryLinks
	 * <em>Incoming Story Links</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The target story pattern object
	 * of this link. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(AbstractStoryPatternObject)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAbstractStoryPatternLink_Target()
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getIncomingStoryLinks
	 * @model opposite="incomingStoryLinks" required="true"
	 * @generated
	 */
	AbstractStoryPatternObject getTarget();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getTarget
	 * <em>Target</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(AbstractStoryPatternObject value);

	/**
	 * Returns the value of the '<em><b>Check Only Existence</b></em>'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> If true, the pattern matcher should not use this link
	 * to match objects and check only the existence of the link. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Check Only Existence</em>' attribute.
	 * @see #setCheckOnlyExistence(boolean)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAbstractStoryPatternLink_CheckOnlyExistence()
	 * @model
	 * @generated
	 */
	boolean isCheckOnlyExistence();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#isCheckOnlyExistence <em>Check Only Existence</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Check Only Existence</em>' attribute.
	 * @see #isCheckOnlyExistence()
	 * @generated
	 */
	void setCheckOnlyExistence(boolean value);

	/**
	 * Returns the value of the '<em><b>Matching Priority</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The matching priority of this link. If the matching priority is set, i.e.
	 * not zero, the pattern matcher will choose links with the highest matching
	 * priority first to bind elements. The highest priority is one. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Matching Priority</em>' attribute.
	 * @see #setMatchingPriority(int)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAbstractStoryPatternLink_MatchingPriority()
	 * @model
	 * @generated
	 */
	int getMatchingPriority();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getMatchingPriority <em>Matching Priority</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Matching Priority</em>' attribute.
	 * @see #getMatchingPriority()
	 * @generated
	 */
	void setMatchingPriority(int value);

	/**
	 * Returns the value of the '<em><b>Opposite Story Pattern Link</b></em>'
	 * reference. It is bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getOppositeStoryPatternLink
	 * <em>Opposite Story Pattern Link</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> This reference points to the
	 * StoryPatternLink that is associated with the opposite EReference of this
	 * StoryPatternLinks EReference. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Opposite Story Pattern Link</em>'
	 *         reference.
	 * @see #setOppositeStoryPatternLink(AbstractStoryPatternLink)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAbstractStoryPatternLink_OppositeStoryPatternLink()
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getOppositeStoryPatternLink
	 * @model opposite="oppositeStoryPatternLink"
	 * @generated
	 */
	AbstractStoryPatternLink getOppositeStoryPatternLink();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getOppositeStoryPatternLink <em>Opposite Story Pattern Link</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opposite Story Pattern Link</em>' reference.
	 * @see #getOppositeStoryPatternLink()
	 * @generated
	 */
	void setOppositeStoryPatternLink(AbstractStoryPatternLink value);

} // AbstractStoryPatternLink
