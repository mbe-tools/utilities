/**
 */
package de.hpi.sam.storyDiagramEcore.callActions;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;

import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Get Property Value Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction#getInstanceVariable <em>Instance Variable</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction#getProperty <em>Property</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getGetPropertyValueAction()
 * @model
 * @generated
 */
public interface GetPropertyValueAction extends CallAction {
	/**
	 * Returns the value of the '<em><b>Instance Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * An Expression that must return an object in whose context the method will be executed.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Instance Variable</em>' containment reference.
	 * @see #setInstanceVariable(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getGetPropertyValueAction_InstanceVariable()
	 * @model containment="true"
	 * @generated
	 */
	Expression getInstanceVariable();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction#getInstanceVariable <em>Instance Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance Variable</em>' containment reference.
	 * @see #getInstanceVariable()
	 * @generated
	 */
	void setInstanceVariable(Expression value);

	/**
	 * Returns the value of the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' reference.
	 * @see #setProperty(EStructuralFeature)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getGetPropertyValueAction_Property()
	 * @model required="true"
	 * @generated
	 */
	EStructuralFeature getProperty();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction#getProperty <em>Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' reference.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(EStructuralFeature value);

} // GetPropertyValueAction
