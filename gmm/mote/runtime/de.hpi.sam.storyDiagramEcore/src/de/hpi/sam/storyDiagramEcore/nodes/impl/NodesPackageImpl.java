/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage;
import de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsPackageImpl;
import de.hpi.sam.storyDiagramEcore.helpers.HelpersPackage;
import de.hpi.sam.storyDiagramEcore.helpers.impl.HelpersPackageImpl;
import de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl;
import de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.DecisionNode;
import de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.ForEachSemanticsEnumeration;
import de.hpi.sam.storyDiagramEcore.nodes.ForkNode;
import de.hpi.sam.storyDiagramEcore.nodes.InitialNode;
import de.hpi.sam.storyDiagramEcore.nodes.JoinNode;
import de.hpi.sam.storyDiagramEcore.nodes.MergeNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesFactory;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge;
import de.hpi.sam.storyDiagramEcore.nodes.Semaphore;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class NodesPackageImpl extends EPackageImpl implements NodesPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activityEdgeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activityFinalNodeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flowFinalNodeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activityNodeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expressionActivityNodeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass decisionNodeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass initialNodeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mergeNodeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass storyActionNodeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass forkNodeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass joinNodeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass semaphoreEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass synchronizationEdgeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass releaseSemaphoreEdgeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acquireSemaphoreEdgeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum activityEdgeGuardEnumerationEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum forEachSemanticsEnumerationEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private NodesPackageImpl() {
		super(eNS_URI, NodesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link NodesPackage#eINSTANCE} when
	 * that field is accessed. Clients should not invoke it directly. Instead,
	 * they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static NodesPackage init() {
		if (isInited)
			return (NodesPackage) EPackage.Registry.INSTANCE
					.getEPackage(NodesPackage.eNS_URI);

		// Obtain or create and register package
		NodesPackageImpl theNodesPackage = (NodesPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof NodesPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new NodesPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		StoryDiagramEcorePackageImpl theStoryDiagramEcorePackage = (StoryDiagramEcorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI) instanceof StoryDiagramEcorePackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI)
				: StoryDiagramEcorePackage.eINSTANCE);
		CallActionsPackageImpl theCallActionsPackage = (CallActionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(CallActionsPackage.eNS_URI) instanceof CallActionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(CallActionsPackage.eNS_URI)
				: CallActionsPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI)
				: ExpressionsPackage.eINSTANCE);
		SdmPackageImpl theSdmPackage = (SdmPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(SdmPackage.eNS_URI) instanceof SdmPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(SdmPackage.eNS_URI) : SdmPackage.eINSTANCE);
		HelpersPackageImpl theHelpersPackage = (HelpersPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(HelpersPackage.eNS_URI) instanceof HelpersPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(HelpersPackage.eNS_URI) : HelpersPackage.eINSTANCE);

		// Create package meta-data objects
		theNodesPackage.createPackageContents();
		theStoryDiagramEcorePackage.createPackageContents();
		theCallActionsPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theSdmPackage.createPackageContents();
		theHelpersPackage.createPackageContents();

		// Initialize created meta-data
		theNodesPackage.initializePackageContents();
		theStoryDiagramEcorePackage.initializePackageContents();
		theCallActionsPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theSdmPackage.initializePackageContents();
		theHelpersPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theNodesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(NodesPackage.eNS_URI, theNodesPackage);
		return theNodesPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActivityEdge() {
		return activityEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityEdge_Source() {
		return (EReference) activityEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityEdge_Target() {
		return (EReference) activityEdgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityEdge_Activity() {
		return (EReference) activityEdgeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivityEdge_GuardType() {
		return (EAttribute) activityEdgeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityEdge_GuardExpression() {
		return (EReference) activityEdgeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityEdge_ReleaseEdges() {
		return (EReference) activityEdgeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityEdge_AcquireEdges() {
		return (EReference) activityEdgeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActivityFinalNode() {
		return activityFinalNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityFinalNode_ReturnValue() {
		return (EReference) activityFinalNodeEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityFinalNode_OutParameterValues() {
		return (EReference) activityFinalNodeEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFlowFinalNode() {
		return flowFinalNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActivityNode() {
		return activityNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityNode_Incoming() {
		return (EReference) activityNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityNode_Outgoing() {
		return (EReference) activityNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityNode_Activity() {
		return (EReference) activityNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpressionActivityNode() {
		return expressionActivityNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExpressionActivityNode_Expression() {
		return (EReference) expressionActivityNodeEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDecisionNode() {
		return decisionNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInitialNode() {
		return initialNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMergeNode() {
		return mergeNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStoryActionNode() {
		return storyActionNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStoryActionNode_ForEach() {
		return (EAttribute) storyActionNodeEClass.getEStructuralFeatures().get(
				0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryActionNode_StoryPatternObjects() {
		return (EReference) storyActionNodeEClass.getEStructuralFeatures().get(
				1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryActionNode_StoryPatternLinks() {
		return (EReference) storyActionNodeEClass.getEStructuralFeatures().get(
				2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryActionNode_Constraints() {
		return (EReference) storyActionNodeEClass.getEStructuralFeatures().get(
				3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStoryActionNode_NacSemantic() {
		return (EAttribute) storyActionNodeEClass.getEStructuralFeatures().get(
				4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStoryActionNode_ForEachSemantics() {
		return (EAttribute) storyActionNodeEClass.getEStructuralFeatures().get(
				5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getForkNode() {
		return forkNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJoinNode() {
		return joinNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSemaphore() {
		return semaphoreEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSemaphore_SynchronizationEdges() {
		return (EReference) semaphoreEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSemaphore_TokenCount() {
		return (EAttribute) semaphoreEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSynchronizationEdge() {
		return synchronizationEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSynchronizationEdge_Semaphore() {
		return (EReference) synchronizationEdgeEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSynchronizationEdge_Weight() {
		return (EAttribute) synchronizationEdgeEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReleaseSemaphoreEdge() {
		return releaseSemaphoreEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReleaseSemaphoreEdge_ActivityEdge() {
		return (EReference) releaseSemaphoreEdgeEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcquireSemaphoreEdge() {
		return acquireSemaphoreEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcquireSemaphoreEdge_ActivityEdge() {
		return (EReference) acquireSemaphoreEdgeEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getActivityEdgeGuardEnumeration() {
		return activityEdgeGuardEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getForEachSemanticsEnumeration() {
		return forEachSemanticsEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NodesFactory getNodesFactory() {
		return (NodesFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		activityEdgeEClass = createEClass(ACTIVITY_EDGE);
		createEReference(activityEdgeEClass, ACTIVITY_EDGE__SOURCE);
		createEReference(activityEdgeEClass, ACTIVITY_EDGE__TARGET);
		createEReference(activityEdgeEClass, ACTIVITY_EDGE__ACTIVITY);
		createEAttribute(activityEdgeEClass, ACTIVITY_EDGE__GUARD_TYPE);
		createEReference(activityEdgeEClass, ACTIVITY_EDGE__GUARD_EXPRESSION);
		createEReference(activityEdgeEClass, ACTIVITY_EDGE__RELEASE_EDGES);
		createEReference(activityEdgeEClass, ACTIVITY_EDGE__ACQUIRE_EDGES);

		activityFinalNodeEClass = createEClass(ACTIVITY_FINAL_NODE);
		createEReference(activityFinalNodeEClass,
				ACTIVITY_FINAL_NODE__RETURN_VALUE);
		createEReference(activityFinalNodeEClass,
				ACTIVITY_FINAL_NODE__OUT_PARAMETER_VALUES);

		flowFinalNodeEClass = createEClass(FLOW_FINAL_NODE);

		activityNodeEClass = createEClass(ACTIVITY_NODE);
		createEReference(activityNodeEClass, ACTIVITY_NODE__INCOMING);
		createEReference(activityNodeEClass, ACTIVITY_NODE__OUTGOING);
		createEReference(activityNodeEClass, ACTIVITY_NODE__ACTIVITY);

		expressionActivityNodeEClass = createEClass(EXPRESSION_ACTIVITY_NODE);
		createEReference(expressionActivityNodeEClass,
				EXPRESSION_ACTIVITY_NODE__EXPRESSION);

		decisionNodeEClass = createEClass(DECISION_NODE);

		initialNodeEClass = createEClass(INITIAL_NODE);

		mergeNodeEClass = createEClass(MERGE_NODE);

		storyActionNodeEClass = createEClass(STORY_ACTION_NODE);
		createEAttribute(storyActionNodeEClass, STORY_ACTION_NODE__FOR_EACH);
		createEReference(storyActionNodeEClass,
				STORY_ACTION_NODE__STORY_PATTERN_OBJECTS);
		createEReference(storyActionNodeEClass,
				STORY_ACTION_NODE__STORY_PATTERN_LINKS);
		createEReference(storyActionNodeEClass, STORY_ACTION_NODE__CONSTRAINTS);
		createEAttribute(storyActionNodeEClass, STORY_ACTION_NODE__NAC_SEMANTIC);
		createEAttribute(storyActionNodeEClass,
				STORY_ACTION_NODE__FOR_EACH_SEMANTICS);

		forkNodeEClass = createEClass(FORK_NODE);

		joinNodeEClass = createEClass(JOIN_NODE);

		semaphoreEClass = createEClass(SEMAPHORE);
		createEReference(semaphoreEClass, SEMAPHORE__SYNCHRONIZATION_EDGES);
		createEAttribute(semaphoreEClass, SEMAPHORE__TOKEN_COUNT);

		synchronizationEdgeEClass = createEClass(SYNCHRONIZATION_EDGE);
		createEReference(synchronizationEdgeEClass,
				SYNCHRONIZATION_EDGE__SEMAPHORE);
		createEAttribute(synchronizationEdgeEClass,
				SYNCHRONIZATION_EDGE__WEIGHT);

		releaseSemaphoreEdgeEClass = createEClass(RELEASE_SEMAPHORE_EDGE);
		createEReference(releaseSemaphoreEdgeEClass,
				RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE);

		acquireSemaphoreEdgeEClass = createEClass(ACQUIRE_SEMAPHORE_EDGE);
		createEReference(acquireSemaphoreEdgeEClass,
				ACQUIRE_SEMAPHORE_EDGE__ACTIVITY_EDGE);

		// Create enums
		activityEdgeGuardEnumerationEEnum = createEEnum(ACTIVITY_EDGE_GUARD_ENUMERATION);
		forEachSemanticsEnumerationEEnum = createEEnum(FOR_EACH_SEMANTICS_ENUMERATION);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StoryDiagramEcorePackage theStoryDiagramEcorePackage = (StoryDiagramEcorePackage) EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI);
		ExpressionsPackage theExpressionsPackage = (ExpressionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI);
		SdmPackage theSdmPackage = (SdmPackage) EPackage.Registry.INSTANCE
				.getEPackage(SdmPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		activityEdgeEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		activityFinalNodeEClass.getESuperTypes().add(this.getActivityNode());
		flowFinalNodeEClass.getESuperTypes().add(this.getActivityNode());
		activityNodeEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		expressionActivityNodeEClass.getESuperTypes().add(
				this.getActivityNode());
		decisionNodeEClass.getESuperTypes().add(this.getActivityNode());
		initialNodeEClass.getESuperTypes().add(this.getActivityNode());
		mergeNodeEClass.getESuperTypes().add(this.getActivityNode());
		storyActionNodeEClass.getESuperTypes().add(this.getActivityNode());
		forkNodeEClass.getESuperTypes().add(this.getActivityNode());
		joinNodeEClass.getESuperTypes().add(this.getActivityNode());
		semaphoreEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		synchronizationEdgeEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		releaseSemaphoreEdgeEClass.getESuperTypes().add(
				this.getSynchronizationEdge());
		acquireSemaphoreEdgeEClass.getESuperTypes().add(
				this.getSynchronizationEdge());

		// Initialize classes and features; add operations and parameters
		initEClass(activityEdgeEClass, ActivityEdge.class, "ActivityEdge",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActivityEdge_Source(), this.getActivityNode(),
				this.getActivityNode_Outgoing(), "source", null, 1, 1,
				ActivityEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getActivityEdge_Target(), this.getActivityNode(),
				this.getActivityNode_Incoming(), "target", null, 1, 1,
				ActivityEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getActivityEdge_Activity(),
				theStoryDiagramEcorePackage.getActivity(),
				theStoryDiagramEcorePackage.getActivity_Edges(), "activity",
				null, 1, 1, ActivityEdge.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActivityEdge_GuardType(),
				this.getActivityEdgeGuardEnumeration(), "guardType", null, 1,
				1, ActivityEdge.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getActivityEdge_GuardExpression(),
				theExpressionsPackage.getExpression(), null, "guardExpression",
				null, 0, 1, ActivityEdge.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivityEdge_ReleaseEdges(),
				this.getReleaseSemaphoreEdge(),
				this.getReleaseSemaphoreEdge_ActivityEdge(), "releaseEdges",
				null, 0, -1, ActivityEdge.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivityEdge_AcquireEdges(),
				this.getAcquireSemaphoreEdge(),
				this.getAcquireSemaphoreEdge_ActivityEdge(), "acquireEdges",
				null, 0, -1, ActivityEdge.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activityFinalNodeEClass, ActivityFinalNode.class,
				"ActivityFinalNode", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActivityFinalNode_ReturnValue(),
				theExpressionsPackage.getExpression(), null, "returnValue",
				null, 0, 1, ActivityFinalNode.class, IS_TRANSIENT, IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getActivityFinalNode_OutParameterValues(),
				theExpressionsPackage.getExpression(), null,
				"outParameterValues", null, 0, -1, ActivityFinalNode.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(flowFinalNodeEClass, FlowFinalNode.class, "FlowFinalNode",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(activityNodeEClass, ActivityNode.class, "ActivityNode",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActivityNode_Incoming(), this.getActivityEdge(),
				this.getActivityEdge_Target(), "incoming", null, 0, -1,
				ActivityNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getActivityNode_Outgoing(), this.getActivityEdge(),
				this.getActivityEdge_Source(), "outgoing", null, 0, -1,
				ActivityNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getActivityNode_Activity(),
				theStoryDiagramEcorePackage.getActivity(),
				theStoryDiagramEcorePackage.getActivity_Nodes(), "activity",
				null, 1, 1, ActivityNode.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(expressionActivityNodeEClass, ExpressionActivityNode.class,
				"ExpressionActivityNode", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExpressionActivityNode_Expression(),
				theExpressionsPackage.getExpression(), null, "expression",
				null, 1, 1, ExpressionActivityNode.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(decisionNodeEClass, DecisionNode.class, "DecisionNode",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(initialNodeEClass, InitialNode.class, "InitialNode",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mergeNodeEClass, MergeNode.class, "MergeNode", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(storyActionNodeEClass, StoryActionNode.class,
				"StoryActionNode", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStoryActionNode_ForEach(),
				ecorePackage.getEBoolean(), "forEach", null, 0, 1,
				StoryActionNode.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getStoryActionNode_StoryPatternObjects(),
				theSdmPackage.getAbstractStoryPatternObject(), null,
				"storyPatternObjects", null, 0, -1, StoryActionNode.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getStoryActionNode_StoryPatternLinks(),
				theSdmPackage.getAbstractStoryPatternLink(), null,
				"storyPatternLinks", null, 0, -1, StoryActionNode.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getStoryActionNode_Constraints(),
				theExpressionsPackage.getExpression(), null, "constraints",
				null, 0, -1, StoryActionNode.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStoryActionNode_NacSemantic(),
				theSdmPackage.getStoryPatternNACSematicEnumeration(),
				"nacSemantic", "OR", 0, 1, StoryActionNode.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStoryActionNode_ForEachSemantics(),
				this.getForEachSemanticsEnumeration(), "forEachSemantics",
				"FRESH_MATCH", 0, 1, StoryActionNode.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(forkNodeEClass, ForkNode.class, "ForkNode", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(joinNodeEClass, JoinNode.class, "JoinNode", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(semaphoreEClass, Semaphore.class, "Semaphore", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSemaphore_SynchronizationEdges(),
				this.getSynchronizationEdge(),
				this.getSynchronizationEdge_Semaphore(),
				"synchronizationEdges", null, 0, -1, Semaphore.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getSemaphore_TokenCount(), ecorePackage.getEInt(),
				"tokenCount", null, 1, 1, Semaphore.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(synchronizationEdgeEClass, SynchronizationEdge.class,
				"SynchronizationEdge", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSynchronizationEdge_Semaphore(), this.getSemaphore(),
				this.getSemaphore_SynchronizationEdges(), "semaphore", null, 1,
				1, SynchronizationEdge.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSynchronizationEdge_Weight(), ecorePackage.getEInt(),
				"weight", null, 0, 1, SynchronizationEdge.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(releaseSemaphoreEdgeEClass, ReleaseSemaphoreEdge.class,
				"ReleaseSemaphoreEdge", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReleaseSemaphoreEdge_ActivityEdge(),
				this.getActivityEdge(), this.getActivityEdge_ReleaseEdges(),
				"activityEdge", null, 1, 1, ReleaseSemaphoreEdge.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(acquireSemaphoreEdgeEClass, AcquireSemaphoreEdge.class,
				"AcquireSemaphoreEdge", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAcquireSemaphoreEdge_ActivityEdge(),
				this.getActivityEdge(), this.getActivityEdge_AcquireEdges(),
				"activityEdge", null, 1, 1, AcquireSemaphoreEdge.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(activityEdgeGuardEnumerationEEnum,
				ActivityEdgeGuardEnumeration.class,
				"ActivityEdgeGuardEnumeration");
		addEEnumLiteral(activityEdgeGuardEnumerationEEnum,
				ActivityEdgeGuardEnumeration.NONE);
		addEEnumLiteral(activityEdgeGuardEnumerationEEnum,
				ActivityEdgeGuardEnumeration.FOR_EACH);
		addEEnumLiteral(activityEdgeGuardEnumerationEEnum,
				ActivityEdgeGuardEnumeration.END);
		addEEnumLiteral(activityEdgeGuardEnumerationEEnum,
				ActivityEdgeGuardEnumeration.SUCCESS);
		addEEnumLiteral(activityEdgeGuardEnumerationEEnum,
				ActivityEdgeGuardEnumeration.FAILURE);
		addEEnumLiteral(activityEdgeGuardEnumerationEEnum,
				ActivityEdgeGuardEnumeration.BOOLEAN);
		addEEnumLiteral(activityEdgeGuardEnumerationEEnum,
				ActivityEdgeGuardEnumeration.ELSE);

		initEEnum(forEachSemanticsEnumerationEEnum,
				ForEachSemanticsEnumeration.class,
				"ForEachSemanticsEnumeration");
		addEEnumLiteral(forEachSemanticsEnumerationEEnum,
				ForEachSemanticsEnumeration.FRESH_MATCH);
		addEEnumLiteral(forEachSemanticsEnumerationEEnum,
				ForEachSemanticsEnumeration.PRE_SELECT);
		addEEnumLiteral(forEachSemanticsEnumerationEEnum,
				ForEachSemanticsEnumeration.INTERLEAVED);
	}

} // NodesPackageImpl
