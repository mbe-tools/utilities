/**
 */
package de.hpi.sam.storyDiagramEcore.callActions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Comparator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getAbstractComparator()
 * @model abstract="true"
 * @generated
 */
public interface AbstractComparator extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model object1Required="true" object2Required="true"
	 * @generated
	 */
	boolean equals(Object object1, Object object2);

} // AbstractComparator
