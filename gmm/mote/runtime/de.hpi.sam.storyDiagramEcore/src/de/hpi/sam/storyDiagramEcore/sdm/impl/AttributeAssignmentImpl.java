/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import de.hpi.sam.storyDiagramEcore.callActions.AbstractComparator;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Attribute Assignment</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AttributeAssignmentImpl#getEStructuralFeature <em>EStructural Feature</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AttributeAssignmentImpl#getAssignmentExpression <em>Assignment Expression</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.AttributeAssignmentImpl#getCustomComparator <em>Custom Comparator</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributeAssignmentImpl extends NamedElementImpl implements
		AttributeAssignment {
	/**
	 * The cached value of the '{@link #getEStructuralFeature() <em>EStructural Feature</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getEStructuralFeature()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature eStructuralFeature;

	/**
	 * The cached value of the '{@link #getAssignmentExpression()
	 * <em>Assignment Expression</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getAssignmentExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression assignmentExpression;

	/**
	 * The cached value of the '{@link #getCustomComparator() <em>Custom Comparator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomComparator()
	 * @generated
	 * @ordered
	 */
	protected AbstractComparator customComparator;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeAssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SdmPackage.Literals.ATTRIBUTE_ASSIGNMENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature getEStructuralFeature() {
		if (eStructuralFeature != null && eStructuralFeature.eIsProxy()) {
			InternalEObject oldEStructuralFeature = (InternalEObject) eStructuralFeature;
			eStructuralFeature = (EStructuralFeature) eResolveProxy(oldEStructuralFeature);
			if (eStructuralFeature != oldEStructuralFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							SdmPackage.ATTRIBUTE_ASSIGNMENT__ESTRUCTURAL_FEATURE,
							oldEStructuralFeature, eStructuralFeature));
			}
		}
		return eStructuralFeature;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature basicGetEStructuralFeature() {
		return eStructuralFeature;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setEStructuralFeature(EStructuralFeature newEStructuralFeature) {
		EStructuralFeature oldEStructuralFeature = eStructuralFeature;
		eStructuralFeature = newEStructuralFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.ATTRIBUTE_ASSIGNMENT__ESTRUCTURAL_FEATURE,
					oldEStructuralFeature, eStructuralFeature));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getAssignmentExpression() {
		return assignmentExpression;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssignmentExpression(
			Expression newAssignmentExpression, NotificationChain msgs) {
		Expression oldAssignmentExpression = assignmentExpression;
		assignmentExpression = newAssignmentExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					SdmPackage.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION,
					oldAssignmentExpression, newAssignmentExpression);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssignmentExpression(Expression newAssignmentExpression) {
		if (newAssignmentExpression != assignmentExpression) {
			NotificationChain msgs = null;
			if (assignmentExpression != null)
				msgs = ((InternalEObject) assignmentExpression)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- SdmPackage.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION,
								null, msgs);
			if (newAssignmentExpression != null)
				msgs = ((InternalEObject) newAssignmentExpression)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- SdmPackage.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION,
								null, msgs);
			msgs = basicSetAssignmentExpression(newAssignmentExpression, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION,
					newAssignmentExpression, newAssignmentExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractComparator getCustomComparator() {
		return customComparator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCustomComparator(
			AbstractComparator newCustomComparator, NotificationChain msgs) {
		AbstractComparator oldCustomComparator = customComparator;
		customComparator = newCustomComparator;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					SdmPackage.ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR,
					oldCustomComparator, newCustomComparator);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCustomComparator(AbstractComparator newCustomComparator) {
		if (newCustomComparator != customComparator) {
			NotificationChain msgs = null;
			if (customComparator != null)
				msgs = ((InternalEObject) customComparator)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- SdmPackage.ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR,
								null, msgs);
			if (newCustomComparator != null)
				msgs = ((InternalEObject) newCustomComparator)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- SdmPackage.ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR,
								null, msgs);
			msgs = basicSetCustomComparator(newCustomComparator, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR,
					newCustomComparator, newCustomComparator));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION:
			return basicSetAssignmentExpression(null, msgs);
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR:
			return basicSetCustomComparator(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__ESTRUCTURAL_FEATURE:
			if (resolve)
				return getEStructuralFeature();
			return basicGetEStructuralFeature();
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION:
			return getAssignmentExpression();
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR:
			return getCustomComparator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__ESTRUCTURAL_FEATURE:
			setEStructuralFeature((EStructuralFeature) newValue);
			return;
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION:
			setAssignmentExpression((Expression) newValue);
			return;
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR:
			setCustomComparator((AbstractComparator) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__ESTRUCTURAL_FEATURE:
			setEStructuralFeature((EStructuralFeature) null);
			return;
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION:
			setAssignmentExpression((Expression) null);
			return;
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR:
			setCustomComparator((AbstractComparator) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__ESTRUCTURAL_FEATURE:
			return eStructuralFeature != null;
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION:
			return assignmentExpression != null;
		case SdmPackage.ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR:
			return customComparator != null;
		}
		return super.eIsSet(featureID);
	}

} // AttributeAssignmentImpl
