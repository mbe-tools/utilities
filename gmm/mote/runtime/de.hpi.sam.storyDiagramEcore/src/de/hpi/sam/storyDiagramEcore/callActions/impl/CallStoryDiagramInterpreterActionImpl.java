/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Call Story Diagram Interpreter Action</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CallStoryDiagramInterpreterActionImpl#getActivity <em>Activity</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CallStoryDiagramInterpreterActionImpl#getParameters <em>Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CallStoryDiagramInterpreterActionImpl extends CallActionImpl
		implements CallStoryDiagramInterpreterAction {
	/**
	 * The cached value of the '{@link #getActivity() <em>Activity</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getActivity()
	 * @generated
	 * @ordered
	 */
	protected Activity activity;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<CallActionParameter> parameters;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected CallStoryDiagramInterpreterActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CallActionsPackage.Literals.CALL_STORY_DIAGRAM_INTERPRETER_ACTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Activity getActivity() {
		if (activity != null && activity.eIsProxy()) {
			InternalEObject oldActivity = (InternalEObject) activity;
			activity = (Activity) eResolveProxy(oldActivity);
			if (activity != oldActivity) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION__ACTIVITY,
							oldActivity, activity));
			}
		}
		return activity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Activity basicGetActivity() {
		return activity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivity(Activity newActivity) {
		Activity oldActivity = activity;
		activity = newActivity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION__ACTIVITY,
					oldActivity, activity));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CallActionParameter> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<CallActionParameter>(
					CallActionParameter.class,
					this,
					CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			super.toString();

		StringBuffer buffer = new StringBuffer();

		buffer.append("call SDInterpreter(");

		if (getActivity() != null) {
			if (getActivity().getSpecification() != null) {
				buffer.append(getActivity().getSpecification().getName());
			} else {
				if (getActivity().getName() != null
						&& !"".equals(getActivity().getName())) {
					buffer.append(getActivity().getName());
				} else {
					buffer.append("[null]");
				}
			}
		} else {
			buffer.append("[null]");
		}

		buffer.append(", ");

		for (CallActionParameter parameter : getParameters()) {
			buffer.append(parameter.getName());
			buffer.append(":");
			if (parameter.getParameterClassfier() != null) {
				buffer.append(parameter.getParameterClassfier().getName());
			}
			buffer.append(" := ");
			buffer.append(parameter.getParameterValueAction());

			buffer.append(", ");
		}

		buffer.delete(buffer.length() - 2, buffer.length());

		buffer.append(")");

		return buffer.toString().trim();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION__PARAMETERS:
			return ((InternalEList<?>) getParameters()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION__ACTIVITY:
			if (resolve)
				return getActivity();
			return basicGetActivity();
		case CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION__PARAMETERS:
			return getParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION__ACTIVITY:
			setActivity((Activity) newValue);
			return;
		case CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION__PARAMETERS:
			getParameters().clear();
			getParameters().addAll(
					(Collection<? extends CallActionParameter>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION__ACTIVITY:
			setActivity((Activity) null);
			return;
		case CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION__PARAMETERS:
			getParameters().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION__ACTIVITY:
			return activity != null;
		case CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION__PARAMETERS:
			return parameters != null && !parameters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // CallStoryDiagramInterpreterActionImpl
