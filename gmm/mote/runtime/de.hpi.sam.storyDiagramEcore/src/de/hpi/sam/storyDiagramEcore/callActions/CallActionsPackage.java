/**
 */
package de.hpi.sam.storyDiagramEcore.callActions;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsFactory
 * @model kind="package"
 * @generated
 */
public interface CallActionsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "callActions";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de/hpi/sam/storyDiagramEcore/callActions.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.hpi.sam.storyDiagramEcore.callActions";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CallActionsPackage eINSTANCE = de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionImpl <em>Call Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getCallAction()
	 * @generated
	 */
	int CALL_ACTION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__CLASSIFIER = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Call Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionParameterImpl <em>Call Action Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionParameterImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getCallActionParameter()
	 * @generated
	 */
	int CALL_ACTION_PARAMETER = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_PARAMETER__NAME = StoryDiagramEcorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_PARAMETER__DESCRIPTION = StoryDiagramEcorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_PARAMETER__UUID = StoryDiagramEcorePackage.NAMED_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Parameter Value Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter Classfier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_PARAMETER__PARAMETER_CLASSFIER = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Call Action Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_PARAMETER_FEATURE_COUNT = StoryDiagramEcorePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CallStoryDiagramInterpreterActionImpl <em>Call Story Diagram Interpreter Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallStoryDiagramInterpreterActionImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getCallStoryDiagramInterpreterAction()
	 * @generated
	 */
	int CALL_STORY_DIAGRAM_INTERPRETER_ACTION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STORY_DIAGRAM_INTERPRETER_ACTION__NAME = CALL_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STORY_DIAGRAM_INTERPRETER_ACTION__DESCRIPTION = CALL_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STORY_DIAGRAM_INTERPRETER_ACTION__UUID = CALL_ACTION__UUID;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STORY_DIAGRAM_INTERPRETER_ACTION__CLASSIFIER = CALL_ACTION__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STORY_DIAGRAM_INTERPRETER_ACTION__ACTIVITY = CALL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STORY_DIAGRAM_INTERPRETER_ACTION__PARAMETERS = CALL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Call Story Diagram Interpreter Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STORY_DIAGRAM_INTERPRETER_ACTION_FEATURE_COUNT = CALL_ACTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.LiteralDeclarationActionImpl <em>Literal Declaration Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.LiteralDeclarationActionImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getLiteralDeclarationAction()
	 * @generated
	 */
	int LITERAL_DECLARATION_ACTION = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_DECLARATION_ACTION__NAME = CALL_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_DECLARATION_ACTION__DESCRIPTION = CALL_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_DECLARATION_ACTION__UUID = CALL_ACTION__UUID;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_DECLARATION_ACTION__CLASSIFIER = CALL_ACTION__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_DECLARATION_ACTION__LITERAL = CALL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Literal Declaration Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_DECLARATION_ACTION_FEATURE_COUNT = CALL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.MethodCallActionImpl <em>Method Call Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.MethodCallActionImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getMethodCallAction()
	 * @generated
	 */
	int METHOD_CALL_ACTION = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_ACTION__NAME = CALL_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_ACTION__DESCRIPTION = CALL_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_ACTION__UUID = CALL_ACTION__UUID;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_ACTION__CLASSIFIER = CALL_ACTION__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_ACTION__PARAMETERS = CALL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Method</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_ACTION__METHOD = CALL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Method Class Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_ACTION__METHOD_CLASS_NAME = CALL_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Method Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_ACTION__METHOD_NAME = CALL_ACTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Instance Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_ACTION__INSTANCE_VARIABLE = CALL_ACTION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Method Call Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_ACTION_FEATURE_COUNT = CALL_ACTION_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.NewObjectActionImpl <em>New Object Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.NewObjectActionImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getNewObjectAction()
	 * @generated
	 */
	int NEW_OBJECT_ACTION = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_OBJECT_ACTION__NAME = CALL_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_OBJECT_ACTION__DESCRIPTION = CALL_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_OBJECT_ACTION__UUID = CALL_ACTION__UUID;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_OBJECT_ACTION__CLASSIFIER = CALL_ACTION__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Constructor Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_OBJECT_ACTION__CONSTRUCTOR_PARAMETERS = CALL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>New Object Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_OBJECT_ACTION_FEATURE_COUNT = CALL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.VariableReferenceActionImpl <em>Variable Reference Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.VariableReferenceActionImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getVariableReferenceAction()
	 * @generated
	 */
	int VARIABLE_REFERENCE_ACTION = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE_ACTION__NAME = CALL_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE_ACTION__DESCRIPTION = CALL_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE_ACTION__UUID = CALL_ACTION__UUID;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE_ACTION__CLASSIFIER = CALL_ACTION__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Variable Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE_ACTION__VARIABLE_NAME = CALL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable Reference Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE_ACTION_FEATURE_COUNT = CALL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.VariableDeclarationActionImpl <em>Variable Declaration Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.VariableDeclarationActionImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getVariableDeclarationAction()
	 * @generated
	 */
	int VARIABLE_DECLARATION_ACTION = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION_ACTION__NAME = VARIABLE_REFERENCE_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION_ACTION__DESCRIPTION = VARIABLE_REFERENCE_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION_ACTION__UUID = VARIABLE_REFERENCE_ACTION__UUID;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION_ACTION__CLASSIFIER = VARIABLE_REFERENCE_ACTION__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Variable Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION_ACTION__VARIABLE_NAME = VARIABLE_REFERENCE_ACTION__VARIABLE_NAME;

	/**
	 * The feature id for the '<em><b>Value Assignment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION_ACTION__VALUE_ASSIGNMENT = VARIABLE_REFERENCE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable Declaration Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION_ACTION_FEATURE_COUNT = VARIABLE_REFERENCE_ACTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CompareActionImpl <em>Compare Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CompareActionImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getCompareAction()
	 * @generated
	 */
	int COMPARE_ACTION = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_ACTION__NAME = CALL_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_ACTION__DESCRIPTION = CALL_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_ACTION__UUID = CALL_ACTION__UUID;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_ACTION__CLASSIFIER = CALL_ACTION__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Expression1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_ACTION__EXPRESSION1 = CALL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_ACTION__EXPRESSION2 = CALL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Comparator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_ACTION__COMPARATOR = CALL_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Compare Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_ACTION_FEATURE_COUNT = CALL_ACTION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.NullValueActionImpl <em>Null Value Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.NullValueActionImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getNullValueAction()
	 * @generated
	 */
	int NULL_VALUE_ACTION = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VALUE_ACTION__NAME = CALL_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VALUE_ACTION__DESCRIPTION = CALL_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VALUE_ACTION__UUID = CALL_ACTION__UUID;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VALUE_ACTION__CLASSIFIER = CALL_ACTION__CLASSIFIER;

	/**
	 * The number of structural features of the '<em>Null Value Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VALUE_ACTION_FEATURE_COUNT = CALL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.OperationActionImpl <em>Operation Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.OperationActionImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getOperationAction()
	 * @generated
	 */
	int OPERATION_ACTION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ACTION__NAME = CALL_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ACTION__DESCRIPTION = CALL_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ACTION__UUID = CALL_ACTION__UUID;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ACTION__CLASSIFIER = CALL_ACTION__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Operand1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ACTION__OPERAND1 = CALL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operand2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ACTION__OPERAND2 = CALL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ACTION__OPERATOR = CALL_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Operation Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_ACTION_FEATURE_COUNT = CALL_ACTION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.AbstractComparatorImpl <em>Abstract Comparator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.AbstractComparatorImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getAbstractComparator()
	 * @generated
	 */
	int ABSTRACT_COMPARATOR = 11;

	/**
	 * The number of structural features of the '<em>Abstract Comparator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPARATOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CaseInsensitiveComparatorImpl <em>Case Insensitive Comparator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CaseInsensitiveComparatorImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getCaseInsensitiveComparator()
	 * @generated
	 */
	int CASE_INSENSITIVE_COMPARATOR = 12;

	/**
	 * The number of structural features of the '<em>Case Insensitive Comparator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_INSENSITIVE_COMPARATOR_FEATURE_COUNT = ABSTRACT_COMPARATOR_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.GetPropertyValueActionImpl <em>Get Property Value Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.GetPropertyValueActionImpl
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getGetPropertyValueAction()
	 * @generated
	 */
	int GET_PROPERTY_VALUE_ACTION = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PROPERTY_VALUE_ACTION__NAME = CALL_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PROPERTY_VALUE_ACTION__DESCRIPTION = CALL_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PROPERTY_VALUE_ACTION__UUID = CALL_ACTION__UUID;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PROPERTY_VALUE_ACTION__CLASSIFIER = CALL_ACTION__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Instance Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PROPERTY_VALUE_ACTION__INSTANCE_VARIABLE = CALL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PROPERTY_VALUE_ACTION__PROPERTY = CALL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Get Property Value Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PROPERTY_VALUE_ACTION_FEATURE_COUNT = CALL_ACTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.storyDiagramEcore.callActions.Operators <em>Operators</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.storyDiagramEcore.callActions.Operators
	 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getOperators()
	 * @generated
	 */
	int OPERATORS = 14;

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.CallAction <em>Call Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Action</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallAction
	 * @generated
	 */
	EClass getCallAction();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.callActions.CallAction#getClassifier <em>Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Classifier</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallAction#getClassifier()
	 * @see #getCallAction()
	 * @generated
	 */
	EReference getCallAction_Classifier();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter <em>Call Action Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Action Parameter</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter
	 * @generated
	 */
	EClass getCallActionParameter();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter#getParameterValueAction <em>Parameter Value Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter Value Action</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter#getParameterValueAction()
	 * @see #getCallActionParameter()
	 * @generated
	 */
	EReference getCallActionParameter_ParameterValueAction();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter#getParameterClassfier <em>Parameter Classfier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter Classfier</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter#getParameterClassfier()
	 * @see #getCallActionParameter()
	 * @generated
	 */
	EReference getCallActionParameter_ParameterClassfier();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction <em>Call Story Diagram Interpreter Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Story Diagram Interpreter Action</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction
	 * @generated
	 */
	EClass getCallStoryDiagramInterpreterAction();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction#getActivity <em>Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Activity</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction#getActivity()
	 * @see #getCallStoryDiagramInterpreterAction()
	 * @generated
	 */
	EReference getCallStoryDiagramInterpreterAction_Activity();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction#getParameters()
	 * @see #getCallStoryDiagramInterpreterAction()
	 * @generated
	 */
	EReference getCallStoryDiagramInterpreterAction_Parameters();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction <em>Literal Declaration Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Literal Declaration Action</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction
	 * @generated
	 */
	EClass getLiteralDeclarationAction();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Literal</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction#getLiteral()
	 * @see #getLiteralDeclarationAction()
	 * @generated
	 */
	EAttribute getLiteralDeclarationAction_Literal();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction <em>Method Call Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Method Call Action</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction
	 * @generated
	 */
	EClass getMethodCallAction();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getParameters()
	 * @see #getMethodCallAction()
	 * @generated
	 */
	EReference getMethodCallAction_Parameters();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Method</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getMethod()
	 * @see #getMethodCallAction()
	 * @generated
	 */
	EReference getMethodCallAction_Method();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getMethodClassName <em>Method Class Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Method Class Name</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getMethodClassName()
	 * @see #getMethodCallAction()
	 * @generated
	 */
	EAttribute getMethodCallAction_MethodClassName();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getMethodName <em>Method Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Method Name</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getMethodName()
	 * @see #getMethodCallAction()
	 * @generated
	 */
	EAttribute getMethodCallAction_MethodName();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getInstanceVariable <em>Instance Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Instance Variable</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction#getInstanceVariable()
	 * @see #getMethodCallAction()
	 * @generated
	 */
	EReference getMethodCallAction_InstanceVariable();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction <em>New Object Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>New Object Action</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction
	 * @generated
	 */
	EClass getNewObjectAction();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction#getConstructorParameters <em>Constructor Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constructor Parameters</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction#getConstructorParameters()
	 * @see #getNewObjectAction()
	 * @generated
	 */
	EReference getNewObjectAction_ConstructorParameters();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction <em>Variable Declaration Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Declaration Action</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction
	 * @generated
	 */
	EClass getVariableDeclarationAction();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction#getValueAssignment <em>Value Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value Assignment</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction#getValueAssignment()
	 * @see #getVariableDeclarationAction()
	 * @generated
	 */
	EReference getVariableDeclarationAction_ValueAssignment();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction <em>Variable Reference Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Reference Action</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction
	 * @generated
	 */
	EClass getVariableReferenceAction();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction#getVariableName <em>Variable Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variable Name</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction#getVariableName()
	 * @see #getVariableReferenceAction()
	 * @generated
	 */
	EAttribute getVariableReferenceAction_VariableName();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.CompareAction <em>Compare Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compare Action</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CompareAction
	 * @generated
	 */
	EClass getCompareAction();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.callActions.CompareAction#getExpression1 <em>Expression1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression1</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CompareAction#getExpression1()
	 * @see #getCompareAction()
	 * @generated
	 */
	EReference getCompareAction_Expression1();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.callActions.CompareAction#getExpression2 <em>Expression2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression2</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CompareAction#getExpression2()
	 * @see #getCompareAction()
	 * @generated
	 */
	EReference getCompareAction_Expression2();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.callActions.CompareAction#getComparator <em>Comparator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Comparator</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CompareAction#getComparator()
	 * @see #getCompareAction()
	 * @generated
	 */
	EReference getCompareAction_Comparator();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.NullValueAction <em>Null Value Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Null Value Action</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.NullValueAction
	 * @generated
	 */
	EClass getNullValueAction();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.OperationAction <em>Operation Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Action</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.OperationAction
	 * @generated
	 */
	EClass getOperationAction();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.callActions.OperationAction#getOperand1 <em>Operand1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand1</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.OperationAction#getOperand1()
	 * @see #getOperationAction()
	 * @generated
	 */
	EReference getOperationAction_Operand1();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.callActions.OperationAction#getOperand2 <em>Operand2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand2</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.OperationAction#getOperand2()
	 * @see #getOperationAction()
	 * @generated
	 */
	EReference getOperationAction_Operand2();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.storyDiagramEcore.callActions.OperationAction#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.OperationAction#getOperator()
	 * @see #getOperationAction()
	 * @generated
	 */
	EAttribute getOperationAction_Operator();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.AbstractComparator <em>Abstract Comparator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Comparator</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.AbstractComparator
	 * @generated
	 */
	EClass getAbstractComparator();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.CaseInsensitiveComparator <em>Case Insensitive Comparator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Case Insensitive Comparator</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CaseInsensitiveComparator
	 * @generated
	 */
	EClass getCaseInsensitiveComparator();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction <em>Get Property Value Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Get Property Value Action</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction
	 * @generated
	 */
	EClass getGetPropertyValueAction();

	/**
	 * Returns the meta object for the containment reference '{@link de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction#getInstanceVariable <em>Instance Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Instance Variable</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction#getInstanceVariable()
	 * @see #getGetPropertyValueAction()
	 * @generated
	 */
	EReference getGetPropertyValueAction_InstanceVariable();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Property</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction#getProperty()
	 * @see #getGetPropertyValueAction()
	 * @generated
	 */
	EReference getGetPropertyValueAction_Property();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.storyDiagramEcore.callActions.Operators <em>Operators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Operators</em>'.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.Operators
	 * @generated
	 */
	EEnum getOperators();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CallActionsFactory getCallActionsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionImpl <em>Call Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getCallAction()
		 * @generated
		 */
		EClass CALL_ACTION = eINSTANCE.getCallAction();

		/**
		 * The meta object literal for the '<em><b>Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_ACTION__CLASSIFIER = eINSTANCE
				.getCallAction_Classifier();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionParameterImpl <em>Call Action Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionParameterImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getCallActionParameter()
		 * @generated
		 */
		EClass CALL_ACTION_PARAMETER = eINSTANCE.getCallActionParameter();

		/**
		 * The meta object literal for the '<em><b>Parameter Value Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION = eINSTANCE
				.getCallActionParameter_ParameterValueAction();

		/**
		 * The meta object literal for the '<em><b>Parameter Classfier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_ACTION_PARAMETER__PARAMETER_CLASSFIER = eINSTANCE
				.getCallActionParameter_ParameterClassfier();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CallStoryDiagramInterpreterActionImpl <em>Call Story Diagram Interpreter Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallStoryDiagramInterpreterActionImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getCallStoryDiagramInterpreterAction()
		 * @generated
		 */
		EClass CALL_STORY_DIAGRAM_INTERPRETER_ACTION = eINSTANCE
				.getCallStoryDiagramInterpreterAction();

		/**
		 * The meta object literal for the '<em><b>Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_STORY_DIAGRAM_INTERPRETER_ACTION__ACTIVITY = eINSTANCE
				.getCallStoryDiagramInterpreterAction_Activity();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_STORY_DIAGRAM_INTERPRETER_ACTION__PARAMETERS = eINSTANCE
				.getCallStoryDiagramInterpreterAction_Parameters();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.LiteralDeclarationActionImpl <em>Literal Declaration Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.LiteralDeclarationActionImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getLiteralDeclarationAction()
		 * @generated
		 */
		EClass LITERAL_DECLARATION_ACTION = eINSTANCE
				.getLiteralDeclarationAction();

		/**
		 * The meta object literal for the '<em><b>Literal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LITERAL_DECLARATION_ACTION__LITERAL = eINSTANCE
				.getLiteralDeclarationAction_Literal();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.MethodCallActionImpl <em>Method Call Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.MethodCallActionImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getMethodCallAction()
		 * @generated
		 */
		EClass METHOD_CALL_ACTION = eINSTANCE.getMethodCallAction();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METHOD_CALL_ACTION__PARAMETERS = eINSTANCE
				.getMethodCallAction_Parameters();

		/**
		 * The meta object literal for the '<em><b>Method</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METHOD_CALL_ACTION__METHOD = eINSTANCE
				.getMethodCallAction_Method();

		/**
		 * The meta object literal for the '<em><b>Method Class Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute METHOD_CALL_ACTION__METHOD_CLASS_NAME = eINSTANCE
				.getMethodCallAction_MethodClassName();

		/**
		 * The meta object literal for the '<em><b>Method Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute METHOD_CALL_ACTION__METHOD_NAME = eINSTANCE
				.getMethodCallAction_MethodName();

		/**
		 * The meta object literal for the '<em><b>Instance Variable</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METHOD_CALL_ACTION__INSTANCE_VARIABLE = eINSTANCE
				.getMethodCallAction_InstanceVariable();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.NewObjectActionImpl <em>New Object Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.NewObjectActionImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getNewObjectAction()
		 * @generated
		 */
		EClass NEW_OBJECT_ACTION = eINSTANCE.getNewObjectAction();

		/**
		 * The meta object literal for the '<em><b>Constructor Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEW_OBJECT_ACTION__CONSTRUCTOR_PARAMETERS = eINSTANCE
				.getNewObjectAction_ConstructorParameters();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.VariableDeclarationActionImpl <em>Variable Declaration Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.VariableDeclarationActionImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getVariableDeclarationAction()
		 * @generated
		 */
		EClass VARIABLE_DECLARATION_ACTION = eINSTANCE
				.getVariableDeclarationAction();

		/**
		 * The meta object literal for the '<em><b>Value Assignment</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_DECLARATION_ACTION__VALUE_ASSIGNMENT = eINSTANCE
				.getVariableDeclarationAction_ValueAssignment();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.VariableReferenceActionImpl <em>Variable Reference Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.VariableReferenceActionImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getVariableReferenceAction()
		 * @generated
		 */
		EClass VARIABLE_REFERENCE_ACTION = eINSTANCE
				.getVariableReferenceAction();

		/**
		 * The meta object literal for the '<em><b>Variable Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE_REFERENCE_ACTION__VARIABLE_NAME = eINSTANCE
				.getVariableReferenceAction_VariableName();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CompareActionImpl <em>Compare Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CompareActionImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getCompareAction()
		 * @generated
		 */
		EClass COMPARE_ACTION = eINSTANCE.getCompareAction();

		/**
		 * The meta object literal for the '<em><b>Expression1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPARE_ACTION__EXPRESSION1 = eINSTANCE
				.getCompareAction_Expression1();

		/**
		 * The meta object literal for the '<em><b>Expression2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPARE_ACTION__EXPRESSION2 = eINSTANCE
				.getCompareAction_Expression2();

		/**
		 * The meta object literal for the '<em><b>Comparator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPARE_ACTION__COMPARATOR = eINSTANCE
				.getCompareAction_Comparator();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.NullValueActionImpl <em>Null Value Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.NullValueActionImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getNullValueAction()
		 * @generated
		 */
		EClass NULL_VALUE_ACTION = eINSTANCE.getNullValueAction();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.OperationActionImpl <em>Operation Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.OperationActionImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getOperationAction()
		 * @generated
		 */
		EClass OPERATION_ACTION = eINSTANCE.getOperationAction();

		/**
		 * The meta object literal for the '<em><b>Operand1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_ACTION__OPERAND1 = eINSTANCE
				.getOperationAction_Operand1();

		/**
		 * The meta object literal for the '<em><b>Operand2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_ACTION__OPERAND2 = eINSTANCE
				.getOperationAction_Operand2();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_ACTION__OPERATOR = eINSTANCE
				.getOperationAction_Operator();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.AbstractComparatorImpl <em>Abstract Comparator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.AbstractComparatorImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getAbstractComparator()
		 * @generated
		 */
		EClass ABSTRACT_COMPARATOR = eINSTANCE.getAbstractComparator();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CaseInsensitiveComparatorImpl <em>Case Insensitive Comparator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CaseInsensitiveComparatorImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getCaseInsensitiveComparator()
		 * @generated
		 */
		EClass CASE_INSENSITIVE_COMPARATOR = eINSTANCE
				.getCaseInsensitiveComparator();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.impl.GetPropertyValueActionImpl <em>Get Property Value Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.GetPropertyValueActionImpl
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getGetPropertyValueAction()
		 * @generated
		 */
		EClass GET_PROPERTY_VALUE_ACTION = eINSTANCE
				.getGetPropertyValueAction();

		/**
		 * The meta object literal for the '<em><b>Instance Variable</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GET_PROPERTY_VALUE_ACTION__INSTANCE_VARIABLE = eINSTANCE
				.getGetPropertyValueAction_InstanceVariable();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GET_PROPERTY_VALUE_ACTION__PROPERTY = eINSTANCE
				.getGetPropertyValueAction_Property();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.storyDiagramEcore.callActions.Operators <em>Operators</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.storyDiagramEcore.callActions.Operators
		 * @see de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl#getOperators()
		 * @generated
		 */
		EEnum OPERATORS = eINSTANCE.getOperators();

	}

} //CallActionsPackage
