/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions;

import org.eclipse.emf.common.util.EList;

import de.hpi.sam.storyDiagramEcore.Activity;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Call Story Diagram Interpreter Action</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A CallStoryDiagramInterpreterAction can be used to call the StoryDiagramInterpreter to execute another activity. The classifier of the CallStoryDiagramInterpreterAction is the expected return type of the activity to be executed. Its return value is also the return value of this CallAction. A CallStoryDiagramInterpreterAction also contains a list of CallActions whose value will be supplied as parameters to the activity, as well as a CallAction that returns the thisObject in whose context the activity will be executed.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction#getActivity <em>Activity</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction#getParameters <em>Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCallStoryDiagramInterpreterAction()
 * @model
 * @generated
 */
public interface CallStoryDiagramInterpreterAction extends CallAction {
	/**
	 * Returns the value of the '<em><b>Activity</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * activity to be executed. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Activity</em>' reference.
	 * @see #setActivity(Activity)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCallStoryDiagramInterpreterAction_Activity()
	 * @model required="true"
	 * @generated
	 */
	Activity getActivity();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction#getActivity
	 * <em>Activity</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Activity</em>' reference.
	 * @see #getActivity()
	 * @generated
	 */
	void setActivity(Activity value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter}.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The parameters supplied to the activity to execute. Each parameter is a
	 * CallActionParameter that defines the name, classifier and value of the
	 * parameter. If there is a 'this' object in the story activity to be
	 * executed, it must be supplied as a special parameter. <!-- end-model-doc
	 * -->
	 * 
	 * @return the value of the '<em>Parameters</em>' containment reference
	 *         list.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getCallStoryDiagramInterpreterAction_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<CallActionParameter> getParameters();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tif (eIsProxy())\n\t\t\tsuper.toString();\n\n\t\tStringBuffer buffer = new StringBuffer();\n\n\t\tbuffer.append(\"call SDInterpreter(\");\n\n\t\tif (getActivity() != null)\n\t\t{\n\t\t\tif (getActivity().getSpecification() != null)\n\t\t\t{\n\t\t\t\tbuffer.append(getActivity().getSpecification().getName());\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\tif (getActivity().getName() != null && !\"\".equals(getActivity().getName()))\n\t\t\t\t{\n\t\t\t\t\tbuffer.append(getActivity().getName());\n\t\t\t\t}\n\t\t\t\telse\n\t\t\t\t{\n\t\t\t\t\tbuffer.append(\"[null]\");\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t\telse\n\t\t{\n\t\t\tbuffer.append(\"[null]\");\n\t\t}\n\n\t\tbuffer.append(\", \");\n\n\t\tfor (CallActionParameter parameter : getParameters())\n\t\t{\n\t\t\tbuffer.append(parameter.getName());\n\t\t\tbuffer.append(\":\");\n\t\t\tif (parameter.getParameterClassfier() != null)\n\t\t\t{\n\t\t\t\tbuffer.append(parameter.getParameterClassfier().getName());\n\t\t\t}\n\t\t\tbuffer.append(\" := \");\n\t\t\tbuffer.append(parameter.getParameterValueAction());\n\n\t\t\tbuffer.append(\", \");\n\t\t}\n\n\t\tbuffer.delete(buffer.length() - 2, buffer.length());\n\n\t\tbuffer.append(\")\");\n\n\t\treturn buffer.toString().trim();'"
	 * @generated
	 */
	String toString();

} // CallStoryDiagramInterpreterAction
