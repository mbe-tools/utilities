/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>EContainer Story Pattern Link</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.EContainerStoryPatternLinkImpl#isAllowIndirectContainment <em>Allow Indirect Containment</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EContainerStoryPatternLinkImpl extends
		AbstractStoryPatternLinkImpl implements EContainerStoryPatternLink {
	/**
	 * The default value of the '{@link #isAllowIndirectContainment() <em>Allow Indirect Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAllowIndirectContainment()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ALLOW_INDIRECT_CONTAINMENT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAllowIndirectContainment() <em>Allow Indirect Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAllowIndirectContainment()
	 * @generated
	 * @ordered
	 */
	protected boolean allowIndirectContainment = ALLOW_INDIRECT_CONTAINMENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected EContainerStoryPatternLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SdmPackage.Literals.ECONTAINER_STORY_PATTERN_LINK;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAllowIndirectContainment() {
		return allowIndirectContainment;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllowIndirectContainment(boolean newAllowIndirectContainment) {
		boolean oldAllowIndirectContainment = allowIndirectContainment;
		allowIndirectContainment = newAllowIndirectContainment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					SdmPackage.ECONTAINER_STORY_PATTERN_LINK__ALLOW_INDIRECT_CONTAINMENT,
					oldAllowIndirectContainment, allowIndirectContainment));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SdmPackage.ECONTAINER_STORY_PATTERN_LINK__ALLOW_INDIRECT_CONTAINMENT:
			return isAllowIndirectContainment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SdmPackage.ECONTAINER_STORY_PATTERN_LINK__ALLOW_INDIRECT_CONTAINMENT:
			setAllowIndirectContainment((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SdmPackage.ECONTAINER_STORY_PATTERN_LINK__ALLOW_INDIRECT_CONTAINMENT:
			setAllowIndirectContainment(ALLOW_INDIRECT_CONTAINMENT_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SdmPackage.ECONTAINER_STORY_PATTERN_LINK__ALLOW_INDIRECT_CONTAINMENT:
			return allowIndirectContainment != ALLOW_INDIRECT_CONTAINMENT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (allowIndirectContainment: ");
		result.append(allowIndirectContainment);
		result.append(')');
		return result.toString();
	}

} // EContainerStoryPatternLinkImpl
