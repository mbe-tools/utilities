/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.ExternalReference;
import de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides
 * an adapter <code>createXXX</code> method for each class of the model. <!--
 * end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage
 * @generated
 */
public class SdmAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static SdmPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public SdmAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = SdmPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc --> This implementation returns <code>true</code> if
	 * the object is either the model's package or is an instance object of the
	 * model. <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SdmSwitch<Adapter> modelSwitch = new SdmSwitch<Adapter>() {
		@Override
		public Adapter caseAttributeAssignment(AttributeAssignment object) {
			return createAttributeAssignmentAdapter();
		}

		@Override
		public Adapter caseStoryPatternElement(StoryPatternElement object) {
			return createStoryPatternElementAdapter();
		}

		@Override
		public Adapter caseAbstractStoryPatternLink(
				AbstractStoryPatternLink object) {
			return createAbstractStoryPatternLinkAdapter();
		}

		@Override
		public Adapter caseStoryPatternLink(StoryPatternLink object) {
			return createStoryPatternLinkAdapter();
		}

		@Override
		public Adapter caseStoryPatternContainmentLink(
				StoryPatternContainmentLink object) {
			return createStoryPatternContainmentLinkAdapter();
		}

		@Override
		public Adapter caseStoryPatternExpressionLink(
				StoryPatternExpressionLink object) {
			return createStoryPatternExpressionLinkAdapter();
		}

		@Override
		public Adapter caseStoryPatternObject(StoryPatternObject object) {
			return createStoryPatternObjectAdapter();
		}

		@Override
		public Adapter caseAbstractStoryPatternObject(
				AbstractStoryPatternObject object) {
			return createAbstractStoryPatternObjectAdapter();
		}

		@Override
		public Adapter caseMapEntryStoryPatternLink(
				MapEntryStoryPatternLink object) {
			return createMapEntryStoryPatternLinkAdapter();
		}

		@Override
		public Adapter caseEContainerStoryPatternLink(
				EContainerStoryPatternLink object) {
			return createEContainerStoryPatternLinkAdapter();
		}

		@Override
		public Adapter caseLinkPositionConstraint(LinkPositionConstraint object) {
			return createLinkPositionConstraintAdapter();
		}

		@Override
		public Adapter caseLinkOrderConstraint(LinkOrderConstraint object) {
			return createLinkOrderConstraintAdapter();
		}

		@Override
		public Adapter caseExternalReference(ExternalReference object) {
			return createExternalReferenceAdapter();
		}

		@Override
		public Adapter caseNamedElement(NamedElement object) {
			return createNamedElementAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment <em>Attribute Assignment</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment
	 * @generated
	 */
	public Adapter createAttributeAssignmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement <em>Story Pattern Element</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement
	 * @generated
	 */
	public Adapter createStoryPatternElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink <em>Abstract Story Pattern Link</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink
	 * @generated
	 */
	public Adapter createAbstractStoryPatternLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink <em>Story Pattern Link</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink
	 * @generated
	 */
	public Adapter createStoryPatternLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink <em>Story Pattern Containment Link</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink
	 * @generated
	 */
	public Adapter createStoryPatternContainmentLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink <em>Story Pattern Expression Link</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink
	 * @generated
	 */
	public Adapter createStoryPatternExpressionLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject <em>Story Pattern Object</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject
	 * @generated
	 */
	public Adapter createStoryPatternObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject <em>Abstract Story Pattern Object</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject
	 * @generated
	 */
	public Adapter createAbstractStoryPatternObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink <em>Map Entry Story Pattern Link</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink
	 * @generated
	 */
	public Adapter createMapEntryStoryPatternLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink <em>EContainer Story Pattern Link</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink
	 * @generated
	 */
	public Adapter createEContainerStoryPatternLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint <em>Link Position Constraint</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint
	 * @generated
	 */
	public Adapter createLinkPositionConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint <em>Link Order Constraint</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint
	 * @generated
	 */
	public Adapter createLinkOrderConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.sdm.ExternalReference <em>External Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.ExternalReference
	 * @generated
	 */
	public Adapter createExternalReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so
	 * that we can easily ignore cases; it's useful to ignore a case when
	 * inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} // SdmAdapterFactory
