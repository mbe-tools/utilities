/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Abstract Story Pattern Object</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * AbstractStoryPatternObject is the super class of all kinds of story pattern objects.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getOutgoingStoryLinks <em>Outgoing Story Links</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getIncomingStoryLinks <em>Incoming Story Links</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getCreateModelObjectExpression <em>Create Model Object Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAbstractStoryPatternObject()
 * @model abstract="true"
 * @generated
 */
public interface AbstractStoryPatternObject extends StoryPatternElement {
	/**
	 * Returns the value of the '<em><b>Classifier</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * classifier of this story pattern object. Usually an EClass object whose
	 * instances (EObjects) have to be matched in the object graph. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Classifier</em>' reference.
	 * @see #setClassifier(EClassifier)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAbstractStoryPatternObject_Classifier()
	 * @model required="true"
	 * @generated
	 */
	EClassifier getClassifier();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getClassifier <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Classifier</em>' reference.
	 * @see #getClassifier()
	 * @generated
	 */
	void setClassifier(EClassifier value);

	/**
	 * Returns the value of the '<em><b>Outgoing Story Links</b></em>' reference
	 * list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink}. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getSource
	 * <em>Source</em>}'. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> The list of StoryPatternLinks starting from this
	 * StoryPatternObject. This is the opposite of the the source association of
	 * the StoryPatternLink. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Outgoing Story Links</em>' reference list.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAbstractStoryPatternObject_OutgoingStoryLinks()
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<AbstractStoryPatternLink> getOutgoingStoryLinks();

	/**
	 * Returns the value of the '<em><b>Incoming Story Links</b></em>' reference
	 * list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink}. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getTarget
	 * <em>Target</em>}'. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> The list of StoryPatternLinks pointing to this
	 * StoryPatternObject. This is the opposite of the the target association of
	 * the StoryPatternLink. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Incoming Story Links</em>' reference list.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAbstractStoryPatternObject_IncomingStoryLinks()
	 * @see de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<AbstractStoryPatternLink> getIncomingStoryLinks();

	/**
	 * Returns the value of the '<em><b>Create Model Object Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Create Model Object Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Create Model Object Expression</em>' containment reference.
	 * @see #setCreateModelObjectExpression(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getAbstractStoryPatternObject_CreateModelObjectExpression()
	 * @model containment="true"
	 * @generated
	 */
	Expression getCreateModelObjectExpression();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject#getCreateModelObjectExpression <em>Create Model Object Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Create Model Object Expression</em>' containment reference.
	 * @see #getCreateModelObjectExpression()
	 * @generated
	 */
	void setCreateModelObjectExpression(Expression value);

} // AbstractStoryPatternObject
