/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Decision Node</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A decision node branches the control flow based on guards at the outgoing edges. A decision node must have one incoming edge and two or more outgoing edges with guards. One of the guards must be ELSE.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getDecisionNode()
 * @model
 * @generated
 */
public interface DecisionNode extends ActivityNode {
} // DecisionNode
