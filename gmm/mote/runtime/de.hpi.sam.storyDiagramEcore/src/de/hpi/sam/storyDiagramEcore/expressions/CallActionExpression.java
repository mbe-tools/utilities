/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.expressions;

import org.eclipse.emf.common.util.EList;

import de.hpi.sam.storyDiagramEcore.callActions.CallAction;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Call Action Expression</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression#getCallActions <em>Call Actions</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage#getCallActionExpression()
 * @model
 * @generated
 */
public interface CallActionExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Call Actions</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.callActions.CallAction}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Call Actions</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Call Actions</em>' containment reference
	 *         list.
	 * @see de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage#getCallActionExpression_CallActions()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<CallAction> getCallActions();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (eIsProxy()) return super.toString();\r\n\r\nif (getCallActions().isEmpty())\r\n{\r\n\treturn \"[null]\";\r\n}\r\nelse\r\n{\r\n\tStringBuffer buffer = new StringBuffer();\r\n\r\n\tString NL = System.getProperty(\"line.separator\");\r\n\r\n\tfor (CallAction c : getCallActions())\r\n\t{\r\n\t\tbuffer.append(c.toString());\r\n\t\tbuffer.append(NL);\r\n\t}\r\n\r\n\treturn buffer.toString().trim();\r\n}'"
	 * @generated
	 */
	String toString();

} // CallActionExpression
