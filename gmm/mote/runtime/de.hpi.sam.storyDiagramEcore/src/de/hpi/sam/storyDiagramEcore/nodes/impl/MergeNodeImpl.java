/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.impl;

import org.eclipse.emf.ecore.EClass;

import de.hpi.sam.storyDiagramEcore.nodes.MergeNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Merge Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class MergeNodeImpl extends ActivityNodeImpl implements MergeNode {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MergeNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NodesPackage.Literals.MERGE_NODE;
	}

} // MergeNodeImpl
