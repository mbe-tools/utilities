/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Map Entry Story Pattern Link</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.MapEntryStoryPatternLinkImpl#getValueTarget <em>Value Target</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.MapEntryStoryPatternLinkImpl#getEStructuralFeature <em>EStructural Feature</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.MapEntryStoryPatternLinkImpl#getClassifier <em>Classifier</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MapEntryStoryPatternLinkImpl extends AbstractStoryPatternLinkImpl
		implements MapEntryStoryPatternLink {
	/**
	 * The cached value of the '{@link #getValueTarget() <em>Value Target</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getValueTarget()
	 * @generated
	 * @ordered
	 */
	protected AbstractStoryPatternObject valueTarget;

	/**
	 * The cached value of the '{@link #getEStructuralFeature() <em>EStructural Feature</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getEStructuralFeature()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature eStructuralFeature;

	/**
	 * The cached value of the '{@link #getClassifier() <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClassifier()
	 * @generated
	 * @ordered
	 */
	protected EClassifier classifier;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MapEntryStoryPatternLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SdmPackage.Literals.MAP_ENTRY_STORY_PATTERN_LINK;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractStoryPatternObject getValueTarget() {
		if (valueTarget != null && valueTarget.eIsProxy()) {
			InternalEObject oldValueTarget = (InternalEObject) valueTarget;
			valueTarget = (AbstractStoryPatternObject) eResolveProxy(oldValueTarget);
			if (valueTarget != oldValueTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__VALUE_TARGET,
							oldValueTarget, valueTarget));
			}
		}
		return valueTarget;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractStoryPatternObject basicGetValueTarget() {
		return valueTarget;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueTarget(AbstractStoryPatternObject newValueTarget) {
		AbstractStoryPatternObject oldValueTarget = valueTarget;
		valueTarget = newValueTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__VALUE_TARGET,
					oldValueTarget, valueTarget));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature getEStructuralFeature() {
		if (eStructuralFeature != null && eStructuralFeature.eIsProxy()) {
			InternalEObject oldEStructuralFeature = (InternalEObject) eStructuralFeature;
			eStructuralFeature = (EStructuralFeature) eResolveProxy(oldEStructuralFeature);
			if (eStructuralFeature != oldEStructuralFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE,
							oldEStructuralFeature, eStructuralFeature));
			}
		}
		return eStructuralFeature;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature basicGetEStructuralFeature() {
		return eStructuralFeature;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setEStructuralFeature(EStructuralFeature newEStructuralFeature) {
		EStructuralFeature oldEStructuralFeature = eStructuralFeature;
		eStructuralFeature = newEStructuralFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE,
					oldEStructuralFeature, eStructuralFeature));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier getClassifier() {
		if (classifier != null && classifier.eIsProxy()) {
			InternalEObject oldClassifier = (InternalEObject) classifier;
			classifier = (EClassifier) eResolveProxy(oldClassifier);
			if (classifier != oldClassifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__CLASSIFIER,
							oldClassifier, classifier));
			}
		}
		return classifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier basicGetClassifier() {
		return classifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassifier(EClassifier newClassifier) {
		EClassifier oldClassifier = classifier;
		classifier = newClassifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__CLASSIFIER,
					oldClassifier, classifier));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__VALUE_TARGET:
			if (resolve)
				return getValueTarget();
			return basicGetValueTarget();
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE:
			if (resolve)
				return getEStructuralFeature();
			return basicGetEStructuralFeature();
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__CLASSIFIER:
			if (resolve)
				return getClassifier();
			return basicGetClassifier();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__VALUE_TARGET:
			setValueTarget((AbstractStoryPatternObject) newValue);
			return;
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE:
			setEStructuralFeature((EStructuralFeature) newValue);
			return;
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__CLASSIFIER:
			setClassifier((EClassifier) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__VALUE_TARGET:
			setValueTarget((AbstractStoryPatternObject) null);
			return;
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE:
			setEStructuralFeature((EStructuralFeature) null);
			return;
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__CLASSIFIER:
			setClassifier((EClassifier) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__VALUE_TARGET:
			return valueTarget != null;
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE:
			return eStructuralFeature != null;
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK__CLASSIFIER:
			return classifier != null;
		}
		return super.eIsSet(featureID);
	}

	@Override
	public String toString() {
		final StringBuilder strBuild = new StringBuilder();
		strBuild.append(getEStructuralFeature().getName() + " ");
		strBuild.append(super.toString());

		return strBuild.toString();
	}

} // MapEntryStoryPatternLinkImpl
