/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Release Semaphore Edge</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Connects an activity edge to a semaphore. Indicates that tokens are released
 * when the activity edge is executed.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge#getActivityEdge <em>Activity Edge</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getReleaseSemaphoreEdge()
 * @model
 * @generated
 */
public interface ReleaseSemaphoreEdge extends SynchronizationEdge {
	/**
	 * Returns the value of the '<em><b>Activity Edge</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getReleaseEdges <em>Release Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity Edge</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity Edge</em>' reference.
	 * @see #setActivityEdge(ActivityEdge)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getReleaseSemaphoreEdge_ActivityEdge()
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getReleaseEdges
	 * @model opposite="releaseEdges" required="true"
	 * @generated
	 */
	ActivityEdge getActivityEdge();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge#getActivityEdge <em>Activity Edge</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Activity Edge</em>' reference.
	 * @see #getActivityEdge()
	 * @generated
	 */
	void setActivityEdge(ActivityEdge value);

} // ReleaseSemaphoreEdge
