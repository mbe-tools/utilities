/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>New Object Action</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The NewObjectAction creates a new instance object. If the classifier of this CallAction is an EClass, the appropriate EMF factory will be used to create the object. Parameters are ignored in this case. If the classifier is something else, Java's reflection mechanism will be used to find an appropriate constructor method using the supplied constructorParameters.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction#getConstructorParameters <em>Constructor Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getNewObjectAction()
 * @model
 * @generated
 */
public interface NewObjectAction extends CallAction {
	/**
	 * Returns the value of the '<em><b>Constructor Parameters</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter}.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> A
	 * list of CallActionParameters that are used to choose the right
	 * constructor. The parameter values are supplied to the constructor when
	 * the new object is created. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Constructor Parameters</em>' containment
	 *         reference list.
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getNewObjectAction_ConstructorParameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<CallActionParameter> getConstructorParameters();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (eIsProxy()) return super.toString();\r\n\r\nStringBuffer buffer = new StringBuffer();\r\nbuffer.append(\"new \");\r\n\r\nif (getClassifier() != null && getClassifier().getName() != null && !\"\".equals(getClassifier().getName()))\r\n{\r\n\tbuffer.append(getClassifier().getName());\r\n}\r\nelse\r\n{\r\n\tbuffer.append(\"[null]\");\r\n}\r\n\r\nbuffer.append(\"(\");\r\n\r\nfor (CallActionParameter param : getConstructorParameters())\r\n{\r\n\tbuffer.append(param.toString());\r\n}\r\n\r\nbuffer.append(\")\");\r\n\r\nreturn buffer.toString().trim();'"
	 * @generated
	 */
	String toString();

} // NewObjectAction
