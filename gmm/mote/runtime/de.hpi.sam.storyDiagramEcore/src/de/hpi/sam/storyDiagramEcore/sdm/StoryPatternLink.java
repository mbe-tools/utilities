/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Story Pattern Link</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Story pattern links represent references between instance objects.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getEStructuralFeature <em>EStructural Feature</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getLinkPositionConstraint <em>Link Position Constraint</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getOutgoingLinkOrderConstraints <em>Outgoing Link Order Constraints</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getIncomingLinkOrderConstraints <em>Incoming Link Order Constraints</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getEOppositeReference <em>EOpposite Reference</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getExternalReference <em>External Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternLink()
 * @model
 * @generated
 */
public interface StoryPatternLink extends AbstractStoryPatternLink {
	/**
	 * Returns the value of the '<em><b>EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The EReference this story pattern link is bound to. <!-- end-model-doc
	 * -->
	 * 
	 * @return the value of the '<em>EStructural Feature</em>' reference.
	 * @see #setEStructuralFeature(EStructuralFeature)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternLink_EStructuralFeature()
	 * @model required="true"
	 * @generated
	 */
	EStructuralFeature getEStructuralFeature();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getEStructuralFeature <em>EStructural Feature</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>EStructural Feature</em>' reference.
	 * @see #getEStructuralFeature()
	 * @generated
	 */
	void setEStructuralFeature(EStructuralFeature value);

	/**
	 * Returns the value of the '<em><b>Link Position Constraint</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint#getStoryPatternLink <em>Story Pattern Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link Position Constraint</em>' containment
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link Position Constraint</em>' containment reference.
	 * @see #setLinkPositionConstraint(LinkPositionConstraint)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternLink_LinkPositionConstraint()
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint#getStoryPatternLink
	 * @model opposite="storyPatternLink" containment="true"
	 * @generated
	 */
	LinkPositionConstraint getLinkPositionConstraint();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getLinkPositionConstraint
	 * <em>Link Position Constraint</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Link Position Constraint</em>'
	 *            containment reference.
	 * @see #getLinkPositionConstraint()
	 * @generated
	 */
	void setLinkPositionConstraint(LinkPositionConstraint value);

	/**
	 * Returns the value of the '<em><b>Outgoing Link Order Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint}.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getPredecessorLink <em>Predecessor Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Link Order Constraints</em>'
	 * containment reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Link Order Constraints</em>' containment reference list.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternLink_OutgoingLinkOrderConstraints()
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getPredecessorLink
	 * @model opposite="predecessorLink" containment="true" ordered="false"
	 * @generated
	 */
	EList<LinkOrderConstraint> getOutgoingLinkOrderConstraints();

	/**
	 * Returns the value of the '<em><b>Incoming Link Order Constraints</b></em>' reference list.
	 * The list contents are of type {@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint}.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getSuccessorLink <em>Successor Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Link Order Constraints</em>'
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Link Order Constraints</em>' reference list.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternLink_IncomingLinkOrderConstraints()
	 * @see de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint#getSuccessorLink
	 * @model opposite="successorLink" ordered="false"
	 * @generated
	 */
	EList<LinkOrderConstraint> getIncomingLinkOrderConstraints();

	/**
	 * Returns the value of the '<em><b>EOpposite Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EOpposite Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EOpposite Reference</em>' reference.
	 * @see #setEOppositeReference(EReference)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternLink_EOppositeReference()
	 * @model
	 * @generated
	 */
	EReference getEOppositeReference();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getEOppositeReference <em>EOpposite Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EOpposite Reference</em>' reference.
	 * @see #getEOppositeReference()
	 * @generated
	 */
	void setEOppositeReference(EReference value);

	/**
	 * Returns the value of the '<em><b>External Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Reference</em>' containment reference.
	 * @see #setExternalReference(ExternalReference)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternLink_ExternalReference()
	 * @model containment="true"
	 * @generated
	 */
	ExternalReference getExternalReference();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink#getExternalReference <em>External Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Reference</em>' containment reference.
	 * @see #getExternalReference()
	 * @generated
	 */
	void setExternalReference(ExternalReference value);

} // StoryPatternLink
