package de.hpi.sam.storyDiagramEcore;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;

public class StoryDiagramEcoreUtils
{
	private static List<String>	expressionLanguages	= null;

	public static List<String> getAvailableExpressionLanguages()
	{
		if (expressionLanguages == null)
		{
			expressionLanguages = new ArrayList<String>();

			if (Platform.getExtensionRegistry() != null)
			{
				IConfigurationElement[] configurationElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
						StoryDiagramEcoreConstants.EXPRESSION_LANGUAGES_EXTENSION_POINT_ID);

				for (IConfigurationElement configurationElement : configurationElements)
				{
					String s = configurationElement
							.getAttribute(StoryDiagramEcoreConstants.EXPRESSION_LANGUAGES_EXPRESSION_LANGUAGE_ATTRIBUTE_NAME);

					if (s != null && !("".equals(s)))
					{
						expressionLanguages.add(s);
					}
				}
			}
		}

		return expressionLanguages;
	}

	public static EStructuralFeature getConnectingEStructuralFeature(final EClass srcClass, final EClassifier dstClassifier)
	{
		EList<EClass> typeList;
		if (dstClassifier instanceof EClass)
		{
			typeList = ((EClass) dstClassifier).getEAllSuperTypes();
		}
		else
		{
			typeList = ECollections.emptyEList();
		}

		EList<EStructuralFeature> featureList = srcClass.getEAllStructuralFeatures();
		EClassifier classifier;
		EStructuralFeature resultFeature = null;
		for (EStructuralFeature feature : featureList)
		{
			classifier = feature.getEType();
			if (classifier.equals(dstClassifier) || typeList.contains(classifier))
			{
				if (resultFeature == null)
				{
					resultFeature = feature;
				}
				else
				{
					return null;
				}
			}
		}

		return resultFeature;
	}

	public static StoryPatternLink getStoryPatternLinkInCase(AbstractStoryPatternObject source, AbstractStoryPatternObject target,
			EReference reference)
	{
		StoryActionNode san = (StoryActionNode) source.eContainer();

		for (AbstractStoryPatternLink link : san.getStoryPatternLinks())
		{
			if (link instanceof StoryPatternLink && link.getSource().equals(source) && link.getTarget().equals(target)
					&& ((StoryPatternLink) link).getEStructuralFeature().equals(reference))
			{
				return (StoryPatternLink) link;
			}

		}

		return null;
	}

	public static EReference getMapTypedEReference(final EClass srcClass, final EClassifier dstClassifier)
	{
		EList<EClass> typeList;
		if (dstClassifier instanceof EClass)
		{
			typeList = ((EClass) dstClassifier).getEAllSuperTypes();
		}
		else
		{
			typeList = ECollections.emptyEList();
		}

		EList<EReference> refList = srcClass.getEAllReferences();
		EReference resultRef = null;
		EClassifier classifier;

		for (EReference eReference : refList)
		{
			if (eReference.getEGenericType().getETypeArguments() != null && eReference.getEGenericType().getETypeArguments().size() > 0
					&& eReference.getEGenericType().getETypeArguments().get(0).getEClassifier() != null)
			{
				classifier = eReference.getEGenericType().getETypeArguments().get(0).getEClassifier();
			}
			else
			{
				continue;
			}

			if (eReference.getUpperBound() == EReference.UNBOUNDED_MULTIPLICITY && eReference.isContainment()
					&& eReference.getEType().getInstanceTypeName().equals("java.util.Map$Entry")
					&& (dstClassifier.equals(classifier) || typeList.contains(classifier)))
			{

				if (resultRef == null)
				{
					resultRef = eReference;
				}
				else
				{
					return null;
				}
			}
		}

		return resultRef;
	}
}
