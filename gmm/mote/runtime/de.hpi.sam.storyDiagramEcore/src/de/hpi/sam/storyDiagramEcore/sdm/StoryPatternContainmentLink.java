/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Story Pattern Containment Link</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The StoryPatternContainmentLink is a special StoryPatternLink that can be used to express, that an element is directly or indirectly contained in another container element. Ordinary StoryPatternLink can only express direct containment.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternContainmentLink()
 * @model
 * @generated
 */
public interface StoryPatternContainmentLink extends AbstractStoryPatternLink {
} // StoryPatternContainmentLink
