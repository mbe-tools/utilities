/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionsPackageImpl;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage;
import de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsPackageImpl;
import de.hpi.sam.storyDiagramEcore.helpers.HelpersPackage;
import de.hpi.sam.storyDiagramEcore.helpers.impl.HelpersPackageImpl;
import de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.ExternalReference;
import de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.SdmFactory;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class SdmPackageImpl extends EPackageImpl implements SdmPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeAssignmentEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass storyPatternElementEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractStoryPatternLinkEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass storyPatternLinkEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass storyPatternContainmentLinkEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass storyPatternExpressionLinkEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass storyPatternObjectEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractStoryPatternObjectEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mapEntryStoryPatternLinkEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eContainerStoryPatternLinkEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass linkPositionConstraintEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass linkOrderConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externalReferenceEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum bindingTypeEnumerationEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum storyPatternMatchTypeEnumerationEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum storyPatternModifierEnumerationEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum storyPatternNACSematicEnumerationEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum linkPositionConstraintEnumerationEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum linkOrderConstraintEnumerationEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SdmPackageImpl() {
		super(eNS_URI, SdmFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link SdmPackage#eINSTANCE} when that
	 * field is accessed. Clients should not invoke it directly. Instead, they
	 * should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SdmPackage init() {
		if (isInited)
			return (SdmPackage) EPackage.Registry.INSTANCE
					.getEPackage(SdmPackage.eNS_URI);

		// Obtain or create and register package
		SdmPackageImpl theSdmPackage = (SdmPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof SdmPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new SdmPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		StoryDiagramEcorePackageImpl theStoryDiagramEcorePackage = (StoryDiagramEcorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI) instanceof StoryDiagramEcorePackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI)
				: StoryDiagramEcorePackage.eINSTANCE);
		CallActionsPackageImpl theCallActionsPackage = (CallActionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(CallActionsPackage.eNS_URI) instanceof CallActionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(CallActionsPackage.eNS_URI)
				: CallActionsPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI)
				: ExpressionsPackage.eINSTANCE);
		NodesPackageImpl theNodesPackage = (NodesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(NodesPackage.eNS_URI) instanceof NodesPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(NodesPackage.eNS_URI) : NodesPackage.eINSTANCE);
		HelpersPackageImpl theHelpersPackage = (HelpersPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(HelpersPackage.eNS_URI) instanceof HelpersPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(HelpersPackage.eNS_URI) : HelpersPackage.eINSTANCE);

		// Create package meta-data objects
		theSdmPackage.createPackageContents();
		theStoryDiagramEcorePackage.createPackageContents();
		theCallActionsPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theNodesPackage.createPackageContents();
		theHelpersPackage.createPackageContents();

		// Initialize created meta-data
		theSdmPackage.initializePackageContents();
		theStoryDiagramEcorePackage.initializePackageContents();
		theCallActionsPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theNodesPackage.initializePackageContents();
		theHelpersPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSdmPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SdmPackage.eNS_URI, theSdmPackage);
		return theSdmPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttributeAssignment() {
		return attributeAssignmentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeAssignment_EStructuralFeature() {
		return (EReference) attributeAssignmentEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeAssignment_AssignmentExpression() {
		return (EReference) attributeAssignmentEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeAssignment_CustomComparator() {
		return (EReference) attributeAssignmentEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStoryPatternElement() {
		return storyPatternElementEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStoryPatternElement_Modifier() {
		return (EAttribute) storyPatternElementEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStoryPatternElement_MatchType() {
		return (EAttribute) storyPatternElementEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractStoryPatternLink() {
		return abstractStoryPatternLinkEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractStoryPatternLink_Source() {
		return (EReference) abstractStoryPatternLinkEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractStoryPatternLink_Target() {
		return (EReference) abstractStoryPatternLinkEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractStoryPatternLink_CheckOnlyExistence() {
		return (EAttribute) abstractStoryPatternLinkEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractStoryPatternLink_MatchingPriority() {
		return (EAttribute) abstractStoryPatternLinkEClass
				.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractStoryPatternLink_OppositeStoryPatternLink() {
		return (EReference) abstractStoryPatternLinkEClass
				.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStoryPatternLink() {
		return storyPatternLinkEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryPatternLink_EStructuralFeature() {
		return (EReference) storyPatternLinkEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryPatternLink_LinkPositionConstraint() {
		return (EReference) storyPatternLinkEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryPatternLink_OutgoingLinkOrderConstraints() {
		return (EReference) storyPatternLinkEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryPatternLink_IncomingLinkOrderConstraints() {
		return (EReference) storyPatternLinkEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryPatternLink_EOppositeReference() {
		return (EReference) storyPatternLinkEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryPatternLink_ExternalReference() {
		return (EReference) storyPatternLinkEClass.getEStructuralFeatures()
				.get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStoryPatternContainmentLink() {
		return storyPatternContainmentLinkEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStoryPatternExpressionLink() {
		return storyPatternExpressionLinkEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryPatternExpressionLink_Expression() {
		return (EReference) storyPatternExpressionLinkEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStoryPatternObject() {
		return storyPatternObjectEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStoryPatternObject_BindingType() {
		return (EAttribute) storyPatternObjectEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryPatternObject_AttributeAssignments() {
		return (EReference) storyPatternObjectEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryPatternObject_Constraints() {
		return (EReference) storyPatternObjectEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStoryPatternObject_DirectAssignmentExpression() {
		return (EReference) storyPatternObjectEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractStoryPatternObject() {
		return abstractStoryPatternObjectEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractStoryPatternObject_Classifier() {
		return (EReference) abstractStoryPatternObjectEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractStoryPatternObject_OutgoingStoryLinks() {
		return (EReference) abstractStoryPatternObjectEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractStoryPatternObject_IncomingStoryLinks() {
		return (EReference) abstractStoryPatternObjectEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractStoryPatternObject_CreateModelObjectExpression() {
		return (EReference) abstractStoryPatternObjectEClass
				.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMapEntryStoryPatternLink() {
		return mapEntryStoryPatternLinkEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMapEntryStoryPatternLink_ValueTarget() {
		return (EReference) mapEntryStoryPatternLinkEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMapEntryStoryPatternLink_EStructuralFeature() {
		return (EReference) mapEntryStoryPatternLinkEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMapEntryStoryPatternLink_Classifier() {
		return (EReference) mapEntryStoryPatternLinkEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEContainerStoryPatternLink() {
		return eContainerStoryPatternLinkEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEContainerStoryPatternLink_AllowIndirectContainment() {
		return (EAttribute) eContainerStoryPatternLinkEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLinkPositionConstraint() {
		return linkPositionConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLinkPositionConstraint_ConstraintType() {
		return (EAttribute) linkPositionConstraintEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLinkPositionConstraint_StoryPatternLink() {
		return (EReference) linkPositionConstraintEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLinkOrderConstraint() {
		return linkOrderConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLinkOrderConstraint_ConstraintType() {
		return (EAttribute) linkOrderConstraintEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLinkOrderConstraint_PredecessorLink() {
		return (EReference) linkOrderConstraintEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLinkOrderConstraint_SuccessorLink() {
		return (EReference) linkOrderConstraintEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExternalReference() {
		return externalReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExternalReference_Expression() {
		return (EReference) externalReferenceEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExternalReference_Many() {
		return (EAttribute) externalReferenceEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBindingTypeEnumeration() {
		return bindingTypeEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStoryPatternMatchTypeEnumeration() {
		return storyPatternMatchTypeEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStoryPatternModifierEnumeration() {
		return storyPatternModifierEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStoryPatternNACSematicEnumeration() {
		return storyPatternNACSematicEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLinkPositionConstraintEnumeration() {
		return linkPositionConstraintEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLinkOrderConstraintEnumeration() {
		return linkOrderConstraintEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SdmFactory getSdmFactory() {
		return (SdmFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		attributeAssignmentEClass = createEClass(ATTRIBUTE_ASSIGNMENT);
		createEReference(attributeAssignmentEClass,
				ATTRIBUTE_ASSIGNMENT__ESTRUCTURAL_FEATURE);
		createEReference(attributeAssignmentEClass,
				ATTRIBUTE_ASSIGNMENT__ASSIGNMENT_EXPRESSION);
		createEReference(attributeAssignmentEClass,
				ATTRIBUTE_ASSIGNMENT__CUSTOM_COMPARATOR);

		storyPatternElementEClass = createEClass(STORY_PATTERN_ELEMENT);
		createEAttribute(storyPatternElementEClass,
				STORY_PATTERN_ELEMENT__MODIFIER);
		createEAttribute(storyPatternElementEClass,
				STORY_PATTERN_ELEMENT__MATCH_TYPE);

		abstractStoryPatternLinkEClass = createEClass(ABSTRACT_STORY_PATTERN_LINK);
		createEReference(abstractStoryPatternLinkEClass,
				ABSTRACT_STORY_PATTERN_LINK__SOURCE);
		createEReference(abstractStoryPatternLinkEClass,
				ABSTRACT_STORY_PATTERN_LINK__TARGET);
		createEAttribute(abstractStoryPatternLinkEClass,
				ABSTRACT_STORY_PATTERN_LINK__CHECK_ONLY_EXISTENCE);
		createEAttribute(abstractStoryPatternLinkEClass,
				ABSTRACT_STORY_PATTERN_LINK__MATCHING_PRIORITY);
		createEReference(abstractStoryPatternLinkEClass,
				ABSTRACT_STORY_PATTERN_LINK__OPPOSITE_STORY_PATTERN_LINK);

		storyPatternLinkEClass = createEClass(STORY_PATTERN_LINK);
		createEReference(storyPatternLinkEClass,
				STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE);
		createEReference(storyPatternLinkEClass,
				STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT);
		createEReference(storyPatternLinkEClass,
				STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS);
		createEReference(storyPatternLinkEClass,
				STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS);
		createEReference(storyPatternLinkEClass,
				STORY_PATTERN_LINK__EOPPOSITE_REFERENCE);
		createEReference(storyPatternLinkEClass,
				STORY_PATTERN_LINK__EXTERNAL_REFERENCE);

		storyPatternContainmentLinkEClass = createEClass(STORY_PATTERN_CONTAINMENT_LINK);

		storyPatternExpressionLinkEClass = createEClass(STORY_PATTERN_EXPRESSION_LINK);
		createEReference(storyPatternExpressionLinkEClass,
				STORY_PATTERN_EXPRESSION_LINK__EXPRESSION);

		storyPatternObjectEClass = createEClass(STORY_PATTERN_OBJECT);
		createEAttribute(storyPatternObjectEClass,
				STORY_PATTERN_OBJECT__BINDING_TYPE);
		createEReference(storyPatternObjectEClass,
				STORY_PATTERN_OBJECT__ATTRIBUTE_ASSIGNMENTS);
		createEReference(storyPatternObjectEClass,
				STORY_PATTERN_OBJECT__CONSTRAINTS);
		createEReference(storyPatternObjectEClass,
				STORY_PATTERN_OBJECT__DIRECT_ASSIGNMENT_EXPRESSION);

		abstractStoryPatternObjectEClass = createEClass(ABSTRACT_STORY_PATTERN_OBJECT);
		createEReference(abstractStoryPatternObjectEClass,
				ABSTRACT_STORY_PATTERN_OBJECT__CLASSIFIER);
		createEReference(abstractStoryPatternObjectEClass,
				ABSTRACT_STORY_PATTERN_OBJECT__OUTGOING_STORY_LINKS);
		createEReference(abstractStoryPatternObjectEClass,
				ABSTRACT_STORY_PATTERN_OBJECT__INCOMING_STORY_LINKS);
		createEReference(abstractStoryPatternObjectEClass,
				ABSTRACT_STORY_PATTERN_OBJECT__CREATE_MODEL_OBJECT_EXPRESSION);

		mapEntryStoryPatternLinkEClass = createEClass(MAP_ENTRY_STORY_PATTERN_LINK);
		createEReference(mapEntryStoryPatternLinkEClass,
				MAP_ENTRY_STORY_PATTERN_LINK__VALUE_TARGET);
		createEReference(mapEntryStoryPatternLinkEClass,
				MAP_ENTRY_STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE);
		createEReference(mapEntryStoryPatternLinkEClass,
				MAP_ENTRY_STORY_PATTERN_LINK__CLASSIFIER);

		eContainerStoryPatternLinkEClass = createEClass(ECONTAINER_STORY_PATTERN_LINK);
		createEAttribute(eContainerStoryPatternLinkEClass,
				ECONTAINER_STORY_PATTERN_LINK__ALLOW_INDIRECT_CONTAINMENT);

		linkPositionConstraintEClass = createEClass(LINK_POSITION_CONSTRAINT);
		createEAttribute(linkPositionConstraintEClass,
				LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE);
		createEReference(linkPositionConstraintEClass,
				LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK);

		linkOrderConstraintEClass = createEClass(LINK_ORDER_CONSTRAINT);
		createEAttribute(linkOrderConstraintEClass,
				LINK_ORDER_CONSTRAINT__CONSTRAINT_TYPE);
		createEReference(linkOrderConstraintEClass,
				LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK);
		createEReference(linkOrderConstraintEClass,
				LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK);

		externalReferenceEClass = createEClass(EXTERNAL_REFERENCE);
		createEReference(externalReferenceEClass,
				EXTERNAL_REFERENCE__EXPRESSION);
		createEAttribute(externalReferenceEClass, EXTERNAL_REFERENCE__MANY);

		// Create enums
		bindingTypeEnumerationEEnum = createEEnum(BINDING_TYPE_ENUMERATION);
		storyPatternMatchTypeEnumerationEEnum = createEEnum(STORY_PATTERN_MATCH_TYPE_ENUMERATION);
		storyPatternModifierEnumerationEEnum = createEEnum(STORY_PATTERN_MODIFIER_ENUMERATION);
		storyPatternNACSematicEnumerationEEnum = createEEnum(STORY_PATTERN_NAC_SEMATIC_ENUMERATION);
		linkPositionConstraintEnumerationEEnum = createEEnum(LINK_POSITION_CONSTRAINT_ENUMERATION);
		linkOrderConstraintEnumerationEEnum = createEEnum(LINK_ORDER_CONSTRAINT_ENUMERATION);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StoryDiagramEcorePackage theStoryDiagramEcorePackage = (StoryDiagramEcorePackage) EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI);
		ExpressionsPackage theExpressionsPackage = (ExpressionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI);
		CallActionsPackage theCallActionsPackage = (CallActionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(CallActionsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		attributeAssignmentEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		storyPatternElementEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		abstractStoryPatternLinkEClass.getESuperTypes().add(
				this.getStoryPatternElement());
		storyPatternLinkEClass.getESuperTypes().add(
				this.getAbstractStoryPatternLink());
		storyPatternContainmentLinkEClass.getESuperTypes().add(
				this.getAbstractStoryPatternLink());
		storyPatternExpressionLinkEClass.getESuperTypes().add(
				this.getAbstractStoryPatternLink());
		storyPatternObjectEClass.getESuperTypes().add(
				this.getAbstractStoryPatternObject());
		abstractStoryPatternObjectEClass.getESuperTypes().add(
				this.getStoryPatternElement());
		mapEntryStoryPatternLinkEClass.getESuperTypes().add(
				this.getAbstractStoryPatternLink());
		eContainerStoryPatternLinkEClass.getESuperTypes().add(
				this.getAbstractStoryPatternLink());
		externalReferenceEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());

		// Initialize classes and features; add operations and parameters
		initEClass(attributeAssignmentEClass, AttributeAssignment.class,
				"AttributeAssignment", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttributeAssignment_EStructuralFeature(),
				ecorePackage.getEStructuralFeature(), null,
				"eStructuralFeature", null, 1, 1, AttributeAssignment.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getAttributeAssignment_AssignmentExpression(),
				theExpressionsPackage.getExpression(), null,
				"assignmentExpression", null, 1, 1, AttributeAssignment.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getAttributeAssignment_CustomComparator(),
				theCallActionsPackage.getAbstractComparator(), null,
				"customComparator", null, 0, 1, AttributeAssignment.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(storyPatternElementEClass, StoryPatternElement.class,
				"StoryPatternElement", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStoryPatternElement_Modifier(),
				this.getStoryPatternModifierEnumeration(), "modifier", null, 1,
				1, StoryPatternElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getStoryPatternElement_MatchType(),
				this.getStoryPatternMatchTypeEnumeration(), "matchType", null,
				1, 1, StoryPatternElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(abstractStoryPatternLinkEClass,
				AbstractStoryPatternLink.class, "AbstractStoryPatternLink",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractStoryPatternLink_Source(),
				this.getAbstractStoryPatternObject(),
				this.getAbstractStoryPatternObject_OutgoingStoryLinks(),
				"source", null, 1, 1, AbstractStoryPatternLink.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getAbstractStoryPatternLink_Target(),
				this.getAbstractStoryPatternObject(),
				this.getAbstractStoryPatternObject_IncomingStoryLinks(),
				"target", null, 1, 1, AbstractStoryPatternLink.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getAbstractStoryPatternLink_CheckOnlyExistence(),
				ecorePackage.getEBoolean(), "checkOnlyExistence", null, 0, 1,
				AbstractStoryPatternLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getAbstractStoryPatternLink_MatchingPriority(),
				ecorePackage.getEInt(), "matchingPriority", null, 0, 1,
				AbstractStoryPatternLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getAbstractStoryPatternLink_OppositeStoryPatternLink(),
				this.getAbstractStoryPatternLink(),
				this.getAbstractStoryPatternLink_OppositeStoryPatternLink(),
				"oppositeStoryPatternLink", null, 0, 1,
				AbstractStoryPatternLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(storyPatternLinkEClass, StoryPatternLink.class,
				"StoryPatternLink", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStoryPatternLink_EStructuralFeature(),
				ecorePackage.getEStructuralFeature(), null,
				"eStructuralFeature", null, 1, 1, StoryPatternLink.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getStoryPatternLink_LinkPositionConstraint(),
				this.getLinkPositionConstraint(),
				this.getLinkPositionConstraint_StoryPatternLink(),
				"linkPositionConstraint", null, 0, 1, StoryPatternLink.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getStoryPatternLink_OutgoingLinkOrderConstraints(),
				this.getLinkOrderConstraint(),
				this.getLinkOrderConstraint_PredecessorLink(),
				"outgoingLinkOrderConstraints", null, 0, -1,
				StoryPatternLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getStoryPatternLink_IncomingLinkOrderConstraints(),
				this.getLinkOrderConstraint(),
				this.getLinkOrderConstraint_SuccessorLink(),
				"incomingLinkOrderConstraints", null, 0, -1,
				StoryPatternLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getStoryPatternLink_EOppositeReference(),
				ecorePackage.getEReference(), null, "eOppositeReference", null,
				0, 1, StoryPatternLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStoryPatternLink_ExternalReference(),
				this.getExternalReference(), null, "externalReference", null,
				0, 1, StoryPatternLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(storyPatternContainmentLinkEClass,
				StoryPatternContainmentLink.class,
				"StoryPatternContainmentLink", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(storyPatternExpressionLinkEClass,
				StoryPatternExpressionLink.class, "StoryPatternExpressionLink",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStoryPatternExpressionLink_Expression(),
				theExpressionsPackage.getExpression(), null, "expression",
				null, 1, 1, StoryPatternExpressionLink.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(storyPatternObjectEClass, StoryPatternObject.class,
				"StoryPatternObject", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStoryPatternObject_BindingType(),
				this.getBindingTypeEnumeration(), "bindingType", null, 1, 1,
				StoryPatternObject.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getStoryPatternObject_AttributeAssignments(),
				this.getAttributeAssignment(), null, "attributeAssignments",
				null, 0, -1, StoryPatternObject.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStoryPatternObject_Constraints(),
				theExpressionsPackage.getExpression(), null, "constraints",
				null, 0, -1, StoryPatternObject.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStoryPatternObject_DirectAssignmentExpression(),
				theExpressionsPackage.getExpression(), null,
				"directAssignmentExpression", null, 0, 1,
				StoryPatternObject.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractStoryPatternObjectEClass,
				AbstractStoryPatternObject.class, "AbstractStoryPatternObject",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractStoryPatternObject_Classifier(),
				ecorePackage.getEClassifier(), null, "classifier", null, 1, 1,
				AbstractStoryPatternObject.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractStoryPatternObject_OutgoingStoryLinks(),
				this.getAbstractStoryPatternLink(),
				this.getAbstractStoryPatternLink_Source(),
				"outgoingStoryLinks", null, 0, -1,
				AbstractStoryPatternObject.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractStoryPatternObject_IncomingStoryLinks(),
				this.getAbstractStoryPatternLink(),
				this.getAbstractStoryPatternLink_Target(),
				"incomingStoryLinks", null, 0, -1,
				AbstractStoryPatternObject.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(
				getAbstractStoryPatternObject_CreateModelObjectExpression(),
				theExpressionsPackage.getExpression(), null,
				"createModelObjectExpression", null, 0, 1,
				AbstractStoryPatternObject.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mapEntryStoryPatternLinkEClass,
				MapEntryStoryPatternLink.class, "MapEntryStoryPatternLink",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMapEntryStoryPatternLink_ValueTarget(),
				this.getAbstractStoryPatternObject(), null, "valueTarget",
				null, 0, 1, MapEntryStoryPatternLink.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMapEntryStoryPatternLink_EStructuralFeature(),
				ecorePackage.getEStructuralFeature(), null,
				"eStructuralFeature", null, 1, 1,
				MapEntryStoryPatternLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMapEntryStoryPatternLink_Classifier(),
				ecorePackage.getEClassifier(), null, "classifier", null, 1, 1,
				MapEntryStoryPatternLink.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eContainerStoryPatternLinkEClass,
				EContainerStoryPatternLink.class, "EContainerStoryPatternLink",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(
				getEContainerStoryPatternLink_AllowIndirectContainment(),
				ecorePackage.getEBoolean(), "allowIndirectContainment", null,
				0, 1, EContainerStoryPatternLink.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(linkPositionConstraintEClass, LinkPositionConstraint.class,
				"LinkPositionConstraint", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLinkPositionConstraint_ConstraintType(),
				this.getLinkPositionConstraintEnumeration(), "constraintType",
				"FIRST", 1, 1, LinkPositionConstraint.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getLinkPositionConstraint_StoryPatternLink(),
				this.getStoryPatternLink(),
				this.getStoryPatternLink_LinkPositionConstraint(),
				"storyPatternLink", null, 1, 1, LinkPositionConstraint.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				!IS_ORDERED);

		initEClass(linkOrderConstraintEClass, LinkOrderConstraint.class,
				"LinkOrderConstraint", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLinkOrderConstraint_ConstraintType(),
				this.getLinkOrderConstraintEnumeration(), "constraintType",
				null, 1, 1, LinkOrderConstraint.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getLinkOrderConstraint_PredecessorLink(),
				this.getStoryPatternLink(),
				this.getStoryPatternLink_OutgoingLinkOrderConstraints(),
				"predecessorLink", null, 1, 1, LinkOrderConstraint.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				!IS_ORDERED);
		initEReference(getLinkOrderConstraint_SuccessorLink(),
				this.getStoryPatternLink(),
				this.getStoryPatternLink_IncomingLinkOrderConstraints(),
				"successorLink", null, 1, 1, LinkOrderConstraint.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				!IS_ORDERED);

		initEClass(externalReferenceEClass, ExternalReference.class,
				"ExternalReference", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExternalReference_Expression(),
				theExpressionsPackage.getExpression(), null, "expression",
				null, 1, 1, ExternalReference.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExternalReference_Many(), ecorePackage.getEBoolean(),
				"many", null, 1, 1, ExternalReference.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(bindingTypeEnumerationEEnum, BindingTypeEnumeration.class,
				"BindingTypeEnumeration");
		addEEnumLiteral(bindingTypeEnumerationEEnum,
				BindingTypeEnumeration.UNBOUND);
		addEEnumLiteral(bindingTypeEnumerationEEnum,
				BindingTypeEnumeration.BOUND);
		addEEnumLiteral(bindingTypeEnumerationEEnum,
				BindingTypeEnumeration.CAN_BE_BOUND);

		initEEnum(storyPatternMatchTypeEnumerationEEnum,
				StoryPatternMatchTypeEnumeration.class,
				"StoryPatternMatchTypeEnumeration");
		addEEnumLiteral(storyPatternMatchTypeEnumerationEEnum,
				StoryPatternMatchTypeEnumeration.NONE);
		addEEnumLiteral(storyPatternMatchTypeEnumerationEEnum,
				StoryPatternMatchTypeEnumeration.NEGATIVE);
		addEEnumLiteral(storyPatternMatchTypeEnumerationEEnum,
				StoryPatternMatchTypeEnumeration.OPTIONAL);
		addEEnumLiteral(storyPatternMatchTypeEnumerationEEnum,
				StoryPatternMatchTypeEnumeration.SET);
		addEEnumLiteral(storyPatternMatchTypeEnumerationEEnum,
				StoryPatternMatchTypeEnumeration.THIS_OBJECT);

		initEEnum(storyPatternModifierEnumerationEEnum,
				StoryPatternModifierEnumeration.class,
				"StoryPatternModifierEnumeration");
		addEEnumLiteral(storyPatternModifierEnumerationEEnum,
				StoryPatternModifierEnumeration.NONE);
		addEEnumLiteral(storyPatternModifierEnumerationEEnum,
				StoryPatternModifierEnumeration.CREATE);
		addEEnumLiteral(storyPatternModifierEnumerationEEnum,
				StoryPatternModifierEnumeration.DESTROY);

		initEEnum(storyPatternNACSematicEnumerationEEnum,
				StoryPatternNACSematicEnumeration.class,
				"StoryPatternNACSematicEnumeration");
		addEEnumLiteral(storyPatternNACSematicEnumerationEEnum,
				StoryPatternNACSematicEnumeration.OR);
		addEEnumLiteral(storyPatternNACSematicEnumerationEEnum,
				StoryPatternNACSematicEnumeration.AND);

		initEEnum(linkPositionConstraintEnumerationEEnum,
				LinkPositionConstraintEnumeration.class,
				"LinkPositionConstraintEnumeration");
		addEEnumLiteral(linkPositionConstraintEnumerationEEnum,
				LinkPositionConstraintEnumeration.FIRST);
		addEEnumLiteral(linkPositionConstraintEnumerationEEnum,
				LinkPositionConstraintEnumeration.MIDDLE);
		addEEnumLiteral(linkPositionConstraintEnumerationEEnum,
				LinkPositionConstraintEnumeration.LAST);

		initEEnum(linkOrderConstraintEnumerationEEnum,
				LinkOrderConstraintEnumeration.class,
				"LinkOrderConstraintEnumeration");
		addEEnumLiteral(linkOrderConstraintEnumerationEEnum,
				LinkOrderConstraintEnumeration.DIRECT_SUCCESSOR);
		addEEnumLiteral(linkOrderConstraintEnumerationEEnum,
				LinkOrderConstraintEnumeration.SUCCESSOR);
	}

} // SdmPackageImpl
