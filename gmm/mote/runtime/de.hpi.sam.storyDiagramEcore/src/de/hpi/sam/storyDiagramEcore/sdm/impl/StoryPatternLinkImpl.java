/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.sdm.ExternalReference;
import de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Story Pattern Link</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternLinkImpl#getEStructuralFeature <em>EStructural Feature</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternLinkImpl#getLinkPositionConstraint <em>Link Position Constraint</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternLinkImpl#getOutgoingLinkOrderConstraints <em>Outgoing Link Order Constraints</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternLinkImpl#getIncomingLinkOrderConstraints <em>Incoming Link Order Constraints</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternLinkImpl#getEOppositeReference <em>EOpposite Reference</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.StoryPatternLinkImpl#getExternalReference <em>External Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StoryPatternLinkImpl extends AbstractStoryPatternLinkImpl
		implements StoryPatternLink {
	/**
	 * The cached value of the '{@link #getEStructuralFeature() <em>EStructural Feature</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getEStructuralFeature()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature eStructuralFeature;

	/**
	 * The cached value of the '{@link #getLinkPositionConstraint()
	 * <em>Link Position Constraint</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getLinkPositionConstraint()
	 * @generated
	 * @ordered
	 */
	protected LinkPositionConstraint linkPositionConstraint;
	/**
	 * The cached value of the '{@link #getOutgoingLinkOrderConstraints() <em>Outgoing Link Order Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOutgoingLinkOrderConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkOrderConstraint> outgoingLinkOrderConstraints;
	/**
	 * The cached value of the '{@link #getIncomingLinkOrderConstraints()
	 * <em>Incoming Link Order Constraints</em>}' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getIncomingLinkOrderConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkOrderConstraint> incomingLinkOrderConstraints;

	/**
	 * The cached value of the '{@link #getEOppositeReference() <em>EOpposite Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEOppositeReference()
	 * @generated
	 * @ordered
	 */
	protected EReference eOppositeReference;

	/**
	 * The cached value of the '{@link #getExternalReference() <em>External Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalReference()
	 * @generated
	 * @ordered
	 */
	protected ExternalReference externalReference;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected StoryPatternLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SdmPackage.Literals.STORY_PATTERN_LINK;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature getEStructuralFeature() {
		if (eStructuralFeature != null && eStructuralFeature.eIsProxy()) {
			InternalEObject oldEStructuralFeature = (InternalEObject) eStructuralFeature;
			eStructuralFeature = (EStructuralFeature) eResolveProxy(oldEStructuralFeature);
			if (eStructuralFeature != oldEStructuralFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SdmPackage.STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE,
							oldEStructuralFeature, eStructuralFeature));
			}
		}
		return eStructuralFeature;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature basicGetEStructuralFeature() {
		return eStructuralFeature;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setEStructuralFeature(EStructuralFeature newEStructuralFeature) {
		EStructuralFeature oldEStructuralFeature = eStructuralFeature;
		eStructuralFeature = newEStructuralFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE,
					oldEStructuralFeature, eStructuralFeature));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LinkPositionConstraint getLinkPositionConstraint() {
		return linkPositionConstraint;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLinkPositionConstraint(
			LinkPositionConstraint newLinkPositionConstraint,
			NotificationChain msgs) {
		LinkPositionConstraint oldLinkPositionConstraint = linkPositionConstraint;
		linkPositionConstraint = newLinkPositionConstraint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					SdmPackage.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT,
					oldLinkPositionConstraint, newLinkPositionConstraint);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkPositionConstraint(
			LinkPositionConstraint newLinkPositionConstraint) {
		if (newLinkPositionConstraint != linkPositionConstraint) {
			NotificationChain msgs = null;
			if (linkPositionConstraint != null)
				msgs = ((InternalEObject) linkPositionConstraint)
						.eInverseRemove(
								this,
								SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK,
								LinkPositionConstraint.class, msgs);
			if (newLinkPositionConstraint != null)
				msgs = ((InternalEObject) newLinkPositionConstraint)
						.eInverseAdd(
								this,
								SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK,
								LinkPositionConstraint.class, msgs);
			msgs = basicSetLinkPositionConstraint(newLinkPositionConstraint,
					msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT,
					newLinkPositionConstraint, newLinkPositionConstraint));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkOrderConstraint> getOutgoingLinkOrderConstraints() {
		if (outgoingLinkOrderConstraints == null) {
			outgoingLinkOrderConstraints = new EObjectContainmentWithInverseEList<LinkOrderConstraint>(
					LinkOrderConstraint.class,
					this,
					SdmPackage.STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS,
					SdmPackage.LINK_ORDER_CONSTRAINT__PREDECESSOR_LINK);
		}
		return outgoingLinkOrderConstraints;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkOrderConstraint> getIncomingLinkOrderConstraints() {
		if (incomingLinkOrderConstraints == null) {
			incomingLinkOrderConstraints = new EObjectWithInverseResolvingEList<LinkOrderConstraint>(
					LinkOrderConstraint.class,
					this,
					SdmPackage.STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS,
					SdmPackage.LINK_ORDER_CONSTRAINT__SUCCESSOR_LINK);
		}
		return incomingLinkOrderConstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEOppositeReference() {
		if (eOppositeReference != null && eOppositeReference.eIsProxy()) {
			InternalEObject oldEOppositeReference = (InternalEObject) eOppositeReference;
			eOppositeReference = (EReference) eResolveProxy(oldEOppositeReference);
			if (eOppositeReference != oldEOppositeReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SdmPackage.STORY_PATTERN_LINK__EOPPOSITE_REFERENCE,
							oldEOppositeReference, eOppositeReference));
			}
		}
		return eOppositeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetEOppositeReference() {
		return eOppositeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEOppositeReference(EReference newEOppositeReference) {
		EReference oldEOppositeReference = eOppositeReference;
		eOppositeReference = newEOppositeReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.STORY_PATTERN_LINK__EOPPOSITE_REFERENCE,
					oldEOppositeReference, eOppositeReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalReference getExternalReference() {
		return externalReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExternalReference(
			ExternalReference newExternalReference, NotificationChain msgs) {
		ExternalReference oldExternalReference = externalReference;
		externalReference = newExternalReference;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					SdmPackage.STORY_PATTERN_LINK__EXTERNAL_REFERENCE,
					oldExternalReference, newExternalReference);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalReference(ExternalReference newExternalReference) {
		if (newExternalReference != externalReference) {
			NotificationChain msgs = null;
			if (externalReference != null)
				msgs = ((InternalEObject) externalReference)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- SdmPackage.STORY_PATTERN_LINK__EXTERNAL_REFERENCE,
								null, msgs);
			if (newExternalReference != null)
				msgs = ((InternalEObject) newExternalReference)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- SdmPackage.STORY_PATTERN_LINK__EXTERNAL_REFERENCE,
								null, msgs);
			msgs = basicSetExternalReference(newExternalReference, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.STORY_PATTERN_LINK__EXTERNAL_REFERENCE,
					newExternalReference, newExternalReference));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT:
			if (linkPositionConstraint != null)
				msgs = ((InternalEObject) linkPositionConstraint)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- SdmPackage.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT,
								null, msgs);
			return basicSetLinkPositionConstraint(
					(LinkPositionConstraint) otherEnd, msgs);
		case SdmPackage.STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOutgoingLinkOrderConstraints())
					.basicAdd(otherEnd, msgs);
		case SdmPackage.STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getIncomingLinkOrderConstraints())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT:
			return basicSetLinkPositionConstraint(null, msgs);
		case SdmPackage.STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS:
			return ((InternalEList<?>) getOutgoingLinkOrderConstraints())
					.basicRemove(otherEnd, msgs);
		case SdmPackage.STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS:
			return ((InternalEList<?>) getIncomingLinkOrderConstraints())
					.basicRemove(otherEnd, msgs);
		case SdmPackage.STORY_PATTERN_LINK__EXTERNAL_REFERENCE:
			return basicSetExternalReference(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE:
			if (resolve)
				return getEStructuralFeature();
			return basicGetEStructuralFeature();
		case SdmPackage.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT:
			return getLinkPositionConstraint();
		case SdmPackage.STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS:
			return getOutgoingLinkOrderConstraints();
		case SdmPackage.STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS:
			return getIncomingLinkOrderConstraints();
		case SdmPackage.STORY_PATTERN_LINK__EOPPOSITE_REFERENCE:
			if (resolve)
				return getEOppositeReference();
			return basicGetEOppositeReference();
		case SdmPackage.STORY_PATTERN_LINK__EXTERNAL_REFERENCE:
			return getExternalReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE:
			setEStructuralFeature((EStructuralFeature) newValue);
			return;
		case SdmPackage.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT:
			setLinkPositionConstraint((LinkPositionConstraint) newValue);
			return;
		case SdmPackage.STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS:
			getOutgoingLinkOrderConstraints().clear();
			getOutgoingLinkOrderConstraints().addAll(
					(Collection<? extends LinkOrderConstraint>) newValue);
			return;
		case SdmPackage.STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS:
			getIncomingLinkOrderConstraints().clear();
			getIncomingLinkOrderConstraints().addAll(
					(Collection<? extends LinkOrderConstraint>) newValue);
			return;
		case SdmPackage.STORY_PATTERN_LINK__EOPPOSITE_REFERENCE:
			setEOppositeReference((EReference) newValue);
			return;
		case SdmPackage.STORY_PATTERN_LINK__EXTERNAL_REFERENCE:
			setExternalReference((ExternalReference) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE:
			setEStructuralFeature((EStructuralFeature) null);
			return;
		case SdmPackage.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT:
			setLinkPositionConstraint((LinkPositionConstraint) null);
			return;
		case SdmPackage.STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS:
			getOutgoingLinkOrderConstraints().clear();
			return;
		case SdmPackage.STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS:
			getIncomingLinkOrderConstraints().clear();
			return;
		case SdmPackage.STORY_PATTERN_LINK__EOPPOSITE_REFERENCE:
			setEOppositeReference((EReference) null);
			return;
		case SdmPackage.STORY_PATTERN_LINK__EXTERNAL_REFERENCE:
			setExternalReference((ExternalReference) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SdmPackage.STORY_PATTERN_LINK__ESTRUCTURAL_FEATURE:
			return eStructuralFeature != null;
		case SdmPackage.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT:
			return linkPositionConstraint != null;
		case SdmPackage.STORY_PATTERN_LINK__OUTGOING_LINK_ORDER_CONSTRAINTS:
			return outgoingLinkOrderConstraints != null
					&& !outgoingLinkOrderConstraints.isEmpty();
		case SdmPackage.STORY_PATTERN_LINK__INCOMING_LINK_ORDER_CONSTRAINTS:
			return incomingLinkOrderConstraints != null
					&& !incomingLinkOrderConstraints.isEmpty();
		case SdmPackage.STORY_PATTERN_LINK__EOPPOSITE_REFERENCE:
			return eOppositeReference != null;
		case SdmPackage.STORY_PATTERN_LINK__EXTERNAL_REFERENCE:
			return externalReference != null;
		}
		return super.eIsSet(featureID);
	}

} // StoryPatternLinkImpl
