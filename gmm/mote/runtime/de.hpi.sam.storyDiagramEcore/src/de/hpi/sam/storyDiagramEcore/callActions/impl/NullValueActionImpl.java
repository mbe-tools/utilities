/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.impl;

import org.eclipse.emf.ecore.EClass;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.NullValueAction;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Null Value Action</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class NullValueActionImpl extends CallActionImpl implements
		NullValueAction {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected NullValueActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CallActionsPackage.Literals.NULL_VALUE_ACTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		return "null";
	}

} // NullValueActionImpl
