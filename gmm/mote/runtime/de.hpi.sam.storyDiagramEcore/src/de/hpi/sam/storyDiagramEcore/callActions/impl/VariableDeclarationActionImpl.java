/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Variable Declaration Action</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.VariableDeclarationActionImpl#getValueAssignment <em>Value Assignment</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VariableDeclarationActionImpl extends VariableReferenceActionImpl
		implements VariableDeclarationAction {
	/**
	 * The cached value of the '{@link #getValueAssignment() <em>Value Assignment</em>}' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getValueAssignment()
	 * @generated
	 * @ordered
	 */
	protected Expression valueAssignment;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableDeclarationActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CallActionsPackage.Literals.VARIABLE_DECLARATION_ACTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getValueAssignment() {
		return valueAssignment;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValueAssignment(
			Expression newValueAssignment, NotificationChain msgs) {
		Expression oldValueAssignment = valueAssignment;
		valueAssignment = newValueAssignment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
					this,
					Notification.SET,
					CallActionsPackage.VARIABLE_DECLARATION_ACTION__VALUE_ASSIGNMENT,
					oldValueAssignment, newValueAssignment);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueAssignment(Expression newValueAssignment) {
		if (newValueAssignment != valueAssignment) {
			NotificationChain msgs = null;
			if (valueAssignment != null)
				msgs = ((InternalEObject) valueAssignment)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.VARIABLE_DECLARATION_ACTION__VALUE_ASSIGNMENT,
								null, msgs);
			if (newValueAssignment != null)
				msgs = ((InternalEObject) newValueAssignment)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.VARIABLE_DECLARATION_ACTION__VALUE_ASSIGNMENT,
								null, msgs);
			msgs = basicSetValueAssignment(newValueAssignment, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					CallActionsPackage.VARIABLE_DECLARATION_ACTION__VALUE_ASSIGNMENT,
					newValueAssignment, newValueAssignment));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer buffer = new StringBuffer();

		if (getClassifier() != null && getClassifier().getName() != null
				&& !"".equals(getClassifier().getName())) {
			buffer.append(getClassifier().getName());
		} else {
			buffer.append("[null]");
		}

		buffer.append(" ");
		buffer.append(getVariableName());
		buffer.append(" := ");

		if (getValueAssignment() != null) {
			buffer.append(getValueAssignment().toString());
		} else {
			buffer.append("[null]");
		}

		return buffer.toString().trim();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CallActionsPackage.VARIABLE_DECLARATION_ACTION__VALUE_ASSIGNMENT:
			return basicSetValueAssignment(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CallActionsPackage.VARIABLE_DECLARATION_ACTION__VALUE_ASSIGNMENT:
			return getValueAssignment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CallActionsPackage.VARIABLE_DECLARATION_ACTION__VALUE_ASSIGNMENT:
			setValueAssignment((Expression) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CallActionsPackage.VARIABLE_DECLARATION_ACTION__VALUE_ASSIGNMENT:
			setValueAssignment((Expression) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CallActionsPackage.VARIABLE_DECLARATION_ACTION__VALUE_ASSIGNMENT:
			return valueAssignment != null;
		}
		return super.eIsSet(featureID);
	}

} // VariableDeclarationActionImpl
