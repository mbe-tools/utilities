/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import de.hpi.sam.storyDiagramEcore.sdm.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.EContainerStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.SdmFactory;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class SdmFactoryImpl extends EFactoryImpl implements SdmFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static SdmFactory init() {
		try {
			SdmFactory theSdmFactory = (SdmFactory) EPackage.Registry.INSTANCE
					.getEFactory(SdmPackage.eNS_URI);
			if (theSdmFactory != null) {
				return theSdmFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SdmFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public SdmFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case SdmPackage.ATTRIBUTE_ASSIGNMENT:
			return createAttributeAssignment();
		case SdmPackage.STORY_PATTERN_LINK:
			return createStoryPatternLink();
		case SdmPackage.STORY_PATTERN_CONTAINMENT_LINK:
			return createStoryPatternContainmentLink();
		case SdmPackage.STORY_PATTERN_EXPRESSION_LINK:
			return createStoryPatternExpressionLink();
		case SdmPackage.STORY_PATTERN_OBJECT:
			return createStoryPatternObject();
		case SdmPackage.MAP_ENTRY_STORY_PATTERN_LINK:
			return createMapEntryStoryPatternLink();
		case SdmPackage.ECONTAINER_STORY_PATTERN_LINK:
			return createEContainerStoryPatternLink();
		case SdmPackage.LINK_POSITION_CONSTRAINT:
			return createLinkPositionConstraint();
		case SdmPackage.LINK_ORDER_CONSTRAINT:
			return createLinkOrderConstraint();
		case SdmPackage.EXTERNAL_REFERENCE:
			return createExternalReference();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case SdmPackage.BINDING_TYPE_ENUMERATION:
			return createBindingTypeEnumerationFromString(eDataType,
					initialValue);
		case SdmPackage.STORY_PATTERN_MATCH_TYPE_ENUMERATION:
			return createStoryPatternMatchTypeEnumerationFromString(eDataType,
					initialValue);
		case SdmPackage.STORY_PATTERN_MODIFIER_ENUMERATION:
			return createStoryPatternModifierEnumerationFromString(eDataType,
					initialValue);
		case SdmPackage.STORY_PATTERN_NAC_SEMATIC_ENUMERATION:
			return createStoryPatternNACSematicEnumerationFromString(eDataType,
					initialValue);
		case SdmPackage.LINK_POSITION_CONSTRAINT_ENUMERATION:
			return createLinkPositionConstraintEnumerationFromString(eDataType,
					initialValue);
		case SdmPackage.LINK_ORDER_CONSTRAINT_ENUMERATION:
			return createLinkOrderConstraintEnumerationFromString(eDataType,
					initialValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case SdmPackage.BINDING_TYPE_ENUMERATION:
			return convertBindingTypeEnumerationToString(eDataType,
					instanceValue);
		case SdmPackage.STORY_PATTERN_MATCH_TYPE_ENUMERATION:
			return convertStoryPatternMatchTypeEnumerationToString(eDataType,
					instanceValue);
		case SdmPackage.STORY_PATTERN_MODIFIER_ENUMERATION:
			return convertStoryPatternModifierEnumerationToString(eDataType,
					instanceValue);
		case SdmPackage.STORY_PATTERN_NAC_SEMATIC_ENUMERATION:
			return convertStoryPatternNACSematicEnumerationToString(eDataType,
					instanceValue);
		case SdmPackage.LINK_POSITION_CONSTRAINT_ENUMERATION:
			return convertLinkPositionConstraintEnumerationToString(eDataType,
					instanceValue);
		case SdmPackage.LINK_ORDER_CONSTRAINT_ENUMERATION:
			return convertLinkOrderConstraintEnumerationToString(eDataType,
					instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeAssignment createAttributeAssignment() {
		AttributeAssignmentImpl attributeAssignment = new AttributeAssignmentImpl();
		return attributeAssignment;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternLink createStoryPatternLink() {
		StoryPatternLinkImpl storyPatternLink = new StoryPatternLinkImpl();
		return storyPatternLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternContainmentLink createStoryPatternContainmentLink() {
		StoryPatternContainmentLinkImpl storyPatternContainmentLink = new StoryPatternContainmentLinkImpl();
		return storyPatternContainmentLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternExpressionLink createStoryPatternExpressionLink() {
		StoryPatternExpressionLinkImpl storyPatternExpressionLink = new StoryPatternExpressionLinkImpl();
		return storyPatternExpressionLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternObject createStoryPatternObject() {
		StoryPatternObjectImpl storyPatternObject = new StoryPatternObjectImpl();
		return storyPatternObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MapEntryStoryPatternLink createMapEntryStoryPatternLink() {
		MapEntryStoryPatternLinkImpl mapEntryStoryPatternLink = new MapEntryStoryPatternLinkImpl();
		return mapEntryStoryPatternLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EContainerStoryPatternLink createEContainerStoryPatternLink() {
		EContainerStoryPatternLinkImpl eContainerStoryPatternLink = new EContainerStoryPatternLinkImpl();
		return eContainerStoryPatternLink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LinkPositionConstraint createLinkPositionConstraint() {
		LinkPositionConstraintImpl linkPositionConstraint = new LinkPositionConstraintImpl();
		return linkPositionConstraint;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LinkOrderConstraint createLinkOrderConstraint() {
		LinkOrderConstraintImpl linkOrderConstraint = new LinkOrderConstraintImpl();
		return linkOrderConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalReference createExternalReference() {
		ExternalReferenceImpl externalReference = new ExternalReferenceImpl();
		return externalReference;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public BindingTypeEnumeration createBindingTypeEnumerationFromString(
			EDataType eDataType, String initialValue) {
		BindingTypeEnumeration result = BindingTypeEnumeration
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBindingTypeEnumerationToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternMatchTypeEnumeration createStoryPatternMatchTypeEnumerationFromString(
			EDataType eDataType, String initialValue) {
		StoryPatternMatchTypeEnumeration result = StoryPatternMatchTypeEnumeration
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStoryPatternMatchTypeEnumerationToString(
			EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternModifierEnumeration createStoryPatternModifierEnumerationFromString(
			EDataType eDataType, String initialValue) {
		StoryPatternModifierEnumeration result = StoryPatternModifierEnumeration
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStoryPatternModifierEnumerationToString(
			EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public StoryPatternNACSematicEnumeration createStoryPatternNACSematicEnumerationFromString(
			EDataType eDataType, String initialValue) {
		StoryPatternNACSematicEnumeration result = StoryPatternNACSematicEnumeration
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStoryPatternNACSematicEnumerationToString(
			EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LinkPositionConstraintEnumeration createLinkPositionConstraintEnumerationFromString(
			EDataType eDataType, String initialValue) {
		LinkPositionConstraintEnumeration result = LinkPositionConstraintEnumeration
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLinkPositionConstraintEnumerationToString(
			EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LinkOrderConstraintEnumeration createLinkOrderConstraintEnumerationFromString(
			EDataType eDataType, String initialValue) {
		LinkOrderConstraintEnumeration result = LinkOrderConstraintEnumeration
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLinkOrderConstraintEnumerationToString(
			EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SdmPackage getSdmPackage() {
		return (SdmPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SdmPackage getPackage() {
		return SdmPackage.eINSTANCE;
	}

} // SdmFactoryImpl
