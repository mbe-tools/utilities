/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Map Entry Story Pattern Link</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The MapEntryStoryPatternLink represents a map entry. The target reference points at the key object, the value reference at the value object. The value reference can be omitted.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink#getValueTarget <em>Value Target</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink#getEStructuralFeature <em>EStructural Feature</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink#getClassifier <em>Classifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getMapEntryStoryPatternLink()
 * @model
 * @generated
 */
public interface MapEntryStoryPatternLink extends AbstractStoryPatternLink {
	/**
	 * Returns the value of the '<em><b>Value Target</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Value Target</em>' reference.
	 * @see #setValueTarget(AbstractStoryPatternObject)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getMapEntryStoryPatternLink_ValueTarget()
	 * @model
	 * @generated
	 */
	AbstractStoryPatternObject getValueTarget();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink#getValueTarget <em>Value Target</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Value Target</em>' reference.
	 * @see #getValueTarget()
	 * @generated
	 */
	void setValueTarget(AbstractStoryPatternObject value);

	/**
	 * Returns the value of the '<em><b>EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The EReference that conatins the map entry. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>EStructural Feature</em>' reference.
	 * @see #setEStructuralFeature(EStructuralFeature)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getMapEntryStoryPatternLink_EStructuralFeature()
	 * @model required="true"
	 * @generated
	 */
	EStructuralFeature getEStructuralFeature();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink#getEStructuralFeature <em>EStructural Feature</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>EStructural Feature</em>' reference.
	 * @see #getEStructuralFeature()
	 * @generated
	 */
	void setEStructuralFeature(EStructuralFeature value);

	/**
	 * Returns the value of the '<em><b>Classifier</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * classifier of the map entry, i.e. an EClass whose instance type name is
	 * 'java.util.Map$Entry'. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Classifier</em>' reference.
	 * @see #setClassifier(EClassifier)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getMapEntryStoryPatternLink_Classifier()
	 * @model required="true"
	 * @generated
	 */
	EClassifier getClassifier();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink#getClassifier <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Classifier</em>' reference.
	 * @see #getClassifier()
	 * @generated
	 */
	void setClassifier(EClassifier value);

} // MapEntryStoryPatternLink
