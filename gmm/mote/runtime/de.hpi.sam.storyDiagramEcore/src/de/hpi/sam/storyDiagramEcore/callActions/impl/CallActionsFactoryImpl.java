/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.impl;

import de.hpi.sam.storyDiagramEcore.callActions.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsFactory;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction;
import de.hpi.sam.storyDiagramEcore.callActions.CompareAction;
import de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction;
import de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction;
import de.hpi.sam.storyDiagramEcore.callActions.NullValueAction;
import de.hpi.sam.storyDiagramEcore.callActions.OperationAction;
import de.hpi.sam.storyDiagramEcore.callActions.Operators;
import de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class CallActionsFactoryImpl extends EFactoryImpl implements
		CallActionsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static CallActionsFactory init() {
		try {
			CallActionsFactory theCallActionsFactory = (CallActionsFactory) EPackage.Registry.INSTANCE
					.getEFactory(CallActionsPackage.eNS_URI);
			if (theCallActionsFactory != null) {
				return theCallActionsFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CallActionsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public CallActionsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case CallActionsPackage.CALL_ACTION_PARAMETER:
			return createCallActionParameter();
		case CallActionsPackage.CALL_STORY_DIAGRAM_INTERPRETER_ACTION:
			return createCallStoryDiagramInterpreterAction();
		case CallActionsPackage.LITERAL_DECLARATION_ACTION:
			return createLiteralDeclarationAction();
		case CallActionsPackage.METHOD_CALL_ACTION:
			return createMethodCallAction();
		case CallActionsPackage.NEW_OBJECT_ACTION:
			return createNewObjectAction();
		case CallActionsPackage.VARIABLE_DECLARATION_ACTION:
			return createVariableDeclarationAction();
		case CallActionsPackage.VARIABLE_REFERENCE_ACTION:
			return createVariableReferenceAction();
		case CallActionsPackage.COMPARE_ACTION:
			return createCompareAction();
		case CallActionsPackage.NULL_VALUE_ACTION:
			return createNullValueAction();
		case CallActionsPackage.OPERATION_ACTION:
			return createOperationAction();
		case CallActionsPackage.CASE_INSENSITIVE_COMPARATOR:
			return createCaseInsensitiveComparator();
		case CallActionsPackage.GET_PROPERTY_VALUE_ACTION:
			return createGetPropertyValueAction();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case CallActionsPackage.OPERATORS:
			return createOperatorsFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case CallActionsPackage.OPERATORS:
			return convertOperatorsToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CallActionParameter createCallActionParameter() {
		CallActionParameterImpl callActionParameter = new CallActionParameterImpl();
		return callActionParameter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CallStoryDiagramInterpreterAction createCallStoryDiagramInterpreterAction() {
		CallStoryDiagramInterpreterActionImpl callStoryDiagramInterpreterAction = new CallStoryDiagramInterpreterActionImpl();
		return callStoryDiagramInterpreterAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LiteralDeclarationAction createLiteralDeclarationAction() {
		LiteralDeclarationActionImpl literalDeclarationAction = new LiteralDeclarationActionImpl();
		return literalDeclarationAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MethodCallAction createMethodCallAction() {
		MethodCallActionImpl methodCallAction = new MethodCallActionImpl();
		return methodCallAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NewObjectAction createNewObjectAction() {
		NewObjectActionImpl newObjectAction = new NewObjectActionImpl();
		return newObjectAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public VariableDeclarationAction createVariableDeclarationAction() {
		VariableDeclarationActionImpl variableDeclarationAction = new VariableDeclarationActionImpl();
		return variableDeclarationAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public VariableReferenceAction createVariableReferenceAction() {
		VariableReferenceActionImpl variableReferenceAction = new VariableReferenceActionImpl();
		return variableReferenceAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CompareAction createCompareAction() {
		CompareActionImpl compareAction = new CompareActionImpl();
		return compareAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NullValueAction createNullValueAction() {
		NullValueActionImpl nullValueAction = new NullValueActionImpl();
		return nullValueAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public OperationAction createOperationAction() {
		OperationActionImpl operationAction = new OperationActionImpl();
		return operationAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseInsensitiveComparator createCaseInsensitiveComparator() {
		CaseInsensitiveComparatorImpl caseInsensitiveComparator = new CaseInsensitiveComparatorImpl();
		return caseInsensitiveComparator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPropertyValueAction createGetPropertyValueAction() {
		GetPropertyValueActionImpl getPropertyValueAction = new GetPropertyValueActionImpl();
		return getPropertyValueAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Operators createOperatorsFromString(EDataType eDataType,
			String initialValue) {
		Operators result = Operators.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperatorsToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CallActionsPackage getCallActionsPackage() {
		return (CallActionsPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CallActionsPackage getPackage() {
		return CallActionsPackage.eINSTANCE;
	}

} // CallActionsFactoryImpl
