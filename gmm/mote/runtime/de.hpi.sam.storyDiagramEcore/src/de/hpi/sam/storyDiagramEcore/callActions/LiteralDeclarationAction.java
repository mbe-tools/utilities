/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Literal Declaration Action</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The LiteralDeclarationAction declares a literal, e.g. Integer "42".
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction#getLiteral <em>Literal</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getLiteralDeclarationAction()
 * @model
 * @generated
 */
public interface LiteralDeclarationAction extends CallAction {
	/**
	 * Returns the value of the '<em><b>Literal</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * literal as String. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Literal</em>' attribute.
	 * @see #setLiteral(String)
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getLiteralDeclarationAction_Literal()
	 * @model required="true"
	 * @generated
	 */
	String getLiteral();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction#getLiteral
	 * <em>Literal</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Literal</em>' attribute.
	 * @see #getLiteral()
	 * @generated
	 */
	void setLiteral(String value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (eIsProxy()) return super.toString();\r\n\r\nif (getLiteral() != null && !\"\".equals(getLiteral()))\r\n{\r\n\tif (getClassifier() != null && getClassifier() == org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEString())\r\n\t{\r\n\t\treturn \'\"\' + getLiteral() + \'\"\';\r\n\t}\r\n\telse\r\n\t{\r\n\t\treturn getLiteral();\r\n\t}\r\n}\r\nelse return \"[null]\";'"
	 * @generated
	 */
	String toString();

} // LiteralDeclarationAction
