/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

import org.eclipse.emf.common.util.EList;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Activity Edge</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The acitivty edges connect activity nodes within an activity. A guard can be set (see ActivityEdgeGuardEnumeration). If the guard is set to boolean a guardExpression must be provided that returns a boolean value.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getSource <em>Source</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getTarget <em>Target</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getActivity <em>Activity</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getGuardType <em>Guard Type</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getGuardExpression <em>Guard Expression</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getReleaseEdges <em>Release Edges</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getAcquireEdges <em>Acquire Edges</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityEdge()
 * @model
 * @generated
 */
public interface ActivityEdge extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getOutgoing
	 * <em>Outgoing</em>}'. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> The source activity node of this edge. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(ActivityNode)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityEdge_Source()
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getOutgoing
	 * @model opposite="outgoing" required="true"
	 * @generated
	 */
	ActivityNode getSource();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getSource
	 * <em>Source</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(ActivityNode value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getIncoming
	 * <em>Incoming</em>}'. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> The target activity node of this edge. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(ActivityNode)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityEdge_Target()
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityNode#getIncoming
	 * @model opposite="incoming" required="true"
	 * @generated
	 */
	ActivityNode getTarget();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getTarget
	 * <em>Target</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ActivityNode value);

	/**
	 * Returns the value of the '<em><b>Activity</b></em>' container reference.
	 * It is bidirectional and its opposite is '
	 * {@link de.hpi.sam.storyDiagramEcore.Activity#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The activity that contains this activity edge. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Activity</em>' container reference.
	 * @see #setActivity(Activity)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityEdge_Activity()
	 * @see de.hpi.sam.storyDiagramEcore.Activity#getEdges
	 * @model opposite="edges" required="true" transient="false"
	 * @generated
	 */
	Activity getActivity();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getActivity <em>Activity</em>}' container reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Activity</em>' container reference.
	 * @see #getActivity()
	 * @generated
	 */
	void setActivity(Activity value);

	/**
	 * Returns the value of the '<em><b>Guard Type</b></em>' attribute. The
	 * literals are from the enumeration
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration}.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The type of this guard. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Guard Type</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration
	 * @see #setGuardType(ActivityEdgeGuardEnumeration)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityEdge_GuardType()
	 * @model required="true"
	 * @generated
	 */
	ActivityEdgeGuardEnumeration getGuardType();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getGuardType <em>Guard Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Guard Type</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration
	 * @see #getGuardType()
	 * @generated
	 */
	void setGuardType(ActivityEdgeGuardEnumeration value);

	/**
	 * Returns the value of the '<em><b>Guard Expression</b></em>' containment
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> If the type of the guard is BOOLEAN, this expression
	 * contains the Boolean condition. The constraint expression must return a
	 * boolean value. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Guard Expression</em>' containment
	 *         reference.
	 * @see #setGuardExpression(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityEdge_GuardExpression()
	 * @model containment="true"
	 * @generated
	 */
	Expression getGuardExpression();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getGuardExpression <em>Guard Expression</em>}' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Guard Expression</em>' containment reference.
	 * @see #getGuardExpression()
	 * @generated
	 */
	void setGuardExpression(Expression value);

	/**
	 * Returns the value of the '<em><b>Release Edges</b></em>' reference list.
	 * The list contents are of type {@link de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge}.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge#getActivityEdge <em>Activity Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Release Edges</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Release Edges</em>' reference list.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityEdge_ReleaseEdges()
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge#getActivityEdge
	 * @model opposite="activityEdge"
	 * @generated
	 */
	EList<ReleaseSemaphoreEdge> getReleaseEdges();

	/**
	 * Returns the value of the '<em><b>Acquire Edges</b></em>' reference list.
	 * The list contents are of type {@link de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge}.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge#getActivityEdge <em>Activity Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acquire Edges</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acquire Edges</em>' reference list.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityEdge_AcquireEdges()
	 * @see de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge#getActivityEdge
	 * @model opposite="activityEdge"
	 * @generated
	 */
	EList<AcquireSemaphoreEdge> getAcquireEdges();

} // ActivityEdge
