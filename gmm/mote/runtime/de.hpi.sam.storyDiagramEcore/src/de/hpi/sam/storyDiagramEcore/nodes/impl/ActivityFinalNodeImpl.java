/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Activity Final Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityFinalNodeImpl#getReturnValue <em>Return Value</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityFinalNodeImpl#getOutParameterValues <em>Out Parameter Values</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActivityFinalNodeImpl extends ActivityNodeImpl implements
		ActivityFinalNode {
	/**
	 * The cached value of the '{@link #getOutParameterValues()
	 * <em>Out Parameter Values</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getOutParameterValues()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> outParameterValues;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityFinalNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NodesPackage.Literals.ACTIVITY_FINAL_NODE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	@Deprecated
	public Expression getReturnValue() {
		if (this.getOutParameterValues().size() == 1) {
			return this.getOutParameterValues().get(0);
		} else {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	public NotificationChain basicSetReturnValue(Expression newReturnValue,
			NotificationChain msgs) {
		Expression oldReturnValue = this.getReturnValue();
		this.setReturnValue(newReturnValue);

		if (this.eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					NodesPackage.ACTIVITY_FINAL_NODE__RETURN_VALUE,
					oldReturnValue, newReturnValue);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	@Deprecated
	public void setReturnValue(Expression newReturnValue) {
		if (newReturnValue == null) {
			this.getOutParameterValues().clear();
		} else if (this.getOutParameterValues().size() <= 1) {
			this.getOutParameterValues().add(0, newReturnValue);
		} else {
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Expression> getOutParameterValues() {
		if (outParameterValues == null) {
			outParameterValues = new EObjectContainmentEList<Expression>(
					Expression.class, this,
					NodesPackage.ACTIVITY_FINAL_NODE__OUT_PARAMETER_VALUES);
		}
		return outParameterValues;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case NodesPackage.ACTIVITY_FINAL_NODE__RETURN_VALUE:
			return basicSetReturnValue(null, msgs);
		case NodesPackage.ACTIVITY_FINAL_NODE__OUT_PARAMETER_VALUES:
			return ((InternalEList<?>) getOutParameterValues()).basicRemove(
					otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case NodesPackage.ACTIVITY_FINAL_NODE__RETURN_VALUE:
			return getReturnValue();
		case NodesPackage.ACTIVITY_FINAL_NODE__OUT_PARAMETER_VALUES:
			return getOutParameterValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case NodesPackage.ACTIVITY_FINAL_NODE__RETURN_VALUE:
			setReturnValue((Expression) newValue);
			return;
		case NodesPackage.ACTIVITY_FINAL_NODE__OUT_PARAMETER_VALUES:
			getOutParameterValues().clear();
			getOutParameterValues().addAll(
					(Collection<? extends Expression>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case NodesPackage.ACTIVITY_FINAL_NODE__RETURN_VALUE:
			setReturnValue((Expression) null);
			return;
		case NodesPackage.ACTIVITY_FINAL_NODE__OUT_PARAMETER_VALUES:
			getOutParameterValues().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case NodesPackage.ACTIVITY_FINAL_NODE__RETURN_VALUE:
			return getReturnValue() != null;
		case NodesPackage.ACTIVITY_FINAL_NODE__OUT_PARAMETER_VALUES:
			return outParameterValues != null && !outParameterValues.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // ActivityFinalNodeImpl
