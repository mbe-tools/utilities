/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

import org.eclipse.emf.common.util.EList;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Activity Final Node</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An ActivityFinalNode marks the termination of an activity, comparable to a return statement in Java code. There must be at least on final node in an activity. A final node must have at least one incoming edge and no outgoing edges. If an ActivityFinalNode is reached, all control flows of the activity are terminated.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode#getReturnValue <em>Return Value</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode#getOutParameterValues <em>Out Parameter Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityFinalNode()
 * @model
 * @generated
 */
public interface ActivityFinalNode extends ActivityNode {
	/**
	 * Returns the value of the '<em><b>Return Value</b></em>' containment
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> An expression that is evaluated to compute the return
	 * value of this activity. The expression must return a value of the return
	 * type of this operation. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Return Value</em>' containment reference.
	 * @see #setReturnValue(Expression)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityFinalNode_ReturnValue()
	 * @model containment="true"
	 * @generated
	 */
	Expression getReturnValue();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode#getReturnValue <em>Return Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Value</em>' containment reference.
	 * @see #getReturnValue()
	 * @generated
	 */
	void setReturnValue(Expression value);

	/**
	 * Returns the value of the '<em><b>Out Parameter Values</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.expressions.Expression}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Parameter Values</em>' containment
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Out Parameter Values</em>' containment
	 *         reference list.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getActivityFinalNode_OutParameterValues()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getOutParameterValues();

} // ActivityFinalNode
