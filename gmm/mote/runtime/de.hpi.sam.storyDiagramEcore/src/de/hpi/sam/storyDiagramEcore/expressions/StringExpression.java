/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.expressions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>String Expression</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The StringExpression contains an expression string of an expression language. The available expression languages are supplied by other plugins.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.expressions.StringExpression#getExpressionString <em>Expression String</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.expressions.StringExpression#getExpressionLanguage <em>Expression Language</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.expressions.StringExpression#getAst <em>Ast</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage#getStringExpression()
 * @model
 * @generated
 */
public interface StringExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Expression String</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The expression string. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Expression String</em>' attribute.
	 * @see #setExpressionString(String)
	 * @see de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage#getStringExpression_ExpressionString()
	 * @model required="true"
	 * @generated
	 */
	String getExpressionString();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.expressions.StringExpression#getExpressionString <em>Expression String</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Expression String</em>' attribute.
	 * @see #getExpressionString()
	 * @generated
	 */
	void setExpressionString(String value);

	/**
	 * Returns the value of the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The expression language of the expression string. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Expression Language</em>' attribute.
	 * @see #setExpressionLanguage(String)
	 * @see de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage#getStringExpression_ExpressionLanguage()
	 * @model required="true"
	 * @generated
	 */
	String getExpressionLanguage();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.expressions.StringExpression#getExpressionLanguage <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Expression Language</em>' attribute.
	 * @see #getExpressionLanguage()
	 * @generated
	 */
	void setExpressionLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Ast</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * This containment reference can be used to store the abstract syntax tree
	 * of the expression in the model. This does not work when the model is
	 * stored in textual syntax. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Ast</em>' containment reference.
	 * @see #setAst(EObject)
	 * @see de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage#getStringExpression_Ast()
	 * @model containment="true"
	 * @generated
	 */
	EObject getAst();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.expressions.StringExpression#getAst <em>Ast</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Ast</em>' containment reference.
	 * @see #getAst()
	 * @generated
	 */
	void setAst(EObject value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (eIsProxy())\r\n{\r\n\treturn super.toString();\r\n}\r\nelse\r\n{\r\n\tif (getExpressionString() == null || \"\".equals(getExpressionString()))\r\n\t{\r\n\t\treturn \"[null]\";\r\n\t}\r\n\telse\r\n\t{\r\n\t\treturn getExpressionString();\r\n\t}\r\n}'"
	 * @generated
	 */
	String toString();

} // StringExpression
