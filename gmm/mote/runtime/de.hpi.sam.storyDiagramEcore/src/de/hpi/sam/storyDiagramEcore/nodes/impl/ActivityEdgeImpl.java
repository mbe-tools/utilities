/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl;
import de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdgeGuardEnumeration;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Activity Edge</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityEdgeImpl#getSource <em>Source</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityEdgeImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityEdgeImpl#getActivity <em>Activity</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityEdgeImpl#getGuardType <em>Guard Type</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityEdgeImpl#getGuardExpression <em>Guard Expression</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityEdgeImpl#getReleaseEdges <em>Release Edges</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.impl.ActivityEdgeImpl#getAcquireEdges <em>Acquire Edges</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActivityEdgeImpl extends NamedElementImpl implements ActivityEdge {
	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected ActivityNode source;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected ActivityNode target;

	/**
	 * The default value of the '{@link #getGuardType() <em>Guard Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getGuardType()
	 * @generated
	 * @ordered
	 */
	protected static final ActivityEdgeGuardEnumeration GUARD_TYPE_EDEFAULT = ActivityEdgeGuardEnumeration.NONE;

	/**
	 * The cached value of the '{@link #getGuardType() <em>Guard Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getGuardType()
	 * @generated
	 * @ordered
	 */
	protected ActivityEdgeGuardEnumeration guardType = GUARD_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getGuardExpression() <em>Guard Expression</em>}' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getGuardExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression guardExpression;

	/**
	 * The cached value of the '{@link #getReleaseEdges() <em>Release Edges</em>}' reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getReleaseEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<ReleaseSemaphoreEdge> releaseEdges;

	/**
	 * The cached value of the '{@link #getAcquireEdges() <em>Acquire Edges</em>}' reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getAcquireEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<AcquireSemaphoreEdge> acquireEdges;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityEdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NodesPackage.Literals.ACTIVITY_EDGE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityNode getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject) source;
			source = (ActivityNode) eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							NodesPackage.ACTIVITY_EDGE__SOURCE, oldSource,
							source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityNode basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSource(ActivityNode newSource,
			NotificationChain msgs) {
		ActivityNode oldSource = source;
		source = newSource;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, NodesPackage.ACTIVITY_EDGE__SOURCE,
					oldSource, newSource);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(ActivityNode newSource) {
		if (newSource != source) {
			NotificationChain msgs = null;
			if (source != null)
				msgs = ((InternalEObject) source).eInverseRemove(this,
						NodesPackage.ACTIVITY_NODE__OUTGOING,
						ActivityNode.class, msgs);
			if (newSource != null)
				msgs = ((InternalEObject) newSource).eInverseAdd(this,
						NodesPackage.ACTIVITY_NODE__OUTGOING,
						ActivityNode.class, msgs);
			msgs = basicSetSource(newSource, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					NodesPackage.ACTIVITY_EDGE__SOURCE, newSource, newSource));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityNode getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject) target;
			target = (ActivityNode) eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							NodesPackage.ACTIVITY_EDGE__TARGET, oldTarget,
							target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityNode basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTarget(ActivityNode newTarget,
			NotificationChain msgs) {
		ActivityNode oldTarget = target;
		target = newTarget;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, NodesPackage.ACTIVITY_EDGE__TARGET,
					oldTarget, newTarget);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(ActivityNode newTarget) {
		if (newTarget != target) {
			NotificationChain msgs = null;
			if (target != null)
				msgs = ((InternalEObject) target).eInverseRemove(this,
						NodesPackage.ACTIVITY_NODE__INCOMING,
						ActivityNode.class, msgs);
			if (newTarget != null)
				msgs = ((InternalEObject) newTarget).eInverseAdd(this,
						NodesPackage.ACTIVITY_NODE__INCOMING,
						ActivityNode.class, msgs);
			msgs = basicSetTarget(newTarget, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					NodesPackage.ACTIVITY_EDGE__TARGET, newTarget, newTarget));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Activity getActivity() {
		if (eContainerFeatureID() != NodesPackage.ACTIVITY_EDGE__ACTIVITY)
			return null;
		return (Activity) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActivity(Activity newActivity,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newActivity,
				NodesPackage.ACTIVITY_EDGE__ACTIVITY, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivity(Activity newActivity) {
		if (newActivity != eInternalContainer()
				|| (eContainerFeatureID() != NodesPackage.ACTIVITY_EDGE__ACTIVITY && newActivity != null)) {
			if (EcoreUtil.isAncestor(this, newActivity))
				throw new IllegalArgumentException(
						"Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newActivity != null)
				msgs = ((InternalEObject) newActivity).eInverseAdd(this,
						StoryDiagramEcorePackage.ACTIVITY__EDGES,
						Activity.class, msgs);
			msgs = basicSetActivity(newActivity, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					NodesPackage.ACTIVITY_EDGE__ACTIVITY, newActivity,
					newActivity));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityEdgeGuardEnumeration getGuardType() {
		return guardType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setGuardType(ActivityEdgeGuardEnumeration newGuardType) {
		ActivityEdgeGuardEnumeration oldGuardType = guardType;
		guardType = newGuardType == null ? GUARD_TYPE_EDEFAULT : newGuardType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					NodesPackage.ACTIVITY_EDGE__GUARD_TYPE, oldGuardType,
					guardType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getGuardExpression() {
		return guardExpression;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGuardExpression(
			Expression newGuardExpression, NotificationChain msgs) {
		Expression oldGuardExpression = guardExpression;
		guardExpression = newGuardExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					NodesPackage.ACTIVITY_EDGE__GUARD_EXPRESSION,
					oldGuardExpression, newGuardExpression);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setGuardExpression(Expression newGuardExpression) {
		if (newGuardExpression != guardExpression) {
			NotificationChain msgs = null;
			if (guardExpression != null)
				msgs = ((InternalEObject) guardExpression).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- NodesPackage.ACTIVITY_EDGE__GUARD_EXPRESSION,
						null, msgs);
			if (newGuardExpression != null)
				msgs = ((InternalEObject) newGuardExpression).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- NodesPackage.ACTIVITY_EDGE__GUARD_EXPRESSION,
						null, msgs);
			msgs = basicSetGuardExpression(newGuardExpression, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					NodesPackage.ACTIVITY_EDGE__GUARD_EXPRESSION,
					newGuardExpression, newGuardExpression));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReleaseSemaphoreEdge> getReleaseEdges() {
		if (releaseEdges == null) {
			releaseEdges = new EObjectWithInverseResolvingEList<ReleaseSemaphoreEdge>(
					ReleaseSemaphoreEdge.class, this,
					NodesPackage.ACTIVITY_EDGE__RELEASE_EDGES,
					NodesPackage.RELEASE_SEMAPHORE_EDGE__ACTIVITY_EDGE);
		}
		return releaseEdges;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AcquireSemaphoreEdge> getAcquireEdges() {
		if (acquireEdges == null) {
			acquireEdges = new EObjectWithInverseResolvingEList<AcquireSemaphoreEdge>(
					AcquireSemaphoreEdge.class, this,
					NodesPackage.ACTIVITY_EDGE__ACQUIRE_EDGES,
					NodesPackage.ACQUIRE_SEMAPHORE_EDGE__ACTIVITY_EDGE);
		}
		return acquireEdges;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case NodesPackage.ACTIVITY_EDGE__SOURCE:
			if (source != null)
				msgs = ((InternalEObject) source).eInverseRemove(this,
						NodesPackage.ACTIVITY_NODE__OUTGOING,
						ActivityNode.class, msgs);
			return basicSetSource((ActivityNode) otherEnd, msgs);
		case NodesPackage.ACTIVITY_EDGE__TARGET:
			if (target != null)
				msgs = ((InternalEObject) target).eInverseRemove(this,
						NodesPackage.ACTIVITY_NODE__INCOMING,
						ActivityNode.class, msgs);
			return basicSetTarget((ActivityNode) otherEnd, msgs);
		case NodesPackage.ACTIVITY_EDGE__ACTIVITY:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetActivity((Activity) otherEnd, msgs);
		case NodesPackage.ACTIVITY_EDGE__RELEASE_EDGES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getReleaseEdges())
					.basicAdd(otherEnd, msgs);
		case NodesPackage.ACTIVITY_EDGE__ACQUIRE_EDGES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getAcquireEdges())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case NodesPackage.ACTIVITY_EDGE__SOURCE:
			return basicSetSource(null, msgs);
		case NodesPackage.ACTIVITY_EDGE__TARGET:
			return basicSetTarget(null, msgs);
		case NodesPackage.ACTIVITY_EDGE__ACTIVITY:
			return basicSetActivity(null, msgs);
		case NodesPackage.ACTIVITY_EDGE__GUARD_EXPRESSION:
			return basicSetGuardExpression(null, msgs);
		case NodesPackage.ACTIVITY_EDGE__RELEASE_EDGES:
			return ((InternalEList<?>) getReleaseEdges()).basicRemove(otherEnd,
					msgs);
		case NodesPackage.ACTIVITY_EDGE__ACQUIRE_EDGES:
			return ((InternalEList<?>) getAcquireEdges()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(
			NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case NodesPackage.ACTIVITY_EDGE__ACTIVITY:
			return eInternalContainer().eInverseRemove(this,
					StoryDiagramEcorePackage.ACTIVITY__EDGES, Activity.class,
					msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case NodesPackage.ACTIVITY_EDGE__SOURCE:
			if (resolve)
				return getSource();
			return basicGetSource();
		case NodesPackage.ACTIVITY_EDGE__TARGET:
			if (resolve)
				return getTarget();
			return basicGetTarget();
		case NodesPackage.ACTIVITY_EDGE__ACTIVITY:
			return getActivity();
		case NodesPackage.ACTIVITY_EDGE__GUARD_TYPE:
			return getGuardType();
		case NodesPackage.ACTIVITY_EDGE__GUARD_EXPRESSION:
			return getGuardExpression();
		case NodesPackage.ACTIVITY_EDGE__RELEASE_EDGES:
			return getReleaseEdges();
		case NodesPackage.ACTIVITY_EDGE__ACQUIRE_EDGES:
			return getAcquireEdges();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case NodesPackage.ACTIVITY_EDGE__SOURCE:
			setSource((ActivityNode) newValue);
			return;
		case NodesPackage.ACTIVITY_EDGE__TARGET:
			setTarget((ActivityNode) newValue);
			return;
		case NodesPackage.ACTIVITY_EDGE__ACTIVITY:
			setActivity((Activity) newValue);
			return;
		case NodesPackage.ACTIVITY_EDGE__GUARD_TYPE:
			setGuardType((ActivityEdgeGuardEnumeration) newValue);
			return;
		case NodesPackage.ACTIVITY_EDGE__GUARD_EXPRESSION:
			setGuardExpression((Expression) newValue);
			return;
		case NodesPackage.ACTIVITY_EDGE__RELEASE_EDGES:
			getReleaseEdges().clear();
			getReleaseEdges().addAll(
					(Collection<? extends ReleaseSemaphoreEdge>) newValue);
			return;
		case NodesPackage.ACTIVITY_EDGE__ACQUIRE_EDGES:
			getAcquireEdges().clear();
			getAcquireEdges().addAll(
					(Collection<? extends AcquireSemaphoreEdge>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case NodesPackage.ACTIVITY_EDGE__SOURCE:
			setSource((ActivityNode) null);
			return;
		case NodesPackage.ACTIVITY_EDGE__TARGET:
			setTarget((ActivityNode) null);
			return;
		case NodesPackage.ACTIVITY_EDGE__ACTIVITY:
			setActivity((Activity) null);
			return;
		case NodesPackage.ACTIVITY_EDGE__GUARD_TYPE:
			setGuardType(GUARD_TYPE_EDEFAULT);
			return;
		case NodesPackage.ACTIVITY_EDGE__GUARD_EXPRESSION:
			setGuardExpression((Expression) null);
			return;
		case NodesPackage.ACTIVITY_EDGE__RELEASE_EDGES:
			getReleaseEdges().clear();
			return;
		case NodesPackage.ACTIVITY_EDGE__ACQUIRE_EDGES:
			getAcquireEdges().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case NodesPackage.ACTIVITY_EDGE__SOURCE:
			return source != null;
		case NodesPackage.ACTIVITY_EDGE__TARGET:
			return target != null;
		case NodesPackage.ACTIVITY_EDGE__ACTIVITY:
			return getActivity() != null;
		case NodesPackage.ACTIVITY_EDGE__GUARD_TYPE:
			return guardType != GUARD_TYPE_EDEFAULT;
		case NodesPackage.ACTIVITY_EDGE__GUARD_EXPRESSION:
			return guardExpression != null;
		case NodesPackage.ACTIVITY_EDGE__RELEASE_EDGES:
			return releaseEdges != null && !releaseEdges.isEmpty();
		case NodesPackage.ACTIVITY_EDGE__ACQUIRE_EDGES:
			return acquireEdges != null && !acquireEdges.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (guardType: ");
		result.append(guardType);
		result.append(')');
		return result.toString();
	}

} // ActivityEdgeImpl
