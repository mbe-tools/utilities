/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '
 * <em><b>For Each Semantics Enumeration</b></em>', and utility methods for
 * working with them. <!-- end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getForEachSemanticsEnumeration()
 * @model
 * @generated
 */
public enum ForEachSemanticsEnumeration implements Enumerator {
	/**
	 * The '<em><b>FRESH MATCH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FRESH_MATCH_VALUE
	 * @generated
	 * @ordered
	 */
	FRESH_MATCH(0, "FRESH_MATCH", "FRESH_MATCH"),

	/**
	 * The '<em><b>PRE SELECT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PRE_SELECT_VALUE
	 * @generated
	 * @ordered
	 */
	PRE_SELECT(1, "PRE_SELECT", "PRE_SELECT"),

	/**
	 * The '<em><b>INTERLEAVED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTERLEAVED_VALUE
	 * @generated
	 * @ordered
	 */
	INTERLEAVED(2, "INTERLEAVED", "INTERLEAVED");

	/**
	 * The '<em><b>FRESH MATCH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FRESH MATCH</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FRESH_MATCH
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FRESH_MATCH_VALUE = 0;

	/**
	 * The '<em><b>PRE SELECT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PRE SELECT</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PRE_SELECT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PRE_SELECT_VALUE = 1;

	/**
	 * The '<em><b>INTERLEAVED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INTERLEAVED</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTERLEAVED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INTERLEAVED_VALUE = 2;

	/**
	 * An array of all the '<em><b>For Each Semantics Enumeration</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static final ForEachSemanticsEnumeration[] VALUES_ARRAY = new ForEachSemanticsEnumeration[] {
			FRESH_MATCH, PRE_SELECT, INTERLEAVED, };

	/**
	 * A public read-only list of all the '
	 * <em><b>For Each Semantics Enumeration</b></em>' enumerators. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final List<ForEachSemanticsEnumeration> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>For Each Semantics Enumeration</b></em>' literal with
	 * the specified literal value. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	public static ForEachSemanticsEnumeration get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ForEachSemanticsEnumeration result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>For Each Semantics Enumeration</b></em>' literal with the specified name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static ForEachSemanticsEnumeration getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ForEachSemanticsEnumeration result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>For Each Semantics Enumeration</b></em>' literal with
	 * the specified integer value. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	public static ForEachSemanticsEnumeration get(int value) {
		switch (value) {
		case FRESH_MATCH_VALUE:
			return FRESH_MATCH;
		case PRE_SELECT_VALUE:
			return PRE_SELECT;
		case INTERLEAVED_VALUE:
			return INTERLEAVED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	private ForEachSemanticsEnumeration(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} // ForEachSemanticsEnumeration
