/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.impl;

import org.eclipse.emf.ecore.EClass;

import de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Flow Final Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class FlowFinalNodeImpl extends ActivityNodeImpl implements
		FlowFinalNode {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected FlowFinalNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NodesPackage.Literals.FLOW_FINAL_NODE;
	}

} // FlowFinalNodeImpl
