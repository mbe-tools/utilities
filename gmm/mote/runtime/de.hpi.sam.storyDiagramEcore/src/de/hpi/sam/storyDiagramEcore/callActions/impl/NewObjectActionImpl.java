/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>New Object Action</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.NewObjectActionImpl#getConstructorParameters <em>Constructor Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NewObjectActionImpl extends CallActionImpl implements
		NewObjectAction {
	/**
	 * The cached value of the '{@link #getConstructorParameters()
	 * <em>Constructor Parameters</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getConstructorParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<CallActionParameter> constructorParameters;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected NewObjectActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CallActionsPackage.Literals.NEW_OBJECT_ACTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CallActionParameter> getConstructorParameters() {
		if (constructorParameters == null) {
			constructorParameters = new EObjectContainmentEList<CallActionParameter>(
					CallActionParameter.class,
					this,
					CallActionsPackage.NEW_OBJECT_ACTION__CONSTRUCTOR_PARAMETERS);
		}
		return constructorParameters;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer buffer = new StringBuffer();
		buffer.append("new ");

		if (getClassifier() != null && getClassifier().getName() != null
				&& !"".equals(getClassifier().getName())) {
			buffer.append(getClassifier().getName());
		} else {
			buffer.append("[null]");
		}

		buffer.append("(");

		for (CallActionParameter param : getConstructorParameters()) {
			buffer.append(param.toString());
		}

		buffer.append(")");

		return buffer.toString().trim();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CallActionsPackage.NEW_OBJECT_ACTION__CONSTRUCTOR_PARAMETERS:
			return ((InternalEList<?>) getConstructorParameters()).basicRemove(
					otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CallActionsPackage.NEW_OBJECT_ACTION__CONSTRUCTOR_PARAMETERS:
			return getConstructorParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CallActionsPackage.NEW_OBJECT_ACTION__CONSTRUCTOR_PARAMETERS:
			getConstructorParameters().clear();
			getConstructorParameters().addAll(
					(Collection<? extends CallActionParameter>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CallActionsPackage.NEW_OBJECT_ACTION__CONSTRUCTOR_PARAMETERS:
			getConstructorParameters().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CallActionsPackage.NEW_OBJECT_ACTION__CONSTRUCTOR_PARAMETERS:
			return constructorParameters != null
					&& !constructorParameters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // NewObjectActionImpl
