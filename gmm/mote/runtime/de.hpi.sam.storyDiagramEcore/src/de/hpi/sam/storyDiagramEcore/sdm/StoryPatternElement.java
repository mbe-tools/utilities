/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm;

import de.hpi.sam.storyDiagramEcore.NamedElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Story Pattern Element</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The abstract super class for all story pattern elements (story pattern objects and links).
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement#getModifier <em>Modifier</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement#getMatchType <em>Match Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternElement()
 * @model abstract="true"
 * @generated
 */
public interface StoryPatternElement extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Modifier</b></em>' attribute. The
	 * default value is <code>"NULL"</code>. The literals are from the
	 * enumeration
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration}.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * The modifier indicates whether this element is to be created, destroyed,
	 * etc. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Modifier</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration
	 * @see #setModifier(StoryPatternModifierEnumeration)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternElement_Modifier()
	 * @model default="NULL" required="true"
	 * @generated
	 */
	StoryPatternModifierEnumeration getModifier();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement#getModifier
	 * <em>Modifier</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Modifier</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration
	 * @see #getModifier()
	 * @generated
	 */
	void setModifier(StoryPatternModifierEnumeration value);

	/**
	 * Returns the value of the '<em><b>Match Type</b></em>' attribute. The
	 * literals are from the enumeration
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration}
	 * . <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Indicates how the story pattern element should be matched. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Match Type</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration
	 * @see #setMatchType(StoryPatternMatchTypeEnumeration)
	 * @see de.hpi.sam.storyDiagramEcore.sdm.SdmPackage#getStoryPatternElement_MatchType()
	 * @model required="true"
	 * @generated
	 */
	StoryPatternMatchTypeEnumeration getMatchType();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternElement#getMatchType <em>Match Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Match Type</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration
	 * @see #getMatchType()
	 * @generated
	 */
	void setMatchType(StoryPatternMatchTypeEnumeration value);

} // StoryPatternElement
