/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.impl;

import org.eclipse.emf.ecore.EClass;

import de.hpi.sam.storyDiagramEcore.nodes.JoinNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Join Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class JoinNodeImpl extends ActivityNodeImpl implements JoinNode {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected JoinNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NodesPackage.Literals.JOIN_NODE;
	}

} // JoinNodeImpl
