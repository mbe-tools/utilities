/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.sdm.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Link Position Constraint</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.LinkPositionConstraintImpl#getConstraintType <em>Constraint Type</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.sdm.impl.LinkPositionConstraintImpl#getStoryPatternLink <em>Story Pattern Link</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LinkPositionConstraintImpl extends EObjectImpl implements
		LinkPositionConstraint {
	/**
	 * The default value of the '{@link #getConstraintType() <em>Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getConstraintType()
	 * @generated
	 * @ordered
	 */
	protected static final LinkPositionConstraintEnumeration CONSTRAINT_TYPE_EDEFAULT = LinkPositionConstraintEnumeration.FIRST;

	/**
	 * The cached value of the '{@link #getConstraintType() <em>Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getConstraintType()
	 * @generated
	 * @ordered
	 */
	protected LinkPositionConstraintEnumeration constraintType = CONSTRAINT_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkPositionConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SdmPackage.Literals.LINK_POSITION_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LinkPositionConstraintEnumeration getConstraintType() {
		return constraintType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraintType(
			LinkPositionConstraintEnumeration newConstraintType) {
		LinkPositionConstraintEnumeration oldConstraintType = constraintType;
		constraintType = newConstraintType == null ? CONSTRAINT_TYPE_EDEFAULT
				: newConstraintType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE,
					oldConstraintType, constraintType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */

	public StoryPatternLink getStoryPatternLink() {
		if (eContainerFeatureID() != SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK)
			return null;
		return (StoryPatternLink) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */

	public NotificationChain basicSetStoryPatternLink(
			StoryPatternLink newStoryPatternLink, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newStoryPatternLink,
				SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setStoryPatternLink(StoryPatternLink newStoryPatternLink) {
		if (newStoryPatternLink != eInternalContainer()
				|| (eContainerFeatureID() != SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK && newStoryPatternLink != null)) {
			if (EcoreUtil.isAncestor(this, newStoryPatternLink))
				throw new IllegalArgumentException(
						"Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newStoryPatternLink != null)
				msgs = ((InternalEObject) newStoryPatternLink)
						.eInverseAdd(
								this,
								SdmPackage.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT,
								StoryPatternLink.class, msgs);
			msgs = basicSetStoryPatternLink(newStoryPatternLink, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK,
					newStoryPatternLink, newStoryPatternLink));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetStoryPatternLink((StoryPatternLink) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK:
			return basicSetStoryPatternLink(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(
			NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK:
			return eInternalContainer().eInverseRemove(this,
					SdmPackage.STORY_PATTERN_LINK__LINK_POSITION_CONSTRAINT,
					StoryPatternLink.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SdmPackage.LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE:
			return getConstraintType();
		case SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK:
			return getStoryPatternLink();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SdmPackage.LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE:
			setConstraintType((LinkPositionConstraintEnumeration) newValue);
			return;
		case SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK:
			setStoryPatternLink((StoryPatternLink) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SdmPackage.LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE:
			setConstraintType(CONSTRAINT_TYPE_EDEFAULT);
			return;
		case SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK:
			setStoryPatternLink((StoryPatternLink) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SdmPackage.LINK_POSITION_CONSTRAINT__CONSTRAINT_TYPE:
			return constraintType != CONSTRAINT_TYPE_EDEFAULT;
		case SdmPackage.LINK_POSITION_CONSTRAINT__STORY_PATTERN_LINK:
			return getStoryPatternLink() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (constraintType: ");
		result.append(constraintType);
		result.append(')');
		return result.toString();
	}

} // LinkPositionConstraintImpl
