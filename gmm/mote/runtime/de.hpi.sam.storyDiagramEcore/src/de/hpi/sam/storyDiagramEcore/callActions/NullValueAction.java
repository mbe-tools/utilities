/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Null Value Action</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The NullValueAction returns the value 'null' when it is evaluated.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#getNullValueAction()
 * @model
 * @generated
 */
public interface NullValueAction extends CallAction {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (eIsProxy()) return super.toString();\r\n\t\t\r\nreturn \"null\";'"
	 * @generated
	 */
	String toString();
} // NullValueAction
