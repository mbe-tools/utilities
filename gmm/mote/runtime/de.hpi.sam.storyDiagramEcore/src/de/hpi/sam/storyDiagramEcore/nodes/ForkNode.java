/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Fork Node</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A ForkNode splits a single control flow into multiple control flows.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getForkNode()
 * @model
 * @generated
 */
public interface ForkNode extends ActivityNode {
} // ForkNode
