/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore;

import org.eclipse.emf.ecore.EClassifier;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Activity Parameter</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.ActivityParameter#getType <em>Type</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.ActivityParameter#getDirection <em>Direction</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getActivityParameter()
 * @model
 * @generated
 */
public interface ActivityParameter extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(EClassifier)
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getActivityParameter_Type()
	 * @model required="true"
	 * @generated
	 */
	EClassifier getType();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.ActivityParameter#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(EClassifier value);

	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute. The
	 * default value is <code>"IN"</code>. The literals are from the enumeration
	 * {@link de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum
	 * @see #setDirection(ActivityParameterDirectionEnum)
	 * @see de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage#getActivityParameter_Direction()
	 * @model default="IN" required="true"
	 * @generated
	 */
	ActivityParameterDirectionEnum getDirection();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.ActivityParameter#getDirection
	 * <em>Direction</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Direction</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(ActivityParameterDirectionEnum value);

} // ActivityParameter
