/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.ActivityParameter;
import de.hpi.sam.storyDiagramEcore.Import;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.Semaphore;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Activity</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.impl.ActivityImpl#getNodes <em>Nodes</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.impl.ActivityImpl#getSpecification <em>Specification</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.impl.ActivityImpl#getEdges <em>Edges</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.impl.ActivityImpl#getSemaphores <em>Semaphores</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.impl.ActivityImpl#getImports <em>Imports</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.impl.ActivityImpl#getParameters <em>Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActivityImpl extends NamedElementImpl implements Activity {
	/**
	 * The cached value of the '{@link #getNodes() <em>Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<ActivityNode> nodes;

	/**
	 * The cached value of the '{@link #getSpecification() <em>Specification</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSpecification()
	 * @generated
	 * @ordered
	 */
	protected EOperation specification;

	/**
	 * The cached value of the '{@link #getEdges() <em>Edges</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<ActivityEdge> edges;

	/**
	 * The cached value of the '{@link #getSemaphores() <em>Semaphores</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSemaphores()
	 * @generated
	 * @ordered
	 */
	protected EList<Semaphore> semaphores;

	/**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
	protected EList<Import> imports;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ActivityParameter> parameters;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StoryDiagramEcorePackage.Literals.ACTIVITY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActivityNode> getNodes() {
		if (nodes == null) {
			nodes = new EObjectContainmentWithInverseEList<ActivityNode>(
					ActivityNode.class, this,
					StoryDiagramEcorePackage.ACTIVITY__NODES,
					NodesPackage.ACTIVITY_NODE__ACTIVITY);
		}
		return nodes;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSpecification() {
		if (specification != null && specification.eIsProxy()) {
			InternalEObject oldSpecification = (InternalEObject) specification;
			specification = (EOperation) eResolveProxy(oldSpecification);
			if (specification != oldSpecification) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							StoryDiagramEcorePackage.ACTIVITY__SPECIFICATION,
							oldSpecification, specification));
			}
		}
		return specification;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation basicGetSpecification() {
		return specification;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecification(EOperation newSpecification) {
		EOperation oldSpecification = specification;
		specification = newSpecification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					StoryDiagramEcorePackage.ACTIVITY__SPECIFICATION,
					oldSpecification, specification));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActivityEdge> getEdges() {
		if (edges == null) {
			edges = new EObjectContainmentWithInverseEList<ActivityEdge>(
					ActivityEdge.class, this,
					StoryDiagramEcorePackage.ACTIVITY__EDGES,
					NodesPackage.ACTIVITY_EDGE__ACTIVITY);
		}
		return edges;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Semaphore> getSemaphores() {
		if (semaphores == null) {
			semaphores = new EObjectContainmentEList<Semaphore>(
					Semaphore.class, this,
					StoryDiagramEcorePackage.ACTIVITY__SEMAPHORES);
		}
		return semaphores;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Import> getImports() {
		if (imports == null) {
			imports = new EObjectContainmentEList<Import>(Import.class, this,
					StoryDiagramEcorePackage.ACTIVITY__IMPORTS);
		}
		return imports;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActivityParameter> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<ActivityParameter>(
					ActivityParameter.class, this,
					StoryDiagramEcorePackage.ACTIVITY__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case StoryDiagramEcorePackage.ACTIVITY__NODES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getNodes())
					.basicAdd(otherEnd, msgs);
		case StoryDiagramEcorePackage.ACTIVITY__EDGES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getEdges())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case StoryDiagramEcorePackage.ACTIVITY__NODES:
			return ((InternalEList<?>) getNodes()).basicRemove(otherEnd, msgs);
		case StoryDiagramEcorePackage.ACTIVITY__EDGES:
			return ((InternalEList<?>) getEdges()).basicRemove(otherEnd, msgs);
		case StoryDiagramEcorePackage.ACTIVITY__SEMAPHORES:
			return ((InternalEList<?>) getSemaphores()).basicRemove(otherEnd,
					msgs);
		case StoryDiagramEcorePackage.ACTIVITY__IMPORTS:
			return ((InternalEList<?>) getImports())
					.basicRemove(otherEnd, msgs);
		case StoryDiagramEcorePackage.ACTIVITY__PARAMETERS:
			return ((InternalEList<?>) getParameters()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case StoryDiagramEcorePackage.ACTIVITY__NODES:
			return getNodes();
		case StoryDiagramEcorePackage.ACTIVITY__SPECIFICATION:
			if (resolve)
				return getSpecification();
			return basicGetSpecification();
		case StoryDiagramEcorePackage.ACTIVITY__EDGES:
			return getEdges();
		case StoryDiagramEcorePackage.ACTIVITY__SEMAPHORES:
			return getSemaphores();
		case StoryDiagramEcorePackage.ACTIVITY__IMPORTS:
			return getImports();
		case StoryDiagramEcorePackage.ACTIVITY__PARAMETERS:
			return getParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case StoryDiagramEcorePackage.ACTIVITY__NODES:
			getNodes().clear();
			getNodes().addAll((Collection<? extends ActivityNode>) newValue);
			return;
		case StoryDiagramEcorePackage.ACTIVITY__SPECIFICATION:
			setSpecification((EOperation) newValue);
			return;
		case StoryDiagramEcorePackage.ACTIVITY__EDGES:
			getEdges().clear();
			getEdges().addAll((Collection<? extends ActivityEdge>) newValue);
			return;
		case StoryDiagramEcorePackage.ACTIVITY__SEMAPHORES:
			getSemaphores().clear();
			getSemaphores().addAll((Collection<? extends Semaphore>) newValue);
			return;
		case StoryDiagramEcorePackage.ACTIVITY__IMPORTS:
			getImports().clear();
			getImports().addAll((Collection<? extends Import>) newValue);
			return;
		case StoryDiagramEcorePackage.ACTIVITY__PARAMETERS:
			getParameters().clear();
			getParameters().addAll(
					(Collection<? extends ActivityParameter>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case StoryDiagramEcorePackage.ACTIVITY__NODES:
			getNodes().clear();
			return;
		case StoryDiagramEcorePackage.ACTIVITY__SPECIFICATION:
			setSpecification((EOperation) null);
			return;
		case StoryDiagramEcorePackage.ACTIVITY__EDGES:
			getEdges().clear();
			return;
		case StoryDiagramEcorePackage.ACTIVITY__SEMAPHORES:
			getSemaphores().clear();
			return;
		case StoryDiagramEcorePackage.ACTIVITY__IMPORTS:
			getImports().clear();
			return;
		case StoryDiagramEcorePackage.ACTIVITY__PARAMETERS:
			getParameters().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case StoryDiagramEcorePackage.ACTIVITY__NODES:
			return nodes != null && !nodes.isEmpty();
		case StoryDiagramEcorePackage.ACTIVITY__SPECIFICATION:
			return specification != null;
		case StoryDiagramEcorePackage.ACTIVITY__EDGES:
			return edges != null && !edges.isEmpty();
		case StoryDiagramEcorePackage.ACTIVITY__SEMAPHORES:
			return semaphores != null && !semaphores.isEmpty();
		case StoryDiagramEcorePackage.ACTIVITY__IMPORTS:
			return imports != null && !imports.isEmpty();
		case StoryDiagramEcorePackage.ACTIVITY__PARAMETERS:
			return parameters != null && !parameters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // ActivityImpl
