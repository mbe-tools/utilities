/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

import org.eclipse.emf.common.util.EList;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Story Action Node</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A story action contains a story pattern that describes a graph transformation. A story action node can be marked forEach, which means that the story pattern is executed for all possible matches.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#isForEach <em>For Each</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getStoryPatternObjects <em>Story Pattern Objects</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getStoryPatternLinks <em>Story Pattern Links</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getNacSemantic <em>Nac Semantic</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getForEachSemantics <em>For Each Semantics</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getStoryActionNode()
 * @model
 * @generated
 */
public interface StoryActionNode extends ActivityNode {
	/**
	 * Returns the value of the '<em><b>For Each</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> Marks
	 * this story action node as forEach so the story pattern is executed as
	 * long as possible. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>For Each</em>' attribute.
	 * @see #setForEach(boolean)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getStoryActionNode_ForEach()
	 * @model
	 * @generated
	 */
	boolean isForEach();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#isForEach
	 * <em>For Each</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>For Each</em>' attribute.
	 * @see #isForEach()
	 * @generated
	 */
	void setForEach(boolean value);

	/**
	 * Returns the value of the '<em><b>Story Pattern Objects</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject}. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * story pattern objects contained in this story action node. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Story Pattern Objects</em>' containment
	 *         reference list.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getStoryActionNode_StoryPatternObjects()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractStoryPatternObject> getStoryPatternObjects();

	/**
	 * Returns the value of the '<em><b>Story Pattern Links</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink}. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * story pattern links contained in this story action node. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Story Pattern Links</em>' containment
	 *         reference list.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getStoryActionNode_StoryPatternLinks()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractStoryPatternLink> getStoryPatternLinks();

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link de.hpi.sam.storyDiagramEcore.expressions.Expression}. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> A list
	 * of constraint expressions that must hold for a successful match of the
	 * story action node. The constraint expressions are evaluated after a
	 * binding could be found for all story pattern objects and all story
	 * pattern links are checked successfully. If the constraint expressions are
	 * not satisfied, another binding is sought. The constraint expressions must
	 * return a boolean value. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Constraints</em>' containment reference
	 *         list.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getStoryActionNode_Constraints()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getConstraints();

	/**
	 * Returns the value of the '<em><b>Nac Semantic</b></em>' attribute. The
	 * default value is <code>"OR"</code>. The literals are from the enumeration
	 * {@link de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration}
	 * . <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * This attribute defines, how Negative Application Conditions should be
	 * interpreted. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Nac Semantic</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration
	 * @see #setNacSemantic(StoryPatternNACSematicEnumeration)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getStoryActionNode_NacSemantic()
	 * @model default="OR"
	 * @generated
	 */
	StoryPatternNACSematicEnumeration getNacSemantic();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getNacSemantic <em>Nac Semantic</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Nac Semantic</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.sdm.StoryPatternNACSematicEnumeration
	 * @see #getNacSemantic()
	 * @generated
	 */
	void setNacSemantic(StoryPatternNACSematicEnumeration value);

	/**
	 * Returns the value of the '<em><b>For Each Semantics</b></em>' attribute.
	 * The default value is <code>"FRESH_MATCH"</code>.
	 * The literals are from the enumeration {@link de.hpi.sam.storyDiagramEcore.nodes.ForEachSemanticsEnumeration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For Each Semantics</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For Each Semantics</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ForEachSemanticsEnumeration
	 * @see #setForEachSemantics(ForEachSemanticsEnumeration)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getStoryActionNode_ForEachSemantics()
	 * @model default="FRESH_MATCH"
	 * @generated
	 */
	ForEachSemanticsEnumeration getForEachSemantics();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode#getForEachSemantics <em>For Each Semantics</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>For Each Semantics</em>' attribute.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ForEachSemanticsEnumeration
	 * @see #getForEachSemantics()
	 * @generated
	 */
	void setForEachSemantics(ForEachSemanticsEnumeration value);

} // StoryActionNode
