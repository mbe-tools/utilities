/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.storyDiagramEcore.NamedElement;
import de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.DecisionNode;
import de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.ForkNode;
import de.hpi.sam.storyDiagramEcore.nodes.InitialNode;
import de.hpi.sam.storyDiagramEcore.nodes.JoinNode;
import de.hpi.sam.storyDiagramEcore.nodes.MergeNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge;
import de.hpi.sam.storyDiagramEcore.nodes.Semaphore;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides
 * an adapter <code>createXXX</code> method for each class of the model. <!--
 * end-user-doc -->
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage
 * @generated
 */
public class NodesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static NodesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public NodesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = NodesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc --> This implementation returns <code>true</code> if
	 * the object is either the model's package or is an instance object of the
	 * model. <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected NodesSwitch<Adapter> modelSwitch = new NodesSwitch<Adapter>() {
		@Override
		public Adapter caseActivityEdge(ActivityEdge object) {
			return createActivityEdgeAdapter();
		}

		@Override
		public Adapter caseActivityFinalNode(ActivityFinalNode object) {
			return createActivityFinalNodeAdapter();
		}

		@Override
		public Adapter caseFlowFinalNode(FlowFinalNode object) {
			return createFlowFinalNodeAdapter();
		}

		@Override
		public Adapter caseActivityNode(ActivityNode object) {
			return createActivityNodeAdapter();
		}

		@Override
		public Adapter caseExpressionActivityNode(ExpressionActivityNode object) {
			return createExpressionActivityNodeAdapter();
		}

		@Override
		public Adapter caseDecisionNode(DecisionNode object) {
			return createDecisionNodeAdapter();
		}

		@Override
		public Adapter caseInitialNode(InitialNode object) {
			return createInitialNodeAdapter();
		}

		@Override
		public Adapter caseMergeNode(MergeNode object) {
			return createMergeNodeAdapter();
		}

		@Override
		public Adapter caseStoryActionNode(StoryActionNode object) {
			return createStoryActionNodeAdapter();
		}

		@Override
		public Adapter caseForkNode(ForkNode object) {
			return createForkNodeAdapter();
		}

		@Override
		public Adapter caseJoinNode(JoinNode object) {
			return createJoinNodeAdapter();
		}

		@Override
		public Adapter caseSemaphore(Semaphore object) {
			return createSemaphoreAdapter();
		}

		@Override
		public Adapter caseSynchronizationEdge(SynchronizationEdge object) {
			return createSynchronizationEdgeAdapter();
		}

		@Override
		public Adapter caseReleaseSemaphoreEdge(ReleaseSemaphoreEdge object) {
			return createReleaseSemaphoreEdgeAdapter();
		}

		@Override
		public Adapter caseAcquireSemaphoreEdge(AcquireSemaphoreEdge object) {
			return createAcquireSemaphoreEdgeAdapter();
		}

		@Override
		public Adapter caseNamedElement(NamedElement object) {
			return createNamedElementAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge <em>Activity Edge</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge
	 * @generated
	 */
	public Adapter createActivityEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode <em>Activity Final Node</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode
	 * @generated
	 */
	public Adapter createActivityFinalNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode <em>Flow Final Node</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.FlowFinalNode
	 * @generated
	 */
	public Adapter createFlowFinalNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityNode <em>Activity Node</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityNode
	 * @generated
	 */
	public Adapter createActivityNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode <em>Expression Activity Node</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode
	 * @generated
	 */
	public Adapter createExpressionActivityNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.DecisionNode <em>Decision Node</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.DecisionNode
	 * @generated
	 */
	public Adapter createDecisionNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.InitialNode <em>Initial Node</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.InitialNode
	 * @generated
	 */
	public Adapter createInitialNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.MergeNode <em>Merge Node</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so
	 * that we can easily ignore cases; it's useful to ignore a case when
	 * inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.MergeNode
	 * @generated
	 */
	public Adapter createMergeNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode <em>Story Action Node</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode
	 * @generated
	 */
	public Adapter createStoryActionNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.ForkNode <em>Fork Node</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that
	 * we can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ForkNode
	 * @generated
	 */
	public Adapter createForkNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.JoinNode <em>Join Node</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that
	 * we can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.JoinNode
	 * @generated
	 */
	public Adapter createJoinNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.Semaphore <em>Semaphore</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that
	 * we can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.Semaphore
	 * @generated
	 */
	public Adapter createSemaphoreAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge <em>Synchronization Edge</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge
	 * @generated
	 */
	public Adapter createSynchronizationEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge <em>Release Semaphore Edge</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ReleaseSemaphoreEdge
	 * @generated
	 */
	public Adapter createReleaseSemaphoreEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge <em>Acquire Semaphore Edge</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge
	 * @generated
	 */
	public Adapter createAcquireSemaphoreEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.storyDiagramEcore.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so
	 * that we can easily ignore cases; it's useful to ignore a case when
	 * inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.storyDiagramEcore.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} // NodesAdapterFactory
