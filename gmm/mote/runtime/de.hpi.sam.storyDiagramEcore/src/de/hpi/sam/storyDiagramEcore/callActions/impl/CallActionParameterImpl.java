/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.impl.NamedElementImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Call Action Parameter</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionParameterImpl#getParameterValueAction <em>Parameter Value Action</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.callActions.impl.CallActionParameterImpl#getParameterClassfier <em>Parameter Classfier</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CallActionParameterImpl extends NamedElementImpl implements
		CallActionParameter {
	/**
	 * The cached value of the '{@link #getParameterValueAction()
	 * <em>Parameter Value Action</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getParameterValueAction()
	 * @generated
	 * @ordered
	 */
	protected Expression parameterValueAction;

	/**
	 * The cached value of the '{@link #getParameterClassfier() <em>Parameter Classfier</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getParameterClassfier()
	 * @generated
	 * @ordered
	 */
	protected EClassifier parameterClassfier;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected CallActionParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CallActionsPackage.Literals.CALL_ACTION_PARAMETER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getParameterValueAction() {
		return parameterValueAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterValueAction(
			Expression newParameterValueAction, NotificationChain msgs) {
		Expression oldParameterValueAction = parameterValueAction;
		parameterValueAction = newParameterValueAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
					this,
					Notification.SET,
					CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION,
					oldParameterValueAction, newParameterValueAction);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterValueAction(Expression newParameterValueAction) {
		if (newParameterValueAction != parameterValueAction) {
			NotificationChain msgs = null;
			if (parameterValueAction != null)
				msgs = ((InternalEObject) parameterValueAction)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION,
								null, msgs);
			if (newParameterValueAction != null)
				msgs = ((InternalEObject) newParameterValueAction)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION,
								null, msgs);
			msgs = basicSetParameterValueAction(newParameterValueAction, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION,
					newParameterValueAction, newParameterValueAction));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier getParameterClassfier() {
		if (parameterClassfier != null && parameterClassfier.eIsProxy()) {
			InternalEObject oldParameterClassfier = (InternalEObject) parameterClassfier;
			parameterClassfier = (EClassifier) eResolveProxy(oldParameterClassfier);
			if (parameterClassfier != oldParameterClassfier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_CLASSFIER,
							oldParameterClassfier, parameterClassfier));
			}
		}
		return parameterClassfier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier basicGetParameterClassfier() {
		return parameterClassfier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterClassfier(EClassifier newParameterClassfier) {
		EClassifier oldParameterClassfier = parameterClassfier;
		parameterClassfier = newParameterClassfier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_CLASSFIER,
					oldParameterClassfier, parameterClassfier));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer buffer = new StringBuffer();

		if (getParameterValueAction() == null) {
			buffer.append("[null]");
		} else {
			buffer.append(getParameterValueAction().toString());
		}

		return buffer.toString().trim();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION:
			return basicSetParameterValueAction(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION:
			return getParameterValueAction();
		case CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_CLASSFIER:
			if (resolve)
				return getParameterClassfier();
			return basicGetParameterClassfier();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION:
			setParameterValueAction((Expression) newValue);
			return;
		case CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_CLASSFIER:
			setParameterClassfier((EClassifier) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION:
			setParameterValueAction((Expression) null);
			return;
		case CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_CLASSFIER:
			setParameterClassfier((EClassifier) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION:
			return parameterValueAction != null;
		case CallActionsPackage.CALL_ACTION_PARAMETER__PARAMETER_CLASSFIER:
			return parameterClassfier != null;
		}
		return super.eIsSet(featureID);
	}

} // CallActionParameterImpl
