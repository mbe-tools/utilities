/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

import de.hpi.sam.storyDiagramEcore.NamedElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Synchronization Edge</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge#getSemaphore <em>Semaphore</em>}</li>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge#getWeight <em>Weight</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getSynchronizationEdge()
 * @model abstract="true"
 * @generated
 */
public interface SynchronizationEdge extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Semaphore</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.storyDiagramEcore.nodes.Semaphore#getSynchronizationEdges <em>Synchronization Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Semaphore</em>' container reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semaphore</em>' container reference.
	 * @see #setSemaphore(Semaphore)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getSynchronizationEdge_Semaphore()
	 * @see de.hpi.sam.storyDiagramEcore.nodes.Semaphore#getSynchronizationEdges
	 * @model opposite="synchronizationEdges" required="true" transient="false"
	 * @generated
	 */
	Semaphore getSemaphore();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge#getSemaphore <em>Semaphore</em>}' container reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Semaphore</em>' container reference.
	 * @see #getSemaphore()
	 * @generated
	 */
	void setSemaphore(Semaphore value);

	/**
	 * Returns the value of the '<em><b>Weight</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Weight</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Weight</em>' attribute.
	 * @see #setWeight(int)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getSynchronizationEdge_Weight()
	 * @model
	 * @generated
	 */
	int getWeight();

	/**
	 * Sets the value of the '
	 * {@link de.hpi.sam.storyDiagramEcore.nodes.SynchronizationEdge#getWeight
	 * <em>Weight</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Weight</em>' attribute.
	 * @see #getWeight()
	 * @generated
	 */
	void setWeight(int value);

} // SynchronizationEdge
