/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.storyDiagramEcore.nodes;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Acquire Semaphore Edge</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Connects an activity edge to a semaphore. Indicates that tokens are acquired
 * when the activity edge is executed. If not enough tokens are available, the execution
 * is blocked until enough tokens become available.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge#getActivityEdge <em>Activity Edge</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getAcquireSemaphoreEdge()
 * @model
 * @generated
 */
public interface AcquireSemaphoreEdge extends SynchronizationEdge {
	/**
	 * Returns the value of the '<em><b>Activity Edge</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getAcquireEdges <em>Acquire Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity Edge</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity Edge</em>' reference.
	 * @see #setActivityEdge(ActivityEdge)
	 * @see de.hpi.sam.storyDiagramEcore.nodes.NodesPackage#getAcquireSemaphoreEdge_ActivityEdge()
	 * @see de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge#getAcquireEdges
	 * @model opposite="acquireEdges" required="true"
	 * @generated
	 */
	ActivityEdge getActivityEdge();

	/**
	 * Sets the value of the '{@link de.hpi.sam.storyDiagramEcore.nodes.AcquireSemaphoreEdge#getActivityEdge <em>Activity Edge</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Activity Edge</em>' reference.
	 * @see #getActivityEdge()
	 * @generated
	 */
	void setActivityEdge(ActivityEdge value);

} // AcquireSemaphoreEdge
