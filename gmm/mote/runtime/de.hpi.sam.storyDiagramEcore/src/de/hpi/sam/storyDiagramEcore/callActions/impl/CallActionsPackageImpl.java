/**
 * <copyright> </copyright> $Id$
 */
package de.hpi.sam.storyDiagramEcore.callActions.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.storyDiagramEcore.callActions.AbstractComparator;
import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsFactory;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction;
import de.hpi.sam.storyDiagramEcore.callActions.CaseInsensitiveComparator;
import de.hpi.sam.storyDiagramEcore.callActions.CompareAction;
import de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction;
import de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction;
import de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction;
import de.hpi.sam.storyDiagramEcore.callActions.NullValueAction;
import de.hpi.sam.storyDiagramEcore.callActions.OperationAction;
import de.hpi.sam.storyDiagramEcore.callActions.Operators;
import de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsPackage;
import de.hpi.sam.storyDiagramEcore.expressions.impl.ExpressionsPackageImpl;
import de.hpi.sam.storyDiagramEcore.helpers.HelpersPackage;
import de.hpi.sam.storyDiagramEcore.helpers.impl.HelpersPackageImpl;
import de.hpi.sam.storyDiagramEcore.impl.StoryDiagramEcorePackageImpl;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.hpi.sam.storyDiagramEcore.nodes.impl.NodesPackageImpl;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.impl.SdmPackageImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class CallActionsPackageImpl extends EPackageImpl implements
		CallActionsPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callActionEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callActionParameterEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callStoryDiagramInterpreterActionEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass literalDeclarationActionEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass methodCallActionEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass newObjectActionEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableDeclarationActionEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableReferenceActionEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compareActionEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nullValueActionEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractComparatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass caseInsensitiveComparatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getPropertyValueActionEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum operatorsEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CallActionsPackageImpl() {
		super(eNS_URI, CallActionsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link CallActionsPackage#eINSTANCE}
	 * when that field is accessed. Clients should not invoke it directly.
	 * Instead, they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CallActionsPackage init() {
		if (isInited)
			return (CallActionsPackage) EPackage.Registry.INSTANCE
					.getEPackage(CallActionsPackage.eNS_URI);

		// Obtain or create and register package
		CallActionsPackageImpl theCallActionsPackage = (CallActionsPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof CallActionsPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new CallActionsPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		StoryDiagramEcorePackageImpl theStoryDiagramEcorePackage = (StoryDiagramEcorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI) instanceof StoryDiagramEcorePackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI)
				: StoryDiagramEcorePackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI)
				: ExpressionsPackage.eINSTANCE);
		NodesPackageImpl theNodesPackage = (NodesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(NodesPackage.eNS_URI) instanceof NodesPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(NodesPackage.eNS_URI) : NodesPackage.eINSTANCE);
		SdmPackageImpl theSdmPackage = (SdmPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(SdmPackage.eNS_URI) instanceof SdmPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(SdmPackage.eNS_URI) : SdmPackage.eINSTANCE);
		HelpersPackageImpl theHelpersPackage = (HelpersPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(HelpersPackage.eNS_URI) instanceof HelpersPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(HelpersPackage.eNS_URI) : HelpersPackage.eINSTANCE);

		// Create package meta-data objects
		theCallActionsPackage.createPackageContents();
		theStoryDiagramEcorePackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theNodesPackage.createPackageContents();
		theSdmPackage.createPackageContents();
		theHelpersPackage.createPackageContents();

		// Initialize created meta-data
		theCallActionsPackage.initializePackageContents();
		theStoryDiagramEcorePackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theNodesPackage.initializePackageContents();
		theSdmPackage.initializePackageContents();
		theHelpersPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCallActionsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CallActionsPackage.eNS_URI,
				theCallActionsPackage);
		return theCallActionsPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallAction() {
		return callActionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallAction_Classifier() {
		return (EReference) callActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallActionParameter() {
		return callActionParameterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallActionParameter_ParameterValueAction() {
		return (EReference) callActionParameterEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallActionParameter_ParameterClassfier() {
		return (EReference) callActionParameterEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallStoryDiagramInterpreterAction() {
		return callStoryDiagramInterpreterActionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallStoryDiagramInterpreterAction_Activity() {
		return (EReference) callStoryDiagramInterpreterActionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallStoryDiagramInterpreterAction_Parameters() {
		return (EReference) callStoryDiagramInterpreterActionEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLiteralDeclarationAction() {
		return literalDeclarationActionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLiteralDeclarationAction_Literal() {
		return (EAttribute) literalDeclarationActionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMethodCallAction() {
		return methodCallActionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethodCallAction_Parameters() {
		return (EReference) methodCallActionEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethodCallAction_Method() {
		return (EReference) methodCallActionEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethodCallAction_MethodClassName() {
		return (EAttribute) methodCallActionEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethodCallAction_MethodName() {
		return (EAttribute) methodCallActionEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethodCallAction_InstanceVariable() {
		return (EReference) methodCallActionEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNewObjectAction() {
		return newObjectActionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNewObjectAction_ConstructorParameters() {
		return (EReference) newObjectActionEClass.getEStructuralFeatures().get(
				0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariableDeclarationAction() {
		return variableDeclarationActionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariableDeclarationAction_ValueAssignment() {
		return (EReference) variableDeclarationActionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariableReferenceAction() {
		return variableReferenceActionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariableReferenceAction_VariableName() {
		return (EAttribute) variableReferenceActionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompareAction() {
		return compareActionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompareAction_Expression1() {
		return (EReference) compareActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompareAction_Expression2() {
		return (EReference) compareActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompareAction_Comparator() {
		return (EReference) compareActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNullValueAction() {
		return nullValueActionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationAction() {
		return operationActionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationAction_Operand1() {
		return (EReference) operationActionEClass.getEStructuralFeatures().get(
				0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationAction_Operand2() {
		return (EReference) operationActionEClass.getEStructuralFeatures().get(
				1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationAction_Operator() {
		return (EAttribute) operationActionEClass.getEStructuralFeatures().get(
				2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractComparator() {
		return abstractComparatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCaseInsensitiveComparator() {
		return caseInsensitiveComparatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetPropertyValueAction() {
		return getPropertyValueActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPropertyValueAction_InstanceVariable() {
		return (EReference) getPropertyValueActionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPropertyValueAction_Property() {
		return (EReference) getPropertyValueActionEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOperators() {
		return operatorsEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CallActionsFactory getCallActionsFactory() {
		return (CallActionsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		callActionEClass = createEClass(CALL_ACTION);
		createEReference(callActionEClass, CALL_ACTION__CLASSIFIER);

		callActionParameterEClass = createEClass(CALL_ACTION_PARAMETER);
		createEReference(callActionParameterEClass,
				CALL_ACTION_PARAMETER__PARAMETER_VALUE_ACTION);
		createEReference(callActionParameterEClass,
				CALL_ACTION_PARAMETER__PARAMETER_CLASSFIER);

		callStoryDiagramInterpreterActionEClass = createEClass(CALL_STORY_DIAGRAM_INTERPRETER_ACTION);
		createEReference(callStoryDiagramInterpreterActionEClass,
				CALL_STORY_DIAGRAM_INTERPRETER_ACTION__ACTIVITY);
		createEReference(callStoryDiagramInterpreterActionEClass,
				CALL_STORY_DIAGRAM_INTERPRETER_ACTION__PARAMETERS);

		literalDeclarationActionEClass = createEClass(LITERAL_DECLARATION_ACTION);
		createEAttribute(literalDeclarationActionEClass,
				LITERAL_DECLARATION_ACTION__LITERAL);

		methodCallActionEClass = createEClass(METHOD_CALL_ACTION);
		createEReference(methodCallActionEClass, METHOD_CALL_ACTION__PARAMETERS);
		createEReference(methodCallActionEClass, METHOD_CALL_ACTION__METHOD);
		createEAttribute(methodCallActionEClass,
				METHOD_CALL_ACTION__METHOD_CLASS_NAME);
		createEAttribute(methodCallActionEClass,
				METHOD_CALL_ACTION__METHOD_NAME);
		createEReference(methodCallActionEClass,
				METHOD_CALL_ACTION__INSTANCE_VARIABLE);

		newObjectActionEClass = createEClass(NEW_OBJECT_ACTION);
		createEReference(newObjectActionEClass,
				NEW_OBJECT_ACTION__CONSTRUCTOR_PARAMETERS);

		variableDeclarationActionEClass = createEClass(VARIABLE_DECLARATION_ACTION);
		createEReference(variableDeclarationActionEClass,
				VARIABLE_DECLARATION_ACTION__VALUE_ASSIGNMENT);

		variableReferenceActionEClass = createEClass(VARIABLE_REFERENCE_ACTION);
		createEAttribute(variableReferenceActionEClass,
				VARIABLE_REFERENCE_ACTION__VARIABLE_NAME);

		compareActionEClass = createEClass(COMPARE_ACTION);
		createEReference(compareActionEClass, COMPARE_ACTION__EXPRESSION1);
		createEReference(compareActionEClass, COMPARE_ACTION__EXPRESSION2);
		createEReference(compareActionEClass, COMPARE_ACTION__COMPARATOR);

		nullValueActionEClass = createEClass(NULL_VALUE_ACTION);

		operationActionEClass = createEClass(OPERATION_ACTION);
		createEReference(operationActionEClass, OPERATION_ACTION__OPERAND1);
		createEReference(operationActionEClass, OPERATION_ACTION__OPERAND2);
		createEAttribute(operationActionEClass, OPERATION_ACTION__OPERATOR);

		abstractComparatorEClass = createEClass(ABSTRACT_COMPARATOR);

		caseInsensitiveComparatorEClass = createEClass(CASE_INSENSITIVE_COMPARATOR);

		getPropertyValueActionEClass = createEClass(GET_PROPERTY_VALUE_ACTION);
		createEReference(getPropertyValueActionEClass,
				GET_PROPERTY_VALUE_ACTION__INSTANCE_VARIABLE);
		createEReference(getPropertyValueActionEClass,
				GET_PROPERTY_VALUE_ACTION__PROPERTY);

		// Create enums
		operatorsEEnum = createEEnum(OPERATORS);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StoryDiagramEcorePackage theStoryDiagramEcorePackage = (StoryDiagramEcorePackage) EPackage.Registry.INSTANCE
				.getEPackage(StoryDiagramEcorePackage.eNS_URI);
		ExpressionsPackage theExpressionsPackage = (ExpressionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		callActionEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		callActionParameterEClass.getESuperTypes().add(
				theStoryDiagramEcorePackage.getNamedElement());
		callStoryDiagramInterpreterActionEClass.getESuperTypes().add(
				this.getCallAction());
		literalDeclarationActionEClass.getESuperTypes().add(
				this.getCallAction());
		methodCallActionEClass.getESuperTypes().add(this.getCallAction());
		newObjectActionEClass.getESuperTypes().add(this.getCallAction());
		variableDeclarationActionEClass.getESuperTypes().add(
				this.getVariableReferenceAction());
		variableReferenceActionEClass.getESuperTypes()
				.add(this.getCallAction());
		compareActionEClass.getESuperTypes().add(this.getCallAction());
		nullValueActionEClass.getESuperTypes().add(this.getCallAction());
		operationActionEClass.getESuperTypes().add(this.getCallAction());
		caseInsensitiveComparatorEClass.getESuperTypes().add(
				this.getAbstractComparator());
		getPropertyValueActionEClass.getESuperTypes().add(this.getCallAction());

		// Initialize classes and features; add operations and parameters
		initEClass(callActionEClass, CallAction.class, "CallAction",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCallAction_Classifier(),
				ecorePackage.getEClassifier(), null, "classifier", null, 0, 1,
				CallAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(callActionParameterEClass, CallActionParameter.class,
				"CallActionParameter", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCallActionParameter_ParameterValueAction(),
				theExpressionsPackage.getExpression(), null,
				"parameterValueAction", null, 1, 1, CallActionParameter.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getCallActionParameter_ParameterClassfier(),
				ecorePackage.getEClassifier(), null, "parameterClassfier",
				null, 1, 1, CallActionParameter.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(callActionParameterEClass, ecorePackage.getEString(),
				"toString", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(callStoryDiagramInterpreterActionEClass,
				CallStoryDiagramInterpreterAction.class,
				"CallStoryDiagramInterpreterAction", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCallStoryDiagramInterpreterAction_Activity(),
				theStoryDiagramEcorePackage.getActivity(), null, "activity",
				null, 1, 1, CallStoryDiagramInterpreterAction.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getCallStoryDiagramInterpreterAction_Parameters(),
				this.getCallActionParameter(), null, "parameters", null, 0, -1,
				CallStoryDiagramInterpreterAction.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(callStoryDiagramInterpreterActionEClass,
				ecorePackage.getEString(), "toString", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(literalDeclarationActionEClass,
				LiteralDeclarationAction.class, "LiteralDeclarationAction",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLiteralDeclarationAction_Literal(),
				ecorePackage.getEString(), "literal", null, 1, 1,
				LiteralDeclarationAction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		addEOperation(literalDeclarationActionEClass,
				ecorePackage.getEString(), "toString", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(methodCallActionEClass, MethodCallAction.class,
				"MethodCallAction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMethodCallAction_Parameters(),
				this.getCallActionParameter(), null, "parameters", null, 0, -1,
				MethodCallAction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMethodCallAction_Method(),
				ecorePackage.getEOperation(), null, "method", null, 0, 1,
				MethodCallAction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMethodCallAction_MethodClassName(),
				ecorePackage.getEString(), "methodClassName", null, 0, 1,
				MethodCallAction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMethodCallAction_MethodName(),
				ecorePackage.getEString(), "methodName", null, 0, 1,
				MethodCallAction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMethodCallAction_InstanceVariable(),
				theExpressionsPackage.getExpression(), null,
				"instanceVariable", null, 0, 1, MethodCallAction.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		addEOperation(methodCallActionEClass, ecorePackage.getEString(),
				"toString", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(newObjectActionEClass, NewObjectAction.class,
				"NewObjectAction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNewObjectAction_ConstructorParameters(),
				this.getCallActionParameter(), null, "constructorParameters",
				null, 0, -1, NewObjectAction.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(newObjectActionEClass, ecorePackage.getEString(),
				"toString", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(variableDeclarationActionEClass,
				VariableDeclarationAction.class, "VariableDeclarationAction",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVariableDeclarationAction_ValueAssignment(),
				theExpressionsPackage.getExpression(), null, "valueAssignment",
				null, 1, 1, VariableDeclarationAction.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(variableDeclarationActionEClass,
				ecorePackage.getEString(), "toString", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(variableReferenceActionEClass,
				VariableReferenceAction.class, "VariableReferenceAction",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVariableReferenceAction_VariableName(),
				ecorePackage.getEString(), "variableName", null, 1, 1,
				VariableReferenceAction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		addEOperation(variableReferenceActionEClass, ecorePackage.getEString(),
				"toString", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(compareActionEClass, CompareAction.class, "CompareAction",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCompareAction_Expression1(),
				theExpressionsPackage.getExpression(), null, "expression1",
				null, 1, 1, CompareAction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCompareAction_Expression2(),
				theExpressionsPackage.getExpression(), null, "expression2",
				null, 1, 1, CompareAction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCompareAction_Comparator(),
				this.getAbstractComparator(), null, "comparator", null, 0, 1,
				CompareAction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(compareActionEClass, ecorePackage.getEString(),
				"toString", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(nullValueActionEClass, NullValueAction.class,
				"NullValueAction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		addEOperation(nullValueActionEClass, ecorePackage.getEString(),
				"toString", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(operationActionEClass, OperationAction.class,
				"OperationAction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationAction_Operand1(),
				theExpressionsPackage.getExpression(), null, "operand1", null,
				1, 1, OperationAction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationAction_Operand2(),
				theExpressionsPackage.getExpression(), null, "operand2", null,
				0, 1, OperationAction.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationAction_Operator(), this.getOperators(),
				"operator", null, 1, 1, OperationAction.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		addEOperation(operationActionEClass, ecorePackage.getEString(),
				"toString", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(abstractComparatorEClass, AbstractComparator.class,
				"AbstractComparator", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(abstractComparatorEClass,
				ecorePackage.getEBoolean(), "equals", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "object1", 1, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "object2", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(caseInsensitiveComparatorEClass,
				CaseInsensitiveComparator.class, "CaseInsensitiveComparator",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(getPropertyValueActionEClass, GetPropertyValueAction.class,
				"GetPropertyValueAction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGetPropertyValueAction_InstanceVariable(),
				theExpressionsPackage.getExpression(), null,
				"instanceVariable", null, 0, 1, GetPropertyValueAction.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getGetPropertyValueAction_Property(),
				ecorePackage.getEStructuralFeature(), null, "property", null,
				1, 1, GetPropertyValueAction.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(operatorsEEnum, Operators.class, "Operators");
		addEEnumLiteral(operatorsEEnum, Operators.EQUALS);
		addEEnumLiteral(operatorsEEnum, Operators.GREATER_THAN);
		addEEnumLiteral(operatorsEEnum, Operators.LESS_THAN);
		addEEnumLiteral(operatorsEEnum, Operators.GREATER_OR_EQUALS);
		addEEnumLiteral(operatorsEEnum, Operators.LESS_OR_EQUALS);
		addEEnumLiteral(operatorsEEnum, Operators.NOT_EQUAL);
		addEEnumLiteral(operatorsEEnum, Operators.NOT);
		addEEnumLiteral(operatorsEEnum, Operators.AND);
		addEEnumLiteral(operatorsEEnum, Operators.OR);
		addEEnumLiteral(operatorsEEnum, Operators.ADD);
		addEEnumLiteral(operatorsEEnum, Operators.SUBTRACT);
		addEEnumLiteral(operatorsEEnum, Operators.MULTIPLY);
		addEEnumLiteral(operatorsEEnum, Operators.DIVIDE);
		addEEnumLiteral(operatorsEEnum, Operators.MODULO);
	}

} // CallActionsPackageImpl
