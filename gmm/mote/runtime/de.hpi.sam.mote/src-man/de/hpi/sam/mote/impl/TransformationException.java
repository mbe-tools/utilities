package de.hpi.sam.mote.impl;

public class TransformationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final boolean systemic;


	public TransformationException( final String p_message ) {
		super( p_message );
		
		systemic = true;
	}

	public TransformationException( final String p_message,
									final boolean pb_systemic ) {
		super( p_message );
		
		systemic = pb_systemic;
	}

	public TransformationException(	final String message, 
									final Throwable p_nestedException ) {
		super( message, p_nestedException );

		systemic = true;
	}

	public TransformationException(	final Throwable p_nestedException ) {
		super( p_nestedException );

		systemic = true;
	}

	public boolean isSystemic() {
		return systemic;
	}
}
