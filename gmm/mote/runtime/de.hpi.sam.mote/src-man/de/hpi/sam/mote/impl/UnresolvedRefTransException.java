package de.hpi.sam.mote.impl;

import org.eclipse.emf.common.util.URI;

public class UnresolvedRefTransException extends TransformationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6350704409340516679L;
	
	private final URI refObjectUri;

	public UnresolvedRefTransException( final Throwable p_th ) {
		super( null, p_th );
		
		refObjectUri = null;
	}
	
	public UnresolvedRefTransException( final String p_message,
										final URI p_refObjectUri ) {
		super( p_message );
		
		refObjectUri = p_refObjectUri;
	}

	public URI getRefObjectUri() {
		return refObjectUri;
	}
}
