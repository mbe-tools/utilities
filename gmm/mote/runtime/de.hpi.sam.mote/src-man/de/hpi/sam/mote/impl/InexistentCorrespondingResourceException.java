package de.hpi.sam.mote.impl;

import org.eclipse.emf.ecore.resource.Resource;

public class InexistentCorrespondingResourceException extends TransformationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6485339490850738L;

	private final Resource inexistendRes;

	public InexistentCorrespondingResourceException( final Resource p_resource ) {
		super( (String) null );
		
		inexistendRes = p_resource;
	}

	
	public Resource getInexistendResource() {
		return inexistendRes;
	}
}
