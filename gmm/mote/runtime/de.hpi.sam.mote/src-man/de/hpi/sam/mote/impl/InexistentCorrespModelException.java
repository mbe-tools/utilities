package de.hpi.sam.mote.impl;

import org.eclipse.emf.ecore.resource.Resource;


public class InexistentCorrespModelException extends TransformationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5776859917812290285L;
	

	private final Resource leftResource;

	private final Resource rightResource;

	private final String transId;

	public InexistentCorrespModelException( final Resource p_leftResource,
											final Resource p_rightResource,
											final String p_transId,
											final String p_message ) {
		super( p_message );
		
		leftResource = p_leftResource;
		rightResource = p_rightResource;
		transId = p_transId;
	}
	
	public Resource getLeftResource() {
		return leftResource;
	}

	public Resource getRightResource() {
		return rightResource;
	}

	public String getTransId() {
		return transId;
	}
}
