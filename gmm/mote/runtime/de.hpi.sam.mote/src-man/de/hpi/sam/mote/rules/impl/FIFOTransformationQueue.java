package de.hpi.sam.mote.rules.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.helpers.ModificationTag;

/**
 * An implementation of TransformationQueue based on a fifo queue. The elements
 * are ordered by their insertion time.
 * 
 * @author Stephan
 */
public class FIFOTransformationQueue extends TransformationQueueImpl
{
	private List<ModificationTag>			queue	= new LinkedList<ModificationTag>();

	private Map<TGGNode, ModificationTag>	map		= new HashMap<TGGNode, ModificationTag>();

	@Override
	public void add(ModificationTag modificationTag)
	{
		TGGNode corrNode = modificationTag.getCorrespondenceNode();

		ModificationTag existingTag = map.get(corrNode);

		if (existingTag != null)
		{
			queue.remove(existingTag);
		}

		queue.add(modificationTag);

		map.put(corrNode, modificationTag);
		
		//System.out.println( "Executed rule " + modificationTag.getCorrespondenceNode() );
	}

	@Override
	public void clear()
	{
		queue.clear();
		map.clear();
	}

	@Override
	public EList<ModificationTag> get(TGGNode corrNode)
	{
		EList<ModificationTag> list = new BasicEList<ModificationTag>();

		ModificationTag tag = map.get(corrNode);

		if (tag != null)
		{
			list.add(tag);
		}

		return list;
	}

	@Override
	public boolean isEmpty()
	{
		return queue.isEmpty();
	}

	@Override
	public ModificationTag pop()
	{
		if (queue.isEmpty())
		{
			return null;
		}

		ModificationTag modificationTag = queue.remove(0);

		map.remove(modificationTag.getCorrespondenceNode());

		return modificationTag;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();

		for (ModificationTag tag : queue)
		{
			builder.append(tag.getCorrespondenceNode());
			builder.append("(");
			builder.append("synchronize: ");
			builder.append(tag.isSynchronize());
			builder.append(", ");
			builder.append(tag.getCorrespondenceNode().getSources().get(0));
			builder.append("); ");
		}

		return builder.toString();
	}

	@Override
	public EList<TGGNode> getElements()
	{
		EList<TGGNode> elements = new BasicEList<TGGNode>();

		for (ModificationTag mt : queue)
		{
			elements.add(mt.getCorrespondenceNode());
		}

		return elements;
	}

	@Override
	public int size()
	{
		return queue.size();
	}
}
