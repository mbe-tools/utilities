package de.hpi.sam.mote;

public class TGGConstants
{

	public static final String	MOTE_RULESET_EXTENSION__EXTENSION_NAME					= "de.hpi.sam.mote.ruleSetExtension";
	public static final String	MOTE_RULESET_EXTENSION__RULE_SET_NAME					= "name";
	public static final String	MOTE_RULESET_EXTENSION__RULE_SET_ID						= "id";
	public static final String	MOTE_RULESET_EXTENSION__PACKAGE_NS_URI					= "packageNsURI";
	public static final String	MOTE_RULESET_EXTENSION__SOURCE_MODEL_ID					= "sourceModelId";
	public static final String	MOTE_RULESET_EXTENSION__TARGET_MODEL_ID					= "targetModelId";

	public static final String	MOTE_DIAGRAM_ADAPTER_EXTENSION__EXTENSION_NAME			= "de.hpi.sam.mote.diagramAdapterExtension";
	public static final String	MOTE_DIAGRAM_ADAPTER_EXTENSION__DIAGRAM_ADAPTER_ID		= "id";
	public static final String	MOTE_DIAGRAM_ADAPTER_EXTENSION__PACKAGE_NS_URI			= "packageNsURI";
	public static final String	MOTE_DIAGRAM_ADAPTER_EXTENSION__MODEL_ID				= "modelId";
	public static final String	MOTE_DIAGRAM_ADAPTER_EXTENSION__CREATION_METHOD_NAME	= "creationMethodName";
}
