package de.hpi.sam.mote.helpers.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

public class SimpleReverseNavigationStoreImpl extends ReverseNavigationStoreImpl
{

	/*
	 * Maps a target element and the EReference pointing to it to the source
	 * elements (can be more than one). This assumes that there can be only one
	 * link of a specific type pointing from a source to a target object.
	 */
	private Map<EObject, Map<EReference, Set<EObject>>>	navigationMap	= new HashMap<EObject, Map<EReference, Set<EObject>>>();

	@Override
	public void addLink(EObject source, EObject target, EReference eReference)
	{
		Map<EReference, Set<EObject>> map = navigationMap.get(target);

		if (map == null)
		{
			map = new HashMap<EReference, Set<EObject>>();
			navigationMap.put(target, map);
		}

		Set<EObject> objects = map.get(eReference);
		
		if (objects == null)
		{
			objects = new HashSet<EObject>();
			map.put(eReference, objects);
		}
		
		objects.add(source);
	}

	@Override
	public void removeLink(EObject source, EObject target, EReference eReference)
	{
		Map<EReference, Set<EObject>> map = navigationMap.get(target);

		if (map == null)
		{
			map = new HashMap<EReference, Set<EObject>>();
			navigationMap.put(target, map);
			map.put(eReference, new HashSet<EObject>());
		}

		Set<EObject> objects = map.get(eReference);
		
		if (objects != null)
		{
			objects.remove(source);
		}
	}

	@Override
	public EList<EObject> getLinkSources(EObject target, EReference eReference)
	{
		Map<EReference, Set<EObject>> map = navigationMap.get(target);

		if (map == null)
		{
			map = new HashMap<EReference, Set<EObject>>();
			navigationMap.put(target, map);
			map.put(eReference, new HashSet<EObject>());

			return new BasicEList<EObject>();
		}

		return new BasicEList<EObject>(map.get(eReference));
	}

	@Override
	public void clear()
	{
		navigationMap.clear();
	}
}
