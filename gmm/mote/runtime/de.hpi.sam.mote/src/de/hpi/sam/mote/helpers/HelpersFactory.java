/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.helpers;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see de.hpi.sam.mote.helpers.HelpersPackage
 * @generated
 */
public interface HelpersFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	HelpersFactory eINSTANCE = de.hpi.sam.mote.helpers.impl.HelpersFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Modification Tag</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Modification Tag</em>'.
	 * @generated
	 */
	ModificationTag createModificationTag();

	/**
	 * Returns a new object of class '<em>Rule Set Tag</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Rule Set Tag</em>'.
	 * @generated
	 */
	RuleSetTag createRuleSetTag();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	HelpersPackage getHelpersPackage();

} // HelpersFactory
