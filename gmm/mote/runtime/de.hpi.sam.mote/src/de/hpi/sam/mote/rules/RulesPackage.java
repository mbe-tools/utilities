/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hpi.sam.mote.rules.RulesFactory
 * @model kind="package"
 * @generated
 */
public interface RulesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "rules";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mote/rules/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mote.rules";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	RulesPackage eINSTANCE = de.hpi.sam.mote.rules.impl.RulesPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.rules.impl.TGGMappingImpl <em>TGG Mapping</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.rules.impl.TGGMappingImpl
	 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTGGMapping()
	 * @generated
	 */
	int TGG_MAPPING = 0;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_MAPPING__CREATED_CORR_NODES = 0;

	/**
	 * The number of structural features of the '<em>TGG Mapping</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_MAPPING_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.rules.impl.TGGAxiomImpl <em>TGG Axiom</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.rules.impl.TGGAxiomImpl
	 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTGGAxiom()
	 * @generated
	 */
	int TGG_AXIOM = 1;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_AXIOM__CREATED_CORR_NODES = TGG_MAPPING__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_AXIOM__RULE_SET = TGG_MAPPING_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>TGG Axiom</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_AXIOM_FEATURE_COUNT = TGG_MAPPING_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.rules.impl.TGGRuleImpl <em>TGG Rule</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.rules.impl.TGGRuleImpl
	 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTGGRule()
	 * @generated
	 */
	int TGG_RULE = 2;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__CREATED_CORR_NODES = TGG_MAPPING__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__RULE_SET = TGG_MAPPING_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE__INPUT_CORR_NODE_TYPES = TGG_MAPPING_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>TGG Rule</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_FEATURE_COUNT = TGG_MAPPING_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl <em>TGG Rule Set</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.rules.impl.TGGRuleSetImpl
	 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTGGRuleSet()
	 * @generated
	 */
	int TGG_RULE_SET = 3;

	/**
	 * The feature id for the '<em><b>Root Corr Node</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__ROOT_CORR_NODE = 0;

	/**
	 * The feature id for the '<em><b>Axiom</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__AXIOM = 1;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__RULES = 2;

	/**
	 * The feature id for the '<em><b>Process Notifications</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__PROCESS_NOTIFICATIONS = 3;

	/**
	 * The feature id for the '<em><b>Correspondence Nodes</b></em>' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__CORRESPONDENCE_NODES = 4;

	/**
	 * The feature id for the '<em><b>Source Model Elements</b></em>' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__SOURCE_MODEL_ELEMENTS = 5;

	/**
	 * The feature id for the '<em><b>Target Model Elements</b></em>' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__TARGET_MODEL_ELEMENTS = 6;

	/**
	 * The feature id for the '<em><b>Uncovered Source Model Elements</b></em>' map.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS = 7;

	/**
	 * The feature id for the '<em><b>Uncovered Target Model Elements</b></em>' map.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS = 8;

	/**
	 * The feature id for the '<em><b>Source Model Elements Adapter</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__SOURCE_MODEL_ELEMENTS_ADAPTER = 9;

	/**
	 * The feature id for the '<em><b>Target Model Elements Adapter</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__TARGET_MODEL_ELEMENTS_ADAPTER = 10;

	/**
	 * The feature id for the '<em><b>Transformation Queue</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__TRANSFORMATION_QUEUE = 11;

	/**
	 * The feature id for the '<em><b>Source Reverse Navigation Store</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__SOURCE_REVERSE_NAVIGATION_STORE = 12;

	/**
	 * The feature id for the '<em><b>Target Reverse Navigation Store</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__TARGET_REVERSE_NAVIGATION_STORE = 13;

	/**
	 * The feature id for the '<em><b>Source Model Root Node</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__SOURCE_MODEL_ROOT_NODE = 14;

	/**
	 * The feature id for the '<em><b>Target Model Root Node</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__TARGET_MODEL_ROOT_NODE = 15;

	/**
	 * The feature id for the '<em><b>Corr Nodes To Delete</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__CORR_NODES_TO_DELETE = 16;

	/**
	 * The feature id for the '<em><b>Resource Set</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__RESOURCE_SET = 17;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__ID = 18;

	/**
	 * The feature id for the '<em><b>External References</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__EXTERNAL_REFERENCES = 19;

	/**
	 * The feature id for the '<em><b>External Ref Patterns</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET__EXTERNAL_REF_PATTERNS = 20;

	/**
	 * The number of structural features of the '<em>TGG Rule Set</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_RULE_SET_FEATURE_COUNT = 21;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.rules.impl.TransformationQueueImpl <em>Transformation Queue</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.mote.rules.impl.TransformationQueueImpl
	 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTransformationQueue()
	 * @generated
	 */
	int TRANSFORMATION_QUEUE = 4;

	/**
	 * The number of structural features of the '<em>Transformation Queue</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_QUEUE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '
	 * {@link de.hpi.sam.mote.rules.impl.MatchStorageImpl
	 * <em>Match Storage</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.mote.rules.impl.MatchStorageImpl
	 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getMatchStorage()
	 * @generated
	 */
	int MATCH_STORAGE = 5;

	/**
	 * The feature id for the '<em><b>Matches</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH_STORAGE__MATCHES = 0;

	/**
	 * The number of structural features of the '<em>Match Storage</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH_STORAGE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.rules.impl.MatchImpl <em>Match</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.rules.impl.MatchImpl
	 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getMatch()
	 * @generated
	 */
	int MATCH = 6;

	/**
	 * The feature id for the '<em><b>Application Context</b></em>' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MATCH__APPLICATION_CONTEXT = 0;

	/**
	 * The feature id for the '<em><b>Source Created Elements</b></em>' map.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__SOURCE_CREATED_ELEMENTS = 1;

	/**
	 * The feature id for the '<em><b>Corr Created Elements</b></em>' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MATCH__CORR_CREATED_ELEMENTS = 2;

	/**
	 * The feature id for the '<em><b>Target Created Elements</b></em>' map.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__TARGET_CREATED_ELEMENTS = 3;

	/**
	 * The number of structural features of the '<em>Match</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MATCH_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.rules.impl.ReferencePatternImpl <em>Reference Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.rules.impl.ReferencePatternImpl
	 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getReferencePattern()
	 * @generated
	 */
	int REFERENCE_PATTERN = 7;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_PATTERN__REFERENCE = 0;

	/**
	 * The feature id for the '<em><b>Source Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_PATTERN__SOURCE_CLASSIFIER = 1;

	/**
	 * The feature id for the '<em><b>Target Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_PATTERN__TARGET_CLASSIFIER = 2;

	/**
	 * The number of structural features of the '<em>Reference Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_PATTERN_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.rules.impl.TransformationExecutionResultImpl <em>Transformation Execution Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.rules.impl.TransformationExecutionResultImpl
	 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTransformationExecutionResult()
	 * @generated
	 */
	int TRANSFORMATION_EXECUTION_RESULT = 8;

	/**
	 * The feature id for the '<em><b>Transformation Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_RESULT = 0;

	/**
	 * The feature id for the '<em><b>Transformation Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_DIRECTION = 1;

	/**
	 * The feature id for the '<em><b>Left Uncovered Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EXECUTION_RESULT__LEFT_UNCOVERED_ELEMENTS = 2;

	/**
	 * The feature id for the '<em><b>Right Uncovered Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EXECUTION_RESULT__RIGHT_UNCOVERED_ELEMENTS = 3;

	/**
	 * The feature id for the '<em><b>Execution Trace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EXECUTION_RESULT__EXECUTION_TRACE = 4;

	/**
	 * The number of structural features of the '<em>Transformation Execution Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_EXECUTION_RESULT_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.rules.TransformationResult <em>Transformation Result</em>}' enum.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.mote.rules.TransformationResult
	 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTransformationResult()
	 * @generated
	 */
	int TRANSFORMATION_RESULT = 9;

	/**
	 * The meta object id for the '<em>Method</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.reflect.Method
	 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getMethod()
	 * @generated
	 */
	int METHOD = 10;

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.mote.rules.TGGMapping <em>TGG Mapping</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>TGG Mapping</em>'.
	 * @see de.hpi.sam.mote.rules.TGGMapping
	 * @generated
	 */
	EClass getTGGMapping();

	/**
	 * Returns the meta object for the reference list '
	 * {@link de.hpi.sam.mote.rules.TGGMapping#getCreatedCorrNodes
	 * <em>Created Corr Nodes</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference list '
	 *         <em>Created Corr Nodes</em>'.
	 * @see de.hpi.sam.mote.rules.TGGMapping#getCreatedCorrNodes()
	 * @see #getTGGMapping()
	 * @generated
	 */
	EReference getTGGMapping_CreatedCorrNodes();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.rules.TGGAxiom <em>TGG Axiom</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>TGG Axiom</em>'.
	 * @see de.hpi.sam.mote.rules.TGGAxiom
	 * @generated
	 */
	EClass getTGGAxiom();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.rules.TGGAxiom#getRuleSet <em>Rule Set</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Rule Set</em>'.
	 * @see de.hpi.sam.mote.rules.TGGAxiom#getRuleSet()
	 * @see #getTGGAxiom()
	 * @generated
	 */
	EReference getTGGAxiom_RuleSet();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.rules.TGGRule <em>TGG Rule</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>TGG Rule</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRule
	 * @generated
	 */
	EClass getTGGRule();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.hpi.sam.mote.rules.TGGRule#getRuleSet <em>Rule Set</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Rule Set</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRule#getRuleSet()
	 * @see #getTGGRule()
	 * @generated
	 */
	EReference getTGGRule_RuleSet();

	/**
	 * Returns the meta object for the attribute list '{@link de.hpi.sam.mote.rules.TGGRule#getInputCorrNodeTypes <em>Input Corr Node Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Input Corr Node Types</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRule#getInputCorrNodeTypes()
	 * @see #getTGGRule()
	 * @generated
	 */
	EAttribute getTGGRule_InputCorrNodeTypes();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.mote.rules.TGGRuleSet <em>TGG Rule Set</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>TGG Rule Set</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet
	 * @generated
	 */
	EClass getTGGRuleSet();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.rules.TGGRuleSet#getRootCorrNode <em>Root Corr Node</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Root Corr Node</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getRootCorrNode()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_RootCorrNode();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.hpi.sam.mote.rules.TGGRuleSet#getAxiom <em>Axiom</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Axiom</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getAxiom()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_Axiom();

	/**
	 * Returns the meta object for the reference list '
	 * {@link de.hpi.sam.mote.rules.TGGRuleSet#getRules <em>Rules</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Rules</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getRules()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_Rules();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.rules.TGGRuleSet#isProcessNotifications <em>Process Notifications</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Process Notifications</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#isProcessNotifications()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EAttribute getTGGRuleSet_ProcessNotifications();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.mote.rules.TGGRuleSet#getCorrespondenceNodes <em>Correspondence Nodes</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the map '<em>Correspondence Nodes</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getCorrespondenceNodes()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_CorrespondenceNodes();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.mote.rules.TGGRuleSet#getSourceModelElements <em>Source Model Elements</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the map '<em>Source Model Elements</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getSourceModelElements()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_SourceModelElements();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.mote.rules.TGGRuleSet#getTargetModelElements <em>Target Model Elements</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the map '<em>Target Model Elements</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getTargetModelElements()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_TargetModelElements();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.mote.rules.TGGRuleSet#getUncoveredSourceModelElements <em>Uncovered Source Model Elements</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the map '<em>Uncovered Source Model Elements</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getUncoveredSourceModelElements()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_UncoveredSourceModelElements();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.mote.rules.TGGRuleSet#getUncoveredTargetModelElements <em>Uncovered Target Model Elements</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the map '<em>Uncovered Target Model Elements</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getUncoveredTargetModelElements()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_UncoveredTargetModelElements();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.rules.TGGRuleSet#getSourceModelElementsAdapter <em>Source Model Elements Adapter</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Model Elements Adapter</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getSourceModelElementsAdapter()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EAttribute getTGGRuleSet_SourceModelElementsAdapter();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.rules.TGGRuleSet#getTargetModelElementsAdapter <em>Target Model Elements Adapter</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Model Elements Adapter</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getTargetModelElementsAdapter()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EAttribute getTGGRuleSet_TargetModelElementsAdapter();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.rules.TGGRuleSet#getTransformationQueue <em>Transformation Queue</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference '<em>Transformation Queue</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getTransformationQueue()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_TransformationQueue();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.rules.TGGRuleSet#getSourceReverseNavigationStore <em>Source Reverse Navigation Store</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference '<em>Source Reverse Navigation Store</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getSourceReverseNavigationStore()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_SourceReverseNavigationStore();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.rules.TGGRuleSet#getTargetReverseNavigationStore <em>Target Reverse Navigation Store</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference '<em>Target Reverse Navigation Store</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getTargetReverseNavigationStore()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_TargetReverseNavigationStore();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.rules.TGGRuleSet#getSourceModelRootNode <em>Source Model Root Node</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference '<em>Source Model Root Node</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getSourceModelRootNode()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_SourceModelRootNode();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.rules.TGGRuleSet#getTargetModelRootNode <em>Target Model Root Node</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference '<em>Target Model Root Node</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getTargetModelRootNode()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_TargetModelRootNode();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.mote.rules.TGGRuleSet#getCorrNodesToDelete <em>Corr Nodes To Delete</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference list '<em>Corr Nodes To Delete</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getCorrNodesToDelete()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_CorrNodesToDelete();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.rules.TGGRuleSet#getResourceSet <em>Resource Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Resource Set</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getResourceSet()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EAttribute getTGGRuleSet_ResourceSet();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.rules.TGGRuleSet#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getId()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EAttribute getTGGRuleSet_Id();

	/**
	 * Returns the meta object for the attribute list '{@link de.hpi.sam.mote.rules.TGGRuleSet#getExternalReferences <em>External References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>External References</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getExternalReferences()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EAttribute getTGGRuleSet_ExternalReferences();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.mote.rules.TGGRuleSet#getExternalRefPatterns <em>External Ref Patterns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>External Ref Patterns</em>'.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getExternalRefPatterns()
	 * @see #getTGGRuleSet()
	 * @generated
	 */
	EReference getTGGRuleSet_ExternalRefPatterns();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.rules.TransformationQueue <em>Transformation Queue</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Transformation Queue</em>'.
	 * @see de.hpi.sam.mote.rules.TransformationQueue
	 * @generated
	 */
	EClass getTransformationQueue();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.mote.rules.MatchStorage <em>Match Storage</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Match Storage</em>'.
	 * @see de.hpi.sam.mote.rules.MatchStorage
	 * @generated
	 */
	EClass getMatchStorage();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hpi.sam.mote.rules.MatchStorage#getMatches <em>Matches</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Matches</em>'.
	 * @see de.hpi.sam.mote.rules.MatchStorage#getMatches()
	 * @see #getMatchStorage()
	 * @generated
	 */
	EReference getMatchStorage_Matches();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.rules.Match <em>Match</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Match</em>'.
	 * @see de.hpi.sam.mote.rules.Match
	 * @generated
	 */
	EClass getMatch();

	/**
	 * Returns the meta object for the map '
	 * {@link de.hpi.sam.mote.rules.Match#getApplicationContext
	 * <em>Application Context</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the map '<em>Application Context</em>'.
	 * @see de.hpi.sam.mote.rules.Match#getApplicationContext()
	 * @see #getMatch()
	 * @generated
	 */
	EReference getMatch_ApplicationContext();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.mote.rules.Match#getSourceCreatedElements <em>Source Created Elements</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the map '<em>Source Created Elements</em>'.
	 * @see de.hpi.sam.mote.rules.Match#getSourceCreatedElements()
	 * @see #getMatch()
	 * @generated
	 */
	EReference getMatch_SourceCreatedElements();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.mote.rules.Match#getCorrCreatedElements <em>Corr Created Elements</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the map '<em>Corr Created Elements</em>'.
	 * @see de.hpi.sam.mote.rules.Match#getCorrCreatedElements()
	 * @see #getMatch()
	 * @generated
	 */
	EReference getMatch_CorrCreatedElements();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.mote.rules.Match#getTargetCreatedElements <em>Target Created Elements</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the map '<em>Target Created Elements</em>'.
	 * @see de.hpi.sam.mote.rules.Match#getTargetCreatedElements()
	 * @see #getMatch()
	 * @generated
	 */
	EReference getMatch_TargetCreatedElements();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.rules.ReferencePattern <em>Reference Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Pattern</em>'.
	 * @see de.hpi.sam.mote.rules.ReferencePattern
	 * @generated
	 */
	EClass getReferencePattern();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.rules.ReferencePattern#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference</em>'.
	 * @see de.hpi.sam.mote.rules.ReferencePattern#getReference()
	 * @see #getReferencePattern()
	 * @generated
	 */
	EReference getReferencePattern_Reference();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.rules.ReferencePattern#getSourceClassifier <em>Source Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source Classifier</em>'.
	 * @see de.hpi.sam.mote.rules.ReferencePattern#getSourceClassifier()
	 * @see #getReferencePattern()
	 * @generated
	 */
	EReference getReferencePattern_SourceClassifier();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.rules.ReferencePattern#getTargetClassifier <em>Target Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target Classifier</em>'.
	 * @see de.hpi.sam.mote.rules.ReferencePattern#getTargetClassifier()
	 * @see #getReferencePattern()
	 * @generated
	 */
	EReference getReferencePattern_TargetClassifier();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.rules.TransformationExecutionResult <em>Transformation Execution Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transformation Execution Result</em>'.
	 * @see de.hpi.sam.mote.rules.TransformationExecutionResult
	 * @generated
	 */
	EClass getTransformationExecutionResult();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getTransformationResult <em>Transformation Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transformation Result</em>'.
	 * @see de.hpi.sam.mote.rules.TransformationExecutionResult#getTransformationResult()
	 * @see #getTransformationExecutionResult()
	 * @generated
	 */
	EAttribute getTransformationExecutionResult_TransformationResult();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getTransformationDirection <em>Transformation Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transformation Direction</em>'.
	 * @see de.hpi.sam.mote.rules.TransformationExecutionResult#getTransformationDirection()
	 * @see #getTransformationExecutionResult()
	 * @generated
	 */
	EAttribute getTransformationExecutionResult_TransformationDirection();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getLeftUncoveredElements <em>Left Uncovered Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Left Uncovered Elements</em>'.
	 * @see de.hpi.sam.mote.rules.TransformationExecutionResult#getLeftUncoveredElements()
	 * @see #getTransformationExecutionResult()
	 * @generated
	 */
	EReference getTransformationExecutionResult_LeftUncoveredElements();

	/**
	 * Returns the meta object for the reference list '{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getRightUncoveredElements <em>Right Uncovered Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Right Uncovered Elements</em>'.
	 * @see de.hpi.sam.mote.rules.TransformationExecutionResult#getRightUncoveredElements()
	 * @see #getTransformationExecutionResult()
	 * @generated
	 */
	EReference getTransformationExecutionResult_RightUncoveredElements();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getExecutionTrace <em>Execution Trace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Execution Trace</em>'.
	 * @see de.hpi.sam.mote.rules.TransformationExecutionResult#getExecutionTrace()
	 * @see #getTransformationExecutionResult()
	 * @generated
	 */
	EReference getTransformationExecutionResult_ExecutionTrace();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.mote.rules.TransformationResult <em>Transformation Result</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for enum '<em>Transformation Result</em>'.
	 * @see de.hpi.sam.mote.rules.TransformationResult
	 * @generated
	 */
	EEnum getTransformationResult();

	/**
	 * Returns the meta object for data type '{@link java.lang.reflect.Method <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Method</em>'.
	 * @see java.lang.reflect.Method
	 * @model instanceClass="java.lang.reflect.Method"
	 * @generated
	 */
	EDataType getMethod();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RulesFactory getRulesFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.rules.impl.TGGMappingImpl <em>TGG Mapping</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.mote.rules.impl.TGGMappingImpl
		 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTGGMapping()
		 * @generated
		 */
		EClass TGG_MAPPING = eINSTANCE.getTGGMapping();

		/**
		 * The meta object literal for the '<em><b>Created Corr Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_MAPPING__CREATED_CORR_NODES = eINSTANCE.getTGGMapping_CreatedCorrNodes();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.rules.impl.TGGAxiomImpl <em>TGG Axiom</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.rules.impl.TGGAxiomImpl
		 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTGGAxiom()
		 * @generated
		 */
		EClass TGG_AXIOM = eINSTANCE.getTGGAxiom();

		/**
		 * The meta object literal for the '<em><b>Rule Set</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_AXIOM__RULE_SET = eINSTANCE.getTGGAxiom_RuleSet();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.rules.impl.TGGRuleImpl <em>TGG Rule</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.rules.impl.TGGRuleImpl
		 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTGGRule()
		 * @generated
		 */
		EClass TGG_RULE = eINSTANCE.getTGGRule();

		/**
		 * The meta object literal for the '<em><b>Rule Set</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE__RULE_SET = eINSTANCE.getTGGRule_RuleSet();

		/**
		 * The meta object literal for the '<em><b>Input Corr Node Types</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_RULE__INPUT_CORR_NODE_TYPES = eINSTANCE.getTGGRule_InputCorrNodeTypes();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl <em>TGG Rule Set</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.mote.rules.impl.TGGRuleSetImpl
		 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTGGRuleSet()
		 * @generated
		 */
		EClass TGG_RULE_SET = eINSTANCE.getTGGRuleSet();

		/**
		 * The meta object literal for the '<em><b>Root Corr Node</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE_SET__ROOT_CORR_NODE = eINSTANCE.getTGGRuleSet_RootCorrNode();

		/**
		 * The meta object literal for the '<em><b>Axiom</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE_SET__AXIOM = eINSTANCE.getTGGRuleSet_Axiom();

		/**
		 * The meta object literal for the '<em><b>Rules</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE_SET__RULES = eINSTANCE.getTGGRuleSet_Rules();

		/**
		 * The meta object literal for the '
		 * <em><b>Process Notifications</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute TGG_RULE_SET__PROCESS_NOTIFICATIONS = eINSTANCE.getTGGRuleSet_ProcessNotifications();

		/**
		 * The meta object literal for the '<em><b>Correspondence Nodes</b></em>' map feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE_SET__CORRESPONDENCE_NODES = eINSTANCE.getTGGRuleSet_CorrespondenceNodes();

		/**
		 * The meta object literal for the '
		 * <em><b>Source Model Elements</b></em>' map feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference TGG_RULE_SET__SOURCE_MODEL_ELEMENTS = eINSTANCE.getTGGRuleSet_SourceModelElements();

		/**
		 * The meta object literal for the '
		 * <em><b>Target Model Elements</b></em>' map feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference TGG_RULE_SET__TARGET_MODEL_ELEMENTS = eINSTANCE.getTGGRuleSet_TargetModelElements();

		/**
		 * The meta object literal for the '
		 * <em><b>Uncovered Source Model Elements</b></em>' map feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS = eINSTANCE.getTGGRuleSet_UncoveredSourceModelElements();

		/**
		 * The meta object literal for the '
		 * <em><b>Uncovered Target Model Elements</b></em>' map feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS = eINSTANCE.getTGGRuleSet_UncoveredTargetModelElements();

		/**
		 * The meta object literal for the '<em><b>Source Model Elements Adapter</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_RULE_SET__SOURCE_MODEL_ELEMENTS_ADAPTER = eINSTANCE.getTGGRuleSet_SourceModelElementsAdapter();

		/**
		 * The meta object literal for the '<em><b>Target Model Elements Adapter</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_RULE_SET__TARGET_MODEL_ELEMENTS_ADAPTER = eINSTANCE.getTGGRuleSet_TargetModelElementsAdapter();

		/**
		 * The meta object literal for the '<em><b>Transformation Queue</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE_SET__TRANSFORMATION_QUEUE = eINSTANCE.getTGGRuleSet_TransformationQueue();

		/**
		 * The meta object literal for the '<em><b>Source Reverse Navigation Store</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE_SET__SOURCE_REVERSE_NAVIGATION_STORE = eINSTANCE.getTGGRuleSet_SourceReverseNavigationStore();

		/**
		 * The meta object literal for the '<em><b>Target Reverse Navigation Store</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE_SET__TARGET_REVERSE_NAVIGATION_STORE = eINSTANCE.getTGGRuleSet_TargetReverseNavigationStore();

		/**
		 * The meta object literal for the '
		 * <em><b>Source Model Root Node</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference TGG_RULE_SET__SOURCE_MODEL_ROOT_NODE = eINSTANCE.getTGGRuleSet_SourceModelRootNode();

		/**
		 * The meta object literal for the '
		 * <em><b>Target Model Root Node</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference TGG_RULE_SET__TARGET_MODEL_ROOT_NODE = eINSTANCE.getTGGRuleSet_TargetModelRootNode();

		/**
		 * The meta object literal for the '<em><b>Corr Nodes To Delete</b></em>
		 * ' reference list feature. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @generated
		 */
		EReference TGG_RULE_SET__CORR_NODES_TO_DELETE = eINSTANCE.getTGGRuleSet_CorrNodesToDelete();

		/**
		 * The meta object literal for the '<em><b>Resource Set</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_RULE_SET__RESOURCE_SET = eINSTANCE.getTGGRuleSet_ResourceSet();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_RULE_SET__ID = eINSTANCE.getTGGRuleSet_Id();

		/**
		 * The meta object literal for the '<em><b>External References</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TGG_RULE_SET__EXTERNAL_REFERENCES = eINSTANCE.getTGGRuleSet_ExternalReferences();

		/**
		 * The meta object literal for the '<em><b>External Ref Patterns</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_RULE_SET__EXTERNAL_REF_PATTERNS = eINSTANCE.getTGGRuleSet_ExternalRefPatterns();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.rules.impl.TransformationQueueImpl <em>Transformation Queue</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.mote.rules.impl.TransformationQueueImpl
		 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTransformationQueue()
		 * @generated
		 */
		EClass TRANSFORMATION_QUEUE = eINSTANCE.getTransformationQueue();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.rules.impl.MatchStorageImpl <em>Match Storage</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.mote.rules.impl.MatchStorageImpl
		 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getMatchStorage()
		 * @generated
		 */
		EClass MATCH_STORAGE = eINSTANCE.getMatchStorage();

		/**
		 * The meta object literal for the '<em><b>Matches</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATCH_STORAGE__MATCHES = eINSTANCE.getMatchStorage_Matches();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.rules.impl.MatchImpl <em>Match</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.rules.impl.MatchImpl
		 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getMatch()
		 * @generated
		 */
		EClass MATCH = eINSTANCE.getMatch();

		/**
		 * The meta object literal for the '<em><b>Application Context</b></em>' map feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATCH__APPLICATION_CONTEXT = eINSTANCE.getMatch_ApplicationContext();

		/**
		 * The meta object literal for the '
		 * <em><b>Source Created Elements</b></em>' map feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference MATCH__SOURCE_CREATED_ELEMENTS = eINSTANCE.getMatch_SourceCreatedElements();

		/**
		 * The meta object literal for the '
		 * <em><b>Corr Created Elements</b></em>' map feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference MATCH__CORR_CREATED_ELEMENTS = eINSTANCE.getMatch_CorrCreatedElements();

		/**
		 * The meta object literal for the '
		 * <em><b>Target Created Elements</b></em>' map feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference MATCH__TARGET_CREATED_ELEMENTS = eINSTANCE.getMatch_TargetCreatedElements();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.rules.impl.ReferencePatternImpl <em>Reference Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.rules.impl.ReferencePatternImpl
		 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getReferencePattern()
		 * @generated
		 */
		EClass REFERENCE_PATTERN = eINSTANCE.getReferencePattern();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_PATTERN__REFERENCE = eINSTANCE.getReferencePattern_Reference();

		/**
		 * The meta object literal for the '<em><b>Source Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_PATTERN__SOURCE_CLASSIFIER = eINSTANCE.getReferencePattern_SourceClassifier();

		/**
		 * The meta object literal for the '<em><b>Target Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_PATTERN__TARGET_CLASSIFIER = eINSTANCE.getReferencePattern_TargetClassifier();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.rules.impl.TransformationExecutionResultImpl <em>Transformation Execution Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.rules.impl.TransformationExecutionResultImpl
		 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTransformationExecutionResult()
		 * @generated
		 */
		EClass TRANSFORMATION_EXECUTION_RESULT = eINSTANCE.getTransformationExecutionResult();

		/**
		 * The meta object literal for the '<em><b>Transformation Result</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_RESULT = eINSTANCE.getTransformationExecutionResult_TransformationResult();

		/**
		 * The meta object literal for the '<em><b>Transformation Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_DIRECTION = eINSTANCE.getTransformationExecutionResult_TransformationDirection();

		/**
		 * The meta object literal for the '<em><b>Left Uncovered Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_EXECUTION_RESULT__LEFT_UNCOVERED_ELEMENTS = eINSTANCE.getTransformationExecutionResult_LeftUncoveredElements();

		/**
		 * The meta object literal for the '<em><b>Right Uncovered Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_EXECUTION_RESULT__RIGHT_UNCOVERED_ELEMENTS = eINSTANCE.getTransformationExecutionResult_RightUncoveredElements();

		/**
		 * The meta object literal for the '<em><b>Execution Trace</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_EXECUTION_RESULT__EXECUTION_TRACE = eINSTANCE.getTransformationExecutionResult_ExecutionTrace();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.rules.TransformationResult <em>Transformation Result</em>}' enum.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.mote.rules.TransformationResult
		 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getTransformationResult()
		 * @generated
		 */
		EEnum TRANSFORMATION_RESULT = eINSTANCE.getTransformationResult();

		/**
		 * The meta object literal for the '<em>Method</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.reflect.Method
		 * @see de.hpi.sam.mote.rules.impl.RulesPackageImpl#getMethod()
		 * @generated
		 */
		EDataType METHOD = eINSTANCE.getMethod();

	}

} // RulesPackage
