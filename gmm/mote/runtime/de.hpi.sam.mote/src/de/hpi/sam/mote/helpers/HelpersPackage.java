/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.helpers;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hpi.sam.mote.helpers.HelpersFactory
 * @model kind="package"
 * @generated
 */
public interface HelpersPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "helpers";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mote/helpers/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mote.helpers";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	HelpersPackage eINSTANCE = de.hpi.sam.mote.helpers.impl.HelpersPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.helpers.impl.MapEntryImpl <em>Map Entry</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.helpers.impl.MapEntryImpl
	 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getMapEntry()
	 * @generated
	 */
	int MAP_ENTRY = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Map Entry</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.helpers.impl.ModificationTagImpl <em>Modification Tag</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.mote.helpers.impl.ModificationTagImpl
	 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getModificationTag()
	 * @generated
	 */
	int MODIFICATION_TAG = 1;

	/**
	 * The feature id for the '<em><b>Correspondence Node</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODIFICATION_TAG__CORRESPONDENCE_NODE = 0;

	/**
	 * The feature id for the '<em><b>Synchronize</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MODIFICATION_TAG__SYNCHRONIZE = 1;

	/**
	 * The number of structural features of the '<em>Modification Tag</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODIFICATION_TAG_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.helpers.impl.RuleSetTagImpl <em>Rule Set Tag</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.helpers.impl.RuleSetTagImpl
	 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getRuleSetTag()
	 * @generated
	 */
	int RULE_SET_TAG = 2;

	/**
	 * The feature id for the '<em><b>Rule Set ID</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_SET_TAG__RULE_SET_ID = 0;

	/**
	 * The feature id for the '<em><b>Source Model Uri</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_SET_TAG__SOURCE_MODEL_URI = 1;

	/**
	 * The feature id for the '<em><b>Target Model Uri</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_SET_TAG__TARGET_MODEL_URI = 2;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_SET_TAG__RULE_SET = 3;

	/**
	 * The feature id for the '<em><b>Rule Set Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_SET_TAG__RULE_SET_NAME = 4;

	/**
	 * The feature id for the '<em><b>Source Model ID</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_SET_TAG__SOURCE_MODEL_ID = 5;

	/**
	 * The feature id for the '<em><b>Target Model ID</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int RULE_SET_TAG__TARGET_MODEL_ID = 6;

	/**
	 * The feature id for the '<em><b>Rule Set Package Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_SET_TAG__RULE_SET_PACKAGE_NS_URI = 7;

	/**
	 * The number of structural features of the '<em>Rule Set Tag</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_SET_TAG_FEATURE_COUNT = 8;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.helpers.impl.ReverseNavigationStoreImpl <em>Reverse Navigation Store</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.mote.helpers.impl.ReverseNavigationStoreImpl
	 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getReverseNavigationStore()
	 * @generated
	 */
	int REVERSE_NAVIGATION_STORE = 3;

	/**
	 * The number of structural features of the '<em>Reverse Navigation Store</em>' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_NAVIGATION_STORE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '<em>Adapter</em>' data type. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.common.notify.Adapter
	 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getAdapter()
	 * @generated
	 */
	int ADAPTER = 4;

	/**
	 * The meta object id for the '<em>Transformation Exception</em>' data type.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.impl.TransformationException
	 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getTransformationException()
	 * @generated
	 */
	int TRANSFORMATION_EXCEPTION = 5;

	/**
	 * The meta object id for the '<em>URI</em>' data type.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.URI
	 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getURI()
	 * @generated
	 */
	int URI = 6;

	/**
	 * The meta object id for the '<em>IProgress Monitor</em>' data type. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.core.runtime.IProgressMonitor
	 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getIProgressMonitor()
	 * @generated
	 */
	int IPROGRESS_MONITOR = 7;

	/**
	 * The meta object id for the '<em>Unresolved Ref Trans Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.impl.UnresolvedRefTransException
	 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getUnresolvedRefTransException()
	 * @generated
	 */
	int UNRESOLVED_REF_TRANS_EXCEPTION = 8;

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Map Entry</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="KeyType" keyRequired="true"
	 *        valueDataType="ValueType"
	 * @generated
	 */
	EClass getMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMapEntry()
	 * @generated
	 */
	EAttribute getMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMapEntry()
	 * @generated
	 */
	EAttribute getMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.helpers.ModificationTag <em>Modification Tag</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Modification Tag</em>'.
	 * @see de.hpi.sam.mote.helpers.ModificationTag
	 * @generated
	 */
	EClass getModificationTag();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.hpi.sam.mote.helpers.ModificationTag#getCorrespondenceNode
	 * <em>Correspondence Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Correspondence Node</em>'.
	 * @see de.hpi.sam.mote.helpers.ModificationTag#getCorrespondenceNode()
	 * @see #getModificationTag()
	 * @generated
	 */
	EReference getModificationTag_CorrespondenceNode();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.helpers.ModificationTag#isSynchronize <em>Synchronize</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Synchronize</em>'.
	 * @see de.hpi.sam.mote.helpers.ModificationTag#isSynchronize()
	 * @see #getModificationTag()
	 * @generated
	 */
	EAttribute getModificationTag_Synchronize();

	/**
	 * Returns the meta object for class '
	 * {@link de.hpi.sam.mote.helpers.RuleSetTag <em>Rule Set Tag</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Rule Set Tag</em>'.
	 * @see de.hpi.sam.mote.helpers.RuleSetTag
	 * @generated
	 */
	EClass getRuleSetTag();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.helpers.RuleSetTag#getRuleSetID <em>Rule Set ID</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rule Set ID</em>'.
	 * @see de.hpi.sam.mote.helpers.RuleSetTag#getRuleSetID()
	 * @see #getRuleSetTag()
	 * @generated
	 */
	EAttribute getRuleSetTag_RuleSetID();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.helpers.RuleSetTag#getRuleSetName <em>Rule Set Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rule Set Name</em>'.
	 * @see de.hpi.sam.mote.helpers.RuleSetTag#getRuleSetName()
	 * @see #getRuleSetTag()
	 * @generated
	 */
	EAttribute getRuleSetTag_RuleSetName();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.helpers.RuleSetTag#getSourceModelID <em>Source Model ID</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Model ID</em>'.
	 * @see de.hpi.sam.mote.helpers.RuleSetTag#getSourceModelID()
	 * @see #getRuleSetTag()
	 * @generated
	 */
	EAttribute getRuleSetTag_SourceModelID();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.helpers.RuleSetTag#getTargetModelID <em>Target Model ID</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Model ID</em>'.
	 * @see de.hpi.sam.mote.helpers.RuleSetTag#getTargetModelID()
	 * @see #getRuleSetTag()
	 * @generated
	 */
	EAttribute getRuleSetTag_TargetModelID();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.helpers.RuleSetTag#getRuleSetPackageNsURI <em>Rule Set Package Ns URI</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Rule Set Package Ns URI</em>'.
	 * @see de.hpi.sam.mote.helpers.RuleSetTag#getRuleSetPackageNsURI()
	 * @see #getRuleSetTag()
	 * @generated
	 */
	EAttribute getRuleSetTag_RuleSetPackageNsURI();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.mote.helpers.RuleSetTag#getSourceModelUri
	 * <em>Source Model Uri</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Source Model Uri</em>'.
	 * @see de.hpi.sam.mote.helpers.RuleSetTag#getSourceModelUri()
	 * @see #getRuleSetTag()
	 * @generated
	 */
	EAttribute getRuleSetTag_SourceModelUri();

	/**
	 * Returns the meta object for the attribute '
	 * {@link de.hpi.sam.mote.helpers.RuleSetTag#getTargetModelUri
	 * <em>Target Model Uri</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Target Model Uri</em>'.
	 * @see de.hpi.sam.mote.helpers.RuleSetTag#getTargetModelUri()
	 * @see #getRuleSetTag()
	 * @generated
	 */
	EAttribute getRuleSetTag_TargetModelUri();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.helpers.RuleSetTag#getRuleSet <em>Rule Set</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Rule Set</em>'.
	 * @see de.hpi.sam.mote.helpers.RuleSetTag#getRuleSet()
	 * @see #getRuleSetTag()
	 * @generated
	 */
	EReference getRuleSetTag_RuleSet();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.helpers.ReverseNavigationStore <em>Reverse Navigation Store</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>Reverse Navigation Store</em>'.
	 * @see de.hpi.sam.mote.helpers.ReverseNavigationStore
	 * @generated
	 */
	EClass getReverseNavigationStore();

	/**
	 * Returns the meta object for data type '
	 * {@link org.eclipse.emf.common.notify.Adapter <em>Adapter</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for data type '<em>Adapter</em>'.
	 * @see org.eclipse.emf.common.notify.Adapter
	 * @model instanceClass="org.eclipse.emf.common.notify.Adapter"
	 * @generated
	 */
	EDataType getAdapter();

	/**
	 * Returns the meta object for data type '{@link de.hpi.sam.mote.impl.TransformationException <em>Transformation Exception</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for data type '<em>Transformation Exception</em>'.
	 * @see de.hpi.sam.mote.impl.TransformationException
	 * @model instanceClass="de.hpi.sam.mote.impl.TransformationException"
	 * @generated
	 */
	EDataType getTransformationException();

	/**
	 * Returns the meta object for data type '
	 * {@link org.eclipse.emf.common.util.URI <em>URI</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for data type '<em>URI</em>'.
	 * @see org.eclipse.emf.common.util.URI
	 * @model instanceClass="org.eclipse.emf.common.util.URI"
	 * @generated
	 */
	EDataType getURI();

	/**
	 * Returns the meta object for data type '
	 * {@link org.eclipse.core.runtime.IProgressMonitor
	 * <em>IProgress Monitor</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for data type '<em>IProgress Monitor</em>'.
	 * @see org.eclipse.core.runtime.IProgressMonitor
	 * @model instanceClass="org.eclipse.core.runtime.IProgressMonitor"
	 * @generated
	 */
	EDataType getIProgressMonitor();

	/**
	 * Returns the meta object for data type '{@link de.hpi.sam.mote.impl.UnresolvedRefTransException <em>Unresolved Ref Trans Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Unresolved Ref Trans Exception</em>'.
	 * @see de.hpi.sam.mote.impl.UnresolvedRefTransException
	 * @model instanceClass="de.hpi.sam.mote.impl.UnresolvedRefTransException"
	 * @generated
	 */
	EDataType getUnresolvedRefTransException();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	HelpersFactory getHelpersFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.helpers.impl.MapEntryImpl <em>Map Entry</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.helpers.impl.MapEntryImpl
		 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getMapEntry()
		 * @generated
		 */
		EClass MAP_ENTRY = eINSTANCE.getMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAP_ENTRY__KEY = eINSTANCE.getMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAP_ENTRY__VALUE = eINSTANCE.getMapEntry_Value();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.helpers.impl.ModificationTagImpl <em>Modification Tag</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.mote.helpers.impl.ModificationTagImpl
		 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getModificationTag()
		 * @generated
		 */
		EClass MODIFICATION_TAG = eINSTANCE.getModificationTag();

		/**
		 * The meta object literal for the '<em><b>Correspondence Node</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODIFICATION_TAG__CORRESPONDENCE_NODE = eINSTANCE.getModificationTag_CorrespondenceNode();

		/**
		 * The meta object literal for the '<em><b>Synchronize</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODIFICATION_TAG__SYNCHRONIZE = eINSTANCE.getModificationTag_Synchronize();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.helpers.impl.RuleSetTagImpl <em>Rule Set Tag</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see de.hpi.sam.mote.helpers.impl.RuleSetTagImpl
		 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getRuleSetTag()
		 * @generated
		 */
		EClass RULE_SET_TAG = eINSTANCE.getRuleSetTag();

		/**
		 * The meta object literal for the '<em><b>Rule Set ID</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE_SET_TAG__RULE_SET_ID = eINSTANCE.getRuleSetTag_RuleSetID();

		/**
		 * The meta object literal for the '<em><b>Rule Set Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE_SET_TAG__RULE_SET_NAME = eINSTANCE.getRuleSetTag_RuleSetName();

		/**
		 * The meta object literal for the '<em><b>Source Model ID</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE_SET_TAG__SOURCE_MODEL_ID = eINSTANCE.getRuleSetTag_SourceModelID();

		/**
		 * The meta object literal for the '<em><b>Target Model ID</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE_SET_TAG__TARGET_MODEL_ID = eINSTANCE.getRuleSetTag_TargetModelID();

		/**
		 * The meta object literal for the '
		 * <em><b>Rule Set Package Ns URI</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute RULE_SET_TAG__RULE_SET_PACKAGE_NS_URI = eINSTANCE.getRuleSetTag_RuleSetPackageNsURI();

		/**
		 * The meta object literal for the '<em><b>Source Model Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE_SET_TAG__SOURCE_MODEL_URI = eINSTANCE.getRuleSetTag_SourceModelUri();

		/**
		 * The meta object literal for the '<em><b>Target Model Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE_SET_TAG__TARGET_MODEL_URI = eINSTANCE.getRuleSetTag_TargetModelUri();

		/**
		 * The meta object literal for the '<em><b>Rule Set</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_SET_TAG__RULE_SET = eINSTANCE.getRuleSetTag_RuleSet();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.helpers.impl.ReverseNavigationStoreImpl <em>Reverse Navigation Store</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.helpers.impl.ReverseNavigationStoreImpl
		 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getReverseNavigationStore()
		 * @generated
		 */
		EClass REVERSE_NAVIGATION_STORE = eINSTANCE.getReverseNavigationStore();

		/**
		 * The meta object literal for the '<em>Adapter</em>' data type. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.eclipse.emf.common.notify.Adapter
		 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getAdapter()
		 * @generated
		 */
		EDataType ADAPTER = eINSTANCE.getAdapter();

		/**
		 * The meta object literal for the '<em>Transformation Exception</em>' data type.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.impl.TransformationException
		 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getTransformationException()
		 * @generated
		 */
		EDataType TRANSFORMATION_EXCEPTION = eINSTANCE.getTransformationException();

		/**
		 * The meta object literal for the '<em>URI</em>' data type. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.eclipse.emf.common.util.URI
		 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getURI()
		 * @generated
		 */
		EDataType URI = eINSTANCE.getURI();

		/**
		 * The meta object literal for the '<em>IProgress Monitor</em>' data type.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see org.eclipse.core.runtime.IProgressMonitor
		 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getIProgressMonitor()
		 * @generated
		 */
		EDataType IPROGRESS_MONITOR = eINSTANCE.getIProgressMonitor();

		/**
		 * The meta object literal for the '<em>Unresolved Ref Trans Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.impl.UnresolvedRefTransException
		 * @see de.hpi.sam.mote.helpers.impl.HelpersPackageImpl#getUnresolvedRefTransException()
		 * @generated
		 */
		EDataType UNRESOLVED_REF_TRANS_EXCEPTION = eINSTANCE.getUnresolvedRefTransException();

	}

} // HelpersPackage
