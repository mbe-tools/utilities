/**
 */
package de.hpi.sam.mote.rules;

import de.hpi.sam.mote.TransformationDirection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transformation Execution Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getTransformationResult <em>Transformation Result</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getTransformationDirection <em>Transformation Direction</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getLeftUncoveredElements <em>Left Uncovered Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getRightUncoveredElements <em>Right Uncovered Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getExecutionTrace <em>Execution Trace</em>}</li>
 * </ul>
 *
 * @see de.hpi.sam.mote.rules.RulesPackage#getTransformationExecutionResult()
 * @model
 * @generated
 */
public interface TransformationExecutionResult extends EObject {
	/**
	 * Returns the value of the '<em><b>Transformation Result</b></em>' attribute.
	 * The literals are from the enumeration {@link de.hpi.sam.mote.rules.TransformationResult}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transformation Result</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transformation Result</em>' attribute.
	 * @see de.hpi.sam.mote.rules.TransformationResult
	 * @see #setTransformationResult(TransformationResult)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTransformationExecutionResult_TransformationResult()
	 * @model required="true"
	 * @generated
	 */
	TransformationResult getTransformationResult();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getTransformationResult <em>Transformation Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transformation Result</em>' attribute.
	 * @see de.hpi.sam.mote.rules.TransformationResult
	 * @see #getTransformationResult()
	 * @generated
	 */
	void setTransformationResult(TransformationResult value);

	/**
	 * Returns the value of the '<em><b>Transformation Direction</b></em>' attribute.
	 * The literals are from the enumeration {@link de.hpi.sam.mote.TransformationDirection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transformation Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transformation Direction</em>' attribute.
	 * @see de.hpi.sam.mote.TransformationDirection
	 * @see #setTransformationDirection(TransformationDirection)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTransformationExecutionResult_TransformationDirection()
	 * @model required="true"
	 * @generated
	 */
	TransformationDirection getTransformationDirection();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getTransformationDirection <em>Transformation Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transformation Direction</em>' attribute.
	 * @see de.hpi.sam.mote.TransformationDirection
	 * @see #getTransformationDirection()
	 * @generated
	 */
	void setTransformationDirection(TransformationDirection value);

	/**
	 * Returns the value of the '<em><b>Left Uncovered Elements</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Uncovered Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Uncovered Elements</em>' reference list.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTransformationExecutionResult_LeftUncoveredElements()
	 * @model
	 * @generated
	 */
	EList<EObject> getLeftUncoveredElements();

	/**
	 * Returns the value of the '<em><b>Right Uncovered Elements</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Uncovered Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Uncovered Elements</em>' reference list.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTransformationExecutionResult_RightUncoveredElements()
	 * @model
	 * @generated
	 */
	EList<EObject> getRightUncoveredElements();

	/**
	 * Returns the value of the '<em><b>Execution Trace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Trace</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Trace</em>' reference.
	 * @see #setExecutionTrace(EObject)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTransformationExecutionResult_ExecutionTrace()
	 * @model
	 * @generated
	 */
	EObject getExecutionTrace();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TransformationExecutionResult#getExecutionTrace <em>Execution Trace</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Trace</em>' reference.
	 * @see #getExecutionTrace()
	 * @generated
	 */
	void setExecutionTrace(EObject value);

} // TransformationExecutionResult
