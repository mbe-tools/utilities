/**
 */
package de.hpi.sam.mote.rules.impl;

import de.hpi.sam.mote.rules.ReferencePattern;
import de.hpi.sam.mote.rules.RulesPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reference Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.impl.ReferencePatternImpl#getReference <em>Reference</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.ReferencePatternImpl#getSourceClassifier <em>Source Classifier</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.ReferencePatternImpl#getTargetClassifier <em>Target Classifier</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReferencePatternImpl extends EObjectImpl implements ReferencePattern {
	/**
	 * The cached value of the '{@link #getReference() <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReference()
	 * @generated
	 * @ordered
	 */
	protected EReference reference;

	/**
	 * The cached value of the '{@link #getSourceClassifier() <em>Source Classifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceClassifier()
	 * @generated
	 * @ordered
	 */
	protected EClassifier sourceClassifier;

	/**
	 * The cached value of the '{@link #getTargetClassifier() <em>Target Classifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetClassifier()
	 * @generated
	 * @ordered
	 */
	protected EClassifier targetClassifier;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferencePatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RulesPackage.Literals.REFERENCE_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReference() {
		if (reference != null && reference.eIsProxy()) {
			InternalEObject oldReference = (InternalEObject)reference;
			reference = (EReference)eResolveProxy(oldReference);
			if (reference != oldReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RulesPackage.REFERENCE_PATTERN__REFERENCE, oldReference, reference));
			}
		}
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetReference() {
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReference(EReference newReference) {
		EReference oldReference = reference;
		reference = newReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.REFERENCE_PATTERN__REFERENCE, oldReference, reference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier getSourceClassifier() {
		if (sourceClassifier != null && sourceClassifier.eIsProxy()) {
			InternalEObject oldSourceClassifier = (InternalEObject)sourceClassifier;
			sourceClassifier = (EClassifier)eResolveProxy(oldSourceClassifier);
			if (sourceClassifier != oldSourceClassifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RulesPackage.REFERENCE_PATTERN__SOURCE_CLASSIFIER, oldSourceClassifier, sourceClassifier));
			}
		}
		return sourceClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier basicGetSourceClassifier() {
		return sourceClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceClassifier(EClassifier newSourceClassifier) {
		EClassifier oldSourceClassifier = sourceClassifier;
		sourceClassifier = newSourceClassifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.REFERENCE_PATTERN__SOURCE_CLASSIFIER, oldSourceClassifier, sourceClassifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier getTargetClassifier() {
		if (targetClassifier != null && targetClassifier.eIsProxy()) {
			InternalEObject oldTargetClassifier = (InternalEObject)targetClassifier;
			targetClassifier = (EClassifier)eResolveProxy(oldTargetClassifier);
			if (targetClassifier != oldTargetClassifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RulesPackage.REFERENCE_PATTERN__TARGET_CLASSIFIER, oldTargetClassifier, targetClassifier));
			}
		}
		return targetClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier basicGetTargetClassifier() {
		return targetClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetClassifier(EClassifier newTargetClassifier) {
		EClassifier oldTargetClassifier = targetClassifier;
		targetClassifier = newTargetClassifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.REFERENCE_PATTERN__TARGET_CLASSIFIER, oldTargetClassifier, targetClassifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RulesPackage.REFERENCE_PATTERN__REFERENCE:
				if (resolve) return getReference();
				return basicGetReference();
			case RulesPackage.REFERENCE_PATTERN__SOURCE_CLASSIFIER:
				if (resolve) return getSourceClassifier();
				return basicGetSourceClassifier();
			case RulesPackage.REFERENCE_PATTERN__TARGET_CLASSIFIER:
				if (resolve) return getTargetClassifier();
				return basicGetTargetClassifier();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RulesPackage.REFERENCE_PATTERN__REFERENCE:
				setReference((EReference)newValue);
				return;
			case RulesPackage.REFERENCE_PATTERN__SOURCE_CLASSIFIER:
				setSourceClassifier((EClassifier)newValue);
				return;
			case RulesPackage.REFERENCE_PATTERN__TARGET_CLASSIFIER:
				setTargetClassifier((EClassifier)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RulesPackage.REFERENCE_PATTERN__REFERENCE:
				setReference((EReference)null);
				return;
			case RulesPackage.REFERENCE_PATTERN__SOURCE_CLASSIFIER:
				setSourceClassifier((EClassifier)null);
				return;
			case RulesPackage.REFERENCE_PATTERN__TARGET_CLASSIFIER:
				setTargetClassifier((EClassifier)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RulesPackage.REFERENCE_PATTERN__REFERENCE:
				return reference != null;
			case RulesPackage.REFERENCE_PATTERN__SOURCE_CLASSIFIER:
				return sourceClassifier != null;
			case RulesPackage.REFERENCE_PATTERN__TARGET_CLASSIFIER:
				return targetClassifier != null;
		}
		return super.eIsSet(featureID);
	}

} //ReferencePatternImpl
