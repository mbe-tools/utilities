package de.hpi.sam.mote.util;

import org.eclipse.emf.common.notify.Notification;

public interface MoteNotification extends Notification
{

	int	RULE_EXECUTED				= 11;

	int	TRANSFORMATION_QUEUE_POP	= 12;

	int	TRANSFORMATION_QUEUE_TOP	= 13;

}
