/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.helpers;

import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.mote.TGGNode;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Modification Tag</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A ModificationTag contains the correspondence node where a modification occured. The flag 'synchronize' indicates whether existing element should be synchronized or new elements should be transformed. If synchronize is true, the consistency of the elements connected to the correspondence node is checked. Otherwise, new elements below the correspondence node are sought.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.helpers.ModificationTag#getCorrespondenceNode <em>Correspondence Node</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.ModificationTag#isSynchronize <em>Synchronize</em>}</li>
 * </ul>
 *
 * @see de.hpi.sam.mote.helpers.HelpersPackage#getModificationTag()
 * @model
 * @generated
 */
public interface ModificationTag extends EObject {
	/**
	 * Returns the value of the '<em><b>Correspondence Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correspondence Node</em>' reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correspondence Node</em>' reference.
	 * @see #setCorrespondenceNode(TGGNode)
	 * @see de.hpi.sam.mote.helpers.HelpersPackage#getModificationTag_CorrespondenceNode()
	 * @model resolveProxies="false" ordered="false"
	 * @generated
	 */
	TGGNode getCorrespondenceNode();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.helpers.ModificationTag#getCorrespondenceNode <em>Correspondence Node</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Correspondence Node</em>' reference.
	 * @see #getCorrespondenceNode()
	 * @generated
	 */
	void setCorrespondenceNode(TGGNode value);

	/**
	 * Returns the value of the '<em><b>Synchronize</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synchronize</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Synchronize</em>' attribute.
	 * @see #setSynchronize(boolean)
	 * @see de.hpi.sam.mote.helpers.HelpersPackage#getModificationTag_Synchronize()
	 * @model
	 * @generated
	 */
	boolean isSynchronize();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.helpers.ModificationTag#isSynchronize <em>Synchronize</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Synchronize</em>' attribute.
	 * @see #isSynchronize()
	 * @generated
	 */
	void setSynchronize(boolean value);

} // ModificationTag
