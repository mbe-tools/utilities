/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.mote.rules.TGGMapping;
import de.hpi.sam.mote.rules.TGGRuleSet;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>TGG Node</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.TGGNode#getNext <em>Next</em>}</li>
 *   <li>{@link de.hpi.sam.mote.TGGNode#getPrevious <em>Previous</em>}</li>
 *   <li>{@link de.hpi.sam.mote.TGGNode#getRuleSet <em>Rule Set</em>}</li>
 *   <li>{@link de.hpi.sam.mote.TGGNode#getCreationRule <em>Creation Rule</em>}</li>
 *   <li>{@link de.hpi.sam.mote.TGGNode#getSources <em>Sources</em>}</li>
 *   <li>{@link de.hpi.sam.mote.TGGNode#getTargets <em>Targets</em>}</li>
 * </ul>
 *
 * @see de.hpi.sam.mote.MotePackage#getTGGNode()
 * @model abstract="true"
 * @generated
 */
public interface TGGNode extends EObject {
	/**
	 * Returns the value of the '<em><b>Next</b></em>' reference list.
	 * The list contents are of type {@link de.hpi.sam.mote.TGGNode}.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.mote.TGGNode#getPrevious <em>Previous</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next</em>' reference list isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next</em>' reference list.
	 * @see de.hpi.sam.mote.MotePackage#getTGGNode_Next()
	 * @see de.hpi.sam.mote.TGGNode#getPrevious
	 * @model opposite="previous" resolveProxies="false" ordered="false"
	 * @generated
	 */
	EList<TGGNode> getNext();

	/**
	 * Returns the value of the '<em><b>Previous</b></em>' reference list. The
	 * list contents are of type {@link de.hpi.sam.mote.TGGNode}. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.mote.TGGNode#getNext <em>Next</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Previous</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Previous</em>' reference list.
	 * @see de.hpi.sam.mote.MotePackage#getTGGNode_Previous()
	 * @see de.hpi.sam.mote.TGGNode#getNext
	 * @model opposite="next" resolveProxies="false" ordered="false"
	 * @generated
	 */
	EList<TGGNode> getPrevious();

	/**
	 * Returns the value of the '<em><b>Rule Set</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Set</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Rule Set</em>' reference.
	 * @see #setRuleSet(TGGRuleSet)
	 * @see de.hpi.sam.mote.MotePackage#getTGGNode_RuleSet()
	 * @model resolveProxies="false" ordered="false"
	 * @generated
	 */
	TGGRuleSet getRuleSet();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.TGGNode#getRuleSet
	 * <em>Rule Set</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Rule Set</em>' reference.
	 * @see #getRuleSet()
	 * @generated
	 */
	void setRuleSet(TGGRuleSet value);

	/**
	 * Returns the value of the '<em><b>Creation Rule</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.mote.rules.TGGMapping#getCreatedCorrNodes <em>Created Corr Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Creation Rule</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Creation Rule</em>' reference.
	 * @see #setCreationRule(TGGMapping)
	 * @see de.hpi.sam.mote.MotePackage#getTGGNode_CreationRule()
	 * @see de.hpi.sam.mote.rules.TGGMapping#getCreatedCorrNodes
	 * @model opposite="createdCorrNodes" resolveProxies="false" ordered="false"
	 * @generated
	 */
	TGGMapping getCreationRule();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.TGGNode#getCreationRule <em>Creation Rule</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Creation Rule</em>' reference.
	 * @see #getCreationRule()
	 * @generated
	 */
	void setCreationRule(TGGMapping value);

	/**
	 * Returns the value of the '<em><b>Sources</b></em>' reference list. The
	 * list contents are of type {@link org.eclipse.emf.ecore.EObject}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sources</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Sources</em>' reference list.
	 * @see de.hpi.sam.mote.MotePackage#getTGGNode_Sources()
	 * @model
	 * @generated
	 */
	EList<EObject> getSources();

	/**
	 * Returns the value of the '<em><b>Targets</b></em>' reference list. The
	 * list contents are of type {@link org.eclipse.emf.ecore.EObject}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Targets</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Targets</em>' reference list.
	 * @see de.hpi.sam.mote.MotePackage#getTGGNode_Targets()
	 * @model
	 * @generated
	 */
	EList<EObject> getTargets();

} // TGGNode
