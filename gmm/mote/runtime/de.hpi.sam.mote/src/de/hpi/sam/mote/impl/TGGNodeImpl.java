/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.mote.MotePackage;
import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.mote.rules.TGGMapping;
import de.hpi.sam.mote.rules.TGGRuleSet;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>TGG Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.impl.TGGNodeImpl#getNext <em>Next</em>}</li>
 *   <li>{@link de.hpi.sam.mote.impl.TGGNodeImpl#getPrevious <em>Previous</em>}</li>
 *   <li>{@link de.hpi.sam.mote.impl.TGGNodeImpl#getRuleSet <em>Rule Set</em>}</li>
 *   <li>{@link de.hpi.sam.mote.impl.TGGNodeImpl#getCreationRule <em>Creation Rule</em>}</li>
 *   <li>{@link de.hpi.sam.mote.impl.TGGNodeImpl#getSources <em>Sources</em>}</li>
 *   <li>{@link de.hpi.sam.mote.impl.TGGNodeImpl#getTargets <em>Targets</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class TGGNodeImpl extends EObjectImpl implements TGGNode {
	/**
	 * The cached value of the '{@link #getNext() <em>Next</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNext()
	 * @generated
	 * @ordered
	 */
	protected EList<TGGNode> next;

	/**
	 * The cached value of the '{@link #getPrevious() <em>Previous</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPrevious()
	 * @generated
	 * @ordered
	 */
	protected EList<TGGNode> previous;

	/**
	 * The cached value of the '{@link #getRuleSet() <em>Rule Set</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuleSet()
	 * @generated
	 * @ordered
	 */
	protected TGGRuleSet ruleSet;

	/**
	 * The cached value of the '{@link #getCreationRule() <em>Creation Rule</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCreationRule()
	 * @generated
	 * @ordered
	 */
	protected TGGMapping creationRule;

	/**
	 * The cached value of the '{@link #getSources() <em>Sources</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSources()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> sources;

	/**
	 * The cached value of the '{@link #getTargets() <em>Targets</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTargets()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> targets;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected TGGNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MotePackage.Literals.TGG_NODE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TGGNode> getNext() {
		if (next == null) {
			next = new EObjectWithInverseEList.ManyInverse<TGGNode>(TGGNode.class, this, MotePackage.TGG_NODE__NEXT, MotePackage.TGG_NODE__PREVIOUS);
		}
		return next;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TGGNode> getPrevious() {
		if (previous == null) {
			previous = new EObjectWithInverseEList.ManyInverse<TGGNode>(TGGNode.class, this, MotePackage.TGG_NODE__PREVIOUS, MotePackage.TGG_NODE__NEXT);
		}
		return previous;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGRuleSet getRuleSet() {
		return ruleSet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleSet(TGGRuleSet newRuleSet) {
		TGGRuleSet oldRuleSet = ruleSet;
		ruleSet = newRuleSet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MotePackage.TGG_NODE__RULE_SET, oldRuleSet, ruleSet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGMapping getCreationRule() {
		return creationRule;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCreationRule(TGGMapping newCreationRule,
			NotificationChain msgs) {
		TGGMapping oldCreationRule = creationRule;
		creationRule = newCreationRule;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MotePackage.TGG_NODE__CREATION_RULE, oldCreationRule, newCreationRule);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreationRule(TGGMapping newCreationRule) {
		if (newCreationRule != creationRule) {
			NotificationChain msgs = null;
			if (creationRule != null)
				msgs = ((InternalEObject)creationRule).eInverseRemove(this, RulesPackage.TGG_MAPPING__CREATED_CORR_NODES, TGGMapping.class, msgs);
			if (newCreationRule != null)
				msgs = ((InternalEObject)newCreationRule).eInverseAdd(this, RulesPackage.TGG_MAPPING__CREATED_CORR_NODES, TGGMapping.class, msgs);
			msgs = basicSetCreationRule(newCreationRule, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MotePackage.TGG_NODE__CREATION_RULE, newCreationRule, newCreationRule));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getSources() {
		if (sources == null) {
			sources = new EObjectResolvingEList<EObject>(EObject.class, this, MotePackage.TGG_NODE__SOURCES);
		}
		return sources;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getTargets() {
		if (targets == null) {
			targets = new EObjectResolvingEList<EObject>(EObject.class, this, MotePackage.TGG_NODE__TARGETS);
		}
		return targets;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MotePackage.TGG_NODE__NEXT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getNext()).basicAdd(otherEnd, msgs);
			case MotePackage.TGG_NODE__PREVIOUS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPrevious()).basicAdd(otherEnd, msgs);
			case MotePackage.TGG_NODE__CREATION_RULE:
				if (creationRule != null)
					msgs = ((InternalEObject)creationRule).eInverseRemove(this, RulesPackage.TGG_MAPPING__CREATED_CORR_NODES, TGGMapping.class, msgs);
				return basicSetCreationRule((TGGMapping)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MotePackage.TGG_NODE__NEXT:
				return ((InternalEList<?>)getNext()).basicRemove(otherEnd, msgs);
			case MotePackage.TGG_NODE__PREVIOUS:
				return ((InternalEList<?>)getPrevious()).basicRemove(otherEnd, msgs);
			case MotePackage.TGG_NODE__CREATION_RULE:
				return basicSetCreationRule(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MotePackage.TGG_NODE__NEXT:
				return getNext();
			case MotePackage.TGG_NODE__PREVIOUS:
				return getPrevious();
			case MotePackage.TGG_NODE__RULE_SET:
				return getRuleSet();
			case MotePackage.TGG_NODE__CREATION_RULE:
				return getCreationRule();
			case MotePackage.TGG_NODE__SOURCES:
				return getSources();
			case MotePackage.TGG_NODE__TARGETS:
				return getTargets();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MotePackage.TGG_NODE__NEXT:
				getNext().clear();
				getNext().addAll((Collection<? extends TGGNode>)newValue);
				return;
			case MotePackage.TGG_NODE__PREVIOUS:
				getPrevious().clear();
				getPrevious().addAll((Collection<? extends TGGNode>)newValue);
				return;
			case MotePackage.TGG_NODE__RULE_SET:
				setRuleSet((TGGRuleSet)newValue);
				return;
			case MotePackage.TGG_NODE__CREATION_RULE:
				setCreationRule((TGGMapping)newValue);
				return;
			case MotePackage.TGG_NODE__SOURCES:
				getSources().clear();
				getSources().addAll((Collection<? extends EObject>)newValue);
				return;
			case MotePackage.TGG_NODE__TARGETS:
				getTargets().clear();
				getTargets().addAll((Collection<? extends EObject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MotePackage.TGG_NODE__NEXT:
				getNext().clear();
				return;
			case MotePackage.TGG_NODE__PREVIOUS:
				getPrevious().clear();
				return;
			case MotePackage.TGG_NODE__RULE_SET:
				setRuleSet((TGGRuleSet)null);
				return;
			case MotePackage.TGG_NODE__CREATION_RULE:
				setCreationRule((TGGMapping)null);
				return;
			case MotePackage.TGG_NODE__SOURCES:
				getSources().clear();
				return;
			case MotePackage.TGG_NODE__TARGETS:
				getTargets().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MotePackage.TGG_NODE__NEXT:
				return next != null && !next.isEmpty();
			case MotePackage.TGG_NODE__PREVIOUS:
				return previous != null && !previous.isEmpty();
			case MotePackage.TGG_NODE__RULE_SET:
				return ruleSet != null;
			case MotePackage.TGG_NODE__CREATION_RULE:
				return creationRule != null;
			case MotePackage.TGG_NODE__SOURCES:
				return sources != null && !sources.isEmpty();
			case MotePackage.TGG_NODE__TARGETS:
				return targets != null && !targets.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	@Override
	public String toString() {
		final StringBuilder strBuild = new StringBuilder( eClass().getName() ); 
		strBuild.append( " Sources: "  + getSources() );
		strBuild.append( " Targets: "  + getTargets() );
		
		return strBuild.toString();
	}
} // TGGNodeImpl
