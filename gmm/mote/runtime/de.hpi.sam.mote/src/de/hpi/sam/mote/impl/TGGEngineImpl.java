/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.provider.EcoreItemProviderAdapterFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;

import de.hpi.sam.mote.MoteEngineCoveragePolicy;
import de.hpi.sam.mote.MoteEngineRelationPolicy;
import de.hpi.sam.mote.MotePackage;
import de.hpi.sam.mote.TGGEngine;
import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.TransformationDirection;
import de.hpi.sam.mote.helpers.HelpersFactory;
import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.helpers.ModificationTag;
import de.hpi.sam.mote.helpers.RuleSetTag;
import de.hpi.sam.mote.helpers.impl.MapEntryImpl;
import de.hpi.sam.mote.rules.ReferencePattern;
import de.hpi.sam.mote.rules.RulesFactory;
import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.mote.rules.TGGRule;
import de.hpi.sam.mote.rules.TGGRuleSet;
import de.hpi.sam.mote.rules.TransformationExecutionResult;
import de.hpi.sam.mote.rules.TransformationQueue;
import de.hpi.sam.mote.rules.TransformationResult;
import de.hpi.sam.mote.util.MoteChangeListener;
import de.hpi.sam.mote.util.MoteNotification;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>TGG Engine</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.impl.TGGEngineImpl#getAvailableRuleSets <em>Available Rule Sets</em>}</li>
 *   <li>{@link de.hpi.sam.mote.impl.TGGEngineImpl#getUsedRuleSets <em>Used Rule Sets</em>}</li>
 *   <li>{@link de.hpi.sam.mote.impl.TGGEngineImpl#isRuntimeChecksEnabled <em>Runtime Checks Enabled</em>}</li>
 *   <li>{@link de.hpi.sam.mote.impl.TGGEngineImpl#isNotificationsEnabled <em>Notifications Enabled</em>}</li>
 *   <li>{@link de.hpi.sam.mote.impl.TGGEngineImpl#getRelationPolicies <em>Relation Policies</em>}</li>
 *   <li>{@link de.hpi.sam.mote.impl.TGGEngineImpl#getCoveragePolicies <em>Coverage Policies</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TGGEngineImpl extends EObjectImpl implements TGGEngine {
	
	private final AdapterFactoryLabelProvider emfLabelProvider = new AdapterFactoryLabelProvider( new EcoreItemProviderAdapterFactory() );

	/**
	 * The cached value of the '{@link #getAvailableRuleSets() <em>Available Rule Sets</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getAvailableRuleSets()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, RuleSetTag> availableRuleSets;

	/**
	 * The cached value of the '{@link #getUsedRuleSets()
	 * <em>Used Rule Sets</em>}' map. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getUsedRuleSets()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, EList<RuleSetTag>> usedRuleSets;

	/**
	 * The default value of the '{@link #isRuntimeChecksEnabled() <em>Runtime Checks Enabled</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isRuntimeChecksEnabled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RUNTIME_CHECKS_ENABLED_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isRuntimeChecksEnabled() <em>Runtime Checks Enabled</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isRuntimeChecksEnabled()
	 * @generated
	 * @ordered
	 */
	protected boolean runtimeChecksEnabled = RUNTIME_CHECKS_ENABLED_EDEFAULT;

	/**
	 * The default value of the '{@link #isNotificationsEnabled() <em>Notifications Enabled</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isNotificationsEnabled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NOTIFICATIONS_ENABLED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNotificationsEnabled() <em>Notifications Enabled</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isNotificationsEnabled()
	 * @generated
	 * @ordered
	 */
	protected boolean notificationsEnabled = NOTIFICATIONS_ENABLED_EDEFAULT;
	
	/**
	 * The cached value of the '{@link #getRelationPolicies() <em>Relation Policies</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationPolicies()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, MoteEngineRelationPolicy> relationPolicies;

	/**
	 * The cached value of the '{@link #getCoveragePolicies() <em>Coverage Policies</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoveragePolicies()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, MoteEngineCoveragePolicy> coveragePolicies;

	private final ResourceSet rulesResourceSet;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected TGGEngineImpl() {
		super();
		
		rulesResourceSet = new ResourceSetImpl();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MotePackage.Literals.TGG_ENGINE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<String, RuleSetTag> getAvailableRuleSets() {
		if (availableRuleSets == null) {
			availableRuleSets = new EcoreEMap<String,RuleSetTag>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, MotePackage.TGG_ENGINE__AVAILABLE_RULE_SETS);
		}
		return availableRuleSets;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<String, EList<RuleSetTag>> getUsedRuleSets() {
		if (usedRuleSets == null) {
			usedRuleSets = new EcoreEMap<String,EList<RuleSetTag>>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, MotePackage.TGG_ENGINE__USED_RULE_SETS);
		}
		return usedRuleSets;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isRuntimeChecksEnabled() {
		return runtimeChecksEnabled;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRuntimeChecksEnabled(boolean newRuntimeChecksEnabled) {
		boolean oldRuntimeChecksEnabled = runtimeChecksEnabled;
		runtimeChecksEnabled = newRuntimeChecksEnabled;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MotePackage.TGG_ENGINE__RUNTIME_CHECKS_ENABLED, oldRuntimeChecksEnabled, runtimeChecksEnabled));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isNotificationsEnabled() {
		return notificationsEnabled;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNotificationsEnabled(boolean newNotificationsEnabled) {
		boolean oldNotificationsEnabled = notificationsEnabled;
		notificationsEnabled = newNotificationsEnabled;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MotePackage.TGG_ENGINE__NOTIFICATIONS_ENABLED, oldNotificationsEnabled, notificationsEnabled));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, MoteEngineRelationPolicy> getRelationPolicies() {
		if (relationPolicies == null) {
			relationPolicies = new EcoreEMap<String,MoteEngineRelationPolicy>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, MotePackage.TGG_ENGINE__RELATION_POLICIES);
		}
		return relationPolicies;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, MoteEngineCoveragePolicy> getCoveragePolicies() {
		if (coveragePolicies == null) {
			coveragePolicies = new EcoreEMap<String,MoteEngineCoveragePolicy>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, MotePackage.TGG_ENGINE__COVERAGE_POLICIES);
		}
		return coveragePolicies;
	}
	
	private boolean isCovered( 	final EObject p_eObject,
								final String p_ruleSetId ) {
		final MoteEngineCoveragePolicy covPolicy = getCoveragePolicies().get( p_ruleSetId );
		
		if ( covPolicy == null ) {
			return true;
		}
		
		return covPolicy.isCovered( p_eObject );
	}

	/**
	 * Initialize the specified rule set with corresponding nodes for the objects located in a resource external
	 * to the specified resource but referred by element(s) of the specified resource. This is essential otherwise
	 * MoTE will not transform objects having references to external objects
	 * @param p_ruleSet
	 * @param p_inputResource
	 * @param p_createdDependencies
	 * @param pb_inputIsLeft
	 * @param p_processedElements
	 * @param p_monitor
	 * @throws TransformationException
	 */
	private void initRuleSetWithExternalObjects( 	final TGGRuleSet p_ruleSet,
													final Resource p_inputResource,
													//final Set<Resource> p_createdDependencies,
													final EMap<Resource, EList<EObject>> p_createdDependencies,
													final boolean pb_inputIsLeft,
													final Set<EObject> p_processedElements,
													final IProgressMonitor p_monitor )
	throws TransformationException {
		final Iterator<EObject> contentIt = p_inputResource.getAllContents();

		// For all contained EObjects
		while ( contentIt.hasNext() ) {
			final EObject element = contentIt.next();
			
			initRuleSetWithExternalObjects( p_ruleSet, element, p_createdDependencies, pb_inputIsLeft, p_processedElements, p_monitor );
		}
	}

	private void initRuleSetWithExternalObjects( 	final TGGRuleSet p_ruleSet,
													final EObject p_element,
													//final Set<Resource> p_createdDependencies,
													final EMap<Resource, EList<EObject>> p_createdDependencies,
													final boolean pb_inputIsLeft,
													final Set<EObject> p_processedElements,
													final IProgressMonitor p_monitor )
	throws TransformationException {
		final String ruleSetId = p_ruleSet.getId();
		final Resource inputResource = p_element.eResource();
			
		if ( p_processedElements.add( p_element ) ) {
			//final EContentsEList.FeatureIterator<EObject> iterator = (EContentsEList.FeatureIterator<EObject>) p_element.eCrossReferences().iterator();
			
			// For all references of this object
			//while ( iterator.hasNext() ) {
				//final EObject object = iterator.next();
				
			// For all references from this object that are not in the same resource, including object referred recursively through other objects
			for ( final Map.Entry<EObject, EList<EReference>> extObjEntry : p_ruleSet.externalReferences( inputResource, p_element, null ).entrySet() ) {
				final EObject extObj = extObjEntry.getKey();
				
				if ( isCovered( extObj, ruleSetId ) && !hasModelElement( extObj, p_ruleSet, pb_inputIsLeft ) ) {
					final TGGNode corrNode = getCorrNode( 	ruleSetId,
															extObj,
															p_createdDependencies,
															pb_inputIsLeft,
															p_processedElements );
					
					if ( corrNode == null ) {
						
						// Check if this external object can be used to create elements. TODO: Temporary fix.
						for ( final ReferencePattern refPattern : p_ruleSet.getExternalRefPatterns() ) {
							if ( 	refPattern.getSourceClassifier().isInstance( p_element ) &&
									refPattern.getTargetClassifier().isInstance( extObj ) ) {
								boolean found = false;
								
								for ( final EReference ref : extObjEntry.getValue() ) {
									if ( ref == refPattern.getReference() ) {
										found = true;
										
										if ( pb_inputIsLeft ) {
											p_ruleSet.getUncoveredSourceModelElements().put( extObj, null );
										}
										else {
											p_ruleSet.getUncoveredTargetModelElements().put( extObj, null );
										}
										
										break;
									}
								}
								
								if ( found ) {
									break;
								}
							}
						}
					}
					else {
						addModelElements( corrNode, p_ruleSet );
					}
				}
			}
		}
	}
	
	private URI correspondingUri( 	final String p_ruleSetId,
									final Resource p_resource ) {
		final MoteEngineRelationPolicy relPolicy = getRelationPolicies().get( p_ruleSetId );
	
		if ( relPolicy == null ) {
			throw new IllegalStateException( "No relation policy defined for rule set id " + p_ruleSetId );//return p_resource.getURI().trimFileExtension().appendFileExtension( p_outFileExt );
		}

		return relPolicy.correspondingUri( p_resource );
	}
	
	/**
	 * Obtain the correspondence node for a given model object. Create the corresponding resource if it 
	 * does not exists, or just create the mapping model otherwise.
	 * @param p_ruleSetId
	 * @param p_extObject
	 * @param p_createdDependencies
	 * @param pb_sourceIsInput
	 * @param p_outFileExt
	 * @param p_processedElements
	 * @return
	 * @throws TransformationException
	 */
	private TGGNode getCorrNode( 	final String p_ruleSetId,
									final EObject p_extObject,
									//final Set<Resource> p_createdDependencies,
									final EMap<Resource, EList<EObject>> p_createdDependencies,
									final boolean pb_sourceIsInput,
									final Set<EObject> p_processedElements )
	throws TransformationException {
		final Resource inputRes = p_extObject.eResource();
		final URI outResUri = correspondingUri( p_ruleSetId, inputRes );

		// If there is no corresponding resource so no correspondence node
		if ( outResUri == null ) {
			return null;
		}
		
		System.out.println( "Searching correspondence node for external object " + emfLabelProvider.getText( p_extObject ) + " of resource " + p_extObject.eResource().getURI() + "." );

		final ResourceSet resSet = inputRes.getResourceSet();
		final URI inpResUri = inputRes.getURI();
		final URI sourceResUri;
		final URI targetResUri;

		if ( pb_sourceIsInput ) {
			sourceResUri = inpResUri;
			targetResUri = outResUri;
		}
		else {
			sourceResUri = outResUri;
			targetResUri = inpResUri;
		}
		
		// Search for the element in a potentially existing rule set for the object's resource
		final TGGRuleSet ruleSet = getUsedRuleSet( p_ruleSetId, sourceResUri, targetResUri, resSet );
		final TGGNode corrNode = findCorrNode( p_extObject, ruleSet, pb_sourceIsInput );
		
		if ( corrNode != null ) {
			return corrNode;
		}

		initRuleSetWithExternalObjects( ruleSet, p_extObject, p_createdDependencies, pb_sourceIsInput, p_processedElements, new NullProgressMonitor() );
		
		// Now create/update the traceability model for the object and return the corresponding node.		
		Resource crossOutRes = resSet.getResource( outResUri, false );
		TransformationDirection transDir = pb_sourceIsInput ? TransformationDirection.FORWARD : TransformationDirection.REVERSE;
		final boolean resExists = resSet.getURIConverter().exists( outResUri, null );
		
		// If the corresponding resource has not been created yet.
		if ( ( crossOutRes == null || crossOutRes.getContents().isEmpty() ) && !resExists ) {
			if ( crossOutRes == null ) {
				crossOutRes = resSet.createResource( outResUri );
			}

			if ( !p_createdDependencies.containsKey( crossOutRes ) ) {
				p_createdDependencies.put( crossOutRes, new BasicEList<EObject>() );
			}
//			p_createdDependencies.add( crossOutRes );
		}
		else {
			if ( resExists ) {
				transDir = TransformationDirection.MAPPING;
			}
			
			crossOutRes = resSet.getResource( outResUri, true );
		}
		
		final Resource sourceResource;
		final Resource targetResource;
			
		if ( pb_sourceIsInput ) {
			sourceResource = inputRes;
			targetResource = crossOutRes;
		}
		else {
			sourceResource = crossOutRes;
			targetResource = inputRes;
		}

		// Execute a transformation to create the resource if it does not exists. Otherwise, just create
		// the mapping model so that the required correspondence nodes are created.
		
		final EObject container = p_extObject.eContainer();
		
		if ( container != null ) {
			final TGGNode parentCorrNode = findCorrNode( container, ruleSet, pb_sourceIsInput );
			
			if ( parentCorrNode != null ) {
				final ModificationTag modTag = HelpersFactory.eINSTANCE.createModificationTag();
				modTag.setCorrespondenceNode( parentCorrNode );
				ruleSet.getTransformationQueue().add( modTag );
			}
		}
		
		System.out.println( new Date() + ": Updating traceability model for external object " + emfLabelProvider.getText( p_extObject ) + " of resource " + inpResUri + "..." );
		
		final TransformationExecutionResult result = updateTraceabilityModel(	sourceResource,
																				targetResource,
																				transDir,
																				ruleSet,
																				new NullProgressMonitor() );
		
		if ( result.getTransformationResult() != TransformationResult.RULE_APPLIED ) {
			throw new TransformationException( 	"Error transforming model for managing references of resource " + 
												inputRes + " towards resource " + crossOutRes + "." );
		}
		
		final EObject execTrace = result.getExecutionTrace();
		
		if ( execTrace != null ) {
			EList<EObject> traces = p_createdDependencies.get( crossOutRes );
			
			if ( traces == null ) {
				traces = new BasicEList<EObject>();
				p_createdDependencies.put( crossOutRes, traces );
			}
			
			traces.add( execTrace );
		}
		
		// Get the corr node from the processed rule set
		final TGGNode foundCorrNode = findCorrNode( p_extObject, ruleSet, pb_sourceIsInput );
		
		if ( foundCorrNode == null ) {
			throw new TransformationException( "Unable to find correspondence node for external object " + emfLabelProvider.getText( p_extObject ) + " of resource " + p_extObject.eResource().getURI() );
		}
		
		return foundCorrNode;
	}
	
	private boolean initRuleSetWithExternalObjects(	final TGGRuleSet p_ruleSet,
													final Resource p_leftResource,
													final Resource p_rightResource,
													final EMap<Resource, EList<EObject>> p_createdDependencies,
													final TransformationDirection p_direction,
													final Set<EObject> p_unmanagedObjects,
													final IProgressMonitor p_monitor )
	throws TransformationException {
		if ( p_direction == TransformationDirection.MAPPING ) {
			initRuleSetWithExternalObjects( p_ruleSet,
											p_leftResource,
											p_createdDependencies,
											true,
											p_unmanagedObjects,
											p_monitor );
			initRuleSetWithExternalObjects( p_ruleSet,
											p_rightResource,
											p_createdDependencies,
											false,
											p_unmanagedObjects,
											p_monitor );
		}
		else {
			final Resource inputRes = p_direction == TransformationDirection.FORWARD ? p_leftResource : p_rightResource;
			initRuleSetWithExternalObjects( p_ruleSet,
											inputRes,
											p_createdDependencies,
											inputRes == p_leftResource,
											p_unmanagedObjects,
											p_monitor );
		}
		
		return !p_leftResource.getContents().isEmpty() && !p_rightResource.getContents().isEmpty() &&
				p_ruleSet.getRootCorrNode() != null;
	}

	private TransformationResult mapping(	final TGGRuleSet p_ruleSet,
											final Resource p_sourceResource,
											final Resource p_targetResource,
											final boolean pb_forward,
											final Set<EObject> p_unmanagedElements,
											final IProgressMonitor p_monitor )
	throws TransformationException {
		if ( !p_sourceResource.getContents().isEmpty() && !p_targetResource.getContents().isEmpty() ) {
			final String message = "Starting to map " + p_sourceResource.getURI() + " with " + p_targetResource.getURI();
			System.out.println( new Date() + ": " + message );
			
			// Ensure the external elements have correspondence nodes otherwise the current objects
			// that refer to them will not be mapped.
			initRuleSetWithExternalObjects( p_ruleSet,
											pb_forward ? p_sourceResource : p_targetResource,
											null,
											pb_forward,
											p_unmanagedElements,
											p_monitor );
			
			System.out.println( new Date() + ": Starting to match SD rules..." );
			final EObject sourceRoot = p_sourceResource.getContents().get(0);
			p_ruleSet.initializeSourceModel( sourceRoot );
			final EObject targetRoot = p_targetResource.getContents().get(0);
			p_ruleSet.initializeTargetModel( targetRoot );
			
			final TransformationResult result = p_ruleSet.getAxiom().mappingTransformation( sourceRoot, targetRoot );

			if ( result != TransformationResult.RULE_APPLIED ) {
				return result;
			}

			// Execute the transformation rules
			executeTransformationRules( p_ruleSet, TransformationDirection.MAPPING, false, p_monitor );
			System.out.println( new Date() + ": Finished " + message );
		}
		
		return TransformationResult.RULE_APPLIED;
	}
	
	private RuleSetTag getUsedRuleSetTag( 	final String p_ruleSetId,
											final URI p_sourceResUri,
											final URI p_targetResUri,
											final ResourceSet p_resSet ) {
		if ( getAvailableRuleSets().isEmpty() ) {
			initializeEngine();
		}

		/*
		 * Get an existing ruleSet or create a new one.
		 */
		if ( getUsedRuleSets().get( p_ruleSetId ) != null ) {
			// Look in the list of usedRuleSets for a suitable tag
			for ( final RuleSetTag rst : new ArrayList<RuleSetTag>( getUsedRuleSets().get( p_ruleSetId ) ) ) {
				if ( rst.getSourceModelUri().equals( p_sourceResUri ) && rst.getTargetModelUri().equals( p_targetResUri ) ) {
					final TGGRuleSet ruleSet = rst.getRuleSet();
					final TGGNode rootNode = ruleSet.getRootCorrNode();
					
					if ( rootNode == null ) {
						return rst;
					}
					
					if ( rootNode.getSources().isEmpty() || rootNode.getSources().get( 0 ).eResource() == null ) {
						// Remove the tags for which the resource has been deleted.
						getUsedRuleSets().get( p_ruleSetId ).remove( rst );
					}
					else if ( p_resSet == rootNode.getSources().get( 0 ).eResource().getResourceSet() ) {
						return rst;
					}

					if ( rootNode.getTargets().isEmpty() || rootNode.getTargets().get( 0 ).eResource() == null ) {
						// Remove the tags for which the resource has been deleted.
						getUsedRuleSets().get( p_ruleSetId ).remove( rst );
					}
					else if ( p_resSet == rootNode.getTargets().get( 0 ).eResource().getResourceSet() ) {
						return rst;
					}
				}
			}
		}
		else {
			getUsedRuleSets().put( p_ruleSetId, new BasicEList<RuleSetTag>() );
		}
		
		return getAvailableRuleSets().get( p_ruleSetId );
	}
	
	public TGGRuleSet getUsedRuleSet( 	final String p_ruleSetId,
										final URI p_sourceResUri,
										final URI p_targetResUri,
										final ResourceSet p_resSet )
	throws TransformationException {
		RuleSetTag ruleSetTag = getUsedRuleSetTag( p_ruleSetId, p_sourceResUri, p_targetResUri, p_resSet );

		if ( ruleSetTag == null ) {
			throw new TransformationException( "The ruleSetID " + p_ruleSetId + " does not exist." );
		}

		// Initialize the ruleSet if it is not already in use
		if ( ruleSetTag.getRuleSet() == null ) {
			ruleSetTag = initializeRuleSet( ruleSetTag, p_sourceResUri, p_targetResUri );
			getUsedRuleSets().get( p_ruleSetId ).add( ruleSetTag );
		}

		return ruleSetTag.getRuleSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" exceptions="de.hpi.sam.mote.helpers.TransformationException" leftResourceRequired="true" rightResourceRequired="true" dependenciesMany="true"
	 * @generated NOT
	 */
	@Override
	public TransformationExecutionResult transform(	final Resource p_leftResource,
													final Resource p_rightResource,
													final EMap<Resource, EList<EObject>> p_dependencies,
													final TransformationDirection p_transformationDirection,
													final String p_ruleSetID,
													final boolean pb_synchronize )
	throws TransformationException {
		return transform( p_leftResource, p_rightResource, p_dependencies, p_transformationDirection, p_ruleSetID, pb_synchronize, new NullProgressMonitor() );
	}
	
	private void validateTransformParameters(	final Resource p_leftResource,
												final Resource p_rightResource,
												final TransformationDirection p_transformationDirection,
												final String p_ruleSetID )
	throws TransformationException {
		if ( p_leftResource == null ) {
			throw new TransformationException("sourceResource is null.", null);
		}
		else if (p_rightResource == null) {
			throw new TransformationException("targetResource is null.", null);
		}
		else if ( p_transformationDirection == null ) {
			throw new TransformationException("The parameter 'transformationDirection' may not be null.",
					null);
		}
		else if ( p_ruleSetID == null ) {
			throw new TransformationException("The parameter 'ruleSetID' may not be null.", null);
		}
	}
	
	public TransformationExecutionResult createTransformationExecutionResult( 	final TransformationResult p_result ) {
		return createTransformationExecutionResult( p_result, null );
	}
	
	private TransformationExecutionResult createTransformationExecutionResult( 	final TransformationResult p_result,
																				final EObject p_executionTrace ) {
		return createTransformationExecutionResult( p_result, p_executionTrace, null, null );
	}
	
	private TransformationExecutionResult createTransformationExecutionResult( 	final TransformationResult p_result,
																				final EObject p_executionTrace,
																				final Collection<EObject> p_leftUncoveredElements,
																				final Collection<EObject> p_rightUncoveredElements ) {
		final TransformationExecutionResult result = RulesFactory.eINSTANCE.createTransformationExecutionResult();
		result.setTransformationResult( p_result );
		result.setExecutionTrace( p_executionTrace );
		
		if ( p_leftUncoveredElements !=  null ) {
			result.getLeftUncoveredElements().addAll( p_leftUncoveredElements );
		}
		
		if ( p_rightUncoveredElements !=  null ) {
			result.getRightUncoveredElements().addAll( p_rightUncoveredElements );
		}
		
		return result;
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public TransformationExecutionResult transform(	final Resource p_leftResource,
													final Resource p_rightResource,
													final EMap<Resource, EList<EObject>> p_dependencies,
													final TransformationDirection p_transformationDirection,
													final String p_ruleSetID,
													final boolean pb_synchronize, 
													final IProgressMonitor p_monitor )
	throws TransformationException {
		
		// Check parameters
		validateTransformParameters( p_leftResource, p_rightResource, p_transformationDirection, p_ruleSetID );
		
		final StringBuilder strBuild = new StringBuilder( new Date().toString() + ": " );
		
		if ( pb_synchronize ) {
			strBuild.append( "Synchronizing " );
		}
		else {
			strBuild.append( "Creating " );
		}
		
		if ( TransformationDirection.FORWARD.equals( p_transformationDirection ) ) {
			strBuild.append( p_rightResource.getURI() );
			strBuild.append( " from " );
			strBuild.append( p_leftResource.getURI() );
		}
		else {
			strBuild.append( p_leftResource.getURI() );
			strBuild.append( " from " );
			strBuild.append( p_rightResource.getURI() );
		}
		
		strBuild.append( "." );
		
		System.out.println( strBuild.toString() );

		final TGGRuleSet ruleSet = getUsedRuleSet( p_ruleSetID, p_leftResource.getURI(), p_rightResource.getURI(), p_leftResource.getResourceSet() );
		
		// Check if resource contain elements
		if ( pb_synchronize	|| p_transformationDirection == TransformationDirection.FORWARD	|| p_transformationDirection == TransformationDirection.MAPPING ) {
			if ( p_leftResource.getContents().isEmpty() ) {
				resourceDeleted( p_rightResource );
				p_rightResource.unload();
				p_rightResource.getResourceSet().getResources().remove( p_rightResource );
				
				return createTransformationExecutionResult( TransformationResult.RULE_APPLIED );
			}
		}

		if ( pb_synchronize	|| p_transformationDirection == TransformationDirection.MAPPING	|| p_transformationDirection == TransformationDirection.REVERSE ) {
			if ( p_rightResource.getContents().isEmpty() ) {
				resourceDeleted( p_leftResource );
				p_leftResource.unload();
				p_leftResource.getResourceSet().getResources().remove( p_leftResource );

				return createTransformationExecutionResult( TransformationResult.RULE_APPLIED );
			}
		}
		
		ruleSet.transformationStarted( p_leftResource, p_rightResource, p_transformationDirection, pb_synchronize );

		if ( pb_synchronize ) {
			if ( ruleSet.getRootCorrNode() == null ) {
				// The rule set should have been created before through a call of checkConsistency.
				throw new InexistentCorrespModelException( 	p_leftResource,
															p_rightResource,
															p_ruleSetID,
															"No correspondence model exists to synchronize the models." );
			}
		}
		else {
			/*
			 * If this is a transformation, the correspondence model and the
			 * target model of the transformation must be discarded.
			 */
			ruleSet.clear();
		}

		final EMap<Resource, EList<EObject>> createdDependencies = new BasicEMap<Resource, EList<EObject>>();
		final boolean synchronizeAfterInitExternal = initRuleSetWithExternalObjects( 	ruleSet,
																						p_leftResource,
																						p_rightResource,
																						createdDependencies,
																						p_transformationDirection,
																						new HashSet<EObject>(),
																						SubMonitor.convert( p_monitor, 100 ) );

		final TransformationResult result = executeAllRules( 	p_leftResource,
																p_rightResource,
																p_transformationDirection,
																synchronizeAfterInitExternal,
																ruleSet,
																p_monitor );
		
		final EObject execTrace = ruleSet.transformationFinished( p_leftResource, p_rightResource, p_transformationDirection, synchronizeAfterInitExternal );
		
//		if ( result == TransformationResult.RULE_NOT_APPLIED ) {
//			return createTransformationExecutionResult( result, execTrace );
//		}
		
		if ( p_transformationDirection != TransformationDirection.MAPPING ) {
			
			// We need to ensure that the dependences are completely transformed in case they depend on 
			// elements of the current resource
			final Resource outRes;
			
			if ( p_transformationDirection == TransformationDirection.FORWARD ) {
				outRes = p_rightResource;
			}
			else {
				outRes = p_leftResource;
			}

			final Iterator<Map.Entry<Resource, EList<EObject>>> createdDepIt = createdDependencies.entrySet().iterator();
			
			while ( createdDepIt.hasNext() ) {
				Map.Entry<Resource, EList<EObject>> entry = createdDepIt.next();
				final Resource depRes = entry.getKey();
				
				
				if ( depRes == outRes ) {
					createdDepIt.remove();
				}
				else {
					final URI inpResUri = correspondingUri( p_ruleSetID, depRes );
	
					if ( inpResUri != null ) {
						final Resource leftDepRes;
						final Resource rightDepRes;
						final ResourceSet resSet = depRes.getResourceSet(); 
						
						if ( p_transformationDirection == TransformationDirection.FORWARD ) {
							leftDepRes = resSet.getResource( inpResUri, false );
							rightDepRes = depRes;
						}
						else {
							leftDepRes = depRes;
							rightDepRes = resSet.getResource( inpResUri, false );
						}
	
						final TransformationExecutionResult depResult = transform( 	leftDepRes,
																					rightDepRes,
																					p_dependencies,
																					p_transformationDirection,
																					p_ruleSetID,
																					true,
																					SubMonitor.convert( p_monitor, 100 ) );
						
						if ( depResult.getTransformationResult() == TransformationResult.RULE_NOT_APPLIED ) {
							return depResult;
						}
					}

					EList<EObject> actualTraces = p_dependencies.get( depRes );
					
					if ( actualTraces == null ) {
						actualTraces = new BasicEList<EObject>();
						p_dependencies.put( depRes, actualTraces );
					}
					
					actualTraces.addAll( entry.getValue() );
				}
			}
	
			//p_dependencies.addAll( createdDependencies );
		}
		
		return createTransformationExecutionResult( result, execTrace);
	}
	
	private TransformationResult executeAllRules( 	final Resource p_leftResource,
													final Resource p_rightResource,
													final TransformationDirection p_transformationDirection,
													final boolean pb_synchronize,
													final TGGRuleSet p_ruleSet,
													final IProgressMonitor p_monitor )
	throws TransformationException {

		// Execute the axiom to create the root elements
		if ( !pb_synchronize ) {
			p_ruleSet.setProcessNotifications( false );

			if ( p_transformationDirection == TransformationDirection.FORWARD ) {
				final EObject leftRoot = p_leftResource.getContents().get( 0 );
				p_ruleSet.initializeSourceModel( leftRoot );
				final TransformationResult result = p_ruleSet.getAxiom().forwardTransformation( leftRoot );
				
				if ( result != TransformationResult.RULE_APPLIED ) {
					return result;
				}

				final EObject rightRoot = p_ruleSet.getTargetModelRootNode();
				p_rightResource.getContents().add( rightRoot );
				p_ruleSet.initializeTargetModel( rightRoot );
			} 
			else if ( p_transformationDirection == TransformationDirection.MAPPING ) {
				final EObject leftRoot = p_leftResource.getContents().get( 0 );
				p_ruleSet.initializeSourceModel( leftRoot );
				final EObject rightRoot = p_rightResource.getContents().get( 0 );
				p_ruleSet.initializeTargetModel( rightRoot );
				
				final TransformationResult result = p_ruleSet.getAxiom().mappingTransformation( leftRoot, rightRoot );

				if ( result != TransformationResult.RULE_APPLIED ) {
					return result;
				}
			}
			else {//if (transformationDirection == TransformationDirection.REVERSE) {
				final EObject rightRoot = p_rightResource.getContents().get( 0 );
				p_ruleSet.initializeTargetModel( rightRoot );
				final TransformationResult result = p_ruleSet.getAxiom().reverseTransformation( rightRoot );

				if ( result != TransformationResult.RULE_APPLIED ) {
					return result;
				}

				final EObject leftRoot = p_ruleSet.getSourceModelRootNode();
				p_leftResource.getContents().add( leftRoot );
				p_ruleSet.initializeSourceModel( leftRoot );
			}

			eNotify(new ENotificationImpl(this, MoteNotification.RULE_EXECUTED, MotePackage.TGG_ENGINE, null, p_ruleSet.getAxiom()));
			p_ruleSet.setProcessNotifications(true);
		}
	
		final StringBuilder message = new StringBuilder( new Date().toString() );
		message.append( ": Starting to execute transformation rules " );
		
		if ( p_transformationDirection == TransformationDirection.FORWARD ) {
			message.append( "from " );
			message.append( p_leftResource.getURI() );
			message.append( " to " );
			message.append( p_rightResource.getURI() );
		}
		else if ( p_transformationDirection == TransformationDirection.REVERSE ) {
			message.append( "from " );
			message.append( p_rightResource.getURI() );
			message.append( " to " );
			message.append( p_leftResource.getURI() );
		}
		else {
			message.append( "between " );
			message.append( p_leftResource.getURI() );
			message.append( " and " );
			message.append( p_rightResource.getURI() );
		}

		message.append( "..." );
		
		System.out.println( message.toString() );

		// Execute the actual transformation or synchronization now
		executeTransformationRules( p_ruleSet, p_transformationDirection, pb_synchronize, p_monitor );

		System.out.println( new Date() + ": Ended executing transformation rules..." );

//		p_ruleSet.transformationFinished( p_leftResource, p_rightResource, p_transformationDirection, pb_synchronize );
		
		return TransformationResult.RULE_APPLIED;
	}
	
	private void clearRuleSet( 	final TGGRuleSet p_ruleSet,
								final URI p_sourceResourceUri ) {
		// Do not clear external elements
		final Collection<TGGNode> corrNodes = new HashSet<TGGNode>();
		
		for ( final TGGNode node : p_ruleSet.getSourceModelElements().values() ) {
			for ( final EObject srcEleme : node.getSources() ) {
				if ( srcEleme.eResource() != null && !p_sourceResourceUri.equals( srcEleme.eResource().getURI() ) ) {
					corrNodes.add( node );
					
					break;
				}
			}
		}

		p_ruleSet.clear();
		
		for ( final TGGNode node : corrNodes ) {
			addModelElements( node, p_ruleSet );
		}
	}
	
	private TransformationExecutionResult updateTraceabilityModel(	final Resource p_leftResource,
																	final Resource p_rightResource,
																	final TransformationDirection p_transformationDirection,
																	final TGGRuleSet p_ruleSet,
																	final IProgressMonitor p_monitor )
	throws TransformationException {
		final boolean synchronize = !p_leftResource.getContents().isEmpty() && !p_rightResource.getContents().isEmpty() && 
									p_ruleSet.getRootCorrNode() != null;

		p_ruleSet.transformationStarted( p_leftResource, p_rightResource, p_transformationDirection, synchronize);
		
		if ( !synchronize ) {
			clearRuleSet( p_ruleSet, p_leftResource.getURI() );
		}

		final TransformationResult result = executeAllRules( p_leftResource, p_rightResource, p_transformationDirection, synchronize, p_ruleSet, p_monitor );
		final EObject execTrace = p_ruleSet.transformationFinished( p_leftResource, p_rightResource, p_transformationDirection, synchronize );
		
		return createTransformationExecutionResult( result, execTrace );
	}

	private TGGRuleSet createRuleSet(	final String p_ruleSetId,
										final String p_ruleSetPackageNsURI)
	throws TransformationException {
		// Get the ruleSet package
		final EPackage ruleSetPackage = Registry.INSTANCE
				.getEPackage(p_ruleSetPackageNsURI);

		// Search for a class that is derived from TGGRuleSet
		EClass ruleSetEclass = null;

		for (final EClassifier ec : ruleSetPackage.getEClassifiers()) {
			if (ec instanceof EClass) {
				if (((EClass) ec).getEAllSuperTypes().contains(
						RulesPackage.Literals.TGG_RULE_SET)) {
					ruleSetEclass = (EClass) ec;
					break;
				}
			}
		}

		if (ruleSetEclass == null) {
			throw new TransformationException(
					"Could not find a class inheriting from TGGRuleSet in package "
							+ ruleSetPackage);
		}

		// Get the ruleSet factory
		final EFactory ruleSetFactory = ruleSetPackage.getEFactoryInstance();

		// Create a new ruleSet
		final TGGRuleSet ruleSet = (TGGRuleSet) ruleSetFactory
				.create(ruleSetEclass);

		if ( ruleSet == null ) {
			throw new TransformationException( "The ruleSet with class " + ruleSetEclass + " could not be created." );
		}

		ruleSet.setResourceSet( rulesResourceSet );
		ruleSet.setId( p_ruleSetId );
		ruleSet.initializeRuleSet();

		return ruleSet;
	}
	
	private boolean hasModelElement( 	final EObject p_object,
										final TGGRuleSet p_ruleSet,
										final boolean pb_searchFromLeft ) {
		return findCorrNode( p_object, p_ruleSet, pb_searchFromLeft ) != null; 
	}

	/**
	 * @param p_object
	 * @param p_ruleSet
	 * @param p_direction
	 * @return
	 */
	private TGGNode findCorrNode(	final EObject p_object,
									final TGGRuleSet p_ruleSet,
									final boolean pb_searchFromLeft ) {
		final EMap<EObject, TGGNode> elements = pb_searchFromLeft ? p_ruleSet.getSourceModelElements() : p_ruleSet.getTargetModelElements();

		for ( final Map.Entry<EObject, TGGNode> entry : elements.entrySet() ) {
			if ( p_object == entry.getKey() ) {
				return entry.getValue();
			}
		}

		return null;
	}
	
	private void addModelElements(	final TGGNode p_corrNode,
									final TGGRuleSet p_ruleSet ) {
		final TGGNode corrNodeCopy = EcoreUtil.copy( p_corrNode );
		
		for ( final EObject sourceObj : corrNodeCopy.getSources() ) {
			p_ruleSet.getSourceModelElements().put( sourceObj, corrNodeCopy );
		}

		for ( final EObject targetObj : corrNodeCopy.getTargets() ) {
			p_ruleSet.getTargetModelElements().put( targetObj, corrNodeCopy );
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void initializeEngine() {
		// Populate list of available ruleSets
		final IExtensionRegistry extensionRegistry = Platform.getExtensionRegistry();

		org.eclipse.core.runtime.IConfigurationElement[] extensions = extensionRegistry
				.getConfigurationElementsFor(de.hpi.sam.mote.TGGConstants.MOTE_RULESET_EXTENSION__EXTENSION_NAME);

		for (org.eclipse.core.runtime.IConfigurationElement configurationElement : extensions) {
			RuleSetTag tag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE
					.createRuleSetTag();

			tag.setRuleSetID(configurationElement
					.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_RULESET_EXTENSION__RULE_SET_ID));
			tag.setRuleSetName(configurationElement
					.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_RULESET_EXTENSION__RULE_SET_NAME));
			tag.setRuleSetPackageNsURI(configurationElement
					.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_RULESET_EXTENSION__PACKAGE_NS_URI));
			tag.setSourceModelID(configurationElement
					.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_RULESET_EXTENSION__SOURCE_MODEL_ID));
			tag.setTargetModelID(configurationElement
					.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_RULESET_EXTENSION__TARGET_MODEL_ID));

			// Check that all fields are not empty, that id is not in use and
			// add the ruleSetTag to the list of tags
			if (tag.getRuleSetID() != null && !"".equals(tag.getRuleSetID())
					&& tag.getRuleSetName() != null
					&& !"".equals(tag.getRuleSetName())
					&& tag.getRuleSetPackageNsURI() != null
					&& !"".equals(tag.getRuleSetPackageNsURI())
					&& tag.getSourceModelID() != null
					&& !"".equals(tag.getSourceModelID())
					&& tag.getTargetModelID() != null
					&& !"".equals(tag.getTargetModelID())) {
				this.getAvailableRuleSets().put(tag.getRuleSetID(), tag);
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public RuleSetTag initializeRuleSet(	final RuleSetTag ruleSetTag, 
											final URI sourceUri,
											final URI targetUri )
	throws TransformationException {
		/*
		 * First make a copy of the ruleSetTag
		 */
		RuleSetTag newTag = EcoreUtil.copy(ruleSetTag);
		final TGGRuleSet newRuleSet = createRuleSet( ruleSetTag.getRuleSetID(), newTag.getRuleSetPackageNsURI() );

		newTag.setSourceModelUri(sourceUri);
		newTag.setTargetModelUri(targetUri);

		newTag.setRuleSet(newRuleSet);

		return newTag;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void executeTransformationRules(final TGGRuleSet ruleSet,
			TransformationDirection transformationDirection,
			boolean synchronize, IProgressMonitor monitor)
			throws TransformationException {
		ruleSet.setProcessNotifications(false);

		final SubMonitor subMonitor = SubMonitor.convert(monitor, ruleSet.getUncoveredSourceModelElements().size() + ruleSet.getUncoveredTargetModelElements().size());

		Adapter ruleSetProgressAdapter = new AdapterImpl() {
			@Override
			public void notifyChanged(Notification msg) {
				if (msg.getFeature() == RulesPackage.Literals.TGG_RULE_SET__CORRESPONDENCE_NODES) {
					subMonitor.worked(2);

					subMonitor.setWorkRemaining(ruleSet.getUncoveredSourceModelElements().size() + ruleSet.getUncoveredTargetModelElements().size());
				}
			}
		};

		if (monitor != null && !(monitor instanceof NullProgressMonitor)) {
			ruleSet.eAdapters().add(ruleSetProgressAdapter);
		}
		/*
		 * If the transformation queue of the ruleSet is empty, perform a batch
		 * transformation
		 */
		boolean batchMode = false;

		if (ruleSet.getTransformationQueue().isEmpty()) {
			batchMode = true;

			de.hpi.sam.mote.helpers.ModificationTag modTag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE.createModificationTag();
			modTag.setCorrespondenceNode(ruleSet.getRootCorrNode());
			ruleSet.getTransformationQueue().add(modTag);

			/*
			 * In case of synchronization, add all direct children of the root
			 * node to the queue.
			 */
			if (synchronize) {
				for (de.hpi.sam.mote.TGGNode n : ruleSet.getRootCorrNode().getNext()) {
					modTag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE.createModificationTag();
					modTag.setCorrespondenceNode(n);
					modTag.setSynchronize(true);
					ruleSet.getTransformationQueue().add(modTag);
				}
			}
		}

		boolean rulesPerformedModifications = true;

		while (rulesPerformedModifications) {
			/*
			 * If any modifications were performed during the previous
			 * synchronization run, add all correspondence nodes to delete to
			 * the queue, and repeat the synchronization run.
			 */
			if (rulesPerformedModifications) {
				rulesPerformedModifications = false;

				for (TGGNode node : ruleSet.getCorrNodesToDelete()) {
					ruleSet.addModificationTagToQueue(node, false, false, true);
				}

				ruleSet.getCorrNodesToDelete().clear();
			}

			while (!ruleSet.getTransformationQueue().isEmpty()) {
				ModificationTag modTag = ruleSet.getTransformationQueue().pop();

				final TGGNode corrNode = modTag.getCorrespondenceNode();

				if (this.isNotificationsEnabled()) {
					this.eNotify(new ENotificationImpl(this, MoteNotification.TRANSFORMATION_QUEUE_TOP,	MotePackage.TGG_ENGINE, corrNode, null));
				}

				/*
				 * Skip correspondence nodes that have been deleted but are
				 * still in the transformation queue.
				 */								// DB: TODO: check this. There seems to be correspondence nodes added to the correspondence nodes
												// of the external elements. Those do not have a creation rule set.
				if (corrNode.getRuleSet() != null && corrNode.getCreationRule() != null ) {
					if (modTag.isSynchronize()) {
						//subMonitor.setTaskName("Executing rule '" + corrNode.getCreationRule().eClass().getName() + "'...");
						System.out.println( new Date() + ": Executing synchronization rule '" + corrNode.getCreationRule().eClass().getName() + " on corr node " + corrNode + "'...");

						/*
						 * In case of synchronization, execute the rule that
						 * created this correspondence node.
						 */
						switch (transformationDirection) {
						case FORWARD: {
							final boolean modified = corrNode.getCreationRule().forwardSynchronization(corrNode);
							rulesPerformedModifications = modified || rulesPerformedModifications;
							break;
						}
						case MAPPING: {
							rulesPerformedModifications = corrNode.getCreationRule().mappingSynchronization(corrNode)
									|| rulesPerformedModifications;
							break;
						}
						case REVERSE: {
							rulesPerformedModifications = corrNode.getCreationRule().reverseSynchronization(corrNode)
									|| rulesPerformedModifications;
							break;
						}
						}

					} else {
						/*
						 * In case of transformation, execute all rules that are
						 * applicable for this correspondence node.
						 */
						for (de.hpi.sam.mote.rules.TGGRule rule : ruleSet.getRules()) {
							if (rule.acceptsParentCorrNode(corrNode)) {
//								subMonitor.setTaskName("Executing rule '" + rule.eClass().getName() + "'...");
								System.out.println( new Date() + ": Executing transformation rule '" + rule.eClass().getName() + " on corr node " + corrNode + "'...");

								TransformationResult result = null;

								switch (transformationDirection) {
									case FORWARD: {
										result = rule.forwardTransformation(corrNode);
										break;
									}
									case MAPPING: {
										result = rule.mappingTransformation(corrNode);
										break;
									}
									case REVERSE: {
										result = rule.reverseTransformation(corrNode);
										break;
									}
								}

								switch (result) {
									case CONFLICT_DETECTED: {
										throw new TransformationException("Conflict detected during execution of rule '" + rule + "'.", null);
	
									}
									case RULE_APPLIED: {
										rulesPerformedModifications = true;
										System.out.println( new Date() + ": Applied transformation rule " + rule.eClass().getName() + "." );
									}
									default:
										System.out.println( new Date() + ": Transformation rule " + rule.eClass().getName() + " was not applied." );
										break;
								}

								if (rulesPerformedModifications	&& this.isNotificationsEnabled()) {
									this.eNotify(new ENotificationImpl(this, MoteNotification.RULE_EXECUTED, MotePackage.TGG_ENGINE, null, rule));
								}
							}
						}

						if (this.isNotificationsEnabled()) {
							this.eNotify(new ENotificationImpl(this, MoteNotification.TRANSFORMATION_QUEUE_POP,	MotePackage.TGG_ENGINE, corrNode, null));
						}
					}

					/*
					 * In case of batch synchronization, add successors to queue
					 */
					if (synchronize) {
						if ( batchMode ) {
							for ( final TGGNode n : corrNode.getNext()) {
								modTag = HelpersFactory.eINSTANCE.createModificationTag();
								modTag.setCorrespondenceNode(n);
								modTag.setSynchronize(true);
								
								// DB: Mod tags were not added..
								ruleSet.getTransformationQueue().add( modTag );
							}
						}
						else {
							final TransformationQueue ruleSetQueue = ruleSet.getTransformationQueue();
							
							if ( ruleSetQueue.isEmpty() ) {
								final Adapter adapter;
								
								if ( transformationDirection == TransformationDirection.FORWARD ) {
									adapter = ruleSet.getSourceModelElementsAdapter();
								}
								else if ( transformationDirection == TransformationDirection.REVERSE ) {
									adapter = ruleSet.getTargetModelElementsAdapter();
								}
								else {
									adapter = null;
								}
								
								if ( adapter instanceof MoteChangeListener ) {
									final TransformationQueue delQueue = ( (MoteChangeListener) adapter ).getDeletionQueue();
									
									while ( !delQueue.isEmpty() ) {
										final ModificationTag delTag = delQueue.pop();
										ruleSet.getTransformationQueue().add( delTag );
									}
								}
							}
						}
					}
				}
			}
		}

		/*
		 * Delete all elements that are marked to delete
		 */
		for ( final TGGNode node : ruleSet.getCorrNodesToDelete() ) {
			ruleSet.deleteElements( node, transformationDirection );
		}

		monitor.done();

		ruleSet.eAdapters().remove(ruleSetProgressAdapter);

		ruleSet.setProcessNotifications(true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public void performRuntimeChecks(TGGRuleSet ruleSet,
			TransformationDirection transformationDirection,
			IProgressMonitor monitor) throws TransformationException {
		/*
		 * Get root corr node
		 */
		TGGNode root = ruleSet.getRootCorrNode();

		Queue<TGGNode> queue = new LinkedList<TGGNode>();
		queue.add(root);

		while (!queue.isEmpty()) {
			TGGNode node = queue.remove();

			for (TGGRule rule : ruleSet.getRules()) {
				switch (transformationDirection) {
				case FORWARD: {
					if (!rule.forwardRuntimeCheck(node)) {
						throw new TransformationException(
								"Runtime check failed for rule '" + rule
										+ "' with parent node '" + node + "'.",
								null);
					}
					break;
				}
				case REVERSE: {
					if (!rule.reverseRuntimeCheck(node)) {
						throw new TransformationException(
								"Runtime check failed for rule '" + rule
										+ "' with parent node '" + node + "'.",
								null);
					}
					break;
				}
				default: {
					throw new TransformationException(
							"Runtime checks for transformation direction '"
									+ transformationDirection
									+ "' are currently not supported.", null);

				}

				}

			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EObject> correspondingSourceElements(	final Resource p_sourceResource,
														final EObject p_targetElement,
														final String p_ruleSetId )
	throws TransformationException {
		final EList<EObject> elements = new BasicEList<EObject>();
		
		final URI sourceUri = p_sourceResource.getURI();
		final Resource targetResource = p_targetElement.eResource();
		final URI targetUri = targetResource.getURI();
		final TGGRuleSet ruleSet = getUsedRuleSet( 	p_ruleSetId,
													sourceUri, 
													targetUri, 
													targetResource.getResourceSet() );
		
		if ( ruleSet.getRootCorrNode() == null ) {
			mapping( 	ruleSet,
						p_sourceResource,
						targetResource,
						false,
						new HashSet<EObject>(),
						new NullProgressMonitor() );
		}
		
		final TGGNode corrNode = findCorrNode( p_targetElement, ruleSet, false );
		
		if ( corrNode != null ) {
			elements.addAll( corrNode.getSources() );
		}
		
		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EObject> correspondingTargetElements(	final EObject p_sourceElement, 
														final Resource p_targetResource,
														final String p_ruleSetId )
	throws TransformationException {
		final EList<EObject> elements = new BasicEList<EObject>();
		
		final Resource sourceResource = p_sourceElement.eResource();
		final URI sourceUri = sourceResource.getURI();
		final URI targetUri = p_targetResource.getURI();
		final TGGRuleSet ruleSet = getUsedRuleSet( 	p_ruleSetId,
													sourceUri, 
													targetUri, 
													sourceResource.getResourceSet() );
		
		if ( ruleSet.getRootCorrNode() == null ) {
			mapping( 	ruleSet,
						sourceResource,
						p_targetResource,
						true,
						new HashSet<EObject>(),
						new NullProgressMonitor() );
		}
		
		final TGGNode corrNode = findCorrNode( p_sourceElement, ruleSet, true );
		
		if ( corrNode != null ) {
			elements.addAll( corrNode.getTargets() );
		}
		
		return elements;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void resourceDeleted( final Resource p_resource ) {
		if ( !getUsedRuleSets().isEmpty() ) {
			final URI resUri = p_resource.getURI();
			final Collection<EObject> extCrossRefs = new HashSet<EObject>();
			final Iterator<EObject> contentIt = p_resource.getAllContents();
			
			while( contentIt.hasNext() ) {
				for ( final EObject crossRef : contentIt.next().eCrossReferences() ) {
					final Resource crossRes = crossRef.eResource();
					
					if ( crossRes == null || crossRes != p_resource ) {
						extCrossRefs.add( crossRef );
					}
				}
			}
			
			for ( final Collection<RuleSetTag> rstags : getUsedRuleSets().values() ) {
				for ( final RuleSetTag rstag : rstags ) {
					final TGGRuleSet ruleSet = rstag.getRuleSet();
					
					if ( ruleSet != null ) {
						if ( rstag.getSourceModelUri().equals( resUri ) || rstag.getTargetModelUri().equals( resUri ) ) {
							ruleSet.clear();
						}
						else {
							// See if the resource depends on the deleted resource
							for ( final EObject obj : ruleSet.getSourceModelElements().keySet() ) {
								if ( extCrossRefs.contains( obj ) ) {
									ruleSet.clear();
									
									break;
								}
							}
	
							for ( final EObject obj : ruleSet.getTargetModelElements().keySet() ) {
								if ( extCrossRefs.contains( obj ) ) {
									ruleSet.clear();
									
									break;
								}
							}
						}
					}	
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public TransformationExecutionResult checkConsistency(	final Resource p_leftResource,
															final Resource p_rightResource, 
															final EMap<Resource, EList<EObject>> p_dependencies,
															final String p_ruleSetID )
	throws TransformationException {
		// TODO: Handle the case where a resource is empty. 
		final URI sourceUri = p_leftResource.getURI();
		final URI targetUri = p_rightResource.getURI();
		final TGGRuleSet ruleSet = getUsedRuleSet( 	p_ruleSetID,
													sourceUri, 
													targetUri, 
													p_leftResource.getResourceSet() );
		ruleSet.clear();
		
		// Turn off processing of change notifications
		ruleSet.setProcessNotifications( false );

		final StringBuilder strBuild = new StringBuilder( new Date().toString() );
		strBuild.append( ": Checking consistency of " );
		strBuild.append( sourceUri );
		strBuild.append( " with " );
		strBuild.append( targetUri );
		System.out.println( strBuild.toString() );
		
		final TransformationDirection direction = TransformationDirection.MAPPING;

		// Handle objects in external resources.
		//final Set<Resource> createdDependencies = new HashSet<Resource>();
		final boolean synchronizeAfterInitExternal = initRuleSetWithExternalObjects( 	ruleSet, 
																						p_leftResource, 
																						p_rightResource,
																						p_dependencies,
																						direction,
																						new HashSet<EObject>(),
																						new NullProgressMonitor() );
		
		final TransformationResult result = executeAllRules( 	p_leftResource,
																p_rightResource,
																direction,
																synchronizeAfterInitExternal,
																ruleSet,
																new NullProgressMonitor() );

		final EObject execTrace = ruleSet.transformationFinished( p_leftResource, p_rightResource, direction, synchronizeAfterInitExternal );

		if ( result != TransformationResult.RULE_APPLIED ) {
			throw new TransformationException( 	"Error execution mapping transformation for resources " + 
												p_leftResource + " and  " + p_rightResource + "." );
		}

		// Create a list of all uncovered source and target elements
		final EList<EObject> uncoveredLeftElements = new BasicEList<EObject>( ruleSet.getUncoveredSourceModelElements().keySet() );
		final EList<EObject> uncoveredRightElements = new BasicEList<EObject>( ruleSet.getUncoveredTargetModelElements().keySet() );
//		final EList<EList<EObject>> returnList = new BasicEList<EList<EObject>>();
//		returnList.add(uncoveredSourceElements);
//		returnList.add(uncoveredTargetElements);
		System.out.println( new Date() + ": Consistency check finished." );
		
		ruleSet.setProcessNotifications( true );
		
		
		return createTransformationExecutionResult( result, execTrace, uncoveredLeftElements, uncoveredRightElements );

		//return returnList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EObject> correspondingTargetElements(	final String p_ruleSetId,
														final EObject p_sourceElement,
														final URI p_targetResourceUri ) 
	throws TransformationException {
		final EList<EObject> corrObjects = new BasicEList<EObject>();
		final Resource sourceRes = p_sourceElement.eResource();
		
		if ( sourceRes != null ) {
			final TGGRuleSet tggRuleSet = getUsedRuleSet( 	p_ruleSetId, 
															sourceRes.getURI(), 
															p_targetResourceUri, 
															sourceRes.getResourceSet() );
			
			if ( tggRuleSet != null ) {
				
			}
		}
		
		return corrObjects;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MotePackage.TGG_ENGINE__AVAILABLE_RULE_SETS:
				return ((InternalEList<?>)getAvailableRuleSets()).basicRemove(otherEnd, msgs);
			case MotePackage.TGG_ENGINE__USED_RULE_SETS:
				return ((InternalEList<?>)getUsedRuleSets()).basicRemove(otherEnd, msgs);
			case MotePackage.TGG_ENGINE__RELATION_POLICIES:
				return ((InternalEList<?>)getRelationPolicies()).basicRemove(otherEnd, msgs);
			case MotePackage.TGG_ENGINE__COVERAGE_POLICIES:
				return ((InternalEList<?>)getCoveragePolicies()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MotePackage.TGG_ENGINE__AVAILABLE_RULE_SETS:
				if (coreType) return getAvailableRuleSets();
				else return getAvailableRuleSets().map();
			case MotePackage.TGG_ENGINE__USED_RULE_SETS:
				if (coreType) return getUsedRuleSets();
				else return getUsedRuleSets().map();
			case MotePackage.TGG_ENGINE__RUNTIME_CHECKS_ENABLED:
				return isRuntimeChecksEnabled();
			case MotePackage.TGG_ENGINE__NOTIFICATIONS_ENABLED:
				return isNotificationsEnabled();
			case MotePackage.TGG_ENGINE__RELATION_POLICIES:
				if (coreType) return getRelationPolicies();
				else return getRelationPolicies().map();
			case MotePackage.TGG_ENGINE__COVERAGE_POLICIES:
				if (coreType) return getCoveragePolicies();
				else return getCoveragePolicies().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MotePackage.TGG_ENGINE__AVAILABLE_RULE_SETS:
				((EStructuralFeature.Setting)getAvailableRuleSets()).set(newValue);
				return;
			case MotePackage.TGG_ENGINE__USED_RULE_SETS:
				((EStructuralFeature.Setting)getUsedRuleSets()).set(newValue);
				return;
			case MotePackage.TGG_ENGINE__RUNTIME_CHECKS_ENABLED:
				setRuntimeChecksEnabled((Boolean)newValue);
				return;
			case MotePackage.TGG_ENGINE__NOTIFICATIONS_ENABLED:
				setNotificationsEnabled((Boolean)newValue);
				return;
			case MotePackage.TGG_ENGINE__RELATION_POLICIES:
				((EStructuralFeature.Setting)getRelationPolicies()).set(newValue);
				return;
			case MotePackage.TGG_ENGINE__COVERAGE_POLICIES:
				((EStructuralFeature.Setting)getCoveragePolicies()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MotePackage.TGG_ENGINE__AVAILABLE_RULE_SETS:
				getAvailableRuleSets().clear();
				return;
			case MotePackage.TGG_ENGINE__USED_RULE_SETS:
				getUsedRuleSets().clear();
				return;
			case MotePackage.TGG_ENGINE__RUNTIME_CHECKS_ENABLED:
				setRuntimeChecksEnabled(RUNTIME_CHECKS_ENABLED_EDEFAULT);
				return;
			case MotePackage.TGG_ENGINE__NOTIFICATIONS_ENABLED:
				setNotificationsEnabled(NOTIFICATIONS_ENABLED_EDEFAULT);
				return;
			case MotePackage.TGG_ENGINE__RELATION_POLICIES:
				getRelationPolicies().clear();
				return;
			case MotePackage.TGG_ENGINE__COVERAGE_POLICIES:
				getCoveragePolicies().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MotePackage.TGG_ENGINE__AVAILABLE_RULE_SETS:
				return availableRuleSets != null && !availableRuleSets.isEmpty();
			case MotePackage.TGG_ENGINE__USED_RULE_SETS:
				return usedRuleSets != null && !usedRuleSets.isEmpty();
			case MotePackage.TGG_ENGINE__RUNTIME_CHECKS_ENABLED:
				return runtimeChecksEnabled != RUNTIME_CHECKS_ENABLED_EDEFAULT;
			case MotePackage.TGG_ENGINE__NOTIFICATIONS_ENABLED:
				return notificationsEnabled != NOTIFICATIONS_ENABLED_EDEFAULT;
			case MotePackage.TGG_ENGINE__RELATION_POLICIES:
				return relationPolicies != null && !relationPolicies.isEmpty();
			case MotePackage.TGG_ENGINE__COVERAGE_POLICIES:
				return coveragePolicies != null && !coveragePolicies.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (runtimeChecksEnabled: ");
		result.append(runtimeChecksEnabled);
		result.append(", notificationsEnabled: ");
		result.append(notificationsEnabled);
		result.append(')');
		return result.toString();
	}

} // TGGEngineImpl
