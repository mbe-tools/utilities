/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.impl.TransformationException;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>TGG Rule</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * TGGRule is the abstract base class for all transformation rules. Transformation rules perform model transformation or synchronization.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRule#getRuleSet <em>Rule Set</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRule#getInputCorrNodeTypes <em>Input Corr Node Types</em>}</li>
 * </ul>
 *
 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRule()
 * @model abstract="true"
 * @generated
 */
public interface TGGRule extends TGGMapping {
	/**
	 * Returns the value of the '<em><b>Rule Set</b></em>' reference. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.mote.rules.TGGRuleSet#getRules <em>Rules</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Set</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc --> <!-- begin-model-doc --> Reference to the ruleSet
	 * this rule belongs to. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Rule Set</em>' reference.
	 * @see #setRuleSet(TGGRuleSet)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRule_RuleSet()
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getRules
	 * @model opposite="rules" resolveProxies="false" ordered="false"
	 * @generated
	 */
	TGGRuleSet getRuleSet();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRule#getRuleSet
	 * <em>Rule Set</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Rule Set</em>' reference.
	 * @see #getRuleSet()
	 * @generated
	 */
	void setRuleSet(TGGRuleSet value);

	/**
	 * Returns the value of the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Class}&lt;?>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Corr Node Types</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Corr Node Types</em>' attribute list.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRule_InputCorrNodeTypes()
	 * @model required="true" transient="true" changeable="false" derived="true"
	 * @generated
	 */
	EList<Class<?>> getInputCorrNodeTypes();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Performs a forward transformation. New elements are sought for and
	 * transformed if possible. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	TransformationResult forwardTransformation(TGGNode parentCorrNode)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Performs a mapping transformation. New elements are sought for and
	 * transformed if possible. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	TransformationResult mappingTransformation(TGGNode parentCorrNode)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Performs a reverse transformation. New elements are sought for and
	 * transformed if possible. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	TransformationResult reverseTransformation(TGGNode parentCorrNode)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Returns true if the rule can use the specified correspondence node as a
	 * parent correspondence node. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	Boolean acceptsParentCorrNode(TGGNode corrNode);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Boolean forwardRuntimeCheck(TGGNode corrNode);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Boolean reverseRuntimeCheck(TGGNode corrNode);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void initInputCorrNodeTypes();

} // TGGRule
