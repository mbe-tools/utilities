/**
 */
package de.hpi.sam.mote.rules;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.ReferencePattern#getReference <em>Reference</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.ReferencePattern#getSourceClassifier <em>Source Classifier</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.ReferencePattern#getTargetClassifier <em>Target Classifier</em>}</li>
 * </ul>
 *
 * @see de.hpi.sam.mote.rules.RulesPackage#getReferencePattern()
 * @model
 * @generated
 */
public interface ReferencePattern extends EObject {
	/**
	 * Returns the value of the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' reference.
	 * @see #setReference(EReference)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getReferencePattern_Reference()
	 * @model required="true"
	 * @generated
	 */
	EReference getReference();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.ReferencePattern#getReference <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' reference.
	 * @see #getReference()
	 * @generated
	 */
	void setReference(EReference value);

	/**
	 * Returns the value of the '<em><b>Source Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Classifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Classifier</em>' reference.
	 * @see #setSourceClassifier(EClassifier)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getReferencePattern_SourceClassifier()
	 * @model required="true"
	 * @generated
	 */
	EClassifier getSourceClassifier();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.ReferencePattern#getSourceClassifier <em>Source Classifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Classifier</em>' reference.
	 * @see #getSourceClassifier()
	 * @generated
	 */
	void setSourceClassifier(EClassifier value);

	/**
	 * Returns the value of the '<em><b>Target Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Classifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Classifier</em>' reference.
	 * @see #setTargetClassifier(EClassifier)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getReferencePattern_TargetClassifier()
	 * @model required="true"
	 * @generated
	 */
	EClassifier getTargetClassifier();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.ReferencePattern#getTargetClassifier <em>Target Classifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Classifier</em>' reference.
	 * @see #getTargetClassifier()
	 * @generated
	 */
	void setTargetClassifier(EClassifier value);

} // ReferencePattern
