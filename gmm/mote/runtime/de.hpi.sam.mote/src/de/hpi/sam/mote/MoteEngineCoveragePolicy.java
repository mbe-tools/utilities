/**
 */
package de.hpi.sam.mote;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Engine Coverage Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hpi.sam.mote.MotePackage#getMoteEngineCoveragePolicy()
 * @model
 * @generated
 */
public interface MoteEngineCoveragePolicy extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" modelElementRequired="true"
	 * @generated
	 */
	boolean isCovered(EObject modelElement);

} // MoteEngineCoveragePolicy
