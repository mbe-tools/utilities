/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EContentsEList;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.TransformationDirection;
import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.helpers.ReverseNavigationStore;
import de.hpi.sam.mote.helpers.impl.MapEntryImpl;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.impl.UnresolvedRefTransException;
import de.hpi.sam.mote.rules.ReferencePattern;
import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.mote.rules.TGGAxiom;
import de.hpi.sam.mote.rules.TGGRule;
import de.hpi.sam.mote.rules.TGGRuleSet;
import de.hpi.sam.mote.rules.TransformationQueue;
import de.hpi.sam.mote.util.MoteChangeListener;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>TGG Rule Set</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getRootCorrNode <em>Root Corr Node</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getAxiom <em>Axiom</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getRules <em>Rules</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#isProcessNotifications <em>Process Notifications</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getCorrespondenceNodes <em>Correspondence Nodes</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getSourceModelElements <em>Source Model Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getTargetModelElements <em>Target Model Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getUncoveredSourceModelElements <em>Uncovered Source Model Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getUncoveredTargetModelElements <em>Uncovered Target Model Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getSourceModelElementsAdapter <em>Source Model Elements Adapter</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getTargetModelElementsAdapter <em>Target Model Elements Adapter</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getTransformationQueue <em>Transformation Queue</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getSourceReverseNavigationStore <em>Source Reverse Navigation Store</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getTargetReverseNavigationStore <em>Target Reverse Navigation Store</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getSourceModelRootNode <em>Source Model Root Node</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getTargetModelRootNode <em>Target Model Root Node</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getCorrNodesToDelete <em>Corr Nodes To Delete</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getResourceSet <em>Resource Set</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getId <em>Id</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getExternalReferences <em>External References</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleSetImpl#getExternalRefPatterns <em>External Ref Patterns</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TGGRuleSetImpl extends EObjectImpl implements TGGRuleSet {
	/**
	 * The cached value of the '{@link #getRootCorrNode() <em>Root Corr Node</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getRootCorrNode()
	 * @generated
	 * @ordered
	 */
	protected TGGNode rootCorrNode;

	/**
	 * The cached value of the '{@link #getAxiom() <em>Axiom</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getAxiom()
	 * @generated
	 * @ordered
	 */
	protected TGGAxiom axiom;

	/**
	 * The cached value of the '{@link #getRules() <em>Rules</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRules()
	 * @generated
	 * @ordered
	 */
	protected EList<TGGRule> rules;

	/**
	 * The default value of the '{@link #isProcessNotifications() <em>Process Notifications</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isProcessNotifications()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PROCESS_NOTIFICATIONS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isProcessNotifications() <em>Process Notifications</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isProcessNotifications()
	 * @generated
	 * @ordered
	 */
	protected boolean processNotifications = PROCESS_NOTIFICATIONS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCorrespondenceNodes() <em>Correspondence Nodes</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCorrespondenceNodes()
	 * @generated
	 * @ordered
	 */
	protected EMap<TGGNode, Object> correspondenceNodes;

	/**
	 * The cached value of the '{@link #getSourceModelElements() <em>Source Model Elements</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSourceModelElements()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, TGGNode> sourceModelElements;

	/**
	 * The cached value of the '{@link #getTargetModelElements() <em>Target Model Elements</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getTargetModelElements()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, TGGNode> targetModelElements;

	/**
	 * The cached value of the '{@link #getUncoveredSourceModelElements() <em>Uncovered Source Model Elements</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUncoveredSourceModelElements()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, Object> uncoveredSourceModelElements;

	/**
	 * The cached value of the '{@link #getUncoveredTargetModelElements() <em>Uncovered Target Model Elements</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUncoveredTargetModelElements()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, Object> uncoveredTargetModelElements;

	/**
	 * The default value of the '{@link #getSourceModelElementsAdapter() <em>Source Model Elements Adapter</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getSourceModelElementsAdapter()
	 * @generated
	 * @ordered
	 */
	protected static final Adapter SOURCE_MODEL_ELEMENTS_ADAPTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceModelElementsAdapter() <em>Source Model Elements Adapter</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getSourceModelElementsAdapter()
	 * @generated
	 * @ordered
	 */
	protected Adapter sourceModelElementsAdapter = SOURCE_MODEL_ELEMENTS_ADAPTER_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetModelElementsAdapter() <em>Target Model Elements Adapter</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getTargetModelElementsAdapter()
	 * @generated
	 * @ordered
	 */
	protected static final Adapter TARGET_MODEL_ELEMENTS_ADAPTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetModelElementsAdapter() <em>Target Model Elements Adapter</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getTargetModelElementsAdapter()
	 * @generated
	 * @ordered
	 */
	protected Adapter targetModelElementsAdapter = TARGET_MODEL_ELEMENTS_ADAPTER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTransformationQueue() <em>Transformation Queue</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getTransformationQueue()
	 * @generated
	 * @ordered
	 */
	protected TransformationQueue transformationQueue;

	/**
	 * The cached value of the '{@link #getSourceReverseNavigationStore() <em>Source Reverse Navigation Store</em>}' reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getSourceReverseNavigationStore()
	 * @generated
	 * @ordered
	 */
	protected ReverseNavigationStore sourceReverseNavigationStore;

	/**
	 * The cached value of the '{@link #getTargetReverseNavigationStore() <em>Target Reverse Navigation Store</em>}' reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getTargetReverseNavigationStore()
	 * @generated
	 * @ordered
	 */
	protected ReverseNavigationStore targetReverseNavigationStore;

	/**
	 * The cached value of the '{@link #getSourceModelRootNode() <em>Source Model Root Node</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSourceModelRootNode()
	 * @generated
	 * @ordered
	 */
	protected EObject sourceModelRootNode;

	/**
	 * The cached value of the '{@link #getTargetModelRootNode() <em>Target Model Root Node</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getTargetModelRootNode()
	 * @generated
	 * @ordered
	 */
	protected EObject targetModelRootNode;

	/**
	 * The cached value of the '{@link #getCorrNodesToDelete() <em>Corr Nodes To Delete</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrNodesToDelete()
	 * @generated
	 * @ordered
	 */
	protected EList<TGGNode> corrNodesToDelete;

	/**
	 * The default value of the '{@link #getResourceSet() <em>Resource Set</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceSet()
	 * @generated
	 * @ordered
	 */
	protected static final ResourceSet RESOURCE_SET_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResourceSet() <em>Resource Set</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceSet()
	 * @generated
	 * @ordered
	 */
	protected ResourceSet resourceSet = RESOURCE_SET_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExternalReferences() <em>External References</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<Method> externalReferences;

	/**
	 * The cached value of the '{@link #getExternalRefPatterns() <em>External Ref Patterns</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalRefPatterns()
	 * @generated
	 * @ordered
	 */
	protected EList<ReferencePattern> externalRefPatterns;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected TGGRuleSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RulesPackage.Literals.TGG_RULE_SET;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TGGNode getRootCorrNode() {
		return rootCorrNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRootCorrNode(TGGNode newRootCorrNode) {
		TGGNode oldRootCorrNode = rootCorrNode;
		rootCorrNode = newRootCorrNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__ROOT_CORR_NODE, oldRootCorrNode, rootCorrNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TGGAxiom getAxiom() {
		return axiom;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAxiom(TGGAxiom newAxiom,
			NotificationChain msgs) {
		TGGAxiom oldAxiom = axiom;
		axiom = newAxiom;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__AXIOM, oldAxiom, newAxiom);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAxiom(TGGAxiom newAxiom) {
		if (newAxiom != axiom) {
			NotificationChain msgs = null;
			if (axiom != null)
				msgs = ((InternalEObject)axiom).eInverseRemove(this, RulesPackage.TGG_AXIOM__RULE_SET, TGGAxiom.class, msgs);
			if (newAxiom != null)
				msgs = ((InternalEObject)newAxiom).eInverseAdd(this, RulesPackage.TGG_AXIOM__RULE_SET, TGGAxiom.class, msgs);
			msgs = basicSetAxiom(newAxiom, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__AXIOM, newAxiom, newAxiom));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TGGRule> getRules() {
		if (rules == null) {
			rules = new EObjectWithInverseEList<TGGRule>(TGGRule.class, this, RulesPackage.TGG_RULE_SET__RULES, RulesPackage.TGG_RULE__RULE_SET);
		}
		return rules;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isProcessNotifications() {
		return processNotifications;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProcessNotifications(boolean newProcessNotifications) {
		boolean oldProcessNotifications = processNotifications;
		processNotifications = newProcessNotifications;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__PROCESS_NOTIFICATIONS, oldProcessNotifications, processNotifications));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<TGGNode, Object> getCorrespondenceNodes() {
		if (correspondenceNodes == null) {
			correspondenceNodes = new EcoreEMap<TGGNode,Object>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, RulesPackage.TGG_RULE_SET__CORRESPONDENCE_NODES);
		}
		return correspondenceNodes;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<EObject, TGGNode> getSourceModelElements() {
		if (sourceModelElements == null) {
			sourceModelElements = new EcoreEMap<EObject,TGGNode>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS);
		}
		return sourceModelElements;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<EObject, TGGNode> getTargetModelElements() {
		if (targetModelElements == null) {
			targetModelElements = new EcoreEMap<EObject,TGGNode>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS);
		}
		return targetModelElements;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<EObject, Object> getUncoveredSourceModelElements() {
		if (uncoveredSourceModelElements == null) {
			uncoveredSourceModelElements = new EcoreEMap<EObject,Object>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, RulesPackage.TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS);
		}
		return uncoveredSourceModelElements;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<EObject, Object> getUncoveredTargetModelElements() {
		if (uncoveredTargetModelElements == null) {
			uncoveredTargetModelElements = new EcoreEMap<EObject,Object>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, RulesPackage.TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS);
		}
		return uncoveredTargetModelElements;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter getSourceModelElementsAdapter() {
		return sourceModelElementsAdapter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceModelElementsAdapter(
			Adapter newSourceModelElementsAdapter) {
		Adapter oldSourceModelElementsAdapter = sourceModelElementsAdapter;
		sourceModelElementsAdapter = newSourceModelElementsAdapter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS_ADAPTER, oldSourceModelElementsAdapter, sourceModelElementsAdapter));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter getTargetModelElementsAdapter() {
		return targetModelElementsAdapter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetModelElementsAdapter(
			Adapter newTargetModelElementsAdapter) {
		Adapter oldTargetModelElementsAdapter = targetModelElementsAdapter;
		targetModelElementsAdapter = newTargetModelElementsAdapter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS_ADAPTER, oldTargetModelElementsAdapter, targetModelElementsAdapter));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TransformationQueue getTransformationQueue() {
		return transformationQueue;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTransformationQueue(
			TransformationQueue newTransformationQueue) {
		TransformationQueue oldTransformationQueue = transformationQueue;
		transformationQueue = newTransformationQueue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__TRANSFORMATION_QUEUE, oldTransformationQueue, transformationQueue));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ReverseNavigationStore getSourceReverseNavigationStore() {
		return sourceReverseNavigationStore;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceReverseNavigationStore(
			ReverseNavigationStore newSourceReverseNavigationStore) {
		ReverseNavigationStore oldSourceReverseNavigationStore = sourceReverseNavigationStore;
		sourceReverseNavigationStore = newSourceReverseNavigationStore;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__SOURCE_REVERSE_NAVIGATION_STORE, oldSourceReverseNavigationStore, sourceReverseNavigationStore));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ReverseNavigationStore getTargetReverseNavigationStore() {
		return targetReverseNavigationStore;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetReverseNavigationStore(
			ReverseNavigationStore newTargetReverseNavigationStore) {
		ReverseNavigationStore oldTargetReverseNavigationStore = targetReverseNavigationStore;
		targetReverseNavigationStore = newTargetReverseNavigationStore;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__TARGET_REVERSE_NAVIGATION_STORE, oldTargetReverseNavigationStore, targetReverseNavigationStore));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject getSourceModelRootNode() {
		return sourceModelRootNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceModelRootNode(EObject newSourceModelRootNode) {
		EObject oldSourceModelRootNode = sourceModelRootNode;
		sourceModelRootNode = newSourceModelRootNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ROOT_NODE, oldSourceModelRootNode, sourceModelRootNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject getTargetModelRootNode() {
		return targetModelRootNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetModelRootNode(EObject newTargetModelRootNode) {
		EObject oldTargetModelRootNode = targetModelRootNode;
		targetModelRootNode = newTargetModelRootNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__TARGET_MODEL_ROOT_NODE, oldTargetModelRootNode, targetModelRootNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TGGNode> getCorrNodesToDelete() {
		if (corrNodesToDelete == null) {
			corrNodesToDelete = new EObjectResolvingEList<TGGNode>(TGGNode.class, this, RulesPackage.TGG_RULE_SET__CORR_NODES_TO_DELETE);
		}
		return corrNodesToDelete;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceSet getResourceSet() {
		return resourceSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceSet(ResourceSet newResourceSet) {
		ResourceSet oldResourceSet = resourceSet;
		resourceSet = newResourceSet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__RESOURCE_SET, oldResourceSet, resourceSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE_SET__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Method> getExternalReferences() {
		if (externalReferences == null) {
			externalReferences = new EDataTypeUniqueEList<Method>(Method.class, this, RulesPackage.TGG_RULE_SET__EXTERNAL_REFERENCES);
		}
		return externalReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferencePattern> getExternalRefPatterns() {
		if (externalRefPatterns == null) {
			externalRefPatterns = new EObjectContainmentEList<ReferencePattern>(ReferencePattern.class, this, RulesPackage.TGG_RULE_SET__EXTERNAL_REF_PATTERNS);
		}
		return externalRefPatterns;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public void clear() {
		// Remove the adapters from model elements
		if (this.getSourceModelElementsAdapter() != null) {
			for (EObject eObject : this.getSourceModelElements().keySet()) {
				eObject.eAdapters()
						.remove(this.getSourceModelElementsAdapter());
			}
		}

		if (this.getTargetModelElementsAdapter() != null) {
			for (EObject eObject : this.getTargetModelElements().keySet()) {
				eObject.eAdapters()
						.remove(this.getTargetModelElementsAdapter());
			}
		}

		// Remove the correspondence nodes from the rules
		if (this.getAxiom() != null) {
			this.getAxiom().getCreatedCorrNodes().clear();
		}

		for (de.hpi.sam.mote.rules.TGGRule rule : this.getRules()) {
			rule.getCreatedCorrNodes().clear();
		}

		// Remove the correspondence nodes from the ruleSet
		this.getSourceModelElements().clear();
		this.getTargetModelElements().clear();
		this.getCorrespondenceNodes().clear();
		this.getUncoveredSourceModelElements().clear();
		this.getUncoveredTargetModelElements().clear();

		this.getTransformationQueue().clear();

		this.setRootCorrNode(null);
		this.setSourceModelRootNode(null);
		this.setTargetModelRootNode(null);

		if (this.getSourceReverseNavigationStore() != null) {
			this.getSourceReverseNavigationStore().clear();
		}

		if (this.getTargetReverseNavigationStore() != null) {
			this.getTargetReverseNavigationStore().clear();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void createRules() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}
	
	protected boolean containsPattern( final ReferencePattern p_refPattern ) {
		for ( final ReferencePattern refPattern : getExternalRefPatterns() ) {
			if ( 	refPattern.getReference() == p_refPattern.getReference() &&
					refPattern.getSourceClassifier() == p_refPattern.getSourceClassifier() &&
					refPattern.getTargetClassifier() == p_refPattern.getTargetClassifier() ) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void initializeRuleSet() {
				setTransformationQueue(new FIFOTransformationQueue());
				
				clear();
		
				createRules();
		
				setTransformationQueue(new de.hpi.sam.mote.rules.impl.FIFOTransformationQueue());
		
				if (getSourceReverseNavigationStore() == null) {
					setSourceReverseNavigationStore(new de.hpi.sam.mote.helpers.impl.SimpleReverseNavigationStoreImpl());
				}
		
				if (getTargetReverseNavigationStore() == null) {
					setTargetReverseNavigationStore(new de.hpi.sam.mote.helpers.impl.SimpleReverseNavigationStoreImpl());
				}
		
				/*
				 * Create model adapters
				 */
				if (getSourceModelElementsAdapter() == null) {
					setSourceModelElementsAdapter(createModelElementsAdapter(getSourceModelElements(), getUncoveredSourceModelElements(),
							getSourceReverseNavigationStore()));
				}
		
				if (getTargetModelElementsAdapter() == null) {
					setTargetModelElementsAdapter(createModelElementsAdapter(getTargetModelElements(), getUncoveredTargetModelElements(),
							getTargetReverseNavigationStore()));
				}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void initializeSourceModel(EObject sourceRoot)
			throws TransformationException {
		if (sourceRoot == null) {
			throw new TransformationException("sourceRoot is null", null);
		}

		if (!this.getSourceModelElements().containsKey(sourceRoot)) {
			/*
			 * Add the root to the list of uncovered elements
			 */
			this.getUncoveredSourceModelElements().put(sourceRoot, null);
		}

		/*
		 * Attach the source elements adapter
		 */
		if ( sourceModelElementsAdapter != null ) {
			sourceRoot.eAdapters().add(sourceModelElementsAdapter);
		}

		// addLinksToReverseNavigationStore(sourceRoot,
		// getSourceReverseNavigationStore());

		/*
		 * Add the children to the list of uncovered elements
		 */
		org.eclipse.emf.common.util.TreeIterator<EObject> it = sourceRoot
				.eAllContents();

		while (it.hasNext()) {
			org.eclipse.emf.ecore.EObject eObject = it.next();

			this.getUncoveredSourceModelElements().put(eObject, null);

			/*
			 * Attach the source elements adapter
			 */
			// eObject.eAdapters().add(sourceModelElementsAdapter);

			// addLinksToReverseNavigationStore(eObject,
			// getSourceReverseNavigationStore());
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void initializeTargetModel(EObject targetRoot)
			throws TransformationException {
		if (targetRoot == null) {
			throw new TransformationException("targetRoot is null", null);
		}

		if (!this.getTargetModelElements().containsKey(targetRoot)) {
			/*
			 * Add the root to the list of uncovered elements
			 */
			this.getUncoveredTargetModelElements().put(targetRoot, null);
		}
		/*
		 * Attach the target elements adapter
		 */
		if ( targetModelElementsAdapter != null ) {
			targetRoot.eAdapters().add(targetModelElementsAdapter);
		}

		// addLinksToReverseNavigationStore(targetRoot,
		// getSourceReverseNavigationStore());

		/*
		 * Add the children to the list of uncovered elements
		 */
		org.eclipse.emf.common.util.TreeIterator<EObject> it = targetRoot
				.eAllContents();

		while (it.hasNext()) {
			org.eclipse.emf.ecore.EObject eObject = it.next();

			this.getUncoveredTargetModelElements().put(eObject, null);

			/*
			 * Attach the target elements adapter
			 */
			// eObject.eAdapters().add(targetModelElementsAdapter);

			// addLinksToReverseNavigationStore(eObject,
			// getSourceReverseNavigationStore());
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public void deleteElements(TGGNode corrNode,
			TransformationDirection direction) {
		for (de.hpi.sam.mote.TGGNode childNode : new java.util.ArrayList<de.hpi.sam.mote.TGGNode>(
				corrNode.getNext())) {
			this.deleteElements(childNode, direction);
		}

		for (org.eclipse.emf.ecore.EObject o : corrNode.getSources()) {
			this.getSourceModelElements().removeKey(o);

			if (direction == TransformationDirection.REVERSE) {
				EcoreUtil.delete(o, true);
				this.getUncoveredSourceModelElements().removeKey(o);
			} else if (this.getSourceModelElements().containsKey(o)) {
				this.getUncoveredSourceModelElements().put(o, null);
			}
		}

		for (org.eclipse.emf.ecore.EObject o : corrNode.getTargets()) {
			this.getTargetModelElements().removeKey(o);

			if (direction == TransformationDirection.FORWARD) {
				EcoreUtil.delete(o, true);
				this.getUncoveredTargetModelElements().removeKey(o);
			} else if (this.getTargetModelElements().containsKey(o)) {
				this.getUncoveredTargetModelElements().put(o, null);
			}
		}

		/*
		 * Delete correspondence node
		 */
		this.getCorrespondenceNodes().remove(corrNode);
		corrNode.setRuleSet(null);

		corrNode.setCreationRule(null);

		if (corrNode == this.getRootCorrNode()) {
			this.setRootCorrNode(null);
		}

		org.eclipse.emf.ecore.util.EcoreUtil.delete(corrNode, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public void addModificationTagToQueue(TGGNode corrNode, boolean transform,
			boolean synchronize, boolean synchronizeSelf) {
		if (transform || synchronizeSelf) {
			de.hpi.sam.mote.helpers.ModificationTag modTag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE
					.createModificationTag();

			modTag.setCorrespondenceNode(corrNode);
			modTag.setSynchronize(synchronizeSelf);

			this.getTransformationQueue().add(modTag);
		}

		if (synchronize) {
			for (de.hpi.sam.mote.TGGNode nextNode : corrNode.getNext()) {
				de.hpi.sam.mote.helpers.ModificationTag modTag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE
						.createModificationTag();

				modTag.setCorrespondenceNode(nextNode);
				modTag.setSynchronize(true);

				this.getTransformationQueue().add(modTag);
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Adapter createModelElementsAdapter(	final Object modelElementsMap,
												final Object uncoveredModelElementsMap,
												final ReverseNavigationStore reverseNavigationStore ) {
		final EMap<EObject, TGGNode> meMap = (EMap<EObject, TGGNode>) modelElementsMap;
		final EMap<EObject, Object> umeMap = (EMap<EObject, Object>) uncoveredModelElementsMap;
		
		return new MoteChangeListener( this, meMap, umeMap, reverseNavigationStore );
//		final ReverseNavigationStore rns = reverseNavigationStore;
//
//		return new org.eclipse.emf.ecore.util.EContentAdapter() {
//			@Override
//			public void notifyChanged(Notification msg) {
//				super.notifyChanged(msg);
//
//				if ( TGGRuleSetImpl.this.isProcessNotifications() && msg.getNotifier() instanceof EObject && !msg.isTouch() ) {
//					final EObject modelElement = (EObject) msg.getNotifier();
//					final int eventType = msg.getEventType();
//					
//					/*
//					 * Adjust the reverse navigation store.
//					 */
//					if ( msg.getFeature() instanceof EReference ) {
//						final EReference eReference = (EReference) msg.getFeature();
//
//						if (!eReference.isContainment()	&& eReference.getEOpposite() == null) {
//							final List<Object> elements = new ArrayList<Object>();
//							final Object newValue = msg.getNewValue(); 
//
//							if (	eventType == Notification.ADD || eventType == Notification.SET &&
//									newValue != null ) {
//								elements.add(msg.getNewValue());
//							} 
//							else if ( eventType == Notification.ADD_MANY ) {
//								elements.addAll((List<Object>) newValue );
//							}
//
//							for (final Object element : elements) {
//								if ( element instanceof EObject ) {
//									final EObject elementEObj = (EObject) element;
//									rns.addLink( modelElement, elementEObj, eReference );
//								}
//							}
//
//							elements.clear();
//
//							if (	eventType == Notification.REMOVE	||
//									eventType == Notification.SET &&
//									msg.getOldValue() != null) {
//								elements.add(msg.getOldValue());
//							}
//							else if ( eventType == Notification.REMOVE_MANY) {
//								elements.addAll((List<Object>) msg.getOldValue());
//							}
//
//							for ( final Object element : elements) {
//								if ( element instanceof EObject) {
//									final EObject elementEObj = (EObject) element;
//
//									rns.removeLink( modelElement, elementEObj, eReference);
//								}
//							}
//						}
//					}
//
//					/*
//					 * Get the correspondence node of the element
//					 */
//					final TGGNode corrNode = meMap.get(modelElement);
//
//					if (corrNode != null && TGGRuleSetImpl.this.getTransformationQueue().get(corrNode).isEmpty()) {
//						/*
//						 * Create a modification tag for the correspondence node
//						 */
//						final ModificationTag modTag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE.createModificationTag();
//						modTag.setCorrespondenceNode(corrNode);
//						modTag.setSynchronize(true);
//						TGGRuleSetImpl.this.getTransformationQueue().add(modTag);
//					}
//
//					/*
//					 * If a new element was added, attach the source model
//					 * elements adapter to it. Also, add them to the list of
//					 * uncovered elements if they do not have a correspondence
//					 * node.
//					 */
//					java.util.List<Object> values = new ArrayList<Object>();
//
//					if (	eventType == Notification.ADD ||
//							eventType == Notification.SET &&
//							msg.getNewValue() != null) {
//						if (msg.getNewValue() instanceof EObject) {
//							values.add(msg.getNewValue());
//						}
//					} 
//					else if ( eventType == Notification.ADD_MANY) {
//						values.addAll((java.util.List<Object>) msg.getNewValue());
//					}
//
//					for ( final Object value : values ) {
//						if ( value instanceof EObject) {
//							final EObject newValue = (EObject) value;
//
//							if (!meMap.containsKey(msg.getNewValue())) {
//								// uncoveredElements
//								umeMap.put(newValue, null);
//
//								// Check children
//								final TreeIterator<EObject> it = newValue.eAllContents();
//
//								while (it.hasNext()) {
//									EObject eObject = it.next();
//
//									// uncoveredElements
//									if (!meMap.containsKey(eObject)) {
//										umeMap.put(eObject, null);
//									}
//								}
//							}
//						}
//					}
//
//					values.clear();
//
//					if ( eventType == Notification.SET && msg.getNewValue() == null) {
//						if (modelElement.eContainer() == null) {
//							umeMap.removeKey(modelElement);
//
//							org.eclipse.emf.common.util.TreeIterator<EObject> it = modelElement
//									.eAllContents();
//
//							while (it.hasNext()) {
//								final EObject eObject = it.next();
//
//								if (!meMap.containsKey(eObject)) {
//									umeMap.removeKey(eObject);
//								}
//							}
//						}
//					}
//
//					if ( eventType == Notification.REMOVE) {
//						values.add(msg.getOldValue());
//					}
//					else if ( eventType == Notification.REMOVE_MANY) {
//						values.addAll((java.util.List<Object>) msg.getOldValue());
//					}
//
//					for (Object value : values) {
//						if (value instanceof EObject && ((EObject) value).eContainer() == null && !meMap.containsKey(value)) {
//							umeMap.removeKey(value);
//
//							final TreeIterator<EObject> it = ((EObject) value).eAllContents();
//
//							while (it.hasNext()) {
//								EObject eObject = it.next();
//
//								if (!meMap.containsKey(eObject)) {
//									umeMap.removeKey(eObject);
//								}
//							}
//						}
//					}
//				}
//			}
//		};
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public void addLinksToReverseNavigationStore(EObject eObject,
			ReverseNavigationStore reverseNavigationStore) {
		for (org.eclipse.emf.ecore.EReference eReference : eObject.eClass()
				.getEAllReferences()) {
			if (!eReference.isContainment()
					&& eReference.getEOpposite() == null) {
				if (!eReference.isMany()) {
					org.eclipse.emf.ecore.EObject target = (org.eclipse.emf.ecore.EObject) eObject
							.eGet(eReference);

					if (target != null) {
						this.getSourceReverseNavigationStore().addLink(eObject,
								target, eReference);
					}
				} else {
					@SuppressWarnings("unchecked")
					org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EObject> targets = (org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EObject>) eObject
							.eGet(eReference);

					for (org.eclipse.emf.ecore.EObject target : targets) {
						this.getTargetReverseNavigationStore().addLink(eObject,
								target, eReference);
					}
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public boolean isPredecessor(TGGNode startNode, TGGNode possiblePredecessor) {
		if (startNode.getPrevious().contains(possiblePredecessor)) {
			return true;
		} else {
			for (TGGNode previousNode : startNode.getPrevious()) {
				if (this.isPredecessor(previousNode, possiblePredecessor)) {
					return true;
				}
			}

			return false;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public void printMessage(String message) {
		System.out.println(message);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public void reportError(String errorMessage) throws TransformationException {
		System.out.println(errorMessage);

		throw new TransformationException(errorMessage, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void transformationStarted(final Resource sourceResource,
			final Resource targetResource,
			final TransformationDirection direction, final boolean synchronize) throws TransformationException {
		//Do nothing
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject transformationFinished(final Resource sourceResource,
			final Resource targetResource,
			final TransformationDirection direction, final boolean synchronize) throws TransformationException {
		//Do nothing
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EObject> sourceModelElements(EObject targetModelElement) {
		final EList<EObject> elements = new BasicEList<EObject>();

		for (final TGGNode corrNode : getCorrespondenceNodes().keySet()) {
			for (final EObject targetElem : corrNode.getTargets()) {
				if (targetModelElement == targetElem) {
					elements.addAll(corrNode.getSources());

					return elements;
				}
			}
		}

		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EObject> targetModelElements(EObject sourceModelElement) {
		final EList<EObject> elements = new BasicEList<EObject>();

		for (final TGGNode corrNode : getCorrespondenceNodes().keySet()) {
			for (final EObject sourceElem : corrNode.getSources()) {
				if (sourceModelElement == sourceElem) {
					elements.addAll(corrNode.getTargets());

					return elements;
				}
			}
		}

		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EObject existingModelObject( final TGGNode corrNode, 
										final EClassifier p_classifier,
										final boolean pb_searchFromSource ) {
		final Collection<EObject> objects =  pb_searchFromSource ? corrNode.getSources() : corrNode.getTargets();
		final Collection<EObject> mappedElements = pb_searchFromSource ? getSourceModelElements().keySet() : getTargetModelElements().keySet();
		
		for ( final EObject object : objects ) {
			final EClass objClass = object.eClass();
			
			if ( p_classifier == objClass ) {

				// Prune the mapped references.
				pruneMappedReferencedObjects( object, mappedElements );
				
				// Important to remove the object to be reused from the corr node.
				objects.remove( object );
				
				return object;
			}
		}
		
		return null;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean containsCorrNodesOfAllTypes(EList<Class<?>> types) {
		for ( final Class<?> type : types ) {
			boolean containsType = false;
			
			// Important, use either the source or taget model elements (and not the correspondence nodes)
			// because external elements the corr nodes property os not valued for external model elements
			for ( final TGGNode corrNode : getSourceModelElements().values() ) {
				if ( type.isInstance( corrNode ) ) {
					containsType = true;
					
					break;
				}
			}
			
			if ( !containsType ) {
				return false;
			}
		}
		
		return true;
	}

	private void pruneMappedReferencedObjects( 	final EObject p_object,
												final Collection<EObject> p_mappedElements ) {
		for ( final EReference ref : p_object.eClass().getEAllReferences() ) {
			if ( ref.isChangeable() ) {
				if ( ref.isMany() ) {
					@SuppressWarnings("unchecked")
					final Iterator<EObject> elementsIt = ( (Collection<EObject>) p_object.eGet( ref ) ).iterator();
					
					while ( elementsIt.hasNext() ) {
						if ( p_mappedElements.contains( elementsIt.next() ) ) {
							elementsIt.remove();
						}
					}
				}
				else {
					final Object refObj = p_object.eGet( ref );
					
					if ( p_mappedElements.contains( refObj ) ) {
						p_object.eUnset( ref );
					}
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RulesPackage.TGG_RULE_SET__AXIOM:
				if (axiom != null)
					msgs = ((InternalEObject)axiom).eInverseRemove(this, RulesPackage.TGG_AXIOM__RULE_SET, TGGAxiom.class, msgs);
				return basicSetAxiom((TGGAxiom)otherEnd, msgs);
			case RulesPackage.TGG_RULE_SET__RULES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRules()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RulesPackage.TGG_RULE_SET__AXIOM:
				return basicSetAxiom(null, msgs);
			case RulesPackage.TGG_RULE_SET__RULES:
				return ((InternalEList<?>)getRules()).basicRemove(otherEnd, msgs);
			case RulesPackage.TGG_RULE_SET__CORRESPONDENCE_NODES:
				return ((InternalEList<?>)getCorrespondenceNodes()).basicRemove(otherEnd, msgs);
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS:
				return ((InternalEList<?>)getSourceModelElements()).basicRemove(otherEnd, msgs);
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS:
				return ((InternalEList<?>)getTargetModelElements()).basicRemove(otherEnd, msgs);
			case RulesPackage.TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS:
				return ((InternalEList<?>)getUncoveredSourceModelElements()).basicRemove(otherEnd, msgs);
			case RulesPackage.TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS:
				return ((InternalEList<?>)getUncoveredTargetModelElements()).basicRemove(otherEnd, msgs);
			case RulesPackage.TGG_RULE_SET__EXTERNAL_REF_PATTERNS:
				return ((InternalEList<?>)getExternalRefPatterns()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RulesPackage.TGG_RULE_SET__ROOT_CORR_NODE:
				return getRootCorrNode();
			case RulesPackage.TGG_RULE_SET__AXIOM:
				return getAxiom();
			case RulesPackage.TGG_RULE_SET__RULES:
				return getRules();
			case RulesPackage.TGG_RULE_SET__PROCESS_NOTIFICATIONS:
				return isProcessNotifications();
			case RulesPackage.TGG_RULE_SET__CORRESPONDENCE_NODES:
				if (coreType) return getCorrespondenceNodes();
				else return getCorrespondenceNodes().map();
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS:
				if (coreType) return getSourceModelElements();
				else return getSourceModelElements().map();
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS:
				if (coreType) return getTargetModelElements();
				else return getTargetModelElements().map();
			case RulesPackage.TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS:
				if (coreType) return getUncoveredSourceModelElements();
				else return getUncoveredSourceModelElements().map();
			case RulesPackage.TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS:
				if (coreType) return getUncoveredTargetModelElements();
				else return getUncoveredTargetModelElements().map();
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS_ADAPTER:
				return getSourceModelElementsAdapter();
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS_ADAPTER:
				return getTargetModelElementsAdapter();
			case RulesPackage.TGG_RULE_SET__TRANSFORMATION_QUEUE:
				return getTransformationQueue();
			case RulesPackage.TGG_RULE_SET__SOURCE_REVERSE_NAVIGATION_STORE:
				return getSourceReverseNavigationStore();
			case RulesPackage.TGG_RULE_SET__TARGET_REVERSE_NAVIGATION_STORE:
				return getTargetReverseNavigationStore();
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ROOT_NODE:
				return getSourceModelRootNode();
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ROOT_NODE:
				return getTargetModelRootNode();
			case RulesPackage.TGG_RULE_SET__CORR_NODES_TO_DELETE:
				return getCorrNodesToDelete();
			case RulesPackage.TGG_RULE_SET__RESOURCE_SET:
				return getResourceSet();
			case RulesPackage.TGG_RULE_SET__ID:
				return getId();
			case RulesPackage.TGG_RULE_SET__EXTERNAL_REFERENCES:
				return getExternalReferences();
			case RulesPackage.TGG_RULE_SET__EXTERNAL_REF_PATTERNS:
				return getExternalRefPatterns();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RulesPackage.TGG_RULE_SET__ROOT_CORR_NODE:
				setRootCorrNode((TGGNode)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__AXIOM:
				setAxiom((TGGAxiom)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__RULES:
				getRules().clear();
				getRules().addAll((Collection<? extends TGGRule>)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__PROCESS_NOTIFICATIONS:
				setProcessNotifications((Boolean)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__CORRESPONDENCE_NODES:
				((EStructuralFeature.Setting)getCorrespondenceNodes()).set(newValue);
				return;
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS:
				((EStructuralFeature.Setting)getSourceModelElements()).set(newValue);
				return;
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS:
				((EStructuralFeature.Setting)getTargetModelElements()).set(newValue);
				return;
			case RulesPackage.TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS:
				((EStructuralFeature.Setting)getUncoveredSourceModelElements()).set(newValue);
				return;
			case RulesPackage.TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS:
				((EStructuralFeature.Setting)getUncoveredTargetModelElements()).set(newValue);
				return;
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS_ADAPTER:
				setSourceModelElementsAdapter((Adapter)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS_ADAPTER:
				setTargetModelElementsAdapter((Adapter)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__TRANSFORMATION_QUEUE:
				setTransformationQueue((TransformationQueue)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__SOURCE_REVERSE_NAVIGATION_STORE:
				setSourceReverseNavigationStore((ReverseNavigationStore)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__TARGET_REVERSE_NAVIGATION_STORE:
				setTargetReverseNavigationStore((ReverseNavigationStore)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ROOT_NODE:
				setSourceModelRootNode((EObject)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ROOT_NODE:
				setTargetModelRootNode((EObject)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__CORR_NODES_TO_DELETE:
				getCorrNodesToDelete().clear();
				getCorrNodesToDelete().addAll((Collection<? extends TGGNode>)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__RESOURCE_SET:
				setResourceSet((ResourceSet)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__ID:
				setId((String)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__EXTERNAL_REFERENCES:
				getExternalReferences().clear();
				getExternalReferences().addAll((Collection<? extends Method>)newValue);
				return;
			case RulesPackage.TGG_RULE_SET__EXTERNAL_REF_PATTERNS:
				getExternalRefPatterns().clear();
				getExternalRefPatterns().addAll((Collection<? extends ReferencePattern>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RulesPackage.TGG_RULE_SET__ROOT_CORR_NODE:
				setRootCorrNode((TGGNode)null);
				return;
			case RulesPackage.TGG_RULE_SET__AXIOM:
				setAxiom((TGGAxiom)null);
				return;
			case RulesPackage.TGG_RULE_SET__RULES:
				getRules().clear();
				return;
			case RulesPackage.TGG_RULE_SET__PROCESS_NOTIFICATIONS:
				setProcessNotifications(PROCESS_NOTIFICATIONS_EDEFAULT);
				return;
			case RulesPackage.TGG_RULE_SET__CORRESPONDENCE_NODES:
				getCorrespondenceNodes().clear();
				return;
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS:
				getSourceModelElements().clear();
				return;
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS:
				getTargetModelElements().clear();
				return;
			case RulesPackage.TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS:
				getUncoveredSourceModelElements().clear();
				return;
			case RulesPackage.TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS:
				getUncoveredTargetModelElements().clear();
				return;
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS_ADAPTER:
				setSourceModelElementsAdapter(SOURCE_MODEL_ELEMENTS_ADAPTER_EDEFAULT);
				return;
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS_ADAPTER:
				setTargetModelElementsAdapter(TARGET_MODEL_ELEMENTS_ADAPTER_EDEFAULT);
				return;
			case RulesPackage.TGG_RULE_SET__TRANSFORMATION_QUEUE:
				setTransformationQueue((TransformationQueue)null);
				return;
			case RulesPackage.TGG_RULE_SET__SOURCE_REVERSE_NAVIGATION_STORE:
				setSourceReverseNavigationStore((ReverseNavigationStore)null);
				return;
			case RulesPackage.TGG_RULE_SET__TARGET_REVERSE_NAVIGATION_STORE:
				setTargetReverseNavigationStore((ReverseNavigationStore)null);
				return;
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ROOT_NODE:
				setSourceModelRootNode((EObject)null);
				return;
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ROOT_NODE:
				setTargetModelRootNode((EObject)null);
				return;
			case RulesPackage.TGG_RULE_SET__CORR_NODES_TO_DELETE:
				getCorrNodesToDelete().clear();
				return;
			case RulesPackage.TGG_RULE_SET__RESOURCE_SET:
				setResourceSet(RESOURCE_SET_EDEFAULT);
				return;
			case RulesPackage.TGG_RULE_SET__ID:
				setId(ID_EDEFAULT);
				return;
			case RulesPackage.TGG_RULE_SET__EXTERNAL_REFERENCES:
				getExternalReferences().clear();
				return;
			case RulesPackage.TGG_RULE_SET__EXTERNAL_REF_PATTERNS:
				getExternalRefPatterns().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RulesPackage.TGG_RULE_SET__ROOT_CORR_NODE:
				return rootCorrNode != null;
			case RulesPackage.TGG_RULE_SET__AXIOM:
				return axiom != null;
			case RulesPackage.TGG_RULE_SET__RULES:
				return rules != null && !rules.isEmpty();
			case RulesPackage.TGG_RULE_SET__PROCESS_NOTIFICATIONS:
				return processNotifications != PROCESS_NOTIFICATIONS_EDEFAULT;
			case RulesPackage.TGG_RULE_SET__CORRESPONDENCE_NODES:
				return correspondenceNodes != null && !correspondenceNodes.isEmpty();
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS:
				return sourceModelElements != null && !sourceModelElements.isEmpty();
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS:
				return targetModelElements != null && !targetModelElements.isEmpty();
			case RulesPackage.TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS:
				return uncoveredSourceModelElements != null && !uncoveredSourceModelElements.isEmpty();
			case RulesPackage.TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS:
				return uncoveredTargetModelElements != null && !uncoveredTargetModelElements.isEmpty();
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS_ADAPTER:
				return SOURCE_MODEL_ELEMENTS_ADAPTER_EDEFAULT == null ? sourceModelElementsAdapter != null : !SOURCE_MODEL_ELEMENTS_ADAPTER_EDEFAULT.equals(sourceModelElementsAdapter);
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS_ADAPTER:
				return TARGET_MODEL_ELEMENTS_ADAPTER_EDEFAULT == null ? targetModelElementsAdapter != null : !TARGET_MODEL_ELEMENTS_ADAPTER_EDEFAULT.equals(targetModelElementsAdapter);
			case RulesPackage.TGG_RULE_SET__TRANSFORMATION_QUEUE:
				return transformationQueue != null;
			case RulesPackage.TGG_RULE_SET__SOURCE_REVERSE_NAVIGATION_STORE:
				return sourceReverseNavigationStore != null;
			case RulesPackage.TGG_RULE_SET__TARGET_REVERSE_NAVIGATION_STORE:
				return targetReverseNavigationStore != null;
			case RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ROOT_NODE:
				return sourceModelRootNode != null;
			case RulesPackage.TGG_RULE_SET__TARGET_MODEL_ROOT_NODE:
				return targetModelRootNode != null;
			case RulesPackage.TGG_RULE_SET__CORR_NODES_TO_DELETE:
				return corrNodesToDelete != null && !corrNodesToDelete.isEmpty();
			case RulesPackage.TGG_RULE_SET__RESOURCE_SET:
				return RESOURCE_SET_EDEFAULT == null ? resourceSet != null : !RESOURCE_SET_EDEFAULT.equals(resourceSet);
			case RulesPackage.TGG_RULE_SET__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case RulesPackage.TGG_RULE_SET__EXTERNAL_REFERENCES:
				return externalReferences != null && !externalReferences.isEmpty();
			case RulesPackage.TGG_RULE_SET__EXTERNAL_REF_PATTERNS:
				return externalRefPatterns != null && !externalRefPatterns.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
//		result.append(" (processNotifications: ");
//		result.append(processNotifications);
//		result.append(", sourceModelElementsAdapter: ");
//		result.append(sourceModelElementsAdapter);
//		result.append(", targetModelElementsAdapter: ");
//		result.append(targetModelElementsAdapter);
//		result.append(", resourceSet: ");
//		result.append(resourceSet);
		result.append("(id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

	/**
	 * Return a collection of objects not contained in the specified resource and referred by the
	 * specified object, either directly or recursively through references to other objects contained in the
	 * specified resource.
	 * @param p_containingRes
	 * @param p_object
	 * @return
	 * @throws ResourceAccessException
	 */
	@SuppressWarnings("unchecked")
	public EMap<EObject, EList<EReference>> externalReferences(	final Resource p_containingRes,
																final EObject p_object,
																final EStructuralFeature p_feature )
	throws UnresolvedRefTransException {
		final EMap<EObject, EList<EReference>> extRefs = externalReferences( p_containingRes, p_object, p_feature, new HashSet<EObject>() );
		
		// Then look for artificial references declared as methods in the TGG and that that may refer to external objects
		for ( final Method extRef : getExternalReferences() ) {
			final Class<?> firstParamType = extRef.getParameterTypes()[ 0 ];
			
			if ( firstParamType.isInstance( p_object ) ) {
				try {
					Object result = extRef.invoke( null, new Object[] { p_object } );
					
					if ( result != null ) {
						if ( result instanceof Collection<?> ) {
							for ( final EObject extElem : (Collection<EObject>) result ) {
								if ( extElem.eResource() != p_containingRes && !extRefs.containsKey( extElem ) ) {
									extRefs.put( extElem, null );
								}
							}
						}
						else {
							final EObject extElem = (EObject) result;
							
							if ( extElem.eResource() != p_containingRes && !extRefs.containsKey( extElem ) ) {
								extRefs.put( extElem, null );
							}
						}
					}
				}
				catch ( final IllegalAccessException p_ex ) {
					throw new UnresolvedRefTransException( p_ex );
				}
				catch ( final IllegalArgumentException p_ex ) {
					throw new UnresolvedRefTransException( p_ex );
				}
				catch ( final InvocationTargetException p_ex ) {
					throw new UnresolvedRefTransException( p_ex.getTargetException() );
				}
			}
		}
		
		return extRefs;
	}

	/**
	 * Return a collection of objects not contained in the specified resource and referred by the
	 * specified object, either directly or recursively through references to other objects contained in the
	 * specified resource.
	 * @param p_parentRefRes
	 * @param p_object
	 * @param p_travObjects
	 * @return
	 * @throws UnresolvedRefTransException
	 */
	private static EMap<EObject, EList<EReference>> externalReferences(	final Resource p_parentResource,
																		final EObject p_object,
																		final EStructuralFeature p_feature,
																		final Set<EObject> p_travObjects )
	throws UnresolvedRefTransException {
		final EMap<EObject, EList<EReference>> refs = new BasicEMap<EObject, EList<EReference>>();
		
		if ( !p_travObjects.contains( p_object ) ) {
			p_travObjects.add( p_object );
			final Resource crossRes = p_object.eResource();
			
			if ( crossRes == null ) {
				throw new UnresolvedRefTransException( "Unresolved resource for object " + p_object, EcoreUtil.getURI( p_object ) );
			}
	
			// The object is outside so we need it
			if ( crossRes != p_parentResource ) {
				EList<EReference> features = refs.get( p_object );
				
				if ( features == null ) {
					features = new BasicEList<EReference>();
					refs.put( p_object, features );
				}
				
				if ( p_feature instanceof EReference && !features.contains( p_feature ) ) {
					features.add( (EReference) p_feature );
				}
			}
			else {
//				final EObject container = p_object.eContainer();
//				
//				if ( container != null ) {
//					refs.addAll( externalReferences( p_parentResource, container, null, p_travObjects )  );
//				}

				// The object is inside so we need to see if any of its cross references has a reference to the outside
				final EContentsEList.FeatureIterator<EObject> iterator = (EContentsEList.FeatureIterator<EObject>) p_object.eCrossReferences().iterator();
				
				while ( iterator.hasNext() ) {
					final EObject object = iterator.next();
					refs.addAll( externalReferences( p_parentResource, object, iterator.feature(), p_travObjects ) );
				}
			}
		}
		
		return refs;
	}
} // TGGRuleSetImpl
