/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.helpers;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.mote.rules.TGGRuleSet;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Rule Set Tag</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The ruleSetTag stores information about a ruleSet.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.helpers.RuleSetTag#getRuleSetID <em>Rule Set ID</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.RuleSetTag#getSourceModelUri <em>Source Model Uri</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.RuleSetTag#getTargetModelUri <em>Target Model Uri</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.RuleSetTag#getRuleSet <em>Rule Set</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.RuleSetTag#getRuleSetName <em>Rule Set Name</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.RuleSetTag#getSourceModelID <em>Source Model ID</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.RuleSetTag#getTargetModelID <em>Target Model ID</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.RuleSetTag#getRuleSetPackageNsURI <em>Rule Set Package Ns URI</em>}</li>
 * </ul>
 *
 * @see de.hpi.sam.mote.helpers.HelpersPackage#getRuleSetTag()
 * @model
 * @generated
 */
public interface RuleSetTag extends EObject {
	/**
	 * Returns the value of the '<em><b>Rule Set ID</b></em>' attribute. The
	 * default value is <code>""</code>. <!-- begin-user-doc --> <!--
	 * end-user-doc --> <!-- begin-model-doc --> The ID of the ruleSet. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Rule Set ID</em>' attribute.
	 * @see #setRuleSetID(String)
	 * @see de.hpi.sam.mote.helpers.HelpersPackage#getRuleSetTag_RuleSetID()
	 * @model default=""
	 * @generated
	 */
	String getRuleSetID();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.helpers.RuleSetTag#getRuleSetID <em>Rule Set ID</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Rule Set ID</em>' attribute.
	 * @see #getRuleSetID()
	 * @generated
	 */
	void setRuleSetID(String value);

	/**
	 * Returns the value of the '<em><b>Rule Set Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> The
	 * name of the ruleSet. This will be used on the UI. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Rule Set Name</em>' attribute.
	 * @see #setRuleSetName(String)
	 * @see de.hpi.sam.mote.helpers.HelpersPackage#getRuleSetTag_RuleSetName()
	 * @model
	 * @generated
	 */
	String getRuleSetName();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.helpers.RuleSetTag#getRuleSetName <em>Rule Set Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Rule Set Name</em>' attribute.
	 * @see #getRuleSetName()
	 * @generated
	 */
	void setRuleSetName(String value);

	/**
	 * Returns the value of the '<em><b>Source Model ID</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> ID
	 * of the source models that can be transformed by this ruleSet. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Source Model ID</em>' attribute.
	 * @see #setSourceModelID(String)
	 * @see de.hpi.sam.mote.helpers.HelpersPackage#getRuleSetTag_SourceModelID()
	 * @model
	 * @generated
	 */
	String getSourceModelID();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.helpers.RuleSetTag#getSourceModelID <em>Source Model ID</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Source Model ID</em>' attribute.
	 * @see #getSourceModelID()
	 * @generated
	 */
	void setSourceModelID(String value);

	/**
	 * Returns the value of the '<em><b>Target Model ID</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> ID
	 * of the target models that can be transformed by this ruleSet. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Target Model ID</em>' attribute.
	 * @see #setTargetModelID(String)
	 * @see de.hpi.sam.mote.helpers.HelpersPackage#getRuleSetTag_TargetModelID()
	 * @model
	 * @generated
	 */
	String getTargetModelID();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.helpers.RuleSetTag#getTargetModelID <em>Target Model ID</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Target Model ID</em>' attribute.
	 * @see #getTargetModelID()
	 * @generated
	 */
	void setTargetModelID(String value);

	/**
	 * Returns the value of the '<em><b>Rule Set Package Ns URI</b></em>'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> Name of the factory that is used to create the
	 * ruleSet. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Rule Set Package Ns URI</em>' attribute.
	 * @see #setRuleSetPackageNsURI(String)
	 * @see de.hpi.sam.mote.helpers.HelpersPackage#getRuleSetTag_RuleSetPackageNsURI()
	 * @model
	 * @generated
	 */
	String getRuleSetPackageNsURI();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.helpers.RuleSetTag#getRuleSetPackageNsURI <em>Rule Set Package Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule Set Package Ns URI</em>' attribute.
	 * @see #getRuleSetPackageNsURI()
	 * @generated
	 */
	void setRuleSetPackageNsURI(String value);

	/**
	 * Returns the value of the '<em><b>Source Model Uri</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * URI of the source model. Used by the transformation engine to indentify
	 * proper ruleSets. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Source Model Uri</em>' attribute.
	 * @see #setSourceModelUri(URI)
	 * @see de.hpi.sam.mote.helpers.HelpersPackage#getRuleSetTag_SourceModelUri()
	 * @model dataType="de.hpi.sam.mote.helpers.URI"
	 * @generated
	 */
	URI getSourceModelUri();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.helpers.RuleSetTag#getSourceModelUri <em>Source Model Uri</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Source Model Uri</em>' attribute.
	 * @see #getSourceModelUri()
	 * @generated
	 */
	void setSourceModelUri(URI value);

	/**
	 * Returns the value of the '<em><b>Target Model Uri</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * URI of the target model. Used by the transformation engine to indentify
	 * proper ruleSets. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Target Model Uri</em>' attribute.
	 * @see #setTargetModelUri(URI)
	 * @see de.hpi.sam.mote.helpers.HelpersPackage#getRuleSetTag_TargetModelUri()
	 * @model dataType="de.hpi.sam.mote.helpers.URI"
	 * @generated
	 */
	URI getTargetModelUri();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.helpers.RuleSetTag#getTargetModelUri <em>Target Model Uri</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Target Model Uri</em>' attribute.
	 * @see #getTargetModelUri()
	 * @generated
	 */
	void setTargetModelUri(URI value);

	/**
	 * Returns the value of the '<em><b>Rule Set</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Set</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Rule Set</em>' reference.
	 * @see #setRuleSet(TGGRuleSet)
	 * @see de.hpi.sam.mote.helpers.HelpersPackage#getRuleSetTag_RuleSet()
	 * @model resolveProxies="false" ordered="false"
	 * @generated
	 */
	TGGRuleSet getRuleSet();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.helpers.RuleSetTag#getRuleSet <em>Rule Set</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule Set</em>' reference.
	 * @see #getRuleSet()
	 * @generated
	 */
	void setRuleSet(TGGRuleSet value);

} // RuleSetTag
