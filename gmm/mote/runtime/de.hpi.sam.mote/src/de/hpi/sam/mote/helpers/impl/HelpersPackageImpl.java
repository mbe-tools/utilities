/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.helpers.impl;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.mote.MotePackage;
import de.hpi.sam.mote.helpers.HelpersFactory;
import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.helpers.ModificationTag;
import de.hpi.sam.mote.helpers.ReverseNavigationStore;
import de.hpi.sam.mote.helpers.RuleSetTag;
import de.hpi.sam.mote.impl.MotePackageImpl;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.impl.UnresolvedRefTransException;
import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.mote.rules.impl.RulesPackageImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class HelpersPackageImpl extends EPackageImpl implements HelpersPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mapEntryEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modificationTagEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleSetTagEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reverseNavigationStoreEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType adapterEDataType = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType transformationExceptionEDataType = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType uriEDataType = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType iProgressMonitorEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType unresolvedRefTransExceptionEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.mote.helpers.HelpersPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private HelpersPackageImpl() {
		super(eNS_URI, HelpersFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link HelpersPackage#eINSTANCE} when
	 * that field is accessed. Clients should not invoke it directly. Instead,
	 * they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static HelpersPackage init() {
		if (isInited) return (HelpersPackage)EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI);

		// Obtain or create and register package
		HelpersPackageImpl theHelpersPackage = (HelpersPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof HelpersPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new HelpersPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		MotePackageImpl theMotePackage = (MotePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MotePackage.eNS_URI) instanceof MotePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MotePackage.eNS_URI) : MotePackage.eINSTANCE);
		RulesPackageImpl theRulesPackage = (RulesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI) instanceof RulesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI) : RulesPackage.eINSTANCE);

		// Create package meta-data objects
		theHelpersPackage.createPackageContents();
		theMotePackage.createPackageContents();
		theRulesPackage.createPackageContents();

		// Initialize created meta-data
		theHelpersPackage.initializePackageContents();
		theMotePackage.initializePackageContents();
		theRulesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theHelpersPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(HelpersPackage.eNS_URI, theHelpersPackage);
		return theHelpersPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMapEntry() {
		return mapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMapEntry_Key() {
		return (EAttribute)mapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMapEntry_Value() {
		return (EAttribute)mapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getModificationTag() {
		return modificationTagEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getModificationTag_CorrespondenceNode() {
		return (EReference)modificationTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getModificationTag_Synchronize() {
		return (EAttribute)modificationTagEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRuleSetTag() {
		return ruleSetTagEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRuleSetTag_RuleSetID() {
		return (EAttribute)ruleSetTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRuleSetTag_RuleSetName() {
		return (EAttribute)ruleSetTagEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRuleSetTag_SourceModelID() {
		return (EAttribute)ruleSetTagEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRuleSetTag_TargetModelID() {
		return (EAttribute)ruleSetTagEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRuleSetTag_RuleSetPackageNsURI() {
		return (EAttribute)ruleSetTagEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRuleSetTag_SourceModelUri() {
		return (EAttribute)ruleSetTagEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRuleSetTag_TargetModelUri() {
		return (EAttribute)ruleSetTagEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRuleSetTag_RuleSet() {
		return (EReference)ruleSetTagEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getReverseNavigationStore() {
		return reverseNavigationStoreEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getAdapter() {
		return adapterEDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getTransformationException() {
		return transformationExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getURI() {
		return uriEDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getIProgressMonitor() {
		return iProgressMonitorEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getUnresolvedRefTransException() {
		return unresolvedRefTransExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public HelpersFactory getHelpersFactory() {
		return (HelpersFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		mapEntryEClass = createEClass(MAP_ENTRY);
		createEAttribute(mapEntryEClass, MAP_ENTRY__KEY);
		createEAttribute(mapEntryEClass, MAP_ENTRY__VALUE);

		modificationTagEClass = createEClass(MODIFICATION_TAG);
		createEReference(modificationTagEClass, MODIFICATION_TAG__CORRESPONDENCE_NODE);
		createEAttribute(modificationTagEClass, MODIFICATION_TAG__SYNCHRONIZE);

		ruleSetTagEClass = createEClass(RULE_SET_TAG);
		createEAttribute(ruleSetTagEClass, RULE_SET_TAG__RULE_SET_ID);
		createEAttribute(ruleSetTagEClass, RULE_SET_TAG__SOURCE_MODEL_URI);
		createEAttribute(ruleSetTagEClass, RULE_SET_TAG__TARGET_MODEL_URI);
		createEReference(ruleSetTagEClass, RULE_SET_TAG__RULE_SET);
		createEAttribute(ruleSetTagEClass, RULE_SET_TAG__RULE_SET_NAME);
		createEAttribute(ruleSetTagEClass, RULE_SET_TAG__SOURCE_MODEL_ID);
		createEAttribute(ruleSetTagEClass, RULE_SET_TAG__TARGET_MODEL_ID);
		createEAttribute(ruleSetTagEClass, RULE_SET_TAG__RULE_SET_PACKAGE_NS_URI);

		reverseNavigationStoreEClass = createEClass(REVERSE_NAVIGATION_STORE);

		// Create data types
		adapterEDataType = createEDataType(ADAPTER);
		transformationExceptionEDataType = createEDataType(TRANSFORMATION_EXCEPTION);
		uriEDataType = createEDataType(URI);
		iProgressMonitorEDataType = createEDataType(IPROGRESS_MONITOR);
		unresolvedRefTransExceptionEDataType = createEDataType(UNRESOLVED_REF_TRANS_EXCEPTION);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MotePackage theMotePackage = (MotePackage)EPackage.Registry.INSTANCE.getEPackage(MotePackage.eNS_URI);
		RulesPackage theRulesPackage = (RulesPackage)EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI);

		// Create type parameters
		ETypeParameter mapEntryEClass_KeyType = addETypeParameter(mapEntryEClass, "KeyType");
		ETypeParameter mapEntryEClass_ValueType = addETypeParameter(mapEntryEClass, "ValueType");

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(mapEntryEClass, Map.Entry.class, "MapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		EGenericType g1 = createEGenericType(mapEntryEClass_KeyType);
		initEAttribute(getMapEntry_Key(), g1, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(mapEntryEClass_ValueType);
		initEAttribute(getMapEntry_Value(), g1, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(modificationTagEClass, ModificationTag.class, "ModificationTag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModificationTag_CorrespondenceNode(), theMotePackage.getTGGNode(), null, "correspondenceNode", null, 0, 1, ModificationTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getModificationTag_Synchronize(), ecorePackage.getEBoolean(), "synchronize", null, 0, 1, ModificationTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ruleSetTagEClass, RuleSetTag.class, "RuleSetTag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRuleSetTag_RuleSetID(), ecorePackage.getEString(), "ruleSetID", "", 0, 1, RuleSetTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuleSetTag_SourceModelUri(), this.getURI(), "sourceModelUri", null, 0, 1, RuleSetTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuleSetTag_TargetModelUri(), this.getURI(), "targetModelUri", null, 0, 1, RuleSetTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleSetTag_RuleSet(), theRulesPackage.getTGGRuleSet(), null, "ruleSet", null, 0, 1, RuleSetTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRuleSetTag_RuleSetName(), ecorePackage.getEString(), "ruleSetName", null, 0, 1, RuleSetTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuleSetTag_SourceModelID(), ecorePackage.getEString(), "sourceModelID", null, 0, 1, RuleSetTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuleSetTag_TargetModelID(), ecorePackage.getEString(), "targetModelID", null, 0, 1, RuleSetTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuleSetTag_RuleSetPackageNsURI(), ecorePackage.getEString(), "ruleSetPackageNsURI", null, 0, 1, RuleSetTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(reverseNavigationStoreEClass, ReverseNavigationStore.class, "ReverseNavigationStore", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(reverseNavigationStoreEClass, null, "addLink", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "target", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEReference(), "eReference", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(reverseNavigationStoreEClass, null, "removeLink", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "target", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEReference(), "eReference", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(reverseNavigationStoreEClass, ecorePackage.getEObject(), "getLinkSources", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "target", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEReference(), "eReference", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(reverseNavigationStoreEClass, null, "clear", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(adapterEDataType, Adapter.class, "Adapter", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(transformationExceptionEDataType, TransformationException.class, "TransformationException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(uriEDataType, org.eclipse.emf.common.util.URI.class, "URI", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iProgressMonitorEDataType, IProgressMonitor.class, "IProgressMonitor", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(unresolvedRefTransExceptionEDataType, UnresolvedRefTransException.class, "UnresolvedRefTransException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
	}

} // HelpersPackageImpl
