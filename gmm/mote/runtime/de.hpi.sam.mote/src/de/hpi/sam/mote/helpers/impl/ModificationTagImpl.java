/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.helpers.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.helpers.ModificationTag;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Modification Tag</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.helpers.impl.ModificationTagImpl#getCorrespondenceNode <em>Correspondence Node</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.impl.ModificationTagImpl#isSynchronize <em>Synchronize</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModificationTagImpl extends EObjectImpl implements ModificationTag {
	/**
	 * The cached value of the '{@link #getCorrespondenceNode() <em>Correspondence Node</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCorrespondenceNode()
	 * @generated
	 * @ordered
	 */
	protected TGGNode correspondenceNode;

	/**
	 * The default value of the '{@link #isSynchronize() <em>Synchronize</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isSynchronize()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SYNCHRONIZE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSynchronize() <em>Synchronize</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isSynchronize()
	 * @generated
	 * @ordered
	 */
	protected boolean synchronize = SYNCHRONIZE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ModificationTagImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HelpersPackage.Literals.MODIFICATION_TAG;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGNode getCorrespondenceNode() {
		return correspondenceNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCorrespondenceNode(TGGNode newCorrespondenceNode) {
		TGGNode oldCorrespondenceNode = correspondenceNode;
		correspondenceNode = newCorrespondenceNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.MODIFICATION_TAG__CORRESPONDENCE_NODE, oldCorrespondenceNode, correspondenceNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSynchronize() {
		return synchronize;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSynchronize(boolean newSynchronize) {
		boolean oldSynchronize = synchronize;
		synchronize = newSynchronize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.MODIFICATION_TAG__SYNCHRONIZE, oldSynchronize, synchronize));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HelpersPackage.MODIFICATION_TAG__CORRESPONDENCE_NODE:
				return getCorrespondenceNode();
			case HelpersPackage.MODIFICATION_TAG__SYNCHRONIZE:
				return isSynchronize();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HelpersPackage.MODIFICATION_TAG__CORRESPONDENCE_NODE:
				setCorrespondenceNode((TGGNode)newValue);
				return;
			case HelpersPackage.MODIFICATION_TAG__SYNCHRONIZE:
				setSynchronize((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HelpersPackage.MODIFICATION_TAG__CORRESPONDENCE_NODE:
				setCorrespondenceNode((TGGNode)null);
				return;
			case HelpersPackage.MODIFICATION_TAG__SYNCHRONIZE:
				setSynchronize(SYNCHRONIZE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HelpersPackage.MODIFICATION_TAG__CORRESPONDENCE_NODE:
				return correspondenceNode != null;
			case HelpersPackage.MODIFICATION_TAG__SYNCHRONIZE:
				return synchronize != SYNCHRONIZE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (synchronize: ");
		result.append(synchronize);
		result.append(')');
		return result.toString();
	}

} // ModificationTagImpl
