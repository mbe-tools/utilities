/**
 */
package de.hpi.sam.mote.rules.impl;

import de.hpi.sam.mote.TransformationDirection;

import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.mote.rules.TransformationExecutionResult;
import de.hpi.sam.mote.rules.TransformationResult;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transformation Execution Result</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TransformationExecutionResultImpl#getTransformationResult <em>Transformation Result</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TransformationExecutionResultImpl#getTransformationDirection <em>Transformation Direction</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TransformationExecutionResultImpl#getLeftUncoveredElements <em>Left Uncovered Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TransformationExecutionResultImpl#getRightUncoveredElements <em>Right Uncovered Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TransformationExecutionResultImpl#getExecutionTrace <em>Execution Trace</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransformationExecutionResultImpl extends EObjectImpl implements TransformationExecutionResult {
	/**
	 * The default value of the '{@link #getTransformationResult() <em>Transformation Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransformationResult()
	 * @generated
	 * @ordered
	 */
	protected static final TransformationResult TRANSFORMATION_RESULT_EDEFAULT = TransformationResult.RULE_APPLIED;

	/**
	 * The cached value of the '{@link #getTransformationResult() <em>Transformation Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransformationResult()
	 * @generated
	 * @ordered
	 */
	protected TransformationResult transformationResult = TRANSFORMATION_RESULT_EDEFAULT;

	/**
	 * The default value of the '{@link #getTransformationDirection() <em>Transformation Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransformationDirection()
	 * @generated
	 * @ordered
	 */
	protected static final TransformationDirection TRANSFORMATION_DIRECTION_EDEFAULT = TransformationDirection.FORWARD;

	/**
	 * The cached value of the '{@link #getTransformationDirection() <em>Transformation Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransformationDirection()
	 * @generated
	 * @ordered
	 */
	protected TransformationDirection transformationDirection = TRANSFORMATION_DIRECTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLeftUncoveredElements() <em>Left Uncovered Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftUncoveredElements()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> leftUncoveredElements;

	/**
	 * The cached value of the '{@link #getRightUncoveredElements() <em>Right Uncovered Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightUncoveredElements()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> rightUncoveredElements;

	/**
	 * The cached value of the '{@link #getExecutionTrace() <em>Execution Trace</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionTrace()
	 * @generated
	 * @ordered
	 */
	protected EObject executionTrace;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransformationExecutionResultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RulesPackage.Literals.TRANSFORMATION_EXECUTION_RESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationResult getTransformationResult() {
		return transformationResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransformationResult(TransformationResult newTransformationResult) {
		TransformationResult oldTransformationResult = transformationResult;
		transformationResult = newTransformationResult == null ? TRANSFORMATION_RESULT_EDEFAULT : newTransformationResult;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_RESULT, oldTransformationResult, transformationResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationDirection getTransformationDirection() {
		return transformationDirection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransformationDirection(TransformationDirection newTransformationDirection) {
		TransformationDirection oldTransformationDirection = transformationDirection;
		transformationDirection = newTransformationDirection == null ? TRANSFORMATION_DIRECTION_EDEFAULT : newTransformationDirection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_DIRECTION, oldTransformationDirection, transformationDirection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getLeftUncoveredElements() {
		if (leftUncoveredElements == null) {
			leftUncoveredElements = new EObjectResolvingEList<EObject>(EObject.class, this, RulesPackage.TRANSFORMATION_EXECUTION_RESULT__LEFT_UNCOVERED_ELEMENTS);
		}
		return leftUncoveredElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getRightUncoveredElements() {
		if (rightUncoveredElements == null) {
			rightUncoveredElements = new EObjectResolvingEList<EObject>(EObject.class, this, RulesPackage.TRANSFORMATION_EXECUTION_RESULT__RIGHT_UNCOVERED_ELEMENTS);
		}
		return rightUncoveredElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getExecutionTrace() {
		if (executionTrace != null && executionTrace.eIsProxy()) {
			InternalEObject oldExecutionTrace = (InternalEObject)executionTrace;
			executionTrace = eResolveProxy(oldExecutionTrace);
			if (executionTrace != oldExecutionTrace) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RulesPackage.TRANSFORMATION_EXECUTION_RESULT__EXECUTION_TRACE, oldExecutionTrace, executionTrace));
			}
		}
		return executionTrace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetExecutionTrace() {
		return executionTrace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionTrace(EObject newExecutionTrace) {
		EObject oldExecutionTrace = executionTrace;
		executionTrace = newExecutionTrace;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TRANSFORMATION_EXECUTION_RESULT__EXECUTION_TRACE, oldExecutionTrace, executionTrace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_RESULT:
				return getTransformationResult();
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_DIRECTION:
				return getTransformationDirection();
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__LEFT_UNCOVERED_ELEMENTS:
				return getLeftUncoveredElements();
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__RIGHT_UNCOVERED_ELEMENTS:
				return getRightUncoveredElements();
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__EXECUTION_TRACE:
				if (resolve) return getExecutionTrace();
				return basicGetExecutionTrace();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_RESULT:
				setTransformationResult((TransformationResult)newValue);
				return;
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_DIRECTION:
				setTransformationDirection((TransformationDirection)newValue);
				return;
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__LEFT_UNCOVERED_ELEMENTS:
				getLeftUncoveredElements().clear();
				getLeftUncoveredElements().addAll((Collection<? extends EObject>)newValue);
				return;
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__RIGHT_UNCOVERED_ELEMENTS:
				getRightUncoveredElements().clear();
				getRightUncoveredElements().addAll((Collection<? extends EObject>)newValue);
				return;
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__EXECUTION_TRACE:
				setExecutionTrace((EObject)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_RESULT:
				setTransformationResult(TRANSFORMATION_RESULT_EDEFAULT);
				return;
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_DIRECTION:
				setTransformationDirection(TRANSFORMATION_DIRECTION_EDEFAULT);
				return;
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__LEFT_UNCOVERED_ELEMENTS:
				getLeftUncoveredElements().clear();
				return;
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__RIGHT_UNCOVERED_ELEMENTS:
				getRightUncoveredElements().clear();
				return;
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__EXECUTION_TRACE:
				setExecutionTrace((EObject)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_RESULT:
				return transformationResult != TRANSFORMATION_RESULT_EDEFAULT;
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_DIRECTION:
				return transformationDirection != TRANSFORMATION_DIRECTION_EDEFAULT;
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__LEFT_UNCOVERED_ELEMENTS:
				return leftUncoveredElements != null && !leftUncoveredElements.isEmpty();
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__RIGHT_UNCOVERED_ELEMENTS:
				return rightUncoveredElements != null && !rightUncoveredElements.isEmpty();
			case RulesPackage.TRANSFORMATION_EXECUTION_RESULT__EXECUTION_TRACE:
				return executionTrace != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (transformationResult: ");
		result.append(transformationResult);
		result.append(", transformationDirection: ");
		result.append(transformationDirection);
		result.append(')');
		return result.toString();
	}

} //TransformationExecutionResultImpl
