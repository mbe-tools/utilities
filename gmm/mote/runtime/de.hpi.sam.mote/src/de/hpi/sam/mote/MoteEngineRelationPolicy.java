/**
 */
package de.hpi.sam.mote;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.Resource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Engine Relation Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hpi.sam.mote.MotePackage#getMoteEngineRelationPolicy()
 * @model
 * @generated
 */
public interface MoteEngineRelationPolicy extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="de.hpi.sam.mote.helpers.URI" resourceRequired="true"
	 * @generated
	 */
	URI correspondingUri(Resource resource);

} // MoteEngineRelationPolicy
