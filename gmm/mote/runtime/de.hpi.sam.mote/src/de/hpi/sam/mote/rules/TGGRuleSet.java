/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules;

import java.lang.reflect.Method;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.TransformationDirection;
import de.hpi.sam.mote.helpers.ReverseNavigationStore;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.impl.UnresolvedRefTransException;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>TGG Rule Set</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getRootCorrNode <em>Root Corr Node</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getAxiom <em>Axiom</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getRules <em>Rules</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#isProcessNotifications <em>Process Notifications</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getCorrespondenceNodes <em>Correspondence Nodes</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getSourceModelElements <em>Source Model Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getTargetModelElements <em>Target Model Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getUncoveredSourceModelElements <em>Uncovered Source Model Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getUncoveredTargetModelElements <em>Uncovered Target Model Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getSourceModelElementsAdapter <em>Source Model Elements Adapter</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getTargetModelElementsAdapter <em>Target Model Elements Adapter</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getTransformationQueue <em>Transformation Queue</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getSourceReverseNavigationStore <em>Source Reverse Navigation Store</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getTargetReverseNavigationStore <em>Target Reverse Navigation Store</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getSourceModelRootNode <em>Source Model Root Node</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getTargetModelRootNode <em>Target Model Root Node</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getCorrNodesToDelete <em>Corr Nodes To Delete</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getResourceSet <em>Resource Set</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getId <em>Id</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getExternalReferences <em>External References</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.TGGRuleSet#getExternalRefPatterns <em>External Ref Patterns</em>}</li>
 * </ul>
 *
 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet()
 * @model
 * @generated
 */
public interface TGGRuleSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Root Corr Node</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Corr Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Root Corr Node</em>' reference.
	 * @see #setRootCorrNode(TGGNode)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_RootCorrNode()
	 * @model resolveProxies="false" ordered="false"
	 * @generated
	 */
	TGGNode getRootCorrNode();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRuleSet#getRootCorrNode <em>Root Corr Node</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Root Corr Node</em>' reference.
	 * @see #getRootCorrNode()
	 * @generated
	 */
	void setRootCorrNode(TGGNode value);

	/**
	 * Returns the value of the '<em><b>Axiom</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.hpi.sam.mote.rules.TGGAxiom#getRuleSet <em>Rule Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Axiom</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Axiom</em>' reference.
	 * @see #setAxiom(TGGAxiom)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_Axiom()
	 * @see de.hpi.sam.mote.rules.TGGAxiom#getRuleSet
	 * @model opposite="ruleSet" resolveProxies="false" ordered="false"
	 * @generated
	 */
	TGGAxiom getAxiom();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRuleSet#getAxiom <em>Axiom</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Axiom</em>' reference.
	 * @see #getAxiom()
	 * @generated
	 */
	void setAxiom(TGGAxiom value);

	/**
	 * Returns the value of the '<em><b>Rules</b></em>' reference list. The list
	 * contents are of type {@link de.hpi.sam.mote.rules.TGGRule}. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.mote.rules.TGGRule#getRuleSet <em>Rule Set</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rules</em>' reference list isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Rules</em>' reference list.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_Rules()
	 * @see de.hpi.sam.mote.rules.TGGRule#getRuleSet
	 * @model opposite="ruleSet" resolveProxies="false" ordered="false"
	 * @generated
	 */
	EList<TGGRule> getRules();

	/**
	 * Returns the value of the '<em><b>Process Notifications</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Notifications</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Notifications</em>' attribute.
	 * @see #setProcessNotifications(boolean)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_ProcessNotifications()
	 * @model
	 * @generated
	 */
	boolean isProcessNotifications();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRuleSet#isProcessNotifications <em>Process Notifications</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Process Notifications</em>' attribute.
	 * @see #isProcessNotifications()
	 * @generated
	 */
	void setProcessNotifications(boolean value);

	/**
	 * Returns the value of the '<em><b>Correspondence Nodes</b></em>' map.
	 * The key is of type {@link KeyType},
	 * and the value is of type {@link ValueType},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correspondence Nodes</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correspondence Nodes</em>' map.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_CorrespondenceNodes()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>" ordered="false"
	 * @generated
	 */
	EMap<TGGNode, Object> getCorrespondenceNodes();

	/**
	 * Returns the value of the '<em><b>Source Model Elements</b></em>' map.
	 * The key is of type {@link KeyType},
	 * and the value is of type {@link ValueType},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Model Elements</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Model Elements</em>' map.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_SourceModelElements()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>" ordered="false"
	 * @generated
	 */
	EMap<EObject, TGGNode> getSourceModelElements();

	/**
	 * Returns the value of the '<em><b>Target Model Elements</b></em>' map.
	 * The key is of type {@link KeyType},
	 * and the value is of type {@link ValueType},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Model Elements</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Model Elements</em>' map.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_TargetModelElements()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>" ordered="false"
	 * @generated
	 */
	EMap<EObject, TGGNode> getTargetModelElements();

	/**
	 * Returns the value of the '<em><b>Uncovered Source Model Elements</b></em>' map.
	 * The key is of type {@link KeyType},
	 * and the value is of type {@link ValueType},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uncovered Source Model Elements</em>' map
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uncovered Source Model Elements</em>' map.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_UncoveredSourceModelElements()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>" ordered="false"
	 * @generated
	 */
	EMap<EObject, Object> getUncoveredSourceModelElements();

	/**
	 * Returns the value of the '<em><b>Uncovered Target Model Elements</b></em>' map.
	 * The key is of type {@link KeyType},
	 * and the value is of type {@link ValueType},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uncovered Target Model Elements</em>' map
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uncovered Target Model Elements</em>' map.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_UncoveredTargetModelElements()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>" ordered="false"
	 * @generated
	 */
	EMap<EObject, Object> getUncoveredTargetModelElements();

	/**
	 * Returns the value of the '<em><b>Source Model Elements Adapter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Model Elements Adapter</em>' attribute
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Model Elements Adapter</em>' attribute.
	 * @see #setSourceModelElementsAdapter(Adapter)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_SourceModelElementsAdapter()
	 * @model dataType="de.hpi.sam.mote.helpers.Adapter" transient="true"
	 * @generated
	 */
	Adapter getSourceModelElementsAdapter();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRuleSet#getSourceModelElementsAdapter <em>Source Model Elements Adapter</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Model Elements Adapter</em>' attribute.
	 * @see #getSourceModelElementsAdapter()
	 * @generated
	 */
	void setSourceModelElementsAdapter(Adapter value);

	/**
	 * Returns the value of the '<em><b>Target Model Elements Adapter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Model Elements Adapter</em>' attribute
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Model Elements Adapter</em>' attribute.
	 * @see #setTargetModelElementsAdapter(Adapter)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_TargetModelElementsAdapter()
	 * @model dataType="de.hpi.sam.mote.helpers.Adapter" transient="true"
	 * @generated
	 */
	Adapter getTargetModelElementsAdapter();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRuleSet#getTargetModelElementsAdapter <em>Target Model Elements Adapter</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Model Elements Adapter</em>' attribute.
	 * @see #getTargetModelElementsAdapter()
	 * @generated
	 */
	void setTargetModelElementsAdapter(Adapter value);

	/**
	 * Returns the value of the '<em><b>Transformation Queue</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transformation Queue</em>' reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transformation Queue</em>' reference.
	 * @see #setTransformationQueue(TransformationQueue)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_TransformationQueue()
	 * @model resolveProxies="false" transient="true" ordered="false"
	 * @generated
	 */
	TransformationQueue getTransformationQueue();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRuleSet#getTransformationQueue <em>Transformation Queue</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Transformation Queue</em>' reference.
	 * @see #getTransformationQueue()
	 * @generated
	 */
	void setTransformationQueue(TransformationQueue value);

	/**
	 * Returns the value of the '<em><b>Source Reverse Navigation Store</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Reverse Navigation Store</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Reverse Navigation Store</em>' reference.
	 * @see #setSourceReverseNavigationStore(ReverseNavigationStore)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_SourceReverseNavigationStore()
	 * @model resolveProxies="false" transient="true" ordered="false"
	 * @generated
	 */
	ReverseNavigationStore getSourceReverseNavigationStore();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRuleSet#getSourceReverseNavigationStore <em>Source Reverse Navigation Store</em>}' reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Reverse Navigation Store</em>' reference.
	 * @see #getSourceReverseNavigationStore()
	 * @generated
	 */
	void setSourceReverseNavigationStore(ReverseNavigationStore value);

	/**
	 * Returns the value of the '<em><b>Target Reverse Navigation Store</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Reverse Navigation Store</em>'
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Reverse Navigation Store</em>' reference.
	 * @see #setTargetReverseNavigationStore(ReverseNavigationStore)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_TargetReverseNavigationStore()
	 * @model resolveProxies="false" transient="true" ordered="false"
	 * @generated
	 */
	ReverseNavigationStore getTargetReverseNavigationStore();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRuleSet#getTargetReverseNavigationStore <em>Target Reverse Navigation Store</em>}' reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Reverse Navigation Store</em>' reference.
	 * @see #getTargetReverseNavigationStore()
	 * @generated
	 */
	void setTargetReverseNavigationStore(ReverseNavigationStore value);

	/**
	 * Returns the value of the '<em><b>Source Model Root Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Model Root Node</em>' reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Model Root Node</em>' reference.
	 * @see #setSourceModelRootNode(EObject)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_SourceModelRootNode()
	 * @model resolveProxies="false" ordered="false"
	 * @generated
	 */
	EObject getSourceModelRootNode();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRuleSet#getSourceModelRootNode <em>Source Model Root Node</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Source Model Root Node</em>' reference.
	 * @see #getSourceModelRootNode()
	 * @generated
	 */
	void setSourceModelRootNode(EObject value);

	/**
	 * Returns the value of the '<em><b>Target Model Root Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Model Root Node</em>' reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Model Root Node</em>' reference.
	 * @see #setTargetModelRootNode(EObject)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_TargetModelRootNode()
	 * @model resolveProxies="false" ordered="false"
	 * @generated
	 */
	EObject getTargetModelRootNode();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRuleSet#getTargetModelRootNode <em>Target Model Root Node</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Target Model Root Node</em>' reference.
	 * @see #getTargetModelRootNode()
	 * @generated
	 */
	void setTargetModelRootNode(EObject value);

	/**
	 * Returns the value of the '<em><b>Corr Nodes To Delete</b></em>' reference
	 * list. The list contents are of type {@link de.hpi.sam.mote.TGGNode}. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> This
	 * reference contains all correspondence nodes that could not be
	 * synchronized during a synchronization run. If another correspondence node
	 * was synchronized during that run, the elements in this list are moved to
	 * the transformation queue, and the synchronization is repeated. This is
	 * repeated until no modifications were done in a synchronization run. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Corr Nodes To Delete</em>' reference list.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_CorrNodesToDelete()
	 * @model
	 * @generated
	 */
	EList<TGGNode> getCorrNodesToDelete();

	/**
	 * Returns the value of the '<em><b>Resource Set</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Set</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Set</em>' attribute.
	 * @see #setResourceSet(ResourceSet)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_ResourceSet()
	 * @model transient="true"
	 * @generated
	 */
	ResourceSet getResourceSet();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRuleSet#getResourceSet <em>Resource Set</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Set</em>' attribute.
	 * @see #getResourceSet()
	 * @generated
	 */
	void setResourceSet(ResourceSet value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGRuleSet#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>External References</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.reflect.Method}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External References</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External References</em>' attribute list.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_ExternalReferences()
	 * @model dataType="de.hpi.sam.mote.rules.Method" transient="true"
	 * @generated
	 */
	EList<Method> getExternalReferences();

	/**
	 * Returns the value of the '<em><b>External Ref Patterns</b></em>' containment reference list.
	 * The list contents are of type {@link de.hpi.sam.mote.rules.ReferencePattern}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Ref Patterns</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Ref Patterns</em>' containment reference list.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGRuleSet_ExternalRefPatterns()
	 * @model containment="true"
	 * @generated
	 */
	EList<ReferencePattern> getExternalRefPatterns();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Discards all correspondence nodes created by this ruleSet. <!--
	 * end-model-doc -->
	 * 
	 * @model annotation=
	 *        "http://www.eclipse.org/emf/2002/GenModel body='\t\t// Remove the adapters from model elements\r\n\t\tif (getSourceModelElementsAdapter() != null)\r\n\t\t{\r\n\t\t\tfor (EObject eObject : getSourceModelElements().keySet())\r\n\t\t\t{\r\n\t\t\t\teObject.eAdapters().remove(getSourceModelElementsAdapter());\r\n\t\t\t}\r\n\t\t}\r\n\r\n\t\tif (getTargetModelElementsAdapter() != null)\r\n\t\t{\r\n\t\t\tfor (EObject eObject : getTargetModelElements().keySet())\r\n\t\t\t{\r\n\t\t\t\teObject.eAdapters().remove(getTargetModelElementsAdapter());\r\n\t\t\t}\r\n\t\t}\r\n\r\n\t\t// Remove the correspondence nodes from the rules\r\n\t\tif (getAxiom() != null)\r\n\t\t{\r\n\t\t\tgetAxiom().getCreatedCorrNodes().clear();\r\n\t\t}\r\n\r\n\t\tfor (de.hpi.sam.mote.rules.TGGRule rule : getRules())\r\n\t\t{\r\n\t\t\trule.getCreatedCorrNodes().clear();\r\n\t\t}\r\n\r\n\t\t// Remove the correspondence nodes from the ruleSet\r\n\t\tgetSourceModelElements().clear();\r\n\t\tgetTargetModelElements().clear();\r\n\t\tgetCorrespondenceNodes().clear();\r\n\t\tgetUncoveredSourceModelElements().clear();\r\n\t\tgetUncoveredTargetModelElements().clear();\r\n\r\n\t\tgetTransformationQueue().clear();\r\n\r\n\t\tsetRootCorrNode(null);\r\n\t\tsetSourceModelRootNode(null);\r\n\t\tsetTargetModelRootNode(null);\r\n\r\n\t\tif (getSourceReverseNavigationStore() != null)\r\n\t\t{\r\n\t\t\tgetSourceReverseNavigationStore().clear();\r\n\t\t}\r\n\r\n\t\tif (getTargetReverseNavigationStore() != null)\r\n\t\t{\r\n\t\t\tgetTargetReverseNavigationStore().clear();\r\n\t\t}'"
	 * @generated
	 */
	void clear();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Creates all rules in the ruleSet. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	void createRules();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Initializes the ruleSet, the transformation queue, model element adapters, etc. are set up.
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tsetTransformationQueue(new FIFOTransformationQueue());\r\n\t\t\r\n\t\tclear();\r\n\r\n\t\tcreateRules();\r\n\r\n\t\tsetTransformationQueue(new de.hpi.sam.mote.rules.impl.FIFOTransformationQueue());\r\n\r\n\t\tif (getSourceReverseNavigationStore() == null)\r\n\t\t{\r\n\t\t\tsetSourceReverseNavigationStore(new de.hpi.sam.mote.helpers.impl.SimpleReverseNavigationStoreImpl());\r\n\t\t}\r\n\r\n\t\tif (getTargetReverseNavigationStore() == null)\r\n\t\t{\r\n\t\t\tsetTargetReverseNavigationStore(new de.hpi.sam.mote.helpers.impl.SimpleReverseNavigationStoreImpl());\r\n\t\t}\r\n\r\n\t\t/*\r\n\t\t * Create model adapters\r\n\t\t \052/\r\n\t\tif (getSourceModelElementsAdapter() == null)\r\n\t\t{\r\n\t\t\tsetSourceModelElementsAdapter(createModelElementsAdapter(getSourceModelElements(), getUncoveredSourceModelElements(),\r\n\t\t\t\t\tgetSourceReverseNavigationStore()));\r\n\t\t}\r\n\r\n\t\tif (getTargetModelElementsAdapter() == null)\r\n\t\t{\r\n\t\t\tsetTargetModelElementsAdapter(createModelElementsAdapter(getTargetModelElements(), getUncoveredTargetModelElements(),\r\n\t\t\t\t\tgetTargetReverseNavigationStore()));\r\n\t\t}'"
	 * @generated
	 */
	void initializeRuleSet();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Initializes the source model, i.e. model adapters are attached to all
	 * elements. <!-- end-model-doc -->
	 * 
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException"
	 *        annotation=
	 *        "http://www.eclipse.org/emf/2002/GenModel body='\t\tif (sourceRoot == null)\r\n\t\t{\r\n\t\t\tthrow new TransformationException(\"sourceRoot is null\", null);\r\n\t\t}\r\n\r\n\t\tif (!getSourceModelElements().containsKey(sourceRoot))\r\n\t\t{\r\n\t\t\t/*\r\n\t\t\t * Add the root to the list of uncovered elements\r\n\t\t\t \052/\r\n\t\t\tgetUncoveredSourceModelElements().put(sourceRoot, null);\r\n\t\t}\r\n\r\n\t\t/*\r\n\t\t * Attach the source elements adapter\r\n\t\t \052/\r\n\t\tsourceRoot.eAdapters().add(sourceModelElementsAdapter);\r\n\r\n\t\taddLinksToReverseNavigationStore(sourceRoot, getSourceReverseNavigationStore());\r\n\r\n\t\t/*\r\n\t\t * Add the children to the list of uncovered elements\r\n\t\t \052/\r\n\t\torg.eclipse.emf.common.util.TreeIterator<EObject> it = sourceRoot.eAllContents();\r\n\r\n\t\twhile (it.hasNext())\r\n\t\t{\r\n\t\t\torg.eclipse.emf.ecore.EObject eObject = it.next();\r\n\r\n\t\t\tgetUncoveredSourceModelElements().put(eObject, null);\r\n\r\n\t\t\t/*\r\n\t\t\t * Attach the source elements adapter\r\n\t\t\t \052/\r\n\t\t\t//eObject.eAdapters().add(sourceModelElementsAdapter);\r\n\r\n\t\t\taddLinksToReverseNavigationStore(eObject, getSourceReverseNavigationStore());\r\n\t\t}'"
	 * @generated
	 */
	void initializeSourceModel(EObject sourceRoot)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Initializes the target model, i.e. model adapters are attached to all
	 * elements. <!-- end-model-doc -->
	 * 
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException"
	 *        annotation=
	 *        "http://www.eclipse.org/emf/2002/GenModel body='\t\tif (targetRoot == null)\r\n\t\t{\r\n\t\t\tthrow new TransformationException(\"targetRoot is null\", null);\r\n\t\t}\r\n\r\n\t\tif (!getTargetModelElements().containsKey(targetRoot))\r\n\t\t{\r\n\t\t\t/*\r\n\t\t\t * Add the root to the list of uncovered elements\r\n\t\t\t \052/\r\n\t\t\tgetUncoveredTargetModelElements().put(targetRoot, null);\r\n\t\t}\r\n\t\t/*\r\n\t\t * Attach the target elements adapter\r\n\t\t \052/\r\n\t\ttargetRoot.eAdapters().add(targetModelElementsAdapter);\r\n\r\n\t\taddLinksToReverseNavigationStore(targetRoot, getSourceReverseNavigationStore());\r\n\r\n\t\t/*\r\n\t\t * Add the children to the list of uncovered elements\r\n\t\t \052/\r\n\t\torg.eclipse.emf.common.util.TreeIterator<EObject> it = targetRoot.eAllContents();\r\n\r\n\t\twhile (it.hasNext())\r\n\t\t{\r\n\t\t\torg.eclipse.emf.ecore.EObject eObject = it.next();\r\n\r\n\t\t\tgetUncoveredTargetModelElements().put(eObject, null);\r\n\r\n\t\t\t/*\r\n\t\t\t * Attach the target elements adapter\r\n\t\t\t \052/\r\n\t\t\t//eObject.eAdapters().add(targetModelElementsAdapter);\r\n\r\n\t\t\taddLinksToReverseNavigationStore(eObject, getSourceReverseNavigationStore());\r\n\t\t}'"
	 * @generated
	 */
	void initializeTargetModel(EObject targetRoot)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Deletes a correspondence node and its attached model elements (depending
	 * on the transformation direction). All succeeding correspondences and
	 * their model elements are deleted, as well. <!-- end-model-doc -->
	 * 
	 * @model annotation=
	 *        "http://www.eclipse.org/emf/2002/GenModel body='\t\tfor (de.hpi.sam.mote.TGGNode childNode : new java.util.ArrayList<de.hpi.sam.mote.TGGNode>(corrNode.getNext()))\r\n\t\t{\r\n\t\t\tdeleteElements(childNode, direction);\r\n\t\t}\r\n\r\n\t\tfor (org.eclipse.emf.ecore.EObject o : corrNode.getSources())\r\n\t\t{\r\n\t\t\tgetSourceModelElements().removeKey(o);\r\n\r\n\t\t\tif (direction == TransformationDirection.REVERSE)\r\n\t\t\t{\r\n\t\t\t\tEcoreUtil.delete(o, true);\r\n\t\t\t\tgetUncoveredSourceModelElements().removeKey(o);\r\n\t\t\t}\r\n\t\t\telse if (getSourceModelElements().containsKey(o))\r\n\t\t\t{\r\n\t\t\t\tgetUncoveredSourceModelElements().put(o, null);\r\n\t\t\t}\r\n\t\t}\r\n\r\n\t\tfor (org.eclipse.emf.ecore.EObject o : corrNode.getTargets())\r\n\t\t{\r\n\t\t\tgetTargetModelElements().removeKey(o);\r\n\r\n\t\t\tif (direction == TransformationDirection.FORWARD)\r\n\t\t\t{\r\n\t\t\t\tEcoreUtil.delete(o, true);\r\n\t\t\t\tgetUncoveredTargetModelElements().removeKey(o);\r\n\t\t\t}\r\n\t\t\telse if (getTargetModelElements().containsKey(o))\r\n\t\t\t{\r\n\t\t\t\tgetUncoveredTargetModelElements().put(o, null);\r\n\t\t\t}\r\n\t\t}\r\n\r\n\t\t/*\r\n\t\t * Delete correspondence node\r\n\t\t \052/\r\n\t\tgetCorrespondenceNodes().remove(corrNode);\r\n\t\tcorrNode.setRuleSet(null);\r\n\r\n\t\tcorrNode.setCreationRule(null);\r\n\r\n\t\tif (corrNode == getRootCorrNode())\r\n\t\t{\r\n\t\t\tsetRootCorrNode(null);\r\n\t\t}\r\n\r\n\t\torg.eclipse.emf.ecore.util.EcoreUtil.delete(corrNode, true);'"
	 * @generated
	 */
	void deleteElements(TGGNode corrNode, TransformationDirection direction);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Creates one (or more) modification tags and adds them to the queue.
	 * 
	 * @param corrNode
	 *            The correspondence node to add to the queue.
	 * @param transform
	 *            If true, a modification tag will be created that contains the
	 *            correspondence node with synchronize set to false.
	 * @param synchronize
	 *            If true, a modification tag will be created for all succeeding
	 *            correspondence nodes with synchronize set to true.
	 * @param synchronizeSelf
	 *            If true, a modification tag will be created for the
	 *            correspondence nodes with synchronize set to true. <!--
	 *            end-model-doc -->
	 * @model annotation=
	 *        "http://www.eclipse.org/emf/2002/GenModel body='\t\tif (transform || synchronizeSelf)\r\n\t\t{\r\n\t\t\tde.hpi.sam.mote.helpers.ModificationTag modTag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE.createModificationTag();\r\n\r\n\t\t\tmodTag.setCorrespondenceNode(corrNode);\r\n\t\t\tmodTag.setSynchronize(synchronizeSelf);\r\n\r\n\t\t\tgetTransformationQueue().add(modTag);\r\n\t\t}\r\n\r\n\t\tif (synchronize)\r\n\t\t{\r\n\t\t\tfor (de.hpi.sam.mote.TGGNode nextNode : corrNode.getNext())\r\n\t\t\t{\r\n\t\t\t\tde.hpi.sam.mote.helpers.ModificationTag modTag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE.createModificationTag();\r\n\r\n\t\t\t\tmodTag.setCorrespondenceNode(nextNode);\r\n\t\t\t\tmodTag.setSynchronize(true);\r\n\r\n\t\t\t\tgetTransformationQueue().add(modTag);\r\n\t\t\t}\r\n\t\t}'"
	 * @generated
	 */
	void addModificationTagToQueue(TGGNode corrNode, boolean transform,
			boolean synchronize, boolean synchronizeSelf);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Creates an adapter that can be attached to model elements. The adapter
	 * takes care of maintaining the ruleSet's data structures if models change.
	 * <!-- end-model-doc -->
	 * 
	 * @model dataType="de.hpi.sam.mote.helpers.Adapter" annotation=
	 *        "http://www.eclipse.org/emf/2002/GenModel body='\t\tfinal EMap<EObject, TGGNode> meMap = (EMap<EObject, TGGNode>) modelElementsMap;\r\n\t\tfinal EMap<EObject, Object> umeMap = (EMap<EObject, Object>) uncoveredModelElementsMap;\r\n\t\tfinal ReverseNavigationStore rns = reverseNavigationStore;\r\n\r\n\t\treturn new org.eclipse.emf.ecore.util.EContentAdapter()\r\n\t\t{\r\n\t\t\t@Override\r\n\t\t\tpublic void notifyChanged(Notification msg)\r\n\t\t\t{\r\n\t\t\t\tsuper.notifyChanged(msg);\r\n\t\t\t\t\r\n\t\t\t\tif (isProcessNotifications() && msg.getNotifier() instanceof EObject)\r\n\t\t\t\t{\r\n\r\n\t\t\t\t\tEObject modelElement = (EObject) msg.getNotifier();\r\n\r\n\t\t\t\t\t/*\r\n\t\t\t\t\t * Adjust the reverse navigation store.\r\n\t\t\t\t\t \052/\r\n\t\t\t\t\tif (msg.getFeature() instanceof EReference)\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\tEReference eReference = (EReference) msg.getFeature();\r\n\r\n\t\t\t\t\t\tif (!eReference.isContainment() && eReference.getEOpposite() == null)\r\n\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\tjava.util.List<Object> elements = new ArrayList<Object>();\r\n\r\n\t\t\t\t\t\t\tif (msg.getEventType() == org.eclipse.emf.common.notify.Notification.ADD\r\n\t\t\t\t\t\t\t\t\t|| msg.getEventType() == org.eclipse.emf.common.notify.Notification.SET && msg.getNewValue() != null)\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\telements.add(msg.getNewValue());\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\telse if (msg.getEventType() == org.eclipse.emf.common.notify.Notification.ADD_MANY)\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\telements.addAll((List<Object>) msg.getNewValue());\r\n\t\t\t\t\t\t\t}\r\n\r\n\t\t\t\t\t\t\tfor (Object o : elements)\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\tif (o instanceof EObject)\r\n\t\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\t\tEObject element = (EObject) o;\r\n\r\n\t\t\t\t\t\t\t\t\trns.addLink(modelElement, element, eReference);\r\n\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t}\r\n\r\n\t\t\t\t\t\t\telements.clear();\r\n\r\n\t\t\t\t\t\t\tif (msg.getEventType() == org.eclipse.emf.common.notify.Notification.REMOVE\r\n\t\t\t\t\t\t\t\t\t|| msg.getEventType() == org.eclipse.emf.common.notify.Notification.SET && msg.getOldValue() != null)\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\telements.add(msg.getOldValue());\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\telse if (msg.getEventType() == org.eclipse.emf.common.notify.Notification.REMOVE_MANY)\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\telements.addAll((List<Object>) msg.getOldValue());\r\n\t\t\t\t\t\t\t}\r\n\r\n\t\t\t\t\t\t\tfor (Object o : elements)\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\tif (o instanceof EObject)\r\n\t\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\t\tEObject element = (EObject) o;\r\n\r\n\t\t\t\t\t\t\t\t\trns.removeLink(modelElement, element, eReference);\r\n\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}\r\n\r\n\t\t\t\t\t/*\r\n\t\t\t\t\t * Get the correspondence node of the element\r\n\t\t\t\t\t \052/\r\n\t\t\t\t\tTGGNode corrNode = meMap.get(modelElement);\r\n\r\n\t\t\t\t\tif (corrNode != null && getTransformationQueue().get(corrNode).isEmpty())\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\t/*\r\n\t\t\t\t\t\t * Create a modification tag for the correspondence node\r\n\t\t\t\t\t\t \052/\r\n\t\t\t\t\t\tde.hpi.sam.mote.helpers.ModificationTag modTag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE\r\n\t\t\t\t\t\t\t\t.createModificationTag();\r\n\r\n\t\t\t\t\t\tmodTag.setCorrespondenceNode(corrNode);\r\n\t\t\t\t\t\tmodTag.setSynchronize(true);\r\n\t\t\t\t\t\tgetTransformationQueue().add(modTag);\r\n\t\t\t\t\t}\r\n\r\n\t\t\t\t\t/*\r\n\t\t\t\t\t * If a new element was added, attach the source model\r\n\t\t\t\t\t * elements adapter to it. Also, add them to the list of\r\n\t\t\t\t\t * uncovered elements if they do not have a correspondence\r\n\t\t\t\t\t * node.\r\n\t\t\t\t\t \052/\r\n\t\t\t\t\tjava.util.List<Object> values = new ArrayList<Object>();\r\n\r\n\t\t\t\t\tif (msg.getEventType() == org.eclipse.emf.common.notify.Notification.ADD\r\n\t\t\t\t\t\t\t|| msg.getEventType() == org.eclipse.emf.common.notify.Notification.SET && msg.getNewValue() != null)\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\tif (msg.getNewValue() instanceof EObject)\r\n\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\tvalues.add(msg.getNewValue());\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (msg.getEventType() == org.eclipse.emf.common.notify.Notification.ADD_MANY)\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\tvalues.addAll((java.util.List<Object>) msg.getNewValue());\r\n\t\t\t\t\t}\r\n\r\n\t\t\t\t\tfor (Object o : values)\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\tif (o instanceof EObject)\r\n\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\torg.eclipse.emf.ecore.EObject newValue = (org.eclipse.emf.ecore.EObject) o;\r\n\r\n\t\t\t\t\t\t\tif (!meMap.containsKey(msg.getNewValue()))\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\t// uncoveredElements\r\n\t\t\t\t\t\t\t\tumeMap.put(newValue, null);\r\n\r\n\t\t\t\t\t\t\t\t// Check children\r\n\t\t\t\t\t\t\t\torg.eclipse.emf.common.util.TreeIterator<EObject> it = newValue.eAllContents();\r\n\r\n\t\t\t\t\t\t\t\twhile (it.hasNext())\r\n\t\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\t\tEObject eObject = it.next();\r\n\r\n\t\t\t\t\t\t\t\t\t// uncoveredElements\r\n\t\t\t\t\t\t\t\t\tif (!meMap.containsKey(eObject))\r\n\t\t\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\t\t\tumeMap.put(eObject, null);\r\n\t\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}\r\n\r\n\t\t\t\t\tvalues.clear();\r\n\r\n\t\t\t\t\tif (msg.getEventType() == org.eclipse.emf.common.notify.Notification.SET && msg.getNewValue() == null)\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\tif (modelElement.eContainer() == null)\r\n\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\tumeMap.removeKey(modelElement);\r\n\r\n\t\t\t\t\t\t\torg.eclipse.emf.common.util.TreeIterator<EObject> it = modelElement.eAllContents();\r\n\r\n\t\t\t\t\t\t\twhile (it.hasNext())\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\tEObject eObject = it.next();\r\n\r\n\t\t\t\t\t\t\t\tif (!meMap.containsKey(eObject))\r\n\t\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\t\tumeMap.removeKey(eObject);\r\n\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}\r\n\r\n\t\t\t\t\tif (msg.getEventType() == org.eclipse.emf.common.notify.Notification.REMOVE)\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\tvalues.add(msg.getOldValue());\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (msg.getEventType() == org.eclipse.emf.common.notify.Notification.REMOVE_MANY)\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\tvalues.addAll((java.util.List<Object>) msg.getOldValue());\r\n\t\t\t\t\t}\r\n\r\n\t\t\t\t\tfor (Object value : values)\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\tif (value instanceof EObject && ((EObject) value).eContainer() == null && !meMap.containsKey(value))\r\n\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\tumeMap.removeKey(value);\r\n\r\n\t\t\t\t\t\t\torg.eclipse.emf.common.util.TreeIterator<EObject> it = ((EObject) value).eAllContents();\r\n\r\n\t\t\t\t\t\t\twhile (it.hasNext())\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\tEObject eObject = it.next();\r\n\r\n\t\t\t\t\t\t\t\tif (!meMap.containsKey(eObject))\r\n\t\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\t\tumeMap.removeKey(eObject);\r\n\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t};'"
	 * @generated
	 */
	Adapter createModelElementsAdapter(Object modelElementsMap,
			Object uncoveredModelElementsMap,
			ReverseNavigationStore reverseNavigationStore);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Adds all unidirectional, non-containment links of the object to the
	 * specified reverse navigation store. <!-- end-model-doc -->
	 * 
	 * @model annotation=
	 *        "http://www.eclipse.org/emf/2002/GenModel body='\t\tfor (org.eclipse.emf.ecore.EReference eReference : eObject.eClass().getEAllReferences())\r\n\t\t{\r\n\t\t\tif (!eReference.isContainment() && eReference.getEOpposite() == null)\r\n\t\t\t{\r\n\t\t\t\tif (!eReference.isMany())\r\n\t\t\t\t{\r\n\t\t\t\t\torg.eclipse.emf.ecore.EObject target = (org.eclipse.emf.ecore.EObject) eObject.eGet(eReference);\r\n\r\n\t\t\t\t\tif (target != null)\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\tgetSourceReverseNavigationStore().addLink(eObject, target, eReference);\r\n\t\t\t\t\t}\r\n\t\t\t\t}\r\n\t\t\t\telse\r\n\t\t\t\t{\r\n\t\t\t\t\torg.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EObject> targets = (org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EObject>) eObject\r\n\t\t\t\t\t\t\t.eGet(eReference);\r\n\r\n\t\t\t\t\tfor (org.eclipse.emf.ecore.EObject target : targets)\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\tgetTargetReverseNavigationStore().addLink(eObject, target, eReference);\r\n\t\t\t\t\t}\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t}'"
	 * @generated
	 */
	void addLinksToReverseNavigationStore(EObject eObject,
			ReverseNavigationStore reverseNavigationStore);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Returns true if possiblePredecessor is a predecessor of startNode. <!--
	 * end-model-doc -->
	 * 
	 * @model annotation=
	 *        "http://www.eclipse.org/emf/2002/GenModel body='\t\tif (startNode.getPrevious().contains(possiblePredecessor))\r\n\t\t{\r\n\t\t\treturn true;\r\n\t\t}\r\n\t\telse\r\n\t\t{\r\n\t\t\tfor (TGGNode previousNode : startNode.getPrevious())\r\n\t\t\t{\r\n\t\t\t\tif (isPredecessor(previousNode, possiblePredecessor))\r\n\t\t\t\t{\r\n\t\t\t\t\treturn true;\r\n\t\t\t\t}\r\n\t\t\t}\r\n\r\n\t\t\treturn false;\r\n\t\t}'"
	 * @generated
	 */
	boolean isPredecessor(TGGNode startNode, TGGNode possiblePredecessor);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='System.out.println(message);'"
	 * @generated
	 */
	void printMessage(String message);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException" errorMessageRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tSystem.out.println(errorMessage);\r\n\t\t\r\n\t\tthrow new TransformationException(errorMessage, null);'"
	 * @generated
	 */
	void reportError(String errorMessage) throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Called by the TGG engine when the transformation has started.
	 * <!-- end-model-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='//Do nothing'"
	 * @generated
	 */
	void transformationStarted(Resource sourceResource,
			Resource targetResource, TransformationDirection direction,
			boolean synchronize) throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Called by the TGG engine when the transformation has finished.
	 * <!-- end-model-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='//Do nothing\r\nreturn null;'"
	 * @generated
	 */
	EObject transformationFinished(Resource sourceResource,
			Resource targetResource, TransformationDirection direction,
			boolean synchronize) throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model targetModelElementRequired="true"
	 * @generated
	 */
	EList<EObject> sourceModelElements(EObject targetModelElement);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sourceModelElementRequired="true"
	 * @generated
	 */
	EList<EObject> targetModelElements(EObject sourceModelElement);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" corrNodeRequired="true" classifierRequired="true" searchFromSourceRequired="true"
	 * @generated
	 */
	EObject existingModelObject(TGGNode corrNode, EClassifier classifier, boolean searchFromSource);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" typesMany="true"
	 * @generated
	 */
	boolean containsCorrNodesOfAllTypes(EList<Class<?>> types);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>" exceptions="de.hpi.sam.mote.helpers.UnresolvedRefTransException" resourceRequired="true" eObjectRequired="true" featureRequired="true"
	 * @generated
	 */
	EMap<EObject, EList<EReference>> externalReferences(Resource resource, EObject eObject, EStructuralFeature feature) throws UnresolvedRefTransException;

} // TGGRuleSet
