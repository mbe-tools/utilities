/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.helpers.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.helpers.RuleSetTag;
import de.hpi.sam.mote.rules.TGGRuleSet;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Rule Set Tag</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.helpers.impl.RuleSetTagImpl#getRuleSetID <em>Rule Set ID</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.impl.RuleSetTagImpl#getSourceModelUri <em>Source Model Uri</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.impl.RuleSetTagImpl#getTargetModelUri <em>Target Model Uri</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.impl.RuleSetTagImpl#getRuleSet <em>Rule Set</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.impl.RuleSetTagImpl#getRuleSetName <em>Rule Set Name</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.impl.RuleSetTagImpl#getSourceModelID <em>Source Model ID</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.impl.RuleSetTagImpl#getTargetModelID <em>Target Model ID</em>}</li>
 *   <li>{@link de.hpi.sam.mote.helpers.impl.RuleSetTagImpl#getRuleSetPackageNsURI <em>Rule Set Package Ns URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RuleSetTagImpl extends EObjectImpl implements RuleSetTag {
	/**
	 * The default value of the '{@link #getRuleSetID() <em>Rule Set ID</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuleSetID()
	 * @generated
	 * @ordered
	 */
	protected static final String RULE_SET_ID_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getRuleSetID() <em>Rule Set ID</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuleSetID()
	 * @generated
	 * @ordered
	 */
	protected String ruleSetID = RULE_SET_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceModelUri() <em>Source Model Uri</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSourceModelUri()
	 * @generated
	 * @ordered
	 */
	protected static final URI SOURCE_MODEL_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceModelUri() <em>Source Model Uri</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSourceModelUri()
	 * @generated
	 * @ordered
	 */
	protected URI sourceModelUri = SOURCE_MODEL_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetModelUri() <em>Target Model Uri</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getTargetModelUri()
	 * @generated
	 * @ordered
	 */
	protected static final URI TARGET_MODEL_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetModelUri() <em>Target Model Uri</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getTargetModelUri()
	 * @generated
	 * @ordered
	 */
	protected URI targetModelUri = TARGET_MODEL_URI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRuleSet() <em>Rule Set</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuleSet()
	 * @generated
	 * @ordered
	 */
	protected TGGRuleSet ruleSet;

	/**
	 * The default value of the '{@link #getRuleSetName() <em>Rule Set Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getRuleSetName()
	 * @generated
	 * @ordered
	 */
	protected static final String RULE_SET_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRuleSetName() <em>Rule Set Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuleSetName()
	 * @generated
	 * @ordered
	 */
	protected String ruleSetName = RULE_SET_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceModelID() <em>Source Model ID</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSourceModelID()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_MODEL_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceModelID() <em>Source Model ID</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSourceModelID()
	 * @generated
	 * @ordered
	 */
	protected String sourceModelID = SOURCE_MODEL_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetModelID() <em>Target Model ID</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getTargetModelID()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_MODEL_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetModelID() <em>Target Model ID</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getTargetModelID()
	 * @generated
	 * @ordered
	 */
	protected String targetModelID = TARGET_MODEL_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getRuleSetPackageNsURI() <em>Rule Set Package Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuleSetPackageNsURI()
	 * @generated
	 * @ordered
	 */
	protected static final String RULE_SET_PACKAGE_NS_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRuleSetPackageNsURI() <em>Rule Set Package Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuleSetPackageNsURI()
	 * @generated
	 * @ordered
	 */
	protected String ruleSetPackageNsURI = RULE_SET_PACKAGE_NS_URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected RuleSetTagImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HelpersPackage.Literals.RULE_SET_TAG;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getRuleSetID() {
		return ruleSetID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleSetID(String newRuleSetID) {
		String oldRuleSetID = ruleSetID;
		ruleSetID = newRuleSetID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.RULE_SET_TAG__RULE_SET_ID, oldRuleSetID, ruleSetID));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getRuleSetName() {
		return ruleSetName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleSetName(String newRuleSetName) {
		String oldRuleSetName = ruleSetName;
		ruleSetName = newRuleSetName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.RULE_SET_TAG__RULE_SET_NAME, oldRuleSetName, ruleSetName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourceModelID() {
		return sourceModelID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceModelID(String newSourceModelID) {
		String oldSourceModelID = sourceModelID;
		sourceModelID = newSourceModelID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.RULE_SET_TAG__SOURCE_MODEL_ID, oldSourceModelID, sourceModelID));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getTargetModelID() {
		return targetModelID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetModelID(String newTargetModelID) {
		String oldTargetModelID = targetModelID;
		targetModelID = newTargetModelID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.RULE_SET_TAG__TARGET_MODEL_ID, oldTargetModelID, targetModelID));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getRuleSetPackageNsURI() {
		return ruleSetPackageNsURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleSetPackageNsURI(String newRuleSetPackageNsURI) {
		String oldRuleSetPackageNsURI = ruleSetPackageNsURI;
		ruleSetPackageNsURI = newRuleSetPackageNsURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.RULE_SET_TAG__RULE_SET_PACKAGE_NS_URI, oldRuleSetPackageNsURI, ruleSetPackageNsURI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public URI getSourceModelUri() {
		return sourceModelUri;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceModelUri(URI newSourceModelUri) {
		URI oldSourceModelUri = sourceModelUri;
		sourceModelUri = newSourceModelUri;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.RULE_SET_TAG__SOURCE_MODEL_URI, oldSourceModelUri, sourceModelUri));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public URI getTargetModelUri() {
		return targetModelUri;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetModelUri(URI newTargetModelUri) {
		URI oldTargetModelUri = targetModelUri;
		targetModelUri = newTargetModelUri;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.RULE_SET_TAG__TARGET_MODEL_URI, oldTargetModelUri, targetModelUri));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGRuleSet getRuleSet() {
		return ruleSet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleSet(TGGRuleSet newRuleSet) {
		TGGRuleSet oldRuleSet = ruleSet;
		ruleSet = newRuleSet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.RULE_SET_TAG__RULE_SET, oldRuleSet, ruleSet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HelpersPackage.RULE_SET_TAG__RULE_SET_ID:
				return getRuleSetID();
			case HelpersPackage.RULE_SET_TAG__SOURCE_MODEL_URI:
				return getSourceModelUri();
			case HelpersPackage.RULE_SET_TAG__TARGET_MODEL_URI:
				return getTargetModelUri();
			case HelpersPackage.RULE_SET_TAG__RULE_SET:
				return getRuleSet();
			case HelpersPackage.RULE_SET_TAG__RULE_SET_NAME:
				return getRuleSetName();
			case HelpersPackage.RULE_SET_TAG__SOURCE_MODEL_ID:
				return getSourceModelID();
			case HelpersPackage.RULE_SET_TAG__TARGET_MODEL_ID:
				return getTargetModelID();
			case HelpersPackage.RULE_SET_TAG__RULE_SET_PACKAGE_NS_URI:
				return getRuleSetPackageNsURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HelpersPackage.RULE_SET_TAG__RULE_SET_ID:
				setRuleSetID((String)newValue);
				return;
			case HelpersPackage.RULE_SET_TAG__SOURCE_MODEL_URI:
				setSourceModelUri((URI)newValue);
				return;
			case HelpersPackage.RULE_SET_TAG__TARGET_MODEL_URI:
				setTargetModelUri((URI)newValue);
				return;
			case HelpersPackage.RULE_SET_TAG__RULE_SET:
				setRuleSet((TGGRuleSet)newValue);
				return;
			case HelpersPackage.RULE_SET_TAG__RULE_SET_NAME:
				setRuleSetName((String)newValue);
				return;
			case HelpersPackage.RULE_SET_TAG__SOURCE_MODEL_ID:
				setSourceModelID((String)newValue);
				return;
			case HelpersPackage.RULE_SET_TAG__TARGET_MODEL_ID:
				setTargetModelID((String)newValue);
				return;
			case HelpersPackage.RULE_SET_TAG__RULE_SET_PACKAGE_NS_URI:
				setRuleSetPackageNsURI((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HelpersPackage.RULE_SET_TAG__RULE_SET_ID:
				setRuleSetID(RULE_SET_ID_EDEFAULT);
				return;
			case HelpersPackage.RULE_SET_TAG__SOURCE_MODEL_URI:
				setSourceModelUri(SOURCE_MODEL_URI_EDEFAULT);
				return;
			case HelpersPackage.RULE_SET_TAG__TARGET_MODEL_URI:
				setTargetModelUri(TARGET_MODEL_URI_EDEFAULT);
				return;
			case HelpersPackage.RULE_SET_TAG__RULE_SET:
				setRuleSet((TGGRuleSet)null);
				return;
			case HelpersPackage.RULE_SET_TAG__RULE_SET_NAME:
				setRuleSetName(RULE_SET_NAME_EDEFAULT);
				return;
			case HelpersPackage.RULE_SET_TAG__SOURCE_MODEL_ID:
				setSourceModelID(SOURCE_MODEL_ID_EDEFAULT);
				return;
			case HelpersPackage.RULE_SET_TAG__TARGET_MODEL_ID:
				setTargetModelID(TARGET_MODEL_ID_EDEFAULT);
				return;
			case HelpersPackage.RULE_SET_TAG__RULE_SET_PACKAGE_NS_URI:
				setRuleSetPackageNsURI(RULE_SET_PACKAGE_NS_URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HelpersPackage.RULE_SET_TAG__RULE_SET_ID:
				return RULE_SET_ID_EDEFAULT == null ? ruleSetID != null : !RULE_SET_ID_EDEFAULT.equals(ruleSetID);
			case HelpersPackage.RULE_SET_TAG__SOURCE_MODEL_URI:
				return SOURCE_MODEL_URI_EDEFAULT == null ? sourceModelUri != null : !SOURCE_MODEL_URI_EDEFAULT.equals(sourceModelUri);
			case HelpersPackage.RULE_SET_TAG__TARGET_MODEL_URI:
				return TARGET_MODEL_URI_EDEFAULT == null ? targetModelUri != null : !TARGET_MODEL_URI_EDEFAULT.equals(targetModelUri);
			case HelpersPackage.RULE_SET_TAG__RULE_SET:
				return ruleSet != null;
			case HelpersPackage.RULE_SET_TAG__RULE_SET_NAME:
				return RULE_SET_NAME_EDEFAULT == null ? ruleSetName != null : !RULE_SET_NAME_EDEFAULT.equals(ruleSetName);
			case HelpersPackage.RULE_SET_TAG__SOURCE_MODEL_ID:
				return SOURCE_MODEL_ID_EDEFAULT == null ? sourceModelID != null : !SOURCE_MODEL_ID_EDEFAULT.equals(sourceModelID);
			case HelpersPackage.RULE_SET_TAG__TARGET_MODEL_ID:
				return TARGET_MODEL_ID_EDEFAULT == null ? targetModelID != null : !TARGET_MODEL_ID_EDEFAULT.equals(targetModelID);
			case HelpersPackage.RULE_SET_TAG__RULE_SET_PACKAGE_NS_URI:
				return RULE_SET_PACKAGE_NS_URI_EDEFAULT == null ? ruleSetPackageNsURI != null : !RULE_SET_PACKAGE_NS_URI_EDEFAULT.equals(ruleSetPackageNsURI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ruleSetID: ");
		result.append(ruleSetID);
		result.append(", sourceModelUri: ");
		result.append(sourceModelUri);
		result.append(", targetModelUri: ");
		result.append(targetModelUri);
		result.append(", ruleSetName: ");
		result.append(ruleSetName);
		result.append(", sourceModelID: ");
		result.append(sourceModelID);
		result.append(", targetModelID: ");
		result.append(targetModelID);
		result.append(", ruleSetPackageNsURI: ");
		result.append(ruleSetPackageNsURI);
		result.append(')');
		return result.toString();
	}

} // RuleSetTagImpl
