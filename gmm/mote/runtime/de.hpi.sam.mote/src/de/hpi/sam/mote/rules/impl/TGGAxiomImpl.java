/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.mote.rules.TGGAxiom;
import de.hpi.sam.mote.rules.TGGRuleSet;
import de.hpi.sam.mote.rules.TransformationResult;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>TGG Axiom</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGAxiomImpl#getRuleSet <em>Rule Set</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class TGGAxiomImpl extends TGGMappingImpl implements TGGAxiom {
	/**
	 * The cached value of the '{@link #getRuleSet() <em>Rule Set</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuleSet()
	 * @generated
	 * @ordered
	 */
	protected TGGRuleSet ruleSet;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected TGGAxiomImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RulesPackage.Literals.TGG_AXIOM;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGRuleSet getRuleSet() {
		return ruleSet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRuleSet(TGGRuleSet newRuleSet,
			NotificationChain msgs) {
		TGGRuleSet oldRuleSet = ruleSet;
		ruleSet = newRuleSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_AXIOM__RULE_SET, oldRuleSet, newRuleSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleSet(TGGRuleSet newRuleSet) {
		if (newRuleSet != ruleSet) {
			NotificationChain msgs = null;
			if (ruleSet != null)
				msgs = ((InternalEObject)ruleSet).eInverseRemove(this, RulesPackage.TGG_RULE_SET__AXIOM, TGGRuleSet.class, msgs);
			if (newRuleSet != null)
				msgs = ((InternalEObject)newRuleSet).eInverseAdd(this, RulesPackage.TGG_RULE_SET__AXIOM, TGGRuleSet.class, msgs);
			msgs = basicSetRuleSet(newRuleSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_AXIOM__RULE_SET, newRuleSet, newRuleSet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationResult forwardTransformation(EObject source)
			throws TransformationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationResult mappingTransformation(EObject source,
			EObject target) throws TransformationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationResult reverseTransformation(EObject target)
			throws TransformationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RulesPackage.TGG_AXIOM__RULE_SET:
				if (ruleSet != null)
					msgs = ((InternalEObject)ruleSet).eInverseRemove(this, RulesPackage.TGG_RULE_SET__AXIOM, TGGRuleSet.class, msgs);
				return basicSetRuleSet((TGGRuleSet)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RulesPackage.TGG_AXIOM__RULE_SET:
				return basicSetRuleSet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RulesPackage.TGG_AXIOM__RULE_SET:
				return getRuleSet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RulesPackage.TGG_AXIOM__RULE_SET:
				setRuleSet((TGGRuleSet)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RulesPackage.TGG_AXIOM__RULE_SET:
				setRuleSet((TGGRuleSet)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RulesPackage.TGG_AXIOM__RULE_SET:
				return ruleSet != null;
		}
		return super.eIsSet(featureID);
	}

} // TGGAxiomImpl
