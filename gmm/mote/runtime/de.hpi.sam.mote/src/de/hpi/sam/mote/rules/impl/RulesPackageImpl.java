/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules.impl;

import java.lang.reflect.Method;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.mote.MotePackage;
import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.helpers.impl.HelpersPackageImpl;
import de.hpi.sam.mote.impl.MotePackageImpl;
import de.hpi.sam.mote.rules.Match;
import de.hpi.sam.mote.rules.MatchStorage;
import de.hpi.sam.mote.rules.ReferencePattern;
import de.hpi.sam.mote.rules.RulesFactory;
import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.mote.rules.TGGAxiom;
import de.hpi.sam.mote.rules.TGGMapping;
import de.hpi.sam.mote.rules.TGGRule;
import de.hpi.sam.mote.rules.TGGRuleSet;
import de.hpi.sam.mote.rules.TransformationExecutionResult;
import de.hpi.sam.mote.rules.TransformationQueue;
import de.hpi.sam.mote.rules.TransformationResult;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class RulesPackageImpl extends EPackageImpl implements RulesPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tggMappingEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tggAxiomEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tggRuleEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tggRuleSetEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transformationQueueEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass matchStorageEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass matchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referencePatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transformationExecutionResultEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum transformationResultEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType methodEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.mote.rules.RulesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RulesPackageImpl() {
		super(eNS_URI, RulesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link RulesPackage#eINSTANCE} when
	 * that field is accessed. Clients should not invoke it directly. Instead,
	 * they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RulesPackage init() {
		if (isInited) return (RulesPackage)EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI);

		// Obtain or create and register package
		RulesPackageImpl theRulesPackage = (RulesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RulesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RulesPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		MotePackageImpl theMotePackage = (MotePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MotePackage.eNS_URI) instanceof MotePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MotePackage.eNS_URI) : MotePackage.eINSTANCE);
		HelpersPackageImpl theHelpersPackage = (HelpersPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI) instanceof HelpersPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI) : HelpersPackage.eINSTANCE);

		// Create package meta-data objects
		theRulesPackage.createPackageContents();
		theMotePackage.createPackageContents();
		theHelpersPackage.createPackageContents();

		// Initialize created meta-data
		theRulesPackage.initializePackageContents();
		theMotePackage.initializePackageContents();
		theHelpersPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRulesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RulesPackage.eNS_URI, theRulesPackage);
		return theRulesPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTGGMapping() {
		return tggMappingEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGMapping_CreatedCorrNodes() {
		return (EReference)tggMappingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTGGAxiom() {
		return tggAxiomEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGAxiom_RuleSet() {
		return (EReference)tggAxiomEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTGGRule() {
		return tggRuleEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRule_RuleSet() {
		return (EReference)tggRuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGRule_InputCorrNodeTypes() {
		return (EAttribute)tggRuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTGGRuleSet() {
		return tggRuleSetEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_RootCorrNode() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_Axiom() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_Rules() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGRuleSet_ProcessNotifications() {
		return (EAttribute)tggRuleSetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_CorrespondenceNodes() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_SourceModelElements() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_TargetModelElements() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_UncoveredSourceModelElements() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_UncoveredTargetModelElements() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGRuleSet_SourceModelElementsAdapter() {
		return (EAttribute)tggRuleSetEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGRuleSet_TargetModelElementsAdapter() {
		return (EAttribute)tggRuleSetEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_TransformationQueue() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_SourceReverseNavigationStore() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_TargetReverseNavigationStore() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_SourceModelRootNode() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_TargetModelRootNode() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_CorrNodesToDelete() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGRuleSet_ResourceSet() {
		return (EAttribute)tggRuleSetEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGRuleSet_Id() {
		return (EAttribute)tggRuleSetEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGRuleSet_ExternalReferences() {
		return (EAttribute)tggRuleSetEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGRuleSet_ExternalRefPatterns() {
		return (EReference)tggRuleSetEClass.getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransformationQueue() {
		return transformationQueueEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMatchStorage() {
		return matchStorageEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMatchStorage_Matches() {
		return (EReference)matchStorageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMatch() {
		return matchEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMatch_ApplicationContext() {
		return (EReference)matchEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMatch_SourceCreatedElements() {
		return (EReference)matchEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMatch_CorrCreatedElements() {
		return (EReference)matchEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMatch_TargetCreatedElements() {
		return (EReference)matchEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferencePattern() {
		return referencePatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferencePattern_Reference() {
		return (EReference)referencePatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferencePattern_SourceClassifier() {
		return (EReference)referencePatternEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferencePattern_TargetClassifier() {
		return (EReference)referencePatternEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransformationExecutionResult() {
		return transformationExecutionResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransformationExecutionResult_TransformationResult() {
		return (EAttribute)transformationExecutionResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransformationExecutionResult_TransformationDirection() {
		return (EAttribute)transformationExecutionResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationExecutionResult_LeftUncoveredElements() {
		return (EReference)transformationExecutionResultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationExecutionResult_RightUncoveredElements() {
		return (EReference)transformationExecutionResultEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationExecutionResult_ExecutionTrace() {
		return (EReference)transformationExecutionResultEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTransformationResult() {
		return transformationResultEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getMethod() {
		return methodEDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RulesFactory getRulesFactory() {
		return (RulesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tggMappingEClass = createEClass(TGG_MAPPING);
		createEReference(tggMappingEClass, TGG_MAPPING__CREATED_CORR_NODES);

		tggAxiomEClass = createEClass(TGG_AXIOM);
		createEReference(tggAxiomEClass, TGG_AXIOM__RULE_SET);

		tggRuleEClass = createEClass(TGG_RULE);
		createEReference(tggRuleEClass, TGG_RULE__RULE_SET);
		createEAttribute(tggRuleEClass, TGG_RULE__INPUT_CORR_NODE_TYPES);

		tggRuleSetEClass = createEClass(TGG_RULE_SET);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__ROOT_CORR_NODE);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__AXIOM);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__RULES);
		createEAttribute(tggRuleSetEClass, TGG_RULE_SET__PROCESS_NOTIFICATIONS);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__CORRESPONDENCE_NODES);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__SOURCE_MODEL_ELEMENTS);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__TARGET_MODEL_ELEMENTS);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS);
		createEAttribute(tggRuleSetEClass, TGG_RULE_SET__SOURCE_MODEL_ELEMENTS_ADAPTER);
		createEAttribute(tggRuleSetEClass, TGG_RULE_SET__TARGET_MODEL_ELEMENTS_ADAPTER);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__TRANSFORMATION_QUEUE);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__SOURCE_REVERSE_NAVIGATION_STORE);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__TARGET_REVERSE_NAVIGATION_STORE);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__SOURCE_MODEL_ROOT_NODE);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__TARGET_MODEL_ROOT_NODE);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__CORR_NODES_TO_DELETE);
		createEAttribute(tggRuleSetEClass, TGG_RULE_SET__RESOURCE_SET);
		createEAttribute(tggRuleSetEClass, TGG_RULE_SET__ID);
		createEAttribute(tggRuleSetEClass, TGG_RULE_SET__EXTERNAL_REFERENCES);
		createEReference(tggRuleSetEClass, TGG_RULE_SET__EXTERNAL_REF_PATTERNS);

		transformationQueueEClass = createEClass(TRANSFORMATION_QUEUE);

		matchStorageEClass = createEClass(MATCH_STORAGE);
		createEReference(matchStorageEClass, MATCH_STORAGE__MATCHES);

		matchEClass = createEClass(MATCH);
		createEReference(matchEClass, MATCH__APPLICATION_CONTEXT);
		createEReference(matchEClass, MATCH__SOURCE_CREATED_ELEMENTS);
		createEReference(matchEClass, MATCH__CORR_CREATED_ELEMENTS);
		createEReference(matchEClass, MATCH__TARGET_CREATED_ELEMENTS);

		referencePatternEClass = createEClass(REFERENCE_PATTERN);
		createEReference(referencePatternEClass, REFERENCE_PATTERN__REFERENCE);
		createEReference(referencePatternEClass, REFERENCE_PATTERN__SOURCE_CLASSIFIER);
		createEReference(referencePatternEClass, REFERENCE_PATTERN__TARGET_CLASSIFIER);

		transformationExecutionResultEClass = createEClass(TRANSFORMATION_EXECUTION_RESULT);
		createEAttribute(transformationExecutionResultEClass, TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_RESULT);
		createEAttribute(transformationExecutionResultEClass, TRANSFORMATION_EXECUTION_RESULT__TRANSFORMATION_DIRECTION);
		createEReference(transformationExecutionResultEClass, TRANSFORMATION_EXECUTION_RESULT__LEFT_UNCOVERED_ELEMENTS);
		createEReference(transformationExecutionResultEClass, TRANSFORMATION_EXECUTION_RESULT__RIGHT_UNCOVERED_ELEMENTS);
		createEReference(transformationExecutionResultEClass, TRANSFORMATION_EXECUTION_RESULT__EXECUTION_TRACE);

		// Create enums
		transformationResultEEnum = createEEnum(TRANSFORMATION_RESULT);

		// Create data types
		methodEDataType = createEDataType(METHOD);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MotePackage theMotePackage = (MotePackage)EPackage.Registry.INSTANCE.getEPackage(MotePackage.eNS_URI);
		HelpersPackage theHelpersPackage = (HelpersPackage)EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		tggAxiomEClass.getESuperTypes().add(this.getTGGMapping());
		tggRuleEClass.getESuperTypes().add(this.getTGGMapping());

		// Initialize classes and features; add operations and parameters
		initEClass(tggMappingEClass, TGGMapping.class, "TGGMapping", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTGGMapping_CreatedCorrNodes(), theMotePackage.getTGGNode(), theMotePackage.getTGGNode_CreationRule(), "createdCorrNodes", null, 0, -1, TGGMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		EOperation op = addEOperation(tggMappingEClass, ecorePackage.getEBooleanObject(), "forwardSynchronization", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "corrNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggMappingEClass, ecorePackage.getEBooleanObject(), "mappingSynchronization", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "corrNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggMappingEClass, ecorePackage.getEBooleanObject(), "reverseSynchronization", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "corrNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		initEClass(tggAxiomEClass, TGGAxiom.class, "TGGAxiom", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTGGAxiom_RuleSet(), this.getTGGRuleSet(), this.getTGGRuleSet_Axiom(), "ruleSet", null, 0, 1, TGGAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = addEOperation(tggAxiomEClass, this.getTransformationResult(), "forwardTransformation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggAxiomEClass, this.getTransformationResult(), "mappingTransformation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "target", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggAxiomEClass, this.getTransformationResult(), "reverseTransformation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "target", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		initEClass(tggRuleEClass, TGGRule.class, "TGGRule", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTGGRule_RuleSet(), this.getTGGRuleSet(), this.getTGGRuleSet_Rules(), "ruleSet", null, 0, 1, TGGRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEJavaClass());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getTGGRule_InputCorrNodeTypes(), g1, "inputCorrNodeTypes", null, 1, -1, TGGRule.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = addEOperation(tggRuleEClass, this.getTransformationResult(), "forwardTransformation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "parentCorrNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggRuleEClass, this.getTransformationResult(), "mappingTransformation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "parentCorrNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggRuleEClass, this.getTransformationResult(), "reverseTransformation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "parentCorrNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggRuleEClass, ecorePackage.getEBooleanObject(), "acceptsParentCorrNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "corrNode", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleEClass, ecorePackage.getEBooleanObject(), "forwardRuntimeCheck", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "corrNode", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleEClass, ecorePackage.getEBooleanObject(), "reverseRuntimeCheck", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "corrNode", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(tggRuleEClass, null, "initInputCorrNodeTypes", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(tggRuleSetEClass, TGGRuleSet.class, "TGGRuleSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTGGRuleSet_RootCorrNode(), theMotePackage.getTGGNode(), null, "rootCorrNode", null, 0, 1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTGGRuleSet_Axiom(), this.getTGGAxiom(), this.getTGGAxiom_RuleSet(), "axiom", null, 0, 1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTGGRuleSet_Rules(), this.getTGGRule(), this.getTGGRule_RuleSet(), "rules", null, 0, -1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTGGRuleSet_ProcessNotifications(), ecorePackage.getEBoolean(), "processNotifications", null, 0, 1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(theMotePackage.getTGGNode());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEReference(getTGGRuleSet_CorrespondenceNodes(), g1, null, "correspondenceNodes", null, 0, -1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theMotePackage.getTGGNode());
		g1.getETypeArguments().add(g2);
		initEReference(getTGGRuleSet_SourceModelElements(), g1, null, "sourceModelElements", null, 0, -1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theMotePackage.getTGGNode());
		g1.getETypeArguments().add(g2);
		initEReference(getTGGRuleSet_TargetModelElements(), g1, null, "targetModelElements", null, 0, -1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEReference(getTGGRuleSet_UncoveredSourceModelElements(), g1, null, "uncoveredSourceModelElements", null, 0, -1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEReference(getTGGRuleSet_UncoveredTargetModelElements(), g1, null, "uncoveredTargetModelElements", null, 0, -1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTGGRuleSet_SourceModelElementsAdapter(), theHelpersPackage.getAdapter(), "sourceModelElementsAdapter", null, 0, 1, TGGRuleSet.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGRuleSet_TargetModelElementsAdapter(), theHelpersPackage.getAdapter(), "targetModelElementsAdapter", null, 0, 1, TGGRuleSet.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTGGRuleSet_TransformationQueue(), this.getTransformationQueue(), null, "transformationQueue", null, 0, 1, TGGRuleSet.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTGGRuleSet_SourceReverseNavigationStore(), theHelpersPackage.getReverseNavigationStore(), null, "sourceReverseNavigationStore", null, 0, 1, TGGRuleSet.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTGGRuleSet_TargetReverseNavigationStore(), theHelpersPackage.getReverseNavigationStore(), null, "targetReverseNavigationStore", null, 0, 1, TGGRuleSet.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTGGRuleSet_SourceModelRootNode(), ecorePackage.getEObject(), null, "sourceModelRootNode", null, 0, 1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTGGRuleSet_TargetModelRootNode(), ecorePackage.getEObject(), null, "targetModelRootNode", null, 0, 1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTGGRuleSet_CorrNodesToDelete(), theMotePackage.getTGGNode(), null, "corrNodesToDelete", null, 0, -1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGRuleSet_ResourceSet(), ecorePackage.getEResourceSet(), "resourceSet", null, 0, 1, TGGRuleSet.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGRuleSet_Id(), ecorePackage.getEString(), "id", null, 1, 1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGRuleSet_ExternalReferences(), this.getMethod(), "externalReferences", null, 0, -1, TGGRuleSet.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTGGRuleSet_ExternalRefPatterns(), this.getReferencePattern(), null, "externalRefPatterns", null, 0, -1, TGGRuleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(tggRuleSetEClass, null, "clear", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(tggRuleSetEClass, null, "createRules", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(tggRuleSetEClass, null, "initializeRuleSet", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleSetEClass, null, "initializeSourceModel", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "sourceRoot", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggRuleSetEClass, null, "initializeTargetModel", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "targetRoot", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggRuleSetEClass, null, "deleteElements", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "corrNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTransformationDirection(), "direction", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleSetEClass, null, "addModificationTagToQueue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "corrNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "transform", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "synchronize", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "synchronizeSelf", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleSetEClass, theHelpersPackage.getAdapter(), "createModelElementsAdapter", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "modelElementsMap", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "uncoveredModelElementsMap", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getReverseNavigationStore(), "reverseNavigationStore", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleSetEClass, null, "addLinksToReverseNavigationStore", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getReverseNavigationStore(), "reverseNavigationStore", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleSetEClass, ecorePackage.getEBoolean(), "isPredecessor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "startNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "possiblePredecessor", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleSetEClass, null, "printMessage", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "message", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleSetEClass, null, "reportError", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "errorMessage", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggRuleSetEClass, null, "transformationStarted", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "sourceResource", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "targetResource", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTransformationDirection(), "direction", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "synchronize", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggRuleSetEClass, ecorePackage.getEObject(), "transformationFinished", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "sourceResource", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "targetResource", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTransformationDirection(), "direction", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "synchronize", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggRuleSetEClass, ecorePackage.getEObject(), "sourceModelElements", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "targetModelElement", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleSetEClass, ecorePackage.getEObject(), "targetModelElements", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "sourceModelElement", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleSetEClass, ecorePackage.getEObject(), "existingModelObject", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "corrNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEClassifier(), "classifier", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "searchFromSource", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleSetEClass, ecorePackage.getEBoolean(), "containsCorrNodesOfAllTypes", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "types", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(tggRuleSetEClass, null, "externalReferences", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "resource", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "eObject", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEStructuralFeature(), "feature", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getUnresolvedRefTransException());
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEEList());
		g1.getETypeArguments().add(g2);
		EGenericType g3 = createEGenericType(ecorePackage.getEReference());
		g2.getETypeArguments().add(g3);
		initEOperation(op, g1);

		initEClass(transformationQueueEClass, TransformationQueue.class, "TransformationQueue", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(transformationQueueEClass, null, "add", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getModificationTag(), "modificationTag", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(transformationQueueEClass, theHelpersPackage.getModificationTag(), "pop", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(transformationQueueEClass, ecorePackage.getEBoolean(), "isEmpty", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(transformationQueueEClass, theHelpersPackage.getModificationTag(), "get", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTGGNode(), "corrNode", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(transformationQueueEClass, null, "clear", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(transformationQueueEClass, theMotePackage.getTGGNode(), "getElements", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(transformationQueueEClass, ecorePackage.getEInt(), "size", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(matchStorageEClass, MatchStorage.class, "MatchStorage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMatchStorage_Matches(), this.getMatch(), null, "matches", null, 0, -1, MatchStorage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(matchEClass, Match.class, "Match", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		initEReference(getMatch_ApplicationContext(), g1, null, "applicationContext", null, 0, -1, Match.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		initEReference(getMatch_SourceCreatedElements(), g1, null, "sourceCreatedElements", null, 0, -1, Match.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		initEReference(getMatch_CorrCreatedElements(), g1, null, "corrCreatedElements", null, 0, -1, Match.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		initEReference(getMatch_TargetCreatedElements(), g1, null, "targetCreatedElements", null, 0, -1, Match.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referencePatternEClass, ReferencePattern.class, "ReferencePattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferencePattern_Reference(), ecorePackage.getEReference(), null, "reference", null, 1, 1, ReferencePattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReferencePattern_SourceClassifier(), ecorePackage.getEClassifier(), null, "sourceClassifier", null, 1, 1, ReferencePattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReferencePattern_TargetClassifier(), ecorePackage.getEClassifier(), null, "targetClassifier", null, 1, 1, ReferencePattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(transformationExecutionResultEClass, TransformationExecutionResult.class, "TransformationExecutionResult", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTransformationExecutionResult_TransformationResult(), this.getTransformationResult(), "transformationResult", null, 1, 1, TransformationExecutionResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTransformationExecutionResult_TransformationDirection(), theMotePackage.getTransformationDirection(), "transformationDirection", null, 1, 1, TransformationExecutionResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransformationExecutionResult_LeftUncoveredElements(), ecorePackage.getEObject(), null, "leftUncoveredElements", null, 0, -1, TransformationExecutionResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransformationExecutionResult_RightUncoveredElements(), ecorePackage.getEObject(), null, "rightUncoveredElements", null, 0, -1, TransformationExecutionResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransformationExecutionResult_ExecutionTrace(), ecorePackage.getEObject(), null, "executionTrace", null, 0, 1, TransformationExecutionResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(transformationResultEEnum, TransformationResult.class, "TransformationResult");
		addEEnumLiteral(transformationResultEEnum, TransformationResult.RULE_APPLIED);
		addEEnumLiteral(transformationResultEEnum, TransformationResult.RULE_NOT_APPLIED);
		addEEnumLiteral(transformationResultEEnum, TransformationResult.CONFLICT_DETECTED);

		// Initialize data types
		initEDataType(methodEDataType, Method.class, "Method", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
	}

} // RulesPackageImpl
