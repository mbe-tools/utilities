/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see de.hpi.sam.mote.rules.RulesPackage
 * @generated
 */
public interface RulesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	RulesFactory eINSTANCE = de.hpi.sam.mote.rules.impl.RulesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>TGG Rule Set</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>TGG Rule Set</em>'.
	 * @generated
	 */
	TGGRuleSet createTGGRuleSet();

	/**
	 * Returns a new object of class '<em>Match Storage</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Match Storage</em>'.
	 * @generated
	 */
	MatchStorage createMatchStorage();

	/**
	 * Returns a new object of class '<em>Match</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Match</em>'.
	 * @generated
	 */
	Match createMatch();

	/**
	 * Returns a new object of class '<em>Reference Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference Pattern</em>'.
	 * @generated
	 */
	ReferencePattern createReferencePattern();

	/**
	 * Returns a new object of class '<em>Transformation Execution Result</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transformation Execution Result</em>'.
	 * @generated
	 */
	TransformationExecutionResult createTransformationExecutionResult();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	RulesPackage getRulesPackage();

} // RulesFactory
