/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.helpers.impl;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.hpi.sam.mote.helpers.HelpersFactory;
import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.helpers.ModificationTag;
import de.hpi.sam.mote.helpers.RuleSetTag;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.impl.UnresolvedRefTransException;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class HelpersFactoryImpl extends EFactoryImpl implements HelpersFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static HelpersFactory init() {
		try {
			HelpersFactory theHelpersFactory = (HelpersFactory)EPackage.Registry.INSTANCE.getEFactory(HelpersPackage.eNS_URI);
			if (theHelpersFactory != null) {
				return theHelpersFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new HelpersFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public HelpersFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case HelpersPackage.MAP_ENTRY: return (EObject)createMapEntry();
			case HelpersPackage.MODIFICATION_TAG: return createModificationTag();
			case HelpersPackage.RULE_SET_TAG: return createRuleSetTag();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case HelpersPackage.ADAPTER:
				return createAdapterFromString(eDataType, initialValue);
			case HelpersPackage.TRANSFORMATION_EXCEPTION:
				return createTransformationExceptionFromString(eDataType, initialValue);
			case HelpersPackage.URI:
				return createURIFromString(eDataType, initialValue);
			case HelpersPackage.IPROGRESS_MONITOR:
				return createIProgressMonitorFromString(eDataType, initialValue);
			case HelpersPackage.UNRESOLVED_REF_TRANS_EXCEPTION:
				return createUnresolvedRefTransExceptionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case HelpersPackage.ADAPTER:
				return convertAdapterToString(eDataType, instanceValue);
			case HelpersPackage.TRANSFORMATION_EXCEPTION:
				return convertTransformationExceptionToString(eDataType, instanceValue);
			case HelpersPackage.URI:
				return convertURIToString(eDataType, instanceValue);
			case HelpersPackage.IPROGRESS_MONITOR:
				return convertIProgressMonitorToString(eDataType, instanceValue);
			case HelpersPackage.UNRESOLVED_REF_TRANS_EXCEPTION:
				return convertUnresolvedRefTransExceptionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public <KeyType, ValueType> Map.Entry<KeyType, ValueType> createMapEntry() {
		MapEntryImpl<KeyType, ValueType> mapEntry = new MapEntryImpl<KeyType, ValueType>();
		return mapEntry;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ModificationTag createModificationTag() {
		ModificationTagImpl modificationTag = new ModificationTagImpl();
		return modificationTag;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public RuleSetTag createRuleSetTag() {
		RuleSetTagImpl ruleSetTag = new RuleSetTagImpl();
		return ruleSetTag;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Adapter createAdapterFromString(EDataType eDataType,
			String initialValue) {
		return (Adapter)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAdapterToString(EDataType eDataType,
			Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationException createTransformationExceptionFromString(
			EDataType eDataType, String initialValue) {
		return (TransformationException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTransformationExceptionToString(EDataType eDataType,
			Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public URI createURIFromString(EDataType eDataType, String initialValue) {
		return (URI)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertURIToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public IProgressMonitor createIProgressMonitorFromString(
			EDataType eDataType, String initialValue) {
		return (IProgressMonitor)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIProgressMonitorToString(EDataType eDataType,
			Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnresolvedRefTransException createUnresolvedRefTransExceptionFromString(EDataType eDataType, String initialValue) {
		return (UnresolvedRefTransException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUnresolvedRefTransExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public HelpersPackage getHelpersPackage() {
		return (HelpersPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static HelpersPackage getPackage() {
		return HelpersPackage.eINSTANCE;
	}

} // HelpersFactoryImpl
