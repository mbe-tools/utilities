/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules.impl;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.helpers.impl.MapEntryImpl;
import de.hpi.sam.mote.rules.Match;
import de.hpi.sam.mote.rules.RulesPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Match</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.impl.MatchImpl#getApplicationContext <em>Application Context</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.MatchImpl#getSourceCreatedElements <em>Source Created Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.MatchImpl#getCorrCreatedElements <em>Corr Created Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.MatchImpl#getTargetCreatedElements <em>Target Created Elements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MatchImpl extends EObjectImpl implements Match {
	/**
	 * The cached value of the '{@link #getApplicationContext() <em>Application Context</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getApplicationContext()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, EObject> applicationContext;

	/**
	 * The cached value of the '{@link #getSourceCreatedElements() <em>Source Created Elements</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSourceCreatedElements()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, EObject> sourceCreatedElements;

	/**
	 * The cached value of the '{@link #getCorrCreatedElements() <em>Corr Created Elements</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCorrCreatedElements()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, EObject> corrCreatedElements;

	/**
	 * The cached value of the '{@link #getTargetCreatedElements() <em>Target Created Elements</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getTargetCreatedElements()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, EObject> targetCreatedElements;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MatchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RulesPackage.Literals.MATCH;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<String, EObject> getApplicationContext() {
		if (applicationContext == null) {
			applicationContext = new EcoreEMap<String,EObject>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, RulesPackage.MATCH__APPLICATION_CONTEXT);
		}
		return applicationContext;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<String, EObject> getSourceCreatedElements() {
		if (sourceCreatedElements == null) {
			sourceCreatedElements = new EcoreEMap<String,EObject>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, RulesPackage.MATCH__SOURCE_CREATED_ELEMENTS);
		}
		return sourceCreatedElements;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<String, EObject> getCorrCreatedElements() {
		if (corrCreatedElements == null) {
			corrCreatedElements = new EcoreEMap<String,EObject>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, RulesPackage.MATCH__CORR_CREATED_ELEMENTS);
		}
		return corrCreatedElements;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<String, EObject> getTargetCreatedElements() {
		if (targetCreatedElements == null) {
			targetCreatedElements = new EcoreEMap<String,EObject>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, RulesPackage.MATCH__TARGET_CREATED_ELEMENTS);
		}
		return targetCreatedElements;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RulesPackage.MATCH__APPLICATION_CONTEXT:
				return ((InternalEList<?>)getApplicationContext()).basicRemove(otherEnd, msgs);
			case RulesPackage.MATCH__SOURCE_CREATED_ELEMENTS:
				return ((InternalEList<?>)getSourceCreatedElements()).basicRemove(otherEnd, msgs);
			case RulesPackage.MATCH__CORR_CREATED_ELEMENTS:
				return ((InternalEList<?>)getCorrCreatedElements()).basicRemove(otherEnd, msgs);
			case RulesPackage.MATCH__TARGET_CREATED_ELEMENTS:
				return ((InternalEList<?>)getTargetCreatedElements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RulesPackage.MATCH__APPLICATION_CONTEXT:
				if (coreType) return getApplicationContext();
				else return getApplicationContext().map();
			case RulesPackage.MATCH__SOURCE_CREATED_ELEMENTS:
				if (coreType) return getSourceCreatedElements();
				else return getSourceCreatedElements().map();
			case RulesPackage.MATCH__CORR_CREATED_ELEMENTS:
				if (coreType) return getCorrCreatedElements();
				else return getCorrCreatedElements().map();
			case RulesPackage.MATCH__TARGET_CREATED_ELEMENTS:
				if (coreType) return getTargetCreatedElements();
				else return getTargetCreatedElements().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RulesPackage.MATCH__APPLICATION_CONTEXT:
				((EStructuralFeature.Setting)getApplicationContext()).set(newValue);
				return;
			case RulesPackage.MATCH__SOURCE_CREATED_ELEMENTS:
				((EStructuralFeature.Setting)getSourceCreatedElements()).set(newValue);
				return;
			case RulesPackage.MATCH__CORR_CREATED_ELEMENTS:
				((EStructuralFeature.Setting)getCorrCreatedElements()).set(newValue);
				return;
			case RulesPackage.MATCH__TARGET_CREATED_ELEMENTS:
				((EStructuralFeature.Setting)getTargetCreatedElements()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RulesPackage.MATCH__APPLICATION_CONTEXT:
				getApplicationContext().clear();
				return;
			case RulesPackage.MATCH__SOURCE_CREATED_ELEMENTS:
				getSourceCreatedElements().clear();
				return;
			case RulesPackage.MATCH__CORR_CREATED_ELEMENTS:
				getCorrCreatedElements().clear();
				return;
			case RulesPackage.MATCH__TARGET_CREATED_ELEMENTS:
				getTargetCreatedElements().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RulesPackage.MATCH__APPLICATION_CONTEXT:
				return applicationContext != null && !applicationContext.isEmpty();
			case RulesPackage.MATCH__SOURCE_CREATED_ELEMENTS:
				return sourceCreatedElements != null && !sourceCreatedElements.isEmpty();
			case RulesPackage.MATCH__CORR_CREATED_ELEMENTS:
				return corrCreatedElements != null && !corrCreatedElements.isEmpty();
			case RulesPackage.MATCH__TARGET_CREATED_ELEMENTS:
				return targetCreatedElements != null && !targetCreatedElements.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // MatchImpl
