/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hpi.sam.mote.MoteFactory
 * @model kind="package"
 * @generated
 */
public interface MotePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mote";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mote/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mote";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	MotePackage eINSTANCE = de.hpi.sam.mote.impl.MotePackageImpl.init();

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.impl.TGGEngineImpl
	 * <em>TGG Engine</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see de.hpi.sam.mote.impl.TGGEngineImpl
	 * @see de.hpi.sam.mote.impl.MotePackageImpl#getTGGEngine()
	 * @generated
	 */
	int TGG_ENGINE = 0;

	/**
	 * The feature id for the '<em><b>Available Rule Sets</b></em>' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_ENGINE__AVAILABLE_RULE_SETS = 0;

	/**
	 * The feature id for the '<em><b>Used Rule Sets</b></em>' map. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_ENGINE__USED_RULE_SETS = 1;

	/**
	 * The feature id for the '<em><b>Runtime Checks Enabled</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_ENGINE__RUNTIME_CHECKS_ENABLED = 2;

	/**
	 * The feature id for the '<em><b>Notifications Enabled</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_ENGINE__NOTIFICATIONS_ENABLED = 3;

	/**
	 * The feature id for the '<em><b>Relation Policies</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_ENGINE__RELATION_POLICIES = 4;

	/**
	 * The feature id for the '<em><b>Coverage Policies</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_ENGINE__COVERAGE_POLICIES = 5;

	/**
	 * The number of structural features of the '<em>TGG Engine</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TGG_ENGINE_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.impl.TGGNodeImpl <em>TGG Node</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.impl.TGGNodeImpl
	 * @see de.hpi.sam.mote.impl.MotePackageImpl#getTGGNode()
	 * @generated
	 */
	int TGG_NODE = 1;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_NODE__NEXT = 0;

	/**
	 * The feature id for the '<em><b>Previous</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_NODE__PREVIOUS = 1;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_NODE__RULE_SET = 2;

	/**
	 * The feature id for the '<em><b>Creation Rule</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_NODE__CREATION_RULE = 3;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_NODE__SOURCES = 4;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_NODE__TARGETS = 5;

	/**
	 * The number of structural features of the '<em>TGG Node</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int TGG_NODE_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.impl.MoteEngineRelationPolicyImpl <em>Engine Relation Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.impl.MoteEngineRelationPolicyImpl
	 * @see de.hpi.sam.mote.impl.MotePackageImpl#getMoteEngineRelationPolicy()
	 * @generated
	 */
	int MOTE_ENGINE_RELATION_POLICY = 2;

	/**
	 * The number of structural features of the '<em>Engine Relation Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_ENGINE_RELATION_POLICY_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.impl.MoteEngineCoveragePolicyImpl <em>Engine Coverage Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hpi.sam.mote.impl.MoteEngineCoveragePolicyImpl
	 * @see de.hpi.sam.mote.impl.MotePackageImpl#getMoteEngineCoveragePolicy()
	 * @generated
	 */
	int MOTE_ENGINE_COVERAGE_POLICY = 3;

	/**
	 * The number of structural features of the '<em>Engine Coverage Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_ENGINE_COVERAGE_POLICY_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.hpi.sam.mote.TransformationDirection <em>Transformation Direction</em>}' enum.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see de.hpi.sam.mote.TransformationDirection
	 * @see de.hpi.sam.mote.impl.MotePackageImpl#getTransformationDirection()
	 * @generated
	 */
	int TRANSFORMATION_DIRECTION = 4;

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.TGGEngine <em>TGG Engine</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>TGG Engine</em>'.
	 * @see de.hpi.sam.mote.TGGEngine
	 * @generated
	 */
	EClass getTGGEngine();

	/**
	 * Returns the meta object for the map '
	 * {@link de.hpi.sam.mote.TGGEngine#getAvailableRuleSets
	 * <em>Available Rule Sets</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the map '<em>Available Rule Sets</em>'.
	 * @see de.hpi.sam.mote.TGGEngine#getAvailableRuleSets()
	 * @see #getTGGEngine()
	 * @generated
	 */
	EReference getTGGEngine_AvailableRuleSets();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.mote.TGGEngine#getUsedRuleSets <em>Used Rule Sets</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Used Rule Sets</em>'.
	 * @see de.hpi.sam.mote.TGGEngine#getUsedRuleSets()
	 * @see #getTGGEngine()
	 * @generated
	 */
	EReference getTGGEngine_UsedRuleSets();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.TGGEngine#isRuntimeChecksEnabled <em>Runtime Checks Enabled</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Runtime Checks Enabled</em>'.
	 * @see de.hpi.sam.mote.TGGEngine#isRuntimeChecksEnabled()
	 * @see #getTGGEngine()
	 * @generated
	 */
	EAttribute getTGGEngine_RuntimeChecksEnabled();

	/**
	 * Returns the meta object for the attribute '{@link de.hpi.sam.mote.TGGEngine#isNotificationsEnabled <em>Notifications Enabled</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Notifications Enabled</em>'.
	 * @see de.hpi.sam.mote.TGGEngine#isNotificationsEnabled()
	 * @see #getTGGEngine()
	 * @generated
	 */
	EAttribute getTGGEngine_NotificationsEnabled();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.mote.TGGEngine#getRelationPolicies <em>Relation Policies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Relation Policies</em>'.
	 * @see de.hpi.sam.mote.TGGEngine#getRelationPolicies()
	 * @see #getTGGEngine()
	 * @generated
	 */
	EReference getTGGEngine_RelationPolicies();

	/**
	 * Returns the meta object for the map '{@link de.hpi.sam.mote.TGGEngine#getCoveragePolicies <em>Coverage Policies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Coverage Policies</em>'.
	 * @see de.hpi.sam.mote.TGGEngine#getCoveragePolicies()
	 * @see #getTGGEngine()
	 * @generated
	 */
	EReference getTGGEngine_CoveragePolicies();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.TGGNode <em>TGG Node</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>TGG Node</em>'.
	 * @see de.hpi.sam.mote.TGGNode
	 * @generated
	 */
	EClass getTGGNode();

	/**
	 * Returns the meta object for the reference list '
	 * {@link de.hpi.sam.mote.TGGNode#getNext <em>Next</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Next</em>'.
	 * @see de.hpi.sam.mote.TGGNode#getNext()
	 * @see #getTGGNode()
	 * @generated
	 */
	EReference getTGGNode_Next();

	/**
	 * Returns the meta object for the reference list '
	 * {@link de.hpi.sam.mote.TGGNode#getPrevious <em>Previous</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Previous</em>'.
	 * @see de.hpi.sam.mote.TGGNode#getPrevious()
	 * @see #getTGGNode()
	 * @generated
	 */
	EReference getTGGNode_Previous();

	/**
	 * Returns the meta object for the reference '
	 * {@link de.hpi.sam.mote.TGGNode#getRuleSet <em>Rule Set</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Rule Set</em>'.
	 * @see de.hpi.sam.mote.TGGNode#getRuleSet()
	 * @see #getTGGNode()
	 * @generated
	 */
	EReference getTGGNode_RuleSet();

	/**
	 * Returns the meta object for the reference '{@link de.hpi.sam.mote.TGGNode#getCreationRule <em>Creation Rule</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Creation Rule</em>'.
	 * @see de.hpi.sam.mote.TGGNode#getCreationRule()
	 * @see #getTGGNode()
	 * @generated
	 */
	EReference getTGGNode_CreationRule();

	/**
	 * Returns the meta object for the reference list '
	 * {@link de.hpi.sam.mote.TGGNode#getSources <em>Sources</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Sources</em>'.
	 * @see de.hpi.sam.mote.TGGNode#getSources()
	 * @see #getTGGNode()
	 * @generated
	 */
	EReference getTGGNode_Sources();

	/**
	 * Returns the meta object for the reference list '
	 * {@link de.hpi.sam.mote.TGGNode#getTargets <em>Targets</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Targets</em>'.
	 * @see de.hpi.sam.mote.TGGNode#getTargets()
	 * @see #getTGGNode()
	 * @generated
	 */
	EReference getTGGNode_Targets();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.MoteEngineRelationPolicy <em>Engine Relation Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Engine Relation Policy</em>'.
	 * @see de.hpi.sam.mote.MoteEngineRelationPolicy
	 * @generated
	 */
	EClass getMoteEngineRelationPolicy();

	/**
	 * Returns the meta object for class '{@link de.hpi.sam.mote.MoteEngineCoveragePolicy <em>Engine Coverage Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Engine Coverage Policy</em>'.
	 * @see de.hpi.sam.mote.MoteEngineCoveragePolicy
	 * @generated
	 */
	EClass getMoteEngineCoveragePolicy();

	/**
	 * Returns the meta object for enum '{@link de.hpi.sam.mote.TransformationDirection <em>Transformation Direction</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for enum '<em>Transformation Direction</em>'.
	 * @see de.hpi.sam.mote.TransformationDirection
	 * @generated
	 */
	EEnum getTransformationDirection();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MoteFactory getMoteFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.impl.TGGEngineImpl <em>TGG Engine</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.impl.TGGEngineImpl
		 * @see de.hpi.sam.mote.impl.MotePackageImpl#getTGGEngine()
		 * @generated
		 */
		EClass TGG_ENGINE = eINSTANCE.getTGGEngine();

		/**
		 * The meta object literal for the '<em><b>Available Rule Sets</b></em>' map feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_ENGINE__AVAILABLE_RULE_SETS = eINSTANCE.getTGGEngine_AvailableRuleSets();

		/**
		 * The meta object literal for the '<em><b>Used Rule Sets</b></em>' map feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_ENGINE__USED_RULE_SETS = eINSTANCE.getTGGEngine_UsedRuleSets();

		/**
		 * The meta object literal for the '
		 * <em><b>Runtime Checks Enabled</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute TGG_ENGINE__RUNTIME_CHECKS_ENABLED = eINSTANCE.getTGGEngine_RuntimeChecksEnabled();

		/**
		 * The meta object literal for the '
		 * <em><b>Notifications Enabled</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute TGG_ENGINE__NOTIFICATIONS_ENABLED = eINSTANCE.getTGGEngine_NotificationsEnabled();

		/**
		 * The meta object literal for the '<em><b>Relation Policies</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_ENGINE__RELATION_POLICIES = eINSTANCE.getTGGEngine_RelationPolicies();

		/**
		 * The meta object literal for the '<em><b>Coverage Policies</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_ENGINE__COVERAGE_POLICIES = eINSTANCE.getTGGEngine_CoveragePolicies();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.impl.TGGNodeImpl <em>TGG Node</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.impl.TGGNodeImpl
		 * @see de.hpi.sam.mote.impl.MotePackageImpl#getTGGNode()
		 * @generated
		 */
		EClass TGG_NODE = eINSTANCE.getTGGNode();

		/**
		 * The meta object literal for the '<em><b>Next</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_NODE__NEXT = eINSTANCE.getTGGNode_Next();

		/**
		 * The meta object literal for the '<em><b>Previous</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_NODE__PREVIOUS = eINSTANCE.getTGGNode_Previous();

		/**
		 * The meta object literal for the '<em><b>Rule Set</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_NODE__RULE_SET = eINSTANCE.getTGGNode_RuleSet();

		/**
		 * The meta object literal for the '<em><b>Creation Rule</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_NODE__CREATION_RULE = eINSTANCE.getTGGNode_CreationRule();

		/**
		 * The meta object literal for the '<em><b>Sources</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_NODE__SOURCES = eINSTANCE.getTGGNode_Sources();

		/**
		 * The meta object literal for the '<em><b>Targets</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference TGG_NODE__TARGETS = eINSTANCE.getTGGNode_Targets();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.impl.MoteEngineRelationPolicyImpl <em>Engine Relation Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.impl.MoteEngineRelationPolicyImpl
		 * @see de.hpi.sam.mote.impl.MotePackageImpl#getMoteEngineRelationPolicy()
		 * @generated
		 */
		EClass MOTE_ENGINE_RELATION_POLICY = eINSTANCE.getMoteEngineRelationPolicy();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.impl.MoteEngineCoveragePolicyImpl <em>Engine Coverage Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.impl.MoteEngineCoveragePolicyImpl
		 * @see de.hpi.sam.mote.impl.MotePackageImpl#getMoteEngineCoveragePolicy()
		 * @generated
		 */
		EClass MOTE_ENGINE_COVERAGE_POLICY = eINSTANCE.getMoteEngineCoveragePolicy();

		/**
		 * The meta object literal for the '{@link de.hpi.sam.mote.TransformationDirection <em>Transformation Direction</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hpi.sam.mote.TransformationDirection
		 * @see de.hpi.sam.mote.impl.MotePackageImpl#getTransformationDirection()
		 * @generated
		 */
		EEnum TRANSFORMATION_DIRECTION = eINSTANCE.getTransformationDirection();

	}

} // MotePackage
