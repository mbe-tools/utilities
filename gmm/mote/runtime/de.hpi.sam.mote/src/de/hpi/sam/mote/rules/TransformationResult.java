/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '
 * <em><b>Transformation Result</b></em>', and utility methods for working with
 * them. <!-- end-user-doc -->
 * @see de.hpi.sam.mote.rules.RulesPackage#getTransformationResult()
 * @model
 * @generated
 */
public enum TransformationResult implements Enumerator {
	/**
	 * The '<em><b>RULE APPLIED</b></em>' literal object.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #RULE_APPLIED_VALUE
	 * @generated
	 * @ordered
	 */
	RULE_APPLIED(0, "RULE_APPLIED", "RULE_APPLIED"),

	/**
	 * The '<em><b>RULE NOT APPLIED</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #RULE_NOT_APPLIED_VALUE
	 * @generated
	 * @ordered
	 */
	RULE_NOT_APPLIED(1, "RULE_NOT_APPLIED", "RULE_NOT_APPLIED"),

	/**
	 * The '<em><b>CONFLICT DETECTED</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #CONFLICT_DETECTED_VALUE
	 * @generated
	 * @ordered
	 */
	CONFLICT_DETECTED(2, "CONFLICT_DETECTED", "CONFLICT_DETECTED");

	/**
	 * The '<em><b>RULE APPLIED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RULE APPLIED</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RULE_APPLIED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RULE_APPLIED_VALUE = 0;

	/**
	 * The '<em><b>RULE NOT APPLIED</b></em>' literal value.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>RULE NOT APPLIED</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RULE_NOT_APPLIED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RULE_NOT_APPLIED_VALUE = 1;

	/**
	 * The '<em><b>CONFLICT DETECTED</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CONFLICT DETECTED</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #CONFLICT_DETECTED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CONFLICT_DETECTED_VALUE = 2;

	/**
	 * An array of all the '<em><b>Transformation Result</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static final TransformationResult[] VALUES_ARRAY = new TransformationResult[] {
			RULE_APPLIED,
			RULE_NOT_APPLIED,
			CONFLICT_DETECTED,
		};

	/**
	 * A public read-only list of all the '<em><b>Transformation Result</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<TransformationResult> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Transformation Result</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static TransformationResult get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			TransformationResult result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Transformation Result</b></em>' literal with the specified name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static TransformationResult getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			TransformationResult result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Transformation Result</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static TransformationResult get(int value) {
		switch (value) {
			case RULE_APPLIED_VALUE: return RULE_APPLIED;
			case RULE_NOT_APPLIED_VALUE: return RULE_NOT_APPLIED;
			case CONFLICT_DETECTED_VALUE: return CONFLICT_DETECTED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	private TransformationResult(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} // TransformationResult
