/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.impl;

import de.hpi.sam.mote.MoteEngineCoveragePolicy;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.mote.MoteEngineRelationPolicy;
import de.hpi.sam.mote.MoteFactory;
import de.hpi.sam.mote.MotePackage;
import de.hpi.sam.mote.TGGEngine;
import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.TransformationDirection;
import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.helpers.impl.HelpersPackageImpl;
import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.mote.rules.impl.RulesPackageImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class MotePackageImpl extends EPackageImpl implements MotePackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tggEngineEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tggNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moteEngineRelationPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moteEngineCoveragePolicyEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum transformationDirectionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hpi.sam.mote.MotePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MotePackageImpl() {
		super(eNS_URI, MoteFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link MotePackage#eINSTANCE} when that
	 * field is accessed. Clients should not invoke it directly. Instead, they
	 * should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MotePackage init() {
		if (isInited) return (MotePackage)EPackage.Registry.INSTANCE.getEPackage(MotePackage.eNS_URI);

		// Obtain or create and register package
		MotePackageImpl theMotePackage = (MotePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MotePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MotePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		RulesPackageImpl theRulesPackage = (RulesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI) instanceof RulesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI) : RulesPackage.eINSTANCE);
		HelpersPackageImpl theHelpersPackage = (HelpersPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI) instanceof HelpersPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI) : HelpersPackage.eINSTANCE);

		// Create package meta-data objects
		theMotePackage.createPackageContents();
		theRulesPackage.createPackageContents();
		theHelpersPackage.createPackageContents();

		// Initialize created meta-data
		theMotePackage.initializePackageContents();
		theRulesPackage.initializePackageContents();
		theHelpersPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMotePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MotePackage.eNS_URI, theMotePackage);
		return theMotePackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTGGEngine() {
		return tggEngineEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGEngine_AvailableRuleSets() {
		return (EReference)tggEngineEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGEngine_UsedRuleSets() {
		return (EReference)tggEngineEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGEngine_RuntimeChecksEnabled() {
		return (EAttribute)tggEngineEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTGGEngine_NotificationsEnabled() {
		return (EAttribute)tggEngineEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGEngine_RelationPolicies() {
		return (EReference)tggEngineEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGEngine_CoveragePolicies() {
		return (EReference)tggEngineEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTGGNode() {
		return tggNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGNode_Next() {
		return (EReference)tggNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGNode_Previous() {
		return (EReference)tggNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGNode_RuleSet() {
		return (EReference)tggNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGNode_CreationRule() {
		return (EReference)tggNodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGNode_Sources() {
		return (EReference)tggNodeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTGGNode_Targets() {
		return (EReference)tggNodeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMoteEngineRelationPolicy() {
		return moteEngineRelationPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMoteEngineCoveragePolicy() {
		return moteEngineCoveragePolicyEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTransformationDirection() {
		return transformationDirectionEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MoteFactory getMoteFactory() {
		return (MoteFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tggEngineEClass = createEClass(TGG_ENGINE);
		createEReference(tggEngineEClass, TGG_ENGINE__AVAILABLE_RULE_SETS);
		createEReference(tggEngineEClass, TGG_ENGINE__USED_RULE_SETS);
		createEAttribute(tggEngineEClass, TGG_ENGINE__RUNTIME_CHECKS_ENABLED);
		createEAttribute(tggEngineEClass, TGG_ENGINE__NOTIFICATIONS_ENABLED);
		createEReference(tggEngineEClass, TGG_ENGINE__RELATION_POLICIES);
		createEReference(tggEngineEClass, TGG_ENGINE__COVERAGE_POLICIES);

		tggNodeEClass = createEClass(TGG_NODE);
		createEReference(tggNodeEClass, TGG_NODE__NEXT);
		createEReference(tggNodeEClass, TGG_NODE__PREVIOUS);
		createEReference(tggNodeEClass, TGG_NODE__RULE_SET);
		createEReference(tggNodeEClass, TGG_NODE__CREATION_RULE);
		createEReference(tggNodeEClass, TGG_NODE__SOURCES);
		createEReference(tggNodeEClass, TGG_NODE__TARGETS);

		moteEngineRelationPolicyEClass = createEClass(MOTE_ENGINE_RELATION_POLICY);

		moteEngineCoveragePolicyEClass = createEClass(MOTE_ENGINE_COVERAGE_POLICY);

		// Create enums
		transformationDirectionEEnum = createEEnum(TRANSFORMATION_DIRECTION);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RulesPackage theRulesPackage = (RulesPackage)EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI);
		HelpersPackage theHelpersPackage = (HelpersPackage)EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theRulesPackage);
		getESubpackages().add(theHelpersPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(tggEngineEClass, TGGEngine.class, "TGGEngine", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		EGenericType g1 = createEGenericType(theHelpersPackage.getMapEntry());
		EGenericType g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theHelpersPackage.getRuleSetTag());
		g1.getETypeArguments().add(g2);
		initEReference(getTGGEngine_AvailableRuleSets(), g1, null, "availableRuleSets", null, 0, -1, TGGEngine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEEList());
		g1.getETypeArguments().add(g2);
		EGenericType g3 = createEGenericType(theHelpersPackage.getRuleSetTag());
		g2.getETypeArguments().add(g3);
		initEReference(getTGGEngine_UsedRuleSets(), g1, null, "usedRuleSets", null, 0, -1, TGGEngine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTGGEngine_RuntimeChecksEnabled(), ecorePackage.getEBoolean(), "runtimeChecksEnabled", "true", 0, 1, TGGEngine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTGGEngine_NotificationsEnabled(), ecorePackage.getEBoolean(), "notificationsEnabled", "false", 0, 1, TGGEngine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getMoteEngineRelationPolicy());
		g1.getETypeArguments().add(g2);
		initEReference(getTGGEngine_RelationPolicies(), g1, null, "relationPolicies", null, 0, -1, TGGEngine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getMoteEngineCoveragePolicy());
		g1.getETypeArguments().add(g2);
		initEReference(getTGGEngine_CoveragePolicies(), g1, null, "coveragePolicies", null, 0, -1, TGGEngine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		addEOperation(tggEngineEClass, null, "initializeEngine", 0, 1, IS_UNIQUE, IS_ORDERED);

		EOperation op = addEOperation(tggEngineEClass, theHelpersPackage.getRuleSetTag(), "initializeRuleSet", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getRuleSetTag(), "ruleSetTag", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getURI(), "sourceUri", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getURI(), "targetUri", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggEngineEClass, theRulesPackage.getTransformationExecutionResult(), "transform", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "leftResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "rightResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEResource());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEEList());
		g1.getETypeArguments().add(g2);
		g3 = createEGenericType(ecorePackage.getEObject());
		g2.getETypeArguments().add(g3);
		addEParameter(op, g1, "dependencies", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTransformationDirection(), "transformationDirection", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ruleSetID", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "synchronize", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggEngineEClass, theRulesPackage.getTransformationExecutionResult(), "transform", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "leftResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "rightResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEResource());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEEList());
		g1.getETypeArguments().add(g2);
		g3 = createEGenericType(ecorePackage.getEObject());
		g2.getETypeArguments().add(g3);
		addEParameter(op, g1, "dependencies", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTransformationDirection(), "transformationDirection", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ruleSetID", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "synchronize", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggEngineEClass, theRulesPackage.getTransformationExecutionResult(), "checkConsistency", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "leftResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "rightResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEResource());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEEList());
		g1.getETypeArguments().add(g2);
		g3 = createEGenericType(ecorePackage.getEObject());
		g2.getETypeArguments().add(g3);
		addEParameter(op, g1, "dependencies", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ruleSetID", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggEngineEClass, null, "executeTransformationRules", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theRulesPackage.getTGGRuleSet(), "ruleSet", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTransformationDirection(), "transformationDirection", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "synchronize", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggEngineEClass, null, "performRuntimeChecks", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theRulesPackage.getTGGRuleSet(), "ruleSet", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTransformationDirection(), "transformationDirection", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggEngineEClass, ecorePackage.getEObject(), "correspondingSourceElements", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "sourceResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "targetElement", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ruleSetId", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggEngineEClass, ecorePackage.getEObject(), "correspondingTargetElements", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "sourceElement", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "targetResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ruleSetId", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(tggEngineEClass, null, "resourceDeleted", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "resource", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(tggNodeEClass, TGGNode.class, "TGGNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTGGNode_Next(), this.getTGGNode(), this.getTGGNode_Previous(), "next", null, 0, -1, TGGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTGGNode_Previous(), this.getTGGNode(), this.getTGGNode_Next(), "previous", null, 0, -1, TGGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTGGNode_RuleSet(), theRulesPackage.getTGGRuleSet(), null, "ruleSet", null, 0, 1, TGGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTGGNode_CreationRule(), theRulesPackage.getTGGMapping(), theRulesPackage.getTGGMapping_CreatedCorrNodes(), "creationRule", null, 0, 1, TGGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTGGNode_Sources(), ecorePackage.getEObject(), null, "sources", null, 0, -1, TGGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTGGNode_Targets(), ecorePackage.getEObject(), null, "targets", null, 0, -1, TGGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(moteEngineRelationPolicyEClass, MoteEngineRelationPolicy.class, "MoteEngineRelationPolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(moteEngineRelationPolicyEClass, theHelpersPackage.getURI(), "correspondingUri", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "resource", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(moteEngineCoveragePolicyEClass, MoteEngineCoveragePolicy.class, "MoteEngineCoveragePolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(moteEngineCoveragePolicyEClass, ecorePackage.getEBoolean(), "isCovered", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "modelElement", 1, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(transformationDirectionEEnum, TransformationDirection.class, "TransformationDirection");
		addEEnumLiteral(transformationDirectionEEnum, TransformationDirection.FORWARD);
		addEEnumLiteral(transformationDirectionEEnum, TransformationDirection.REVERSE);
		addEEnumLiteral(transformationDirectionEEnum, TransformationDirection.MAPPING);

		// Create resource
		createResource(eNS_URI);
	}

} // MotePackageImpl
