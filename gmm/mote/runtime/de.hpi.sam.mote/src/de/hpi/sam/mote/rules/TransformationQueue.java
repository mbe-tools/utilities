/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.helpers.ModificationTag;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Transformation Queue</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see de.hpi.sam.mote.rules.RulesPackage#getTransformationQueue()
 * @model abstract="true"
 * @generated
 */
public interface TransformationQueue extends EObject {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Add a modification tag to the queue. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	void add(ModificationTag modificationTag);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Remove and return the first modification tag of the queue. The meaning of
	 * "first" depends on the implementation. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	ModificationTag pop();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Returns true if there are no elements in the queue. <!-- end-model-doc
	 * -->
	 * 
	 * @model kind="operation"
	 * @generated
	 */
	boolean isEmpty();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Return the modification tags belonging to this correspondence node. <!--
	 * end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	EList<ModificationTag> get(TGGNode corrNode);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Discard all data in the queue. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	void clear();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<TGGNode> getElements();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	int size();

} // TransformationQueue
