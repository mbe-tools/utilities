/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.helpers;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Reverse Navigation Store</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The reverseNavigationStore is responsible for storing information to allow an easy navigation of unidirectional links in reverse direction.
 * <!-- end-model-doc -->
 *
 *
 * @see de.hpi.sam.mote.helpers.HelpersPackage#getReverseNavigationStore()
 * @model abstract="true"
 * @generated
 */
public interface ReverseNavigationStore extends EObject {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Add a link to the ReverseNavigationStore. The source object has a link of
	 * type eReference that points to target. The store should save information
	 * about this link to allow navigation in the opposite direction. <!--
	 * end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	void addLink(EObject source, EObject target, EReference eReference);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Remove a link from the ReverseNavigationStore. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	void removeLink(EObject source, EObject target, EReference eReference);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Returns all source objects that have a link of type eReference pointing
	 * to target. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	EList<EObject> getLinkSources(EObject target, EReference eReference);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clear();

} // ReverseNavigationStore
