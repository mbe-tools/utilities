/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Match</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.Match#getApplicationContext <em>Application Context</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.Match#getSourceCreatedElements <em>Source Created Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.Match#getCorrCreatedElements <em>Corr Created Elements</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.Match#getTargetCreatedElements <em>Target Created Elements</em>}</li>
 * </ul>
 *
 * @see de.hpi.sam.mote.rules.RulesPackage#getMatch()
 * @model
 * @generated
 */
public interface Match extends EObject {
	/**
	 * Returns the value of the '<em><b>Application Context</b></em>' map. The
	 * key is of type {@link KeyType}, and the value is of type
	 * {@link ValueType}, <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> This reference contains all model elements that could
	 * be matched to elements of the application context of the TGG rule. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Application Context</em>' map.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getMatch_ApplicationContext()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>"
	 * @generated
	 */
	EMap<String, EObject> getApplicationContext();

	/**
	 * Returns the value of the '<em><b>Source Created Elements</b></em>' map.
	 * The key is of type {@link KeyType}, and the value is of type
	 * {@link ValueType}, <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> This reference contains all model elements that could
	 * be matched to source (or target respectively depending on the actual
	 * transformation direction) elements that are created by the TGG rule. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Source Created Elements</em>' map.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getMatch_SourceCreatedElements()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>"
	 * @generated
	 */
	EMap<String, EObject> getSourceCreatedElements();

	/**
	 * Returns the value of the '<em><b>Corr Created Elements</b></em>' map. The
	 * key is of type {@link KeyType}, and the value is of type
	 * {@link ValueType}, <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> This reference contains all model elements that could
	 * be matched to source (or target respectively depending on the actual
	 * transformation direction) elements that are created by the TGG rule. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Corr Created Elements</em>' map.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getMatch_CorrCreatedElements()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>"
	 * @generated
	 */
	EMap<String, EObject> getCorrCreatedElements();

	/**
	 * Returns the value of the '<em><b>Target Created Elements</b></em>' map.
	 * The key is of type {@link KeyType}, and the value is of type
	 * {@link ValueType}, <!-- begin-user-doc --> <!-- end-user-doc --> <!--
	 * begin-model-doc --> This reference contains all model elements that could
	 * be matched to source (or target respectively depending on the actual
	 * transformation direction) elements that are created by the TGG rule. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Target Created Elements</em>' map.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getMatch_TargetCreatedElements()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>"
	 * @generated
	 */
	EMap<String, EObject> getTargetCreatedElements();

} // Match
