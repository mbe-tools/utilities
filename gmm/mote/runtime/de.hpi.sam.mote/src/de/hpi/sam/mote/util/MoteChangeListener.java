package de.hpi.sam.mote.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EContentAdapter;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.helpers.HelpersFactory;
import de.hpi.sam.mote.helpers.ModificationTag;
import de.hpi.sam.mote.helpers.ReverseNavigationStore;
import de.hpi.sam.mote.rules.TGGRuleSet;
import de.hpi.sam.mote.rules.TransformationQueue;
import de.hpi.sam.mote.rules.impl.FIFOTransformationQueue;

public class MoteChangeListener extends EContentAdapter {
	//private boolean							active;

	private final Set<EObject>				rootObjects;

	private final EMap<EObject, TGGNode>	coveredElementsMap;

	private final EMap<EObject, Object>		uncoveredElementsMap;

	private final TGGRuleSet				tggRuleSet;

	private final TransformationQueue		deletionQueue;

	public TransformationQueue getDeletionQueue() {
		return deletionQueue;
	}

	final ReverseNavigationStore revNavigStore;


	public MoteChangeListener(	final TGGRuleSet p_tggRuleSet, 
								final EMap<EObject, TGGNode> p_coveredElementsMap, 
								final EMap<EObject, Object> p_uncoveredElementsMap,
								final ReverseNavigationStore p_revNavigStore ) {
		super();

		//active = true;
		tggRuleSet = p_tggRuleSet;
		rootObjects = new HashSet<EObject>();
		coveredElementsMap = p_coveredElementsMap;
		uncoveredElementsMap = p_uncoveredElementsMap;
		revNavigStore = p_revNavigStore;
		deletionQueue = new FIFOTransformationQueue();
	}
	
	private boolean shouldProcessNotification() {
		if ( tggRuleSet != null ) {
			return tggRuleSet.isProcessNotifications();
		}

		return false;
	}

//	public void activate() {
//		assert !this.active;
//
//		this.active = true;
//	}
//
//	public void deactivate() {
//		assert this.active;
//
//		this.active = false;
//	}

	public void attachAdapter(final Collection<EObject> rootObjects) {
		this.rootObjects.addAll(rootObjects);

		for (final EObject eObject : this.rootObjects) {
			eObject.eAdapters().add(this);
		}

	}

	public void detachAdapter() {
		for (final EObject eObject : this.rootObjects) {
			eObject.eAdapters().remove(this);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void notifyChanged(final Notification notification) {
		if ( shouldProcessNotification() ) {
			super.notifyChanged(notification);
	
			if ( notification.getNotifier() instanceof EObject && !notification.isTouch()) {
				final EObject notifier = (EObject) notification.getNotifier();
				addModificationTagToQueue( notifier );
	
				/*
				 * Get the correspondence node of the element and add it to the
				 * queue
				 */
				if (notification.getFeature() instanceof EReference ) {
					final EReference eReference = (EReference) notification.getFeature();
	
					switch (notification.getEventType()) {
						case Notification.ADD:
							elementsAdded(notifier, Collections.singletonList((EObject) notification.getNewValue()), eReference);
							
							break;
	
						case Notification.ADD_MANY:
							elementsAdded(notifier, (Collection<EObject>) notification.getNewValue(), eReference);
							
							break;
	
						case Notification.REMOVE:
							elementsRemoved(notifier, Collections.singletonList((EObject) notification.getOldValue()), eReference );
							
							break;
	
						case Notification.REMOVE_MANY:
							elementsRemoved(notifier, (Collection<EObject>) notification.getOldValue(), eReference);
							
							break;
	
						case Notification.SET:
							if (notification.getNewValue() == null )	{
								if ( notifier.eContainer() == null) {
									uncoveredElementsMap.removeKey( notifier );
		
									final TreeIterator<EObject> it = notifier.eAllContents();
		
									while (it.hasNext()) {
										final EObject eObject = it.next();
		
										if (!coveredElementsMap.containsKey(eObject)) {
											uncoveredElementsMap.removeKey(eObject);
										}
									}
								}
							}
							else {//if (notification.getNewValue() != null)	{
								elementsAdded(notifier, Collections.singletonList((EObject) notification.getNewValue()), eReference);
							}
	
							if ( notification.getOldValue() != null )	{
								elementsRemoved(notifier, Collections.singletonList((EObject) notification.getOldValue()), eReference, false );
							}
							
							break;
					}
				}
			}
		}
	}
	
	private ModificationTag createModificationTag( final TGGNode p_corrNode ) {
		final ModificationTag modTag = HelpersFactory.eINSTANCE.createModificationTag();
		modTag.setCorrespondenceNode( p_corrNode );
		modTag.setSynchronize( true );
		
		return modTag;
	}

	private void elementsAdded(	final EObject notifier,
								final Collection<EObject> elements,
								final EReference reference ) {
		/*
		 * Maintain uncoveredElements and inverse links for the elements and
		 * their subtrees
		 */
		if (reference.isContainment()) {
			final Set<EObject> allElements = new HashSet<EObject>();

			for (final EObject eObject : elements) {
				allElements.add(eObject);

				final Iterator<EObject> it = eObject.eAllContents();

				while (it.hasNext()) {
					allElements.add(it.next());
				}
			}

			for (final EObject eObject : allElements) {
				if (!this.coveredElementsMap.containsKey(eObject)) {
					this.uncoveredElementsMap.put(eObject, null);
				}
			}
		}
		else if ( reference.getEOpposite() == null ) {
			for (final EObject eObject : elements) {
				revNavigStore.addLink(notifier, eObject, reference);
			}
		}
	}
	
	private boolean addModificationTagToQueue( final EObject p_modelElement ) {
		return addModificationTagToQueue( p_modelElement, false );
	}
	
	/**
	 * Assume the deleting notifications always arrive last.
	 * @param p_modelElement
	 * @param pb_addToDeletion
	 * @return
	 */
	private boolean addModificationTagToQueue( 	final EObject p_modelElement,
												final boolean pb_addToDeletion ) {
		final TGGNode corrNode = coveredElementsMap.get( p_modelElement );
		
		if ( corrNode != null ) {
			final TransformationQueue transQueue; 
			final TransformationQueue ruleSetQueue = tggRuleSet.getTransformationQueue();

			if ( pb_addToDeletion ) {
				transQueue = deletionQueue;
				
				for ( final TGGNode nextNode : new ArrayList<TGGNode>( corrNode.getNext() ) ) {
					if ( !ruleSetQueue.get( nextNode ).isEmpty() ) {
						corrNode.getNext().remove( nextNode );
					}
				}
			}
			else {
				transQueue = ruleSetQueue;
			}
			
			if ( transQueue.get( corrNode ).isEmpty() ) {
				transQueue.add( createModificationTag( corrNode ) );

				return true;
			}
		}
		
		return false;
	}

	private void elementsRemoved(	final EObject notifier,
									final Collection<EObject> elements,
									final EReference reference ) {
		elementsRemoved( notifier, elements, reference, true );
	}

	private void elementsRemoved(	final EObject notifier,
									final Collection<EObject> elements,
									final EReference reference,
									final boolean pb_addToDeletionQueue ) {
		/*
		 * Maintain uncoveredElements and inverse links for the elements and
		 * their subtrees
		 */
		if ( reference.isContainment() ) {
			final Set<EObject> allElements = new HashSet<EObject>();

			for (final EObject eObject : elements) {
				allElements.add(eObject);
				addModificationTagToQueue( eObject, pb_addToDeletionQueue );

				final Iterator<EObject> it = eObject.eAllContents();

				while (it.hasNext()) {
					allElements.add(it.next());
				}
			}

			for (final EObject eObject : allElements) {
				if ( eObject.eContainer() == null && !coveredElementsMap.containsKey(eObject)) {
					this.uncoveredElementsMap.removeKey(eObject);
				}
			}
		} 
		else if ( reference.getEOpposite() == null ) {
			for ( final EObject element : elements ) {
				revNavigStore.removeLink( notifier, element, reference );
			}
		}
//		else if (this.tggEngine.getOperationalTGG().getUnidirectionalReferences().contains(reference)) {
//			for (final EObject eObject : elements)
//			{
//				/*
//				 * If the link was covered by the transformation, add the
//				 * corresponding TGGNode to the queue.
//				 */
//				TGGNode tggNode = this.tggEngine.isLinkCovered(notifier, eObject, reference);
//
//				if (tggNode != null)
//				{
//					this.transformationQueue.add(tggNode, true);
//				}
//
//				this.tggEngine.unregisterInverseLink(notifier, eObject, reference);
//
//				tggNode = this.coveredElementsMap.get(eObject);
//
//				if (tggNode != null)
//				{
//					this.transformationQueue.add(tggNode, true);
//				}
//			}
//		}
	}

//	@SuppressWarnings("unchecked")
//	private void registerAllInverseLinks(final EObject eObject)	{
//		for (final EReference eReference : eObject.eClass().getEAllReferences())
//		{
//			if (this.tggEngine.getOperationalTGG().getUnidirectionalReferences().contains(eReference))
//			{
//				if (!eReference.isMany())
//				{
//					if (eObject.eGet(eReference) != null)
//					{
//						this.tggEngine.registerInverseLink(eObject, (EObject) eObject.eGet(eReference), eReference);
//
//						final TGGNode tggNode = this.coveredElementsMap.get(eObject.eGet(eReference));
//
//						if (tggNode != null)
//						{
//							this.transformationQueue.add(tggNode, true);
//						}
//					}
//				}
//				else
//				{
//					for (final EObject referencedEObject : (Collection<EObject>) eObject.eGet(eReference))
//					{
//						this.tggEngine.registerInverseLink(eObject, referencedEObject, eReference);
//
//						final TGGNode tggNode = this.coveredElementsMap.get(eObject.eGet(eReference));
//
//						if (tggNode != null)
//						{
//							this.transformationQueue.add(tggNode, true);
//						}
//					}
//				}
//			}
//		}
//	}
//
//	@SuppressWarnings("unchecked")
//	private void unregisterAllInverseLinks(final EObject eObject)
//	{
//		for (final EReference eReference : eObject.eClass().getEAllReferences())
//		{
//			if (this.tggEngine.getOperationalTGG().getUnidirectionalReferences().contains(eReference))
//			{
//				if (!eReference.isMany())
//				{
//					if (eObject.eGet(eReference) != null)
//					{
//						this.tggEngine.unregisterInverseLink(eObject, (EObject) eObject.eGet(eReference), eReference);
//
//						final TGGNode tggNode = this.coveredElementsMap.get(eObject.eGet(eReference));
//
//						if (tggNode != null)
//						{
//							this.transformationQueue.add(tggNode, true);
//						}
//					}
//				}
//				else
//				{
//					for (final EObject referencedEObject : (Collection<EObject>) eObject.eGet(eReference))
//					{
//						this.tggEngine.unregisterInverseLink(eObject, referencedEObject, eReference);
//
//						final TGGNode tggNode = this.coveredElementsMap.get(eObject.eGet(eReference));
//
//						if (tggNode != null)
//						{
//							this.transformationQueue.add(tggNode, true);
//						}
//					}
//				}
//			}
//		}
//	}
}
