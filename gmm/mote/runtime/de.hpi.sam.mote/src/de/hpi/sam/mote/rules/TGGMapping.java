/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.impl.TransformationException;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>TGG Mapping</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The class TGGMapping is the abstract base class for all transformation rules and axioms.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.TGGMapping#getCreatedCorrNodes <em>Created Corr Nodes</em>}</li>
 * </ul>
 *
 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGMapping()
 * @model abstract="true"
 * @generated
 */
public interface TGGMapping extends EObject {
	/**
	 * Returns the value of the '<em><b>Created Corr Nodes</b></em>' reference
	 * list. The list contents are of type {@link de.hpi.sam.mote.TGGNode}. It
	 * is bidirectional and its opposite is '
	 * {@link de.hpi.sam.mote.TGGNode#getCreationRule <em>Creation Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Created Corr Nodes</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc --> <!-- begin-model-doc --> This list contains all
	 * correspondence nodes created by the transformation rule. <!--
	 * end-model-doc -->
	 * 
	 * @return the value of the '<em>Created Corr Nodes</em>' reference list.
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGMapping_CreatedCorrNodes()
	 * @see de.hpi.sam.mote.TGGNode#getCreationRule
	 * @model opposite="creationRule" resolveProxies="false" ordered="false"
	 * @generated
	 */
	EList<TGGNode> getCreatedCorrNodes();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Checks and enforces consistency between the elements connected to the
	 * supplied correspondence node. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	Boolean forwardSynchronization(TGGNode corrNode)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Checks and enforces consistency between the elements connected to the
	 * supplied correspondence node. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	Boolean mappingSynchronization(TGGNode corrNode)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Checks and enforces consistency between the elements connected to the
	 * supplied correspondence node. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	Boolean reverseSynchronization(TGGNode corrNode)
			throws TransformationException;

} // TGGMapping
