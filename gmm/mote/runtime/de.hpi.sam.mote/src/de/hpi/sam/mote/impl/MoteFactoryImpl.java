/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.impl;

import de.hpi.sam.mote.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.hpi.sam.mote.MoteFactory;
import de.hpi.sam.mote.MotePackage;
import de.hpi.sam.mote.TGGEngine;
import de.hpi.sam.mote.TransformationDirection;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class MoteFactoryImpl extends EFactoryImpl implements MoteFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static MoteFactory init() {
		try {
			MoteFactory theMoteFactory = (MoteFactory)EPackage.Registry.INSTANCE.getEFactory(MotePackage.eNS_URI);
			if (theMoteFactory != null) {
				return theMoteFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MoteFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public MoteFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MotePackage.TGG_ENGINE: return createTGGEngine();
			case MotePackage.MOTE_ENGINE_RELATION_POLICY: return createMoteEngineRelationPolicy();
			case MotePackage.MOTE_ENGINE_COVERAGE_POLICY: return createMoteEngineCoveragePolicy();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case MotePackage.TRANSFORMATION_DIRECTION:
				return createTransformationDirectionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case MotePackage.TRANSFORMATION_DIRECTION:
				return convertTransformationDirectionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGEngine createTGGEngine() {
		TGGEngineImpl tggEngine = new TGGEngineImpl();
		return tggEngine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MoteEngineRelationPolicy createMoteEngineRelationPolicy() {
		MoteEngineRelationPolicyImpl moteEngineRelationPolicy = new MoteEngineRelationPolicyImpl();
		return moteEngineRelationPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MoteEngineCoveragePolicy createMoteEngineCoveragePolicy() {
		MoteEngineCoveragePolicyImpl moteEngineCoveragePolicy = new MoteEngineCoveragePolicyImpl();
		return moteEngineCoveragePolicy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationDirection createTransformationDirectionFromString(
			EDataType eDataType, String initialValue) {
		TransformationDirection result = TransformationDirection.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTransformationDirectionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MotePackage getMotePackage() {
		return (MotePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MotePackage getPackage() {
		return MotePackage.eINSTANCE;
	}

} // MoteFactoryImpl
