/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.hpi.sam.mote.MotePackage;
import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.mote.rules.TGGMapping;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>TGG Mapping</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGMappingImpl#getCreatedCorrNodes <em>Created Corr Nodes</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class TGGMappingImpl extends EObjectImpl implements TGGMapping {
	/**
	 * The cached value of the '{@link #getCreatedCorrNodes() <em>Created Corr Nodes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreatedCorrNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<TGGNode> createdCorrNodes;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected TGGMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RulesPackage.Literals.TGG_MAPPING;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TGGNode> getCreatedCorrNodes() {
		if (createdCorrNodes == null) {
			createdCorrNodes = new EObjectWithInverseEList<TGGNode>(TGGNode.class, this, RulesPackage.TGG_MAPPING__CREATED_CORR_NODES, MotePackage.TGG_NODE__CREATION_RULE);
		}
		return createdCorrNodes;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean forwardSynchronization(TGGNode corrNode)
			throws TransformationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean mappingSynchronization(TGGNode corrNode)
			throws TransformationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean reverseSynchronization(TGGNode corrNode)
			throws TransformationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RulesPackage.TGG_MAPPING__CREATED_CORR_NODES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCreatedCorrNodes()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RulesPackage.TGG_MAPPING__CREATED_CORR_NODES:
				return ((InternalEList<?>)getCreatedCorrNodes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RulesPackage.TGG_MAPPING__CREATED_CORR_NODES:
				return getCreatedCorrNodes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RulesPackage.TGG_MAPPING__CREATED_CORR_NODES:
				getCreatedCorrNodes().clear();
				getCreatedCorrNodes().addAll((Collection<? extends TGGNode>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RulesPackage.TGG_MAPPING__CREATED_CORR_NODES:
				getCreatedCorrNodes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RulesPackage.TGG_MAPPING__CREATED_CORR_NODES:
				return createdCorrNodes != null && !createdCorrNodes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // TGGMappingImpl
