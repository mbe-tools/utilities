/**
 */
package de.hpi.sam.mote.impl;

import de.hpi.sam.mote.MoteEngineCoveragePolicy;
import de.hpi.sam.mote.MotePackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Engine Coverage Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MoteEngineCoveragePolicyImpl extends EObjectImpl implements MoteEngineCoveragePolicy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MoteEngineCoveragePolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MotePackage.Literals.MOTE_ENGINE_COVERAGE_POLICY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCovered(EObject modelElement) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

} //MoteEngineCoveragePolicyImpl
