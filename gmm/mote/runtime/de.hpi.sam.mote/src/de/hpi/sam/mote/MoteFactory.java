/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see de.hpi.sam.mote.MotePackage
 * @generated
 */
public interface MoteFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	MoteFactory eINSTANCE = de.hpi.sam.mote.impl.MoteFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>TGG Engine</em>'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return a new object of class '<em>TGG Engine</em>'.
	 * @generated
	 */
	TGGEngine createTGGEngine();

	/**
	 * Returns a new object of class '<em>Engine Relation Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Engine Relation Policy</em>'.
	 * @generated
	 */
	MoteEngineRelationPolicy createMoteEngineRelationPolicy();

	/**
	 * Returns a new object of class '<em>Engine Coverage Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Engine Coverage Policy</em>'.
	 * @generated
	 */
	MoteEngineCoveragePolicy createMoteEngineCoveragePolicy();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MotePackage getMotePackage();

} // MoteFactory
