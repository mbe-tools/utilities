/**
 */
package de.hpi.sam.mote.impl;

import de.hpi.sam.mote.MoteEngineRelationPolicy;
import de.hpi.sam.mote.MotePackage;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.resource.Resource;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Engine Relation Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MoteEngineRelationPolicyImpl extends EObjectImpl implements MoteEngineRelationPolicy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MoteEngineRelationPolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MotePackage.Literals.MOTE_ENGINE_RELATION_POLICY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public URI correspondingUri(Resource resource) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

} //MoteEngineRelationPolicyImpl
