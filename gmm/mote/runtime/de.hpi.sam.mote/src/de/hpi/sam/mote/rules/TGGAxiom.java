/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules;

import org.eclipse.emf.ecore.EObject;

import de.hpi.sam.mote.impl.TransformationException;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>TGG Axiom</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * TGGAxiom is the abstract base class for axioms. An axiom creates a correspondence node that is associated with the root elements of the source and target models.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.TGGAxiom#getRuleSet <em>Rule Set</em>}</li>
 * </ul>
 *
 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGAxiom()
 * @model abstract="true"
 * @generated
 */
public interface TGGAxiom extends TGGMapping {
	/**
	 * Returns the value of the '<em><b>Rule Set</b></em>' reference. It is
	 * bidirectional and its opposite is '
	 * {@link de.hpi.sam.mote.rules.TGGRuleSet#getAxiom <em>Axiom</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Set</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc --> <!-- begin-model-doc --> Reference to the ruleSet
	 * this axiom belongs to. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Rule Set</em>' reference.
	 * @see #setRuleSet(TGGRuleSet)
	 * @see de.hpi.sam.mote.rules.RulesPackage#getTGGAxiom_RuleSet()
	 * @see de.hpi.sam.mote.rules.TGGRuleSet#getAxiom
	 * @model opposite="axiom" resolveProxies="false" ordered="false"
	 * @generated
	 */
	TGGRuleSet getRuleSet();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.rules.TGGAxiom#getRuleSet
	 * <em>Rule Set</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Rule Set</em>' reference.
	 * @see #getRuleSet()
	 * @generated
	 */
	void setRuleSet(TGGRuleSet value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Performs a forward transformation. The root element of the source (left)
	 * model is expected. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	TransformationResult forwardTransformation(EObject source)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Performs a mapping transformation. The root elements of the source and
	 * the target model are expected. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	TransformationResult mappingTransformation(EObject source, EObject target)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Performs a reverse transformation. The root element of the target model
	 * is expected. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	TransformationResult reverseTransformation(EObject target)
			throws TransformationException;

} // TGGAxiom
