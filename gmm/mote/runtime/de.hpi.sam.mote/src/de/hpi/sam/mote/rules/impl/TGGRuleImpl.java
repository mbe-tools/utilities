/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote.rules.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.rules.RulesPackage;
import de.hpi.sam.mote.rules.TGGRule;
import de.hpi.sam.mote.rules.TGGRuleSet;
import de.hpi.sam.mote.rules.TransformationResult;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>TGG Rule</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleImpl#getRuleSet <em>Rule Set</em>}</li>
 *   <li>{@link de.hpi.sam.mote.rules.impl.TGGRuleImpl#getInputCorrNodeTypes <em>Input Corr Node Types</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class TGGRuleImpl extends TGGMappingImpl implements TGGRule {
	/**
	 * The cached value of the '{@link #getRuleSet() <em>Rule Set</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRuleSet()
	 * @generated
	 * @ordered
	 */
	protected TGGRuleSet ruleSet;

	/**
	 * The cached value of the '{@link #getInputCorrNodeTypes() <em>Input Corr Node Types</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputCorrNodeTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<Class<?>> inputCorrNodeTypes;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected TGGRuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RulesPackage.Literals.TGG_RULE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TGGRuleSet getRuleSet() {
		return ruleSet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRuleSet(TGGRuleSet newRuleSet,
			NotificationChain msgs) {
		TGGRuleSet oldRuleSet = ruleSet;
		ruleSet = newRuleSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE__RULE_SET, oldRuleSet, newRuleSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuleSet(TGGRuleSet newRuleSet) {
		if (newRuleSet != ruleSet) {
			NotificationChain msgs = null;
			if (ruleSet != null)
				msgs = ((InternalEObject)ruleSet).eInverseRemove(this, RulesPackage.TGG_RULE_SET__RULES, TGGRuleSet.class, msgs);
			if (newRuleSet != null)
				msgs = ((InternalEObject)newRuleSet).eInverseAdd(this, RulesPackage.TGG_RULE_SET__RULES, TGGRuleSet.class, msgs);
			msgs = basicSetRuleSet(newRuleSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RulesPackage.TGG_RULE__RULE_SET, newRuleSet, newRuleSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Class<?>> getInputCorrNodeTypes() {
		if (inputCorrNodeTypes == null) {
			inputCorrNodeTypes = new EDataTypeUniqueEList<Class<?>>(Class.class, this, RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES);
			
			initInputCorrNodeTypes();
		}
		return inputCorrNodeTypes;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationResult forwardTransformation(TGGNode parentCorrNode)
			throws TransformationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationResult mappingTransformation(TGGNode parentCorrNode)
			throws TransformationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationResult reverseTransformation(TGGNode parentCorrNode)
			throws TransformationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean acceptsParentCorrNode(TGGNode corrNode) {
		final EList<Class<?>> types  = getInputCorrNodeTypes();
		
		for ( final Class<?> accClass : types ) {
			// Check that the type of the parent corr node is acceptable (within the list of all input corr node types
			// of the TGG rule).
			if ( accClass.isInstance( corrNode ) ) {
				
				// Check that there is at least one corr node instance in the rule set for each input corr node type of the TGG rule
				return getRuleSet().containsCorrNodesOfAllTypes( types );
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean forwardRuntimeCheck(TGGNode corrNode) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean reverseRuntimeCheck(TGGNode corrNode) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initInputCorrNodeTypes() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RulesPackage.TGG_RULE__RULE_SET:
				if (ruleSet != null)
					msgs = ((InternalEObject)ruleSet).eInverseRemove(this, RulesPackage.TGG_RULE_SET__RULES, TGGRuleSet.class, msgs);
				return basicSetRuleSet((TGGRuleSet)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RulesPackage.TGG_RULE__RULE_SET:
				return basicSetRuleSet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RulesPackage.TGG_RULE__RULE_SET:
				return getRuleSet();
			case RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES:
				return getInputCorrNodeTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RulesPackage.TGG_RULE__RULE_SET:
				setRuleSet((TGGRuleSet)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RulesPackage.TGG_RULE__RULE_SET:
				setRuleSet((TGGRuleSet)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RulesPackage.TGG_RULE__RULE_SET:
				return ruleSet != null;
			case RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES:
				return inputCorrNodeTypes != null && !inputCorrNodeTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (inputCorrNodeTypes: ");
		result.append(inputCorrNodeTypes);
		result.append(')');
		return result.toString();
	}

} // TGGRuleImpl
