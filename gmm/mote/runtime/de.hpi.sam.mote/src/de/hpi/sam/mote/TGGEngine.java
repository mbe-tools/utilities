/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.hpi.sam.mote;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import de.hpi.sam.mote.helpers.RuleSetTag;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.rules.TGGRuleSet;
import de.hpi.sam.mote.rules.TransformationExecutionResult;
import de.hpi.sam.mote.rules.TransformationResult;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>TGG Engine</b></em>'. <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The transformation engine executes the rules of a ruleSet to perform the model transformation or synchronization.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.hpi.sam.mote.TGGEngine#getAvailableRuleSets <em>Available Rule Sets</em>}</li>
 *   <li>{@link de.hpi.sam.mote.TGGEngine#getUsedRuleSets <em>Used Rule Sets</em>}</li>
 *   <li>{@link de.hpi.sam.mote.TGGEngine#isRuntimeChecksEnabled <em>Runtime Checks Enabled</em>}</li>
 *   <li>{@link de.hpi.sam.mote.TGGEngine#isNotificationsEnabled <em>Notifications Enabled</em>}</li>
 *   <li>{@link de.hpi.sam.mote.TGGEngine#getRelationPolicies <em>Relation Policies</em>}</li>
 *   <li>{@link de.hpi.sam.mote.TGGEngine#getCoveragePolicies <em>Coverage Policies</em>}</li>
 * </ul>
 *
 * @see de.hpi.sam.mote.MotePackage#getTGGEngine()
 * @model
 * @generated
 */
public interface TGGEngine extends EObject {
	/**
	 * Returns the value of the '<em><b>Available Rule Sets</b></em>' map.
	 * The key is of type {@link KeyType},
	 * and the value is of type {@link ValueType},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Available Rule Sets</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Available Rule Sets</em>' map.
	 * @see de.hpi.sam.mote.MotePackage#getTGGEngine_AvailableRuleSets()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>" ordered="false"
	 * @generated
	 */
	EMap<String, RuleSetTag> getAvailableRuleSets();

	/**
	 * Returns the value of the '<em><b>Used Rule Sets</b></em>' map. The key is
	 * of type {@link KeyType}, and the value is of type {@link ValueType}, <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Used Rule Sets</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Used Rule Sets</em>' map.
	 * @see de.hpi.sam.mote.MotePackage#getTGGEngine_UsedRuleSets()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>"
	 *        ordered="false"
	 * @generated
	 */
	EMap<String, EList<RuleSetTag>> getUsedRuleSets();

	/**
	 * Returns the value of the '<em><b>Runtime Checks Enabled</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Runtime Checks Enabled</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Checks Enabled</em>' attribute.
	 * @see #setRuntimeChecksEnabled(boolean)
	 * @see de.hpi.sam.mote.MotePackage#getTGGEngine_RuntimeChecksEnabled()
	 * @model default="true"
	 * @generated
	 */
	boolean isRuntimeChecksEnabled();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.TGGEngine#isRuntimeChecksEnabled <em>Runtime Checks Enabled</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Runtime Checks Enabled</em>' attribute.
	 * @see #isRuntimeChecksEnabled()
	 * @generated
	 */
	void setRuntimeChecksEnabled(boolean value);

	/**
	 * Returns the value of the '<em><b>Notifications Enabled</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Notifications Enabled</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Notifications Enabled</em>' attribute.
	 * @see #setNotificationsEnabled(boolean)
	 * @see de.hpi.sam.mote.MotePackage#getTGGEngine_NotificationsEnabled()
	 * @model default="false"
	 * @generated
	 */
	boolean isNotificationsEnabled();

	/**
	 * Sets the value of the '{@link de.hpi.sam.mote.TGGEngine#isNotificationsEnabled <em>Notifications Enabled</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Notifications Enabled</em>' attribute.
	 * @see #isNotificationsEnabled()
	 * @generated
	 */
	void setNotificationsEnabled(boolean value);

	/**
	 * Returns the value of the '<em><b>Relation Policies</b></em>' map.
	 * The key is of type {@link KeyType},
	 * and the value is of type {@link ValueType},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relation Policies</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relation Policies</em>' map.
	 * @see de.hpi.sam.mote.MotePackage#getTGGEngine_RelationPolicies()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>" ordered="false"
	 * @generated
	 */
	EMap<String, MoteEngineRelationPolicy> getRelationPolicies();

	/**
	 * Returns the value of the '<em><b>Coverage Policies</b></em>' map.
	 * The key is of type {@link KeyType},
	 * and the value is of type {@link ValueType},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coverage Policies</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coverage Policies</em>' map.
	 * @see de.hpi.sam.mote.MotePackage#getTGGEngine_CoveragePolicies()
	 * @model mapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>" ordered="false"
	 * @generated
	 */
	EMap<String, MoteEngineCoveragePolicy> getCoveragePolicies();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Initializes the engine. Searches for plugins extending the ruleSet and
	 * diagramAdapter extension points. <!-- end-model-doc -->
	 * 
	 * @model annotation=
	 *        "http://www.eclipse.org/emf/2002/GenModel body='\t\t// Populate list of available ruleSets\r\n\t\torg.eclipse.core.runtime.IExtensionRegistry extensionRegistry = org.eclipse.core.runtime.Platform.getExtensionRegistry();\r\n\t\t\r\n\t\torg.eclipse.core.runtime.IConfigurationElement[] extensions = extensionRegistry\r\n\t\t\t\t.getConfigurationElementsFor(de.hpi.sam.mote.TGGConstants.MOTE_RULESET_EXTENSION__EXTENSION_NAME);\r\n\t\t\r\n\t\tfor (org.eclipse.core.runtime.IConfigurationElement configurationElement : extensions)\r\n\t\t{\r\n\t\t\tRuleSetTag tag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE.createRuleSetTag();\r\n\t\t\t\r\n\t\t\ttag.setRuleSetID(configurationElement.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_RULESET_EXTENSION__RULE_SET_ID));\r\n\t\t\ttag.setRuleSetName(configurationElement.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_RULESET_EXTENSION__RULE_SET_NAME));\r\n\t\t\ttag.setRuleSetPackageNsURI(configurationElement\r\n\t\t\t\t\t.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_RULESET_EXTENSION__PACKAGE_NS_URI));\r\n\t\t\ttag.setSourceModelID(configurationElement\r\n\t\t\t\t\t.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_RULESET_EXTENSION__SOURCE_MODEL_ID));\r\n\t\t\ttag.setTargetModelID(configurationElement\r\n\t\t\t\t\t.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_RULESET_EXTENSION__TARGET_MODEL_ID));\r\n\t\t\t\r\n\t\t\t// Check that all fields are not empty, that id is not in use and\r\n\t\t\t// add the ruleSetTag to the list of tags\r\n\t\t\tif (tag.getRuleSetID() != null && !\"\".equals(tag.getRuleSetID()) && tag.getRuleSetName() != null\r\n\t\t\t\t\t&& !\"\".equals(tag.getRuleSetName()) && tag.getRuleSetPackageNsURI() != null && !\"\".equals(tag.getRuleSetPackageNsURI())\r\n\t\t\t\t\t&& tag.getSourceModelID() != null && !\"\".equals(tag.getSourceModelID()) && tag.getTargetModelID() != null\r\n\t\t\t\t\t&& !\"\".equals(tag.getTargetModelID()))\r\n\t\t\t{\r\n\t\t\t\tgetAvailableRuleSets().put(tag.getRuleSetID(), tag);\r\n\t\t\t}\r\n\t\t}\r\n\t\t\r\n\t\t// Setup list of diagram adapters\r\n\t\textensions = extensionRegistry\r\n\t\t\t\t.getConfigurationElementsFor(de.hpi.sam.mote.TGGConstants.MOTE_DIAGRAM_ADAPTER_EXTENSION__EXTENSION_NAME);\r\n\t\t\r\n\t\tfor (org.eclipse.core.runtime.IConfigurationElement configurationElement : extensions)\r\n\t\t{\r\n\t\t\tDiagramAdapterTag tag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE.createDiagramAdapterTag();\r\n\t\t\t\r\n\t\t\ttag.setCreationMethodName(configurationElement\r\n\t\t\t\t\t.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_DIAGRAM_ADAPTER_EXTENSION__CREATION_METHOD_NAME));\r\n\t\t\ttag.setDiagramAdapterPackageNsURI(configurationElement\r\n\t\t\t\t\t.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_DIAGRAM_ADAPTER_EXTENSION__PACKAGE_NS_URI));\r\n\t\t\ttag.setDiagramAdapterID(configurationElement\r\n\t\t\t\t\t.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_DIAGRAM_ADAPTER_EXTENSION__DIAGRAM_ADAPTER_ID));\r\n\t\t\ttag.setModelID(configurationElement.getAttribute(de.hpi.sam.mote.TGGConstants.MOTE_DIAGRAM_ADAPTER_EXTENSION__MODEL_ID));\r\n\t\t\t\r\n\t\t\tif (tag.getCreationMethodName() != null && !\"\".equals(tag.getCreationMethodName())\r\n\t\t\t\t\t&& tag.getDiagramAdapterPackageNsURI() != null && !\"\".equals(tag.getDiagramAdapterPackageNsURI())\r\n\t\t\t\t\t&& tag.getDiagramAdapterID() != null && !\"\".equals(tag.getDiagramAdapterID()) && tag.getModelID() != null\r\n\t\t\t\t\t&& !\"\".equals(tag.getModelID()))\r\n\t\t\t{\r\n\t\t\t\tgetAvailableDiagramAdapters().put(tag.getModelID(), tag);\r\n\t\t\t}\r\n\t\t}'"
	 * @generated
	 */
	void initializeEngine();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Creates a new ruleSet object using the information in the ruleSetTag.
	 * <!-- end-model-doc -->
	 * 
	 * @model sourceUriDataType="de.hpi.sam.mote.helpers.URI"
	 *        targetUriDataType="de.hpi.sam.mote.helpers.URI" annotation=
	 *        "http://www.eclipse.org/emf/2002/GenModel body='\t\ttry\r\n\t\t{\r\n\t\t\t/*\r\n\t\t\t * First make a copy of the ruleSetTag\r\n\t\t\t \052/\r\n\t\t\tRuleSetTag tag = (RuleSetTag) EcoreUtil.copy(ruleSetTag);\r\n\r\n\t\t\t// Get the ruleSet package\r\n\t\t\torg.eclipse.emf.ecore.EPackage ruleSetPackage = org.eclipse.emf.ecore.impl.EPackageRegistryImpl.INSTANCE.getEPackage(tag\r\n\t\t\t\t\t.getRuleSetPackageNsURI());\r\n\r\n\t\t\t// Search for a class that is derived from TGGRuleSet\r\n\t\t\torg.eclipse.emf.ecore.EClass ruleSetEclass = null;\r\n\t\t\tfor (org.eclipse.emf.ecore.EClassifier ec : ruleSetPackage.getEClassifiers())\r\n\t\t\t{\r\n\t\t\t\tif (ec instanceof org.eclipse.emf.ecore.EClass)\r\n\t\t\t\t{\r\n\t\t\t\t\tif (((org.eclipse.emf.ecore.EClass) ec).getEAllSuperTypes().contains(\r\n\t\t\t\t\t\t\tde.hpi.sam.mote.rules.RulesPackage.Literals.TGG_RULE_SET))\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\truleSetEclass = (org.eclipse.emf.ecore.EClass) ec;\r\n\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t}\r\n\t\t\t\t}\r\n\t\t\t}\r\n\r\n\t\t\tif (ruleSetEclass == null)\r\n\t\t\t{\r\n\t\t\t\tthrow new Exception(\"Could not find a class inheriting from TGGRuleSet in package \" + ruleSetPackage);\r\n\t\t\t}\r\n\r\n\t\t\t// Get the ruleSet factory\r\n\t\t\torg.eclipse.emf.ecore.EFactory ruleSetFactory = ruleSetPackage.getEFactoryInstance();\r\n\r\n\t\t\t// Create a new ruleSet\r\n\t\t\tTGGRuleSet ruleSet = (TGGRuleSet) ruleSetFactory.create(ruleSetEclass);\r\n\r\n\t\t\tif (ruleSet == null)\r\n\t\t\t{\r\n\t\t\t\tthrow new TransformationException(\"The ruleSet with ID \" + ruleSetTag.getRuleSetID() + \" could not be created.\", null);\r\n\t\t\t}\r\n\r\n\t\t\truleSet.initializeRuleSet();\r\n\r\n\t\t\truleSetTag.setSourceModelUri(sourceUri);\r\n\t\t\truleSetTag.setTargetModelUri(targetUri);\r\n\r\n\t\t\truleSetTag.setRuleSet(ruleSet);\r\n\r\n\t\t\treturn ruleSetTag;\r\n\t\t}\r\n\t\tcatch (Exception ex)\r\n\t\t{\r\n\t\t\tex.printStackTrace();\r\n\t\t\treturn null;\r\n\t\t}'"
	 * @generated
	 */
	RuleSetTag initializeRuleSet(RuleSetTag ruleSetTag, URI sourceUri,
			URI targetUri) throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Performs a model transformation in memory on the specified resources.
	 * <!-- end-model-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException" leftResourceRequired="true" rightResourceRequired="true" dependenciesMapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>" monitorDataType="de.hpi.sam.mote.helpers.IProgressMonitor"
	 * @generated
	 */
	TransformationExecutionResult transform(Resource leftResource, Resource rightResource, EMap<Resource, EList<EObject>> dependencies, TransformationDirection transformationDirection, String ruleSetID, boolean synchronize, IProgressMonitor monitor) throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" exceptions="de.hpi.sam.mote.helpers.TransformationException" leftResourceRequired="true" rightResourceRequired="true" dependenciesMapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>"
	 * @generated
	 */
	TransformationExecutionResult transform(Resource leftResource, Resource rightResource, EMap<Resource, EList<EObject>> dependencies, TransformationDirection transformationDirection, String ruleSetID, boolean synchronize) throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" exceptions="de.hpi.sam.mote.helpers.TransformationException" leftResourceRequired="true" rightResourceRequired="true" dependenciesMapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>" ruleSetIDRequired="true"
	 * @generated
	 */
	TransformationExecutionResult checkConsistency(Resource leftResource, Resource rightResource, EMap<Resource, EList<EObject>> dependencies, String ruleSetID) throws TransformationException;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Executes the model transformation. <!-- end-model-doc -->
	 * 
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException"
	 *        monitorDataType="de.hpi.sam.mote.helpers.IProgressMonitor"
	 *        annotation=
	 *        "http://www.eclipse.org/emf/2002/GenModel body='\t\truleSet.setProcessNotifications(false);\r\n\r\n\t\t/*\r\n\t\t * If the transformation queue of the ruleSet is empty, perform a batch\r\n\t\t * transformation\r\n\t\t \052/\r\n\t\tboolean batchMode = false;\r\n\r\n\t\tif (ruleSet.getTransformationQueue().isEmpty())\r\n\t\t{\r\n\t\t\tbatchMode = true;\r\n\r\n\t\t\tde.hpi.sam.mote.helpers.ModificationTag modTag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE.createModificationTag();\r\n\t\t\tmodTag.setCorrespondenceNode(ruleSet.getRootCorrNode());\r\n\t\t\truleSet.getTransformationQueue().add(modTag);\r\n\r\n\t\t\t/*\r\n\t\t\t * In case of synchronization, add all direct children of the root\r\n\t\t\t * node to the queue.\r\n\t\t\t \052/\r\n\t\t\tif (synchronize)\r\n\t\t\t{\r\n\t\t\t\tfor (de.hpi.sam.mote.TGGNode n : ruleSet.getRootCorrNode().getNext())\r\n\t\t\t\t{\r\n\t\t\t\t\tmodTag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE.createModificationTag();\r\n\t\t\t\t\tmodTag.setCorrespondenceNode(n);\r\n\t\t\t\t\tmodTag.setSynchronize(true);\r\n\t\t\t\t\truleSet.getTransformationQueue().add(modTag);\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t}\r\n\r\n\t\tboolean rulesPerformedModifications = true;\r\n\r\n\t\twhile (rulesPerformedModifications)\r\n\t\t{\r\n\t\t\t/*\r\n\t\t\t * If any modifications were performed during the previous\r\n\t\t\t * synchronization run, add all correspondence nodes to delete to\r\n\t\t\t * the queue, and repeat the synchronization run.\r\n\t\t\t \052/\r\n\t\t\tif (rulesPerformedModifications)\r\n\t\t\t{\r\n\t\t\t\trulesPerformedModifications = false;\r\n\r\n\t\t\t\tfor (TGGNode node : ruleSet.getCorrNodesToDelete())\r\n\t\t\t\t{\r\n\t\t\t\t\truleSet.addModificationTagToQueue(node, false, false, true);\r\n\t\t\t\t}\r\n\r\n\t\t\t\truleSet.getCorrNodesToDelete().clear();\r\n\t\t\t}\r\n\r\n\t\t\twhile (!ruleSet.getTransformationQueue().isEmpty())\r\n\t\t\t{\r\n\t\t\t\tde.hpi.sam.mote.helpers.ModificationTag modTag = ruleSet.getTransformationQueue().pop();\r\n\r\n\t\t\t\tde.hpi.sam.mote.TGGNode corrNode = modTag.getCorrespondenceNode();\r\n\r\n\t\t\t\t/*\r\n\t\t\t\t * Skip correspondence nodes that have been deleted but are\r\n\t\t\t\t * still in the transformation queue.\r\n\t\t\t\t \052/\r\n\t\t\t\tif (corrNode.getRuleSet() != null)\r\n\t\t\t\t{\r\n\t\t\t\t\tif (modTag.isSynchronize())\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\t/*\r\n\t\t\t\t\t\t * In case of synchronization, execute the rule that\r\n\t\t\t\t\t\t * created this correspondence node.\r\n\t\t\t\t\t\t \052/\r\n\t\t\t\t\t\tswitch (transformationDirection)\r\n\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\tcase FORWARD:\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\trulesPerformedModifications = ((de.hpi.sam.mote.rules.TGGMapping) corrNode.getCreationRule())\r\n\t\t\t\t\t\t\t\t\t\t.forwardSynchronization(corrNode)\r\n\t\t\t\t\t\t\t\t\t\t|| rulesPerformedModifications;\r\n\t\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\tcase MAPPING:\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\trulesPerformedModifications = ((de.hpi.sam.mote.rules.TGGMapping) corrNode.getCreationRule())\r\n\t\t\t\t\t\t\t\t\t\t.mappingSynchronization(corrNode)\r\n\t\t\t\t\t\t\t\t\t\t|| rulesPerformedModifications;\r\n\t\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\tcase REVERSE:\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\trulesPerformedModifications = ((de.hpi.sam.mote.rules.TGGMapping) corrNode.getCreationRule())\r\n\t\t\t\t\t\t\t\t\t\t.reverseSynchronization(corrNode)\r\n\t\t\t\t\t\t\t\t\t\t|| rulesPerformedModifications;\r\n\t\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\t/*\r\n\t\t\t\t\t\t * In case of transformation, execute all rules that are\r\n\t\t\t\t\t\t * applicable for this correspondence node.\r\n\t\t\t\t\t\t \052/\r\n\t\t\t\t\t\tfor (de.hpi.sam.mote.rules.TGGRule rule : ruleSet.getRules())\r\n\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\tif (rule.acceptsParentCorrNode(corrNode))\r\n\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\tswitch (transformationDirection)\r\n\t\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\t\tcase FORWARD:\r\n\t\t\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\t\t\trulesPerformedModifications = rule.forwardTransformation(corrNode) || rulesPerformedModifications;\r\n\t\t\t\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t\t\tcase MAPPING:\r\n\t\t\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\t\t\trulesPerformedModifications = rule.mappingTransformation(corrNode) || rulesPerformedModifications;\r\n\t\t\t\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t\t\tcase REVERSE:\r\n\t\t\t\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\t\t\t\trulesPerformedModifications = rule.reverseTransformation(corrNode) || rulesPerformedModifications;\r\n\t\t\t\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}\r\n\r\n\t\t\t\t\t/*\r\n\t\t\t\t\t * In case of batch synchronization, add successors to queue\r\n\t\t\t\t\t \052/\r\n\t\t\t\t\tif (batchMode && synchronize)\r\n\t\t\t\t\t{\r\n\t\t\t\t\t\tfor (de.hpi.sam.mote.TGGNode n : corrNode.getNext())\r\n\t\t\t\t\t\t{\r\n\t\t\t\t\t\t\tmodTag = de.hpi.sam.mote.helpers.HelpersFactory.eINSTANCE.createModificationTag();\r\n\t\t\t\t\t\t\tmodTag.setCorrespondenceNode(n);\r\n\t\t\t\t\t\t\tmodTag.setSynchronize(true);\r\n\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t}\r\n\t\t\r\n\t\t/*\r\n\t\t * Delete all elements that are marked to delete\r\n\t\t \052/\r\n\t\tfor (TGGNode node : ruleSet.getCorrNodesToDelete())\r\n\t\t{\r\n\t\t\truleSet.deleteElements(node, transformationDirection);\r\n\t\t}\r\n\r\n\t\truleSet.setProcessNotifications(true);'"
	 * @generated
	 */
	void executeTransformationRules(TGGRuleSet ruleSet,
			TransformationDirection transformationDirection,
			boolean synchronize, IProgressMonitor monitor)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException" monitorDataType="de.hpi.sam.mote.helpers.IProgressMonitor"
	 * @generated
	 */
	void performRuntimeChecks(TGGRuleSet ruleSet,
			TransformationDirection transformationDirection,
			IProgressMonitor monitor) throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException" sourceResourceRequired="true" targetElementRequired="true" ruleSetIdRequired="true"
	 * @generated
	 */
	EList<EObject> correspondingSourceElements(Resource sourceResource, EObject targetElement, String ruleSetId) throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException" sourceElementRequired="true" targetResourceRequired="true" ruleSetIdRequired="true"
	 * @generated
	 */
	EList<EObject> correspondingTargetElements(EObject sourceElement, Resource targetResource, String ruleSetId) throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceRequired="true"
	 * @generated
	 */
	void resourceDeleted(Resource resource);

	TransformationExecutionResult createTransformationExecutionResult( TransformationResult p_result );

} // TGGEngine
