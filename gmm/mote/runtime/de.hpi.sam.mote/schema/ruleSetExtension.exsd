<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="de.hpi.sam.mote" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appinfo>
         <meta.schema plugin="de.hpi.sam.mote" id="ruleSetExtension" name="RuleSet Extension Point"/>
      </appinfo>
      <documentation>
         RuleSet must extend this extension point so the transformation engine is able to discover them.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appinfo>
            <meta.element />
         </appinfo>
      </annotation>
      <complexType>
         <sequence>
            <element ref="ruleSet"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appinfo>
                  <meta.attribute translatable="true"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="ruleSet">
      <annotation>
         <documentation>
            A set of rules.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="name" type="string" use="required">
            <annotation>
               <documentation>
                  The name of the ruleSet.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  The unique ID of the ruleSet.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="packageNsURI" type="string" use="required">
            <annotation>
               <documentation>
                  Namespace URI of the package that contains the factory to create the ruleSet.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="sourceModelId" type="string" use="required">
            <annotation>
               <documentation>
                  ID of the source (left) model of the rules.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="targetModelId" type="string" use="required">
            <annotation>
               <documentation>
                  ID of the target (right) model of the rules.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="ruleSetOptions" type="string" use="required">
            <annotation>
               <documentation>
                  The options supported by the ruleSet, i.e. forward, mapping and reverse transformation and synchronization. This must be the result of a bitwise AND of the values in RuleSetOptionsEnumeration.
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appinfo>
         <meta.section type="since"/>
      </appinfo>
      <documentation>
         [Enter the first release in which this extension point appears.]
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="examples"/>
      </appinfo>
      <documentation>
         [Enter extension point usage example here.]
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="apiinfo"/>
      </appinfo>
      <documentation>
         [Enter API information here.]
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="implementation"/>
      </appinfo>
      <documentation>
         [Enter information about supplied implementation of this extension point.]
      </documentation>
   </annotation>


</schema>
