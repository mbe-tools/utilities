package de.mdelab.sdm.interpreter.sde.eclipse;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.eclipse.EclipseExpressionInterpreterManager;
import de.mdelab.sdm.interpreter.core.facade.MetamodelFacadeFactory;
import de.mdelab.sdm.interpreter.core.notifications.NotificationEmitter;
import de.mdelab.sdm.interpreter.core.patternmatcher.MatchingStrategy;
import de.mdelab.sdm.interpreter.core.variables.VariablesScope;
import de.mdelab.sdm.interpreter.sde.patternmatcher.patternPartBased.SDEPatternPartBasedMatcher;

/**
 * Implementation of the pattern part based matcher for the StoryDiagramEcore
 * metamodel and use in Eclipse.
 * 
 * @author stephan
 * 
 */
public class SDEEclipsePatternPartBasedMatcher extends SDEPatternPartBasedMatcher {
//
//	public SDEEclipsePatternPartBasedMatcher(
//			StoryActionNode storyPattern,
//			VariablesScope<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> variableScope,
//			MetamodelFacadeFactory<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> facadeFactory,
//			ClassLoader classLoader) throws SDMException
//	{
//		this(	storyPattern,
//				variableScope,
//				facadeFactory,
//				classLoader,
//				new NotificationEmitter<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>());
//	}

	public SDEEclipsePatternPartBasedMatcher(
			StoryActionNode storyPattern,
			VariablesScope<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> variablesScope,
			MatchingStrategy<StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> matchingStrategy,
			MetamodelFacadeFactory<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> facadeFactory,
			ClassLoader classLoader,
			NotificationEmitter<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> notificationEmitter)
	throws SDMException	{
		super(	storyPattern,
				variablesScope,
				matchingStrategy,
				facadeFactory,
				new EclipseExpressionInterpreterManager<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>(
						facadeFactory, classLoader, notificationEmitter), notificationEmitter);
	}

}
