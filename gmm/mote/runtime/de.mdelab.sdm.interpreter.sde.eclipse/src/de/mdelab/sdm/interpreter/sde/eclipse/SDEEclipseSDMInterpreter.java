package de.mdelab.sdm.interpreter.sde.eclipse;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.eclipse.EclipseExpressionInterpreterManager;
import de.mdelab.sdm.interpreter.core.facade.MetamodelFacadeFactory;
import de.mdelab.sdm.interpreter.core.notifications.NotificationEmitter;
import de.mdelab.sdm.interpreter.sde.SDESDMInterpreter;
import de.mdelab.sdm.interpreter.sde.facade.SDEMetamodelFacadeFactory;

/**
 * Implementation of the SDM interpreter for the StoryDiagramEcore metamodel and
 * use in Eclipse.
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEEclipseSDMInterpreter extends SDESDMInterpreter
{
	public SDEEclipseSDMInterpreter(
			ClassLoader classLoader,
			NotificationEmitter<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> notificationEmitter)
	throws SDMException {
		this( SDEMetamodelFacadeFactory.INSTANCE, classLoader, notificationEmitter );
	}

	public SDEEclipseSDMInterpreter(
			MetamodelFacadeFactory<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> facadeFactory,
			ClassLoader classLoader,
			NotificationEmitter<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> notificationEmitter)
	throws SDMException	{
		super(	facadeFactory,
				new EclipseExpressionInterpreterManager<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>(
					// DB: we need a new instance for each interpreter to ensure cache is cleared.
						facadeFactory, classLoader, notificationEmitter ),
				notificationEmitter );
	}

	public SDEEclipseSDMInterpreter(ClassLoader classLoader)
	throws SDMException	{
		this( 	SDEMetamodelFacadeFactory.INSTANCE,
				classLoader,
				new NotificationEmitter<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>() );
	}
}
