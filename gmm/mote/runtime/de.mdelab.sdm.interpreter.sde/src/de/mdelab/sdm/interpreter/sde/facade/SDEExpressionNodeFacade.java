package de.mdelab.sdm.interpreter.sde.facade;

import java.util.ArrayList;
import java.util.Collection;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.ExpressionActivityNode;
import de.mdelab.sdm.interpreter.core.facade.IExpressionNodeFacade;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEExpressionNodeFacade extends SDEActivityNodeFacade implements IExpressionNodeFacade<ActivityNode, ActivityEdge, Expression>
{

	@Override
	public Collection<Expression> getExpressions(ActivityNode activityNode)
	{
		assert activityNode instanceof ExpressionActivityNode;

		Collection<Expression> collection = new ArrayList<Expression>(1);
		collection.add(((ExpressionActivityNode) activityNode).getExpression());

		return collection;
	}

	@Override
	public ActivityEdge getNextActivityEdge(ActivityNode activityNode)
	{
		assert activityNode.getOutgoing().size() == 1;

		return activityNode.getOutgoing().get(0);
	}

}
