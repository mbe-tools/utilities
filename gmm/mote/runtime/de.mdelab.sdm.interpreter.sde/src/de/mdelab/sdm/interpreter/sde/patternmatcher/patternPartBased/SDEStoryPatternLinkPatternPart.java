package de.mdelab.sdm.interpreter.sde.patternmatcher.patternPartBased;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkOrderConstraintEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraint;
import de.hpi.sam.storyDiagramEcore.sdm.LinkPositionConstraintEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.facade.IInstanceFacade;
import de.mdelab.sdm.interpreter.core.facade.IStoryPatternLinkFacade;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.CheckPatternPartTransaction;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.ECheckResult;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.EMatchType;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.MatchState;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.MatchTransaction;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPart;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPartBasedMatcher;
import de.mdelab.sdm.interpreter.core.variables.Variable;

/**
 * @author Stephan Hildebrandt
 */
public class SDEStoryPatternLinkPatternPart extends SDEPatternPart<AbstractStoryPatternObject, StoryPatternLink> {
	
	protected final IStoryPatternLinkFacade<AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> facade;

	public SDEStoryPatternLinkPatternPart(
			final PatternPartBasedMatcher<?, ?, ?, ?, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> patternMatcher,
			final StoryPatternLink storyPatternLink) {
		super(	patternMatcher, 
				storyPatternLink, 
				new AbstractStoryPatternObject[] { storyPatternLink.getSource(), storyPatternLink.getTarget()} );

		/*
		 * if a story pattern object is created, the link must also be created
		 */
		assert (storyPatternLink.getSource().getModifier() != StoryPatternModifierEnumeration.CREATE)
				|| (storyPatternLink.getModifier() == StoryPatternModifierEnumeration.CREATE);
		assert (storyPatternLink.getTarget().getModifier() != StoryPatternModifierEnumeration.CREATE)
				|| (storyPatternLink.getModifier() == StoryPatternModifierEnumeration.CREATE);

		/*
		 * if a story pattern object is destroyed, the link must also be
		 * destroyed
		 */
		assert (storyPatternLink.getSource().getModifier() != StoryPatternModifierEnumeration.DESTROY)
				|| (storyPatternLink.getModifier() == StoryPatternModifierEnumeration.DESTROY);
		assert (storyPatternLink.getTarget().getModifier() != StoryPatternModifierEnumeration.DESTROY)
				|| (storyPatternLink.getModifier() == StoryPatternModifierEnumeration.DESTROY);

		/*
		 * If one of the link ends is optional, the links must be optional as
		 * well.
		 */
		assert (storyPatternLink.getSource().getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL)
				|| (storyPatternLink.getMatchType() == StoryPatternMatchTypeEnumeration.OPTIONAL);
		assert (storyPatternLink.getTarget().getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL)
				|| (storyPatternLink.getMatchType() == StoryPatternMatchTypeEnumeration.OPTIONAL);

		/*
		 * If the link is created or destroyed, the link's feature must be
		 * changeable
		 */
		// DB This assert cannot be verified for abstract rules.
//		assert ((storyPatternLink.getModifier() != StoryPatternModifierEnumeration.CREATE) && (storyPatternLink.getModifier() != StoryPatternModifierEnumeration.DESTROY))
//				|| storyPatternLink.getEStructuralFeature().isChangeable();
		
		facade = patternMatcher.getFacadeFactory().getStoryPatternLinkFacade();
	}

	@Override
	public MatchState createMatchState() {
		return new SDEStoryPatternLinkMatchState<Iterator<Object>>();
	}

	@SuppressWarnings("unchecked")
	@Override
	public ECheckResult check() throws SDMException
	{
		final AbstractStoryPatternObject source = this.storyPatternLink.getSource();
		final AbstractStoryPatternObject target = this.storyPatternLink.getTarget();

		if (!this.isCreate && this.patternMatcher.isBound(source) && this.patternMatcher.isBound(target))
		{
			final EObject sourceInstanceObject = (EObject) this.patternMatcher.getInstanceObject(source);
			checkUnresolvedProxy( sourceInstanceObject );
			
			final Object targetInstanceObject = this.patternMatcher.getInstanceObject(target);
			checkUnresolvedProxy( targetInstanceObject );

			final Object featureValue = facade.getFeatureValue( storyPatternLink,
																sourceInstanceObject,
																patternMatcher.getExpressionInterpreterManager(),
																patternMatcher.getVariablesScope() ); 
			checkUnresolvedProxy( featureValue );

			if (!facade.isMany( storyPatternLink ) )
			{
				assert this.storyPatternLink.getLinkPositionConstraint() == null;
				assert this.storyPatternLink.getIncomingLinkOrderConstraints().isEmpty();
				assert this.storyPatternLink.getOutgoingLinkOrderConstraints().isEmpty();

				if ( featureValue == targetInstanceObject)
				{
					this.patternMatcher.getNotificationEmitter().linkCheckSuccessful(source, sourceInstanceObject, this.storyPatternLink,
							target, targetInstanceObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);

					return ECheckResult.OK;
				}

				patternMatcher.getNotificationEmitter().linkCheckFailed(source, sourceInstanceObject, this.storyPatternLink,
							target, targetInstanceObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);

				return ECheckResult.FAIL;
			}

			if (( (Collection<Object>) featureValue ).contains(targetInstanceObject))
			{
				this.patternMatcher.getNotificationEmitter().linkCheckSuccessful(source, sourceInstanceObject, this.storyPatternLink,
						target, targetInstanceObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);

				return ECheckResult.OK;
			}

			patternMatcher.getNotificationEmitter().linkCheckFailed(source, sourceInstanceObject, storyPatternLink,
					target, targetInstanceObject, patternMatcher.getVariablesScope(), patternMatcher);

			return ECheckResult.FAIL;
		}
		
		if (this.isCreate
				&& ((this.storyPatternLink.getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL) || (this.patternMatcher
						.isBound(this.storyPatternLink.getSource()) && this.patternMatcher.isBound(this.storyPatternLink.getTarget()))))
		{
			return ECheckResult.OK;
		}

		return ECheckResult.UNKNOWN;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean match(final MatchState matchState) throws SDMException
	{
		assert matchState instanceof SDEStoryPatternLinkMatchState;

		final SDEStoryPatternLinkMatchState<Iterator<Object>> ms = (SDEStoryPatternLinkMatchState<Iterator<Object>>) matchState;

		AbstractStoryPatternObject sourceSpo = this.storyPatternLink.getSource();
		AbstractStoryPatternObject targetSpo = this.storyPatternLink.getTarget();

		assert this.patternMatcher.isBound(sourceSpo) || this.patternMatcher.isBound(targetSpo);
		assert !(this.patternMatcher.isBound(sourceSpo) && this.patternMatcher.isBound(targetSpo));

		/*
		 * The source object is bound. Search for a match of the target object.
		 */
		if ( patternMatcher.isBound( sourceSpo ) ) {
			final EObject sourceInstanceObject = (EObject) patternMatcher.getInstanceObject(sourceSpo);
			final Object targetValue = facade.getFeatureValue( 	storyPatternLink,
																sourceInstanceObject,
																patternMatcher.getExpressionInterpreterManager(),
																patternMatcher.getVariablesScope() );

			return matchTargetObject(	ms,
										sourceSpo,
										targetSpo,
										sourceInstanceObject,
										targetValue,
										facade.isMany( storyPatternLink ),
										false );
		}
		
		if ( facade.isReference( storyPatternLink ) ) {
			/*
			 * Start matching from the target object
			 */
			sourceSpo = this.storyPatternLink.getTarget();
			targetSpo = this.storyPatternLink.getSource();

			final EObject sourceInstanceObject = (EObject) this.patternMatcher.getInstanceObject(sourceSpo);
			final Object targetValue = facade.getReverseFeatureValue( storyPatternLink, sourceInstanceObject );
			final boolean matched = matchTargetObject(	ms,
														sourceSpo,
														targetSpo,
														sourceInstanceObject, 
														targetValue,
														facade.reverseReferenceIsMany( storyPatternLink ),
														true );
			
			if ( !matched ) {
				patternMatcher.getNotificationEmitter().storyPatternObjectNotBound(	targetSpo,
																					patternMatcher.getVariablesScope(),
																					patternMatcher );
			}
			
			return matched;
		}

		throw new UnsupportedOperationException();
	}
	
	private boolean match( 	final Object p_targetInstanceObject,
							final AbstractStoryPatternObject p_targetSpo, 
							final Object p_targetObject2 )
	throws SDMException {
		checkUnresolvedProxy( p_targetInstanceObject );
		
		if ( p_targetObject2 == null ) {
			return patternMatcher.matchStoryPatternObject( p_targetSpo, p_targetInstanceObject );
		}

		checkUnresolvedProxy( p_targetObject2 );
		
		return p_targetObject2 == p_targetInstanceObject;
	}

		
	private Object match( 	final Collection<Object> p_objects,
							final AbstractStoryPatternObject p_targetSpo, 
							final Object p_targetObject,
							final LinkPositionConstraintEnumeration p_posEnum )
	throws SDMException {
		assert !(p_targetSpo == null && p_targetObject == null );
		
		if ( p_objects.isEmpty() ) {
			return false;
		}

		final Iterator<Object> iterator = p_objects.iterator();
	
		switch ( p_posEnum ) {
			case FIRST: {
				final Object targetInstanceObject = iterator.next();
				
				if ( match( targetInstanceObject, p_targetSpo, p_targetObject ) ) {
					return targetInstanceObject;
				}
				
				return null;
			}
			case MIDDLE: {
				if ( p_objects.size() > 2 ) {
					Object targetInstanceObject = iterator.next();
	
					while ( iterator.hasNext() ) {
						targetInstanceObject = iterator.next();
						
						if ( iterator.hasNext() ) {
							if ( match( targetInstanceObject, p_targetSpo, p_targetObject ) ) {
								return targetInstanceObject;
							}
						}
					}
				}
				
				return null;
			}
			case LAST: {
				Object targetInstanceObject = iterator.next();
				
				while ( iterator.hasNext() ) {
					targetInstanceObject = iterator.next();
				}
				
				if ( match( targetInstanceObject, p_targetSpo, p_targetObject ) ) {
					return targetInstanceObject;
				}
				
				return null;
			}
			default:
				throw new IllegalArgumentException( "Unknown poition constraint " + p_posEnum );
		}
	}

	@SuppressWarnings("unchecked")
	private boolean matchTargetObject(	final SDEStoryPatternLinkMatchState<Iterator<Object>> matchState,
										final AbstractStoryPatternObject sourceSpo,
										final AbstractStoryPatternObject targetSpo,
										final EObject sourceInstanceObject,
										final Object p_targetObject,
										final boolean pb_many,
										final boolean pb_startingFromTarget )
	throws SDMException {
		assert matchState != null;
		assert sourceSpo != null;
		assert targetSpo != null;
		assert sourceInstanceObject != null;

		patternMatcher.getNotificationEmitter().traversingLink(this.storyPatternLink, sourceSpo, sourceInstanceObject, targetSpo,
				patternMatcher.getVariablesScope(), this.patternMatcher);
		
		checkUnresolvedProxy( sourceInstanceObject );
		checkUnresolvedProxy( p_targetObject );
		final LinkPositionConstraint posCst = getStoryPatternLink().getLinkPositionConstraint();

		if ( pb_startingFromTarget && posCst != null ) {
			// DB: First match the target (source) object
			if ( patternMatcher.matchStoryPatternObject( targetSpo, p_targetObject ) ) {
				// DB This means that there is a collection of source objects and we need that this
				// object satisfies the position constraints
				final Collection<Object> sourceObjects = (Collection<Object>) facade.getFeatureValue( 	storyPatternLink,
																										p_targetObject, 
																										patternMatcher.getExpressionInterpreterManager(),
																										patternMatcher.getVariablesScope() );
				final Object matchedTargetObj = match( 	sourceObjects, 
														sourceSpo,
														sourceInstanceObject,
														posCst.getConstraintType() );
				
				if ( matchedTargetObj == sourceInstanceObject //&& 	// Matching the link position 
						/*patternMatcher.matchStoryPatternObject( targetSpo, p_targetObject )*/ ) {
					patternMatcher.getNotificationEmitter().storyPatternObjectBound( 	targetSpo,
																						matchedTargetObj,
																						patternMatcher.getVariablesScope(),
																						patternMatcher );
					
					return true;
				}
			}
		}
		/*
		 * This is a to-one feature.
		 */
		else if ( !pb_many ) {
			/*
			 * Link constraints for to-one features do not make sense.
			 */
			assert this.storyPatternLink.getLinkPositionConstraint() == null;
			assert this.storyPatternLink.getOutgoingLinkOrderConstraints().isEmpty();
			assert this.storyPatternLink.getIncomingLinkOrderConstraints().isEmpty();

			if (this.patternMatcher.matchStoryPatternObject(targetSpo, p_targetObject/*targetInstanceObject*/))
			{
				this.patternMatcher.getNotificationEmitter().storyPatternObjectBound(targetSpo, p_targetObject/*targetInstanceObject*/,
						this.patternMatcher.getVariablesScope(), this.patternMatcher);

				return true;
			}
		}
		/*
		 * This is a to-many feature.
		 */
		// DB Handle matching the order constraint
		else {
			if ( posCst == null ) {
				Iterator<Object> linkIterator = matchState.getLinkIterator();
				
	
				if ((linkIterator == null) || (sourceInstanceObject != matchState.getSourceInstanceObject()))
				{
					linkIterator = ((Collection<Object>) p_targetObject ).iterator();
	
					matchState.setLinkIterator(linkIterator);
					matchState.setSourceInstanceObject(sourceInstanceObject);
				}
	
				while (linkIterator.hasNext()) {
					final Object targetInstanceObject = linkIterator.next();
					checkUnresolvedProxy( targetInstanceObject );
	
					if (this.patternMatcher.matchStoryPatternObject(targetSpo, targetInstanceObject))
					{
						this.patternMatcher.getNotificationEmitter().storyPatternObjectBound(targetSpo, targetInstanceObject,
								this.patternMatcher.getVariablesScope(), this.patternMatcher);
	
						return true;
					}
				}
			}
			// DB Handle matching the order constraint
			else {
				final Object matchedTargetObj = match( 	(Collection<Object>) p_targetObject, 
														targetSpo,
														null,
														posCst.getConstraintType() );
				
				if ( matchedTargetObj == null ) {
					return false;
				}

				patternMatcher.getNotificationEmitter().storyPatternObjectBound( 	targetSpo,
																					matchedTargetObj,
																					patternMatcher.getVariablesScope(),
																					patternMatcher );
					
				return true;
			}
		}

		patternMatcher.getNotificationEmitter().storyPatternObjectNotBound(targetSpo, this.patternMatcher.getVariablesScope(),
				patternMatcher);

		return false;
	}
	
	private LinkPositionConstraintEnumeration positionConstraint( final Object p_object ) {
		final Variable<?> var = patternMatcher.getVariablesScope().getVariableFromValue( p_object );
		
		if ( var != null ) {
			final String spoName = var.getName();
			final EStructuralFeature feature = getStoryPatternLink().getEStructuralFeature();
			final Enumeration<MatchTransaction> transactions = patternMatcher.matchingStack.elements();
			
			while ( transactions.hasMoreElements() ) {
				final MatchTransaction transaction = transactions.nextElement();
				
				if ( transaction instanceof CheckPatternPartTransaction<?, ?, ?, ?, ?> ) {
					final CheckPatternPartTransaction<?, ?, ?, ?, ?> checkTrans = (CheckPatternPartTransaction<?, ?, ?, ?, ?>) transaction;
					@SuppressWarnings("unchecked")
					final PatternPart<AbstractStoryPatternObject, ?, ?, ?, ?> patternPart = (PatternPart<AbstractStoryPatternObject, ?, ?, ?, ?>) checkTrans.getPatternPart();
					for ( final AbstractStoryPatternObject spo : patternPart.getStoryPatternObjects() ) {
						if ( spoName.equals( spo.getName() ) ) {
							for ( final AbstractStoryPatternLink link : spo.getIncomingStoryLinks() ) {
								final StoryPatternLink spoLink = (StoryPatternLink) link;
								
								if ( feature == spoLink.getEStructuralFeature() ) {
									if ( spoLink.getLinkPositionConstraint() != null ) {
										return spoLink.getLinkPositionConstraint().getConstraintType();
									}
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doCreateLink() {
		final EObject sourceEObject = (EObject) patternMatcher.getInstanceObject(this.storyPatternLink.getSource());
		final Object targetObject = patternMatcher.getInstanceObject(this.storyPatternLink.getTarget());
		final IInstanceFacade<?, ?, EStructuralFeature, ?> instanceFacede = (IInstanceFacade<?, ?, EStructuralFeature, ?>) patternMatcher.getFacadeFactory().getInstanceFacade();
		final EStructuralFeature actualFeatureToSet = instanceFacede.getConcreteFeature( sourceEObject, storyPatternLink.getEStructuralFeature(), targetObject );
		
		if ( actualFeatureToSet != null ) {
			// DB: Assert moved from constructor.
			assert ((storyPatternLink.getModifier() != StoryPatternModifierEnumeration.CREATE) && (storyPatternLink.getModifier() != StoryPatternModifierEnumeration.DESTROY))
			|| actualFeatureToSet.isChangeable();
	
			if ( !actualFeatureToSet.isMany() ) {
				assert this.storyPatternLink.getLinkPositionConstraint() == null;
	
				/*
				 * Do not create instance links for optional create link that
				 * already exist.
				 */
				if ( 	storyPatternLink.getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL ||
						sourceEObject.eGet(this.storyPatternLink.getEStructuralFeature()) != targetObject ) {
					sourceEObject.eSet( actualFeatureToSet, targetObject );
				}
			}
			else {
				final List<Object> objects = (List<Object>) sourceEObject.eGet( actualFeatureToSet );
	
				/*
				 * Do not create instance links for optional create link that
				 * already exist.
				 */
				if ( storyPatternLink.getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL || !objects.contains( targetObject) ) {
					// If there is a link position constraint, insert the element at the front or end of the list
					if ( storyPatternLink.getLinkPositionConstraint() != null ) {
						switch ( storyPatternLink.getLinkPositionConstraint().getConstraintType() ) {
							case FIRST:	{
								objects.add( 0, targetObject );
								
								break;
							}
							case MIDDLE: {
								if ( objects.isEmpty() ) {
									objects.add( targetObject );
								}
								else if ( objects.size() == 1 ) {
									// Determine if this is the first or last element
									final LinkPositionConstraintEnumeration positionConstraint = positionConstraint( objects.get( 0 ) );
									
									if ( positionConstraint == LinkPositionConstraintEnumeration.LAST ) {
										objects.add( 0, targetObject );
									}
									else {
										objects.add( targetObject );
									}
								}
								else {
									objects.add( 1, targetObject );
								}
								
								break;
							}
							case LAST: {
								objects.add( targetObject );
								
								break;
							}
						}
					}
					else
					{
						boolean done = false;
	
						/*
						 * If there are incoming link order constraints, pick one
						 * that is connected to a link with an existing target
						 * object and insert the current target object after it.
						 */
						for (final LinkOrderConstraint loc : this.storyPatternLink.getIncomingLinkOrderConstraints())
						{
							assert loc.getConstraintType() == LinkOrderConstraintEnumeration.DIRECT_SUCCESSOR;
	
							final Variable<EClassifier> variable = this.patternMatcher.getVariablesScope().getVariable(
									loc.getPredecessorLink().getTarget().getName());
	
							if (variable != null)
							{
								objects.add(objects.indexOf(variable.getValue()) + 1, targetObject);
	
								done = true;
	
								break;
							}
						}
	
						if (!done)
						{
							/*
							 * If there are outgoing link order constraints, pick
							 * one that is connected to a link with an existing
							 * target object and insert the current target object
							 * before it.
							 */
							for (final LinkOrderConstraint loc : this.storyPatternLink.getOutgoingLinkOrderConstraints())
							{
								assert loc.getConstraintType() == LinkOrderConstraintEnumeration.DIRECT_SUCCESSOR;
	
								final Variable<EClassifier> variable = this.patternMatcher.getVariablesScope().getVariable(
										loc.getSuccessorLink().getTarget().getName());
	
								if (variable != null)
								{
									objects.add(objects.indexOf(variable.getValue()), targetObject);
	
									done = true;
	
									break;
								}
							}
						}
	
						if (!done)
						{
							/*
							 * Simply insert the target object at the end.
							 */
							objects.add(targetObject);
						}
					}
				}
			}
	
			patternMatcher.getNotificationEmitter().instanceLinkCreated(storyPatternLink.getSource(), sourceEObject,
					storyPatternLink, storyPatternLink.getTarget(), targetObject, patternMatcher.getVariablesScope(),
					patternMatcher);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void doDestroyLink(final Map<AbstractStoryPatternObject, Object> deletedObjects)
	{
		assert this.storyPatternLink.getModifier() == StoryPatternModifierEnumeration.DESTROY;

		final AbstractStoryPatternObject sourceSpo = this.storyPatternLink.getSource();
		final AbstractStoryPatternObject targetSpo = this.storyPatternLink.getTarget();

		assert (deletedObjects.get(sourceSpo) == null) || (deletedObjects.get(sourceSpo) instanceof EObject);

		EObject sourceInstanceObject = (EObject) deletedObjects.get(sourceSpo);
		Object targetInstanceObject = deletedObjects.get(targetSpo);

		if (sourceInstanceObject == null)
		{
			/*
			 * The source spo was not destroyed
			 */
			final Variable<EClassifier> sourceVariable = this.patternMatcher.getVariablesScope().getVariable(sourceSpo.getName());

			if (sourceVariable != null)
			{
				assert sourceSpo.getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL;
				assert sourceVariable.getValue() instanceof EObject;

				sourceInstanceObject = (EObject) sourceVariable.getValue();
			}
		}

		if (targetInstanceObject == null)
		{
			/*
			 * The target spo was not destroyed.
			 */
			final Variable<EClassifier> targetVariable = this.patternMatcher.getVariablesScope().getVariable(targetSpo.getName());

			if (targetVariable != null)
			{
				assert targetSpo.getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL;

				targetInstanceObject = targetVariable.getValue();
			}
		}

		if ((sourceInstanceObject != null) && (targetInstanceObject != null))
		{
			final IInstanceFacade<?, ?, EStructuralFeature, ?> instanceFacede = (IInstanceFacade<?, ?, EStructuralFeature, ?>) patternMatcher.getFacadeFactory().getInstanceFacade();
			final EStructuralFeature actualFeatureToSet = instanceFacede.getConcreteFeature( sourceInstanceObject, storyPatternLink.getEStructuralFeature(), targetInstanceObject );

			if ( actualFeatureToSet != null ) {
				// DB: Assert moved from constructor.
				assert ((storyPatternLink.getModifier() != StoryPatternModifierEnumeration.CREATE) && (storyPatternLink.getModifier() != StoryPatternModifierEnumeration.DESTROY))
				|| actualFeatureToSet.isChangeable();
	
				/*
				 * Delete the link
				 */
				if ( !storyPatternLink.getEStructuralFeature().isMany() ) {
					sourceInstanceObject.eSet( actualFeatureToSet, null );
				}
				else {
					boolean delete = true;
	
					if ( storyPatternLink.getMatchType() == StoryPatternMatchTypeEnumeration.OPTIONAL ) {
						final List<Object> targetObjects = (List<Object>) sourceInstanceObject.eGet( actualFeatureToSet );
	
						/*
						 * Only destroy the link if link constraints are satisfied.
						 */
						if ( storyPatternLink.getLinkPositionConstraint() != null ) {
							switch ( storyPatternLink.getLinkPositionConstraint().getConstraintType() ) {
								case FIRST:	{
									delete = targetObjects.get(0) == targetInstanceObject;
									
									break;
								}

								case MIDDLE: {
									if ( targetObjects.size() > 2 ) {
										delete = false;
										
										for ( int index = 1; index < targetObjects.size() - 1; index++ ) {
											if ( targetObjects.get( index ) == targetInstanceObject ) {
												delete = true;
												
												break;
											}
										}
									}
									else {
										delete = false;
									}
									
									break;
								}
	
								case LAST: {
									delete = targetObjects.get(targetObjects.size() - 1) == targetInstanceObject;
									break;
								}
	
								default:
									throw new UnsupportedOperationException();
							}
						}
	
						if ( delete )	{
							final int targetIndex = targetObjects.indexOf(targetInstanceObject);
	
							/*
							 * Check all incoming link order constraints coming in
							 * from a link with a bound target object.
							 */
							for ( final LinkOrderConstraint loc : storyPatternLink.getIncomingLinkOrderConstraints() )	{
								if ( patternMatcher.isBound( loc.getPredecessorLink().getTarget() ) ) {
									final Variable<EClassifier> variable = patternMatcher.getVariablesScope().getVariable( loc.getPredecessorLink().getTarget().getName() );
									Object instanceObject = null;
	
									if (variable == null) {
										instanceObject = deletedObjects.get( loc.getPredecessorLink().getTarget() );
									}
	
									assert instanceObject != null;
	
									switch (loc.getConstraintType())
									{
										case DIRECT_SUCCESSOR:
										{
											if ((targetIndex == 0) || (targetObjects.get(targetIndex - 1) != instanceObject))
											{
												delete = false;
											}
	
											break;
										}
										case SUCCESSOR:
										{
											if (targetObjects.indexOf(instanceObject) >= targetIndex)
											{
												delete = false;
											}
	
											break;
										}
										default:
											throw new UnsupportedOperationException();
									}
								}
	
								if (!delete)
								{
									break;
								}
							}
	
							if (delete)
							{
								/*
								 * Check all outgoing link order constraints going
								 * out to a link with a bound target object.
								 */
								for (final LinkOrderConstraint loc : this.storyPatternLink.getOutgoingLinkOrderConstraints())
								{
									if (this.patternMatcher.isBound(loc.getSuccessorLink().getTarget()))
									{
										final Variable<EClassifier> variable = this.patternMatcher.getVariablesScope().getVariable(
												loc.getSuccessorLink().getTarget().getName());
	
										Object instanceObject = null;
	
										if (variable == null)
										{
											instanceObject = deletedObjects.get(loc.getPredecessorLink().getTarget());
										}
	
										assert instanceObject != null;
	
										switch (loc.getConstraintType())
										{
											case DIRECT_SUCCESSOR:
											{
												if ((targetIndex == 0) || (targetObjects.get(targetIndex + 1) != instanceObject))
												{
													delete = false;
												}
	
												break;
											}
											case SUCCESSOR:
											{
												if (targetObjects.indexOf(instanceObject) <= targetIndex)
												{
													delete = false;
												}
	
												break;
											}
											default:
												throw new UnsupportedOperationException();
										}
									}
	
									if (!delete)
									{
										break;
									}
								}
							}
						}
					}
	
					if (delete)
					{												// DB:
						((Collection<Object>) sourceInstanceObject.eGet(actualFeatureToSet/*this.storyPatternLink.getEStructuralFeature()*/))
								.remove(targetInstanceObject);
					}
				}
	
				this.patternMatcher.getNotificationEmitter().instanceLinkDestroyed(sourceSpo, sourceInstanceObject, this.storyPatternLink,
						targetSpo, targetInstanceObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);
			}
		}
	}
//	
//	private int calculateActualMatchSize( 	final AbstractStoryPatternObject p_spo,
//											final Collection<? extends Object> p_objects )
//	throws SDMException {
//		//return p_objects.size();
//		int realSize = 0;
//		final EClassifier spoClassifier = patternMatcher.getFacadeFactory().getStoryPatternObjectFacade().getClassifier( p_spo );
//		
//		for ( final Object object : p_objects ) {
//			if ( patternMatcher.checkTypeConformance( spoClassifier, object ) && patternMatcher.checkStoryPatternObjectConstraints( p_spo, object ) ) {
//				realSize++;
//			}
//		}
//		
//		return realSize;
//	}

	@SuppressWarnings("unchecked")
	@Override
	public int calculateMatchingCost()
	throws SDMException {
		final AbstractStoryPatternObject sourceSpo = storyPatternLink.getSource();
		final AbstractStoryPatternObject targetSpo = storyPatternLink.getTarget();

		assert !( patternMatcher.isBound( sourceSpo ) && patternMatcher.isBound( targetSpo ) );

		if ( patternMatcher.isBound(sourceSpo) ) {
			if (!facade.isMany( storyPatternLink ) || ( storyPatternLink.getLinkPositionConstraint() != null ) ) {
				return 	1 +
						getNumberOfIncomingUnidirectionalLinks( storyPatternLink.getTarget() );
			}
			
			final Variable<EClassifier> sourceVariable = patternMatcher.getVariablesScope().getVariable(sourceSpo.getName());

			assert sourceVariable.getValue() instanceof EObject;

			final EObject sourceEObject = (EObject) sourceVariable.getValue();
				
			try {
				final Collection<Object> objects = (Collection<Object>) facade.getFeatureValue( storyPatternLink,
																								sourceEObject,
																								patternMatcher.getExpressionInterpreterManager(),
																								patternMatcher.getVariablesScope() );
				final int size = objects.size();//calculateActualMatchSize( targetSpo, objects );
				
				return 	size /*objects.size()*/ * facade.matchingCostPenalty( storyPatternLink ) +
						storyPatternLink.getIncomingLinkOrderConstraints().size() +
						getNumberOfIncomingUnidirectionalLinks( storyPatternLink.getTarget() );
			}
			catch( final SDMException p_ex ) {
				throw new RuntimeException( p_ex );
			}
		}
		
		if ( patternMatcher.isBound( targetSpo ) && facade.isReference( storyPatternLink ) ) {
			if ( facade.isContainment( storyPatternLink ) ) {
				/*
				 * We can get the direct container by calling eContainer()
				 * on the target object.
				 */
				return 	2 +
						storyPatternLink.getIncomingLinkOrderConstraints().size() + 
						getNumberOfIncomingUnidirectionalLinks(this.storyPatternLink.getSource());
			}
			
			if ( facade.hasReverseReference( storyPatternLink ) ) {
				if ( !facade.reverseReferenceIsMany( storyPatternLink ) || ( storyPatternLink.getLinkPositionConstraint() != null))	{
					return 	1 +
							getNumberOfIncomingUnidirectionalLinks( storyPatternLink.getSource() );
				}

				final Variable<EClassifier> targetVariable = patternMatcher.getVariablesScope().getVariable( targetSpo.getName() );

				assert targetVariable.getValue() instanceof EObject;

				final EObject targetEObject = (EObject) targetVariable.getValue();
				final Collection<EObject> revRefValues = (Collection<EObject>) facade.getReverseFeatureValue( storyPatternLink, targetEObject );
				final int size = revRefValues.size(); //calculateActualMatchSize( sourceSpo, revRefValues );
				
				return 	size /*revRefValues.size()*/ * facade.matchingCostPenalty( storyPatternLink ) +
						storyPatternLink.getIncomingLinkOrderConstraints().size() +
						getNumberOfIncomingUnidirectionalLinks( storyPatternLink.getSource() );
			}
		}

		return PatternPart.MATCHING_NOT_POSSIBLE;
	}

	@Override
	public EMatchType doGetMatchType() {
		switch (this.storyPatternLink.getModifier())
		{
			case NONE:
				switch (this.storyPatternLink.getMatchType())
				{
					case NONE:
						return EMatchType.MANDATORY;
					case OPTIONAL:
						return EMatchType.OPTIONAL;
					case NEGATIVE:
						throw new UnsupportedOperationException();
					default:
						throw new UnsupportedOperationException();
				}
			case CREATE:
				switch (this.storyPatternLink.getMatchType())
				{
					case NONE:
						return EMatchType.OPTIONAL;
					case OPTIONAL:
						return EMatchType.OPTIONAL;
					case NEGATIVE:
						throw new UnsupportedOperationException();
					default:
						throw new UnsupportedOperationException();
				}
			case DESTROY:
				switch (this.storyPatternLink.getMatchType())
				{
					case NONE:
						return EMatchType.MANDATORY;
					case OPTIONAL:
						return EMatchType.OPTIONAL;
					case NEGATIVE:
						throw new UnsupportedOperationException();
					default:
						throw new UnsupportedOperationException();
				}
			default:
				throw new UnsupportedOperationException();
		}
	}
}
