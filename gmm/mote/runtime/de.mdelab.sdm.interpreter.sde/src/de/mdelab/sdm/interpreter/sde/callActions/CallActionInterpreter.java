package de.mdelab.sdm.interpreter.sde.callActions;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.callActions.AbstractComparator;
import de.hpi.sam.storyDiagramEcore.callActions.CallAction;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionParameter;
import de.hpi.sam.storyDiagramEcore.callActions.CallActionsPackage;
import de.hpi.sam.storyDiagramEcore.callActions.CallStoryDiagramInterpreterAction;
import de.hpi.sam.storyDiagramEcore.callActions.CompareAction;
import de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction;
import de.hpi.sam.storyDiagramEcore.callActions.LiteralDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.MethodCallAction;
import de.hpi.sam.storyDiagramEcore.callActions.NewObjectAction;
import de.hpi.sam.storyDiagramEcore.callActions.NullValueAction;
import de.hpi.sam.storyDiagramEcore.callActions.OperationAction;
import de.hpi.sam.storyDiagramEcore.callActions.Operators;
import de.hpi.sam.storyDiagramEcore.callActions.VariableDeclarationAction;
import de.hpi.sam.storyDiagramEcore.callActions.VariableReferenceAction;
import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.SDMInterpreterConstants;
import de.mdelab.sdm.interpreter.core.expressions.ExpressionInterpreter;
import de.mdelab.sdm.interpreter.core.expressions.ExpressionInterpreterManager;
import de.mdelab.sdm.interpreter.core.facade.MetamodelFacadeFactory;
import de.mdelab.sdm.interpreter.core.notifications.NotificationEmitter;
import de.mdelab.sdm.interpreter.core.variables.Variable;
import de.mdelab.sdm.interpreter.core.variables.VariablesScope;
import de.mdelab.sdm.interpreter.sde.SDESDMInterpreter;

/**
 * The CallActionInterpreter executes CallActions, an expression language
 * without a concrete textual syntax. Its main purpose is to allow calling Java
 * methods and other story diagrams from a story diagram.
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class CallActionInterpreter extends ExpressionInterpreter<Expression, EClassifier>
{

	@Override
	public Variable<EClassifier> evaluateExpression(Expression expression, EClassifier contextClassifier, Object contextInstance,
			VariablesScope<?, ?, ?, ?, ?, ?, EClassifier, ?, Expression> variablesScope) throws SDMException
	{
		assert expression instanceof CallActionExpression : "The CallActionInterpreter can only evaluate CallActionExpressions, but not '"
				+ expression.eClass() + "'.";

		Variable<EClassifier> variable = null;

		for (CallAction callAction : ((CallActionExpression) expression).getCallActions())
		{
			EClass callActionEClass = callAction.eClass();

			if (callActionEClass == CallActionsPackage.Literals.LITERAL_DECLARATION_ACTION)
			{
				variable = this.evaluate((LiteralDeclarationAction) callAction, contextClassifier, contextInstance, variablesScope);
			}
			else if (callActionEClass == CallActionsPackage.Literals.CALL_STORY_DIAGRAM_INTERPRETER_ACTION)
			{
				variable = this
						.evaluate((CallStoryDiagramInterpreterAction) callAction, contextClassifier, contextInstance, variablesScope);
			}
			else if (callActionEClass == CallActionsPackage.Literals.NEW_OBJECT_ACTION)
			{
				variable = this.evaluate((NewObjectAction) callAction, contextClassifier, contextInstance, variablesScope);
			}
			else if (callActionEClass == CallActionsPackage.Literals.VARIABLE_DECLARATION_ACTION)
			{
				variable = this.evaluate((VariableDeclarationAction) callAction, contextClassifier, contextInstance, variablesScope);
			}
			else if (callActionEClass == CallActionsPackage.Literals.OPERATION_ACTION)
			{
				variable = this.evaluate((OperationAction) callAction, contextClassifier, contextInstance, variablesScope);
			}
			else if (callActionEClass == CallActionsPackage.Literals.NULL_VALUE_ACTION)
			{
				variable = this.evaluate((NullValueAction) callAction, contextClassifier, contextInstance, variablesScope);
			}
			else if (callActionEClass == CallActionsPackage.Literals.METHOD_CALL_ACTION)
			{
				variable = this.evaluate((MethodCallAction) callAction, contextClassifier, contextInstance, variablesScope);
			}
			// DB: Added
			else if (callActionEClass == CallActionsPackage.Literals.GET_PROPERTY_VALUE_ACTION ) {
				variable = evaluate((GetPropertyValueAction) callAction, contextClassifier, contextInstance, variablesScope);
			}
			else if (callActionEClass == CallActionsPackage.Literals.VARIABLE_REFERENCE_ACTION)
			{
				variable = this.evaluate((VariableReferenceAction) callAction, contextClassifier, contextInstance, variablesScope);
			}
			else if (callActionEClass == CallActionsPackage.Literals.COMPARE_ACTION)
			{
				variable = this.evaluate((CompareAction) callAction, contextClassifier, contextInstance, variablesScope);
			}
			else
			{
				throw new UnsupportedOperationException();
			}
		}

		if (variable == null)
		{
			throw new UnsupportedOperationException();
		}
		else
		{
			return variable;
		}
	}

	protected Variable<EClassifier> evaluate(CompareAction callAction, EClassifier contextClassifier, Object contextInstance,
			VariablesScope<?, ?, ?, ?, ?, ?, EClassifier, ?, Expression> variablesScope) throws SDMException
	{
		Object o1 = this.getExpressionInterpreterManager()
				.evaluateExpression(callAction.getExpression1(), contextClassifier, contextInstance, variablesScope).getValue();
		Object o2 = this.getExpressionInterpreterManager()
				.evaluateExpression(callAction.getExpression2(), contextClassifier, contextInstance, variablesScope).getValue();

		final boolean value;

		// DB: Handling custom comparator
		final AbstractComparator comparator = callAction.getComparator();
		
		if ( comparator == null ) {
			if (o1 == o2) {
				value = true;
			}
			else {
				value = o1 != null && o1.equals(o2);
			}
		}
		else {
			value = comparator.equals( o1, o2 );
		}

		return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, EcorePackage.Literals.EBOOLEAN, value);
	}

	protected Variable<EClassifier> evaluate(VariableReferenceAction callAction, EClassifier contextClassifier, Object contextInstance,
			VariablesScope<?, ?, ?, ?, ?, ?, EClassifier, ?, Expression> variablesScope) throws SDMException
	{
		Variable<EClassifier> variable = variablesScope.getVariable(callAction.getVariableName());

		if (variable != null)
		{
			assert variable.getClassifier() == callAction.getClassifier() || variable.getClassifier() instanceof EClass
					&& callAction.getClassifier() instanceof EClass
					&& ((EClass) callAction.getClassifier()).isSuperTypeOf((EClass) variable.getClassifier());

			return variable;
		}
		else
		{
			throw new SDMException("The variable " + callAction.getVariableName() + " is undefined.");
		}
	}

	protected Variable<EClassifier> evaluate(MethodCallAction callAction, EClassifier contextClassifier, Object contextInstance,
			VariablesScope<?, ?, ?, ?, ?, ?, EClassifier, ?, Expression> variablesScope) throws SDMException
	{
		CallActionParameter[] parameters = callAction.getParameters().toArray(new CallActionParameter[] {});

		// Create array of parameter types and parameters
		Class<?>[] parameterTypes = new Class<?>[parameters.length];
		Object[] parameterObjects = new Object[parameters.length];

		try
		{
			for (int i = 0; i < parameters.length; i++)
			{
				parameterObjects[i] = this.getExpressionInterpreterManager()
						.evaluateExpression(parameters[i].getParameterValueAction(), contextClassifier, contextInstance, variablesScope)
						.getValue();

				parameterTypes[i] = parameters[i].getParameterClassfier().getInstanceClass();

				if (parameterTypes[i] == null)
				{
					throw new SDMException("Instance class of '" + parameters[i].getParameterClassfier() + "' is null.");
				}
			}

			// Get class and method
			Class<?> c;
			Method m;

			Object methodInstanceObject = null;

			// static method call
			if (callAction.getInstanceVariable() == null)
			{
				c = Class.forName(callAction.getMethodClassName(), true, this.getExpressionInterpreterManager().getClassLoader());
				m = c.getMethod(callAction.getMethodName(), parameterTypes);
			}
			else
			{
				// dynamic method call

				methodInstanceObject = this.getExpressionInterpreterManager()
						.evaluateExpression(callAction.getInstanceVariable(), contextClassifier, contextInstance, variablesScope)
						.getValue();

				if (methodInstanceObject != null)
				{
					c = methodInstanceObject.getClass();
				}
				else
				{
					throw new NullPointerException("methodInstanceObject expression '" + callAction.getInstanceVariable()
							+ "'evaluated to null");
				}

				if (callAction.getMethod() != null)
				{
					m = c.getMethod(callAction.getMethod().getName(), parameterTypes);
				}
				else
				{
					m = c.getMethod(callAction.getMethodName(), parameterTypes);
				}
			}

			// Invoke method
			Object returnValue = m.invoke(methodInstanceObject, parameterObjects);

			if (callAction.getClassifier() == null)
			{
				/*
				 * A method without return value
				 */
				if (returnValue != null)
				{
					throw new SDMException("MethodCallAction '" + callAction + "' has no classifier but the called method returned '"
							+ returnValue + "'.");
				}

				return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, EcorePackage.Literals.EJAVA_OBJECT, null);
			}
			else
			{
				return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, callAction.getClassifier(), returnValue);
			}

		}
		catch (Throwable e)
		{
			throw new SDMException("Error executing method call action: " + callAction, e);
		}
	}

	protected Variable<EClassifier> evaluate(	final GetPropertyValueAction p_action, 
												final EClassifier p_contextClassifier,
												final Object p_contextInstance,
												final VariablesScope<?, ?, ?, ?, ?, ?, EClassifier, ?, Expression> p_variablesScope )
	throws SDMException {
		final EObject instanceObject;
		final Expression instanceVar = p_action.getInstanceVariable();
		
		
		if ( instanceVar == null ) {
			instanceObject = (EObject) p_contextInstance;
		}
		else {
			instanceObject = (EObject) getExpressionInterpreterManager().evaluateExpression( 	instanceVar,
																								p_contextClassifier,
																								p_contextInstance,
																								p_variablesScope ).getValue();
		}
		
		// Invoke method
		final EStructuralFeature feature = p_action.getProperty();
		final Object returnValue = instanceObject.eGet( feature );
		
		return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, feature.getEType(), returnValue );
	}

	protected Variable<EClassifier> evaluate(NullValueAction callAction, EClassifier contextClassifier, Object contextInstance,
			VariablesScope<?, ?, ?, ?, ?, ?, EClassifier, ?, Expression> variablesScope)
	{
		return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, callAction.getClassifier(), null);
	}

	protected Variable<EClassifier> evaluate(OperationAction callAction, EClassifier contextClassifier, Object contextInstance,
			VariablesScope<?, ?, ?, ?, ?, ?, EClassifier, ?, Expression> variablesScope) throws SDMException
	{
		Variable<EClassifier> v1 = this.getExpressionInterpreterManager().evaluateExpression(callAction.getOperand1(), contextClassifier,
				contextInstance, variablesScope);
		Object o1 = v1.getValue();

		Variable<EClassifier> v2 = null;
		Object o2 = null;

		if (callAction.getOperand2() != null)
		{
			v2 = this.getExpressionInterpreterManager().evaluateExpression(callAction.getOperand2(), contextClassifier, contextInstance,
					variablesScope);
			o2 = v2.getValue();
		}
		else if (callAction.getOperator() != Operators.NOT)
		{
			throw new SDMException("Error executing operation action: " + callAction + " (" + callAction.getOperator()
					+ " needs two operands)");
		}

		switch (callAction.getOperator())
		{
			case EQUALS:
			case NOT_EQUAL:
			case GREATER_OR_EQUALS:
			case GREATER_THAN:
			case LESS_OR_EQUALS:
			case LESS_THAN:
			{
				boolean value;
				if (o1 == null || o2 == null)
				{
					value = o1 == o2;
				}
				else if (o1 instanceof Comparable<?> && o2 instanceof Comparable<?> && o1.getClass().equals(o2.getClass()))
				{
					@SuppressWarnings(
					{
							"unchecked", "rawtypes"
					})
					int result = ((Comparable) o1).compareTo(o2);
					switch (callAction.getOperator())
					{
						case EQUALS:
							value = result == 0 ? true : false;
							break;
						case GREATER_OR_EQUALS:
							value = result >= 0 ? true : false;
							break;
						case GREATER_THAN:
							value = result > 0 ? true : false;
							break;
						case LESS_OR_EQUALS:
							value = result <= 0 ? true : false;
							break;
						case LESS_THAN:
							value = result < 0 ? true : false;
							break;
						case NOT_EQUAL:
							value = result != 0 ? true : false;
							break;
						default:
							throw new UnsupportedOperationException();
					}
				}
				else if (o1 instanceof Number && o2 instanceof Number)
				{
					switch (callAction.getOperator())
					{
						case EQUALS:
							value = ((Number) o1).doubleValue() == ((Number) o2).doubleValue();
							break;
						case GREATER_OR_EQUALS:
							value = ((Number) o1).doubleValue() >= ((Number) o2).doubleValue();
							break;
						case GREATER_THAN:
							value = ((Number) o1).doubleValue() > ((Number) o2).doubleValue();
							break;
						case LESS_OR_EQUALS:
							value = ((Number) o1).doubleValue() <= ((Number) o2).doubleValue();
							break;
						case LESS_THAN:
							value = ((Number) o1).doubleValue() < ((Number) o2).doubleValue();
							break;
						case NOT_EQUAL:
							value = ((Number) o1).doubleValue() != ((Number) o2).doubleValue();
							break;
						default:
							throw new UnsupportedOperationException();
					}
				}
				else if (callAction.getOperator() == Operators.EQUALS)
				{
					value = o1 == o2;
				}
				else if (callAction.getOperator() == Operators.NOT_EQUAL)
				{
					value = o1 != o2;
				}
				else
				{
					throw new SDMException(
							"Error executing operation action: "
									+ callAction
									+ " (If objects need to be compared, they must implement java.lang.Comparable, or only '=' and '!=' comparisons are possible.)");
				}

				return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, EcorePackage.Literals.EBOOLEAN, value);
			}
			case AND:
			case OR:
			{
				if (o1 instanceof Boolean && o2 instanceof Boolean)
				{
					boolean value;
					Boolean b1 = (Boolean) o1;
					Boolean b2 = (Boolean) o2;

					switch (callAction.getOperator())
					{
						case AND:
							value = b1 && b2;
							break;
						case OR:
							value = b1 || b2;
							break;
						default:
							throw new UnsupportedOperationException();
					}

					return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, EcorePackage.Literals.EBOOLEAN, value);
				}
				else
				{
					throw new SDMException("Error executing operation action: " + callAction
							+ " (Logical operators may only be applied to type Boolean)");
				}
			}
			case NOT:
				if (o1 instanceof Boolean)
				{
					return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, EcorePackage.Literals.EBOOLEAN,
							!(Boolean) o1);
				}
				else
				{
					throw new SDMException("Error executing operation action: " + callAction
							+ " (Logical operators may only be applied to type Boolean)");
				}

			case ADD:
				if (o1 instanceof String && o2 instanceof String)
				{
					return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, EcorePackage.Literals.ESTRING, (String) o1
							+ (String) o2);
				}
			case DIVIDE:
			case MODULO:
			case MULTIPLY:
			case SUBTRACT:
			{
				if (o1 instanceof Number && o2 instanceof Number)
				{

					BigDecimal bd1 = new BigDecimal(o1.toString());
					BigDecimal bd2 = new BigDecimal(o2.toString());

					BigDecimal result;
					switch (callAction.getOperator())
					{
						case ADD:
							result = bd1.add(bd2);
							break;
						case SUBTRACT:
							result = bd1.subtract(bd2);
							break;
						case MULTIPLY:
							result = bd1.multiply(bd2);
							break;
						case DIVIDE:
							result = bd1.divide(bd2, RoundingMode.HALF_UP);
							break;
						case MODULO:
							result = bd1.remainder(bd2);
							break;
						default:
							throw new UnsupportedOperationException();
					}

					EClassifier classifier = callAction.getClassifier();

					Object finalResult;

					if (classifier == EcorePackage.Literals.EBYTE || classifier == EcorePackage.Literals.EBYTE_OBJECT)
					{
						finalResult = result.byteValue();
					}
					else if (classifier == EcorePackage.Literals.EINT || classifier == EcorePackage.Literals.EINTEGER_OBJECT)
					{
						finalResult = result.intValue();
					}
					else if (classifier == EcorePackage.Literals.EDOUBLE || classifier == EcorePackage.Literals.EDOUBLE_OBJECT)
					{
						finalResult = result.doubleValue();
					}
					else if (classifier == EcorePackage.Literals.EFLOAT || classifier == EcorePackage.Literals.EFLOAT_OBJECT)
					{
						finalResult = result.floatValue();
					}
					else if (classifier == EcorePackage.Literals.ELONG || classifier == EcorePackage.Literals.ELONG_OBJECT)
					{
						finalResult = result.longValue();
					}
					else if (classifier == EcorePackage.Literals.ESHORT || classifier == EcorePackage.Literals.ESHORT_OBJECT)
					{
						finalResult = result.shortValue();
					}
					else
					{
						throw new UnsupportedOperationException();
					}

					return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, classifier, finalResult);
				}
				else
				{
					throw new UnsupportedOperationException();
				}
			}
			default:
				throw new UnsupportedOperationException();
		}
	}

	protected Variable<EClassifier> evaluate(VariableDeclarationAction callAction, EClassifier contextClassifier, Object contextInstance,
			VariablesScope<?, ?, ?, ?, ?, ?, EClassifier, ?, Expression> variablesScope) throws SDMException
	{
		assert callAction.getValueAssignment() != null : "The value assignment is null in " + callAction + ".";

		// Evaluate the assignment;
		Object value = this.getExpressionInterpreterManager()
				.evaluateExpression(callAction.getValueAssignment(), contextClassifier, contextInstance, variablesScope).getValue();

		/*
		 * Check that the types are compatible. Null is considered compatible to
		 * all types.
		 */
		if (value != null && !callAction.getClassifier().isInstance(value))
		{
			throw new SDMException(value + " is not an instance of " + callAction.getClassifier().getName());
		}

		Variable<EClassifier> variable = variablesScope.createVariable(callAction.getVariableName(), callAction.getClassifier(), value);

		return variable;
	}

	protected Variable<EClassifier> evaluate(LiteralDeclarationAction callAction, EClassifier contextClassifier, Object contextInstance,
			VariablesScope<?, ?, ?, ?, ?, ?, EClassifier, ?, Expression> variablesScope) throws SDMException
	{
		LiteralDeclarationAction literalDeclarationAction = callAction;

		/*
		 * Let EcoreFactory create an Object of the appropriate primitive type
		 */
		if (literalDeclarationAction.getClassifier() instanceof EDataType)
		{
			EDataType eDataType = (EDataType) literalDeclarationAction.getClassifier();

			return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, eDataType, eDataType.getEPackage()
					.getEFactoryInstance().createFromString(eDataType, literalDeclarationAction.getLiteral()));
		}
		else
		{
			throw new SDMException(literalDeclarationAction.getClassifier() + " must be an EDataType.");
		}
	}

	protected Variable<EClassifier> evaluate(CallStoryDiagramInterpreterAction callAction, EClassifier contextClassifier,
			Object contextInstance, VariablesScope<?, ?, ?, ?, ?, ?, EClassifier, ?, Expression> variablesScope) throws SDMException
	{
		@SuppressWarnings("unchecked")
		final SDESDMInterpreter sdi = new SDESDMInterpreter( (MetamodelFacadeFactory<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>) getExpressionInterpreterManager().getFacadeFactory(),
				(ExpressionInterpreterManager<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>) this
						.getExpressionInterpreterManager(),
				(NotificationEmitter<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>) this
						.getExpressionInterpreterManager().getNotificationEmitter());

		/*
		 * Create list of variable bindings, first the THIS object, then the
		 * other parameters.
		 */
		List<Variable<EClassifier>> parameters = new LinkedList<Variable<EClassifier>>();

		for (CallActionParameter parameter : callAction.getParameters())
		{
			Variable<EClassifier> variable = new Variable<EClassifier>(parameter.getName(), parameter.getParameterClassfier(), this
					.getExpressionInterpreterManager()
					.evaluateExpression(parameter.getParameterValueAction(), contextClassifier, contextInstance, variablesScope).getValue());

			parameters.add(variable);
		}

		Map<String, Variable<EClassifier>> returnValues = sdi.executeActivity(callAction.getActivity(), parameters);

		Variable<EClassifier> returnVariable = returnValues.get(SDMInterpreterConstants.RETURN_VALUE_VAR_NAME);

		if (returnVariable != null)
		{
			returnVariable = new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, callAction.getClassifier(),
					returnVariable.getValue());
		}
		else if (callAction.getClassifier() != null)
		{
			returnVariable = new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, callAction.getClassifier(), null);
		}
		else
		{
			returnVariable = new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, EcorePackage.Literals.EJAVA_OBJECT, null);
		}

		return returnVariable;
	}

	protected Variable<EClassifier> evaluate(NewObjectAction callAction, EClassifier contextClassifier, Object contextInstance,
			VariablesScope<?, ?, ?, ?, ?, ?, EClassifier, ?, Expression> variablesScope) throws SDMException
	{
		if (callAction.getClassifier().eClass() == EcorePackage.Literals.ECLASS)
		{
			return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, callAction.getClassifier(), callAction
					.getClassifier().getEPackage().getEFactoryInstance().create((EClass) callAction.getClassifier()));
		}
		else if (callAction.getClassifier().eClass() == EcorePackage.Literals.EDATA_TYPE)
		{
			try
			{
				CallActionParameter[] parameters = callAction.getConstructorParameters().toArray(new CallActionParameter[] {});

				// Create array of parameter types and parameters
				Class<?>[] parameterTypes = new Class<?>[parameters.length];
				Object[] parameterObjects = new Object[parameters.length];

				for (int i = 0; i < parameters.length; i++)
				{
					parameterObjects[i] = this
							.getExpressionInterpreterManager()
							.evaluateExpression(parameters[i].getParameterValueAction(), contextClassifier, contextInstance, variablesScope)
							.getValue();

					parameterTypes[i] = parameters[i].getParameterClassfier().getInstanceClass();
				}

				Class<?> c = Class.forName(callAction.getClassifier().getInstanceTypeName(), true, this.getExpressionInterpreterManager()
						.getClassLoader());

				Constructor<?> constructor = c.getConstructor(parameterTypes);

				return new Variable<EClassifier>(SDMInterpreterConstants.INTERNAL_VAR_NAME, callAction.getClassifier(),
						constructor.newInstance(parameterObjects));
			}
			catch (Exception ex)
			{
				throw new SDMException("Error while creating new object: " + callAction, ex);
			}
		}
		else
		{
			throw new UnsupportedOperationException();
		}
	}

}
