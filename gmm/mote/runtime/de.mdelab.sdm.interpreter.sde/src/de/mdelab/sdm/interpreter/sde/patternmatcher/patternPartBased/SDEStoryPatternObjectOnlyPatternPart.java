package de.mdelab.sdm.interpreter.sde.patternmatcher.patternPartBased;

import java.util.Map;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.ECheckResult;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.EMatchType;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.MatchState;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPartBasedMatcher;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEStoryPatternObjectOnlyPatternPart extends SDEPatternPart<AbstractStoryPatternObject, AbstractStoryPatternLink>
{

	public SDEStoryPatternObjectOnlyPatternPart(
			PatternPartBasedMatcher<?, ?, ?, ?, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> patternMatcher,
			AbstractStoryPatternObject storyPatternObject)
	{
		super(patternMatcher, null, new AbstractStoryPatternObject[]
		{
			storyPatternObject
		});

		assert storyPatternObject instanceof StoryPatternObject;
	}

	@Override
	protected void doCreateLink()
	{
		// Do nothing
	}

	@Override
	protected void doDestroyLink(Map<AbstractStoryPatternObject, Object> deletedObjects)
	{
		// Do nothing
	}

	@Override
	public ECheckResult check()
	{
		return ECheckResult.OK;
	}

	@Override
	public int calculateMatchingCost()
	{
		return 0;
	}

	@Override
	public EMatchType doGetMatchType()
	{
		return EMatchType.OPTIONAL;
	}

	@Override
	public boolean match(MatchState matchState) throws SDMException
	{
		assert this.isCreate || this.patternMatcher.isBound(this.storyPatternObjects[0]);

		return true;
	}

	@Override
	public MatchState createMatchState()
	{
		return null;
	}
}
