package de.mdelab.sdm.interpreter.sde.facade;

import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.expressions.StringExpression;
import de.mdelab.sdm.interpreter.core.facade.IExpressionFacade;
import de.mdelab.sdm.interpreter.sde.facade.adapters.SDEInterpreterExpressionAsdapterFactory;
import de.mdelab.sdm.interpreter.sde.facade.adapters.StringExpressionASTHolderAdapter;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEExpressionFacade implements IExpressionFacade<Expression>
{

	@Override
	public String getExpressionLanguage(Expression expression)
	{
		if (expression instanceof CallActionExpression)
		{
			// TODO
			return "CallAction";
		}
		else
		{
			return ((StringExpression) expression).getExpressionLanguage();
		}
	}

	@Override
	public String getExpressionLanguageVersion(Expression expression)
	{
		return "1.0";
	}

	@Override
	public String getExpressionString(Expression expression)
	{
		if (expression instanceof CallActionExpression)
		{
			return expression.toString();
		}
		else
		{
			return ((StringExpression) expression).getExpressionString();
		}
	}

	@Override
	public Object getExpressionAST(Expression expression)
	{
		assert expression instanceof StringExpression;

		StringExpressionASTHolderAdapter adapter = (StringExpressionASTHolderAdapter) SDEInterpreterExpressionAsdapterFactory.INSTANCE
				.adapt(expression, SDEInterpreterExpressionAsdapterFactory.INSTANCE);

		return adapter.getAST();
	}

	@Override
	public void setExpressionAST(Expression expression, Object expressionAST)
	{
		assert expression instanceof StringExpression;

		StringExpressionASTHolderAdapter adapter = (StringExpressionASTHolderAdapter) SDEInterpreterExpressionAsdapterFactory.INSTANCE
				.adapt(expression, SDEInterpreterExpressionAsdapterFactory.INSTANCE);

		adapter.setAST(expressionAST);
	}

}
