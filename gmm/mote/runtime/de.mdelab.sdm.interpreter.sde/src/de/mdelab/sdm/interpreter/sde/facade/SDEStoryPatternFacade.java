package de.mdelab.sdm.interpreter.sde.facade;

import java.util.Collection;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.mdelab.sdm.interpreter.core.facade.IStoryPatternFacade;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEStoryPatternFacade implements
		IStoryPatternFacade<StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, Expression>
{

	@Override
	public Collection<AbstractStoryPatternObject> getStoryPatternObjects(StoryActionNode sp)
	{
		return sp.getStoryPatternObjects();
	}

	@Override
	public String getName(StoryActionNode sp)
	{
		return sp.getName();
	}

	@Override
	public Collection<Expression> getConstraints(StoryActionNode sp)
	{
		return sp.getConstraints();
	}

}
