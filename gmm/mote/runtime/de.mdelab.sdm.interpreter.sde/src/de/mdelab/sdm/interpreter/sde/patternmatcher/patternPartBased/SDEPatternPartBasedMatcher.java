package de.mdelab.sdm.interpreter.sde.patternmatcher.patternPartBased;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.expressions.ExpressionInterpreterManager;
import de.mdelab.sdm.interpreter.core.facade.MetamodelFacadeFactory;
import de.mdelab.sdm.interpreter.core.notifications.NotificationEmitter;
import de.mdelab.sdm.interpreter.core.patternmatcher.MatchingStrategy;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPart;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPartBasedMatcher;
import de.mdelab.sdm.interpreter.core.variables.Variable;
import de.mdelab.sdm.interpreter.core.variables.VariablesScope;

/**
 * Implementation of the pattern part based matcher for the StoryDiagramEcore
 * metamodel.
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEPatternPartBasedMatcher
		extends
		PatternPartBasedMatcher<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>
{

//	public SDEPatternPartBasedMatcher(
//			StoryActionNode storyPattern,
//			VariablesScope<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> variablesScope,
//			MetamodelFacadeFactory<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> facadeFactory,
//			ExpressionInterpreterManager<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> expressionInterpreterManager,
//			NotificationEmitter<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> notificationEmitter)
//			throws SDMException
//	{
//		super(	storyPattern,
//				variablesScope,
//				new DefaultMatchingStrategy<StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>(facadeFactory),
//				facadeFactory,
//				expressionInterpreterManager,
//				notificationEmitter );
//	}

	public SDEPatternPartBasedMatcher(
			StoryActionNode storyPattern,
			VariablesScope<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> variablesScope,
			MatchingStrategy<StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> matchingStrategy,
			MetamodelFacadeFactory<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> facadeFactory,
			ExpressionInterpreterManager<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> expressionInterpreterManager,
			NotificationEmitter<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> notificationEmitter)
			throws SDMException	{
		super(storyPattern, variablesScope, matchingStrategy, facadeFactory, expressionInterpreterManager, notificationEmitter);
	}

	@Override
	protected Collection<PatternPart<AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>> createPatternParts()
	{
		List<AbstractStoryPatternLink> storyPatternLinks = new LinkedList<AbstractStoryPatternLink>(this.getStoryPattern()
				.getStoryPatternLinks());

		Collection<PatternPart<AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>> patternParts = new HashSet<PatternPart<AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>>();

		Set<AbstractStoryPatternObject> mapEntryValueTargets = new HashSet<AbstractStoryPatternObject>();

		/*
		 * Split the story pattern into pattern parts. A pattern part consists
		 * either of a single isolated story pattern object or a link with its
		 * connected objects.
		 */
		while (!storyPatternLinks.isEmpty())
		{
			AbstractStoryPatternLink link = storyPatternLinks.remove(0);

			EClass linkEClass = link.eClass();

			SDEPatternPart<AbstractStoryPatternObject, ? extends AbstractStoryPatternLink> part = null;

			if (linkEClass == SdmPackage.Literals.STORY_PATTERN_LINK)
			{
				part = new SDEStoryPatternLinkPatternPart(this, (StoryPatternLink) link);
			}
			else if (linkEClass == SdmPackage.Literals.STORY_PATTERN_CONTAINMENT_LINK)
			{
				part = new SDEStoryPatternContainmentLinkPatternPart(this, (StoryPatternContainmentLink) link);
			}
			else if (linkEClass == SdmPackage.Literals.ECONTAINER_STORY_PATTERN_LINK)
			{
				assert link.getOppositeStoryPatternLink() != null;

				// Skip this link, create the pattern part for the opposite link
				continue;
			}
			else if (linkEClass == SdmPackage.Literals.STORY_PATTERN_EXPRESSION_LINK)
			{
				part = new SDEStoryPatternExpressionLinkPatternPart(this, (StoryPatternExpressionLink) link);
			}
			else if (linkEClass == SdmPackage.Literals.MAP_ENTRY_STORY_PATTERN_LINK)
			{
				MapEntryStoryPatternLink mapLink = (MapEntryStoryPatternLink) link;

				part = new SDEMapEntryLinkPatternPart(this, mapLink);

				if (mapLink.getValueTarget() != null)
				{
					mapEntryValueTargets.add(mapLink.getValueTarget());
				}
			}
			else
			{
				throw new UnsupportedOperationException();
			}

			assert part != null;

			patternParts.add(part);

			if (link.getOppositeStoryPatternLink() != null)
			{
				storyPatternLinks.remove(link.getOppositeStoryPatternLink());
			}
		}

		/*
		 * Create pattern parts for objects that are not connected to any links.
		 */
		for (AbstractStoryPatternObject spo : this.getStoryPattern().getStoryPatternObjects())
		{
			if (spo.getIncomingStoryLinks().isEmpty() && spo.getOutgoingStoryLinks().isEmpty() && !mapEntryValueTargets.contains(spo))
			{
				SDEStoryPatternObjectOnlyPatternPart part = new SDEStoryPatternObjectOnlyPatternPart(this, spo);

				patternParts.add(part);
			}
		}

		return patternParts;
	}

	@Override
	public EObject createModelObject( final AbstractStoryPatternObject p_storyPatternObject )
	throws SDMException {
		final Expression creationExpression = spoFacade.getCreateModelObjectExpression( p_storyPatternObject );
		EObject createdObj = null;
		
		if ( creationExpression != null ) {
			final Variable<EClassifier> var = getExpressionInterpreterManager().evaluateExpression( creationExpression, null, null, getVariablesScope() );

			createdObj = (EObject) var.getValue(); 
		}
		
		// Creation expression returned null so create new object the standard way.
		if ( createdObj == null ) {
			final EFactory factory = p_storyPatternObject.getClassifier().getEPackage().getEFactoryInstance();
			
			// DB: Template classes
			EClass actualClass = (EClass) p_storyPatternObject.getClassifier();
			
			if ( actualClass.isAbstract() ) {
				// Search for a rule variable of EClass type indicating the class to be instantiated.
				for ( final Variable<?> var : getVariablesScope().getVariables() ) {
					if ( var.getClassifier() == EcorePackage.Literals.ECLASS ) {
						actualClass = (EClass) var.getValue();
						
						break;
					}
				}
			}
			
			createdObj = factory.create( actualClass );
		}
		
		return createdObj;
	}
}
