package de.mdelab.sdm.interpreter.sde.facade;

import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.mdelab.sdm.interpreter.core.facade.IActivityEdgeFacade;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEActivityEdgeFacade implements IActivityEdgeFacade<ActivityNode, ActivityEdge>
{
	@Override
	public ActivityNode getSource(final ActivityEdge activityEdge)
	{
		assert activityEdge != null;
		assert activityEdge.getSource() != null;

		return activityEdge.getSource();
	}

	@Override
	public ActivityNode getTarget(final ActivityEdge activityEdge)
	{
		assert activityEdge != null;
		assert activityEdge.getTarget() != null;

		return activityEdge.getTarget();
	}

}
