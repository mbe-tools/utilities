package de.mdelab.sdm.interpreter.sde.notifications;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.mdelab.sdm.interpreter.core.notifications.NotificationEmitter;

/**
 * Specialization of the NotificationEmitter for EMF and the StoryDiagramEcore
 * metamodel.
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDENotificationEmitter
		extends
		NotificationEmitter<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>
{

}
