package de.mdelab.sdm.interpreter.sde.facade;

import org.eclipse.emf.ecore.EClassifier;

import de.mdelab.sdm.interpreter.core.facade.IClassifierFacade;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEClassifierFacade implements IClassifierFacade<EClassifier> {

	@Override
	public boolean isInstance(EClassifier classifier, Object value)	{
		return classifier.isInstance(value);
	}

	@Override
	public String getName(EClassifier classifier) {
		return classifier.getName();
	}
}
