package de.mdelab.sdm.interpreter.sde.facade.adapters;

import org.eclipse.emf.common.notify.Adapter;

import de.hpi.sam.storyDiagramEcore.expressions.util.ExpressionsAdapterFactory;

/**
 * This adapter factory creates StringExpressionASTHolderAdapters, which story
 * the AST of an already parsed textual expression.
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEInterpreterExpressionAsdapterFactory extends ExpressionsAdapterFactory
{
	public static SDEInterpreterExpressionAsdapterFactory	INSTANCE	= new SDEInterpreterExpressionAsdapterFactory();

	@Override
	public Adapter createStringExpressionAdapter()
	{
		return new StringExpressionASTHolderAdapter();
	}
}
