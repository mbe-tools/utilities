package de.mdelab.sdm.interpreter.sde.facade;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.mdelab.sdm.interpreter.core.facade.IActivityEdgeFacade;
import de.mdelab.sdm.interpreter.core.facade.IActivityFacade;
import de.mdelab.sdm.interpreter.core.facade.IClassifierFacade;
import de.mdelab.sdm.interpreter.core.facade.IDecisionNodeFacade;
import de.mdelab.sdm.interpreter.core.facade.IExpressionFacade;
import de.mdelab.sdm.interpreter.core.facade.IExpressionNodeFacade;
import de.mdelab.sdm.interpreter.core.facade.IFeatureFacade;
import de.mdelab.sdm.interpreter.core.facade.IFinalNodeFacade;
import de.mdelab.sdm.interpreter.core.facade.IInstanceFacade;
import de.mdelab.sdm.interpreter.core.facade.IStoryNodeFacade;
import de.mdelab.sdm.interpreter.core.facade.IStoryPatternFacade;
import de.mdelab.sdm.interpreter.core.facade.IStoryPatternLinkFacade;
import de.mdelab.sdm.interpreter.core.facade.IStoryPatternObjectFacade;
import de.mdelab.sdm.interpreter.core.facade.IUnspecificActivityNodeFacade;
import de.mdelab.sdm.interpreter.core.facade.MetamodelFacadeFactory;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEMetamodelFacadeFactory
		extends
		MetamodelFacadeFactory<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>
{
	public static SDEMetamodelFacadeFactory	INSTANCE	= new SDEMetamodelFacadeFactory();

	private SDEMetamodelFacadeFactory()	{
		super();
	}

	@Override
	protected IClassifierFacade<EClassifier> createClassifierFacade()
	{
		return new SDEClassifierFacade();
	}

	@Override
	protected IFeatureFacade<EStructuralFeature> createFeatureFacade()
	{
		return new SDEFeatureFacade();
	}

	@Override
	protected IStoryPatternObjectFacade<AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> createStoryPatternObjectFacade()
	{
		return new SDEStoryPatternObjectFacade();
	}

	@Override
	protected IStoryPatternLinkFacade<AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> createStoryPatternLinkFacade()
	{
		return new SDEStoryPatternLinkFacade();
	}

	@Override
	protected IStoryPatternFacade<StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, Expression> createStoryPatternFacade()
	{
		return new SDEStoryPatternFacade();
	}

	@Override
	protected IInstanceFacade<AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> createInstanceFacade()
	{
		return new SDEInstanceFacade();
	}

	@Override
	protected IExpressionFacade<Expression> createExpressionFacade()
	{
		return new SDEExpressionFacade();
	}

	@Override
	protected IActivityFacade<Activity, ActivityNode> createActivityFacade()
	{
		return new SDEActivityFacade();
	}

	@Override
	protected IUnspecificActivityNodeFacade<ActivityNode, ActivityEdge> createUnspecificActivityNodeFacade()
	{
		return new SDEUnspecificActivityNodeFacade();
	}

	@Override
	protected IStoryNodeFacade<ActivityNode, ActivityEdge, StoryActionNode> createStoryNodeFacade()
	{
		return new SDEStoryNodeFacade();
	}

	@Override
	protected IExpressionNodeFacade<ActivityNode, ActivityEdge, Expression> createExpressionNodeFacade()
	{
		return new SDEExpressionNodeFacade();
	}

	@Override
	protected IDecisionNodeFacade<ActivityNode, ActivityEdge, Expression> createDecisionNodeFacade()
	{
		return new SDEDecisionNodeFacade();
	}

	@Override
	protected IFinalNodeFacade<ActivityNode, Expression> createFinalNodeFacade()
	{
		return new SDEFinalNodeFacade();
	}

	@Override
	protected IActivityEdgeFacade<ActivityNode, ActivityEdge> createActivityEdgeFacade()
	{
		return new SDEActivityEdgeFacade();
	}

}
