package de.mdelab.sdm.interpreter.sde.facade;

import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.mdelab.sdm.interpreter.core.facade.EForEachSemantics;
import de.mdelab.sdm.interpreter.core.facade.IStoryNodeFacade;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEStoryNodeFacade extends SDEActivityNodeFacade implements IStoryNodeFacade<ActivityNode, ActivityEdge, StoryActionNode>
{

	@Override
	public StoryActionNode getStoryPattern(final ActivityNode activityNode)
	{
		assert activityNode instanceof StoryActionNode;

		return (StoryActionNode) activityNode;
	}

	@Override
	public EForEachSemantics getForEachSemantics(final ActivityNode activityNode)
	{
		assert activityNode instanceof StoryActionNode;

		final StoryActionNode storyActionNode = (StoryActionNode) activityNode;

		if (!storyActionNode.isForEach())
		{
			return EForEachSemantics.SINGLE_MATCH;
		}
		else
		{
			switch (storyActionNode.getForEachSemantics())
			{
				case FRESH_MATCH:
					return EForEachSemantics.FRESH_MATCH;
				case INTERLEAVED:
					return EForEachSemantics.INTERLEAVED;
				case PRE_SELECT:
					return EForEachSemantics.PRE_SELECT;
				default:
					throw new UnsupportedOperationException();
			}
		}
	}

	@Override
	public ActivityEdge getSuccessNextEdge(final ActivityNode activityNode)
	{
		assert activityNode instanceof StoryActionNode;
		assert activityNode.getOutgoing().size() >= 1;
		assert activityNode.getOutgoing().size() <= 2;

		if ((activityNode.getOutgoing().size() == 1) && !((StoryActionNode) activityNode).isForEach())
		{
			return activityNode.getOutgoing().get(0);
		}
		else
		{
			for (final ActivityEdge edge : activityNode.getOutgoing())
			{
				switch (edge.getGuardType())
				{
					case SUCCESS:
					case FOR_EACH:
						return edge;
					default:
						break;
				}
			}

			return null;
		}
	}

	@Override
	public ActivityEdge getFailureNextEdge(final ActivityNode activityNode)
	{
		assert activityNode instanceof StoryActionNode;
		assert activityNode.getOutgoing().size() >= 1;
		assert activityNode.getOutgoing().size() <= 2;

		if ((activityNode.getOutgoing().size() == 1) && !((StoryActionNode) activityNode).isForEach())
		{
			return activityNode.getOutgoing().get(0);
		}
		else
		{
			for (final ActivityEdge edge : activityNode.getOutgoing())
			{
				switch (edge.getGuardType())
				{
					case FAILURE:
					case END:
						return edge;
					default:
						break;
				}
			}

			return null;
		}

	}

}
