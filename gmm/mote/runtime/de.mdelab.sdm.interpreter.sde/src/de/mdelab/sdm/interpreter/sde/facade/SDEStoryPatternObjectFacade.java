package de.mdelab.sdm.interpreter.sde.facade;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.storyDiagramEcore.sdm.BindingTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;
import de.mdelab.sdm.interpreter.core.facade.IStoryPatternObjectFacade;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEStoryPatternObjectFacade implements
		IStoryPatternObjectFacade<AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>
{

	@Override
	public String getName(AbstractStoryPatternObject spo)
	{
		return spo.getName();
	}

	@Override
	public EClassifier getClassifier(AbstractStoryPatternObject spo)
	{
		return spo.getClassifier();
	}

	@Override
	public boolean isBound(AbstractStoryPatternObject spo)
	{
		assert spo instanceof StoryPatternObject;

		return ((StoryPatternObject) spo).getBindingType() == BindingTypeEnumeration.BOUND;
	}

	@Override
	public boolean isMaybeBound(AbstractStoryPatternObject spo)
	{
		assert spo instanceof StoryPatternObject;

		return ((StoryPatternObject) spo).getBindingType() == BindingTypeEnumeration.CAN_BE_BOUND;
	}

	@Override
	public boolean isCreate(AbstractStoryPatternObject spo)
	{
		return spo.getModifier() == StoryPatternModifierEnumeration.CREATE;
	}
	
	@Override
	public Expression getCreateModelObjectExpression( final AbstractStoryPatternObject p_spo ) {
		return p_spo.getCreateModelObjectExpression();
	}

	@Override
	public boolean isDestroy(AbstractStoryPatternObject spo)
	{
		return spo.getModifier() == StoryPatternModifierEnumeration.DESTROY;
	}

	@Override
	public boolean isOptional(AbstractStoryPatternObject spo)
	{
		return spo.getMatchType() == StoryPatternMatchTypeEnumeration.OPTIONAL;
	}

	@Override
	public Map<EStructuralFeature, Expression> getAttributeAssignments(AbstractStoryPatternObject spo)
	{
		assert spo instanceof StoryPatternObject;

		StoryPatternObject s = (StoryPatternObject) spo;

		if (!s.getAttributeAssignments().isEmpty())
		{
			Map<EStructuralFeature, Expression> map = new HashMap<EStructuralFeature, Expression>();

			for (AttributeAssignment assignment : ((StoryPatternObject) spo).getAttributeAssignments())
			{
				map.put(assignment.getEStructuralFeature(), assignment.getAssignmentExpression());
			}

			return map;
		}

		return null;
	}

	@Override
	public Expression getAssignmentExpression(AbstractStoryPatternObject spo)
	{
		assert spo instanceof StoryPatternObject;

		return ((StoryPatternObject) spo).getDirectAssignmentExpression();
	}

	@Override
	public Collection<Expression> getConstraints(AbstractStoryPatternObject spo)
	{
		assert spo instanceof StoryPatternObject;

		return ((StoryPatternObject) spo).getConstraints();
	}

}
