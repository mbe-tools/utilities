package de.mdelab.sdm.interpreter.sde.patternmatcher.patternPartBased;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.ECheckResult;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.EMatchType;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.MatchState;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPart;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPartBasedMatcher;
import de.mdelab.sdm.interpreter.core.variables.Variable;

/**
 * This pattern part matches and manipulates MapEntryLinks, i.e. key-value
 * mappings.
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEMapEntryLinkPatternPart extends SDEPatternPart<AbstractStoryPatternObject, MapEntryStoryPatternLink>
{

	public SDEMapEntryLinkPatternPart(
			final PatternPartBasedMatcher<?, ?, ?, ?, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> patternMatcher,
			final MapEntryStoryPatternLink storyPatternLink)
	{
		super(patternMatcher, storyPatternLink, storyPatternLink.getValueTarget() == null ? new AbstractStoryPatternObject[]
		{
				storyPatternLink.getSource(), storyPatternLink.getTarget()
		} : new AbstractStoryPatternObject[]
		{
				storyPatternLink.getSource(), storyPatternLink.getTarget(), storyPatternLink.getValueTarget()
		});

		/*
		 * if a story pattern object is created, the link must also be created
		 */
		assert (storyPatternLink.getSource().getModifier() != StoryPatternModifierEnumeration.CREATE)
				|| (storyPatternLink.getModifier() == StoryPatternModifierEnumeration.CREATE);
		assert (storyPatternLink.getTarget().getModifier() != StoryPatternModifierEnumeration.CREATE)
				|| (storyPatternLink.getModifier() == StoryPatternModifierEnumeration.CREATE);

		/*
		 * if a story pattern object is destroyed, the link must also be
		 * destroyed
		 */
		assert (storyPatternLink.getSource().getModifier() != StoryPatternModifierEnumeration.DESTROY)
				|| (storyPatternLink.getModifier() == StoryPatternModifierEnumeration.DESTROY);
		assert (storyPatternLink.getTarget().getModifier() != StoryPatternModifierEnumeration.DESTROY)
				|| (storyPatternLink.getModifier() == StoryPatternModifierEnumeration.DESTROY);

		/*
		 * If one of the link ends is optional, the links must be optional as
		 * well.
		 */
		assert (storyPatternLink.getSource().getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL)
				|| (storyPatternLink.getMatchType() == StoryPatternMatchTypeEnumeration.OPTIONAL);
		assert (storyPatternLink.getTarget().getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL)
				|| (storyPatternLink.getMatchType() == StoryPatternMatchTypeEnumeration.OPTIONAL);

		/*
		 * If the link is created or destroyed, the link's feature must be
		 * changeable
		 */
		assert ((storyPatternLink.getModifier() != StoryPatternModifierEnumeration.CREATE) && (storyPatternLink.getModifier() != StoryPatternModifierEnumeration.DESTROY))
				|| storyPatternLink.getEStructuralFeature().isChangeable();

		if (storyPatternLink.getValueTarget() != null)
		{
			assert (storyPatternLink.getValueTarget().getModifier() != StoryPatternModifierEnumeration.CREATE)
					|| (storyPatternLink.getModifier() == StoryPatternModifierEnumeration.CREATE);

			assert (storyPatternLink.getValueTarget().getModifier() != StoryPatternModifierEnumeration.DESTROY)
					|| (storyPatternLink.getModifier() == StoryPatternModifierEnumeration.DESTROY);

			assert (storyPatternLink.getValueTarget().getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL)
					|| (storyPatternLink.getMatchType() == StoryPatternMatchTypeEnumeration.OPTIONAL);
		}

		assert storyPatternLink.getOppositeStoryPatternLink() == null;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void doCreateLink()
	{
		if (this.storyPatternLink.getModifier() == StoryPatternModifierEnumeration.CREATE)
		{
			final Variable<EClassifier> sourceVariable = this.patternMatcher.getVariablesScope().getVariable(
					this.storyPatternLink.getSource().getName());
			final Variable<EClassifier> targetVariable = this.patternMatcher.getVariablesScope().getVariable(
					this.storyPatternLink.getTarget().getName());

			assert sourceVariable != null;
			assert targetVariable != null;

			assert sourceVariable.getValue() instanceof EObject;

			final EObject sourceEObject = (EObject) sourceVariable.getValue();

			if (this.storyPatternLink.getValueTarget() == null)
			{
				((EMap<Object, Object>) sourceEObject.eGet(this.storyPatternLink.getEStructuralFeature())).put(targetVariable.getValue(),
						null);

				this.patternMatcher.getNotificationEmitter().instanceLinkCreated(this.storyPatternLink.getSource(), sourceEObject,
						this.storyPatternLink, this.storyPatternLink.getTarget(), targetVariable.getValue(),
						this.patternMatcher.getVariablesScope(), this.patternMatcher);
			}
			else
			{
				final Variable<EClassifier> valueTargetVariable = this.patternMatcher.getVariablesScope().getVariable(
						this.storyPatternLink.getValueTarget().getName());

				assert valueTargetVariable != null;

				((EMap<Object, Object>) sourceEObject.eGet(this.storyPatternLink.getEStructuralFeature())).put(targetVariable.getValue(),
						valueTargetVariable.getValue());

				this.patternMatcher.getNotificationEmitter().instanceLinkCreated(this.storyPatternLink.getSource(), sourceEObject,
						this.storyPatternLink, this.storyPatternLink.getTarget(), targetVariable.getValue(),
						this.patternMatcher.getVariablesScope(), this.patternMatcher);

				this.patternMatcher.getNotificationEmitter().instanceLinkCreated(this.storyPatternLink.getSource(), sourceEObject,
						this.storyPatternLink, this.storyPatternLink.getValueTarget(), valueTargetVariable.getValue(),
						this.patternMatcher.getVariablesScope(), this.patternMatcher);
			}
		}
	}

	@Override
	protected void doDestroyLink(final Map<AbstractStoryPatternObject, Object> deletedObjects)
	{
		assert this.storyPatternLink.getModifier() == StoryPatternModifierEnumeration.DESTROY;

		final AbstractStoryPatternObject sourceSpo = this.storyPatternLink.getSource();
		final AbstractStoryPatternObject targetSpo = this.storyPatternLink.getTarget();

		assert (deletedObjects.get(sourceSpo) == null) || (deletedObjects.get(sourceSpo) instanceof EObject);

		EObject sourceInstanceObject = (EObject) deletedObjects.get(sourceSpo);
		Object targetInstanceObject = deletedObjects.get(targetSpo);

		/*
		 * The source was not deleted or it is optional and was not matched.
		 */
		if (sourceInstanceObject == null)
		{
			final Variable<EClassifier> sourceVariable = this.patternMatcher.getVariablesScope().getVariable(sourceSpo.getName());

			if (sourceVariable != null)
			{
				assert sourceSpo.getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL;
				assert sourceVariable.getValue() instanceof EObject;

				sourceInstanceObject = (EObject) sourceVariable.getValue();
			}
		}

		/*
		 * The target was not deleted
		 */
		if (targetInstanceObject == null)
		{
			final Variable<EClassifier> targetVariable = this.patternMatcher.getVariablesScope().getVariable(targetSpo.getName());

			if (targetVariable != null)
			{
				assert targetSpo.getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL;

				targetInstanceObject = targetVariable.getValue();
			}
		}

		/*
		 * Delete the link
		 */
		if ((sourceInstanceObject != null) && (targetInstanceObject != null))
		{
			@SuppressWarnings("unchecked")
			final Object value = ((EMap<Object, Object>) sourceInstanceObject.eGet(this.storyPatternLink.getEStructuralFeature()))
					.removeKey(targetInstanceObject);

			this.patternMatcher.getNotificationEmitter().instanceLinkDestroyed(sourceSpo, sourceInstanceObject, this.storyPatternLink,
					targetSpo, targetInstanceObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);

			if (this.storyPatternLink.getValueTarget() != null)
			{
				this.patternMatcher.getNotificationEmitter().instanceLinkDestroyed(sourceSpo, sourceInstanceObject, this.storyPatternLink,
						this.storyPatternLink.getValueTarget(), value, this.patternMatcher.getVariablesScope(), this.patternMatcher);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public ECheckResult check()
	throws SDMException	{
		/*
		 * To check a map entry link, the source and key objects must be bound.
		 * The value object may still be unbound.
		 */
		if ((this.storyPatternLink.getModifier() != StoryPatternModifierEnumeration.CREATE)
				&& this.patternMatcher.isBound(this.storyPatternLink.getSource())
				&& this.patternMatcher.isBound(this.storyPatternLink.getTarget()))
		{
			final AbstractStoryPatternObject sourceSpo = this.storyPatternLink.getSource();
			final AbstractStoryPatternObject keySpo = this.storyPatternLink.getTarget();
			final AbstractStoryPatternObject valueSpo = this.storyPatternLink.getValueTarget();

			final EObject sourceInstanceObject = (EObject) this.patternMatcher.getInstanceObject(sourceSpo);
			checkUnresolvedProxy( sourceInstanceObject );

			final Object keyInstanceObject = this.patternMatcher.getInstanceObject(keySpo);
			checkUnresolvedProxy( keyInstanceObject );

			/*
			 * There is no value or the value is unbound.
			 */
			if ((valueSpo == null) || !this.patternMatcher.isBound(valueSpo)) {
				if ( ((EMap<Object, Object>) sourceInstanceObject.eGet( storyPatternLink.getEStructuralFeature() ) ).containsKey( keyInstanceObject ) )	{
					patternMatcher.getNotificationEmitter().linkCheckSuccessful(	sourceSpo,
																					sourceInstanceObject,
																					storyPatternLink,
																					keySpo,
																					keyInstanceObject,
																					patternMatcher.getVariablesScope(), 
																					patternMatcher );

					if ( valueSpo != null ) {
						/*
						 * Return unknown if there is an unbound value.
						 */
						return ECheckResult.UNKNOWN;
					}

					return ECheckResult.OK;
				}

				patternMatcher.getNotificationEmitter().linkCheckFailed(	sourceSpo,
																			sourceInstanceObject,
																			storyPatternLink,
																			keySpo,
																			keyInstanceObject,
																			patternMatcher.getVariablesScope(),
																			patternMatcher );

				return ECheckResult.FAIL;
			}
			else
			{
				final Object valueObject = this.patternMatcher.getInstanceObject(valueSpo);
				checkUnresolvedProxy( valueObject );
				final Object gotObject = ((EMap<Object, Object>) sourceInstanceObject.eGet(this.storyPatternLink.getEStructuralFeature())).get(keyInstanceObject);
				checkUnresolvedProxy( gotObject );

				if (valueObject == gotObject ) {
					this.patternMatcher.getNotificationEmitter().linkCheckSuccessful(sourceSpo, sourceInstanceObject,
							this.storyPatternLink, keySpo, keyInstanceObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);

					this.patternMatcher.getNotificationEmitter().linkCheckSuccessful(sourceSpo, sourceInstanceObject,
							this.storyPatternLink, valueSpo, valueObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);

					return ECheckResult.OK;
				}
				else
				{
					this.patternMatcher.getNotificationEmitter().linkCheckFailed(sourceSpo, sourceInstanceObject, this.storyPatternLink,
							keySpo, keyInstanceObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);

					this.patternMatcher.getNotificationEmitter().linkCheckFailed(sourceSpo, sourceInstanceObject, this.storyPatternLink,
							valueSpo, valueObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);

					return ECheckResult.FAIL;
				}
			}
		}
		else if ((this.storyPatternLink.getModifier() == StoryPatternModifierEnumeration.CREATE)
				&& (this.storyPatternLink.getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL))
		{
			return ECheckResult.OK;
		}
		else
		{
			return ECheckResult.UNKNOWN;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean match(final MatchState matchState) throws SDMException
	{
		assert matchState instanceof SDEStoryPatternLinkMatchState;

		final SDEStoryPatternLinkMatchState<Iterator<Entry<Object, Object>>> ms = (SDEStoryPatternLinkMatchState<Iterator<Entry<Object, Object>>>) matchState;

		final AbstractStoryPatternObject sourceSpo = this.storyPatternLink.getSource();
		final AbstractStoryPatternObject keySpo = this.storyPatternLink.getTarget();
		final AbstractStoryPatternObject valueSpo = this.storyPatternLink.getValueTarget();

		assert sourceSpo != null;
		assert keySpo != null;

		assert this.patternMatcher.isBound(sourceSpo);

		final EObject sourceInstanceObject = (EObject) this.patternMatcher.getInstanceObject(sourceSpo);
		checkUnresolvedProxy( sourceInstanceObject );

		/*
		 * There may be two cases: 1. The key object is unbound, the value
		 * object may not exist or be bound or unbound. 2. The key object is
		 * already bound, the value object must exist and be unbound.
		 */
		if (!this.patternMatcher.isBound(keySpo))
		{
			this.patternMatcher.getNotificationEmitter().traversingLink(this.storyPatternLink, sourceSpo, sourceInstanceObject, keySpo,
					this.patternMatcher.getVariablesScope(), this.patternMatcher);

			Iterator<Entry<Object, Object>> iterator = ms.getLinkIterator();

			if (iterator == null)
			{
				iterator = ((EMap<Object, Object>) sourceInstanceObject.eGet(this.storyPatternLink.getEStructuralFeature())).entrySet()
						.iterator();

				ms.setLinkIterator(iterator);
				ms.setSourceInstanceObject(sourceInstanceObject);
			}

			while (iterator.hasNext())
			{
				final Entry<Object, Object> entry = iterator.next();

				final Object keyObject = entry.getKey();
				final Object valueObject = entry.getValue();

				/*
				 * If there is a bound value object, compare it to this entry's
				 * value.
				 */
				if ((valueSpo != null) && this.patternMatcher.isBound(valueSpo))
				{
					if (this.patternMatcher.getInstanceObject(valueSpo) != valueObject)
					{
						continue;
					}
				}

				if (this.patternMatcher.matchStoryPatternObject(keySpo, keyObject))
				{
					this.patternMatcher.getNotificationEmitter().storyPatternObjectBound(keySpo, keyObject,
							this.patternMatcher.getVariablesScope(), this.patternMatcher);

					if ((valueSpo != null) && !this.patternMatcher.isBound(valueSpo))
					{
						this.patternMatcher.getNotificationEmitter().traversingLink(this.storyPatternLink, sourceSpo, sourceInstanceObject,
								valueSpo, this.patternMatcher.getVariablesScope(), this.patternMatcher);

						if (this.patternMatcher.matchStoryPatternObject(valueSpo, valueObject))
						{
							this.patternMatcher.getNotificationEmitter().storyPatternObjectBound(valueSpo, valueObject,
									this.patternMatcher.getVariablesScope(), this.patternMatcher);

							return true;
						}
						else
						{
							this.patternMatcher.getNotificationEmitter().storyPatternObjectNotBound(valueSpo,
									this.patternMatcher.getVariablesScope(), this.patternMatcher);

							/*
							 * roll back the match transaction of keySpo
							 */
							this.patternMatcher.rollBackLastMatchStoryPatternObjectTransaction();
						}
					}
					else
					{
						return true;
					}
				}
			}

			this.patternMatcher.getNotificationEmitter().storyPatternObjectNotBound(keySpo, this.patternMatcher.getVariablesScope(),
					this.patternMatcher);

			if ((valueSpo != null) && !this.patternMatcher.isBound(valueSpo))
			{
				this.patternMatcher.getNotificationEmitter().storyPatternObjectNotBound(valueSpo, this.patternMatcher.getVariablesScope(),
						this.patternMatcher);
			}
		}
		else
		{
			/*
			 * The key is bound, the value is unbound.
			 */
			assert valueSpo != null;
			assert !this.patternMatcher.isBound(valueSpo);

			final Object keyInstanceObject = this.patternMatcher.getInstanceObject(keySpo);

			this.patternMatcher.getNotificationEmitter().traversingLink(this.storyPatternLink, sourceSpo, sourceInstanceObject, valueSpo,
					this.patternMatcher.getVariablesScope(), this.patternMatcher);

			if (this.patternMatcher.matchStoryPatternObject(valueSpo, ((EMap<Object, Object>) sourceInstanceObject
					.eGet(this.storyPatternLink.getEStructuralFeature())).get(keyInstanceObject)))
			{
				return true;
			}

			this.patternMatcher.getNotificationEmitter().storyPatternObjectNotBound(valueSpo, this.patternMatcher.getVariablesScope(),
					this.patternMatcher);
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int calculateMatchingCost()
	{
		assert !(this.patternMatcher.isBound(this.storyPatternLink.getSource())
				&& this.patternMatcher.isBound(this.storyPatternLink.getTarget()) && this.patternMatcher.isBound(this.storyPatternLink
				.getValueTarget()));

		if (this.patternMatcher.isBound(this.storyPatternLink.getSource()))
		{
			/*
			 * If the key is already bound, the matching cost is very low.
			 */
			if (this.patternMatcher.isBound(this.storyPatternLink.getTarget()))
			{
				return 2 + this.getNumberOfIncomingUnidirectionalLinks(this.storyPatternLink.getValueTarget());
			}
			else
			{
				/*
				 * Otherwise, the cost is the number of map entries.
				 */
				final Variable<EClassifier> sourceVariable = this.patternMatcher.getVariablesScope().getVariable(
						this.storyPatternLink.getSource().getName());

				assert sourceVariable.getValue() instanceof EObject;

				final EObject sourceEObject = (EObject) sourceVariable.getValue();

				return ((EMap<Object, Object>) sourceEObject.eGet(this.storyPatternLink.getEStructuralFeature())).size()
						+ this.getNumberOfIncomingUnidirectionalLinks(this.storyPatternLink.getTarget());
			}
		}
		else
		{
			return PatternPart.MATCHING_NOT_POSSIBLE;
		}
	}

	@Override
	public EMatchType doGetMatchType()
	{
		switch (this.storyPatternLink.getModifier())
		{
			case NONE:
				switch (this.storyPatternLink.getMatchType())
				{
					case NONE:
						return EMatchType.MANDATORY;
					case OPTIONAL:
						return EMatchType.OPTIONAL;
					case NEGATIVE:
						throw new UnsupportedOperationException();
					default:
						throw new UnsupportedOperationException();
				}
			case CREATE:
				switch (this.storyPatternLink.getMatchType())
				{
					case NONE:
						return EMatchType.OPTIONAL;
					case OPTIONAL:
						return EMatchType.OPTIONAL;
					case NEGATIVE:
						throw new UnsupportedOperationException();
					default:
						throw new UnsupportedOperationException();
				}
			case DESTROY:
				switch (this.storyPatternLink.getMatchType())
				{
					case NONE:
						return EMatchType.MANDATORY;
					case OPTIONAL:
						return EMatchType.OPTIONAL;
					case NEGATIVE:
						throw new UnsupportedOperationException();
					default:
						throw new UnsupportedOperationException();
				}
			default:
				throw new UnsupportedOperationException();
		}
	}

	@Override
	public MatchState createMatchState() {
		return new SDEStoryPatternLinkMatchState<Iterator<Entry<Object, Object>>>();
	}
}
