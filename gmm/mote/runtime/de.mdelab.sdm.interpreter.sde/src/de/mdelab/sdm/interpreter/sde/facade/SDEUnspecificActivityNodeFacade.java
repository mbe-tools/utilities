package de.mdelab.sdm.interpreter.sde.facade;

import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.mdelab.sdm.interpreter.core.facade.IUnspecificActivityNodeFacade;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEUnspecificActivityNodeFacade extends SDEActivityNodeFacade implements
		IUnspecificActivityNodeFacade<ActivityNode, ActivityEdge>
{

	@Override
	public ActivityEdge getNextActivityEdge(ActivityNode activityNode)
	{
		assert activityNode.getOutgoing().size() == 1;

		return activityNode.getOutgoing().get(0);
	}

}
