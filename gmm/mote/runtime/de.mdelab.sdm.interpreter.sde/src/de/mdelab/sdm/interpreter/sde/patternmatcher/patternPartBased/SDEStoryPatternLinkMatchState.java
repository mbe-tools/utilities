package de.mdelab.sdm.interpreter.sde.patternmatcher.patternPartBased;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;

import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.MatchState;

public class SDEStoryPatternLinkMatchState<ITERATOR_TYPE extends Iterator<?>> extends MatchState {
	
	private ITERATOR_TYPE	linkIterator;

	private EObject			sourceInstanceObject;

	public SDEStoryPatternLinkMatchState() {
		super();
	}
	
	public ITERATOR_TYPE getLinkIterator()
	{
		return this.linkIterator;
	}

	public void setLinkIterator(ITERATOR_TYPE linkIterator)
	{
		this.linkIterator = linkIterator;
	}

	public EObject getSourceInstanceObject()
	{
		return this.sourceInstanceObject;
	}

	public void setSourceInstanceObject(EObject sourceInstanceObject)
	{
		this.sourceInstanceObject = sourceInstanceObject;
	}
	
	@Override
	public String toString() {
		final StringBuilder strBuild = new StringBuilder( "sourceInstanceObject: " );
		strBuild.append( sourceInstanceObject );
		
		return strBuild.toString();
	}

	@Override
	public boolean isComplete() {
		return linkIterator != null && !linkIterator.hasNext();
	}
}
