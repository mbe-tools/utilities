package de.mdelab.sdm.interpreter.sde.facade;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.ExpressionImport;
import de.hpi.sam.storyDiagramEcore.Import;
import de.hpi.sam.storyDiagramEcore.StoryDiagramEcorePackage;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.mdelab.sdm.interpreter.core.SDMInterpreterConstants;
import de.mdelab.sdm.interpreter.core.facade.IActivityFacade;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEActivityFacade implements IActivityFacade<Activity, ActivityNode>
{
	@Override
	public ActivityNode getInitialNode(Activity activity)
	{
		assert activity != null;

		for (ActivityNode n : activity.getNodes())
		{
			if (n.eClass() == NodesPackage.Literals.INITIAL_NODE)
			{
				return n;
			}
		}

		return null;
	}

	@Override
	public String getName(Activity activity)
	{
		return activity.getName();
	}

	@Override
	public Map<String, Map<String, List<String>>> getExpressionImports(Activity activity)
	{
		assert activity != null;

		if (activity.getImports().isEmpty())
		{
			return Collections.emptyMap();
		}
		else
		{
			Map<String, Map<String, List<String>>> map = new HashMap<String, Map<String, List<String>>>();

			for (Import imp : activity.getImports())
			{
				if (imp.eClass() == StoryDiagramEcorePackage.Literals.EXPRESSION_IMPORT)
				{
					ExpressionImport expressionImport = (ExpressionImport) imp;

					Map<String, List<String>> m1 = map.get(expressionImport.getExpressionLanguage());

					if (m1 == null)
					{
						m1 = new HashMap<String, List<String>>();
						map.put(expressionImport.getExpressionLanguage(), m1);
					}

					List<String> list = m1.get(SDMInterpreterConstants.DEFAULT_EXPRESSION_LANGUAGE_VERSION);

					if (list == null)
					{
						list = new LinkedList<String>();
						m1.put(SDMInterpreterConstants.DEFAULT_EXPRESSION_LANGUAGE_VERSION, list);
					}

					list.add(expressionImport.getResolvedImportedFileURI());
				}
				else
				{
					throw new UnsupportedOperationException();
				}
			}

			return map;
		}
	}

}
