package de.mdelab.sdm.interpreter.sde.patternmatcher.patternPartBased;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.ECheckResult;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.EMatchType;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.MatchState;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPart;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPartBasedMatcher;
import de.mdelab.sdm.interpreter.core.variables.Variable;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEStoryPatternExpressionLinkPatternPart extends SDEPatternPart<AbstractStoryPatternObject, StoryPatternExpressionLink>
{

	public SDEStoryPatternExpressionLinkPatternPart(
			final PatternPartBasedMatcher<?, ?, ?, ?, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> patternMatcher,
			final StoryPatternExpressionLink storyPatternLink)
	{
		super(patternMatcher, storyPatternLink, new AbstractStoryPatternObject[]
		{
				storyPatternLink.getSource(), storyPatternLink.getTarget()
		});

		/*
		 * expression links cannot be created
		 */
		assert ((storyPatternLink.getModifier() != StoryPatternModifierEnumeration.CREATE) & (storyPatternLink.getSource().getModifier() != StoryPatternModifierEnumeration.CREATE))
				&& (storyPatternLink.getTarget().getModifier() != StoryPatternModifierEnumeration.CREATE);

		/*
		 * expression links cannot be destroyed
		 */
		assert ((storyPatternLink.getModifier() != StoryPatternModifierEnumeration.DESTROY) & (storyPatternLink.getSource().getModifier() != StoryPatternModifierEnumeration.DESTROY))
				&& (storyPatternLink.getTarget().getModifier() != StoryPatternModifierEnumeration.DESTROY);

		/*
		 * If one of the link ends is optional, the links must be optional as
		 * well.
		 */
		assert (storyPatternLink.getSource().getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL)
				|| (storyPatternLink.getMatchType() == StoryPatternMatchTypeEnumeration.OPTIONAL);
		assert (storyPatternLink.getTarget().getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL)
				|| (storyPatternLink.getMatchType() == StoryPatternMatchTypeEnumeration.OPTIONAL);

		assert storyPatternLink.getExpression() != null;
		assert storyPatternLink.getOppositeStoryPatternLink() == null;
	}

	@Override
	protected void doCreateLink()
	{
		// Do nothing
	}

	@Override
	protected void doDestroyLink(final Map<AbstractStoryPatternObject, Object> deletedObjects)
	{
		// Do nothing
	}

	@SuppressWarnings("unchecked")
	@Override
	public ECheckResult check() throws SDMException
	{
		if (this.patternMatcher.isBound(this.storyPatternLink.getSource())
				&& this.patternMatcher.isBound(this.storyPatternLink.getTarget()))
		{
			final AbstractStoryPatternObject sourceSpo = this.storyPatternLink.getSource();
			final AbstractStoryPatternObject targetSpo = this.storyPatternLink.getTarget();

			final EObject sourceInstanceObject = (EObject) this.patternMatcher.getInstanceObject(sourceSpo);
			final Object targetInstanceObject = this.patternMatcher.getInstanceObject(targetSpo);

			/*
			 * Evaluate the expression
			 */
			final Variable<EClassifier> result = this.patternMatcher.getExpressionInterpreterManager().evaluateExpression(
					this.storyPatternLink.getExpression(), sourceSpo.getClassifier(), sourceInstanceObject,
					this.patternMatcher.getVariablesScope());

			if (result.getValue() instanceof Collection<?>)
			{
				if (((Collection<Object>) result.getValue()).contains(targetInstanceObject))
				{
					this.patternMatcher.getNotificationEmitter().linkCheckSuccessful(sourceSpo, sourceInstanceObject,
							this.storyPatternLink, targetSpo, targetInstanceObject, this.patternMatcher.getVariablesScope(),
							this.patternMatcher);

					return ECheckResult.OK;
				}
				else
				{
					this.patternMatcher.getNotificationEmitter().linkCheckFailed(sourceSpo, sourceInstanceObject, this.storyPatternLink,
							targetSpo, targetInstanceObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);

					return ECheckResult.FAIL;
				}
			}
			else
			{
				if (result.getValue() == targetInstanceObject)
				{
					this.patternMatcher.getNotificationEmitter().linkCheckSuccessful(sourceSpo, sourceInstanceObject,
							this.storyPatternLink, targetSpo, targetInstanceObject, this.patternMatcher.getVariablesScope(),
							this.patternMatcher);

					return ECheckResult.OK;
				}
				else
				{
					this.patternMatcher.getNotificationEmitter().linkCheckFailed(sourceSpo, sourceInstanceObject, this.storyPatternLink,
							targetSpo, targetInstanceObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);

					return ECheckResult.FAIL;
				}
			}
		}
		else
		{
			return ECheckResult.UNKNOWN;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean match(final MatchState matchState) throws SDMException
	{
		final AbstractStoryPatternObject sourceSpo = this.storyPatternLink.getSource();
		final AbstractStoryPatternObject targetSpo = this.storyPatternLink.getTarget();

		assert this.patternMatcher.isBound(sourceSpo);
		assert !this.patternMatcher.isBound(targetSpo);

		final EObject sourceInstanceObject = (EObject) this.patternMatcher.getInstanceObject(sourceSpo);

		/*
		 * Evaluate link expression
		 */
		final Variable<EClassifier> result = this.patternMatcher.getExpressionInterpreterManager().evaluateExpression(
				this.storyPatternLink.getExpression(), sourceSpo.getClassifier(), sourceInstanceObject,
				this.patternMatcher.getVariablesScope());

		assert result != null;

		this.patternMatcher.getNotificationEmitter().traversingLink(this.storyPatternLink, sourceSpo, sourceInstanceObject, targetSpo,
				this.patternMatcher.getVariablesScope(), this.patternMatcher);

		if (result.getValue() instanceof Collection<?>)
		{
			for (final Object targetObject : (Collection<Object>) result.getValue())
			{
				if (this.patternMatcher.matchStoryPatternObject(targetSpo, targetObject))
				{
					return true;
				}
			}
		}
		else
		{
			if ((result.getValue() != null) && this.patternMatcher.matchStoryPatternObject(targetSpo, result.getValue()))
			{
				return true;
			}
		}

		this.patternMatcher.getNotificationEmitter().storyPatternObjectNotBound(targetSpo, this.patternMatcher.getVariablesScope(),
				this.patternMatcher);

		return false;
	}

	@Override
	public int calculateMatchingCost()
	{
		assert !(this.patternMatcher.isBound(this.storyPatternLink.getSource()) && this.patternMatcher.isBound(this.storyPatternLink
				.getTarget()));

		if (this.patternMatcher.isBound(this.storyPatternLink.getSource()))
		{
			/*
			 * TODO Perform a thorough analysis of the expression to give a
			 * useful estimate.
			 */
			return 42 + this.getNumberOfIncomingUnidirectionalLinks(this.storyPatternLink.getTarget());
		}
		else
		{
			return PatternPart.MATCHING_NOT_POSSIBLE;
		}
	}

	@Override
	public EMatchType doGetMatchType()
	{
		switch (this.storyPatternLink.getModifier())
		{
			case NONE:
				switch (this.storyPatternLink.getMatchType())
				{
					case NONE:
						return EMatchType.MANDATORY;
					case OPTIONAL:
						return EMatchType.OPTIONAL;
					case NEGATIVE:
						throw new UnsupportedOperationException();
					default:
						break;
				}
			case CREATE:
				throw new UnsupportedOperationException();
			case DESTROY:
				switch (this.storyPatternLink.getMatchType())
				{
					case NONE:
						return EMatchType.MANDATORY;
					case OPTIONAL:
						return EMatchType.OPTIONAL;
					case NEGATIVE:
						throw new UnsupportedOperationException();
					default:
						throw new UnsupportedOperationException();
				}
			default:
				throw new UnsupportedOperationException();
		}
	}

	@Override
	public MatchState createMatchState()
	{
		/*
		 * We do not need a match state for expression links.
		 */
		return null;
	}
}
