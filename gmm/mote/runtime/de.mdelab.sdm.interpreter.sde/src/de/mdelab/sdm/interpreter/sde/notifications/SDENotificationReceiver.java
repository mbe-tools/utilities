package de.mdelab.sdm.interpreter.sde.notifications;

import org.eclipse.emf.ecore.EClassifier;

import de.mdelab.sdm.interpreter.core.notifications.NotificationReceiver;

public interface SDENotificationReceiver extends NotificationReceiver<EClassifier>
{

}
