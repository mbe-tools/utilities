package de.mdelab.sdm.interpreter.sde.facade;

import org.eclipse.emf.ecore.EClass;

import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.NodesPackage;
import de.mdelab.sdm.interpreter.core.facade.EActivityNodeType;
import de.mdelab.sdm.interpreter.core.facade.IActivityNodeFacade;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public abstract class SDEActivityNodeFacade implements IActivityNodeFacade<ActivityNode>
{

	@Override
	public EActivityNodeType getActivityNodeType(ActivityNode activityNode)
	{
		EClass activityNodeEClass = activityNode.eClass();

		if (activityNodeEClass == NodesPackage.Literals.STORY_ACTION_NODE)
		{
			return EActivityNodeType.STORY_NODE;
		}
		else if (activityNodeEClass == NodesPackage.Literals.EXPRESSION_ACTIVITY_NODE)
		{
			return EActivityNodeType.STATEMENT_NODE;
		}
		else if (activityNodeEClass == NodesPackage.Literals.DECISION_NODE)
		{
			return EActivityNodeType.DECISION_NODE;
		}
		else if (activityNodeEClass == NodesPackage.Literals.ACTIVITY_FINAL_NODE)
		{
			return EActivityNodeType.ACTIVITY_FINAL_NODE;
		}
		else if (activityNodeEClass == NodesPackage.Literals.INITIAL_NODE)
		{
			return EActivityNodeType.NON_EXECUTABLE_NODE;
		}
		else if (activityNodeEClass == NodesPackage.Literals.MERGE_NODE)
		{
			return EActivityNodeType.NON_EXECUTABLE_NODE;
		}
		else
		{
			throw new UnsupportedOperationException();
		}
	}

	@Override
	public String getName(ActivityNode activityNode)
	{
		return activityNode.getName();
	}
}
