package de.mdelab.sdm.interpreter.sde.facade;

import java.util.HashMap;
import java.util.Map;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.DecisionNode;
import de.mdelab.sdm.interpreter.core.facade.IDecisionNodeFacade;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEDecisionNodeFacade extends SDEActivityNodeFacade implements IDecisionNodeFacade<ActivityNode, ActivityEdge, Expression>
{

	@Override
	public Map<Expression, ActivityEdge> getConditionalNextEdges(ActivityNode activityNode)
	{
		assert activityNode instanceof DecisionNode;

		Map<Expression, ActivityEdge> map = new HashMap<Expression, ActivityEdge>();

		for (ActivityEdge edge : activityNode.getOutgoing())
		{
			switch (edge.getGuardType())
			{
				case BOOLEAN:
					map.put(edge.getGuardExpression(), edge);
					break;

				case ELSE:
					break;

				default:
					throw new UnsupportedOperationException();
			}
		}

		return map;
	}

	@Override
	public ActivityEdge getUnconditionalNextEdge(ActivityNode activityNode)
	{
		for (ActivityEdge edge : activityNode.getOutgoing())
		{
			switch (edge.getGuardType())
			{
				case BOOLEAN:
					break;

				case ELSE:
					return edge;

				default:
					throw new UnsupportedOperationException();
			}
		}

		throw new UnsupportedOperationException();
	}

}
