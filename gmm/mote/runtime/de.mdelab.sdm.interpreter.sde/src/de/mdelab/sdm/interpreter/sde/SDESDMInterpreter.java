package de.mdelab.sdm.interpreter.sde;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.SDMInterpreter;
import de.mdelab.sdm.interpreter.core.SDMInterpreterConstants;
import de.mdelab.sdm.interpreter.core.expressions.ExpressionInterpreterManager;
import de.mdelab.sdm.interpreter.core.facade.MetamodelFacadeFactory;
import de.mdelab.sdm.interpreter.core.notifications.NotificationEmitter;
import de.mdelab.sdm.interpreter.core.patternmatcher.MatchingStrategy;
import de.mdelab.sdm.interpreter.core.patternmatcher.StoryPatternMatcher;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.OptimizedDefaultMatchingStrategy;
import de.mdelab.sdm.interpreter.core.variables.Variable;
import de.mdelab.sdm.interpreter.core.variables.VariablesScope;
import de.mdelab.sdm.interpreter.sde.patternmatcher.patternPartBased.SDEPatternPartBasedMatcher;

/**
 * Subclass of the SDMInterpreter specialized for EMF and the StoryDiagramEcore
 * metamodel.
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDESDMInterpreter
		extends
		SDMInterpreter<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>
{

	public SDESDMInterpreter(
			MetamodelFacadeFactory<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> facadeFactory,
			ExpressionInterpreterManager<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> expressionInterpreterManager )
	{
		this( facadeFactory, expressionInterpreterManager, null);
	}

	public SDESDMInterpreter(
			MetamodelFacadeFactory<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> facadeFactory,
			ExpressionInterpreterManager<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> expressionInterpreterManager,
			NotificationEmitter<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> notificationEmitter)
	{
		super( facadeFactory, expressionInterpreterManager, notificationEmitter);
	}

	@Override
	protected StoryPatternMatcher<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> createStoryPatternMatcher(
			StoryActionNode storyPattern,
			VariablesScope<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> variablesScope)
	throws SDMException	{
		final MetamodelFacadeFactory<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> factory = getFacadeFactory();
		
		return new SDEPatternPartBasedMatcher(	storyPattern,
												variablesScope,
												createMatchingStrategy( factory ),
												factory, 
												getExpressionInterpreterManager(), 
												getNotificationEmitter() );
	}
	
	protected MatchingStrategy<StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> createMatchingStrategy( final MetamodelFacadeFactory<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> p_factory ) {
		return new OptimizedDefaultMatchingStrategy<StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>( p_factory );
	}

	/**
	 * Execute the specified activity in the context of thisObject.
	 * 
	 * @param activity
	 * @param thisObject
	 * @param parameters
	 * @return
	 * @throws SDMException
	 */
	public Object executeActivity(Activity activity, EObject thisObject, List<Object> parameters) throws SDMException
	{
		if (activity == null)
		{
			throw new SDMException("activity is null");
		}
		else if (activity.getSpecification() == null)
		{
			throw new SDMException("activity '" + activity + "' has no specification.");
		}
		else if (thisObject == null)
		{
			throw new SDMException("thisObject is null");
		}

		List<Variable<EClassifier>> variables = new ArrayList<Variable<EClassifier>>();

		/*
		 * Add variable for this
		 */
		variables.add(new Variable<EClassifier>("this", activity.getSpecification().getEContainingClass(), thisObject));

		for (int i = 0; i < activity.getSpecification().getEParameters().size(); i++)
		{
			EParameter parameter = activity.getSpecification().getEParameters().get(i);

			variables.add(new Variable<EClassifier>(parameter.getName(), parameter.getEType(), parameters.get(i)));
		}

		Variable<EClassifier> returnValue = this.executeActivity(activity, variables).get(SDMInterpreterConstants.RETURN_VALUE_VAR_NAME);
		
		if ( !getStoryPatternMatchers().isEmpty() || !getVariableScopes().isEmpty() ) {
			throw new IllegalStateException();
		}

		if (returnValue != null)
		{
			if (activity.getSpecification().getEType() == null)
			{
				throw new SDMException("activity '" + activity
						+ "' returned a value although its specification does not specify a return type.");
			}
			else if (activity.getSpecification().getEType().isInstance(returnValue.getValue()))
			{
				return returnValue.getValue();
			}
			else
			{
				throw new SDMException("The expected return type is '" + activity.getSpecification().getEType() + "', but '"
						+ returnValue.getValue() + "' was returned.");
			}
		}
		else
		{
			if (activity.getSpecification().getEType() == null)
			{
				/*
				 * Everything is ok.
				 */
				return null;
			}
			else
			{
				throw new SDMException("The activity '" + activity + "' did not return a value although the return type '"
						+ activity.getSpecification().getEType() + "' is specified.");
			}
		}
	}

	@Override
	protected ActivityNode executeCustomNode(
			ActivityNode node,
			VariablesScope<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> variablesScope)
	{
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}
}
