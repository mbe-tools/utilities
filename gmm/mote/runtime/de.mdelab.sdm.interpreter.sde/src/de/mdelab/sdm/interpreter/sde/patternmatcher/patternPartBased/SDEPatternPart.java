package de.mdelab.sdm.interpreter.sde.patternmatcher.patternPartBased;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternObject;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.UnresolvedReferenceException;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.EMatchType;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPart;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPartBasedMatcher;
import de.mdelab.sdm.interpreter.core.variables.Variable;

/**
 * Abstract class of all pattern parts specific for the StoryDiagramEcore
 * metamodel.
 * 
 * @author Stephan Hildebrandt
 * 
 * @param <SPO>
 * @param <SPL>
 */
public abstract class SDEPatternPart<SPO extends AbstractStoryPatternObject, SPL extends AbstractStoryPatternLink> extends
		PatternPart<AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>
{
	protected boolean			isCreate	= false;

	protected boolean			isDestroy	= false;

	protected final SPL			storyPatternLink;

	protected final SPO[]		storyPatternObjects;

	protected final EMatchType	matchType;
	
	public SDEPatternPart(
			final PatternPartBasedMatcher<?, ?, ?, ?, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> patternMatcher,
			final SPL storyPatternLink, final SPO[] storyPatternObjects) {
		super(patternMatcher);
		
		assert storyPatternObjects.length > 0;
		assert (storyPatternLink != null) || (storyPatternObjects.length == 1);

		this.storyPatternLink = storyPatternLink;
		this.storyPatternObjects = storyPatternObjects;

		this.matchType = this.doGetMatchType();

		if (storyPatternLink != null) {
			switch (storyPatternLink.getModifier())	{
				case CREATE:
					isCreate = true;
					
					break;

				case DESTROY:
					isDestroy = true;
					
					break;
				default:
					break;
			}
		}

		for (final SPO spo : storyPatternObjects) {
			switch (spo.getModifier()) {
				case CREATE:
					isCreate = true;
					
					break;

				case DESTROY:
					isDestroy = true;
					
					break;
				
				default:
					break;
			}
		}
	}

	@Override
	public AbstractStoryPatternObject[] getStoryPatternObjects()
	{
		return this.storyPatternObjects;
	}

	public SPL getStoryPatternLink()
	{
		return this.storyPatternLink;
	}

	@Override
	public void createObjects() {
		if ( isCreate ) {
			/*
			 * Create objects
			 */
			for (final AbstractStoryPatternObject spo : this.storyPatternObjects) {
				if ( spo.getModifier() == StoryPatternModifierEnumeration.CREATE ) {
					final EClass spoEClass = spo.eClass();

					if (spoEClass == SdmPackage.Literals.STORY_PATTERN_OBJECT) {
						/*
						 * Skip objects that already exist. This may be the case
						 * for optionally created objects and objects already
						 * created by another pattern part.
						 */
						if (!patternMatcher.getVariablesScope().variableExists(spo.getName()))	{
							final StoryPatternObject storyPatternObject = (StoryPatternObject) spo;

							assert storyPatternObject.getClassifier() instanceof EClass;
							
							// DB: Fix to reuse the same instance
							try {
								final EObject eObject = (EObject) patternMatcher.createModelObject( storyPatternObject );

								patternMatcher.getNotificationEmitter().instanceObjectCreated(	spo,
																								eObject,
																								patternMatcher.getVariablesScope(),
																								patternMatcher );

								patternMatcher.getVariablesScope().createVariable(storyPatternObject.getName(),
										storyPatternObject.getClassifier(), eObject);
							}
							catch( final SDMException p_ex ) {
								throw new RuntimeException( p_ex );
							}
						}
					}
					else {
						throw new UnsupportedOperationException();
					}
				}
			}
		}
	}
//	
//	private EObject existingModelObject( final AbstractStoryPatternObject p_storyPatternObject )
//	throws SDMException {
//		final String corrNodeVarName = p_storyPatternObject.getExistingCorrNode();
//		
//		if ( corrNodeVarName != null ) {
//			final Variable<EClassifier> var = patternMatcher.getVariablesScope().getVariable( corrNodeVarName );
//			
//			if ( var != null ) {
//				final Object value = var.getValue();
//				
//				if ( value instanceof EObject ) {
//					final EObject referringObj = (EObject) value;
//					final EClassifier eClassifier = p_storyPatternObject.getClassifier();
//					
//					for ( final EReference reference : referringObj.eClass().getEAllReferences() ) {
//						if ( !reference.isContainment() && reference.isChangeable() ) {
//							if ( reference.isMany() ) {
//								@SuppressWarnings("unchecked")
//								final List<EObject> refObjects = (List<EObject>) referringObj.eGet( reference ); 
//								
//								for ( final EObject elem : refObjects ) {
//									// Temporarily use the element with that is of the instance.
//									// TODO: Develop means to ensure it is the right object.
//									if ( eClassifier.isInstance( elem ) ) {
//										refObjects.remove( elem );
//			
//										return elem;
//									}
//								}
//							}
//							else {
//								final Object elem = referringObj.eGet( reference );
//								
//								if ( eClassifier.isInstance( elem ) ) {
//									referringObj.eSet( reference, null );
//	
//									return (EObject) elem;
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//		
//		final EFactory factory = p_storyPatternObject.getClassifier().getEPackage().getEFactoryInstance();
//
//		// DB: Template classes
//		EClass actualClass = (EClass) p_storyPatternObject.getClassifier();
//		
//		if ( actualClass.isAbstract() ) {
//			// Search for a rule variable of EClass type indicating the class to be instantiated.
//			for ( final Variable<?> var : patternMatcher.getVariablesScope().getVariables() ) {
//				if ( var.getClassifier() == EcorePackage.Literals.ECLASS ) {
//					actualClass = (EClass) var.getValue();
//					
//					break;
//				}
//			}
//		}
//		
//		return factory.create( actualClass /*(EClass) storyPatternObject.getClassifier()*/);
//	}

	@Override
	public void destroyObjects()
	{
		if (this.isDestroy)
		{
			for (final AbstractStoryPatternObject spo : this.storyPatternObjects)
			{
				if (spo.getModifier() == StoryPatternModifierEnumeration.DESTROY)
				{
					final Variable<EClassifier> variable = this.patternMatcher.getVariablesScope().deleteVariable(spo.getName());

					/*
					 * Variable may be null in case of optional objects and
					 * objects destroyed in a different pattern part.
					 */
					if (variable != null)
					{
						assert variable.getValue() instanceof EObject;

						final EObject instanceObject = (EObject) variable.getValue();

						assert instanceObject != null;

						EcoreUtil.delete(instanceObject, true);

						this.patternMatcher.getNotificationEmitter().instanceObjectDestroyed(spo, instanceObject,
								this.patternMatcher.getVariablesScope(), this.patternMatcher);
					}
				}
			}
		}
	}

	@Override
	public void createLinks() throws SDMException
	{
		if (this.isCreate)
		{
			this.doCreateLink();
		}
	}

	protected abstract void doCreateLink();

	@Override
	public final void destroyLinks(final java.util.Map<AbstractStoryPatternObject, Object> deletedObjects)
	{
		if (this.isDestroy)
		{
			this.doDestroyLink(deletedObjects);
		}

	};

	private int	numberOfIncomingUnidirectionalLinks	= -1;

	public int getNumberOfIncomingUnidirectionalLinks(final AbstractStoryPatternObject spo)
	{
		if (this.numberOfIncomingUnidirectionalLinks == -1)
		{
			int i = 0;

			for (final AbstractStoryPatternLink spl : spo.getIncomingStoryLinks())
			{
				if (!this.isReverseNavigable(spl))
				{
					i++;
				}
			}
			this.numberOfIncomingUnidirectionalLinks = i;
		}

		return this.numberOfIncomingUnidirectionalLinks;
	}

	protected boolean isReverseNavigable(final AbstractStoryPatternLink spl)
	{
		final EClass eClass = spl.eClass();

		if (eClass == SdmPackage.Literals.STORY_PATTERN_CONTAINMENT_LINK)
		{
			return true;
		}
		else if (eClass == SdmPackage.Literals.STORY_PATTERN_EXPRESSION_LINK)
		{
			return false;
		}
		else if (eClass == SdmPackage.Literals.STORY_PATTERN_LINK)
		{
			final StoryPatternLink link = (StoryPatternLink) spl;

			if (link.getEStructuralFeature() instanceof EReference)
			{
				final EReference ref = (EReference) link.getEStructuralFeature();

				return ref.isContainment() || (ref.getEOpposite() != null);
			}
			else
			{
				return false;
			}
		}
		else if (eClass == SdmPackage.Literals.MAP_ENTRY_STORY_PATTERN_LINK)
		{
			return false;
		}
		else if (eClass == SdmPackage.Literals.ECONTAINER_STORY_PATTERN_LINK)
		{
			return true;
		}
		else
		{
			throw new UnsupportedOperationException();
		}
	}

	protected abstract void doDestroyLink(Map<AbstractStoryPatternObject, Object> deletedObjects);

	@Override
	public EMatchType getMatchType()
	{
		return this.matchType;
	}

	protected abstract EMatchType doGetMatchType();
	
	@Override
	public String toString() {
		final StringBuilder strBuilder = new StringBuilder( getClass().getSimpleName() );
		strBuilder.append( " link " );
		strBuilder.append( String.valueOf( getStoryPatternLink() ) );
	
		return strBuilder.toString();
	}
	
	protected void checkUnresolvedProxy( final Object p_object )
	throws SDMException {
		if ( p_object instanceof EObject ) {
			checkUnresolvedProxy( (EObject) p_object ); 
		}
	}
	
	protected void checkUnresolvedProxy( final EObject p_object ) 
	throws SDMException {
		if ( p_object.eIsProxy() ) {
			throw new UnresolvedReferenceException( "Unresolved object " + p_object, EcoreUtil.getURI( p_object ).toString() );
		}
	}
}
