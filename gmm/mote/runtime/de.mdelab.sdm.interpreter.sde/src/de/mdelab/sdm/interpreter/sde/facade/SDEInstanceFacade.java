package de.mdelab.sdm.interpreter.sde.facade;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.mdelab.sdm.interpreter.core.facade.IInstanceFacade;
import de.mdelab.sdm.interpreter.sde.util.ClassHierarchyComparator;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEInstanceFacade implements IInstanceFacade<AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>
{
	private static final ClassHierarchyComparator classHierarchyComp = new ClassHierarchyComparator();

	@Override
	public void setAttributeValue(Object sourceInstanceObject, EStructuralFeature feature, Object value)
	{
//		assert sourceInstanceObject != null;
		assert feature != null;
		assert sourceInstanceObject instanceof EObject;
		
		// DB: Handle abstract features in TGG rules.
		feature = getConcreteFeature( sourceInstanceObject,	feature, value );
		
		if ( feature != null ) {
			assert feature.isChangeable();
	
			( (EObject) sourceInstanceObject ).eSet( feature, value );
		}
	}
	
	@Override
	public EStructuralFeature getConcreteFeature( 	final Object p_sourceInstanceObject,
													final EStructuralFeature p_feature,
													Object p_targetObject ) {
		EStructuralFeature closestFeature = p_feature;
		Class<?> closestFeatureClass = closestFeature.getEType().getInstanceClass();

		// DB: Temp patch for Adele to AADL
		if ( closestFeature.isDerived() ) {
			if ( p_targetObject == null ) {
				if ( p_feature.isMany() ) {
					throw new IllegalStateException( "Features with multiplicity greater than 1 not handled." );
				}
				
				final Object actualObject = ( (EObject) p_sourceInstanceObject ).eGet( p_feature );
				
				if ( actualObject == p_targetObject ) {
					// Return a null feature meaning that nothing needs to be set.
					return null;
				}
				
				p_targetObject = actualObject;
			}

			final EClass sourceObjectClass = ( (EObject) p_sourceInstanceObject ).eClass();
			
			for ( final EStructuralFeature feature : sourceObjectClass.getEAllStructuralFeatures() ) {
				if ( feature.isChangeable() && !feature.isDerived() && feature.getEType().isInstance( p_targetObject ) ) {
					final Class<?> currentFeatureClass = feature.getEType().getInstanceClass();
					final int comparison = classHierarchyComp.compare( closestFeatureClass, currentFeatureClass );
					
					// If the classes are not related (equals), use the current class because it ensure the starting feature is replaced with
					// one that is changeable.
					if ( comparison >= 0 ) {
						closestFeature = feature;
						closestFeatureClass = closestFeature.getEType().getInstanceClass();
					}
				}
			}
		}

		return closestFeature;
	}
}
