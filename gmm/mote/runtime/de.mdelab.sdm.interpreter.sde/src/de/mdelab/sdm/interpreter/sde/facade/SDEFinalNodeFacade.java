package de.mdelab.sdm.interpreter.sde.facade;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import de.hpi.sam.storyDiagramEcore.ActivityParameter;
import de.hpi.sam.storyDiagramEcore.ActivityParameterDirectionEnum;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityFinalNode;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.mdelab.sdm.interpreter.core.SDMInterpreterConstants;
import de.mdelab.sdm.interpreter.core.facade.IFinalNodeFacade;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEFinalNodeFacade extends SDEActivityNodeFacade implements IFinalNodeFacade<ActivityNode, Expression>
{
	@Override
	public Map<String, Expression> getOutParameterExpressions(ActivityNode finalNode)
	{
		assert finalNode instanceof ActivityFinalNode;

		ActivityFinalNode activityFinalNode = (ActivityFinalNode) finalNode;

		Map<String, Expression> map = new HashMap<String, Expression>();

		if (finalNode.getActivity().getSpecification() == null && !finalNode.getActivity().getParameters().isEmpty())
		{
			/*
			 * Get a copy of the list of the activities parameters
			 */
			LinkedList<ActivityParameter> parameters = new LinkedList<ActivityParameter>(finalNode.getActivity().getParameters());

			/*
			 * Remove all IN parameters
			 */
			for (Iterator<ActivityParameter> it = parameters.iterator(); it.hasNext();)
			{
				if (it.next().getDirection() == ActivityParameterDirectionEnum.IN)
				{
					it.remove();
				}
			}

			assert parameters.size() == activityFinalNode.getOutParameterValues().size();

			/*
			 * Create the map of outparameter expressions
			 */
			for (int i = 0; i < parameters.size(); i++)
			{
				map.put(parameters.get(i).getName(), activityFinalNode.getOutParameterValues().get(i));
			}
		}

		/*
		 * In case the activity has a specification and a return value.
		 */
		if (activityFinalNode.getReturnValue() != null)
		{
			map.put(SDMInterpreterConstants.RETURN_VALUE_VAR_NAME, activityFinalNode.getReturnValue());
		}

		return map;
	}
}
