package de.mdelab.sdm.interpreter.sde.expressions;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.Activity;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityEdge;
import de.hpi.sam.storyDiagramEcore.nodes.ActivityNode;
import de.hpi.sam.storyDiagramEcore.nodes.StoryActionNode;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.mdelab.sdm.interpreter.core.expressions.ExpressionInterpreterManager;
import de.mdelab.sdm.interpreter.core.facade.MetamodelFacadeFactory;
import de.mdelab.sdm.interpreter.core.notifications.NotificationEmitter;

public class SDEExpressionInterpreterManager
		extends
		ExpressionInterpreterManager<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression>
{

	public SDEExpressionInterpreterManager(	ClassLoader classLoader,
											NotificationEmitter<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> notificationEmitter,
											MetamodelFacadeFactory<Activity, ActivityNode, ActivityEdge, StoryActionNode, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> facadeFactory ) {
		super( facadeFactory, classLoader, notificationEmitter );
	}
//
//	public SDEExpressionInterpreterManager(ClassLoader classLoader)	{
//		this(classLoader, new SDENotificationEmitter());
//	}
}
