package de.mdelab.sdm.interpreter.sde.facade.adapters;

import org.eclipse.emf.common.notify.impl.AdapterImpl;

/**
 * Holds the AST of an already parsed textual expression.
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class StringExpressionASTHolderAdapter extends AdapterImpl
{
	private Object	ast;

	@Override
	public boolean isAdapterForType(Object type)
	{
		return type == SDEInterpreterExpressionAsdapterFactory.INSTANCE;
	}

	public Object getAST()
	{
		return this.ast;
	}

	public void setAST(Object ast)
	{
		this.ast = ast;
	}
}
