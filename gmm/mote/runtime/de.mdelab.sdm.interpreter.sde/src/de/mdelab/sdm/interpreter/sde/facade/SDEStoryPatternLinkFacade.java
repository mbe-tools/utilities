package de.mdelab.sdm.interpreter.sde.facade;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.ExternalReference;
import de.hpi.sam.storyDiagramEcore.sdm.MapEntryStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.SdmPackage;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternExpressionLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternLink;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.expressions.ExpressionInterpreterManager;
import de.mdelab.sdm.interpreter.core.facade.IStoryPatternLinkFacade;
import de.mdelab.sdm.interpreter.core.variables.Variable;
import de.mdelab.sdm.interpreter.core.variables.VariablesScope;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEStoryPatternLinkFacade implements IStoryPatternLinkFacade<AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> {
	
	private static final int DEFAULT_DERIVED_FEATURE_COST_PENALTY = 1;
//	
//	private Object getFeatureValueFromCache( 	final EObject p_eObject,
//												final EStructuralFeature p_feature ) {
//		CacheAdapter adapter = CacheAdapter.getCacheAdapter( p_eObject );
//		
//		if ( adapter == null ) {
//			adapter = new CacheAdapter();
//		}
//
//		Object result = adapter.get( p_eObject.eResource(), p_eObject, p_feature );
//		
//		if ( result == null ) {
//			result = p_eObject.eGet( p_feature );
//			adapter.put( p_eObject.eResource(), p_eObject, p_feature, result );
//		}
//		
//		return result;
//	}

	@Override
	public String getName(AbstractStoryPatternLink spl)
	{
		EClass splEClass = spl.eClass();

		if (splEClass == SdmPackage.Literals.STORY_PATTERN_LINK) {
			final StoryPatternLink splConcrete = (StoryPatternLink) spl;
			assert splConcrete.getEStructuralFeature() != null || splConcrete.getExternalReference() != null;

			if ( splConcrete.getEStructuralFeature() != null ) {
				return splConcrete.getEStructuralFeature().getName();
			}

			return splConcrete.getExternalReference().getName();
		}
		else if (splEClass == SdmPackage.Literals.STORY_PATTERN_CONTAINMENT_LINK)
		{
			return "[containment]";
		}
		else if (splEClass == SdmPackage.Literals.ECONTAINER_STORY_PATTERN_LINK)
		{
			return "[eContainer]";
		}
		else if (splEClass == SdmPackage.Literals.STORY_PATTERN_EXPRESSION_LINK)
		{
			return ((StoryPatternExpressionLink) spl).getExpression().toString();
		}
		else if (splEClass == SdmPackage.Literals.MAP_ENTRY_STORY_PATTERN_LINK)
		{
			assert ((MapEntryStoryPatternLink) spl).getEStructuralFeature() != null;

			return ((MapEntryStoryPatternLink) spl).getEStructuralFeature().getName();
		}
		else
		{
			throw new UnsupportedOperationException();
		}
	}

	@Override
	public boolean isMany( final AbstractStoryPatternLink p_spl ) {
		final EClass splEClass = p_spl.eClass();

		if ( splEClass == SdmPackage.Literals.STORY_PATTERN_LINK ) {
			final StoryPatternLink storyPatLink = (StoryPatternLink) p_spl;
			final ExternalReference externalReference = storyPatLink.getExternalReference();
			
			if ( externalReference == null ) {
				return storyPatLink.getEStructuralFeature().isMany();
			}
			
			//final CallAction call = externalReference.getCallActions().get( 0 );
//			final EClassifier classifier = externalReference.getClassifier();

//			if ( classifier == EcorePackage.Literals.EE_LIST ) {
//				return true;
//			}
			
			return externalReference.isMany();
		}

		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isReference( final AbstractStoryPatternLink p_spl ) {
		final EClass splEClass = p_spl.eClass();

		if ( splEClass == SdmPackage.Literals.STORY_PATTERN_LINK ) {
			final StoryPatternLink storyPatLink = (StoryPatternLink) p_spl;
			final ExternalReference externalReference = storyPatLink.getExternalReference();
			
			if ( externalReference == null ) {
				return storyPatLink.getEStructuralFeature() instanceof EReference;
			}
			
			return true;
		}

		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isContainment( final AbstractStoryPatternLink p_spl ) {
		final EClass splEClass = p_spl.eClass();

		if ( splEClass == SdmPackage.Literals.STORY_PATTERN_LINK ) {
			final StoryPatternLink storyPatLink = (StoryPatternLink) p_spl;
			final ExternalReference externalReference = storyPatLink.getExternalReference();
			
			if ( externalReference == null ) {
				final EStructuralFeature feature = storyPatLink.getEStructuralFeature();
				
				return feature instanceof EReference && ( (EReference) feature ).isContainment();
			}
			
			return false;
		}

		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasReverseReference( final AbstractStoryPatternLink p_spl ) {
		final EClass splEClass = p_spl.eClass();

		if ( splEClass == SdmPackage.Literals.STORY_PATTERN_LINK ) {
			final StoryPatternLink storyPatLink = (StoryPatternLink) p_spl;
			final ExternalReference externalReference = storyPatLink.getExternalReference();
			
			if ( externalReference == null ) {
				final EStructuralFeature feature = storyPatLink.getEStructuralFeature();
				
				if ( feature instanceof EReference ) {
					final EReference ref = (EReference) feature;
					
					return ref.getEOpposite() != null || storyPatLink.getEOppositeReference() != null;
				}
				
				return false;
			}
			
			return false;
		}

		throw new UnsupportedOperationException();
	}

	@Override
	public boolean reverseReferenceIsMany( final AbstractStoryPatternLink p_spl ) {
		final EClass splEClass = p_spl.eClass();

		if ( splEClass == SdmPackage.Literals.STORY_PATTERN_LINK ) {
			final StoryPatternLink storyPatLink = (StoryPatternLink) p_spl;
			final ExternalReference externalReference = storyPatLink.getExternalReference();
			
			if ( externalReference == null ) {
				if ( storyPatLink.getEOppositeReference() != null ) {
					return storyPatLink.getEOppositeReference().isMany();
				}
				
 				final EStructuralFeature feature = storyPatLink.getEStructuralFeature();
				
				if ( feature instanceof EReference ) {
					final EReference ref = (EReference) feature;
					
					if ( ref.getEOpposite() != null ) {
						return ref.getEOpposite().isMany();
					}
				}
				
				return false;
			}
			
			return false;
		}

		throw new UnsupportedOperationException();
	}

	@Override
	public Object getFeatureValue(	final AbstractStoryPatternLink p_spl,
									final Object p_instanceObject,
									final ExpressionInterpreterManager<?, ?, ?, ?, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> p_exprInterpreter,
									final VariablesScope<?, ?, ?, ?, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> p_variableScope )
	throws SDMException {
		final EClass splEClass = p_spl.eClass();

		if ( splEClass == SdmPackage.Literals.STORY_PATTERN_LINK ) {
			final StoryPatternLink storyPatLink = (StoryPatternLink) p_spl;
			final ExternalReference externalRefExpr = storyPatLink.getExternalReference();
			
			if ( externalRefExpr == null ) {
				final EStructuralFeature feature = storyPatLink.getEStructuralFeature();
				final EObject eObject = (EObject) p_instanceObject;
				
//				if ( feature.isDerived() && feature.isVolatile() ) {
//					return getFeatureValueFromCache( eObject, feature );
//				}
				
				return eObject.eGet( feature );
			}
			
			final Variable<EClassifier> variable = p_exprInterpreter.evaluateExpression( externalRefExpr.getExpression(), null, null, p_variableScope );
			
			return variable.getValue();
		}

		throw new UnsupportedOperationException();
	}

	@Override
	public Object getReverseFeatureValue(	final AbstractStoryPatternLink p_spl,
											final Object p_instanceObject ) {
		final EClass splEClass = p_spl.eClass();

		if ( splEClass == SdmPackage.Literals.STORY_PATTERN_LINK ) {
			final StoryPatternLink storyPatLink = (StoryPatternLink) p_spl;
			
			if ( storyPatLink.getEOppositeReference() != null ) {
				return ( (EObject) p_instanceObject ).eGet( storyPatLink.getEOppositeReference() );
			}

			final EStructuralFeature feature = storyPatLink.getEStructuralFeature();
				
			if ( feature instanceof EReference ) {
				final EReference ref = (EReference) feature;
					
				if ( ref.getEOpposite() != null ) {
					return ( (EObject) p_instanceObject ).eGet( ref.getEOpposite() );
				}
					
				if ( ref.isContainment() ) {
					return ( (EObject) p_instanceObject ).eContainer();
				}
			}
		}

		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isDerived( final AbstractStoryPatternLink p_spl ) {
		final EClass splEClass = p_spl.eClass();

		if ( splEClass == SdmPackage.Literals.STORY_PATTERN_LINK ) {
			final StoryPatternLink storyPatLink = (StoryPatternLink) p_spl;
			final ExternalReference externalReference = storyPatLink.getExternalReference();
			
			if ( externalReference == null ) {
				return storyPatLink.getEStructuralFeature().isDerived();
			}
			
			return true;
		}

		throw new UnsupportedOperationException();
	}

	@Override
	public int matchingCostPenalty( final AbstractStoryPatternLink p_spl ) {
		// TODO: allow declaring this penalty per SPL.
		return isDerived( p_spl ) ? DEFAULT_DERIVED_FEATURE_COST_PENALTY : 1;
	}
}
