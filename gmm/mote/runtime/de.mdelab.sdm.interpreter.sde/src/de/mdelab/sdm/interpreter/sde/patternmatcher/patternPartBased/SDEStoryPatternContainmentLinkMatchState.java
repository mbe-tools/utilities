package de.mdelab.sdm.interpreter.sde.patternmatcher.patternPartBased;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

public class SDEStoryPatternContainmentLinkMatchState extends SDEStoryPatternLinkMatchState<TreeIterator<EObject>>
{
	private EObject	lastContainer;
	
	public SDEStoryPatternContainmentLinkMatchState() {
		super();
	}

	public EObject getLastContainer()
	{
		return this.lastContainer;
	}

	public void setLastContainer(EObject lastContainer)
	{
		this.lastContainer = lastContainer;
	}
}
