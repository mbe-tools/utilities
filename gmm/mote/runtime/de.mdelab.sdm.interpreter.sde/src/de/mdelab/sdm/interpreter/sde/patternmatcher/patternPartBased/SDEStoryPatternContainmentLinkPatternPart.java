package de.mdelab.sdm.interpreter.sde.patternmatcher.patternPartBased;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternLink;
import de.hpi.sam.storyDiagramEcore.sdm.AbstractStoryPatternObject;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternContainmentLink;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternMatchTypeEnumeration;
import de.hpi.sam.storyDiagramEcore.sdm.StoryPatternModifierEnumeration;
import de.mdelab.sdm.interpreter.core.SDMException;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.ECheckResult;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.EMatchType;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.MatchState;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPart;
import de.mdelab.sdm.interpreter.core.patternmatcher.patternPartBased.PatternPartBasedMatcher;
import de.mdelab.sdm.interpreter.core.variables.Variable;

/**
 * 
 * @author Stephan Hildebrandt
 * 
 */
public class SDEStoryPatternContainmentLinkPatternPart extends SDEPatternPart<AbstractStoryPatternObject, StoryPatternContainmentLink>
{

	public SDEStoryPatternContainmentLinkPatternPart(
			final PatternPartBasedMatcher<?, ?, ?, ?, AbstractStoryPatternObject, AbstractStoryPatternLink, EClassifier, EStructuralFeature, Expression> patternMatcher,
			final StoryPatternContainmentLink storyPatternLink)
	{
		super(patternMatcher, storyPatternLink, new AbstractStoryPatternObject[]
		{
				storyPatternLink.getSource(), storyPatternLink.getTarget()
		});

		/*
		 * containment links are never created
		 */
		assert (storyPatternLink.getModifier() != StoryPatternModifierEnumeration.CREATE)
				&& (storyPatternLink.getSource().getModifier() != StoryPatternModifierEnumeration.CREATE)
				&& (storyPatternLink.getTarget().getModifier() != StoryPatternModifierEnumeration.CREATE);

		/*
		 * if a story pattern object is destroyed, the link must also be
		 * destroyed
		 */
		assert (storyPatternLink.getSource().getModifier() != StoryPatternModifierEnumeration.DESTROY)
				|| (storyPatternLink.getModifier() == StoryPatternModifierEnumeration.DESTROY);
		assert (storyPatternLink.getTarget().getModifier() != StoryPatternModifierEnumeration.DESTROY)
				|| (storyPatternLink.getModifier() == StoryPatternModifierEnumeration.DESTROY);

		/*
		 * If one of the link ends is optional, the link must be optional as
		 * well.
		 */
		assert (storyPatternLink.getSource().getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL)
				|| (storyPatternLink.getMatchType() == StoryPatternMatchTypeEnumeration.OPTIONAL);
		assert (storyPatternLink.getTarget().getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL)
				|| (storyPatternLink.getMatchType() == StoryPatternMatchTypeEnumeration.OPTIONAL);
	}

	@Override
	protected void doCreateLink()
	{
		// Do nothing
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void doDestroyLink(final Map<AbstractStoryPatternObject, Object> deletedObjects)
	{
		assert this.storyPatternLink.getModifier() == StoryPatternModifierEnumeration.DESTROY;

		final AbstractStoryPatternObject sourceSpo = this.storyPatternLink.getSource();
		final AbstractStoryPatternObject targetSpo = this.storyPatternLink.getTarget();

		assert (deletedObjects.get(targetSpo) == null) || (deletedObjects.get(targetSpo) instanceof EObject);

		Object sourceInstanceObject = deletedObjects.get(sourceSpo);

		if (sourceInstanceObject == null)
		{
			/*
			 * The source object was not deleted or it is optional and was not
			 * even matched.
			 */
			final Variable<EClassifier> sourceVariable = this.patternMatcher.getVariablesScope().getVariable(sourceSpo.getName());

			if (sourceVariable != null)
			{
				assert sourceSpo.getMatchType() != StoryPatternMatchTypeEnumeration.OPTIONAL;

				sourceInstanceObject = sourceVariable.getValue();
			}
		}

		EObject targetInstanceObject = (EObject) deletedObjects.get(targetSpo);

		if ((sourceInstanceObject != null) && (targetInstanceObject == null))
		{
			assert targetSpo.getModifier() != StoryPatternModifierEnumeration.DESTROY;

			/*
			 * The target object was not destroyed. Destroy the link now.
			 */
			final Variable<EClassifier> targetVariable = this.patternMatcher.getVariablesScope().getVariable(targetSpo.getName());

			assert targetVariable != null;
			assert targetVariable.getValue() instanceof EObject;

			targetInstanceObject = (EObject) targetVariable.getValue();

			final EObject container = targetInstanceObject.eContainer();

			if (!targetInstanceObject.eContainingFeature().isMany())
			{
				container.eSet(targetInstanceObject.eContainingFeature(), null);
			}
			else
			{
				((Collection<EObject>) container.eGet(targetInstanceObject.eContainingFeature())).remove(targetInstanceObject);
			}
		}

		this.patternMatcher.getNotificationEmitter().instanceLinkDestroyed(sourceSpo, sourceInstanceObject, this.storyPatternLink,
				targetSpo, targetInstanceObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);
	}

	@Override
	public ECheckResult check()
	{
		if (this.patternMatcher.isBound(this.storyPatternLink.getSource())
				&& this.patternMatcher.isBound(this.storyPatternLink.getTarget()))
		{
			final AbstractStoryPatternObject sourceSpo = this.storyPatternLink.getSource();
			final AbstractStoryPatternObject targetSpo = this.storyPatternLink.getTarget();

			final EObject sourceInstanceObject = (EObject) this.patternMatcher.getInstanceObject(sourceSpo);
			final Object targetInstanceObject = this.patternMatcher.getInstanceObject(targetSpo);

			final TreeIterator<EObject> it = sourceInstanceObject.eAllContents();

			while (it.hasNext())
			{
				if (it.next() == targetInstanceObject)
				{
					this.patternMatcher.getNotificationEmitter().linkCheckSuccessful(sourceSpo, sourceInstanceObject,
							this.storyPatternLink, targetSpo, targetInstanceObject, this.patternMatcher.getVariablesScope(),
							this.patternMatcher);

					return ECheckResult.OK;
				}
			}

			this.patternMatcher.getNotificationEmitter().linkCheckFailed(sourceSpo, sourceInstanceObject, this.storyPatternLink, targetSpo,
					targetInstanceObject, this.patternMatcher.getVariablesScope(), this.patternMatcher);

			return ECheckResult.FAIL;
		}
//		else
//		{
		return ECheckResult.UNKNOWN;
//		}
	}

	@Override
	public MatchState createMatchState() {
		return new SDEStoryPatternContainmentLinkMatchState();
	}

	@Override
	public boolean match(final MatchState matchState) throws SDMException
	{
		assert matchState != null;
		assert matchState instanceof SDEStoryPatternContainmentLinkMatchState;

		final SDEStoryPatternContainmentLinkMatchState ms = (SDEStoryPatternContainmentLinkMatchState) matchState;

		final AbstractStoryPatternObject sourceSpo = this.storyPatternLink.getSource();
		final AbstractStoryPatternObject targetSpo = this.storyPatternLink.getTarget();

		if (this.patternMatcher.isBound(sourceSpo))
		{
			/*
			 * Use a tree iterator to search all children of the source object
			 * for a match for the target object.
			 */
			assert !this.patternMatcher.isBound(targetSpo);

			final EObject sourceInstanceObject = (EObject) this.patternMatcher.getInstanceObject(sourceSpo);

			this.patternMatcher.getNotificationEmitter().traversingLink(this.storyPatternLink, sourceSpo, sourceInstanceObject, targetSpo,
					this.patternMatcher.getVariablesScope(), this.patternMatcher);

			/*
			 * Find the first target object that matches.
			 */
			TreeIterator<EObject> iterator = ms.getLinkIterator();

			if (iterator == null)
			{
				iterator = sourceInstanceObject.eAllContents();
				ms.setLinkIterator(iterator);
				ms.setSourceInstanceObject(sourceInstanceObject);
				ms.setLastContainer(null);
			}

			while (iterator.hasNext())
			{
				final Object targetObject = iterator.next();

				if (this.patternMatcher.matchStoryPatternObject(targetSpo, targetObject))
				{
					return true;
				}
			}

			this.patternMatcher.getNotificationEmitter().storyPatternObjectNotBound(targetSpo, this.patternMatcher.getVariablesScope(),
					this.patternMatcher);
		}
		else if (this.patternMatcher.isBound(targetSpo))
		{
			/*
			 * Walk the containment hierarchy upwards from the target object
			 * until a match for the source object was found or the end of the
			 * hierarchy was reached.
			 */
			assert !this.patternMatcher.isBound(sourceSpo);

			final EObject targetInstanceObject = (EObject) this.patternMatcher.getInstanceObject(targetSpo);

			this.patternMatcher.getNotificationEmitter().traversingLink(this.storyPatternLink, targetSpo, targetInstanceObject, sourceSpo,
					this.patternMatcher.getVariablesScope(), this.patternMatcher);

			/*
			 * Find the first target object that matches.
			 */
			EObject sourceInstanceObject = ms.getLastContainer();

			if (sourceInstanceObject == null)
			{
				sourceInstanceObject = targetInstanceObject.eContainer();
				ms.setLastContainer(sourceInstanceObject);
				ms.setLinkIterator(null);
				ms.setSourceInstanceObject(targetInstanceObject);
			}

			while (sourceInstanceObject != null)
			{
				if (this.patternMatcher.matchStoryPatternObject(sourceSpo, sourceInstanceObject))
				{
					ms.setLastContainer(sourceInstanceObject);
					return true;
				}

				sourceInstanceObject = sourceInstanceObject.eContainer();
			}

			ms.setLastContainer(null);

			this.patternMatcher.getNotificationEmitter().storyPatternObjectNotBound(sourceSpo, this.patternMatcher.getVariablesScope(),
					this.patternMatcher);
		}
		else
		{
			/*
			 * Should never happen
			 */
			throw new UnsupportedOperationException();
		}

		return false;
	}

	@Override
	public int calculateMatchingCost()
	{
		assert !(this.patternMatcher.isBound(this.storyPatternLink.getSource()) && this.patternMatcher.isBound(this.storyPatternLink
				.getTarget()));

		if (this.patternMatcher.isBound(this.storyPatternLink.getTarget()))
		{
			/*
			 * If the target object is bound, we can go from the target upwards
			 * to the source using eContainer(). Return an estimation of the
			 * expected containment depth.
			 */
			return 5 + this.getNumberOfIncomingUnidirectionalLinks(this.storyPatternLink.getSource());
		}
		else if (this.patternMatcher.isBound(this.storyPatternLink.getSource()))
		{
			/*
			 * If the source is bound, we have to search all children. We cannot
			 * provide an exact estimate.
			 */
			return Integer.MAX_VALUE;
		}
		else
		{
			return PatternPart.MATCHING_NOT_POSSIBLE;
		}
	}

	@Override
	public EMatchType doGetMatchType()
	{
		switch (this.storyPatternLink.getModifier())
		{
			case NONE:
				switch (this.storyPatternLink.getMatchType())
				{
					case NONE:
						return EMatchType.MANDATORY;
					case OPTIONAL:
						return EMatchType.OPTIONAL;
					case NEGATIVE:
						throw new UnsupportedOperationException();
					default:
						throw new UnsupportedOperationException();
				}
			case CREATE:
				throw new UnsupportedOperationException();
			case DESTROY:
				switch (this.storyPatternLink.getMatchType())
				{
					case NONE:
						return EMatchType.MANDATORY;
					case OPTIONAL:
						return EMatchType.OPTIONAL;
					case NEGATIVE:
						throw new UnsupportedOperationException();
					default:
						throw new UnsupportedOperationException();
				}
			default:
				throw new UnsupportedOperationException();
		}
	}
}
