package fr.labsticc.gmm.merge.emfcomp;

import org.eclipse.emf.compare.one.diff.engine.GenericDiffEngine;

public class GmmDiffEngine extends GenericDiffEngine {

	public GmmDiffEngine() {
		super();
	}
	
	/**
	 * Returns the implementation of a {@link org.eclipse.emf.compare.one.diff.engine.check.AbstractCheck}
	 * responsible for the verification of updates on reference values.
	 * 
	 * @return The implementation of a {@link org.eclipse.emf.compare.one.diff.engine.check.AbstractCheck}
	 *         responsible for the verification of updates on reference values.
	 * @since 1.0
	 */
	@Override
	protected FixedReferencesCheck getReferencesChecker() {
		return new FixedReferencesCheck(getMatchManager());
	}
}
