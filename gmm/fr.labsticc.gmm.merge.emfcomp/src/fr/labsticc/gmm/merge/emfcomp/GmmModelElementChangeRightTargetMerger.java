package fr.labsticc.gmm.merge.emfcomp;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.emf.compare.one.EMFComparePlugin;
import org.eclipse.emf.compare.one.FactoryException;
import org.eclipse.emf.compare.one.diff.internal.merge.impl.ModelElementChangeRightTargetMerger;
import org.eclipse.emf.compare.one.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.one.diff.metamodel.ModelElementChangeRightTarget;
import org.eclipse.emf.compare.one.diff.metamodel.ReferenceChangeRightTarget;
import org.eclipse.emf.compare.one.diff.metamodel.ReferenceOrderChange;
import org.eclipse.emf.compare.one.util.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * Merger for an {@link ModelElementChangeRightTarget} operation.<br/>
 * <p>
 * Are considered for this merger :
 * <ul>
 * <li>RemoveModelElement</li>
 * <li>RemoteAddModelElement</li>
 * </ul>
 * </p>
 * 
 * @author <a href="mailto:laurent.goubet@obeo.fr">Laurent Goubet</a>
 */
@SuppressWarnings("restriction")
public class GmmModelElementChangeRightTargetMerger extends ModelElementChangeRightTargetMerger {
	
	public GmmModelElementChangeRightTargetMerger() {
		super();
	}
	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.diff.merge.api.AbstractMerger#doApplyInOrigin()
	 */
	@Override
	public void doApplyInOrigin() {
		final ModelElementChangeRightTarget theDiff = (ModelElementChangeRightTarget)this.diff;
		final EObject origin = theDiff.getLeftParent();
		final EObject element = theDiff.getRightElement();
		final EObject newOne = copy(element);
		final EReference ref = element.eContainmentFeature();
		if (ref != null) {
			// DB: Fixing the multi-level containment hierarchy merge problem.
			if ( origin != null ) {
				try {
					int expectedIndex = -1;
					if (ref.isMany()) {
						final Object containmentRefVal = element.eContainer().eGet(ref);
						if (containmentRefVal instanceof List<?>) {
							@SuppressWarnings("unchecked")
							final List<EObject> listVal = (List<EObject>)containmentRefVal;
							expectedIndex = listVal.indexOf(element);
						}
					}
					EFactory.eAdd(origin, ref.getName(), newOne, expectedIndex, true);
					setXMIID(newOne, getXMIID(element));
				} catch (final FactoryException e) {
					EMFComparePlugin.log(e, true);
				}
			}
		} else if (origin == null && getDiffModel().getLeftRoots().size() > 0) {
			getDiffModel().getLeftRoots().get(0).eResource().getContents().add(newOne);
		} else if (origin != null) {
			origin.eResource().getContents().add(newOne);
		} else {
			// FIXME Throw exception : couldn't merge this
		}
		// we should now have a look for AddReferencesLinks needing this object
		final Iterator<EObject> siblings = getDiffModel().eAllContents();
		while (siblings.hasNext()) {
			final DiffElement op = (DiffElement)siblings.next();
			if (op instanceof ReferenceChangeRightTarget) {
				final ReferenceChangeRightTarget link = (ReferenceChangeRightTarget)op;
				// now if I'm in the target References I should put my copy in the origin
				if (link.getLeftTarget() != null && link.getLeftTarget() == element) {
					link.setRightTarget(newOne);
				}
			} else if (op instanceof ReferenceOrderChange) {
				final ReferenceOrderChange link = (ReferenceOrderChange)op;
				if (link.getLeftElement() == origin && link.getReference() == ref) {
					final ListIterator<EObject> targetIterator = link.getLeftTarget().listIterator();
					boolean replaced = false;
					while (!replaced && targetIterator.hasNext()) {
						final EObject target = targetIterator.next();
						if (target.eIsProxy()
								&& equalProxyURIs(((InternalEObject)target).eProxyURI(),
										EcoreUtil.getURI(element))) {
							targetIterator.set(newOne);
							replaced = true;
						}
					}
				}
			}
		}
	}
}
