package fr.labsticc.gmm.merge.emfcomp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.one.diff.merge.service.MergeService;
import org.eclipse.emf.compare.one.diff.metamodel.AttributeChange;
import org.eclipse.emf.compare.one.diff.metamodel.AttributeOrderChange;
import org.eclipse.emf.compare.one.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.one.diff.metamodel.DiffModel;
import org.eclipse.emf.compare.one.diff.metamodel.ReferenceChange;
import org.eclipse.emf.compare.one.diff.metamodel.ReferenceOrderChange;
import org.eclipse.emf.compare.one.diff.metamodel.UpdateAttribute;
import org.eclipse.emf.compare.one.diff.service.DiffService;
import org.eclipse.emf.compare.one.match.MatchOptions;
import org.eclipse.emf.compare.one.match.metamodel.MatchModel;
import org.eclipse.emf.compare.one.match.service.MatchService;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import fr.labsticc.framework.emf.core.util.EMFUtil;
import fr.labsticc.gmm.merge.service.DefaultGmmDifference;
import fr.labsticc.gmm.merge.service.IGmmDifference;
import fr.labsticc.gmm.merge.service.IGmmMergeService;

/**
 * @author dblouin
 * This is an implementation of the GMM merge service framework making use of the EMF compare framework
 * version 1, but modified to meet the GMM requirements. For example, the complete isolation between the 
 * source and target resources is implemented by a modified EMFCompareEObjectCopier.
 * @see fr.labsticc.gmm.merge.service.IGmmMergeService
 * @see org.eclipse.emf.compare.one.diff.merge.EMFCompareEObjectCopier
 */
public class EmfCompGmmMergeService implements IGmmMergeService {

	private final Comparator<DiffElement> differencesComparator; 
	
	public EmfCompGmmMergeService() {
		differencesComparator = new Comparator<DiffElement>() {
			
			private boolean dependsOn( 	final DiffElement p_difElement1,
										final DiffElement p_difElement2,
										final Set<DiffElement> p_traversedDiffs ) {
				if ( p_difElement1 == null || p_difElement2 == null ) {
					return false;
				}
				
				if ( p_difElement1 == p_difElement2 ) {
					return true;
				}
				
				p_traversedDiffs.add( p_difElement2 );
				
				for ( final DiffElement requiredDiff : p_difElement2.getRequiredBy() ) {
					if ( p_traversedDiffs.contains( requiredDiff ) || dependsOn( p_difElement1, requiredDiff, p_traversedDiffs ) ) {
						return true;
					}
				}
				
				return false;
			}

			@Override
			public int compare(	final DiffElement p_elem1,
								final DiffElement p_elem2 ) {
				final boolean elem1DepensOnElem2 = dependsOn( p_elem1, p_elem2, new HashSet<DiffElement>() );
				final boolean elem2DepensOnElem1 = dependsOn( p_elem2, p_elem1, new HashSet<DiffElement>() );
				
				if ( elem1DepensOnElem2 && !elem2DepensOnElem1 ) {
					return - 1;
				} 
				        
		        if ( elem2DepensOnElem1 && !elem1DepensOnElem2 ) {
		            return 1;
		        } 
						
		        return 0;
			}
		};
	}
	
	/**
	 * Important: synchronize this method because the difference because the  used GenericDiffEngine singleton is not 
	 * multi-thread. Other calls may reset it causing its  matchManager to be set to null while a diff operation is still 
	 * running in another thread. Several threads may access this method when the GMM nature is activated, thus calling
	 * GMM on resources, which may recall GMM in another thread.
	 * @param p_sourceResource
	 * @param p_targetResource
	 * @return true if any change was merged, false otherwise.
	 */
	@Override
	public synchronized boolean merge( 	final Resource p_sourceResource,
										final Resource p_targetResource,
										final boolean pb_useIds,
										final boolean pb_treeWay,
										final Double p_nameSimilarityWeight,
										final Double p_positionSimilarityWeight,
										final Collection<EStructuralFeature> p_orderAgnosticFeatures,
										final Collection<EStructuralFeature> p_ignoredFeatures,
										Map<?, ?> p_options ) {
		// EMF compare does not work when the source resource is empty.
		if ( p_sourceResource.getContents().isEmpty() ) {
			if ( p_targetResource.getContents().isEmpty() ) {
				return false;
			}
			
			p_targetResource.getContents().clear();
		
			return true;
		}
		
		if ( p_targetResource.getContents().isEmpty() ) {
			EMFUtil.copyResourceContent( p_sourceResource, p_targetResource, p_options );
		
			return true;
		}
		
		List<DiffElement> differences = ecDifferences( 	p_sourceResource, 
														p_targetResource,
														pb_useIds,
														pb_treeWay,
														p_nameSimilarityWeight,
														p_positionSimilarityWeight,
														p_orderAgnosticFeatures,
														p_ignoredFeatures );
		
		if ( differences.isEmpty() ) {
			return false;
		}
		
		assert checkUniqueResourceSet( p_sourceResource ) : "Source resource " + p_sourceResource + " makes use of several resource sets.";
		assert checkUniqueResourceSet( p_targetResource ) : "Target resource " + p_targetResource + " makes use of several resource sets.";
		
		int counter = 0;
							// TODO: Dirty fix for the match problem.
		while ( !differences.isEmpty() && counter < 10 ) {
			
			// Merges all differences from model1 to model2
			final List<DiffElement> sortedDifferences = new ArrayList<DiffElement>( differences );
			Collections.sort( sortedDifferences, differencesComparator );
			System.out.println( new Date() + ": Merging changes from " + p_sourceResource.getURI().toString() );
			MergeService.merge( sortedDifferences, true );

			assert checkUniqueResourceSet( p_targetResource ) : "Target resource " + p_targetResource + " makes use of several resource sets after merge from source resource " + p_sourceResource + ".";
			
			differences = ecDifferences( 	p_sourceResource, 
											p_targetResource,
											pb_useIds,
											pb_treeWay,
											p_nameSimilarityWeight,
											p_positionSimilarityWeight,
											p_orderAgnosticFeatures,
											p_ignoredFeatures );
			counter++;
		}
		
		return true;
	}
	
	private boolean checkUniqueResourceSet( final Resource p_resResource ) {
		final Iterator<EObject> contentIt = p_resResource.getAllContents();
		final ResourceSet resSet = p_resResource.getResourceSet();
		
		while ( contentIt.hasNext() ) {
			final EObject element = contentIt.next();
			
			for ( final EObject extObj : element.eCrossReferences() ) {
				final Resource extRes = extObj.eResource();
				
				if ( extRes != null && extRes.getResourceSet() != resSet ) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	@Override
	public EList<IGmmDifference> differences( 	final Resource p_resLeft,
												final Resource p_resRight,
												final boolean pb_useIds,
												final boolean pb_threeWay,
												Double p_nameSimilarityWeight,
												Double p_positionSimilarityWeight,
												final Collection<EStructuralFeature> p_orderAgnosticFeatures,
												final Collection<EStructuralFeature> p_ignoredFeatures ) {
		final EList<IGmmDifference> differences = new BasicEList<IGmmDifference>();
		final Collection<DiffElement> ecDifferences = ecDifferences( 	p_resLeft,
																		p_resRight,
																		pb_useIds,
																		pb_threeWay,
																		p_nameSimilarityWeight,
																		p_positionSimilarityWeight,
																		p_orderAgnosticFeatures,
																		p_ignoredFeatures );
		
		for ( final DiffElement ecDiff : ecDifferences ) {
			final IGmmDifference gmmDif = new DefaultGmmDifference( ecDiff.toString() );
			differences.add( gmmDif );
		}
		
		return differences;
	}
	
	private Map<String, Object> createMatchOptions( final boolean pb_useIds,
													final Double p_nameSimilarityWeight,
													final Double p_positionSimilarityWeight ) {
		final Map<String, Object> matchOptions = new HashMap<String, Object>();
		
		if ( !pb_useIds ) {
			matchOptions.put( MatchOptions.OPTION_IGNORE_ID, Boolean.TRUE );
			matchOptions.put( MatchOptions.OPTION_IGNORE_XMI_ID, Boolean.TRUE );
		}

		if ( p_nameSimilarityWeight != null ) {
			matchOptions.put( MatchOptions.OPTION_MATCH_NAME_WEIGHT, p_nameSimilarityWeight );
		}

		if ( p_positionSimilarityWeight != null ) {
			matchOptions.put( MatchOptions.OPTION_MATCH_POSITION_WEIGHT, p_positionSimilarityWeight );
		}
		
		return matchOptions;
	}
	
	private EList<DiffElement> ecDifferences( 	final Resource p_resLeft,
												final Resource p_resRight,
												final boolean pb_useIds,
												final boolean pb_treeWay,
												final Double p_nameSimilarityWeight,
												final Double p_positionSimilarityWeight,
												final Collection<EStructuralFeature> p_orderAgnosticFeatures,
												final Collection<EStructuralFeature> p_ignoredFeatures ) {
		if ( p_resLeft == p_resRight ) {
			return new BasicEList<DiffElement>();
		}

		try {
			final MatchModel match;
			
			if ( pb_treeWay ) {
				match = MatchService.doResourceMatch( p_resLeft, p_resRight, p_resRight, createMatchOptions( pb_useIds, p_nameSimilarityWeight, p_positionSimilarityWeight ) );
			}
			else {
				match = MatchService.doResourceMatch( p_resLeft, p_resRight, createMatchOptions( pb_useIds, p_nameSimilarityWeight, p_positionSimilarityWeight ) );
			}
			
			final DiffModel diffModel = DiffService.doDiff( match, pb_treeWay );
		
			final EList<DiffElement> differences = filterDifferences( diffModel, pb_useIds, p_orderAgnosticFeatures, p_ignoredFeatures );

			return differences;
		} 
		catch ( final InterruptedException p_ex ) {
			p_ex.printStackTrace();
			
			return null;
		}
	}

	private EList<DiffElement> ecDifferences( 	final EObject p_objLeft,
												final EObject p_objRight,
												final boolean pb_useIds,
												final Double p_nameSimilarityWeight,
												final Double p_positionSimilarityWeight,
												final Collection<EStructuralFeature> p_orderAgnosticFeatures,
												final Collection<EStructuralFeature> p_ignoredFeatures ) {
		try {
			
			final MatchModel match = MatchService.doMatch( p_objLeft, p_objRight, createMatchOptions( pb_useIds, p_nameSimilarityWeight, p_nameSimilarityWeight ) );
			final DiffModel diffModel = DiffService.doDiff( match, false );
		
			final EList<DiffElement> differences = filterDifferences( diffModel, pb_useIds, p_orderAgnosticFeatures, p_ignoredFeatures );

			return differences;
		} 
		catch ( final InterruptedException p_ex ) {
			p_ex.printStackTrace();
			
			return null;
		}
	}
	
	protected boolean shouldConsiderDifference( final DiffElement p_element,
												final boolean pb_useIds,
												final Collection<EStructuralFeature> p_orderAgnosticFeatures,
												final Collection<EStructuralFeature> p_ignoredFeatures ) {
		if ( !pb_useIds && p_element instanceof UpdateAttribute ) {
			final UpdateAttribute updAttElem = (UpdateAttribute) p_element;
			final EAttribute attribute = updAttElem.getAttribute();

			return !attribute.isID();
		}
		
		if ( p_element instanceof ReferenceChange ) {
			final ReferenceChange refChange = (ReferenceChange) p_element;
			final EReference reference = refChange.getReference();
			
			if ( p_ignoredFeatures != null && p_ignoredFeatures.contains( reference ) ) {
				return false;
			}
			
			if ( p_element instanceof ReferenceOrderChange ) {
				if ( !reference.isOrdered() || ( p_orderAgnosticFeatures != null && p_orderAgnosticFeatures.contains( reference ) ) ) {
					return false;
				}
			}
		}
		
		if ( p_element instanceof AttributeChange ) {
			final AttributeChange attChange = (AttributeChange) p_element;
			final EAttribute attribute = attChange.getAttribute();

			if ( p_ignoredFeatures != null && p_ignoredFeatures.contains( attribute ) ) {
				return false;
			}

			if ( p_element instanceof AttributeOrderChange ) {
				if ( !attribute.isOrdered() || ( p_orderAgnosticFeatures != null && p_orderAgnosticFeatures.contains( attribute ) ) ) {
					return false;
				}
			}
		}

		return true;
	}
	
	protected EList<DiffElement> filterDifferences( final DiffModel p_diffModel,
													final boolean pb_useIds,
													final Collection<EStructuralFeature> p_orderAgnosticFeatures,
													final Collection<EStructuralFeature> p_ignoredFeatures ) {
		final EList<DiffElement> filteredDiffs = new BasicEList<DiffElement>();
	
		for ( final DiffElement element : p_diffModel.getDifferences() ) {
			if ( shouldConsiderDifference( element, pb_useIds, p_orderAgnosticFeatures, p_ignoredFeatures ) ) {
				filteredDiffs.add( element );
			}
		}
	
		return filteredDiffs;
	}

	@Override
	public boolean equals(	final EObject p_objLeft, 
							final EObject p_objRight,
							final boolean pb_useIds,
							Double p_nameSimilarityWeight,
							Double p_positionSimilarityWeight ) {
		return ecDifferences( p_objLeft, p_objRight, pb_useIds, p_nameSimilarityWeight, p_positionSimilarityWeight, null, null ).isEmpty();
	}

	@Override
	public boolean equals(	final Resource p_resLeft,
							final Resource p_resRight,
							boolean pb_useIds,
							Double p_nameSimilarityWeight,
							Double p_positionSimilarityWeight ) {
		return ecDifferences( p_resLeft, p_resRight, pb_useIds, false, p_nameSimilarityWeight, p_positionSimilarityWeight, null, null ).isEmpty();
	}
}
