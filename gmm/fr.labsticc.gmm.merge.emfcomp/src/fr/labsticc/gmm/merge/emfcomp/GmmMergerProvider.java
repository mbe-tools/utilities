package fr.labsticc.gmm.merge.emfcomp;

import java.util.Map;

import org.eclipse.emf.compare.one.diff.internal.merge.DefaultMergerProvider;
import org.eclipse.emf.compare.one.diff.merge.IMerger;
import org.eclipse.emf.compare.one.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.one.diff.metamodel.ModelElementChangeLeftTarget;
import org.eclipse.emf.compare.one.diff.metamodel.ModelElementChangeRightTarget;
import org.eclipse.emf.compare.one.diff.metamodel.UpdateReference;

@SuppressWarnings("restriction")
public class GmmMergerProvider extends DefaultMergerProvider {

	public GmmMergerProvider() {
		final Map<Class<? extends DiffElement>, Class<? extends IMerger>> mergerTypes = getMergers();
		mergerTypes.put(ModelElementChangeRightTarget.class, GmmModelElementChangeRightTargetMerger.class);
		mergerTypes.put(ModelElementChangeLeftTarget.class, GmmModelElementChangeLeftTargetMerger.class);
		mergerTypes.put(UpdateReference.class, GmmUpdateReferenceMerger.class);
	}
}
