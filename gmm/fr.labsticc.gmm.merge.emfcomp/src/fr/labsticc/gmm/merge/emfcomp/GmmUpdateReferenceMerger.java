package fr.labsticc.gmm.merge.emfcomp;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.compare.one.diff.internal.merge.impl.UpdateReferenceMerger;
import org.eclipse.emf.compare.one.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.one.diff.metamodel.ModelElementChangeLeftTarget;

@SuppressWarnings("restriction")
public class GmmUpdateReferenceMerger extends UpdateReferenceMerger {
	
	public GmmUpdateReferenceMerger() {
		super();
	}

	@Override
	protected List<DiffElement> getDependencies(boolean applyInOrigin) {
		final List<DiffElement> diffs = diff.getRequires();
		final List<DiffElement> result = new ArrayList<DiffElement>();

		for (DiffElement diffElement : diffs) {
		// DB: Deletions should not be dependencies and merged at the end.
		/*	if (applyInOrigin && diffElement instanceof ModelElementChangeRightTarget) {
				result.add(diffElement);
			} else*/ if (!applyInOrigin && diffElement instanceof ModelElementChangeLeftTarget) {
				result.add(diffElement);
			}
		}
		
		return result;
	}
}
