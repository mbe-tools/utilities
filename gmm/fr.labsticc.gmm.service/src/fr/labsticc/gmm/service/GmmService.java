package fr.labsticc.gmm.service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.jface.viewers.ILabelProvider;

import fr.labsticc.framework.constraints.core.exception.ConstraintException;
import fr.labsticc.framework.constraints.model.constraints.CardinalityConstraint;
import fr.labsticc.framework.constraints.model.constraints.Constraint;
import fr.labsticc.framework.constraints.model.constraints.EvalContext;
import fr.labsticc.framework.constraints.model.service.IConstraintExpressionServicesFacade;
import fr.labsticc.framework.core.exception.SystemException;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.MetaModelSubset;
import fr.labsticc.gmm.model.megamodel.Model;
import fr.labsticc.gmm.model.megamodel.SetOperation;
import fr.labsticc.gmm.model.megamodel.SubsettedMetaModel;

public class GmmService {
	
	private final IConstraintExpressionServicesFacade expressionEvaluator;

	private final ILabelProvider labelProvider;

	public GmmService( 	final IConstraintExpressionServicesFacade p_expressionEvaluator,
						final ILabelProvider p_labelProvider ) {
		super();
		
		expressionEvaluator = p_expressionEvaluator;
		labelProvider = p_labelProvider;
	}
	
	public SubsettedMetaModel getSubsettedMetaModel( 	final Resource p_modelResource,
														final Model p_megaModel ) {
		return getSubsettedMetaModel( p_modelResource.getURI(), p_megaModel );
	}

	public SubsettedMetaModel getSubsettedMetaModel( 	final URI p_modelResourceUri,
														final Model p_megaModel ) {
		for ( final Model model : p_megaModel.getOwnedModels() ) {
			if ( model instanceof MetaModel && ( (MetaModel) model ).declares( p_modelResourceUri ) ) {
				for ( final Model subModel : model.getOwnedModels() ) {
					if ( subModel instanceof SubsettedMetaModel && ( (SubsettedMetaModel) subModel ).declares( p_modelResourceUri ) ) {
						return (SubsettedMetaModel) subModel;
					}
				}
			}
		}
		
		return null;
	}
	
	public boolean isAllowed( 	final EModelElement p_modelElement,
								final SubsettedMetaModel p_metaModel )
	throws ConstraintException, SystemException {
		return isAllowed( p_modelElement, p_metaModel.getSubset() );
	}

	public boolean isAllowed( 	final EModelElement p_modelElement,
								final MetaModelSubset p_metaModelSubset )
	throws ConstraintException, SystemException {
		final EvalContext context = expressionEvaluator.createEvalContext( p_modelElement );
		
		for ( final CardinalityConstraint cardConstraint : p_metaModelSubset.getOwnedConstraints() ) {
			final fr.labsticc.framework.constraints.model.constraints.Diagnostic result = expressionEvaluator.evaluate( cardConstraint, context );
			
			if ( !result.isValid() ) {
				return false;
			}
		}

		for( final SetOperation setOperation : p_metaModelSubset.getOwnedCompositionOperations() ) {
			final MetaModelSubset targetSubset = setOperation.getTargetSubset();
			
			switch ( setOperation.getType() ) {
				case INTERSECTION:
					if ( !isAllowed( p_modelElement, targetSubset ) ) {
						return false;
					}
				
					break;
				
				case UNION:
					if ( isAllowed( p_modelElement, targetSubset ) ) {
						return true;
					}
					
					break;
	
				default:
					throw new UnsupportedOperationException( "Usupported set operation; " + setOperation.getType() + "." );
			}
		}
		
		return true;
	}

	public void validate( 	final EValidator p_validator,
							final Model p_megaModel,
							final EObject p_eObject,
							final DiagnosticChain p_diagnostics,
							final Map<Object, Object> p_context ) {
		final SubsettedMetaModel subMetaModel = getSubsettedMetaModel( p_eObject.eResource(), p_megaModel );
		
		if ( subMetaModel != null ) {
			final EvalContext context = expressionEvaluator.createEvalContext( p_eObject );
			
			for ( final CardinalityConstraint cardConstraint : subMetaModel.getSubset().getOwnedConstraints() ) {
				try {
					final fr.labsticc.framework.constraints.model.constraints.Diagnostic result = expressionEvaluator.evaluate( cardConstraint, context );
					
					if ( !result.isValid() ) {
						createDiagnostic( p_validator, p_eObject, result, p_diagnostics, p_context, cardConstraint );
					}
				}
				catch( final SystemException p_ex ) {
					throw new RuntimeException( p_ex );
				}
				catch( final ConstraintException p_ex ) {
					p_ex.printStackTrace();
					
					// FIXME: Add exception message.
					createDiagnostic( p_validator, p_eObject, p_ex, p_diagnostics, p_context, cardConstraint );
				}
			}
		}
	}
	  
	private void createDiagnostic(	final EValidator p_validator,
									final EObject p_contextObj,
									final ConstraintException p_ex,
									final DiagnosticChain p_diagnostics,
									final Map<Object, Object> p_context,
									final Constraint p_constraint ) {
		if ( p_diagnostics != null ) {
			final Object[] messageSubstitutions = new Object[] { p_constraint.getName(), labelProvider.getText( p_contextObj ) };
			final StringBuilder message = new StringBuilder( getString( "_UI_GenericConstraint_diagnostic", messageSubstitutions ) );
			message.append( " due to exception " );
			message.append( p_ex.getLocalizedMessage() );
			final List<EObject> rootAndRelatedObjects = new ArrayList<EObject>();
			rootAndRelatedObjects.add( p_contextObj );
			
			p_diagnostics.add( new BasicDiagnostic( Diagnostic.ERROR, getSource( p_validator ), 0, message.toString(), rootAndRelatedObjects.toArray() ) );
		}
	}
	
	private void createDiagnostic(	final EValidator p_validator,
									final EObject p_contextObj,
									final fr.labsticc.framework.constraints.model.constraints.Diagnostic p_fwDiagnostic,
									final DiagnosticChain p_diagnostics,
									final Map<Object, Object> p_context,
									final Constraint p_constraint ) {
		if ( p_diagnostics != null ) {
			final Object[] messageSubstitutions = new Object[] { p_constraint.getName(), labelProvider.getText( p_contextObj ) };
		    final String message = getString( "_UI_GenericConstraint_diagnostic", messageSubstitutions );
		    final List<EObject> rootAndRelatedObjects = new ArrayList<EObject>();
		    rootAndRelatedObjects.add( p_contextObj );
		    rootAndRelatedObjects.addAll( p_fwDiagnostic.getRelatedModelElements() );
	   
		    p_diagnostics.add( new BasicDiagnostic( Diagnostic.ERROR, getSource( p_validator ), 0, message, rootAndRelatedObjects.toArray() ) );
		}
	}
	
	private String getSource( final EValidator p_validator ) {
		try {
			final Field sourceField = p_validator.getClass().getField( "DIAGNOSTIC_SOURCE" );
			
			return (String) sourceField.get( p_validator );
		}
		catch ( final NoSuchFieldException p_ex ) {
			return EObjectValidator.DIAGNOSTIC_SOURCE;
		} 
		catch ( final SecurityException p_ex ) {
			throw new RuntimeException( p_ex );
		}
		catch ( final IllegalAccessException p_ex ) {
			throw new RuntimeException( p_ex );
		}
	}

	private String getString(	final String p_key,
								final Object[] p_substitutions ) {
	    return p_substitutions == null ? getResourceLocator().getString( p_key ) : getResourceLocator().getString( p_key, p_substitutions );
	}

	private ResourceLocator getResourceLocator() {
	    return EcorePlugin.INSTANCE;
	}
}
