/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation;
import fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation;
import fr.labsticc.gmm.model.megamodel.FactualRelation;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation;
import fr.labsticc.gmm.model.megamodel.Model;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Meta Model Related Consistency Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModelRelatedConsistencyRelationImpl#getMetaModels <em>Meta Models</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModelRelatedConsistencyRelationImpl#getLeftMetaModel <em>Left Meta Model</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModelRelatedConsistencyRelationImpl#getRightMetaModel <em>Right Meta Model</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModelRelatedConsistencyRelationImpl#isDeletingLeft <em>Deleting Left</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModelRelatedConsistencyRelationImpl#isDeletingRight <em>Deleting Right</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class BinaryMetaModelRelatedConsistencyRelationImpl extends ObligationRelationImpl implements BinaryMetaModelRelatedConsistencyRelation {
	/**
	 * The cached value of the '{@link #getMetaModels() <em>Meta Models</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetaModels()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap metaModels;

	/**
	 * The default value of the '{@link #isDeletingLeft() <em>Deleting Left</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeletingLeft()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DELETING_LEFT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDeletingLeft() <em>Deleting Left</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeletingLeft()
	 * @generated
	 * @ordered
	 */
	protected boolean deletingLeft = DELETING_LEFT_EDEFAULT;

	/**
	 * The default value of the '{@link #isDeletingRight() <em>Deleting Right</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeletingRight()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DELETING_RIGHT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDeletingRight() <em>Deleting Right</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeletingRight()
	 * @generated
	 * @ordered
	 */
	protected boolean deletingRight = DELETING_RIGHT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryMetaModelRelatedConsistencyRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getMetaModels() {
		if (metaModels == null) {
			metaModels = new BasicFeatureMap(this, MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__META_MODELS);
		}
		return metaModels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel getLeftMetaModel() {
		MetaModel leftMetaModel = basicGetLeftMetaModel();
		return leftMetaModel != null && leftMetaModel.eIsProxy() ? (MetaModel)eResolveProxy((InternalEObject)leftMetaModel) : leftMetaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel basicGetLeftMetaModel() {
		return (MetaModel)getMetaModels().get(MegamodelPackage.Literals.BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeftMetaModel(MetaModel newLeftMetaModel) {
		((FeatureMap.Internal)getMetaModels()).set(MegamodelPackage.Literals.BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL, newLeftMetaModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel getRightMetaModel() {
		MetaModel rightMetaModel = basicGetRightMetaModel();
		return rightMetaModel != null && rightMetaModel.eIsProxy() ? (MetaModel)eResolveProxy((InternalEObject)rightMetaModel) : rightMetaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel basicGetRightMetaModel() {
		return (MetaModel)getMetaModels().get(MegamodelPackage.Literals.BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightMetaModel(MetaModel newRightMetaModel) {
		((FeatureMap.Internal)getMetaModels()).set(MegamodelPackage.Literals.BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL, newRightMetaModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDeletingLeft() {
		return deletingLeft;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeletingLeft(boolean newDeletingLeft) {
		boolean oldDeletingLeft = deletingLeft;
		deletingLeft = newDeletingLeft;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_LEFT, oldDeletingLeft, deletingLeft));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDeletingRight() {
		return deletingRight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeletingRight(boolean newDeletingRight) {
		boolean oldDeletingRight = deletingRight;
		deletingRight = newDeletingRight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_RIGHT, oldDeletingRight, deletingRight));
	}

	@Override
	public boolean concerns(EObject modelElement) {
		if ( modelElement == null ) {
			return false;
		}
		
		for ( final MetaModel metaModel : getMetaModelsList() ) {
			if ( metaModel.declares( modelElement ) ) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<MetaModel> getMetaModelsList() {
		final EList<MetaModel> models = new BasicEList<MetaModel>();
		final Iterator<Object> modelsIt = getMetaModels().valueListIterator();
		
		while ( modelsIt.hasNext() ) {
			models.add( (MetaModel) modelsIt.next() );
		}
		
		return models;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__META_MODELS:
				return ((InternalEList<?>)getMetaModels()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__META_MODELS:
				if (coreType) return getMetaModels();
				return ((FeatureMap.Internal)getMetaModels()).getWrapper();
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__LEFT_META_MODEL:
				if (resolve) return getLeftMetaModel();
				return basicGetLeftMetaModel();
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__RIGHT_META_MODEL:
				if (resolve) return getRightMetaModel();
				return basicGetRightMetaModel();
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_LEFT:
				return isDeletingLeft();
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_RIGHT:
				return isDeletingRight();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__META_MODELS:
				((FeatureMap.Internal)getMetaModels()).set(newValue);
				return;
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__LEFT_META_MODEL:
				setLeftMetaModel((MetaModel)newValue);
				return;
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__RIGHT_META_MODEL:
				setRightMetaModel((MetaModel)newValue);
				return;
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_LEFT:
				setDeletingLeft((Boolean)newValue);
				return;
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_RIGHT:
				setDeletingRight((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__META_MODELS:
				getMetaModels().clear();
				return;
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__LEFT_META_MODEL:
				setLeftMetaModel((MetaModel)null);
				return;
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__RIGHT_META_MODEL:
				setRightMetaModel((MetaModel)null);
				return;
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_LEFT:
				setDeletingLeft(DELETING_LEFT_EDEFAULT);
				return;
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_RIGHT:
				setDeletingRight(DELETING_RIGHT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__META_MODELS:
				return metaModels != null && !metaModels.isEmpty();
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__LEFT_META_MODEL:
				return basicGetLeftMetaModel() != null;
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__RIGHT_META_MODEL:
				return basicGetRightMetaModel() != null;
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_LEFT:
				return deletingLeft != DELETING_LEFT_EDEFAULT;
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_RIGHT:
				return deletingRight != DELETING_RIGHT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == FactualRelation.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == MetaModelRelatedRelation.class) {
			switch (derivedFeatureID) {
				case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__META_MODELS: return MegamodelPackage.META_MODEL_RELATED_RELATION__META_MODELS;
				default: return -1;
			}
		}
		if (baseClass == BinaryMetaModeRelatedlRelation.class) {
			switch (derivedFeatureID) {
				case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__LEFT_META_MODEL: return MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL;
				case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__RIGHT_META_MODEL: return MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == FactualRelation.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == MetaModelRelatedRelation.class) {
			switch (baseFeatureID) {
				case MegamodelPackage.META_MODEL_RELATED_RELATION__META_MODELS: return MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__META_MODELS;
				default: return -1;
			}
		}
		if (baseClass == BinaryMetaModeRelatedlRelation.class) {
			switch (baseFeatureID) {
				case MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL: return MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__LEFT_META_MODEL;
				case MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL: return MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__RIGHT_META_MODEL;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean canDelete( final Model p_model ) {
		if ( super.canDelete( p_model ) ) {
			return true;
		}
		
		if ( 	( getLeftMetaModel().declares( p_model ) && isDeletingLeft() ) ||
				( getRightMetaModel().declares( p_model ) && isDeletingRight() ) ) {
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean concerns(final URI resourceUri) {
		if ( resourceUri == null ) {
			return false;
		}
		
		for ( final MetaModel metaModel : getMetaModelsList() ) {
			if ( metaModel.declares( resourceUri ) ) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (metaModels: ");
		result.append(metaModels);
		result.append(", deletingLeft: ");
		result.append(deletingLeft);
		result.append(", deletingRight: ");
		result.append(deletingRight);
		result.append(')');
		return result.toString();
	}

} //BinaryMetaModelRelatedConsistencyRelationImpl
