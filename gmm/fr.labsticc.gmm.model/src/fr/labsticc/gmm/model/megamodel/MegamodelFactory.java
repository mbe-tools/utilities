/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage
 * @generated
 */
public interface MegamodelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MegamodelFactory eINSTANCE = fr.labsticc.gmm.model.megamodel.impl.MegamodelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Gmm Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Gmm Specification</em>'.
	 * @generated
	 */
	GmmSpecification createGmmSpecification();

	/**
	 * Returns a new object of class '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model</em>'.
	 * @generated
	 */
	Model createModel();

	/**
	 * Returns a new object of class '<em>Error Description</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Error Description</em>'.
	 * @generated
	 */
	ErrorDescription createErrorDescription();

	/**
	 * Returns a new object of class '<em>Conformance Relation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conformance Relation</em>'.
	 * @generated
	 */
	ConformanceRelation createConformanceRelation();

	/**
	 * Returns a new object of class '<em>Meta Model Subset</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Meta Model Subset</em>'.
	 * @generated
	 */
	MetaModelSubset createMetaModelSubset();

	/**
	 * Returns a new object of class '<em>Activity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Activity</em>'.
	 * @generated
	 */
	Activity createActivity();

	/**
	 * Returns a new object of class '<em>Subset Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subset Relationship</em>'.
	 * @generated
	 */
	SubsetRelationship createSubsetRelationship();

	/**
	 * Returns a new object of class '<em>Subsetted Meta Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subsetted Meta Model</em>'.
	 * @generated
	 */
	SubsettedMetaModel createSubsettedMetaModel();

	/**
	 * Returns a new object of class '<em>Project Uri Rel Bin Rel Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Project Uri Rel Bin Rel Policy</em>'.
	 * @generated
	 */
	ProjectUriRelBinRelPolicy createProjectUriRelBinRelPolicy();

	/**
	 * Returns a new object of class '<em>Execution Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Execution Context</em>'.
	 * @generated
	 */
	ExecutionContext createExecutionContext();

	/**
	 * Returns a new object of class '<em>Gmm Engine</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Gmm Engine</em>'.
	 * @generated
	 */
	GmmEngine createGmmEngine();

	/**
	 * Returns a new object of class '<em>Set Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Set Operation</em>'.
	 * @generated
	 */
	SetOperation createSetOperation();

	/**
	 * Returns a new object of class '<em>Tool</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tool</em>'.
	 * @generated
	 */
	Tool createTool();

	/**
	 * Returns a new object of class '<em>Uri Related Binary Rel Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uri Related Binary Rel Policy</em>'.
	 * @generated
	 */
	UriRelatedBinaryRelPolicy createUriRelatedBinaryRelPolicy();

	/**
	 * Returns a new object of class '<em>Modeling Environment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Modeling Environment</em>'.
	 * @generated
	 */
	ModelingEnvironment createModelingEnvironment();

	/**
	 * Returns a new object of class '<em>Meta Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Meta Model</em>'.
	 * @generated
	 */
	MetaModel createMetaModel();

	/**
	 * Returns a new object of class '<em>Comparison Settings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Comparison Settings</em>'.
	 * @generated
	 */
	ComparisonSettings createComparisonSettings();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MegamodelPackage getMegamodelPackage();

} //MegamodelFactory
