/**
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.common.util.URI;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gmm Engine</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.GmmEngine#getMegaModel <em>Mega Model</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getGmmEngine()
 * @model
 * @generated
 */
public interface GmmEngine extends NamedElement {

	/**
	 * Returns the value of the '<em><b>Mega Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mega Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mega Model</em>' reference.
	 * @see #setMegaModel(Model)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getGmmEngine_MegaModel()
	 * @model required="true"
	 * @generated
	 */
	Model getMegaModel();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.GmmEngine#getMegaModel <em>Mega Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mega Model</em>' reference.
	 * @see #getMegaModel()
	 * @generated
	 */
	void setMegaModel(Model value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model changedUriDataType="fr.labsticc.gmm.model.megamodel.URI" changedUriRequired="true" executionContextRequired="true"
	 * @generated
	 */
	void establishValidity(URI changedUri, ExecutionContext executionContext);
} // GmmEngine
