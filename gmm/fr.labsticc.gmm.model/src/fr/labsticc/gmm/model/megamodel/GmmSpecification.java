/**
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gmm Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.GmmSpecification#getOwnedActivities <em>Owned Activities</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getGmmSpecification()
 * @model
 * @generated
 */
public interface GmmSpecification extends Model {

	/**
	 * Returns the value of the '<em><b>Owned Activities</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.gmm.model.megamodel.Activity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Activities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Activities</em>' containment reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getGmmSpecification_OwnedActivities()
	 * @model containment="true"
	 * @generated
	 */
	EList<Activity> getOwnedActivities();

} // GmmSpecification
