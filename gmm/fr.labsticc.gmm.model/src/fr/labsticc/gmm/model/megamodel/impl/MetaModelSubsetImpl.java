/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.labsticc.framework.constraints.model.constraints.CardinalityConstraint;
import fr.labsticc.gmm.model.megamodel.Activity;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModelSubset;
import fr.labsticc.gmm.model.megamodel.SetOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Meta Model Subset</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelSubsetImpl#getOwnedConstraints <em>Owned Constraints</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelSubsetImpl#getActivities <em>Activities</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelSubsetImpl#getOwnedCompositionOperations <em>Owned Composition Operations</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MetaModelSubsetImpl extends NamedElementImpl implements MetaModelSubset {
	/**
	 * The cached value of the '{@link #getOwnedConstraints() <em>Owned Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<CardinalityConstraint> ownedConstraints;

	/**
	 * The cached value of the '{@link #getActivities() <em>Activities</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivities()
	 * @generated
	 * @ordered
	 */
	protected EList<Activity> activities;

	/**
	 * The cached value of the '{@link #getOwnedCompositionOperations() <em>Owned Composition Operations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedCompositionOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<SetOperation> ownedCompositionOperations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetaModelSubsetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.META_MODEL_SUBSET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CardinalityConstraint> getOwnedConstraints() {
		if (ownedConstraints == null) {
			ownedConstraints = new EObjectContainmentEList<CardinalityConstraint>(CardinalityConstraint.class, this, MegamodelPackage.META_MODEL_SUBSET__OWNED_CONSTRAINTS);
		}
		return ownedConstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Activity> getActivities() {
		if (activities == null) {
			activities = new EObjectWithInverseResolvingEList.ManyInverse<Activity>(Activity.class, this, MegamodelPackage.META_MODEL_SUBSET__ACTIVITIES, MegamodelPackage.ACTIVITY__REQUIRED_SUBSETS);
		}
		return activities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SetOperation> getOwnedCompositionOperations() {
		if (ownedCompositionOperations == null) {
			ownedCompositionOperations = new EObjectContainmentEList<SetOperation>(SetOperation.class, this, MegamodelPackage.META_MODEL_SUBSET__OWNED_COMPOSITION_OPERATIONS);
		}
		return ownedCompositionOperations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL_SUBSET__ACTIVITIES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getActivities()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL_SUBSET__OWNED_CONSTRAINTS:
				return ((InternalEList<?>)getOwnedConstraints()).basicRemove(otherEnd, msgs);
			case MegamodelPackage.META_MODEL_SUBSET__ACTIVITIES:
				return ((InternalEList<?>)getActivities()).basicRemove(otherEnd, msgs);
			case MegamodelPackage.META_MODEL_SUBSET__OWNED_COMPOSITION_OPERATIONS:
				return ((InternalEList<?>)getOwnedCompositionOperations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL_SUBSET__OWNED_CONSTRAINTS:
				return getOwnedConstraints();
			case MegamodelPackage.META_MODEL_SUBSET__ACTIVITIES:
				return getActivities();
			case MegamodelPackage.META_MODEL_SUBSET__OWNED_COMPOSITION_OPERATIONS:
				return getOwnedCompositionOperations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL_SUBSET__OWNED_CONSTRAINTS:
				getOwnedConstraints().clear();
				getOwnedConstraints().addAll((Collection<? extends CardinalityConstraint>)newValue);
				return;
			case MegamodelPackage.META_MODEL_SUBSET__ACTIVITIES:
				getActivities().clear();
				getActivities().addAll((Collection<? extends Activity>)newValue);
				return;
			case MegamodelPackage.META_MODEL_SUBSET__OWNED_COMPOSITION_OPERATIONS:
				getOwnedCompositionOperations().clear();
				getOwnedCompositionOperations().addAll((Collection<? extends SetOperation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL_SUBSET__OWNED_CONSTRAINTS:
				getOwnedConstraints().clear();
				return;
			case MegamodelPackage.META_MODEL_SUBSET__ACTIVITIES:
				getActivities().clear();
				return;
			case MegamodelPackage.META_MODEL_SUBSET__OWNED_COMPOSITION_OPERATIONS:
				getOwnedCompositionOperations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL_SUBSET__OWNED_CONSTRAINTS:
				return ownedConstraints != null && !ownedConstraints.isEmpty();
			case MegamodelPackage.META_MODEL_SUBSET__ACTIVITIES:
				return activities != null && !activities.isEmpty();
			case MegamodelPackage.META_MODEL_SUBSET__OWNED_COMPOSITION_OPERATIONS:
				return ownedCompositionOperations != null && !ownedCompositionOperations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MetaModelSubsetImpl
