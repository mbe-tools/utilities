/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel;

import fr.labsticc.framework.constraints.model.constraints.Target;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Meta Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.MetaModel#getPackages <em>Packages</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.MetaModel#getOwnedSubsets <em>Owned Subsets</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.MetaModel#getOwnedComparisonSettings <em>Owned Comparison Settings</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.MetaModel#getOwnedConstraintTargets <em>Owned Constraint Targets</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getMetaModel()
 * @model
 * @generated
 */
public interface MetaModel extends Model {
	/**
	 * Returns the value of the '<em><b>Packages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EPackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packages</em>' reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getMetaModel_Packages()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<EPackage> getPackages();

	/**
	 * Returns the value of the '<em><b>Owned Subsets</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.gmm.model.megamodel.MetaModelSubset}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Subsets</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Subsets</em>' containment reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getMetaModel_OwnedSubsets()
	 * @model containment="true"
	 * @generated
	 */
	EList<MetaModelSubset> getOwnedSubsets();

	/**
	 * Returns the value of the '<em><b>Owned Comparison Settings</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Comparison Settings</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Comparison Settings</em>' containment reference.
	 * @see #setOwnedComparisonSettings(ComparisonSettings)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getMetaModel_OwnedComparisonSettings()
	 * @model containment="true"
	 * @generated
	 */
	ComparisonSettings getOwnedComparisonSettings();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.MetaModel#getOwnedComparisonSettings <em>Owned Comparison Settings</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owned Comparison Settings</em>' containment reference.
	 * @see #getOwnedComparisonSettings()
	 * @generated
	 */
	void setOwnedComparisonSettings(ComparisonSettings value);

	/**
	 * Returns the value of the '<em><b>Owned Constraint Targets</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.framework.constraints.model.constraints.Target}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Constraint Targets</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Constraint Targets</em>' containment reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getMetaModel_OwnedConstraintTargets()
	 * @model containment="true"
	 * @generated
	 */
	EList<Target> getOwnedConstraintTargets();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" instanceRequired="true"
	 * @generated
	 */
	boolean declares(EObject instance);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 * @generated
	 */
	double getNameSimilarityWeight();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 * @generated
	 */
	double getPositionSimilarityWeight();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	boolean useIdForCompare();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Check that the URIr file extension is included in the allowed file extensions.
	 * <!-- end-model-doc -->
	 * @model required="true" uriDataType="fr.labsticc.gmm.model.megamodel.URI" uriRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ( uri == null ) {\r\n\treturn false;\r\n}\r\n\r\nreturn getFileExtensions().contains(  uri.fileExtension() );'"
	 * @generated
	 */
	boolean declares(URI uri);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" modelRequired="true"
	 * @generated
	 */
	boolean declares(Model model);

} // MetaModel
