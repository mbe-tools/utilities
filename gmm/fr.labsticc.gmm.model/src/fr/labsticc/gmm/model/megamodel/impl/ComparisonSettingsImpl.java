/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import fr.labsticc.gmm.model.megamodel.ComparisonSettings;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Comparison Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ComparisonSettingsImpl#isUseIdForCompare <em>Use Id For Compare</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ComparisonSettingsImpl#getNameSimilarityWeight <em>Name Similarity Weight</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ComparisonSettingsImpl#getPositionSimilarityWeight <em>Position Similarity Weight</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComparisonSettingsImpl extends EObjectImpl implements ComparisonSettings {
	/**
	 * The default value of the '{@link #isUseIdForCompare() <em>Use Id For Compare</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseIdForCompare()
	 * @generated
	 * @ordered
	 */
	protected static final boolean USE_ID_FOR_COMPARE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUseIdForCompare() <em>Use Id For Compare</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseIdForCompare()
	 * @generated
	 * @ordered
	 */
	protected boolean useIdForCompare = USE_ID_FOR_COMPARE_EDEFAULT;

	/**
	 * The default value of the '{@link #getNameSimilarityWeight() <em>Name Similarity Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameSimilarityWeight()
	 * @generated
	 * @ordered
	 */
	protected static final Double NAME_SIMILARITY_WEIGHT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNameSimilarityWeight() <em>Name Similarity Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameSimilarityWeight()
	 * @generated
	 * @ordered
	 */
	protected Double nameSimilarityWeight = NAME_SIMILARITY_WEIGHT_EDEFAULT;

	/**
	 * The default value of the '{@link #getPositionSimilarityWeight() <em>Position Similarity Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionSimilarityWeight()
	 * @generated
	 * @ordered
	 */
	protected static final Double POSITION_SIMILARITY_WEIGHT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPositionSimilarityWeight() <em>Position Similarity Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionSimilarityWeight()
	 * @generated
	 * @ordered
	 */
	protected Double positionSimilarityWeight = POSITION_SIMILARITY_WEIGHT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComparisonSettingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.COMPARISON_SETTINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUseIdForCompare() {
		return useIdForCompare;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseIdForCompare(boolean newUseIdForCompare) {
		boolean oldUseIdForCompare = useIdForCompare;
		useIdForCompare = newUseIdForCompare;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.COMPARISON_SETTINGS__USE_ID_FOR_COMPARE, oldUseIdForCompare, useIdForCompare));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getNameSimilarityWeight() {
		return nameSimilarityWeight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameSimilarityWeight(Double newNameSimilarityWeight) {
		Double oldNameSimilarityWeight = nameSimilarityWeight;
		nameSimilarityWeight = newNameSimilarityWeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.COMPARISON_SETTINGS__NAME_SIMILARITY_WEIGHT, oldNameSimilarityWeight, nameSimilarityWeight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getPositionSimilarityWeight() {
		return positionSimilarityWeight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPositionSimilarityWeight(Double newPositionSimilarityWeight) {
		Double oldPositionSimilarityWeight = positionSimilarityWeight;
		positionSimilarityWeight = newPositionSimilarityWeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.COMPARISON_SETTINGS__POSITION_SIMILARITY_WEIGHT, oldPositionSimilarityWeight, positionSimilarityWeight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.COMPARISON_SETTINGS__USE_ID_FOR_COMPARE:
				return isUseIdForCompare();
			case MegamodelPackage.COMPARISON_SETTINGS__NAME_SIMILARITY_WEIGHT:
				return getNameSimilarityWeight();
			case MegamodelPackage.COMPARISON_SETTINGS__POSITION_SIMILARITY_WEIGHT:
				return getPositionSimilarityWeight();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.COMPARISON_SETTINGS__USE_ID_FOR_COMPARE:
				setUseIdForCompare((Boolean)newValue);
				return;
			case MegamodelPackage.COMPARISON_SETTINGS__NAME_SIMILARITY_WEIGHT:
				setNameSimilarityWeight((Double)newValue);
				return;
			case MegamodelPackage.COMPARISON_SETTINGS__POSITION_SIMILARITY_WEIGHT:
				setPositionSimilarityWeight((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.COMPARISON_SETTINGS__USE_ID_FOR_COMPARE:
				setUseIdForCompare(USE_ID_FOR_COMPARE_EDEFAULT);
				return;
			case MegamodelPackage.COMPARISON_SETTINGS__NAME_SIMILARITY_WEIGHT:
				setNameSimilarityWeight(NAME_SIMILARITY_WEIGHT_EDEFAULT);
				return;
			case MegamodelPackage.COMPARISON_SETTINGS__POSITION_SIMILARITY_WEIGHT:
				setPositionSimilarityWeight(POSITION_SIMILARITY_WEIGHT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.COMPARISON_SETTINGS__USE_ID_FOR_COMPARE:
				return useIdForCompare != USE_ID_FOR_COMPARE_EDEFAULT;
			case MegamodelPackage.COMPARISON_SETTINGS__NAME_SIMILARITY_WEIGHT:
				return NAME_SIMILARITY_WEIGHT_EDEFAULT == null ? nameSimilarityWeight != null : !NAME_SIMILARITY_WEIGHT_EDEFAULT.equals(nameSimilarityWeight);
			case MegamodelPackage.COMPARISON_SETTINGS__POSITION_SIMILARITY_WEIGHT:
				return POSITION_SIMILARITY_WEIGHT_EDEFAULT == null ? positionSimilarityWeight != null : !POSITION_SIMILARITY_WEIGHT_EDEFAULT.equals(positionSimilarityWeight);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (useIdForCompare: ");
		result.append(useIdForCompare);
		result.append(", nameSimilarityWeight: ");
		result.append(nameSimilarityWeight);
		result.append(", positionSimilarityWeight: ");
		result.append(positionSimilarityWeight);
		result.append(')');
		return result.toString();
	}

} //ComparisonSettingsImpl
