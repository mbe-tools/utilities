/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conformance Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ConformanceRelation#getModels <em>Models</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ConformanceRelation#getMetaModel <em>Meta Model</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getConformanceRelation()
 * @model
 * @generated
 */
public interface ConformanceRelation extends FactualRelation {

	/**
	 * Returns the value of the '<em><b>Models</b></em>' reference list.
	 * The list contents are of type {@link fr.labsticc.gmm.model.megamodel.Model}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Models</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Models</em>' reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getConformanceRelation_Models()
	 * @model required="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#connectedModels'"
	 * @generated
	 */
	EList<Model> getModels();

	/**
	 * Returns the value of the '<em><b>Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Meta Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Meta Model</em>' reference.
	 * @see #setMetaModel(MetaModel)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getConformanceRelation_MetaModel()
	 * @model required="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#connectedModels'"
	 * @generated
	 */
	MetaModel getMetaModel();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ConformanceRelation#getMetaModel <em>Meta Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Meta Model</em>' reference.
	 * @see #getMetaModel()
	 * @generated
	 */
	void setMetaModel(MetaModel value);
} // ConformanceRelation
