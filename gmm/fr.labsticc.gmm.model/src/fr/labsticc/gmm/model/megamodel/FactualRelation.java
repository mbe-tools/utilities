/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structural Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getFactualRelation()
 * @model abstract="true"
 * @generated
 */
public interface FactualRelation extends Relation {
} // StructuralRelation
