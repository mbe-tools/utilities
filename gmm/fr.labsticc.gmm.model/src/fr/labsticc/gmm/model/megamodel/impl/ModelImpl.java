/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.labsticc.gmm.model.megamodel.ErrorDescription;
import fr.labsticc.gmm.model.megamodel.FactualRelation;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.Model;
import fr.labsticc.gmm.model.megamodel.ObligationRelation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ModelImpl#getOwnedRelations <em>Owned Relations</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ModelImpl#getOwnedModels <em>Owned Models</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ModelImpl#getParentModel <em>Parent Model</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ModelImpl#getOwnedObligationRelations <em>Owned Obligation Relations</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ModelImpl#getFileExtensions <em>File Extensions</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ModelImpl#getResources <em>Resources</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ModelImpl#getOwnedFactualRelations <em>Owned Factual Relations</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ModelImpl#getError <em>Error</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ModelImpl extends NamedElementImpl implements Model {
	/**
	 * The cached value of the '{@link #getOwnedRelations() <em>Owned Relations</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedRelations()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap ownedRelations;

	/**
	 * The cached value of the '{@link #getOwnedModels() <em>Owned Models</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedModels()
	 * @generated
	 * @ordered
	 */
	protected EList<Model> ownedModels;

	/**
	 * The cached value of the '{@link #getFileExtensions() <em>File Extensions</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFileExtensions()
	 * @generated
	 * @ordered
	 */
	protected EList<String> fileExtensions;

	/**
	 * The cached value of the '{@link #getResources() <em>Resources</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResources()
	 * @generated
	 * @ordered
	 */
	protected EList<Resource> resources;

	/**
	 * The cached value of the '{@link #getError() <em>Error</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getError()
	 * @generated
	 * @ordered
	 */
	protected ErrorDescription error;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getFileExtensions() {
		if (fileExtensions == null) {
			fileExtensions = new EDataTypeUniqueEList<String>(String.class, this, MegamodelPackage.MODEL__FILE_EXTENSIONS);
		}
		return fileExtensions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Resource> getResources() {
		if (resources == null) {
			resources = new EDataTypeUniqueEList<Resource>(Resource.class, this, MegamodelPackage.MODEL__RESOURCES);
		}
		return resources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FactualRelation> getOwnedFactualRelations() {
		return getOwnedRelations().list(MegamodelPackage.Literals.MODEL__OWNED_FACTUAL_RELATIONS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ErrorDescription getError() {
		return error;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetError(ErrorDescription newError, NotificationChain msgs) {
		ErrorDescription oldError = error;
		error = newError;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MegamodelPackage.MODEL__ERROR, oldError, newError);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setError(ErrorDescription newError) {
		if (newError != error) {
			NotificationChain msgs = null;
			if (error != null)
				msgs = ((InternalEObject)error).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MegamodelPackage.MODEL__ERROR, null, msgs);
			if (newError != null)
				msgs = ((InternalEObject)newError).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MegamodelPackage.MODEL__ERROR, null, msgs);
			msgs = basicSetError(newError, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.MODEL__ERROR, newError, newError));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getOwnedRelations() {
		if (ownedRelations == null) {
			ownedRelations = new BasicFeatureMap(this, MegamodelPackage.MODEL__OWNED_RELATIONS);
		}
		return ownedRelations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Model> getOwnedModels() {
		if (ownedModels == null) {
			ownedModels = new EObjectContainmentWithInverseEList<Model>(Model.class, this, MegamodelPackage.MODEL__OWNED_MODELS, MegamodelPackage.MODEL__PARENT_MODEL);
		}
		return ownedModels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model getParentModel() {
		if (eContainerFeatureID() != MegamodelPackage.MODEL__PARENT_MODEL) return null;
		return (Model)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentModel(Model newParentModel, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParentModel, MegamodelPackage.MODEL__PARENT_MODEL, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentModel(Model newParentModel) {
		if (newParentModel != eInternalContainer() || (eContainerFeatureID() != MegamodelPackage.MODEL__PARENT_MODEL && newParentModel != null)) {
			if (EcoreUtil.isAncestor(this, newParentModel))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentModel != null)
				msgs = ((InternalEObject)newParentModel).eInverseAdd(this, MegamodelPackage.MODEL__OWNED_MODELS, Model.class, msgs);
			msgs = basicSetParentModel(newParentModel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.MODEL__PARENT_MODEL, newParentModel, newParentModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObligationRelation> getOwnedObligationRelations() {
		return getOwnedRelations().list(MegamodelPackage.Literals.MODEL__OWNED_OBLIGATION_RELATIONS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
		@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.MODEL__OWNED_MODELS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedModels()).basicAdd(otherEnd, msgs);
			case MegamodelPackage.MODEL__PARENT_MODEL:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParentModel((Model)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.MODEL__OWNED_RELATIONS:
				return ((InternalEList<?>)getOwnedRelations()).basicRemove(otherEnd, msgs);
			case MegamodelPackage.MODEL__OWNED_MODELS:
				return ((InternalEList<?>)getOwnedModels()).basicRemove(otherEnd, msgs);
			case MegamodelPackage.MODEL__PARENT_MODEL:
				return basicSetParentModel(null, msgs);
			case MegamodelPackage.MODEL__OWNED_OBLIGATION_RELATIONS:
				return ((InternalEList<?>)getOwnedObligationRelations()).basicRemove(otherEnd, msgs);
			case MegamodelPackage.MODEL__OWNED_FACTUAL_RELATIONS:
				return ((InternalEList<?>)getOwnedFactualRelations()).basicRemove(otherEnd, msgs);
			case MegamodelPackage.MODEL__ERROR:
				return basicSetError(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case MegamodelPackage.MODEL__PARENT_MODEL:
				return eInternalContainer().eInverseRemove(this, MegamodelPackage.MODEL__OWNED_MODELS, Model.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.MODEL__OWNED_RELATIONS:
				if (coreType) return getOwnedRelations();
				return ((FeatureMap.Internal)getOwnedRelations()).getWrapper();
			case MegamodelPackage.MODEL__OWNED_MODELS:
				return getOwnedModels();
			case MegamodelPackage.MODEL__PARENT_MODEL:
				return getParentModel();
			case MegamodelPackage.MODEL__OWNED_OBLIGATION_RELATIONS:
				return getOwnedObligationRelations();
			case MegamodelPackage.MODEL__FILE_EXTENSIONS:
				return getFileExtensions();
			case MegamodelPackage.MODEL__RESOURCES:
				return getResources();
			case MegamodelPackage.MODEL__OWNED_FACTUAL_RELATIONS:
				return getOwnedFactualRelations();
			case MegamodelPackage.MODEL__ERROR:
				return getError();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
		@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.MODEL__OWNED_RELATIONS:
				((FeatureMap.Internal)getOwnedRelations()).set(newValue);
				return;
			case MegamodelPackage.MODEL__OWNED_MODELS:
				getOwnedModels().clear();
				getOwnedModels().addAll((Collection<? extends Model>)newValue);
				return;
			case MegamodelPackage.MODEL__PARENT_MODEL:
				setParentModel((Model)newValue);
				return;
			case MegamodelPackage.MODEL__OWNED_OBLIGATION_RELATIONS:
				getOwnedObligationRelations().clear();
				getOwnedObligationRelations().addAll((Collection<? extends ObligationRelation>)newValue);
				return;
			case MegamodelPackage.MODEL__FILE_EXTENSIONS:
				getFileExtensions().clear();
				getFileExtensions().addAll((Collection<? extends String>)newValue);
				return;
			case MegamodelPackage.MODEL__RESOURCES:
				getResources().clear();
				getResources().addAll((Collection<? extends Resource>)newValue);
				return;
			case MegamodelPackage.MODEL__OWNED_FACTUAL_RELATIONS:
				getOwnedFactualRelations().clear();
				getOwnedFactualRelations().addAll((Collection<? extends FactualRelation>)newValue);
				return;
			case MegamodelPackage.MODEL__ERROR:
				setError((ErrorDescription)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.MODEL__OWNED_RELATIONS:
				getOwnedRelations().clear();
				return;
			case MegamodelPackage.MODEL__OWNED_MODELS:
				getOwnedModels().clear();
				return;
			case MegamodelPackage.MODEL__PARENT_MODEL:
				setParentModel((Model)null);
				return;
			case MegamodelPackage.MODEL__OWNED_OBLIGATION_RELATIONS:
				getOwnedObligationRelations().clear();
				return;
			case MegamodelPackage.MODEL__FILE_EXTENSIONS:
				getFileExtensions().clear();
				return;
			case MegamodelPackage.MODEL__RESOURCES:
				getResources().clear();
				return;
			case MegamodelPackage.MODEL__OWNED_FACTUAL_RELATIONS:
				getOwnedFactualRelations().clear();
				return;
			case MegamodelPackage.MODEL__ERROR:
				setError((ErrorDescription)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.MODEL__OWNED_RELATIONS:
				return ownedRelations != null && !ownedRelations.isEmpty();
			case MegamodelPackage.MODEL__OWNED_MODELS:
				return ownedModels != null && !ownedModels.isEmpty();
			case MegamodelPackage.MODEL__PARENT_MODEL:
				return getParentModel() != null;
			case MegamodelPackage.MODEL__OWNED_OBLIGATION_RELATIONS:
				return !getOwnedObligationRelations().isEmpty();
			case MegamodelPackage.MODEL__FILE_EXTENSIONS:
				return fileExtensions != null && !fileExtensions.isEmpty();
			case MegamodelPackage.MODEL__RESOURCES:
				return resources != null && !resources.isEmpty();
			case MegamodelPackage.MODEL__OWNED_FACTUAL_RELATIONS:
				return !getOwnedFactualRelations().isEmpty();
			case MegamodelPackage.MODEL__ERROR:
				return error != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ownedRelations: ");
		result.append(ownedRelations);
		result.append(", fileExtensions: ");
		result.append(fileExtensions);
		result.append(", resources: ");
		result.append(resources);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public String getDisplayName() {
		final StringBuilder result = new StringBuilder();
		int index = 0;
		
		for( final Resource res : getResources() ) {
			result.append( res.getURI().lastSegment() );
			index++;
			
			if ( index == getResources().size() - 1 ) {
				result.append( "and " );
			}
			else if ( index < getResources().size() ) {
				result.append( ", " );
			}
		}
		
		return result.toString();
	}

} //ModelImpl
