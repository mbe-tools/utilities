package fr.labsticc.gmm.model.megamodel.util;

import org.eclipse.core.runtime.QualifiedName;

public interface GmmConstants {

	String META_MODEL_NS = "meta-model.";
	String SUBSET = "subset";

	QualifiedName subsettedMetaModelQn = new QualifiedName( META_MODEL_NS, SUBSET );
}
