/**
 */
package fr.labsticc.gmm.model.megamodel.util;

import fr.labsticc.gmm.model.megamodel.*;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import fr.labsticc.gmm.model.megamodel.Activity;
import fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation;
import fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation;
import fr.labsticc.gmm.model.megamodel.BinaryRelationPolicy;
import fr.labsticc.gmm.model.megamodel.ComparisonSettings;
import fr.labsticc.gmm.model.megamodel.ConformanceRelation;
import fr.labsticc.gmm.model.megamodel.ErrorDescription;
import fr.labsticc.gmm.model.megamodel.ExecutionContext;
import fr.labsticc.gmm.model.megamodel.FactualRelation;
import fr.labsticc.gmm.model.megamodel.GmmEngine;
import fr.labsticc.gmm.model.megamodel.GmmSpecification;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation;
import fr.labsticc.gmm.model.megamodel.MetaModelSubset;
import fr.labsticc.gmm.model.megamodel.Model;
import fr.labsticc.gmm.model.megamodel.ModelElementsCoveragePolicy;
import fr.labsticc.gmm.model.megamodel.ModelingEnvironment;
import fr.labsticc.gmm.model.megamodel.NamedElement;
import fr.labsticc.gmm.model.megamodel.ObligationRelation;
import fr.labsticc.gmm.model.megamodel.ProjectUriRelBinRelPolicy;
import fr.labsticc.gmm.model.megamodel.Relation;
import fr.labsticc.gmm.model.megamodel.RelationPolicy;
import fr.labsticc.gmm.model.megamodel.SetOperation;
import fr.labsticc.gmm.model.megamodel.SubsetRelationship;
import fr.labsticc.gmm.model.megamodel.SubsettedMetaModel;
import fr.labsticc.gmm.model.megamodel.UriRelatedBinaryRelPolicy;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage
 * @generated
 */
public class MegamodelSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MegamodelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MegamodelSwitch() {
		if (modelPackage == null) {
			modelPackage = MegamodelPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case MegamodelPackage.GMM_SPECIFICATION: {
				GmmSpecification gmmSpecification = (GmmSpecification)theEObject;
				T result = caseGmmSpecification(gmmSpecification);
				if (result == null) result = caseModel(gmmSpecification);
				if (result == null) result = caseNamedElement(gmmSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.NAMED_ELEMENT: {
				NamedElement namedElement = (NamedElement)theEObject;
				T result = caseNamedElement(namedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.MODEL: {
				Model model = (Model)theEObject;
				T result = caseModel(model);
				if (result == null) result = caseNamedElement(model);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.ERROR_DESCRIPTION: {
				ErrorDescription errorDescription = (ErrorDescription)theEObject;
				T result = caseErrorDescription(errorDescription);
				if (result == null) result = caseNamedElement(errorDescription);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.RELATION: {
				Relation relation = (Relation)theEObject;
				T result = caseRelation(relation);
				if (result == null) result = caseNamedElement(relation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.META_MODEL: {
				MetaModel metaModel = (MetaModel)theEObject;
				T result = caseMetaModel(metaModel);
				if (result == null) result = caseModel(metaModel);
				if (result == null) result = caseNamedElement(metaModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.COMPARISON_SETTINGS: {
				ComparisonSettings comparisonSettings = (ComparisonSettings)theEObject;
				T result = caseComparisonSettings(comparisonSettings);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.FACTUAL_RELATION: {
				FactualRelation factualRelation = (FactualRelation)theEObject;
				T result = caseFactualRelation(factualRelation);
				if (result == null) result = caseRelation(factualRelation);
				if (result == null) result = caseNamedElement(factualRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.OBLIGATION_RELATION: {
				ObligationRelation obligationRelation = (ObligationRelation)theEObject;
				T result = caseObligationRelation(obligationRelation);
				if (result == null) result = caseRelation(obligationRelation);
				if (result == null) result = caseNamedElement(obligationRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.CONFORMANCE_RELATION: {
				ConformanceRelation conformanceRelation = (ConformanceRelation)theEObject;
				T result = caseConformanceRelation(conformanceRelation);
				if (result == null) result = caseFactualRelation(conformanceRelation);
				if (result == null) result = caseRelation(conformanceRelation);
				if (result == null) result = caseNamedElement(conformanceRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.META_MODEL_RELATED_RELATION: {
				MetaModelRelatedRelation metaModelRelatedRelation = (MetaModelRelatedRelation)theEObject;
				T result = caseMetaModelRelatedRelation(metaModelRelatedRelation);
				if (result == null) result = caseFactualRelation(metaModelRelatedRelation);
				if (result == null) result = caseRelation(metaModelRelatedRelation);
				if (result == null) result = caseNamedElement(metaModelRelatedRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION: {
				BinaryMetaModeRelatedlRelation binaryMetaModeRelatedlRelation = (BinaryMetaModeRelatedlRelation)theEObject;
				T result = caseBinaryMetaModeRelatedlRelation(binaryMetaModeRelatedlRelation);
				if (result == null) result = caseMetaModelRelatedRelation(binaryMetaModeRelatedlRelation);
				if (result == null) result = caseFactualRelation(binaryMetaModeRelatedlRelation);
				if (result == null) result = caseRelation(binaryMetaModeRelatedlRelation);
				if (result == null) result = caseNamedElement(binaryMetaModeRelatedlRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION: {
				BinaryMetaModelRelatedConsistencyRelation binaryMetaModelRelatedConsistencyRelation = (BinaryMetaModelRelatedConsistencyRelation)theEObject;
				T result = caseBinaryMetaModelRelatedConsistencyRelation(binaryMetaModelRelatedConsistencyRelation);
				if (result == null) result = caseObligationRelation(binaryMetaModelRelatedConsistencyRelation);
				if (result == null) result = caseBinaryMetaModeRelatedlRelation(binaryMetaModelRelatedConsistencyRelation);
				if (result == null) result = caseMetaModelRelatedRelation(binaryMetaModelRelatedConsistencyRelation);
				if (result == null) result = caseNamedElement(binaryMetaModelRelatedConsistencyRelation);
				if (result == null) result = caseFactualRelation(binaryMetaModelRelatedConsistencyRelation);
				if (result == null) result = caseRelation(binaryMetaModelRelatedConsistencyRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.RELATION_POLICY: {
				RelationPolicy relationPolicy = (RelationPolicy)theEObject;
				T result = caseRelationPolicy(relationPolicy);
				if (result == null) result = caseNamedElement(relationPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.BINARY_RELATION_POLICY: {
				BinaryRelationPolicy binaryRelationPolicy = (BinaryRelationPolicy)theEObject;
				T result = caseBinaryRelationPolicy(binaryRelationPolicy);
				if (result == null) result = caseRelationPolicy(binaryRelationPolicy);
				if (result == null) result = caseNamedElement(binaryRelationPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.URI_RELATED_BINARY_REL_POLICY: {
				UriRelatedBinaryRelPolicy uriRelatedBinaryRelPolicy = (UriRelatedBinaryRelPolicy)theEObject;
				T result = caseUriRelatedBinaryRelPolicy(uriRelatedBinaryRelPolicy);
				if (result == null) result = caseBinaryRelationPolicy(uriRelatedBinaryRelPolicy);
				if (result == null) result = caseRelationPolicy(uriRelatedBinaryRelPolicy);
				if (result == null) result = caseNamedElement(uriRelatedBinaryRelPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.PROJECT_URI_REL_BIN_REL_POLICY: {
				ProjectUriRelBinRelPolicy projectUriRelBinRelPolicy = (ProjectUriRelBinRelPolicy)theEObject;
				T result = caseProjectUriRelBinRelPolicy(projectUriRelBinRelPolicy);
				if (result == null) result = caseUriRelatedBinaryRelPolicy(projectUriRelBinRelPolicy);
				if (result == null) result = caseBinaryRelationPolicy(projectUriRelBinRelPolicy);
				if (result == null) result = caseRelationPolicy(projectUriRelBinRelPolicy);
				if (result == null) result = caseNamedElement(projectUriRelBinRelPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.MODELING_ENVIRONMENT: {
				ModelingEnvironment modelingEnvironment = (ModelingEnvironment)theEObject;
				T result = caseModelingEnvironment(modelingEnvironment);
				if (result == null) result = caseModel(modelingEnvironment);
				if (result == null) result = caseNamedElement(modelingEnvironment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.EXECUTION_CONTEXT: {
				ExecutionContext executionContext = (ExecutionContext)theEObject;
				T result = caseExecutionContext(executionContext);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.MODEL_ELEMENTS_COVERAGE_POLICY: {
				ModelElementsCoveragePolicy modelElementsCoveragePolicy = (ModelElementsCoveragePolicy)theEObject;
				T result = caseModelElementsCoveragePolicy(modelElementsCoveragePolicy);
				if (result == null) result = caseNamedElement(modelElementsCoveragePolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.SUBSETTED_META_MODEL: {
				SubsettedMetaModel subsettedMetaModel = (SubsettedMetaModel)theEObject;
				T result = caseSubsettedMetaModel(subsettedMetaModel);
				if (result == null) result = caseMetaModel(subsettedMetaModel);
				if (result == null) result = caseModel(subsettedMetaModel);
				if (result == null) result = caseNamedElement(subsettedMetaModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.META_MODEL_SUBSET: {
				MetaModelSubset metaModelSubset = (MetaModelSubset)theEObject;
				T result = caseMetaModelSubset(metaModelSubset);
				if (result == null) result = caseNamedElement(metaModelSubset);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.ACTIVITY: {
				Activity activity = (Activity)theEObject;
				T result = caseActivity(activity);
				if (result == null) result = caseNamedElement(activity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.SUBSET_RELATIONSHIP: {
				SubsetRelationship subsetRelationship = (SubsetRelationship)theEObject;
				T result = caseSubsetRelationship(subsetRelationship);
				if (result == null) result = caseNamedElement(subsetRelationship);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.GMM_ENGINE: {
				GmmEngine gmmEngine = (GmmEngine)theEObject;
				T result = caseGmmEngine(gmmEngine);
				if (result == null) result = caseNamedElement(gmmEngine);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.SET_OPERATION: {
				SetOperation setOperation = (SetOperation)theEObject;
				T result = caseSetOperation(setOperation);
				if (result == null) result = caseNamedElement(setOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MegamodelPackage.TOOL: {
				Tool tool = (Tool)theEObject;
				T result = caseTool(tool);
				if (result == null) result = caseModel(tool);
				if (result == null) result = caseNamedElement(tool);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gmm Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gmm Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGmmSpecification(GmmSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModel(Model object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Error Description</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Error Description</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseErrorDescription(ErrorDescription object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Meta Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Meta Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetaModel(MetaModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Comparison Settings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Comparison Settings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComparisonSettings(ComparisonSettings object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelation(Relation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Factual Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Factual Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFactualRelation(FactualRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Obligation Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Obligation Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObligationRelation(ObligationRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conformance Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conformance Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConformanceRelation(ConformanceRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Meta Model Related Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Meta Model Related Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetaModelRelatedRelation(MetaModelRelatedRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Meta Mode Relatedl Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Meta Mode Relatedl Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryMetaModeRelatedlRelation(BinaryMetaModeRelatedlRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Meta Model Related Consistency Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Meta Model Related Consistency Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryMetaModelRelatedConsistencyRelation(BinaryMetaModelRelatedConsistencyRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relation Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relation Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelationPolicy(RelationPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Relation Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Relation Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryRelationPolicy(BinaryRelationPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uri Related Binary Rel Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uri Related Binary Rel Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUriRelatedBinaryRelPolicy(UriRelatedBinaryRelPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Modeling Environment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Modeling Environment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelingEnvironment(ModelingEnvironment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Elements Coverage Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Elements Coverage Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelElementsCoveragePolicy(ModelElementsCoveragePolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Meta Model Subset</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Meta Model Subset</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetaModelSubset(MetaModelSubset object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivity(Activity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subset Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subset Relationship</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubsetRelationship(SubsetRelationship object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subsetted Meta Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subsetted Meta Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubsettedMetaModel(SubsettedMetaModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Project Uri Rel Bin Rel Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Project Uri Rel Bin Rel Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProjectUriRelBinRelPolicy(ProjectUriRelBinRelPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Execution Context</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Execution Context</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExecutionContext(ExecutionContext object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gmm Engine</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gmm Engine</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGmmEngine(GmmEngine object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Set Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Set Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSetOperation(SetOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tool</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tool</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTool(Tool object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //MegamodelSwitch
