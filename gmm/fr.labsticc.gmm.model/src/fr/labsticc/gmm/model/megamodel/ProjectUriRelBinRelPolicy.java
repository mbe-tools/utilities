/**
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.common.util.URI;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Persistent Props Project Uri Rel Bin Rel Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getProjectUriRelBinRelPolicy()
 * @model
 * @generated
 */
public interface ProjectUriRelBinRelPolicy extends UriRelatedBinaryRelPolicy {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model projectUriDataType="fr.labsticc.gmm.model.megamodel.URI" projectUriRequired="true"
	 * @generated
	 */
	void unsetCorrespondingProject(URI projectUri);
} // PersistentPropsProjectUriRelBinRelPolicy
