/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel.impl;

import fr.labsticc.framework.core.exception.FunctionalException;
import fr.labsticc.framework.core.exception.SystemException;
import fr.labsticc.gmm.model.megamodel.*;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MegamodelFactoryImpl extends EFactoryImpl implements MegamodelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MegamodelFactory init() {
		try {
			MegamodelFactory theMegamodelFactory = (MegamodelFactory)EPackage.Registry.INSTANCE.getEFactory(MegamodelPackage.eNS_URI);
			if (theMegamodelFactory != null) {
				return theMegamodelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MegamodelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MegamodelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MegamodelPackage.GMM_SPECIFICATION: return createGmmSpecification();
			case MegamodelPackage.MODEL: return createModel();
			case MegamodelPackage.ERROR_DESCRIPTION: return createErrorDescription();
			case MegamodelPackage.META_MODEL: return createMetaModel();
			case MegamodelPackage.COMPARISON_SETTINGS: return createComparisonSettings();
			case MegamodelPackage.CONFORMANCE_RELATION: return createConformanceRelation();
			case MegamodelPackage.URI_RELATED_BINARY_REL_POLICY: return createUriRelatedBinaryRelPolicy();
			case MegamodelPackage.PROJECT_URI_REL_BIN_REL_POLICY: return createProjectUriRelBinRelPolicy();
			case MegamodelPackage.MODELING_ENVIRONMENT: return createModelingEnvironment();
			case MegamodelPackage.EXECUTION_CONTEXT: return createExecutionContext();
			case MegamodelPackage.SUBSETTED_META_MODEL: return createSubsettedMetaModel();
			case MegamodelPackage.META_MODEL_SUBSET: return createMetaModelSubset();
			case MegamodelPackage.ACTIVITY: return createActivity();
			case MegamodelPackage.SUBSET_RELATIONSHIP: return createSubsetRelationship();
			case MegamodelPackage.GMM_ENGINE: return createGmmEngine();
			case MegamodelPackage.SET_OPERATION: return createSetOperation();
			case MegamodelPackage.TOOL: return createTool();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case MegamodelPackage.ERROR_TYPE:
				return createErrorTypeFromString(eDataType, initialValue);
			case MegamodelPackage.OPERATION_TYPE:
				return createOperationTypeFromString(eDataType, initialValue);
			case MegamodelPackage.VALIDITY_STATUS:
				return createValidityStatusFromString(eDataType, initialValue);
			case MegamodelPackage.ENABLEMENT_TYPE:
				return createEnablementTypeFromString(eDataType, initialValue);
			case MegamodelPackage.SUBSET_RELATIONSHIP_TYPE:
				return createSubsetRelationshipTypeFromString(eDataType, initialValue);
			case MegamodelPackage.SET_OPERATION_TYPE:
				return createSetOperationTypeFromString(eDataType, initialValue);
			case MegamodelPackage.URI:
				return createURIFromString(eDataType, initialValue);
			case MegamodelPackage.FUNCTIONAL_EXCEPTION:
				return createFunctionalExceptionFromString(eDataType, initialValue);
			case MegamodelPackage.SYSTEM_EXCEPTION:
				return createSystemExceptionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case MegamodelPackage.ERROR_TYPE:
				return convertErrorTypeToString(eDataType, instanceValue);
			case MegamodelPackage.OPERATION_TYPE:
				return convertOperationTypeToString(eDataType, instanceValue);
			case MegamodelPackage.VALIDITY_STATUS:
				return convertValidityStatusToString(eDataType, instanceValue);
			case MegamodelPackage.ENABLEMENT_TYPE:
				return convertEnablementTypeToString(eDataType, instanceValue);
			case MegamodelPackage.SUBSET_RELATIONSHIP_TYPE:
				return convertSubsetRelationshipTypeToString(eDataType, instanceValue);
			case MegamodelPackage.SET_OPERATION_TYPE:
				return convertSetOperationTypeToString(eDataType, instanceValue);
			case MegamodelPackage.URI:
				return convertURIToString(eDataType, instanceValue);
			case MegamodelPackage.FUNCTIONAL_EXCEPTION:
				return convertFunctionalExceptionToString(eDataType, instanceValue);
			case MegamodelPackage.SYSTEM_EXCEPTION:
				return convertSystemExceptionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GmmSpecification createGmmSpecification() {
		GmmSpecificationImpl gmmSpecification = new GmmSpecificationImpl();
		return gmmSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model createModel() {
		ModelImpl model = new ModelImpl();
		return model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ErrorDescription createErrorDescription() {
		ErrorDescriptionImpl errorDescription = new ErrorDescriptionImpl();
		return errorDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConformanceRelation createConformanceRelation() {
		ConformanceRelationImpl conformanceRelation = new ConformanceRelationImpl();
		return conformanceRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModelSubset createMetaModelSubset() {
		MetaModelSubsetImpl metaModelSubset = new MetaModelSubsetImpl();
		return metaModelSubset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Activity createActivity() {
		ActivityImpl activity = new ActivityImpl();
		return activity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubsetRelationship createSubsetRelationship() {
		SubsetRelationshipImpl subsetRelationship = new SubsetRelationshipImpl();
		return subsetRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubsettedMetaModel createSubsettedMetaModel() {
		SubsettedMetaModelImpl subsettedMetaModel = new SubsettedMetaModelImpl();
		return subsettedMetaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProjectUriRelBinRelPolicy createProjectUriRelBinRelPolicy() {
		ProjectUriRelBinRelPolicyImpl projectUriRelBinRelPolicy = new ProjectUriRelBinRelPolicyImpl();
		return projectUriRelBinRelPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionContext createExecutionContext() {
		ExecutionContextImpl executionContext = new ExecutionContextImpl();
		return executionContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GmmEngine createGmmEngine() {
		GmmEngineImpl gmmEngine = new GmmEngineImpl();
		return gmmEngine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SetOperation createSetOperation() {
		SetOperationImpl setOperation = new SetOperationImpl();
		return setOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tool createTool() {
		ToolImpl tool = new ToolImpl();
		return tool;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValidityStatus createValidityStatusFromString(EDataType eDataType, String initialValue) {
		ValidityStatus result = ValidityStatus.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertValidityStatusToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnablementType createEnablementTypeFromString(EDataType eDataType, String initialValue) {
		EnablementType result = EnablementType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEnablementTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubsetRelationshipType createSubsetRelationshipTypeFromString(EDataType eDataType, String initialValue) {
		SubsetRelationshipType result = SubsetRelationshipType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSubsetRelationshipTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SetOperationType createSetOperationTypeFromString(EDataType eDataType, String initialValue) {
		SetOperationType result = SetOperationType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSetOperationTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ErrorType createErrorTypeFromString(EDataType eDataType, String initialValue) {
		ErrorType result = ErrorType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertErrorTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationType createOperationTypeFromString(EDataType eDataType, String initialValue) {
		OperationType result = OperationType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperationTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public URI createURIFromString(EDataType eDataType, String initialValue) {
		return (URI)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertURIToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalException createFunctionalExceptionFromString(EDataType eDataType, String initialValue) {
		return (FunctionalException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFunctionalExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemException createSystemExceptionFromString(EDataType eDataType, String initialValue) {
		return (SystemException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSystemExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UriRelatedBinaryRelPolicy createUriRelatedBinaryRelPolicy() {
		UriRelatedBinaryRelPolicyImpl uriRelatedBinaryRelPolicy = new UriRelatedBinaryRelPolicyImpl();
		return uriRelatedBinaryRelPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelingEnvironment createModelingEnvironment() {
		ModelingEnvironmentImpl modelingEnvironment = new ModelingEnvironmentImpl();
		return modelingEnvironment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel createMetaModel() {
		MetaModelImpl metaModel = new MetaModelImpl();
		return metaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonSettings createComparisonSettings() {
		ComparisonSettingsImpl comparisonSettings = new ComparisonSettingsImpl();
		return comparisonSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MegamodelPackage getMegamodelPackage() {
		return (MegamodelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MegamodelPackage getPackage() {
		return MegamodelPackage.eINSTANCE;
	}

} //MegamodelFactoryImpl
