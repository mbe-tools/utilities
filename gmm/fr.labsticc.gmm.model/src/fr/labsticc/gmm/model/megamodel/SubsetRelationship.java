/**
 */
package fr.labsticc.gmm.model.megamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subset Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.SubsetRelationship#getType <em>Type</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.SubsetRelationship#getTargetSubset <em>Target Subset</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getSubsetRelationship()
 * @model
 * @generated
 */
public interface SubsetRelationship extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.labsticc.gmm.model.megamodel.SubsetRelationshipType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see fr.labsticc.gmm.model.megamodel.SubsetRelationshipType
	 * @see #setType(SubsetRelationshipType)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getSubsetRelationship_Type()
	 * @model required="true"
	 * @generated
	 */
	SubsetRelationshipType getType();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.SubsetRelationship#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see fr.labsticc.gmm.model.megamodel.SubsetRelationshipType
	 * @see #getType()
	 * @generated
	 */
	void setType(SubsetRelationshipType value);

	/**
	 * Returns the value of the '<em><b>Target Subset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Subset</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Subset</em>' reference.
	 * @see #setTargetSubset(MetaModelSubset)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getSubsetRelationship_TargetSubset()
	 * @model required="true"
	 * @generated
	 */
	MetaModelSubset getTargetSubset();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.SubsetRelationship#getTargetSubset <em>Target Subset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Subset</em>' reference.
	 * @see #getTargetSubset()
	 * @generated
	 */
	void setTargetSubset(MetaModelSubset value);

} // SubsetRelationship
