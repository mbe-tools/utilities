/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel;

import fr.labsticc.framework.core.exception.FunctionalException;
import fr.labsticc.framework.core.exception.SystemException;
import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operational Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ObligationRelation#getOwnedCoveragePolicy <em>Owned Coverage Policy</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ObligationRelation#getEnablementType <em>Enablement Type</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ObligationRelation#isDeleting <em>Deleting</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ObligationRelation#getValidityStatus <em>Validity Status</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getObligationRelation()
 * @model abstract="true"
 * @generated
 */
public interface ObligationRelation extends Relation {
	/**
	 * Returns the value of the '<em><b>Owned Coverage Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Coverage Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Coverage Policy</em>' containment reference.
	 * @see #setOwnedCoveragePolicy(ModelElementsCoveragePolicy)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getObligationRelation_OwnedCoveragePolicy()
	 * @model containment="true"
	 * @generated
	 */
	ModelElementsCoveragePolicy getOwnedCoveragePolicy();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ObligationRelation#getOwnedCoveragePolicy <em>Owned Coverage Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owned Coverage Policy</em>' containment reference.
	 * @see #getOwnedCoveragePolicy()
	 * @generated
	 */
	void setOwnedCoveragePolicy(ModelElementsCoveragePolicy value);

	/**
	 * Returns the value of the '<em><b>Enablement Type</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.labsticc.gmm.model.megamodel.EnablementType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enablement Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enablement Type</em>' attribute.
	 * @see fr.labsticc.gmm.model.megamodel.EnablementType
	 * @see #setEnablementType(EnablementType)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getObligationRelation_EnablementType()
	 * @model required="true"
	 * @generated
	 */
	EnablementType getEnablementType();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ObligationRelation#getEnablementType <em>Enablement Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enablement Type</em>' attribute.
	 * @see fr.labsticc.gmm.model.megamodel.EnablementType
	 * @see #getEnablementType()
	 * @generated
	 */
	void setEnablementType(EnablementType value);

	/**
	 * Returns the value of the '<em><b>Deleting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deleting</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deleting</em>' attribute.
	 * @see #setDeleting(boolean)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getObligationRelation_Deleting()
	 * @model
	 * @generated
	 */
	boolean isDeleting();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ObligationRelation#isDeleting <em>Deleting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deleting</em>' attribute.
	 * @see #isDeleting()
	 * @generated
	 */
	void setDeleting(boolean value);

	/**
	 * Returns the value of the '<em><b>Validity Status</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.labsticc.gmm.model.megamodel.ValidityStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Validity Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validity Status</em>' attribute.
	 * @see fr.labsticc.gmm.model.megamodel.ValidityStatus
	 * @see #setValidityStatus(ValidityStatus)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getObligationRelation_ValidityStatus()
	 * @model required="true"
	 * @generated
	 */
	ValidityStatus getValidityStatus();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ObligationRelation#getValidityStatus <em>Validity Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Validity Status</em>' attribute.
	 * @see fr.labsticc.gmm.model.megamodel.ValidityStatus
	 * @see #getValidityStatus()
	 * @generated
	 */
	void setValidityStatus(ValidityStatus value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="fr.labsticc.gmm.model.megamodel.FunctionalException fr.labsticc.gmm.model.megamodel.SystemException" modelingEnvironmentRequired="true" executionContextRequired="true"
	 * @generated
	 */
	EList<Model> establishValidity(ModelingEnvironment modelingEnvironment, ExecutionContext executionContext) throws FunctionalException, SystemException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model modelRequired="true" executionContextRequired="true"
	 * @generated
	 */
	boolean isEnabled(Model model, ExecutionContext executionContext);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model modelRequired="true"
	 * @generated
	 */
	boolean canDelete(Model model);

} // OperationalRelation
