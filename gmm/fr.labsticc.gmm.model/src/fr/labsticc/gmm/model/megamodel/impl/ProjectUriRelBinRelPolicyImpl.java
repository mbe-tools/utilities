/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;

import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation;
import fr.labsticc.gmm.model.megamodel.ProjectUriRelBinRelPolicy;
import fr.labsticc.gmm.model.megamodel.SubsettedMetaModel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Persistent Props Project Uri Rel Bin Rel Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ProjectUriRelBinRelPolicyImpl extends UriRelatedBinaryRelPolicyImpl implements ProjectUriRelBinRelPolicy {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProjectUriRelBinRelPolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.PROJECT_URI_REL_BIN_REL_POLICY;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void unsetCorrespondingProject( final URI p_projectUri ) {
		final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject( p_projectUri.toPlatformString( true ) );
		
		try {
			final IProject corrProject = correspondingProject( project );
			final QualifiedName qualName = getMappedProjectPropQualName();
			project.setPersistentProperty( qualName, null );
			
			if ( corrProject != null && corrProject != project ) {
				corrProject.setPersistentProperty( qualName, null );
			}
		}
		catch( final CoreException p_ex ) {
			throw new RuntimeException( p_ex );
		}
	}

	protected QualifiedName getMappedProjectPropQualName() {
		return new QualifiedName( getClass().getName(), getRelation().getName() );
	}
	
	protected IProject correspondingProject( final IProject p_project )
	throws CoreException {
		final String mappedProjectName = p_project.getPersistentProperty( getMappedProjectPropQualName() );

		if ( mappedProjectName != null ) {
			return ResourcesPlugin.getWorkspace().getRoot().getProject( mappedProjectName );
		}

		return null;
	}
	
	/* Create a uri for the same file name located in a corresponding project.
	 * (non-Javadoc)
	 * @see fr.labsticc.gmm.model.megamodel.impl.UriRelatedBinaryRelPolicyImpl#correspondingUri(org.eclipse.emf.common.util.URI)
	 */
	@Override
	public URI correspondingUri( final URI p_uri ) {
		if ( p_uri == null ) {
			return null;
		}

		final MetaModelRelatedRelation parentRelation = (MetaModelRelatedRelation) getRelation();
		
		// Must check that this URI is concerned by this relation because dependencies may not be concerned
		// by the relation (e.g.: with subset meta-models).
		if ( parentRelation.concerns( p_uri ) ) {
			final String extension = p_uri.fileExtension();
			
			if ( extension == null ) {
				final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject( p_uri.toPlatformString( true ) );
				
				if ( project.exists() ) {
					try {
						final IProject corrProject = correspondingProject( project );
						
						if ( corrProject == null ) {
							return null;
						}
						
						return URI.createPlatformResourceURI( corrProject.getName(), true );
					}
					catch( final CoreException p_ex ) {
						throw new RuntimeException( p_ex );
					}
				}
			}
			else {
				for ( final MetaModel metaModel : parentRelation.getMetaModelsList() ) {
					if ( !metaModel.declares( p_uri ) ) {
						final IResource res = ResourcesPlugin.getWorkspace().getRoot().getFile( new Path( p_uri.toPlatformString( true ) ) );
						
						try {
							final IProject project = correspondingProject( res.getProject() );
							
							if ( project != null ) {
								URI corrUri = URI.createPlatformResourceURI( project.getName(), true );
								
								for ( final String segment : p_uri.segmentsList().subList( 2, p_uri.segmentCount() ) ) {
									corrUri = corrUri.appendSegment( segment );
								}
								
								corrUri = corrUri.trimFileExtension();
								
								if ( metaModel instanceof SubsettedMetaModel ) {
									corrUri = corrUri.trimFileExtension().appendFileExtension( ( (SubsettedMetaModel) metaModel ).getSubsetExtensions().get( 0 ) );
								}
	
								return corrUri.appendFileExtension( metaModel.getFileExtensions().get( 0 ) );
							}
						}
						catch( final CoreException p_ex ) {
							throw new RuntimeException( p_ex );
						}
					}
				}
			}
		}
				
		return null;
	}
	
	private boolean correspondingUrisWithoutProject(	final URI p_leftUri, 
														final URI p_rightUri ) {
		if ( p_leftUri.segmentCount() != p_rightUri.segmentCount() ) {
			return false;
		}

		for ( int index = 2; index < p_leftUri.segmentCount() - 1; index++ ) {
			if ( !p_leftUri.segment( index ).equals( p_rightUri.segment( index ) ) ) {
				return false;
			}
		}
		
		// Compare last segments
		return p_leftUri.trimFileExtension().trimFileExtension().lastSegment().equals( p_rightUri.trimFileExtension().trimFileExtension().lastSegment() );
	}
	
	@Override
	public boolean areRelated(	final URI leftUri,
								final URI rightUri ) {
		if ( leftUri == null || rightUri == null || !correspondingUrisWithoutProject( leftUri, rightUri ) ) {
			return false;
		}
		
		final IResource leftRes = ResourcesPlugin.getWorkspace().getRoot().getFile( new Path( leftUri.toPlatformString( true ) ) );
		
		try {
			final IProject leftProject = leftRes.getProject();
			final QualifiedName mappedProjectQn = getMappedProjectPropQualName();
			final String leftMappedProjectName = leftProject.getPersistentProperty( mappedProjectQn );
	
			final IResource rightRes = ResourcesPlugin.getWorkspace().getRoot().getFile( new Path( rightUri.toPlatformString( true ) ) );
			final IProject rightProject = rightRes.getProject();
			final String rightMappedProjectName = rightProject.getPersistentProperty( mappedProjectQn );
			
			return	leftProject.getName().equals( rightMappedProjectName ) &&
					rightProject.getName().equals( leftMappedProjectName );
		}
		catch( final CoreException p_ex ) {
			throw new RuntimeException( p_ex );
		}
	}
} //PersistentPropsProjectUriRelBinRelPolicyImpl
