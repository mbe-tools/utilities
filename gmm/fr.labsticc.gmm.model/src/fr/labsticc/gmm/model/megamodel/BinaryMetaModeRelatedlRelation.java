/**
 */
package fr.labsticc.gmm.model.megamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Meta Mode Relatedl Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation#getLeftMetaModel <em>Left Meta Model</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation#getRightMetaModel <em>Right Meta Model</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getBinaryMetaModeRelatedlRelation()
 * @model abstract="true"
 * @generated
 */
public interface BinaryMetaModeRelatedlRelation extends MetaModelRelatedRelation {
	/**
	 * Returns the value of the '<em><b>Left Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Meta Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Meta Model</em>' reference.
	 * @see #setLeftMetaModel(MetaModel)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getBinaryMetaModeRelatedlRelation_LeftMetaModel()
	 * @model required="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#metaModels'"
	 * @generated
	 */
	MetaModel getLeftMetaModel();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation#getLeftMetaModel <em>Left Meta Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Meta Model</em>' reference.
	 * @see #getLeftMetaModel()
	 * @generated
	 */
	void setLeftMetaModel(MetaModel value);

	/**
	 * Returns the value of the '<em><b>Right Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Meta Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Meta Model</em>' reference.
	 * @see #setRightMetaModel(MetaModel)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getBinaryMetaModeRelatedlRelation_RightMetaModel()
	 * @model required="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#metaModels'"
	 * @generated
	 */
	MetaModel getRightMetaModel();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation#getRightMetaModel <em>Right Meta Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Meta Model</em>' reference.
	 * @see #getRightMetaModel()
	 * @generated
	 */
	void setRightMetaModel(MetaModel value);

} // BinaryMetaModeRelatedlRelation
