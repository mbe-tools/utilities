/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.labsticc.framework.constraints.model.constraints.ConstraintsPackage;
import fr.labsticc.framework.core.exception.FunctionalException;
import fr.labsticc.framework.core.exception.SystemException;
import fr.labsticc.gmm.model.megamodel.Activity;
import fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation;
import fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation;
import fr.labsticc.gmm.model.megamodel.BinaryRelationPolicy;
import fr.labsticc.gmm.model.megamodel.ComparisonSettings;
import fr.labsticc.gmm.model.megamodel.ConformanceRelation;
import fr.labsticc.gmm.model.megamodel.EnablementType;
import fr.labsticc.gmm.model.megamodel.ErrorDescription;
import fr.labsticc.gmm.model.megamodel.ErrorType;
import fr.labsticc.gmm.model.megamodel.ExecutionContext;
import fr.labsticc.gmm.model.megamodel.FactualRelation;
import fr.labsticc.gmm.model.megamodel.GmmEngine;
import fr.labsticc.gmm.model.megamodel.GmmSpecification;
import fr.labsticc.gmm.model.megamodel.MegamodelFactory;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation;
import fr.labsticc.gmm.model.megamodel.MetaModelSubset;
import fr.labsticc.gmm.model.megamodel.Model;
import fr.labsticc.gmm.model.megamodel.ModelElementsCoveragePolicy;
import fr.labsticc.gmm.model.megamodel.ModelingEnvironment;
import fr.labsticc.gmm.model.megamodel.NamedElement;
import fr.labsticc.gmm.model.megamodel.ObligationRelation;
import fr.labsticc.gmm.model.megamodel.OperationType;
import fr.labsticc.gmm.model.megamodel.ProjectUriRelBinRelPolicy;
import fr.labsticc.gmm.model.megamodel.Relation;
import fr.labsticc.gmm.model.megamodel.RelationPolicy;
import fr.labsticc.gmm.model.megamodel.SetOperation;
import fr.labsticc.gmm.model.megamodel.SetOperationType;
import fr.labsticc.gmm.model.megamodel.SubsetRelationship;
import fr.labsticc.gmm.model.megamodel.SubsetRelationshipType;
import fr.labsticc.gmm.model.megamodel.SubsettedMetaModel;
import fr.labsticc.gmm.model.megamodel.Tool;
import fr.labsticc.gmm.model.megamodel.UriRelatedBinaryRelPolicy;
import fr.labsticc.gmm.model.megamodel.ValidityStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MegamodelPackageImpl extends EPackageImpl implements MegamodelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gmmSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass errorDescriptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass factualRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass obligationRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conformanceRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metaModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass comparisonSettingsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metaModelRelatedRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryMetaModeRelatedlRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryMetaModelRelatedConsistencyRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelElementsCoveragePolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metaModelSubsetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subsetRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subsettedMetaModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass projectUriRelBinRelPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass executionContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gmmEngineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass setOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass toolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum validityStatusEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum enablementTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum subsetRelationshipTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum setOperationTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum errorTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum operationTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType uriEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType functionalExceptionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType systemExceptionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relationPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryRelationPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uriRelatedBinaryRelPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelingEnvironmentEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MegamodelPackageImpl() {
		super(eNS_URI, MegamodelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link MegamodelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MegamodelPackage init() {
		if (isInited) return (MegamodelPackage)EPackage.Registry.INSTANCE.getEPackage(MegamodelPackage.eNS_URI);

		// Obtain or create and register package
		MegamodelPackageImpl theMegamodelPackage = (MegamodelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MegamodelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MegamodelPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ConstraintsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theMegamodelPackage.createPackageContents();

		// Initialize created meta-data
		theMegamodelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMegamodelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MegamodelPackage.eNS_URI, theMegamodelPackage);
		return theMegamodelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGmmSpecification() {
		return gmmSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGmmSpecification_OwnedActivities() {
		return (EReference)gmmSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Description() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_DisplayName() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Id() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModel() {
		return modelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModel_FileExtensions() {
		return (EAttribute)modelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModel_Resources() {
		return (EAttribute)modelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModel_OwnedFactualRelations() {
		return (EReference)modelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModel_Error() {
		return (EReference)modelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getErrorDescription() {
		return errorDescriptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getErrorDescription_ErrorStatus() {
		return (EAttribute)errorDescriptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getErrorDescription_ErroneousElements() {
		return (EAttribute)errorDescriptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModel_OwnedRelations() {
		return (EAttribute)modelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModel_OwnedModels() {
		return (EReference)modelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModel_ParentModel() {
		return (EReference)modelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModel_OwnedObligationRelations() {
		return (EReference)modelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRelation() {
		return relationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRelation_ConnectedModels() {
		return (EAttribute)relationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRelation_ChainedRelations() {
		return (EReference)relationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRelation_OwnedRelationPolicy() {
		return (EReference)relationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRelation_Intention() {
		return (EAttribute)relationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFactualRelation() {
		return factualRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObligationRelation() {
		return obligationRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObligationRelation_OwnedCoveragePolicy() {
		return (EReference)obligationRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObligationRelation_EnablementType() {
		return (EAttribute)obligationRelationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObligationRelation_Deleting() {
		return (EAttribute)obligationRelationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObligationRelation_ValidityStatus() {
		return (EAttribute)obligationRelationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConformanceRelation() {
		return conformanceRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConformanceRelation_Models() {
		return (EReference)conformanceRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConformanceRelation_MetaModel() {
		return (EReference)conformanceRelationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetaModel() {
		return metaModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetaModel_Packages() {
		return (EReference)metaModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetaModel_OwnedSubsets() {
		return (EReference)metaModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetaModel_OwnedComparisonSettings() {
		return (EReference)metaModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetaModel_OwnedConstraintTargets() {
		return (EReference)metaModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComparisonSettings() {
		return comparisonSettingsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComparisonSettings_UseIdForCompare() {
		return (EAttribute)comparisonSettingsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComparisonSettings_NameSimilarityWeight() {
		return (EAttribute)comparisonSettingsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComparisonSettings_PositionSimilarityWeight() {
		return (EAttribute)comparisonSettingsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetaModelRelatedRelation() {
		return metaModelRelatedRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMetaModelRelatedRelation_MetaModels() {
		return (EAttribute)metaModelRelatedRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBinaryMetaModeRelatedlRelation() {
		return binaryMetaModeRelatedlRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinaryMetaModeRelatedlRelation_LeftMetaModel() {
		return (EReference)binaryMetaModeRelatedlRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinaryMetaModeRelatedlRelation_RightMetaModel() {
		return (EReference)binaryMetaModeRelatedlRelationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBinaryMetaModelRelatedConsistencyRelation() {
		return binaryMetaModelRelatedConsistencyRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryMetaModelRelatedConsistencyRelation_DeletingLeft() {
		return (EAttribute)binaryMetaModelRelatedConsistencyRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryMetaModelRelatedConsistencyRelation_DeletingRight() {
		return (EAttribute)binaryMetaModelRelatedConsistencyRelationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelElementsCoveragePolicy() {
		return modelElementsCoveragePolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetaModelSubset() {
		return metaModelSubsetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetaModelSubset_OwnedConstraints() {
		return (EReference)metaModelSubsetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetaModelSubset_Activities() {
		return (EReference)metaModelSubsetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetaModelSubset_OwnedCompositionOperations() {
		return (EReference)metaModelSubsetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActivity() {
		return activityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivity_RequiredSubsets() {
		return (EReference)activityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivity_SupportingTools() {
		return (EReference)activityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubsetRelationship() {
		return subsetRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubsetRelationship_Type() {
		return (EAttribute)subsetRelationshipEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubsetRelationship_TargetSubset() {
		return (EReference)subsetRelationshipEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubsettedMetaModel() {
		return subsettedMetaModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubsettedMetaModel_Subset() {
		return (EReference)subsettedMetaModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubsettedMetaModel_RootMetaModel() {
		return (EReference)subsettedMetaModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubsettedMetaModel_SubsetExtensions() {
		return (EAttribute)subsettedMetaModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProjectUriRelBinRelPolicy() {
		return projectUriRelBinRelPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExecutionContext() {
		return executionContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionContext_ManualLaunch() {
		return (EAttribute)executionContextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionContext_SourceModelOperationType() {
		return (EAttribute)executionContextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExecutionContext_PreviousRelation() {
		return (EReference)executionContextEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGmmEngine() {
		return gmmEngineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGmmEngine_MegaModel() {
		return (EReference)gmmEngineEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSetOperation() {
		return setOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSetOperation_Type() {
		return (EAttribute)setOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetOperation_TargetSubset() {
		return (EReference)setOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTool() {
		return toolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTool_SupportedActivities() {
		return (EReference)toolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getValidityStatus() {
		return validityStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEnablementType() {
		return enablementTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSubsetRelationshipType() {
		return subsetRelationshipTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSetOperationType() {
		return setOperationTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getErrorType() {
		return errorTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOperationType() {
		return operationTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getURI() {
		return uriEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getFunctionalException() {
		return functionalExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSystemException() {
		return systemExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRelationPolicy() {
		return relationPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRelationPolicy_Relation() {
		return (EReference)relationPolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBinaryRelationPolicy() {
		return binaryRelationPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUriRelatedBinaryRelPolicy() {
		return uriRelatedBinaryRelPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelingEnvironment() {
		return modelingEnvironmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelingEnvironment_SourceModel() {
		return (EReference)modelingEnvironmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelingEnvironment_Relation() {
		return (EReference)modelingEnvironmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MegamodelFactory getMegamodelFactory() {
		return (MegamodelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		gmmSpecificationEClass = createEClass(GMM_SPECIFICATION);
		createEReference(gmmSpecificationEClass, GMM_SPECIFICATION__OWNED_ACTIVITIES);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__DESCRIPTION);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__DISPLAY_NAME);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__ID);

		modelEClass = createEClass(MODEL);
		createEAttribute(modelEClass, MODEL__OWNED_RELATIONS);
		createEReference(modelEClass, MODEL__OWNED_MODELS);
		createEReference(modelEClass, MODEL__PARENT_MODEL);
		createEReference(modelEClass, MODEL__OWNED_OBLIGATION_RELATIONS);
		createEAttribute(modelEClass, MODEL__FILE_EXTENSIONS);
		createEAttribute(modelEClass, MODEL__RESOURCES);
		createEReference(modelEClass, MODEL__OWNED_FACTUAL_RELATIONS);
		createEReference(modelEClass, MODEL__ERROR);

		errorDescriptionEClass = createEClass(ERROR_DESCRIPTION);
		createEAttribute(errorDescriptionEClass, ERROR_DESCRIPTION__ERROR_STATUS);
		createEAttribute(errorDescriptionEClass, ERROR_DESCRIPTION__ERRONEOUS_ELEMENTS);

		relationEClass = createEClass(RELATION);
		createEAttribute(relationEClass, RELATION__CONNECTED_MODELS);
		createEReference(relationEClass, RELATION__CHAINED_RELATIONS);
		createEReference(relationEClass, RELATION__OWNED_RELATION_POLICY);
		createEAttribute(relationEClass, RELATION__INTENTION);

		metaModelEClass = createEClass(META_MODEL);
		createEReference(metaModelEClass, META_MODEL__PACKAGES);
		createEReference(metaModelEClass, META_MODEL__OWNED_SUBSETS);
		createEReference(metaModelEClass, META_MODEL__OWNED_COMPARISON_SETTINGS);
		createEReference(metaModelEClass, META_MODEL__OWNED_CONSTRAINT_TARGETS);

		comparisonSettingsEClass = createEClass(COMPARISON_SETTINGS);
		createEAttribute(comparisonSettingsEClass, COMPARISON_SETTINGS__USE_ID_FOR_COMPARE);
		createEAttribute(comparisonSettingsEClass, COMPARISON_SETTINGS__NAME_SIMILARITY_WEIGHT);
		createEAttribute(comparisonSettingsEClass, COMPARISON_SETTINGS__POSITION_SIMILARITY_WEIGHT);

		factualRelationEClass = createEClass(FACTUAL_RELATION);

		obligationRelationEClass = createEClass(OBLIGATION_RELATION);
		createEReference(obligationRelationEClass, OBLIGATION_RELATION__OWNED_COVERAGE_POLICY);
		createEAttribute(obligationRelationEClass, OBLIGATION_RELATION__ENABLEMENT_TYPE);
		createEAttribute(obligationRelationEClass, OBLIGATION_RELATION__DELETING);
		createEAttribute(obligationRelationEClass, OBLIGATION_RELATION__VALIDITY_STATUS);

		conformanceRelationEClass = createEClass(CONFORMANCE_RELATION);
		createEReference(conformanceRelationEClass, CONFORMANCE_RELATION__MODELS);
		createEReference(conformanceRelationEClass, CONFORMANCE_RELATION__META_MODEL);

		metaModelRelatedRelationEClass = createEClass(META_MODEL_RELATED_RELATION);
		createEAttribute(metaModelRelatedRelationEClass, META_MODEL_RELATED_RELATION__META_MODELS);

		binaryMetaModeRelatedlRelationEClass = createEClass(BINARY_META_MODE_RELATEDL_RELATION);
		createEReference(binaryMetaModeRelatedlRelationEClass, BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL);
		createEReference(binaryMetaModeRelatedlRelationEClass, BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL);

		binaryMetaModelRelatedConsistencyRelationEClass = createEClass(BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION);
		createEAttribute(binaryMetaModelRelatedConsistencyRelationEClass, BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_LEFT);
		createEAttribute(binaryMetaModelRelatedConsistencyRelationEClass, BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_RIGHT);

		relationPolicyEClass = createEClass(RELATION_POLICY);
		createEReference(relationPolicyEClass, RELATION_POLICY__RELATION);

		binaryRelationPolicyEClass = createEClass(BINARY_RELATION_POLICY);

		uriRelatedBinaryRelPolicyEClass = createEClass(URI_RELATED_BINARY_REL_POLICY);

		projectUriRelBinRelPolicyEClass = createEClass(PROJECT_URI_REL_BIN_REL_POLICY);

		modelingEnvironmentEClass = createEClass(MODELING_ENVIRONMENT);
		createEReference(modelingEnvironmentEClass, MODELING_ENVIRONMENT__SOURCE_MODEL);
		createEReference(modelingEnvironmentEClass, MODELING_ENVIRONMENT__RELATION);

		executionContextEClass = createEClass(EXECUTION_CONTEXT);
		createEAttribute(executionContextEClass, EXECUTION_CONTEXT__MANUAL_LAUNCH);
		createEAttribute(executionContextEClass, EXECUTION_CONTEXT__SOURCE_MODEL_OPERATION_TYPE);
		createEReference(executionContextEClass, EXECUTION_CONTEXT__PREVIOUS_RELATION);

		modelElementsCoveragePolicyEClass = createEClass(MODEL_ELEMENTS_COVERAGE_POLICY);

		subsettedMetaModelEClass = createEClass(SUBSETTED_META_MODEL);
		createEReference(subsettedMetaModelEClass, SUBSETTED_META_MODEL__SUBSET);
		createEReference(subsettedMetaModelEClass, SUBSETTED_META_MODEL__ROOT_META_MODEL);
		createEAttribute(subsettedMetaModelEClass, SUBSETTED_META_MODEL__SUBSET_EXTENSIONS);

		metaModelSubsetEClass = createEClass(META_MODEL_SUBSET);
		createEReference(metaModelSubsetEClass, META_MODEL_SUBSET__OWNED_CONSTRAINTS);
		createEReference(metaModelSubsetEClass, META_MODEL_SUBSET__ACTIVITIES);
		createEReference(metaModelSubsetEClass, META_MODEL_SUBSET__OWNED_COMPOSITION_OPERATIONS);

		activityEClass = createEClass(ACTIVITY);
		createEReference(activityEClass, ACTIVITY__REQUIRED_SUBSETS);
		createEReference(activityEClass, ACTIVITY__SUPPORTING_TOOLS);

		subsetRelationshipEClass = createEClass(SUBSET_RELATIONSHIP);
		createEAttribute(subsetRelationshipEClass, SUBSET_RELATIONSHIP__TYPE);
		createEReference(subsetRelationshipEClass, SUBSET_RELATIONSHIP__TARGET_SUBSET);

		gmmEngineEClass = createEClass(GMM_ENGINE);
		createEReference(gmmEngineEClass, GMM_ENGINE__MEGA_MODEL);

		setOperationEClass = createEClass(SET_OPERATION);
		createEAttribute(setOperationEClass, SET_OPERATION__TYPE);
		createEReference(setOperationEClass, SET_OPERATION__TARGET_SUBSET);

		toolEClass = createEClass(TOOL);
		createEReference(toolEClass, TOOL__SUPPORTED_ACTIVITIES);

		// Create enums
		errorTypeEEnum = createEEnum(ERROR_TYPE);
		operationTypeEEnum = createEEnum(OPERATION_TYPE);
		validityStatusEEnum = createEEnum(VALIDITY_STATUS);
		enablementTypeEEnum = createEEnum(ENABLEMENT_TYPE);
		subsetRelationshipTypeEEnum = createEEnum(SUBSET_RELATIONSHIP_TYPE);
		setOperationTypeEEnum = createEEnum(SET_OPERATION_TYPE);

		// Create data types
		uriEDataType = createEDataType(URI);
		functionalExceptionEDataType = createEDataType(FUNCTIONAL_EXCEPTION);
		systemExceptionEDataType = createEDataType(SYSTEM_EXCEPTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ConstraintsPackage theConstraintsPackage = (ConstraintsPackage)EPackage.Registry.INSTANCE.getEPackage(ConstraintsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		gmmSpecificationEClass.getESuperTypes().add(this.getModel());
		modelEClass.getESuperTypes().add(this.getNamedElement());
		errorDescriptionEClass.getESuperTypes().add(this.getNamedElement());
		relationEClass.getESuperTypes().add(this.getNamedElement());
		metaModelEClass.getESuperTypes().add(this.getModel());
		factualRelationEClass.getESuperTypes().add(this.getRelation());
		obligationRelationEClass.getESuperTypes().add(this.getRelation());
		conformanceRelationEClass.getESuperTypes().add(this.getFactualRelation());
		metaModelRelatedRelationEClass.getESuperTypes().add(this.getFactualRelation());
		binaryMetaModeRelatedlRelationEClass.getESuperTypes().add(this.getMetaModelRelatedRelation());
		binaryMetaModelRelatedConsistencyRelationEClass.getESuperTypes().add(this.getObligationRelation());
		binaryMetaModelRelatedConsistencyRelationEClass.getESuperTypes().add(this.getBinaryMetaModeRelatedlRelation());
		relationPolicyEClass.getESuperTypes().add(this.getNamedElement());
		binaryRelationPolicyEClass.getESuperTypes().add(this.getRelationPolicy());
		uriRelatedBinaryRelPolicyEClass.getESuperTypes().add(this.getBinaryRelationPolicy());
		projectUriRelBinRelPolicyEClass.getESuperTypes().add(this.getUriRelatedBinaryRelPolicy());
		modelingEnvironmentEClass.getESuperTypes().add(this.getModel());
		modelElementsCoveragePolicyEClass.getESuperTypes().add(this.getNamedElement());
		subsettedMetaModelEClass.getESuperTypes().add(this.getMetaModel());
		metaModelSubsetEClass.getESuperTypes().add(this.getNamedElement());
		activityEClass.getESuperTypes().add(this.getNamedElement());
		subsetRelationshipEClass.getESuperTypes().add(this.getNamedElement());
		gmmEngineEClass.getESuperTypes().add(this.getNamedElement());
		setOperationEClass.getESuperTypes().add(this.getNamedElement());
		toolEClass.getESuperTypes().add(this.getModel());

		// Initialize classes and features; add operations and parameters
		initEClass(gmmSpecificationEClass, GmmSpecification.class, "GmmSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGmmSpecification_OwnedActivities(), this.getActivity(), null, "ownedActivities", null, 0, -1, GmmSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedElement_Description(), ecorePackage.getEString(), "description", null, 0, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedElement_DisplayName(), ecorePackage.getEString(), "displayName", null, 0, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedElement_Id(), ecorePackage.getEString(), "id", null, 1, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(modelEClass, Model.class, "Model", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getModel_OwnedRelations(), ecorePackage.getEFeatureMapEntry(), "ownedRelations", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModel_OwnedModels(), this.getModel(), this.getModel_ParentModel(), "ownedModels", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModel_ParentModel(), this.getModel(), this.getModel_OwnedModels(), "parentModel", null, 0, 1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModel_OwnedObligationRelations(), this.getObligationRelation(), null, "ownedObligationRelations", null, 0, -1, Model.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getModel_FileExtensions(), ecorePackage.getEString(), "fileExtensions", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModel_Resources(), ecorePackage.getEResource(), "resources", null, 0, -1, Model.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModel_OwnedFactualRelations(), this.getFactualRelation(), null, "ownedFactualRelations", null, 0, -1, Model.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getModel_Error(), this.getErrorDescription(), null, "error", null, 0, 1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(errorDescriptionEClass, ErrorDescription.class, "ErrorDescription", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getErrorDescription_ErrorStatus(), this.getErrorType(), "errorStatus", null, 0, 1, ErrorDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(this.getModel());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEEList());
		g1.getETypeArguments().add(g2);
		EGenericType g3 = createEGenericType(ecorePackage.getEObject());
		g2.getETypeArguments().add(g3);
		initEAttribute(getErrorDescription_ErroneousElements(), g1, "erroneousElements", null, 0, 1, ErrorDescription.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(relationEClass, Relation.class, "Relation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRelation_ConnectedModels(), ecorePackage.getEFeatureMapEntry(), "connectedModels", null, 0, -1, Relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRelation_ChainedRelations(), this.getRelation(), null, "chainedRelations", null, 0, -1, Relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRelation_OwnedRelationPolicy(), this.getRelationPolicy(), this.getRelationPolicy_Relation(), "ownedRelationPolicy", null, 1, 1, Relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRelation_Intention(), ecorePackage.getEString(), "intention", null, 1, 1, Relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(relationEClass, ecorePackage.getEString(), "getDisplayName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "uris", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(relationEClass, ecorePackage.getEBoolean(), "concerns", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "modelElement", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(relationEClass, ecorePackage.getEBoolean(), "concerns", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "resourceUri", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(metaModelEClass, MetaModel.class, "MetaModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMetaModel_Packages(), ecorePackage.getEPackage(), null, "packages", null, 0, -1, MetaModel.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMetaModel_OwnedSubsets(), this.getMetaModelSubset(), null, "ownedSubsets", null, 0, -1, MetaModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetaModel_OwnedComparisonSettings(), this.getComparisonSettings(), null, "ownedComparisonSettings", null, 0, 1, MetaModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetaModel_OwnedConstraintTargets(), theConstraintsPackage.getTarget(), null, "ownedConstraintTargets", null, 0, -1, MetaModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(metaModelEClass, ecorePackage.getEBoolean(), "declares", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getModel(), "model", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(metaModelEClass, ecorePackage.getEBoolean(), "declares", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "uri", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(metaModelEClass, ecorePackage.getEBoolean(), "declares", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "instance", 1, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(metaModelEClass, ecorePackage.getEDouble(), "getNameSimilarityWeight", 1, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(metaModelEClass, ecorePackage.getEDouble(), "getPositionSimilarityWeight", 1, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(metaModelEClass, ecorePackage.getEBoolean(), "useIdForCompare", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(comparisonSettingsEClass, ComparisonSettings.class, "ComparisonSettings", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComparisonSettings_UseIdForCompare(), ecorePackage.getEBoolean(), "useIdForCompare", null, 1, 1, ComparisonSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComparisonSettings_NameSimilarityWeight(), ecorePackage.getEDoubleObject(), "nameSimilarityWeight", null, 0, 1, ComparisonSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComparisonSettings_PositionSimilarityWeight(), ecorePackage.getEDoubleObject(), "positionSimilarityWeight", null, 0, 1, ComparisonSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(factualRelationEClass, FactualRelation.class, "FactualRelation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(obligationRelationEClass, ObligationRelation.class, "ObligationRelation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObligationRelation_OwnedCoveragePolicy(), this.getModelElementsCoveragePolicy(), null, "ownedCoveragePolicy", null, 0, 1, ObligationRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getObligationRelation_EnablementType(), this.getEnablementType(), "enablementType", null, 1, 1, ObligationRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getObligationRelation_Deleting(), ecorePackage.getEBoolean(), "deleting", null, 0, 1, ObligationRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getObligationRelation_ValidityStatus(), this.getValidityStatus(), "validityStatus", null, 1, 1, ObligationRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(obligationRelationEClass, this.getModel(), "establishValidity", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getModelingEnvironment(), "modelingEnvironment", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getExecutionContext(), "executionContext", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getFunctionalException());
		addEException(op, this.getSystemException());

		op = addEOperation(obligationRelationEClass, ecorePackage.getEBoolean(), "isEnabled", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getModel(), "model", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getExecutionContext(), "executionContext", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(obligationRelationEClass, ecorePackage.getEBoolean(), "canDelete", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getModel(), "model", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(conformanceRelationEClass, ConformanceRelation.class, "ConformanceRelation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConformanceRelation_Models(), this.getModel(), null, "models", null, 1, -1, ConformanceRelation.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getConformanceRelation_MetaModel(), this.getMetaModel(), null, "metaModel", null, 1, 1, ConformanceRelation.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(metaModelRelatedRelationEClass, MetaModelRelatedRelation.class, "MetaModelRelatedRelation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMetaModelRelatedRelation_MetaModels(), ecorePackage.getEFeatureMapEntry(), "metaModels", null, 1, -1, MetaModelRelatedRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(metaModelRelatedRelationEClass, this.getMetaModel(), "getMetaModelsList", 1, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(binaryMetaModeRelatedlRelationEClass, BinaryMetaModeRelatedlRelation.class, "BinaryMetaModeRelatedlRelation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBinaryMetaModeRelatedlRelation_LeftMetaModel(), this.getMetaModel(), null, "leftMetaModel", null, 1, 1, BinaryMetaModeRelatedlRelation.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryMetaModeRelatedlRelation_RightMetaModel(), this.getMetaModel(), null, "rightMetaModel", null, 1, 1, BinaryMetaModeRelatedlRelation.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(binaryMetaModelRelatedConsistencyRelationEClass, BinaryMetaModelRelatedConsistencyRelation.class, "BinaryMetaModelRelatedConsistencyRelation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBinaryMetaModelRelatedConsistencyRelation_DeletingLeft(), ecorePackage.getEBoolean(), "deletingLeft", null, 0, 1, BinaryMetaModelRelatedConsistencyRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBinaryMetaModelRelatedConsistencyRelation_DeletingRight(), ecorePackage.getEBoolean(), "deletingRight", null, 0, 1, BinaryMetaModelRelatedConsistencyRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(relationPolicyEClass, RelationPolicy.class, "RelationPolicy", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRelationPolicy_Relation(), this.getRelation(), this.getRelation_OwnedRelationPolicy(), "relation", null, 1, 1, RelationPolicy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(binaryRelationPolicyEClass, BinaryRelationPolicy.class, "BinaryRelationPolicy", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(uriRelatedBinaryRelPolicyEClass, UriRelatedBinaryRelPolicy.class, "UriRelatedBinaryRelPolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(uriRelatedBinaryRelPolicyEClass, ecorePackage.getEBoolean(), "areRelated", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "leftUri", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "rightUri", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(uriRelatedBinaryRelPolicyEClass, this.getURI(), "correspondingUri", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "uri", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(projectUriRelBinRelPolicyEClass, ProjectUriRelBinRelPolicy.class, "ProjectUriRelBinRelPolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(projectUriRelBinRelPolicyEClass, null, "unsetCorrespondingProject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "projectUri", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(modelingEnvironmentEClass, ModelingEnvironment.class, "ModelingEnvironment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModelingEnvironment_SourceModel(), this.getModel(), null, "sourceModel", null, 1, 1, ModelingEnvironment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModelingEnvironment_Relation(), this.getRelation(), null, "relation", null, 1, 1, ModelingEnvironment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(executionContextEClass, ExecutionContext.class, "ExecutionContext", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExecutionContext_ManualLaunch(), ecorePackage.getEBoolean(), "manualLaunch", null, 0, 1, ExecutionContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionContext_SourceModelOperationType(), this.getOperationType(), "sourceModelOperationType", null, 1, 1, ExecutionContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExecutionContext_PreviousRelation(), this.getRelation(), null, "previousRelation", null, 0, 1, ExecutionContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(modelElementsCoveragePolicyEClass, ModelElementsCoveragePolicy.class, "ModelElementsCoveragePolicy", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(modelElementsCoveragePolicyEClass, ecorePackage.getEBoolean(), "isConsidered", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "modelElement", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(subsettedMetaModelEClass, SubsettedMetaModel.class, "SubsettedMetaModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubsettedMetaModel_Subset(), this.getMetaModelSubset(), null, "subset", null, 1, 1, SubsettedMetaModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubsettedMetaModel_RootMetaModel(), this.getMetaModel(), null, "rootMetaModel", null, 1, 1, SubsettedMetaModel.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getSubsettedMetaModel_SubsetExtensions(), ecorePackage.getEString(), "subsetExtensions", null, 1, -1, SubsettedMetaModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(metaModelSubsetEClass, MetaModelSubset.class, "MetaModelSubset", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMetaModelSubset_OwnedConstraints(), theConstraintsPackage.getCardinalityConstraint(), null, "ownedConstraints", null, 0, -1, MetaModelSubset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetaModelSubset_Activities(), this.getActivity(), this.getActivity_RequiredSubsets(), "activities", null, 1, -1, MetaModelSubset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetaModelSubset_OwnedCompositionOperations(), this.getSetOperation(), null, "ownedCompositionOperations", null, 0, -1, MetaModelSubset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activityEClass, Activity.class, "Activity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActivity_RequiredSubsets(), this.getMetaModelSubset(), this.getMetaModelSubset_Activities(), "requiredSubsets", null, 0, -1, Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivity_SupportingTools(), this.getTool(), this.getTool_SupportedActivities(), "supportingTools", null, 0, -1, Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(subsetRelationshipEClass, SubsetRelationship.class, "SubsetRelationship", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSubsetRelationship_Type(), this.getSubsetRelationshipType(), "type", null, 1, 1, SubsetRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubsetRelationship_TargetSubset(), this.getMetaModelSubset(), null, "targetSubset", null, 1, 1, SubsetRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(gmmEngineEClass, GmmEngine.class, "GmmEngine", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGmmEngine_MegaModel(), this.getModel(), null, "megaModel", null, 1, 1, GmmEngine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(gmmEngineEClass, null, "establishValidity", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "changedUri", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getExecutionContext(), "executionContext", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(setOperationEClass, SetOperation.class, "SetOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSetOperation_Type(), this.getSetOperationType(), "type", null, 1, 1, SetOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSetOperation_TargetSubset(), this.getMetaModelSubset(), null, "targetSubset", null, 1, 1, SetOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(toolEClass, Tool.class, "Tool", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTool_SupportedActivities(), this.getActivity(), this.getActivity_SupportingTools(), "supportedActivities", null, 1, -1, Tool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(errorTypeEEnum, ErrorType.class, "ErrorType");
		addEEnumLiteral(errorTypeEEnum, ErrorType.GMM);
		addEEnumLiteral(errorTypeEEnum, ErrorType.OTHER);

		initEEnum(operationTypeEEnum, OperationType.class, "OperationType");
		addEEnumLiteral(operationTypeEEnum, OperationType.CREATED);
		addEEnumLiteral(operationTypeEEnum, OperationType.READ);
		addEEnumLiteral(operationTypeEEnum, OperationType.UPDATED);
		addEEnumLiteral(operationTypeEEnum, OperationType.DELETED);

		initEEnum(validityStatusEEnum, ValidityStatus.class, "ValidityStatus");
		addEEnumLiteral(validityStatusEEnum, ValidityStatus.VALID);
		addEEnumLiteral(validityStatusEEnum, ValidityStatus.INVALID);

		initEEnum(enablementTypeEEnum, EnablementType.class, "EnablementType");
		addEEnumLiteral(enablementTypeEEnum, EnablementType.NO_ERROR);
		addEEnumLiteral(enablementTypeEEnum, EnablementType.MANUAL_ONLY_NO_ERROR);
		addEEnumLiteral(enablementTypeEEnum, EnablementType.MANUAL_ONLY);
		addEEnumLiteral(enablementTypeEEnum, EnablementType.ALWAYS);
		addEEnumLiteral(enablementTypeEEnum, EnablementType.NEVER);

		initEEnum(subsetRelationshipTypeEEnum, SubsetRelationshipType.class, "SubsetRelationshipType");
		addEEnumLiteral(subsetRelationshipTypeEEnum, SubsetRelationshipType.INCLUSION);
		addEEnumLiteral(subsetRelationshipTypeEEnum, SubsetRelationshipType.INCOMPATIBILITY);
		addEEnumLiteral(subsetRelationshipTypeEEnum, SubsetRelationshipType.EQUIVALENCE);
		addEEnumLiteral(subsetRelationshipTypeEEnum, SubsetRelationshipType.INTERSECTION);

		initEEnum(setOperationTypeEEnum, SetOperationType.class, "SetOperationType");
		addEEnumLiteral(setOperationTypeEEnum, SetOperationType.UNION);
		addEEnumLiteral(setOperationTypeEEnum, SetOperationType.INTERSECTION);
		addEEnumLiteral(setOperationTypeEEnum, SetOperationType.DIFFERENCE);
		addEEnumLiteral(setOperationTypeEEnum, SetOperationType.SYMMETRIC_DIFFERENCE);
		addEEnumLiteral(setOperationTypeEEnum, SetOperationType.POWER_SET);

		// Initialize data types
		initEDataType(uriEDataType, org.eclipse.emf.common.util.URI.class, "URI", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(functionalExceptionEDataType, FunctionalException.class, "FunctionalException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(systemExceptionEDataType, SystemException.class, "SystemException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";	
		addAnnotation
		  (getModel_OwnedRelations(), 
		   source, 
		   new String[] {
			 "kind", "group"
		   });	
		addAnnotation
		  (getModel_OwnedObligationRelations(), 
		   source, 
		   new String[] {
			 "group", "#ownedRelations"
		   });	
		addAnnotation
		  (getModel_OwnedFactualRelations(), 
		   source, 
		   new String[] {
			 "group", "#ownedRelations"
		   });	
		addAnnotation
		  (getRelation_ConnectedModels(), 
		   source, 
		   new String[] {
			 "kind", "group"
		   });	
		addAnnotation
		  (getConformanceRelation_Models(), 
		   source, 
		   new String[] {
			 "group", "#connectedModels"
		   });	
		addAnnotation
		  (getConformanceRelation_MetaModel(), 
		   source, 
		   new String[] {
			 "group", "#connectedModels"
		   });	
		addAnnotation
		  (getMetaModelRelatedRelation_MetaModels(), 
		   source, 
		   new String[] {
			 "kind", "group"
		   });	
		addAnnotation
		  (getBinaryMetaModeRelatedlRelation_LeftMetaModel(), 
		   source, 
		   new String[] {
			 "group", "#metaModels"
		   });	
		addAnnotation
		  (getBinaryMetaModeRelatedlRelation_RightMetaModel(), 
		   source, 
		   new String[] {
			 "group", "#metaModels"
		   });
	}

} //MegamodelPackageImpl
