/**
 */
package fr.labsticc.gmm.model.megamodel;

import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Error Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ErrorDescription#getErrorStatus <em>Error Status</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ErrorDescription#getErroneousElements <em>Erroneous Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getErrorDescription()
 * @model
 * @generated
 */
public interface ErrorDescription extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Error Status</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.labsticc.gmm.model.megamodel.ErrorType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Error Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Error Status</em>' attribute.
	 * @see fr.labsticc.gmm.model.megamodel.ErrorType
	 * @see #setErrorStatus(ErrorType)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getErrorDescription_ErrorStatus()
	 * @model
	 * @generated
	 */
	ErrorType getErrorStatus();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ErrorDescription#getErrorStatus <em>Error Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Error Status</em>' attribute.
	 * @see fr.labsticc.gmm.model.megamodel.ErrorType
	 * @see #getErrorStatus()
	 * @generated
	 */
	void setErrorStatus(ErrorType value);

	/**
	 * Returns the value of the '<em><b>Erroneous Elements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Erroneous Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Erroneous Elements</em>' attribute.
	 * @see #setErroneousElements(Map)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getErrorDescription_ErroneousElements()
	 * @model transient="true"
	 * @generated
	 */
	Map<Model, EList<EObject>> getErroneousElements();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ErrorDescription#getErroneousElements <em>Erroneous Elements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Erroneous Elements</em>' attribute.
	 * @see #getErroneousElements()
	 * @generated
	 */
	void setErroneousElements(Map<Model, EList<EObject>> value);

} // ErrorDescription
