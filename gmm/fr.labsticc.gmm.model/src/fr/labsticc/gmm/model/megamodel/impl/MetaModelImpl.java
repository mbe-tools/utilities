/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel.impl;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.labsticc.framework.constraints.model.constraints.Target;
import fr.labsticc.gmm.model.megamodel.ComparisonSettings;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.MetaModelSubset;
import fr.labsticc.gmm.model.megamodel.Model;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Meta Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelImpl#getPackages <em>Packages</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelImpl#getOwnedSubsets <em>Owned Subsets</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelImpl#getOwnedComparisonSettings <em>Owned Comparison Settings</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelImpl#getOwnedConstraintTargets <em>Owned Constraint Targets</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MetaModelImpl extends ModelImpl implements MetaModel {
	/**
	 * The cached value of the '{@link #getOwnedSubsets() <em>Owned Subsets</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedSubsets()
	 * @generated
	 * @ordered
	 */
	protected EList<MetaModelSubset> ownedSubsets;

	/**
	 * The cached value of the '{@link #getOwnedComparisonSettings() <em>Owned Comparison Settings</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedComparisonSettings()
	 * @generated
	 * @ordered
	 */
	protected ComparisonSettings ownedComparisonSettings;

	/**
	 * The cached value of the '{@link #getOwnedConstraintTargets() <em>Owned Constraint Targets</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedConstraintTargets()
	 * @generated
	 * @ordered
	 */
	protected EList<Target> ownedConstraintTargets;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetaModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.META_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EPackage> getPackages() {
		final EList<EPackage> packages = new BasicEList<EPackage>();
		
		for ( final Resource packageRes : getResources() ) {
			final Iterator<EObject> allContent = packageRes.getAllContents(); 
			
			while ( allContent.hasNext() ) {
				final EObject element = allContent.next();
				
				if ( element instanceof EPackage ) {
					packages.add( (EPackage) element );
				}
			}
		}
		
		return packages;
//		return (EList) getRootObjects();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MetaModelSubset> getOwnedSubsets() {
		if (ownedSubsets == null) {
			ownedSubsets = new EObjectContainmentEList<MetaModelSubset>(MetaModelSubset.class, this, MegamodelPackage.META_MODEL__OWNED_SUBSETS);
		}
		return ownedSubsets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonSettings getOwnedComparisonSettings() {
		return ownedComparisonSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOwnedComparisonSettings(ComparisonSettings newOwnedComparisonSettings, NotificationChain msgs) {
		ComparisonSettings oldOwnedComparisonSettings = ownedComparisonSettings;
		ownedComparisonSettings = newOwnedComparisonSettings;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MegamodelPackage.META_MODEL__OWNED_COMPARISON_SETTINGS, oldOwnedComparisonSettings, newOwnedComparisonSettings);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOwnedComparisonSettings(ComparisonSettings newOwnedComparisonSettings) {
		if (newOwnedComparisonSettings != ownedComparisonSettings) {
			NotificationChain msgs = null;
			if (ownedComparisonSettings != null)
				msgs = ((InternalEObject)ownedComparisonSettings).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MegamodelPackage.META_MODEL__OWNED_COMPARISON_SETTINGS, null, msgs);
			if (newOwnedComparisonSettings != null)
				msgs = ((InternalEObject)newOwnedComparisonSettings).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MegamodelPackage.META_MODEL__OWNED_COMPARISON_SETTINGS, null, msgs);
			msgs = basicSetOwnedComparisonSettings(newOwnedComparisonSettings, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.META_MODEL__OWNED_COMPARISON_SETTINGS, newOwnedComparisonSettings, newOwnedComparisonSettings));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Target> getOwnedConstraintTargets() {
		if (ownedConstraintTargets == null) {
			ownedConstraintTargets = new EObjectContainmentEList<Target>(Target.class, this, MegamodelPackage.META_MODEL__OWNED_CONSTRAINT_TARGETS);
		}
		return ownedConstraintTargets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean declares(EObject instance) {
		if ( instance == null ) {
			return false;
		}

		if ( getPackages().isEmpty() ) {
			if ( instance.eResource() == null ) {
				return false;
			}
			
			return declares( instance.eResource().getURI() );
		}
		
		final EPackage ePackage = instance.eClass().getEPackage();
		
		return getPackages().contains( ePackage );
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean declares(final URI uri) {
		if ( uri == null ) {
			return false;
		}
		
		return getFileExtensions().contains(  uri.fileExtension() );
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean declares( final Model p_model ) {
		for ( final Resource res : p_model.getResources() ) {
			if ( declares( res.getURI() ) ) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL__OWNED_SUBSETS:
				return ((InternalEList<?>)getOwnedSubsets()).basicRemove(otherEnd, msgs);
			case MegamodelPackage.META_MODEL__OWNED_COMPARISON_SETTINGS:
				return basicSetOwnedComparisonSettings(null, msgs);
			case MegamodelPackage.META_MODEL__OWNED_CONSTRAINT_TARGETS:
				return ((InternalEList<?>)getOwnedConstraintTargets()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL__PACKAGES:
				return getPackages();
			case MegamodelPackage.META_MODEL__OWNED_SUBSETS:
				return getOwnedSubsets();
			case MegamodelPackage.META_MODEL__OWNED_COMPARISON_SETTINGS:
				return getOwnedComparisonSettings();
			case MegamodelPackage.META_MODEL__OWNED_CONSTRAINT_TARGETS:
				return getOwnedConstraintTargets();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL__OWNED_SUBSETS:
				getOwnedSubsets().clear();
				getOwnedSubsets().addAll((Collection<? extends MetaModelSubset>)newValue);
				return;
			case MegamodelPackage.META_MODEL__OWNED_COMPARISON_SETTINGS:
				setOwnedComparisonSettings((ComparisonSettings)newValue);
				return;
			case MegamodelPackage.META_MODEL__OWNED_CONSTRAINT_TARGETS:
				getOwnedConstraintTargets().clear();
				getOwnedConstraintTargets().addAll((Collection<? extends Target>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL__OWNED_SUBSETS:
				getOwnedSubsets().clear();
				return;
			case MegamodelPackage.META_MODEL__OWNED_COMPARISON_SETTINGS:
				setOwnedComparisonSettings((ComparisonSettings)null);
				return;
			case MegamodelPackage.META_MODEL__OWNED_CONSTRAINT_TARGETS:
				getOwnedConstraintTargets().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL__PACKAGES:
				return !getPackages().isEmpty();
			case MegamodelPackage.META_MODEL__OWNED_SUBSETS:
				return ownedSubsets != null && !ownedSubsets.isEmpty();
			case MegamodelPackage.META_MODEL__OWNED_COMPARISON_SETTINGS:
				return ownedComparisonSettings != null;
			case MegamodelPackage.META_MODEL__OWNED_CONSTRAINT_TARGETS:
				return ownedConstraintTargets != null && !ownedConstraintTargets.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	public boolean useIdForCompare() {
		final ComparisonSettings compSettings = getOwnedComparisonSettings();
		
		if ( compSettings == null ) {
			return false;
		}
		
		return compSettings.isUseIdForCompare();
	}
	
	public double getNameSimilarityWeight() {
		final ComparisonSettings compSettings = getOwnedComparisonSettings();
		
		if ( compSettings == null ) {
			return 0.5d;
		}
		
		return compSettings.getNameSimilarityWeight();
	}
	
	public double getPositionSimilarityWeight() {
		final ComparisonSettings compSettings = getOwnedComparisonSettings();
		
		if ( compSettings == null ) {
			return 0.5d;
		}
		
		return compSettings.getPositionSimilarityWeight();
	}
	
	@Override
	public String getDisplayName() {
		return displayName;
	}
} //MetaModelImpl
