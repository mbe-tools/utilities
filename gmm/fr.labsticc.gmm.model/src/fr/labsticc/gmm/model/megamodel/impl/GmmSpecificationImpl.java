/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import fr.labsticc.gmm.model.megamodel.Activity;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import fr.labsticc.gmm.model.megamodel.GmmSpecification;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Gmm Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.GmmSpecificationImpl#getOwnedActivities <em>Owned Activities</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GmmSpecificationImpl extends ModelImpl implements GmmSpecification {
	/**
	 * The cached value of the '{@link #getOwnedActivities() <em>Owned Activities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedActivities()
	 * @generated
	 * @ordered
	 */
	protected EList<Activity> ownedActivities;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GmmSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.GMM_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Activity> getOwnedActivities() {
		if (ownedActivities == null) {
			ownedActivities = new EObjectContainmentEList<Activity>(Activity.class, this, MegamodelPackage.GMM_SPECIFICATION__OWNED_ACTIVITIES);
		}
		return ownedActivities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.GMM_SPECIFICATION__OWNED_ACTIVITIES:
				return ((InternalEList<?>)getOwnedActivities()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.GMM_SPECIFICATION__OWNED_ACTIVITIES:
				return getOwnedActivities();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.GMM_SPECIFICATION__OWNED_ACTIVITIES:
				getOwnedActivities().clear();
				getOwnedActivities().addAll((Collection<? extends Activity>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.GMM_SPECIFICATION__OWNED_ACTIVITIES:
				getOwnedActivities().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.GMM_SPECIFICATION__OWNED_ACTIVITIES:
				return ownedActivities != null && !ownedActivities.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //GmmSpecificationImpl
