/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import fr.labsticc.gmm.model.megamodel.ExecutionContext;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;

import fr.labsticc.gmm.model.megamodel.OperationType;
import fr.labsticc.gmm.model.megamodel.Relation;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Execution Context</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ExecutionContextImpl#isManualLaunch <em>Manual Launch</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ExecutionContextImpl#getSourceModelOperationType <em>Source Model Operation Type</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ExecutionContextImpl#getPreviousRelation <em>Previous Relation</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ExecutionContextImpl extends EObjectImpl implements ExecutionContext {
	/**
	 * The default value of the '{@link #isManualLaunch() <em>Manual Launch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isManualLaunch()
	 * @generated
	 * @ordered
	 */
	protected static final boolean MANUAL_LAUNCH_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isManualLaunch() <em>Manual Launch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isManualLaunch()
	 * @generated
	 * @ordered
	 */
	protected boolean manualLaunch = MANUAL_LAUNCH_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceModelOperationType() <em>Source Model Operation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceModelOperationType()
	 * @generated
	 * @ordered
	 */
	protected static final OperationType SOURCE_MODEL_OPERATION_TYPE_EDEFAULT = OperationType.CREATED;

	/**
	 * The cached value of the '{@link #getSourceModelOperationType() <em>Source Model Operation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceModelOperationType()
	 * @generated
	 * @ordered
	 */
	protected OperationType sourceModelOperationType = SOURCE_MODEL_OPERATION_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPreviousRelation() <em>Previous Relation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreviousRelation()
	 * @generated
	 * @ordered
	 */
	protected Relation previousRelation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecutionContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.EXECUTION_CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isManualLaunch() {
		return manualLaunch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setManualLaunch(boolean newManualLaunch) {
		boolean oldManualLaunch = manualLaunch;
		manualLaunch = newManualLaunch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.EXECUTION_CONTEXT__MANUAL_LAUNCH, oldManualLaunch, manualLaunch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationType getSourceModelOperationType() {
		return sourceModelOperationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceModelOperationType(OperationType newSourceModelOperationType) {
		OperationType oldSourceModelOperationType = sourceModelOperationType;
		sourceModelOperationType = newSourceModelOperationType == null ? SOURCE_MODEL_OPERATION_TYPE_EDEFAULT : newSourceModelOperationType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.EXECUTION_CONTEXT__SOURCE_MODEL_OPERATION_TYPE, oldSourceModelOperationType, sourceModelOperationType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Relation getPreviousRelation() {
		if (previousRelation != null && previousRelation.eIsProxy()) {
			InternalEObject oldPreviousRelation = (InternalEObject)previousRelation;
			previousRelation = (Relation)eResolveProxy(oldPreviousRelation);
			if (previousRelation != oldPreviousRelation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MegamodelPackage.EXECUTION_CONTEXT__PREVIOUS_RELATION, oldPreviousRelation, previousRelation));
			}
		}
		return previousRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Relation basicGetPreviousRelation() {
		return previousRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreviousRelation(Relation newPreviousRelation) {
		Relation oldPreviousRelation = previousRelation;
		previousRelation = newPreviousRelation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.EXECUTION_CONTEXT__PREVIOUS_RELATION, oldPreviousRelation, previousRelation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.EXECUTION_CONTEXT__MANUAL_LAUNCH:
				return isManualLaunch();
			case MegamodelPackage.EXECUTION_CONTEXT__SOURCE_MODEL_OPERATION_TYPE:
				return getSourceModelOperationType();
			case MegamodelPackage.EXECUTION_CONTEXT__PREVIOUS_RELATION:
				if (resolve) return getPreviousRelation();
				return basicGetPreviousRelation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.EXECUTION_CONTEXT__MANUAL_LAUNCH:
				setManualLaunch((Boolean)newValue);
				return;
			case MegamodelPackage.EXECUTION_CONTEXT__SOURCE_MODEL_OPERATION_TYPE:
				setSourceModelOperationType((OperationType)newValue);
				return;
			case MegamodelPackage.EXECUTION_CONTEXT__PREVIOUS_RELATION:
				setPreviousRelation((Relation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.EXECUTION_CONTEXT__MANUAL_LAUNCH:
				setManualLaunch(MANUAL_LAUNCH_EDEFAULT);
				return;
			case MegamodelPackage.EXECUTION_CONTEXT__SOURCE_MODEL_OPERATION_TYPE:
				setSourceModelOperationType(SOURCE_MODEL_OPERATION_TYPE_EDEFAULT);
				return;
			case MegamodelPackage.EXECUTION_CONTEXT__PREVIOUS_RELATION:
				setPreviousRelation((Relation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.EXECUTION_CONTEXT__MANUAL_LAUNCH:
				return manualLaunch != MANUAL_LAUNCH_EDEFAULT;
			case MegamodelPackage.EXECUTION_CONTEXT__SOURCE_MODEL_OPERATION_TYPE:
				return sourceModelOperationType != SOURCE_MODEL_OPERATION_TYPE_EDEFAULT;
			case MegamodelPackage.EXECUTION_CONTEXT__PREVIOUS_RELATION:
				return previousRelation != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (manualLaunch: ");
		result.append(manualLaunch);
		result.append(", sourceModelOperationType: ");
		result.append(sourceModelOperationType);
		result.append(')');
		return result.toString();
	}

} //ExecutionContextImpl
