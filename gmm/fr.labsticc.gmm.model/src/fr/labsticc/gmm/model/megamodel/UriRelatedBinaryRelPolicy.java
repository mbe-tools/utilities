/**
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.common.util.URI;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uri Related Binary Rel Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The URIs are related if thy are equals besides their file extension.
 * <!-- end-model-doc -->
 *
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getUriRelatedBinaryRelPolicy()
 * @model
 * @generated
 */
public interface UriRelatedBinaryRelPolicy extends BinaryRelationPolicy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The URIs are related if thy are equals besides their file extension.
	 * <!-- end-model-doc -->
	 * @model leftUriDataType="fr.labsticc.gmm.model.megamodel.URI" leftUriRequired="true" rightUriDataType="fr.labsticc.gmm.model.megamodel.URI" rightUriRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ( leftUri == null ||  rightUri == null ) {\r\n\treturn false;\r\n}\r\n\r\nreturn leftUri.trimFileExtension().equals( rightUri.trimFileExtension() );'"
	 * @generated
	 */
	boolean areRelated(URI leftUri, URI rightUri);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Calculate the corresponding URI.
	 * <!-- end-model-doc -->
	 * @model dataType="fr.labsticc.gmm.model.megamodel.URI" required="true" uriDataType="fr.labsticc.gmm.model.megamodel.URI" uriRequired="true"
	 * @generated
	 */
	URI correspondingUri(URI uri);

} // UriRelatedBinaryRelPolicy
