/**
 */
package fr.labsticc.gmm.model.megamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Modeling Environment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ModelingEnvironment#getSourceModel <em>Source Model</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ModelingEnvironment#getRelation <em>Relation</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModelingEnvironment()
 * @model
 * @generated
 */
public interface ModelingEnvironment extends Model {

	/**
	 * Returns the value of the '<em><b>Source Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Model</em>' reference.
	 * @see #setSourceModel(Model)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModelingEnvironment_SourceModel()
	 * @model required="true"
	 * @generated
	 */
	Model getSourceModel();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ModelingEnvironment#getSourceModel <em>Source Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Model</em>' reference.
	 * @see #getSourceModel()
	 * @generated
	 */
	void setSourceModel(Model value);

	/**
	 * Returns the value of the '<em><b>Relation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relation</em>' reference.
	 * @see #setRelation(Relation)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModelingEnvironment_Relation()
	 * @model required="true"
	 * @generated
	 */
	Relation getRelation();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ModelingEnvironment#getRelation <em>Relation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relation</em>' reference.
	 * @see #getRelation()
	 * @generated
	 */
	void setRelation(Relation value);

} // ModelingEnvironment
