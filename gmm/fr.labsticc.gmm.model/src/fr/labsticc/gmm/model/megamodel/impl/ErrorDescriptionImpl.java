/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.labsticc.gmm.model.megamodel.ErrorDescription;
import fr.labsticc.gmm.model.megamodel.ErrorType;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.Model;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Error Description</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ErrorDescriptionImpl#getErrorStatus <em>Error Status</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ErrorDescriptionImpl#getErroneousElements <em>Erroneous Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ErrorDescriptionImpl extends NamedElementImpl implements ErrorDescription {
	/**
	 * The default value of the '{@link #getErrorStatus() <em>Error Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorStatus()
	 * @generated
	 * @ordered
	 */
	protected static final ErrorType ERROR_STATUS_EDEFAULT = ErrorType.GMM;

	/**
	 * The cached value of the '{@link #getErrorStatus() <em>Error Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorStatus()
	 * @generated
	 * @ordered
	 */
	protected ErrorType errorStatus = ERROR_STATUS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getErroneousElements() <em>Erroneous Elements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErroneousElements()
	 * @generated
	 * @ordered
	 */
	protected Map<Model, EList<EObject>> erroneousElements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ErrorDescriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.ERROR_DESCRIPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ErrorType getErrorStatus() {
		return errorStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setErrorStatus(ErrorType newErrorStatus) {
		ErrorType oldErrorStatus = errorStatus;
		errorStatus = newErrorStatus == null ? ERROR_STATUS_EDEFAULT : newErrorStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.ERROR_DESCRIPTION__ERROR_STATUS, oldErrorStatus, errorStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<Model, EList<EObject>> getErroneousElements() {
		return erroneousElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setErroneousElements(Map<Model, EList<EObject>> newErroneousElements) {
		Map<Model, EList<EObject>> oldErroneousElements = erroneousElements;
		erroneousElements = newErroneousElements;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.ERROR_DESCRIPTION__ERRONEOUS_ELEMENTS, oldErroneousElements, erroneousElements));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.ERROR_DESCRIPTION__ERROR_STATUS:
				return getErrorStatus();
			case MegamodelPackage.ERROR_DESCRIPTION__ERRONEOUS_ELEMENTS:
				return getErroneousElements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.ERROR_DESCRIPTION__ERROR_STATUS:
				setErrorStatus((ErrorType)newValue);
				return;
			case MegamodelPackage.ERROR_DESCRIPTION__ERRONEOUS_ELEMENTS:
				setErroneousElements((Map<Model, EList<EObject>>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.ERROR_DESCRIPTION__ERROR_STATUS:
				setErrorStatus(ERROR_STATUS_EDEFAULT);
				return;
			case MegamodelPackage.ERROR_DESCRIPTION__ERRONEOUS_ELEMENTS:
				setErroneousElements((Map<Model, EList<EObject>>)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.ERROR_DESCRIPTION__ERROR_STATUS:
				return errorStatus != ERROR_STATUS_EDEFAULT;
			case MegamodelPackage.ERROR_DESCRIPTION__ERRONEOUS_ELEMENTS:
				return erroneousElements != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (errorStatus: ");
		result.append(errorStatus);
		result.append(", erroneousElements: ");
		result.append(erroneousElements);
		result.append(')');
		return result.toString();
	}

} //ErrorDescriptionImpl
