/**
 */
package fr.labsticc.gmm.model.megamodel;



/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Relation Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getBinaryRelationPolicy()
 * @model abstract="true"
 * @generated
 */
public interface BinaryRelationPolicy extends RelationPolicy {

} // BinaryRelationPolicy
