/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.MetaModelSubset;
import fr.labsticc.gmm.model.megamodel.SubsettedMetaModel;

import java.util.Collection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subsetted Meta Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.SubsettedMetaModelImpl#getSubset <em>Subset</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.SubsettedMetaModelImpl#getRootMetaModel <em>Root Meta Model</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.SubsettedMetaModelImpl#getSubsetExtensions <em>Subset Extensions</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SubsettedMetaModelImpl extends MetaModelImpl implements SubsettedMetaModel {

	/**
	 * The cached value of the '{@link #getSubset() <em>Subset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubset()
	 * @generated
	 * @ordered
	 */
	protected MetaModelSubset subset;

	/**
	 * The cached value of the '{@link #getSubsetExtensions() <em>Subset Extensions</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubsetExtensions()
	 * @generated
	 * @ordered
	 */
	protected EList<String> subsetExtensions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubsettedMetaModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.SUBSETTED_META_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModelSubset getSubset() {
		if (subset != null && subset.eIsProxy()) {
			InternalEObject oldSubset = (InternalEObject)subset;
			subset = (MetaModelSubset)eResolveProxy(oldSubset);
			if (subset != oldSubset) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MegamodelPackage.SUBSETTED_META_MODEL__SUBSET, oldSubset, subset));
			}
		}
		return subset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModelSubset basicGetSubset() {
		return subset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubset(MetaModelSubset newSubset) {
		MetaModelSubset oldSubset = subset;
		subset = newSubset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.SUBSETTED_META_MODEL__SUBSET, oldSubset, subset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel getRootMetaModel() {
		MetaModel rootMetaModel = basicGetRootMetaModel();
		return rootMetaModel != null && rootMetaModel.eIsProxy() ? (MetaModel)eResolveProxy((InternalEObject)rootMetaModel) : rootMetaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public MetaModel basicGetRootMetaModel() {
		return (MetaModel) eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getSubsetExtensions() {
		if (subsetExtensions == null) {
			subsetExtensions = new EDataTypeUniqueEList<String>(String.class, this, MegamodelPackage.SUBSETTED_META_MODEL__SUBSET_EXTENSIONS);
		}
		return subsetExtensions;
	}

	@Override
	public boolean declares( final URI p_uri ) {
		final MetaModel metaModel = getRootMetaModel();
		
		if ( metaModel.declares( p_uri ) ) {
			final String uriSubsetExt = URI.createPlatformResourceURI( p_uri.trimFileExtension().lastSegment(), true ).fileExtension();
			
			if ( uriSubsetExt != null ) {
				for ( final String subsetExt : getSubsetExtensions() ) {
					if ( uriSubsetExt.equals( subsetExt ) ) {
						return true;
					}
				}
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.SUBSETTED_META_MODEL__SUBSET:
				if (resolve) return getSubset();
				return basicGetSubset();
			case MegamodelPackage.SUBSETTED_META_MODEL__ROOT_META_MODEL:
				if (resolve) return getRootMetaModel();
				return basicGetRootMetaModel();
			case MegamodelPackage.SUBSETTED_META_MODEL__SUBSET_EXTENSIONS:
				return getSubsetExtensions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.SUBSETTED_META_MODEL__SUBSET:
				setSubset((MetaModelSubset)newValue);
				return;
			case MegamodelPackage.SUBSETTED_META_MODEL__SUBSET_EXTENSIONS:
				getSubsetExtensions().clear();
				getSubsetExtensions().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.SUBSETTED_META_MODEL__SUBSET:
				setSubset((MetaModelSubset)null);
				return;
			case MegamodelPackage.SUBSETTED_META_MODEL__SUBSET_EXTENSIONS:
				getSubsetExtensions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.SUBSETTED_META_MODEL__SUBSET:
				return subset != null;
			case MegamodelPackage.SUBSETTED_META_MODEL__ROOT_META_MODEL:
				return basicGetRootMetaModel() != null;
			case MegamodelPackage.SUBSETTED_META_MODEL__SUBSET_EXTENSIONS:
				return subsetExtensions != null && !subsetExtensions.isEmpty();
		}
		return super.eIsSet(featureID);
	}
/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (subsetExtensions: ");
		result.append(subsetExtensions);
		result.append(')');
		return result.toString();
	}
} //SubsettedMetaModelImpl
