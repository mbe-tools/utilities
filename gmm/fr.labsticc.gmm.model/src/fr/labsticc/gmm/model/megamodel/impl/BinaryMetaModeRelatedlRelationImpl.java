/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModel;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Meta Mode Relatedl Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModeRelatedlRelationImpl#getLeftMetaModel <em>Left Meta Model</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModeRelatedlRelationImpl#getRightMetaModel <em>Right Meta Model</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class BinaryMetaModeRelatedlRelationImpl extends MetaModelRelatedRelationImpl implements BinaryMetaModeRelatedlRelation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryMetaModeRelatedlRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.BINARY_META_MODE_RELATEDL_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel getLeftMetaModel() {
		MetaModel leftMetaModel = basicGetLeftMetaModel();
		return leftMetaModel != null && leftMetaModel.eIsProxy() ? (MetaModel)eResolveProxy((InternalEObject)leftMetaModel) : leftMetaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel basicGetLeftMetaModel() {
		return (MetaModel)getMetaModels().get(MegamodelPackage.Literals.BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeftMetaModel(MetaModel newLeftMetaModel) {
		((FeatureMap.Internal)getMetaModels()).set(MegamodelPackage.Literals.BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL, newLeftMetaModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel getRightMetaModel() {
		MetaModel rightMetaModel = basicGetRightMetaModel();
		return rightMetaModel != null && rightMetaModel.eIsProxy() ? (MetaModel)eResolveProxy((InternalEObject)rightMetaModel) : rightMetaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel basicGetRightMetaModel() {
		return (MetaModel)getMetaModels().get(MegamodelPackage.Literals.BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightMetaModel(MetaModel newRightMetaModel) {
		((FeatureMap.Internal)getMetaModels()).set(MegamodelPackage.Literals.BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL, newRightMetaModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL:
				if (resolve) return getLeftMetaModel();
				return basicGetLeftMetaModel();
			case MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL:
				if (resolve) return getRightMetaModel();
				return basicGetRightMetaModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL:
				setLeftMetaModel((MetaModel)newValue);
				return;
			case MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL:
				setRightMetaModel((MetaModel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL:
				setLeftMetaModel((MetaModel)null);
				return;
			case MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL:
				setRightMetaModel((MetaModel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL:
				return basicGetLeftMetaModel() != null;
			case MegamodelPackage.BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL:
				return basicGetRightMetaModel() != null;
		}
		return super.eIsSet(featureID);
	}

} //BinaryMetaModeRelatedlRelationImpl
