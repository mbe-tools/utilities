/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel.impl;

import java.util.Iterator;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Meta Model Related Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelRelatedRelationImpl#getMetaModels <em>Meta Models</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class MetaModelRelatedRelationImpl extends FactualRelationImpl implements MetaModelRelatedRelation {
	/**
	 * The cached value of the '{@link #getMetaModels() <em>Meta Models</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetaModels()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap metaModels;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetaModelRelatedRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.META_MODEL_RELATED_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getMetaModels() {
		if (metaModels == null) {
			metaModels = new BasicFeatureMap(this, MegamodelPackage.META_MODEL_RELATED_RELATION__META_MODELS);
		}
		return metaModels;
	}
	
	@Override
	public boolean concerns(final URI resourceUri) {
		if ( resourceUri == null ) {
			return false;
		}
		
		for ( final MetaModel metaModel : getMetaModelsList() ) {
			if ( metaModel.declares( resourceUri ) ) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public boolean concerns(EObject modelElement) {
		if ( modelElement == null ) {
			return false;
		}
		
		for ( final MetaModel metaModel : getMetaModelsList() ) {
			if ( metaModel.declares( modelElement ) ) {
				return true;
			}
		}
		
		return false;
	}
	
	public EList<MetaModel> getMetaModelsList() {
		final EList<MetaModel> models = new BasicEList<MetaModel>();
		final Iterator<Object> modelsIt = getMetaModels().valueListIterator();
		
		while ( modelsIt.hasNext() ) {
			models.add( (MetaModel) modelsIt.next() );
		}
		
		return models;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL_RELATED_RELATION__META_MODELS:
				return ((InternalEList<?>)getMetaModels()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL_RELATED_RELATION__META_MODELS:
				if (coreType) return getMetaModels();
				return ((FeatureMap.Internal)getMetaModels()).getWrapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL_RELATED_RELATION__META_MODELS:
				((FeatureMap.Internal)getMetaModels()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL_RELATED_RELATION__META_MODELS:
				getMetaModels().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.META_MODEL_RELATED_RELATION__META_MODELS:
				return metaModels != null && !metaModels.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (metaModels: ");
		result.append(metaModels);
		result.append(')');
		return result.toString();
	}

} //MetaModelRelatedRelationImpl
