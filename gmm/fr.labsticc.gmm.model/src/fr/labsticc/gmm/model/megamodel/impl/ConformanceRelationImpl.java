/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel.impl;


import fr.labsticc.gmm.model.megamodel.ConformanceRelation;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.Model;

import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conformance Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ConformanceRelationImpl#getModels <em>Models</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ConformanceRelationImpl#getMetaModel <em>Meta Model</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConformanceRelationImpl extends FactualRelationImpl implements ConformanceRelation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConformanceRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.CONFORMANCE_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Model> getModels() {
		return getConnectedModels().list(MegamodelPackage.Literals.CONFORMANCE_RELATION__MODELS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel getMetaModel() {
		MetaModel metaModel = basicGetMetaModel();
		return metaModel != null && metaModel.eIsProxy() ? (MetaModel)eResolveProxy((InternalEObject)metaModel) : metaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel basicGetMetaModel() {
		return (MetaModel)getConnectedModels().get(MegamodelPackage.Literals.CONFORMANCE_RELATION__META_MODEL, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetaModel(MetaModel newMetaModel) {
		((FeatureMap.Internal)getConnectedModels()).set(MegamodelPackage.Literals.CONFORMANCE_RELATION__META_MODEL, newMetaModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.CONFORMANCE_RELATION__MODELS:
				return getModels();
			case MegamodelPackage.CONFORMANCE_RELATION__META_MODEL:
				if (resolve) return getMetaModel();
				return basicGetMetaModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.CONFORMANCE_RELATION__MODELS:
				getModels().clear();
				getModels().addAll((Collection<? extends Model>)newValue);
				return;
			case MegamodelPackage.CONFORMANCE_RELATION__META_MODEL:
				setMetaModel((MetaModel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.CONFORMANCE_RELATION__MODELS:
				getModels().clear();
				return;
			case MegamodelPackage.CONFORMANCE_RELATION__META_MODEL:
				setMetaModel((MetaModel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.CONFORMANCE_RELATION__MODELS:
				return !getModels().isEmpty();
			case MegamodelPackage.CONFORMANCE_RELATION__META_MODEL:
				return basicGetMetaModel() != null;
		}
		return super.eIsSet(featureID);
	}

} //ConformanceRelationImpl
