/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Model#getOwnedRelations <em>Owned Relations</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Model#getOwnedModels <em>Owned Models</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Model#getParentModel <em>Parent Model</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Model#getOwnedObligationRelations <em>Owned Obligation Relations</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Model#getFileExtensions <em>File Extensions</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Model#getResources <em>Resources</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Model#getOwnedFactualRelations <em>Owned Factual Relations</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Model#getError <em>Error</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModel()
 * @model
 * @generated
 */
public interface Model extends NamedElement {
	/**
	 * Returns the value of the '<em><b>File Extensions</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>File Extensions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>File Extensions</em>' attribute list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModel_FileExtensions()
	 * @model
	 * @generated
	 */
	EList<String> getFileExtensions();

	/**
	 * Returns the value of the '<em><b>Resources</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.resource.Resource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resources</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resources</em>' attribute list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModel_Resources()
	 * @model transient="true"
	 * @generated
	 */
	EList<Resource> getResources();

	/**
	 * Returns the value of the '<em><b>Owned Factual Relations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.gmm.model.megamodel.FactualRelation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Structural Relations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Factual Relations</em>' containment reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModel_OwnedFactualRelations()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#ownedRelations'"
	 * @generated
	 */
	EList<FactualRelation> getOwnedFactualRelations();

	/**
	 * Returns the value of the '<em><b>Error</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Error</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Error</em>' containment reference.
	 * @see #setError(ErrorDescription)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModel_Error()
	 * @model containment="true"
	 * @generated
	 */
	ErrorDescription getError();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.Model#getError <em>Error</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Error</em>' containment reference.
	 * @see #getError()
	 * @generated
	 */
	void setError(ErrorDescription value);

	/**
	 * Returns the value of the '<em><b>Owned Relations</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Relations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Relations</em>' attribute list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModel_OwnedRelations()
	 * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group'"
	 * @generated
	 */
	FeatureMap getOwnedRelations();

	/**
	 * Returns the value of the '<em><b>Owned Models</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.gmm.model.megamodel.Model}.
	 * It is bidirectional and its opposite is '{@link fr.labsticc.gmm.model.megamodel.Model#getParentModel <em>Parent Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Models</em>' containment reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModel_OwnedModels()
	 * @see fr.labsticc.gmm.model.megamodel.Model#getParentModel
	 * @model opposite="parentModel" containment="true"
	 * @generated
	 */
	EList<Model> getOwnedModels();

	/**
	 * Returns the value of the '<em><b>Parent Model</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.labsticc.gmm.model.megamodel.Model#getOwnedModels <em>Owned Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Model</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Model</em>' container reference.
	 * @see #setParentModel(Model)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModel_ParentModel()
	 * @see fr.labsticc.gmm.model.megamodel.Model#getOwnedModels
	 * @model opposite="ownedModels" transient="false"
	 * @generated
	 */
	Model getParentModel();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.Model#getParentModel <em>Parent Model</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Model</em>' container reference.
	 * @see #getParentModel()
	 * @generated
	 */
	void setParentModel(Model value);

	/**
	 * Returns the value of the '<em><b>Owned Obligation Relations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.gmm.model.megamodel.ObligationRelation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Operational Relations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Obligation Relations</em>' containment reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModel_OwnedObligationRelations()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#ownedRelations'"
	 * @generated
	 */
	EList<ObligationRelation> getOwnedObligationRelations();

} // Model
