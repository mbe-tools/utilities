/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import fr.labsticc.gmm.model.megamodel.ExecutionContext;
import fr.labsticc.gmm.model.megamodel.GmmEngine;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;

import fr.labsticc.gmm.model.megamodel.Model;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Gmm Engine</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.GmmEngineImpl#getMegaModel <em>Mega Model</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GmmEngineImpl extends NamedElementImpl implements GmmEngine {
	/**
	 * The cached value of the '{@link #getMegaModel() <em>Mega Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMegaModel()
	 * @generated
	 * @ordered
	 */
	protected Model megaModel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GmmEngineImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.GMM_ENGINE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model getMegaModel() {
		if (megaModel != null && megaModel.eIsProxy()) {
			InternalEObject oldMegaModel = (InternalEObject)megaModel;
			megaModel = (Model)eResolveProxy(oldMegaModel);
			if (megaModel != oldMegaModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MegamodelPackage.GMM_ENGINE__MEGA_MODEL, oldMegaModel, megaModel));
			}
		}
		return megaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model basicGetMegaModel() {
		return megaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMegaModel(Model newMegaModel) {
		Model oldMegaModel = megaModel;
		megaModel = newMegaModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.GMM_ENGINE__MEGA_MODEL, oldMegaModel, megaModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void establishValidity(URI changedUri, ExecutionContext executionContext) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.GMM_ENGINE__MEGA_MODEL:
				if (resolve) return getMegaModel();
				return basicGetMegaModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.GMM_ENGINE__MEGA_MODEL:
				setMegaModel((Model)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.GMM_ENGINE__MEGA_MODEL:
				setMegaModel((Model)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.GMM_ENGINE__MEGA_MODEL:
				return megaModel != null;
		}
		return super.eIsSet(featureID);
	}

} //GmmEngineImpl
