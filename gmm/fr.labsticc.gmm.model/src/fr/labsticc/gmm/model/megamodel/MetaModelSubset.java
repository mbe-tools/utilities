/**
 */
package fr.labsticc.gmm.model.megamodel;

import fr.labsticc.framework.constraints.model.constraints.CardinalityConstraint;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Meta Model Subset</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.MetaModelSubset#getOwnedConstraints <em>Owned Constraints</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.MetaModelSubset#getActivities <em>Activities</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.MetaModelSubset#getOwnedCompositionOperations <em>Owned Composition Operations</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getMetaModelSubset()
 * @model
 * @generated
 */
public interface MetaModelSubset extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Owned Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.framework.constraints.model.constraints.CardinalityConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Constraints</em>' containment reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getMetaModelSubset_OwnedConstraints()
	 * @model containment="true"
	 * @generated
	 */
	EList<CardinalityConstraint> getOwnedConstraints();

	/**
	 * Returns the value of the '<em><b>Activities</b></em>' reference list.
	 * The list contents are of type {@link fr.labsticc.gmm.model.megamodel.Activity}.
	 * It is bidirectional and its opposite is '{@link fr.labsticc.gmm.model.megamodel.Activity#getRequiredSubsets <em>Required Subsets</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activities</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activities</em>' reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getMetaModelSubset_Activities()
	 * @see fr.labsticc.gmm.model.megamodel.Activity#getRequiredSubsets
	 * @model opposite="requiredSubsets" required="true"
	 * @generated
	 */
	EList<Activity> getActivities();

	/**
	 * Returns the value of the '<em><b>Owned Composition Operations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.gmm.model.megamodel.SetOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Composition Operations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Composition Operations</em>' containment reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getMetaModelSubset_OwnedCompositionOperations()
	 * @model containment="true"
	 * @generated
	 */
	EList<SetOperation> getOwnedCompositionOperations();

} // MetaModelSubset
