/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModelSubset;
import fr.labsticc.gmm.model.megamodel.SubsetRelationship;
import fr.labsticc.gmm.model.megamodel.SubsetRelationshipType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subset Relationship</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.SubsetRelationshipImpl#getType <em>Type</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.SubsetRelationshipImpl#getTargetSubset <em>Target Subset</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SubsetRelationshipImpl extends NamedElementImpl implements SubsetRelationship {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final SubsetRelationshipType TYPE_EDEFAULT = SubsetRelationshipType.INCLUSION;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected SubsetRelationshipType type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTargetSubset() <em>Target Subset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSubset()
	 * @generated
	 * @ordered
	 */
	protected MetaModelSubset targetSubset;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubsetRelationshipImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.SUBSET_RELATIONSHIP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubsetRelationshipType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(SubsetRelationshipType newType) {
		SubsetRelationshipType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.SUBSET_RELATIONSHIP__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModelSubset getTargetSubset() {
		if (targetSubset != null && targetSubset.eIsProxy()) {
			InternalEObject oldTargetSubset = (InternalEObject)targetSubset;
			targetSubset = (MetaModelSubset)eResolveProxy(oldTargetSubset);
			if (targetSubset != oldTargetSubset) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MegamodelPackage.SUBSET_RELATIONSHIP__TARGET_SUBSET, oldTargetSubset, targetSubset));
			}
		}
		return targetSubset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModelSubset basicGetTargetSubset() {
		return targetSubset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetSubset(MetaModelSubset newTargetSubset) {
		MetaModelSubset oldTargetSubset = targetSubset;
		targetSubset = newTargetSubset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.SUBSET_RELATIONSHIP__TARGET_SUBSET, oldTargetSubset, targetSubset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.SUBSET_RELATIONSHIP__TYPE:
				return getType();
			case MegamodelPackage.SUBSET_RELATIONSHIP__TARGET_SUBSET:
				if (resolve) return getTargetSubset();
				return basicGetTargetSubset();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.SUBSET_RELATIONSHIP__TYPE:
				setType((SubsetRelationshipType)newValue);
				return;
			case MegamodelPackage.SUBSET_RELATIONSHIP__TARGET_SUBSET:
				setTargetSubset((MetaModelSubset)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.SUBSET_RELATIONSHIP__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case MegamodelPackage.SUBSET_RELATIONSHIP__TARGET_SUBSET:
				setTargetSubset((MetaModelSubset)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.SUBSET_RELATIONSHIP__TYPE:
				return type != TYPE_EDEFAULT;
			case MegamodelPackage.SUBSET_RELATIONSHIP__TARGET_SUBSET:
				return targetSubset != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //SubsetRelationshipImpl
