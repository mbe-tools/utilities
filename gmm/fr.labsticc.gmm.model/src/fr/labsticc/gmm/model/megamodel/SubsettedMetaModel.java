/**
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subsetted Meta Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.SubsettedMetaModel#getSubset <em>Subset</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.SubsettedMetaModel#getRootMetaModel <em>Root Meta Model</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.SubsettedMetaModel#getSubsetExtensions <em>Subset Extensions</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getSubsettedMetaModel()
 * @model
 * @generated
 */
public interface SubsettedMetaModel extends MetaModel {
	/**
	 * Returns the value of the '<em><b>Subset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subset</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subset</em>' reference.
	 * @see #setSubset(MetaModelSubset)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getSubsettedMetaModel_Subset()
	 * @model required="true"
	 * @generated
	 */
	MetaModelSubset getSubset();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.SubsettedMetaModel#getSubset <em>Subset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subset</em>' reference.
	 * @see #getSubset()
	 * @generated
	 */
	void setSubset(MetaModelSubset value);

	/**
	 * Returns the value of the '<em><b>Root Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Meta Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Meta Model</em>' reference.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getSubsettedMetaModel_RootMetaModel()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	MetaModel getRootMetaModel();

	/**
	 * Returns the value of the '<em><b>Subset Extensions</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subset Extensions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subset Extensions</em>' attribute list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getSubsettedMetaModel_SubsetExtensions()
	 * @model required="true"
	 * @generated
	 */
	EList<String> getSubsetExtensions();

} // SubsettedMetaModel
