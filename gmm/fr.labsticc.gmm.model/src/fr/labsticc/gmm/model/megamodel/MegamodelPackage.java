/**
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.labsticc.gmm.model.megamodel.MegamodelFactory
 * @model kind="package"
 * @generated
 */
public interface MegamodelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "megamodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.labsticc.fr/megamodel/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "megamodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MegamodelPackage eINSTANCE = fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.NamedElementImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__DISPLAY_NAME = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__ID = 3;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.ModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.ModelImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getModel()
	 * @generated
	 */
	int MODEL = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__DISPLAY_NAME = NAMED_ELEMENT__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Owned Relations</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__OWNED_RELATIONS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Owned Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__OWNED_MODELS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parent Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__PARENT_MODEL = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Owned Obligation Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__OWNED_OBLIGATION_RELATIONS = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>File Extensions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__FILE_EXTENSIONS = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__RESOURCES = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Owned Factual Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__OWNED_FACTUAL_RELATIONS = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Error</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__ERROR = NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.GmmSpecificationImpl <em>Gmm Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.GmmSpecificationImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getGmmSpecification()
	 * @generated
	 */
	int GMM_SPECIFICATION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__NAME = MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__DESCRIPTION = MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__DISPLAY_NAME = MODEL__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__ID = MODEL__ID;

	/**
	 * The feature id for the '<em><b>Owned Relations</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__OWNED_RELATIONS = MODEL__OWNED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__OWNED_MODELS = MODEL__OWNED_MODELS;

	/**
	 * The feature id for the '<em><b>Parent Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__PARENT_MODEL = MODEL__PARENT_MODEL;

	/**
	 * The feature id for the '<em><b>Owned Obligation Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__OWNED_OBLIGATION_RELATIONS = MODEL__OWNED_OBLIGATION_RELATIONS;

	/**
	 * The feature id for the '<em><b>File Extensions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__FILE_EXTENSIONS = MODEL__FILE_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__RESOURCES = MODEL__RESOURCES;

	/**
	 * The feature id for the '<em><b>Owned Factual Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__OWNED_FACTUAL_RELATIONS = MODEL__OWNED_FACTUAL_RELATIONS;

	/**
	 * The feature id for the '<em><b>Error</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__ERROR = MODEL__ERROR;

	/**
	 * The feature id for the '<em><b>Owned Activities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION__OWNED_ACTIVITIES = MODEL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Gmm Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_SPECIFICATION_FEATURE_COUNT = MODEL_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.ErrorDescriptionImpl <em>Error Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.ErrorDescriptionImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getErrorDescription()
	 * @generated
	 */
	int ERROR_DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_DESCRIPTION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_DESCRIPTION__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_DESCRIPTION__DISPLAY_NAME = NAMED_ELEMENT__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_DESCRIPTION__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Error Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_DESCRIPTION__ERROR_STATUS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Erroneous Elements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_DESCRIPTION__ERRONEOUS_ELEMENTS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Error Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_DESCRIPTION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelImpl <em>Meta Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.MetaModelImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getMetaModel()
	 * @generated
	 */
	int META_MODEL = 5;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.ComparisonSettingsImpl <em>Comparison Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.ComparisonSettingsImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getComparisonSettings()
	 * @generated
	 */
	int COMPARISON_SETTINGS = 6;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.RelationImpl <em>Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.RelationImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getRelation()
	 * @generated
	 */
	int RELATION = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__DISPLAY_NAME = NAMED_ELEMENT__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Connected Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__CONNECTED_MODELS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Chained Relations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__CHAINED_RELATIONS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Owned Relation Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__OWNED_RELATION_POLICY = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Intention</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__INTENTION = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__NAME = MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__DESCRIPTION = MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__DISPLAY_NAME = MODEL__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__ID = MODEL__ID;

	/**
	 * The feature id for the '<em><b>Owned Relations</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__OWNED_RELATIONS = MODEL__OWNED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__OWNED_MODELS = MODEL__OWNED_MODELS;

	/**
	 * The feature id for the '<em><b>Parent Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__PARENT_MODEL = MODEL__PARENT_MODEL;

	/**
	 * The feature id for the '<em><b>Owned Obligation Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__OWNED_OBLIGATION_RELATIONS = MODEL__OWNED_OBLIGATION_RELATIONS;

	/**
	 * The feature id for the '<em><b>File Extensions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__FILE_EXTENSIONS = MODEL__FILE_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__RESOURCES = MODEL__RESOURCES;

	/**
	 * The feature id for the '<em><b>Owned Factual Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__OWNED_FACTUAL_RELATIONS = MODEL__OWNED_FACTUAL_RELATIONS;

	/**
	 * The feature id for the '<em><b>Error</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__ERROR = MODEL__ERROR;

	/**
	 * The feature id for the '<em><b>Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__PACKAGES = MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Owned Subsets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__OWNED_SUBSETS = MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Owned Comparison Settings</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__OWNED_COMPARISON_SETTINGS = MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Owned Constraint Targets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL__OWNED_CONSTRAINT_TARGETS = MODEL_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Meta Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_FEATURE_COUNT = MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Use Id For Compare</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARISON_SETTINGS__USE_ID_FOR_COMPARE = 0;

	/**
	 * The feature id for the '<em><b>Name Similarity Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARISON_SETTINGS__NAME_SIMILARITY_WEIGHT = 1;

	/**
	 * The feature id for the '<em><b>Position Similarity Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARISON_SETTINGS__POSITION_SIMILARITY_WEIGHT = 2;

	/**
	 * The number of structural features of the '<em>Comparison Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARISON_SETTINGS_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.FactualRelationImpl <em>Factual Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.FactualRelationImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getFactualRelation()
	 * @generated
	 */
	int FACTUAL_RELATION = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTUAL_RELATION__NAME = RELATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTUAL_RELATION__DESCRIPTION = RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTUAL_RELATION__DISPLAY_NAME = RELATION__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTUAL_RELATION__ID = RELATION__ID;

	/**
	 * The feature id for the '<em><b>Connected Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTUAL_RELATION__CONNECTED_MODELS = RELATION__CONNECTED_MODELS;

	/**
	 * The feature id for the '<em><b>Chained Relations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTUAL_RELATION__CHAINED_RELATIONS = RELATION__CHAINED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Relation Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTUAL_RELATION__OWNED_RELATION_POLICY = RELATION__OWNED_RELATION_POLICY;

	/**
	 * The feature id for the '<em><b>Intention</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTUAL_RELATION__INTENTION = RELATION__INTENTION;

	/**
	 * The number of structural features of the '<em>Factual Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACTUAL_RELATION_FEATURE_COUNT = RELATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.ObligationRelationImpl <em>Obligation Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.ObligationRelationImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getObligationRelation()
	 * @generated
	 */
	int OBLIGATION_RELATION = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION__NAME = RELATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION__DESCRIPTION = RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION__DISPLAY_NAME = RELATION__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION__ID = RELATION__ID;

	/**
	 * The feature id for the '<em><b>Connected Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION__CONNECTED_MODELS = RELATION__CONNECTED_MODELS;

	/**
	 * The feature id for the '<em><b>Chained Relations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION__CHAINED_RELATIONS = RELATION__CHAINED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Relation Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION__OWNED_RELATION_POLICY = RELATION__OWNED_RELATION_POLICY;

	/**
	 * The feature id for the '<em><b>Intention</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION__INTENTION = RELATION__INTENTION;

	/**
	 * The feature id for the '<em><b>Owned Coverage Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION__OWNED_COVERAGE_POLICY = RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Enablement Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION__ENABLEMENT_TYPE = RELATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Deleting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION__DELETING = RELATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Validity Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION__VALIDITY_STATUS = RELATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Obligation Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_RELATION_FEATURE_COUNT = RELATION_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.ConformanceRelationImpl <em>Conformance Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.ConformanceRelationImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getConformanceRelation()
	 * @generated
	 */
	int CONFORMANCE_RELATION = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMANCE_RELATION__NAME = FACTUAL_RELATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMANCE_RELATION__DESCRIPTION = FACTUAL_RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMANCE_RELATION__DISPLAY_NAME = FACTUAL_RELATION__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMANCE_RELATION__ID = FACTUAL_RELATION__ID;

	/**
	 * The feature id for the '<em><b>Connected Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMANCE_RELATION__CONNECTED_MODELS = FACTUAL_RELATION__CONNECTED_MODELS;

	/**
	 * The feature id for the '<em><b>Chained Relations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMANCE_RELATION__CHAINED_RELATIONS = FACTUAL_RELATION__CHAINED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Relation Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMANCE_RELATION__OWNED_RELATION_POLICY = FACTUAL_RELATION__OWNED_RELATION_POLICY;

	/**
	 * The feature id for the '<em><b>Intention</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMANCE_RELATION__INTENTION = FACTUAL_RELATION__INTENTION;

	/**
	 * The feature id for the '<em><b>Models</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMANCE_RELATION__MODELS = FACTUAL_RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMANCE_RELATION__META_MODEL = FACTUAL_RELATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Conformance Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMANCE_RELATION_FEATURE_COUNT = FACTUAL_RELATION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelRelatedRelationImpl <em>Meta Model Related Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.MetaModelRelatedRelationImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getMetaModelRelatedRelation()
	 * @generated
	 */
	int META_MODEL_RELATED_RELATION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_RELATED_RELATION__NAME = FACTUAL_RELATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_RELATED_RELATION__DESCRIPTION = FACTUAL_RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_RELATED_RELATION__DISPLAY_NAME = FACTUAL_RELATION__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_RELATED_RELATION__ID = FACTUAL_RELATION__ID;

	/**
	 * The feature id for the '<em><b>Connected Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_RELATED_RELATION__CONNECTED_MODELS = FACTUAL_RELATION__CONNECTED_MODELS;

	/**
	 * The feature id for the '<em><b>Chained Relations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_RELATED_RELATION__CHAINED_RELATIONS = FACTUAL_RELATION__CHAINED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Relation Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_RELATED_RELATION__OWNED_RELATION_POLICY = FACTUAL_RELATION__OWNED_RELATION_POLICY;

	/**
	 * The feature id for the '<em><b>Intention</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_RELATED_RELATION__INTENTION = FACTUAL_RELATION__INTENTION;

	/**
	 * The feature id for the '<em><b>Meta Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_RELATED_RELATION__META_MODELS = FACTUAL_RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Meta Model Related Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_RELATED_RELATION_FEATURE_COUNT = FACTUAL_RELATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModeRelatedlRelationImpl <em>Binary Meta Mode Relatedl Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModeRelatedlRelationImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getBinaryMetaModeRelatedlRelation()
	 * @generated
	 */
	int BINARY_META_MODE_RELATEDL_RELATION = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODE_RELATEDL_RELATION__NAME = META_MODEL_RELATED_RELATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODE_RELATEDL_RELATION__DESCRIPTION = META_MODEL_RELATED_RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODE_RELATEDL_RELATION__DISPLAY_NAME = META_MODEL_RELATED_RELATION__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODE_RELATEDL_RELATION__ID = META_MODEL_RELATED_RELATION__ID;

	/**
	 * The feature id for the '<em><b>Connected Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODE_RELATEDL_RELATION__CONNECTED_MODELS = META_MODEL_RELATED_RELATION__CONNECTED_MODELS;

	/**
	 * The feature id for the '<em><b>Chained Relations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODE_RELATEDL_RELATION__CHAINED_RELATIONS = META_MODEL_RELATED_RELATION__CHAINED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Relation Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODE_RELATEDL_RELATION__OWNED_RELATION_POLICY = META_MODEL_RELATED_RELATION__OWNED_RELATION_POLICY;

	/**
	 * The feature id for the '<em><b>Intention</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODE_RELATEDL_RELATION__INTENTION = META_MODEL_RELATED_RELATION__INTENTION;

	/**
	 * The feature id for the '<em><b>Meta Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODE_RELATEDL_RELATION__META_MODELS = META_MODEL_RELATED_RELATION__META_MODELS;

	/**
	 * The feature id for the '<em><b>Left Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL = META_MODEL_RELATED_RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL = META_MODEL_RELATED_RELATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary Meta Mode Relatedl Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODE_RELATEDL_RELATION_FEATURE_COUNT = META_MODEL_RELATED_RELATION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModelRelatedConsistencyRelationImpl <em>Binary Meta Model Related Consistency Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModelRelatedConsistencyRelationImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getBinaryMetaModelRelatedConsistencyRelation()
	 * @generated
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__NAME = OBLIGATION_RELATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DESCRIPTION = OBLIGATION_RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DISPLAY_NAME = OBLIGATION_RELATION__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__ID = OBLIGATION_RELATION__ID;

	/**
	 * The feature id for the '<em><b>Connected Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__CONNECTED_MODELS = OBLIGATION_RELATION__CONNECTED_MODELS;

	/**
	 * The feature id for the '<em><b>Chained Relations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__CHAINED_RELATIONS = OBLIGATION_RELATION__CHAINED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Relation Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__OWNED_RELATION_POLICY = OBLIGATION_RELATION__OWNED_RELATION_POLICY;

	/**
	 * The feature id for the '<em><b>Intention</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__INTENTION = OBLIGATION_RELATION__INTENTION;

	/**
	 * The feature id for the '<em><b>Owned Coverage Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__OWNED_COVERAGE_POLICY = OBLIGATION_RELATION__OWNED_COVERAGE_POLICY;

	/**
	 * The feature id for the '<em><b>Enablement Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__ENABLEMENT_TYPE = OBLIGATION_RELATION__ENABLEMENT_TYPE;

	/**
	 * The feature id for the '<em><b>Deleting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING = OBLIGATION_RELATION__DELETING;

	/**
	 * The feature id for the '<em><b>Validity Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__VALIDITY_STATUS = OBLIGATION_RELATION__VALIDITY_STATUS;

	/**
	 * The feature id for the '<em><b>Meta Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__META_MODELS = OBLIGATION_RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Left Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__LEFT_META_MODEL = OBLIGATION_RELATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__RIGHT_META_MODEL = OBLIGATION_RELATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Deleting Left</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_LEFT = OBLIGATION_RELATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Deleting Right</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_RIGHT = OBLIGATION_RELATION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Binary Meta Model Related Consistency Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION_FEATURE_COUNT = OBLIGATION_RELATION_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.RelationPolicyImpl <em>Relation Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.RelationPolicyImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getRelationPolicy()
	 * @generated
	 */
	int RELATION_POLICY = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_POLICY__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_POLICY__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_POLICY__DISPLAY_NAME = NAMED_ELEMENT__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_POLICY__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_POLICY__RELATION = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Relation Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_POLICY_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.BinaryRelationPolicyImpl <em>Binary Relation Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.BinaryRelationPolicyImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getBinaryRelationPolicy()
	 * @generated
	 */
	int BINARY_RELATION_POLICY = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_RELATION_POLICY__NAME = RELATION_POLICY__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_RELATION_POLICY__DESCRIPTION = RELATION_POLICY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_RELATION_POLICY__DISPLAY_NAME = RELATION_POLICY__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_RELATION_POLICY__ID = RELATION_POLICY__ID;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_RELATION_POLICY__RELATION = RELATION_POLICY__RELATION;

	/**
	 * The number of structural features of the '<em>Binary Relation Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_RELATION_POLICY_FEATURE_COUNT = RELATION_POLICY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.UriRelatedBinaryRelPolicyImpl <em>Uri Related Binary Rel Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.UriRelatedBinaryRelPolicyImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getUriRelatedBinaryRelPolicy()
	 * @generated
	 */
	int URI_RELATED_BINARY_REL_POLICY = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URI_RELATED_BINARY_REL_POLICY__NAME = BINARY_RELATION_POLICY__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URI_RELATED_BINARY_REL_POLICY__DESCRIPTION = BINARY_RELATION_POLICY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URI_RELATED_BINARY_REL_POLICY__DISPLAY_NAME = BINARY_RELATION_POLICY__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URI_RELATED_BINARY_REL_POLICY__ID = BINARY_RELATION_POLICY__ID;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URI_RELATED_BINARY_REL_POLICY__RELATION = BINARY_RELATION_POLICY__RELATION;

	/**
	 * The number of structural features of the '<em>Uri Related Binary Rel Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URI_RELATED_BINARY_REL_POLICY_FEATURE_COUNT = BINARY_RELATION_POLICY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.ModelingEnvironmentImpl <em>Modeling Environment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.ModelingEnvironmentImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getModelingEnvironment()
	 * @generated
	 */
	int MODELING_ENVIRONMENT = 17;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.ModelElementsCoveragePolicyImpl <em>Model Elements Coverage Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.ModelElementsCoveragePolicyImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getModelElementsCoveragePolicy()
	 * @generated
	 */
	int MODEL_ELEMENTS_COVERAGE_POLICY = 19;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelSubsetImpl <em>Meta Model Subset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.MetaModelSubsetImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getMetaModelSubset()
	 * @generated
	 */
	int META_MODEL_SUBSET = 21;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.ActivityImpl <em>Activity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.ActivityImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getActivity()
	 * @generated
	 */
	int ACTIVITY = 22;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.SubsetRelationshipImpl <em>Subset Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.SubsetRelationshipImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getSubsetRelationship()
	 * @generated
	 */
	int SUBSET_RELATIONSHIP = 23;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.SubsettedMetaModelImpl <em>Subsetted Meta Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.SubsettedMetaModelImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getSubsettedMetaModel()
	 * @generated
	 */
	int SUBSETTED_META_MODEL = 20;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.ProjectUriRelBinRelPolicyImpl <em>Project Uri Rel Bin Rel Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.ProjectUriRelBinRelPolicyImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getProjectUriRelBinRelPolicy()
	 * @generated
	 */
	int PROJECT_URI_REL_BIN_REL_POLICY = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_URI_REL_BIN_REL_POLICY__NAME = URI_RELATED_BINARY_REL_POLICY__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_URI_REL_BIN_REL_POLICY__DESCRIPTION = URI_RELATED_BINARY_REL_POLICY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_URI_REL_BIN_REL_POLICY__DISPLAY_NAME = URI_RELATED_BINARY_REL_POLICY__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_URI_REL_BIN_REL_POLICY__ID = URI_RELATED_BINARY_REL_POLICY__ID;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_URI_REL_BIN_REL_POLICY__RELATION = URI_RELATED_BINARY_REL_POLICY__RELATION;

	/**
	 * The number of structural features of the '<em>Project Uri Rel Bin Rel Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_URI_REL_BIN_REL_POLICY_FEATURE_COUNT = URI_RELATED_BINARY_REL_POLICY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__NAME = MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__DESCRIPTION = MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__DISPLAY_NAME = MODEL__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__ID = MODEL__ID;

	/**
	 * The feature id for the '<em><b>Owned Relations</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__OWNED_RELATIONS = MODEL__OWNED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__OWNED_MODELS = MODEL__OWNED_MODELS;

	/**
	 * The feature id for the '<em><b>Parent Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__PARENT_MODEL = MODEL__PARENT_MODEL;

	/**
	 * The feature id for the '<em><b>Owned Obligation Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__OWNED_OBLIGATION_RELATIONS = MODEL__OWNED_OBLIGATION_RELATIONS;

	/**
	 * The feature id for the '<em><b>File Extensions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__FILE_EXTENSIONS = MODEL__FILE_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__RESOURCES = MODEL__RESOURCES;

	/**
	 * The feature id for the '<em><b>Owned Factual Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__OWNED_FACTUAL_RELATIONS = MODEL__OWNED_FACTUAL_RELATIONS;

	/**
	 * The feature id for the '<em><b>Error</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__ERROR = MODEL__ERROR;

	/**
	 * The feature id for the '<em><b>Source Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__SOURCE_MODEL = MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT__RELATION = MODEL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Modeling Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELING_ENVIRONMENT_FEATURE_COUNT = MODEL_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.ExecutionContextImpl <em>Execution Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.ExecutionContextImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getExecutionContext()
	 * @generated
	 */
	int EXECUTION_CONTEXT = 18;

	/**
	 * The feature id for the '<em><b>Manual Launch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONTEXT__MANUAL_LAUNCH = 0;

	/**
	 * The feature id for the '<em><b>Source Model Operation Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONTEXT__SOURCE_MODEL_OPERATION_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Previous Relation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONTEXT__PREVIOUS_RELATION = 2;

	/**
	 * The number of structural features of the '<em>Execution Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONTEXT_FEATURE_COUNT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENTS_COVERAGE_POLICY__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENTS_COVERAGE_POLICY__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENTS_COVERAGE_POLICY__DISPLAY_NAME = NAMED_ELEMENT__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENTS_COVERAGE_POLICY__ID = NAMED_ELEMENT__ID;

	/**
	 * The number of structural features of the '<em>Model Elements Coverage Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENTS_COVERAGE_POLICY_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__NAME = META_MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__DESCRIPTION = META_MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__DISPLAY_NAME = META_MODEL__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__ID = META_MODEL__ID;

	/**
	 * The feature id for the '<em><b>Owned Relations</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__OWNED_RELATIONS = META_MODEL__OWNED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__OWNED_MODELS = META_MODEL__OWNED_MODELS;

	/**
	 * The feature id for the '<em><b>Parent Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__PARENT_MODEL = META_MODEL__PARENT_MODEL;

	/**
	 * The feature id for the '<em><b>Owned Obligation Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__OWNED_OBLIGATION_RELATIONS = META_MODEL__OWNED_OBLIGATION_RELATIONS;

	/**
	 * The feature id for the '<em><b>File Extensions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__FILE_EXTENSIONS = META_MODEL__FILE_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__RESOURCES = META_MODEL__RESOURCES;

	/**
	 * The feature id for the '<em><b>Owned Factual Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__OWNED_FACTUAL_RELATIONS = META_MODEL__OWNED_FACTUAL_RELATIONS;

	/**
	 * The feature id for the '<em><b>Error</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__ERROR = META_MODEL__ERROR;

	/**
	 * The feature id for the '<em><b>Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__PACKAGES = META_MODEL__PACKAGES;

	/**
	 * The feature id for the '<em><b>Owned Subsets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__OWNED_SUBSETS = META_MODEL__OWNED_SUBSETS;

	/**
	 * The feature id for the '<em><b>Owned Comparison Settings</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__OWNED_COMPARISON_SETTINGS = META_MODEL__OWNED_COMPARISON_SETTINGS;

	/**
	 * The feature id for the '<em><b>Owned Constraint Targets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__OWNED_CONSTRAINT_TARGETS = META_MODEL__OWNED_CONSTRAINT_TARGETS;

	/**
	 * The feature id for the '<em><b>Subset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__SUBSET = META_MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Root Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__ROOT_META_MODEL = META_MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Subset Extensions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL__SUBSET_EXTENSIONS = META_MODEL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Subsetted Meta Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSETTED_META_MODEL_FEATURE_COUNT = META_MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_SUBSET__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_SUBSET__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_SUBSET__DISPLAY_NAME = NAMED_ELEMENT__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_SUBSET__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Owned Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_SUBSET__OWNED_CONSTRAINTS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Activities</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_SUBSET__ACTIVITIES = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Owned Composition Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_SUBSET__OWNED_COMPOSITION_OPERATIONS = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Meta Model Subset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_SUBSET_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__DISPLAY_NAME = NAMED_ELEMENT__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Required Subsets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__REQUIRED_SUBSETS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Supporting Tools</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__SUPPORTING_TOOLS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSET_RELATIONSHIP__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSET_RELATIONSHIP__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSET_RELATIONSHIP__DISPLAY_NAME = NAMED_ELEMENT__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSET_RELATIONSHIP__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSET_RELATIONSHIP__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target Subset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSET_RELATIONSHIP__TARGET_SUBSET = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Subset Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSET_RELATIONSHIP_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.GmmEngineImpl <em>Gmm Engine</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.GmmEngineImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getGmmEngine()
	 * @generated
	 */
	int GMM_ENGINE = 24;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_ENGINE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_ENGINE__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_ENGINE__DISPLAY_NAME = NAMED_ELEMENT__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_ENGINE__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Mega Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_ENGINE__MEGA_MODEL = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Gmm Engine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GMM_ENGINE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.SetOperationImpl <em>Set Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.SetOperationImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getSetOperation()
	 * @generated
	 */
	int SET_OPERATION = 25;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_OPERATION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_OPERATION__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_OPERATION__DISPLAY_NAME = NAMED_ELEMENT__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_OPERATION__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_OPERATION__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target Subset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_OPERATION__TARGET_SUBSET = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Set Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_OPERATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.impl.ToolImpl <em>Tool</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.impl.ToolImpl
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getTool()
	 * @generated
	 */
	int TOOL = 26;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__NAME = MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__DESCRIPTION = MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__DISPLAY_NAME = MODEL__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__ID = MODEL__ID;

	/**
	 * The feature id for the '<em><b>Owned Relations</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__OWNED_RELATIONS = MODEL__OWNED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__OWNED_MODELS = MODEL__OWNED_MODELS;

	/**
	 * The feature id for the '<em><b>Parent Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__PARENT_MODEL = MODEL__PARENT_MODEL;

	/**
	 * The feature id for the '<em><b>Owned Obligation Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__OWNED_OBLIGATION_RELATIONS = MODEL__OWNED_OBLIGATION_RELATIONS;

	/**
	 * The feature id for the '<em><b>File Extensions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__FILE_EXTENSIONS = MODEL__FILE_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__RESOURCES = MODEL__RESOURCES;

	/**
	 * The feature id for the '<em><b>Owned Factual Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__OWNED_FACTUAL_RELATIONS = MODEL__OWNED_FACTUAL_RELATIONS;

	/**
	 * The feature id for the '<em><b>Error</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__ERROR = MODEL__ERROR;

	/**
	 * The feature id for the '<em><b>Supported Activities</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL__SUPPORTED_ACTIVITIES = MODEL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Tool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOL_FEATURE_COUNT = MODEL_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.ValidityStatus <em>Validity Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.ValidityStatus
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getValidityStatus()
	 * @generated
	 */
	int VALIDITY_STATUS = 29;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.EnablementType <em>Enablement Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.EnablementType
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getEnablementType()
	 * @generated
	 */
	int ENABLEMENT_TYPE = 30;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.SubsetRelationshipType <em>Subset Relationship Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.SubsetRelationshipType
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getSubsetRelationshipType()
	 * @generated
	 */
	int SUBSET_RELATIONSHIP_TYPE = 31;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.SetOperationType <em>Set Operation Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.SetOperationType
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getSetOperationType()
	 * @generated
	 */
	int SET_OPERATION_TYPE = 32;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.ErrorType <em>Error Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.ErrorType
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getErrorType()
	 * @generated
	 */
	int ERROR_TYPE = 27;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.model.megamodel.OperationType <em>Operation Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.model.megamodel.OperationType
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getOperationType()
	 * @generated
	 */
	int OPERATION_TYPE = 28;

	/**
	 * The meta object id for the '<em>URI</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.URI
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getURI()
	 * @generated
	 */
	int URI = 33;


	/**
	 * The meta object id for the '<em>Functional Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.framework.core.exception.FunctionalException
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getFunctionalException()
	 * @generated
	 */
	int FUNCTIONAL_EXCEPTION = 34;

	/**
	 * The meta object id for the '<em>System Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.framework.core.exception.SystemException
	 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getSystemException()
	 * @generated
	 */
	int SYSTEM_EXCEPTION = 35;


	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.GmmSpecification <em>Gmm Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gmm Specification</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.GmmSpecification
	 * @generated
	 */
	EClass getGmmSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.gmm.model.megamodel.GmmSpecification#getOwnedActivities <em>Owned Activities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Activities</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.GmmSpecification#getOwnedActivities()
	 * @see #getGmmSpecification()
	 * @generated
	 */
	EReference getGmmSpecification_OwnedActivities();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.NamedElement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.NamedElement#getDescription()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Description();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.NamedElement#getDisplayName <em>Display Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Name</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.NamedElement#getDisplayName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_DisplayName();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.NamedElement#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.NamedElement#getId()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Id();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Model
	 * @generated
	 */
	EClass getModel();

	/**
	 * Returns the meta object for the attribute list '{@link fr.labsticc.gmm.model.megamodel.Model#getOwnedRelations <em>Owned Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Owned Relations</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Model#getOwnedRelations()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_OwnedRelations();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.gmm.model.megamodel.Model#getOwnedModels <em>Owned Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Models</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Model#getOwnedModels()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_OwnedModels();

	/**
	 * Returns the meta object for the container reference '{@link fr.labsticc.gmm.model.megamodel.Model#getParentModel <em>Parent Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Model</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Model#getParentModel()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_ParentModel();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.gmm.model.megamodel.Model#getOwnedObligationRelations <em>Owned Obligation Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Obligation Relations</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Model#getOwnedObligationRelations()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_OwnedObligationRelations();

	/**
	 * Returns the meta object for the attribute list '{@link fr.labsticc.gmm.model.megamodel.Model#getFileExtensions <em>File Extensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>File Extensions</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Model#getFileExtensions()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_FileExtensions();

	/**
	 * Returns the meta object for the attribute list '{@link fr.labsticc.gmm.model.megamodel.Model#getResources <em>Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Resources</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Model#getResources()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_Resources();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.gmm.model.megamodel.Model#getOwnedFactualRelations <em>Owned Factual Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Factual Relations</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Model#getOwnedFactualRelations()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_OwnedFactualRelations();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.gmm.model.megamodel.Model#getError <em>Error</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Error</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Model#getError()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_Error();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.ErrorDescription <em>Error Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Error Description</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ErrorDescription
	 * @generated
	 */
	EClass getErrorDescription();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.ErrorDescription#getErrorStatus <em>Error Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Error Status</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ErrorDescription#getErrorStatus()
	 * @see #getErrorDescription()
	 * @generated
	 */
	EAttribute getErrorDescription_ErrorStatus();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.ErrorDescription#getErroneousElements <em>Erroneous Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Erroneous Elements</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ErrorDescription#getErroneousElements()
	 * @see #getErrorDescription()
	 * @generated
	 */
	EAttribute getErrorDescription_ErroneousElements();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.MetaModel <em>Meta Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Meta Model</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.MetaModel
	 * @generated
	 */
	EClass getMetaModel();

	/**
	 * Returns the meta object for the reference list '{@link fr.labsticc.gmm.model.megamodel.MetaModel#getPackages <em>Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Packages</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.MetaModel#getPackages()
	 * @see #getMetaModel()
	 * @generated
	 */
	EReference getMetaModel_Packages();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.gmm.model.megamodel.MetaModel#getOwnedSubsets <em>Owned Subsets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Subsets</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.MetaModel#getOwnedSubsets()
	 * @see #getMetaModel()
	 * @generated
	 */
	EReference getMetaModel_OwnedSubsets();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.gmm.model.megamodel.MetaModel#getOwnedComparisonSettings <em>Owned Comparison Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Owned Comparison Settings</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.MetaModel#getOwnedComparisonSettings()
	 * @see #getMetaModel()
	 * @generated
	 */
	EReference getMetaModel_OwnedComparisonSettings();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.gmm.model.megamodel.MetaModel#getOwnedConstraintTargets <em>Owned Constraint Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Constraint Targets</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.MetaModel#getOwnedConstraintTargets()
	 * @see #getMetaModel()
	 * @generated
	 */
	EReference getMetaModel_OwnedConstraintTargets();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.ComparisonSettings <em>Comparison Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Comparison Settings</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ComparisonSettings
	 * @generated
	 */
	EClass getComparisonSettings();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.ComparisonSettings#isUseIdForCompare <em>Use Id For Compare</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Use Id For Compare</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ComparisonSettings#isUseIdForCompare()
	 * @see #getComparisonSettings()
	 * @generated
	 */
	EAttribute getComparisonSettings_UseIdForCompare();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.ComparisonSettings#getNameSimilarityWeight <em>Name Similarity Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name Similarity Weight</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ComparisonSettings#getNameSimilarityWeight()
	 * @see #getComparisonSettings()
	 * @generated
	 */
	EAttribute getComparisonSettings_NameSimilarityWeight();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.ComparisonSettings#getPositionSimilarityWeight <em>Position Similarity Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position Similarity Weight</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ComparisonSettings#getPositionSimilarityWeight()
	 * @see #getComparisonSettings()
	 * @generated
	 */
	EAttribute getComparisonSettings_PositionSimilarityWeight();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.Relation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Relation
	 * @generated
	 */
	EClass getRelation();

	/**
	 * Returns the meta object for the attribute list '{@link fr.labsticc.gmm.model.megamodel.Relation#getConnectedModels <em>Connected Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Connected Models</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Relation#getConnectedModels()
	 * @see #getRelation()
	 * @generated
	 */
	EAttribute getRelation_ConnectedModels();

	/**
	 * Returns the meta object for the reference list '{@link fr.labsticc.gmm.model.megamodel.Relation#getChainedRelations <em>Chained Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Chained Relations</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Relation#getChainedRelations()
	 * @see #getRelation()
	 * @generated
	 */
	EReference getRelation_ChainedRelations();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.gmm.model.megamodel.Relation#getOwnedRelationPolicy <em>Owned Relation Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Owned Relation Policy</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Relation#getOwnedRelationPolicy()
	 * @see #getRelation()
	 * @generated
	 */
	EReference getRelation_OwnedRelationPolicy();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.Relation#getIntention <em>Intention</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Intention</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Relation#getIntention()
	 * @see #getRelation()
	 * @generated
	 */
	EAttribute getRelation_Intention();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.FactualRelation <em>Factual Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Factual Relation</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.FactualRelation
	 * @generated
	 */
	EClass getFactualRelation();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.ObligationRelation <em>Obligation Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Obligation Relation</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ObligationRelation
	 * @generated
	 */
	EClass getObligationRelation();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.gmm.model.megamodel.ObligationRelation#getOwnedCoveragePolicy <em>Owned Coverage Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Owned Coverage Policy</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ObligationRelation#getOwnedCoveragePolicy()
	 * @see #getObligationRelation()
	 * @generated
	 */
	EReference getObligationRelation_OwnedCoveragePolicy();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.ObligationRelation#getEnablementType <em>Enablement Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Enablement Type</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ObligationRelation#getEnablementType()
	 * @see #getObligationRelation()
	 * @generated
	 */
	EAttribute getObligationRelation_EnablementType();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.ObligationRelation#isDeleting <em>Deleting</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deleting</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ObligationRelation#isDeleting()
	 * @see #getObligationRelation()
	 * @generated
	 */
	EAttribute getObligationRelation_Deleting();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.ObligationRelation#getValidityStatus <em>Validity Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Validity Status</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ObligationRelation#getValidityStatus()
	 * @see #getObligationRelation()
	 * @generated
	 */
	EAttribute getObligationRelation_ValidityStatus();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.ConformanceRelation <em>Conformance Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conformance Relation</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ConformanceRelation
	 * @generated
	 */
	EClass getConformanceRelation();

	/**
	 * Returns the meta object for the reference list '{@link fr.labsticc.gmm.model.megamodel.ConformanceRelation#getModels <em>Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Models</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ConformanceRelation#getModels()
	 * @see #getConformanceRelation()
	 * @generated
	 */
	EReference getConformanceRelation_Models();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.model.megamodel.ConformanceRelation#getMetaModel <em>Meta Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Meta Model</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ConformanceRelation#getMetaModel()
	 * @see #getConformanceRelation()
	 * @generated
	 */
	EReference getConformanceRelation_MetaModel();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation <em>Meta Model Related Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Meta Model Related Relation</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation
	 * @generated
	 */
	EClass getMetaModelRelatedRelation();

	/**
	 * Returns the meta object for the attribute list '{@link fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation#getMetaModels <em>Meta Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Meta Models</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation#getMetaModels()
	 * @see #getMetaModelRelatedRelation()
	 * @generated
	 */
	EAttribute getMetaModelRelatedRelation_MetaModels();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation <em>Binary Meta Mode Relatedl Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Meta Mode Relatedl Relation</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation
	 * @generated
	 */
	EClass getBinaryMetaModeRelatedlRelation();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation#getLeftMetaModel <em>Left Meta Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Left Meta Model</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation#getLeftMetaModel()
	 * @see #getBinaryMetaModeRelatedlRelation()
	 * @generated
	 */
	EReference getBinaryMetaModeRelatedlRelation_LeftMetaModel();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation#getRightMetaModel <em>Right Meta Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Right Meta Model</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation#getRightMetaModel()
	 * @see #getBinaryMetaModeRelatedlRelation()
	 * @generated
	 */
	EReference getBinaryMetaModeRelatedlRelation_RightMetaModel();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation <em>Binary Meta Model Related Consistency Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Meta Model Related Consistency Relation</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation
	 * @generated
	 */
	EClass getBinaryMetaModelRelatedConsistencyRelation();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation#isDeletingLeft <em>Deleting Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deleting Left</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation#isDeletingLeft()
	 * @see #getBinaryMetaModelRelatedConsistencyRelation()
	 * @generated
	 */
	EAttribute getBinaryMetaModelRelatedConsistencyRelation_DeletingLeft();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation#isDeletingRight <em>Deleting Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deleting Right</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation#isDeletingRight()
	 * @see #getBinaryMetaModelRelatedConsistencyRelation()
	 * @generated
	 */
	EAttribute getBinaryMetaModelRelatedConsistencyRelation_DeletingRight();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.RelationPolicy <em>Relation Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation Policy</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.RelationPolicy
	 * @generated
	 */
	EClass getRelationPolicy();

	/**
	 * Returns the meta object for the container reference '{@link fr.labsticc.gmm.model.megamodel.RelationPolicy#getRelation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Relation</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.RelationPolicy#getRelation()
	 * @see #getRelationPolicy()
	 * @generated
	 */
	EReference getRelationPolicy_Relation();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.BinaryRelationPolicy <em>Binary Relation Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Relation Policy</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.BinaryRelationPolicy
	 * @generated
	 */
	EClass getBinaryRelationPolicy();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.UriRelatedBinaryRelPolicy <em>Uri Related Binary Rel Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uri Related Binary Rel Policy</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.UriRelatedBinaryRelPolicy
	 * @generated
	 */
	EClass getUriRelatedBinaryRelPolicy();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.ModelingEnvironment <em>Modeling Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Modeling Environment</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ModelingEnvironment
	 * @generated
	 */
	EClass getModelingEnvironment();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.model.megamodel.ModelingEnvironment#getSourceModel <em>Source Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source Model</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ModelingEnvironment#getSourceModel()
	 * @see #getModelingEnvironment()
	 * @generated
	 */
	EReference getModelingEnvironment_SourceModel();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.model.megamodel.ModelingEnvironment#getRelation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Relation</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ModelingEnvironment#getRelation()
	 * @see #getModelingEnvironment()
	 * @generated
	 */
	EReference getModelingEnvironment_Relation();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.ModelElementsCoveragePolicy <em>Model Elements Coverage Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Elements Coverage Policy</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ModelElementsCoveragePolicy
	 * @generated
	 */
	EClass getModelElementsCoveragePolicy();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.MetaModelSubset <em>Meta Model Subset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Meta Model Subset</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.MetaModelSubset
	 * @generated
	 */
	EClass getMetaModelSubset();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.gmm.model.megamodel.MetaModelSubset#getOwnedConstraints <em>Owned Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Constraints</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.MetaModelSubset#getOwnedConstraints()
	 * @see #getMetaModelSubset()
	 * @generated
	 */
	EReference getMetaModelSubset_OwnedConstraints();

	/**
	 * Returns the meta object for the reference list '{@link fr.labsticc.gmm.model.megamodel.MetaModelSubset#getActivities <em>Activities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Activities</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.MetaModelSubset#getActivities()
	 * @see #getMetaModelSubset()
	 * @generated
	 */
	EReference getMetaModelSubset_Activities();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.gmm.model.megamodel.MetaModelSubset#getOwnedCompositionOperations <em>Owned Composition Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Composition Operations</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.MetaModelSubset#getOwnedCompositionOperations()
	 * @see #getMetaModelSubset()
	 * @generated
	 */
	EReference getMetaModelSubset_OwnedCompositionOperations();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.Activity <em>Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Activity</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Activity
	 * @generated
	 */
	EClass getActivity();

	/**
	 * Returns the meta object for the reference list '{@link fr.labsticc.gmm.model.megamodel.Activity#getRequiredSubsets <em>Required Subsets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Subsets</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Activity#getRequiredSubsets()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_RequiredSubsets();

	/**
	 * Returns the meta object for the reference list '{@link fr.labsticc.gmm.model.megamodel.Activity#getSupportingTools <em>Supporting Tools</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Supporting Tools</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Activity#getSupportingTools()
	 * @see #getActivity()
	 * @generated
	 */
	EReference getActivity_SupportingTools();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.SubsetRelationship <em>Subset Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subset Relationship</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.SubsetRelationship
	 * @generated
	 */
	EClass getSubsetRelationship();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.SubsetRelationship#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.SubsetRelationship#getType()
	 * @see #getSubsetRelationship()
	 * @generated
	 */
	EAttribute getSubsetRelationship_Type();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.model.megamodel.SubsetRelationship#getTargetSubset <em>Target Subset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target Subset</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.SubsetRelationship#getTargetSubset()
	 * @see #getSubsetRelationship()
	 * @generated
	 */
	EReference getSubsetRelationship_TargetSubset();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.SubsettedMetaModel <em>Subsetted Meta Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subsetted Meta Model</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.SubsettedMetaModel
	 * @generated
	 */
	EClass getSubsettedMetaModel();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.model.megamodel.SubsettedMetaModel#getSubset <em>Subset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Subset</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.SubsettedMetaModel#getSubset()
	 * @see #getSubsettedMetaModel()
	 * @generated
	 */
	EReference getSubsettedMetaModel_Subset();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.model.megamodel.SubsettedMetaModel#getRootMetaModel <em>Root Meta Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Root Meta Model</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.SubsettedMetaModel#getRootMetaModel()
	 * @see #getSubsettedMetaModel()
	 * @generated
	 */
	EReference getSubsettedMetaModel_RootMetaModel();

	/**
	 * Returns the meta object for the attribute list '{@link fr.labsticc.gmm.model.megamodel.SubsettedMetaModel#getSubsetExtensions <em>Subset Extensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Subset Extensions</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.SubsettedMetaModel#getSubsetExtensions()
	 * @see #getSubsettedMetaModel()
	 * @generated
	 */
	EAttribute getSubsettedMetaModel_SubsetExtensions();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.ProjectUriRelBinRelPolicy <em>Project Uri Rel Bin Rel Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Project Uri Rel Bin Rel Policy</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ProjectUriRelBinRelPolicy
	 * @generated
	 */
	EClass getProjectUriRelBinRelPolicy();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.ExecutionContext <em>Execution Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Execution Context</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ExecutionContext
	 * @generated
	 */
	EClass getExecutionContext();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.ExecutionContext#isManualLaunch <em>Manual Launch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Manual Launch</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ExecutionContext#isManualLaunch()
	 * @see #getExecutionContext()
	 * @generated
	 */
	EAttribute getExecutionContext_ManualLaunch();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.ExecutionContext#getSourceModelOperationType <em>Source Model Operation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Model Operation Type</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ExecutionContext#getSourceModelOperationType()
	 * @see #getExecutionContext()
	 * @generated
	 */
	EAttribute getExecutionContext_SourceModelOperationType();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.model.megamodel.ExecutionContext#getPreviousRelation <em>Previous Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Previous Relation</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ExecutionContext#getPreviousRelation()
	 * @see #getExecutionContext()
	 * @generated
	 */
	EReference getExecutionContext_PreviousRelation();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.GmmEngine <em>Gmm Engine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gmm Engine</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.GmmEngine
	 * @generated
	 */
	EClass getGmmEngine();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.model.megamodel.GmmEngine#getMegaModel <em>Mega Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Mega Model</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.GmmEngine#getMegaModel()
	 * @see #getGmmEngine()
	 * @generated
	 */
	EReference getGmmEngine_MegaModel();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.SetOperation <em>Set Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Set Operation</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.SetOperation
	 * @generated
	 */
	EClass getSetOperation();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.model.megamodel.SetOperation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.SetOperation#getType()
	 * @see #getSetOperation()
	 * @generated
	 */
	EAttribute getSetOperation_Type();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.model.megamodel.SetOperation#getTargetSubset <em>Target Subset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target Subset</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.SetOperation#getTargetSubset()
	 * @see #getSetOperation()
	 * @generated
	 */
	EReference getSetOperation_TargetSubset();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.model.megamodel.Tool <em>Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tool</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Tool
	 * @generated
	 */
	EClass getTool();

	/**
	 * Returns the meta object for the reference list '{@link fr.labsticc.gmm.model.megamodel.Tool#getSupportedActivities <em>Supported Activities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Supported Activities</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.Tool#getSupportedActivities()
	 * @see #getTool()
	 * @generated
	 */
	EReference getTool_SupportedActivities();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.gmm.model.megamodel.ValidityStatus <em>Validity Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Validity Status</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ValidityStatus
	 * @generated
	 */
	EEnum getValidityStatus();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.gmm.model.megamodel.EnablementType <em>Enablement Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Enablement Type</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.EnablementType
	 * @generated
	 */
	EEnum getEnablementType();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.gmm.model.megamodel.SubsetRelationshipType <em>Subset Relationship Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Subset Relationship Type</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.SubsetRelationshipType
	 * @generated
	 */
	EEnum getSubsetRelationshipType();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.gmm.model.megamodel.SetOperationType <em>Set Operation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Set Operation Type</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.SetOperationType
	 * @generated
	 */
	EEnum getSetOperationType();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.gmm.model.megamodel.ErrorType <em>Error Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Error Type</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.ErrorType
	 * @generated
	 */
	EEnum getErrorType();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.gmm.model.megamodel.OperationType <em>Operation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Operation Type</em>'.
	 * @see fr.labsticc.gmm.model.megamodel.OperationType
	 * @generated
	 */
	EEnum getOperationType();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.common.util.URI <em>URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>URI</em>'.
	 * @see org.eclipse.emf.common.util.URI
	 * @model instanceClass="org.eclipse.emf.common.util.URI"
	 * @generated
	 */
	EDataType getURI();

	/**
	 * Returns the meta object for data type '{@link fr.labsticc.framework.core.exception.FunctionalException <em>Functional Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Functional Exception</em>'.
	 * @see fr.labsticc.framework.core.exception.FunctionalException
	 * @model instanceClass="fr.labsticc.framework.core.exception.FunctionalException"
	 * @generated
	 */
	EDataType getFunctionalException();

	/**
	 * Returns the meta object for data type '{@link fr.labsticc.framework.core.exception.SystemException <em>System Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>System Exception</em>'.
	 * @see fr.labsticc.framework.core.exception.SystemException
	 * @model instanceClass="fr.labsticc.framework.core.exception.SystemException"
	 * @generated
	 */
	EDataType getSystemException();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MegamodelFactory getMegamodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.GmmSpecificationImpl <em>Gmm Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.GmmSpecificationImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getGmmSpecification()
		 * @generated
		 */
		EClass GMM_SPECIFICATION = eINSTANCE.getGmmSpecification();

		/**
		 * The meta object literal for the '<em><b>Owned Activities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GMM_SPECIFICATION__OWNED_ACTIVITIES = eINSTANCE.getGmmSpecification_OwnedActivities();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.NamedElementImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__DESCRIPTION = eINSTANCE.getNamedElement_Description();

		/**
		 * The meta object literal for the '<em><b>Display Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__DISPLAY_NAME = eINSTANCE.getNamedElement_DisplayName();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__ID = eINSTANCE.getNamedElement_Id();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.ModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.ModelImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getModel()
		 * @generated
		 */
		EClass MODEL = eINSTANCE.getModel();

		/**
		 * The meta object literal for the '<em><b>Owned Relations</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__OWNED_RELATIONS = eINSTANCE.getModel_OwnedRelations();

		/**
		 * The meta object literal for the '<em><b>Owned Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__OWNED_MODELS = eINSTANCE.getModel_OwnedModels();

		/**
		 * The meta object literal for the '<em><b>Parent Model</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__PARENT_MODEL = eINSTANCE.getModel_ParentModel();

		/**
		 * The meta object literal for the '<em><b>Owned Obligation Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__OWNED_OBLIGATION_RELATIONS = eINSTANCE.getModel_OwnedObligationRelations();

		/**
		 * The meta object literal for the '<em><b>File Extensions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__FILE_EXTENSIONS = eINSTANCE.getModel_FileExtensions();

		/**
		 * The meta object literal for the '<em><b>Resources</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__RESOURCES = eINSTANCE.getModel_Resources();

		/**
		 * The meta object literal for the '<em><b>Owned Factual Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__OWNED_FACTUAL_RELATIONS = eINSTANCE.getModel_OwnedFactualRelations();

		/**
		 * The meta object literal for the '<em><b>Error</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__ERROR = eINSTANCE.getModel_Error();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.ErrorDescriptionImpl <em>Error Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.ErrorDescriptionImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getErrorDescription()
		 * @generated
		 */
		EClass ERROR_DESCRIPTION = eINSTANCE.getErrorDescription();

		/**
		 * The meta object literal for the '<em><b>Error Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ERROR_DESCRIPTION__ERROR_STATUS = eINSTANCE.getErrorDescription_ErrorStatus();

		/**
		 * The meta object literal for the '<em><b>Erroneous Elements</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ERROR_DESCRIPTION__ERRONEOUS_ELEMENTS = eINSTANCE.getErrorDescription_ErroneousElements();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelImpl <em>Meta Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.MetaModelImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getMetaModel()
		 * @generated
		 */
		EClass META_MODEL = eINSTANCE.getMetaModel();

		/**
		 * The meta object literal for the '<em><b>Packages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_MODEL__PACKAGES = eINSTANCE.getMetaModel_Packages();

		/**
		 * The meta object literal for the '<em><b>Owned Subsets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_MODEL__OWNED_SUBSETS = eINSTANCE.getMetaModel_OwnedSubsets();

		/**
		 * The meta object literal for the '<em><b>Owned Comparison Settings</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_MODEL__OWNED_COMPARISON_SETTINGS = eINSTANCE.getMetaModel_OwnedComparisonSettings();

		/**
		 * The meta object literal for the '<em><b>Owned Constraint Targets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_MODEL__OWNED_CONSTRAINT_TARGETS = eINSTANCE.getMetaModel_OwnedConstraintTargets();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.ComparisonSettingsImpl <em>Comparison Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.ComparisonSettingsImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getComparisonSettings()
		 * @generated
		 */
		EClass COMPARISON_SETTINGS = eINSTANCE.getComparisonSettings();

		/**
		 * The meta object literal for the '<em><b>Use Id For Compare</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPARISON_SETTINGS__USE_ID_FOR_COMPARE = eINSTANCE.getComparisonSettings_UseIdForCompare();

		/**
		 * The meta object literal for the '<em><b>Name Similarity Weight</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPARISON_SETTINGS__NAME_SIMILARITY_WEIGHT = eINSTANCE.getComparisonSettings_NameSimilarityWeight();

		/**
		 * The meta object literal for the '<em><b>Position Similarity Weight</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPARISON_SETTINGS__POSITION_SIMILARITY_WEIGHT = eINSTANCE.getComparisonSettings_PositionSimilarityWeight();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.RelationImpl <em>Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.RelationImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getRelation()
		 * @generated
		 */
		EClass RELATION = eINSTANCE.getRelation();

		/**
		 * The meta object literal for the '<em><b>Connected Models</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATION__CONNECTED_MODELS = eINSTANCE.getRelation_ConnectedModels();

		/**
		 * The meta object literal for the '<em><b>Chained Relations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATION__CHAINED_RELATIONS = eINSTANCE.getRelation_ChainedRelations();

		/**
		 * The meta object literal for the '<em><b>Owned Relation Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATION__OWNED_RELATION_POLICY = eINSTANCE.getRelation_OwnedRelationPolicy();

		/**
		 * The meta object literal for the '<em><b>Intention</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATION__INTENTION = eINSTANCE.getRelation_Intention();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.FactualRelationImpl <em>Factual Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.FactualRelationImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getFactualRelation()
		 * @generated
		 */
		EClass FACTUAL_RELATION = eINSTANCE.getFactualRelation();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.ObligationRelationImpl <em>Obligation Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.ObligationRelationImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getObligationRelation()
		 * @generated
		 */
		EClass OBLIGATION_RELATION = eINSTANCE.getObligationRelation();

		/**
		 * The meta object literal for the '<em><b>Owned Coverage Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBLIGATION_RELATION__OWNED_COVERAGE_POLICY = eINSTANCE.getObligationRelation_OwnedCoveragePolicy();

		/**
		 * The meta object literal for the '<em><b>Enablement Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBLIGATION_RELATION__ENABLEMENT_TYPE = eINSTANCE.getObligationRelation_EnablementType();

		/**
		 * The meta object literal for the '<em><b>Deleting</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBLIGATION_RELATION__DELETING = eINSTANCE.getObligationRelation_Deleting();

		/**
		 * The meta object literal for the '<em><b>Validity Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBLIGATION_RELATION__VALIDITY_STATUS = eINSTANCE.getObligationRelation_ValidityStatus();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.ConformanceRelationImpl <em>Conformance Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.ConformanceRelationImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getConformanceRelation()
		 * @generated
		 */
		EClass CONFORMANCE_RELATION = eINSTANCE.getConformanceRelation();

		/**
		 * The meta object literal for the '<em><b>Models</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFORMANCE_RELATION__MODELS = eINSTANCE.getConformanceRelation_Models();

		/**
		 * The meta object literal for the '<em><b>Meta Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFORMANCE_RELATION__META_MODEL = eINSTANCE.getConformanceRelation_MetaModel();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelRelatedRelationImpl <em>Meta Model Related Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.MetaModelRelatedRelationImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getMetaModelRelatedRelation()
		 * @generated
		 */
		EClass META_MODEL_RELATED_RELATION = eINSTANCE.getMetaModelRelatedRelation();

		/**
		 * The meta object literal for the '<em><b>Meta Models</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute META_MODEL_RELATED_RELATION__META_MODELS = eINSTANCE.getMetaModelRelatedRelation_MetaModels();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModeRelatedlRelationImpl <em>Binary Meta Mode Relatedl Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModeRelatedlRelationImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getBinaryMetaModeRelatedlRelation()
		 * @generated
		 */
		EClass BINARY_META_MODE_RELATEDL_RELATION = eINSTANCE.getBinaryMetaModeRelatedlRelation();

		/**
		 * The meta object literal for the '<em><b>Left Meta Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL = eINSTANCE.getBinaryMetaModeRelatedlRelation_LeftMetaModel();

		/**
		 * The meta object literal for the '<em><b>Right Meta Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL = eINSTANCE.getBinaryMetaModeRelatedlRelation_RightMetaModel();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModelRelatedConsistencyRelationImpl <em>Binary Meta Model Related Consistency Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModelRelatedConsistencyRelationImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getBinaryMetaModelRelatedConsistencyRelation()
		 * @generated
		 */
		EClass BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION = eINSTANCE.getBinaryMetaModelRelatedConsistencyRelation();

		/**
		 * The meta object literal for the '<em><b>Deleting Left</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_LEFT = eINSTANCE.getBinaryMetaModelRelatedConsistencyRelation_DeletingLeft();

		/**
		 * The meta object literal for the '<em><b>Deleting Right</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_RIGHT = eINSTANCE.getBinaryMetaModelRelatedConsistencyRelation_DeletingRight();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.RelationPolicyImpl <em>Relation Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.RelationPolicyImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getRelationPolicy()
		 * @generated
		 */
		EClass RELATION_POLICY = eINSTANCE.getRelationPolicy();

		/**
		 * The meta object literal for the '<em><b>Relation</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATION_POLICY__RELATION = eINSTANCE.getRelationPolicy_Relation();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.BinaryRelationPolicyImpl <em>Binary Relation Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.BinaryRelationPolicyImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getBinaryRelationPolicy()
		 * @generated
		 */
		EClass BINARY_RELATION_POLICY = eINSTANCE.getBinaryRelationPolicy();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.UriRelatedBinaryRelPolicyImpl <em>Uri Related Binary Rel Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.UriRelatedBinaryRelPolicyImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getUriRelatedBinaryRelPolicy()
		 * @generated
		 */
		EClass URI_RELATED_BINARY_REL_POLICY = eINSTANCE.getUriRelatedBinaryRelPolicy();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.ModelingEnvironmentImpl <em>Modeling Environment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.ModelingEnvironmentImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getModelingEnvironment()
		 * @generated
		 */
		EClass MODELING_ENVIRONMENT = eINSTANCE.getModelingEnvironment();

		/**
		 * The meta object literal for the '<em><b>Source Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODELING_ENVIRONMENT__SOURCE_MODEL = eINSTANCE.getModelingEnvironment_SourceModel();

		/**
		 * The meta object literal for the '<em><b>Relation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODELING_ENVIRONMENT__RELATION = eINSTANCE.getModelingEnvironment_Relation();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.ModelElementsCoveragePolicyImpl <em>Model Elements Coverage Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.ModelElementsCoveragePolicyImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getModelElementsCoveragePolicy()
		 * @generated
		 */
		EClass MODEL_ELEMENTS_COVERAGE_POLICY = eINSTANCE.getModelElementsCoveragePolicy();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.MetaModelSubsetImpl <em>Meta Model Subset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.MetaModelSubsetImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getMetaModelSubset()
		 * @generated
		 */
		EClass META_MODEL_SUBSET = eINSTANCE.getMetaModelSubset();

		/**
		 * The meta object literal for the '<em><b>Owned Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_MODEL_SUBSET__OWNED_CONSTRAINTS = eINSTANCE.getMetaModelSubset_OwnedConstraints();

		/**
		 * The meta object literal for the '<em><b>Activities</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_MODEL_SUBSET__ACTIVITIES = eINSTANCE.getMetaModelSubset_Activities();

		/**
		 * The meta object literal for the '<em><b>Owned Composition Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_MODEL_SUBSET__OWNED_COMPOSITION_OPERATIONS = eINSTANCE.getMetaModelSubset_OwnedCompositionOperations();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.ActivityImpl <em>Activity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.ActivityImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getActivity()
		 * @generated
		 */
		EClass ACTIVITY = eINSTANCE.getActivity();

		/**
		 * The meta object literal for the '<em><b>Required Subsets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__REQUIRED_SUBSETS = eINSTANCE.getActivity_RequiredSubsets();

		/**
		 * The meta object literal for the '<em><b>Supporting Tools</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY__SUPPORTING_TOOLS = eINSTANCE.getActivity_SupportingTools();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.SubsetRelationshipImpl <em>Subset Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.SubsetRelationshipImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getSubsetRelationship()
		 * @generated
		 */
		EClass SUBSET_RELATIONSHIP = eINSTANCE.getSubsetRelationship();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBSET_RELATIONSHIP__TYPE = eINSTANCE.getSubsetRelationship_Type();

		/**
		 * The meta object literal for the '<em><b>Target Subset</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBSET_RELATIONSHIP__TARGET_SUBSET = eINSTANCE.getSubsetRelationship_TargetSubset();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.SubsettedMetaModelImpl <em>Subsetted Meta Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.SubsettedMetaModelImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getSubsettedMetaModel()
		 * @generated
		 */
		EClass SUBSETTED_META_MODEL = eINSTANCE.getSubsettedMetaModel();

		/**
		 * The meta object literal for the '<em><b>Subset</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBSETTED_META_MODEL__SUBSET = eINSTANCE.getSubsettedMetaModel_Subset();

		/**
		 * The meta object literal for the '<em><b>Root Meta Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBSETTED_META_MODEL__ROOT_META_MODEL = eINSTANCE.getSubsettedMetaModel_RootMetaModel();

		/**
		 * The meta object literal for the '<em><b>Subset Extensions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBSETTED_META_MODEL__SUBSET_EXTENSIONS = eINSTANCE.getSubsettedMetaModel_SubsetExtensions();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.ProjectUriRelBinRelPolicyImpl <em>Project Uri Rel Bin Rel Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.ProjectUriRelBinRelPolicyImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getProjectUriRelBinRelPolicy()
		 * @generated
		 */
		EClass PROJECT_URI_REL_BIN_REL_POLICY = eINSTANCE.getProjectUriRelBinRelPolicy();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.ExecutionContextImpl <em>Execution Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.ExecutionContextImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getExecutionContext()
		 * @generated
		 */
		EClass EXECUTION_CONTEXT = eINSTANCE.getExecutionContext();

		/**
		 * The meta object literal for the '<em><b>Manual Launch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_CONTEXT__MANUAL_LAUNCH = eINSTANCE.getExecutionContext_ManualLaunch();

		/**
		 * The meta object literal for the '<em><b>Source Model Operation Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_CONTEXT__SOURCE_MODEL_OPERATION_TYPE = eINSTANCE.getExecutionContext_SourceModelOperationType();

		/**
		 * The meta object literal for the '<em><b>Previous Relation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_CONTEXT__PREVIOUS_RELATION = eINSTANCE.getExecutionContext_PreviousRelation();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.GmmEngineImpl <em>Gmm Engine</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.GmmEngineImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getGmmEngine()
		 * @generated
		 */
		EClass GMM_ENGINE = eINSTANCE.getGmmEngine();

		/**
		 * The meta object literal for the '<em><b>Mega Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GMM_ENGINE__MEGA_MODEL = eINSTANCE.getGmmEngine_MegaModel();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.SetOperationImpl <em>Set Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.SetOperationImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getSetOperation()
		 * @generated
		 */
		EClass SET_OPERATION = eINSTANCE.getSetOperation();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SET_OPERATION__TYPE = eINSTANCE.getSetOperation_Type();

		/**
		 * The meta object literal for the '<em><b>Target Subset</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_OPERATION__TARGET_SUBSET = eINSTANCE.getSetOperation_TargetSubset();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.impl.ToolImpl <em>Tool</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.impl.ToolImpl
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getTool()
		 * @generated
		 */
		EClass TOOL = eINSTANCE.getTool();

		/**
		 * The meta object literal for the '<em><b>Supported Activities</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOOL__SUPPORTED_ACTIVITIES = eINSTANCE.getTool_SupportedActivities();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.ValidityStatus <em>Validity Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.ValidityStatus
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getValidityStatus()
		 * @generated
		 */
		EEnum VALIDITY_STATUS = eINSTANCE.getValidityStatus();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.EnablementType <em>Enablement Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.EnablementType
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getEnablementType()
		 * @generated
		 */
		EEnum ENABLEMENT_TYPE = eINSTANCE.getEnablementType();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.SubsetRelationshipType <em>Subset Relationship Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.SubsetRelationshipType
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getSubsetRelationshipType()
		 * @generated
		 */
		EEnum SUBSET_RELATIONSHIP_TYPE = eINSTANCE.getSubsetRelationshipType();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.SetOperationType <em>Set Operation Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.SetOperationType
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getSetOperationType()
		 * @generated
		 */
		EEnum SET_OPERATION_TYPE = eINSTANCE.getSetOperationType();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.ErrorType <em>Error Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.ErrorType
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getErrorType()
		 * @generated
		 */
		EEnum ERROR_TYPE = eINSTANCE.getErrorType();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.model.megamodel.OperationType <em>Operation Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.model.megamodel.OperationType
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getOperationType()
		 * @generated
		 */
		EEnum OPERATION_TYPE = eINSTANCE.getOperationType();

		/**
		 * The meta object literal for the '<em>URI</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.common.util.URI
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getURI()
		 * @generated
		 */
		EDataType URI = eINSTANCE.getURI();

		/**
		 * The meta object literal for the '<em>Functional Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.framework.core.exception.FunctionalException
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getFunctionalException()
		 * @generated
		 */
		EDataType FUNCTIONAL_EXCEPTION = eINSTANCE.getFunctionalException();

		/**
		 * The meta object literal for the '<em>System Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.framework.core.exception.SystemException
		 * @see fr.labsticc.gmm.model.megamodel.impl.MegamodelPackageImpl#getSystemException()
		 * @generated
		 */
		EDataType SYSTEM_EXCEPTION = eINSTANCE.getSystemException();

	}

} //MegamodelPackage
