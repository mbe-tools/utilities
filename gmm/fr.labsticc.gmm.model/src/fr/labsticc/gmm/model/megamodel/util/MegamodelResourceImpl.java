/**
 */
package fr.labsticc.gmm.model.megamodel.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see fr.labsticc.gmm.model.megamodel.util.MegamodelResourceFactoryImpl
 * @generated
 */
public class MegamodelResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated NOT
	 */
	public MegamodelResourceImpl(URI uri) {
		super(uri);
		
		// DB: Allow preserving the config sheme.
		getDefaultSaveOptions().put( OPTION_URI_HANDLER, new URIHandlerImpl.PlatformSchemeAware() );
	}

	@Override
	protected boolean useUUIDs() {
		return false;
	}
} //MegamodelResourceImpl
