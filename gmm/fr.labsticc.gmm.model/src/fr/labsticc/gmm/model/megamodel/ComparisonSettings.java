/**
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comparison Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ComparisonSettings#isUseIdForCompare <em>Use Id For Compare</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ComparisonSettings#getNameSimilarityWeight <em>Name Similarity Weight</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ComparisonSettings#getPositionSimilarityWeight <em>Position Similarity Weight</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getComparisonSettings()
 * @model
 * @generated
 */
public interface ComparisonSettings extends EObject {
	/**
	 * Returns the value of the '<em><b>Use Id For Compare</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Id For Compare</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Id For Compare</em>' attribute.
	 * @see #setUseIdForCompare(boolean)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getComparisonSettings_UseIdForCompare()
	 * @model required="true"
	 * @generated
	 */
	boolean isUseIdForCompare();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ComparisonSettings#isUseIdForCompare <em>Use Id For Compare</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Id For Compare</em>' attribute.
	 * @see #isUseIdForCompare()
	 * @generated
	 */
	void setUseIdForCompare(boolean value);

	/**
	 * Returns the value of the '<em><b>Name Similarity Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Similarity Weight</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Similarity Weight</em>' attribute.
	 * @see #setNameSimilarityWeight(Double)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getComparisonSettings_NameSimilarityWeight()
	 * @model
	 * @generated
	 */
	Double getNameSimilarityWeight();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ComparisonSettings#getNameSimilarityWeight <em>Name Similarity Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Similarity Weight</em>' attribute.
	 * @see #getNameSimilarityWeight()
	 * @generated
	 */
	void setNameSimilarityWeight(Double value);

	/**
	 * Returns the value of the '<em><b>Position Similarity Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position Similarity Weight</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position Similarity Weight</em>' attribute.
	 * @see #setPositionSimilarityWeight(Double)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getComparisonSettings_PositionSimilarityWeight()
	 * @model
	 * @generated
	 */
	Double getPositionSimilarityWeight();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ComparisonSettings#getPositionSimilarityWeight <em>Position Similarity Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position Similarity Weight</em>' attribute.
	 * @see #getPositionSimilarityWeight()
	 * @generated
	 */
	void setPositionSimilarityWeight(Double value);

} // ComparisonSettings
