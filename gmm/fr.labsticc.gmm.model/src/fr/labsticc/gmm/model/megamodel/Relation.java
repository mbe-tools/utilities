/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Relation#getConnectedModels <em>Connected Models</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Relation#getChainedRelations <em>Chained Relations</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Relation#getOwnedRelationPolicy <em>Owned Relation Policy</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Relation#getIntention <em>Intention</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getRelation()
 * @model abstract="true"
 * @generated
 */
public interface Relation extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Connected Models</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connected Models</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connected Models</em>' attribute list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getRelation_ConnectedModels()
	 * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group'"
	 * @generated
	 */
	FeatureMap getConnectedModels();

	/**
	 * Returns the value of the '<em><b>Chained Relations</b></em>' reference list.
	 * The list contents are of type {@link fr.labsticc.gmm.model.megamodel.Relation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chained Relations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chained Relations</em>' reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getRelation_ChainedRelations()
	 * @model
	 * @generated
	 */
	EList<Relation> getChainedRelations();

	/**
	 * Returns the value of the '<em><b>Owned Relation Policy</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link fr.labsticc.gmm.model.megamodel.RelationPolicy#getRelation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Relation Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Relation Policy</em>' containment reference.
	 * @see #setOwnedRelationPolicy(RelationPolicy)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getRelation_OwnedRelationPolicy()
	 * @see fr.labsticc.gmm.model.megamodel.RelationPolicy#getRelation
	 * @model opposite="relation" containment="true" required="true"
	 * @generated
	 */
	RelationPolicy getOwnedRelationPolicy();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.Relation#getOwnedRelationPolicy <em>Owned Relation Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owned Relation Policy</em>' containment reference.
	 * @see #getOwnedRelationPolicy()
	 * @generated
	 */
	void setOwnedRelationPolicy(RelationPolicy value);

	/**
	 * Returns the value of the '<em><b>Intention</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intention</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intention</em>' attribute.
	 * @see #setIntention(String)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getRelation_Intention()
	 * @model required="true"
	 * @generated
	 */
	String getIntention();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.Relation#getIntention <em>Intention</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Intention</em>' attribute.
	 * @see #getIntention()
	 * @generated
	 */
	void setIntention(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model urisDataType="fr.labsticc.gmm.model.megamodel.URI" urisMany="true"
	 * @generated
	 */
	String getDisplayName(EList<URI> uris);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" modelElementRequired="true"
	 * @generated
	 */
	boolean concerns(EObject modelElement);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" resourceUriDataType="fr.labsticc.gmm.model.megamodel.URI" resourceUriRequired="true"
	 * @generated
	 */
	boolean concerns(URI resourceUri);

} // Relation
