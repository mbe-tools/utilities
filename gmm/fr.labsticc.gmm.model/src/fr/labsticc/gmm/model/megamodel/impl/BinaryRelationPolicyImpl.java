/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import org.eclipse.emf.ecore.EClass;

import fr.labsticc.gmm.model.megamodel.BinaryRelationPolicy;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Relation Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class BinaryRelationPolicyImpl extends RelationPolicyImpl implements BinaryRelationPolicy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryRelationPolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.BINARY_RELATION_POLICY;
	}

} //BinaryRelationPolicyImpl
