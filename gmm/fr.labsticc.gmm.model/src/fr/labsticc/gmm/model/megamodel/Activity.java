/**
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Activity#getRequiredSubsets <em>Required Subsets</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Activity#getSupportingTools <em>Supporting Tools</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getActivity()
 * @model
 * @generated
 */
public interface Activity extends NamedElement {

	/**
	 * Returns the value of the '<em><b>Required Subsets</b></em>' reference list.
	 * The list contents are of type {@link fr.labsticc.gmm.model.megamodel.MetaModelSubset}.
	 * It is bidirectional and its opposite is '{@link fr.labsticc.gmm.model.megamodel.MetaModelSubset#getActivities <em>Activities</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Subsets</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Subsets</em>' reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getActivity_RequiredSubsets()
	 * @see fr.labsticc.gmm.model.megamodel.MetaModelSubset#getActivities
	 * @model opposite="activities"
	 * @generated
	 */
	EList<MetaModelSubset> getRequiredSubsets();

	/**
	 * Returns the value of the '<em><b>Supporting Tools</b></em>' reference list.
	 * The list contents are of type {@link fr.labsticc.gmm.model.megamodel.Tool}.
	 * It is bidirectional and its opposite is '{@link fr.labsticc.gmm.model.megamodel.Tool#getSupportedActivities <em>Supported Activities</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Supporting Tools</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Supporting Tools</em>' reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getActivity_SupportingTools()
	 * @see fr.labsticc.gmm.model.megamodel.Tool#getSupportedActivities
	 * @model opposite="supportedActivities"
	 * @generated
	 */
	EList<Tool> getSupportingTools();

} // Activity
