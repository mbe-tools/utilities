/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel.impl;

import fr.labsticc.framework.core.exception.FunctionalException;
import fr.labsticc.framework.core.exception.SystemException;
import fr.labsticc.gmm.model.megamodel.EnablementType;
import fr.labsticc.gmm.model.megamodel.ExecutionContext;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.Model;
import fr.labsticc.gmm.model.megamodel.ModelElementsCoveragePolicy;
import fr.labsticc.gmm.model.megamodel.ModelingEnvironment;
import fr.labsticc.gmm.model.megamodel.ObligationRelation;
import fr.labsticc.gmm.model.megamodel.ValidityStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operational Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ObligationRelationImpl#getOwnedCoveragePolicy <em>Owned Coverage Policy</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ObligationRelationImpl#getEnablementType <em>Enablement Type</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ObligationRelationImpl#isDeleting <em>Deleting</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ObligationRelationImpl#getValidityStatus <em>Validity Status</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ObligationRelationImpl extends RelationImpl implements ObligationRelation {
	/**
	 * The cached value of the '{@link #getOwnedCoveragePolicy() <em>Owned Coverage Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedCoveragePolicy()
	 * @generated
	 * @ordered
	 */
	protected ModelElementsCoveragePolicy ownedCoveragePolicy;

	/**
	 * The default value of the '{@link #getEnablementType() <em>Enablement Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnablementType()
	 * @generated
	 * @ordered
	 */
	protected static final EnablementType ENABLEMENT_TYPE_EDEFAULT = EnablementType.NO_ERROR;
	/**
	 * The cached value of the '{@link #getEnablementType() <em>Enablement Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnablementType()
	 * @generated
	 * @ordered
	 */
	protected EnablementType enablementType = ENABLEMENT_TYPE_EDEFAULT;
	/**
	 * The default value of the '{@link #isDeleting() <em>Deleting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeleting()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DELETING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDeleting() <em>Deleting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeleting()
	 * @generated
	 * @ordered
	 */
	protected boolean deleting = DELETING_EDEFAULT;
	/**
	 * The default value of the '{@link #getValidityStatus() <em>Validity Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidityStatus()
	 * @generated
	 * @ordered
	 */
	protected static final ValidityStatus VALIDITY_STATUS_EDEFAULT = ValidityStatus.VALID;

	/**
	 * The cached value of the '{@link #getValidityStatus() <em>Validity Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidityStatus()
	 * @generated
	 * @ordered
	 */
	protected ValidityStatus validityStatus = VALIDITY_STATUS_EDEFAULT;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObligationRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.OBLIGATION_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelElementsCoveragePolicy getOwnedCoveragePolicy() {
		return ownedCoveragePolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOwnedCoveragePolicy(ModelElementsCoveragePolicy newOwnedCoveragePolicy, NotificationChain msgs) {
		ModelElementsCoveragePolicy oldOwnedCoveragePolicy = ownedCoveragePolicy;
		ownedCoveragePolicy = newOwnedCoveragePolicy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MegamodelPackage.OBLIGATION_RELATION__OWNED_COVERAGE_POLICY, oldOwnedCoveragePolicy, newOwnedCoveragePolicy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOwnedCoveragePolicy(ModelElementsCoveragePolicy newOwnedCoveragePolicy) {
		if (newOwnedCoveragePolicy != ownedCoveragePolicy) {
			NotificationChain msgs = null;
			if (ownedCoveragePolicy != null)
				msgs = ((InternalEObject)ownedCoveragePolicy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MegamodelPackage.OBLIGATION_RELATION__OWNED_COVERAGE_POLICY, null, msgs);
			if (newOwnedCoveragePolicy != null)
				msgs = ((InternalEObject)newOwnedCoveragePolicy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MegamodelPackage.OBLIGATION_RELATION__OWNED_COVERAGE_POLICY, null, msgs);
			msgs = basicSetOwnedCoveragePolicy(newOwnedCoveragePolicy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.OBLIGATION_RELATION__OWNED_COVERAGE_POLICY, newOwnedCoveragePolicy, newOwnedCoveragePolicy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnablementType getEnablementType() {
		return enablementType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnablementType(EnablementType newEnablementType) {
		EnablementType oldEnablementType = enablementType;
		enablementType = newEnablementType == null ? ENABLEMENT_TYPE_EDEFAULT : newEnablementType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.OBLIGATION_RELATION__ENABLEMENT_TYPE, oldEnablementType, enablementType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDeleting() {
		return deleting;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeleting(boolean newDeleting) {
		boolean oldDeleting = deleting;
		deleting = newDeleting;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.OBLIGATION_RELATION__DELETING, oldDeleting, deleting));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValidityStatus getValidityStatus() {
		return validityStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValidityStatus(ValidityStatus newValidityStatus) {
		ValidityStatus oldValidityStatus = validityStatus;
		validityStatus = newValidityStatus == null ? VALIDITY_STATUS_EDEFAULT : newValidityStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.OBLIGATION_RELATION__VALIDITY_STATUS, oldValidityStatus, validityStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Model> establishValidity(ModelingEnvironment modelingEnvironment, ExecutionContext executionContext) throws FunctionalException, SystemException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isEnabled(	final Model p_model,
								final ExecutionContext p_execContext ) {
		switch ( getEnablementType() ) {
			case ALWAYS:
				return true;
			case MANUAL_ONLY:
				return p_execContext.isManualLaunch();
			case MANUAL_ONLY_NO_ERROR:
				return p_execContext.isManualLaunch() && !hasAnyError( p_model );
			case NEVER:
				return false;
			case NO_ERROR:
				return !hasAnyError( p_model );
			default:
				throw new IllegalArgumentException( "Unknown enablement type: " + getEnablementType() + "." );
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean canDelete(Model model) {
		return isDeleting();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.OBLIGATION_RELATION__OWNED_COVERAGE_POLICY:
				return basicSetOwnedCoveragePolicy(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.OBLIGATION_RELATION__OWNED_COVERAGE_POLICY:
				return getOwnedCoveragePolicy();
			case MegamodelPackage.OBLIGATION_RELATION__ENABLEMENT_TYPE:
				return getEnablementType();
			case MegamodelPackage.OBLIGATION_RELATION__DELETING:
				return isDeleting();
			case MegamodelPackage.OBLIGATION_RELATION__VALIDITY_STATUS:
				return getValidityStatus();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.OBLIGATION_RELATION__OWNED_COVERAGE_POLICY:
				setOwnedCoveragePolicy((ModelElementsCoveragePolicy)newValue);
				return;
			case MegamodelPackage.OBLIGATION_RELATION__ENABLEMENT_TYPE:
				setEnablementType((EnablementType)newValue);
				return;
			case MegamodelPackage.OBLIGATION_RELATION__DELETING:
				setDeleting((Boolean)newValue);
				return;
			case MegamodelPackage.OBLIGATION_RELATION__VALIDITY_STATUS:
				setValidityStatus((ValidityStatus)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.OBLIGATION_RELATION__OWNED_COVERAGE_POLICY:
				setOwnedCoveragePolicy((ModelElementsCoveragePolicy)null);
				return;
			case MegamodelPackage.OBLIGATION_RELATION__ENABLEMENT_TYPE:
				setEnablementType(ENABLEMENT_TYPE_EDEFAULT);
				return;
			case MegamodelPackage.OBLIGATION_RELATION__DELETING:
				setDeleting(DELETING_EDEFAULT);
				return;
			case MegamodelPackage.OBLIGATION_RELATION__VALIDITY_STATUS:
				setValidityStatus(VALIDITY_STATUS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.OBLIGATION_RELATION__OWNED_COVERAGE_POLICY:
				return ownedCoveragePolicy != null;
			case MegamodelPackage.OBLIGATION_RELATION__ENABLEMENT_TYPE:
				return enablementType != ENABLEMENT_TYPE_EDEFAULT;
			case MegamodelPackage.OBLIGATION_RELATION__DELETING:
				return deleting != DELETING_EDEFAULT;
			case MegamodelPackage.OBLIGATION_RELATION__VALIDITY_STATUS:
				return validityStatus != VALIDITY_STATUS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (enablementType: ");
		result.append(enablementType);
		result.append(", deleting: ");
		result.append(deleting);
		result.append(", validityStatus: ");
		result.append(validityStatus);
		result.append(')');
		return result.toString();
	}

} //OperationalRelationImpl
