/**
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.Tool#getSupportedActivities <em>Supported Activities</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getTool()
 * @model
 * @generated
 */
public interface Tool extends Model {
	/**
	 * Returns the value of the '<em><b>Supported Activities</b></em>' reference list.
	 * The list contents are of type {@link fr.labsticc.gmm.model.megamodel.Activity}.
	 * It is bidirectional and its opposite is '{@link fr.labsticc.gmm.model.megamodel.Activity#getSupportingTools <em>Supporting Tools</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Supported Activities</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Supported Activities</em>' reference list.
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getTool_SupportedActivities()
	 * @see fr.labsticc.gmm.model.megamodel.Activity#getSupportingTools
	 * @model opposite="supportingTools" required="true"
	 * @generated
	 */
	EList<Activity> getSupportedActivities();

} // Tool
