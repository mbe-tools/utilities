/**
 */
package fr.labsticc.gmm.model.megamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relation Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.RelationPolicy#getRelation <em>Relation</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getRelationPolicy()
 * @model abstract="true"
 * @generated
 */
public interface RelationPolicy extends NamedElement {

	/**
	 * Returns the value of the '<em><b>Relation</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.labsticc.gmm.model.megamodel.Relation#getOwnedRelationPolicy <em>Owned Relation Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relation</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relation</em>' container reference.
	 * @see #setRelation(Relation)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getRelationPolicy_Relation()
	 * @see fr.labsticc.gmm.model.megamodel.Relation#getOwnedRelationPolicy
	 * @model opposite="ownedRelationPolicy" required="true" transient="false"
	 * @generated
	 */
	Relation getRelation();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.RelationPolicy#getRelation <em>Relation</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relation</em>' container reference.
	 * @see #getRelation()
	 * @generated
	 */
	void setRelation(Relation value);
} // RelationPolicy
