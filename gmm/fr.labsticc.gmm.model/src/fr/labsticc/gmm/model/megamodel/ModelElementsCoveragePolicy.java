/**
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Elements Coverage Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getModelElementsCoveragePolicy()
 * @model abstract="true"
 * @generated
 */
public interface ModelElementsCoveragePolicy extends NamedElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" modelElementRequired="true"
	 * @generated
	 */
	boolean isConsidered(EObject modelElement);

} // ModelElementsCoveragePolicy
