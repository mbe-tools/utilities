/**
 */
package fr.labsticc.gmm.model.megamodel;



/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Meta Model Related Consistency Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation#isDeletingLeft <em>Deleting Left</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation#isDeletingRight <em>Deleting Right</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getBinaryMetaModelRelatedConsistencyRelation()
 * @model abstract="true"
 * @generated
 */
public interface BinaryMetaModelRelatedConsistencyRelation extends ObligationRelation, BinaryMetaModeRelatedlRelation {

	/**
	 * Returns the value of the '<em><b>Deleting Left</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deleting Left</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deleting Left</em>' attribute.
	 * @see #setDeletingLeft(boolean)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getBinaryMetaModelRelatedConsistencyRelation_DeletingLeft()
	 * @model
	 * @generated
	 */
	boolean isDeletingLeft();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation#isDeletingLeft <em>Deleting Left</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deleting Left</em>' attribute.
	 * @see #isDeletingLeft()
	 * @generated
	 */
	void setDeletingLeft(boolean value);

	/**
	 * Returns the value of the '<em><b>Deleting Right</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deleting Right</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deleting Right</em>' attribute.
	 * @see #setDeletingRight(boolean)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getBinaryMetaModelRelatedConsistencyRelation_DeletingRight()
	 * @model
	 * @generated
	 */
	boolean isDeletingRight();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation#isDeletingRight <em>Deleting Right</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deleting Right</em>' attribute.
	 * @see #isDeletingRight()
	 * @generated
	 */
	void setDeletingRight(boolean value);

} // BinaryMetaModelRelatedConsistencyRelation
