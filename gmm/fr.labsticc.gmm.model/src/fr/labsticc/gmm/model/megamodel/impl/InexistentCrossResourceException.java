package fr.labsticc.gmm.model.megamodel.impl;

import org.eclipse.emf.common.util.URI;

import fr.labsticc.framework.core.exception.FunctionalException;

public class InexistentCrossResourceException extends FunctionalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6306220747539340695L;
	
	final URI inexistentResUri;

	public InexistentCrossResourceException( final URI p_uri ) {
		inexistentResUri = p_uri;
	}
	
	public URI getInexistentResUri() {
		return inexistentResUri;
	}
}
