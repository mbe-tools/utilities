/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation;
import fr.labsticc.gmm.model.megamodel.SubsettedMetaModel;
import fr.labsticc.gmm.model.megamodel.UriRelatedBinaryRelPolicy;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Uri Related Binary Rel Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class UriRelatedBinaryRelPolicyImpl extends BinaryRelationPolicyImpl implements UriRelatedBinaryRelPolicy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UriRelatedBinaryRelPolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.URI_RELATED_BINARY_REL_POLICY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean areRelated(final URI leftUri, final URI rightUri) {
		if ( leftUri == null ||  rightUri == null ) {
			return false;
		}
		
		return leftUri.trimFileExtension().equals( rightUri.trimFileExtension() );
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public URI correspondingUri(final URI uri) {
		if ( uri == null ) {
			return  null;
		}
				
		final MetaModelRelatedRelation parentRelation = (MetaModelRelatedRelation) getRelation();
				
		for ( final MetaModel metaModel : parentRelation.getMetaModelsList() ) {
			if ( !metaModel.declares( uri ) ) {
				
				// Trim twice in case of subset
				URI corrUri = uri.trimFileExtension();
				
				if ( metaModel instanceof SubsettedMetaModel ) {
					corrUri = corrUri.trimFileExtension().appendFileExtension( ( (SubsettedMetaModel) metaModel ).getSubsetExtensions().get( 0 ) );
				}
				
				return corrUri.appendFileExtension( metaModel.getFileExtensions().get( 0 ) );
			}
		}
				
		return null;
	}

} //UriRelatedBinaryRelPolicyImpl
