/**
 */
package fr.labsticc.gmm.model.megamodel.impl;

import fr.labsticc.gmm.model.megamodel.Activity;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModelSubset;

import fr.labsticc.gmm.model.megamodel.Tool;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ActivityImpl#getRequiredSubsets <em>Required Subsets</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.ActivityImpl#getSupportingTools <em>Supporting Tools</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActivityImpl extends NamedElementImpl implements Activity {
	/**
	 * The cached value of the '{@link #getRequiredSubsets() <em>Required Subsets</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredSubsets()
	 * @generated
	 * @ordered
	 */
	protected EList<MetaModelSubset> requiredSubsets;

	/**
	 * The cached value of the '{@link #getSupportingTools() <em>Supporting Tools</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSupportingTools()
	 * @generated
	 * @ordered
	 */
	protected EList<Tool> supportingTools;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.ACTIVITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MetaModelSubset> getRequiredSubsets() {
		if (requiredSubsets == null) {
			requiredSubsets = new EObjectWithInverseResolvingEList.ManyInverse<MetaModelSubset>(MetaModelSubset.class, this, MegamodelPackage.ACTIVITY__REQUIRED_SUBSETS, MegamodelPackage.META_MODEL_SUBSET__ACTIVITIES);
		}
		return requiredSubsets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Tool> getSupportingTools() {
		if (supportingTools == null) {
			supportingTools = new EObjectWithInverseResolvingEList.ManyInverse<Tool>(Tool.class, this, MegamodelPackage.ACTIVITY__SUPPORTING_TOOLS, MegamodelPackage.TOOL__SUPPORTED_ACTIVITIES);
		}
		return supportingTools;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.ACTIVITY__REQUIRED_SUBSETS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRequiredSubsets()).basicAdd(otherEnd, msgs);
			case MegamodelPackage.ACTIVITY__SUPPORTING_TOOLS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSupportingTools()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.ACTIVITY__REQUIRED_SUBSETS:
				return ((InternalEList<?>)getRequiredSubsets()).basicRemove(otherEnd, msgs);
			case MegamodelPackage.ACTIVITY__SUPPORTING_TOOLS:
				return ((InternalEList<?>)getSupportingTools()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.ACTIVITY__REQUIRED_SUBSETS:
				return getRequiredSubsets();
			case MegamodelPackage.ACTIVITY__SUPPORTING_TOOLS:
				return getSupportingTools();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.ACTIVITY__REQUIRED_SUBSETS:
				getRequiredSubsets().clear();
				getRequiredSubsets().addAll((Collection<? extends MetaModelSubset>)newValue);
				return;
			case MegamodelPackage.ACTIVITY__SUPPORTING_TOOLS:
				getSupportingTools().clear();
				getSupportingTools().addAll((Collection<? extends Tool>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.ACTIVITY__REQUIRED_SUBSETS:
				getRequiredSubsets().clear();
				return;
			case MegamodelPackage.ACTIVITY__SUPPORTING_TOOLS:
				getSupportingTools().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.ACTIVITY__REQUIRED_SUBSETS:
				return requiredSubsets != null && !requiredSubsets.isEmpty();
			case MegamodelPackage.ACTIVITY__SUPPORTING_TOOLS:
				return supportingTools != null && !supportingTools.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ActivityImpl
