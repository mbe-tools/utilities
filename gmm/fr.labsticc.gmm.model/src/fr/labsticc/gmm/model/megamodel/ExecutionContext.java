/**
 */
package fr.labsticc.gmm.model.megamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Execution Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ExecutionContext#isManualLaunch <em>Manual Launch</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ExecutionContext#getSourceModelOperationType <em>Source Model Operation Type</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.ExecutionContext#getPreviousRelation <em>Previous Relation</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getExecutionContext()
 * @model
 * @generated
 */
public interface ExecutionContext extends EObject {
	/**
	 * Returns the value of the '<em><b>Manual Launch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Manual Launch</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Manual Launch</em>' attribute.
	 * @see #setManualLaunch(boolean)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getExecutionContext_ManualLaunch()
	 * @model
	 * @generated
	 */
	boolean isManualLaunch();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ExecutionContext#isManualLaunch <em>Manual Launch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Manual Launch</em>' attribute.
	 * @see #isManualLaunch()
	 * @generated
	 */
	void setManualLaunch(boolean value);

	/**
	 * Returns the value of the '<em><b>Source Model Operation Type</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.labsticc.gmm.model.megamodel.OperationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Model Operation Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Model Operation Type</em>' attribute.
	 * @see fr.labsticc.gmm.model.megamodel.OperationType
	 * @see #setSourceModelOperationType(OperationType)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getExecutionContext_SourceModelOperationType()
	 * @model required="true"
	 * @generated
	 */
	OperationType getSourceModelOperationType();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ExecutionContext#getSourceModelOperationType <em>Source Model Operation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Model Operation Type</em>' attribute.
	 * @see fr.labsticc.gmm.model.megamodel.OperationType
	 * @see #getSourceModelOperationType()
	 * @generated
	 */
	void setSourceModelOperationType(OperationType value);

	/**
	 * Returns the value of the '<em><b>Previous Relation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Previous Relation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Previous Relation</em>' reference.
	 * @see #setPreviousRelation(Relation)
	 * @see fr.labsticc.gmm.model.megamodel.MegamodelPackage#getExecutionContext_PreviousRelation()
	 * @model
	 * @generated
	 */
	Relation getPreviousRelation();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.model.megamodel.ExecutionContext#getPreviousRelation <em>Previous Relation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Previous Relation</em>' reference.
	 * @see #getPreviousRelation()
	 * @generated
	 */
	void setPreviousRelation(Relation value);

} // ExecutionContext
