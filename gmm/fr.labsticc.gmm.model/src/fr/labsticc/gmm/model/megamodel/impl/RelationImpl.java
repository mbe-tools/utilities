/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.model.megamodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.Model;
import fr.labsticc.gmm.model.megamodel.Relation;
import fr.labsticc.gmm.model.megamodel.RelationPolicy;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.RelationImpl#getConnectedModels <em>Connected Models</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.RelationImpl#getChainedRelations <em>Chained Relations</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.RelationImpl#getOwnedRelationPolicy <em>Owned Relation Policy</em>}</li>
 *   <li>{@link fr.labsticc.gmm.model.megamodel.impl.RelationImpl#getIntention <em>Intention</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class RelationImpl extends NamedElementImpl implements Relation {
	/**
	 * The cached value of the '{@link #getConnectedModels() <em>Connected Models</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectedModels()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap connectedModels;
	/**
	 * The cached value of the '{@link #getChainedRelations() <em>Chained Relations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainedRelations()
	 * @generated
	 * @ordered
	 */
	protected EList<Relation> chainedRelations;
	/**
	 * The cached value of the '{@link #getOwnedRelationPolicy() <em>Owned Relation Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedRelationPolicy()
	 * @generated
	 * @ordered
	 */
	protected RelationPolicy ownedRelationPolicy;
	/**
	 * The default value of the '{@link #getIntention() <em>Intention</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntention()
	 * @generated
	 * @ordered
	 */
	protected static final String INTENTION_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getIntention() <em>Intention</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntention()
	 * @generated
	 * @ordered
	 */
	protected String intention = INTENTION_EDEFAULT;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MegamodelPackage.Literals.RELATION;
	}

	protected boolean hasAnyError( final Model p_model ) {
		return p_model.getError() != null;
	}

	protected boolean hasErrorsOtherThanGmm( final Model p_model ) {
		if ( p_model.getError() == null ) {
			return false;
		}
	
		switch ( p_model.getError().getErrorStatus() ) {
			case GMM:
				return false;
		
			default:
				return true;
		}
	}

	protected boolean hasOnlyGmmErrors( final Model p_model ) {
		if ( p_model.getError() == null ) {
			return false;
		}
	
		switch ( p_model.getError().getErrorStatus() ) {
			case GMM:
				return true;
		
			default:
				return false;
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getConnectedModels() {
		if (connectedModels == null) {
			connectedModels = new BasicFeatureMap(this, MegamodelPackage.RELATION__CONNECTED_MODELS);
		}
		return connectedModels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Relation> getChainedRelations() {
		if (chainedRelations == null) {
			chainedRelations = new EObjectResolvingEList<Relation>(Relation.class, this, MegamodelPackage.RELATION__CHAINED_RELATIONS);
		}
		return chainedRelations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationPolicy getOwnedRelationPolicy() {
		return ownedRelationPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOwnedRelationPolicy(RelationPolicy newOwnedRelationPolicy, NotificationChain msgs) {
		RelationPolicy oldOwnedRelationPolicy = ownedRelationPolicy;
		ownedRelationPolicy = newOwnedRelationPolicy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MegamodelPackage.RELATION__OWNED_RELATION_POLICY, oldOwnedRelationPolicy, newOwnedRelationPolicy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOwnedRelationPolicy(RelationPolicy newOwnedRelationPolicy) {
		if (newOwnedRelationPolicy != ownedRelationPolicy) {
			NotificationChain msgs = null;
			if (ownedRelationPolicy != null)
				msgs = ((InternalEObject)ownedRelationPolicy).eInverseRemove(this, MegamodelPackage.RELATION_POLICY__RELATION, RelationPolicy.class, msgs);
			if (newOwnedRelationPolicy != null)
				msgs = ((InternalEObject)newOwnedRelationPolicy).eInverseAdd(this, MegamodelPackage.RELATION_POLICY__RELATION, RelationPolicy.class, msgs);
			msgs = basicSetOwnedRelationPolicy(newOwnedRelationPolicy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.RELATION__OWNED_RELATION_POLICY, newOwnedRelationPolicy, newOwnedRelationPolicy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIntention() {
		return intention;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntention(String newIntention) {
		String oldIntention = intention;
		intention = newIntention;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MegamodelPackage.RELATION__INTENTION, oldIntention, intention));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getDisplayName(EList<URI> uris) {
		return getDisplayName();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean concerns(EObject modelElement) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean concerns(URI resourceUri) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.RELATION__OWNED_RELATION_POLICY:
				if (ownedRelationPolicy != null)
					msgs = ((InternalEObject)ownedRelationPolicy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MegamodelPackage.RELATION__OWNED_RELATION_POLICY, null, msgs);
				return basicSetOwnedRelationPolicy((RelationPolicy)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MegamodelPackage.RELATION__CONNECTED_MODELS:
				return ((InternalEList<?>)getConnectedModels()).basicRemove(otherEnd, msgs);
			case MegamodelPackage.RELATION__OWNED_RELATION_POLICY:
				return basicSetOwnedRelationPolicy(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MegamodelPackage.RELATION__CONNECTED_MODELS:
				if (coreType) return getConnectedModels();
				return ((FeatureMap.Internal)getConnectedModels()).getWrapper();
			case MegamodelPackage.RELATION__CHAINED_RELATIONS:
				return getChainedRelations();
			case MegamodelPackage.RELATION__OWNED_RELATION_POLICY:
				return getOwnedRelationPolicy();
			case MegamodelPackage.RELATION__INTENTION:
				return getIntention();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MegamodelPackage.RELATION__CONNECTED_MODELS:
				((FeatureMap.Internal)getConnectedModels()).set(newValue);
				return;
			case MegamodelPackage.RELATION__CHAINED_RELATIONS:
				getChainedRelations().clear();
				getChainedRelations().addAll((Collection<? extends Relation>)newValue);
				return;
			case MegamodelPackage.RELATION__OWNED_RELATION_POLICY:
				setOwnedRelationPolicy((RelationPolicy)newValue);
				return;
			case MegamodelPackage.RELATION__INTENTION:
				setIntention((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MegamodelPackage.RELATION__CONNECTED_MODELS:
				getConnectedModels().clear();
				return;
			case MegamodelPackage.RELATION__CHAINED_RELATIONS:
				getChainedRelations().clear();
				return;
			case MegamodelPackage.RELATION__OWNED_RELATION_POLICY:
				setOwnedRelationPolicy((RelationPolicy)null);
				return;
			case MegamodelPackage.RELATION__INTENTION:
				setIntention(INTENTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MegamodelPackage.RELATION__CONNECTED_MODELS:
				return connectedModels != null && !connectedModels.isEmpty();
			case MegamodelPackage.RELATION__CHAINED_RELATIONS:
				return chainedRelations != null && !chainedRelations.isEmpty();
			case MegamodelPackage.RELATION__OWNED_RELATION_POLICY:
				return ownedRelationPolicy != null;
			case MegamodelPackage.RELATION__INTENTION:
				return INTENTION_EDEFAULT == null ? intention != null : !INTENTION_EDEFAULT.equals(intention);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (connectedModels: ");
		result.append(connectedModels);
		result.append(", intention: ");
		result.append(intention);
		result.append(')');
		return result.toString();
	}

} //RelationImpl
