/**
 */
package fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.XtexteditoradapterPackage
 * @generated
 */
public interface XtexteditoradapterFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	XtexteditoradapterFactory eINSTANCE = fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.impl.XtexteditoradapterFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Xtext Editor Adapter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Xtext Editor Adapter</em>'.
	 * @generated
	 */
	XtextEditorAdapter createXtextEditorAdapter();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	XtexteditoradapterPackage getXtexteditoradapterPackage();

} //XtexteditoradapterFactory
