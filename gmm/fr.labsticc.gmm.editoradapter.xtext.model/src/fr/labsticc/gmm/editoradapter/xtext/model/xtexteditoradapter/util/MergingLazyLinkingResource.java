package fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.util;

/**
 * @author dblouin
 * Tentative to create a lazy linking xtext resource that maintains the same instances objects in the resource.
 * Finally found a simpler solution implemented in the editor adapter.
 */
public class MergingLazyLinkingResource /*extends LazyLinkingResource*/ {

	public MergingLazyLinkingResource() {
		super();
	}

	/**
	 * @param oldParseResult the previous parse result that should be detached if necessary.
	 * @param newParseResult the current parse result that should be attached to the content of this resource
	 * @since 2.1
	 */
//	@Override
//	protected void updateInternalState(	final IParseResult oldParseResult,
//										final IParseResult newParseResult ) {
//		if ( 	oldParseResult != null && oldParseResult.getRootASTElement() != null &&
//				oldParseResult.getRootASTElement() != newParseResult.getRootASTElement() ) {
//			final EObject oldRootAstElement = oldParseResult.getRootASTElement();
//			final EObject newRootAstElement = newParseResult.getRootASTElement();
//				
//			merge( newRootAstElement, oldRootAstElement );
//			( (ParseResult) newParseResult ).setRootASTElement( oldRootAstElement );
//		}
//		
//		updateInternalState(newParseResult);
//	}

//	@Override
//	protected void updateInternalState( final IParseResult p_newParseResult ) {
//		setParseResult( p_newParseResult );
//		final EObject newRoot =  p_newParseResult.getRootASTElement();
//		
//		if ( newRoot != null && !getContents().contains( newRoot ) ) {
//			getContents().add(0, newRoot );
//			reattachModificationTracker( newRoot );
//			clearErrorsAndWarnings();
//			addSyntaxErrors();
//
//			// Only doLinking because it will reset the object tree references.
//			doLinking();
//		}
//		else {
//			reattachModificationTracker( newRoot );
//			clearErrorsAndWarnings();
//			addSyntaxErrors();
//		}
//	}
//	
//	private void merge( final EObject p_newRoot,
//						final EObject p_oldRoot ) {
//		try {
//			// Matching model elements
//			final MatchModel match = MatchService.doMatch( p_newRoot, p_oldRoot, Collections.<String, Object> emptyMap()) ;
//			// Computing differences
//			final DiffModel diff = DiffService.doDiff( match, false );
//			// Merges all differences from model1 to model2
//			final List<DiffElement> differences = new ArrayList<DiffElement>( diff.getOwnedElements() );
//			MergeService.merge( differences, true );
//			
//			// Merging adapters is essential for the validation that uses the ast nodes stored in the adapters.
//			mergeAdapters( p_newRoot, p_oldRoot, match );
//		}
//		catch( final InterruptedException p_ex ) {
//		}
//	}
//	
//	private void mergeAdapters( final EObject p_newRoot,
//								final EObject p_oldRoot,
//								final MatchModel p_matchModel ) {
//		for ( final Adapter newAdapter : p_newRoot.eAdapters() ) {
//			final Class<?> newAdapterClass = newAdapter.getClass();
//			
//			// TODO: Verify if can assume one adapter per class...
//			for ( final Adapter oldAdapter : new ArrayList<Adapter>( p_oldRoot.eAdapters() ) ) {
//				if ( newAdapterClass == oldAdapter.getClass() ) {
//					p_oldRoot.eAdapters().remove( oldAdapter );
//					p_oldRoot.eAdapters().add( newAdapter );
//					
//					break;
//				}
//			}
//		}
//		
//		for ( final EObject child : p_newRoot.eContents() ) {
//			final EObject matchedEleme = findRightMatchedElement( child, p_matchModel );
//			
//			if ( matchedEleme != null ) {
//				mergeAdapters( child, matchedEleme, p_matchModel );
//			}
//		}
//	}
//	
//	private EObject findRightMatchedElement( 	final EObject p_element,
//												final MatchModel p_matchModel )	{
//		for ( final MatchElement matchElement : p_matchModel.getMatchedElements() ) {
//			final EObject matchedElement = findRightMatchedElement( p_element, matchElement );
//			
//			if ( matchedElement != null ) {
//				return matchedElement;
//			}
//		}
//		
//		return null;
//	}
//
//	
//	private EObject findRightMatchedElement( 	final EObject p_element,
//												final MatchElement p_matchElement )	{
//		if ( p_matchElement instanceof Match2Elements ) {
//			final Match2Elements match2elem = (Match2Elements) p_matchElement;
//			
//			if ( p_element == match2elem.getLeftElement() ) {
//				return match2elem.getRightElement();
//			}
//		}
//		
//		for ( final MatchElement subMatchElement : p_matchElement.getSubMatchElements() ) {
//			final EObject matchedElement = findRightMatchedElement( p_element, subMatchElement );
//			
//			if ( matchedElement != null ) {
//				return matchedElement;
//			}
//		}
//		
//		return null;
//	}
}
