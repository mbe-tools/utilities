/**
 */
package fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.impl;

import fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class XtexteditoradapterFactoryImpl extends EFactoryImpl implements XtexteditoradapterFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static XtexteditoradapterFactory init() {
		try {
			XtexteditoradapterFactory theXtexteditoradapterFactory = (XtexteditoradapterFactory)EPackage.Registry.INSTANCE.getEFactory(XtexteditoradapterPackage.eNS_URI);
			if (theXtexteditoradapterFactory != null) {
				return theXtexteditoradapterFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new XtexteditoradapterFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XtexteditoradapterFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case XtexteditoradapterPackage.XTEXT_EDITOR_ADAPTER: return createXtextEditorAdapter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XtextEditorAdapter createXtextEditorAdapter() {
		XtextEditorAdapterImpl xtextEditorAdapter = new XtextEditorAdapterImpl();
		return xtextEditorAdapter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XtexteditoradapterPackage getXtexteditoradapterPackage() {
		return (XtexteditoradapterPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static XtexteditoradapterPackage getPackage() {
		return XtexteditoradapterPackage.eINSTANCE;
	}

} //XtexteditoradapterFactoryImpl
