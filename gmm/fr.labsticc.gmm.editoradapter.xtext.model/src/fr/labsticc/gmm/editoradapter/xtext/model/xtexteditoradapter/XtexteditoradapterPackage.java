/**
 */
package fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.XtexteditoradapterFactory
 * @model kind="package"
 * @generated
 */
public interface XtexteditoradapterPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "xtexteditoradapter";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.labsticc.fr/xtexteditoradapter/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "xtexteditoradapter";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	XtexteditoradapterPackage eINSTANCE = fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.impl.XtexteditoradapterPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.impl.XtextEditorAdapterImpl <em>Xtext Editor Adapter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.impl.XtextEditorAdapterImpl
	 * @see fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.impl.XtexteditoradapterPackageImpl#getXtextEditorAdapter()
	 * @generated
	 */
	int XTEXT_EDITOR_ADAPTER = 0;

	/**
	 * The number of structural features of the '<em>Xtext Editor Adapter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTEXT_EDITOR_ADAPTER_FEATURE_COUNT = EditoradapterPackage.EDITING_DOMAIN_PROVIDER_EDITOR_ADAPTER_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.XtextEditorAdapter <em>Xtext Editor Adapter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Xtext Editor Adapter</em>'.
	 * @see fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.XtextEditorAdapter
	 * @generated
	 */
	EClass getXtextEditorAdapter();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	XtexteditoradapterFactory getXtexteditoradapterFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.impl.XtextEditorAdapterImpl <em>Xtext Editor Adapter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.impl.XtextEditorAdapterImpl
		 * @see fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.impl.XtexteditoradapterPackageImpl#getXtextEditorAdapter()
		 * @generated
		 */
		EClass XTEXT_EDITOR_ADAPTER = eINSTANCE.getXtextEditorAdapter();

	}

} //XtexteditoradapterPackage
