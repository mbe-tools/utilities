/**
 */
package fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter;

import fr.labsticc.gmm.editoradapter.model.editoradapter.EditingDomainProviderEditorAdapter;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Xtext Editor Adapter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.XtexteditoradapterPackage#getXtextEditorAdapter()
 * @model
 * @generated
 */
public interface XtextEditorAdapter extends EditingDomainProviderEditorAdapter {
} // XtextEditorAdapter
