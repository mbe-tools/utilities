/**
 */
package fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage;
import fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.XtextEditorAdapter;
import fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.XtexteditoradapterFactory;
import fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.XtexteditoradapterPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class XtexteditoradapterPackageImpl extends EPackageImpl implements XtexteditoradapterPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xtextEditorAdapterEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.XtexteditoradapterPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private XtexteditoradapterPackageImpl() {
		super(eNS_URI, XtexteditoradapterFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link XtexteditoradapterPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static XtexteditoradapterPackage init() {
		if (isInited) return (XtexteditoradapterPackage)EPackage.Registry.INSTANCE.getEPackage(XtexteditoradapterPackage.eNS_URI);

		// Obtain or create and register package
		XtexteditoradapterPackageImpl theXtexteditoradapterPackage = (XtexteditoradapterPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof XtexteditoradapterPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new XtexteditoradapterPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EditoradapterPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theXtexteditoradapterPackage.createPackageContents();

		// Initialize created meta-data
		theXtexteditoradapterPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theXtexteditoradapterPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(XtexteditoradapterPackage.eNS_URI, theXtexteditoradapterPackage);
		return theXtexteditoradapterPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXtextEditorAdapter() {
		return xtextEditorAdapterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XtexteditoradapterFactory getXtexteditoradapterFactory() {
		return (XtexteditoradapterFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		xtextEditorAdapterEClass = createEClass(XTEXT_EDITOR_ADAPTER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EditoradapterPackage theEditoradapterPackage = (EditoradapterPackage)EPackage.Registry.INSTANCE.getEPackage(EditoradapterPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		xtextEditorAdapterEClass.getESuperTypes().add(theEditoradapterPackage.getEditingDomainProviderEditorAdapter());

		// Initialize classes and features; add operations and parameters
		initEClass(xtextEditorAdapterEClass, XtextEditorAdapter.class, "XtextEditorAdapter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //XtexteditoradapterPackageImpl
