/**
 */
package fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.impl;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.xtext.resource.DefaultLocationInFileProvider;
import org.eclipse.xtext.resource.ILocationInFileProvider;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.model.XtextDocument;
import org.eclipse.xtext.util.ITextRegion;
import org.eclipse.xtext.util.ITextRegionWithLineInformation;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

import fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditingDomainProviderEditorAdapterImpl;
import fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.XtextEditorAdapter;
import fr.labsticc.gmm.editoradapter.xtext.model.xtexteditoradapter.XtexteditoradapterPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Xtext Editor Adapter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class XtextEditorAdapterImpl extends EditingDomainProviderEditorAdapterImpl implements XtextEditorAdapter {

	private final ILocationInFileProvider locationProvider;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected XtextEditorAdapterImpl() {
		super();
		
		locationProvider = new DefaultLocationInFileProvider();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XtexteditoradapterPackage.Literals.XTEXT_EDITOR_ADAPTER;
	}

	@Override
	protected Resource getResource( final URI p_resUri,
									final IEditorPart p_editor,
									final boolean pb_modify ) {
		if ( p_editor instanceof XtextEditor ) {
			final IXtextDocument document = ( (XtextEditor) p_editor ).getDocument();
			final ResourceReferenceExtractorUnitOfWork extractor = new ResourceReferenceExtractorUnitOfWork(); 
			document.readOnly( extractor );

			return extractor.getResource();
		}
		
		return super.getResource( p_resUri, p_editor, pb_modify );
	}
	
	private static class ResourceReferenceExtractorUnitOfWork extends IUnitOfWork.Void<XtextResource> {

		private Resource resource;

		public ResourceReferenceExtractorUnitOfWork() {
		}

		@Override
		public void process(XtextResource state) throws Exception {
			resource = state;
		}
		
		public Resource getResource() {
			return resource;
		}
	}
	
	@Override
	protected boolean save( final URI p_uri,
							final IEditorPart p_editorPart ) {
        if ( p_editorPart instanceof XtextEditor ) {
	    	final IXtextDocument document = ( (XtextEditor) p_editorPart ).getDocument();
	    	Assert.isLegal(document instanceof XtextDocument);
	    	final ResourceReferenceExtractorUnitOfWork extractor = new ResourceReferenceExtractorUnitOfWork(); 
	    	document.readOnly( extractor );
	    	final XtextResource resource = (XtextResource) extractor.getResource();
	    	
//	    	try {
//	    		resource.save( null );
//	    	}
//	    	catch( final IOException p_ex ) {
//	    		p_ex.printStackTrace();
//	    	}
	
	    	Display.getDefault().asyncExec( new Runnable() {
				
				@Override
				public void run() {
					document.set( resource.getSerializer().serialize( resource.getContents().get( 0 ) ) );
					p_editorPart.doSave( null );
//	        		Cannot use init because it will re-instantiate the resource
//						p_editorPart.init( p_editorPart.getEditorSite(), p_editorPart.getEditorInput() );
				}
			});
			
			return true;
        }
        
        return super.save( p_uri, p_editorPart );
	}
	
	@Override
	public Object getMarkerElementId( final EObject p_element ) {
		final EObject editorObject = getEditorObject( p_element );
		final ITextRegion textRegion = locationProvider.getFullTextRegion( editorObject );
		
		if ( textRegion instanceof ITextRegionWithLineInformation ) {
			final ITextRegionWithLineInformation lineInfo = (ITextRegionWithLineInformation) textRegion;
			
			return ( lineInfo.getLineNumber() + lineInfo.getEndLineNumber() ) / 2 + 1;
		}
		
		return null;
	}
} //XtextEditorAdapterImpl
