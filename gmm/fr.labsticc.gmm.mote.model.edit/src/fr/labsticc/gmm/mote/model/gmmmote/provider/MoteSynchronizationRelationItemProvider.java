/**
 */
package fr.labsticc.gmm.mote.model.gmmmote.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import fr.labsticc.gmm.model.megamodel.provider.BinaryMetaModelRelatedConsistencyRelationItemProvider;
import fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage;
import fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation;

/**
 * This is the item provider adapter for a {@link fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MoteSynchronizationRelationItemProvider
	extends BinaryMetaModelRelatedConsistencyRelationItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MoteSynchronizationRelationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addEnginePropertyDescriptor(object);
			addRuleSetIdPropertyDescriptor(object);
			addExecutionTraceFileExtensionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Engine feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnginePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MoteSynchronizationRelation_engine_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MoteSynchronizationRelation_engine_feature", "_UI_MoteSynchronizationRelation_type"),
				 GmmmotePackage.Literals.MOTE_SYNCHRONIZATION_RELATION__ENGINE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Rule Set Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRuleSetIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MoteSynchronizationRelation_ruleSetId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MoteSynchronizationRelation_ruleSetId_feature", "_UI_MoteSynchronizationRelation_type"),
				 GmmmotePackage.Literals.MOTE_SYNCHRONIZATION_RELATION__RULE_SET_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Execution Trace File Extension feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExecutionTraceFileExtensionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MoteSynchronizationRelation_executionTraceFileExtension_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MoteSynchronizationRelation_executionTraceFileExtension_feature", "_UI_MoteSynchronizationRelation_type"),
				 GmmmotePackage.Literals.MOTE_SYNCHRONIZATION_RELATION__EXECUTION_TRACE_FILE_EXTENSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns MoteSynchronizationRelation.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MoteSynchronizationRelation"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((MoteSynchronizationRelation)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_MoteSynchronizationRelation_type") :
			getString("_UI_MoteSynchronizationRelation_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MoteSynchronizationRelation.class)) {
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__RULE_SET_ID:
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__EXECUTION_TRACE_FILE_EXTENSION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
