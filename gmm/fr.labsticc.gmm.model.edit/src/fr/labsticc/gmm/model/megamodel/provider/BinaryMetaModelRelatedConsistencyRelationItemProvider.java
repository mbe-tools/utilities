/**
 */
package fr.labsticc.gmm.model.megamodel.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;

/**
 * This is the item provider adapter for a {@link fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class BinaryMetaModelRelatedConsistencyRelationItemProvider
	extends ObligationRelationItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryMetaModelRelatedConsistencyRelationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addLeftMetaModelPropertyDescriptor(object);
			addRightMetaModelPropertyDescriptor(object);
			addDeletingLeftPropertyDescriptor(object);
			addDeletingRightPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Left Meta Model feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLeftMetaModelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BinaryMetaModeRelatedlRelation_leftMetaModel_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BinaryMetaModeRelatedlRelation_leftMetaModel_feature", "_UI_BinaryMetaModeRelatedlRelation_type"),
				 MegamodelPackage.Literals.BINARY_META_MODE_RELATEDL_RELATION__LEFT_META_MODEL,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Right Meta Model feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRightMetaModelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BinaryMetaModeRelatedlRelation_rightMetaModel_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BinaryMetaModeRelatedlRelation_rightMetaModel_feature", "_UI_BinaryMetaModeRelatedlRelation_type"),
				 MegamodelPackage.Literals.BINARY_META_MODE_RELATEDL_RELATION__RIGHT_META_MODEL,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Deleting Left feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDeletingLeftPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BinaryMetaModelRelatedConsistencyRelation_deletingLeft_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BinaryMetaModelRelatedConsistencyRelation_deletingLeft_feature", "_UI_BinaryMetaModelRelatedConsistencyRelation_type"),
				 MegamodelPackage.Literals.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_LEFT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Deleting Right feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDeletingRightPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BinaryMetaModelRelatedConsistencyRelation_deletingRight_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BinaryMetaModelRelatedConsistencyRelation_deletingRight_feature", "_UI_BinaryMetaModelRelatedConsistencyRelation_type"),
				 MegamodelPackage.Literals.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_RIGHT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((BinaryMetaModelRelatedConsistencyRelation)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_BinaryMetaModelRelatedConsistencyRelation_type") :
			getString("_UI_BinaryMetaModelRelatedConsistencyRelation_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(BinaryMetaModelRelatedConsistencyRelation.class)) {
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_LEFT:
			case MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_RIGHT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
