/**
 */
package fr.labsticc.gmm.model.megamodel.provider;


import fr.labsticc.framework.constraints.model.constraints.ConstraintsFactory;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import fr.labsticc.gmm.model.megamodel.MegamodelFactory;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModel;

/**
 * This is the item provider adapter for a {@link fr.labsticc.gmm.model.megamodel.MetaModel} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MetaModelItemProvider
	extends ModelItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModelItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addPackagesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Packages feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPackagesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MetaModel_packages_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MetaModel_packages_feature", "_UI_MetaModel_type"),
				 MegamodelPackage.Literals.META_MODEL__PACKAGES,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MegamodelPackage.Literals.META_MODEL__OWNED_SUBSETS);
			childrenFeatures.add(MegamodelPackage.Literals.META_MODEL__OWNED_COMPARISON_SETTINGS);
			childrenFeatures.add(MegamodelPackage.Literals.META_MODEL__OWNED_CONSTRAINT_TARGETS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MetaModel.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MetaModel"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((MetaModel)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_MetaModel_type") :
			getString("_UI_MetaModel_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MetaModel.class)) {
			case MegamodelPackage.META_MODEL__OWNED_SUBSETS:
			case MegamodelPackage.META_MODEL__OWNED_COMPARISON_SETTINGS:
			case MegamodelPackage.META_MODEL__OWNED_CONSTRAINT_TARGETS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.META_MODEL__OWNED_SUBSETS,
				 MegamodelFactory.eINSTANCE.createMetaModelSubset()));

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.META_MODEL__OWNED_COMPARISON_SETTINGS,
				 MegamodelFactory.eINSTANCE.createComparisonSettings()));

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.META_MODEL__OWNED_CONSTRAINT_TARGETS,
				 ConstraintsFactory.eINSTANCE.createTarget()));
	}

}
