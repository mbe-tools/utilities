/**
 */
package fr.labsticc.gmm.model.megamodel.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import fr.labsticc.gmm.model.megamodel.MegamodelFactory;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.Model;

/**
 * This is the item provider adapter for a {@link fr.labsticc.gmm.model.megamodel.Model} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ModelItemProvider
	extends NamedElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFileExtensionsPropertyDescriptor(object);
			addResourcesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the File Extensions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFileExtensionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Model_fileExtensions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Model_fileExtensions_feature", "_UI_Model_type"),
				 MegamodelPackage.Literals.MODEL__FILE_EXTENSIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Resources feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addResourcesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Model_resources_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Model_resources_feature", "_UI_Model_type"),
				 MegamodelPackage.Literals.MODEL__RESOURCES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MegamodelPackage.Literals.MODEL__OWNED_MODELS);
			childrenFeatures.add(MegamodelPackage.Literals.MODEL__OWNED_OBLIGATION_RELATIONS);
			childrenFeatures.add(MegamodelPackage.Literals.MODEL__OWNED_FACTUAL_RELATIONS);
			childrenFeatures.add(MegamodelPackage.Literals.MODEL__ERROR);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Model.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Model"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Model)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Model_type") :
			getString("_UI_Model_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Model.class)) {
			case MegamodelPackage.MODEL__FILE_EXTENSIONS:
			case MegamodelPackage.MODEL__RESOURCES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case MegamodelPackage.MODEL__OWNED_MODELS:
			case MegamodelPackage.MODEL__OWNED_OBLIGATION_RELATIONS:
			case MegamodelPackage.MODEL__OWNED_FACTUAL_RELATIONS:
			case MegamodelPackage.MODEL__ERROR:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.MODEL__OWNED_MODELS,
				 MegamodelFactory.eINSTANCE.createModel()));

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.MODEL__OWNED_MODELS,
				 MegamodelFactory.eINSTANCE.createGmmSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.MODEL__OWNED_MODELS,
				 MegamodelFactory.eINSTANCE.createMetaModel()));

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.MODEL__OWNED_MODELS,
				 MegamodelFactory.eINSTANCE.createModelingEnvironment()));

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.MODEL__OWNED_MODELS,
				 MegamodelFactory.eINSTANCE.createSubsettedMetaModel()));

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.MODEL__OWNED_MODELS,
				 MegamodelFactory.eINSTANCE.createTool()));

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.MODEL__OWNED_FACTUAL_RELATIONS,
				 MegamodelFactory.eINSTANCE.createConformanceRelation()));

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.MODEL__ERROR,
				 MegamodelFactory.eINSTANCE.createErrorDescription()));
	}

}
