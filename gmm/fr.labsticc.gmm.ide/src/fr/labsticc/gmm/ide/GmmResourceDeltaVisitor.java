package fr.labsticc.gmm.ide;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.runtime.CoreException;

import fr.labsticc.framework.core.exception.IExceptionHandler;

public class GmmResourceDeltaVisitor implements IResourceDeltaVisitor {

	private final IExceptionHandler exceptionHandler;
	
	private final GmmManager gmmManager;
	
	public GmmResourceDeltaVisitor( final GmmManager p_gmmManager,
									final IExceptionHandler p_exceptionHandler ) {
		gmmManager = p_gmmManager;
		exceptionHandler = p_exceptionHandler;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse.core.resources.IResourceDelta)
	 */
	@Override
	public boolean visit( final IResourceDelta p_delta )
	throws CoreException {
		if ( gmmManager != null ) {
			final IResource resource = p_delta.getResource();
			
			switch ( p_delta.getKind() ) {
				case IResourceDelta.ADDED :
					// handle added resource
					try {
						gmmManager.manageAddedResource( resource );
					}
					catch( final Throwable p_th ) {
						exceptionHandler.handleException( p_th, resource );
					}
					
					break;
				case IResourceDelta.REMOVED :
					// handle removed resource
					try {
						gmmManager.manageRemovedResource( resource );
					}
					catch( final Throwable p_th ) {
						exceptionHandler.handleException( p_th, resource );
					}
					
					break;
				case IResourceDelta.CHANGED :
					// handle changed resource
					try {
						gmmManager.manageChangedResource( resource );
					}
					catch( final Throwable p_th ) {
						exceptionHandler.handleException( p_th, resource );
					}
					
					break;
			}
		}
		
		return true;
	}
}
