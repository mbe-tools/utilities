package fr.labsticc.gmm.ide;

import org.eclipse.ui.IStartup;

import fr.labsticc.framework.constraints.ide.ConstraintsIDEPlugin;

public class Startup implements IStartup {

	@Override
	public void earlyStartup() {
		 GmmPlugin.getDefault();
		 ConstraintsIDEPlugin.getDefault();
	}
}
