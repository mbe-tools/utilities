package fr.labsticc.gmm.ide.menus;

import java.util.Collection;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import fr.labsticc.framework.emf.core.util.EMFUtil;
import fr.labsticc.gmm.ide.GmmManager;
import fr.labsticc.gmm.ide.GmmPlugin;

public class AbstractGmmDynamicPopupMenus extends ContributionItem {

	protected static final String GMM_ELEMENT_KEY = "GMM_ELEM_KEY";
	
	//protected final GmmManager gmmManager;
	

	public AbstractGmmDynamicPopupMenus() {
		this( null );
	}

	public AbstractGmmDynamicPopupMenus( final String p_id ) {
		super( p_id );

		//gmmManager = GmmPlugin.getDefault().getGmmManager();
	}

	@Override
	public boolean isDynamic() {
		return true;
	}
	
	protected Collection<IResource> getSelectedResources() {
		final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		final ISelection selection = window.getActivePage().getSelection();
		
		if ( selection != null ) {
			return EMFUtil.selectedObjects( selection, IResource.class );
		}
		
		return null;
	}
	
	protected GmmManager getGmmManager() {
		return GmmPlugin.getDefault().getGmmManager();
	}
}
