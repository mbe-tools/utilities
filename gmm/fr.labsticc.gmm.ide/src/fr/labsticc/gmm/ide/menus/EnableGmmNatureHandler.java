package fr.labsticc.gmm.ide.menus;

import java.util.Set;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;

import fr.labsticc.gmm.ide.GmmPlugin;

public class EnableGmmNatureHandler extends AbstractGmmNatureHandler {
	
	/**
	 * The constructor.
	 */
	public EnableGmmNatureHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	@Override
	public Object execute( final ExecutionEvent p_event )
	throws ExecutionException {
		final Set<IProject> projects = getSelectedProjects( p_event );
		GmmPlugin.getDefault().getGmmManager().gmmNatureEnablingRequested( projects, true );
		
		return null;
	}
}
