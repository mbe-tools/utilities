package fr.labsticc.gmm.ide.menus;

import java.util.Collection;

import org.eclipse.core.resources.IResource;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import fr.labsticc.gmm.model.megamodel.SubsettedMetaModel;

public class GmmMmDeclareAsDynPopupMenus extends AbstractGmmDynamicPopupMenus {
	
	public GmmMmDeclareAsDynPopupMenus() {
		super();
	}

	public GmmMmDeclareAsDynPopupMenus( final String p_id ) {
		super( p_id );
	}
	
	@Override
	public void fill(	final Menu p_menu,
						final int pi_index ) {
		final Collection<IResource> selectedRes = getSelectedResources();
		
		if ( selectedRes != null ) {
			final EList<URI> uris = new BasicEList<URI>();
			
			for ( final IResource res : selectedRes ) {
				uris.add( URI.createPlatformResourceURI( res.getFullPath().toString(), true ) );
			}
			
			for ( final SubsettedMetaModel metaModel : getGmmManager().allSubsettedMetaModels( selectedRes ) ) {
				final String displayName = metaModel.getDisplayName();
				
				if ( displayName != null ) {
					final MenuItem menuItem = new MenuItem( p_menu, SWT.CHECK, pi_index );
					menuItem.setText( "Declare as " + displayName + " model..." );
					menuItem.setData( GMM_ELEMENT_KEY, metaModel );
					menuItem.addSelectionListener( new SelectionAdapter() {
						
						@Override
						public void widgetSelected( final SelectionEvent p_event ) {
							final MenuItem menuItem = (MenuItem) p_event.getSource();
							final SubsettedMetaModel metaModel = (SubsettedMetaModel) menuItem.getData( GMM_ELEMENT_KEY );
							//getGmmManager().declareAs( metaModel, getSelectedResources() );
						}
					} );
				}
			}
		}
	}
}
