package fr.labsticc.gmm.ide.menus;

import java.util.Collection;

import org.eclipse.core.resources.IResource;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import fr.labsticc.gmm.ide.GmmManager;
import fr.labsticc.gmm.model.megamodel.EnablementType;
import fr.labsticc.gmm.model.megamodel.ObligationRelation;

public class GmmEstabValidityDynPopupMenus extends AbstractGmmDynamicPopupMenus {
	
	public GmmEstabValidityDynPopupMenus() {
		super();
	}

	public GmmEstabValidityDynPopupMenus( final String p_id ) {
		super( p_id );
	}
	
	@Override
	public void fill(	final Menu p_menu,
						final int pi_index ) {
		final Collection<IResource> selectedRes = getSelectedResources();
		
		if ( selectedRes != null ) {
			final EList<URI> uris = new BasicEList<URI>();
			
			for ( final IResource res : selectedRes ) {
				uris.add( URI.createPlatformResourceURI( res.getFullPath().toString(), true ) );
			}

			final GmmManager gmmManager = getGmmManager();
			
			if ( gmmManager != null ) {
				for ( final ObligationRelation relation : gmmManager.allConcerningRelations( selectedRes ) ) {
					final String displayName = relation.getDisplayName( uris );
					
					if ( displayName != null ) {
						final MenuItem menuItem = new MenuItem( p_menu, SWT.CHECK, pi_index );
						menuItem.setText( displayName );
						menuItem.setData( GMM_ELEMENT_KEY, relation );
						menuItem.setEnabled( relation.getEnablementType() != EnablementType.NEVER );
						menuItem.addSelectionListener( new SelectionAdapter() {
							
							@Override
							public void widgetSelected( final SelectionEvent p_event ) {
								final MenuItem menuItem = (MenuItem) p_event.getSource();
								final ObligationRelation rel = (ObligationRelation) menuItem.getData( GMM_ELEMENT_KEY );
								gmmManager.establishValidity( rel, getSelectedResources() );
							}
						} );
					}
				}
			}
		}
	}
}
