package fr.labsticc.gmm.ide.menus;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import fr.labsticc.framework.core.exception.IExceptionHandler;
import fr.labsticc.gmm.ide.GmmPlugin;

public abstract class AbstractGmmNatureHandler extends AbstractHandler {

	private final IExceptionHandler exceptionHandler;

	protected AbstractGmmNatureHandler() {
		exceptionHandler = GmmPlugin.getDefault().getExceptionHandler();
	}

	protected Set<IProject> getSelectedProjects( final ExecutionEvent p_event ) {
		final ISelection currentSelection = HandlerUtil.getCurrentSelection( p_event );
		final Set<IProject> projects = new HashSet<IProject>();

		if ( currentSelection instanceof IStructuredSelection ) {
			for ( final Iterator<?> it = ((IStructuredSelection) currentSelection ).iterator(); it.hasNext(); ) {
				final Object element = it.next();

				if ( element instanceof IProject ) {
					projects.add( (IProject) element );
				}
				else if ( element instanceof IAdaptable ) {
					final IProject project = (IProject) ( (IAdaptable) element ).getAdapter( IProject.class );
					
					if ( project != null ) {
						projects.add( project );
					}
				}
			}
		}
		
		return projects;
	}
	
	protected IExceptionHandler getExceptionHandler() {
		return exceptionHandler;
	}
}
