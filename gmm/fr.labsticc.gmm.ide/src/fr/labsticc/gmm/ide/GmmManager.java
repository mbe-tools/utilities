package fr.labsticc.gmm.ide;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.IPartListener;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;

import fr.labsticc.framework.core.exception.FunctionalException;
import fr.labsticc.framework.core.exception.IExceptionHandler;
import fr.labsticc.framework.core.exception.ResourceAccessException;
import fr.labsticc.framework.core.exception.SystemException;
import fr.labsticc.framework.emf.core.util.EMFUtil;
import fr.labsticc.framework.emf.core.util.ResourceDepComparator;
import fr.labsticc.framework.emf.view.ide.EMFEditorUtil;
import fr.labsticc.framework.ide.resource.ExtensionsResourcesVisitor;
import fr.labsticc.framework.ide.util.PluginUtil;
import fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapter;
import fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag;
import fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterFactory;
import fr.labsticc.gmm.ide.builder.GmmNature;
import fr.labsticc.gmm.model.megamodel.ErrorDescription;
import fr.labsticc.gmm.model.megamodel.ErrorType;
import fr.labsticc.gmm.model.megamodel.ExecutionContext;
import fr.labsticc.gmm.model.megamodel.MegamodelFactory;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation;
import fr.labsticc.gmm.model.megamodel.Model;
import fr.labsticc.gmm.model.megamodel.ModelingEnvironment;
import fr.labsticc.gmm.model.megamodel.ObligationRelation;
import fr.labsticc.gmm.model.megamodel.OperationType;
import fr.labsticc.gmm.model.megamodel.Relation;
import fr.labsticc.gmm.model.megamodel.RelationPolicy;
import fr.labsticc.gmm.model.megamodel.SubsettedMetaModel;
import fr.labsticc.gmm.model.megamodel.UriRelatedBinaryRelPolicy;

// TODO: split into GmmManager and GmmEngine.
public class GmmManager {
	
	private static final String GMM_MARKER_TYPE = "fr.labsticc.gmm.ide.gmmProblem";
	
	private Map<String, EditorAdapterTag> availableEditorAdapters;

	private Map<String, List<EditorAdapterTag>> usedEditorAdapters;

	private final Class<? extends ResourceSet> resourceSetClass;
	
	private final IExceptionHandler exceptionHandler;
	
	private ILabelProvider labelProvider;
	
	private final Comparator<IProject> projectsDepComparator;
	
	private final ResourceDepComparator resourcesDepComparator;
	
	private boolean enabled;

	private final IPartListener partListener;
	
	public GmmManager( 	final IExceptionHandler p_exceptionHandler,
						final ResourceSet p_resSet ) {
		enabled = true;
		exceptionHandler = p_exceptionHandler;
		resourceSetClass = p_resSet == null ? ResourceSetImpl.class : p_resSet.getClass();
		initializeEditorAdapters();
		labelProvider = null;
		
		projectsDepComparator = new Comparator<IProject>() {
			@Override
		    public int compare(	final IProject p_proj1, 
		    					final IProject p_proj2 ) {
				if( p_proj1 == null ) {
		            return p_proj2 == null ? 0 : 1;
		        }
	
		        // At this point, we know that c1 and c2 are not null
		        if ( p_proj1.equals( p_proj2 ) ) {
		            return 0;
		        }
	
		        try {
			        boolean proj1DepOnProj2 = dependsOn( p_proj1, p_proj2 );
			        boolean proj2DepOnProj1 = dependsOn( p_proj2, p_proj1 );
			        
			        if ( proj1DepOnProj2 && !proj2DepOnProj1 ) {
			            return 1;
			        } 
		
			        if ( proj2DepOnProj1 && !proj1DepOnProj2 ) {
			            return - 1;
			        } 
		
			        return 0;
		        }
		        catch( final CoreException p_ex ) {
		        	throw new RuntimeException( p_ex );
		        }
		    }
			
			private boolean dependsOn( 	final IProject p_project1,
										final IProject p_project2 ) 
			throws CoreException {
				return dependsOn( p_project1, p_project2, new HashSet<IProject>() );
			}
			
			private boolean dependsOn( 	final IProject p_project1,
										final IProject p_project2,
										final Set<IProject> p_traversedProj ) 
			throws CoreException {
				if ( p_project1 == p_project2 ) {
					return true;
				}
				
				for ( final IProject refProject : p_project1.getReferencedProjects() ) {
					if ( !p_traversedProj.contains( refProject ) ) {
						p_traversedProj.add( refProject );

						if ( dependsOn( refProject, p_project2 ) ) {
							return true;
						}
					}
				}
				
				return false;
			}
		};
		
		resourcesDepComparator = new ResourceDepComparator();
		
		partListener = new IPartListener() { 
//	
//			@Override
//			public void partOpened( final IWorkbenchPart p_part ) {
//				try {
//					final URI managedResUri = getGmmManagedResUri( p_part );
//					
//					if ( managedResUri != null ) {
//						manageLoadedResource( managedResUri );
//					}
//				}
//				catch( final ResourceAccessException p_ex ) {
//					exceptionHandler.handleException( p_ex, p_part );
//				}
//				catch( final CoreException p_ex ) {
//					exceptionHandler.handleException( p_ex, p_part );
//				}
//			}

			@Override
			public void partActivated( final MPart p_part ) {
				try {
					final URI managedResUri = getGmmManagedResUri( p_part );
					
					if ( managedResUri != null ) {
						manageLoadedResource( managedResUri );
					}
				}
				catch( final ResourceAccessException p_ex ) {
					exceptionHandler.handleException( p_ex, p_part );
				}
				catch( final CoreException p_ex ) {
					exceptionHandler.handleException( p_ex, p_part );
				}
			}

			@Override
			public void partBroughtToTop(MPart part) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void partDeactivated(MPart part) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void partHidden(MPart part) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void partVisible(MPart part) {
				// TODO Auto-generated method stub
				
			}
		};

		addPartListener();
	}
	
	private void initializeEditorAdapters() {
		// Setup list of diagram adapters
		final IExtensionRegistry extensionRegistry = Platform.getExtensionRegistry();
		
		// TODO: Remove the need for extension point. Declare editor adapters in mega-model.
		final IConfigurationElement[] extensions = extensionRegistry.getConfigurationElementsFor( "fr.labsticc.gmm.ide.editorAdapterExtension" );
	
		for ( final IConfigurationElement configurationElement : extensions ) {
			final EditorAdapterTag tag = EditoradapterFactory.eINSTANCE.createEditorAdapterTag();
			tag.setCreationMethodName(configurationElement.getAttribute( "creationMethodName" ) );
			tag.setEditorAdapterPackageNsURI(configurationElement.getAttribute( "packageNsURI" ));
			tag.setEditorAdapterID(configurationElement.getAttribute( "id" ) );
			tag.setModelID( configurationElement.getAttribute( "modelId" ) );
	
			if (	tag.getCreationMethodName() != null	&& !"".equals(tag.getCreationMethodName()) && 
					tag.getEditorAdapterPackageNsURI() != null && !"".equals(tag.getEditorAdapterPackageNsURI()) && 
					tag.getEditorAdapterID() != null && !"".equals(tag.getEditorAdapterID()) && 
					tag.getModelID() != null && !"".equals(tag.getModelID())) {
				getAvailableEditorAdapters().put(tag.getModelID(), tag);
			}
		}
	}
	
	@Override
	public void finalize() 
	throws Throwable {
//		Display.getDefault().syncExec( new Runnable() {
//			
//			@Override
//			public void run() {
//				final EPartService service = PlatformUI.getWorkbench().getService( EPartService.class );
//				service.removePartListener( partListener );
//				final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
//				
//				if ( window != null ) {
//					window.getPartService().removePartListener( partListener );
//				}
//			}
//		} );
		
		super.finalize();
	}
	
	private void addPartListener() {
//		Display.getDefault().syncExec( new Runnable() {
//		    @Override
//		    public void run() {
//				final EPartService service = PlatformUI.getWorkbench().getService( EPartService.class );
//				service.addPartListener( partListener );
//				IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
//				
//				if ( window == null ) {
//					Display.getDefault().getActiveShell().g;
//
//				}
//					window.getPartService().addPartListener( partListener );
					
					// Search for all editors already opened
//					for ( final IWorkbenchWindow workWindow : PlatformUI.getWorkbench().getWorkbenchWindows() ) {
//					    for ( final IWorkbenchPage page : workWindow.getPages() ) {
//					        for ( final IEditorReference editorRef : page.getEditorReferences() ) {
//						        final IEditorPart editor = editorRef.getEditor( false );
//						        
//						        if ( editor != null ) {
//						        	partListener.partOpened( editor );
//						        }
//					        }
//					    }
//					}
				//}
//		    }
//		} );
	}
	
	private Set<URI> modelResourceUris( final Collection<MetaModel> p_metaModels ) 
	throws CoreException, ResourceAccessException {
		final Set<URI> modelRootResourceUris = new HashSet<URI>();
		
		if ( !p_metaModels.isEmpty() ) {
			final Collection<String> extensions = new ArrayList<String>();

			for ( final MetaModel metaModel : p_metaModels ) {
				extensions.addAll( metaModel.getFileExtensions() );
			}

			final ExtensionsResourcesVisitor visitor = new ExtensionsResourcesVisitor( extensions );
			ResourcesPlugin.getWorkspace().getRoot().accept( visitor );
	
			for ( final IResource resource : visitor.getFoundResources() ) {
				modelRootResourceUris.add( URI.createPlatformResourceURI( resource.getFullPath().toString(), true ) );
			}
		}
		
		return modelRootResourceUris;
	}
	
	public Model getMegaModel() {
		return GmmPlugin.getDefault().getMegaModel();
	}
	
	private boolean hasErrorsOtherThanGmm( final Resource p_resource ) 
	throws CoreException {
		if ( !p_resource.getErrors().isEmpty() ) {
			return true;
		}
		
		return hasErrorsOtherThanGmm( EMFUtil.convertToIDEResource( p_resource ) );
	}
	
	private boolean hasErrorsOtherThanGmm( final IResource p_resource ) 
	throws CoreException {
		if ( p_resource.exists() ) {
			for ( final IMarker marker : p_resource.findMarkers( null, true, IResource.DEPTH_ONE ) ) {
				if ( 	marker.exists() && !GMM_MARKER_TYPE.equals( marker.getType() ) && 
						(Integer) marker.getAttribute( IMarker.SEVERITY ) == IMarker.SEVERITY_ERROR ) {
					return true;
				}
			}
		}
		
		return false;
	}

	private boolean hasOnlyGmmErrors( final Resource p_resource ) 
	throws CoreException {
		return hasOnlyGmmErrors( EMFUtil.convertToIDEResource( p_resource ) );
	}
		
	private boolean hasOnlyGmmErrors( final IResource p_resource ) 
	throws CoreException {
		if ( p_resource.exists() ) {
			if ( hasErrorsOtherThanGmm( p_resource ) ) {
				return false;
			}
			
			for ( final IMarker marker : p_resource.findMarkers( GMM_MARKER_TYPE, true, IResource.DEPTH_ONE ) ) {
				if ( marker.exists() ) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	private ResourceSet createResourceSet()
	throws ResourceAccessException {
		try {
			return resourceSetClass.newInstance();
		}
		catch( InstantiationException p_ex ) {
			throw new ResourceAccessException( p_ex );
		}
		catch( IllegalAccessException p_ex ) {
			throw new ResourceAccessException( p_ex );
		}
	}
	
	/**
	 * @param p_resource
	 * @param pb_testExists
	 * @param pb_considerGmm
	 * @return The URI, or null if the resource has errors.
	 * @throws CoreException
	 */
	private URI getUri( final IResource p_resource,
						final boolean pb_testExists ) 
	throws CoreException {
		if ( 	p_resource instanceof IFile && ( !pb_testExists || p_resource.exists() ) && 
				EMFUtil.isEMFResource( p_resource ) ) {
			final IPath path = ( (IFile) p_resource ).getFullPath();
			
			return URI.createPlatformResourceURI( path.toString(), true );
		}
		
		return null;
	}

	private void processChangedResource( 	final URI p_sourceResUri,
											final ExecutionContext p_execContext ) {
		try {
			processChangedResource( p_sourceResUri, createResourceSet(), p_execContext );
		}
		catch( final ResourceAccessException p_ex ) {
			exceptionHandler.handleException( p_ex, p_sourceResUri );
		}
	}
	
	private void processChangedResource( 	final URI p_sourceResUri,
											final ResourceSet p_resSet,
											final ExecutionContext p_execContext ) {
		final Model megaModel = getMegaModel();
		
		if ( megaModel != null ) {
			//try {
	    	for ( final Relation relation : megaModel.getOwnedObligationRelations() ) {
	    		if ( relation instanceof ObligationRelation ) {
	    			try {
	    				establishValidity( (ObligationRelation) relation, p_sourceResUri, p_resSet, p_execContext );
	    			}
	    			catch( final CoreException p_ex ) {
	    				exceptionHandler.handleException( p_ex, relation );
	    			}
	    			catch( final SystemException p_ex ) {
	    				exceptionHandler.handleException( p_ex, relation );
	    			}
	    			catch( final FunctionalException p_ex ) {
	    				exceptionHandler.handleException( p_ex, relation );
	    			}
	    		}
	    	}
//			}
//			catch( final InexistentCrossResourceException p_ex ) {
//				
//				// Corresponding resource is missing so try to establish validity
//				processChangedResource( p_ex.getInexistentResUri(), p_resSet, p_filter );
//				
//				// Retry after validity has been re-established.
//				processChangedResource( p_sourceResUri, p_resSet, p_filter );
//			}
		}
	}
	
	public void establishValidity( 	final ObligationRelation p_obligRelation,
									final Collection<IResource> p_resources ) {
		try {
			final ResourceSet resSet = createResourceSet();
			final ExecutionContext execContext = createExecutionContext( true, OperationType.UPDATED );
//		
//			for ( final IResource resource : p_resources ) {
//				if ( resource instanceof IContainer ) {
//					concernedRes.addAll( concernedResources( p_obligRelation, (IContainer) resource, resSet, noErrorsWritableResourcesFilter ) );
//				}
//				else {
//					addConcernedResources( concernedRes, p_obligRelation, (IFile) resource, resSet, noErrorsWritableResourcesFilter );
//				}
//			}
			
			//final List<Resource> concernedResList = new ArrayList<Resource>( concernedRes );
			final WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {
				
				@Override
				protected void execute( final IProgressMonitor p_monitor )
				throws CoreException, InvocationTargetException, InterruptedException {
					p_monitor.beginTask( "Applying obligation relations on resources...", p_resources.size() );
	
					// Sort resources according to their dependencies
					//Collections.sort( concernedResList, resourcesComparator );
					//p_monitor.worked( 1 );
					
					try {
						for ( final IResource res : p_resources ) {
							establishValidity( p_obligRelation, getUri( res, true ), resSet, execContext );
							p_monitor.worked( 1 );
						}
						
						final Set<IProject> projects = new HashSet<IProject>();

						for ( final IResource res : p_resources ) {
							final IProject project = res.getProject();
							projects.add( project );
							projects.addAll( PluginUtil.getReferencedProjects( project ) );
						}

						//final List<IProject> sortedProjects = new ArrayList<IProject>( projects );
						//Collections.sort( sortedProjects, projectsComparator );
						
						final UriRelatedBinaryRelPolicy mmUriRelationPolicy;
						
						if ( p_obligRelation instanceof MetaModelRelatedRelation ) {
							final RelationPolicy relPolicy = ( (MetaModelRelatedRelation) p_obligRelation ).getOwnedRelationPolicy();
							
							if ( relPolicy instanceof UriRelatedBinaryRelPolicy ) {
								mmUriRelationPolicy = (UriRelatedBinaryRelPolicy) relPolicy;
							}
							else {
								mmUriRelationPolicy =  null;
							}
						}
						else {
							mmUriRelationPolicy = null;
						}
						
						for ( final IProject project : projects ) {
							if ( !project.getResourceAttributes().isReadOnly() ) {
								PluginUtil.addNature( project, GmmNature.NATURE_ID );
								
								if ( mmUriRelationPolicy != null ) {
									final URI projectUri = URI.createPlatformResourceURI( project.getFullPath().toString(), true );
									final URI correspProjUri = mmUriRelationPolicy.correspondingUri( projectUri );
									
									if ( correspProjUri != null ) {
										final IProject corrProject = ResourcesPlugin.getWorkspace().getRoot().getProject( correspProjUri.lastSegment() );
										
										if ( corrProject != null && corrProject.getResourceAttributes() != null && !corrProject.getResourceAttributes().isReadOnly() ) {
											PluginUtil.addNature( corrProject, GmmNature.NATURE_ID );
										}
									}
								}
							}
						}
					}
					catch( final Throwable p_th ) {
						throw new InvocationTargetException( p_th );
					}
					finally {
						p_monitor.done();
					}
				}
			};
	
			new ProgressMonitorDialog( PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell() ).run( true, false, operation );
		}
		catch( final InterruptedException p_ex ) {
			// Should not happen because not interruptible
		}
		catch( final InvocationTargetException p_ex ) {
			exceptionHandler.handleException( p_ex, p_resources );
		}
		catch( final ResourceAccessException p_ex ) {
			exceptionHandler.handleException( p_ex, p_resources );
		}
	}
	
	private ExecutionContext createExecutionContext( 	final boolean pb_manualLaunch,
														final OperationType p_operationType ) {
		final ExecutionContext execContext = MegamodelFactory.eINSTANCE.createExecutionContext();
		execContext.setManualLaunch( pb_manualLaunch );
		execContext.setSourceModelOperationType( p_operationType );
		
		return execContext;
	}
	
	private void establishValidity( final ObligationRelation p_obligRelation,
									final URI p_sourceResUri,
									final ResourceSet p_resSet,
									final ExecutionContext p_execContext ) 
	throws CoreException, SystemException, FunctionalException {
		if ( p_obligRelation.concerns( p_sourceResUri ) ) {
			final ModelingEnvironment environment = createModelingEnvironment( 	p_obligRelation, 
																				p_sourceResUri,
																				p_execContext,
																				p_resSet );
			
			if ( environment != null ) {
				final Collection<Model> updatedModels = p_obligRelation.establishValidity( environment, p_execContext );
				final boolean readOnly = isReadOnly( environment.getSourceModel() );
				
				for ( final Model updatedModel : updatedModels ) {
					save( p_sourceResUri, p_obligRelation, updatedModel, readOnly );
				}
				
				// Perform chained relations on every target models even if it was not updated
				// Needed in case updated in memory only (e.g. mode traceability model)
				for ( final Model targetModel : environment.getOwnedModels() ) {
					for ( final Resource changedRes : targetModel.getResources() ) {
						final URI changedResUri = changedRes.getURI();
						
						for ( final Relation chainedRelation : p_obligRelation.getChainedRelations() ) {
							if ( chainedRelation instanceof ObligationRelation ) {
								final ExecutionContext chainedExecContext = EcoreUtil.copy( p_execContext );
								chainedExecContext.setPreviousRelation( p_obligRelation );
								establishValidity( 	(ObligationRelation) chainedRelation,
													changedResUri,
													p_resSet,
													chainedExecContext );
							}
						}
					}
				}

				deleteInconsistencyMarkers( environment );
				createMarkers( environment );

// TODO: U-ncoment when the proxy resolution problem of circular cross references is solved. See AdeleOsateSynchronizationRelationImpl.					
//					if ( !updatedModels.isEmpty() && p_obligRelation instanceof ConsistencyRelation ) {
//						checkConsistency( (ConsistencyRelation) p_obligRelation, environment, p_resSet, noErrorsButGmmResourcesFilter );
//					}
			}
		}
//		}
	}
	
	private boolean isReadOnly( final Model p_model ) {
		boolean readOnly = true;

		for ( final Resource res : p_model.getResources() ) {
			readOnly = EMFUtil.isReadOnly( res ) && readOnly;
		}
		
		return readOnly;
	}
	
	private void save( 	final URI p_sourceUri,
						final ObligationRelation p_relation,
						final Model p_model,
						final boolean pb_setReadOnly ) 
	throws ResourceAccessException, CoreException {
		for ( final Resource resource : p_model.getResources() ) {
			try {
				if ( resource.getContents().isEmpty() ) {
					if ( p_relation.canDelete( p_model ) ) {
						resource.delete( null );
					}
				}
				else {
					if ( !EMFUtil.isReadOnly( resource ) ) {
						final Map<String,String> saveOptions = new HashMap<String, String>();
						saveOptions.put( Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER );
						
						System.out.println( "Saving resource " + resource.getURI() );
						resource.save( saveOptions );

						if ( pb_setReadOnly ) {
							EMFUtil.setReadOnly( resource );
						}
					}
				}
			}
			catch( final IOException p_ex ) {
				// TODO: Notify relation that save did not work so that cache can be rolled back.
				throw new ResourceAccessException( p_ex );
			}
		}
	}
	
	public void manageAddedResource( final IResource p_resource ) 
	throws CoreException, ResourceAccessException {
		if ( isEnabled() ) {
			final URI uri = getUri( p_resource, true );
			
			if ( uri != null ) {
				System.out.println( new Date() + ": Received added resource notification for resource " + uri.lastSegment() );
				final ResourceSet resSet = createResourceSet();
				final ExecutionContext execContext = createExecutionContext( false, OperationType.CREATED );
				
				// Call process changed resources so that if any corresponding resources does not exist it can 
				// be created.
				processChangedResource( uri, resSet, execContext );
			}
		}
	}
	
	public void manageChangedResource( 	final IResource p_resource ) 
	throws CoreException, ResourceAccessException {
		if ( isEnabled() ) {
			final URI uri = getUri( p_resource, true );
	
			if ( uri != null ) {
				System.out.println( new Date() + ": Received changed resource notification for resource " + uri.lastSegment() );
				final ResourceSet resSet = createResourceSet();
				final ExecutionContext execContext = createExecutionContext( false, OperationType.UPDATED );
				processChangedResource( uri, resSet, execContext );
			}
		}
	}
	
	public void manageRemovedResource( final IResource p_resource ) 
	throws CoreException, ResourceAccessException {
		if ( isEnabled() ) {
			// The resource has been deleted so we don't want to test that it exists.
			final URI uri = getUri( p_resource, false );
			
			if ( uri != null ) {
				System.out.println( new Date() + ": Received removed resource notification for resource " + uri.lastSegment() );
				final ExecutionContext execContext = createExecutionContext( false, OperationType.DELETED );
				
				// Do not filter resources and let the obligation relations decide what to do.
				processChangedResource( uri, execContext );
			}
		}
	}
	
	private void manageLoadedResource( final URI p_resourceUri ) 
	throws CoreException, ResourceAccessException {
		if ( isEnabled() &&!EMFUtil.isReadOnly( p_resourceUri ) ) {
			System.out.println( new Date() + ": Received loaded resource notification for resource " + p_resourceUri.lastSegment() );
			
			final WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {
				
				@Override
				protected void execute( final IProgressMonitor p_monitor )
				throws CoreException, InvocationTargetException, InterruptedException {
					p_monitor.beginTask( "Managing loaded resource...", 1 );
					
					try {
						final ResourceSet resSet = createResourceSet();
						final ExecutionContext execContext = createExecutionContext( false, OperationType.READ );
						processChangedResource( p_resourceUri, resSet, execContext );
						p_monitor.worked( 1 );
					}
					catch( final ResourceAccessException p_ex ) {
						throw new InvocationTargetException( p_ex );
					}
					finally {
						p_monitor.done();
					}
				}
			};
			
			try {
				new ProgressMonitorDialog( PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell() ).run( false, false, operation );
			}
			catch( final InterruptedException p_ex ) {
				// Should not happen because not interruptible
			}
			catch( final InvocationTargetException p_ex ) {
				exceptionHandler.handleException( p_ex.getTargetException(), p_resourceUri );
			}
		}
	}
	
	private void deleteInconsistencyMarkers( final ModelingEnvironment p_environment )
	throws CoreException, ResourceAccessException {
		final Model sourceModel = p_environment.getSourceModel();
		final Relation relation = p_environment.getRelation();
		deleteInconsistencyMarkers( relation, sourceModel );
		
		for ( final Model model : p_environment.getOwnedModels() ) {
			deleteInconsistencyMarkers( relation, model );
		}
	}

	private void deleteInconsistencyMarkers( 	final Relation p_relation,
												final Model p_model ) 
	throws CoreException, ResourceAccessException {
		for ( final Resource modelRes : p_model.getResources() ) {
			deleteInconsistencyMarkers( p_relation.getName(), EMFUtil.convertToIDEResource( modelRes ) );
		}
	}
	
	private void createMarkers( final ModelingEnvironment p_modelingEnvironment ) 
	throws CoreException, ResourceAccessException {
		final Relation relation = p_modelingEnvironment.getRelation();
		final Model sourceModel = p_modelingEnvironment.getSourceModel();
		boolean hasError = createMarkers( relation, sourceModel );
		
		for ( final Model model : p_modelingEnvironment.getOwnedModels() ) {
			hasError = hasError || createMarkers( relation, model );
		}

		if ( hasError ) {
			final StringBuilder message = new StringBuilder( "Global Model Management errors detected for relation ");
			message.append( relation.getDisplayName() );
			message.append( ". Please edit the models manually to fix the errors and re-enable Global Model Management." );
			showErrorDialog( relation.getDisplayName() + " Error", message.toString() );
		}
	}

	private boolean createMarkers( 	final Relation p_relation,
									final Model p_model ) 
	throws CoreException, ResourceAccessException {
		final ErrorDescription error = p_model.getError();
		boolean hasError = false;
		
		if ( error != null && error.getErroneousElements() != null ) {
			for ( final Model otherModel : error.getErroneousElements().keySet() ) {
				final String targetModelName = otherModel.getName();

				for ( final EObject eObject : error.getErroneousElements().get( otherModel ) ) {
					final Resource elemRes = eObject.eResource();
					final IResource markerRes = EMFUtil.convertToIDEResource( elemRes );
					
					if ( markerRes.isAccessible() ) {
						final EditorAdapter edAdapter = getEditorAdapter( elemRes.getURI() );
						final Object markerObjId = edAdapter == null ? eObject : edAdapter.getMarkerElementId( eObject );
						final String message = "Model element " + getText( eObject ) + " is inconsistent with model " + targetModelName + ".";
						createInconsistencyMarker( p_relation.getName(), markerRes, message, markerObjId );
						hasError = true;
					}
				}
			}
		}
		
		return hasError;
	}

	private void createInconsistencyMarker(	final String p_relationId,
											final IResource p_resource, 
											final String p_message,
											final Object p_objectId ) 
	throws CoreException {
		final IMarker marker = p_resource.createMarker( GMM_MARKER_TYPE );
		marker.setAttribute( IMarker.SOURCE_ID, p_relationId );
		marker.setAttribute( IMarker.MESSAGE, p_relationId + ":" + p_message );
		marker.setAttribute( IMarker.SEVERITY, IMarker.SEVERITY_ERROR );
		
		if ( p_objectId instanceof EObject ) {
			final URI objUri = EcoreUtil.getURI( (EObject) p_objectId );
			marker.setAttribute( EValidator.URI_ATTRIBUTE, objUri.toString() );
		}
		else if ( p_objectId instanceof Integer ) {
			marker.setAttribute( IMarker.LINE_NUMBER, (Integer) p_objectId );
		}
	}
	
	private void showErrorDialog( 	final String p_title,
									final String p_message ) {
		Display.getDefault().asyncExec( new Runnable() {
			
			@Override
			public void run() {
				MessageDialog.openError( Display.getCurrent().getActiveShell(), p_title, p_message );
			}
		} );
	}
	
	private void determineErrorStatus( final Model p_model )
	throws CoreException {
		ErrorType errType = null;
		
		for ( final Resource res : p_model.getResources() ) {
			if ( hasErrorsOtherThanGmm( res ) ) {
				errType = ErrorType.OTHER;
				
				break;
			}
			
			if ( hasOnlyGmmErrors( res ) ) {
				errType = ErrorType.GMM;
			}
		}

		if ( errType != null ) {
			final ErrorDescription error = MegamodelFactory.eINSTANCE.createErrorDescription();
			error.setErrorStatus( errType );
			p_model.setError( error );
		}
 	}
	
	private boolean isEnabled( 	final Relation p_relation, 
								final Model p_model,
								final ExecutionContext p_execContext ) {
		if ( p_relation instanceof ObligationRelation ) {
			return ( (ObligationRelation) p_relation ).isEnabled( p_model, p_execContext );
		}
		
		return true;
	}
	
	private ModelingEnvironment createModelingEnvironment( 	final Relation p_relation,
															final URI p_changedResourceUri,
															final ExecutionContext p_execContext,
															final ResourceSet p_resSet )
	throws ResourceAccessException, CoreException {
		// Important: Do not read the resource from opened editor, which may not have received the changes.
		final Resource sourceResource = getResource( p_changedResourceUri, p_resSet, false );
		final boolean concerned = sourceResource.getContents().isEmpty() ? p_relation.concerns( sourceResource.getURI() ) : p_relation.concerns( sourceResource.getContents().get( 0 ) );

		if ( concerned ) {
			final Model sourceModel = MegamodelFactory.eINSTANCE.createModel();
			sourceModel.setName( p_changedResourceUri.lastSegment() );
			sourceModel.getResources().add( sourceResource );
			determineErrorStatus( sourceModel );
			
			if ( isEnabled( p_relation, sourceModel, p_execContext ) ) {
				final ModelingEnvironment environment = MegamodelFactory.eINSTANCE.createModelingEnvironment();
				environment.setRelation( p_relation );
				environment.setSourceModel( sourceModel );
				
				if ( p_relation instanceof MetaModelRelatedRelation ) {
					createTargetModels( (MetaModelRelatedRelation) p_relation, environment, p_changedResourceUri, p_execContext, p_resSet );
				}
				else {
					throw new IllegalArgumentException();
				}
				
				return environment;
			}
		}
		
		return null;
	}
	
	private void createTargetModels( 	final MetaModelRelatedRelation p_mmRelation,
										final ModelingEnvironment p_environment,
										final URI p_concernedResourceUri,
										final ExecutionContext p_execContext,
										final ResourceSet p_resSet )
	throws CoreException, ResourceAccessException {
		final Collection<MetaModel> prunedMetaModels = new ArrayList<MetaModel>();
		
		for ( final MetaModel metaModel : p_mmRelation.getMetaModelsList() ) {
			if ( !metaModel.declares( p_concernedResourceUri ) ) {
				prunedMetaModels.add( metaModel );
			}
		}
		
		final UriRelatedBinaryRelPolicy relationPolicy = (UriRelatedBinaryRelPolicy) p_mmRelation.getOwnedRelationPolicy();
		
		for ( final URI rootResourceUri : modelResourceUris( prunedMetaModels ) ) {
			if ( relationPolicy == null || relationPolicy.areRelated( p_concernedResourceUri, rootResourceUri ) ) {
				
				// Important: Do not read the resource from opened editor, which may not have received the changes.
				final Resource resource = getResource( rootResourceUri, p_resSet, false );
				final Model model = MegamodelFactory.eINSTANCE.createModel();
				model.setName( rootResourceUri.lastSegment() );
				model.getResources().add( resource );
				determineErrorStatus( model );
				
				if ( isEnabled( p_mmRelation, model, p_execContext ) ) {
					p_environment.getOwnedModels().add( model );
				}
			}
		}
		
		// There should be at least one corresponding model
		if ( p_environment.getOwnedModels().isEmpty() ) {
			final URI corrUri = relationPolicy.correspondingUri( p_concernedResourceUri );
			
			if ( corrUri != null ) {
				final Model model = MegamodelFactory.eINSTANCE.createModel();
				
				// The corresponding resource does not exists, so we create it
				final Resource targetRes = getResource( corrUri, p_resSet, false );
				model.setName( corrUri.lastSegment() );
				model.getResources().add( targetRes );
				p_environment.getOwnedModels().add( model );
			}
		}
	}
	
	private String getEditorAdapterId( final URI p_resourceUri ) {
		return p_resourceUri.fileExtension();
	}
	
	private URI getGmmManagedResUri( final MPart p_part ) 
	throws CoreException {
 		if ( p_part instanceof IEditorPart ) {
 			final String resName = EMFEditorUtil.getResourceName( (IEditorPart) p_part );
 			
 			// The resource name may be null if an editor was opened and the resource does 
 			// not exists anymore when the workspace is re-opened.
 			if( resName != null ) {
 				return getGmmManagedResUri( resName );
 			}
 		}
 		
 		return null;
	}
	
	private URI getGmmManagedResUri( final String p_resName ) 
	throws CoreException {
		final URI tmpResUri = URI.createURI( new File( p_resName ).getPath(), true );

		if ( !"file".equals( tmpResUri.scheme() ) ) {
			final URI resUri = URI.createPlatformResourceURI( p_resName, true );
			
 			//Some files like the temporary files generated when comparing files do not have a project, so do not handle them.
			if ( resUri.segmentCount() > 2 && EMFUtil.isEMFResource( resUri.fileExtension() ) ) {
				final IResource resource = EMFUtil.convertToIDEResource( resUri );
	
				if ( resource.exists() && resource.getProject().hasNature( GmmNature.NATURE_ID ) ) {
					return resUri;
				}
			}
		}
		
		return null;
	}

	private Map<String, List<EditorAdapterTag>> getUsedEditorAdapters() {
		if (usedEditorAdapters == null) {
			usedEditorAdapters = new HashMap<String, List<EditorAdapterTag>>();
		}
		
		return usedEditorAdapters;
	}

	public Map<String, EditorAdapterTag> getAvailableEditorAdapters() {
		if (availableEditorAdapters == null) {
			availableEditorAdapters = new HashMap<String, EditorAdapterTag>();
		}
		return availableEditorAdapters;
	}

	private Resource getResourceFromEditor( final URI p_resourceUri ) 
	throws ResourceAccessException {
		final EditorAdapter adpater = getEditorAdapter( p_resourceUri );
		
		if ( adpater == null) {
			return null;
		}

		adpater.load( p_resourceUri );
		
		return adpater.getEditorResource( p_resourceUri, false );
	}
	
	public Resource getResource( final URI p_resourceUri ) 
	throws ResourceAccessException {
		return getResource( p_resourceUri, createResourceSet(), true );
	}

	private Resource getResource( 	final URI p_resourceUri,
									final ResourceSet p_resSet,
									final boolean pb_searchInEditor ) 
	throws ResourceAccessException {
		if ( pb_searchInEditor ) {
			final Resource editorRes = getResourceFromEditor( p_resourceUri );
			
			if ( editorRes != null ) {
				return editorRes;
			}
		}
		
		final Resource resource = p_resSet.getResource( p_resourceUri, false );
		
		if ( resource != null ) {
			return resource;
		}
		
		if ( p_resSet.getURIConverter().exists( p_resourceUri, null ) ) {
			return p_resSet.getResource( p_resourceUri, true );
		}

		return p_resSet.createResource( p_resourceUri );
	}

	private EditorAdapter getEditorAdapter(final URI p_resUri ) 
	throws ResourceAccessException {
		return getEditorAdapter( getEditorAdapterId( p_resUri ) );
	}

	private EditorAdapter getEditorAdapter( final String p_modelId ) 
	throws ResourceAccessException {

		// Check if there are suitable diagramAdapters.
		EditorAdapterTag editorAdapterTag = getAvailableEditorAdapters().get(p_modelId);

		if (editorAdapterTag == null) {
			return null;
		}

		EditorAdapter editorAdapter = null;

		if ( getUsedEditorAdapters().get(p_modelId) != null) {
			for ( final EditorAdapterTag adapterTag : getUsedEditorAdapters().get( p_modelId ) ) {
				if ( 	adapterTag.getCreationMethodName().equals( editorAdapterTag.getCreationMethodName()) && 
						adapterTag.getEditorAdapterID().equals( editorAdapterTag.getEditorAdapterID()) &&
						adapterTag.getEditorAdapterPackageNsURI().equals( editorAdapterTag.getEditorAdapterPackageNsURI()) &&
						adapterTag.getModelID().equals(	editorAdapterTag.getModelID())) {
					editorAdapter = adapterTag.getEditorAdapter();
					
					break;
				}
			}
		}
		else {
			getUsedEditorAdapters().put( p_modelId, new BasicEList<EditorAdapterTag>());
		}

		// Initialize the diagram adapters if the are not already in use
		if ( editorAdapter == null) {
			editorAdapterTag = initializeEditorAdapter( editorAdapterTag );
			editorAdapter = editorAdapterTag.getEditorAdapter();

			if (!getUsedEditorAdapters().containsKey( editorAdapterTag.getModelID())) {
				getUsedEditorAdapters().put( editorAdapterTag.getModelID(), new BasicEList<EditorAdapterTag>());
			}

			getUsedEditorAdapters().get(editorAdapterTag.getModelID()).add( editorAdapterTag );
		}

		return editorAdapter;
	}
	
	private EditorAdapterTag initializeEditorAdapter( final EditorAdapterTag p_editorAdapterTag ) 
	throws ResourceAccessException {

		 // First make a copy of the tag
		final EditorAdapterTag tag = EcoreUtil.copy( p_editorAdapterTag );

		// Get the diagramAdapter's package
		final EPackage editorAdapterPackage = Registry.INSTANCE.getEPackage( tag.getEditorAdapterPackageNsURI() );

		// Get the diagramAdapter factory
		final EFactory editorAdapterFactory = editorAdapterPackage.getEFactoryInstance();

		try {
			// Get the method to create the diagramAdapter
			final Method creationMethod = editorAdapterFactory.getClass().getMethod(tag.getCreationMethodName(), (Class[]) null);
	
			// Invoke the method
			final EditorAdapter editorAdapter = (EditorAdapter) creationMethod.invoke( editorAdapterFactory, (Object[]) null );
	
			if ( editorAdapter == null ) {
				throw new ResourceAccessException( "The diagram adapter with ID '" + tag.getEditorAdapterID() + "' could not be created.", null );
			}
	
			tag.setEditorAdapter( editorAdapter );
	
			return tag;
		}
		catch( final NoSuchMethodException p_ex ) {
			throw new ResourceAccessException( p_ex );
		}
		catch( final InvocationTargetException p_ex ) {
			throw new ResourceAccessException( p_ex );
		}
		catch( final IllegalAccessException p_ex ) {
			throw new ResourceAccessException( p_ex );
		}
	}
	
	private void gmmNatureEnabled( 	final Collection<IProject> p_projects,
									final IProgressMonitor p_monitor ) 
	throws CoreException, ResourceAccessException, FunctionalException {
		final Set<IProject> allProjects = PluginUtil.getReferencedProjects( p_projects );
		final List<IProject> projects = new ArrayList<IProject>( allProjects );
		final IProgressMonitor subMonitor = SubMonitor.convert( p_monitor, projects.size() );
		final ExecutionContext execContext = createExecutionContext( false, OperationType.READ );
		Collections.sort( projects, projectsDepComparator );
		
		for ( final IProject project : projects ) {
			if ( !project.hasNature( GmmNature.NATURE_ID ) ) {
				if ( !project.getResourceAttributes().isReadOnly() ) {
					PluginUtil.addNature( project, GmmNature.NATURE_ID );
					manageResources( project, execContext, subMonitor );
				}
			}
			
			subMonitor.worked( 1 );
		}
	}
	
	private void gmmNatureDisabled( final Collection<IProject> p_projects,
									final IProgressMonitor p_monitor ) 
	throws CoreException, ResourceAccessException, FunctionalException {
		final Set<IProject> allProjects = PluginUtil.getReferencedProjects( p_projects );
		PluginUtil.removeNature( allProjects, GmmNature.NATURE_ID );
	}
	
	private class GmmJob extends WorkspaceJob {
		
		private final Collection<IProject> projects;
		
		public GmmJob( final Collection<IProject> p_projects ) {
			super( "Global Model Management" );
			
			projects = p_projects;
			
			setUser( true );
			setSystem( true );
		}
		
		
		@Override
		public IStatus runInWorkspace( final IProgressMonitor p_monitor )
		throws CoreException {
			p_monitor.beginTask( "Applying global model management of projects...", 1 );
			
			try {
				gmmNatureEnabled( projects, p_monitor );
				
				p_monitor.worked( 1 );
			}
			catch( final ResourceAccessException p_ex ) {
				throw new CoreException( new Status( IStatus.ERROR, GmmPlugin.getDefault().getBundle().getSymbolicName(), null, p_ex ) );
			}
			catch( final FunctionalException p_ex ) {
				throw new CoreException( new Status( IStatus.ERROR, GmmPlugin.getDefault().getBundle().getSymbolicName(), null, p_ex ) );
			}
			finally {
				p_monitor.done();
			}
			
			return Status.OK_STATUS;
		}
	}
	
	public void gmmNatureDisablingRequested( 	final Collection<IProject> p_projects,
												final boolean pb_execInWorkspaceOp ) {
		try {
			if ( pb_execInWorkspaceOp ) {
				final WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {
					
					@Override
					protected void execute( final IProgressMonitor p_monitor )
					throws CoreException, InvocationTargetException, InterruptedException {
						p_monitor.beginTask( "Applying global model management of projects...", 1 );
						
						try {
							gmmNatureDisabled( p_projects, p_monitor );
							
							p_monitor.worked( 1 );
						}
						catch( final ResourceAccessException p_ex ) {
							throw new InvocationTargetException( p_ex );
						}
						catch( final FunctionalException p_ex ) {
							throw new InvocationTargetException( p_ex );
						}
						finally {
							p_monitor.done();
						}
					}
				};
		
				new ProgressMonitorDialog( PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell() ).run( true, false, operation );
			}
			else {
				gmmNatureDisabled( p_projects, new NullProgressMonitor() );
			}
		}
		catch( final Throwable p_th ) {
			exceptionHandler.handleException( p_th, p_projects );
		}
	}
	
	public void gmmNatureEnablingRequested( final Collection<IProject> p_projects,
											final boolean pb_execInWorkspaceOp ) {
		try {
			if ( pb_execInWorkspaceOp ) {
//				final WorkspaceJob job = new GmmJob( p_projects );
//				job.schedule();
				final WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {
					
					@Override
					protected void execute( final IProgressMonitor p_monitor )
					throws CoreException, InvocationTargetException, InterruptedException {
						p_monitor.beginTask( "Applying global model management of projects...", 1 );
						
						try {
							gmmNatureEnabled( p_projects, p_monitor );
							
							p_monitor.worked( 1 );
						}
						catch( final ResourceAccessException p_ex ) {
							throw new InvocationTargetException( p_ex );
						}
						catch( final FunctionalException p_ex ) {
							throw new InvocationTargetException( p_ex );
						}
						finally {
							p_monitor.done();
						}
					}
				};

				new ProgressMonitorDialog( PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell() ).run( true, false, operation );
			}
			else {
				gmmNatureEnabled( p_projects, new NullProgressMonitor() );
			}	
		}
		catch( final Throwable p_th ) {
			exceptionHandler.handleException( p_th, p_projects );
		}
	}

	public void manageResources( final IProject p_project ) {
		try {
			manageResources( p_project, createExecutionContext( false, OperationType.READ ), new NullProgressMonitor() );
		} 
		catch ( final Throwable p_th ) {
			exceptionHandler.handleException( p_th, p_project );
		}
	}

	private void manageResources( 	final IProject p_project,
									final ExecutionContext p_execContext,
									final IProgressMonitor p_monitor ) 
	throws CoreException, ResourceAccessException, FunctionalException {
		final ExtensionsResourcesVisitor visitor = new ExtensionsResourcesVisitor( null );
		p_project.accept( visitor );
		final List<Resource> emfRes = new ArrayList<Resource>();
		final ResourceSet resSet = createResourceSet();
	
		for ( final IResource resource : visitor.getFoundResources() ) {
			if ( resource instanceof IFile && EMFUtil.isEMFResource( resource ) ) {
				final IPath path = resource.getFullPath();
				final URI foudResUri = URI.createPlatformResourceURI( path.toString(), true );
			
				if ( isConcerned( foudResUri ) /*&& !hasErrorsOtherThanGmm( resource )*/ ) {
					final Resource concernedRes = resSet.getResource( foudResUri, true );

					if ( concernedRes.getErrors().isEmpty() ) {
						emfRes.add( concernedRes );
					}
				}
			}
		}
		
		// Sort resources according to their dependencies
		Collections.sort( emfRes, resourcesDepComparator );
		manageResources( emfRes, resSet, p_execContext, p_monitor );
	}
	
	private boolean isConcerned( final URI p_resourceUri ) {
		final Model megaModel = getMegaModel();
		
		if ( megaModel != null ) {
	    	for ( final Relation relation : megaModel.getOwnedObligationRelations() ) {
	    		if ( 	relation instanceof MetaModelRelatedRelation && 
	    				( (MetaModelRelatedRelation) relation ).concerns( p_resourceUri ) ) {
	    			return true;
	    		}
	    	}
		}
		
		return false;
	}
	
	private void manageResources( 	final Collection<Resource> p_resources,
									final ResourceSet p_resSet,
									final ExecutionContext p_execContext,
									final IProgressMonitor p_monitor ) 
	throws CoreException, ResourceAccessException {
		SubMonitor.convert( p_monitor, p_resources.size() );

		// Perform GMM on all project resources (create corresponding resource if it does not exists).
		for ( final Resource resource : p_resources ) {
			final URI managedResUri = resource.getURI();
			processChangedResource( managedResUri, p_resSet, p_execContext );
			
			p_monitor.worked( 1 );
		}
	}
	
	private String getText( final Object p_object ) {
		if ( labelProvider == null ) {
			final ComposedAdapterFactory composedAdapterFactory = new ComposedAdapterFactory( ComposedAdapterFactory.Descriptor.Registry.INSTANCE );
			labelProvider =  new AdapterFactoryLabelProvider( composedAdapterFactory );
		}
		
		return labelProvider.getText( p_object );
	}

	private void deleteInconsistencyMarkers( 	final String p_relationName,
												final IResource p_resource ) 
	throws CoreException {
		if ( p_resource.exists() ) {
			for ( final IMarker marker : p_resource.findMarkers( GMM_MARKER_TYPE, true, IResource.DEPTH_ONE ) ) {
				if ( marker.exists() && p_relationName.equals( marker.getAttribute( IMarker.SOURCE_ID ) ) ) {
					marker.delete();
				}
			}
		}
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled( final boolean pb_enabled ) {
		this.enabled = pb_enabled;
	}

	public Set<SubsettedMetaModel> allSubsettedMetaModels( final Collection<IResource> p_selectedRes ) {
		final Set<SubsettedMetaModel> metaModels = new HashSet<SubsettedMetaModel>();
		final Model megaModel = getMegaModel();
		
		if ( megaModel != null ) {
			EMFUtil.fillContentOfTypes( megaModel,
										false, 
										Collections.singletonList( MegamodelPackage.Literals.SUBSETTED_META_MODEL ),
										metaModels,
										false );
			
			final Iterator<SubsettedMetaModel> metaModelsIt = metaModels.iterator();
			
			while( metaModelsIt.hasNext() ) {
				final SubsettedMetaModel metaModel = metaModelsIt.next();
				final MetaModel rootMm = metaModel.getRootMetaModel();
				boolean declares = false;
				
				for ( final IResource res : p_selectedRes ) {
					final URI uri = URI.createPlatformResourceURI( res.getFullPath().toString(), true );
					if ( rootMm.declares( uri ) && !metaModel.declares( uri ) ) {
						declares = true;
						
						break;
					}
				}
				
				if ( !declares ) {
					metaModelsIt.remove();
				}
			}
		}
		
		return metaModels;
	}
	
	public Set<ObligationRelation> allConcerningRelations( final Collection<IResource> p_selectedRes ) {
		final Set<ObligationRelation> applicableRelations = new HashSet<ObligationRelation>();
		final Collection<ObligationRelation> allRelations = new ArrayList<ObligationRelation>();
		final Model megaModel = getMegaModel();
		
		if ( megaModel != null ) {
			EMFUtil.fillContentOfTypes( megaModel,
										false, 
										Collections.singletonList( MegamodelPackage.Literals.OBLIGATION_RELATION ),
										allRelations,
										false );
			
			for ( final IResource resource : p_selectedRes ) {
				try {
					applicableRelations.addAll( applicableRelations( resource, allRelations ) );
				}
				catch( final CoreException p_ex ) {
					p_ex.printStackTrace();
				}
			}
		}
		
		return applicableRelations;
	}
	
	private Set<ObligationRelation> applicableRelations( 	final IResource p_resource,
															final Collection<ObligationRelation> p_relations )
	throws CoreException {
		final Set<ObligationRelation> applicableRelations = new HashSet<ObligationRelation>();
		
		if ( p_resource.isAccessible() ) {
			if ( p_resource instanceof IContainer ) {
				for ( IResource content : ( (IContainer) p_resource ).members() ) {
					applicableRelations.addAll( applicableRelations( content, p_relations ) );
				}
			}
			else if ( p_resource instanceof IFile && EMFUtil.isEMFResource( p_resource.getFileExtension() ) ) {
				final URI uri = URI.createPlatformResourceURI( p_resource.getFullPath().toString(), true );
				
				for ( final ObligationRelation relation : p_relations ) {
					if ( relation instanceof MetaModelRelatedRelation ) {
						if ( ( (MetaModelRelatedRelation) relation ).concerns( uri ) ) {
							applicableRelations.add( relation );
						}
					}
					else {
						applicableRelations.add( relation );
					}
				}
			}
		}
		
		return applicableRelations;
	}
}
