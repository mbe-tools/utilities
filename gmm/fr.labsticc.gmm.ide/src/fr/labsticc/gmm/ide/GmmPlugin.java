package fr.labsticc.gmm.ide;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.labsticc.framework.core.exception.FunctionalException;
import fr.labsticc.framework.core.exception.IExceptionHandler;
import fr.labsticc.framework.core.exception.ResourceAccessException;
import fr.labsticc.framework.core.exception.SystemException;
import fr.labsticc.framework.emf.service.EMFValidationAS;
import fr.labsticc.framework.emf.service.IEMFValidationAS;
import fr.labsticc.framework.emf.view.ide.ConfigurationModelManager;
import fr.labsticc.framework.ide.log.DialogExceptionHandler;
import fr.labsticc.framework.ide.util.PluginUtil;
import fr.labsticc.gmm.model.megamodel.GmmSpecification;
import fr.labsticc.gmm.model.megamodel.MegamodelFactory;
import fr.labsticc.gmm.model.megamodel.Model;

public class GmmPlugin extends AbstractUIPlugin /*implements IStartup*/ {

	private static final String MEGA_MODEL_DEF_LOC_EXT_ID = "megaModelDefaultLocation";
	private static final String MODEL_LOC_ELEM = "modelLocation";
	private static final String MODEL_LOC_URI_ATT = "uri";
	private static final String RESOURCESET_TYPE_EXT_ID = "resourceSetType";
	private static final String RESOURCESET_TYPE_ELEM = "resourceSetType";

	// The shared instance
	private static GmmPlugin plugin;

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static GmmPlugin getDefault() {
		return plugin;
	}
	
	private IExceptionHandler exceptionHandler; 

	private Logger logger;
	
	private GmmSpecification megaModelSpec;
	
	private GmmManager gmmManager;

	/**
	 * The constructor
	 */
	public GmmPlugin() {
		try {
			logger = LoggerFactory.getLogger( getClass() );
			exceptionHandler = new DialogExceptionHandler( logger ) {
				@Override
				public void handleException( 	final Throwable p_th, 
												final Object p_info) {
					p_th.printStackTrace();
					getLog().log( new Status( IStatus.ERROR, getBundle().getSymbolicName(), null, p_th ) );
					super.handleException( p_th, p_info );
				}

				@Override
				public void handleException( 	final SystemException p_ex, 
												final Object p_info ) {
					p_ex.printStackTrace();
					getLog().log( new Status( IStatus.ERROR, getBundle().getSymbolicName(), null, p_ex ) );

					super.handleException( p_ex, p_info );
				}

				@Override
				public void handleException( 	final FunctionalException p_ex, 
												final Object p_info ) {
					p_ex.printStackTrace();
					getLog().log( new Status( IStatus.ERROR, getBundle().getSymbolicName(), null, p_ex ) );

					super.handleException( p_ex, p_info );
				}
			};
			megaModelSpec = null;
			gmmManager = null;
		}
		catch( final Throwable p_th ) {
			p_th.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start( final BundleContext p_context )
	throws Exception {
		System.out.println( "Starting GMM plugin... " );
		super.start( p_context );
	
		plugin = this;
		
		try {
//			final IWorkspaceRunnable myRunnable = new IWorkspaceRunnable() {
//				@Override
//				public void run(IProgressMonitor monitor)
//				throws CoreException {
//					try {
						reloadMegaModel();
						gmmManager = new GmmManager( exceptionHandler, getResourceSet() );
						
						logger.info( getClass().getSimpleName() + " started." );
//					}
//					catch ( final FunctionalException p_ex ) {
//						throw new CoreException( new Status( IStatus.ERROR, getBundle().getSymbolicName(), null, p_ex ) );
//					}
//					catch ( final SystemException p_ex ) {
//						throw new CoreException( new Status( IStatus.ERROR, getBundle().getSymbolicName(), null, p_ex ) );
//					}
//					catch ( final IOException p_ex ) {
//						throw new CoreException( new Status( IStatus.ERROR, getBundle().getSymbolicName(), null, p_ex ) );
//					}
//				}
//			};
			
//			final IWorkspace workspace = ResourcesPlugin.getWorkspace();
//			workspace.run( myRunnable, null, IWorkspace.AVOID_UPDATE, null );

			System.out.println( "GMM plugin started." );
		}
		catch ( final Exception p_ex ) {
			logger.error( toString(), p_ex );
			
			throw p_ex;
		}
	}
	
	public ResourceSet getResourceSet() 
	throws CoreException {
		final IExtension[] extensions = PluginUtil.getExtensionPointExtensions( getBundle().getSymbolicName(), RESOURCESET_TYPE_EXT_ID );
		
		if ( extensions != null ) {
			for( final IExtension extension : extensions ) {
				for( final IConfigurationElement confElem : extension.getConfigurationElements() ) {
					if ( RESOURCESET_TYPE_ELEM.equals ( confElem.getName() ) ) {
						return (ResourceSet) confElem.createExecutableExtension( "class" );
					}
				}
			}
		}

		return null;
	}

	private GmmSpecification initMegaModelSpec()
	throws ResourceAccessException, IOException {
		final IPreferenceStore prefStore = getPreferenceStore();
		final String location = prefStore.getString( IGmmPreferenceConstants.DEFAULT_MEGA_MODEL_LOCATION_KEY );
		
		if ( location == null || "".equals( location ) ) {
			throw new NullPointerException( "Default settings model location should not be null." );
		}

		final Model defaultMegaModel = MegamodelFactory.eINSTANCE.createGmmSpecification();
		defaultMegaModel.setName( "Empty Mega Model" );
		defaultMegaModel.setDescription( "This mega-model was automatically created because no mega-model was specified or it did not exists." );
		defaultMegaModel.setDescription( "Empty Mega Model" );
		
		final GmmSpecification megaModelSpec = (GmmSpecification) new ConfigurationModelManager().initModel( location, defaultMegaModel, getDefaultMegaModelUris() );
		getPreferenceStore().setDefault( IGmmPreferenceConstants.DEFAULT_MEGA_MODEL_LOCATION_KEY, megaModelSpec.eResource().getURI().toString() );
		
		return megaModelSpec;
	}
	
	private void validateMegaModel() 
	throws SystemException, FunctionalException {
		final IEMFValidationAS validationService = new EMFValidationAS();
		
		try {
			validationService.validate( megaModelSpec );
		}
		catch( FunctionalException p_ex ) {
			p_ex.printStackTrace();
		}
		catch( final InterruptedException p_ex ) {
		}
	}
	
	private Collection<URI> getDefaultMegaModelUris() {
		final Collection<URI> uris = new ArrayList<URI>();
		final IExtension[] extensions = PluginUtil.getExtensionPointExtensions( getBundle(), MEGA_MODEL_DEF_LOC_EXT_ID );
		
		if ( extensions != null ) {
			for( final IExtension extension : PluginUtil.sortExtensionsByDependencies( extensions ) ) {
				for( final IConfigurationElement confElem : extension.getConfigurationElements() ) {
					if ( MODEL_LOC_ELEM.equals ( confElem.getName() ) ) {
						uris.add( URI.createURI( confElem.getAttribute( MODEL_LOC_URI_ATT ) ) );
					}
				}
			}
		}

		return uris;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop( final BundleContext p_context )
	throws Exception {
		plugin = null;
		megaModelSpec = null;
		gmmManager = null;
		
		super.stop( p_context );
	}
	
	public void reloadMegaModel() 
	throws FunctionalException, SystemException, IOException {
		megaModelSpec = initMegaModelSpec();
		validateMegaModel();
	}

	public IExceptionHandler getExceptionHandler() {
		return exceptionHandler;
	}

	public Model getMegaModel() {
		return megaModelSpec == null ? null : megaModelSpec.getOwnedModels().isEmpty() ? null : megaModelSpec.getOwnedModels().get( 0 );
	}

	public GmmManager getGmmManager() {
		return gmmManager;
	}
}
