package fr.labsticc.gmm.ide.subsets;

import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;

import fr.labsticc.framework.constraints.ide.IDEConstraintExpressionServicesDelegate;
import fr.labsticc.gmm.ide.GmmPlugin;
import fr.labsticc.gmm.service.GmmService;

public aspect SubsetsManagement {
	
	private final GmmService gmmService;
	
	public SubsetsManagement()
	throws CoreException {
		gmmService = new GmmService( 	new IDEConstraintExpressionServicesDelegate(),
										new AdapterFactoryLabelProvider( new ComposedAdapterFactory( ComposedAdapterFactory.Descriptor.Registry.INSTANCE ) ) );
	}

	pointcut validatePointCut( EValidator p_validator, EClass p_eClass, EObject p_eObject, DiagnosticChain p_diagnostics, Map<Object, Object> p_context ) : 
		execution( public boolean EValidator.validate( EClass, EObject, DiagnosticChain, Map<Object, Object> ) )
		&& target( p_validator )
		&& args( p_eClass, p_eObject, p_diagnostics, p_context );

    after( EValidator p_validator, EClass p_eClass, EObject p_eObject, DiagnosticChain p_diagnostics, Map<Object, Object> p_context ) 
    	returning : validatePointCut( p_validator, p_eClass, p_eObject, p_diagnostics, p_context ) {
    	
    	if ( p_context.get( p_eObject ) != this ) {
    		gmmService.validate( p_validator, GmmPlugin.getDefault().getMegaModel(), p_eObject, p_diagnostics, p_context );
    		
    		// Avoid calling this validation more than once on the same model element.
    		p_context.put( p_eObject, this );
    	}
    }
}
