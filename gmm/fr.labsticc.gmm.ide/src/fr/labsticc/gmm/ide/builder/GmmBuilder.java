package fr.labsticc.gmm.ide.builder;

import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import fr.labsticc.framework.core.exception.IExceptionHandler;
import fr.labsticc.gmm.ide.GmmManager;
import fr.labsticc.gmm.ide.GmmPlugin;
import fr.labsticc.gmm.ide.GmmResourceDeltaVisitor;

public class GmmBuilder extends IncrementalProjectBuilder {

	public static final String BUILDER_ID = "fr.labsticc.gmm.ide.gmmBuilder";

	//private final IExceptionHandler exceptionHandler;
	
	public GmmBuilder() {
	}

	private GmmManager getGmmManager() {
		return GmmPlugin.getDefault().getGmmManager();
	}

	private IExceptionHandler getExceptionHandler() {
		return GmmPlugin.getDefault().getExceptionHandler();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.internal.events.InternalBuilder#build(int,
	 *      java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	protected IProject[] build(int kind, @SuppressWarnings("rawtypes") Map args, IProgressMonitor monitor)
	throws CoreException {
		if (kind == FULL_BUILD) {
			fullBuild(monitor);
		} 
		else {
			IResourceDelta delta = getDelta(getProject());
			if (delta == null) {
				fullBuild(monitor);
			} 
			else {
				incrementalBuild(delta, monitor);
			}
		}
		return null;
	}

	/**
	 * The GMM full build consists of establishing the validity for the read operation of all resource of the project if it has the GMM nature.
	 * This is to be interpreted per relation. For a synchronization relation, it consists of checking the consistency of the resources for 
	 * all relations that concern them.
	 * @param p_monitor
	 * @throws CoreException
	 */
	protected void fullBuild(final IProgressMonitor p_monitor )
	throws CoreException {
		final IProject project = getProject();
		
		if ( project.hasNature( GmmNature.NATURE_ID ) ) {
			// TODO: uncomment when SD match performance issues are fixed getGmmManager().manageResources( project );
		}
	}

	protected void incrementalBuild(IResourceDelta delta,
									IProgressMonitor monitor)
	throws CoreException {
		
		//the visitor does the work.
		delta.accept( new GmmResourceDeltaVisitor( getGmmManager(), getExceptionHandler() ) );
	}
}
