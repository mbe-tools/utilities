/**
 */
package fr.labsticc.gmm.editoradapter.model.editoradapter;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage
 * @generated
 */
public interface EditoradapterFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EditoradapterFactory eINSTANCE = fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditoradapterFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Editing Domain Provider Editor Adapter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Editing Domain Provider Editor Adapter</em>'.
	 * @generated
	 */
	EditingDomainProviderEditorAdapter createEditingDomainProviderEditorAdapter();

	/**
	 * Returns a new object of class '<em>Editor Adapter Tag</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Editor Adapter Tag</em>'.
	 * @generated
	 */
	EditorAdapterTag createEditorAdapterTag();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	EditoradapterPackage getEditoradapterPackage();

} //EditoradapterFactory
