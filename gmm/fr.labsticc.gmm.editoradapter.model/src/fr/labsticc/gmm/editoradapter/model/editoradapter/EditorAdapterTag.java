/**
 */
package fr.labsticc.gmm.editoradapter.model.editoradapter;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Editor Adapter Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The editorAdapterTag stores information about a editorAdapter.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getEditorAdapter <em>Editor Adapter</em>}</li>
 *   <li>{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getEditorAdapterID <em>Editor Adapter ID</em>}</li>
 *   <li>{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getModelID <em>Model ID</em>}</li>
 *   <li>{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getEditorAdapterPackageNsURI <em>Editor Adapter Package Ns URI</em>}</li>
 *   <li>{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getCreationMethodName <em>Creation Method Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage#getEditorAdapterTag()
 * @model
 * @generated
 */
public interface EditorAdapterTag extends EObject {
	/**
	 * Returns the value of the '<em><b>Editor Adapter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor Adapter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editor Adapter</em>' reference.
	 * @see #setEditorAdapter(EditorAdapter)
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage#getEditorAdapterTag_EditorAdapter()
	 * @model resolveProxies="false" ordered="false"
	 * @generated
	 */
	EditorAdapter getEditorAdapter();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getEditorAdapter <em>Editor Adapter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editor Adapter</em>' reference.
	 * @see #getEditorAdapter()
	 * @generated
	 */
	void setEditorAdapter(EditorAdapter value);

	/**
	 * Returns the value of the '<em><b>Editor Adapter ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor Adapter ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editor Adapter ID</em>' attribute.
	 * @see #setEditorAdapterID(String)
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage#getEditorAdapterTag_EditorAdapterID()
	 * @model
	 * @generated
	 */
	String getEditorAdapterID();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getEditorAdapterID <em>Editor Adapter ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editor Adapter ID</em>' attribute.
	 * @see #getEditorAdapterID()
	 * @generated
	 */
	void setEditorAdapterID(String value);

	/**
	 * Returns the value of the '<em><b>Model ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model ID</em>' attribute.
	 * @see #setModelID(String)
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage#getEditorAdapterTag_ModelID()
	 * @model
	 * @generated
	 */
	String getModelID();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getModelID <em>Model ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model ID</em>' attribute.
	 * @see #getModelID()
	 * @generated
	 */
	void setModelID(String value);

	/**
	 * Returns the value of the '<em><b>Editor Adapter Package Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor Adapter Package Ns URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editor Adapter Package Ns URI</em>' attribute.
	 * @see #setEditorAdapterPackageNsURI(String)
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage#getEditorAdapterTag_EditorAdapterPackageNsURI()
	 * @model
	 * @generated
	 */
	String getEditorAdapterPackageNsURI();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getEditorAdapterPackageNsURI <em>Editor Adapter Package Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editor Adapter Package Ns URI</em>' attribute.
	 * @see #getEditorAdapterPackageNsURI()
	 * @generated
	 */
	void setEditorAdapterPackageNsURI(String value);

	/**
	 * Returns the value of the '<em><b>Creation Method Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Creation Method Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Creation Method Name</em>' attribute.
	 * @see #setCreationMethodName(String)
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage#getEditorAdapterTag_CreationMethodName()
	 * @model
	 * @generated
	 */
	String getCreationMethodName();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getCreationMethodName <em>Creation Method Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Creation Method Name</em>' attribute.
	 * @see #getCreationMethodName()
	 * @generated
	 */
	void setCreationMethodName(String value);

} // EditorAdapterTag
