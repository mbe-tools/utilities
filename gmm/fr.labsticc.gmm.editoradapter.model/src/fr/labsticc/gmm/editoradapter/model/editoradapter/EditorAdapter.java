/**
 */
package fr.labsticc.gmm.editoradapter.model.editoradapter;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.Resource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Editor Adapter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * DiagramAdapter is the abstract base class for all diagram adapters. Diagram adapters allow the transformation engine to access a modeling tool's memory. If the modeling tool is another Eclipse editor, the diagram adapter can simply access the editor's resource and return it to the transformation engine. If the modeling tool is an external application, the diagram adapter needs to keep an own resource object to store the EMF model. The methods load() and save() are then used to read and write the model to the external application.
 * <!-- end-model-doc -->
 *
 *
 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage#getEditorAdapter()
 * @model abstract="true"
 * @generated
 */
public interface EditorAdapter extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method returns the resource object that contains the model indicated by the uri parameter. This method may also open the file first with an appropriate editor within Eclipse and return the editor's resource object. Diagram adapters for external tools may perform other tasks.
	 * <!-- end-model-doc -->
	 * @model uriDataType="fr.labsticc.gmm.editoradapter.model.editoradapter.URI"
	 * @generated
	 */
	Resource getEditorResource(URI uri);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method is called by the transformation engine before the transformation. The diagram adapter should then read the model from the external tool if necessary.
	 * <!-- end-model-doc -->
	 * @model uriDataType="fr.labsticc.gmm.editoradapter.model.editoradapter.URI"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='//The default implementation does nothing.'"
	 * @generated
	 */
	void load(URI uri);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method is called by the transformation engine after the transformation. The EMF model should then be transferred to the external tool if necessary.
	 * <!-- end-model-doc -->
	 * @model uriDataType="fr.labsticc.gmm.editoradapter.model.editoradapter.URI"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='//The default implementation does nothing.\r\nreturn false;'"
	 * @generated
	 */
	boolean save(URI uri, boolean restoreEditor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method returns the resource object that contains the model indicated by the uri parameter. This method may also open the file first with an appropriate editor within Eclipse and return the editor's resource object. Diagram adapters for external tools may perform other tasks.
	 * <!-- end-model-doc -->
	 * @model uriDataType="fr.labsticc.gmm.editoradapter.model.editoradapter.URI" modifyRequired="true"
	 * @generated
	 */
	Resource getEditorResource(URI uri, boolean modify);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model elementRequired="true"
	 * @generated
	 */
	Object getMarkerElementId(EObject element);

} // EditorAdapter
