/**
 */
package fr.labsticc.gmm.editoradapter.model.editoradapter.impl;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import fr.labsticc.framework.core.util.ObjectReturnRunnable;
import fr.labsticc.framework.emf.view.ide.EMFEditorUtil;
import fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapter;
import fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Editor Adapter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class EditorAdapterImpl extends EObjectImpl implements EditorAdapter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EditorAdapterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditoradapterPackage.Literals.EDITOR_ADAPTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Resource getEditorResource( final URI p_uri ) {
		return getEditorResource( p_uri, true );
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void load(final URI uri) {
		//The default implementation does nothing.
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean save(final URI uri, final boolean restoreEditor) {
		//The default implementation does nothing.
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource getEditorResource(URI uri, boolean modify) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}
	
	protected EObject getEditorObject( final EObject p_element ) {
		if ( p_element == null || p_element.eResource() == null ) {
			return p_element;
		}

		final URI resUri = p_element.eResource().getURI();
		final Resource edRes = getEditorResource( resUri, false );
		
		if ( edRes == null ) {
			return p_element;
		}
		
		
		final URI eobjUri = EcoreUtil.getURI( p_element );
		
		if ( eobjUri == null ) {
			return p_element;
		}

		return edRes.getEObject( eobjUri.fragment() );
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object getMarkerElementId(EObject p_element) {
		return getEditorObject( p_element );
	}

	protected IEditorPart findFirstEditor( 	final URI p_uri,
											final boolean pb_restore ) {
		final String editorInputName = EMFEditorUtil.getEditorInputName( p_uri );
		final IEditorPart activeEditor = findActiveEditorPart();
		
		if ( activeEditor != null && editorInputName.equals( EMFEditorUtil.getResourceName( activeEditor ) ) ) {
			return activeEditor;
		}

		return findFirstEditor( editorInputName, pb_restore );
	}

	private IEditorPart findFirstEditor( 	final String p_editorInputName,
											final boolean pb_restore ) {
		for ( final IWorkbenchWindow window : PlatformUI.getWorkbench().getWorkbenchWindows() ) {
		    for ( final IWorkbenchPage page : window.getPages() ) {
		        for ( final IEditorReference editorRef : page.getEditorReferences() ) {
			        final IEditorPart currEditor = editorRef.getEditor( pb_restore );
			        
			        if ( 	currEditor != null && 
			        		p_editorInputName.equals( EMFEditorUtil.getResourceName( currEditor ) ) ) {
			        	return currEditor;
			        }
		        }
		    }
		}
		
		return null;
	}
	
	protected IEditorPart findActiveEditorPart() {
		final ObjectReturnRunnable runnable = new ObjectReturnRunnable() {
			
			@Override
			public void run() {
				final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				
				if ( window == null ) {
					result = null;
				}
				else {
		    		final IWorkbenchPage page = window.getActivePage();
		    		
		    		if ( page != null ) {
		    			result = page.getActiveEditor();
		    		}
		    		else {
		    			result = null;
		    		}
				}
			}
		};
		
    	Display.getDefault().syncExec( runnable );
    	
    	return (IEditorPart) runnable.result;
	}
} //EditorAdapterImpl
