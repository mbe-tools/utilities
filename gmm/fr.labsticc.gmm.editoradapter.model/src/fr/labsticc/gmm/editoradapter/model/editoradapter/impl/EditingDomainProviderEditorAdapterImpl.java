/**
 */
package fr.labsticc.gmm.editoradapter.model.editoradapter.impl;

import java.io.IOException;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.command.IdentityCommand;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISaveablePart;

import fr.labsticc.gmm.editoradapter.model.editoradapter.EditingDomainProviderEditorAdapter;
import fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Emf Generated Editor Adapter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class EditingDomainProviderEditorAdapterImpl extends EditorAdapterImpl implements
		EditingDomainProviderEditorAdapter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EditingDomainProviderEditorAdapterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditoradapterPackage.Literals.EDITING_DOMAIN_PROVIDER_EDITOR_ADAPTER;
	}

	protected Resource getResource( final URI p_resUri,
									final IEditorPart p_editor,
									final boolean pb_modify ) {
		if ( p_editor instanceof IEditingDomainProvider ) {
			final EditingDomain editingDomain = ( (IEditingDomainProvider) p_editor ).getEditingDomain();
			
			if ( editingDomain != null ) {
				if ( pb_modify ) {
	
					// Execute the identity command to make the editor dirty
					editingDomain.getCommandStack().execute( new IdentityCommand() );
				}
	
				// Get the root object of the editor
				return editingDomain.getResourceSet().getResource( p_resUri, true );
			}
		}

		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public Resource getEditorResource(	final URI p_uri,
										final boolean pb_modify ) {
		final IEditorPart editorPart = findFirstEditor( p_uri, false );

		return getResource( p_uri, editorPart, pb_modify );
	}
	
	protected boolean save( final URI p_uri,
							final IEditorPart p_editorPart ) {
		if ( p_editorPart instanceof IEditingDomainProvider ) {
			final EditingDomain editingDomain = ( (IEditingDomainProvider) p_editorPart ).getEditingDomain();

			if ( p_editorPart instanceof ISaveablePart ) {
				Display.getDefault().asyncExec( new Runnable() {
					
					@Override
					public void run() {
						( (ISaveablePart) p_editorPart ).doSave( new NullProgressMonitor() );
						makeEditorUndirty( editingDomain );
					}
				});
				
				return true;
			}
				
			if ( editingDomain != null ) {
				final Resource resource = editingDomain.getResourceSet().getResource( p_uri, false );
				
				if ( resource != null ) {
					try {
						resource.save( null );
						makeEditorUndirty( editingDomain );
						
						return true;
					}
					catch ( final IOException p_ex ) {
						p_ex.printStackTrace();
					}
				}
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean save(	final URI p_uri,
							final boolean pb_restore ) {
		final IEditorPart editorPart = findFirstEditor( p_uri, pb_restore );
		
		return save( p_uri, editorPart );
	}
	
	private void makeEditorUndirty( final EditingDomain p_editingDomain ) {
		if ( p_editingDomain != null ) {
			final CommandStack commandStack = p_editingDomain.getCommandStack();
			
			if ( commandStack instanceof BasicCommandStack ) {
				( (BasicCommandStack) commandStack ).saveIsDone();
			}
		}
	}
} //EmfGeneratedEditorAdapterImpl
