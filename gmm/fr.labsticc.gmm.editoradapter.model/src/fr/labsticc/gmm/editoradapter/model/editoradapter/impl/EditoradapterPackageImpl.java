/**
 */
package fr.labsticc.gmm.editoradapter.model.editoradapter.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapter;
import fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag;
import fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterFactory;
import fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage;
import fr.labsticc.gmm.editoradapter.model.editoradapter.EditingDomainProviderEditorAdapter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EditoradapterPackageImpl extends EPackageImpl implements EditoradapterPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass editorAdapterEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass editingDomainProviderEditorAdapterEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass editorAdapterTagEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType uriEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EditoradapterPackageImpl() {
		super(eNS_URI, EditoradapterFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link EditoradapterPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EditoradapterPackage init() {
		if (isInited) return (EditoradapterPackage)EPackage.Registry.INSTANCE.getEPackage(EditoradapterPackage.eNS_URI);

		// Obtain or create and register package
		EditoradapterPackageImpl theEditoradapterPackage = (EditoradapterPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof EditoradapterPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new EditoradapterPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theEditoradapterPackage.createPackageContents();

		// Initialize created meta-data
		theEditoradapterPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theEditoradapterPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(EditoradapterPackage.eNS_URI, theEditoradapterPackage);
		return theEditoradapterPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEditorAdapter() {
		return editorAdapterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEditingDomainProviderEditorAdapter() {
		return editingDomainProviderEditorAdapterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEditorAdapterTag() {
		return editorAdapterTagEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEditorAdapterTag_EditorAdapter() {
		return (EReference)editorAdapterTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEditorAdapterTag_EditorAdapterID() {
		return (EAttribute)editorAdapterTagEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEditorAdapterTag_ModelID() {
		return (EAttribute)editorAdapterTagEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEditorAdapterTag_EditorAdapterPackageNsURI() {
		return (EAttribute)editorAdapterTagEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEditorAdapterTag_CreationMethodName() {
		return (EAttribute)editorAdapterTagEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getURI() {
		return uriEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditoradapterFactory getEditoradapterFactory() {
		return (EditoradapterFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		editorAdapterEClass = createEClass(EDITOR_ADAPTER);

		editingDomainProviderEditorAdapterEClass = createEClass(EDITING_DOMAIN_PROVIDER_EDITOR_ADAPTER);

		editorAdapterTagEClass = createEClass(EDITOR_ADAPTER_TAG);
		createEReference(editorAdapterTagEClass, EDITOR_ADAPTER_TAG__EDITOR_ADAPTER);
		createEAttribute(editorAdapterTagEClass, EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_ID);
		createEAttribute(editorAdapterTagEClass, EDITOR_ADAPTER_TAG__MODEL_ID);
		createEAttribute(editorAdapterTagEClass, EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_PACKAGE_NS_URI);
		createEAttribute(editorAdapterTagEClass, EDITOR_ADAPTER_TAG__CREATION_METHOD_NAME);

		// Create data types
		uriEDataType = createEDataType(URI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		editingDomainProviderEditorAdapterEClass.getESuperTypes().add(this.getEditorAdapter());

		// Initialize classes and features; add operations and parameters
		initEClass(editorAdapterEClass, EditorAdapter.class, "EditorAdapter", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(editorAdapterEClass, ecorePackage.getEResource(), "getEditorResource", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "uri", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(editorAdapterEClass, null, "load", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "uri", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(editorAdapterEClass, ecorePackage.getEBoolean(), "save", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "uri", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "restoreEditor", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(editorAdapterEClass, ecorePackage.getEResource(), "getEditorResource", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "uri", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "modify", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(editorAdapterEClass, ecorePackage.getEJavaObject(), "getMarkerElementId", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "element", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(editingDomainProviderEditorAdapterEClass, EditingDomainProviderEditorAdapter.class, "EditingDomainProviderEditorAdapter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(editorAdapterTagEClass, EditorAdapterTag.class, "EditorAdapterTag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEditorAdapterTag_EditorAdapter(), this.getEditorAdapter(), null, "editorAdapter", null, 0, 1, EditorAdapterTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getEditorAdapterTag_EditorAdapterID(), ecorePackage.getEString(), "editorAdapterID", null, 0, 1, EditorAdapterTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEditorAdapterTag_ModelID(), ecorePackage.getEString(), "modelID", null, 0, 1, EditorAdapterTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEditorAdapterTag_EditorAdapterPackageNsURI(), ecorePackage.getEString(), "editorAdapterPackageNsURI", null, 0, 1, EditorAdapterTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEditorAdapterTag_CreationMethodName(), ecorePackage.getEString(), "creationMethodName", null, 0, 1, EditorAdapterTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize data types
		initEDataType(uriEDataType, org.eclipse.emf.common.util.URI.class, "URI", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //EditoradapterPackageImpl
