/**
 */
package fr.labsticc.gmm.editoradapter.model.editoradapter;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterFactory
 * @model kind="package"
 * @generated
 */
public interface EditoradapterPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "editoradapter";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.labsticc.fr/editoradapter/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "editoradapter";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EditoradapterPackage eINSTANCE = fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditoradapterPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterImpl <em>Editor Adapter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterImpl
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditoradapterPackageImpl#getEditorAdapter()
	 * @generated
	 */
	int EDITOR_ADAPTER = 0;

	/**
	 * The number of structural features of the '<em>Editor Adapter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_ADAPTER_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditingDomainProviderEditorAdapterImpl <em>Editing Domain Provider Editor Adapter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditingDomainProviderEditorAdapterImpl
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditoradapterPackageImpl#getEditingDomainProviderEditorAdapter()
	 * @generated
	 */
	int EDITING_DOMAIN_PROVIDER_EDITOR_ADAPTER = 1;

	/**
	 * The number of structural features of the '<em>Editing Domain Provider Editor Adapter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITING_DOMAIN_PROVIDER_EDITOR_ADAPTER_FEATURE_COUNT = EDITOR_ADAPTER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterTagImpl <em>Editor Adapter Tag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterTagImpl
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditoradapterPackageImpl#getEditorAdapterTag()
	 * @generated
	 */
	int EDITOR_ADAPTER_TAG = 2;

	/**
	 * The feature id for the '<em><b>Editor Adapter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_ADAPTER_TAG__EDITOR_ADAPTER = 0;

	/**
	 * The feature id for the '<em><b>Editor Adapter ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_ID = 1;

	/**
	 * The feature id for the '<em><b>Model ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_ADAPTER_TAG__MODEL_ID = 2;

	/**
	 * The feature id for the '<em><b>Editor Adapter Package Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_PACKAGE_NS_URI = 3;

	/**
	 * The feature id for the '<em><b>Creation Method Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_ADAPTER_TAG__CREATION_METHOD_NAME = 4;

	/**
	 * The number of structural features of the '<em>Editor Adapter Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_ADAPTER_TAG_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '<em>URI</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.URI
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditoradapterPackageImpl#getURI()
	 * @generated
	 */
	int URI = 3;


	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapter <em>Editor Adapter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Editor Adapter</em>'.
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapter
	 * @generated
	 */
	EClass getEditorAdapter();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditingDomainProviderEditorAdapter <em>Editing Domain Provider Editor Adapter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Editing Domain Provider Editor Adapter</em>'.
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditingDomainProviderEditorAdapter
	 * @generated
	 */
	EClass getEditingDomainProviderEditorAdapter();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag <em>Editor Adapter Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Editor Adapter Tag</em>'.
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag
	 * @generated
	 */
	EClass getEditorAdapterTag();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getEditorAdapter <em>Editor Adapter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Editor Adapter</em>'.
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getEditorAdapter()
	 * @see #getEditorAdapterTag()
	 * @generated
	 */
	EReference getEditorAdapterTag_EditorAdapter();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getEditorAdapterID <em>Editor Adapter ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Editor Adapter ID</em>'.
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getEditorAdapterID()
	 * @see #getEditorAdapterTag()
	 * @generated
	 */
	EAttribute getEditorAdapterTag_EditorAdapterID();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getModelID <em>Model ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model ID</em>'.
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getModelID()
	 * @see #getEditorAdapterTag()
	 * @generated
	 */
	EAttribute getEditorAdapterTag_ModelID();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getEditorAdapterPackageNsURI <em>Editor Adapter Package Ns URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Editor Adapter Package Ns URI</em>'.
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getEditorAdapterPackageNsURI()
	 * @see #getEditorAdapterTag()
	 * @generated
	 */
	EAttribute getEditorAdapterTag_EditorAdapterPackageNsURI();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getCreationMethodName <em>Creation Method Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Creation Method Name</em>'.
	 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag#getCreationMethodName()
	 * @see #getEditorAdapterTag()
	 * @generated
	 */
	EAttribute getEditorAdapterTag_CreationMethodName();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.common.util.URI <em>URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>URI</em>'.
	 * @see org.eclipse.emf.common.util.URI
	 * @model instanceClass="org.eclipse.emf.common.util.URI"
	 * @generated
	 */
	EDataType getURI();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EditoradapterFactory getEditoradapterFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterImpl <em>Editor Adapter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterImpl
		 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditoradapterPackageImpl#getEditorAdapter()
		 * @generated
		 */
		EClass EDITOR_ADAPTER = eINSTANCE.getEditorAdapter();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditingDomainProviderEditorAdapterImpl <em>Editing Domain Provider Editor Adapter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditingDomainProviderEditorAdapterImpl
		 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditoradapterPackageImpl#getEditingDomainProviderEditorAdapter()
		 * @generated
		 */
		EClass EDITING_DOMAIN_PROVIDER_EDITOR_ADAPTER = eINSTANCE.getEditingDomainProviderEditorAdapter();

		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterTagImpl <em>Editor Adapter Tag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterTagImpl
		 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditoradapterPackageImpl#getEditorAdapterTag()
		 * @generated
		 */
		EClass EDITOR_ADAPTER_TAG = eINSTANCE.getEditorAdapterTag();

		/**
		 * The meta object literal for the '<em><b>Editor Adapter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDITOR_ADAPTER_TAG__EDITOR_ADAPTER = eINSTANCE.getEditorAdapterTag_EditorAdapter();

		/**
		 * The meta object literal for the '<em><b>Editor Adapter ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_ID = eINSTANCE.getEditorAdapterTag_EditorAdapterID();

		/**
		 * The meta object literal for the '<em><b>Model ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDITOR_ADAPTER_TAG__MODEL_ID = eINSTANCE.getEditorAdapterTag_ModelID();

		/**
		 * The meta object literal for the '<em><b>Editor Adapter Package Ns URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_PACKAGE_NS_URI = eINSTANCE.getEditorAdapterTag_EditorAdapterPackageNsURI();

		/**
		 * The meta object literal for the '<em><b>Creation Method Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDITOR_ADAPTER_TAG__CREATION_METHOD_NAME = eINSTANCE.getEditorAdapterTag_CreationMethodName();

		/**
		 * The meta object literal for the '<em>URI</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.common.util.URI
		 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditoradapterPackageImpl#getURI()
		 * @generated
		 */
		EDataType URI = eINSTANCE.getURI();

	}

} //EditoradapterPackage
