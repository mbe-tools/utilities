/**
 */
package fr.labsticc.gmm.editoradapter.model.editoradapter.impl;

import fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapter;
import fr.labsticc.gmm.editoradapter.model.editoradapter.EditorAdapterTag;
import fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Editor Adapter Tag</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterTagImpl#getEditorAdapter <em>Editor Adapter</em>}</li>
 *   <li>{@link fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterTagImpl#getEditorAdapterID <em>Editor Adapter ID</em>}</li>
 *   <li>{@link fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterTagImpl#getModelID <em>Model ID</em>}</li>
 *   <li>{@link fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterTagImpl#getEditorAdapterPackageNsURI <em>Editor Adapter Package Ns URI</em>}</li>
 *   <li>{@link fr.labsticc.gmm.editoradapter.model.editoradapter.impl.EditorAdapterTagImpl#getCreationMethodName <em>Creation Method Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EditorAdapterTagImpl extends EObjectImpl implements EditorAdapterTag {
	/**
	 * The cached value of the '{@link #getEditorAdapter() <em>Editor Adapter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditorAdapter()
	 * @generated
	 * @ordered
	 */
	protected EditorAdapter editorAdapter;

	/**
	 * The default value of the '{@link #getEditorAdapterID() <em>Editor Adapter ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditorAdapterID()
	 * @generated
	 * @ordered
	 */
	protected static final String EDITOR_ADAPTER_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEditorAdapterID() <em>Editor Adapter ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditorAdapterID()
	 * @generated
	 * @ordered
	 */
	protected String editorAdapterID = EDITOR_ADAPTER_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getModelID() <em>Model ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelID()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelID() <em>Model ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelID()
	 * @generated
	 * @ordered
	 */
	protected String modelID = MODEL_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getEditorAdapterPackageNsURI() <em>Editor Adapter Package Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditorAdapterPackageNsURI()
	 * @generated
	 * @ordered
	 */
	protected static final String EDITOR_ADAPTER_PACKAGE_NS_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEditorAdapterPackageNsURI() <em>Editor Adapter Package Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditorAdapterPackageNsURI()
	 * @generated
	 * @ordered
	 */
	protected String editorAdapterPackageNsURI = EDITOR_ADAPTER_PACKAGE_NS_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getCreationMethodName() <em>Creation Method Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreationMethodName()
	 * @generated
	 * @ordered
	 */
	protected static final String CREATION_METHOD_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCreationMethodName() <em>Creation Method Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreationMethodName()
	 * @generated
	 * @ordered
	 */
	protected String creationMethodName = CREATION_METHOD_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EditorAdapterTagImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditoradapterPackage.Literals.EDITOR_ADAPTER_TAG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorAdapter getEditorAdapter() {
		return editorAdapter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEditorAdapter(EditorAdapter newEditorAdapter) {
		EditorAdapter oldEditorAdapter = editorAdapter;
		editorAdapter = newEditorAdapter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER, oldEditorAdapter, editorAdapter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEditorAdapterID() {
		return editorAdapterID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEditorAdapterID(String newEditorAdapterID) {
		String oldEditorAdapterID = editorAdapterID;
		editorAdapterID = newEditorAdapterID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_ID, oldEditorAdapterID, editorAdapterID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModelID() {
		return modelID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelID(String newModelID) {
		String oldModelID = modelID;
		modelID = newModelID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EditoradapterPackage.EDITOR_ADAPTER_TAG__MODEL_ID, oldModelID, modelID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEditorAdapterPackageNsURI() {
		return editorAdapterPackageNsURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEditorAdapterPackageNsURI(String newEditorAdapterPackageNsURI) {
		String oldEditorAdapterPackageNsURI = editorAdapterPackageNsURI;
		editorAdapterPackageNsURI = newEditorAdapterPackageNsURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_PACKAGE_NS_URI, oldEditorAdapterPackageNsURI, editorAdapterPackageNsURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCreationMethodName() {
		return creationMethodName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreationMethodName(String newCreationMethodName) {
		String oldCreationMethodName = creationMethodName;
		creationMethodName = newCreationMethodName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EditoradapterPackage.EDITOR_ADAPTER_TAG__CREATION_METHOD_NAME, oldCreationMethodName, creationMethodName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER:
				return getEditorAdapter();
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_ID:
				return getEditorAdapterID();
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__MODEL_ID:
				return getModelID();
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_PACKAGE_NS_URI:
				return getEditorAdapterPackageNsURI();
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__CREATION_METHOD_NAME:
				return getCreationMethodName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER:
				setEditorAdapter((EditorAdapter)newValue);
				return;
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_ID:
				setEditorAdapterID((String)newValue);
				return;
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__MODEL_ID:
				setModelID((String)newValue);
				return;
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_PACKAGE_NS_URI:
				setEditorAdapterPackageNsURI((String)newValue);
				return;
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__CREATION_METHOD_NAME:
				setCreationMethodName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER:
				setEditorAdapter((EditorAdapter)null);
				return;
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_ID:
				setEditorAdapterID(EDITOR_ADAPTER_ID_EDEFAULT);
				return;
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__MODEL_ID:
				setModelID(MODEL_ID_EDEFAULT);
				return;
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_PACKAGE_NS_URI:
				setEditorAdapterPackageNsURI(EDITOR_ADAPTER_PACKAGE_NS_URI_EDEFAULT);
				return;
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__CREATION_METHOD_NAME:
				setCreationMethodName(CREATION_METHOD_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER:
				return editorAdapter != null;
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_ID:
				return EDITOR_ADAPTER_ID_EDEFAULT == null ? editorAdapterID != null : !EDITOR_ADAPTER_ID_EDEFAULT.equals(editorAdapterID);
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__MODEL_ID:
				return MODEL_ID_EDEFAULT == null ? modelID != null : !MODEL_ID_EDEFAULT.equals(modelID);
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__EDITOR_ADAPTER_PACKAGE_NS_URI:
				return EDITOR_ADAPTER_PACKAGE_NS_URI_EDEFAULT == null ? editorAdapterPackageNsURI != null : !EDITOR_ADAPTER_PACKAGE_NS_URI_EDEFAULT.equals(editorAdapterPackageNsURI);
			case EditoradapterPackage.EDITOR_ADAPTER_TAG__CREATION_METHOD_NAME:
				return CREATION_METHOD_NAME_EDEFAULT == null ? creationMethodName != null : !CREATION_METHOD_NAME_EDEFAULT.equals(creationMethodName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (editorAdapterID: ");
		result.append(editorAdapterID);
		result.append(", modelID: ");
		result.append(modelID);
		result.append(", editorAdapterPackageNsURI: ");
		result.append(editorAdapterPackageNsURI);
		result.append(", creationMethodName: ");
		result.append(creationMethodName);
		result.append(')');
		return result.toString();
	}

} //EditorAdapterTagImpl
