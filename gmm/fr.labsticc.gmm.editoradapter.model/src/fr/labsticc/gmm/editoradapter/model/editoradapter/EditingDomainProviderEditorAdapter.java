/**
 */
package fr.labsticc.gmm.editoradapter.model.editoradapter;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Emf Generated Editor Adapter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.gmm.editoradapter.model.editoradapter.EditoradapterPackage#getEditingDomainProviderEditorAdapter()
 * @model
 * @generated
 */
public interface EditingDomainProviderEditorAdapter extends EditorAdapter {

} // EmfGeneratedEditorAdapter
