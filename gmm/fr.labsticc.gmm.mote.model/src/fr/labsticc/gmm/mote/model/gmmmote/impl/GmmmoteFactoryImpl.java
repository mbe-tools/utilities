/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.mote.model.gmmmote.impl;

import fr.labsticc.gmm.mote.model.gmmmote.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GmmmoteFactoryImpl extends EFactoryImpl implements GmmmoteFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static GmmmoteFactory init() {
		try {
			GmmmoteFactory theGmmmoteFactory = (GmmmoteFactory)EPackage.Registry.INSTANCE.getEFactory(GmmmotePackage.eNS_URI);
			if (theGmmmoteFactory != null) {
				return theGmmmoteFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new GmmmoteFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GmmmoteFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION: return createMoteSynchronizationRelation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MoteSynchronizationRelation createMoteSynchronizationRelation() {
		MoteSynchronizationRelationImpl moteSynchronizationRelation = new MoteSynchronizationRelationImpl();
		return moteSynchronizationRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GmmmotePackage getGmmmotePackage() {
		return (GmmmotePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static GmmmotePackage getPackage() {
		return GmmmotePackage.eINSTANCE;
	}

} //GmmmoteFactoryImpl
