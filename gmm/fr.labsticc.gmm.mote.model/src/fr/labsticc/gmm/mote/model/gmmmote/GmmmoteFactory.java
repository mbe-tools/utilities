/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.mote.model.gmmmote;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage
 * @generated
 */
public interface GmmmoteFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GmmmoteFactory eINSTANCE = fr.labsticc.gmm.mote.model.gmmmote.impl.GmmmoteFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Mote Synchronization Relation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mote Synchronization Relation</em>'.
	 * @generated
	 */
	MoteSynchronizationRelation createMoteSynchronizationRelation();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	GmmmotePackage getGmmmotePackage();

} //GmmmoteFactory
