/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.mote.model.gmmmote;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import de.hpi.sam.mote.TGGEngine;
import de.hpi.sam.mote.TransformationDirection;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.rules.TransformationExecutionResult;
import de.hpi.sam.mote.rules.TransformationResult;
import fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mote Synchronization Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation#getEngine <em>Engine</em>}</li>
 *   <li>{@link fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation#getRuleSetId <em>Rule Set Id</em>}</li>
 *   <li>{@link fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation#getExecutionTraceFileExtension <em>Execution Trace File Extension</em>}</li>
 * </ul>
 *
 * @see fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage#getMoteSynchronizationRelation()
 * @model
 * @generated
 */
public interface MoteSynchronizationRelation extends BinaryMetaModelRelatedConsistencyRelation {
	/**
	 * Returns the value of the '<em><b>Engine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Engine</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Engine</em>' reference.
	 * @see #setEngine(TGGEngine)
	 * @see fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage#getMoteSynchronizationRelation_Engine()
	 * @model required="true"
	 * @generated
	 */
	TGGEngine getEngine();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation#getEngine <em>Engine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Engine</em>' reference.
	 * @see #getEngine()
	 * @generated
	 */
	void setEngine(TGGEngine value);

	/**
	 * Returns the value of the '<em><b>Rule Set Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Set Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Set Id</em>' attribute.
	 * @see #setRuleSetId(String)
	 * @see fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage#getMoteSynchronizationRelation_RuleSetId()
	 * @model required="true"
	 * @generated
	 */
	String getRuleSetId();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation#getRuleSetId <em>Rule Set Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule Set Id</em>' attribute.
	 * @see #getRuleSetId()
	 * @generated
	 */
	void setRuleSetId(String value);

	/**
	 * Returns the value of the '<em><b>Execution Trace File Extension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Trace File Extension</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Trace File Extension</em>' attribute.
	 * @see #setExecutionTraceFileExtension(String)
	 * @see fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage#getMoteSynchronizationRelation_ExecutionTraceFileExtension()
	 * @model required="true"
	 * @generated
	 */
	String getExecutionTraceFileExtension();

	/**
	 * Sets the value of the '{@link fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation#getExecutionTraceFileExtension <em>Execution Trace File Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Trace File Extension</em>' attribute.
	 * @see #getExecutionTraceFileExtension()
	 * @generated
	 */
	void setExecutionTraceFileExtension(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" exceptions="de.hpi.sam.mote.helpers.TransformationException" leftResourceRequired="true" rightResourceRequired="true" dependenciesMapType="de.hpi.sam.mote.helpers.MapEntry<KeyType, ValueType>" directionRequired="true"
	 * @generated
	 */
	TransformationResult synchronize(Resource leftResource, Resource rightResource, EMap<Resource, EList<EObject>> dependencies, TransformationDirection direction) throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" exceptions="de.hpi.sam.mote.helpers.TransformationException" leftResourceRequired="true" rightResourceRequired="true"
	 * @generated
	 */
	TransformationExecutionResult checkConsistency(Resource leftResource, Resource rightResource) throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="de.hpi.sam.mote.helpers.URI" required="true" executionTraceRequired="true" leftResourceRequired="true" rightResourceRequired="true"
	 * @generated
	 */
	URI createExecutionTraceUri(EObject executionTrace, Resource leftResource, Resource rightResource);

} // MoteSynchronizationRelation
