/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.mote.model.gmmmote.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hpi.sam.mote.MotePackage;
import de.hpi.sam.mote.helpers.HelpersPackage;
import de.hpi.sam.mote.rules.RulesPackage;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.mote.model.gmmmote.GmmmoteFactory;
import fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage;
import fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GmmmotePackageImpl extends EPackageImpl implements GmmmotePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moteSynchronizationRelationEClass = null;
	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GmmmotePackageImpl() {
		super(eNS_URI, GmmmoteFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link GmmmotePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GmmmotePackage init() {
		if (isInited) return (GmmmotePackage)EPackage.Registry.INSTANCE.getEPackage(GmmmotePackage.eNS_URI);

		// Obtain or create and register package
		GmmmotePackageImpl theGmmmotePackage = (GmmmotePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof GmmmotePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new GmmmotePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		MegamodelPackage.eINSTANCE.eClass();
		MotePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theGmmmotePackage.createPackageContents();

		// Initialize created meta-data
		theGmmmotePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGmmmotePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GmmmotePackage.eNS_URI, theGmmmotePackage);
		return theGmmmotePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMoteSynchronizationRelation() {
		return moteSynchronizationRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMoteSynchronizationRelation_Engine() {
		return (EReference)moteSynchronizationRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMoteSynchronizationRelation_RuleSetId() {
		return (EAttribute)moteSynchronizationRelationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMoteSynchronizationRelation_ExecutionTraceFileExtension() {
		return (EAttribute)moteSynchronizationRelationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GmmmoteFactory getGmmmoteFactory() {
		return (GmmmoteFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		moteSynchronizationRelationEClass = createEClass(MOTE_SYNCHRONIZATION_RELATION);
		createEReference(moteSynchronizationRelationEClass, MOTE_SYNCHRONIZATION_RELATION__ENGINE);
		createEAttribute(moteSynchronizationRelationEClass, MOTE_SYNCHRONIZATION_RELATION__RULE_SET_ID);
		createEAttribute(moteSynchronizationRelationEClass, MOTE_SYNCHRONIZATION_RELATION__EXECUTION_TRACE_FILE_EXTENSION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MegamodelPackage theMegamodelPackage = (MegamodelPackage)EPackage.Registry.INSTANCE.getEPackage(MegamodelPackage.eNS_URI);
		MotePackage theMotePackage = (MotePackage)EPackage.Registry.INSTANCE.getEPackage(MotePackage.eNS_URI);
		RulesPackage theRulesPackage = (RulesPackage)EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI);
		HelpersPackage theHelpersPackage = (HelpersPackage)EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		moteSynchronizationRelationEClass.getESuperTypes().add(theMegamodelPackage.getBinaryMetaModelRelatedConsistencyRelation());

		// Initialize classes and features; add operations and parameters
		initEClass(moteSynchronizationRelationEClass, MoteSynchronizationRelation.class, "MoteSynchronizationRelation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMoteSynchronizationRelation_Engine(), theMotePackage.getTGGEngine(), null, "engine", null, 1, 1, MoteSynchronizationRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMoteSynchronizationRelation_RuleSetId(), ecorePackage.getEString(), "ruleSetId", null, 1, 1, MoteSynchronizationRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMoteSynchronizationRelation_ExecutionTraceFileExtension(), ecorePackage.getEString(), "executionTraceFileExtension", null, 1, 1, MoteSynchronizationRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(moteSynchronizationRelationEClass, theRulesPackage.getTransformationResult(), "synchronize", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "leftResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "rightResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(theHelpersPackage.getMapEntry());
		EGenericType g2 = createEGenericType(theEcorePackage.getEResource());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theEcorePackage.getEEList());
		g1.getETypeArguments().add(g2);
		EGenericType g3 = createEGenericType(theEcorePackage.getEObject());
		g2.getETypeArguments().add(g3);
		addEParameter(op, g1, "dependencies", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMotePackage.getTransformationDirection(), "direction", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(moteSynchronizationRelationEClass, theRulesPackage.getTransformationExecutionResult(), "checkConsistency", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "leftResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "rightResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = addEOperation(moteSynchronizationRelationEClass, theHelpersPackage.getURI(), "createExecutionTraceUri", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "executionTrace", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "leftResource", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "rightResource", 1, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //GmmmotePackageImpl
