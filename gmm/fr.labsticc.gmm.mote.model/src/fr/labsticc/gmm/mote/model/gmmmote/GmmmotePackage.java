/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.mote.model.gmmmote;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import fr.labsticc.gmm.model.megamodel.MegamodelPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.labsticc.gmm.mote.model.gmmmote.GmmmoteFactory
 * @model kind="package"
 * @generated
 */
public interface GmmmotePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "gmmmote";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.labsticc.fr/gmmmote/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "gmmmote";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GmmmotePackage eINSTANCE = fr.labsticc.gmm.mote.model.gmmmote.impl.GmmmotePackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.labsticc.gmm.mote.model.gmmmote.impl.MoteSynchronizationRelationImpl <em>Mote Synchronization Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.gmm.mote.model.gmmmote.impl.MoteSynchronizationRelationImpl
	 * @see fr.labsticc.gmm.mote.model.gmmmote.impl.GmmmotePackageImpl#getMoteSynchronizationRelation()
	 * @generated
	 */
	int MOTE_SYNCHRONIZATION_RELATION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__NAME = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__DESCRIPTION = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__DISPLAY_NAME = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__ID = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__ID;

	/**
	 * The feature id for the '<em><b>Connected Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__CONNECTED_MODELS = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__CONNECTED_MODELS;

	/**
	 * The feature id for the '<em><b>Chained Relations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__CHAINED_RELATIONS = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__CHAINED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Relation Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__OWNED_RELATION_POLICY = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__OWNED_RELATION_POLICY;

	/**
	 * The feature id for the '<em><b>Intention</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__INTENTION = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__INTENTION;

	/**
	 * The feature id for the '<em><b>Owned Coverage Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__OWNED_COVERAGE_POLICY = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__OWNED_COVERAGE_POLICY;

	/**
	 * The feature id for the '<em><b>Enablement Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__ENABLEMENT_TYPE = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__ENABLEMENT_TYPE;

	/**
	 * The feature id for the '<em><b>Deleting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__DELETING = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING;

	/**
	 * The feature id for the '<em><b>Validity Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__VALIDITY_STATUS = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__VALIDITY_STATUS;

	/**
	 * The feature id for the '<em><b>Meta Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__META_MODELS = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__META_MODELS;

	/**
	 * The feature id for the '<em><b>Left Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__LEFT_META_MODEL = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__LEFT_META_MODEL;

	/**
	 * The feature id for the '<em><b>Right Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__RIGHT_META_MODEL = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__RIGHT_META_MODEL;

	/**
	 * The feature id for the '<em><b>Deleting Left</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__DELETING_LEFT = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_LEFT;

	/**
	 * The feature id for the '<em><b>Deleting Right</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__DELETING_RIGHT = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_RIGHT;

	/**
	 * The feature id for the '<em><b>Engine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__ENGINE = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rule Set Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__RULE_SET_ID = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Execution Trace File Extension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION__EXECUTION_TRACE_FILE_EXTENSION = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Mote Synchronization Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_SYNCHRONIZATION_RELATION_FEATURE_COUNT = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION_FEATURE_COUNT + 3;


	/**
	 * Returns the meta object for class '{@link fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation <em>Mote Synchronization Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mote Synchronization Relation</em>'.
	 * @see fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation
	 * @generated
	 */
	EClass getMoteSynchronizationRelation();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation#getEngine <em>Engine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Engine</em>'.
	 * @see fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation#getEngine()
	 * @see #getMoteSynchronizationRelation()
	 * @generated
	 */
	EReference getMoteSynchronizationRelation_Engine();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation#getRuleSetId <em>Rule Set Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rule Set Id</em>'.
	 * @see fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation#getRuleSetId()
	 * @see #getMoteSynchronizationRelation()
	 * @generated
	 */
	EAttribute getMoteSynchronizationRelation_RuleSetId();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation#getExecutionTraceFileExtension <em>Execution Trace File Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Trace File Extension</em>'.
	 * @see fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation#getExecutionTraceFileExtension()
	 * @see #getMoteSynchronizationRelation()
	 * @generated
	 */
	EAttribute getMoteSynchronizationRelation_ExecutionTraceFileExtension();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GmmmoteFactory getGmmmoteFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.labsticc.gmm.mote.model.gmmmote.impl.MoteSynchronizationRelationImpl <em>Mote Synchronization Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.gmm.mote.model.gmmmote.impl.MoteSynchronizationRelationImpl
		 * @see fr.labsticc.gmm.mote.model.gmmmote.impl.GmmmotePackageImpl#getMoteSynchronizationRelation()
		 * @generated
		 */
		EClass MOTE_SYNCHRONIZATION_RELATION = eINSTANCE.getMoteSynchronizationRelation();

		/**
		 * The meta object literal for the '<em><b>Engine</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTE_SYNCHRONIZATION_RELATION__ENGINE = eINSTANCE.getMoteSynchronizationRelation_Engine();

		/**
		 * The meta object literal for the '<em><b>Rule Set Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOTE_SYNCHRONIZATION_RELATION__RULE_SET_ID = eINSTANCE.getMoteSynchronizationRelation_RuleSetId();

		/**
		 * The meta object literal for the '<em><b>Execution Trace File Extension</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOTE_SYNCHRONIZATION_RELATION__EXECUTION_TRACE_FILE_EXTENSION = eINSTANCE.getMoteSynchronizationRelation_ExecutionTraceFileExtension();

	}

} //GmmmotePackage
