/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.mote.model.gmmmote.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation;
import fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation;
import fr.labsticc.gmm.model.megamodel.FactualRelation;
import fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation;
import fr.labsticc.gmm.model.megamodel.NamedElement;
import fr.labsticc.gmm.model.megamodel.ObligationRelation;
import fr.labsticc.gmm.model.megamodel.Relation;
import fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage;
import fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage
 * @generated
 */
public class GmmmoteAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static GmmmotePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GmmmoteAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = GmmmotePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GmmmoteSwitch<Adapter> modelSwitch =
		new GmmmoteSwitch<Adapter>() {
			@Override
			public Adapter caseMoteSynchronizationRelation(MoteSynchronizationRelation object) {
				return createMoteSynchronizationRelationAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseRelation(Relation object) {
				return createRelationAdapter();
			}
			@Override
			public Adapter caseObligationRelation(ObligationRelation object) {
				return createObligationRelationAdapter();
			}
			@Override
			public Adapter caseFactualRelation(FactualRelation object) {
				return createFactualRelationAdapter();
			}
			@Override
			public Adapter caseMetaModelRelatedRelation(MetaModelRelatedRelation object) {
				return createMetaModelRelatedRelationAdapter();
			}
			@Override
			public Adapter caseBinaryMetaModeRelatedlRelation(BinaryMetaModeRelatedlRelation object) {
				return createBinaryMetaModeRelatedlRelationAdapter();
			}
			@Override
			public Adapter caseBinaryMetaModelRelatedConsistencyRelation(BinaryMetaModelRelatedConsistencyRelation object) {
				return createBinaryMetaModelRelatedConsistencyRelationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation <em>Mote Synchronization Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation
	 * @generated
	 */
	public Adapter createMoteSynchronizationRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.gmm.model.megamodel.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.gmm.model.megamodel.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.gmm.model.megamodel.Relation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.gmm.model.megamodel.Relation
	 * @generated
	 */
	public Adapter createRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.gmm.model.megamodel.FactualRelation <em>Factual Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.gmm.model.megamodel.FactualRelation
	 * @generated
	 */
	public Adapter createFactualRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation <em>Meta Model Related Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation
	 * @generated
	 */
	public Adapter createMetaModelRelatedRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation <em>Binary Meta Mode Relatedl Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation
	 * @generated
	 */
	public Adapter createBinaryMetaModeRelatedlRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.gmm.model.megamodel.ObligationRelation <em>Obligation Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.gmm.model.megamodel.ObligationRelation
	 * @generated
	 */
	public Adapter createObligationRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation <em>Binary Meta Model Related Consistency Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation
	 * @generated
	 */
	public Adapter createBinaryMetaModelRelatedConsistencyRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //GmmmoteAdapterFactory
