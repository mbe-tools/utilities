/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.gmm.mote.model.gmmmote.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.hpi.sam.mote.MoteEngineCoveragePolicy;
import de.hpi.sam.mote.MoteEngineRelationPolicy;
import de.hpi.sam.mote.TGGEngine;
import de.hpi.sam.mote.TransformationDirection;
import de.hpi.sam.mote.impl.InexistentCorrespondingResourceException;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.rules.TransformationExecutionResult;
import de.hpi.sam.mote.rules.TransformationResult;
import fr.labsticc.framework.core.exception.FunctionalException;
import fr.labsticc.framework.core.exception.SystemException;
import fr.labsticc.framework.emf.core.util.EMFUtil;
import fr.labsticc.gmm.merge.service.GmmMergeUtil;
import fr.labsticc.gmm.merge.service.IGmmMergeService;
import fr.labsticc.gmm.model.megamodel.ErrorDescription;
import fr.labsticc.gmm.model.megamodel.ExecutionContext;
import fr.labsticc.gmm.model.megamodel.MegamodelFactory;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.Model;
import fr.labsticc.gmm.model.megamodel.ModelElementsCoveragePolicy;
import fr.labsticc.gmm.model.megamodel.ModelingEnvironment;
import fr.labsticc.gmm.model.megamodel.OperationType;
import fr.labsticc.gmm.model.megamodel.RelationPolicy;
import fr.labsticc.gmm.model.megamodel.UriRelatedBinaryRelPolicy;
import fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModelRelatedConsistencyRelationImpl;
import fr.labsticc.gmm.model.megamodel.impl.InexistentCrossResourceException;
import fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage;
import fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mote Synchronization Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.labsticc.gmm.mote.model.gmmmote.impl.MoteSynchronizationRelationImpl#getEngine <em>Engine</em>}</li>
 *   <li>{@link fr.labsticc.gmm.mote.model.gmmmote.impl.MoteSynchronizationRelationImpl#getRuleSetId <em>Rule Set Id</em>}</li>
 *   <li>{@link fr.labsticc.gmm.mote.model.gmmmote.impl.MoteSynchronizationRelationImpl#getExecutionTraceFileExtension <em>Execution Trace File Extension</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MoteSynchronizationRelationImpl extends BinaryMetaModelRelatedConsistencyRelationImpl implements MoteSynchronizationRelation {

	//private static final Logger logger = LoggerFactory.getLogger( MoteSynchronizationRelationImpl.class );
	
	private final ResourceSet resourceSet;

	private final IGmmMergeService mergeService;

	/**
	 * The cached value of the '{@link #getEngine() <em>Engine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEngine()
	 * @generated
	 * @ordered
	 */
	protected TGGEngine engine;

	/**
	 * The default value of the '{@link #getRuleSetId() <em>Rule Set Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuleSetId()
	 * @generated
	 * @ordered
	 */
	protected static final String RULE_SET_ID_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getRuleSetId() <em>Rule Set Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuleSetId()
	 * @generated
	 * @ordered
	 */
	protected String ruleSetId = RULE_SET_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getExecutionTraceFileExtension() <em>Execution Trace File Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionTraceFileExtension()
	 * @generated
	 * @ordered
	 */
	protected static final String EXECUTION_TRACE_FILE_EXTENSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExecutionTraceFileExtension() <em>Execution Trace File Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionTraceFileExtension()
	 * @generated
	 * @ordered
	 */
	protected String executionTraceFileExtension = EXECUTION_TRACE_FILE_EXTENSION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected MoteSynchronizationRelationImpl()  {
		super();
		
		resourceSet = createResourceSet();
		
		try {
			mergeService = GmmMergeUtil.getMergeService();
		}
		catch( final CoreException p_ex ) {
			throw new RuntimeException( p_ex );
		}
	}
	
	protected ResourceSet createResourceSet() {
		return new ResourceSetImpl();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GmmmotePackage.Literals.MOTE_SYNCHRONIZATION_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TGGEngine getEngine() {
		if (engine != null && engine.eIsProxy()) {
			InternalEObject oldEngine = (InternalEObject)engine;
			engine = (TGGEngine)eResolveProxy(oldEngine);
			if (engine != oldEngine) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__ENGINE, oldEngine, engine));
			}
		}
		return engine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TGGEngine basicGetEngine() {
		return engine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setEngine(TGGEngine newEngine) {
		TGGEngine oldEngine = engine;
		engine = newEngine;
		
		// Update the engine relation policy.
		setOwnedRelationPolicy( getOwnedRelationPolicy() );
		setOwnedCoveragePolicy( getOwnedCoveragePolicy() );
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__ENGINE, oldEngine, engine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRuleSetId() {
		return ruleSetId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__ENGINE:
				if (resolve) return getEngine();
				return basicGetEngine();
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__RULE_SET_ID:
				return getRuleSetId();
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__EXECUTION_TRACE_FILE_EXTENSION:
				return getExecutionTraceFileExtension();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__ENGINE:
				setEngine((TGGEngine)newValue);
				return;
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__RULE_SET_ID:
				setRuleSetId((String)newValue);
				return;
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__EXECUTION_TRACE_FILE_EXTENSION:
				setExecutionTraceFileExtension((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__ENGINE:
				setEngine((TGGEngine)null);
				return;
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__RULE_SET_ID:
				setRuleSetId(RULE_SET_ID_EDEFAULT);
				return;
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__EXECUTION_TRACE_FILE_EXTENSION:
				setExecutionTraceFileExtension(EXECUTION_TRACE_FILE_EXTENSION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__ENGINE:
				return engine != null;
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__RULE_SET_ID:
				return RULE_SET_ID_EDEFAULT == null ? ruleSetId != null : !RULE_SET_ID_EDEFAULT.equals(ruleSetId);
			case GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__EXECUTION_TRACE_FILE_EXTENSION:
				return EXECUTION_TRACE_FILE_EXTENSION_EDEFAULT == null ? executionTraceFileExtension != null : !EXECUTION_TRACE_FILE_EXTENSION_EDEFAULT.equals(executionTraceFileExtension);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ruleSetId: ");
		result.append(ruleSetId);
		result.append(", executionTraceFileExtension: ");
		result.append(executionTraceFileExtension);
		result.append(')');
		return result.toString();
	}
	
	protected Resource createResourceInCache( final URI p_resUri ) {
		return resourceSet.createResource( p_resUri );
	}
	
	private static EObject modelObject( final Model p_model,
										final EObject p_element ) {
		final URI elemResUri = EcoreUtil.getURI( p_element );

		for ( final Resource srcRes : p_model.getResources() ) {
			final EObject modelObject = srcRes.getResourceSet().getEObject( elemResUri, false );
			
			if ( modelObject != null ) {
				return modelObject;
			}
		}
		
		return null;
	}
	
	private void createInconsistentElements( 	final Model p_model,
												final Model p_otherModel,
												final EList<EObject> p_uncoveredElements ) {
		final EList<EObject> inconsistentElements = new BasicEList<EObject>();
		final ModelElementsCoveragePolicy coveragePolicy = getOwnedCoveragePolicy();
		
		for ( final EObject uncovElem : p_uncoveredElements ) {
			if ( coveragePolicy == null || coveragePolicy.isConsidered( uncovElem ) ) {
				// Return element from the model and not from cache
				// Do not consider objects outside the model
				final EObject modelObject = modelObject( p_model, uncovElem );
				
				if ( modelObject != null ) {
					inconsistentElements.add( modelObject );
				}
			}
		}

		final ErrorDescription errorDesc;
		
		if ( inconsistentElements.isEmpty() ) {
			errorDesc = null;
		}
		else {
			errorDesc = MegamodelFactory.eINSTANCE.createErrorDescription();
			final Map<Model, EList<EObject>> errElements = new HashMap<Model, EList<EObject>>();
			errElements.put( p_otherModel, inconsistentElements );
			errorDesc.setErroneousElements( errElements );
		}

		p_model.setError( errorDesc );
	}
	
	private Model checkConsistency( 	final Model p_leftModel,
										final Resource p_leftResource,
										final Model p_rightModel,
										final Resource p_rightResource ) 
	throws TransformationException {
		final TransformationExecutionResult result = checkConsistency( p_leftResource, p_rightResource );
		createInconsistentElements( p_leftModel, p_rightModel, result.getLeftUncoveredElements() );
		createInconsistentElements( p_rightModel, p_leftModel, result.getRightUncoveredElements() );
		
		final EObject execTrace = result.getExecutionTrace();
		
		if ( execTrace != null ) {
			return createExecutionTraceModel( execTrace, p_leftResource, p_rightResource );
		}
		
		return null;
	}
	
	private Model createExecutionTraceModel(	final EObject p_execTrace,
												final Resource p_leftResource,
												final Resource p_rightResource ) {
		final Model traceModel = MegamodelFactory.eINSTANCE.createModel();
		final URI uri = createExecutionTraceUri( p_execTrace, p_leftResource, p_rightResource );
		Resource traceRes = p_leftResource.getResourceSet().getResource( uri, false );
		
		if ( traceRes == null ) {
			traceRes = p_leftResource.getResourceSet().createResource( uri );
		}
		
		traceRes.getContents().add( p_execTrace );
		traceModel.getResources().add( traceRes );
		
		return traceModel;
	}
	
	private boolean areConsistent(	final Model p_leftModel,
									final Model p_rightModel ) {
		return p_leftModel.getError() == null && p_rightModel.getError() == null;
	}
	
	@Override
	public TransformationExecutionResult checkConsistency( 	final Resource p_leftResource,
															final Resource p_rightResource )
	throws TransformationException {
		final TGGEngine engine = getEngine();
		
		if ( engine.getAvailableRuleSets().isEmpty() ) {
			engine.initializeEngine();
		}

		final EMap<Resource, EList<EObject>> dependencies = new BasicEMap<Resource, EList<EObject>>();
		
		return engine.checkConsistency( p_leftResource, p_rightResource, dependencies, getRuleSetId() );
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public URI createExecutionTraceUri(	final EObject p_executionTrace,
										final Resource leftResource,
										final Resource rightResource ) {
		return leftResource.getURI().trimFileExtension().appendFileExtension( getExecutionTraceFileExtension() );
	}

	/**
	 * Merge the specified resource into the cache. If no changes were detected between 
	 * the specified resource and the cache resource, return null to indicate this.
	 * @param p_resource
	 * @return The resource from the cache after merge, or null if there were no differences
	 * between the specified resource and the cached resource and the check changes flag is set.
	 */
	private Resource mergeIntoCache( 	final Resource p_resource,
										final boolean pb_checkChanges ) {
		final Resource cachedResource = getCreateResourceFromCache( p_resource );
		
		if ( p_resource.getContents().isEmpty() && !cachedResource.getContents().isEmpty() ) {
			clearResourceCache( cachedResource );
			
			return getCreateResourceFromCache( p_resource );
		}

		final MetaModel resMM = metaModel( p_resource );
		final boolean mergedAnyChanges = mergeService.merge( 	p_resource, 
																cachedResource,
																resMM.useIdForCompare(),
																false,
																resMM.getNameSimilarityWeight(),
																resMM.getPositionSimilarityWeight(),
																null,
																null,
																getSaveOptions() );
		
		return mergedAnyChanges || !pb_checkChanges ? cachedResource : null;
	}
	
	private MetaModel metaModel( final Resource p_resource ) {
		return getLeftMetaModel().declares( p_resource.getURI() ) ? getLeftMetaModel() : getRightMetaModel();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Model> establishValidity( 	final ModelingEnvironment p_modelingEnvironment,
											final ExecutionContext p_executionContext ) 
	throws FunctionalException, SystemException {
		final EList<Model> updatedModels = new BasicEList<Model>();
		
		final Model sourceModel = p_modelingEnvironment.getSourceModel();
		
		// Synchronization / transformation can only be performed when there are no errors on the model.
		// Otherwise there is no guarantee on the result.
		for ( final Resource changedResource : sourceModel.getResources() ) {
			
			// It may happen that a resource has been removed without having been changed. In such case,
			// it will not be in the cache and must be created
			final TransformationDirection direction;
			
			if ( getLeftMetaModel().declares( changedResource.getURI() ) ) {
				direction = TransformationDirection.FORWARD;
			}
			else if ( getRightMetaModel().declares( changedResource.getURI() ) ) {
				direction = TransformationDirection.REVERSE;
			}
			else {
				throw new IllegalArgumentException( "Changed object is not declared by any meta-model of the synchronization relation." );
			}
	
			Resource cachedChangedResource = mergeIntoCache( changedResource, true );
			final boolean sourceResChanged = cachedChangedResource != null;
			
			if ( cachedChangedResource == null ) {
				cachedChangedResource = getCreateResourceFromCache( changedResource );
			}

			for ( final Model model : p_modelingEnvironment.getOwnedModels() ) {
				for ( final Resource targetResource : model.getResources() ) {
					if ( EMFUtil.isReadOnly( targetResource ) ) {
						if ( sourceResChanged ) {
							System.out.println( "Warning: target resource " +  targetResource.getURI() + 
								" is read only. Unable to synchronize it with " + changedResource.getURI() );
						}
					}
					else {
						// Merge and obtain the merged target resource from the cache
						Resource cachedTargetResource = mergeIntoCache( targetResource, false );
						
						// If the target resource is not empty, we must synchronize and not perform batch transform
						final boolean synchronize = !targetResource.getContents().isEmpty();
						
						//if ( cachedChangedResource != null && shouldProceed( pb_manualLaunch, synchronize ) ) {
						//if ( shouldPerform( sourceResChanged, synchronize, cachedChangedResource, cachedTargetResource, changedResource, targetResource ) ) {
						try {
							final MetaModel targetMm = metaModel( targetResource );
							final Resource leftResource;
							final Resource rightResource;
							final Model leftModel;
							final Model rightModel;
							
							if ( getLeftMetaModel().declares( changedResource.getURI() ) ) {//TransformationDirection.FORWARD == direction ) {
								leftResource = cachedChangedResource;
								rightResource = cachedTargetResource;
								leftModel = sourceModel;
								rightModel = model;
							}
							else {
								leftResource = cachedTargetResource;
								rightResource = cachedChangedResource;
								leftModel = model;
								rightModel = sourceModel;
							}

							final EMap<Resource, EList<EObject>> dependencies = new BasicEMap<Resource, EList<EObject>>();
							final boolean canTransform;
							
							final Model consistencyTraceModel;
							
							if ( OperationType.READ == p_executionContext.getSourceModelOperationType() ) {
								if ( synchronize ) {
									consistencyTraceModel = checkConsistency( leftModel, leftResource, rightModel, rightResource );
									canTransform = false;
								}
								else {
									consistencyTraceModel = null;
									canTransform = true;
								}
							}
							else if ( OperationType.CREATED == p_executionContext.getSourceModelOperationType() ) {
								// Resource has just been created. Ensure it is consistent with target resource if it exists
								
								if ( synchronize ) {
									consistencyTraceModel = checkConsistency( leftModel, leftResource, rightModel, rightResource );
									canTransform = areConsistent( leftModel, rightModel );
								}
								else {
									consistencyTraceModel = null;
									canTransform = true;
								}
							}
							else if ( OperationType.UPDATED == p_executionContext.getSourceModelOperationType() ) {
								if ( hasOnlyGmmErrors( sourceModel ) || hasOnlyGmmErrors( model ) ) {
									consistencyTraceModel = checkConsistency( leftModel, leftResource, rightModel, rightResource );
									canTransform = areConsistent( leftModel, rightModel );
								}
								else {
									consistencyTraceModel = null;
									canTransform = true;
								}
							}
							else {
								consistencyTraceModel = null;
								canTransform = true;
							}
							
							if ( consistencyTraceModel != null ) {
								updatedModels.add( consistencyTraceModel );
							}
							
							if ( canTransform ) {
								final TransformationExecutionResult result = transform( leftResource, rightResource, dependencies, direction, synchronize );
								final EObject trace = result.getExecutionTrace();

								if ( trace != null ) {
									updatedModels.add( createExecutionTraceModel( trace, leftResource, rightResource )  );
								}
	
								final ResourceSet extResSet = targetResource.getResourceSet();
								
								for ( final Resource depRes : dependencies.keySet() ) {
									final Model depModel = MegamodelFactory.eINSTANCE.createModel();
									final URI depResUri = depRes.getURI();
									Resource extRes = extResSet.getResource( depResUri, false );
									
									if ( extRes == null || !extResSet.getURIConverter().exists( depResUri, null ) ) {
										extRes = extResSet.createResource( depResUri );
									}
									
									mergeService.merge( depRes, 
														extRes,
														targetMm.useIdForCompare(),
														false,
														targetMm.getNameSimilarityWeight(),
														targetMm.getPositionSimilarityWeight(),
														null,
														null,
														getSaveOptions() );
									depModel.getResources().add( extRes );
									depModel.setName( depResUri.lastSegment() );
									updatedModels.add( depModel );
									
									for ( final EObject depTrace : dependencies.get( depRes ) ) {
																// Right resource not used for this implementation so null is OK
										updatedModels.add( createExecutionTraceModel( depTrace, depRes, null )  );
									}
								}
								
								// Avoid merging if resource was deleted and relation is not allowed to delete the model.
								// Otherwise, the resource from the opened editor is cleared which is not what we want.
								if ( 	OperationType.DELETED != p_executionContext.getSourceModelOperationType() || 
										canDelete( model ) ) {

									// Merge the updated resource
									mergeService.merge( cachedTargetResource,
														targetResource,
														targetMm.useIdForCompare(),
														false,
														targetMm.getNameSimilarityWeight(),
														targetMm.getPositionSimilarityWeight(),
														null,
														null,
														getSaveOptions() );
								}
							}

							// Always return the updated model even if its resource has been deleted.
							updatedModels.add( model );
						}
						catch ( final InexistentCorrespondingResourceException p_ex ) {
							final Resource resource = p_ex.getInexistendResource();
							
							if ( !resource.getContents().isEmpty() && concerns( resource.getContents().get( 0 ) ) ) {
								throw new InexistentCrossResourceException( p_ex.getInexistendResource().getURI() );
							}
						}
						catch( final TransformationException p_ex ) {
							if ( !synchronize ) {
								
								// If anything goes wrong and the target resource was empty (not synchronize),
								// we want to clear the cache so that the incompletely constructed resource is cleared
								clearResourceCache( cachedTargetResource );
							}
							
							if ( p_ex.isSystemic() ) {
								throw new SystemException( p_ex ); 
							}
							
							throw new FunctionalException( p_ex.getLocalizedMessage() ); 
						}
						catch( final RuntimeException p_ex ) {
							if ( !synchronize ) {
								
								// If anything goes wrong and the target resource was empty (not synchronize),
								// we want to clear the cache so that the incompletely constructed resource is cleared
								clearResourceCache( cachedTargetResource );
							}
							
							throw p_ex; 
						}
					}
				}
			}
		}
		
		return updatedModels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setRuleSetId(String newRuleSetId) {
		String oldRuleSetId = ruleSetId;
		ruleSetId = newRuleSetId;
		
		setOwnedRelationPolicy( getOwnedRelationPolicy() );
	
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__RULE_SET_ID, oldRuleSetId, ruleSetId));
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExecutionTraceFileExtension() {
		return executionTraceFileExtension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionTraceFileExtension(String newExecutionTraceFileExtension) {
		String oldExecutionTraceFileExtension = executionTraceFileExtension;
		executionTraceFileExtension = newExecutionTraceFileExtension;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__EXECUTION_TRACE_FILE_EXTENSION, oldExecutionTraceFileExtension, executionTraceFileExtension));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public TransformationResult synchronize(	final Resource p_leftResource,
												final Resource p_rightResource,
												final EMap<Resource, EList<EObject>> p_dependencies,
												final TransformationDirection p_direction )
	throws TransformationException {
		final TransformationExecutionResult result = transform( p_leftResource, p_rightResource, p_dependencies, p_direction, true );
		
		return result.getTransformationResult();
	}

	protected TransformationExecutionResult transform(	final Resource p_leftResource,
														final Resource p_rightResource,
														final EMap<Resource, EList<EObject>> p_dependencies,
														final TransformationDirection p_transformationDirection,
														final boolean pb_synchronize )
	throws TransformationException {
		final TGGEngine engine = getEngine();
		
		if ( engine.getAvailableRuleSets().isEmpty() ) {
			engine.initializeEngine();
		}

		return engine.transform( p_leftResource, p_rightResource, p_dependencies, p_transformationDirection, ruleSetId, pb_synchronize );
	}

	private void clearDependentResources( final Resource p_resource ) {
		// Remove all depending resources
		for ( final Resource cachedRes : new ArrayList<Resource>( resourceSet.getResources() ) ) {
			if ( cachedRes != p_resource && EMFUtil.dependsOn( cachedRes, p_resource ) ) {
				unloadResource( cachedRes );
			}
		}
	}
	
	private void unloadResource( final Resource p_resource ) {
		getEngine().resourceDeleted( p_resource );
		EMFUtil.unloadResource( p_resource );
	}
	
	protected void clearResourceCache( final Resource p_resource ) {
		assert p_resource.getResourceSet() == resourceSet : "Can only clear cache with resource from the cache.";
		
		clearDependentResources( p_resource );
		
		final URI resUri = p_resource.getURI();

		for ( final Resource cachedRes : new ArrayList<Resource>( resourceSet.getResources() ) ) {
			if ( resUri.equals( cachedRes.getURI() ) ) {
				unloadResource( cachedRes );
			}
		}
	}
	
	
	/**
	 * Obtain the resource from the cache if it exists in the cache. Otherwise, 
	 * create a new resource and add it	to the cache.
	 * @param p_resourceUri
	 * @return
	 */
	protected Resource getCreateResourceFromCache( final Resource p_resource ) {
		final URI resUri = p_resource.getURI();
		
		final Resource resource = resourceSet.getResource( resUri, false );
		
		if ( resource != null ) {
			return resource;
		}

		return createResourceInCache( resUri );
	}
	
	protected ResourceSet getResourceSet() {
		return resourceSet;
	}
	
	protected Map<?, ?> getSaveOptions() {
		return null;
	}

	@Override
	public void setOwnedRelationPolicy(RelationPolicy newOwnedRelationPolicy) {
		super.setOwnedRelationPolicy( newOwnedRelationPolicy );

		if ( getEngine() != null ) {
			final EMap<String, MoteEngineRelationPolicy> engRelPolicies = getEngine().getRelationPolicies();
			final String ruleSetId = getRuleSetId();
			
			if ( 	ruleSetId != null && engRelPolicies.get( ruleSetId ) == null &&
					newOwnedRelationPolicy instanceof MoteEngineRelationPolicy ) {
				engRelPolicies.put( ruleSetId, (MoteEngineRelationPolicy) newOwnedRelationPolicy );
			}
		}
	}
	
	@Override
	public void setOwnedCoveragePolicy(ModelElementsCoveragePolicy newOwnedCoveragePolicy) {
		super.setOwnedCoveragePolicy( newOwnedCoveragePolicy );

		if ( getEngine() != null ) {
			final EMap<String, MoteEngineCoveragePolicy> policies = getEngine().getCoveragePolicies();
			final String ruleSetId = getRuleSetId();
			
			if ( 	ruleSetId != null && policies.get( ruleSetId ) == null &&
					newOwnedCoveragePolicy instanceof MoteEngineCoveragePolicy ) {
				policies.put( ruleSetId, (MoteEngineCoveragePolicy) newOwnedCoveragePolicy );
			}
		}
	}
	
	@Override
	public String getDisplayName( final EList<URI> p_sourceUris ) {
		boolean sourcesAllFromLeftMM = true;
		boolean sourcesAllFromRightMM = true;
		boolean allCorrResExist = true;
		
		for ( final URI uri : p_sourceUris ) {
			if ( getLeftMetaModel().declares( uri ) ) {
				sourcesAllFromRightMM = false;
			}
			else if ( getRightMetaModel().declares( uri ) ) {
				sourcesAllFromLeftMM = false;
			}
			
			final URI corrUri = ( (UriRelatedBinaryRelPolicy) getOwnedRelationPolicy() ).correspondingUri( uri );
			
			if ( corrUri != null ) {
				allCorrResExist = allCorrResExist && getResourceSet().getURIConverter().exists( corrUri, null );
			}
		}
		
		final String prefix = allCorrResExist ? "Synchronize " : "Generate ";
		final String model = p_sourceUris.size() > 1 ? " models" : " model";
		
		if ( sourcesAllFromRightMM ) {
			return prefix + getLeftMetaModel().getName() + model + " from " + getRightMetaModel().getName() + model;
		}
		
		if ( sourcesAllFromLeftMM ) {
			return prefix + getRightMetaModel().getName() + model + " from " + getLeftMetaModel().getName() + model;
		}
		
		return super.getDisplayName( p_sourceUris );
	}
} //MoteSynchronizationRelationImpl
