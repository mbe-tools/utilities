package fr.mem4csd.utils.eclipse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.util.ManifestElement;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;

/**
 * Utility class for Eclipse bundles related operations.
 * 
 */
public class BundleUtil {

	public static String getBundleCanonicalEntryPath(	final String bundleId,
														final String entry )
	throws IOException {
		final File file = getBundleEntryFile(bundleId, entry);

		return file == null ? null : file.getCanonicalPath();
	}

	public static File getBundleEntryFile(	final String bundleId,
											final String entry )
	throws IOException {
		final Bundle extResBundle = Platform.getBundle(bundleId);

		if (extResBundle == null) {
			throw new IOException("Bundle " + bundleId	+ " not found!");
		}

		final URL entryUrl = extResBundle.getEntry(entry );

		if (entryUrl == null) {
			return null;
		}

		final String path = FileLocator.toFileURL(entryUrl).getPath();

		return new File(path);
	}

	public static String getAbsoluteFileName(	final String fileName,
												final Bundle bundle )
	throws IOException {
		try {
			URL fileUrl = FileLocator.find(bundle, new Path(fileName), null);
			String result = FileLocator.toFileURL(fileUrl).getPath().toString();

			return result;
		} 
		catch (final NullPointerException p_ex) {
			throw new FileNotFoundException("File " + fileName	+ " not found.");
		}
	}
//
//	public static boolean isPluginPresent(final String p_pluginId) {
//		return Platform.getBundle(p_pluginId) != null;
//	}

	public static List<IExtension> findDependentExtensions( final IExtension ext )
	throws BundleException {
		final List<IExtension> dependencies = new ArrayList<IExtension>();
		final String requireBundle = (String) Platform.getBundle(ext.getNamespaceIdentifier()).getHeaders().get(Constants.REQUIRE_BUNDLE);
		
		if ( requireBundle != null ) {
			final ManifestElement[] elements = ManifestElement.parseHeader(	Constants.BUNDLE_CLASSPATH, requireBundle );
			final IExtensionPoint extensionPoint = Platform.getExtensionRegistry().getExtensionPoint(ext.getExtensionPointUniqueIdentifier());

			for ( final ManifestElement manifestElement : elements ) {
				for ( final IExtension depExt : findExtensionsDeclaredInBundle( extensionPoint, manifestElement.getValue() ) ) {
					dependencies.add(depExt);
					dependencies.addAll( findDependentExtensions(depExt) );
				}
			}
		}

		return dependencies;
	}

	public static List<IExtension> findExtensionsDeclaredInBundle( 	final IExtensionPoint extPoint,
																	final String bundleId ) {
		final List<IExtension> delcExt = new ArrayList<IExtension>();

		for (final IExtension ext : extPoint.getExtensions()) {
			if (bundleId.equals(ext.getNamespaceIdentifier())) {
				delcExt.add(ext);
			}
		}

		return delcExt;
	}

	/**
	 * Sorts the specified extensions with respect to their dependencies. In
	 * other words, if an extension depends on another one, it will be placed
	 * after the extension on which it depends.
	 * 
	 * @param extensions
	 *            An array containing the extensions to be sorted.
	 * @return An array of the sorted extensions.
	 */
	public static IExtension[] sortExtensionsByDependencies( final IExtension[] extensions) {
		return sortExtensionsByDependencies( extensions, true );
	}

	public static IExtension[] sortExtensionsByDependencies( 	final IExtension[] extensions,
																final boolean ascending ) {
		final IExtension[] sortedExt = Arrays.copyOf( extensions, extensions.length );
		Arrays.sort( sortedExt, new ExtensionDependenciesComparator( ascending ) );

		return sortedExt;
	}

	/**
	 * Sorts the specified extensions with respect to their dependencies. In
	 * other words, if an extension depends on another one, it will be placed
	 * after the extension on which it depends.
	 * 
	 * @param extensions
	 *            The list to be sorted.
	 */
	public static void sortExtensionsByDependencies( final List<IExtension> extensions ) {
		Collections.sort( extensions, new ExtensionDependenciesComparator() );
	}

	/**
	 * Returns whether the first extension depends on the second extension.
	 * 
	 * @param ext1
	 *            The first extension.
	 * @param ext2
	 *            The second extension.
	 * @return True if the first extension depends on the second extension.
	 * @throws BundleException
	 */
	public static boolean extensionDependsOn( 	final IExtension ext1,
												final IExtension ext2)
	throws BundleException {
		return findDependentExtensions( ext1 ).contains( ext2 );
	}

	public static IExtension[] getExtensionPointExtensions( final Bundle declaringBundle,
															final String extPointID ) {
		return getExtensionPointExtensions( declaringBundle.getSymbolicName(), extPointID );
	}

	public static IExtension[] getExtensionPointExtensions(	final String extPointIdNs,
															final String extPointID ) {
		final IExtensionRegistry extensionRegistry = Platform.getExtensionRegistry();

		return extensionRegistry.getExtensionPoint( extPointIdNs, extPointID).getExtensions();
	}

	public static Object getExecutableExtension(	final String extPointIdNs,
													final String extPointID,
													final String elementAttId )
	throws CoreException {
		for ( final IExtension ext : getExtensionPointExtensions( extPointIdNs, extPointID ) ) {
			for ( final IConfigurationElement element : ext.getConfigurationElements() ) {
				final String classAttribute = element.getAttribute( elementAttId );

				if ( classAttribute != null ) {
					return element.createExecutableExtension( elementAttId );
				}
			}
		}

		return null;
	}

	public static IExtension getExtensionPointExtension( 	final String extPointIdNs,
															final String extPointID,
															final String elementAttId ) {
		for (final IExtension ext : getExtensionPointExtensions( extPointIdNs, extPointID ) ) {
			if ( elementAttId.equals( ext.getSimpleIdentifier() ) ) {
				return ext;
			}
		}

		return null;
	}
}
