package fr.tpt.mem4csd.utils.eclipse.ui.emf;

import fr.mem4csd.utils.emf.EMFUtil;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.ui.viewer.IViewerProvider;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.jface.viewers.IElementComparer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IURIEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

public class EMFEditorUtil {

	private static final IElementComparer uriComparer = new IElementComparer() {
		
		@Override
		public int hashCode(Object element) {
			return 0;
		}
		
		@Override
		public boolean equals(	final Object object1, 
								final Object object2 ) {
			if ( object1 == null ||  object2 == null ) {
				return object2 == object1;
			}
			
			if ( object1 instanceof EObject && object2 instanceof EObject ) {
				return EMFUtil.equalsByURI( (EObject) object1, (EObject) object2 );
			}
			
			if ( object1 instanceof Resource && object2 instanceof Resource ) {
				return ( (Resource) object1 ).getURI().equals(  ( (Resource) object2 ).getURI() );
			}
			
			if ( object1 instanceof ResourceSet && object2 instanceof ResourceSet ) {
				return true;
			}
			
			return object1.equals( object2 );
		}
	};

	/**
	 * Open the proper editor for the model object from its resource file extension (according 
	 * to IDE editor file associations preferences) and selects it.
	 * 
	 * @param modelObjects
	 * @throws PartInitException
	 */
	public static void selectIntoEditor( final List<? extends EObject> modelObjects ) 
	throws PartInitException {
		final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		
		if ( window != null && window.getActivePage() != null ) {
			final Resource commonRes = sameResource( modelObjects );
			
			if ( commonRes == null ) {
				throw new PartInitException( "Cannot select objects from different resources!" );
			}

			final URI uri = commonRes.getURI();
			final IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile( new Path( uri.toPlatformString( true ) ) );
			
			final IEditorPart editor = IDE.openEditor( window.getActivePage(), file );
			
			if ( editor == null ) {
				throw new PartInitException( "Cannot find an ediotr for objects " + modelObjects + "." );
			}
			
			selectIntoEditor( modelObjects, editor );
		}
	}
	
	private static Resource sameResource( final List<? extends EObject> modelObjects ) {
		if ( modelObjects.isEmpty() ) {
			return null;
		}

		final Set<Resource> resources = new HashSet<Resource>();
		
		for ( final EObject object : modelObjects ) {
			assert object.eResource() != null;
			
			if ( !resources.add( object.eResource() ) ) {
				return null;
			}
		}
		
		return resources.iterator().next();
	}
	
	public static void selectIntoEditor( 	final List<? extends EObject> modelObjects,
											final IEditorPart editor ) {
		IElementComparer currentComparer = null;
		StructuredViewer structViewer = null;
		final ISelection selection = new StructuredSelection( modelObjects );

		if ( editor instanceof IViewerProvider ) {
			final Viewer viewer = ( (IViewerProvider) editor ).getViewer();
			
			if ( viewer instanceof StructuredViewer ) {
				structViewer = (StructuredViewer) viewer;
				currentComparer = structViewer.getComparer();
				structViewer.setComparer( uriComparer );
			}

			viewer.setSelection( selection, true );
		}
		else {
			editor.getEditorSite().getSelectionProvider().setSelection( selection );
		}
		
		if ( structViewer != null ) {
			structViewer.setComparer( currentComparer );
		}
	}
	
	public static EditingDomain findEditingDomain( 	final URI uri,
													final boolean restoreEditor ) {
		final IEditingDomainProvider editorEditingDomainProvider = findEditorEditingDomainProvider( uri, restoreEditor );
		
		if ( editorEditingDomainProvider != null ) {
			return editorEditingDomainProvider.getEditingDomain();
		}
		
		return null;
	}
	
	public static String getEditorInputName( final URI uri ) {
		if ( uri.isPlatformResource() ) {
			return uri.toPlatformString( true );
		}

		return uri.toString();
	}
	
	public static String getResourceName( final IEditorPart editorPart ) {
        final IEditorInput editorInput = editorPart.getEditorInput();

    	if ( editorInput instanceof IFileEditorInput ) {
        	return ( (IFileEditorInput) editorInput ).getFile().getFullPath().toString();
        }

    	if ( editorInput instanceof IURIEditorInput ) {
        	return ( (IURIEditorInput) editorInput ).getURI().toString();
        }

        return editorInput == null ? null : editorInput.getName();
	}
	
	public static IEditingDomainProvider findEditorEditingDomainProvider( 	final URI uri,
																			final boolean restoreEditor ) {
		final String editorInputName = getEditorInputName( uri );

		for ( final IWorkbenchWindow window : PlatformUI.getWorkbench().getWorkbenchWindows() ) {
		    for ( final IWorkbenchPage page : window.getPages() ) {
		        for ( final IEditorReference editorRef : page.getEditorReferences() ) {
			        final IEditorPart editor = editorRef.getEditor( restoreEditor );
			        
			        if ( editor != null ) {
			        	final String resourceName = getResourceName( editor );
	
				        if ( editorInputName.equals( resourceName ) && editor instanceof IEditingDomainProvider ) {
				        	return (IEditingDomainProvider) editor;
				        }
			        }
		        }
		    }
		}
		
		return null;
	}
}
