package fr.tpt.mem4csd.utils.eclipse.ui.emf;

import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IItemLabelProvider;

import fr.mem4csd.utils.emf.IValidator;

public class EMFValidator extends EObjectValidator implements IValidator {

	private final AdapterFactory defaultAdapterFactory;

	public EMFValidator() {
		defaultAdapterFactory = new ComposedAdapterFactory( ComposedAdapterFactory.Descriptor.Registry.INSTANCE );
	}
	
	@Override
	public void validate( final EObject... objects ) 
	throws InterruptedException {
	    final Diagnostician diagnostician = createDiagnostician( /*progressMonitor*/);
	    final BasicDiagnostic diagnostic = createDiagnostic( diagnostician, objects );
	    validate( diagnostician, diagnostic, diagnostician.createDefaultContext(), objects );
	    
	    checkDiagnostic( diagnostic );
	}
	
	private boolean checkDiagnostic( final BasicDiagnostic diagnostic ) {
	    return diagnostic.getSeverity() == Diagnostic.OK;
	}
	
	protected void validate( 	final Diagnostician diagnostician,
								final BasicDiagnostic diagnostic,
								final Map<Object, Object> context,
								final EObject... objects ) {
	    //int count = objects.length;
	    
//	    for ( final EObject eObject : objects ) {
//	    	for ( final Iterator<?> iterator = eObject.eAllContents(); iterator.hasNext(); iterator.next() ) {
//	    		++count;
//	    	}
//	    }

	    //progressMonitor.beginTask("", count);

	    
	    for ( final EObject eObject : objects ) {
	    	//progressMonitor.setTaskName(EMFEditUIPlugin.INSTANCE.getString("_UI_Validating_message", new Object [] {diagnostician.getObjectLabel(eObject)}));
	    	diagnostician.validate( eObject, diagnostic, context );
	    }
	}
	
	protected BasicDiagnostic createDiagnostic( final Diagnostician diagnostician,
												final EObject... objects ) {
	    final BasicDiagnostic diagnostic;
	    
	    if ( objects.length == 1 ) {
	    	diagnostic = diagnostician.createDefaultDiagnostic( objects[ 0 ] );
	    }
	    else {
	    	diagnostic = new BasicDiagnostic(	EObjectValidator.DIAGNOSTIC_SOURCE,
	    		  								0,
	    		  								"TODO",
	    		  								//EMFEditUIPlugin.INSTANCE.getString("_UI_DiagnosisOfNObjects_message", 
	    		  								//new String[] { Integer.toString( selectionSize ) } ),
	    		  								objects ); 
	    }
	    
	    return diagnostic;
	}

	protected Diagnostician createDiagnostician( /*final IProgressMonitor progressMonitor*/ ) {
	    return new Diagnostician() {
	        @Override
	        public String getObjectLabel( final EObject eObject ) {
	        	if ( !eObject.eIsProxy() ) {
	        		final IItemLabelProvider itemLabelProvider = (IItemLabelProvider) defaultAdapterFactory.adapt( 	eObject, 
	        																										IItemLabelProvider.class);
	        		if ( itemLabelProvider != null ) {
	        			return itemLabelProvider.getText( eObject );
	        		}
	        	}
	  
	        	return super.getObjectLabel( eObject );
	        }
	  
//	        @Override
//	        public boolean validate(EClass eClass, EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context)
//	        {
//	          progressMonitor.worked(1);
//	          return super.validate(eClass, eObject, diagnostics, context);
//	        }
	      };
	}

	@Override
	public void validate( 	final int pi_featureId,
							final boolean pb_validateFeatureValue,
							final EObject... objects )
	throws InterruptedException {
	    final Diagnostician diagnostician = createDiagnostician( /*progressMonitor*/);
	    final BasicDiagnostic diagnostic = createDiagnostic( diagnostician, objects );
	    final Map<Object, Object> context = diagnostician.createDefaultContext();
		
		for ( final EObject object : objects ) {
			final EClass eClass = object.eClass();
			final EStructuralFeature feature = eClass.getEStructuralFeature( pi_featureId );
			validate_EveryDefaultConstraint( object, feature, diagnostic, context );
			
			if ( pb_validateFeatureValue ) {
				validateFeatureValue( object, feature, diagnostician, diagnostic, context );
			}
		}
		
		checkDiagnostic( diagnostic );
	}
	
	@SuppressWarnings("unchecked")
	protected void validateFeatureValue( 	final EObject parent,
											final EStructuralFeature feature,
											final Diagnostician diagnostician,
											final BasicDiagnostic diagnostic,
											final Map<Object, Object> context ) {
		final Object featureValue = parent.eGet( feature );
		
		if ( featureValue instanceof EObject ) {
			validate( diagnostician, diagnostic, context, (EObject) featureValue );
		}
		else if ( featureValue instanceof EList<?> ) {
			for ( final EObject eObject : (EList<EObject>) featureValue ) {
				validate( diagnostician, diagnostic, context, eObject );
			}
		}
	}
	
	protected void validate( 	final EObject object,
								final EStructuralFeature feature,
								final EObject featureEobjectValue )
	throws InterruptedException {
		if ( featureEobjectValue != null ) {
			validate( featureEobjectValue );
		}
	}
	

	public boolean validate_EveryDefaultConstraint( final EObject object,
													final EStructuralFeature feature,
													final DiagnosticChain diagnostics, 
													final Map<Object, Object> context ) {
		boolean result = validate_MultiplicityConforms( object, feature, diagnostics, context );
	    
		if ( result || diagnostics != null ) {
			result &= validate_ProxyResolves( object, feature, diagnostics, context );
		}
	    
		// FIXME: Finish creating methods per feature
//		if (result || theDiagnostics != null)
//	    {
//	      result &= validate_EveryReferenceIsContained(object, theDiagnostics, context);
//	    }
//	    if (result || theDiagnostics != null)
//	    {
//	      result &= validate_EveryBidirectionalReferenceIsPaired(object, theDiagnostics, context);
//	    }
//	    if (result || theDiagnostics != null)
//	    {
//	      result &= validate_EveryDataValueConforms(object, theDiagnostics, context);
//	    }
//	    if (result || theDiagnostics != null)
//	    {
//	      result &= validate_UniqueID(object, theDiagnostics, context);
//	    }
//	    if (result || theDiagnostics != null)
//	    {
//	      result &= validate_EveryKeyUnique(object, theDiagnostics, context);
//	    }
//	    if (result || theDiagnostics != null)
//	    {
//	      result &= validate_EveryMapEntryUnique(object, theDiagnostics, context);
//	    }
	    return result;
	}

	@SuppressWarnings("unchecked")
	public boolean validate_ProxyResolves( 	final EObject parentObject, 
		  									final EStructuralFeature feature,
		  									final DiagnosticChain diagnostics, 
			  									final Map<Object, Object> context ) {
		final Object featureObject = parentObject.eGet( feature );
		    
		if ( featureObject instanceof EObject ) {
			return validateResolvedProxy( parentObject, feature, (EObject) featureObject, diagnostics, context );
	    }
		  
		boolean result = true;
		  
		if ( featureObject instanceof EList<?> ) {
			for ( final EObject eObject : (EList<EObject>) featureObject ) {
				result = result & validateResolvedProxy( parentObject, feature, eObject, diagnostics, context );
			}
		}

		return result;
	}
	  
	private boolean validateResolvedProxy( 	final EObject parentObject,
		  									final EStructuralFeature feature,
		  									final EObject refObject,
		  									final DiagnosticChain diagnostics, 
		  									final Map<Object, Object> context	) {
		boolean result = true;

		if ( refObject.eIsProxy() ) {
			result = false;
		  
			if ( diagnostics != null ) {
				diagnostics.add
	            (createDiagnostic
	              (Diagnostic.ERROR,
	               DIAGNOSTIC_SOURCE,
	               EOBJECT__EVERY_PROXY_RESOLVES,
	               "_UI_UnresolvedProxy_diagnostic",
	               new Object []
	               {
	                 getFeatureLabel( feature, context ),
	                 getObjectLabel( refObject, context ),
	                 getObjectLabel( refObject, context )
	               },
	               new Object [] { parentObject, feature, refObject },
	               context ));
			}
		}
		  
		return result;
	}
}
