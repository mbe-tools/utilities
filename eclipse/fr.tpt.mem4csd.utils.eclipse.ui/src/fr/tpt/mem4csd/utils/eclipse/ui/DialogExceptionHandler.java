package fr.tpt.mem4csd.utils.eclipse.ui;

import fr.mem4csd.util.exception.BaseExceptionHandler;

import java.util.logging.Logger;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public class DialogExceptionHandler extends BaseExceptionHandler {

	private static final String UNKNOWN_ERROR_MSG = "Please see error log view for more info.";
	
	private final String title;
	
	public DialogExceptionHandler( 	final String title,
									final Logger p_logger ) {
		super( p_logger );
		
		this.title = title;
	}

	@Override
	public void handleException( 	final Throwable p_th, 
									final Object p_info) {
		super.handleException( p_th, p_info );
		
		showError( title, ( ( p_th.getMessage() == null || "".equals( p_th.getLocalizedMessage() ) ) ? ""  : p_th.getLocalizedMessage() + ": " ) + UNKNOWN_ERROR_MSG );
	}

	protected void showError( 	final String p_errorTitle,
								final String p_message ) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				
				final Shell shell = window == null ? null : window.getShell();
				
				if ( shell != null ) {
					MessageDialog.openError( shell,	p_errorTitle, p_message );
				}
			}
		} );
	}
}
