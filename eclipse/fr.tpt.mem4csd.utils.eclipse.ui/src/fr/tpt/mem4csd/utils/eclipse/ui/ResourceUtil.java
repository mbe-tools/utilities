package fr.tpt.mem4csd.utils.eclipse.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourceAttributes;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.ui.IWorkingSet;
import org.eclipse.ui.PlatformUI;

public class ResourceUtil {
	
	private ResourceUtil() {
	}
	
	public static IWorkingSet getWorkingSet( final String workingSetName ) {
		for ( final IWorkingSet workSet : PlatformUI.getWorkbench().getWorkingSetManager().getWorkingSets() ) {
			if ( workingSetName.equals( workSet.getName() ) ) {
				return workSet;
			}
		}
		
		return null;
	}

	public static boolean addToWorkingSet( 	final IProject project,
											final String workingSetName ) {
		final IWorkingSet foundWorkSet = getWorkingSet( workingSetName );

		if ( foundWorkSet != null ) {
			final List<IAdaptable> wsProjects = new ArrayList<IAdaptable>( Arrays.asList( foundWorkSet.getElements() ) );
			wsProjects.add( project );
			foundWorkSet.setElements( wsProjects.toArray( new IAdaptable[ wsProjects.size() ] ) );
			
			return true;
		}
		
		return false;
	}

	public static boolean removeFromWorkingSet( final IProject project,
												final String workingSetName ) {
		final IWorkingSet foundWorkSet = getWorkingSet( workingSetName );

		if ( foundWorkSet != null ) {
			final List<IAdaptable> wsProjects = new ArrayList<IAdaptable>( Arrays.asList( foundWorkSet.getElements() ) );
			final boolean removed = wsProjects.remove( project );
			foundWorkSet.setElements( wsProjects.toArray( new IAdaptable[ wsProjects.size() ] ) );
			
			return removed;
		}
		
		return false;
	}

	public static void removeNature(	final Set<IProject> projects,
										final String natureId)
	throws CoreException {
		for (final IProject project : projects) {
			removeNature(project, natureId);
		}
	}

	public static boolean removeNature(	final IProject project,
										final String natureId )
	throws CoreException {
		if ( project.hasNature(natureId) && !project.getResourceAttributes().isReadOnly() ) {
			final IProjectDescription descr = project.getDescription();
			final List<String> newNatures = new ArrayList<String>();
			newNatures.addAll(Arrays.asList(descr.getNatureIds()));
			newNatures.remove(natureId);
			descr.setNatureIds(newNatures.toArray(new String[newNatures.size()]));
			project.setDescription(descr, null);

			return true;
		}

		return false;
	}

	public static void addNature(	final Set<IProject> projects,
									final String natureId )
	throws CoreException {
		for (final IProject project : projects) {
			addNature(project, natureId);
		}
	}
	
	public static boolean containsProject( 	final Collection<IProject> projects,
											final IProject project ) {
		final String projName = project.getName();
		
		for ( final IProject refProj : projects ) {
			if ( projName.equals( refProj.getName() ) ) {
				return true;
			}
		}
		
		return false;
	}
	
	public static void addReferencedProjects( 	final IProject project,
												final Set<IProject> projects )
	throws CoreException {
		if ( !projects.isEmpty() ) {
			final IProjectDescription descr = project.getDescription();
			final Set<IProject> refProjects = new HashSet<IProject>();
			refProjects.addAll( Arrays.asList( project.getReferencedProjects() ) );
			final String projName = project.getName();
			
			for ( final IProject refProject : projects ) {
				// Avoid self referring projects.
				if ( projName != refProject.getName() && !containsProject( refProjects, refProject ) ) {
					refProjects.add( refProject );
				}
			}
			
			descr.setReferencedProjects( refProjects.toArray( new IProject[ refProjects.size() ] ) );
			project.setDescription(descr, null);
		}
	}

	public static boolean addNature(	final IProject project,
										final String natureId )
	throws CoreException {
		if (project.hasNature(natureId)) {
			return false;
		}

		final IProjectDescription descr = project.getDescription();
		final List<String> newNatures = new ArrayList<String>();
		newNatures.addAll(Arrays.asList(descr.getNatureIds()));
		newNatures.add(natureId);
		descr.setNatureIds(newNatures.toArray(new String[newNatures.size()]));
		project.setDescription(descr, null);

		return true;
	}

	public static Set<IProject> getReferencedProjects( final IProject project )
	throws CoreException {
		return getReferencedProjects( project, new HashSet<String>() );
	}

	public static Set<IProject> getReferencedProjects( final Collection<IProject> projects )
	throws CoreException {
		final Set<IProject> allProjects = new LinkedHashSet<IProject>();

		for ( final IProject project : projects ) {
			if ( project.isAccessible() ) {
				allProjects.addAll( getReferencedProjects( project ) );
				allProjects.add( project );
			}
		}
		
		return allProjects;
	}

	private static Set<IProject> getReferencedProjects( final IProject project,
														final Set<String> traversedProjects ) 
	throws CoreException {
		final Set<IProject> refProjects = new LinkedHashSet<IProject>();
		
		for ( final IProject refProj : project.getReferencedProjects() ) {
			final String projName = refProj.getName();
			
			if ( !traversedProjects.contains( projName ) ) {
				traversedProjects.add( projName );
				
				if ( refProj.isAccessible() ) {
					refProjects.add( refProj );
					refProjects.addAll( getReferencedProjects( refProj, traversedProjects ) );
				}
			}
		}
		
		return refProjects;
	}
	
	public static void setReadOnly( final IResource res,
									final boolean readOnly,
									final boolean recursive ) 
	throws CoreException {
		if ( res.isAccessible() ) {
			final ResourceAttributes resAtt = res.getResourceAttributes();
		
			if ( readOnly != resAtt.isReadOnly() ) {
				resAtt.setReadOnly( readOnly );
				res.setResourceAttributes( resAtt );
			}
			
			if ( recursive && res instanceof IContainer ) {
				setReadOnly( (IContainer) res, readOnly );
			}
		}
	}
	
	private static void setReadOnly( 	final IContainer container,
										final boolean readOnly ) 
	throws CoreException {
		for ( final IResource res : container.members() ) {
			setReadOnly( res, readOnly, true );
		}
	}
}
