package fr.tpt.mem4csd.utils.eclipse.ui;

import fr.mem4csd.utils.eclipse.BundleUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

/**
 * A class to open editors of various kinds within the Eclipse IDE.
 * 
 */
public class EditorUtil {

	public static void openEditors( final Collection<String> resourcePaths ) 
	throws PartInitException, IOException {
		for ( final String path : resourcePaths) {
			openEditor( path );
		}
	}

	public static void openEditor( final String resourcePath ) 
	throws PartInitException, IOException {
		final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		
		if ( window != null && window.getActivePage() != null ) {
			final IFile file = ResourcesPlugin.getWorkspace().getRoot().getFileForLocation( new Path( resourcePath ) );
			
			if ( file == null ) {
				// The file may be outside the workspace
				IFileStore fileStore = EFS.getLocalFileSystem().getStore( new Path( resourcePath ) );
				
				if ( !fileStore.fetchInfo().exists() ) {
					fileStore = EFS.getLocalFileSystem().getStore( new Path( resourcePath ) );

					if ( !fileStore.fetchInfo().exists() ) {
						throw createResourceNotFoundException( resourcePath );
					}
				}

				IDE.openEditorOnFileStore( window.getActivePage(), fileStore );
			}
			else {
				IDE.openEditor( window.getActivePage(), file );
			}
		}
	}
	
	private static IOException createResourceNotFoundException( final String resourcePath ) {
		return new IOException( "Unable to find resource " + resourcePath );
	}

	public static Collection<String> getEditorExtensions( final Class<? extends IEditorPart> editorClass ) {
		for (final IExtension extension : BundleUtil.getExtensionPointExtensions( "org.eclipse.ui", "editors" ) ) {
			final IConfigurationElement[] elements = extension.getConfigurationElements();
			final String editorClassName = editorClass.getName();

			for (final IConfigurationElement element : elements) {
				if (	"editor".equals(element.getName() ) &&
						editorClassName.equals(element.getAttribute("class"))) {
					final Collection<String> extensions = new ArrayList<String>();
					final StringTokenizer tokenizer = new StringTokenizer( element.getAttribute("extensions"), "," );

					while (tokenizer.hasMoreTokens()) {
						extensions.add(tokenizer.nextToken());
					}

					return extensions;
				}
			}
		}

		return null;
	}
}
