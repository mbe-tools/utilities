/**
 */
package fr.tpt.mem4csd.analysis.model.analysis;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Artifact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.analysis.model.analysis.AnalysisArtifact#getResults <em>Results</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.analysis.model.analysis.AnalysisPackage#getAnalysisArtifact()
 * @model
 * @generated
 */
public interface AnalysisArtifact extends EObject {
	/**
	 * Returns the value of the '<em><b>Results</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.analysis.model.analysis.AnalysisResult}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Results</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Results</em>' containment reference list.
	 * @see fr.tpt.mem4csd.analysis.model.analysis.AnalysisPackage#getAnalysisArtifact_Results()
	 * @model containment="true"
	 * @generated
	 */
	EList<AnalysisResult> getResults();

} // AnalysisArtifact
