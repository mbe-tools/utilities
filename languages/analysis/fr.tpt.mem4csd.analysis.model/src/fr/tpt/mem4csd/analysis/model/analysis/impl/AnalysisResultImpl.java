/**
 * AADL-RAMSES
 * 
 * Copyright © 2014 TELECOM ParisTech and CNRS
 * 
 * TELECOM ParisTech/LTCI
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.  If not, see 
 * http://www.eclipse.org/org/documents/epl-v10.php
 */

package fr.tpt.mem4csd.analysis.model.analysis.impl;


import fr.tpt.mem4csd.analysis.model.analysis.AnalysisPackage;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import fr.tpt.mem4csd.analysis.model.analysis.AnalysisResult;
import fr.tpt.mem4csd.analysis.model.analysis.AnalysisSource;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Result</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.analysis.model.analysis.impl.AnalysisResultImpl#getResultUUId <em>Result UU Id</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.analysis.model.analysis.impl.AnalysisResultImpl#getNfpId <em>Nfp Id</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.analysis.model.analysis.impl.AnalysisResultImpl#getSource <em>Source</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AnalysisResultImpl extends MinimalEObjectImpl.Container implements AnalysisResult {
	/**
	 * The default value of the '{@link #getResultUUId() <em>Result UU Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultUUId()
	 * @generated
	 * @ordered
	 */
	protected static final String RESULT_UU_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResultUUId() <em>Result UU Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultUUId()
	 * @generated
	 * @ordered
	 */
	protected String resultUUId = RESULT_UU_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getNfpId() <em>Nfp Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNfpId()
	 * @generated
	 * @ordered
	 */
	protected static final String NFP_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNfpId() <em>Nfp Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNfpId()
	 * @generated
	 * @ordered
	 */
	protected String nfpId = NFP_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected AnalysisSource source;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnalysisResultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnalysisPackage.Literals.ANALYSIS_RESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getResultUUId() {
		return resultUUId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNfpId() {
		return nfpId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNfpId(String newNfpId) {
		String oldNfpId = nfpId;
		nfpId = newNfpId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AnalysisPackage.ANALYSIS_RESULT__NFP_ID, oldNfpId, nfpId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalysisSource getSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public NotificationChain basicSetSource(AnalysisSource newSource, NotificationChain msgs)
  {
		AnalysisSource oldSource = source;
		source = newSource;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AnalysisPackage.ANALYSIS_RESULT__SOURCE, oldSource, newSource);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(AnalysisSource newSource) {
		if (newSource != source) {
			NotificationChain msgs = null;
			if (source != null)
				msgs = ((InternalEObject)source).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AnalysisPackage.ANALYSIS_RESULT__SOURCE, null, msgs);
			if (newSource != null)
				msgs = ((InternalEObject)newSource).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AnalysisPackage.ANALYSIS_RESULT__SOURCE, null, msgs);
			msgs = basicSetSource(newSource, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AnalysisPackage.ANALYSIS_RESULT__SOURCE, newSource, newSource));
	}

	/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case AnalysisPackage.ANALYSIS_RESULT__SOURCE:
				return basicSetSource(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AnalysisPackage.ANALYSIS_RESULT__RESULT_UU_ID:
				return getResultUUId();
			case AnalysisPackage.ANALYSIS_RESULT__NFP_ID:
				return getNfpId();
			case AnalysisPackage.ANALYSIS_RESULT__SOURCE:
				return getSource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AnalysisPackage.ANALYSIS_RESULT__NFP_ID:
				setNfpId((String)newValue);
				return;
			case AnalysisPackage.ANALYSIS_RESULT__SOURCE:
				setSource((AnalysisSource)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AnalysisPackage.ANALYSIS_RESULT__NFP_ID:
				setNfpId(NFP_ID_EDEFAULT);
				return;
			case AnalysisPackage.ANALYSIS_RESULT__SOURCE:
				setSource((AnalysisSource)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AnalysisPackage.ANALYSIS_RESULT__RESULT_UU_ID:
				return RESULT_UU_ID_EDEFAULT == null ? resultUUId != null : !RESULT_UU_ID_EDEFAULT.equals(resultUUId);
			case AnalysisPackage.ANALYSIS_RESULT__NFP_ID:
				return NFP_ID_EDEFAULT == null ? nfpId != null : !NFP_ID_EDEFAULT.equals(nfpId);
			case AnalysisPackage.ANALYSIS_RESULT__SOURCE:
				return source != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (resultUUId: ");
		result.append(resultUUId);
		result.append(", nfpId: ");
		result.append(nfpId);
		result.append(')');
		return result.toString();
	}

} //AnalysisResultImpl
