/**
 * AADL-RAMSES
 * 
 * Copyright © 2014 TELECOM ParisTech and CNRS
 * 
 * TELECOM ParisTech/LTCI
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.  If not, see 
 * http://www.eclipse.org/org/documents/epl-v10.php
 */

package fr.tpt.mem4csd.analysis.model.analysis.util;

import java.io.IOException ;

import org.apache.log4j.Logger ;
import org.eclipse.emf.ecore.resource.Resource ;
import org.eclipse.emf.transaction.RecordingCommand ;
import org.eclipse.emf.transaction.TransactionalCommandStack ;
import org.eclipse.emf.transaction.TransactionalEditingDomain ;

import fr.tpt.mem4csd.analysis.model.analysis.AnalysisArtifact;
import fr.tpt.mem4csd.analysis.model.analysis.AnalysisResult;
import fr.tpt.mem4csd.analysis.model.analysis.AnalysisSource;
import fr.tpt.mem4csd.analysis.model.analysis.QualitativeAnalysisResult;
import fr.tpt.mem4csd.analysis.model.analysis.QuantitativeAnalysisResult;

public class AnalysisUtils {

	private static Logger _LOGGER = Logger.getLogger(AnalysisUtils.class) ;
	

	/**
	 * Saves the given specification at the resource location 
	 *
	 * @param resource   	Resource object associated with the specific disk location
	 * @param specification	AnalysisArtifact object to be saved
	 */
	public static void saveAnalysisArtifact(Resource resource){
		resource.unload();
		
		try
		{
			resource.save(null);
		}
		catch (IOException e)
		{
			String msg = "cannot save resource" ;
			_LOGGER.fatal(msg, e);
		  throw new RuntimeException(msg, e) ;
		}
	}

  public static void updateAnalysisArtifact(final AnalysisArtifact existingArtefact,
                                      final AnalysisArtifact newArtefact)
  {
    
    TransactionalEditingDomain domain = TransactionalEditingDomain.Registry.INSTANCE.getEditingDomain(
        "org.osate.aadl2.ModelEditingDomain");
    RecordingCommand transactionCmd = new RecordingCommand(domain)
    {
      protected void doExecute()
      {
        boolean foundAnalysisToUpdate = false;
        boolean isNew = true;
        for(Object existingAr: existingArtefact.getResults())
        {
          AnalysisResult existingQar =
              (AnalysisResult) existingAr;
          AnalysisSource existingAs = existingQar.getSource();
          for(Object newAr: newArtefact.getResults())
          {
            AnalysisResult newQar =
                (AnalysisResult) newAr;
            if(newQar instanceof QualitativeAnalysisResult
                && existingQar instanceof QuantitativeAnalysisResult)
              continue;
            if(newQar instanceof QuantitativeAnalysisResult
                && existingQar instanceof QualitativeAnalysisResult)
              continue;
            AnalysisSource newAs = newQar.getSource();
            if(newAs.getIterationId() == 
                existingAs.getIterationId()
                &&
                newAs.getMethodName().equals(existingAs.getMethodName())
                &&
                newAs.getScope().equals(existingAs.getScope())
                )
            {
              updateAnalysisResult(existingQar, newQar);
              foundAnalysisToUpdate = true;
              break;
            }
          }
          if(foundAnalysisToUpdate)
          {
            foundAnalysisToUpdate = false;
            isNew = false;
          }
        }
        if(isNew)
          existingArtefact.getResults().addAll(newArtefact.getResults());
      }
    };
    try {
		((TransactionalCommandStack) domain.getCommandStack()).execute(transactionCmd, null);
	} catch (Exception e) {
		_LOGGER.error("Error when updating analysis results file", e);
	}
  }

  public static void updateAnalysisResult(AnalysisResult existingQar,
                                    AnalysisResult newQar)
  {
    if(existingQar instanceof QualitativeAnalysisResult
        && newQar instanceof QualitativeAnalysisResult)
    {
      QualitativeAnalysisResult existingQarCasted = 
          (QualitativeAnalysisResult) existingQar;
      QualitativeAnalysisResult newQarCasted = 
          (QualitativeAnalysisResult) newQar;
      existingQarCasted.setValidated(newQarCasted.isValidated());
    }
    else if(existingQar instanceof QuantitativeAnalysisResult
        && newQar instanceof QuantitativeAnalysisResult)
    {
      QuantitativeAnalysisResult existingQarCasted = 
          (QuantitativeAnalysisResult) existingQar;
      QuantitativeAnalysisResult newQarCasted = 
          (QuantitativeAnalysisResult) newQar;
      existingQarCasted.setMargin(newQarCasted.getMargin());
      existingQarCasted.setValue(newQarCasted.getValue());
    }
  }
  
}