/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.filesystem.model.filesystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.labsticc.filesystem.model.filesystem.FileSystem#getDrives <em>Drives</em>}</li>
 *   <li>{@link fr.labsticc.filesystem.model.filesystem.FileSystem#getSyncs <em>Syncs</em>}</li>
 * </ul>
 *
 * @see fr.labsticc.filesystem.model.filesystem.FilesystemPackage#getFileSystem()
 * @model
 * @generated
 */
public interface FileSystem extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Drives</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.filesystem.model.filesystem.Drive}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Drives</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Drives</em>' containment reference list.
	 * @see fr.labsticc.filesystem.model.filesystem.FilesystemPackage#getFileSystem_Drives()
	 * @model containment="true"
	 * @generated
	 */
	EList<Drive> getDrives();

	/**
	 * Returns the value of the '<em><b>Syncs</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.filesystem.model.filesystem.Sync}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Syncs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Syncs</em>' containment reference list.
	 * @see fr.labsticc.filesystem.model.filesystem.FilesystemPackage#getFileSystem_Syncs()
	 * @model containment="true"
	 * @generated
	 */
	EList<Sync> getSyncs();

} // FileSystem
