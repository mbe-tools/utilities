/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.filesystem.model.filesystem.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.labsticc.filesystem.model.filesystem.Drive;
import fr.labsticc.filesystem.model.filesystem.FileSystem;
import fr.labsticc.filesystem.model.filesystem.FilesystemPackage;
import fr.labsticc.filesystem.model.filesystem.Sync;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>File System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.labsticc.filesystem.model.filesystem.impl.FileSystemImpl#getDrives <em>Drives</em>}</li>
 *   <li>{@link fr.labsticc.filesystem.model.filesystem.impl.FileSystemImpl#getSyncs <em>Syncs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FileSystemImpl extends IdentifiedElementImpl implements FileSystem {
	/**
	 * The cached value of the '{@link #getDrives() <em>Drives</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDrives()
	 * @generated
	 * @ordered
	 */
	protected EList<Drive> drives;

	/**
	 * The cached value of the '{@link #getSyncs() <em>Syncs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSyncs()
	 * @generated
	 * @ordered
	 */
	protected EList<Sync> syncs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FileSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FilesystemPackage.Literals.FILE_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Drive> getDrives() {
		if (drives == null) {
			drives = new EObjectContainmentEList<Drive>(Drive.class, this, FilesystemPackage.FILE_SYSTEM__DRIVES);
		}
		return drives;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Sync> getSyncs() {
		if (syncs == null) {
			syncs = new EObjectContainmentEList<Sync>(Sync.class, this, FilesystemPackage.FILE_SYSTEM__SYNCS);
		}
		return syncs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FilesystemPackage.FILE_SYSTEM__DRIVES:
				return ((InternalEList<?>)getDrives()).basicRemove(otherEnd, msgs);
			case FilesystemPackage.FILE_SYSTEM__SYNCS:
				return ((InternalEList<?>)getSyncs()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FilesystemPackage.FILE_SYSTEM__DRIVES:
				return getDrives();
			case FilesystemPackage.FILE_SYSTEM__SYNCS:
				return getSyncs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FilesystemPackage.FILE_SYSTEM__DRIVES:
				getDrives().clear();
				getDrives().addAll((Collection<? extends Drive>)newValue);
				return;
			case FilesystemPackage.FILE_SYSTEM__SYNCS:
				getSyncs().clear();
				getSyncs().addAll((Collection<? extends Sync>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FilesystemPackage.FILE_SYSTEM__DRIVES:
				getDrives().clear();
				return;
			case FilesystemPackage.FILE_SYSTEM__SYNCS:
				getSyncs().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FilesystemPackage.FILE_SYSTEM__DRIVES:
				return drives != null && !drives.isEmpty();
			case FilesystemPackage.FILE_SYSTEM__SYNCS:
				return syncs != null && !syncs.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FileSystemImpl
