/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.labsticc.filesystem.model.filesystem;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Drive</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.filesystem.model.filesystem.FilesystemPackage#getDrive()
 * @model
 * @generated
 */
public interface Drive extends Folder {
} // Drive
