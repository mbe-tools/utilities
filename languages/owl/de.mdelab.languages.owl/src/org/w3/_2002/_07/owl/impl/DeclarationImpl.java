/**
 */
package org.w3._2002._07.owl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.w3._2002._07.owl.AnnotationProperty;
import org.w3._2002._07.owl.DataProperty;
import org.w3._2002._07.owl.Datatype;
import org.w3._2002._07.owl.Declaration;
import org.w3._2002._07.owl.NamedIndividual;
import org.w3._2002._07.owl.ObjectProperty;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.DeclarationImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DeclarationImpl#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DeclarationImpl#getObjectProperty <em>Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DeclarationImpl#getDataProperty <em>Data Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DeclarationImpl#getAnnotationProperty <em>Annotation Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DeclarationImpl#getNamedIndividual <em>Named Individual</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeclarationImpl extends AxiomImpl implements Declaration {
	/**
	 * The cached value of the '{@link #getClass_() <em>Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClass_()
	 * @generated
	 * @ordered
	 */
	protected org.w3._2002._07.owl.Class class_;

	/**
	 * The cached value of the '{@link #getDatatype() <em>Datatype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatatype()
	 * @generated
	 * @ordered
	 */
	protected Datatype datatype;

	/**
	 * The cached value of the '{@link #getObjectProperty() <em>Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectProperty()
	 * @generated
	 * @ordered
	 */
	protected ObjectProperty objectProperty;

	/**
	 * The cached value of the '{@link #getDataProperty() <em>Data Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataProperty()
	 * @generated
	 * @ordered
	 */
	protected DataProperty dataProperty;

	/**
	 * The cached value of the '{@link #getAnnotationProperty() <em>Annotation Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotationProperty()
	 * @generated
	 * @ordered
	 */
	protected AnnotationProperty annotationProperty;

	/**
	 * The cached value of the '{@link #getNamedIndividual() <em>Named Individual</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamedIndividual()
	 * @generated
	 * @ordered
	 */
	protected NamedIndividual namedIndividual;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getDeclaration();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.w3._2002._07.owl.Class getClass_() {
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClass(org.w3._2002._07.owl.Class newClass, NotificationChain msgs) {
		org.w3._2002._07.owl.Class oldClass = class_;
		class_ = newClass;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.DECLARATION__CLASS, oldClass, newClass);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClass(org.w3._2002._07.owl.Class newClass) {
		if (newClass != class_) {
			NotificationChain msgs = null;
			if (class_ != null)
				msgs = ((InternalEObject)class_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.DECLARATION__CLASS, null, msgs);
			if (newClass != null)
				msgs = ((InternalEObject)newClass).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.DECLARATION__CLASS, null, msgs);
			msgs = basicSetClass(newClass, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.DECLARATION__CLASS, newClass, newClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Datatype getDatatype() {
		return datatype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDatatype(Datatype newDatatype, NotificationChain msgs) {
		Datatype oldDatatype = datatype;
		datatype = newDatatype;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.DECLARATION__DATATYPE, oldDatatype, newDatatype);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDatatype(Datatype newDatatype) {
		if (newDatatype != datatype) {
			NotificationChain msgs = null;
			if (datatype != null)
				msgs = ((InternalEObject)datatype).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.DECLARATION__DATATYPE, null, msgs);
			if (newDatatype != null)
				msgs = ((InternalEObject)newDatatype).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.DECLARATION__DATATYPE, null, msgs);
			msgs = basicSetDatatype(newDatatype, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.DECLARATION__DATATYPE, newDatatype, newDatatype));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectProperty getObjectProperty() {
		return objectProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectProperty(ObjectProperty newObjectProperty, NotificationChain msgs) {
		ObjectProperty oldObjectProperty = objectProperty;
		objectProperty = newObjectProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.DECLARATION__OBJECT_PROPERTY, oldObjectProperty, newObjectProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectProperty(ObjectProperty newObjectProperty) {
		if (newObjectProperty != objectProperty) {
			NotificationChain msgs = null;
			if (objectProperty != null)
				msgs = ((InternalEObject)objectProperty).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.DECLARATION__OBJECT_PROPERTY, null, msgs);
			if (newObjectProperty != null)
				msgs = ((InternalEObject)newObjectProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.DECLARATION__OBJECT_PROPERTY, null, msgs);
			msgs = basicSetObjectProperty(newObjectProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.DECLARATION__OBJECT_PROPERTY, newObjectProperty, newObjectProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataProperty getDataProperty() {
		return dataProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataProperty(DataProperty newDataProperty, NotificationChain msgs) {
		DataProperty oldDataProperty = dataProperty;
		dataProperty = newDataProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.DECLARATION__DATA_PROPERTY, oldDataProperty, newDataProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataProperty(DataProperty newDataProperty) {
		if (newDataProperty != dataProperty) {
			NotificationChain msgs = null;
			if (dataProperty != null)
				msgs = ((InternalEObject)dataProperty).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.DECLARATION__DATA_PROPERTY, null, msgs);
			if (newDataProperty != null)
				msgs = ((InternalEObject)newDataProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.DECLARATION__DATA_PROPERTY, null, msgs);
			msgs = basicSetDataProperty(newDataProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.DECLARATION__DATA_PROPERTY, newDataProperty, newDataProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationProperty getAnnotationProperty() {
		return annotationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotationProperty(AnnotationProperty newAnnotationProperty, NotificationChain msgs) {
		AnnotationProperty oldAnnotationProperty = annotationProperty;
		annotationProperty = newAnnotationProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.DECLARATION__ANNOTATION_PROPERTY, oldAnnotationProperty, newAnnotationProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotationProperty(AnnotationProperty newAnnotationProperty) {
		if (newAnnotationProperty != annotationProperty) {
			NotificationChain msgs = null;
			if (annotationProperty != null)
				msgs = ((InternalEObject)annotationProperty).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.DECLARATION__ANNOTATION_PROPERTY, null, msgs);
			if (newAnnotationProperty != null)
				msgs = ((InternalEObject)newAnnotationProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.DECLARATION__ANNOTATION_PROPERTY, null, msgs);
			msgs = basicSetAnnotationProperty(newAnnotationProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.DECLARATION__ANNOTATION_PROPERTY, newAnnotationProperty, newAnnotationProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedIndividual getNamedIndividual() {
		return namedIndividual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamedIndividual(NamedIndividual newNamedIndividual, NotificationChain msgs) {
		NamedIndividual oldNamedIndividual = namedIndividual;
		namedIndividual = newNamedIndividual;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.DECLARATION__NAMED_INDIVIDUAL, oldNamedIndividual, newNamedIndividual);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamedIndividual(NamedIndividual newNamedIndividual) {
		if (newNamedIndividual != namedIndividual) {
			NotificationChain msgs = null;
			if (namedIndividual != null)
				msgs = ((InternalEObject)namedIndividual).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.DECLARATION__NAMED_INDIVIDUAL, null, msgs);
			if (newNamedIndividual != null)
				msgs = ((InternalEObject)newNamedIndividual).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.DECLARATION__NAMED_INDIVIDUAL, null, msgs);
			msgs = basicSetNamedIndividual(newNamedIndividual, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.DECLARATION__NAMED_INDIVIDUAL, newNamedIndividual, newNamedIndividual));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.DECLARATION__CLASS:
				return basicSetClass(null, msgs);
			case OwlPackage.DECLARATION__DATATYPE:
				return basicSetDatatype(null, msgs);
			case OwlPackage.DECLARATION__OBJECT_PROPERTY:
				return basicSetObjectProperty(null, msgs);
			case OwlPackage.DECLARATION__DATA_PROPERTY:
				return basicSetDataProperty(null, msgs);
			case OwlPackage.DECLARATION__ANNOTATION_PROPERTY:
				return basicSetAnnotationProperty(null, msgs);
			case OwlPackage.DECLARATION__NAMED_INDIVIDUAL:
				return basicSetNamedIndividual(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.DECLARATION__CLASS:
				return getClass_();
			case OwlPackage.DECLARATION__DATATYPE:
				return getDatatype();
			case OwlPackage.DECLARATION__OBJECT_PROPERTY:
				return getObjectProperty();
			case OwlPackage.DECLARATION__DATA_PROPERTY:
				return getDataProperty();
			case OwlPackage.DECLARATION__ANNOTATION_PROPERTY:
				return getAnnotationProperty();
			case OwlPackage.DECLARATION__NAMED_INDIVIDUAL:
				return getNamedIndividual();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.DECLARATION__CLASS:
				setClass((org.w3._2002._07.owl.Class)newValue);
				return;
			case OwlPackage.DECLARATION__DATATYPE:
				setDatatype((Datatype)newValue);
				return;
			case OwlPackage.DECLARATION__OBJECT_PROPERTY:
				setObjectProperty((ObjectProperty)newValue);
				return;
			case OwlPackage.DECLARATION__DATA_PROPERTY:
				setDataProperty((DataProperty)newValue);
				return;
			case OwlPackage.DECLARATION__ANNOTATION_PROPERTY:
				setAnnotationProperty((AnnotationProperty)newValue);
				return;
			case OwlPackage.DECLARATION__NAMED_INDIVIDUAL:
				setNamedIndividual((NamedIndividual)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.DECLARATION__CLASS:
				setClass((org.w3._2002._07.owl.Class)null);
				return;
			case OwlPackage.DECLARATION__DATATYPE:
				setDatatype((Datatype)null);
				return;
			case OwlPackage.DECLARATION__OBJECT_PROPERTY:
				setObjectProperty((ObjectProperty)null);
				return;
			case OwlPackage.DECLARATION__DATA_PROPERTY:
				setDataProperty((DataProperty)null);
				return;
			case OwlPackage.DECLARATION__ANNOTATION_PROPERTY:
				setAnnotationProperty((AnnotationProperty)null);
				return;
			case OwlPackage.DECLARATION__NAMED_INDIVIDUAL:
				setNamedIndividual((NamedIndividual)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.DECLARATION__CLASS:
				return class_ != null;
			case OwlPackage.DECLARATION__DATATYPE:
				return datatype != null;
			case OwlPackage.DECLARATION__OBJECT_PROPERTY:
				return objectProperty != null;
			case OwlPackage.DECLARATION__DATA_PROPERTY:
				return dataProperty != null;
			case OwlPackage.DECLARATION__ANNOTATION_PROPERTY:
				return annotationProperty != null;
			case OwlPackage.DECLARATION__NAMED_INDIVIDUAL:
				return namedIndividual != null;
		}
		return super.eIsSet(featureID);
	}

} //DeclarationImpl
