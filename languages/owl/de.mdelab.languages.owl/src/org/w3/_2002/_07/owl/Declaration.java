/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.Declaration#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Declaration#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Declaration#getObjectProperty <em>Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Declaration#getDataProperty <em>Data Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Declaration#getAnnotationProperty <em>Annotation Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Declaration#getNamedIndividual <em>Named Individual</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getDeclaration()
 * @model extendedMetaData="name='Declaration' kind='elementOnly'"
 * @generated
 */
public interface Declaration extends Axiom {
	/**
	 * Returns the value of the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' containment reference.
	 * @see #setClass(org.w3._2002._07.owl.Class)
	 * @see org.w3._2002._07.owl.OwlPackage#getDeclaration_Class()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Class' namespace='##targetNamespace'"
	 * @generated
	 */
	org.w3._2002._07.owl.Class getClass_();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Declaration#getClass_ <em>Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' containment reference.
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(org.w3._2002._07.owl.Class value);

	/**
	 * Returns the value of the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype</em>' containment reference.
	 * @see #setDatatype(Datatype)
	 * @see org.w3._2002._07.owl.OwlPackage#getDeclaration_Datatype()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Datatype' namespace='##targetNamespace'"
	 * @generated
	 */
	Datatype getDatatype();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Declaration#getDatatype <em>Datatype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype</em>' containment reference.
	 * @see #getDatatype()
	 * @generated
	 */
	void setDatatype(Datatype value);

	/**
	 * Returns the value of the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property</em>' containment reference.
	 * @see #setObjectProperty(ObjectProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDeclaration_ObjectProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectProperty getObjectProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Declaration#getObjectProperty <em>Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Property</em>' containment reference.
	 * @see #getObjectProperty()
	 * @generated
	 */
	void setObjectProperty(ObjectProperty value);

	/**
	 * Returns the value of the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property</em>' containment reference.
	 * @see #setDataProperty(DataProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDeclaration_DataProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	DataProperty getDataProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Declaration#getDataProperty <em>Data Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Property</em>' containment reference.
	 * @see #getDataProperty()
	 * @generated
	 */
	void setDataProperty(DataProperty value);

	/**
	 * Returns the value of the '<em><b>Annotation Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Property</em>' containment reference.
	 * @see #setAnnotationProperty(AnnotationProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDeclaration_AnnotationProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AnnotationProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	AnnotationProperty getAnnotationProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Declaration#getAnnotationProperty <em>Annotation Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Property</em>' containment reference.
	 * @see #getAnnotationProperty()
	 * @generated
	 */
	void setAnnotationProperty(AnnotationProperty value);

	/**
	 * Returns the value of the '<em><b>Named Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Individual</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Individual</em>' containment reference.
	 * @see #setNamedIndividual(NamedIndividual)
	 * @see org.w3._2002._07.owl.OwlPackage#getDeclaration_NamedIndividual()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='NamedIndividual' namespace='##targetNamespace'"
	 * @generated
	 */
	NamedIndividual getNamedIndividual();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Declaration#getNamedIndividual <em>Named Individual</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Named Individual</em>' containment reference.
	 * @see #getNamedIndividual()
	 * @generated
	 */
	void setNamedIndividual(NamedIndividual value);

} // Declaration
