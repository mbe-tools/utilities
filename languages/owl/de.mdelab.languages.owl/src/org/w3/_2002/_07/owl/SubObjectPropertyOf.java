/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Object Property Of</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.SubObjectPropertyOf#getObjectProperty <em>Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubObjectPropertyOf#getObjectInverseOf <em>Object Inverse Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubObjectPropertyOf#getObjectPropertyChain <em>Object Property Chain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubObjectPropertyOf#getObjectInverseOf1 <em>Object Inverse Of1</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getSubObjectPropertyOf()
 * @model extendedMetaData="name='SubObjectPropertyOf' kind='elementOnly'"
 * @generated
 */
public interface SubObjectPropertyOf extends ObjectPropertyAxiom {
	/**
	 * Returns the value of the '<em><b>Object Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getSubObjectPropertyOf_ObjectProperty()
	 * @model containment="true" lower="2" upper="2"
	 *        extendedMetaData="kind='element' name='ObjectProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ObjectProperty> getObjectProperty();

	/**
	 * Returns the value of the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Inverse Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Inverse Of</em>' containment reference.
	 * @see #setObjectInverseOf(ObjectInverseOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubObjectPropertyOf_ObjectInverseOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectInverseOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectInverseOf getObjectInverseOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubObjectPropertyOf#getObjectInverseOf <em>Object Inverse Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Inverse Of</em>' containment reference.
	 * @see #getObjectInverseOf()
	 * @generated
	 */
	void setObjectInverseOf(ObjectInverseOf value);

	/**
	 * Returns the value of the '<em><b>Object Property Chain</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property Chain</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property Chain</em>' containment reference.
	 * @see #setObjectPropertyChain(ObjectPropertyChain)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubObjectPropertyOf_ObjectPropertyChain()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectPropertyChain' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectPropertyChain getObjectPropertyChain();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubObjectPropertyOf#getObjectPropertyChain <em>Object Property Chain</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Property Chain</em>' containment reference.
	 * @see #getObjectPropertyChain()
	 * @generated
	 */
	void setObjectPropertyChain(ObjectPropertyChain value);

	/**
	 * Returns the value of the '<em><b>Object Inverse Of1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Inverse Of1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Inverse Of1</em>' containment reference.
	 * @see #setObjectInverseOf1(ObjectInverseOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubObjectPropertyOf_ObjectInverseOf1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectInverseOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectInverseOf getObjectInverseOf1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubObjectPropertyOf#getObjectInverseOf1 <em>Object Inverse Of1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Inverse Of1</em>' containment reference.
	 * @see #getObjectInverseOf1()
	 * @generated
	 */
	void setObjectInverseOf1(ObjectInverseOf value);

} // SubObjectPropertyOf
