/**
 */
package org.w3._2002._07.owl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.w3._2002._07.owl.DataProperty;
import org.w3._2002._07.owl.OwlPackage;
import org.w3._2002._07.owl.SubDataPropertyOf;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Data Property Of</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.SubDataPropertyOfImpl#getDataProperty <em>Data Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubDataPropertyOfImpl#getDataProperty1 <em>Data Property1</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubDataPropertyOfImpl extends DataPropertyAxiomImpl implements SubDataPropertyOf {
	/**
	 * The cached value of the '{@link #getDataProperty() <em>Data Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataProperty()
	 * @generated
	 * @ordered
	 */
	protected DataProperty dataProperty;

	/**
	 * The cached value of the '{@link #getDataProperty1() <em>Data Property1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataProperty1()
	 * @generated
	 * @ordered
	 */
	protected DataProperty dataProperty1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubDataPropertyOfImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getSubDataPropertyOf();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataProperty getDataProperty() {
		return dataProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataProperty(DataProperty newDataProperty, NotificationChain msgs) {
		DataProperty oldDataProperty = dataProperty;
		dataProperty = newDataProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY, oldDataProperty, newDataProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataProperty(DataProperty newDataProperty) {
		if (newDataProperty != dataProperty) {
			NotificationChain msgs = null;
			if (dataProperty != null)
				msgs = ((InternalEObject)dataProperty).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY, null, msgs);
			if (newDataProperty != null)
				msgs = ((InternalEObject)newDataProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY, null, msgs);
			msgs = basicSetDataProperty(newDataProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY, newDataProperty, newDataProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataProperty getDataProperty1() {
		return dataProperty1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataProperty1(DataProperty newDataProperty1, NotificationChain msgs) {
		DataProperty oldDataProperty1 = dataProperty1;
		dataProperty1 = newDataProperty1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY1, oldDataProperty1, newDataProperty1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataProperty1(DataProperty newDataProperty1) {
		if (newDataProperty1 != dataProperty1) {
			NotificationChain msgs = null;
			if (dataProperty1 != null)
				msgs = ((InternalEObject)dataProperty1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY1, null, msgs);
			if (newDataProperty1 != null)
				msgs = ((InternalEObject)newDataProperty1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY1, null, msgs);
			msgs = basicSetDataProperty1(newDataProperty1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY1, newDataProperty1, newDataProperty1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY:
				return basicSetDataProperty(null, msgs);
			case OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY1:
				return basicSetDataProperty1(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY:
				return getDataProperty();
			case OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY1:
				return getDataProperty1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY:
				setDataProperty((DataProperty)newValue);
				return;
			case OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY1:
				setDataProperty1((DataProperty)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY:
				setDataProperty((DataProperty)null);
				return;
			case OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY1:
				setDataProperty1((DataProperty)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY:
				return dataProperty != null;
			case OwlPackage.SUB_DATA_PROPERTY_OF__DATA_PROPERTY1:
				return dataProperty1 != null;
		}
		return super.eIsSet(featureID);
	}

} //SubDataPropertyOfImpl
