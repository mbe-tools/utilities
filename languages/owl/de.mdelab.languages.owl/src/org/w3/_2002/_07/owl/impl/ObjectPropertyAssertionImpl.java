/**
 */
package org.w3._2002._07.owl.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.w3._2002._07.owl.AnonymousIndividual;
import org.w3._2002._07.owl.NamedIndividual;
import org.w3._2002._07.owl.ObjectInverseOf;
import org.w3._2002._07.owl.ObjectProperty;
import org.w3._2002._07.owl.ObjectPropertyAssertion;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object Property Assertion</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.ObjectPropertyAssertionImpl#getObjectProperty <em>Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.ObjectPropertyAssertionImpl#getObjectInverseOf <em>Object Inverse Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.ObjectPropertyAssertionImpl#getNamedIndividual <em>Named Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.ObjectPropertyAssertionImpl#getAnonymousIndividual <em>Anonymous Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.ObjectPropertyAssertionImpl#getAnonymousIndividual1 <em>Anonymous Individual1</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObjectPropertyAssertionImpl extends AssertionImpl implements ObjectPropertyAssertion {
	/**
	 * The cached value of the '{@link #getObjectProperty() <em>Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectProperty()
	 * @generated
	 * @ordered
	 */
	protected ObjectProperty objectProperty;

	/**
	 * The cached value of the '{@link #getObjectInverseOf() <em>Object Inverse Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectInverseOf()
	 * @generated
	 * @ordered
	 */
	protected ObjectInverseOf objectInverseOf;

	/**
	 * The cached value of the '{@link #getNamedIndividual() <em>Named Individual</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamedIndividual()
	 * @generated
	 * @ordered
	 */
	protected EList<NamedIndividual> namedIndividual;

	/**
	 * The cached value of the '{@link #getAnonymousIndividual() <em>Anonymous Individual</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousIndividual()
	 * @generated
	 * @ordered
	 */
	protected AnonymousIndividual anonymousIndividual;

	/**
	 * The cached value of the '{@link #getAnonymousIndividual1() <em>Anonymous Individual1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousIndividual1()
	 * @generated
	 * @ordered
	 */
	protected AnonymousIndividual anonymousIndividual1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObjectPropertyAssertionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getObjectPropertyAssertion();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectProperty getObjectProperty() {
		return objectProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectProperty(ObjectProperty newObjectProperty, NotificationChain msgs) {
		ObjectProperty oldObjectProperty = objectProperty;
		objectProperty = newObjectProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY, oldObjectProperty, newObjectProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectProperty(ObjectProperty newObjectProperty) {
		if (newObjectProperty != objectProperty) {
			NotificationChain msgs = null;
			if (objectProperty != null)
				msgs = ((InternalEObject)objectProperty).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY, null, msgs);
			if (newObjectProperty != null)
				msgs = ((InternalEObject)newObjectProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY, null, msgs);
			msgs = basicSetObjectProperty(newObjectProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY, newObjectProperty, newObjectProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectInverseOf getObjectInverseOf() {
		return objectInverseOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectInverseOf(ObjectInverseOf newObjectInverseOf, NotificationChain msgs) {
		ObjectInverseOf oldObjectInverseOf = objectInverseOf;
		objectInverseOf = newObjectInverseOf;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF, oldObjectInverseOf, newObjectInverseOf);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectInverseOf(ObjectInverseOf newObjectInverseOf) {
		if (newObjectInverseOf != objectInverseOf) {
			NotificationChain msgs = null;
			if (objectInverseOf != null)
				msgs = ((InternalEObject)objectInverseOf).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF, null, msgs);
			if (newObjectInverseOf != null)
				msgs = ((InternalEObject)newObjectInverseOf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF, null, msgs);
			msgs = basicSetObjectInverseOf(newObjectInverseOf, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF, newObjectInverseOf, newObjectInverseOf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NamedIndividual> getNamedIndividual() {
		if (namedIndividual == null) {
			namedIndividual = new EObjectContainmentEList<NamedIndividual>(NamedIndividual.class, this, OwlPackage.OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL);
		}
		return namedIndividual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousIndividual getAnonymousIndividual() {
		return anonymousIndividual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousIndividual(AnonymousIndividual newAnonymousIndividual, NotificationChain msgs) {
		AnonymousIndividual oldAnonymousIndividual = anonymousIndividual;
		anonymousIndividual = newAnonymousIndividual;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL, oldAnonymousIndividual, newAnonymousIndividual);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousIndividual(AnonymousIndividual newAnonymousIndividual) {
		if (newAnonymousIndividual != anonymousIndividual) {
			NotificationChain msgs = null;
			if (anonymousIndividual != null)
				msgs = ((InternalEObject)anonymousIndividual).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL, null, msgs);
			if (newAnonymousIndividual != null)
				msgs = ((InternalEObject)newAnonymousIndividual).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL, null, msgs);
			msgs = basicSetAnonymousIndividual(newAnonymousIndividual, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL, newAnonymousIndividual, newAnonymousIndividual));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousIndividual getAnonymousIndividual1() {
		return anonymousIndividual1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousIndividual1(AnonymousIndividual newAnonymousIndividual1, NotificationChain msgs) {
		AnonymousIndividual oldAnonymousIndividual1 = anonymousIndividual1;
		anonymousIndividual1 = newAnonymousIndividual1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1, oldAnonymousIndividual1, newAnonymousIndividual1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousIndividual1(AnonymousIndividual newAnonymousIndividual1) {
		if (newAnonymousIndividual1 != anonymousIndividual1) {
			NotificationChain msgs = null;
			if (anonymousIndividual1 != null)
				msgs = ((InternalEObject)anonymousIndividual1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1, null, msgs);
			if (newAnonymousIndividual1 != null)
				msgs = ((InternalEObject)newAnonymousIndividual1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1, null, msgs);
			msgs = basicSetAnonymousIndividual1(newAnonymousIndividual1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1, newAnonymousIndividual1, newAnonymousIndividual1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY:
				return basicSetObjectProperty(null, msgs);
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF:
				return basicSetObjectInverseOf(null, msgs);
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL:
				return ((InternalEList<?>)getNamedIndividual()).basicRemove(otherEnd, msgs);
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL:
				return basicSetAnonymousIndividual(null, msgs);
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				return basicSetAnonymousIndividual1(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY:
				return getObjectProperty();
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF:
				return getObjectInverseOf();
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL:
				return getNamedIndividual();
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL:
				return getAnonymousIndividual();
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				return getAnonymousIndividual1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY:
				setObjectProperty((ObjectProperty)newValue);
				return;
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF:
				setObjectInverseOf((ObjectInverseOf)newValue);
				return;
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL:
				getNamedIndividual().clear();
				getNamedIndividual().addAll((Collection<? extends NamedIndividual>)newValue);
				return;
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL:
				setAnonymousIndividual((AnonymousIndividual)newValue);
				return;
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				setAnonymousIndividual1((AnonymousIndividual)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY:
				setObjectProperty((ObjectProperty)null);
				return;
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF:
				setObjectInverseOf((ObjectInverseOf)null);
				return;
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL:
				getNamedIndividual().clear();
				return;
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL:
				setAnonymousIndividual((AnonymousIndividual)null);
				return;
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				setAnonymousIndividual1((AnonymousIndividual)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY:
				return objectProperty != null;
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF:
				return objectInverseOf != null;
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL:
				return namedIndividual != null && !namedIndividual.isEmpty();
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL:
				return anonymousIndividual != null;
			case OwlPackage.OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				return anonymousIndividual1 != null;
		}
		return super.eIsSet(featureID);
	}

} //ObjectPropertyAssertionImpl
