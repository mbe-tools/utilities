/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Class Of</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectIntersectionOf <em>Object Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectUnionOf <em>Object Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectComplementOf <em>Object Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectOneOf <em>Object One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectSomeValuesFrom <em>Object Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectAllValuesFrom <em>Object All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectHasValue <em>Object Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectHasSelf <em>Object Has Self</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectMinCardinality <em>Object Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectMaxCardinality <em>Object Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectExactCardinality <em>Object Exact Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getDataSomeValuesFrom <em>Data Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getDataAllValuesFrom <em>Data All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getDataHasValue <em>Data Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getDataMinCardinality <em>Data Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getDataMaxCardinality <em>Data Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getDataExactCardinality <em>Data Exact Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectIntersectionOf1 <em>Object Intersection Of1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectUnionOf1 <em>Object Union Of1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectComplementOf1 <em>Object Complement Of1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectOneOf1 <em>Object One Of1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectSomeValuesFrom1 <em>Object Some Values From1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectAllValuesFrom1 <em>Object All Values From1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectHasValue1 <em>Object Has Value1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectHasSelf1 <em>Object Has Self1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectMinCardinality1 <em>Object Min Cardinality1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectMaxCardinality1 <em>Object Max Cardinality1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getObjectExactCardinality1 <em>Object Exact Cardinality1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getDataSomeValuesFrom1 <em>Data Some Values From1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getDataAllValuesFrom1 <em>Data All Values From1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getDataHasValue1 <em>Data Has Value1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getDataMinCardinality1 <em>Data Min Cardinality1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getDataMaxCardinality1 <em>Data Max Cardinality1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubClassOf#getDataExactCardinality1 <em>Data Exact Cardinality1</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf()
 * @model extendedMetaData="name='SubClassOf' kind='elementOnly'"
 * @generated
 */
public interface SubClassOf extends ClassAxiom {
	/**
	 * Returns the value of the '<em><b>Class</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_Class()
	 * @model containment="true" required="true" upper="2"
	 *        extendedMetaData="kind='element' name='Class' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<org.w3._2002._07.owl.Class> getClass_();

	/**
	 * Returns the value of the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Intersection Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Intersection Of</em>' containment reference.
	 * @see #setObjectIntersectionOf(ObjectIntersectionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectIntersectionOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectIntersectionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectIntersectionOf getObjectIntersectionOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectIntersectionOf <em>Object Intersection Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Intersection Of</em>' containment reference.
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	void setObjectIntersectionOf(ObjectIntersectionOf value);

	/**
	 * Returns the value of the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Union Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Union Of</em>' containment reference.
	 * @see #setObjectUnionOf(ObjectUnionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectUnionOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectUnionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectUnionOf getObjectUnionOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectUnionOf <em>Object Union Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Union Of</em>' containment reference.
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	void setObjectUnionOf(ObjectUnionOf value);

	/**
	 * Returns the value of the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Complement Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Complement Of</em>' containment reference.
	 * @see #setObjectComplementOf(ObjectComplementOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectComplementOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectComplementOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectComplementOf getObjectComplementOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectComplementOf <em>Object Complement Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Complement Of</em>' containment reference.
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	void setObjectComplementOf(ObjectComplementOf value);

	/**
	 * Returns the value of the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object One Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object One Of</em>' containment reference.
	 * @see #setObjectOneOf(ObjectOneOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectOneOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectOneOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectOneOf getObjectOneOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectOneOf <em>Object One Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object One Of</em>' containment reference.
	 * @see #getObjectOneOf()
	 * @generated
	 */
	void setObjectOneOf(ObjectOneOf value);

	/**
	 * Returns the value of the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Some Values From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Some Values From</em>' containment reference.
	 * @see #setObjectSomeValuesFrom(ObjectSomeValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectSomeValuesFrom()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectSomeValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectSomeValuesFrom getObjectSomeValuesFrom();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectSomeValuesFrom <em>Object Some Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Some Values From</em>' containment reference.
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	void setObjectSomeValuesFrom(ObjectSomeValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object All Values From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object All Values From</em>' containment reference.
	 * @see #setObjectAllValuesFrom(ObjectAllValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectAllValuesFrom()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectAllValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectAllValuesFrom getObjectAllValuesFrom();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectAllValuesFrom <em>Object All Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object All Values From</em>' containment reference.
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	void setObjectAllValuesFrom(ObjectAllValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Has Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Has Value</em>' containment reference.
	 * @see #setObjectHasValue(ObjectHasValue)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectHasValue()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectHasValue' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectHasValue getObjectHasValue();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectHasValue <em>Object Has Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Has Value</em>' containment reference.
	 * @see #getObjectHasValue()
	 * @generated
	 */
	void setObjectHasValue(ObjectHasValue value);

	/**
	 * Returns the value of the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Has Self</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Has Self</em>' containment reference.
	 * @see #setObjectHasSelf(ObjectHasSelf)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectHasSelf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectHasSelf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectHasSelf getObjectHasSelf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectHasSelf <em>Object Has Self</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Has Self</em>' containment reference.
	 * @see #getObjectHasSelf()
	 * @generated
	 */
	void setObjectHasSelf(ObjectHasSelf value);

	/**
	 * Returns the value of the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Min Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Min Cardinality</em>' containment reference.
	 * @see #setObjectMinCardinality(ObjectMinCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectMinCardinality()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectMinCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectMinCardinality getObjectMinCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectMinCardinality <em>Object Min Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Min Cardinality</em>' containment reference.
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	void setObjectMinCardinality(ObjectMinCardinality value);

	/**
	 * Returns the value of the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Max Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Max Cardinality</em>' containment reference.
	 * @see #setObjectMaxCardinality(ObjectMaxCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectMaxCardinality()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectMaxCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectMaxCardinality getObjectMaxCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectMaxCardinality <em>Object Max Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Max Cardinality</em>' containment reference.
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	void setObjectMaxCardinality(ObjectMaxCardinality value);

	/**
	 * Returns the value of the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Exact Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Exact Cardinality</em>' containment reference.
	 * @see #setObjectExactCardinality(ObjectExactCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectExactCardinality()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectExactCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectExactCardinality getObjectExactCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectExactCardinality <em>Object Exact Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Exact Cardinality</em>' containment reference.
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	void setObjectExactCardinality(ObjectExactCardinality value);

	/**
	 * Returns the value of the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Some Values From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Some Values From</em>' containment reference.
	 * @see #setDataSomeValuesFrom(DataSomeValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_DataSomeValuesFrom()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataSomeValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	DataSomeValuesFrom getDataSomeValuesFrom();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getDataSomeValuesFrom <em>Data Some Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Some Values From</em>' containment reference.
	 * @see #getDataSomeValuesFrom()
	 * @generated
	 */
	void setDataSomeValuesFrom(DataSomeValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data All Values From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data All Values From</em>' containment reference.
	 * @see #setDataAllValuesFrom(DataAllValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_DataAllValuesFrom()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataAllValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	DataAllValuesFrom getDataAllValuesFrom();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getDataAllValuesFrom <em>Data All Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data All Values From</em>' containment reference.
	 * @see #getDataAllValuesFrom()
	 * @generated
	 */
	void setDataAllValuesFrom(DataAllValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Has Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Has Value</em>' containment reference.
	 * @see #setDataHasValue(DataHasValue)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_DataHasValue()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataHasValue' namespace='##targetNamespace'"
	 * @generated
	 */
	DataHasValue getDataHasValue();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getDataHasValue <em>Data Has Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Has Value</em>' containment reference.
	 * @see #getDataHasValue()
	 * @generated
	 */
	void setDataHasValue(DataHasValue value);

	/**
	 * Returns the value of the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Min Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Min Cardinality</em>' containment reference.
	 * @see #setDataMinCardinality(DataMinCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_DataMinCardinality()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataMinCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	DataMinCardinality getDataMinCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getDataMinCardinality <em>Data Min Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Min Cardinality</em>' containment reference.
	 * @see #getDataMinCardinality()
	 * @generated
	 */
	void setDataMinCardinality(DataMinCardinality value);

	/**
	 * Returns the value of the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Max Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Max Cardinality</em>' containment reference.
	 * @see #setDataMaxCardinality(DataMaxCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_DataMaxCardinality()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataMaxCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	DataMaxCardinality getDataMaxCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getDataMaxCardinality <em>Data Max Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Max Cardinality</em>' containment reference.
	 * @see #getDataMaxCardinality()
	 * @generated
	 */
	void setDataMaxCardinality(DataMaxCardinality value);

	/**
	 * Returns the value of the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Exact Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Exact Cardinality</em>' containment reference.
	 * @see #setDataExactCardinality(DataExactCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_DataExactCardinality()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataExactCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	DataExactCardinality getDataExactCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getDataExactCardinality <em>Data Exact Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Exact Cardinality</em>' containment reference.
	 * @see #getDataExactCardinality()
	 * @generated
	 */
	void setDataExactCardinality(DataExactCardinality value);

	/**
	 * Returns the value of the '<em><b>Object Intersection Of1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Intersection Of1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Intersection Of1</em>' containment reference.
	 * @see #setObjectIntersectionOf1(ObjectIntersectionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectIntersectionOf1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectIntersectionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectIntersectionOf getObjectIntersectionOf1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectIntersectionOf1 <em>Object Intersection Of1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Intersection Of1</em>' containment reference.
	 * @see #getObjectIntersectionOf1()
	 * @generated
	 */
	void setObjectIntersectionOf1(ObjectIntersectionOf value);

	/**
	 * Returns the value of the '<em><b>Object Union Of1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Union Of1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Union Of1</em>' containment reference.
	 * @see #setObjectUnionOf1(ObjectUnionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectUnionOf1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectUnionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectUnionOf getObjectUnionOf1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectUnionOf1 <em>Object Union Of1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Union Of1</em>' containment reference.
	 * @see #getObjectUnionOf1()
	 * @generated
	 */
	void setObjectUnionOf1(ObjectUnionOf value);

	/**
	 * Returns the value of the '<em><b>Object Complement Of1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Complement Of1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Complement Of1</em>' containment reference.
	 * @see #setObjectComplementOf1(ObjectComplementOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectComplementOf1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectComplementOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectComplementOf getObjectComplementOf1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectComplementOf1 <em>Object Complement Of1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Complement Of1</em>' containment reference.
	 * @see #getObjectComplementOf1()
	 * @generated
	 */
	void setObjectComplementOf1(ObjectComplementOf value);

	/**
	 * Returns the value of the '<em><b>Object One Of1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object One Of1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object One Of1</em>' containment reference.
	 * @see #setObjectOneOf1(ObjectOneOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectOneOf1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectOneOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectOneOf getObjectOneOf1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectOneOf1 <em>Object One Of1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object One Of1</em>' containment reference.
	 * @see #getObjectOneOf1()
	 * @generated
	 */
	void setObjectOneOf1(ObjectOneOf value);

	/**
	 * Returns the value of the '<em><b>Object Some Values From1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Some Values From1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Some Values From1</em>' containment reference.
	 * @see #setObjectSomeValuesFrom1(ObjectSomeValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectSomeValuesFrom1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectSomeValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectSomeValuesFrom getObjectSomeValuesFrom1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectSomeValuesFrom1 <em>Object Some Values From1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Some Values From1</em>' containment reference.
	 * @see #getObjectSomeValuesFrom1()
	 * @generated
	 */
	void setObjectSomeValuesFrom1(ObjectSomeValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Object All Values From1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object All Values From1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object All Values From1</em>' containment reference.
	 * @see #setObjectAllValuesFrom1(ObjectAllValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectAllValuesFrom1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectAllValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectAllValuesFrom getObjectAllValuesFrom1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectAllValuesFrom1 <em>Object All Values From1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object All Values From1</em>' containment reference.
	 * @see #getObjectAllValuesFrom1()
	 * @generated
	 */
	void setObjectAllValuesFrom1(ObjectAllValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Object Has Value1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Has Value1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Has Value1</em>' containment reference.
	 * @see #setObjectHasValue1(ObjectHasValue)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectHasValue1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectHasValue' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectHasValue getObjectHasValue1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectHasValue1 <em>Object Has Value1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Has Value1</em>' containment reference.
	 * @see #getObjectHasValue1()
	 * @generated
	 */
	void setObjectHasValue1(ObjectHasValue value);

	/**
	 * Returns the value of the '<em><b>Object Has Self1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Has Self1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Has Self1</em>' containment reference.
	 * @see #setObjectHasSelf1(ObjectHasSelf)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectHasSelf1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectHasSelf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectHasSelf getObjectHasSelf1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectHasSelf1 <em>Object Has Self1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Has Self1</em>' containment reference.
	 * @see #getObjectHasSelf1()
	 * @generated
	 */
	void setObjectHasSelf1(ObjectHasSelf value);

	/**
	 * Returns the value of the '<em><b>Object Min Cardinality1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Min Cardinality1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Min Cardinality1</em>' containment reference.
	 * @see #setObjectMinCardinality1(ObjectMinCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectMinCardinality1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectMinCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectMinCardinality getObjectMinCardinality1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectMinCardinality1 <em>Object Min Cardinality1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Min Cardinality1</em>' containment reference.
	 * @see #getObjectMinCardinality1()
	 * @generated
	 */
	void setObjectMinCardinality1(ObjectMinCardinality value);

	/**
	 * Returns the value of the '<em><b>Object Max Cardinality1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Max Cardinality1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Max Cardinality1</em>' containment reference.
	 * @see #setObjectMaxCardinality1(ObjectMaxCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectMaxCardinality1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectMaxCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectMaxCardinality getObjectMaxCardinality1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectMaxCardinality1 <em>Object Max Cardinality1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Max Cardinality1</em>' containment reference.
	 * @see #getObjectMaxCardinality1()
	 * @generated
	 */
	void setObjectMaxCardinality1(ObjectMaxCardinality value);

	/**
	 * Returns the value of the '<em><b>Object Exact Cardinality1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Exact Cardinality1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Exact Cardinality1</em>' containment reference.
	 * @see #setObjectExactCardinality1(ObjectExactCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_ObjectExactCardinality1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectExactCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectExactCardinality getObjectExactCardinality1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getObjectExactCardinality1 <em>Object Exact Cardinality1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Exact Cardinality1</em>' containment reference.
	 * @see #getObjectExactCardinality1()
	 * @generated
	 */
	void setObjectExactCardinality1(ObjectExactCardinality value);

	/**
	 * Returns the value of the '<em><b>Data Some Values From1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Some Values From1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Some Values From1</em>' containment reference.
	 * @see #setDataSomeValuesFrom1(DataSomeValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_DataSomeValuesFrom1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataSomeValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	DataSomeValuesFrom getDataSomeValuesFrom1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getDataSomeValuesFrom1 <em>Data Some Values From1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Some Values From1</em>' containment reference.
	 * @see #getDataSomeValuesFrom1()
	 * @generated
	 */
	void setDataSomeValuesFrom1(DataSomeValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Data All Values From1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data All Values From1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data All Values From1</em>' containment reference.
	 * @see #setDataAllValuesFrom1(DataAllValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_DataAllValuesFrom1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataAllValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	DataAllValuesFrom getDataAllValuesFrom1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getDataAllValuesFrom1 <em>Data All Values From1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data All Values From1</em>' containment reference.
	 * @see #getDataAllValuesFrom1()
	 * @generated
	 */
	void setDataAllValuesFrom1(DataAllValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Data Has Value1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Has Value1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Has Value1</em>' containment reference.
	 * @see #setDataHasValue1(DataHasValue)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_DataHasValue1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataHasValue' namespace='##targetNamespace'"
	 * @generated
	 */
	DataHasValue getDataHasValue1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getDataHasValue1 <em>Data Has Value1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Has Value1</em>' containment reference.
	 * @see #getDataHasValue1()
	 * @generated
	 */
	void setDataHasValue1(DataHasValue value);

	/**
	 * Returns the value of the '<em><b>Data Min Cardinality1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Min Cardinality1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Min Cardinality1</em>' containment reference.
	 * @see #setDataMinCardinality1(DataMinCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_DataMinCardinality1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataMinCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	DataMinCardinality getDataMinCardinality1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getDataMinCardinality1 <em>Data Min Cardinality1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Min Cardinality1</em>' containment reference.
	 * @see #getDataMinCardinality1()
	 * @generated
	 */
	void setDataMinCardinality1(DataMinCardinality value);

	/**
	 * Returns the value of the '<em><b>Data Max Cardinality1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Max Cardinality1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Max Cardinality1</em>' containment reference.
	 * @see #setDataMaxCardinality1(DataMaxCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_DataMaxCardinality1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataMaxCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	DataMaxCardinality getDataMaxCardinality1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getDataMaxCardinality1 <em>Data Max Cardinality1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Max Cardinality1</em>' containment reference.
	 * @see #getDataMaxCardinality1()
	 * @generated
	 */
	void setDataMaxCardinality1(DataMaxCardinality value);

	/**
	 * Returns the value of the '<em><b>Data Exact Cardinality1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Exact Cardinality1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Exact Cardinality1</em>' containment reference.
	 * @see #setDataExactCardinality1(DataExactCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubClassOf_DataExactCardinality1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataExactCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	DataExactCardinality getDataExactCardinality1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubClassOf#getDataExactCardinality1 <em>Data Exact Cardinality1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Exact Cardinality1</em>' containment reference.
	 * @see #getDataExactCardinality1()
	 * @generated
	 */
	void setDataExactCardinality1(DataExactCardinality value);

} // SubClassOf
