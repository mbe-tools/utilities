/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Anonymous Individual</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.AnonymousIndividual#getNodeID <em>Node ID</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getAnonymousIndividual()
 * @model extendedMetaData="name='AnonymousIndividual' kind='empty'"
 * @generated
 */
public interface AnonymousIndividual extends Individual {
	/**
	 * Returns the value of the '<em><b>Node ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node ID</em>' attribute.
	 * @see #setNodeID(String)
	 * @see org.w3._2002._07.owl.OwlPackage#getAnonymousIndividual_NodeID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NCName" required="true"
	 *        extendedMetaData="kind='attribute' name='nodeID'"
	 * @generated
	 */
	String getNodeID();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.AnonymousIndividual#getNodeID <em>Node ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node ID</em>' attribute.
	 * @see #getNodeID()
	 * @generated
	 */
	void setNodeID(String value);

} // AnonymousIndividual
