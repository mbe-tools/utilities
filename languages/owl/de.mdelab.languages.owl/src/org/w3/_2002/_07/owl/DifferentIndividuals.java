/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Different Individuals</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.DifferentIndividuals#getIndividual <em>Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DifferentIndividuals#getNamedIndividual <em>Named Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DifferentIndividuals#getAnonymousIndividual <em>Anonymous Individual</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getDifferentIndividuals()
 * @model extendedMetaData="name='DifferentIndividuals' kind='elementOnly'"
 * @generated
 */
public interface DifferentIndividuals extends Assertion {
	/**
	 * Returns the value of the '<em><b>Individual</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Individual</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Individual</em>' attribute list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDifferentIndividuals_Individual()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='Individual:5'"
	 * @generated
	 */
	FeatureMap getIndividual();

	/**
	 * Returns the value of the '<em><b>Named Individual</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.NamedIndividual}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Individual</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Individual</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDifferentIndividuals_NamedIndividual()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='NamedIndividual' namespace='##targetNamespace' group='#Individual:5'"
	 * @generated
	 */
	EList<NamedIndividual> getNamedIndividual();

	/**
	 * Returns the value of the '<em><b>Anonymous Individual</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.AnonymousIndividual}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Individual</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Individual</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDifferentIndividuals_AnonymousIndividual()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AnonymousIndividual' namespace='##targetNamespace' group='#Individual:5'"
	 * @generated
	 */
	EList<AnonymousIndividual> getAnonymousIndividual();

} // DifferentIndividuals
