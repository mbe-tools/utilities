/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotation Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.w3._2002._07.owl.OwlPackage#getAnnotationAxiom()
 * @model abstract="true"
 *        extendedMetaData="name='AnnotationAxiom' kind='elementOnly'"
 * @generated
 */
public interface AnnotationAxiom extends Axiom {
} // AnnotationAxiom
