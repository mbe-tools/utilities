/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Complement Of</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.DataComplementOf#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataComplementOf#getDataIntersectionOf <em>Data Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataComplementOf#getDataUnionOf <em>Data Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataComplementOf#getDataComplementOf <em>Data Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataComplementOf#getDataOneOf <em>Data One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataComplementOf#getDatatypeRestriction <em>Datatype Restriction</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getDataComplementOf()
 * @model extendedMetaData="name='DataComplementOf' kind='elementOnly'"
 * @generated
 */
public interface DataComplementOf extends DataRange {
	/**
	 * Returns the value of the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype</em>' containment reference.
	 * @see #setDatatype(Datatype)
	 * @see org.w3._2002._07.owl.OwlPackage#getDataComplementOf_Datatype()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Datatype' namespace='##targetNamespace'"
	 * @generated
	 */
	Datatype getDatatype();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DataComplementOf#getDatatype <em>Datatype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype</em>' containment reference.
	 * @see #getDatatype()
	 * @generated
	 */
	void setDatatype(Datatype value);

	/**
	 * Returns the value of the '<em><b>Data Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Intersection Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Intersection Of</em>' containment reference.
	 * @see #setDataIntersectionOf(DataIntersectionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDataComplementOf_DataIntersectionOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataIntersectionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	DataIntersectionOf getDataIntersectionOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DataComplementOf#getDataIntersectionOf <em>Data Intersection Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Intersection Of</em>' containment reference.
	 * @see #getDataIntersectionOf()
	 * @generated
	 */
	void setDataIntersectionOf(DataIntersectionOf value);

	/**
	 * Returns the value of the '<em><b>Data Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Union Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Union Of</em>' containment reference.
	 * @see #setDataUnionOf(DataUnionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDataComplementOf_DataUnionOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataUnionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	DataUnionOf getDataUnionOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DataComplementOf#getDataUnionOf <em>Data Union Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Union Of</em>' containment reference.
	 * @see #getDataUnionOf()
	 * @generated
	 */
	void setDataUnionOf(DataUnionOf value);

	/**
	 * Returns the value of the '<em><b>Data Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Complement Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Complement Of</em>' containment reference.
	 * @see #setDataComplementOf(DataComplementOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDataComplementOf_DataComplementOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataComplementOf' namespace='##targetNamespace'"
	 * @generated
	 */
	DataComplementOf getDataComplementOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DataComplementOf#getDataComplementOf <em>Data Complement Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Complement Of</em>' containment reference.
	 * @see #getDataComplementOf()
	 * @generated
	 */
	void setDataComplementOf(DataComplementOf value);

	/**
	 * Returns the value of the '<em><b>Data One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data One Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data One Of</em>' containment reference.
	 * @see #setDataOneOf(DataOneOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDataComplementOf_DataOneOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataOneOf' namespace='##targetNamespace'"
	 * @generated
	 */
	DataOneOf getDataOneOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DataComplementOf#getDataOneOf <em>Data One Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data One Of</em>' containment reference.
	 * @see #getDataOneOf()
	 * @generated
	 */
	void setDataOneOf(DataOneOf value);

	/**
	 * Returns the value of the '<em><b>Datatype Restriction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype Restriction</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype Restriction</em>' containment reference.
	 * @see #setDatatypeRestriction(DatatypeRestriction)
	 * @see org.w3._2002._07.owl.OwlPackage#getDataComplementOf_DatatypeRestriction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DatatypeRestriction' namespace='##targetNamespace'"
	 * @generated
	 */
	DatatypeRestriction getDatatypeRestriction();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DataComplementOf#getDatatypeRestriction <em>Datatype Restriction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype Restriction</em>' containment reference.
	 * @see #getDatatypeRestriction()
	 * @generated
	 */
	void setDatatypeRestriction(DatatypeRestriction value);

} // DataComplementOf
