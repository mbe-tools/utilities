/**
 */
package org.w3._2002._07.owl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xml.namespace.SpaceType;

import org.w3._2002._07.owl.Annotation;
import org.w3._2002._07.owl.AnnotationAssertion;
import org.w3._2002._07.owl.AnnotationPropertyDomain;
import org.w3._2002._07.owl.AnnotationPropertyRange;
import org.w3._2002._07.owl.AsymmetricObjectProperty;
import org.w3._2002._07.owl.ClassAssertion;
import org.w3._2002._07.owl.DataPropertyAssertion;
import org.w3._2002._07.owl.DataPropertyDomain;
import org.w3._2002._07.owl.DataPropertyRange;
import org.w3._2002._07.owl.DatatypeDefinition;
import org.w3._2002._07.owl.Declaration;
import org.w3._2002._07.owl.DifferentIndividuals;
import org.w3._2002._07.owl.DisjointClasses;
import org.w3._2002._07.owl.DisjointDataProperties;
import org.w3._2002._07.owl.DisjointObjectProperties;
import org.w3._2002._07.owl.DisjointUnion;
import org.w3._2002._07.owl.EquivalentClasses;
import org.w3._2002._07.owl.EquivalentDataProperties;
import org.w3._2002._07.owl.EquivalentObjectProperties;
import org.w3._2002._07.owl.FunctionalDataProperty;
import org.w3._2002._07.owl.FunctionalObjectProperty;
import org.w3._2002._07.owl.HasKey;
import org.w3._2002._07.owl.Import;
import org.w3._2002._07.owl.InverseFunctionalObjectProperty;
import org.w3._2002._07.owl.InverseObjectProperties;
import org.w3._2002._07.owl.IrreflexiveObjectProperty;
import org.w3._2002._07.owl.NegativeDataPropertyAssertion;
import org.w3._2002._07.owl.NegativeObjectPropertyAssertion;
import org.w3._2002._07.owl.ObjectPropertyAssertion;
import org.w3._2002._07.owl.ObjectPropertyDomain;
import org.w3._2002._07.owl.ObjectPropertyRange;
import org.w3._2002._07.owl.Ontology;
import org.w3._2002._07.owl.OwlPackage;
import org.w3._2002._07.owl.Prefix;
import org.w3._2002._07.owl.ReflexiveObjectProperty;
import org.w3._2002._07.owl.SameIndividual;
import org.w3._2002._07.owl.SubAnnotationPropertyOf;
import org.w3._2002._07.owl.SubClassOf;
import org.w3._2002._07.owl.SubDataPropertyOf;
import org.w3._2002._07.owl.SubObjectPropertyOf;
import org.w3._2002._07.owl.SymmetricObjectProperty;
import org.w3._2002._07.owl.TransitiveObjectProperty;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ontology</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getImport <em>Import</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getAnnotation <em>Annotation</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getAxiom <em>Axiom</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getDeclaration <em>Declaration</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getSubClassOf <em>Sub Class Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getEquivalentClasses <em>Equivalent Classes</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getDisjointClasses <em>Disjoint Classes</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getDisjointUnion <em>Disjoint Union</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getSubObjectPropertyOf <em>Sub Object Property Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getEquivalentObjectProperties <em>Equivalent Object Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getDisjointObjectProperties <em>Disjoint Object Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getInverseObjectProperties <em>Inverse Object Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getObjectPropertyDomain <em>Object Property Domain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getObjectPropertyRange <em>Object Property Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getFunctionalObjectProperty <em>Functional Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getInverseFunctionalObjectProperty <em>Inverse Functional Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getReflexiveObjectProperty <em>Reflexive Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getIrreflexiveObjectProperty <em>Irreflexive Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getSymmetricObjectProperty <em>Symmetric Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getAsymmetricObjectProperty <em>Asymmetric Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getTransitiveObjectProperty <em>Transitive Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getSubDataPropertyOf <em>Sub Data Property Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getEquivalentDataProperties <em>Equivalent Data Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getDisjointDataProperties <em>Disjoint Data Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getDataPropertyDomain <em>Data Property Domain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getDataPropertyRange <em>Data Property Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getFunctionalDataProperty <em>Functional Data Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getDatatypeDefinition <em>Datatype Definition</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getHasKey <em>Has Key</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getSameIndividual <em>Same Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getDifferentIndividuals <em>Different Individuals</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getClassAssertion <em>Class Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getObjectPropertyAssertion <em>Object Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getNegativeObjectPropertyAssertion <em>Negative Object Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getDataPropertyAssertion <em>Data Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getNegativeDataPropertyAssertion <em>Negative Data Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getAnnotationAssertion <em>Annotation Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getSubAnnotationPropertyOf <em>Sub Annotation Property Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getAnnotationPropertyDomain <em>Annotation Property Domain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getAnnotationPropertyRange <em>Annotation Property Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getBase <em>Base</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getLang <em>Lang</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getOntologyIRI <em>Ontology IRI</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getSpace <em>Space</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.OntologyImpl#getVersionIRI <em>Version IRI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OntologyImpl extends MinimalEObjectImpl.Container implements Ontology {
	/**
	 * The cached value of the '{@link #getPrefix() <em>Prefix</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefix()
	 * @generated
	 * @ordered
	 */
	protected EList<Prefix> prefix;

	/**
	 * The cached value of the '{@link #getImport() <em>Import</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImport()
	 * @generated
	 * @ordered
	 */
	protected EList<Import> import_;

	/**
	 * The cached value of the '{@link #getAnnotation() <em>Annotation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotation()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotation;

	/**
	 * The cached value of the '{@link #getAxiom() <em>Axiom</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxiom()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap axiom;

	/**
	 * The default value of the '{@link #getBase() <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected static final String BASE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBase() <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected String base = BASE_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getLang() <em>Lang</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLang()
	 * @generated
	 * @ordered
	 */
	protected static final String LANG_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLang() <em>Lang</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLang()
	 * @generated
	 * @ordered
	 */
	protected String lang = LANG_EDEFAULT;

	/**
	 * The default value of the '{@link #getOntologyIRI() <em>Ontology IRI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOntologyIRI()
	 * @generated
	 * @ordered
	 */
	protected static final String ONTOLOGY_IRI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOntologyIRI() <em>Ontology IRI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOntologyIRI()
	 * @generated
	 * @ordered
	 */
	protected String ontologyIRI = ONTOLOGY_IRI_EDEFAULT;

	/**
	 * The default value of the '{@link #getSpace() <em>Space</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpace()
	 * @generated
	 * @ordered
	 */
	protected static final SpaceType SPACE_EDEFAULT = SpaceType.DEFAULT_LITERAL;

	/**
	 * The cached value of the '{@link #getSpace() <em>Space</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpace()
	 * @generated
	 * @ordered
	 */
	protected SpaceType space = SPACE_EDEFAULT;

	/**
	 * This is true if the Space attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean spaceESet;

	/**
	 * The default value of the '{@link #getVersionIRI() <em>Version IRI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersionIRI()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_IRI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersionIRI() <em>Version IRI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersionIRI()
	 * @generated
	 * @ordered
	 */
	protected String versionIRI = VERSION_IRI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OntologyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getOntology();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Prefix> getPrefix() {
		if (prefix == null) {
			prefix = new EObjectContainmentEList<Prefix>(Prefix.class, this, OwlPackage.ONTOLOGY__PREFIX);
		}
		return prefix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Import> getImport() {
		if (import_ == null) {
			import_ = new EObjectContainmentEList<Import>(Import.class, this, OwlPackage.ONTOLOGY__IMPORT);
		}
		return import_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new EObjectContainmentEList<Annotation>(Annotation.class, this, OwlPackage.ONTOLOGY__ANNOTATION);
		}
		return annotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAxiom() {
		if (axiom == null) {
			axiom = new BasicFeatureMap(this, OwlPackage.ONTOLOGY__AXIOM);
		}
		return axiom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Declaration> getDeclaration() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_Declaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubClassOf> getSubClassOf() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_SubClassOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquivalentClasses> getEquivalentClasses() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_EquivalentClasses());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DisjointClasses> getDisjointClasses() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_DisjointClasses());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DisjointUnion> getDisjointUnion() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_DisjointUnion());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubObjectPropertyOf> getSubObjectPropertyOf() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_SubObjectPropertyOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquivalentObjectProperties> getEquivalentObjectProperties() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_EquivalentObjectProperties());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DisjointObjectProperties> getDisjointObjectProperties() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_DisjointObjectProperties());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InverseObjectProperties> getInverseObjectProperties() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_InverseObjectProperties());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectPropertyDomain> getObjectPropertyDomain() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_ObjectPropertyDomain());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectPropertyRange> getObjectPropertyRange() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_ObjectPropertyRange());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionalObjectProperty> getFunctionalObjectProperty() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_FunctionalObjectProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InverseFunctionalObjectProperty> getInverseFunctionalObjectProperty() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_InverseFunctionalObjectProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReflexiveObjectProperty> getReflexiveObjectProperty() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_ReflexiveObjectProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IrreflexiveObjectProperty> getIrreflexiveObjectProperty() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_IrreflexiveObjectProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SymmetricObjectProperty> getSymmetricObjectProperty() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_SymmetricObjectProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AsymmetricObjectProperty> getAsymmetricObjectProperty() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_AsymmetricObjectProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TransitiveObjectProperty> getTransitiveObjectProperty() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_TransitiveObjectProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubDataPropertyOf> getSubDataPropertyOf() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_SubDataPropertyOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquivalentDataProperties> getEquivalentDataProperties() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_EquivalentDataProperties());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DisjointDataProperties> getDisjointDataProperties() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_DisjointDataProperties());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataPropertyDomain> getDataPropertyDomain() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_DataPropertyDomain());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataPropertyRange> getDataPropertyRange() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_DataPropertyRange());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionalDataProperty> getFunctionalDataProperty() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_FunctionalDataProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DatatypeDefinition> getDatatypeDefinition() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_DatatypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HasKey> getHasKey() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_HasKey());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SameIndividual> getSameIndividual() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_SameIndividual());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DifferentIndividuals> getDifferentIndividuals() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_DifferentIndividuals());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassAssertion> getClassAssertion() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_ClassAssertion());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectPropertyAssertion> getObjectPropertyAssertion() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_ObjectPropertyAssertion());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NegativeObjectPropertyAssertion> getNegativeObjectPropertyAssertion() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_NegativeObjectPropertyAssertion());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataPropertyAssertion> getDataPropertyAssertion() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_DataPropertyAssertion());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NegativeDataPropertyAssertion> getNegativeDataPropertyAssertion() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_NegativeDataPropertyAssertion());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnnotationAssertion> getAnnotationAssertion() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_AnnotationAssertion());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubAnnotationPropertyOf> getSubAnnotationPropertyOf() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_SubAnnotationPropertyOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnnotationPropertyDomain> getAnnotationPropertyDomain() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_AnnotationPropertyDomain());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnnotationPropertyRange> getAnnotationPropertyRange() {
		return getAxiom().list(OwlPackage.eINSTANCE.getOntology_AnnotationPropertyRange());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBase() {
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase(String newBase) {
		String oldBase = base;
		base = newBase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ONTOLOGY__BASE, oldBase, base));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ONTOLOGY__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLang(String newLang) {
		String oldLang = lang;
		lang = newLang;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ONTOLOGY__LANG, oldLang, lang));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOntologyIRI() {
		return ontologyIRI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOntologyIRI(String newOntologyIRI) {
		String oldOntologyIRI = ontologyIRI;
		ontologyIRI = newOntologyIRI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ONTOLOGY__ONTOLOGY_IRI, oldOntologyIRI, ontologyIRI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpaceType getSpace() {
		return space;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpace(SpaceType newSpace) {
		SpaceType oldSpace = space;
		space = newSpace == null ? SPACE_EDEFAULT : newSpace;
		boolean oldSpaceESet = spaceESet;
		spaceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ONTOLOGY__SPACE, oldSpace, space, !oldSpaceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSpace() {
		SpaceType oldSpace = space;
		boolean oldSpaceESet = spaceESet;
		space = SPACE_EDEFAULT;
		spaceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OwlPackage.ONTOLOGY__SPACE, oldSpace, SPACE_EDEFAULT, oldSpaceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSpace() {
		return spaceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersionIRI() {
		return versionIRI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersionIRI(String newVersionIRI) {
		String oldVersionIRI = versionIRI;
		versionIRI = newVersionIRI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ONTOLOGY__VERSION_IRI, oldVersionIRI, versionIRI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.ONTOLOGY__PREFIX:
				return ((InternalEList<?>)getPrefix()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__IMPORT:
				return ((InternalEList<?>)getImport()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__ANNOTATION:
				return ((InternalEList<?>)getAnnotation()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__AXIOM:
				return ((InternalEList<?>)getAxiom()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__DECLARATION:
				return ((InternalEList<?>)getDeclaration()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__SUB_CLASS_OF:
				return ((InternalEList<?>)getSubClassOf()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__EQUIVALENT_CLASSES:
				return ((InternalEList<?>)getEquivalentClasses()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__DISJOINT_CLASSES:
				return ((InternalEList<?>)getDisjointClasses()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__DISJOINT_UNION:
				return ((InternalEList<?>)getDisjointUnion()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__SUB_OBJECT_PROPERTY_OF:
				return ((InternalEList<?>)getSubObjectPropertyOf()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__EQUIVALENT_OBJECT_PROPERTIES:
				return ((InternalEList<?>)getEquivalentObjectProperties()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__DISJOINT_OBJECT_PROPERTIES:
				return ((InternalEList<?>)getDisjointObjectProperties()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__INVERSE_OBJECT_PROPERTIES:
				return ((InternalEList<?>)getInverseObjectProperties()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_DOMAIN:
				return ((InternalEList<?>)getObjectPropertyDomain()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_RANGE:
				return ((InternalEList<?>)getObjectPropertyRange()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__FUNCTIONAL_OBJECT_PROPERTY:
				return ((InternalEList<?>)getFunctionalObjectProperty()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__INVERSE_FUNCTIONAL_OBJECT_PROPERTY:
				return ((InternalEList<?>)getInverseFunctionalObjectProperty()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__REFLEXIVE_OBJECT_PROPERTY:
				return ((InternalEList<?>)getReflexiveObjectProperty()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__IRREFLEXIVE_OBJECT_PROPERTY:
				return ((InternalEList<?>)getIrreflexiveObjectProperty()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__SYMMETRIC_OBJECT_PROPERTY:
				return ((InternalEList<?>)getSymmetricObjectProperty()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__ASYMMETRIC_OBJECT_PROPERTY:
				return ((InternalEList<?>)getAsymmetricObjectProperty()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__TRANSITIVE_OBJECT_PROPERTY:
				return ((InternalEList<?>)getTransitiveObjectProperty()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__SUB_DATA_PROPERTY_OF:
				return ((InternalEList<?>)getSubDataPropertyOf()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__EQUIVALENT_DATA_PROPERTIES:
				return ((InternalEList<?>)getEquivalentDataProperties()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__DISJOINT_DATA_PROPERTIES:
				return ((InternalEList<?>)getDisjointDataProperties()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_DOMAIN:
				return ((InternalEList<?>)getDataPropertyDomain()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_RANGE:
				return ((InternalEList<?>)getDataPropertyRange()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__FUNCTIONAL_DATA_PROPERTY:
				return ((InternalEList<?>)getFunctionalDataProperty()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__DATATYPE_DEFINITION:
				return ((InternalEList<?>)getDatatypeDefinition()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__HAS_KEY:
				return ((InternalEList<?>)getHasKey()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__SAME_INDIVIDUAL:
				return ((InternalEList<?>)getSameIndividual()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__DIFFERENT_INDIVIDUALS:
				return ((InternalEList<?>)getDifferentIndividuals()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__CLASS_ASSERTION:
				return ((InternalEList<?>)getClassAssertion()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_ASSERTION:
				return ((InternalEList<?>)getObjectPropertyAssertion()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__NEGATIVE_OBJECT_PROPERTY_ASSERTION:
				return ((InternalEList<?>)getNegativeObjectPropertyAssertion()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_ASSERTION:
				return ((InternalEList<?>)getDataPropertyAssertion()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__NEGATIVE_DATA_PROPERTY_ASSERTION:
				return ((InternalEList<?>)getNegativeDataPropertyAssertion()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__ANNOTATION_ASSERTION:
				return ((InternalEList<?>)getAnnotationAssertion()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__SUB_ANNOTATION_PROPERTY_OF:
				return ((InternalEList<?>)getSubAnnotationPropertyOf()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__ANNOTATION_PROPERTY_DOMAIN:
				return ((InternalEList<?>)getAnnotationPropertyDomain()).basicRemove(otherEnd, msgs);
			case OwlPackage.ONTOLOGY__ANNOTATION_PROPERTY_RANGE:
				return ((InternalEList<?>)getAnnotationPropertyRange()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.ONTOLOGY__PREFIX:
				return getPrefix();
			case OwlPackage.ONTOLOGY__IMPORT:
				return getImport();
			case OwlPackage.ONTOLOGY__ANNOTATION:
				return getAnnotation();
			case OwlPackage.ONTOLOGY__AXIOM:
				if (coreType) return getAxiom();
				return ((FeatureMap.Internal)getAxiom()).getWrapper();
			case OwlPackage.ONTOLOGY__DECLARATION:
				return getDeclaration();
			case OwlPackage.ONTOLOGY__SUB_CLASS_OF:
				return getSubClassOf();
			case OwlPackage.ONTOLOGY__EQUIVALENT_CLASSES:
				return getEquivalentClasses();
			case OwlPackage.ONTOLOGY__DISJOINT_CLASSES:
				return getDisjointClasses();
			case OwlPackage.ONTOLOGY__DISJOINT_UNION:
				return getDisjointUnion();
			case OwlPackage.ONTOLOGY__SUB_OBJECT_PROPERTY_OF:
				return getSubObjectPropertyOf();
			case OwlPackage.ONTOLOGY__EQUIVALENT_OBJECT_PROPERTIES:
				return getEquivalentObjectProperties();
			case OwlPackage.ONTOLOGY__DISJOINT_OBJECT_PROPERTIES:
				return getDisjointObjectProperties();
			case OwlPackage.ONTOLOGY__INVERSE_OBJECT_PROPERTIES:
				return getInverseObjectProperties();
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_DOMAIN:
				return getObjectPropertyDomain();
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_RANGE:
				return getObjectPropertyRange();
			case OwlPackage.ONTOLOGY__FUNCTIONAL_OBJECT_PROPERTY:
				return getFunctionalObjectProperty();
			case OwlPackage.ONTOLOGY__INVERSE_FUNCTIONAL_OBJECT_PROPERTY:
				return getInverseFunctionalObjectProperty();
			case OwlPackage.ONTOLOGY__REFLEXIVE_OBJECT_PROPERTY:
				return getReflexiveObjectProperty();
			case OwlPackage.ONTOLOGY__IRREFLEXIVE_OBJECT_PROPERTY:
				return getIrreflexiveObjectProperty();
			case OwlPackage.ONTOLOGY__SYMMETRIC_OBJECT_PROPERTY:
				return getSymmetricObjectProperty();
			case OwlPackage.ONTOLOGY__ASYMMETRIC_OBJECT_PROPERTY:
				return getAsymmetricObjectProperty();
			case OwlPackage.ONTOLOGY__TRANSITIVE_OBJECT_PROPERTY:
				return getTransitiveObjectProperty();
			case OwlPackage.ONTOLOGY__SUB_DATA_PROPERTY_OF:
				return getSubDataPropertyOf();
			case OwlPackage.ONTOLOGY__EQUIVALENT_DATA_PROPERTIES:
				return getEquivalentDataProperties();
			case OwlPackage.ONTOLOGY__DISJOINT_DATA_PROPERTIES:
				return getDisjointDataProperties();
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_DOMAIN:
				return getDataPropertyDomain();
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_RANGE:
				return getDataPropertyRange();
			case OwlPackage.ONTOLOGY__FUNCTIONAL_DATA_PROPERTY:
				return getFunctionalDataProperty();
			case OwlPackage.ONTOLOGY__DATATYPE_DEFINITION:
				return getDatatypeDefinition();
			case OwlPackage.ONTOLOGY__HAS_KEY:
				return getHasKey();
			case OwlPackage.ONTOLOGY__SAME_INDIVIDUAL:
				return getSameIndividual();
			case OwlPackage.ONTOLOGY__DIFFERENT_INDIVIDUALS:
				return getDifferentIndividuals();
			case OwlPackage.ONTOLOGY__CLASS_ASSERTION:
				return getClassAssertion();
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_ASSERTION:
				return getObjectPropertyAssertion();
			case OwlPackage.ONTOLOGY__NEGATIVE_OBJECT_PROPERTY_ASSERTION:
				return getNegativeObjectPropertyAssertion();
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_ASSERTION:
				return getDataPropertyAssertion();
			case OwlPackage.ONTOLOGY__NEGATIVE_DATA_PROPERTY_ASSERTION:
				return getNegativeDataPropertyAssertion();
			case OwlPackage.ONTOLOGY__ANNOTATION_ASSERTION:
				return getAnnotationAssertion();
			case OwlPackage.ONTOLOGY__SUB_ANNOTATION_PROPERTY_OF:
				return getSubAnnotationPropertyOf();
			case OwlPackage.ONTOLOGY__ANNOTATION_PROPERTY_DOMAIN:
				return getAnnotationPropertyDomain();
			case OwlPackage.ONTOLOGY__ANNOTATION_PROPERTY_RANGE:
				return getAnnotationPropertyRange();
			case OwlPackage.ONTOLOGY__BASE:
				return getBase();
			case OwlPackage.ONTOLOGY__ID:
				return getId();
			case OwlPackage.ONTOLOGY__LANG:
				return getLang();
			case OwlPackage.ONTOLOGY__ONTOLOGY_IRI:
				return getOntologyIRI();
			case OwlPackage.ONTOLOGY__SPACE:
				return getSpace();
			case OwlPackage.ONTOLOGY__VERSION_IRI:
				return getVersionIRI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.ONTOLOGY__PREFIX:
				getPrefix().clear();
				getPrefix().addAll((Collection<? extends Prefix>)newValue);
				return;
			case OwlPackage.ONTOLOGY__IMPORT:
				getImport().clear();
				getImport().addAll((Collection<? extends Import>)newValue);
				return;
			case OwlPackage.ONTOLOGY__ANNOTATION:
				getAnnotation().clear();
				getAnnotation().addAll((Collection<? extends Annotation>)newValue);
				return;
			case OwlPackage.ONTOLOGY__AXIOM:
				((FeatureMap.Internal)getAxiom()).set(newValue);
				return;
			case OwlPackage.ONTOLOGY__DECLARATION:
				getDeclaration().clear();
				getDeclaration().addAll((Collection<? extends Declaration>)newValue);
				return;
			case OwlPackage.ONTOLOGY__SUB_CLASS_OF:
				getSubClassOf().clear();
				getSubClassOf().addAll((Collection<? extends SubClassOf>)newValue);
				return;
			case OwlPackage.ONTOLOGY__EQUIVALENT_CLASSES:
				getEquivalentClasses().clear();
				getEquivalentClasses().addAll((Collection<? extends EquivalentClasses>)newValue);
				return;
			case OwlPackage.ONTOLOGY__DISJOINT_CLASSES:
				getDisjointClasses().clear();
				getDisjointClasses().addAll((Collection<? extends DisjointClasses>)newValue);
				return;
			case OwlPackage.ONTOLOGY__DISJOINT_UNION:
				getDisjointUnion().clear();
				getDisjointUnion().addAll((Collection<? extends DisjointUnion>)newValue);
				return;
			case OwlPackage.ONTOLOGY__SUB_OBJECT_PROPERTY_OF:
				getSubObjectPropertyOf().clear();
				getSubObjectPropertyOf().addAll((Collection<? extends SubObjectPropertyOf>)newValue);
				return;
			case OwlPackage.ONTOLOGY__EQUIVALENT_OBJECT_PROPERTIES:
				getEquivalentObjectProperties().clear();
				getEquivalentObjectProperties().addAll((Collection<? extends EquivalentObjectProperties>)newValue);
				return;
			case OwlPackage.ONTOLOGY__DISJOINT_OBJECT_PROPERTIES:
				getDisjointObjectProperties().clear();
				getDisjointObjectProperties().addAll((Collection<? extends DisjointObjectProperties>)newValue);
				return;
			case OwlPackage.ONTOLOGY__INVERSE_OBJECT_PROPERTIES:
				getInverseObjectProperties().clear();
				getInverseObjectProperties().addAll((Collection<? extends InverseObjectProperties>)newValue);
				return;
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_DOMAIN:
				getObjectPropertyDomain().clear();
				getObjectPropertyDomain().addAll((Collection<? extends ObjectPropertyDomain>)newValue);
				return;
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_RANGE:
				getObjectPropertyRange().clear();
				getObjectPropertyRange().addAll((Collection<? extends ObjectPropertyRange>)newValue);
				return;
			case OwlPackage.ONTOLOGY__FUNCTIONAL_OBJECT_PROPERTY:
				getFunctionalObjectProperty().clear();
				getFunctionalObjectProperty().addAll((Collection<? extends FunctionalObjectProperty>)newValue);
				return;
			case OwlPackage.ONTOLOGY__INVERSE_FUNCTIONAL_OBJECT_PROPERTY:
				getInverseFunctionalObjectProperty().clear();
				getInverseFunctionalObjectProperty().addAll((Collection<? extends InverseFunctionalObjectProperty>)newValue);
				return;
			case OwlPackage.ONTOLOGY__REFLEXIVE_OBJECT_PROPERTY:
				getReflexiveObjectProperty().clear();
				getReflexiveObjectProperty().addAll((Collection<? extends ReflexiveObjectProperty>)newValue);
				return;
			case OwlPackage.ONTOLOGY__IRREFLEXIVE_OBJECT_PROPERTY:
				getIrreflexiveObjectProperty().clear();
				getIrreflexiveObjectProperty().addAll((Collection<? extends IrreflexiveObjectProperty>)newValue);
				return;
			case OwlPackage.ONTOLOGY__SYMMETRIC_OBJECT_PROPERTY:
				getSymmetricObjectProperty().clear();
				getSymmetricObjectProperty().addAll((Collection<? extends SymmetricObjectProperty>)newValue);
				return;
			case OwlPackage.ONTOLOGY__ASYMMETRIC_OBJECT_PROPERTY:
				getAsymmetricObjectProperty().clear();
				getAsymmetricObjectProperty().addAll((Collection<? extends AsymmetricObjectProperty>)newValue);
				return;
			case OwlPackage.ONTOLOGY__TRANSITIVE_OBJECT_PROPERTY:
				getTransitiveObjectProperty().clear();
				getTransitiveObjectProperty().addAll((Collection<? extends TransitiveObjectProperty>)newValue);
				return;
			case OwlPackage.ONTOLOGY__SUB_DATA_PROPERTY_OF:
				getSubDataPropertyOf().clear();
				getSubDataPropertyOf().addAll((Collection<? extends SubDataPropertyOf>)newValue);
				return;
			case OwlPackage.ONTOLOGY__EQUIVALENT_DATA_PROPERTIES:
				getEquivalentDataProperties().clear();
				getEquivalentDataProperties().addAll((Collection<? extends EquivalentDataProperties>)newValue);
				return;
			case OwlPackage.ONTOLOGY__DISJOINT_DATA_PROPERTIES:
				getDisjointDataProperties().clear();
				getDisjointDataProperties().addAll((Collection<? extends DisjointDataProperties>)newValue);
				return;
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_DOMAIN:
				getDataPropertyDomain().clear();
				getDataPropertyDomain().addAll((Collection<? extends DataPropertyDomain>)newValue);
				return;
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_RANGE:
				getDataPropertyRange().clear();
				getDataPropertyRange().addAll((Collection<? extends DataPropertyRange>)newValue);
				return;
			case OwlPackage.ONTOLOGY__FUNCTIONAL_DATA_PROPERTY:
				getFunctionalDataProperty().clear();
				getFunctionalDataProperty().addAll((Collection<? extends FunctionalDataProperty>)newValue);
				return;
			case OwlPackage.ONTOLOGY__DATATYPE_DEFINITION:
				getDatatypeDefinition().clear();
				getDatatypeDefinition().addAll((Collection<? extends DatatypeDefinition>)newValue);
				return;
			case OwlPackage.ONTOLOGY__HAS_KEY:
				getHasKey().clear();
				getHasKey().addAll((Collection<? extends HasKey>)newValue);
				return;
			case OwlPackage.ONTOLOGY__SAME_INDIVIDUAL:
				getSameIndividual().clear();
				getSameIndividual().addAll((Collection<? extends SameIndividual>)newValue);
				return;
			case OwlPackage.ONTOLOGY__DIFFERENT_INDIVIDUALS:
				getDifferentIndividuals().clear();
				getDifferentIndividuals().addAll((Collection<? extends DifferentIndividuals>)newValue);
				return;
			case OwlPackage.ONTOLOGY__CLASS_ASSERTION:
				getClassAssertion().clear();
				getClassAssertion().addAll((Collection<? extends ClassAssertion>)newValue);
				return;
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_ASSERTION:
				getObjectPropertyAssertion().clear();
				getObjectPropertyAssertion().addAll((Collection<? extends ObjectPropertyAssertion>)newValue);
				return;
			case OwlPackage.ONTOLOGY__NEGATIVE_OBJECT_PROPERTY_ASSERTION:
				getNegativeObjectPropertyAssertion().clear();
				getNegativeObjectPropertyAssertion().addAll((Collection<? extends NegativeObjectPropertyAssertion>)newValue);
				return;
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_ASSERTION:
				getDataPropertyAssertion().clear();
				getDataPropertyAssertion().addAll((Collection<? extends DataPropertyAssertion>)newValue);
				return;
			case OwlPackage.ONTOLOGY__NEGATIVE_DATA_PROPERTY_ASSERTION:
				getNegativeDataPropertyAssertion().clear();
				getNegativeDataPropertyAssertion().addAll((Collection<? extends NegativeDataPropertyAssertion>)newValue);
				return;
			case OwlPackage.ONTOLOGY__ANNOTATION_ASSERTION:
				getAnnotationAssertion().clear();
				getAnnotationAssertion().addAll((Collection<? extends AnnotationAssertion>)newValue);
				return;
			case OwlPackage.ONTOLOGY__SUB_ANNOTATION_PROPERTY_OF:
				getSubAnnotationPropertyOf().clear();
				getSubAnnotationPropertyOf().addAll((Collection<? extends SubAnnotationPropertyOf>)newValue);
				return;
			case OwlPackage.ONTOLOGY__ANNOTATION_PROPERTY_DOMAIN:
				getAnnotationPropertyDomain().clear();
				getAnnotationPropertyDomain().addAll((Collection<? extends AnnotationPropertyDomain>)newValue);
				return;
			case OwlPackage.ONTOLOGY__ANNOTATION_PROPERTY_RANGE:
				getAnnotationPropertyRange().clear();
				getAnnotationPropertyRange().addAll((Collection<? extends AnnotationPropertyRange>)newValue);
				return;
			case OwlPackage.ONTOLOGY__BASE:
				setBase((String)newValue);
				return;
			case OwlPackage.ONTOLOGY__ID:
				setId((String)newValue);
				return;
			case OwlPackage.ONTOLOGY__LANG:
				setLang((String)newValue);
				return;
			case OwlPackage.ONTOLOGY__ONTOLOGY_IRI:
				setOntologyIRI((String)newValue);
				return;
			case OwlPackage.ONTOLOGY__SPACE:
				setSpace((SpaceType)newValue);
				return;
			case OwlPackage.ONTOLOGY__VERSION_IRI:
				setVersionIRI((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.ONTOLOGY__PREFIX:
				getPrefix().clear();
				return;
			case OwlPackage.ONTOLOGY__IMPORT:
				getImport().clear();
				return;
			case OwlPackage.ONTOLOGY__ANNOTATION:
				getAnnotation().clear();
				return;
			case OwlPackage.ONTOLOGY__AXIOM:
				getAxiom().clear();
				return;
			case OwlPackage.ONTOLOGY__DECLARATION:
				getDeclaration().clear();
				return;
			case OwlPackage.ONTOLOGY__SUB_CLASS_OF:
				getSubClassOf().clear();
				return;
			case OwlPackage.ONTOLOGY__EQUIVALENT_CLASSES:
				getEquivalentClasses().clear();
				return;
			case OwlPackage.ONTOLOGY__DISJOINT_CLASSES:
				getDisjointClasses().clear();
				return;
			case OwlPackage.ONTOLOGY__DISJOINT_UNION:
				getDisjointUnion().clear();
				return;
			case OwlPackage.ONTOLOGY__SUB_OBJECT_PROPERTY_OF:
				getSubObjectPropertyOf().clear();
				return;
			case OwlPackage.ONTOLOGY__EQUIVALENT_OBJECT_PROPERTIES:
				getEquivalentObjectProperties().clear();
				return;
			case OwlPackage.ONTOLOGY__DISJOINT_OBJECT_PROPERTIES:
				getDisjointObjectProperties().clear();
				return;
			case OwlPackage.ONTOLOGY__INVERSE_OBJECT_PROPERTIES:
				getInverseObjectProperties().clear();
				return;
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_DOMAIN:
				getObjectPropertyDomain().clear();
				return;
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_RANGE:
				getObjectPropertyRange().clear();
				return;
			case OwlPackage.ONTOLOGY__FUNCTIONAL_OBJECT_PROPERTY:
				getFunctionalObjectProperty().clear();
				return;
			case OwlPackage.ONTOLOGY__INVERSE_FUNCTIONAL_OBJECT_PROPERTY:
				getInverseFunctionalObjectProperty().clear();
				return;
			case OwlPackage.ONTOLOGY__REFLEXIVE_OBJECT_PROPERTY:
				getReflexiveObjectProperty().clear();
				return;
			case OwlPackage.ONTOLOGY__IRREFLEXIVE_OBJECT_PROPERTY:
				getIrreflexiveObjectProperty().clear();
				return;
			case OwlPackage.ONTOLOGY__SYMMETRIC_OBJECT_PROPERTY:
				getSymmetricObjectProperty().clear();
				return;
			case OwlPackage.ONTOLOGY__ASYMMETRIC_OBJECT_PROPERTY:
				getAsymmetricObjectProperty().clear();
				return;
			case OwlPackage.ONTOLOGY__TRANSITIVE_OBJECT_PROPERTY:
				getTransitiveObjectProperty().clear();
				return;
			case OwlPackage.ONTOLOGY__SUB_DATA_PROPERTY_OF:
				getSubDataPropertyOf().clear();
				return;
			case OwlPackage.ONTOLOGY__EQUIVALENT_DATA_PROPERTIES:
				getEquivalentDataProperties().clear();
				return;
			case OwlPackage.ONTOLOGY__DISJOINT_DATA_PROPERTIES:
				getDisjointDataProperties().clear();
				return;
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_DOMAIN:
				getDataPropertyDomain().clear();
				return;
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_RANGE:
				getDataPropertyRange().clear();
				return;
			case OwlPackage.ONTOLOGY__FUNCTIONAL_DATA_PROPERTY:
				getFunctionalDataProperty().clear();
				return;
			case OwlPackage.ONTOLOGY__DATATYPE_DEFINITION:
				getDatatypeDefinition().clear();
				return;
			case OwlPackage.ONTOLOGY__HAS_KEY:
				getHasKey().clear();
				return;
			case OwlPackage.ONTOLOGY__SAME_INDIVIDUAL:
				getSameIndividual().clear();
				return;
			case OwlPackage.ONTOLOGY__DIFFERENT_INDIVIDUALS:
				getDifferentIndividuals().clear();
				return;
			case OwlPackage.ONTOLOGY__CLASS_ASSERTION:
				getClassAssertion().clear();
				return;
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_ASSERTION:
				getObjectPropertyAssertion().clear();
				return;
			case OwlPackage.ONTOLOGY__NEGATIVE_OBJECT_PROPERTY_ASSERTION:
				getNegativeObjectPropertyAssertion().clear();
				return;
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_ASSERTION:
				getDataPropertyAssertion().clear();
				return;
			case OwlPackage.ONTOLOGY__NEGATIVE_DATA_PROPERTY_ASSERTION:
				getNegativeDataPropertyAssertion().clear();
				return;
			case OwlPackage.ONTOLOGY__ANNOTATION_ASSERTION:
				getAnnotationAssertion().clear();
				return;
			case OwlPackage.ONTOLOGY__SUB_ANNOTATION_PROPERTY_OF:
				getSubAnnotationPropertyOf().clear();
				return;
			case OwlPackage.ONTOLOGY__ANNOTATION_PROPERTY_DOMAIN:
				getAnnotationPropertyDomain().clear();
				return;
			case OwlPackage.ONTOLOGY__ANNOTATION_PROPERTY_RANGE:
				getAnnotationPropertyRange().clear();
				return;
			case OwlPackage.ONTOLOGY__BASE:
				setBase(BASE_EDEFAULT);
				return;
			case OwlPackage.ONTOLOGY__ID:
				setId(ID_EDEFAULT);
				return;
			case OwlPackage.ONTOLOGY__LANG:
				setLang(LANG_EDEFAULT);
				return;
			case OwlPackage.ONTOLOGY__ONTOLOGY_IRI:
				setOntologyIRI(ONTOLOGY_IRI_EDEFAULT);
				return;
			case OwlPackage.ONTOLOGY__SPACE:
				unsetSpace();
				return;
			case OwlPackage.ONTOLOGY__VERSION_IRI:
				setVersionIRI(VERSION_IRI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.ONTOLOGY__PREFIX:
				return prefix != null && !prefix.isEmpty();
			case OwlPackage.ONTOLOGY__IMPORT:
				return import_ != null && !import_.isEmpty();
			case OwlPackage.ONTOLOGY__ANNOTATION:
				return annotation != null && !annotation.isEmpty();
			case OwlPackage.ONTOLOGY__AXIOM:
				return axiom != null && !axiom.isEmpty();
			case OwlPackage.ONTOLOGY__DECLARATION:
				return !getDeclaration().isEmpty();
			case OwlPackage.ONTOLOGY__SUB_CLASS_OF:
				return !getSubClassOf().isEmpty();
			case OwlPackage.ONTOLOGY__EQUIVALENT_CLASSES:
				return !getEquivalentClasses().isEmpty();
			case OwlPackage.ONTOLOGY__DISJOINT_CLASSES:
				return !getDisjointClasses().isEmpty();
			case OwlPackage.ONTOLOGY__DISJOINT_UNION:
				return !getDisjointUnion().isEmpty();
			case OwlPackage.ONTOLOGY__SUB_OBJECT_PROPERTY_OF:
				return !getSubObjectPropertyOf().isEmpty();
			case OwlPackage.ONTOLOGY__EQUIVALENT_OBJECT_PROPERTIES:
				return !getEquivalentObjectProperties().isEmpty();
			case OwlPackage.ONTOLOGY__DISJOINT_OBJECT_PROPERTIES:
				return !getDisjointObjectProperties().isEmpty();
			case OwlPackage.ONTOLOGY__INVERSE_OBJECT_PROPERTIES:
				return !getInverseObjectProperties().isEmpty();
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_DOMAIN:
				return !getObjectPropertyDomain().isEmpty();
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_RANGE:
				return !getObjectPropertyRange().isEmpty();
			case OwlPackage.ONTOLOGY__FUNCTIONAL_OBJECT_PROPERTY:
				return !getFunctionalObjectProperty().isEmpty();
			case OwlPackage.ONTOLOGY__INVERSE_FUNCTIONAL_OBJECT_PROPERTY:
				return !getInverseFunctionalObjectProperty().isEmpty();
			case OwlPackage.ONTOLOGY__REFLEXIVE_OBJECT_PROPERTY:
				return !getReflexiveObjectProperty().isEmpty();
			case OwlPackage.ONTOLOGY__IRREFLEXIVE_OBJECT_PROPERTY:
				return !getIrreflexiveObjectProperty().isEmpty();
			case OwlPackage.ONTOLOGY__SYMMETRIC_OBJECT_PROPERTY:
				return !getSymmetricObjectProperty().isEmpty();
			case OwlPackage.ONTOLOGY__ASYMMETRIC_OBJECT_PROPERTY:
				return !getAsymmetricObjectProperty().isEmpty();
			case OwlPackage.ONTOLOGY__TRANSITIVE_OBJECT_PROPERTY:
				return !getTransitiveObjectProperty().isEmpty();
			case OwlPackage.ONTOLOGY__SUB_DATA_PROPERTY_OF:
				return !getSubDataPropertyOf().isEmpty();
			case OwlPackage.ONTOLOGY__EQUIVALENT_DATA_PROPERTIES:
				return !getEquivalentDataProperties().isEmpty();
			case OwlPackage.ONTOLOGY__DISJOINT_DATA_PROPERTIES:
				return !getDisjointDataProperties().isEmpty();
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_DOMAIN:
				return !getDataPropertyDomain().isEmpty();
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_RANGE:
				return !getDataPropertyRange().isEmpty();
			case OwlPackage.ONTOLOGY__FUNCTIONAL_DATA_PROPERTY:
				return !getFunctionalDataProperty().isEmpty();
			case OwlPackage.ONTOLOGY__DATATYPE_DEFINITION:
				return !getDatatypeDefinition().isEmpty();
			case OwlPackage.ONTOLOGY__HAS_KEY:
				return !getHasKey().isEmpty();
			case OwlPackage.ONTOLOGY__SAME_INDIVIDUAL:
				return !getSameIndividual().isEmpty();
			case OwlPackage.ONTOLOGY__DIFFERENT_INDIVIDUALS:
				return !getDifferentIndividuals().isEmpty();
			case OwlPackage.ONTOLOGY__CLASS_ASSERTION:
				return !getClassAssertion().isEmpty();
			case OwlPackage.ONTOLOGY__OBJECT_PROPERTY_ASSERTION:
				return !getObjectPropertyAssertion().isEmpty();
			case OwlPackage.ONTOLOGY__NEGATIVE_OBJECT_PROPERTY_ASSERTION:
				return !getNegativeObjectPropertyAssertion().isEmpty();
			case OwlPackage.ONTOLOGY__DATA_PROPERTY_ASSERTION:
				return !getDataPropertyAssertion().isEmpty();
			case OwlPackage.ONTOLOGY__NEGATIVE_DATA_PROPERTY_ASSERTION:
				return !getNegativeDataPropertyAssertion().isEmpty();
			case OwlPackage.ONTOLOGY__ANNOTATION_ASSERTION:
				return !getAnnotationAssertion().isEmpty();
			case OwlPackage.ONTOLOGY__SUB_ANNOTATION_PROPERTY_OF:
				return !getSubAnnotationPropertyOf().isEmpty();
			case OwlPackage.ONTOLOGY__ANNOTATION_PROPERTY_DOMAIN:
				return !getAnnotationPropertyDomain().isEmpty();
			case OwlPackage.ONTOLOGY__ANNOTATION_PROPERTY_RANGE:
				return !getAnnotationPropertyRange().isEmpty();
			case OwlPackage.ONTOLOGY__BASE:
				return BASE_EDEFAULT == null ? base != null : !BASE_EDEFAULT.equals(base);
			case OwlPackage.ONTOLOGY__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case OwlPackage.ONTOLOGY__LANG:
				return LANG_EDEFAULT == null ? lang != null : !LANG_EDEFAULT.equals(lang);
			case OwlPackage.ONTOLOGY__ONTOLOGY_IRI:
				return ONTOLOGY_IRI_EDEFAULT == null ? ontologyIRI != null : !ONTOLOGY_IRI_EDEFAULT.equals(ontologyIRI);
			case OwlPackage.ONTOLOGY__SPACE:
				return isSetSpace();
			case OwlPackage.ONTOLOGY__VERSION_IRI:
				return VERSION_IRI_EDEFAULT == null ? versionIRI != null : !VERSION_IRI_EDEFAULT.equals(versionIRI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (axiom: ");
		result.append(axiom);
		result.append(", base: ");
		result.append(base);
		result.append(", id: ");
		result.append(id);
		result.append(", lang: ");
		result.append(lang);
		result.append(", ontologyIRI: ");
		result.append(ontologyIRI);
		result.append(", space: ");
		if (spaceESet) result.append(space); else result.append("<unset>");
		result.append(", versionIRI: ");
		result.append(versionIRI);
		result.append(')');
		return result.toString();
	}

} //OntologyImpl
