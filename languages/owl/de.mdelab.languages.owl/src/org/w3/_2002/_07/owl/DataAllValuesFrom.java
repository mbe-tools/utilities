/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data All Values From</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataPropertyExpression <em>Data Property Expression</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataProperty <em>Data Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataAllValuesFrom#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataIntersectionOf <em>Data Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataUnionOf <em>Data Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataComplementOf <em>Data Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataOneOf <em>Data One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataAllValuesFrom#getDatatypeRestriction <em>Datatype Restriction</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getDataAllValuesFrom()
 * @model extendedMetaData="name='DataAllValuesFrom' kind='elementOnly'"
 * @generated
 */
public interface DataAllValuesFrom extends ClassExpression {
	/**
	 * Returns the value of the '<em><b>Data Property Expression</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property Expression</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property Expression</em>' attribute list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDataAllValuesFrom_DataPropertyExpression()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='DataPropertyExpression:4'"
	 * @generated
	 */
	FeatureMap getDataPropertyExpression();

	/**
	 * Returns the value of the '<em><b>Data Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDataAllValuesFrom_DataProperty()
	 * @model containment="true" required="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataProperty' namespace='##targetNamespace' group='#DataPropertyExpression:4'"
	 * @generated
	 */
	EList<DataProperty> getDataProperty();

	/**
	 * Returns the value of the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype</em>' containment reference.
	 * @see #setDatatype(Datatype)
	 * @see org.w3._2002._07.owl.OwlPackage#getDataAllValuesFrom_Datatype()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Datatype' namespace='##targetNamespace'"
	 * @generated
	 */
	Datatype getDatatype();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDatatype <em>Datatype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype</em>' containment reference.
	 * @see #getDatatype()
	 * @generated
	 */
	void setDatatype(Datatype value);

	/**
	 * Returns the value of the '<em><b>Data Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Intersection Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Intersection Of</em>' containment reference.
	 * @see #setDataIntersectionOf(DataIntersectionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDataAllValuesFrom_DataIntersectionOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataIntersectionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	DataIntersectionOf getDataIntersectionOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataIntersectionOf <em>Data Intersection Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Intersection Of</em>' containment reference.
	 * @see #getDataIntersectionOf()
	 * @generated
	 */
	void setDataIntersectionOf(DataIntersectionOf value);

	/**
	 * Returns the value of the '<em><b>Data Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Union Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Union Of</em>' containment reference.
	 * @see #setDataUnionOf(DataUnionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDataAllValuesFrom_DataUnionOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataUnionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	DataUnionOf getDataUnionOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataUnionOf <em>Data Union Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Union Of</em>' containment reference.
	 * @see #getDataUnionOf()
	 * @generated
	 */
	void setDataUnionOf(DataUnionOf value);

	/**
	 * Returns the value of the '<em><b>Data Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Complement Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Complement Of</em>' containment reference.
	 * @see #setDataComplementOf(DataComplementOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDataAllValuesFrom_DataComplementOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataComplementOf' namespace='##targetNamespace'"
	 * @generated
	 */
	DataComplementOf getDataComplementOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataComplementOf <em>Data Complement Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Complement Of</em>' containment reference.
	 * @see #getDataComplementOf()
	 * @generated
	 */
	void setDataComplementOf(DataComplementOf value);

	/**
	 * Returns the value of the '<em><b>Data One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data One Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data One Of</em>' containment reference.
	 * @see #setDataOneOf(DataOneOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDataAllValuesFrom_DataOneOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataOneOf' namespace='##targetNamespace'"
	 * @generated
	 */
	DataOneOf getDataOneOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataOneOf <em>Data One Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data One Of</em>' containment reference.
	 * @see #getDataOneOf()
	 * @generated
	 */
	void setDataOneOf(DataOneOf value);

	/**
	 * Returns the value of the '<em><b>Datatype Restriction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype Restriction</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype Restriction</em>' containment reference.
	 * @see #setDatatypeRestriction(DatatypeRestriction)
	 * @see org.w3._2002._07.owl.OwlPackage#getDataAllValuesFrom_DatatypeRestriction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DatatypeRestriction' namespace='##targetNamespace'"
	 * @generated
	 */
	DatatypeRestriction getDatatypeRestriction();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDatatypeRestriction <em>Datatype Restriction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype Restriction</em>' containment reference.
	 * @see #getDatatypeRestriction()
	 * @generated
	 */
	void setDatatypeRestriction(DatatypeRestriction value);

} // DataAllValuesFrom
