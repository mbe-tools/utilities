/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equivalent Object Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.EquivalentObjectProperties#getObjectPropertyExpression <em>Object Property Expression</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.EquivalentObjectProperties#getObjectProperty <em>Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.EquivalentObjectProperties#getObjectInverseOf <em>Object Inverse Of</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getEquivalentObjectProperties()
 * @model extendedMetaData="name='EquivalentObjectProperties' kind='elementOnly'"
 * @generated
 */
public interface EquivalentObjectProperties extends ObjectPropertyAxiom {
	/**
	 * Returns the value of the '<em><b>Object Property Expression</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property Expression</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property Expression</em>' attribute list.
	 * @see org.w3._2002._07.owl.OwlPackage#getEquivalentObjectProperties_ObjectPropertyExpression()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='ObjectPropertyExpression:5'"
	 * @generated
	 */
	FeatureMap getObjectPropertyExpression();

	/**
	 * Returns the value of the '<em><b>Object Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getEquivalentObjectProperties_ObjectProperty()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectProperty' namespace='##targetNamespace' group='#ObjectPropertyExpression:5'"
	 * @generated
	 */
	EList<ObjectProperty> getObjectProperty();

	/**
	 * Returns the value of the '<em><b>Object Inverse Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectInverseOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Inverse Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Inverse Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getEquivalentObjectProperties_ObjectInverseOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectInverseOf' namespace='##targetNamespace' group='#ObjectPropertyExpression:5'"
	 * @generated
	 */
	EList<ObjectInverseOf> getObjectInverseOf();

} // EquivalentObjectProperties
