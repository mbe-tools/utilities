/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Data Property Of</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.SubDataPropertyOf#getDataProperty <em>Data Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubDataPropertyOf#getDataProperty1 <em>Data Property1</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getSubDataPropertyOf()
 * @model extendedMetaData="name='SubDataPropertyOf' kind='elementOnly'"
 * @generated
 */
public interface SubDataPropertyOf extends DataPropertyAxiom {
	/**
	 * Returns the value of the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property</em>' containment reference.
	 * @see #setDataProperty(DataProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubDataPropertyOf_DataProperty()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DataProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	DataProperty getDataProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubDataPropertyOf#getDataProperty <em>Data Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Property</em>' containment reference.
	 * @see #getDataProperty()
	 * @generated
	 */
	void setDataProperty(DataProperty value);

	/**
	 * Returns the value of the '<em><b>Data Property1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property1</em>' containment reference.
	 * @see #setDataProperty1(DataProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubDataPropertyOf_DataProperty1()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DataProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	DataProperty getDataProperty1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubDataPropertyOf#getDataProperty1 <em>Data Property1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Property1</em>' containment reference.
	 * @see #getDataProperty1()
	 * @generated
	 */
	void setDataProperty1(DataProperty value);

} // SubDataPropertyOf
