/**
 */
package org.w3._2002._07.owl.impl;

import org.eclipse.emf.ecore.EClass;

import org.w3._2002._07.owl.AnnotationAxiom;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotation Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AnnotationAxiomImpl extends AxiomImpl implements AnnotationAxiom {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnotationAxiomImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getAnnotationAxiom();
	}

} //AnnotationAxiomImpl
