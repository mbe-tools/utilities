/**
 */
package org.w3._2002._07.owl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.w3._2002._07.owl.AbbreviatedIRI1;
import org.w3._2002._07.owl.AnnotationProperty;
import org.w3._2002._07.owl.AnnotationPropertyRange;
import org.w3._2002._07.owl.IRI;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotation Property Range</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.AnnotationPropertyRangeImpl#getAnnotationProperty <em>Annotation Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.AnnotationPropertyRangeImpl#getIRI <em>IRI</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.AnnotationPropertyRangeImpl#getAbbreviatedIRI <em>Abbreviated IRI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnnotationPropertyRangeImpl extends AnnotationAxiomImpl implements AnnotationPropertyRange {
	/**
	 * The cached value of the '{@link #getAnnotationProperty() <em>Annotation Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotationProperty()
	 * @generated
	 * @ordered
	 */
	protected AnnotationProperty annotationProperty;

	/**
	 * The cached value of the '{@link #getIRI() <em>IRI</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIRI()
	 * @generated
	 * @ordered
	 */
	protected IRI iRI;

	/**
	 * The cached value of the '{@link #getAbbreviatedIRI() <em>Abbreviated IRI</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbbreviatedIRI()
	 * @generated
	 * @ordered
	 */
	protected AbbreviatedIRI1 abbreviatedIRI;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnotationPropertyRangeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getAnnotationPropertyRange();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationProperty getAnnotationProperty() {
		return annotationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotationProperty(AnnotationProperty newAnnotationProperty, NotificationChain msgs) {
		AnnotationProperty oldAnnotationProperty = annotationProperty;
		annotationProperty = newAnnotationProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_PROPERTY_RANGE__ANNOTATION_PROPERTY, oldAnnotationProperty, newAnnotationProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotationProperty(AnnotationProperty newAnnotationProperty) {
		if (newAnnotationProperty != annotationProperty) {
			NotificationChain msgs = null;
			if (annotationProperty != null)
				msgs = ((InternalEObject)annotationProperty).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_PROPERTY_RANGE__ANNOTATION_PROPERTY, null, msgs);
			if (newAnnotationProperty != null)
				msgs = ((InternalEObject)newAnnotationProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_PROPERTY_RANGE__ANNOTATION_PROPERTY, null, msgs);
			msgs = basicSetAnnotationProperty(newAnnotationProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_PROPERTY_RANGE__ANNOTATION_PROPERTY, newAnnotationProperty, newAnnotationProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRI getIRI() {
		return iRI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIRI(IRI newIRI, NotificationChain msgs) {
		IRI oldIRI = iRI;
		iRI = newIRI;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_PROPERTY_RANGE__IRI, oldIRI, newIRI);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIRI(IRI newIRI) {
		if (newIRI != iRI) {
			NotificationChain msgs = null;
			if (iRI != null)
				msgs = ((InternalEObject)iRI).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_PROPERTY_RANGE__IRI, null, msgs);
			if (newIRI != null)
				msgs = ((InternalEObject)newIRI).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_PROPERTY_RANGE__IRI, null, msgs);
			msgs = basicSetIRI(newIRI, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_PROPERTY_RANGE__IRI, newIRI, newIRI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbbreviatedIRI1 getAbbreviatedIRI() {
		return abbreviatedIRI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbbreviatedIRI(AbbreviatedIRI1 newAbbreviatedIRI, NotificationChain msgs) {
		AbbreviatedIRI1 oldAbbreviatedIRI = abbreviatedIRI;
		abbreviatedIRI = newAbbreviatedIRI;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_PROPERTY_RANGE__ABBREVIATED_IRI, oldAbbreviatedIRI, newAbbreviatedIRI);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbbreviatedIRI(AbbreviatedIRI1 newAbbreviatedIRI) {
		if (newAbbreviatedIRI != abbreviatedIRI) {
			NotificationChain msgs = null;
			if (abbreviatedIRI != null)
				msgs = ((InternalEObject)abbreviatedIRI).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_PROPERTY_RANGE__ABBREVIATED_IRI, null, msgs);
			if (newAbbreviatedIRI != null)
				msgs = ((InternalEObject)newAbbreviatedIRI).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_PROPERTY_RANGE__ABBREVIATED_IRI, null, msgs);
			msgs = basicSetAbbreviatedIRI(newAbbreviatedIRI, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_PROPERTY_RANGE__ABBREVIATED_IRI, newAbbreviatedIRI, newAbbreviatedIRI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__ANNOTATION_PROPERTY:
				return basicSetAnnotationProperty(null, msgs);
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__IRI:
				return basicSetIRI(null, msgs);
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__ABBREVIATED_IRI:
				return basicSetAbbreviatedIRI(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__ANNOTATION_PROPERTY:
				return getAnnotationProperty();
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__IRI:
				return getIRI();
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__ABBREVIATED_IRI:
				return getAbbreviatedIRI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__ANNOTATION_PROPERTY:
				setAnnotationProperty((AnnotationProperty)newValue);
				return;
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__IRI:
				setIRI((IRI)newValue);
				return;
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__ABBREVIATED_IRI:
				setAbbreviatedIRI((AbbreviatedIRI1)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__ANNOTATION_PROPERTY:
				setAnnotationProperty((AnnotationProperty)null);
				return;
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__IRI:
				setIRI((IRI)null);
				return;
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__ABBREVIATED_IRI:
				setAbbreviatedIRI((AbbreviatedIRI1)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__ANNOTATION_PROPERTY:
				return annotationProperty != null;
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__IRI:
				return iRI != null;
			case OwlPackage.ANNOTATION_PROPERTY_RANGE__ABBREVIATED_IRI:
				return abbreviatedIRI != null;
		}
		return super.eIsSet(featureID);
	}

} //AnnotationPropertyRangeImpl
