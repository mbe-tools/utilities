/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Annotation Property Of</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.SubAnnotationPropertyOf#getAnnotationProperty <em>Annotation Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.SubAnnotationPropertyOf#getAnnotationProperty1 <em>Annotation Property1</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getSubAnnotationPropertyOf()
 * @model extendedMetaData="name='SubAnnotationPropertyOf' kind='elementOnly'"
 * @generated
 */
public interface SubAnnotationPropertyOf extends AnnotationAxiom {
	/**
	 * Returns the value of the '<em><b>Annotation Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Property</em>' containment reference.
	 * @see #setAnnotationProperty(AnnotationProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubAnnotationPropertyOf_AnnotationProperty()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='AnnotationProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	AnnotationProperty getAnnotationProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubAnnotationPropertyOf#getAnnotationProperty <em>Annotation Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Property</em>' containment reference.
	 * @see #getAnnotationProperty()
	 * @generated
	 */
	void setAnnotationProperty(AnnotationProperty value);

	/**
	 * Returns the value of the '<em><b>Annotation Property1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Property1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Property1</em>' containment reference.
	 * @see #setAnnotationProperty1(AnnotationProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getSubAnnotationPropertyOf_AnnotationProperty1()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='AnnotationProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	AnnotationProperty getAnnotationProperty1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.SubAnnotationPropertyOf#getAnnotationProperty1 <em>Annotation Property1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Property1</em>' containment reference.
	 * @see #getAnnotationProperty1()
	 * @generated
	 */
	void setAnnotationProperty1(AnnotationProperty value);

} // SubAnnotationPropertyOf
