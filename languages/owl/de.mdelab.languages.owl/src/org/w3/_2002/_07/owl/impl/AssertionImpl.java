/**
 */
package org.w3._2002._07.owl.impl;

import org.eclipse.emf.ecore.EClass;

import org.w3._2002._07.owl.Assertion;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assertion</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AssertionImpl extends AxiomImpl implements Assertion {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getAssertion();
	}

} //AssertionImpl
