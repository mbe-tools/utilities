/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Union Of</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getClassExpression <em>Class Expression</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectIntersectionOf <em>Object Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectUnionOf <em>Object Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectComplementOf <em>Object Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectOneOf <em>Object One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectSomeValuesFrom <em>Object Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectAllValuesFrom <em>Object All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectHasValue <em>Object Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectHasSelf <em>Object Has Self</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectMinCardinality <em>Object Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectMaxCardinality <em>Object Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectExactCardinality <em>Object Exact Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getDataSomeValuesFrom <em>Data Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getDataAllValuesFrom <em>Data All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getDataHasValue <em>Data Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getDataMinCardinality <em>Data Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getDataMaxCardinality <em>Data Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectUnionOf#getDataExactCardinality <em>Data Exact Cardinality</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf()
 * @model extendedMetaData="name='ObjectUnionOf' kind='elementOnly'"
 * @generated
 */
public interface ObjectUnionOf extends ClassExpression {
	/**
	 * Returns the value of the '<em><b>Class Expression</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Expression</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Expression</em>' attribute list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_ClassExpression()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='ClassExpression:4'"
	 * @generated
	 */
	FeatureMap getClassExpression();

	/**
	 * Returns the value of the '<em><b>Class</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_Class()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Class' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<org.w3._2002._07.owl.Class> getClass_();

	/**
	 * Returns the value of the '<em><b>Object Intersection Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectIntersectionOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Intersection Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Intersection Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_ObjectIntersectionOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectIntersectionOf' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<ObjectIntersectionOf> getObjectIntersectionOf();

	/**
	 * Returns the value of the '<em><b>Object Union Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectUnionOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Union Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Union Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_ObjectUnionOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectUnionOf' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<ObjectUnionOf> getObjectUnionOf();

	/**
	 * Returns the value of the '<em><b>Object Complement Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectComplementOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Complement Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Complement Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_ObjectComplementOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectComplementOf' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<ObjectComplementOf> getObjectComplementOf();

	/**
	 * Returns the value of the '<em><b>Object One Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectOneOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object One Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object One Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_ObjectOneOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectOneOf' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<ObjectOneOf> getObjectOneOf();

	/**
	 * Returns the value of the '<em><b>Object Some Values From</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectSomeValuesFrom}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Some Values From</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Some Values From</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_ObjectSomeValuesFrom()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectSomeValuesFrom' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<ObjectSomeValuesFrom> getObjectSomeValuesFrom();

	/**
	 * Returns the value of the '<em><b>Object All Values From</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectAllValuesFrom}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object All Values From</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object All Values From</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_ObjectAllValuesFrom()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectAllValuesFrom' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<ObjectAllValuesFrom> getObjectAllValuesFrom();

	/**
	 * Returns the value of the '<em><b>Object Has Value</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectHasValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Has Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Has Value</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_ObjectHasValue()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectHasValue' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<ObjectHasValue> getObjectHasValue();

	/**
	 * Returns the value of the '<em><b>Object Has Self</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectHasSelf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Has Self</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Has Self</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_ObjectHasSelf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectHasSelf' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<ObjectHasSelf> getObjectHasSelf();

	/**
	 * Returns the value of the '<em><b>Object Min Cardinality</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectMinCardinality}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Min Cardinality</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Min Cardinality</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_ObjectMinCardinality()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectMinCardinality' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<ObjectMinCardinality> getObjectMinCardinality();

	/**
	 * Returns the value of the '<em><b>Object Max Cardinality</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectMaxCardinality}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Max Cardinality</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Max Cardinality</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_ObjectMaxCardinality()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectMaxCardinality' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<ObjectMaxCardinality> getObjectMaxCardinality();

	/**
	 * Returns the value of the '<em><b>Object Exact Cardinality</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectExactCardinality}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Exact Cardinality</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Exact Cardinality</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_ObjectExactCardinality()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectExactCardinality' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<ObjectExactCardinality> getObjectExactCardinality();

	/**
	 * Returns the value of the '<em><b>Data Some Values From</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataSomeValuesFrom}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Some Values From</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Some Values From</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_DataSomeValuesFrom()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataSomeValuesFrom' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<DataSomeValuesFrom> getDataSomeValuesFrom();

	/**
	 * Returns the value of the '<em><b>Data All Values From</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataAllValuesFrom}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data All Values From</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data All Values From</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_DataAllValuesFrom()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataAllValuesFrom' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<DataAllValuesFrom> getDataAllValuesFrom();

	/**
	 * Returns the value of the '<em><b>Data Has Value</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataHasValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Has Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Has Value</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_DataHasValue()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataHasValue' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<DataHasValue> getDataHasValue();

	/**
	 * Returns the value of the '<em><b>Data Min Cardinality</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataMinCardinality}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Min Cardinality</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Min Cardinality</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_DataMinCardinality()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataMinCardinality' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<DataMinCardinality> getDataMinCardinality();

	/**
	 * Returns the value of the '<em><b>Data Max Cardinality</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataMaxCardinality}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Max Cardinality</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Max Cardinality</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_DataMaxCardinality()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataMaxCardinality' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<DataMaxCardinality> getDataMaxCardinality();

	/**
	 * Returns the value of the '<em><b>Data Exact Cardinality</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataExactCardinality}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Exact Cardinality</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Exact Cardinality</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectUnionOf_DataExactCardinality()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataExactCardinality' namespace='##targetNamespace' group='#ClassExpression:4'"
	 * @generated
	 */
	EList<DataExactCardinality> getDataExactCardinality();

} // ObjectUnionOf
