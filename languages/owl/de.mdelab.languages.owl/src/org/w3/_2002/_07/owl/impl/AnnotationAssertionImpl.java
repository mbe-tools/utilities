/**
 */
package org.w3._2002._07.owl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.w3._2002._07.owl.AbbreviatedIRI1;
import org.w3._2002._07.owl.AnnotationAssertion;
import org.w3._2002._07.owl.AnnotationProperty;
import org.w3._2002._07.owl.AnonymousIndividual;
import org.w3._2002._07.owl.IRI;
import org.w3._2002._07.owl.Literal;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotation Assertion</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.AnnotationAssertionImpl#getAnnotationProperty <em>Annotation Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.AnnotationAssertionImpl#getIRI <em>IRI</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.AnnotationAssertionImpl#getAbbreviatedIRI <em>Abbreviated IRI</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.AnnotationAssertionImpl#getAnonymousIndividual <em>Anonymous Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.AnnotationAssertionImpl#getIRI1 <em>IRI1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.AnnotationAssertionImpl#getAbbreviatedIRI1 <em>Abbreviated IRI1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.AnnotationAssertionImpl#getAnonymousIndividual1 <em>Anonymous Individual1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.AnnotationAssertionImpl#getLiteral <em>Literal</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnnotationAssertionImpl extends AnnotationAxiomImpl implements AnnotationAssertion {
	/**
	 * The cached value of the '{@link #getAnnotationProperty() <em>Annotation Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotationProperty()
	 * @generated
	 * @ordered
	 */
	protected AnnotationProperty annotationProperty;

	/**
	 * The cached value of the '{@link #getIRI() <em>IRI</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIRI()
	 * @generated
	 * @ordered
	 */
	protected IRI iRI;

	/**
	 * The cached value of the '{@link #getAbbreviatedIRI() <em>Abbreviated IRI</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbbreviatedIRI()
	 * @generated
	 * @ordered
	 */
	protected AbbreviatedIRI1 abbreviatedIRI;

	/**
	 * The cached value of the '{@link #getAnonymousIndividual() <em>Anonymous Individual</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousIndividual()
	 * @generated
	 * @ordered
	 */
	protected AnonymousIndividual anonymousIndividual;

	/**
	 * The cached value of the '{@link #getIRI1() <em>IRI1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIRI1()
	 * @generated
	 * @ordered
	 */
	protected IRI iRI1;

	/**
	 * The cached value of the '{@link #getAbbreviatedIRI1() <em>Abbreviated IRI1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbbreviatedIRI1()
	 * @generated
	 * @ordered
	 */
	protected AbbreviatedIRI1 abbreviatedIRI1;

	/**
	 * The cached value of the '{@link #getAnonymousIndividual1() <em>Anonymous Individual1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousIndividual1()
	 * @generated
	 * @ordered
	 */
	protected AnonymousIndividual anonymousIndividual1;

	/**
	 * The cached value of the '{@link #getLiteral() <em>Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLiteral()
	 * @generated
	 * @ordered
	 */
	protected Literal literal;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnotationAssertionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getAnnotationAssertion();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationProperty getAnnotationProperty() {
		return annotationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotationProperty(AnnotationProperty newAnnotationProperty, NotificationChain msgs) {
		AnnotationProperty oldAnnotationProperty = annotationProperty;
		annotationProperty = newAnnotationProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__ANNOTATION_PROPERTY, oldAnnotationProperty, newAnnotationProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotationProperty(AnnotationProperty newAnnotationProperty) {
		if (newAnnotationProperty != annotationProperty) {
			NotificationChain msgs = null;
			if (annotationProperty != null)
				msgs = ((InternalEObject)annotationProperty).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__ANNOTATION_PROPERTY, null, msgs);
			if (newAnnotationProperty != null)
				msgs = ((InternalEObject)newAnnotationProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__ANNOTATION_PROPERTY, null, msgs);
			msgs = basicSetAnnotationProperty(newAnnotationProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__ANNOTATION_PROPERTY, newAnnotationProperty, newAnnotationProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRI getIRI() {
		return iRI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIRI(IRI newIRI, NotificationChain msgs) {
		IRI oldIRI = iRI;
		iRI = newIRI;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__IRI, oldIRI, newIRI);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIRI(IRI newIRI) {
		if (newIRI != iRI) {
			NotificationChain msgs = null;
			if (iRI != null)
				msgs = ((InternalEObject)iRI).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__IRI, null, msgs);
			if (newIRI != null)
				msgs = ((InternalEObject)newIRI).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__IRI, null, msgs);
			msgs = basicSetIRI(newIRI, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__IRI, newIRI, newIRI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbbreviatedIRI1 getAbbreviatedIRI() {
		return abbreviatedIRI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbbreviatedIRI(AbbreviatedIRI1 newAbbreviatedIRI, NotificationChain msgs) {
		AbbreviatedIRI1 oldAbbreviatedIRI = abbreviatedIRI;
		abbreviatedIRI = newAbbreviatedIRI;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI, oldAbbreviatedIRI, newAbbreviatedIRI);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbbreviatedIRI(AbbreviatedIRI1 newAbbreviatedIRI) {
		if (newAbbreviatedIRI != abbreviatedIRI) {
			NotificationChain msgs = null;
			if (abbreviatedIRI != null)
				msgs = ((InternalEObject)abbreviatedIRI).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI, null, msgs);
			if (newAbbreviatedIRI != null)
				msgs = ((InternalEObject)newAbbreviatedIRI).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI, null, msgs);
			msgs = basicSetAbbreviatedIRI(newAbbreviatedIRI, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI, newAbbreviatedIRI, newAbbreviatedIRI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousIndividual getAnonymousIndividual() {
		return anonymousIndividual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousIndividual(AnonymousIndividual newAnonymousIndividual, NotificationChain msgs) {
		AnonymousIndividual oldAnonymousIndividual = anonymousIndividual;
		anonymousIndividual = newAnonymousIndividual;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL, oldAnonymousIndividual, newAnonymousIndividual);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousIndividual(AnonymousIndividual newAnonymousIndividual) {
		if (newAnonymousIndividual != anonymousIndividual) {
			NotificationChain msgs = null;
			if (anonymousIndividual != null)
				msgs = ((InternalEObject)anonymousIndividual).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL, null, msgs);
			if (newAnonymousIndividual != null)
				msgs = ((InternalEObject)newAnonymousIndividual).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL, null, msgs);
			msgs = basicSetAnonymousIndividual(newAnonymousIndividual, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL, newAnonymousIndividual, newAnonymousIndividual));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRI getIRI1() {
		return iRI1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIRI1(IRI newIRI1, NotificationChain msgs) {
		IRI oldIRI1 = iRI1;
		iRI1 = newIRI1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__IRI1, oldIRI1, newIRI1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIRI1(IRI newIRI1) {
		if (newIRI1 != iRI1) {
			NotificationChain msgs = null;
			if (iRI1 != null)
				msgs = ((InternalEObject)iRI1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__IRI1, null, msgs);
			if (newIRI1 != null)
				msgs = ((InternalEObject)newIRI1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__IRI1, null, msgs);
			msgs = basicSetIRI1(newIRI1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__IRI1, newIRI1, newIRI1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbbreviatedIRI1 getAbbreviatedIRI1() {
		return abbreviatedIRI1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbbreviatedIRI1(AbbreviatedIRI1 newAbbreviatedIRI1, NotificationChain msgs) {
		AbbreviatedIRI1 oldAbbreviatedIRI1 = abbreviatedIRI1;
		abbreviatedIRI1 = newAbbreviatedIRI1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI1, oldAbbreviatedIRI1, newAbbreviatedIRI1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbbreviatedIRI1(AbbreviatedIRI1 newAbbreviatedIRI1) {
		if (newAbbreviatedIRI1 != abbreviatedIRI1) {
			NotificationChain msgs = null;
			if (abbreviatedIRI1 != null)
				msgs = ((InternalEObject)abbreviatedIRI1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI1, null, msgs);
			if (newAbbreviatedIRI1 != null)
				msgs = ((InternalEObject)newAbbreviatedIRI1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI1, null, msgs);
			msgs = basicSetAbbreviatedIRI1(newAbbreviatedIRI1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI1, newAbbreviatedIRI1, newAbbreviatedIRI1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousIndividual getAnonymousIndividual1() {
		return anonymousIndividual1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousIndividual1(AnonymousIndividual newAnonymousIndividual1, NotificationChain msgs) {
		AnonymousIndividual oldAnonymousIndividual1 = anonymousIndividual1;
		anonymousIndividual1 = newAnonymousIndividual1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL1, oldAnonymousIndividual1, newAnonymousIndividual1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousIndividual1(AnonymousIndividual newAnonymousIndividual1) {
		if (newAnonymousIndividual1 != anonymousIndividual1) {
			NotificationChain msgs = null;
			if (anonymousIndividual1 != null)
				msgs = ((InternalEObject)anonymousIndividual1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL1, null, msgs);
			if (newAnonymousIndividual1 != null)
				msgs = ((InternalEObject)newAnonymousIndividual1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL1, null, msgs);
			msgs = basicSetAnonymousIndividual1(newAnonymousIndividual1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL1, newAnonymousIndividual1, newAnonymousIndividual1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Literal getLiteral() {
		return literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLiteral(Literal newLiteral, NotificationChain msgs) {
		Literal oldLiteral = literal;
		literal = newLiteral;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__LITERAL, oldLiteral, newLiteral);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLiteral(Literal newLiteral) {
		if (newLiteral != literal) {
			NotificationChain msgs = null;
			if (literal != null)
				msgs = ((InternalEObject)literal).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__LITERAL, null, msgs);
			if (newLiteral != null)
				msgs = ((InternalEObject)newLiteral).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.ANNOTATION_ASSERTION__LITERAL, null, msgs);
			msgs = basicSetLiteral(newLiteral, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.ANNOTATION_ASSERTION__LITERAL, newLiteral, newLiteral));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.ANNOTATION_ASSERTION__ANNOTATION_PROPERTY:
				return basicSetAnnotationProperty(null, msgs);
			case OwlPackage.ANNOTATION_ASSERTION__IRI:
				return basicSetIRI(null, msgs);
			case OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI:
				return basicSetAbbreviatedIRI(null, msgs);
			case OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL:
				return basicSetAnonymousIndividual(null, msgs);
			case OwlPackage.ANNOTATION_ASSERTION__IRI1:
				return basicSetIRI1(null, msgs);
			case OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI1:
				return basicSetAbbreviatedIRI1(null, msgs);
			case OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				return basicSetAnonymousIndividual1(null, msgs);
			case OwlPackage.ANNOTATION_ASSERTION__LITERAL:
				return basicSetLiteral(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.ANNOTATION_ASSERTION__ANNOTATION_PROPERTY:
				return getAnnotationProperty();
			case OwlPackage.ANNOTATION_ASSERTION__IRI:
				return getIRI();
			case OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI:
				return getAbbreviatedIRI();
			case OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL:
				return getAnonymousIndividual();
			case OwlPackage.ANNOTATION_ASSERTION__IRI1:
				return getIRI1();
			case OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI1:
				return getAbbreviatedIRI1();
			case OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				return getAnonymousIndividual1();
			case OwlPackage.ANNOTATION_ASSERTION__LITERAL:
				return getLiteral();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.ANNOTATION_ASSERTION__ANNOTATION_PROPERTY:
				setAnnotationProperty((AnnotationProperty)newValue);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__IRI:
				setIRI((IRI)newValue);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI:
				setAbbreviatedIRI((AbbreviatedIRI1)newValue);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL:
				setAnonymousIndividual((AnonymousIndividual)newValue);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__IRI1:
				setIRI1((IRI)newValue);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI1:
				setAbbreviatedIRI1((AbbreviatedIRI1)newValue);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				setAnonymousIndividual1((AnonymousIndividual)newValue);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__LITERAL:
				setLiteral((Literal)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.ANNOTATION_ASSERTION__ANNOTATION_PROPERTY:
				setAnnotationProperty((AnnotationProperty)null);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__IRI:
				setIRI((IRI)null);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI:
				setAbbreviatedIRI((AbbreviatedIRI1)null);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL:
				setAnonymousIndividual((AnonymousIndividual)null);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__IRI1:
				setIRI1((IRI)null);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI1:
				setAbbreviatedIRI1((AbbreviatedIRI1)null);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				setAnonymousIndividual1((AnonymousIndividual)null);
				return;
			case OwlPackage.ANNOTATION_ASSERTION__LITERAL:
				setLiteral((Literal)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.ANNOTATION_ASSERTION__ANNOTATION_PROPERTY:
				return annotationProperty != null;
			case OwlPackage.ANNOTATION_ASSERTION__IRI:
				return iRI != null;
			case OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI:
				return abbreviatedIRI != null;
			case OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL:
				return anonymousIndividual != null;
			case OwlPackage.ANNOTATION_ASSERTION__IRI1:
				return iRI1 != null;
			case OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI1:
				return abbreviatedIRI1 != null;
			case OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				return anonymousIndividual1 != null;
			case OwlPackage.ANNOTATION_ASSERTION__LITERAL:
				return literal != null;
		}
		return super.eIsSet(featureID);
	}

} //AnnotationAssertionImpl
