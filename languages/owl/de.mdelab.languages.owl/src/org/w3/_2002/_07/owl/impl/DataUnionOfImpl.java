/**
 */
package org.w3._2002._07.owl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.w3._2002._07.owl.DataComplementOf;
import org.w3._2002._07.owl.DataIntersectionOf;
import org.w3._2002._07.owl.DataOneOf;
import org.w3._2002._07.owl.DataUnionOf;
import org.w3._2002._07.owl.Datatype;
import org.w3._2002._07.owl.DatatypeRestriction;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Union Of</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.DataUnionOfImpl#getDataRange <em>Data Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DataUnionOfImpl#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DataUnionOfImpl#getDataIntersectionOf <em>Data Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DataUnionOfImpl#getDataUnionOf <em>Data Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DataUnionOfImpl#getDataComplementOf <em>Data Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DataUnionOfImpl#getDataOneOf <em>Data One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DataUnionOfImpl#getDatatypeRestriction <em>Datatype Restriction</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataUnionOfImpl extends DataRangeImpl implements DataUnionOf {
	/**
	 * The cached value of the '{@link #getDataRange() <em>Data Range</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataRange()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap dataRange;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataUnionOfImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getDataUnionOf();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getDataRange() {
		if (dataRange == null) {
			dataRange = new BasicFeatureMap(this, OwlPackage.DATA_UNION_OF__DATA_RANGE);
		}
		return dataRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Datatype> getDatatype() {
		return getDataRange().list(OwlPackage.eINSTANCE.getDataUnionOf_Datatype());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataIntersectionOf> getDataIntersectionOf() {
		return getDataRange().list(OwlPackage.eINSTANCE.getDataUnionOf_DataIntersectionOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataUnionOf> getDataUnionOf() {
		return getDataRange().list(OwlPackage.eINSTANCE.getDataUnionOf_DataUnionOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataComplementOf> getDataComplementOf() {
		return getDataRange().list(OwlPackage.eINSTANCE.getDataUnionOf_DataComplementOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataOneOf> getDataOneOf() {
		return getDataRange().list(OwlPackage.eINSTANCE.getDataUnionOf_DataOneOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DatatypeRestriction> getDatatypeRestriction() {
		return getDataRange().list(OwlPackage.eINSTANCE.getDataUnionOf_DatatypeRestriction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.DATA_UNION_OF__DATA_RANGE:
				return ((InternalEList<?>)getDataRange()).basicRemove(otherEnd, msgs);
			case OwlPackage.DATA_UNION_OF__DATATYPE:
				return ((InternalEList<?>)getDatatype()).basicRemove(otherEnd, msgs);
			case OwlPackage.DATA_UNION_OF__DATA_INTERSECTION_OF:
				return ((InternalEList<?>)getDataIntersectionOf()).basicRemove(otherEnd, msgs);
			case OwlPackage.DATA_UNION_OF__DATA_UNION_OF:
				return ((InternalEList<?>)getDataUnionOf()).basicRemove(otherEnd, msgs);
			case OwlPackage.DATA_UNION_OF__DATA_COMPLEMENT_OF:
				return ((InternalEList<?>)getDataComplementOf()).basicRemove(otherEnd, msgs);
			case OwlPackage.DATA_UNION_OF__DATA_ONE_OF:
				return ((InternalEList<?>)getDataOneOf()).basicRemove(otherEnd, msgs);
			case OwlPackage.DATA_UNION_OF__DATATYPE_RESTRICTION:
				return ((InternalEList<?>)getDatatypeRestriction()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.DATA_UNION_OF__DATA_RANGE:
				if (coreType) return getDataRange();
				return ((FeatureMap.Internal)getDataRange()).getWrapper();
			case OwlPackage.DATA_UNION_OF__DATATYPE:
				return getDatatype();
			case OwlPackage.DATA_UNION_OF__DATA_INTERSECTION_OF:
				return getDataIntersectionOf();
			case OwlPackage.DATA_UNION_OF__DATA_UNION_OF:
				return getDataUnionOf();
			case OwlPackage.DATA_UNION_OF__DATA_COMPLEMENT_OF:
				return getDataComplementOf();
			case OwlPackage.DATA_UNION_OF__DATA_ONE_OF:
				return getDataOneOf();
			case OwlPackage.DATA_UNION_OF__DATATYPE_RESTRICTION:
				return getDatatypeRestriction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.DATA_UNION_OF__DATA_RANGE:
				((FeatureMap.Internal)getDataRange()).set(newValue);
				return;
			case OwlPackage.DATA_UNION_OF__DATATYPE:
				getDatatype().clear();
				getDatatype().addAll((Collection<? extends Datatype>)newValue);
				return;
			case OwlPackage.DATA_UNION_OF__DATA_INTERSECTION_OF:
				getDataIntersectionOf().clear();
				getDataIntersectionOf().addAll((Collection<? extends DataIntersectionOf>)newValue);
				return;
			case OwlPackage.DATA_UNION_OF__DATA_UNION_OF:
				getDataUnionOf().clear();
				getDataUnionOf().addAll((Collection<? extends DataUnionOf>)newValue);
				return;
			case OwlPackage.DATA_UNION_OF__DATA_COMPLEMENT_OF:
				getDataComplementOf().clear();
				getDataComplementOf().addAll((Collection<? extends DataComplementOf>)newValue);
				return;
			case OwlPackage.DATA_UNION_OF__DATA_ONE_OF:
				getDataOneOf().clear();
				getDataOneOf().addAll((Collection<? extends DataOneOf>)newValue);
				return;
			case OwlPackage.DATA_UNION_OF__DATATYPE_RESTRICTION:
				getDatatypeRestriction().clear();
				getDatatypeRestriction().addAll((Collection<? extends DatatypeRestriction>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.DATA_UNION_OF__DATA_RANGE:
				getDataRange().clear();
				return;
			case OwlPackage.DATA_UNION_OF__DATATYPE:
				getDatatype().clear();
				return;
			case OwlPackage.DATA_UNION_OF__DATA_INTERSECTION_OF:
				getDataIntersectionOf().clear();
				return;
			case OwlPackage.DATA_UNION_OF__DATA_UNION_OF:
				getDataUnionOf().clear();
				return;
			case OwlPackage.DATA_UNION_OF__DATA_COMPLEMENT_OF:
				getDataComplementOf().clear();
				return;
			case OwlPackage.DATA_UNION_OF__DATA_ONE_OF:
				getDataOneOf().clear();
				return;
			case OwlPackage.DATA_UNION_OF__DATATYPE_RESTRICTION:
				getDatatypeRestriction().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.DATA_UNION_OF__DATA_RANGE:
				return dataRange != null && !dataRange.isEmpty();
			case OwlPackage.DATA_UNION_OF__DATATYPE:
				return !getDatatype().isEmpty();
			case OwlPackage.DATA_UNION_OF__DATA_INTERSECTION_OF:
				return !getDataIntersectionOf().isEmpty();
			case OwlPackage.DATA_UNION_OF__DATA_UNION_OF:
				return !getDataUnionOf().isEmpty();
			case OwlPackage.DATA_UNION_OF__DATA_COMPLEMENT_OF:
				return !getDataComplementOf().isEmpty();
			case OwlPackage.DATA_UNION_OF__DATA_ONE_OF:
				return !getDataOneOf().isEmpty();
			case OwlPackage.DATA_UNION_OF__DATATYPE_RESTRICTION:
				return !getDatatypeRestriction().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dataRange: ");
		result.append(dataRange);
		result.append(')');
		return result.toString();
	}

} //DataUnionOfImpl
