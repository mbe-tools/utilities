/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assertion</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.w3._2002._07.owl.OwlPackage#getAssertion()
 * @model abstract="true"
 *        extendedMetaData="name='Assertion' kind='elementOnly'"
 * @generated
 */
public interface Assertion extends Axiom {
} // Assertion
