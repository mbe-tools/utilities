/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object All Values From</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectProperty <em>Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectInverseOf <em>Object Inverse Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectIntersectionOf <em>Object Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectUnionOf <em>Object Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectComplementOf <em>Object Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectOneOf <em>Object One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectSomeValuesFrom <em>Object Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectAllValuesFrom <em>Object All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectHasValue <em>Object Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectHasSelf <em>Object Has Self</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectMinCardinality <em>Object Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectMaxCardinality <em>Object Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectExactCardinality <em>Object Exact Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataSomeValuesFrom <em>Data Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataAllValuesFrom <em>Data All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataHasValue <em>Data Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataMinCardinality <em>Data Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataMaxCardinality <em>Data Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataExactCardinality <em>Data Exact Cardinality</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom()
 * @model extendedMetaData="name='ObjectAllValuesFrom' kind='elementOnly'"
 * @generated
 */
public interface ObjectAllValuesFrom extends ClassExpression {
	/**
	 * Returns the value of the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property</em>' containment reference.
	 * @see #setObjectProperty(ObjectProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectProperty getObjectProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectProperty <em>Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Property</em>' containment reference.
	 * @see #getObjectProperty()
	 * @generated
	 */
	void setObjectProperty(ObjectProperty value);

	/**
	 * Returns the value of the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Inverse Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Inverse Of</em>' containment reference.
	 * @see #setObjectInverseOf(ObjectInverseOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectInverseOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectInverseOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectInverseOf getObjectInverseOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectInverseOf <em>Object Inverse Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Inverse Of</em>' containment reference.
	 * @see #getObjectInverseOf()
	 * @generated
	 */
	void setObjectInverseOf(ObjectInverseOf value);

	/**
	 * Returns the value of the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' containment reference.
	 * @see #setClass(org.w3._2002._07.owl.Class)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_Class()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Class' namespace='##targetNamespace'"
	 * @generated
	 */
	org.w3._2002._07.owl.Class getClass_();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getClass_ <em>Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' containment reference.
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(org.w3._2002._07.owl.Class value);

	/**
	 * Returns the value of the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Intersection Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Intersection Of</em>' containment reference.
	 * @see #setObjectIntersectionOf(ObjectIntersectionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectIntersectionOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectIntersectionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectIntersectionOf getObjectIntersectionOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectIntersectionOf <em>Object Intersection Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Intersection Of</em>' containment reference.
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	void setObjectIntersectionOf(ObjectIntersectionOf value);

	/**
	 * Returns the value of the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Union Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Union Of</em>' containment reference.
	 * @see #setObjectUnionOf(ObjectUnionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectUnionOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectUnionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectUnionOf getObjectUnionOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectUnionOf <em>Object Union Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Union Of</em>' containment reference.
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	void setObjectUnionOf(ObjectUnionOf value);

	/**
	 * Returns the value of the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Complement Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Complement Of</em>' containment reference.
	 * @see #setObjectComplementOf(ObjectComplementOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectComplementOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectComplementOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectComplementOf getObjectComplementOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectComplementOf <em>Object Complement Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Complement Of</em>' containment reference.
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	void setObjectComplementOf(ObjectComplementOf value);

	/**
	 * Returns the value of the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object One Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object One Of</em>' containment reference.
	 * @see #setObjectOneOf(ObjectOneOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectOneOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectOneOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectOneOf getObjectOneOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectOneOf <em>Object One Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object One Of</em>' containment reference.
	 * @see #getObjectOneOf()
	 * @generated
	 */
	void setObjectOneOf(ObjectOneOf value);

	/**
	 * Returns the value of the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Some Values From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Some Values From</em>' containment reference.
	 * @see #setObjectSomeValuesFrom(ObjectSomeValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectSomeValuesFrom()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectSomeValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectSomeValuesFrom getObjectSomeValuesFrom();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectSomeValuesFrom <em>Object Some Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Some Values From</em>' containment reference.
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	void setObjectSomeValuesFrom(ObjectSomeValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object All Values From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object All Values From</em>' containment reference.
	 * @see #setObjectAllValuesFrom(ObjectAllValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectAllValuesFrom()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectAllValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectAllValuesFrom getObjectAllValuesFrom();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectAllValuesFrom <em>Object All Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object All Values From</em>' containment reference.
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	void setObjectAllValuesFrom(ObjectAllValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Has Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Has Value</em>' containment reference.
	 * @see #setObjectHasValue(ObjectHasValue)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectHasValue()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectHasValue' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectHasValue getObjectHasValue();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectHasValue <em>Object Has Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Has Value</em>' containment reference.
	 * @see #getObjectHasValue()
	 * @generated
	 */
	void setObjectHasValue(ObjectHasValue value);

	/**
	 * Returns the value of the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Has Self</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Has Self</em>' containment reference.
	 * @see #setObjectHasSelf(ObjectHasSelf)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectHasSelf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectHasSelf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectHasSelf getObjectHasSelf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectHasSelf <em>Object Has Self</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Has Self</em>' containment reference.
	 * @see #getObjectHasSelf()
	 * @generated
	 */
	void setObjectHasSelf(ObjectHasSelf value);

	/**
	 * Returns the value of the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Min Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Min Cardinality</em>' containment reference.
	 * @see #setObjectMinCardinality(ObjectMinCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectMinCardinality()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectMinCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectMinCardinality getObjectMinCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectMinCardinality <em>Object Min Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Min Cardinality</em>' containment reference.
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	void setObjectMinCardinality(ObjectMinCardinality value);

	/**
	 * Returns the value of the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Max Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Max Cardinality</em>' containment reference.
	 * @see #setObjectMaxCardinality(ObjectMaxCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectMaxCardinality()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectMaxCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectMaxCardinality getObjectMaxCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectMaxCardinality <em>Object Max Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Max Cardinality</em>' containment reference.
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	void setObjectMaxCardinality(ObjectMaxCardinality value);

	/**
	 * Returns the value of the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Exact Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Exact Cardinality</em>' containment reference.
	 * @see #setObjectExactCardinality(ObjectExactCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_ObjectExactCardinality()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectExactCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectExactCardinality getObjectExactCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectExactCardinality <em>Object Exact Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Exact Cardinality</em>' containment reference.
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	void setObjectExactCardinality(ObjectExactCardinality value);

	/**
	 * Returns the value of the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Some Values From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Some Values From</em>' containment reference.
	 * @see #setDataSomeValuesFrom(DataSomeValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_DataSomeValuesFrom()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataSomeValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	DataSomeValuesFrom getDataSomeValuesFrom();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataSomeValuesFrom <em>Data Some Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Some Values From</em>' containment reference.
	 * @see #getDataSomeValuesFrom()
	 * @generated
	 */
	void setDataSomeValuesFrom(DataSomeValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data All Values From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data All Values From</em>' containment reference.
	 * @see #setDataAllValuesFrom(DataAllValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_DataAllValuesFrom()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataAllValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	DataAllValuesFrom getDataAllValuesFrom();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataAllValuesFrom <em>Data All Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data All Values From</em>' containment reference.
	 * @see #getDataAllValuesFrom()
	 * @generated
	 */
	void setDataAllValuesFrom(DataAllValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Has Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Has Value</em>' containment reference.
	 * @see #setDataHasValue(DataHasValue)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_DataHasValue()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataHasValue' namespace='##targetNamespace'"
	 * @generated
	 */
	DataHasValue getDataHasValue();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataHasValue <em>Data Has Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Has Value</em>' containment reference.
	 * @see #getDataHasValue()
	 * @generated
	 */
	void setDataHasValue(DataHasValue value);

	/**
	 * Returns the value of the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Min Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Min Cardinality</em>' containment reference.
	 * @see #setDataMinCardinality(DataMinCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_DataMinCardinality()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataMinCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	DataMinCardinality getDataMinCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataMinCardinality <em>Data Min Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Min Cardinality</em>' containment reference.
	 * @see #getDataMinCardinality()
	 * @generated
	 */
	void setDataMinCardinality(DataMinCardinality value);

	/**
	 * Returns the value of the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Max Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Max Cardinality</em>' containment reference.
	 * @see #setDataMaxCardinality(DataMaxCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_DataMaxCardinality()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataMaxCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	DataMaxCardinality getDataMaxCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataMaxCardinality <em>Data Max Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Max Cardinality</em>' containment reference.
	 * @see #getDataMaxCardinality()
	 * @generated
	 */
	void setDataMaxCardinality(DataMaxCardinality value);

	/**
	 * Returns the value of the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Exact Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Exact Cardinality</em>' containment reference.
	 * @see #setDataExactCardinality(DataExactCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectAllValuesFrom_DataExactCardinality()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataExactCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	DataExactCardinality getDataExactCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataExactCardinality <em>Data Exact Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Exact Cardinality</em>' containment reference.
	 * @see #getDataExactCardinality()
	 * @generated
	 */
	void setDataExactCardinality(DataExactCardinality value);

} // ObjectAllValuesFrom
