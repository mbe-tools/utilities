/**
 */
package org.w3._2002._07.owl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.w3._2002._07.owl.AnonymousIndividual;
import org.w3._2002._07.owl.NamedIndividual;
import org.w3._2002._07.owl.NegativeObjectPropertyAssertion;
import org.w3._2002._07.owl.ObjectInverseOf;
import org.w3._2002._07.owl.ObjectProperty;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Negative Object Property Assertion</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.NegativeObjectPropertyAssertionImpl#getObjectProperty <em>Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.NegativeObjectPropertyAssertionImpl#getObjectInverseOf <em>Object Inverse Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.NegativeObjectPropertyAssertionImpl#getNamedIndividual <em>Named Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.NegativeObjectPropertyAssertionImpl#getAnonymousIndividual <em>Anonymous Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.NegativeObjectPropertyAssertionImpl#getNamedIndividual1 <em>Named Individual1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.NegativeObjectPropertyAssertionImpl#getAnonymousIndividual1 <em>Anonymous Individual1</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NegativeObjectPropertyAssertionImpl extends AssertionImpl implements NegativeObjectPropertyAssertion {
	/**
	 * The cached value of the '{@link #getObjectProperty() <em>Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectProperty()
	 * @generated
	 * @ordered
	 */
	protected ObjectProperty objectProperty;

	/**
	 * The cached value of the '{@link #getObjectInverseOf() <em>Object Inverse Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectInverseOf()
	 * @generated
	 * @ordered
	 */
	protected ObjectInverseOf objectInverseOf;

	/**
	 * The cached value of the '{@link #getNamedIndividual() <em>Named Individual</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamedIndividual()
	 * @generated
	 * @ordered
	 */
	protected NamedIndividual namedIndividual;

	/**
	 * The cached value of the '{@link #getAnonymousIndividual() <em>Anonymous Individual</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousIndividual()
	 * @generated
	 * @ordered
	 */
	protected AnonymousIndividual anonymousIndividual;

	/**
	 * The cached value of the '{@link #getNamedIndividual1() <em>Named Individual1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamedIndividual1()
	 * @generated
	 * @ordered
	 */
	protected NamedIndividual namedIndividual1;

	/**
	 * The cached value of the '{@link #getAnonymousIndividual1() <em>Anonymous Individual1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousIndividual1()
	 * @generated
	 * @ordered
	 */
	protected AnonymousIndividual anonymousIndividual1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NegativeObjectPropertyAssertionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getNegativeObjectPropertyAssertion();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectProperty getObjectProperty() {
		return objectProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectProperty(ObjectProperty newObjectProperty, NotificationChain msgs) {
		ObjectProperty oldObjectProperty = objectProperty;
		objectProperty = newObjectProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY, oldObjectProperty, newObjectProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectProperty(ObjectProperty newObjectProperty) {
		if (newObjectProperty != objectProperty) {
			NotificationChain msgs = null;
			if (objectProperty != null)
				msgs = ((InternalEObject)objectProperty).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY, null, msgs);
			if (newObjectProperty != null)
				msgs = ((InternalEObject)newObjectProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY, null, msgs);
			msgs = basicSetObjectProperty(newObjectProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY, newObjectProperty, newObjectProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectInverseOf getObjectInverseOf() {
		return objectInverseOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectInverseOf(ObjectInverseOf newObjectInverseOf, NotificationChain msgs) {
		ObjectInverseOf oldObjectInverseOf = objectInverseOf;
		objectInverseOf = newObjectInverseOf;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF, oldObjectInverseOf, newObjectInverseOf);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectInverseOf(ObjectInverseOf newObjectInverseOf) {
		if (newObjectInverseOf != objectInverseOf) {
			NotificationChain msgs = null;
			if (objectInverseOf != null)
				msgs = ((InternalEObject)objectInverseOf).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF, null, msgs);
			if (newObjectInverseOf != null)
				msgs = ((InternalEObject)newObjectInverseOf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF, null, msgs);
			msgs = basicSetObjectInverseOf(newObjectInverseOf, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF, newObjectInverseOf, newObjectInverseOf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedIndividual getNamedIndividual() {
		return namedIndividual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamedIndividual(NamedIndividual newNamedIndividual, NotificationChain msgs) {
		NamedIndividual oldNamedIndividual = namedIndividual;
		namedIndividual = newNamedIndividual;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL, oldNamedIndividual, newNamedIndividual);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamedIndividual(NamedIndividual newNamedIndividual) {
		if (newNamedIndividual != namedIndividual) {
			NotificationChain msgs = null;
			if (namedIndividual != null)
				msgs = ((InternalEObject)namedIndividual).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL, null, msgs);
			if (newNamedIndividual != null)
				msgs = ((InternalEObject)newNamedIndividual).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL, null, msgs);
			msgs = basicSetNamedIndividual(newNamedIndividual, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL, newNamedIndividual, newNamedIndividual));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousIndividual getAnonymousIndividual() {
		return anonymousIndividual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousIndividual(AnonymousIndividual newAnonymousIndividual, NotificationChain msgs) {
		AnonymousIndividual oldAnonymousIndividual = anonymousIndividual;
		anonymousIndividual = newAnonymousIndividual;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL, oldAnonymousIndividual, newAnonymousIndividual);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousIndividual(AnonymousIndividual newAnonymousIndividual) {
		if (newAnonymousIndividual != anonymousIndividual) {
			NotificationChain msgs = null;
			if (anonymousIndividual != null)
				msgs = ((InternalEObject)anonymousIndividual).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL, null, msgs);
			if (newAnonymousIndividual != null)
				msgs = ((InternalEObject)newAnonymousIndividual).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL, null, msgs);
			msgs = basicSetAnonymousIndividual(newAnonymousIndividual, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL, newAnonymousIndividual, newAnonymousIndividual));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedIndividual getNamedIndividual1() {
		return namedIndividual1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamedIndividual1(NamedIndividual newNamedIndividual1, NotificationChain msgs) {
		NamedIndividual oldNamedIndividual1 = namedIndividual1;
		namedIndividual1 = newNamedIndividual1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL1, oldNamedIndividual1, newNamedIndividual1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamedIndividual1(NamedIndividual newNamedIndividual1) {
		if (newNamedIndividual1 != namedIndividual1) {
			NotificationChain msgs = null;
			if (namedIndividual1 != null)
				msgs = ((InternalEObject)namedIndividual1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL1, null, msgs);
			if (newNamedIndividual1 != null)
				msgs = ((InternalEObject)newNamedIndividual1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL1, null, msgs);
			msgs = basicSetNamedIndividual1(newNamedIndividual1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL1, newNamedIndividual1, newNamedIndividual1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousIndividual getAnonymousIndividual1() {
		return anonymousIndividual1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousIndividual1(AnonymousIndividual newAnonymousIndividual1, NotificationChain msgs) {
		AnonymousIndividual oldAnonymousIndividual1 = anonymousIndividual1;
		anonymousIndividual1 = newAnonymousIndividual1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1, oldAnonymousIndividual1, newAnonymousIndividual1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousIndividual1(AnonymousIndividual newAnonymousIndividual1) {
		if (newAnonymousIndividual1 != anonymousIndividual1) {
			NotificationChain msgs = null;
			if (anonymousIndividual1 != null)
				msgs = ((InternalEObject)anonymousIndividual1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1, null, msgs);
			if (newAnonymousIndividual1 != null)
				msgs = ((InternalEObject)newAnonymousIndividual1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1, null, msgs);
			msgs = basicSetAnonymousIndividual1(newAnonymousIndividual1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1, newAnonymousIndividual1, newAnonymousIndividual1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY:
				return basicSetObjectProperty(null, msgs);
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF:
				return basicSetObjectInverseOf(null, msgs);
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL:
				return basicSetNamedIndividual(null, msgs);
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL:
				return basicSetAnonymousIndividual(null, msgs);
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL1:
				return basicSetNamedIndividual1(null, msgs);
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				return basicSetAnonymousIndividual1(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY:
				return getObjectProperty();
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF:
				return getObjectInverseOf();
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL:
				return getNamedIndividual();
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL:
				return getAnonymousIndividual();
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL1:
				return getNamedIndividual1();
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				return getAnonymousIndividual1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY:
				setObjectProperty((ObjectProperty)newValue);
				return;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF:
				setObjectInverseOf((ObjectInverseOf)newValue);
				return;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL:
				setNamedIndividual((NamedIndividual)newValue);
				return;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL:
				setAnonymousIndividual((AnonymousIndividual)newValue);
				return;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL1:
				setNamedIndividual1((NamedIndividual)newValue);
				return;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				setAnonymousIndividual1((AnonymousIndividual)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY:
				setObjectProperty((ObjectProperty)null);
				return;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF:
				setObjectInverseOf((ObjectInverseOf)null);
				return;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL:
				setNamedIndividual((NamedIndividual)null);
				return;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL:
				setAnonymousIndividual((AnonymousIndividual)null);
				return;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL1:
				setNamedIndividual1((NamedIndividual)null);
				return;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				setAnonymousIndividual1((AnonymousIndividual)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY:
				return objectProperty != null;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF:
				return objectInverseOf != null;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL:
				return namedIndividual != null;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL:
				return anonymousIndividual != null;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL1:
				return namedIndividual1 != null;
			case OwlPackage.NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1:
				return anonymousIndividual1 != null;
		}
		return super.eIsSet(featureID);
	}

} //NegativeObjectPropertyAssertionImpl
