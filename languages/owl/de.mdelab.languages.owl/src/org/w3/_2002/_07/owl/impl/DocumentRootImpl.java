/**
 */
package org.w3._2002._07.owl.impl;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.w3._2002._07.owl.AbbreviatedIRI1;
import org.w3._2002._07.owl.Annotation;
import org.w3._2002._07.owl.AnnotationAssertion;
import org.w3._2002._07.owl.AnnotationProperty;
import org.w3._2002._07.owl.AnnotationPropertyDomain;
import org.w3._2002._07.owl.AnnotationPropertyRange;
import org.w3._2002._07.owl.AnonymousIndividual;
import org.w3._2002._07.owl.AsymmetricObjectProperty;
import org.w3._2002._07.owl.ClassAssertion;
import org.w3._2002._07.owl.DataAllValuesFrom;
import org.w3._2002._07.owl.DataComplementOf;
import org.w3._2002._07.owl.DataExactCardinality;
import org.w3._2002._07.owl.DataHasValue;
import org.w3._2002._07.owl.DataIntersectionOf;
import org.w3._2002._07.owl.DataMaxCardinality;
import org.w3._2002._07.owl.DataMinCardinality;
import org.w3._2002._07.owl.DataOneOf;
import org.w3._2002._07.owl.DataProperty;
import org.w3._2002._07.owl.DataPropertyAssertion;
import org.w3._2002._07.owl.DataPropertyDomain;
import org.w3._2002._07.owl.DataPropertyRange;
import org.w3._2002._07.owl.DataSomeValuesFrom;
import org.w3._2002._07.owl.DataUnionOf;
import org.w3._2002._07.owl.Datatype;
import org.w3._2002._07.owl.DatatypeDefinition;
import org.w3._2002._07.owl.DatatypeRestriction;
import org.w3._2002._07.owl.Declaration;
import org.w3._2002._07.owl.DifferentIndividuals;
import org.w3._2002._07.owl.DisjointClasses;
import org.w3._2002._07.owl.DisjointDataProperties;
import org.w3._2002._07.owl.DisjointObjectProperties;
import org.w3._2002._07.owl.DisjointUnion;
import org.w3._2002._07.owl.DocumentRoot;
import org.w3._2002._07.owl.EquivalentClasses;
import org.w3._2002._07.owl.EquivalentDataProperties;
import org.w3._2002._07.owl.EquivalentObjectProperties;
import org.w3._2002._07.owl.FunctionalDataProperty;
import org.w3._2002._07.owl.FunctionalObjectProperty;
import org.w3._2002._07.owl.HasKey;
import org.w3._2002._07.owl.IRI;
import org.w3._2002._07.owl.Import;
import org.w3._2002._07.owl.InverseFunctionalObjectProperty;
import org.w3._2002._07.owl.InverseObjectProperties;
import org.w3._2002._07.owl.IrreflexiveObjectProperty;
import org.w3._2002._07.owl.Literal;
import org.w3._2002._07.owl.NamedIndividual;
import org.w3._2002._07.owl.NegativeDataPropertyAssertion;
import org.w3._2002._07.owl.NegativeObjectPropertyAssertion;
import org.w3._2002._07.owl.ObjectAllValuesFrom;
import org.w3._2002._07.owl.ObjectComplementOf;
import org.w3._2002._07.owl.ObjectExactCardinality;
import org.w3._2002._07.owl.ObjectHasSelf;
import org.w3._2002._07.owl.ObjectHasValue;
import org.w3._2002._07.owl.ObjectIntersectionOf;
import org.w3._2002._07.owl.ObjectInverseOf;
import org.w3._2002._07.owl.ObjectMaxCardinality;
import org.w3._2002._07.owl.ObjectMinCardinality;
import org.w3._2002._07.owl.ObjectOneOf;
import org.w3._2002._07.owl.ObjectProperty;
import org.w3._2002._07.owl.ObjectPropertyAssertion;
import org.w3._2002._07.owl.ObjectPropertyDomain;
import org.w3._2002._07.owl.ObjectPropertyRange;
import org.w3._2002._07.owl.ObjectSomeValuesFrom;
import org.w3._2002._07.owl.ObjectUnionOf;
import org.w3._2002._07.owl.Ontology;
import org.w3._2002._07.owl.OwlPackage;
import org.w3._2002._07.owl.Prefix;
import org.w3._2002._07.owl.ReflexiveObjectProperty;
import org.w3._2002._07.owl.SameIndividual;
import org.w3._2002._07.owl.SubAnnotationPropertyOf;
import org.w3._2002._07.owl.SubClassOf;
import org.w3._2002._07.owl.SubDataPropertyOf;
import org.w3._2002._07.owl.SubObjectPropertyOf;
import org.w3._2002._07.owl.SymmetricObjectProperty;
import org.w3._2002._07.owl.TransitiveObjectProperty;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getAbbreviatedIRI <em>Abbreviated IRI</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getAnnotation <em>Annotation</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getAnnotationAssertion <em>Annotation Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getAnnotationProperty <em>Annotation Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getAnnotationPropertyDomain <em>Annotation Property Domain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getAnnotationPropertyRange <em>Annotation Property Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getAnonymousIndividual <em>Anonymous Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getAsymmetricObjectProperty <em>Asymmetric Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getClassAssertion <em>Class Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataAllValuesFrom <em>Data All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataComplementOf <em>Data Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataExactCardinality <em>Data Exact Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataHasValue <em>Data Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataIntersectionOf <em>Data Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataMaxCardinality <em>Data Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataMinCardinality <em>Data Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataOneOf <em>Data One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataProperty <em>Data Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataPropertyAssertion <em>Data Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataPropertyDomain <em>Data Property Domain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataPropertyRange <em>Data Property Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataSomeValuesFrom <em>Data Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDatatypeDefinition <em>Datatype Definition</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDatatypeRestriction <em>Datatype Restriction</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDataUnionOf <em>Data Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDeclaration <em>Declaration</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDifferentIndividuals <em>Different Individuals</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDisjointClasses <em>Disjoint Classes</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDisjointDataProperties <em>Disjoint Data Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDisjointObjectProperties <em>Disjoint Object Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getDisjointUnion <em>Disjoint Union</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getEquivalentClasses <em>Equivalent Classes</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getEquivalentDataProperties <em>Equivalent Data Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getEquivalentObjectProperties <em>Equivalent Object Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getFunctionalDataProperty <em>Functional Data Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getFunctionalObjectProperty <em>Functional Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getHasKey <em>Has Key</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getImport <em>Import</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getInverseFunctionalObjectProperty <em>Inverse Functional Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getInverseObjectProperties <em>Inverse Object Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getIRI <em>IRI</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getIrreflexiveObjectProperty <em>Irreflexive Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getLiteral <em>Literal</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getNamedIndividual <em>Named Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getNegativeDataPropertyAssertion <em>Negative Data Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getNegativeObjectPropertyAssertion <em>Negative Object Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectAllValuesFrom <em>Object All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectComplementOf <em>Object Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectExactCardinality <em>Object Exact Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectHasSelf <em>Object Has Self</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectHasValue <em>Object Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectIntersectionOf <em>Object Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectInverseOf <em>Object Inverse Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectMaxCardinality <em>Object Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectMinCardinality <em>Object Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectOneOf <em>Object One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectProperty <em>Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectPropertyAssertion <em>Object Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectPropertyDomain <em>Object Property Domain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectPropertyRange <em>Object Property Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectSomeValuesFrom <em>Object Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getObjectUnionOf <em>Object Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getOntology <em>Ontology</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getReflexiveObjectProperty <em>Reflexive Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getSameIndividual <em>Same Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getSubAnnotationPropertyOf <em>Sub Annotation Property Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getSubClassOf <em>Sub Class Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getSubDataPropertyOf <em>Sub Data Property Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getSubObjectPropertyOf <em>Sub Object Property Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getSymmetricObjectProperty <em>Symmetric Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DocumentRootImpl#getTransitiveObjectProperty <em>Transitive Object Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DocumentRootImpl extends MinimalEObjectImpl.Container implements DocumentRoot {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * The cached value of the '{@link #getXMLNSPrefixMap() <em>XMLNS Prefix Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXMLNSPrefixMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> xMLNSPrefixMap;

	/**
	 * The cached value of the '{@link #getXSISchemaLocation() <em>XSI Schema Location</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXSISchemaLocation()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> xSISchemaLocation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DocumentRootImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getDocumentRoot();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, OwlPackage.DOCUMENT_ROOT__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getXMLNSPrefixMap() {
		if (xMLNSPrefixMap == null) {
			xMLNSPrefixMap = new EcoreEMap<String,String>(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, OwlPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		}
		return xMLNSPrefixMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getXSISchemaLocation() {
		if (xSISchemaLocation == null) {
			xSISchemaLocation = new EcoreEMap<String,String>(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, OwlPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		}
		return xSISchemaLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbbreviatedIRI1 getAbbreviatedIRI() {
		return (AbbreviatedIRI1)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_AbbreviatedIRI(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbbreviatedIRI(AbbreviatedIRI1 newAbbreviatedIRI, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_AbbreviatedIRI(), newAbbreviatedIRI, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbbreviatedIRI(AbbreviatedIRI1 newAbbreviatedIRI) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_AbbreviatedIRI(), newAbbreviatedIRI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Annotation getAnnotation() {
		return (Annotation)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_Annotation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotation(Annotation newAnnotation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_Annotation(), newAnnotation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotation(Annotation newAnnotation) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_Annotation(), newAnnotation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationAssertion getAnnotationAssertion() {
		return (AnnotationAssertion)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationAssertion(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotationAssertion(AnnotationAssertion newAnnotationAssertion, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationAssertion(), newAnnotationAssertion, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotationAssertion(AnnotationAssertion newAnnotationAssertion) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationAssertion(), newAnnotationAssertion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationProperty getAnnotationProperty() {
		return (AnnotationProperty)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationProperty(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotationProperty(AnnotationProperty newAnnotationProperty, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationProperty(), newAnnotationProperty, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotationProperty(AnnotationProperty newAnnotationProperty) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationProperty(), newAnnotationProperty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationPropertyDomain getAnnotationPropertyDomain() {
		return (AnnotationPropertyDomain)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationPropertyDomain(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotationPropertyDomain(AnnotationPropertyDomain newAnnotationPropertyDomain, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationPropertyDomain(), newAnnotationPropertyDomain, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotationPropertyDomain(AnnotationPropertyDomain newAnnotationPropertyDomain) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationPropertyDomain(), newAnnotationPropertyDomain);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationPropertyRange getAnnotationPropertyRange() {
		return (AnnotationPropertyRange)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationPropertyRange(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotationPropertyRange(AnnotationPropertyRange newAnnotationPropertyRange, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationPropertyRange(), newAnnotationPropertyRange, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotationPropertyRange(AnnotationPropertyRange newAnnotationPropertyRange) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationPropertyRange(), newAnnotationPropertyRange);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousIndividual getAnonymousIndividual() {
		return (AnonymousIndividual)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_AnonymousIndividual(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousIndividual(AnonymousIndividual newAnonymousIndividual, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_AnonymousIndividual(), newAnonymousIndividual, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousIndividual(AnonymousIndividual newAnonymousIndividual) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_AnonymousIndividual(), newAnonymousIndividual);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsymmetricObjectProperty getAsymmetricObjectProperty() {
		return (AsymmetricObjectProperty)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_AsymmetricObjectProperty(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAsymmetricObjectProperty(AsymmetricObjectProperty newAsymmetricObjectProperty, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_AsymmetricObjectProperty(), newAsymmetricObjectProperty, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsymmetricObjectProperty(AsymmetricObjectProperty newAsymmetricObjectProperty) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_AsymmetricObjectProperty(), newAsymmetricObjectProperty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.w3._2002._07.owl.Class getClass_() {
		return (org.w3._2002._07.owl.Class)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_Class(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClass(org.w3._2002._07.owl.Class newClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_Class(), newClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClass(org.w3._2002._07.owl.Class newClass) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_Class(), newClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassAssertion getClassAssertion() {
		return (ClassAssertion)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ClassAssertion(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassAssertion(ClassAssertion newClassAssertion, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ClassAssertion(), newClassAssertion, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassAssertion(ClassAssertion newClassAssertion) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ClassAssertion(), newClassAssertion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAllValuesFrom getDataAllValuesFrom() {
		return (DataAllValuesFrom)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataAllValuesFrom(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataAllValuesFrom(DataAllValuesFrom newDataAllValuesFrom, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataAllValuesFrom(), newDataAllValuesFrom, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataAllValuesFrom(DataAllValuesFrom newDataAllValuesFrom) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataAllValuesFrom(), newDataAllValuesFrom);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataComplementOf getDataComplementOf() {
		return (DataComplementOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataComplementOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataComplementOf(DataComplementOf newDataComplementOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataComplementOf(), newDataComplementOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataComplementOf(DataComplementOf newDataComplementOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataComplementOf(), newDataComplementOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataExactCardinality getDataExactCardinality() {
		return (DataExactCardinality)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataExactCardinality(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataExactCardinality(DataExactCardinality newDataExactCardinality, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataExactCardinality(), newDataExactCardinality, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataExactCardinality(DataExactCardinality newDataExactCardinality) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataExactCardinality(), newDataExactCardinality);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataHasValue getDataHasValue() {
		return (DataHasValue)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataHasValue(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataHasValue(DataHasValue newDataHasValue, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataHasValue(), newDataHasValue, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataHasValue(DataHasValue newDataHasValue) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataHasValue(), newDataHasValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataIntersectionOf getDataIntersectionOf() {
		return (DataIntersectionOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataIntersectionOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataIntersectionOf(DataIntersectionOf newDataIntersectionOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataIntersectionOf(), newDataIntersectionOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataIntersectionOf(DataIntersectionOf newDataIntersectionOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataIntersectionOf(), newDataIntersectionOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataMaxCardinality getDataMaxCardinality() {
		return (DataMaxCardinality)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataMaxCardinality(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataMaxCardinality(DataMaxCardinality newDataMaxCardinality, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataMaxCardinality(), newDataMaxCardinality, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataMaxCardinality(DataMaxCardinality newDataMaxCardinality) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataMaxCardinality(), newDataMaxCardinality);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataMinCardinality getDataMinCardinality() {
		return (DataMinCardinality)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataMinCardinality(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataMinCardinality(DataMinCardinality newDataMinCardinality, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataMinCardinality(), newDataMinCardinality, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataMinCardinality(DataMinCardinality newDataMinCardinality) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataMinCardinality(), newDataMinCardinality);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataOneOf getDataOneOf() {
		return (DataOneOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataOneOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataOneOf(DataOneOf newDataOneOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataOneOf(), newDataOneOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataOneOf(DataOneOf newDataOneOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataOneOf(), newDataOneOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataProperty getDataProperty() {
		return (DataProperty)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataProperty(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataProperty(DataProperty newDataProperty, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataProperty(), newDataProperty, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataProperty(DataProperty newDataProperty) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataProperty(), newDataProperty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPropertyAssertion getDataPropertyAssertion() {
		return (DataPropertyAssertion)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyAssertion(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataPropertyAssertion(DataPropertyAssertion newDataPropertyAssertion, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyAssertion(), newDataPropertyAssertion, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataPropertyAssertion(DataPropertyAssertion newDataPropertyAssertion) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyAssertion(), newDataPropertyAssertion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPropertyDomain getDataPropertyDomain() {
		return (DataPropertyDomain)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyDomain(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataPropertyDomain(DataPropertyDomain newDataPropertyDomain, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyDomain(), newDataPropertyDomain, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataPropertyDomain(DataPropertyDomain newDataPropertyDomain) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyDomain(), newDataPropertyDomain);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPropertyRange getDataPropertyRange() {
		return (DataPropertyRange)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyRange(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataPropertyRange(DataPropertyRange newDataPropertyRange, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyRange(), newDataPropertyRange, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataPropertyRange(DataPropertyRange newDataPropertyRange) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyRange(), newDataPropertyRange);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSomeValuesFrom getDataSomeValuesFrom() {
		return (DataSomeValuesFrom)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataSomeValuesFrom(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataSomeValuesFrom(DataSomeValuesFrom newDataSomeValuesFrom, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataSomeValuesFrom(), newDataSomeValuesFrom, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataSomeValuesFrom(DataSomeValuesFrom newDataSomeValuesFrom) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataSomeValuesFrom(), newDataSomeValuesFrom);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Datatype getDatatype() {
		return (Datatype)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_Datatype(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDatatype(Datatype newDatatype, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_Datatype(), newDatatype, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDatatype(Datatype newDatatype) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_Datatype(), newDatatype);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatatypeDefinition getDatatypeDefinition() {
		return (DatatypeDefinition)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DatatypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDatatypeDefinition(DatatypeDefinition newDatatypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DatatypeDefinition(), newDatatypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDatatypeDefinition(DatatypeDefinition newDatatypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DatatypeDefinition(), newDatatypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatatypeRestriction getDatatypeRestriction() {
		return (DatatypeRestriction)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DatatypeRestriction(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDatatypeRestriction(DatatypeRestriction newDatatypeRestriction, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DatatypeRestriction(), newDatatypeRestriction, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDatatypeRestriction(DatatypeRestriction newDatatypeRestriction) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DatatypeRestriction(), newDatatypeRestriction);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataUnionOf getDataUnionOf() {
		return (DataUnionOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DataUnionOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataUnionOf(DataUnionOf newDataUnionOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DataUnionOf(), newDataUnionOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataUnionOf(DataUnionOf newDataUnionOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DataUnionOf(), newDataUnionOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Declaration getDeclaration() {
		return (Declaration)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_Declaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDeclaration(Declaration newDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_Declaration(), newDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeclaration(Declaration newDeclaration) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_Declaration(), newDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DifferentIndividuals getDifferentIndividuals() {
		return (DifferentIndividuals)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DifferentIndividuals(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDifferentIndividuals(DifferentIndividuals newDifferentIndividuals, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DifferentIndividuals(), newDifferentIndividuals, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDifferentIndividuals(DifferentIndividuals newDifferentIndividuals) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DifferentIndividuals(), newDifferentIndividuals);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisjointClasses getDisjointClasses() {
		return (DisjointClasses)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DisjointClasses(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDisjointClasses(DisjointClasses newDisjointClasses, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DisjointClasses(), newDisjointClasses, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisjointClasses(DisjointClasses newDisjointClasses) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DisjointClasses(), newDisjointClasses);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisjointDataProperties getDisjointDataProperties() {
		return (DisjointDataProperties)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DisjointDataProperties(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDisjointDataProperties(DisjointDataProperties newDisjointDataProperties, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DisjointDataProperties(), newDisjointDataProperties, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisjointDataProperties(DisjointDataProperties newDisjointDataProperties) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DisjointDataProperties(), newDisjointDataProperties);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisjointObjectProperties getDisjointObjectProperties() {
		return (DisjointObjectProperties)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DisjointObjectProperties(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDisjointObjectProperties(DisjointObjectProperties newDisjointObjectProperties, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DisjointObjectProperties(), newDisjointObjectProperties, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisjointObjectProperties(DisjointObjectProperties newDisjointObjectProperties) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DisjointObjectProperties(), newDisjointObjectProperties);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisjointUnion getDisjointUnion() {
		return (DisjointUnion)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_DisjointUnion(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDisjointUnion(DisjointUnion newDisjointUnion, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_DisjointUnion(), newDisjointUnion, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisjointUnion(DisjointUnion newDisjointUnion) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_DisjointUnion(), newDisjointUnion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquivalentClasses getEquivalentClasses() {
		return (EquivalentClasses)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentClasses(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquivalentClasses(EquivalentClasses newEquivalentClasses, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentClasses(), newEquivalentClasses, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquivalentClasses(EquivalentClasses newEquivalentClasses) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentClasses(), newEquivalentClasses);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquivalentDataProperties getEquivalentDataProperties() {
		return (EquivalentDataProperties)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentDataProperties(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquivalentDataProperties(EquivalentDataProperties newEquivalentDataProperties, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentDataProperties(), newEquivalentDataProperties, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquivalentDataProperties(EquivalentDataProperties newEquivalentDataProperties) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentDataProperties(), newEquivalentDataProperties);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquivalentObjectProperties getEquivalentObjectProperties() {
		return (EquivalentObjectProperties)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentObjectProperties(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquivalentObjectProperties(EquivalentObjectProperties newEquivalentObjectProperties, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentObjectProperties(), newEquivalentObjectProperties, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquivalentObjectProperties(EquivalentObjectProperties newEquivalentObjectProperties) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentObjectProperties(), newEquivalentObjectProperties);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalDataProperty getFunctionalDataProperty() {
		return (FunctionalDataProperty)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_FunctionalDataProperty(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionalDataProperty(FunctionalDataProperty newFunctionalDataProperty, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_FunctionalDataProperty(), newFunctionalDataProperty, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionalDataProperty(FunctionalDataProperty newFunctionalDataProperty) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_FunctionalDataProperty(), newFunctionalDataProperty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalObjectProperty getFunctionalObjectProperty() {
		return (FunctionalObjectProperty)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_FunctionalObjectProperty(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionalObjectProperty(FunctionalObjectProperty newFunctionalObjectProperty, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_FunctionalObjectProperty(), newFunctionalObjectProperty, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionalObjectProperty(FunctionalObjectProperty newFunctionalObjectProperty) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_FunctionalObjectProperty(), newFunctionalObjectProperty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasKey getHasKey() {
		return (HasKey)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_HasKey(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasKey(HasKey newHasKey, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_HasKey(), newHasKey, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasKey(HasKey newHasKey) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_HasKey(), newHasKey);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Import getImport() {
		return (Import)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_Import(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImport(Import newImport, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_Import(), newImport, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImport(Import newImport) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_Import(), newImport);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InverseFunctionalObjectProperty getInverseFunctionalObjectProperty() {
		return (InverseFunctionalObjectProperty)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_InverseFunctionalObjectProperty(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInverseFunctionalObjectProperty(InverseFunctionalObjectProperty newInverseFunctionalObjectProperty, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_InverseFunctionalObjectProperty(), newInverseFunctionalObjectProperty, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInverseFunctionalObjectProperty(InverseFunctionalObjectProperty newInverseFunctionalObjectProperty) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_InverseFunctionalObjectProperty(), newInverseFunctionalObjectProperty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InverseObjectProperties getInverseObjectProperties() {
		return (InverseObjectProperties)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_InverseObjectProperties(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInverseObjectProperties(InverseObjectProperties newInverseObjectProperties, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_InverseObjectProperties(), newInverseObjectProperties, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInverseObjectProperties(InverseObjectProperties newInverseObjectProperties) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_InverseObjectProperties(), newInverseObjectProperties);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRI getIRI() {
		return (IRI)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_IRI(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIRI(IRI newIRI, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_IRI(), newIRI, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIRI(IRI newIRI) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_IRI(), newIRI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IrreflexiveObjectProperty getIrreflexiveObjectProperty() {
		return (IrreflexiveObjectProperty)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_IrreflexiveObjectProperty(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIrreflexiveObjectProperty(IrreflexiveObjectProperty newIrreflexiveObjectProperty, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_IrreflexiveObjectProperty(), newIrreflexiveObjectProperty, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIrreflexiveObjectProperty(IrreflexiveObjectProperty newIrreflexiveObjectProperty) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_IrreflexiveObjectProperty(), newIrreflexiveObjectProperty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Literal getLiteral() {
		return (Literal)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_Literal(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLiteral(Literal newLiteral, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_Literal(), newLiteral, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLiteral(Literal newLiteral) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_Literal(), newLiteral);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedIndividual getNamedIndividual() {
		return (NamedIndividual)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_NamedIndividual(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamedIndividual(NamedIndividual newNamedIndividual, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_NamedIndividual(), newNamedIndividual, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamedIndividual(NamedIndividual newNamedIndividual) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_NamedIndividual(), newNamedIndividual);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NegativeDataPropertyAssertion getNegativeDataPropertyAssertion() {
		return (NegativeDataPropertyAssertion)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_NegativeDataPropertyAssertion(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNegativeDataPropertyAssertion(NegativeDataPropertyAssertion newNegativeDataPropertyAssertion, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_NegativeDataPropertyAssertion(), newNegativeDataPropertyAssertion, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNegativeDataPropertyAssertion(NegativeDataPropertyAssertion newNegativeDataPropertyAssertion) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_NegativeDataPropertyAssertion(), newNegativeDataPropertyAssertion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NegativeObjectPropertyAssertion getNegativeObjectPropertyAssertion() {
		return (NegativeObjectPropertyAssertion)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_NegativeObjectPropertyAssertion(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNegativeObjectPropertyAssertion(NegativeObjectPropertyAssertion newNegativeObjectPropertyAssertion, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_NegativeObjectPropertyAssertion(), newNegativeObjectPropertyAssertion, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNegativeObjectPropertyAssertion(NegativeObjectPropertyAssertion newNegativeObjectPropertyAssertion) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_NegativeObjectPropertyAssertion(), newNegativeObjectPropertyAssertion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectAllValuesFrom getObjectAllValuesFrom() {
		return (ObjectAllValuesFrom)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectAllValuesFrom(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectAllValuesFrom(ObjectAllValuesFrom newObjectAllValuesFrom, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectAllValuesFrom(), newObjectAllValuesFrom, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectAllValuesFrom(ObjectAllValuesFrom newObjectAllValuesFrom) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectAllValuesFrom(), newObjectAllValuesFrom);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectComplementOf getObjectComplementOf() {
		return (ObjectComplementOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectComplementOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectComplementOf(ObjectComplementOf newObjectComplementOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectComplementOf(), newObjectComplementOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectComplementOf(ObjectComplementOf newObjectComplementOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectComplementOf(), newObjectComplementOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectExactCardinality getObjectExactCardinality() {
		return (ObjectExactCardinality)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectExactCardinality(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectExactCardinality(ObjectExactCardinality newObjectExactCardinality, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectExactCardinality(), newObjectExactCardinality, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectExactCardinality(ObjectExactCardinality newObjectExactCardinality) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectExactCardinality(), newObjectExactCardinality);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectHasSelf getObjectHasSelf() {
		return (ObjectHasSelf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectHasSelf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectHasSelf(ObjectHasSelf newObjectHasSelf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectHasSelf(), newObjectHasSelf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectHasSelf(ObjectHasSelf newObjectHasSelf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectHasSelf(), newObjectHasSelf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectHasValue getObjectHasValue() {
		return (ObjectHasValue)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectHasValue(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectHasValue(ObjectHasValue newObjectHasValue, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectHasValue(), newObjectHasValue, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectHasValue(ObjectHasValue newObjectHasValue) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectHasValue(), newObjectHasValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectIntersectionOf getObjectIntersectionOf() {
		return (ObjectIntersectionOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectIntersectionOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectIntersectionOf(ObjectIntersectionOf newObjectIntersectionOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectIntersectionOf(), newObjectIntersectionOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectIntersectionOf(ObjectIntersectionOf newObjectIntersectionOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectIntersectionOf(), newObjectIntersectionOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectInverseOf getObjectInverseOf() {
		return (ObjectInverseOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectInverseOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectInverseOf(ObjectInverseOf newObjectInverseOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectInverseOf(), newObjectInverseOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectInverseOf(ObjectInverseOf newObjectInverseOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectInverseOf(), newObjectInverseOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectMaxCardinality getObjectMaxCardinality() {
		return (ObjectMaxCardinality)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectMaxCardinality(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectMaxCardinality(ObjectMaxCardinality newObjectMaxCardinality, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectMaxCardinality(), newObjectMaxCardinality, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectMaxCardinality(ObjectMaxCardinality newObjectMaxCardinality) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectMaxCardinality(), newObjectMaxCardinality);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectMinCardinality getObjectMinCardinality() {
		return (ObjectMinCardinality)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectMinCardinality(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectMinCardinality(ObjectMinCardinality newObjectMinCardinality, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectMinCardinality(), newObjectMinCardinality, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectMinCardinality(ObjectMinCardinality newObjectMinCardinality) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectMinCardinality(), newObjectMinCardinality);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectOneOf getObjectOneOf() {
		return (ObjectOneOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectOneOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectOneOf(ObjectOneOf newObjectOneOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectOneOf(), newObjectOneOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectOneOf(ObjectOneOf newObjectOneOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectOneOf(), newObjectOneOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectProperty getObjectProperty() {
		return (ObjectProperty)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectProperty(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectProperty(ObjectProperty newObjectProperty, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectProperty(), newObjectProperty, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectProperty(ObjectProperty newObjectProperty) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectProperty(), newObjectProperty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectPropertyAssertion getObjectPropertyAssertion() {
		return (ObjectPropertyAssertion)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyAssertion(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectPropertyAssertion(ObjectPropertyAssertion newObjectPropertyAssertion, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyAssertion(), newObjectPropertyAssertion, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectPropertyAssertion(ObjectPropertyAssertion newObjectPropertyAssertion) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyAssertion(), newObjectPropertyAssertion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectPropertyDomain getObjectPropertyDomain() {
		return (ObjectPropertyDomain)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyDomain(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectPropertyDomain(ObjectPropertyDomain newObjectPropertyDomain, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyDomain(), newObjectPropertyDomain, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectPropertyDomain(ObjectPropertyDomain newObjectPropertyDomain) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyDomain(), newObjectPropertyDomain);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectPropertyRange getObjectPropertyRange() {
		return (ObjectPropertyRange)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyRange(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectPropertyRange(ObjectPropertyRange newObjectPropertyRange, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyRange(), newObjectPropertyRange, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectPropertyRange(ObjectPropertyRange newObjectPropertyRange) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyRange(), newObjectPropertyRange);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectSomeValuesFrom getObjectSomeValuesFrom() {
		return (ObjectSomeValuesFrom)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectSomeValuesFrom(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectSomeValuesFrom(ObjectSomeValuesFrom newObjectSomeValuesFrom, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectSomeValuesFrom(), newObjectSomeValuesFrom, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectSomeValuesFrom(ObjectSomeValuesFrom newObjectSomeValuesFrom) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectSomeValuesFrom(), newObjectSomeValuesFrom);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectUnionOf getObjectUnionOf() {
		return (ObjectUnionOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ObjectUnionOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectUnionOf(ObjectUnionOf newObjectUnionOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ObjectUnionOf(), newObjectUnionOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectUnionOf(ObjectUnionOf newObjectUnionOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ObjectUnionOf(), newObjectUnionOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ontology getOntology() {
		return (Ontology)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_Ontology(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOntology(Ontology newOntology, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_Ontology(), newOntology, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOntology(Ontology newOntology) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_Ontology(), newOntology);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Prefix getPrefix() {
		return (Prefix)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_Prefix(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrefix(Prefix newPrefix, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_Prefix(), newPrefix, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrefix(Prefix newPrefix) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_Prefix(), newPrefix);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReflexiveObjectProperty getReflexiveObjectProperty() {
		return (ReflexiveObjectProperty)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_ReflexiveObjectProperty(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReflexiveObjectProperty(ReflexiveObjectProperty newReflexiveObjectProperty, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_ReflexiveObjectProperty(), newReflexiveObjectProperty, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReflexiveObjectProperty(ReflexiveObjectProperty newReflexiveObjectProperty) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_ReflexiveObjectProperty(), newReflexiveObjectProperty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SameIndividual getSameIndividual() {
		return (SameIndividual)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_SameIndividual(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSameIndividual(SameIndividual newSameIndividual, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_SameIndividual(), newSameIndividual, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSameIndividual(SameIndividual newSameIndividual) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_SameIndividual(), newSameIndividual);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAnnotationPropertyOf getSubAnnotationPropertyOf() {
		return (SubAnnotationPropertyOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_SubAnnotationPropertyOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubAnnotationPropertyOf(SubAnnotationPropertyOf newSubAnnotationPropertyOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_SubAnnotationPropertyOf(), newSubAnnotationPropertyOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubAnnotationPropertyOf(SubAnnotationPropertyOf newSubAnnotationPropertyOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_SubAnnotationPropertyOf(), newSubAnnotationPropertyOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubClassOf getSubClassOf() {
		return (SubClassOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_SubClassOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubClassOf(SubClassOf newSubClassOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_SubClassOf(), newSubClassOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubClassOf(SubClassOf newSubClassOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_SubClassOf(), newSubClassOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubDataPropertyOf getSubDataPropertyOf() {
		return (SubDataPropertyOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_SubDataPropertyOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubDataPropertyOf(SubDataPropertyOf newSubDataPropertyOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_SubDataPropertyOf(), newSubDataPropertyOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubDataPropertyOf(SubDataPropertyOf newSubDataPropertyOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_SubDataPropertyOf(), newSubDataPropertyOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubObjectPropertyOf getSubObjectPropertyOf() {
		return (SubObjectPropertyOf)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_SubObjectPropertyOf(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubObjectPropertyOf(SubObjectPropertyOf newSubObjectPropertyOf, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_SubObjectPropertyOf(), newSubObjectPropertyOf, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubObjectPropertyOf(SubObjectPropertyOf newSubObjectPropertyOf) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_SubObjectPropertyOf(), newSubObjectPropertyOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SymmetricObjectProperty getSymmetricObjectProperty() {
		return (SymmetricObjectProperty)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_SymmetricObjectProperty(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSymmetricObjectProperty(SymmetricObjectProperty newSymmetricObjectProperty, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_SymmetricObjectProperty(), newSymmetricObjectProperty, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymmetricObjectProperty(SymmetricObjectProperty newSymmetricObjectProperty) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_SymmetricObjectProperty(), newSymmetricObjectProperty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitiveObjectProperty getTransitiveObjectProperty() {
		return (TransitiveObjectProperty)getMixed().get(OwlPackage.eINSTANCE.getDocumentRoot_TransitiveObjectProperty(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTransitiveObjectProperty(TransitiveObjectProperty newTransitiveObjectProperty, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OwlPackage.eINSTANCE.getDocumentRoot_TransitiveObjectProperty(), newTransitiveObjectProperty, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransitiveObjectProperty(TransitiveObjectProperty newTransitiveObjectProperty) {
		((FeatureMap.Internal)getMixed()).set(OwlPackage.eINSTANCE.getDocumentRoot_TransitiveObjectProperty(), newTransitiveObjectProperty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.DOCUMENT_ROOT__MIXED:
				return ((InternalEList<?>)getMixed()).basicRemove(otherEnd, msgs);
			case OwlPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return ((InternalEList<?>)getXMLNSPrefixMap()).basicRemove(otherEnd, msgs);
			case OwlPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return ((InternalEList<?>)getXSISchemaLocation()).basicRemove(otherEnd, msgs);
			case OwlPackage.DOCUMENT_ROOT__ABBREVIATED_IRI:
				return basicSetAbbreviatedIRI(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION:
				return basicSetAnnotation(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_ASSERTION:
				return basicSetAnnotationAssertion(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY:
				return basicSetAnnotationProperty(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY_DOMAIN:
				return basicSetAnnotationPropertyDomain(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY_RANGE:
				return basicSetAnnotationPropertyRange(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__ANONYMOUS_INDIVIDUAL:
				return basicSetAnonymousIndividual(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__ASYMMETRIC_OBJECT_PROPERTY:
				return basicSetAsymmetricObjectProperty(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__CLASS:
				return basicSetClass(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__CLASS_ASSERTION:
				return basicSetClassAssertion(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_ALL_VALUES_FROM:
				return basicSetDataAllValuesFrom(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_COMPLEMENT_OF:
				return basicSetDataComplementOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_EXACT_CARDINALITY:
				return basicSetDataExactCardinality(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_HAS_VALUE:
				return basicSetDataHasValue(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_INTERSECTION_OF:
				return basicSetDataIntersectionOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_MAX_CARDINALITY:
				return basicSetDataMaxCardinality(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_MIN_CARDINALITY:
				return basicSetDataMinCardinality(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_ONE_OF:
				return basicSetDataOneOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY:
				return basicSetDataProperty(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_ASSERTION:
				return basicSetDataPropertyAssertion(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_DOMAIN:
				return basicSetDataPropertyDomain(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_RANGE:
				return basicSetDataPropertyRange(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_SOME_VALUES_FROM:
				return basicSetDataSomeValuesFrom(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATATYPE:
				return basicSetDatatype(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATATYPE_DEFINITION:
				return basicSetDatatypeDefinition(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATATYPE_RESTRICTION:
				return basicSetDatatypeRestriction(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DATA_UNION_OF:
				return basicSetDataUnionOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DECLARATION:
				return basicSetDeclaration(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DIFFERENT_INDIVIDUALS:
				return basicSetDifferentIndividuals(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_CLASSES:
				return basicSetDisjointClasses(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_DATA_PROPERTIES:
				return basicSetDisjointDataProperties(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_OBJECT_PROPERTIES:
				return basicSetDisjointObjectProperties(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_UNION:
				return basicSetDisjointUnion(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_CLASSES:
				return basicSetEquivalentClasses(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_DATA_PROPERTIES:
				return basicSetEquivalentDataProperties(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_OBJECT_PROPERTIES:
				return basicSetEquivalentObjectProperties(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__FUNCTIONAL_DATA_PROPERTY:
				return basicSetFunctionalDataProperty(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__FUNCTIONAL_OBJECT_PROPERTY:
				return basicSetFunctionalObjectProperty(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__HAS_KEY:
				return basicSetHasKey(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__IMPORT:
				return basicSetImport(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__INVERSE_FUNCTIONAL_OBJECT_PROPERTY:
				return basicSetInverseFunctionalObjectProperty(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__INVERSE_OBJECT_PROPERTIES:
				return basicSetInverseObjectProperties(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__IRI:
				return basicSetIRI(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__IRREFLEXIVE_OBJECT_PROPERTY:
				return basicSetIrreflexiveObjectProperty(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__LITERAL:
				return basicSetLiteral(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__NAMED_INDIVIDUAL:
				return basicSetNamedIndividual(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__NEGATIVE_DATA_PROPERTY_ASSERTION:
				return basicSetNegativeDataPropertyAssertion(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__NEGATIVE_OBJECT_PROPERTY_ASSERTION:
				return basicSetNegativeObjectPropertyAssertion(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_ALL_VALUES_FROM:
				return basicSetObjectAllValuesFrom(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_COMPLEMENT_OF:
				return basicSetObjectComplementOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_EXACT_CARDINALITY:
				return basicSetObjectExactCardinality(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_HAS_SELF:
				return basicSetObjectHasSelf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_HAS_VALUE:
				return basicSetObjectHasValue(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_INTERSECTION_OF:
				return basicSetObjectIntersectionOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_INVERSE_OF:
				return basicSetObjectInverseOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_MAX_CARDINALITY:
				return basicSetObjectMaxCardinality(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_MIN_CARDINALITY:
				return basicSetObjectMinCardinality(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_ONE_OF:
				return basicSetObjectOneOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY:
				return basicSetObjectProperty(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_ASSERTION:
				return basicSetObjectPropertyAssertion(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_DOMAIN:
				return basicSetObjectPropertyDomain(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_RANGE:
				return basicSetObjectPropertyRange(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_SOME_VALUES_FROM:
				return basicSetObjectSomeValuesFrom(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__OBJECT_UNION_OF:
				return basicSetObjectUnionOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__ONTOLOGY:
				return basicSetOntology(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__PREFIX:
				return basicSetPrefix(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__REFLEXIVE_OBJECT_PROPERTY:
				return basicSetReflexiveObjectProperty(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__SAME_INDIVIDUAL:
				return basicSetSameIndividual(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__SUB_ANNOTATION_PROPERTY_OF:
				return basicSetSubAnnotationPropertyOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__SUB_CLASS_OF:
				return basicSetSubClassOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__SUB_DATA_PROPERTY_OF:
				return basicSetSubDataPropertyOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__SUB_OBJECT_PROPERTY_OF:
				return basicSetSubObjectPropertyOf(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__SYMMETRIC_OBJECT_PROPERTY:
				return basicSetSymmetricObjectProperty(null, msgs);
			case OwlPackage.DOCUMENT_ROOT__TRANSITIVE_OBJECT_PROPERTY:
				return basicSetTransitiveObjectProperty(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.DOCUMENT_ROOT__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case OwlPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				if (coreType) return getXMLNSPrefixMap();
				else return getXMLNSPrefixMap().map();
			case OwlPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				if (coreType) return getXSISchemaLocation();
				else return getXSISchemaLocation().map();
			case OwlPackage.DOCUMENT_ROOT__ABBREVIATED_IRI:
				return getAbbreviatedIRI();
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION:
				return getAnnotation();
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_ASSERTION:
				return getAnnotationAssertion();
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY:
				return getAnnotationProperty();
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY_DOMAIN:
				return getAnnotationPropertyDomain();
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY_RANGE:
				return getAnnotationPropertyRange();
			case OwlPackage.DOCUMENT_ROOT__ANONYMOUS_INDIVIDUAL:
				return getAnonymousIndividual();
			case OwlPackage.DOCUMENT_ROOT__ASYMMETRIC_OBJECT_PROPERTY:
				return getAsymmetricObjectProperty();
			case OwlPackage.DOCUMENT_ROOT__CLASS:
				return getClass_();
			case OwlPackage.DOCUMENT_ROOT__CLASS_ASSERTION:
				return getClassAssertion();
			case OwlPackage.DOCUMENT_ROOT__DATA_ALL_VALUES_FROM:
				return getDataAllValuesFrom();
			case OwlPackage.DOCUMENT_ROOT__DATA_COMPLEMENT_OF:
				return getDataComplementOf();
			case OwlPackage.DOCUMENT_ROOT__DATA_EXACT_CARDINALITY:
				return getDataExactCardinality();
			case OwlPackage.DOCUMENT_ROOT__DATA_HAS_VALUE:
				return getDataHasValue();
			case OwlPackage.DOCUMENT_ROOT__DATA_INTERSECTION_OF:
				return getDataIntersectionOf();
			case OwlPackage.DOCUMENT_ROOT__DATA_MAX_CARDINALITY:
				return getDataMaxCardinality();
			case OwlPackage.DOCUMENT_ROOT__DATA_MIN_CARDINALITY:
				return getDataMinCardinality();
			case OwlPackage.DOCUMENT_ROOT__DATA_ONE_OF:
				return getDataOneOf();
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY:
				return getDataProperty();
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_ASSERTION:
				return getDataPropertyAssertion();
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_DOMAIN:
				return getDataPropertyDomain();
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_RANGE:
				return getDataPropertyRange();
			case OwlPackage.DOCUMENT_ROOT__DATA_SOME_VALUES_FROM:
				return getDataSomeValuesFrom();
			case OwlPackage.DOCUMENT_ROOT__DATATYPE:
				return getDatatype();
			case OwlPackage.DOCUMENT_ROOT__DATATYPE_DEFINITION:
				return getDatatypeDefinition();
			case OwlPackage.DOCUMENT_ROOT__DATATYPE_RESTRICTION:
				return getDatatypeRestriction();
			case OwlPackage.DOCUMENT_ROOT__DATA_UNION_OF:
				return getDataUnionOf();
			case OwlPackage.DOCUMENT_ROOT__DECLARATION:
				return getDeclaration();
			case OwlPackage.DOCUMENT_ROOT__DIFFERENT_INDIVIDUALS:
				return getDifferentIndividuals();
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_CLASSES:
				return getDisjointClasses();
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_DATA_PROPERTIES:
				return getDisjointDataProperties();
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_OBJECT_PROPERTIES:
				return getDisjointObjectProperties();
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_UNION:
				return getDisjointUnion();
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_CLASSES:
				return getEquivalentClasses();
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_DATA_PROPERTIES:
				return getEquivalentDataProperties();
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_OBJECT_PROPERTIES:
				return getEquivalentObjectProperties();
			case OwlPackage.DOCUMENT_ROOT__FUNCTIONAL_DATA_PROPERTY:
				return getFunctionalDataProperty();
			case OwlPackage.DOCUMENT_ROOT__FUNCTIONAL_OBJECT_PROPERTY:
				return getFunctionalObjectProperty();
			case OwlPackage.DOCUMENT_ROOT__HAS_KEY:
				return getHasKey();
			case OwlPackage.DOCUMENT_ROOT__IMPORT:
				return getImport();
			case OwlPackage.DOCUMENT_ROOT__INVERSE_FUNCTIONAL_OBJECT_PROPERTY:
				return getInverseFunctionalObjectProperty();
			case OwlPackage.DOCUMENT_ROOT__INVERSE_OBJECT_PROPERTIES:
				return getInverseObjectProperties();
			case OwlPackage.DOCUMENT_ROOT__IRI:
				return getIRI();
			case OwlPackage.DOCUMENT_ROOT__IRREFLEXIVE_OBJECT_PROPERTY:
				return getIrreflexiveObjectProperty();
			case OwlPackage.DOCUMENT_ROOT__LITERAL:
				return getLiteral();
			case OwlPackage.DOCUMENT_ROOT__NAMED_INDIVIDUAL:
				return getNamedIndividual();
			case OwlPackage.DOCUMENT_ROOT__NEGATIVE_DATA_PROPERTY_ASSERTION:
				return getNegativeDataPropertyAssertion();
			case OwlPackage.DOCUMENT_ROOT__NEGATIVE_OBJECT_PROPERTY_ASSERTION:
				return getNegativeObjectPropertyAssertion();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_ALL_VALUES_FROM:
				return getObjectAllValuesFrom();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_COMPLEMENT_OF:
				return getObjectComplementOf();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_EXACT_CARDINALITY:
				return getObjectExactCardinality();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_HAS_SELF:
				return getObjectHasSelf();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_HAS_VALUE:
				return getObjectHasValue();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_INTERSECTION_OF:
				return getObjectIntersectionOf();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_INVERSE_OF:
				return getObjectInverseOf();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_MAX_CARDINALITY:
				return getObjectMaxCardinality();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_MIN_CARDINALITY:
				return getObjectMinCardinality();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_ONE_OF:
				return getObjectOneOf();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY:
				return getObjectProperty();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_ASSERTION:
				return getObjectPropertyAssertion();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_DOMAIN:
				return getObjectPropertyDomain();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_RANGE:
				return getObjectPropertyRange();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_SOME_VALUES_FROM:
				return getObjectSomeValuesFrom();
			case OwlPackage.DOCUMENT_ROOT__OBJECT_UNION_OF:
				return getObjectUnionOf();
			case OwlPackage.DOCUMENT_ROOT__ONTOLOGY:
				return getOntology();
			case OwlPackage.DOCUMENT_ROOT__PREFIX:
				return getPrefix();
			case OwlPackage.DOCUMENT_ROOT__REFLEXIVE_OBJECT_PROPERTY:
				return getReflexiveObjectProperty();
			case OwlPackage.DOCUMENT_ROOT__SAME_INDIVIDUAL:
				return getSameIndividual();
			case OwlPackage.DOCUMENT_ROOT__SUB_ANNOTATION_PROPERTY_OF:
				return getSubAnnotationPropertyOf();
			case OwlPackage.DOCUMENT_ROOT__SUB_CLASS_OF:
				return getSubClassOf();
			case OwlPackage.DOCUMENT_ROOT__SUB_DATA_PROPERTY_OF:
				return getSubDataPropertyOf();
			case OwlPackage.DOCUMENT_ROOT__SUB_OBJECT_PROPERTY_OF:
				return getSubObjectPropertyOf();
			case OwlPackage.DOCUMENT_ROOT__SYMMETRIC_OBJECT_PROPERTY:
				return getSymmetricObjectProperty();
			case OwlPackage.DOCUMENT_ROOT__TRANSITIVE_OBJECT_PROPERTY:
				return getTransitiveObjectProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.DOCUMENT_ROOT__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				((EStructuralFeature.Setting)getXMLNSPrefixMap()).set(newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				((EStructuralFeature.Setting)getXSISchemaLocation()).set(newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__ABBREVIATED_IRI:
				setAbbreviatedIRI((AbbreviatedIRI1)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION:
				setAnnotation((Annotation)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_ASSERTION:
				setAnnotationAssertion((AnnotationAssertion)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY:
				setAnnotationProperty((AnnotationProperty)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY_DOMAIN:
				setAnnotationPropertyDomain((AnnotationPropertyDomain)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY_RANGE:
				setAnnotationPropertyRange((AnnotationPropertyRange)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__ANONYMOUS_INDIVIDUAL:
				setAnonymousIndividual((AnonymousIndividual)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__ASYMMETRIC_OBJECT_PROPERTY:
				setAsymmetricObjectProperty((AsymmetricObjectProperty)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__CLASS:
				setClass((org.w3._2002._07.owl.Class)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__CLASS_ASSERTION:
				setClassAssertion((ClassAssertion)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_ALL_VALUES_FROM:
				setDataAllValuesFrom((DataAllValuesFrom)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_COMPLEMENT_OF:
				setDataComplementOf((DataComplementOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_EXACT_CARDINALITY:
				setDataExactCardinality((DataExactCardinality)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_HAS_VALUE:
				setDataHasValue((DataHasValue)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_INTERSECTION_OF:
				setDataIntersectionOf((DataIntersectionOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_MAX_CARDINALITY:
				setDataMaxCardinality((DataMaxCardinality)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_MIN_CARDINALITY:
				setDataMinCardinality((DataMinCardinality)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_ONE_OF:
				setDataOneOf((DataOneOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY:
				setDataProperty((DataProperty)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_ASSERTION:
				setDataPropertyAssertion((DataPropertyAssertion)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_DOMAIN:
				setDataPropertyDomain((DataPropertyDomain)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_RANGE:
				setDataPropertyRange((DataPropertyRange)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_SOME_VALUES_FROM:
				setDataSomeValuesFrom((DataSomeValuesFrom)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATATYPE:
				setDatatype((Datatype)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATATYPE_DEFINITION:
				setDatatypeDefinition((DatatypeDefinition)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATATYPE_RESTRICTION:
				setDatatypeRestriction((DatatypeRestriction)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_UNION_OF:
				setDataUnionOf((DataUnionOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DECLARATION:
				setDeclaration((Declaration)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DIFFERENT_INDIVIDUALS:
				setDifferentIndividuals((DifferentIndividuals)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_CLASSES:
				setDisjointClasses((DisjointClasses)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_DATA_PROPERTIES:
				setDisjointDataProperties((DisjointDataProperties)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_OBJECT_PROPERTIES:
				setDisjointObjectProperties((DisjointObjectProperties)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_UNION:
				setDisjointUnion((DisjointUnion)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_CLASSES:
				setEquivalentClasses((EquivalentClasses)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_DATA_PROPERTIES:
				setEquivalentDataProperties((EquivalentDataProperties)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_OBJECT_PROPERTIES:
				setEquivalentObjectProperties((EquivalentObjectProperties)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__FUNCTIONAL_DATA_PROPERTY:
				setFunctionalDataProperty((FunctionalDataProperty)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__FUNCTIONAL_OBJECT_PROPERTY:
				setFunctionalObjectProperty((FunctionalObjectProperty)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__HAS_KEY:
				setHasKey((HasKey)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__IMPORT:
				setImport((Import)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__INVERSE_FUNCTIONAL_OBJECT_PROPERTY:
				setInverseFunctionalObjectProperty((InverseFunctionalObjectProperty)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__INVERSE_OBJECT_PROPERTIES:
				setInverseObjectProperties((InverseObjectProperties)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__IRI:
				setIRI((IRI)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__IRREFLEXIVE_OBJECT_PROPERTY:
				setIrreflexiveObjectProperty((IrreflexiveObjectProperty)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__LITERAL:
				setLiteral((Literal)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__NAMED_INDIVIDUAL:
				setNamedIndividual((NamedIndividual)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__NEGATIVE_DATA_PROPERTY_ASSERTION:
				setNegativeDataPropertyAssertion((NegativeDataPropertyAssertion)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__NEGATIVE_OBJECT_PROPERTY_ASSERTION:
				setNegativeObjectPropertyAssertion((NegativeObjectPropertyAssertion)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_ALL_VALUES_FROM:
				setObjectAllValuesFrom((ObjectAllValuesFrom)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_COMPLEMENT_OF:
				setObjectComplementOf((ObjectComplementOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_EXACT_CARDINALITY:
				setObjectExactCardinality((ObjectExactCardinality)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_HAS_SELF:
				setObjectHasSelf((ObjectHasSelf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_HAS_VALUE:
				setObjectHasValue((ObjectHasValue)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_INTERSECTION_OF:
				setObjectIntersectionOf((ObjectIntersectionOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_INVERSE_OF:
				setObjectInverseOf((ObjectInverseOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_MAX_CARDINALITY:
				setObjectMaxCardinality((ObjectMaxCardinality)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_MIN_CARDINALITY:
				setObjectMinCardinality((ObjectMinCardinality)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_ONE_OF:
				setObjectOneOf((ObjectOneOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY:
				setObjectProperty((ObjectProperty)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_ASSERTION:
				setObjectPropertyAssertion((ObjectPropertyAssertion)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_DOMAIN:
				setObjectPropertyDomain((ObjectPropertyDomain)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_RANGE:
				setObjectPropertyRange((ObjectPropertyRange)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_SOME_VALUES_FROM:
				setObjectSomeValuesFrom((ObjectSomeValuesFrom)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_UNION_OF:
				setObjectUnionOf((ObjectUnionOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__ONTOLOGY:
				setOntology((Ontology)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__PREFIX:
				setPrefix((Prefix)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__REFLEXIVE_OBJECT_PROPERTY:
				setReflexiveObjectProperty((ReflexiveObjectProperty)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__SAME_INDIVIDUAL:
				setSameIndividual((SameIndividual)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__SUB_ANNOTATION_PROPERTY_OF:
				setSubAnnotationPropertyOf((SubAnnotationPropertyOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__SUB_CLASS_OF:
				setSubClassOf((SubClassOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__SUB_DATA_PROPERTY_OF:
				setSubDataPropertyOf((SubDataPropertyOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__SUB_OBJECT_PROPERTY_OF:
				setSubObjectPropertyOf((SubObjectPropertyOf)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__SYMMETRIC_OBJECT_PROPERTY:
				setSymmetricObjectProperty((SymmetricObjectProperty)newValue);
				return;
			case OwlPackage.DOCUMENT_ROOT__TRANSITIVE_OBJECT_PROPERTY:
				setTransitiveObjectProperty((TransitiveObjectProperty)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.DOCUMENT_ROOT__MIXED:
				getMixed().clear();
				return;
			case OwlPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				getXMLNSPrefixMap().clear();
				return;
			case OwlPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				getXSISchemaLocation().clear();
				return;
			case OwlPackage.DOCUMENT_ROOT__ABBREVIATED_IRI:
				setAbbreviatedIRI((AbbreviatedIRI1)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION:
				setAnnotation((Annotation)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_ASSERTION:
				setAnnotationAssertion((AnnotationAssertion)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY:
				setAnnotationProperty((AnnotationProperty)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY_DOMAIN:
				setAnnotationPropertyDomain((AnnotationPropertyDomain)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY_RANGE:
				setAnnotationPropertyRange((AnnotationPropertyRange)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__ANONYMOUS_INDIVIDUAL:
				setAnonymousIndividual((AnonymousIndividual)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__ASYMMETRIC_OBJECT_PROPERTY:
				setAsymmetricObjectProperty((AsymmetricObjectProperty)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__CLASS:
				setClass((org.w3._2002._07.owl.Class)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__CLASS_ASSERTION:
				setClassAssertion((ClassAssertion)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_ALL_VALUES_FROM:
				setDataAllValuesFrom((DataAllValuesFrom)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_COMPLEMENT_OF:
				setDataComplementOf((DataComplementOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_EXACT_CARDINALITY:
				setDataExactCardinality((DataExactCardinality)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_HAS_VALUE:
				setDataHasValue((DataHasValue)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_INTERSECTION_OF:
				setDataIntersectionOf((DataIntersectionOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_MAX_CARDINALITY:
				setDataMaxCardinality((DataMaxCardinality)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_MIN_CARDINALITY:
				setDataMinCardinality((DataMinCardinality)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_ONE_OF:
				setDataOneOf((DataOneOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY:
				setDataProperty((DataProperty)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_ASSERTION:
				setDataPropertyAssertion((DataPropertyAssertion)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_DOMAIN:
				setDataPropertyDomain((DataPropertyDomain)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_RANGE:
				setDataPropertyRange((DataPropertyRange)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_SOME_VALUES_FROM:
				setDataSomeValuesFrom((DataSomeValuesFrom)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATATYPE:
				setDatatype((Datatype)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATATYPE_DEFINITION:
				setDatatypeDefinition((DatatypeDefinition)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATATYPE_RESTRICTION:
				setDatatypeRestriction((DatatypeRestriction)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DATA_UNION_OF:
				setDataUnionOf((DataUnionOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DECLARATION:
				setDeclaration((Declaration)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DIFFERENT_INDIVIDUALS:
				setDifferentIndividuals((DifferentIndividuals)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_CLASSES:
				setDisjointClasses((DisjointClasses)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_DATA_PROPERTIES:
				setDisjointDataProperties((DisjointDataProperties)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_OBJECT_PROPERTIES:
				setDisjointObjectProperties((DisjointObjectProperties)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_UNION:
				setDisjointUnion((DisjointUnion)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_CLASSES:
				setEquivalentClasses((EquivalentClasses)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_DATA_PROPERTIES:
				setEquivalentDataProperties((EquivalentDataProperties)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_OBJECT_PROPERTIES:
				setEquivalentObjectProperties((EquivalentObjectProperties)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__FUNCTIONAL_DATA_PROPERTY:
				setFunctionalDataProperty((FunctionalDataProperty)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__FUNCTIONAL_OBJECT_PROPERTY:
				setFunctionalObjectProperty((FunctionalObjectProperty)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__HAS_KEY:
				setHasKey((HasKey)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__IMPORT:
				setImport((Import)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__INVERSE_FUNCTIONAL_OBJECT_PROPERTY:
				setInverseFunctionalObjectProperty((InverseFunctionalObjectProperty)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__INVERSE_OBJECT_PROPERTIES:
				setInverseObjectProperties((InverseObjectProperties)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__IRI:
				setIRI((IRI)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__IRREFLEXIVE_OBJECT_PROPERTY:
				setIrreflexiveObjectProperty((IrreflexiveObjectProperty)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__LITERAL:
				setLiteral((Literal)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__NAMED_INDIVIDUAL:
				setNamedIndividual((NamedIndividual)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__NEGATIVE_DATA_PROPERTY_ASSERTION:
				setNegativeDataPropertyAssertion((NegativeDataPropertyAssertion)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__NEGATIVE_OBJECT_PROPERTY_ASSERTION:
				setNegativeObjectPropertyAssertion((NegativeObjectPropertyAssertion)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_ALL_VALUES_FROM:
				setObjectAllValuesFrom((ObjectAllValuesFrom)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_COMPLEMENT_OF:
				setObjectComplementOf((ObjectComplementOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_EXACT_CARDINALITY:
				setObjectExactCardinality((ObjectExactCardinality)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_HAS_SELF:
				setObjectHasSelf((ObjectHasSelf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_HAS_VALUE:
				setObjectHasValue((ObjectHasValue)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_INTERSECTION_OF:
				setObjectIntersectionOf((ObjectIntersectionOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_INVERSE_OF:
				setObjectInverseOf((ObjectInverseOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_MAX_CARDINALITY:
				setObjectMaxCardinality((ObjectMaxCardinality)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_MIN_CARDINALITY:
				setObjectMinCardinality((ObjectMinCardinality)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_ONE_OF:
				setObjectOneOf((ObjectOneOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY:
				setObjectProperty((ObjectProperty)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_ASSERTION:
				setObjectPropertyAssertion((ObjectPropertyAssertion)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_DOMAIN:
				setObjectPropertyDomain((ObjectPropertyDomain)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_RANGE:
				setObjectPropertyRange((ObjectPropertyRange)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_SOME_VALUES_FROM:
				setObjectSomeValuesFrom((ObjectSomeValuesFrom)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_UNION_OF:
				setObjectUnionOf((ObjectUnionOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__ONTOLOGY:
				setOntology((Ontology)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__PREFIX:
				setPrefix((Prefix)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__REFLEXIVE_OBJECT_PROPERTY:
				setReflexiveObjectProperty((ReflexiveObjectProperty)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__SAME_INDIVIDUAL:
				setSameIndividual((SameIndividual)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__SUB_ANNOTATION_PROPERTY_OF:
				setSubAnnotationPropertyOf((SubAnnotationPropertyOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__SUB_CLASS_OF:
				setSubClassOf((SubClassOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__SUB_DATA_PROPERTY_OF:
				setSubDataPropertyOf((SubDataPropertyOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__SUB_OBJECT_PROPERTY_OF:
				setSubObjectPropertyOf((SubObjectPropertyOf)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__SYMMETRIC_OBJECT_PROPERTY:
				setSymmetricObjectProperty((SymmetricObjectProperty)null);
				return;
			case OwlPackage.DOCUMENT_ROOT__TRANSITIVE_OBJECT_PROPERTY:
				setTransitiveObjectProperty((TransitiveObjectProperty)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.DOCUMENT_ROOT__MIXED:
				return mixed != null && !mixed.isEmpty();
			case OwlPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return xMLNSPrefixMap != null && !xMLNSPrefixMap.isEmpty();
			case OwlPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return xSISchemaLocation != null && !xSISchemaLocation.isEmpty();
			case OwlPackage.DOCUMENT_ROOT__ABBREVIATED_IRI:
				return getAbbreviatedIRI() != null;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION:
				return getAnnotation() != null;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_ASSERTION:
				return getAnnotationAssertion() != null;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY:
				return getAnnotationProperty() != null;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY_DOMAIN:
				return getAnnotationPropertyDomain() != null;
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY_RANGE:
				return getAnnotationPropertyRange() != null;
			case OwlPackage.DOCUMENT_ROOT__ANONYMOUS_INDIVIDUAL:
				return getAnonymousIndividual() != null;
			case OwlPackage.DOCUMENT_ROOT__ASYMMETRIC_OBJECT_PROPERTY:
				return getAsymmetricObjectProperty() != null;
			case OwlPackage.DOCUMENT_ROOT__CLASS:
				return getClass_() != null;
			case OwlPackage.DOCUMENT_ROOT__CLASS_ASSERTION:
				return getClassAssertion() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_ALL_VALUES_FROM:
				return getDataAllValuesFrom() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_COMPLEMENT_OF:
				return getDataComplementOf() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_EXACT_CARDINALITY:
				return getDataExactCardinality() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_HAS_VALUE:
				return getDataHasValue() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_INTERSECTION_OF:
				return getDataIntersectionOf() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_MAX_CARDINALITY:
				return getDataMaxCardinality() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_MIN_CARDINALITY:
				return getDataMinCardinality() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_ONE_OF:
				return getDataOneOf() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY:
				return getDataProperty() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_ASSERTION:
				return getDataPropertyAssertion() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_DOMAIN:
				return getDataPropertyDomain() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_RANGE:
				return getDataPropertyRange() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_SOME_VALUES_FROM:
				return getDataSomeValuesFrom() != null;
			case OwlPackage.DOCUMENT_ROOT__DATATYPE:
				return getDatatype() != null;
			case OwlPackage.DOCUMENT_ROOT__DATATYPE_DEFINITION:
				return getDatatypeDefinition() != null;
			case OwlPackage.DOCUMENT_ROOT__DATATYPE_RESTRICTION:
				return getDatatypeRestriction() != null;
			case OwlPackage.DOCUMENT_ROOT__DATA_UNION_OF:
				return getDataUnionOf() != null;
			case OwlPackage.DOCUMENT_ROOT__DECLARATION:
				return getDeclaration() != null;
			case OwlPackage.DOCUMENT_ROOT__DIFFERENT_INDIVIDUALS:
				return getDifferentIndividuals() != null;
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_CLASSES:
				return getDisjointClasses() != null;
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_DATA_PROPERTIES:
				return getDisjointDataProperties() != null;
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_OBJECT_PROPERTIES:
				return getDisjointObjectProperties() != null;
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_UNION:
				return getDisjointUnion() != null;
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_CLASSES:
				return getEquivalentClasses() != null;
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_DATA_PROPERTIES:
				return getEquivalentDataProperties() != null;
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_OBJECT_PROPERTIES:
				return getEquivalentObjectProperties() != null;
			case OwlPackage.DOCUMENT_ROOT__FUNCTIONAL_DATA_PROPERTY:
				return getFunctionalDataProperty() != null;
			case OwlPackage.DOCUMENT_ROOT__FUNCTIONAL_OBJECT_PROPERTY:
				return getFunctionalObjectProperty() != null;
			case OwlPackage.DOCUMENT_ROOT__HAS_KEY:
				return getHasKey() != null;
			case OwlPackage.DOCUMENT_ROOT__IMPORT:
				return getImport() != null;
			case OwlPackage.DOCUMENT_ROOT__INVERSE_FUNCTIONAL_OBJECT_PROPERTY:
				return getInverseFunctionalObjectProperty() != null;
			case OwlPackage.DOCUMENT_ROOT__INVERSE_OBJECT_PROPERTIES:
				return getInverseObjectProperties() != null;
			case OwlPackage.DOCUMENT_ROOT__IRI:
				return getIRI() != null;
			case OwlPackage.DOCUMENT_ROOT__IRREFLEXIVE_OBJECT_PROPERTY:
				return getIrreflexiveObjectProperty() != null;
			case OwlPackage.DOCUMENT_ROOT__LITERAL:
				return getLiteral() != null;
			case OwlPackage.DOCUMENT_ROOT__NAMED_INDIVIDUAL:
				return getNamedIndividual() != null;
			case OwlPackage.DOCUMENT_ROOT__NEGATIVE_DATA_PROPERTY_ASSERTION:
				return getNegativeDataPropertyAssertion() != null;
			case OwlPackage.DOCUMENT_ROOT__NEGATIVE_OBJECT_PROPERTY_ASSERTION:
				return getNegativeObjectPropertyAssertion() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_ALL_VALUES_FROM:
				return getObjectAllValuesFrom() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_COMPLEMENT_OF:
				return getObjectComplementOf() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_EXACT_CARDINALITY:
				return getObjectExactCardinality() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_HAS_SELF:
				return getObjectHasSelf() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_HAS_VALUE:
				return getObjectHasValue() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_INTERSECTION_OF:
				return getObjectIntersectionOf() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_INVERSE_OF:
				return getObjectInverseOf() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_MAX_CARDINALITY:
				return getObjectMaxCardinality() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_MIN_CARDINALITY:
				return getObjectMinCardinality() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_ONE_OF:
				return getObjectOneOf() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY:
				return getObjectProperty() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_ASSERTION:
				return getObjectPropertyAssertion() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_DOMAIN:
				return getObjectPropertyDomain() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_RANGE:
				return getObjectPropertyRange() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_SOME_VALUES_FROM:
				return getObjectSomeValuesFrom() != null;
			case OwlPackage.DOCUMENT_ROOT__OBJECT_UNION_OF:
				return getObjectUnionOf() != null;
			case OwlPackage.DOCUMENT_ROOT__ONTOLOGY:
				return getOntology() != null;
			case OwlPackage.DOCUMENT_ROOT__PREFIX:
				return getPrefix() != null;
			case OwlPackage.DOCUMENT_ROOT__REFLEXIVE_OBJECT_PROPERTY:
				return getReflexiveObjectProperty() != null;
			case OwlPackage.DOCUMENT_ROOT__SAME_INDIVIDUAL:
				return getSameIndividual() != null;
			case OwlPackage.DOCUMENT_ROOT__SUB_ANNOTATION_PROPERTY_OF:
				return getSubAnnotationPropertyOf() != null;
			case OwlPackage.DOCUMENT_ROOT__SUB_CLASS_OF:
				return getSubClassOf() != null;
			case OwlPackage.DOCUMENT_ROOT__SUB_DATA_PROPERTY_OF:
				return getSubDataPropertyOf() != null;
			case OwlPackage.DOCUMENT_ROOT__SUB_OBJECT_PROPERTY_OF:
				return getSubObjectPropertyOf() != null;
			case OwlPackage.DOCUMENT_ROOT__SYMMETRIC_OBJECT_PROPERTY:
				return getSymmetricObjectProperty() != null;
			case OwlPackage.DOCUMENT_ROOT__TRANSITIVE_OBJECT_PROPERTY:
				return getTransitiveObjectProperty() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //DocumentRootImpl
