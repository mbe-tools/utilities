/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * 
 *    <div xmlns="http://www.w3.org/1999/xhtml">
 *     <h1>About the XML namespace</h1>
 * 
 *     <div class="bodytext">
 *      <p>
 *       This schema document describes the XML namespace, in a form
 *       suitable for import by other schema documents.
 *      </p>
 *      <p>
 *       See <a href="http://www.w3.org/XML/1998/namespace.html">
 *       http://www.w3.org/XML/1998/namespace.html</a> and
 *       <a href="http://www.w3.org/TR/REC-xml">
 *       http://www.w3.org/TR/REC-xml</a> for information 
 *       about this namespace.
 *      </p>
 *      <p>
 *       Note that local names in this namespace are intended to be
 *       defined only by the World Wide Web Consortium or its subgroups.
 *       The names currently defined in this namespace are listed below.
 *       They should not be used with conflicting semantics by any Working
 *       Group, specification, or document instance.
 *      </p>
 *      <p>   
 *       See further below in this document for more information about <a href="#usage">how to refer to this schema document from your own
 *       XSD schema documents</a> and about <a href="#nsversioning">the
 *       namespace-versioning policy governing this schema document</a>.
 *      </p>
 *     </div>
 *    </div>
 *   
 * 
 *    <div xmlns="http://www.w3.org/1999/xhtml">
 *    
 *     <h3>Father (in any context at all)</h3> 
 * 
 *     <div class="bodytext">
 *      <p>
 *       denotes Jon Bosak, the chair of 
 *       the original XML Working Group.  This name is reserved by 
 *       the following decision of the W3C XML Plenary and 
 *       XML Coordination groups:
 *      </p>
 *      <blockquote>
 *        <p>
 * 	In appreciation for his vision, leadership and
 * 	dedication the W3C XML Plenary on this 10th day of
 * 	February, 2000, reserves for Jon Bosak in perpetuity
 * 	the XML name "xml:Father".
 *        </p>
 *      </blockquote>
 *     </div>
 *    </div>
 *   
 * 
 *    <div id="usage" xml:id="usage" xmlns="http://www.w3.org/1999/xhtml">
 *     <h2>
 *       <a name="usage">About this schema document</a>
 *     </h2>
 * 
 *     <div class="bodytext">
 *      <p>
 *       This schema defines attributes and an attribute group suitable
 *       for use by schemas wishing to allow <code>xml:base</code>,
 *       <code>xml:lang</code>, <code>xml:space</code> or
 *       <code>xml:id</code> attributes on elements they define.
 *      </p>
 *      <p>
 *       To enable this, such a schema must import this schema for
 *       the XML namespace, e.g. as follows:
 *      </p>
 *      <pre>
 *           &lt;schema . . .&gt;
 *            . . .
 *            &lt;import namespace="http://www.w3.org/XML/1998/namespace"
 *                       schemaLocation="http://www.w3.org/2001/xml.xsd"/&gt;
 *      </pre>
 *      <p>
 *       or
 *      </p>
 *      <pre>
 *            &lt;import namespace="http://www.w3.org/XML/1998/namespace"
 *                       schemaLocation="http://www.w3.org/2009/01/xml.xsd"/&gt;
 *      </pre>
 *      <p>
 *       Subsequently, qualified reference to any of the attributes or the
 *       group defined below will have the desired effect, e.g.
 *      </p>
 *      <pre>
 *           &lt;type . . .&gt;
 *            . . .
 *            &lt;attributeGroup ref="xml:specialAttrs"/&gt;
 *      </pre>
 *      <p>
 *       will define a type which will schema-validate an instance element
 *       with any of those attributes.
 *      </p>
 *     </div>
 *    </div>
 *   
 * 
 *    <div id="nsversioning" xml:id="nsversioning" xmlns="http://www.w3.org/1999/xhtml">
 *     <h2>
 *       <a name="nsversioning">Versioning policy for this schema document</a>
 *     </h2>
 *     <div class="bodytext">
 *      <p>
 *       In keeping with the XML Schema WG's standard versioning
 *       policy, this schema document will persist at
 *       <a href="http://www.w3.org/2009/01/xml.xsd">
 *        http://www.w3.org/2009/01/xml.xsd</a>.
 *      </p>
 *      <p>
 *       At the date of issue it can also be found at
 *       <a href="http://www.w3.org/2001/xml.xsd">
 *        http://www.w3.org/2001/xml.xsd</a>.
 *      </p>
 *      <p>
 *       The schema document at that URI may however change in the future,
 *       in order to remain compatible with the latest version of XML
 *       Schema itself, or with the XML namespace itself.  In other words,
 *       if the XML Schema or XML namespaces change, the version of this
 *       document at <a href="http://www.w3.org/2001/xml.xsd">
 *        http://www.w3.org/2001/xml.xsd 
 *       </a> 
 *       will change accordingly; the version at 
 *       <a href="http://www.w3.org/2009/01/xml.xsd">
 *        http://www.w3.org/2009/01/xml.xsd 
 *       </a> 
 *       will not change.
 *      </p>
 *      <p>
 *       Previous dated (and unchanging) versions of this schema 
 *       document are at:
 *      </p>
 *      <ul>
 *       <li>
 *           <a href="http://www.w3.org/2009/01/xml.xsd">
 * 	http://www.w3.org/2009/01/xml.xsd</a>
 *         </li>
 *       <li>
 *           <a href="http://www.w3.org/2007/08/xml.xsd">
 * 	http://www.w3.org/2007/08/xml.xsd</a>
 *         </li>
 *       <li>
 *           <a href="http://www.w3.org/2004/10/xml.xsd">
 * 	http://www.w3.org/2004/10/xml.xsd</a>
 *         </li>
 *       <li>
 *           <a href="http://www.w3.org/2001/03/xml.xsd">
 * 	http://www.w3.org/2001/03/xml.xsd</a>
 *         </li>
 *      </ul>
 *     </div>
 *    </div>
 *   
 * <!-- end-model-doc -->
 * @see org.w3._2002._07.owl.OwlFactory
 * @model kind="package"
 * @generated
 */
public interface OwlPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "owl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.w3.org/2002/07/owl#";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "owl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OwlPackage eINSTANCE = org.w3._2002._07.owl.impl.OwlPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.AbbreviatedIRI1Impl <em>Abbreviated IRI1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.AbbreviatedIRI1Impl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getAbbreviatedIRI1()
	 * @generated
	 */
	int ABBREVIATED_IRI1 = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABBREVIATED_IRI1__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABBREVIATED_IRI1__BASE = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABBREVIATED_IRI1__ID = 2;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABBREVIATED_IRI1__LANG = 3;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABBREVIATED_IRI1__SPACE = 4;

	/**
	 * The number of structural features of the '<em>Abbreviated IRI1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABBREVIATED_IRI1_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Abbreviated IRI1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABBREVIATED_IRI1_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.AnnotationImpl <em>Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.AnnotationImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getAnnotation()
	 * @generated
	 */
	int ANNOTATION = 1;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__ANNOTATION = 0;

	/**
	 * The feature id for the '<em><b>Annotation Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__ANNOTATION_PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__IRI = 2;

	/**
	 * The feature id for the '<em><b>Abbreviated IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__ABBREVIATED_IRI = 3;

	/**
	 * The feature id for the '<em><b>Anonymous Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__ANONYMOUS_INDIVIDUAL = 4;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__LITERAL = 5;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__BASE = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__ID = 7;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__LANG = 8;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__SPACE = 9;

	/**
	 * The number of structural features of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_FEATURE_COUNT = 10;

	/**
	 * The number of operations of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.AxiomImpl <em>Axiom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.AxiomImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getAxiom()
	 * @generated
	 */
	int AXIOM = 10;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM__ANNOTATION = 0;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM__BASE = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM__ID = 2;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM__LANG = 3;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM__SPACE = 4;

	/**
	 * The number of structural features of the '<em>Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIOM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.AnnotationAxiomImpl <em>Annotation Axiom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.AnnotationAxiomImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getAnnotationAxiom()
	 * @generated
	 */
	int ANNOTATION_AXIOM = 3;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_AXIOM__ANNOTATION = AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_AXIOM__BASE = AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_AXIOM__ID = AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_AXIOM__LANG = AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_AXIOM__SPACE = AXIOM__SPACE;

	/**
	 * The number of structural features of the '<em>Annotation Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_AXIOM_FEATURE_COUNT = AXIOM_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Annotation Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_AXIOM_OPERATION_COUNT = AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.AnnotationAssertionImpl <em>Annotation Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.AnnotationAssertionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getAnnotationAssertion()
	 * @generated
	 */
	int ANNOTATION_ASSERTION = 2;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__ANNOTATION = ANNOTATION_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__BASE = ANNOTATION_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__ID = ANNOTATION_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__LANG = ANNOTATION_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__SPACE = ANNOTATION_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Annotation Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__ANNOTATION_PROPERTY = ANNOTATION_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__IRI = ANNOTATION_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Abbreviated IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__ABBREVIATED_IRI = ANNOTATION_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Anonymous Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL = ANNOTATION_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>IRI1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__IRI1 = ANNOTATION_AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Abbreviated IRI1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__ABBREVIATED_IRI1 = ANNOTATION_AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Anonymous Individual1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL1 = ANNOTATION_AXIOM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION__LITERAL = ANNOTATION_AXIOM_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Annotation Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION_FEATURE_COUNT = ANNOTATION_AXIOM_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Annotation Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_ASSERTION_OPERATION_COUNT = ANNOTATION_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.AnnotationPropertyImpl <em>Annotation Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.AnnotationPropertyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getAnnotationProperty()
	 * @generated
	 */
	int ANNOTATION_PROPERTY = 4;

	/**
	 * The feature id for the '<em><b>Abbreviated IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY__ABBREVIATED_IRI = 0;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY__BASE = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY__ID = 2;

	/**
	 * The feature id for the '<em><b>IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY__IRI = 3;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY__LANG = 4;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY__SPACE = 5;

	/**
	 * The number of structural features of the '<em>Annotation Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Annotation Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.AnnotationPropertyDomainImpl <em>Annotation Property Domain</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.AnnotationPropertyDomainImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getAnnotationPropertyDomain()
	 * @generated
	 */
	int ANNOTATION_PROPERTY_DOMAIN = 5;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_DOMAIN__ANNOTATION = ANNOTATION_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_DOMAIN__BASE = ANNOTATION_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_DOMAIN__ID = ANNOTATION_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_DOMAIN__LANG = ANNOTATION_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_DOMAIN__SPACE = ANNOTATION_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Annotation Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_DOMAIN__ANNOTATION_PROPERTY = ANNOTATION_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_DOMAIN__IRI = ANNOTATION_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Abbreviated IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_DOMAIN__ABBREVIATED_IRI = ANNOTATION_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Annotation Property Domain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_DOMAIN_FEATURE_COUNT = ANNOTATION_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Annotation Property Domain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_DOMAIN_OPERATION_COUNT = ANNOTATION_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.AnnotationPropertyRangeImpl <em>Annotation Property Range</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.AnnotationPropertyRangeImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getAnnotationPropertyRange()
	 * @generated
	 */
	int ANNOTATION_PROPERTY_RANGE = 6;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_RANGE__ANNOTATION = ANNOTATION_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_RANGE__BASE = ANNOTATION_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_RANGE__ID = ANNOTATION_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_RANGE__LANG = ANNOTATION_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_RANGE__SPACE = ANNOTATION_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Annotation Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_RANGE__ANNOTATION_PROPERTY = ANNOTATION_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_RANGE__IRI = ANNOTATION_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Abbreviated IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_RANGE__ABBREVIATED_IRI = ANNOTATION_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Annotation Property Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_RANGE_FEATURE_COUNT = ANNOTATION_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Annotation Property Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_PROPERTY_RANGE_OPERATION_COUNT = ANNOTATION_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.IndividualImpl <em>Individual</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.IndividualImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getIndividual()
	 * @generated
	 */
	int INDIVIDUAL = 50;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIVIDUAL__BASE = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIVIDUAL__ID = 1;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIVIDUAL__LANG = 2;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIVIDUAL__SPACE = 3;

	/**
	 * The number of structural features of the '<em>Individual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIVIDUAL_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Individual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIVIDUAL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.AnonymousIndividualImpl <em>Anonymous Individual</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.AnonymousIndividualImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getAnonymousIndividual()
	 * @generated
	 */
	int ANONYMOUS_INDIVIDUAL = 7;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANONYMOUS_INDIVIDUAL__BASE = INDIVIDUAL__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANONYMOUS_INDIVIDUAL__ID = INDIVIDUAL__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANONYMOUS_INDIVIDUAL__LANG = INDIVIDUAL__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANONYMOUS_INDIVIDUAL__SPACE = INDIVIDUAL__SPACE;

	/**
	 * The feature id for the '<em><b>Node ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANONYMOUS_INDIVIDUAL__NODE_ID = INDIVIDUAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Anonymous Individual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANONYMOUS_INDIVIDUAL_FEATURE_COUNT = INDIVIDUAL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Anonymous Individual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANONYMOUS_INDIVIDUAL_OPERATION_COUNT = INDIVIDUAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.AssertionImpl <em>Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.AssertionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getAssertion()
	 * @generated
	 */
	int ASSERTION = 8;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__ANNOTATION = AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__BASE = AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__ID = AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__LANG = AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__SPACE = AXIOM__SPACE;

	/**
	 * The number of structural features of the '<em>Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_FEATURE_COUNT = AXIOM_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_OPERATION_COUNT = AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectPropertyAxiomImpl <em>Object Property Axiom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectPropertyAxiomImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectPropertyAxiom()
	 * @generated
	 */
	int OBJECT_PROPERTY_AXIOM = 71;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_AXIOM__ANNOTATION = AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_AXIOM__BASE = AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_AXIOM__ID = AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_AXIOM__LANG = AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_AXIOM__SPACE = AXIOM__SPACE;

	/**
	 * The number of structural features of the '<em>Object Property Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_AXIOM_FEATURE_COUNT = AXIOM_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Object Property Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_AXIOM_OPERATION_COUNT = AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.AsymmetricObjectPropertyImpl <em>Asymmetric Object Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.AsymmetricObjectPropertyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getAsymmetricObjectProperty()
	 * @generated
	 */
	int ASYMMETRIC_OBJECT_PROPERTY = 9;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYMMETRIC_OBJECT_PROPERTY__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYMMETRIC_OBJECT_PROPERTY__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYMMETRIC_OBJECT_PROPERTY__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYMMETRIC_OBJECT_PROPERTY__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYMMETRIC_OBJECT_PROPERTY__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYMMETRIC_OBJECT_PROPERTY__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYMMETRIC_OBJECT_PROPERTY__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Asymmetric Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYMMETRIC_OBJECT_PROPERTY_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Asymmetric Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYMMETRIC_OBJECT_PROPERTY_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ClassExpressionImpl <em>Class Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ClassExpressionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getClassExpression()
	 * @generated
	 */
	int CLASS_EXPRESSION = 14;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_EXPRESSION__BASE = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_EXPRESSION__ID = 1;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_EXPRESSION__LANG = 2;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_EXPRESSION__SPACE = 3;

	/**
	 * The number of structural features of the '<em>Class Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_EXPRESSION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Class Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ClassImpl <em>Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ClassImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getClass_()
	 * @generated
	 */
	int CLASS = 11;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Abbreviated IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__ABBREVIATED_IRI = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__IRI = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ClassAssertionImpl <em>Class Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ClassAssertionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getClassAssertion()
	 * @generated
	 */
	int CLASS_ASSERTION = 12;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__ANNOTATION = ASSERTION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__BASE = ASSERTION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__ID = ASSERTION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__LANG = ASSERTION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__SPACE = ASSERTION__SPACE;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__CLASS = ASSERTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__OBJECT_INTERSECTION_OF = ASSERTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__OBJECT_UNION_OF = ASSERTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__OBJECT_COMPLEMENT_OF = ASSERTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__OBJECT_ONE_OF = ASSERTION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__OBJECT_SOME_VALUES_FROM = ASSERTION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__OBJECT_ALL_VALUES_FROM = ASSERTION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__OBJECT_HAS_VALUE = ASSERTION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__OBJECT_HAS_SELF = ASSERTION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__OBJECT_MIN_CARDINALITY = ASSERTION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__OBJECT_MAX_CARDINALITY = ASSERTION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__OBJECT_EXACT_CARDINALITY = ASSERTION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__DATA_SOME_VALUES_FROM = ASSERTION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__DATA_ALL_VALUES_FROM = ASSERTION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__DATA_HAS_VALUE = ASSERTION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__DATA_MIN_CARDINALITY = ASSERTION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__DATA_MAX_CARDINALITY = ASSERTION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__DATA_EXACT_CARDINALITY = ASSERTION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Named Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__NAMED_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Anonymous Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION__ANONYMOUS_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 19;

	/**
	 * The number of structural features of the '<em>Class Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION_FEATURE_COUNT = ASSERTION_FEATURE_COUNT + 20;

	/**
	 * The number of operations of the '<em>Class Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ASSERTION_OPERATION_COUNT = ASSERTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ClassAxiomImpl <em>Class Axiom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ClassAxiomImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getClassAxiom()
	 * @generated
	 */
	int CLASS_AXIOM = 13;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_AXIOM__ANNOTATION = AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_AXIOM__BASE = AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_AXIOM__ID = AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_AXIOM__LANG = AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_AXIOM__SPACE = AXIOM__SPACE;

	/**
	 * The number of structural features of the '<em>Class Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_AXIOM_FEATURE_COUNT = AXIOM_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Class Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_AXIOM_OPERATION_COUNT = AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataAllValuesFromImpl <em>Data All Values From</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataAllValuesFromImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataAllValuesFrom()
	 * @generated
	 */
	int DATA_ALL_VALUES_FROM = 15;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM__DATA_PROPERTY_EXPRESSION = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM__DATA_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM__DATATYPE = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM__DATA_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM__DATA_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Data Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM__DATA_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Data One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM__DATA_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Datatype Restriction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM__DATATYPE_RESTRICTION = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Data All Values From</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Data All Values From</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ALL_VALUES_FROM_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataRangeImpl <em>Data Range</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataRangeImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataRange()
	 * @generated
	 */
	int DATA_RANGE = 29;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RANGE__BASE = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RANGE__ID = 1;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RANGE__LANG = 2;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RANGE__SPACE = 3;

	/**
	 * The number of structural features of the '<em>Data Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RANGE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Data Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RANGE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataComplementOfImpl <em>Data Complement Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataComplementOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataComplementOf()
	 * @generated
	 */
	int DATA_COMPLEMENT_OF = 16;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_COMPLEMENT_OF__BASE = DATA_RANGE__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_COMPLEMENT_OF__ID = DATA_RANGE__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_COMPLEMENT_OF__LANG = DATA_RANGE__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_COMPLEMENT_OF__SPACE = DATA_RANGE__SPACE;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_COMPLEMENT_OF__DATATYPE = DATA_RANGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_COMPLEMENT_OF__DATA_INTERSECTION_OF = DATA_RANGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Data Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_COMPLEMENT_OF__DATA_UNION_OF = DATA_RANGE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_COMPLEMENT_OF__DATA_COMPLEMENT_OF = DATA_RANGE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_COMPLEMENT_OF__DATA_ONE_OF = DATA_RANGE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Datatype Restriction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_COMPLEMENT_OF__DATATYPE_RESTRICTION = DATA_RANGE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Data Complement Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_COMPLEMENT_OF_FEATURE_COUNT = DATA_RANGE_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Data Complement Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_COMPLEMENT_OF_OPERATION_COUNT = DATA_RANGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataExactCardinalityImpl <em>Data Exact Cardinality</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataExactCardinalityImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataExactCardinality()
	 * @generated
	 */
	int DATA_EXACT_CARDINALITY = 17;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY__DATA_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY__DATATYPE = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Data Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY__DATA_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY__DATA_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY__DATA_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Data One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY__DATA_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Datatype Restriction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY__DATATYPE_RESTRICTION = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY__CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Data Exact Cardinality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Data Exact Cardinality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EXACT_CARDINALITY_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataHasValueImpl <em>Data Has Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataHasValueImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataHasValue()
	 * @generated
	 */
	int DATA_HAS_VALUE = 18;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_HAS_VALUE__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_HAS_VALUE__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_HAS_VALUE__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_HAS_VALUE__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_HAS_VALUE__DATA_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_HAS_VALUE__LITERAL = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Data Has Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_HAS_VALUE_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Data Has Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_HAS_VALUE_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataIntersectionOfImpl <em>Data Intersection Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataIntersectionOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataIntersectionOf()
	 * @generated
	 */
	int DATA_INTERSECTION_OF = 19;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF__BASE = DATA_RANGE__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF__ID = DATA_RANGE__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF__LANG = DATA_RANGE__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF__SPACE = DATA_RANGE__SPACE;

	/**
	 * The feature id for the '<em><b>Data Range</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF__DATA_RANGE = DATA_RANGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF__DATATYPE = DATA_RANGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Data Intersection Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF__DATA_INTERSECTION_OF = DATA_RANGE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Union Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF__DATA_UNION_OF = DATA_RANGE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Complement Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF__DATA_COMPLEMENT_OF = DATA_RANGE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Data One Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF__DATA_ONE_OF = DATA_RANGE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Datatype Restriction</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF__DATATYPE_RESTRICTION = DATA_RANGE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Data Intersection Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF_FEATURE_COUNT = DATA_RANGE_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Data Intersection Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INTERSECTION_OF_OPERATION_COUNT = DATA_RANGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataMaxCardinalityImpl <em>Data Max Cardinality</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataMaxCardinalityImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataMaxCardinality()
	 * @generated
	 */
	int DATA_MAX_CARDINALITY = 20;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY__DATA_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY__DATATYPE = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Data Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY__DATA_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY__DATA_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY__DATA_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Data One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY__DATA_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Datatype Restriction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY__DATATYPE_RESTRICTION = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY__CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Data Max Cardinality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Data Max Cardinality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MAX_CARDINALITY_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataMinCardinalityImpl <em>Data Min Cardinality</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataMinCardinalityImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataMinCardinality()
	 * @generated
	 */
	int DATA_MIN_CARDINALITY = 21;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY__DATA_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY__DATATYPE = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Data Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY__DATA_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY__DATA_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY__DATA_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Data One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY__DATA_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Datatype Restriction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY__DATATYPE_RESTRICTION = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY__CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Data Min Cardinality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Data Min Cardinality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MIN_CARDINALITY_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataOneOfImpl <em>Data One Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataOneOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataOneOf()
	 * @generated
	 */
	int DATA_ONE_OF = 22;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ONE_OF__BASE = DATA_RANGE__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ONE_OF__ID = DATA_RANGE__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ONE_OF__LANG = DATA_RANGE__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ONE_OF__SPACE = DATA_RANGE__SPACE;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ONE_OF__LITERAL = DATA_RANGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Data One Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ONE_OF_FEATURE_COUNT = DATA_RANGE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Data One Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ONE_OF_OPERATION_COUNT = DATA_RANGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataPropertyExpressionImpl <em>Data Property Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataPropertyExpressionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataPropertyExpression()
	 * @generated
	 */
	int DATA_PROPERTY_EXPRESSION = 27;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_EXPRESSION__BASE = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_EXPRESSION__ID = 1;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_EXPRESSION__LANG = 2;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_EXPRESSION__SPACE = 3;

	/**
	 * The number of structural features of the '<em>Data Property Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_EXPRESSION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Data Property Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataPropertyImpl <em>Data Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataPropertyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataProperty()
	 * @generated
	 */
	int DATA_PROPERTY = 23;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY__BASE = DATA_PROPERTY_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY__ID = DATA_PROPERTY_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY__LANG = DATA_PROPERTY_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY__SPACE = DATA_PROPERTY_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Abbreviated IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY__ABBREVIATED_IRI = DATA_PROPERTY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY__IRI = DATA_PROPERTY_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Data Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_FEATURE_COUNT = DATA_PROPERTY_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Data Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_OPERATION_COUNT = DATA_PROPERTY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataPropertyAssertionImpl <em>Data Property Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataPropertyAssertionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataPropertyAssertion()
	 * @generated
	 */
	int DATA_PROPERTY_ASSERTION = 24;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_ASSERTION__ANNOTATION = ASSERTION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_ASSERTION__BASE = ASSERTION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_ASSERTION__ID = ASSERTION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_ASSERTION__LANG = ASSERTION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_ASSERTION__SPACE = ASSERTION__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_ASSERTION__DATA_PROPERTY = ASSERTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Named Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_ASSERTION__NAMED_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Anonymous Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_ASSERTION__LITERAL = ASSERTION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Data Property Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_ASSERTION_FEATURE_COUNT = ASSERTION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Data Property Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_ASSERTION_OPERATION_COUNT = ASSERTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataPropertyAxiomImpl <em>Data Property Axiom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataPropertyAxiomImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataPropertyAxiom()
	 * @generated
	 */
	int DATA_PROPERTY_AXIOM = 25;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_AXIOM__ANNOTATION = AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_AXIOM__BASE = AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_AXIOM__ID = AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_AXIOM__LANG = AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_AXIOM__SPACE = AXIOM__SPACE;

	/**
	 * The number of structural features of the '<em>Data Property Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_AXIOM_FEATURE_COUNT = AXIOM_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Data Property Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_AXIOM_OPERATION_COUNT = AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataPropertyDomainImpl <em>Data Property Domain</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataPropertyDomainImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataPropertyDomain()
	 * @generated
	 */
	int DATA_PROPERTY_DOMAIN = 26;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__ANNOTATION = DATA_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__BASE = DATA_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__ID = DATA_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__LANG = DATA_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__SPACE = DATA_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__DATA_PROPERTY = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__CLASS = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__OBJECT_INTERSECTION_OF = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__OBJECT_UNION_OF = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__OBJECT_COMPLEMENT_OF = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__OBJECT_ONE_OF = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__OBJECT_SOME_VALUES_FROM = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__OBJECT_ALL_VALUES_FROM = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__OBJECT_HAS_VALUE = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__OBJECT_HAS_SELF = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__OBJECT_MIN_CARDINALITY = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__OBJECT_MAX_CARDINALITY = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__OBJECT_EXACT_CARDINALITY = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__DATA_SOME_VALUES_FROM = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__DATA_ALL_VALUES_FROM = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__DATA_HAS_VALUE = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__DATA_MIN_CARDINALITY = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__DATA_MAX_CARDINALITY = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN__DATA_EXACT_CARDINALITY = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 18;

	/**
	 * The number of structural features of the '<em>Data Property Domain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN_FEATURE_COUNT = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 19;

	/**
	 * The number of operations of the '<em>Data Property Domain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_DOMAIN_OPERATION_COUNT = DATA_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataPropertyRangeImpl <em>Data Property Range</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataPropertyRangeImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataPropertyRange()
	 * @generated
	 */
	int DATA_PROPERTY_RANGE = 28;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE__ANNOTATION = DATA_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE__BASE = DATA_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE__ID = DATA_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE__LANG = DATA_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE__SPACE = DATA_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE__DATA_PROPERTY = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE__DATATYPE = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Data Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE__DATA_INTERSECTION_OF = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE__DATA_UNION_OF = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE__DATA_COMPLEMENT_OF = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Data One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE__DATA_ONE_OF = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Datatype Restriction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE__DATATYPE_RESTRICTION = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Data Property Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE_FEATURE_COUNT = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Data Property Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROPERTY_RANGE_OPERATION_COUNT = DATA_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataSomeValuesFromImpl <em>Data Some Values From</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataSomeValuesFromImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataSomeValuesFrom()
	 * @generated
	 */
	int DATA_SOME_VALUES_FROM = 30;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM__DATA_PROPERTY_EXPRESSION = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM__DATA_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM__DATATYPE = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM__DATA_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM__DATA_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Data Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM__DATA_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Data One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM__DATA_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Datatype Restriction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM__DATATYPE_RESTRICTION = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Data Some Values From</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Data Some Values From</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SOME_VALUES_FROM_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DatatypeImpl <em>Datatype</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DatatypeImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDatatype()
	 * @generated
	 */
	int DATATYPE = 31;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__BASE = DATA_RANGE__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__ID = DATA_RANGE__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__LANG = DATA_RANGE__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__SPACE = DATA_RANGE__SPACE;

	/**
	 * The feature id for the '<em><b>Abbreviated IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__ABBREVIATED_IRI = DATA_RANGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE__IRI = DATA_RANGE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Datatype</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_FEATURE_COUNT = DATA_RANGE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Datatype</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_OPERATION_COUNT = DATA_RANGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DatatypeDefinitionImpl <em>Datatype Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DatatypeDefinitionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDatatypeDefinition()
	 * @generated
	 */
	int DATATYPE_DEFINITION = 32;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION__ANNOTATION = AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION__BASE = AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION__ID = AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION__LANG = AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION__SPACE = AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION__DATATYPE = AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datatype1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION__DATATYPE1 = AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Data Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION__DATA_INTERSECTION_OF = AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION__DATA_UNION_OF = AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION__DATA_COMPLEMENT_OF = AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Data One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION__DATA_ONE_OF = AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Datatype Restriction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION__DATATYPE_RESTRICTION = AXIOM_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Datatype Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION_FEATURE_COUNT = AXIOM_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Datatype Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_DEFINITION_OPERATION_COUNT = AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DatatypeRestrictionImpl <em>Datatype Restriction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DatatypeRestrictionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDatatypeRestriction()
	 * @generated
	 */
	int DATATYPE_RESTRICTION = 33;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_RESTRICTION__BASE = DATA_RANGE__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_RESTRICTION__ID = DATA_RANGE__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_RESTRICTION__LANG = DATA_RANGE__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_RESTRICTION__SPACE = DATA_RANGE__SPACE;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_RESTRICTION__DATATYPE = DATA_RANGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Facet Restriction</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_RESTRICTION__FACET_RESTRICTION = DATA_RANGE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Datatype Restriction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_RESTRICTION_FEATURE_COUNT = DATA_RANGE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Datatype Restriction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATATYPE_RESTRICTION_OPERATION_COUNT = DATA_RANGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DataUnionOfImpl <em>Data Union Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DataUnionOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDataUnionOf()
	 * @generated
	 */
	int DATA_UNION_OF = 34;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF__BASE = DATA_RANGE__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF__ID = DATA_RANGE__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF__LANG = DATA_RANGE__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF__SPACE = DATA_RANGE__SPACE;

	/**
	 * The feature id for the '<em><b>Data Range</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF__DATA_RANGE = DATA_RANGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF__DATATYPE = DATA_RANGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Data Intersection Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF__DATA_INTERSECTION_OF = DATA_RANGE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Union Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF__DATA_UNION_OF = DATA_RANGE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Complement Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF__DATA_COMPLEMENT_OF = DATA_RANGE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Data One Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF__DATA_ONE_OF = DATA_RANGE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Datatype Restriction</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF__DATATYPE_RESTRICTION = DATA_RANGE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Data Union Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF_FEATURE_COUNT = DATA_RANGE_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Data Union Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_UNION_OF_OPERATION_COUNT = DATA_RANGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DeclarationImpl <em>Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DeclarationImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDeclaration()
	 * @generated
	 */
	int DECLARATION = 35;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__ANNOTATION = AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__BASE = AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__ID = AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__LANG = AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__SPACE = AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__CLASS = AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__DATATYPE = AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__OBJECT_PROPERTY = AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__DATA_PROPERTY = AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Annotation Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__ANNOTATION_PROPERTY = AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Named Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__NAMED_INDIVIDUAL = AXIOM_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_FEATURE_COUNT = AXIOM_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_OPERATION_COUNT = AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DifferentIndividualsImpl <em>Different Individuals</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DifferentIndividualsImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDifferentIndividuals()
	 * @generated
	 */
	int DIFFERENT_INDIVIDUALS = 36;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENT_INDIVIDUALS__ANNOTATION = ASSERTION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENT_INDIVIDUALS__BASE = ASSERTION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENT_INDIVIDUALS__ID = ASSERTION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENT_INDIVIDUALS__LANG = ASSERTION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENT_INDIVIDUALS__SPACE = ASSERTION__SPACE;

	/**
	 * The feature id for the '<em><b>Individual</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENT_INDIVIDUALS__INDIVIDUAL = ASSERTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Named Individual</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENT_INDIVIDUALS__NAMED_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Anonymous Individual</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENT_INDIVIDUALS__ANONYMOUS_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Different Individuals</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENT_INDIVIDUALS_FEATURE_COUNT = ASSERTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Different Individuals</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENT_INDIVIDUALS_OPERATION_COUNT = ASSERTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DisjointClassesImpl <em>Disjoint Classes</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DisjointClassesImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDisjointClasses()
	 * @generated
	 */
	int DISJOINT_CLASSES = 37;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__ANNOTATION = CLASS_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__BASE = CLASS_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__ID = CLASS_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__LANG = CLASS_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__SPACE = CLASS_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Class Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__CLASS_EXPRESSION = CLASS_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__CLASS = CLASS_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__OBJECT_INTERSECTION_OF = CLASS_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__OBJECT_UNION_OF = CLASS_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__OBJECT_COMPLEMENT_OF = CLASS_AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__OBJECT_ONE_OF = CLASS_AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__OBJECT_SOME_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__OBJECT_ALL_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__OBJECT_HAS_VALUE = CLASS_AXIOM_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__OBJECT_HAS_SELF = CLASS_AXIOM_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__OBJECT_MIN_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__OBJECT_MAX_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__OBJECT_EXACT_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__DATA_SOME_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__DATA_ALL_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__DATA_HAS_VALUE = CLASS_AXIOM_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__DATA_MIN_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__DATA_MAX_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES__DATA_EXACT_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 18;

	/**
	 * The number of structural features of the '<em>Disjoint Classes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES_FEATURE_COUNT = CLASS_AXIOM_FEATURE_COUNT + 19;

	/**
	 * The number of operations of the '<em>Disjoint Classes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_CLASSES_OPERATION_COUNT = CLASS_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DisjointDataPropertiesImpl <em>Disjoint Data Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DisjointDataPropertiesImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDisjointDataProperties()
	 * @generated
	 */
	int DISJOINT_DATA_PROPERTIES = 38;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_DATA_PROPERTIES__ANNOTATION = DATA_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_DATA_PROPERTIES__BASE = DATA_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_DATA_PROPERTIES__ID = DATA_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_DATA_PROPERTIES__LANG = DATA_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_DATA_PROPERTIES__SPACE = DATA_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_DATA_PROPERTIES__DATA_PROPERTY_EXPRESSION = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_DATA_PROPERTIES__DATA_PROPERTY = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Disjoint Data Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_DATA_PROPERTIES_FEATURE_COUNT = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Disjoint Data Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_DATA_PROPERTIES_OPERATION_COUNT = DATA_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DisjointObjectPropertiesImpl <em>Disjoint Object Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DisjointObjectPropertiesImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDisjointObjectProperties()
	 * @generated
	 */
	int DISJOINT_OBJECT_PROPERTIES = 39;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_OBJECT_PROPERTIES__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_OBJECT_PROPERTIES__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_OBJECT_PROPERTIES__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_OBJECT_PROPERTIES__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_OBJECT_PROPERTIES__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_OBJECT_PROPERTIES__OBJECT_PROPERTY_EXPRESSION = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_OBJECT_PROPERTIES__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_OBJECT_PROPERTIES__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Disjoint Object Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_OBJECT_PROPERTIES_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Disjoint Object Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_OBJECT_PROPERTIES_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DisjointUnionImpl <em>Disjoint Union</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DisjointUnionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDisjointUnion()
	 * @generated
	 */
	int DISJOINT_UNION = 40;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__ANNOTATION = CLASS_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__BASE = CLASS_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__ID = CLASS_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__LANG = CLASS_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__SPACE = CLASS_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__CLASS = CLASS_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Class Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__CLASS_EXPRESSION = CLASS_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Class1</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__CLASS1 = CLASS_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__OBJECT_INTERSECTION_OF = CLASS_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__OBJECT_UNION_OF = CLASS_AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__OBJECT_COMPLEMENT_OF = CLASS_AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__OBJECT_ONE_OF = CLASS_AXIOM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__OBJECT_SOME_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__OBJECT_ALL_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__OBJECT_HAS_VALUE = CLASS_AXIOM_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__OBJECT_HAS_SELF = CLASS_AXIOM_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__OBJECT_MIN_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__OBJECT_MAX_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__OBJECT_EXACT_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__DATA_SOME_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__DATA_ALL_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__DATA_HAS_VALUE = CLASS_AXIOM_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__DATA_MIN_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__DATA_MAX_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION__DATA_EXACT_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 19;

	/**
	 * The number of structural features of the '<em>Disjoint Union</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION_FEATURE_COUNT = CLASS_AXIOM_FEATURE_COUNT + 20;

	/**
	 * The number of operations of the '<em>Disjoint Union</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJOINT_UNION_OPERATION_COUNT = CLASS_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.DocumentRootImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 41;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Abbreviated IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ABBREVIATED_IRI = 3;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ANNOTATION = 4;

	/**
	 * The feature id for the '<em><b>Annotation Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ANNOTATION_ASSERTION = 5;

	/**
	 * The feature id for the '<em><b>Annotation Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ANNOTATION_PROPERTY = 6;

	/**
	 * The feature id for the '<em><b>Annotation Property Domain</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ANNOTATION_PROPERTY_DOMAIN = 7;

	/**
	 * The feature id for the '<em><b>Annotation Property Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ANNOTATION_PROPERTY_RANGE = 8;

	/**
	 * The feature id for the '<em><b>Anonymous Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ANONYMOUS_INDIVIDUAL = 9;

	/**
	 * The feature id for the '<em><b>Asymmetric Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ASYMMETRIC_OBJECT_PROPERTY = 10;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__CLASS = 11;

	/**
	 * The feature id for the '<em><b>Class Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__CLASS_ASSERTION = 12;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_ALL_VALUES_FROM = 13;

	/**
	 * The feature id for the '<em><b>Data Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_COMPLEMENT_OF = 14;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_EXACT_CARDINALITY = 15;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_HAS_VALUE = 16;

	/**
	 * The feature id for the '<em><b>Data Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_INTERSECTION_OF = 17;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_MAX_CARDINALITY = 18;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_MIN_CARDINALITY = 19;

	/**
	 * The feature id for the '<em><b>Data One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_ONE_OF = 20;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_PROPERTY = 21;

	/**
	 * The feature id for the '<em><b>Data Property Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_PROPERTY_ASSERTION = 22;

	/**
	 * The feature id for the '<em><b>Data Property Domain</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_PROPERTY_DOMAIN = 23;

	/**
	 * The feature id for the '<em><b>Data Property Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_PROPERTY_RANGE = 24;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_SOME_VALUES_FROM = 25;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATATYPE = 26;

	/**
	 * The feature id for the '<em><b>Datatype Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATATYPE_DEFINITION = 27;

	/**
	 * The feature id for the '<em><b>Datatype Restriction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATATYPE_RESTRICTION = 28;

	/**
	 * The feature id for the '<em><b>Data Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_UNION_OF = 29;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DECLARATION = 30;

	/**
	 * The feature id for the '<em><b>Different Individuals</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DIFFERENT_INDIVIDUALS = 31;

	/**
	 * The feature id for the '<em><b>Disjoint Classes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DISJOINT_CLASSES = 32;

	/**
	 * The feature id for the '<em><b>Disjoint Data Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DISJOINT_DATA_PROPERTIES = 33;

	/**
	 * The feature id for the '<em><b>Disjoint Object Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DISJOINT_OBJECT_PROPERTIES = 34;

	/**
	 * The feature id for the '<em><b>Disjoint Union</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DISJOINT_UNION = 35;

	/**
	 * The feature id for the '<em><b>Equivalent Classes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__EQUIVALENT_CLASSES = 36;

	/**
	 * The feature id for the '<em><b>Equivalent Data Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__EQUIVALENT_DATA_PROPERTIES = 37;

	/**
	 * The feature id for the '<em><b>Equivalent Object Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__EQUIVALENT_OBJECT_PROPERTIES = 38;

	/**
	 * The feature id for the '<em><b>Functional Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__FUNCTIONAL_DATA_PROPERTY = 39;

	/**
	 * The feature id for the '<em><b>Functional Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__FUNCTIONAL_OBJECT_PROPERTY = 40;

	/**
	 * The feature id for the '<em><b>Has Key</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__HAS_KEY = 41;

	/**
	 * The feature id for the '<em><b>Import</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMPORT = 42;

	/**
	 * The feature id for the '<em><b>Inverse Functional Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__INVERSE_FUNCTIONAL_OBJECT_PROPERTY = 43;

	/**
	 * The feature id for the '<em><b>Inverse Object Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__INVERSE_OBJECT_PROPERTIES = 44;

	/**
	 * The feature id for the '<em><b>IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IRI = 45;

	/**
	 * The feature id for the '<em><b>Irreflexive Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IRREFLEXIVE_OBJECT_PROPERTY = 46;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__LITERAL = 47;

	/**
	 * The feature id for the '<em><b>Named Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__NAMED_INDIVIDUAL = 48;

	/**
	 * The feature id for the '<em><b>Negative Data Property Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__NEGATIVE_DATA_PROPERTY_ASSERTION = 49;

	/**
	 * The feature id for the '<em><b>Negative Object Property Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__NEGATIVE_OBJECT_PROPERTY_ASSERTION = 50;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_ALL_VALUES_FROM = 51;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_COMPLEMENT_OF = 52;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_EXACT_CARDINALITY = 53;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_HAS_SELF = 54;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_HAS_VALUE = 55;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_INTERSECTION_OF = 56;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_INVERSE_OF = 57;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_MAX_CARDINALITY = 58;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_MIN_CARDINALITY = 59;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_ONE_OF = 60;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_PROPERTY = 61;

	/**
	 * The feature id for the '<em><b>Object Property Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_PROPERTY_ASSERTION = 62;

	/**
	 * The feature id for the '<em><b>Object Property Domain</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_PROPERTY_DOMAIN = 63;

	/**
	 * The feature id for the '<em><b>Object Property Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_PROPERTY_RANGE = 64;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_SOME_VALUES_FROM = 65;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OBJECT_UNION_OF = 66;

	/**
	 * The feature id for the '<em><b>Ontology</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ONTOLOGY = 67;

	/**
	 * The feature id for the '<em><b>Prefix</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__PREFIX = 68;

	/**
	 * The feature id for the '<em><b>Reflexive Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__REFLEXIVE_OBJECT_PROPERTY = 69;

	/**
	 * The feature id for the '<em><b>Same Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__SAME_INDIVIDUAL = 70;

	/**
	 * The feature id for the '<em><b>Sub Annotation Property Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__SUB_ANNOTATION_PROPERTY_OF = 71;

	/**
	 * The feature id for the '<em><b>Sub Class Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__SUB_CLASS_OF = 72;

	/**
	 * The feature id for the '<em><b>Sub Data Property Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__SUB_DATA_PROPERTY_OF = 73;

	/**
	 * The feature id for the '<em><b>Sub Object Property Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__SUB_OBJECT_PROPERTY_OF = 74;

	/**
	 * The feature id for the '<em><b>Symmetric Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__SYMMETRIC_OBJECT_PROPERTY = 75;

	/**
	 * The feature id for the '<em><b>Transitive Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__TRANSITIVE_OBJECT_PROPERTY = 76;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 77;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.EquivalentClassesImpl <em>Equivalent Classes</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.EquivalentClassesImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getEquivalentClasses()
	 * @generated
	 */
	int EQUIVALENT_CLASSES = 42;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__ANNOTATION = CLASS_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__BASE = CLASS_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__ID = CLASS_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__LANG = CLASS_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__SPACE = CLASS_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Class Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__CLASS_EXPRESSION = CLASS_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__CLASS = CLASS_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__OBJECT_INTERSECTION_OF = CLASS_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__OBJECT_UNION_OF = CLASS_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__OBJECT_COMPLEMENT_OF = CLASS_AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__OBJECT_ONE_OF = CLASS_AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__OBJECT_SOME_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__OBJECT_ALL_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__OBJECT_HAS_VALUE = CLASS_AXIOM_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__OBJECT_HAS_SELF = CLASS_AXIOM_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__OBJECT_MIN_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__OBJECT_MAX_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__OBJECT_EXACT_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__DATA_SOME_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__DATA_ALL_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__DATA_HAS_VALUE = CLASS_AXIOM_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__DATA_MIN_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__DATA_MAX_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES__DATA_EXACT_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 18;

	/**
	 * The number of structural features of the '<em>Equivalent Classes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES_FEATURE_COUNT = CLASS_AXIOM_FEATURE_COUNT + 19;

	/**
	 * The number of operations of the '<em>Equivalent Classes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_CLASSES_OPERATION_COUNT = CLASS_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.EquivalentDataPropertiesImpl <em>Equivalent Data Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.EquivalentDataPropertiesImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getEquivalentDataProperties()
	 * @generated
	 */
	int EQUIVALENT_DATA_PROPERTIES = 43;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_DATA_PROPERTIES__ANNOTATION = DATA_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_DATA_PROPERTIES__BASE = DATA_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_DATA_PROPERTIES__ID = DATA_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_DATA_PROPERTIES__LANG = DATA_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_DATA_PROPERTIES__SPACE = DATA_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_DATA_PROPERTIES__DATA_PROPERTY_EXPRESSION = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_DATA_PROPERTIES__DATA_PROPERTY = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Equivalent Data Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_DATA_PROPERTIES_FEATURE_COUNT = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Equivalent Data Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_DATA_PROPERTIES_OPERATION_COUNT = DATA_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.EquivalentObjectPropertiesImpl <em>Equivalent Object Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.EquivalentObjectPropertiesImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getEquivalentObjectProperties()
	 * @generated
	 */
	int EQUIVALENT_OBJECT_PROPERTIES = 44;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_OBJECT_PROPERTIES__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_OBJECT_PROPERTIES__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_OBJECT_PROPERTIES__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_OBJECT_PROPERTIES__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_OBJECT_PROPERTIES__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_OBJECT_PROPERTIES__OBJECT_PROPERTY_EXPRESSION = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_OBJECT_PROPERTIES__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_OBJECT_PROPERTIES__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Equivalent Object Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_OBJECT_PROPERTIES_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Equivalent Object Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_OBJECT_PROPERTIES_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.FacetRestrictionImpl <em>Facet Restriction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.FacetRestrictionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getFacetRestriction()
	 * @generated
	 */
	int FACET_RESTRICTION = 45;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACET_RESTRICTION__LITERAL = 0;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACET_RESTRICTION__BASE = 1;

	/**
	 * The feature id for the '<em><b>Facet</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACET_RESTRICTION__FACET = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACET_RESTRICTION__ID = 3;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACET_RESTRICTION__LANG = 4;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACET_RESTRICTION__SPACE = 5;

	/**
	 * The number of structural features of the '<em>Facet Restriction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACET_RESTRICTION_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Facet Restriction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACET_RESTRICTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.FunctionalDataPropertyImpl <em>Functional Data Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.FunctionalDataPropertyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getFunctionalDataProperty()
	 * @generated
	 */
	int FUNCTIONAL_DATA_PROPERTY = 46;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_DATA_PROPERTY__ANNOTATION = DATA_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_DATA_PROPERTY__BASE = DATA_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_DATA_PROPERTY__ID = DATA_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_DATA_PROPERTY__LANG = DATA_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_DATA_PROPERTY__SPACE = DATA_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_DATA_PROPERTY__DATA_PROPERTY = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Functional Data Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_DATA_PROPERTY_FEATURE_COUNT = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Functional Data Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_DATA_PROPERTY_OPERATION_COUNT = DATA_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.FunctionalObjectPropertyImpl <em>Functional Object Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.FunctionalObjectPropertyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getFunctionalObjectProperty()
	 * @generated
	 */
	int FUNCTIONAL_OBJECT_PROPERTY = 47;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_OBJECT_PROPERTY__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_OBJECT_PROPERTY__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_OBJECT_PROPERTY__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_OBJECT_PROPERTY__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_OBJECT_PROPERTY__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_OBJECT_PROPERTY__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_OBJECT_PROPERTY__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Functional Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_OBJECT_PROPERTY_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Functional Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_OBJECT_PROPERTY_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.HasKeyImpl <em>Has Key</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.HasKeyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getHasKey()
	 * @generated
	 */
	int HAS_KEY = 48;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__ANNOTATION = AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__BASE = AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__ID = AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__LANG = AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__SPACE = AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__CLASS = AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_INTERSECTION_OF = AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_UNION_OF = AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_COMPLEMENT_OF = AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_ONE_OF = AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_SOME_VALUES_FROM = AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_ALL_VALUES_FROM = AXIOM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_HAS_VALUE = AXIOM_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_HAS_SELF = AXIOM_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_MIN_CARDINALITY = AXIOM_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_MAX_CARDINALITY = AXIOM_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_EXACT_CARDINALITY = AXIOM_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__DATA_SOME_VALUES_FROM = AXIOM_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__DATA_ALL_VALUES_FROM = AXIOM_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__DATA_HAS_VALUE = AXIOM_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__DATA_MIN_CARDINALITY = AXIOM_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__DATA_MAX_CARDINALITY = AXIOM_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__DATA_EXACT_CARDINALITY = AXIOM_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Object Property Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_PROPERTY_EXPRESSION = AXIOM_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_PROPERTY = AXIOM_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__OBJECT_INVERSE_OF = AXIOM_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>Data Property Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__DATA_PROPERTY_EXPRESSION = AXIOM_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY__DATA_PROPERTY = AXIOM_FEATURE_COUNT + 22;

	/**
	 * The number of structural features of the '<em>Has Key</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY_FEATURE_COUNT = AXIOM_FEATURE_COUNT + 23;

	/**
	 * The number of operations of the '<em>Has Key</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KEY_OPERATION_COUNT = AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ImportImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getImport()
	 * @generated
	 */
	int IMPORT = 49;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__BASE = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__ID = 2;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__LANG = 3;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__SPACE = 4;

	/**
	 * The number of structural features of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.InverseFunctionalObjectPropertyImpl <em>Inverse Functional Object Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.InverseFunctionalObjectPropertyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getInverseFunctionalObjectProperty()
	 * @generated
	 */
	int INVERSE_FUNCTIONAL_OBJECT_PROPERTY = 51;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_FUNCTIONAL_OBJECT_PROPERTY__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_FUNCTIONAL_OBJECT_PROPERTY__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_FUNCTIONAL_OBJECT_PROPERTY__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_FUNCTIONAL_OBJECT_PROPERTY__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_FUNCTIONAL_OBJECT_PROPERTY__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_FUNCTIONAL_OBJECT_PROPERTY__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_FUNCTIONAL_OBJECT_PROPERTY__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Inverse Functional Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_FUNCTIONAL_OBJECT_PROPERTY_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Inverse Functional Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_FUNCTIONAL_OBJECT_PROPERTY_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.InverseObjectPropertiesImpl <em>Inverse Object Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.InverseObjectPropertiesImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getInverseObjectProperties()
	 * @generated
	 */
	int INVERSE_OBJECT_PROPERTIES = 52;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_OBJECT_PROPERTIES__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_OBJECT_PROPERTIES__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_OBJECT_PROPERTIES__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_OBJECT_PROPERTIES__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_OBJECT_PROPERTIES__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY_EXPRESSION = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_OBJECT_PROPERTIES__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Inverse Object Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_OBJECT_PROPERTIES_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Inverse Object Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERSE_OBJECT_PROPERTIES_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.IRIImpl <em>IRI</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.IRIImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getIRI()
	 * @generated
	 */
	int IRI = 53;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRI__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRI__BASE = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRI__ID = 2;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRI__LANG = 3;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRI__SPACE = 4;

	/**
	 * The number of structural features of the '<em>IRI</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRI_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>IRI</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRI_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.IrreflexiveObjectPropertyImpl <em>Irreflexive Object Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.IrreflexiveObjectPropertyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getIrreflexiveObjectProperty()
	 * @generated
	 */
	int IRREFLEXIVE_OBJECT_PROPERTY = 54;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRREFLEXIVE_OBJECT_PROPERTY__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRREFLEXIVE_OBJECT_PROPERTY__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRREFLEXIVE_OBJECT_PROPERTY__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRREFLEXIVE_OBJECT_PROPERTY__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRREFLEXIVE_OBJECT_PROPERTY__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRREFLEXIVE_OBJECT_PROPERTY__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRREFLEXIVE_OBJECT_PROPERTY__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Irreflexive Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRREFLEXIVE_OBJECT_PROPERTY_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Irreflexive Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRREFLEXIVE_OBJECT_PROPERTY_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.LiteralImpl <em>Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.LiteralImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getLiteral()
	 * @generated
	 */
	int LITERAL = 55;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__BASE = 1;

	/**
	 * The feature id for the '<em><b>Datatype IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__DATATYPE_IRI = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__ID = 3;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__LANG = 4;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__SPACE = 5;

	/**
	 * The number of structural features of the '<em>Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.NamedIndividualImpl <em>Named Individual</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.NamedIndividualImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getNamedIndividual()
	 * @generated
	 */
	int NAMED_INDIVIDUAL = 56;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_INDIVIDUAL__BASE = INDIVIDUAL__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_INDIVIDUAL__ID = INDIVIDUAL__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_INDIVIDUAL__LANG = INDIVIDUAL__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_INDIVIDUAL__SPACE = INDIVIDUAL__SPACE;

	/**
	 * The feature id for the '<em><b>Abbreviated IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_INDIVIDUAL__ABBREVIATED_IRI = INDIVIDUAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_INDIVIDUAL__IRI = INDIVIDUAL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Named Individual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_INDIVIDUAL_FEATURE_COUNT = INDIVIDUAL_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Named Individual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_INDIVIDUAL_OPERATION_COUNT = INDIVIDUAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.NegativeDataPropertyAssertionImpl <em>Negative Data Property Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.NegativeDataPropertyAssertionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getNegativeDataPropertyAssertion()
	 * @generated
	 */
	int NEGATIVE_DATA_PROPERTY_ASSERTION = 57;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DATA_PROPERTY_ASSERTION__ANNOTATION = ASSERTION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DATA_PROPERTY_ASSERTION__BASE = ASSERTION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DATA_PROPERTY_ASSERTION__ID = ASSERTION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DATA_PROPERTY_ASSERTION__LANG = ASSERTION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DATA_PROPERTY_ASSERTION__SPACE = ASSERTION__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DATA_PROPERTY_ASSERTION__DATA_PROPERTY = ASSERTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Named Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DATA_PROPERTY_ASSERTION__NAMED_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Anonymous Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DATA_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DATA_PROPERTY_ASSERTION__LITERAL = ASSERTION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Negative Data Property Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DATA_PROPERTY_ASSERTION_FEATURE_COUNT = ASSERTION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Negative Data Property Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DATA_PROPERTY_ASSERTION_OPERATION_COUNT = ASSERTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.NegativeObjectPropertyAssertionImpl <em>Negative Object Property Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.NegativeObjectPropertyAssertionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getNegativeObjectPropertyAssertion()
	 * @generated
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION = 58;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANNOTATION = ASSERTION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION__BASE = ASSERTION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION__ID = ASSERTION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION__LANG = ASSERTION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION__SPACE = ASSERTION__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY = ASSERTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF = ASSERTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Named Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Anonymous Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Named Individual1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL1 = ASSERTION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Anonymous Individual1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1 = ASSERTION_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Negative Object Property Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION_FEATURE_COUNT = ASSERTION_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Negative Object Property Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OBJECT_PROPERTY_ASSERTION_OPERATION_COUNT = ASSERTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectAllValuesFromImpl <em>Object All Values From</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectAllValuesFromImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectAllValuesFrom()
	 * @generated
	 */
	int OBJECT_ALL_VALUES_FROM = 59;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_INVERSE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__CLASS = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_HAS_SELF = CLASS_EXPRESSION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__OBJECT_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__DATA_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__DATA_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__DATA_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__DATA_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__DATA_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM__DATA_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 19;

	/**
	 * The number of structural features of the '<em>Object All Values From</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 20;

	/**
	 * The number of operations of the '<em>Object All Values From</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ALL_VALUES_FROM_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectComplementOfImpl <em>Object Complement Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectComplementOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectComplementOf()
	 * @generated
	 */
	int OBJECT_COMPLEMENT_OF = 60;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__CLASS = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__OBJECT_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__OBJECT_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__OBJECT_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__OBJECT_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__OBJECT_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__OBJECT_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__OBJECT_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__OBJECT_HAS_SELF = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__OBJECT_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__OBJECT_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__OBJECT_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__DATA_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__DATA_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__DATA_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__DATA_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__DATA_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF__DATA_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 17;

	/**
	 * The number of structural features of the '<em>Object Complement Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 18;

	/**
	 * The number of operations of the '<em>Object Complement Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_COMPLEMENT_OF_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectExactCardinalityImpl <em>Object Exact Cardinality</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectExactCardinalityImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectExactCardinality()
	 * @generated
	 */
	int OBJECT_EXACT_CARDINALITY = 61;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_INVERSE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__CLASS = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_HAS_SELF = CLASS_EXPRESSION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__OBJECT_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__DATA_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__DATA_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__DATA_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__DATA_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__DATA_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__DATA_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY__CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 20;

	/**
	 * The number of structural features of the '<em>Object Exact Cardinality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 21;

	/**
	 * The number of operations of the '<em>Object Exact Cardinality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXACT_CARDINALITY_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectHasSelfImpl <em>Object Has Self</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectHasSelfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectHasSelf()
	 * @generated
	 */
	int OBJECT_HAS_SELF = 62;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_SELF__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_SELF__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_SELF__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_SELF__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_SELF__OBJECT_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_SELF__OBJECT_INVERSE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Object Has Self</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_SELF_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Object Has Self</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_SELF_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectHasValueImpl <em>Object Has Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectHasValueImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectHasValue()
	 * @generated
	 */
	int OBJECT_HAS_VALUE = 63;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_VALUE__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_VALUE__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_VALUE__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_VALUE__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_VALUE__OBJECT_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_VALUE__OBJECT_INVERSE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Named Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_VALUE__NAMED_INDIVIDUAL = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Anonymous Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_VALUE__ANONYMOUS_INDIVIDUAL = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Object Has Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_VALUE_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Object Has Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_HAS_VALUE_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectIntersectionOfImpl <em>Object Intersection Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectIntersectionOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectIntersectionOf()
	 * @generated
	 */
	int OBJECT_INTERSECTION_OF = 64;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Class Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__CLASS_EXPRESSION = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__CLASS = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__OBJECT_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__OBJECT_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__OBJECT_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__OBJECT_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__OBJECT_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__OBJECT_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__OBJECT_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__OBJECT_HAS_SELF = CLASS_EXPRESSION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__OBJECT_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__OBJECT_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__OBJECT_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__DATA_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__DATA_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__DATA_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__DATA_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__DATA_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF__DATA_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 18;

	/**
	 * The number of structural features of the '<em>Object Intersection Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 19;

	/**
	 * The number of operations of the '<em>Object Intersection Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INTERSECTION_OF_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectPropertyExpressionImpl <em>Object Property Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectPropertyExpressionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectPropertyExpression()
	 * @generated
	 */
	int OBJECT_PROPERTY_EXPRESSION = 74;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_EXPRESSION__BASE = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_EXPRESSION__ID = 1;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_EXPRESSION__LANG = 2;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_EXPRESSION__SPACE = 3;

	/**
	 * The number of structural features of the '<em>Object Property Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_EXPRESSION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Object Property Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectInverseOfImpl <em>Object Inverse Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectInverseOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectInverseOf()
	 * @generated
	 */
	int OBJECT_INVERSE_OF = 65;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INVERSE_OF__BASE = OBJECT_PROPERTY_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INVERSE_OF__ID = OBJECT_PROPERTY_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INVERSE_OF__LANG = OBJECT_PROPERTY_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INVERSE_OF__SPACE = OBJECT_PROPERTY_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INVERSE_OF__OBJECT_PROPERTY = OBJECT_PROPERTY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Object Inverse Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INVERSE_OF_FEATURE_COUNT = OBJECT_PROPERTY_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Object Inverse Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_INVERSE_OF_OPERATION_COUNT = OBJECT_PROPERTY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectMaxCardinalityImpl <em>Object Max Cardinality</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectMaxCardinalityImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectMaxCardinality()
	 * @generated
	 */
	int OBJECT_MAX_CARDINALITY = 66;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_INVERSE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__CLASS = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_HAS_SELF = CLASS_EXPRESSION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__OBJECT_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__DATA_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__DATA_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__DATA_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__DATA_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__DATA_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__DATA_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY__CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 20;

	/**
	 * The number of structural features of the '<em>Object Max Cardinality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 21;

	/**
	 * The number of operations of the '<em>Object Max Cardinality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MAX_CARDINALITY_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectMinCardinalityImpl <em>Object Min Cardinality</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectMinCardinalityImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectMinCardinality()
	 * @generated
	 */
	int OBJECT_MIN_CARDINALITY = 67;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_INVERSE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__CLASS = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_HAS_SELF = CLASS_EXPRESSION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__OBJECT_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__DATA_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__DATA_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__DATA_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__DATA_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__DATA_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__DATA_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY__CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 20;

	/**
	 * The number of structural features of the '<em>Object Min Cardinality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 21;

	/**
	 * The number of operations of the '<em>Object Min Cardinality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_MIN_CARDINALITY_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectOneOfImpl <em>Object One Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectOneOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectOneOf()
	 * @generated
	 */
	int OBJECT_ONE_OF = 68;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ONE_OF__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ONE_OF__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ONE_OF__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ONE_OF__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Individual</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ONE_OF__INDIVIDUAL = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Named Individual</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ONE_OF__NAMED_INDIVIDUAL = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Anonymous Individual</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ONE_OF__ANONYMOUS_INDIVIDUAL = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Object One Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ONE_OF_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Object One Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ONE_OF_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectPropertyImpl <em>Object Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectPropertyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectProperty()
	 * @generated
	 */
	int OBJECT_PROPERTY = 69;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY__BASE = OBJECT_PROPERTY_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY__ID = OBJECT_PROPERTY_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY__LANG = OBJECT_PROPERTY_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY__SPACE = OBJECT_PROPERTY_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Abbreviated IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY__ABBREVIATED_IRI = OBJECT_PROPERTY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY__IRI = OBJECT_PROPERTY_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_FEATURE_COUNT = OBJECT_PROPERTY_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_OPERATION_COUNT = OBJECT_PROPERTY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectPropertyAssertionImpl <em>Object Property Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectPropertyAssertionImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectPropertyAssertion()
	 * @generated
	 */
	int OBJECT_PROPERTY_ASSERTION = 70;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_ASSERTION__ANNOTATION = ASSERTION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_ASSERTION__BASE = ASSERTION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_ASSERTION__ID = ASSERTION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_ASSERTION__LANG = ASSERTION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_ASSERTION__SPACE = ASSERTION__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_ASSERTION__OBJECT_PROPERTY = ASSERTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_ASSERTION__OBJECT_INVERSE_OF = ASSERTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Named Individual</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_ASSERTION__NAMED_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Anonymous Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Anonymous Individual1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL1 = ASSERTION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Object Property Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_ASSERTION_FEATURE_COUNT = ASSERTION_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Object Property Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_ASSERTION_OPERATION_COUNT = ASSERTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectPropertyChainImpl <em>Object Property Chain</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectPropertyChainImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectPropertyChain()
	 * @generated
	 */
	int OBJECT_PROPERTY_CHAIN = 72;

	/**
	 * The feature id for the '<em><b>Object Property Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_CHAIN__OBJECT_PROPERTY_EXPRESSION = 0;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_CHAIN__OBJECT_PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_CHAIN__OBJECT_INVERSE_OF = 2;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_CHAIN__BASE = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_CHAIN__ID = 4;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_CHAIN__LANG = 5;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_CHAIN__SPACE = 6;

	/**
	 * The number of structural features of the '<em>Object Property Chain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_CHAIN_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Object Property Chain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_CHAIN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectPropertyDomainImpl <em>Object Property Domain</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectPropertyDomainImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectPropertyDomain()
	 * @generated
	 */
	int OBJECT_PROPERTY_DOMAIN = 73;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__CLASS = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_INTERSECTION_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_UNION_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_COMPLEMENT_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_ONE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_SOME_VALUES_FROM = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_ALL_VALUES_FROM = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_HAS_VALUE = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_HAS_SELF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_MIN_CARDINALITY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_MAX_CARDINALITY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__OBJECT_EXACT_CARDINALITY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__DATA_SOME_VALUES_FROM = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__DATA_ALL_VALUES_FROM = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__DATA_HAS_VALUE = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__DATA_MIN_CARDINALITY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__DATA_MAX_CARDINALITY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN__DATA_EXACT_CARDINALITY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 19;

	/**
	 * The number of structural features of the '<em>Object Property Domain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 20;

	/**
	 * The number of operations of the '<em>Object Property Domain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_DOMAIN_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectPropertyRangeImpl <em>Object Property Range</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectPropertyRangeImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectPropertyRange()
	 * @generated
	 */
	int OBJECT_PROPERTY_RANGE = 75;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__CLASS = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_INTERSECTION_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_UNION_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_COMPLEMENT_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_ONE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_SOME_VALUES_FROM = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_ALL_VALUES_FROM = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_HAS_VALUE = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_HAS_SELF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_MIN_CARDINALITY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_MAX_CARDINALITY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__OBJECT_EXACT_CARDINALITY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__DATA_SOME_VALUES_FROM = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__DATA_ALL_VALUES_FROM = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__DATA_HAS_VALUE = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__DATA_MIN_CARDINALITY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__DATA_MAX_CARDINALITY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE__DATA_EXACT_CARDINALITY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 19;

	/**
	 * The number of structural features of the '<em>Object Property Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 20;

	/**
	 * The number of operations of the '<em>Object Property Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PROPERTY_RANGE_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectSomeValuesFromImpl <em>Object Some Values From</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectSomeValuesFromImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectSomeValuesFrom()
	 * @generated
	 */
	int OBJECT_SOME_VALUES_FROM = 76;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_PROPERTY = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_INVERSE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__CLASS = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_HAS_SELF = CLASS_EXPRESSION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__OBJECT_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__DATA_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__DATA_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__DATA_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__DATA_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__DATA_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM__DATA_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 19;

	/**
	 * The number of structural features of the '<em>Object Some Values From</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 20;

	/**
	 * The number of operations of the '<em>Object Some Values From</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SOME_VALUES_FROM_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ObjectUnionOfImpl <em>Object Union Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ObjectUnionOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getObjectUnionOf()
	 * @generated
	 */
	int OBJECT_UNION_OF = 77;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__BASE = CLASS_EXPRESSION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__ID = CLASS_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__LANG = CLASS_EXPRESSION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__SPACE = CLASS_EXPRESSION__SPACE;

	/**
	 * The feature id for the '<em><b>Class Expression</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__CLASS_EXPRESSION = CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__CLASS = CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__OBJECT_INTERSECTION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__OBJECT_UNION_OF = CLASS_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__OBJECT_COMPLEMENT_OF = CLASS_EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__OBJECT_ONE_OF = CLASS_EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__OBJECT_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__OBJECT_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__OBJECT_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__OBJECT_HAS_SELF = CLASS_EXPRESSION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__OBJECT_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__OBJECT_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__OBJECT_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__DATA_SOME_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__DATA_ALL_VALUES_FROM = CLASS_EXPRESSION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__DATA_HAS_VALUE = CLASS_EXPRESSION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__DATA_MIN_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__DATA_MAX_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF__DATA_EXACT_CARDINALITY = CLASS_EXPRESSION_FEATURE_COUNT + 18;

	/**
	 * The number of structural features of the '<em>Object Union Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF_FEATURE_COUNT = CLASS_EXPRESSION_FEATURE_COUNT + 19;

	/**
	 * The number of operations of the '<em>Object Union Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_UNION_OF_OPERATION_COUNT = CLASS_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.OntologyImpl <em>Ontology</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.OntologyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getOntology()
	 * @generated
	 */
	int ONTOLOGY = 78;

	/**
	 * The feature id for the '<em><b>Prefix</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__PREFIX = 0;

	/**
	 * The feature id for the '<em><b>Import</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__IMPORT = 1;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__ANNOTATION = 2;

	/**
	 * The feature id for the '<em><b>Axiom</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__AXIOM = 3;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__DECLARATION = 4;

	/**
	 * The feature id for the '<em><b>Sub Class Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__SUB_CLASS_OF = 5;

	/**
	 * The feature id for the '<em><b>Equivalent Classes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__EQUIVALENT_CLASSES = 6;

	/**
	 * The feature id for the '<em><b>Disjoint Classes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__DISJOINT_CLASSES = 7;

	/**
	 * The feature id for the '<em><b>Disjoint Union</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__DISJOINT_UNION = 8;

	/**
	 * The feature id for the '<em><b>Sub Object Property Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__SUB_OBJECT_PROPERTY_OF = 9;

	/**
	 * The feature id for the '<em><b>Equivalent Object Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__EQUIVALENT_OBJECT_PROPERTIES = 10;

	/**
	 * The feature id for the '<em><b>Disjoint Object Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__DISJOINT_OBJECT_PROPERTIES = 11;

	/**
	 * The feature id for the '<em><b>Inverse Object Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__INVERSE_OBJECT_PROPERTIES = 12;

	/**
	 * The feature id for the '<em><b>Object Property Domain</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__OBJECT_PROPERTY_DOMAIN = 13;

	/**
	 * The feature id for the '<em><b>Object Property Range</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__OBJECT_PROPERTY_RANGE = 14;

	/**
	 * The feature id for the '<em><b>Functional Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__FUNCTIONAL_OBJECT_PROPERTY = 15;

	/**
	 * The feature id for the '<em><b>Inverse Functional Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__INVERSE_FUNCTIONAL_OBJECT_PROPERTY = 16;

	/**
	 * The feature id for the '<em><b>Reflexive Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__REFLEXIVE_OBJECT_PROPERTY = 17;

	/**
	 * The feature id for the '<em><b>Irreflexive Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__IRREFLEXIVE_OBJECT_PROPERTY = 18;

	/**
	 * The feature id for the '<em><b>Symmetric Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__SYMMETRIC_OBJECT_PROPERTY = 19;

	/**
	 * The feature id for the '<em><b>Asymmetric Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__ASYMMETRIC_OBJECT_PROPERTY = 20;

	/**
	 * The feature id for the '<em><b>Transitive Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__TRANSITIVE_OBJECT_PROPERTY = 21;

	/**
	 * The feature id for the '<em><b>Sub Data Property Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__SUB_DATA_PROPERTY_OF = 22;

	/**
	 * The feature id for the '<em><b>Equivalent Data Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__EQUIVALENT_DATA_PROPERTIES = 23;

	/**
	 * The feature id for the '<em><b>Disjoint Data Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__DISJOINT_DATA_PROPERTIES = 24;

	/**
	 * The feature id for the '<em><b>Data Property Domain</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__DATA_PROPERTY_DOMAIN = 25;

	/**
	 * The feature id for the '<em><b>Data Property Range</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__DATA_PROPERTY_RANGE = 26;

	/**
	 * The feature id for the '<em><b>Functional Data Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__FUNCTIONAL_DATA_PROPERTY = 27;

	/**
	 * The feature id for the '<em><b>Datatype Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__DATATYPE_DEFINITION = 28;

	/**
	 * The feature id for the '<em><b>Has Key</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__HAS_KEY = 29;

	/**
	 * The feature id for the '<em><b>Same Individual</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__SAME_INDIVIDUAL = 30;

	/**
	 * The feature id for the '<em><b>Different Individuals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__DIFFERENT_INDIVIDUALS = 31;

	/**
	 * The feature id for the '<em><b>Class Assertion</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__CLASS_ASSERTION = 32;

	/**
	 * The feature id for the '<em><b>Object Property Assertion</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__OBJECT_PROPERTY_ASSERTION = 33;

	/**
	 * The feature id for the '<em><b>Negative Object Property Assertion</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__NEGATIVE_OBJECT_PROPERTY_ASSERTION = 34;

	/**
	 * The feature id for the '<em><b>Data Property Assertion</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__DATA_PROPERTY_ASSERTION = 35;

	/**
	 * The feature id for the '<em><b>Negative Data Property Assertion</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__NEGATIVE_DATA_PROPERTY_ASSERTION = 36;

	/**
	 * The feature id for the '<em><b>Annotation Assertion</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__ANNOTATION_ASSERTION = 37;

	/**
	 * The feature id for the '<em><b>Sub Annotation Property Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__SUB_ANNOTATION_PROPERTY_OF = 38;

	/**
	 * The feature id for the '<em><b>Annotation Property Domain</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__ANNOTATION_PROPERTY_DOMAIN = 39;

	/**
	 * The feature id for the '<em><b>Annotation Property Range</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__ANNOTATION_PROPERTY_RANGE = 40;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__BASE = 41;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__ID = 42;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__LANG = 43;

	/**
	 * The feature id for the '<em><b>Ontology IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__ONTOLOGY_IRI = 44;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__SPACE = 45;

	/**
	 * The feature id for the '<em><b>Version IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__VERSION_IRI = 46;

	/**
	 * The number of structural features of the '<em>Ontology</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY_FEATURE_COUNT = 47;

	/**
	 * The number of operations of the '<em>Ontology</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.PrefixImpl <em>Prefix</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.PrefixImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getPrefix()
	 * @generated
	 */
	int PREFIX = 79;

	/**
	 * The feature id for the '<em><b>IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX__IRI = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX__NAME = 1;

	/**
	 * The number of structural features of the '<em>Prefix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Prefix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.ReflexiveObjectPropertyImpl <em>Reflexive Object Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.ReflexiveObjectPropertyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getReflexiveObjectProperty()
	 * @generated
	 */
	int REFLEXIVE_OBJECT_PROPERTY = 80;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFLEXIVE_OBJECT_PROPERTY__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFLEXIVE_OBJECT_PROPERTY__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFLEXIVE_OBJECT_PROPERTY__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFLEXIVE_OBJECT_PROPERTY__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFLEXIVE_OBJECT_PROPERTY__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFLEXIVE_OBJECT_PROPERTY__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFLEXIVE_OBJECT_PROPERTY__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Reflexive Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFLEXIVE_OBJECT_PROPERTY_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Reflexive Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFLEXIVE_OBJECT_PROPERTY_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.SameIndividualImpl <em>Same Individual</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.SameIndividualImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getSameIndividual()
	 * @generated
	 */
	int SAME_INDIVIDUAL = 81;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_INDIVIDUAL__ANNOTATION = ASSERTION__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_INDIVIDUAL__BASE = ASSERTION__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_INDIVIDUAL__ID = ASSERTION__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_INDIVIDUAL__LANG = ASSERTION__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_INDIVIDUAL__SPACE = ASSERTION__SPACE;

	/**
	 * The feature id for the '<em><b>Individual</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_INDIVIDUAL__INDIVIDUAL = ASSERTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Named Individual</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_INDIVIDUAL__NAMED_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Anonymous Individual</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_INDIVIDUAL__ANONYMOUS_INDIVIDUAL = ASSERTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Same Individual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_INDIVIDUAL_FEATURE_COUNT = ASSERTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Same Individual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_INDIVIDUAL_OPERATION_COUNT = ASSERTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.SubAnnotationPropertyOfImpl <em>Sub Annotation Property Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.SubAnnotationPropertyOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getSubAnnotationPropertyOf()
	 * @generated
	 */
	int SUB_ANNOTATION_PROPERTY_OF = 82;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ANNOTATION_PROPERTY_OF__ANNOTATION = ANNOTATION_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ANNOTATION_PROPERTY_OF__BASE = ANNOTATION_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ANNOTATION_PROPERTY_OF__ID = ANNOTATION_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ANNOTATION_PROPERTY_OF__LANG = ANNOTATION_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ANNOTATION_PROPERTY_OF__SPACE = ANNOTATION_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Annotation Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY = ANNOTATION_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Annotation Property1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY1 = ANNOTATION_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sub Annotation Property Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ANNOTATION_PROPERTY_OF_FEATURE_COUNT = ANNOTATION_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Sub Annotation Property Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ANNOTATION_PROPERTY_OF_OPERATION_COUNT = ANNOTATION_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.SubClassOfImpl <em>Sub Class Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.SubClassOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getSubClassOf()
	 * @generated
	 */
	int SUB_CLASS_OF = 83;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__ANNOTATION = CLASS_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__BASE = CLASS_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__ID = CLASS_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__LANG = CLASS_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__SPACE = CLASS_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__CLASS = CLASS_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_INTERSECTION_OF = CLASS_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_UNION_OF = CLASS_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_COMPLEMENT_OF = CLASS_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_ONE_OF = CLASS_AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_HAS_VALUE = CLASS_AXIOM_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_HAS_SELF = CLASS_AXIOM_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_MIN_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_MAX_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__DATA_SOME_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__DATA_ALL_VALUES_FROM = CLASS_AXIOM_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__DATA_HAS_VALUE = CLASS_AXIOM_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__DATA_MIN_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__DATA_MAX_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__DATA_EXACT_CARDINALITY = CLASS_AXIOM_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Object Intersection Of1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_INTERSECTION_OF1 = CLASS_AXIOM_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Object Union Of1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_UNION_OF1 = CLASS_AXIOM_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Object Complement Of1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_COMPLEMENT_OF1 = CLASS_AXIOM_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>Object One Of1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_ONE_OF1 = CLASS_AXIOM_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>Object Some Values From1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM1 = CLASS_AXIOM_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>Object All Values From1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM1 = CLASS_AXIOM_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>Object Has Value1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_HAS_VALUE1 = CLASS_AXIOM_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>Object Has Self1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_HAS_SELF1 = CLASS_AXIOM_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>Object Min Cardinality1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_MIN_CARDINALITY1 = CLASS_AXIOM_FEATURE_COUNT + 26;

	/**
	 * The feature id for the '<em><b>Object Max Cardinality1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_MAX_CARDINALITY1 = CLASS_AXIOM_FEATURE_COUNT + 27;

	/**
	 * The feature id for the '<em><b>Object Exact Cardinality1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY1 = CLASS_AXIOM_FEATURE_COUNT + 28;

	/**
	 * The feature id for the '<em><b>Data Some Values From1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__DATA_SOME_VALUES_FROM1 = CLASS_AXIOM_FEATURE_COUNT + 29;

	/**
	 * The feature id for the '<em><b>Data All Values From1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__DATA_ALL_VALUES_FROM1 = CLASS_AXIOM_FEATURE_COUNT + 30;

	/**
	 * The feature id for the '<em><b>Data Has Value1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__DATA_HAS_VALUE1 = CLASS_AXIOM_FEATURE_COUNT + 31;

	/**
	 * The feature id for the '<em><b>Data Min Cardinality1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__DATA_MIN_CARDINALITY1 = CLASS_AXIOM_FEATURE_COUNT + 32;

	/**
	 * The feature id for the '<em><b>Data Max Cardinality1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__DATA_MAX_CARDINALITY1 = CLASS_AXIOM_FEATURE_COUNT + 33;

	/**
	 * The feature id for the '<em><b>Data Exact Cardinality1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF__DATA_EXACT_CARDINALITY1 = CLASS_AXIOM_FEATURE_COUNT + 34;

	/**
	 * The number of structural features of the '<em>Sub Class Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF_FEATURE_COUNT = CLASS_AXIOM_FEATURE_COUNT + 35;

	/**
	 * The number of operations of the '<em>Sub Class Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_OF_OPERATION_COUNT = CLASS_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.SubDataPropertyOfImpl <em>Sub Data Property Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.SubDataPropertyOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getSubDataPropertyOf()
	 * @generated
	 */
	int SUB_DATA_PROPERTY_OF = 84;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DATA_PROPERTY_OF__ANNOTATION = DATA_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DATA_PROPERTY_OF__BASE = DATA_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DATA_PROPERTY_OF__ID = DATA_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DATA_PROPERTY_OF__LANG = DATA_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DATA_PROPERTY_OF__SPACE = DATA_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DATA_PROPERTY_OF__DATA_PROPERTY = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data Property1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DATA_PROPERTY_OF__DATA_PROPERTY1 = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sub Data Property Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DATA_PROPERTY_OF_FEATURE_COUNT = DATA_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Sub Data Property Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DATA_PROPERTY_OF_OPERATION_COUNT = DATA_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.SubObjectPropertyOfImpl <em>Sub Object Property Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.SubObjectPropertyOfImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getSubObjectPropertyOf()
	 * @generated
	 */
	int SUB_OBJECT_PROPERTY_OF = 85;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_OBJECT_PROPERTY_OF__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_OBJECT_PROPERTY_OF__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_OBJECT_PROPERTY_OF__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_OBJECT_PROPERTY_OF__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_OBJECT_PROPERTY_OF__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Property Chain</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY_CHAIN = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Inverse Of1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF1 = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Sub Object Property Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_OBJECT_PROPERTY_OF_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Sub Object Property Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_OBJECT_PROPERTY_OF_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.SymmetricObjectPropertyImpl <em>Symmetric Object Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.SymmetricObjectPropertyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getSymmetricObjectProperty()
	 * @generated
	 */
	int SYMMETRIC_OBJECT_PROPERTY = 86;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMMETRIC_OBJECT_PROPERTY__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMMETRIC_OBJECT_PROPERTY__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMMETRIC_OBJECT_PROPERTY__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMMETRIC_OBJECT_PROPERTY__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMMETRIC_OBJECT_PROPERTY__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMMETRIC_OBJECT_PROPERTY__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMMETRIC_OBJECT_PROPERTY__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Symmetric Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMMETRIC_OBJECT_PROPERTY_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Symmetric Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMMETRIC_OBJECT_PROPERTY_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.w3._2002._07.owl.impl.TransitiveObjectPropertyImpl <em>Transitive Object Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3._2002._07.owl.impl.TransitiveObjectPropertyImpl
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getTransitiveObjectProperty()
	 * @generated
	 */
	int TRANSITIVE_OBJECT_PROPERTY = 87;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITIVE_OBJECT_PROPERTY__ANNOTATION = OBJECT_PROPERTY_AXIOM__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITIVE_OBJECT_PROPERTY__BASE = OBJECT_PROPERTY_AXIOM__BASE;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITIVE_OBJECT_PROPERTY__ID = OBJECT_PROPERTY_AXIOM__ID;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITIVE_OBJECT_PROPERTY__LANG = OBJECT_PROPERTY_AXIOM__LANG;

	/**
	 * The feature id for the '<em><b>Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITIVE_OBJECT_PROPERTY__SPACE = OBJECT_PROPERTY_AXIOM__SPACE;

	/**
	 * The feature id for the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITIVE_OBJECT_PROPERTY__OBJECT_PROPERTY = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITIVE_OBJECT_PROPERTY__OBJECT_INVERSE_OF = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Transitive Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITIVE_OBJECT_PROPERTY_FEATURE_COUNT = OBJECT_PROPERTY_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Transitive Object Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITIVE_OBJECT_PROPERTY_OPERATION_COUNT = OBJECT_PROPERTY_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '<em>Abbreviated IRI</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getAbbreviatedIRI()
	 * @generated
	 */
	int ABBREVIATED_IRI = 88;

	/**
	 * The meta object id for the '<em>Name Type</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see org.w3._2002._07.owl.impl.OwlPackageImpl#getNameType()
	 * @generated
	 */
	int NAME_TYPE = 89;


	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.AbbreviatedIRI1 <em>Abbreviated IRI1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abbreviated IRI1</em>'.
	 * @see org.w3._2002._07.owl.AbbreviatedIRI1
	 * @generated
	 */
	EClass getAbbreviatedIRI1();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.AbbreviatedIRI1#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.w3._2002._07.owl.AbbreviatedIRI1#getValue()
	 * @see #getAbbreviatedIRI1()
	 * @generated
	 */
	EAttribute getAbbreviatedIRI1_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.AbbreviatedIRI1#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.AbbreviatedIRI1#getBase()
	 * @see #getAbbreviatedIRI1()
	 * @generated
	 */
	EAttribute getAbbreviatedIRI1_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.AbbreviatedIRI1#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.AbbreviatedIRI1#getId()
	 * @see #getAbbreviatedIRI1()
	 * @generated
	 */
	EAttribute getAbbreviatedIRI1_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.AbbreviatedIRI1#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.AbbreviatedIRI1#getLang()
	 * @see #getAbbreviatedIRI1()
	 * @generated
	 */
	EAttribute getAbbreviatedIRI1_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.AbbreviatedIRI1#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.AbbreviatedIRI1#getSpace()
	 * @see #getAbbreviatedIRI1()
	 * @generated
	 */
	EAttribute getAbbreviatedIRI1_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation</em>'.
	 * @see org.w3._2002._07.owl.Annotation
	 * @generated
	 */
	EClass getAnnotation();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Annotation#getAnnotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotation</em>'.
	 * @see org.w3._2002._07.owl.Annotation#getAnnotation()
	 * @see #getAnnotation()
	 * @generated
	 */
	EReference getAnnotation_Annotation();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.Annotation#getAnnotationProperty <em>Annotation Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Property</em>'.
	 * @see org.w3._2002._07.owl.Annotation#getAnnotationProperty()
	 * @see #getAnnotation()
	 * @generated
	 */
	EReference getAnnotation_AnnotationProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.Annotation#getIRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.Annotation#getIRI()
	 * @see #getAnnotation()
	 * @generated
	 */
	EReference getAnnotation_IRI();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.Annotation#getAbbreviatedIRI <em>Abbreviated IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Abbreviated IRI</em>'.
	 * @see org.w3._2002._07.owl.Annotation#getAbbreviatedIRI()
	 * @see #getAnnotation()
	 * @generated
	 */
	EReference getAnnotation_AbbreviatedIRI();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.Annotation#getAnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.Annotation#getAnonymousIndividual()
	 * @see #getAnnotation()
	 * @generated
	 */
	EReference getAnnotation_AnonymousIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.Annotation#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Literal</em>'.
	 * @see org.w3._2002._07.owl.Annotation#getLiteral()
	 * @see #getAnnotation()
	 * @generated
	 */
	EReference getAnnotation_Literal();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Annotation#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.Annotation#getBase()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Annotation#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.Annotation#getId()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Annotation#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.Annotation#getLang()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Annotation#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.Annotation#getSpace()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.AnnotationAssertion <em>Annotation Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation Assertion</em>'.
	 * @see org.w3._2002._07.owl.AnnotationAssertion
	 * @generated
	 */
	EClass getAnnotationAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationAssertion#getAnnotationProperty <em>Annotation Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Property</em>'.
	 * @see org.w3._2002._07.owl.AnnotationAssertion#getAnnotationProperty()
	 * @see #getAnnotationAssertion()
	 * @generated
	 */
	EReference getAnnotationAssertion_AnnotationProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationAssertion#getIRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.AnnotationAssertion#getIRI()
	 * @see #getAnnotationAssertion()
	 * @generated
	 */
	EReference getAnnotationAssertion_IRI();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationAssertion#getAbbreviatedIRI <em>Abbreviated IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Abbreviated IRI</em>'.
	 * @see org.w3._2002._07.owl.AnnotationAssertion#getAbbreviatedIRI()
	 * @see #getAnnotationAssertion()
	 * @generated
	 */
	EReference getAnnotationAssertion_AbbreviatedIRI();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationAssertion#getAnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.AnnotationAssertion#getAnonymousIndividual()
	 * @see #getAnnotationAssertion()
	 * @generated
	 */
	EReference getAnnotationAssertion_AnonymousIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationAssertion#getIRI1 <em>IRI1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IRI1</em>'.
	 * @see org.w3._2002._07.owl.AnnotationAssertion#getIRI1()
	 * @see #getAnnotationAssertion()
	 * @generated
	 */
	EReference getAnnotationAssertion_IRI1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationAssertion#getAbbreviatedIRI1 <em>Abbreviated IRI1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Abbreviated IRI1</em>'.
	 * @see org.w3._2002._07.owl.AnnotationAssertion#getAbbreviatedIRI1()
	 * @see #getAnnotationAssertion()
	 * @generated
	 */
	EReference getAnnotationAssertion_AbbreviatedIRI1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationAssertion#getAnonymousIndividual1 <em>Anonymous Individual1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Anonymous Individual1</em>'.
	 * @see org.w3._2002._07.owl.AnnotationAssertion#getAnonymousIndividual1()
	 * @see #getAnnotationAssertion()
	 * @generated
	 */
	EReference getAnnotationAssertion_AnonymousIndividual1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationAssertion#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Literal</em>'.
	 * @see org.w3._2002._07.owl.AnnotationAssertion#getLiteral()
	 * @see #getAnnotationAssertion()
	 * @generated
	 */
	EReference getAnnotationAssertion_Literal();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.AnnotationAxiom <em>Annotation Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation Axiom</em>'.
	 * @see org.w3._2002._07.owl.AnnotationAxiom
	 * @generated
	 */
	EClass getAnnotationAxiom();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.AnnotationProperty <em>Annotation Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation Property</em>'.
	 * @see org.w3._2002._07.owl.AnnotationProperty
	 * @generated
	 */
	EClass getAnnotationProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.AnnotationProperty#getAbbreviatedIRI <em>Abbreviated IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abbreviated IRI</em>'.
	 * @see org.w3._2002._07.owl.AnnotationProperty#getAbbreviatedIRI()
	 * @see #getAnnotationProperty()
	 * @generated
	 */
	EAttribute getAnnotationProperty_AbbreviatedIRI();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.AnnotationProperty#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.AnnotationProperty#getBase()
	 * @see #getAnnotationProperty()
	 * @generated
	 */
	EAttribute getAnnotationProperty_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.AnnotationProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.AnnotationProperty#getId()
	 * @see #getAnnotationProperty()
	 * @generated
	 */
	EAttribute getAnnotationProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.AnnotationProperty#getIRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.AnnotationProperty#getIRI()
	 * @see #getAnnotationProperty()
	 * @generated
	 */
	EAttribute getAnnotationProperty_IRI();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.AnnotationProperty#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.AnnotationProperty#getLang()
	 * @see #getAnnotationProperty()
	 * @generated
	 */
	EAttribute getAnnotationProperty_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.AnnotationProperty#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.AnnotationProperty#getSpace()
	 * @see #getAnnotationProperty()
	 * @generated
	 */
	EAttribute getAnnotationProperty_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.AnnotationPropertyDomain <em>Annotation Property Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation Property Domain</em>'.
	 * @see org.w3._2002._07.owl.AnnotationPropertyDomain
	 * @generated
	 */
	EClass getAnnotationPropertyDomain();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationPropertyDomain#getAnnotationProperty <em>Annotation Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Property</em>'.
	 * @see org.w3._2002._07.owl.AnnotationPropertyDomain#getAnnotationProperty()
	 * @see #getAnnotationPropertyDomain()
	 * @generated
	 */
	EReference getAnnotationPropertyDomain_AnnotationProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationPropertyDomain#getIRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.AnnotationPropertyDomain#getIRI()
	 * @see #getAnnotationPropertyDomain()
	 * @generated
	 */
	EReference getAnnotationPropertyDomain_IRI();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationPropertyDomain#getAbbreviatedIRI <em>Abbreviated IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Abbreviated IRI</em>'.
	 * @see org.w3._2002._07.owl.AnnotationPropertyDomain#getAbbreviatedIRI()
	 * @see #getAnnotationPropertyDomain()
	 * @generated
	 */
	EReference getAnnotationPropertyDomain_AbbreviatedIRI();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.AnnotationPropertyRange <em>Annotation Property Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation Property Range</em>'.
	 * @see org.w3._2002._07.owl.AnnotationPropertyRange
	 * @generated
	 */
	EClass getAnnotationPropertyRange();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationPropertyRange#getAnnotationProperty <em>Annotation Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Property</em>'.
	 * @see org.w3._2002._07.owl.AnnotationPropertyRange#getAnnotationProperty()
	 * @see #getAnnotationPropertyRange()
	 * @generated
	 */
	EReference getAnnotationPropertyRange_AnnotationProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationPropertyRange#getIRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.AnnotationPropertyRange#getIRI()
	 * @see #getAnnotationPropertyRange()
	 * @generated
	 */
	EReference getAnnotationPropertyRange_IRI();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AnnotationPropertyRange#getAbbreviatedIRI <em>Abbreviated IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Abbreviated IRI</em>'.
	 * @see org.w3._2002._07.owl.AnnotationPropertyRange#getAbbreviatedIRI()
	 * @see #getAnnotationPropertyRange()
	 * @generated
	 */
	EReference getAnnotationPropertyRange_AbbreviatedIRI();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.AnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.AnonymousIndividual
	 * @generated
	 */
	EClass getAnonymousIndividual();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.AnonymousIndividual#getNodeID <em>Node ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Node ID</em>'.
	 * @see org.w3._2002._07.owl.AnonymousIndividual#getNodeID()
	 * @see #getAnonymousIndividual()
	 * @generated
	 */
	EAttribute getAnonymousIndividual_NodeID();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.Assertion <em>Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assertion</em>'.
	 * @see org.w3._2002._07.owl.Assertion
	 * @generated
	 */
	EClass getAssertion();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.AsymmetricObjectProperty <em>Asymmetric Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asymmetric Object Property</em>'.
	 * @see org.w3._2002._07.owl.AsymmetricObjectProperty
	 * @generated
	 */
	EClass getAsymmetricObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AsymmetricObjectProperty#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.AsymmetricObjectProperty#getObjectProperty()
	 * @see #getAsymmetricObjectProperty()
	 * @generated
	 */
	EReference getAsymmetricObjectProperty_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.AsymmetricObjectProperty#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.AsymmetricObjectProperty#getObjectInverseOf()
	 * @see #getAsymmetricObjectProperty()
	 * @generated
	 */
	EReference getAsymmetricObjectProperty_ObjectInverseOf();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.Axiom <em>Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Axiom</em>'.
	 * @see org.w3._2002._07.owl.Axiom
	 * @generated
	 */
	EClass getAxiom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Axiom#getAnnotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotation</em>'.
	 * @see org.w3._2002._07.owl.Axiom#getAnnotation()
	 * @see #getAxiom()
	 * @generated
	 */
	EReference getAxiom_Annotation();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Axiom#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.Axiom#getBase()
	 * @see #getAxiom()
	 * @generated
	 */
	EAttribute getAxiom_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Axiom#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.Axiom#getId()
	 * @see #getAxiom()
	 * @generated
	 */
	EAttribute getAxiom_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Axiom#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.Axiom#getLang()
	 * @see #getAxiom()
	 * @generated
	 */
	EAttribute getAxiom_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Axiom#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.Axiom#getSpace()
	 * @see #getAxiom()
	 * @generated
	 */
	EAttribute getAxiom_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.Class <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.Class
	 * @generated
	 */
	EClass getClass_();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Class#getAbbreviatedIRI <em>Abbreviated IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abbreviated IRI</em>'.
	 * @see org.w3._2002._07.owl.Class#getAbbreviatedIRI()
	 * @see #getClass_()
	 * @generated
	 */
	EAttribute getClass_AbbreviatedIRI();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Class#getIRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.Class#getIRI()
	 * @see #getClass_()
	 * @generated
	 */
	EAttribute getClass_IRI();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ClassAssertion <em>Class Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Assertion</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion
	 * @generated
	 */
	EClass getClassAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getClass_()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getObjectIntersectionOf()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getObjectUnionOf()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getObjectComplementOf()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getObjectOneOf()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getObjectSomeValuesFrom()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getObjectAllValuesFrom()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getObjectHasValue()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getObjectHasSelf()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getObjectMinCardinality()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getObjectMaxCardinality()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getObjectExactCardinality()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getDataSomeValuesFrom()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getDataAllValuesFrom()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getDataHasValue()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getDataMinCardinality()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getDataMaxCardinality()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getDataExactCardinality()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_DataExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getNamedIndividual <em>Named Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Named Individual</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getNamedIndividual()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_NamedIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ClassAssertion#getAnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.ClassAssertion#getAnonymousIndividual()
	 * @see #getClassAssertion()
	 * @generated
	 */
	EReference getClassAssertion_AnonymousIndividual();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ClassAxiom <em>Class Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Axiom</em>'.
	 * @see org.w3._2002._07.owl.ClassAxiom
	 * @generated
	 */
	EClass getClassAxiom();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ClassExpression <em>Class Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Expression</em>'.
	 * @see org.w3._2002._07.owl.ClassExpression
	 * @generated
	 */
	EClass getClassExpression();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ClassExpression#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.ClassExpression#getBase()
	 * @see #getClassExpression()
	 * @generated
	 */
	EAttribute getClassExpression_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ClassExpression#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.ClassExpression#getId()
	 * @see #getClassExpression()
	 * @generated
	 */
	EAttribute getClassExpression_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ClassExpression#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.ClassExpression#getLang()
	 * @see #getClassExpression()
	 * @generated
	 */
	EAttribute getClassExpression_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ClassExpression#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.ClassExpression#getSpace()
	 * @see #getClassExpression()
	 * @generated
	 */
	EAttribute getClassExpression_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.DataAllValuesFrom
	 * @generated
	 */
	EClass getDataAllValuesFrom();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataPropertyExpression <em>Data Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Data Property Expression</em>'.
	 * @see org.w3._2002._07.owl.DataAllValuesFrom#getDataPropertyExpression()
	 * @see #getDataAllValuesFrom()
	 * @generated
	 */
	EAttribute getDataAllValuesFrom_DataPropertyExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.DataAllValuesFrom#getDataProperty()
	 * @see #getDataAllValuesFrom()
	 * @generated
	 */
	EReference getDataAllValuesFrom_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.DataAllValuesFrom#getDatatype()
	 * @see #getDataAllValuesFrom()
	 * @generated
	 */
	EReference getDataAllValuesFrom_Datatype();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataIntersectionOf <em>Data Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DataAllValuesFrom#getDataIntersectionOf()
	 * @see #getDataAllValuesFrom()
	 * @generated
	 */
	EReference getDataAllValuesFrom_DataIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataUnionOf <em>Data Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Union Of</em>'.
	 * @see org.w3._2002._07.owl.DataAllValuesFrom#getDataUnionOf()
	 * @see #getDataAllValuesFrom()
	 * @generated
	 */
	EReference getDataAllValuesFrom_DataUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataComplementOf <em>Data Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DataAllValuesFrom#getDataComplementOf()
	 * @see #getDataAllValuesFrom()
	 * @generated
	 */
	EReference getDataAllValuesFrom_DataComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDataOneOf <em>Data One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data One Of</em>'.
	 * @see org.w3._2002._07.owl.DataAllValuesFrom#getDataOneOf()
	 * @see #getDataAllValuesFrom()
	 * @generated
	 */
	EReference getDataAllValuesFrom_DataOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataAllValuesFrom#getDatatypeRestriction <em>Datatype Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype Restriction</em>'.
	 * @see org.w3._2002._07.owl.DataAllValuesFrom#getDatatypeRestriction()
	 * @see #getDataAllValuesFrom()
	 * @generated
	 */
	EReference getDataAllValuesFrom_DatatypeRestriction();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataComplementOf <em>Data Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DataComplementOf
	 * @generated
	 */
	EClass getDataComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataComplementOf#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.DataComplementOf#getDatatype()
	 * @see #getDataComplementOf()
	 * @generated
	 */
	EReference getDataComplementOf_Datatype();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataComplementOf#getDataIntersectionOf <em>Data Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DataComplementOf#getDataIntersectionOf()
	 * @see #getDataComplementOf()
	 * @generated
	 */
	EReference getDataComplementOf_DataIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataComplementOf#getDataUnionOf <em>Data Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Union Of</em>'.
	 * @see org.w3._2002._07.owl.DataComplementOf#getDataUnionOf()
	 * @see #getDataComplementOf()
	 * @generated
	 */
	EReference getDataComplementOf_DataUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataComplementOf#getDataComplementOf <em>Data Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DataComplementOf#getDataComplementOf()
	 * @see #getDataComplementOf()
	 * @generated
	 */
	EReference getDataComplementOf_DataComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataComplementOf#getDataOneOf <em>Data One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data One Of</em>'.
	 * @see org.w3._2002._07.owl.DataComplementOf#getDataOneOf()
	 * @see #getDataComplementOf()
	 * @generated
	 */
	EReference getDataComplementOf_DataOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataComplementOf#getDatatypeRestriction <em>Datatype Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype Restriction</em>'.
	 * @see org.w3._2002._07.owl.DataComplementOf#getDatatypeRestriction()
	 * @see #getDataComplementOf()
	 * @generated
	 */
	EReference getDataComplementOf_DatatypeRestriction();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DataExactCardinality
	 * @generated
	 */
	EClass getDataExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataExactCardinality#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.DataExactCardinality#getDataProperty()
	 * @see #getDataExactCardinality()
	 * @generated
	 */
	EReference getDataExactCardinality_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataExactCardinality#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.DataExactCardinality#getDatatype()
	 * @see #getDataExactCardinality()
	 * @generated
	 */
	EReference getDataExactCardinality_Datatype();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataExactCardinality#getDataIntersectionOf <em>Data Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DataExactCardinality#getDataIntersectionOf()
	 * @see #getDataExactCardinality()
	 * @generated
	 */
	EReference getDataExactCardinality_DataIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataExactCardinality#getDataUnionOf <em>Data Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Union Of</em>'.
	 * @see org.w3._2002._07.owl.DataExactCardinality#getDataUnionOf()
	 * @see #getDataExactCardinality()
	 * @generated
	 */
	EReference getDataExactCardinality_DataUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataExactCardinality#getDataComplementOf <em>Data Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DataExactCardinality#getDataComplementOf()
	 * @see #getDataExactCardinality()
	 * @generated
	 */
	EReference getDataExactCardinality_DataComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataExactCardinality#getDataOneOf <em>Data One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data One Of</em>'.
	 * @see org.w3._2002._07.owl.DataExactCardinality#getDataOneOf()
	 * @see #getDataExactCardinality()
	 * @generated
	 */
	EReference getDataExactCardinality_DataOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataExactCardinality#getDatatypeRestriction <em>Datatype Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype Restriction</em>'.
	 * @see org.w3._2002._07.owl.DataExactCardinality#getDatatypeRestriction()
	 * @see #getDataExactCardinality()
	 * @generated
	 */
	EReference getDataExactCardinality_DatatypeRestriction();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataExactCardinality#getCardinality <em>Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DataExactCardinality#getCardinality()
	 * @see #getDataExactCardinality()
	 * @generated
	 */
	EAttribute getDataExactCardinality_Cardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.DataHasValue
	 * @generated
	 */
	EClass getDataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataHasValue#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.DataHasValue#getDataProperty()
	 * @see #getDataHasValue()
	 * @generated
	 */
	EReference getDataHasValue_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataHasValue#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Literal</em>'.
	 * @see org.w3._2002._07.owl.DataHasValue#getLiteral()
	 * @see #getDataHasValue()
	 * @generated
	 */
	EReference getDataHasValue_Literal();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataIntersectionOf <em>Data Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DataIntersectionOf
	 * @generated
	 */
	EClass getDataIntersectionOf();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.DataIntersectionOf#getDataRange <em>Data Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Data Range</em>'.
	 * @see org.w3._2002._07.owl.DataIntersectionOf#getDataRange()
	 * @see #getDataIntersectionOf()
	 * @generated
	 */
	EAttribute getDataIntersectionOf_DataRange();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataIntersectionOf#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.DataIntersectionOf#getDatatype()
	 * @see #getDataIntersectionOf()
	 * @generated
	 */
	EReference getDataIntersectionOf_Datatype();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataIntersectionOf#getDataIntersectionOf <em>Data Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DataIntersectionOf#getDataIntersectionOf()
	 * @see #getDataIntersectionOf()
	 * @generated
	 */
	EReference getDataIntersectionOf_DataIntersectionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataIntersectionOf#getDataUnionOf <em>Data Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Union Of</em>'.
	 * @see org.w3._2002._07.owl.DataIntersectionOf#getDataUnionOf()
	 * @see #getDataIntersectionOf()
	 * @generated
	 */
	EReference getDataIntersectionOf_DataUnionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataIntersectionOf#getDataComplementOf <em>Data Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DataIntersectionOf#getDataComplementOf()
	 * @see #getDataIntersectionOf()
	 * @generated
	 */
	EReference getDataIntersectionOf_DataComplementOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataIntersectionOf#getDataOneOf <em>Data One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data One Of</em>'.
	 * @see org.w3._2002._07.owl.DataIntersectionOf#getDataOneOf()
	 * @see #getDataIntersectionOf()
	 * @generated
	 */
	EReference getDataIntersectionOf_DataOneOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataIntersectionOf#getDatatypeRestriction <em>Datatype Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Datatype Restriction</em>'.
	 * @see org.w3._2002._07.owl.DataIntersectionOf#getDatatypeRestriction()
	 * @see #getDataIntersectionOf()
	 * @generated
	 */
	EReference getDataIntersectionOf_DatatypeRestriction();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DataMaxCardinality
	 * @generated
	 */
	EClass getDataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMaxCardinality#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.DataMaxCardinality#getDataProperty()
	 * @see #getDataMaxCardinality()
	 * @generated
	 */
	EReference getDataMaxCardinality_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMaxCardinality#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.DataMaxCardinality#getDatatype()
	 * @see #getDataMaxCardinality()
	 * @generated
	 */
	EReference getDataMaxCardinality_Datatype();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMaxCardinality#getDataIntersectionOf <em>Data Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DataMaxCardinality#getDataIntersectionOf()
	 * @see #getDataMaxCardinality()
	 * @generated
	 */
	EReference getDataMaxCardinality_DataIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMaxCardinality#getDataUnionOf <em>Data Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Union Of</em>'.
	 * @see org.w3._2002._07.owl.DataMaxCardinality#getDataUnionOf()
	 * @see #getDataMaxCardinality()
	 * @generated
	 */
	EReference getDataMaxCardinality_DataUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMaxCardinality#getDataComplementOf <em>Data Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DataMaxCardinality#getDataComplementOf()
	 * @see #getDataMaxCardinality()
	 * @generated
	 */
	EReference getDataMaxCardinality_DataComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMaxCardinality#getDataOneOf <em>Data One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data One Of</em>'.
	 * @see org.w3._2002._07.owl.DataMaxCardinality#getDataOneOf()
	 * @see #getDataMaxCardinality()
	 * @generated
	 */
	EReference getDataMaxCardinality_DataOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMaxCardinality#getDatatypeRestriction <em>Datatype Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype Restriction</em>'.
	 * @see org.w3._2002._07.owl.DataMaxCardinality#getDatatypeRestriction()
	 * @see #getDataMaxCardinality()
	 * @generated
	 */
	EReference getDataMaxCardinality_DatatypeRestriction();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataMaxCardinality#getCardinality <em>Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DataMaxCardinality#getCardinality()
	 * @see #getDataMaxCardinality()
	 * @generated
	 */
	EAttribute getDataMaxCardinality_Cardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DataMinCardinality
	 * @generated
	 */
	EClass getDataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMinCardinality#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.DataMinCardinality#getDataProperty()
	 * @see #getDataMinCardinality()
	 * @generated
	 */
	EReference getDataMinCardinality_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMinCardinality#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.DataMinCardinality#getDatatype()
	 * @see #getDataMinCardinality()
	 * @generated
	 */
	EReference getDataMinCardinality_Datatype();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMinCardinality#getDataIntersectionOf <em>Data Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DataMinCardinality#getDataIntersectionOf()
	 * @see #getDataMinCardinality()
	 * @generated
	 */
	EReference getDataMinCardinality_DataIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMinCardinality#getDataUnionOf <em>Data Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Union Of</em>'.
	 * @see org.w3._2002._07.owl.DataMinCardinality#getDataUnionOf()
	 * @see #getDataMinCardinality()
	 * @generated
	 */
	EReference getDataMinCardinality_DataUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMinCardinality#getDataComplementOf <em>Data Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DataMinCardinality#getDataComplementOf()
	 * @see #getDataMinCardinality()
	 * @generated
	 */
	EReference getDataMinCardinality_DataComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMinCardinality#getDataOneOf <em>Data One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data One Of</em>'.
	 * @see org.w3._2002._07.owl.DataMinCardinality#getDataOneOf()
	 * @see #getDataMinCardinality()
	 * @generated
	 */
	EReference getDataMinCardinality_DataOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataMinCardinality#getDatatypeRestriction <em>Datatype Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype Restriction</em>'.
	 * @see org.w3._2002._07.owl.DataMinCardinality#getDatatypeRestriction()
	 * @see #getDataMinCardinality()
	 * @generated
	 */
	EReference getDataMinCardinality_DatatypeRestriction();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataMinCardinality#getCardinality <em>Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DataMinCardinality#getCardinality()
	 * @see #getDataMinCardinality()
	 * @generated
	 */
	EAttribute getDataMinCardinality_Cardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataOneOf <em>Data One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data One Of</em>'.
	 * @see org.w3._2002._07.owl.DataOneOf
	 * @generated
	 */
	EClass getDataOneOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataOneOf#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Literal</em>'.
	 * @see org.w3._2002._07.owl.DataOneOf#getLiteral()
	 * @see #getDataOneOf()
	 * @generated
	 */
	EReference getDataOneOf_Literal();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.DataProperty
	 * @generated
	 */
	EClass getDataProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataProperty#getAbbreviatedIRI <em>Abbreviated IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abbreviated IRI</em>'.
	 * @see org.w3._2002._07.owl.DataProperty#getAbbreviatedIRI()
	 * @see #getDataProperty()
	 * @generated
	 */
	EAttribute getDataProperty_AbbreviatedIRI();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataProperty#getIRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.DataProperty#getIRI()
	 * @see #getDataProperty()
	 * @generated
	 */
	EAttribute getDataProperty_IRI();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataPropertyAssertion <em>Data Property Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Property Assertion</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyAssertion
	 * @generated
	 */
	EClass getDataPropertyAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyAssertion#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyAssertion#getDataProperty()
	 * @see #getDataPropertyAssertion()
	 * @generated
	 */
	EReference getDataPropertyAssertion_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyAssertion#getNamedIndividual <em>Named Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Named Individual</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyAssertion#getNamedIndividual()
	 * @see #getDataPropertyAssertion()
	 * @generated
	 */
	EReference getDataPropertyAssertion_NamedIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyAssertion#getAnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyAssertion#getAnonymousIndividual()
	 * @see #getDataPropertyAssertion()
	 * @generated
	 */
	EReference getDataPropertyAssertion_AnonymousIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyAssertion#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Literal</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyAssertion#getLiteral()
	 * @see #getDataPropertyAssertion()
	 * @generated
	 */
	EReference getDataPropertyAssertion_Literal();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataPropertyAxiom <em>Data Property Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Property Axiom</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyAxiom
	 * @generated
	 */
	EClass getDataPropertyAxiom();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataPropertyDomain <em>Data Property Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Property Domain</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain
	 * @generated
	 */
	EClass getDataPropertyDomain();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getDataProperty()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getClass_()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getObjectIntersectionOf()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getObjectUnionOf()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getObjectComplementOf()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getObjectOneOf()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getObjectSomeValuesFrom()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getObjectAllValuesFrom()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getObjectHasValue()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getObjectHasSelf()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getObjectMinCardinality()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getObjectMaxCardinality()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getObjectExactCardinality()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getDataSomeValuesFrom()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getDataAllValuesFrom()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getDataHasValue()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getDataMinCardinality()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getDataMaxCardinality()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyDomain#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyDomain#getDataExactCardinality()
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	EReference getDataPropertyDomain_DataExactCardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataPropertyExpression <em>Data Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Property Expression</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyExpression
	 * @generated
	 */
	EClass getDataPropertyExpression();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataPropertyExpression#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyExpression#getBase()
	 * @see #getDataPropertyExpression()
	 * @generated
	 */
	EAttribute getDataPropertyExpression_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataPropertyExpression#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyExpression#getId()
	 * @see #getDataPropertyExpression()
	 * @generated
	 */
	EAttribute getDataPropertyExpression_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataPropertyExpression#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyExpression#getLang()
	 * @see #getDataPropertyExpression()
	 * @generated
	 */
	EAttribute getDataPropertyExpression_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataPropertyExpression#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyExpression#getSpace()
	 * @see #getDataPropertyExpression()
	 * @generated
	 */
	EAttribute getDataPropertyExpression_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataPropertyRange <em>Data Property Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Property Range</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyRange
	 * @generated
	 */
	EClass getDataPropertyRange();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyRange#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyRange#getDataProperty()
	 * @see #getDataPropertyRange()
	 * @generated
	 */
	EReference getDataPropertyRange_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyRange#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyRange#getDatatype()
	 * @see #getDataPropertyRange()
	 * @generated
	 */
	EReference getDataPropertyRange_Datatype();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyRange#getDataIntersectionOf <em>Data Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyRange#getDataIntersectionOf()
	 * @see #getDataPropertyRange()
	 * @generated
	 */
	EReference getDataPropertyRange_DataIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyRange#getDataUnionOf <em>Data Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Union Of</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyRange#getDataUnionOf()
	 * @see #getDataPropertyRange()
	 * @generated
	 */
	EReference getDataPropertyRange_DataUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyRange#getDataComplementOf <em>Data Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyRange#getDataComplementOf()
	 * @see #getDataPropertyRange()
	 * @generated
	 */
	EReference getDataPropertyRange_DataComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyRange#getDataOneOf <em>Data One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data One Of</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyRange#getDataOneOf()
	 * @see #getDataPropertyRange()
	 * @generated
	 */
	EReference getDataPropertyRange_DataOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataPropertyRange#getDatatypeRestriction <em>Datatype Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype Restriction</em>'.
	 * @see org.w3._2002._07.owl.DataPropertyRange#getDatatypeRestriction()
	 * @see #getDataPropertyRange()
	 * @generated
	 */
	EReference getDataPropertyRange_DatatypeRestriction();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataRange <em>Data Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Range</em>'.
	 * @see org.w3._2002._07.owl.DataRange
	 * @generated
	 */
	EClass getDataRange();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataRange#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.DataRange#getBase()
	 * @see #getDataRange()
	 * @generated
	 */
	EAttribute getDataRange_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataRange#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.DataRange#getId()
	 * @see #getDataRange()
	 * @generated
	 */
	EAttribute getDataRange_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataRange#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.DataRange#getLang()
	 * @see #getDataRange()
	 * @generated
	 */
	EAttribute getDataRange_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.DataRange#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.DataRange#getSpace()
	 * @see #getDataRange()
	 * @generated
	 */
	EAttribute getDataRange_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.DataSomeValuesFrom
	 * @generated
	 */
	EClass getDataSomeValuesFrom();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.DataSomeValuesFrom#getDataPropertyExpression <em>Data Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Data Property Expression</em>'.
	 * @see org.w3._2002._07.owl.DataSomeValuesFrom#getDataPropertyExpression()
	 * @see #getDataSomeValuesFrom()
	 * @generated
	 */
	EAttribute getDataSomeValuesFrom_DataPropertyExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataSomeValuesFrom#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.DataSomeValuesFrom#getDataProperty()
	 * @see #getDataSomeValuesFrom()
	 * @generated
	 */
	EReference getDataSomeValuesFrom_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataSomeValuesFrom#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.DataSomeValuesFrom#getDatatype()
	 * @see #getDataSomeValuesFrom()
	 * @generated
	 */
	EReference getDataSomeValuesFrom_Datatype();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataSomeValuesFrom#getDataIntersectionOf <em>Data Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DataSomeValuesFrom#getDataIntersectionOf()
	 * @see #getDataSomeValuesFrom()
	 * @generated
	 */
	EReference getDataSomeValuesFrom_DataIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataSomeValuesFrom#getDataUnionOf <em>Data Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Union Of</em>'.
	 * @see org.w3._2002._07.owl.DataSomeValuesFrom#getDataUnionOf()
	 * @see #getDataSomeValuesFrom()
	 * @generated
	 */
	EReference getDataSomeValuesFrom_DataUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataSomeValuesFrom#getDataComplementOf <em>Data Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DataSomeValuesFrom#getDataComplementOf()
	 * @see #getDataSomeValuesFrom()
	 * @generated
	 */
	EReference getDataSomeValuesFrom_DataComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataSomeValuesFrom#getDataOneOf <em>Data One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data One Of</em>'.
	 * @see org.w3._2002._07.owl.DataSomeValuesFrom#getDataOneOf()
	 * @see #getDataSomeValuesFrom()
	 * @generated
	 */
	EReference getDataSomeValuesFrom_DataOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DataSomeValuesFrom#getDatatypeRestriction <em>Datatype Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype Restriction</em>'.
	 * @see org.w3._2002._07.owl.DataSomeValuesFrom#getDatatypeRestriction()
	 * @see #getDataSomeValuesFrom()
	 * @generated
	 */
	EReference getDataSomeValuesFrom_DatatypeRestriction();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.Datatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.Datatype
	 * @generated
	 */
	EClass getDatatype();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Datatype#getAbbreviatedIRI <em>Abbreviated IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abbreviated IRI</em>'.
	 * @see org.w3._2002._07.owl.Datatype#getAbbreviatedIRI()
	 * @see #getDatatype()
	 * @generated
	 */
	EAttribute getDatatype_AbbreviatedIRI();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Datatype#getIRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.Datatype#getIRI()
	 * @see #getDatatype()
	 * @generated
	 */
	EAttribute getDatatype_IRI();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DatatypeDefinition <em>Datatype Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Datatype Definition</em>'.
	 * @see org.w3._2002._07.owl.DatatypeDefinition
	 * @generated
	 */
	EClass getDatatypeDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DatatypeDefinition#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.DatatypeDefinition#getDatatype()
	 * @see #getDatatypeDefinition()
	 * @generated
	 */
	EReference getDatatypeDefinition_Datatype();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DatatypeDefinition#getDatatype1 <em>Datatype1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype1</em>'.
	 * @see org.w3._2002._07.owl.DatatypeDefinition#getDatatype1()
	 * @see #getDatatypeDefinition()
	 * @generated
	 */
	EReference getDatatypeDefinition_Datatype1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DatatypeDefinition#getDataIntersectionOf <em>Data Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DatatypeDefinition#getDataIntersectionOf()
	 * @see #getDatatypeDefinition()
	 * @generated
	 */
	EReference getDatatypeDefinition_DataIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DatatypeDefinition#getDataUnionOf <em>Data Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Union Of</em>'.
	 * @see org.w3._2002._07.owl.DatatypeDefinition#getDataUnionOf()
	 * @see #getDatatypeDefinition()
	 * @generated
	 */
	EReference getDatatypeDefinition_DataUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DatatypeDefinition#getDataComplementOf <em>Data Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DatatypeDefinition#getDataComplementOf()
	 * @see #getDatatypeDefinition()
	 * @generated
	 */
	EReference getDatatypeDefinition_DataComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DatatypeDefinition#getDataOneOf <em>Data One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data One Of</em>'.
	 * @see org.w3._2002._07.owl.DatatypeDefinition#getDataOneOf()
	 * @see #getDatatypeDefinition()
	 * @generated
	 */
	EReference getDatatypeDefinition_DataOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DatatypeDefinition#getDatatypeRestriction <em>Datatype Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype Restriction</em>'.
	 * @see org.w3._2002._07.owl.DatatypeDefinition#getDatatypeRestriction()
	 * @see #getDatatypeDefinition()
	 * @generated
	 */
	EReference getDatatypeDefinition_DatatypeRestriction();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DatatypeRestriction <em>Datatype Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Datatype Restriction</em>'.
	 * @see org.w3._2002._07.owl.DatatypeRestriction
	 * @generated
	 */
	EClass getDatatypeRestriction();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DatatypeRestriction#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.DatatypeRestriction#getDatatype()
	 * @see #getDatatypeRestriction()
	 * @generated
	 */
	EReference getDatatypeRestriction_Datatype();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DatatypeRestriction#getFacetRestriction <em>Facet Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Facet Restriction</em>'.
	 * @see org.w3._2002._07.owl.DatatypeRestriction#getFacetRestriction()
	 * @see #getDatatypeRestriction()
	 * @generated
	 */
	EReference getDatatypeRestriction_FacetRestriction();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DataUnionOf <em>Data Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Union Of</em>'.
	 * @see org.w3._2002._07.owl.DataUnionOf
	 * @generated
	 */
	EClass getDataUnionOf();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.DataUnionOf#getDataRange <em>Data Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Data Range</em>'.
	 * @see org.w3._2002._07.owl.DataUnionOf#getDataRange()
	 * @see #getDataUnionOf()
	 * @generated
	 */
	EAttribute getDataUnionOf_DataRange();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataUnionOf#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.DataUnionOf#getDatatype()
	 * @see #getDataUnionOf()
	 * @generated
	 */
	EReference getDataUnionOf_Datatype();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataUnionOf#getDataIntersectionOf <em>Data Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DataUnionOf#getDataIntersectionOf()
	 * @see #getDataUnionOf()
	 * @generated
	 */
	EReference getDataUnionOf_DataIntersectionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataUnionOf#getDataUnionOf <em>Data Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Union Of</em>'.
	 * @see org.w3._2002._07.owl.DataUnionOf#getDataUnionOf()
	 * @see #getDataUnionOf()
	 * @generated
	 */
	EReference getDataUnionOf_DataUnionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataUnionOf#getDataComplementOf <em>Data Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DataUnionOf#getDataComplementOf()
	 * @see #getDataUnionOf()
	 * @generated
	 */
	EReference getDataUnionOf_DataComplementOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataUnionOf#getDataOneOf <em>Data One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data One Of</em>'.
	 * @see org.w3._2002._07.owl.DataUnionOf#getDataOneOf()
	 * @see #getDataUnionOf()
	 * @generated
	 */
	EReference getDataUnionOf_DataOneOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DataUnionOf#getDatatypeRestriction <em>Datatype Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Datatype Restriction</em>'.
	 * @see org.w3._2002._07.owl.DataUnionOf#getDatatypeRestriction()
	 * @see #getDataUnionOf()
	 * @generated
	 */
	EReference getDataUnionOf_DatatypeRestriction();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.Declaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Declaration</em>'.
	 * @see org.w3._2002._07.owl.Declaration
	 * @generated
	 */
	EClass getDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.Declaration#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.Declaration#getClass_()
	 * @see #getDeclaration()
	 * @generated
	 */
	EReference getDeclaration_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.Declaration#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.Declaration#getDatatype()
	 * @see #getDeclaration()
	 * @generated
	 */
	EReference getDeclaration_Datatype();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.Declaration#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.Declaration#getObjectProperty()
	 * @see #getDeclaration()
	 * @generated
	 */
	EReference getDeclaration_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.Declaration#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.Declaration#getDataProperty()
	 * @see #getDeclaration()
	 * @generated
	 */
	EReference getDeclaration_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.Declaration#getAnnotationProperty <em>Annotation Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Property</em>'.
	 * @see org.w3._2002._07.owl.Declaration#getAnnotationProperty()
	 * @see #getDeclaration()
	 * @generated
	 */
	EReference getDeclaration_AnnotationProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.Declaration#getNamedIndividual <em>Named Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Named Individual</em>'.
	 * @see org.w3._2002._07.owl.Declaration#getNamedIndividual()
	 * @see #getDeclaration()
	 * @generated
	 */
	EReference getDeclaration_NamedIndividual();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DifferentIndividuals <em>Different Individuals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Different Individuals</em>'.
	 * @see org.w3._2002._07.owl.DifferentIndividuals
	 * @generated
	 */
	EClass getDifferentIndividuals();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.DifferentIndividuals#getIndividual <em>Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Individual</em>'.
	 * @see org.w3._2002._07.owl.DifferentIndividuals#getIndividual()
	 * @see #getDifferentIndividuals()
	 * @generated
	 */
	EAttribute getDifferentIndividuals_Individual();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DifferentIndividuals#getNamedIndividual <em>Named Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Named Individual</em>'.
	 * @see org.w3._2002._07.owl.DifferentIndividuals#getNamedIndividual()
	 * @see #getDifferentIndividuals()
	 * @generated
	 */
	EReference getDifferentIndividuals_NamedIndividual();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DifferentIndividuals#getAnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.DifferentIndividuals#getAnonymousIndividual()
	 * @see #getDifferentIndividuals()
	 * @generated
	 */
	EReference getDifferentIndividuals_AnonymousIndividual();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DisjointClasses <em>Disjoint Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Disjoint Classes</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses
	 * @generated
	 */
	EClass getDisjointClasses();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.DisjointClasses#getClassExpression <em>Class Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Class Expression</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getClassExpression()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EAttribute getDisjointClasses_ClassExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getClass_()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_Class();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getObjectIntersectionOf()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getObjectUnionOf()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getObjectComplementOf()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getObjectOneOf()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getObjectSomeValuesFrom()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getObjectAllValuesFrom()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getObjectHasValue()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getObjectHasSelf()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getObjectMinCardinality()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getObjectMaxCardinality()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getObjectExactCardinality()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getDataSomeValuesFrom()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getDataAllValuesFrom()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getDataHasValue()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_DataHasValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getDataMinCardinality()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getDataMaxCardinality()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointClasses#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DisjointClasses#getDataExactCardinality()
	 * @see #getDisjointClasses()
	 * @generated
	 */
	EReference getDisjointClasses_DataExactCardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DisjointDataProperties <em>Disjoint Data Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Disjoint Data Properties</em>'.
	 * @see org.w3._2002._07.owl.DisjointDataProperties
	 * @generated
	 */
	EClass getDisjointDataProperties();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.DisjointDataProperties#getDataPropertyExpression <em>Data Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Data Property Expression</em>'.
	 * @see org.w3._2002._07.owl.DisjointDataProperties#getDataPropertyExpression()
	 * @see #getDisjointDataProperties()
	 * @generated
	 */
	EAttribute getDisjointDataProperties_DataPropertyExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointDataProperties#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.DisjointDataProperties#getDataProperty()
	 * @see #getDisjointDataProperties()
	 * @generated
	 */
	EReference getDisjointDataProperties_DataProperty();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DisjointObjectProperties <em>Disjoint Object Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Disjoint Object Properties</em>'.
	 * @see org.w3._2002._07.owl.DisjointObjectProperties
	 * @generated
	 */
	EClass getDisjointObjectProperties();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.DisjointObjectProperties#getObjectPropertyExpression <em>Object Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Object Property Expression</em>'.
	 * @see org.w3._2002._07.owl.DisjointObjectProperties#getObjectPropertyExpression()
	 * @see #getDisjointObjectProperties()
	 * @generated
	 */
	EAttribute getDisjointObjectProperties_ObjectPropertyExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointObjectProperties#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.DisjointObjectProperties#getObjectProperty()
	 * @see #getDisjointObjectProperties()
	 * @generated
	 */
	EReference getDisjointObjectProperties_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointObjectProperties#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.DisjointObjectProperties#getObjectInverseOf()
	 * @see #getDisjointObjectProperties()
	 * @generated
	 */
	EReference getDisjointObjectProperties_ObjectInverseOf();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DisjointUnion <em>Disjoint Union</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Disjoint Union</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion
	 * @generated
	 */
	EClass getDisjointUnion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DisjointUnion#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getClass_()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_Class();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.DisjointUnion#getClassExpression <em>Class Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Class Expression</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getClassExpression()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EAttribute getDisjointUnion_ClassExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getClass1 <em>Class1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class1</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getClass1()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_Class1();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getObjectIntersectionOf()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getObjectUnionOf()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getObjectComplementOf()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getObjectOneOf()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getObjectSomeValuesFrom()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getObjectAllValuesFrom()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getObjectHasValue()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getObjectHasSelf()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getObjectMinCardinality()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getObjectMaxCardinality()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getObjectExactCardinality()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getDataSomeValuesFrom()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getDataAllValuesFrom()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getDataHasValue()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_DataHasValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getDataMinCardinality()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getDataMaxCardinality()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.DisjointUnion#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DisjointUnion#getDataExactCardinality()
	 * @see #getDisjointUnion()
	 * @generated
	 */
	EReference getDisjointUnion_DataExactCardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link org.w3._2002._07.owl.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link org.w3._2002._07.owl.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getAbbreviatedIRI <em>Abbreviated IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Abbreviated IRI</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getAbbreviatedIRI()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_AbbreviatedIRI();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getAnnotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getAnnotation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Annotation();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getAnnotationAssertion <em>Annotation Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Assertion</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getAnnotationAssertion()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_AnnotationAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getAnnotationProperty <em>Annotation Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Property</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getAnnotationProperty()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_AnnotationProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getAnnotationPropertyDomain <em>Annotation Property Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Property Domain</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getAnnotationPropertyDomain()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_AnnotationPropertyDomain();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getAnnotationPropertyRange <em>Annotation Property Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Property Range</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getAnnotationPropertyRange()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_AnnotationPropertyRange();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getAnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getAnonymousIndividual()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_AnonymousIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getAsymmetricObjectProperty <em>Asymmetric Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Asymmetric Object Property</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getAsymmetricObjectProperty()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_AsymmetricObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getClass_()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getClassAssertion <em>Class Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class Assertion</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getClassAssertion()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ClassAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataAllValuesFrom()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataComplementOf <em>Data Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataComplementOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataExactCardinality()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataHasValue()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataIntersectionOf <em>Data Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataIntersectionOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataMaxCardinality()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataMinCardinality()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataOneOf <em>Data One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data One Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataOneOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataProperty()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataPropertyAssertion <em>Data Property Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property Assertion</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataPropertyAssertion()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataPropertyAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataPropertyDomain <em>Data Property Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property Domain</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataPropertyDomain()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataPropertyDomain();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataPropertyRange <em>Data Property Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property Range</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataPropertyRange()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataPropertyRange();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataSomeValuesFrom()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDatatype()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Datatype();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDatatypeDefinition <em>Datatype Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype Definition</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDatatypeDefinition()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DatatypeDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDatatypeRestriction <em>Datatype Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datatype Restriction</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDatatypeRestriction()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DatatypeRestriction();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDataUnionOf <em>Data Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Union Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDataUnionOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DataUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Declaration</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDeclaration()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Declaration();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDifferentIndividuals <em>Different Individuals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Different Individuals</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDifferentIndividuals()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DifferentIndividuals();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDisjointClasses <em>Disjoint Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Disjoint Classes</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDisjointClasses()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DisjointClasses();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDisjointDataProperties <em>Disjoint Data Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Disjoint Data Properties</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDisjointDataProperties()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DisjointDataProperties();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDisjointObjectProperties <em>Disjoint Object Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Disjoint Object Properties</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDisjointObjectProperties()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DisjointObjectProperties();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getDisjointUnion <em>Disjoint Union</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Disjoint Union</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getDisjointUnion()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DisjointUnion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getEquivalentClasses <em>Equivalent Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Equivalent Classes</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getEquivalentClasses()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_EquivalentClasses();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getEquivalentDataProperties <em>Equivalent Data Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Equivalent Data Properties</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getEquivalentDataProperties()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_EquivalentDataProperties();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getEquivalentObjectProperties <em>Equivalent Object Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Equivalent Object Properties</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getEquivalentObjectProperties()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_EquivalentObjectProperties();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getFunctionalDataProperty <em>Functional Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Functional Data Property</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getFunctionalDataProperty()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_FunctionalDataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getFunctionalObjectProperty <em>Functional Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Functional Object Property</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getFunctionalObjectProperty()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_FunctionalObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getHasKey <em>Has Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Has Key</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getHasKey()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_HasKey();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getImport <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Import</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getImport()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Import();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getInverseFunctionalObjectProperty <em>Inverse Functional Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inverse Functional Object Property</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getInverseFunctionalObjectProperty()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_InverseFunctionalObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getInverseObjectProperties <em>Inverse Object Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inverse Object Properties</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getInverseObjectProperties()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_InverseObjectProperties();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getIRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getIRI()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_IRI();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getIrreflexiveObjectProperty <em>Irreflexive Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Irreflexive Object Property</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getIrreflexiveObjectProperty()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_IrreflexiveObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Literal</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getLiteral()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Literal();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getNamedIndividual <em>Named Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Named Individual</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getNamedIndividual()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_NamedIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getNegativeDataPropertyAssertion <em>Negative Data Property Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Negative Data Property Assertion</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getNegativeDataPropertyAssertion()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_NegativeDataPropertyAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getNegativeObjectPropertyAssertion <em>Negative Object Property Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Negative Object Property Assertion</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getNegativeObjectPropertyAssertion()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_NegativeObjectPropertyAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectAllValuesFrom()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectComplementOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectExactCardinality()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectHasSelf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectHasValue()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectIntersectionOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectInverseOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectMaxCardinality()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectMinCardinality()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectOneOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectProperty()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectPropertyAssertion <em>Object Property Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property Assertion</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectPropertyAssertion()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectPropertyAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectPropertyDomain <em>Object Property Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property Domain</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectPropertyDomain()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectPropertyDomain();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectPropertyRange <em>Object Property Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property Range</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectPropertyRange()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectPropertyRange();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectSomeValuesFrom()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getObjectUnionOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getOntology <em>Ontology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Ontology</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getOntology()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Ontology();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getPrefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Prefix</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getPrefix()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Prefix();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getReflexiveObjectProperty <em>Reflexive Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reflexive Object Property</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getReflexiveObjectProperty()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ReflexiveObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getSameIndividual <em>Same Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Same Individual</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getSameIndividual()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_SameIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getSubAnnotationPropertyOf <em>Sub Annotation Property Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sub Annotation Property Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getSubAnnotationPropertyOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_SubAnnotationPropertyOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getSubClassOf <em>Sub Class Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sub Class Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getSubClassOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_SubClassOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getSubDataPropertyOf <em>Sub Data Property Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sub Data Property Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getSubDataPropertyOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_SubDataPropertyOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getSubObjectPropertyOf <em>Sub Object Property Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sub Object Property Of</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getSubObjectPropertyOf()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_SubObjectPropertyOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getSymmetricObjectProperty <em>Symmetric Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Symmetric Object Property</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getSymmetricObjectProperty()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_SymmetricObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.DocumentRoot#getTransitiveObjectProperty <em>Transitive Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Transitive Object Property</em>'.
	 * @see org.w3._2002._07.owl.DocumentRoot#getTransitiveObjectProperty()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_TransitiveObjectProperty();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.EquivalentClasses <em>Equivalent Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equivalent Classes</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses
	 * @generated
	 */
	EClass getEquivalentClasses();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.EquivalentClasses#getClassExpression <em>Class Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Class Expression</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getClassExpression()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EAttribute getEquivalentClasses_ClassExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getClass_()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_Class();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getObjectIntersectionOf()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getObjectUnionOf()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getObjectComplementOf()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getObjectOneOf()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getObjectSomeValuesFrom()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getObjectAllValuesFrom()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getObjectHasValue()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getObjectHasSelf()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getObjectMinCardinality()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getObjectMaxCardinality()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getObjectExactCardinality()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getDataSomeValuesFrom()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getDataAllValuesFrom()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getDataHasValue()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_DataHasValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getDataMinCardinality()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getDataMaxCardinality()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentClasses#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.EquivalentClasses#getDataExactCardinality()
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	EReference getEquivalentClasses_DataExactCardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.EquivalentDataProperties <em>Equivalent Data Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equivalent Data Properties</em>'.
	 * @see org.w3._2002._07.owl.EquivalentDataProperties
	 * @generated
	 */
	EClass getEquivalentDataProperties();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.EquivalentDataProperties#getDataPropertyExpression <em>Data Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Data Property Expression</em>'.
	 * @see org.w3._2002._07.owl.EquivalentDataProperties#getDataPropertyExpression()
	 * @see #getEquivalentDataProperties()
	 * @generated
	 */
	EAttribute getEquivalentDataProperties_DataPropertyExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentDataProperties#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.EquivalentDataProperties#getDataProperty()
	 * @see #getEquivalentDataProperties()
	 * @generated
	 */
	EReference getEquivalentDataProperties_DataProperty();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.EquivalentObjectProperties <em>Equivalent Object Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equivalent Object Properties</em>'.
	 * @see org.w3._2002._07.owl.EquivalentObjectProperties
	 * @generated
	 */
	EClass getEquivalentObjectProperties();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.EquivalentObjectProperties#getObjectPropertyExpression <em>Object Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Object Property Expression</em>'.
	 * @see org.w3._2002._07.owl.EquivalentObjectProperties#getObjectPropertyExpression()
	 * @see #getEquivalentObjectProperties()
	 * @generated
	 */
	EAttribute getEquivalentObjectProperties_ObjectPropertyExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentObjectProperties#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.EquivalentObjectProperties#getObjectProperty()
	 * @see #getEquivalentObjectProperties()
	 * @generated
	 */
	EReference getEquivalentObjectProperties_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.EquivalentObjectProperties#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.EquivalentObjectProperties#getObjectInverseOf()
	 * @see #getEquivalentObjectProperties()
	 * @generated
	 */
	EReference getEquivalentObjectProperties_ObjectInverseOf();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.FacetRestriction <em>Facet Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Facet Restriction</em>'.
	 * @see org.w3._2002._07.owl.FacetRestriction
	 * @generated
	 */
	EClass getFacetRestriction();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.FacetRestriction#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Literal</em>'.
	 * @see org.w3._2002._07.owl.FacetRestriction#getLiteral()
	 * @see #getFacetRestriction()
	 * @generated
	 */
	EReference getFacetRestriction_Literal();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.FacetRestriction#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.FacetRestriction#getBase()
	 * @see #getFacetRestriction()
	 * @generated
	 */
	EAttribute getFacetRestriction_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.FacetRestriction#getFacet <em>Facet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Facet</em>'.
	 * @see org.w3._2002._07.owl.FacetRestriction#getFacet()
	 * @see #getFacetRestriction()
	 * @generated
	 */
	EAttribute getFacetRestriction_Facet();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.FacetRestriction#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.FacetRestriction#getId()
	 * @see #getFacetRestriction()
	 * @generated
	 */
	EAttribute getFacetRestriction_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.FacetRestriction#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.FacetRestriction#getLang()
	 * @see #getFacetRestriction()
	 * @generated
	 */
	EAttribute getFacetRestriction_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.FacetRestriction#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.FacetRestriction#getSpace()
	 * @see #getFacetRestriction()
	 * @generated
	 */
	EAttribute getFacetRestriction_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.FunctionalDataProperty <em>Functional Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Functional Data Property</em>'.
	 * @see org.w3._2002._07.owl.FunctionalDataProperty
	 * @generated
	 */
	EClass getFunctionalDataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.FunctionalDataProperty#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.FunctionalDataProperty#getDataProperty()
	 * @see #getFunctionalDataProperty()
	 * @generated
	 */
	EReference getFunctionalDataProperty_DataProperty();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.FunctionalObjectProperty <em>Functional Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Functional Object Property</em>'.
	 * @see org.w3._2002._07.owl.FunctionalObjectProperty
	 * @generated
	 */
	EClass getFunctionalObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.FunctionalObjectProperty#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.FunctionalObjectProperty#getObjectProperty()
	 * @see #getFunctionalObjectProperty()
	 * @generated
	 */
	EReference getFunctionalObjectProperty_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.FunctionalObjectProperty#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.FunctionalObjectProperty#getObjectInverseOf()
	 * @see #getFunctionalObjectProperty()
	 * @generated
	 */
	EReference getFunctionalObjectProperty_ObjectInverseOf();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.HasKey <em>Has Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Has Key</em>'.
	 * @see org.w3._2002._07.owl.HasKey
	 * @generated
	 */
	EClass getHasKey();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getClass_()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectIntersectionOf()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectUnionOf()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectComplementOf()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectOneOf()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectSomeValuesFrom()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectAllValuesFrom()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectHasValue()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectHasSelf()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectMinCardinality()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectMaxCardinality()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectExactCardinality()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getDataSomeValuesFrom()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getDataAllValuesFrom()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getDataHasValue()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getDataMinCardinality()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getDataMaxCardinality()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.HasKey#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getDataExactCardinality()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_DataExactCardinality();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.HasKey#getObjectPropertyExpression <em>Object Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Object Property Expression</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectPropertyExpression()
	 * @see #getHasKey()
	 * @generated
	 */
	EAttribute getHasKey_ObjectPropertyExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.HasKey#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectProperty()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.HasKey#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getObjectInverseOf()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_ObjectInverseOf();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.HasKey#getDataPropertyExpression <em>Data Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Data Property Expression</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getDataPropertyExpression()
	 * @see #getHasKey()
	 * @generated
	 */
	EAttribute getHasKey_DataPropertyExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.HasKey#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.HasKey#getDataProperty()
	 * @see #getHasKey()
	 * @generated
	 */
	EReference getHasKey_DataProperty();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import</em>'.
	 * @see org.w3._2002._07.owl.Import
	 * @generated
	 */
	EClass getImport();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Import#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.w3._2002._07.owl.Import#getValue()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Import#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.Import#getBase()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Import#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.Import#getId()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Import#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.Import#getLang()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Import#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.Import#getSpace()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.Individual <em>Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Individual</em>'.
	 * @see org.w3._2002._07.owl.Individual
	 * @generated
	 */
	EClass getIndividual();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Individual#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.Individual#getBase()
	 * @see #getIndividual()
	 * @generated
	 */
	EAttribute getIndividual_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Individual#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.Individual#getId()
	 * @see #getIndividual()
	 * @generated
	 */
	EAttribute getIndividual_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Individual#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.Individual#getLang()
	 * @see #getIndividual()
	 * @generated
	 */
	EAttribute getIndividual_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Individual#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.Individual#getSpace()
	 * @see #getIndividual()
	 * @generated
	 */
	EAttribute getIndividual_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.InverseFunctionalObjectProperty <em>Inverse Functional Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inverse Functional Object Property</em>'.
	 * @see org.w3._2002._07.owl.InverseFunctionalObjectProperty
	 * @generated
	 */
	EClass getInverseFunctionalObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.InverseFunctionalObjectProperty#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.InverseFunctionalObjectProperty#getObjectProperty()
	 * @see #getInverseFunctionalObjectProperty()
	 * @generated
	 */
	EReference getInverseFunctionalObjectProperty_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.InverseFunctionalObjectProperty#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.InverseFunctionalObjectProperty#getObjectInverseOf()
	 * @see #getInverseFunctionalObjectProperty()
	 * @generated
	 */
	EReference getInverseFunctionalObjectProperty_ObjectInverseOf();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.InverseObjectProperties <em>Inverse Object Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inverse Object Properties</em>'.
	 * @see org.w3._2002._07.owl.InverseObjectProperties
	 * @generated
	 */
	EClass getInverseObjectProperties();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.InverseObjectProperties#getObjectPropertyExpression <em>Object Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Object Property Expression</em>'.
	 * @see org.w3._2002._07.owl.InverseObjectProperties#getObjectPropertyExpression()
	 * @see #getInverseObjectProperties()
	 * @generated
	 */
	EAttribute getInverseObjectProperties_ObjectPropertyExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.InverseObjectProperties#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.InverseObjectProperties#getObjectProperty()
	 * @see #getInverseObjectProperties()
	 * @generated
	 */
	EReference getInverseObjectProperties_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.InverseObjectProperties#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.InverseObjectProperties#getObjectInverseOf()
	 * @see #getInverseObjectProperties()
	 * @generated
	 */
	EReference getInverseObjectProperties_ObjectInverseOf();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.IRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.IRI
	 * @generated
	 */
	EClass getIRI();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.IRI#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.w3._2002._07.owl.IRI#getValue()
	 * @see #getIRI()
	 * @generated
	 */
	EAttribute getIRI_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.IRI#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.IRI#getBase()
	 * @see #getIRI()
	 * @generated
	 */
	EAttribute getIRI_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.IRI#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.IRI#getId()
	 * @see #getIRI()
	 * @generated
	 */
	EAttribute getIRI_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.IRI#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.IRI#getLang()
	 * @see #getIRI()
	 * @generated
	 */
	EAttribute getIRI_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.IRI#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.IRI#getSpace()
	 * @see #getIRI()
	 * @generated
	 */
	EAttribute getIRI_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.IrreflexiveObjectProperty <em>Irreflexive Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Irreflexive Object Property</em>'.
	 * @see org.w3._2002._07.owl.IrreflexiveObjectProperty
	 * @generated
	 */
	EClass getIrreflexiveObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.IrreflexiveObjectProperty#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.IrreflexiveObjectProperty#getObjectProperty()
	 * @see #getIrreflexiveObjectProperty()
	 * @generated
	 */
	EReference getIrreflexiveObjectProperty_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.IrreflexiveObjectProperty#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.IrreflexiveObjectProperty#getObjectInverseOf()
	 * @see #getIrreflexiveObjectProperty()
	 * @generated
	 */
	EReference getIrreflexiveObjectProperty_ObjectInverseOf();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.Literal <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Literal</em>'.
	 * @see org.w3._2002._07.owl.Literal
	 * @generated
	 */
	EClass getLiteral();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Literal#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.w3._2002._07.owl.Literal#getValue()
	 * @see #getLiteral()
	 * @generated
	 */
	EAttribute getLiteral_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Literal#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.Literal#getBase()
	 * @see #getLiteral()
	 * @generated
	 */
	EAttribute getLiteral_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Literal#getDatatypeIRI <em>Datatype IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Datatype IRI</em>'.
	 * @see org.w3._2002._07.owl.Literal#getDatatypeIRI()
	 * @see #getLiteral()
	 * @generated
	 */
	EAttribute getLiteral_DatatypeIRI();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Literal#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.Literal#getId()
	 * @see #getLiteral()
	 * @generated
	 */
	EAttribute getLiteral_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Literal#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.Literal#getLang()
	 * @see #getLiteral()
	 * @generated
	 */
	EAttribute getLiteral_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Literal#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.Literal#getSpace()
	 * @see #getLiteral()
	 * @generated
	 */
	EAttribute getLiteral_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.NamedIndividual <em>Named Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Individual</em>'.
	 * @see org.w3._2002._07.owl.NamedIndividual
	 * @generated
	 */
	EClass getNamedIndividual();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.NamedIndividual#getAbbreviatedIRI <em>Abbreviated IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abbreviated IRI</em>'.
	 * @see org.w3._2002._07.owl.NamedIndividual#getAbbreviatedIRI()
	 * @see #getNamedIndividual()
	 * @generated
	 */
	EAttribute getNamedIndividual_AbbreviatedIRI();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.NamedIndividual#getIRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.NamedIndividual#getIRI()
	 * @see #getNamedIndividual()
	 * @generated
	 */
	EAttribute getNamedIndividual_IRI();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.NegativeDataPropertyAssertion <em>Negative Data Property Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Negative Data Property Assertion</em>'.
	 * @see org.w3._2002._07.owl.NegativeDataPropertyAssertion
	 * @generated
	 */
	EClass getNegativeDataPropertyAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.NegativeDataPropertyAssertion#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.NegativeDataPropertyAssertion#getDataProperty()
	 * @see #getNegativeDataPropertyAssertion()
	 * @generated
	 */
	EReference getNegativeDataPropertyAssertion_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.NegativeDataPropertyAssertion#getNamedIndividual <em>Named Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Named Individual</em>'.
	 * @see org.w3._2002._07.owl.NegativeDataPropertyAssertion#getNamedIndividual()
	 * @see #getNegativeDataPropertyAssertion()
	 * @generated
	 */
	EReference getNegativeDataPropertyAssertion_NamedIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.NegativeDataPropertyAssertion#getAnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.NegativeDataPropertyAssertion#getAnonymousIndividual()
	 * @see #getNegativeDataPropertyAssertion()
	 * @generated
	 */
	EReference getNegativeDataPropertyAssertion_AnonymousIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.NegativeDataPropertyAssertion#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Literal</em>'.
	 * @see org.w3._2002._07.owl.NegativeDataPropertyAssertion#getLiteral()
	 * @see #getNegativeDataPropertyAssertion()
	 * @generated
	 */
	EReference getNegativeDataPropertyAssertion_Literal();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.NegativeObjectPropertyAssertion <em>Negative Object Property Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Negative Object Property Assertion</em>'.
	 * @see org.w3._2002._07.owl.NegativeObjectPropertyAssertion
	 * @generated
	 */
	EClass getNegativeObjectPropertyAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.NegativeObjectPropertyAssertion#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.NegativeObjectPropertyAssertion#getObjectProperty()
	 * @see #getNegativeObjectPropertyAssertion()
	 * @generated
	 */
	EReference getNegativeObjectPropertyAssertion_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.NegativeObjectPropertyAssertion#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.NegativeObjectPropertyAssertion#getObjectInverseOf()
	 * @see #getNegativeObjectPropertyAssertion()
	 * @generated
	 */
	EReference getNegativeObjectPropertyAssertion_ObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.NegativeObjectPropertyAssertion#getNamedIndividual <em>Named Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Named Individual</em>'.
	 * @see org.w3._2002._07.owl.NegativeObjectPropertyAssertion#getNamedIndividual()
	 * @see #getNegativeObjectPropertyAssertion()
	 * @generated
	 */
	EReference getNegativeObjectPropertyAssertion_NamedIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.NegativeObjectPropertyAssertion#getAnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.NegativeObjectPropertyAssertion#getAnonymousIndividual()
	 * @see #getNegativeObjectPropertyAssertion()
	 * @generated
	 */
	EReference getNegativeObjectPropertyAssertion_AnonymousIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.NegativeObjectPropertyAssertion#getNamedIndividual1 <em>Named Individual1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Named Individual1</em>'.
	 * @see org.w3._2002._07.owl.NegativeObjectPropertyAssertion#getNamedIndividual1()
	 * @see #getNegativeObjectPropertyAssertion()
	 * @generated
	 */
	EReference getNegativeObjectPropertyAssertion_NamedIndividual1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.NegativeObjectPropertyAssertion#getAnonymousIndividual1 <em>Anonymous Individual1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Anonymous Individual1</em>'.
	 * @see org.w3._2002._07.owl.NegativeObjectPropertyAssertion#getAnonymousIndividual1()
	 * @see #getNegativeObjectPropertyAssertion()
	 * @generated
	 */
	EReference getNegativeObjectPropertyAssertion_AnonymousIndividual1();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom
	 * @generated
	 */
	EClass getObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectProperty()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectInverseOf()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getClass_()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectIntersectionOf()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectUnionOf()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectComplementOf()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectOneOf()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectSomeValuesFrom()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectAllValuesFrom()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectHasValue()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectHasSelf()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectMinCardinality()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectMaxCardinality()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getObjectExactCardinality()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getDataSomeValuesFrom()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getDataAllValuesFrom()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getDataHasValue()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getDataMinCardinality()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getDataMaxCardinality()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectAllValuesFrom#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectAllValuesFrom#getDataExactCardinality()
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	EReference getObjectAllValuesFrom_DataExactCardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf
	 * @generated
	 */
	EClass getObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getClass_()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getObjectIntersectionOf()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getObjectUnionOf()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getObjectComplementOf()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getObjectOneOf()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getObjectSomeValuesFrom()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getObjectAllValuesFrom()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getObjectHasValue()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getObjectHasSelf()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getObjectMinCardinality()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getObjectMaxCardinality()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getObjectExactCardinality()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getDataSomeValuesFrom()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getDataAllValuesFrom()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getDataHasValue()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getDataMinCardinality()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getDataMaxCardinality()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectComplementOf#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectComplementOf#getDataExactCardinality()
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	EReference getObjectComplementOf_DataExactCardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality
	 * @generated
	 */
	EClass getObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectProperty()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectInverseOf()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getClass_()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectIntersectionOf()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectUnionOf()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectComplementOf()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectOneOf()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectSomeValuesFrom()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectAllValuesFrom()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectHasValue()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectHasSelf()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectMinCardinality()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectMaxCardinality()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getObjectExactCardinality()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getDataSomeValuesFrom()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getDataAllValuesFrom()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getDataHasValue()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getDataMinCardinality()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getDataMaxCardinality()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectExactCardinality#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getDataExactCardinality()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EReference getObjectExactCardinality_DataExactCardinality();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectExactCardinality#getCardinality <em>Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectExactCardinality#getCardinality()
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	EAttribute getObjectExactCardinality_Cardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.ObjectHasSelf
	 * @generated
	 */
	EClass getObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectHasSelf#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectHasSelf#getObjectProperty()
	 * @see #getObjectHasSelf()
	 * @generated
	 */
	EReference getObjectHasSelf_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectHasSelf#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectHasSelf#getObjectInverseOf()
	 * @see #getObjectHasSelf()
	 * @generated
	 */
	EReference getObjectHasSelf_ObjectInverseOf();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectHasValue
	 * @generated
	 */
	EClass getObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectHasValue#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectHasValue#getObjectProperty()
	 * @see #getObjectHasValue()
	 * @generated
	 */
	EReference getObjectHasValue_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectHasValue#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectHasValue#getObjectInverseOf()
	 * @see #getObjectHasValue()
	 * @generated
	 */
	EReference getObjectHasValue_ObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectHasValue#getNamedIndividual <em>Named Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Named Individual</em>'.
	 * @see org.w3._2002._07.owl.ObjectHasValue#getNamedIndividual()
	 * @see #getObjectHasValue()
	 * @generated
	 */
	EReference getObjectHasValue_NamedIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectHasValue#getAnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.ObjectHasValue#getAnonymousIndividual()
	 * @see #getObjectHasValue()
	 * @generated
	 */
	EReference getObjectHasValue_AnonymousIndividual();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf
	 * @generated
	 */
	EClass getObjectIntersectionOf();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getClassExpression <em>Class Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Class Expression</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getClassExpression()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EAttribute getObjectIntersectionOf_ClassExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getClass_()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_Class();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getObjectIntersectionOf()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getObjectUnionOf()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getObjectComplementOf()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getObjectOneOf()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getObjectSomeValuesFrom()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getObjectAllValuesFrom()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getObjectHasValue()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getObjectHasSelf()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getObjectMinCardinality()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getObjectMaxCardinality()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getObjectExactCardinality()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getDataSomeValuesFrom()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getDataAllValuesFrom()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getDataHasValue()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_DataHasValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getDataMinCardinality()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getDataMaxCardinality()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectIntersectionOf#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectIntersectionOf#getDataExactCardinality()
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	EReference getObjectIntersectionOf_DataExactCardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectInverseOf
	 * @generated
	 */
	EClass getObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectInverseOf#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectInverseOf#getObjectProperty()
	 * @see #getObjectInverseOf()
	 * @generated
	 */
	EReference getObjectInverseOf_ObjectProperty();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality
	 * @generated
	 */
	EClass getObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectProperty()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectInverseOf()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getClass_()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectIntersectionOf()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectUnionOf()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectComplementOf()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectOneOf()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectSomeValuesFrom()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectAllValuesFrom()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectHasValue()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectHasSelf()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectMinCardinality()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectMaxCardinality()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getObjectExactCardinality()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getDataSomeValuesFrom()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getDataAllValuesFrom()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getDataHasValue()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getDataMinCardinality()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getDataMaxCardinality()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getDataExactCardinality()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EReference getObjectMaxCardinality_DataExactCardinality();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectMaxCardinality#getCardinality <em>Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMaxCardinality#getCardinality()
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	EAttribute getObjectMaxCardinality_Cardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality
	 * @generated
	 */
	EClass getObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectProperty()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectInverseOf()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getClass_()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectIntersectionOf()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectUnionOf()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectComplementOf()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectOneOf()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectSomeValuesFrom()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectAllValuesFrom()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectHasValue()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectHasSelf()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectMinCardinality()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectMaxCardinality()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getObjectExactCardinality()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getDataSomeValuesFrom()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getDataAllValuesFrom()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getDataHasValue()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getDataMinCardinality()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getDataMaxCardinality()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectMinCardinality#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getDataExactCardinality()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EReference getObjectMinCardinality_DataExactCardinality();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectMinCardinality#getCardinality <em>Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectMinCardinality#getCardinality()
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	EAttribute getObjectMinCardinality_Cardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectOneOf
	 * @generated
	 */
	EClass getObjectOneOf();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.ObjectOneOf#getIndividual <em>Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Individual</em>'.
	 * @see org.w3._2002._07.owl.ObjectOneOf#getIndividual()
	 * @see #getObjectOneOf()
	 * @generated
	 */
	EAttribute getObjectOneOf_Individual();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectOneOf#getNamedIndividual <em>Named Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Named Individual</em>'.
	 * @see org.w3._2002._07.owl.ObjectOneOf#getNamedIndividual()
	 * @see #getObjectOneOf()
	 * @generated
	 */
	EReference getObjectOneOf_NamedIndividual();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectOneOf#getAnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.ObjectOneOf#getAnonymousIndividual()
	 * @see #getObjectOneOf()
	 * @generated
	 */
	EReference getObjectOneOf_AnonymousIndividual();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectProperty
	 * @generated
	 */
	EClass getObjectProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectProperty#getAbbreviatedIRI <em>Abbreviated IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abbreviated IRI</em>'.
	 * @see org.w3._2002._07.owl.ObjectProperty#getAbbreviatedIRI()
	 * @see #getObjectProperty()
	 * @generated
	 */
	EAttribute getObjectProperty_AbbreviatedIRI();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectProperty#getIRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.ObjectProperty#getIRI()
	 * @see #getObjectProperty()
	 * @generated
	 */
	EAttribute getObjectProperty_IRI();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectPropertyAssertion <em>Object Property Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Property Assertion</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyAssertion
	 * @generated
	 */
	EClass getObjectPropertyAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyAssertion#getObjectProperty()
	 * @see #getObjectPropertyAssertion()
	 * @generated
	 */
	EReference getObjectPropertyAssertion_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyAssertion#getObjectInverseOf()
	 * @see #getObjectPropertyAssertion()
	 * @generated
	 */
	EReference getObjectPropertyAssertion_ObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getNamedIndividual <em>Named Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Named Individual</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyAssertion#getNamedIndividual()
	 * @see #getObjectPropertyAssertion()
	 * @generated
	 */
	EReference getObjectPropertyAssertion_NamedIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getAnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyAssertion#getAnonymousIndividual()
	 * @see #getObjectPropertyAssertion()
	 * @generated
	 */
	EReference getObjectPropertyAssertion_AnonymousIndividual();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getAnonymousIndividual1 <em>Anonymous Individual1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Anonymous Individual1</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyAssertion#getAnonymousIndividual1()
	 * @see #getObjectPropertyAssertion()
	 * @generated
	 */
	EReference getObjectPropertyAssertion_AnonymousIndividual1();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectPropertyAxiom <em>Object Property Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Property Axiom</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyAxiom
	 * @generated
	 */
	EClass getObjectPropertyAxiom();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectPropertyChain <em>Object Property Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Property Chain</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyChain
	 * @generated
	 */
	EClass getObjectPropertyChain();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.ObjectPropertyChain#getObjectPropertyExpression <em>Object Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Object Property Expression</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyChain#getObjectPropertyExpression()
	 * @see #getObjectPropertyChain()
	 * @generated
	 */
	EAttribute getObjectPropertyChain_ObjectPropertyExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectPropertyChain#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyChain#getObjectProperty()
	 * @see #getObjectPropertyChain()
	 * @generated
	 */
	EReference getObjectPropertyChain_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectPropertyChain#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyChain#getObjectInverseOf()
	 * @see #getObjectPropertyChain()
	 * @generated
	 */
	EReference getObjectPropertyChain_ObjectInverseOf();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectPropertyChain#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyChain#getBase()
	 * @see #getObjectPropertyChain()
	 * @generated
	 */
	EAttribute getObjectPropertyChain_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectPropertyChain#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyChain#getId()
	 * @see #getObjectPropertyChain()
	 * @generated
	 */
	EAttribute getObjectPropertyChain_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectPropertyChain#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyChain#getLang()
	 * @see #getObjectPropertyChain()
	 * @generated
	 */
	EAttribute getObjectPropertyChain_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectPropertyChain#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyChain#getSpace()
	 * @see #getObjectPropertyChain()
	 * @generated
	 */
	EAttribute getObjectPropertyChain_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectPropertyDomain <em>Object Property Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Property Domain</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain
	 * @generated
	 */
	EClass getObjectPropertyDomain();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectProperty()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectInverseOf()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getClass_()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectIntersectionOf()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectUnionOf()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectComplementOf()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectOneOf()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectSomeValuesFrom()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectAllValuesFrom()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectHasValue()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectHasSelf()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectMinCardinality()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectMaxCardinality()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getObjectExactCardinality()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getDataSomeValuesFrom()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getDataAllValuesFrom()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getDataHasValue()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getDataMinCardinality()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getDataMaxCardinality()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyDomain#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyDomain#getDataExactCardinality()
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	EReference getObjectPropertyDomain_DataExactCardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectPropertyExpression <em>Object Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Property Expression</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyExpression
	 * @generated
	 */
	EClass getObjectPropertyExpression();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectPropertyExpression#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyExpression#getBase()
	 * @see #getObjectPropertyExpression()
	 * @generated
	 */
	EAttribute getObjectPropertyExpression_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectPropertyExpression#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyExpression#getId()
	 * @see #getObjectPropertyExpression()
	 * @generated
	 */
	EAttribute getObjectPropertyExpression_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectPropertyExpression#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyExpression#getLang()
	 * @see #getObjectPropertyExpression()
	 * @generated
	 */
	EAttribute getObjectPropertyExpression_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.ObjectPropertyExpression#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyExpression#getSpace()
	 * @see #getObjectPropertyExpression()
	 * @generated
	 */
	EAttribute getObjectPropertyExpression_Space();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectPropertyRange <em>Object Property Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Property Range</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange
	 * @generated
	 */
	EClass getObjectPropertyRange();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectProperty()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectInverseOf()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getClass_()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectIntersectionOf()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectUnionOf()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectComplementOf()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectOneOf()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectSomeValuesFrom()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectAllValuesFrom()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectHasValue()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectHasSelf()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectMinCardinality()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectMaxCardinality()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getObjectExactCardinality()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getDataSomeValuesFrom()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getDataAllValuesFrom()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getDataHasValue()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getDataMinCardinality()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getDataMaxCardinality()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectPropertyRange#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectPropertyRange#getDataExactCardinality()
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	EReference getObjectPropertyRange_DataExactCardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom
	 * @generated
	 */
	EClass getObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectProperty()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectInverseOf()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getClass_()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectIntersectionOf()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectUnionOf()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectComplementOf()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectOneOf()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectSomeValuesFrom()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectAllValuesFrom()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectHasValue()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectHasSelf()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectMinCardinality()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectMaxCardinality()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getObjectExactCardinality()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getDataSomeValuesFrom()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getDataAllValuesFrom()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getDataHasValue()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getDataMinCardinality()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getDataMaxCardinality()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ObjectSomeValuesFrom#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectSomeValuesFrom#getDataExactCardinality()
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	EReference getObjectSomeValuesFrom_DataExactCardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf
	 * @generated
	 */
	EClass getObjectUnionOf();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.ObjectUnionOf#getClassExpression <em>Class Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Class Expression</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getClassExpression()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EAttribute getObjectUnionOf_ClassExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getClass_()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_Class();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getObjectIntersectionOf()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getObjectUnionOf()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getObjectComplementOf()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getObjectOneOf()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getObjectSomeValuesFrom()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getObjectAllValuesFrom()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getObjectHasValue()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getObjectHasSelf()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getObjectMinCardinality()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getObjectMaxCardinality()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getObjectExactCardinality()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getDataSomeValuesFrom()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getDataAllValuesFrom()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getDataHasValue()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_DataHasValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getDataMinCardinality()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getDataMaxCardinality()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.ObjectUnionOf#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.ObjectUnionOf#getDataExactCardinality()
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	EReference getObjectUnionOf_DataExactCardinality();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.Ontology <em>Ontology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ontology</em>'.
	 * @see org.w3._2002._07.owl.Ontology
	 * @generated
	 */
	EClass getOntology();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getPrefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Prefix</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getPrefix()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_Prefix();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getImport <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Import</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getImport()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_Import();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getAnnotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotation</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getAnnotation()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_Annotation();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.Ontology#getAxiom <em>Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Axiom</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getAxiom()
	 * @see #getOntology()
	 * @generated
	 */
	EAttribute getOntology_Axiom();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declaration</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getDeclaration()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_Declaration();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getSubClassOf <em>Sub Class Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Class Of</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getSubClassOf()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_SubClassOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getEquivalentClasses <em>Equivalent Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equivalent Classes</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getEquivalentClasses()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_EquivalentClasses();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getDisjointClasses <em>Disjoint Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Disjoint Classes</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getDisjointClasses()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_DisjointClasses();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getDisjointUnion <em>Disjoint Union</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Disjoint Union</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getDisjointUnion()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_DisjointUnion();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getSubObjectPropertyOf <em>Sub Object Property Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Object Property Of</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getSubObjectPropertyOf()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_SubObjectPropertyOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getEquivalentObjectProperties <em>Equivalent Object Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equivalent Object Properties</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getEquivalentObjectProperties()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_EquivalentObjectProperties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getDisjointObjectProperties <em>Disjoint Object Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Disjoint Object Properties</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getDisjointObjectProperties()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_DisjointObjectProperties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getInverseObjectProperties <em>Inverse Object Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inverse Object Properties</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getInverseObjectProperties()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_InverseObjectProperties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getObjectPropertyDomain <em>Object Property Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Property Domain</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getObjectPropertyDomain()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_ObjectPropertyDomain();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getObjectPropertyRange <em>Object Property Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Property Range</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getObjectPropertyRange()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_ObjectPropertyRange();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getFunctionalObjectProperty <em>Functional Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Functional Object Property</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getFunctionalObjectProperty()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_FunctionalObjectProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getInverseFunctionalObjectProperty <em>Inverse Functional Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inverse Functional Object Property</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getInverseFunctionalObjectProperty()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_InverseFunctionalObjectProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getReflexiveObjectProperty <em>Reflexive Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reflexive Object Property</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getReflexiveObjectProperty()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_ReflexiveObjectProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getIrreflexiveObjectProperty <em>Irreflexive Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Irreflexive Object Property</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getIrreflexiveObjectProperty()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_IrreflexiveObjectProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getSymmetricObjectProperty <em>Symmetric Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Symmetric Object Property</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getSymmetricObjectProperty()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_SymmetricObjectProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getAsymmetricObjectProperty <em>Asymmetric Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Asymmetric Object Property</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getAsymmetricObjectProperty()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_AsymmetricObjectProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getTransitiveObjectProperty <em>Transitive Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitive Object Property</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getTransitiveObjectProperty()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_TransitiveObjectProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getSubDataPropertyOf <em>Sub Data Property Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Data Property Of</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getSubDataPropertyOf()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_SubDataPropertyOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getEquivalentDataProperties <em>Equivalent Data Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equivalent Data Properties</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getEquivalentDataProperties()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_EquivalentDataProperties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getDisjointDataProperties <em>Disjoint Data Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Disjoint Data Properties</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getDisjointDataProperties()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_DisjointDataProperties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getDataPropertyDomain <em>Data Property Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Property Domain</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getDataPropertyDomain()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_DataPropertyDomain();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getDataPropertyRange <em>Data Property Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Property Range</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getDataPropertyRange()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_DataPropertyRange();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getFunctionalDataProperty <em>Functional Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Functional Data Property</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getFunctionalDataProperty()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_FunctionalDataProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getDatatypeDefinition <em>Datatype Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Datatype Definition</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getDatatypeDefinition()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_DatatypeDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getHasKey <em>Has Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Key</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getHasKey()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_HasKey();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getSameIndividual <em>Same Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Same Individual</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getSameIndividual()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_SameIndividual();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getDifferentIndividuals <em>Different Individuals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Different Individuals</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getDifferentIndividuals()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_DifferentIndividuals();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getClassAssertion <em>Class Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class Assertion</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getClassAssertion()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_ClassAssertion();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getObjectPropertyAssertion <em>Object Property Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Property Assertion</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getObjectPropertyAssertion()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_ObjectPropertyAssertion();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getNegativeObjectPropertyAssertion <em>Negative Object Property Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Negative Object Property Assertion</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getNegativeObjectPropertyAssertion()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_NegativeObjectPropertyAssertion();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getDataPropertyAssertion <em>Data Property Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Property Assertion</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getDataPropertyAssertion()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_DataPropertyAssertion();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getNegativeDataPropertyAssertion <em>Negative Data Property Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Negative Data Property Assertion</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getNegativeDataPropertyAssertion()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_NegativeDataPropertyAssertion();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getAnnotationAssertion <em>Annotation Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotation Assertion</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getAnnotationAssertion()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_AnnotationAssertion();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getSubAnnotationPropertyOf <em>Sub Annotation Property Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Annotation Property Of</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getSubAnnotationPropertyOf()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_SubAnnotationPropertyOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getAnnotationPropertyDomain <em>Annotation Property Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotation Property Domain</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getAnnotationPropertyDomain()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_AnnotationPropertyDomain();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.Ontology#getAnnotationPropertyRange <em>Annotation Property Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotation Property Range</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getAnnotationPropertyRange()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_AnnotationPropertyRange();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Ontology#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getBase()
	 * @see #getOntology()
	 * @generated
	 */
	EAttribute getOntology_Base();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Ontology#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getId()
	 * @see #getOntology()
	 * @generated
	 */
	EAttribute getOntology_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Ontology#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getLang()
	 * @see #getOntology()
	 * @generated
	 */
	EAttribute getOntology_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Ontology#getOntologyIRI <em>Ontology IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ontology IRI</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getOntologyIRI()
	 * @see #getOntology()
	 * @generated
	 */
	EAttribute getOntology_OntologyIRI();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Ontology#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getSpace()
	 * @see #getOntology()
	 * @generated
	 */
	EAttribute getOntology_Space();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Ontology#getVersionIRI <em>Version IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version IRI</em>'.
	 * @see org.w3._2002._07.owl.Ontology#getVersionIRI()
	 * @see #getOntology()
	 * @generated
	 */
	EAttribute getOntology_VersionIRI();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.Prefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Prefix</em>'.
	 * @see org.w3._2002._07.owl.Prefix
	 * @generated
	 */
	EClass getPrefix();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Prefix#getIRI <em>IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>IRI</em>'.
	 * @see org.w3._2002._07.owl.Prefix#getIRI()
	 * @see #getPrefix()
	 * @generated
	 */
	EAttribute getPrefix_IRI();

	/**
	 * Returns the meta object for the attribute '{@link org.w3._2002._07.owl.Prefix#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.w3._2002._07.owl.Prefix#getName()
	 * @see #getPrefix()
	 * @generated
	 */
	EAttribute getPrefix_Name();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.ReflexiveObjectProperty <em>Reflexive Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reflexive Object Property</em>'.
	 * @see org.w3._2002._07.owl.ReflexiveObjectProperty
	 * @generated
	 */
	EClass getReflexiveObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ReflexiveObjectProperty#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.ReflexiveObjectProperty#getObjectProperty()
	 * @see #getReflexiveObjectProperty()
	 * @generated
	 */
	EReference getReflexiveObjectProperty_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.ReflexiveObjectProperty#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.ReflexiveObjectProperty#getObjectInverseOf()
	 * @see #getReflexiveObjectProperty()
	 * @generated
	 */
	EReference getReflexiveObjectProperty_ObjectInverseOf();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.SameIndividual <em>Same Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Same Individual</em>'.
	 * @see org.w3._2002._07.owl.SameIndividual
	 * @generated
	 */
	EClass getSameIndividual();

	/**
	 * Returns the meta object for the attribute list '{@link org.w3._2002._07.owl.SameIndividual#getIndividual <em>Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Individual</em>'.
	 * @see org.w3._2002._07.owl.SameIndividual#getIndividual()
	 * @see #getSameIndividual()
	 * @generated
	 */
	EAttribute getSameIndividual_Individual();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.SameIndividual#getNamedIndividual <em>Named Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Named Individual</em>'.
	 * @see org.w3._2002._07.owl.SameIndividual#getNamedIndividual()
	 * @see #getSameIndividual()
	 * @generated
	 */
	EReference getSameIndividual_NamedIndividual();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.SameIndividual#getAnonymousIndividual <em>Anonymous Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Anonymous Individual</em>'.
	 * @see org.w3._2002._07.owl.SameIndividual#getAnonymousIndividual()
	 * @see #getSameIndividual()
	 * @generated
	 */
	EReference getSameIndividual_AnonymousIndividual();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.SubAnnotationPropertyOf <em>Sub Annotation Property Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Annotation Property Of</em>'.
	 * @see org.w3._2002._07.owl.SubAnnotationPropertyOf
	 * @generated
	 */
	EClass getSubAnnotationPropertyOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubAnnotationPropertyOf#getAnnotationProperty <em>Annotation Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Property</em>'.
	 * @see org.w3._2002._07.owl.SubAnnotationPropertyOf#getAnnotationProperty()
	 * @see #getSubAnnotationPropertyOf()
	 * @generated
	 */
	EReference getSubAnnotationPropertyOf_AnnotationProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubAnnotationPropertyOf#getAnnotationProperty1 <em>Annotation Property1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Property1</em>'.
	 * @see org.w3._2002._07.owl.SubAnnotationPropertyOf#getAnnotationProperty1()
	 * @see #getSubAnnotationPropertyOf()
	 * @generated
	 */
	EReference getSubAnnotationPropertyOf_AnnotationProperty1();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.SubClassOf <em>Sub Class Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Class Of</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf
	 * @generated
	 */
	EClass getSubClassOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.SubClassOf#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getClass_()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectIntersectionOf <em>Object Intersection Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectIntersectionOf()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectIntersectionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectUnionOf <em>Object Union Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectUnionOf()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectUnionOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectComplementOf <em>Object Complement Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectComplementOf()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectComplementOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectOneOf <em>Object One Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectOneOf()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectOneOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectSomeValuesFrom <em>Object Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectSomeValuesFrom()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectAllValuesFrom <em>Object All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectAllValuesFrom()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectHasValue <em>Object Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectHasValue()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectHasSelf <em>Object Has Self</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectHasSelf()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectHasSelf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectMinCardinality <em>Object Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectMinCardinality()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectMaxCardinality <em>Object Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectMaxCardinality()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectExactCardinality <em>Object Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectExactCardinality()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getDataSomeValuesFrom <em>Data Some Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getDataSomeValuesFrom()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_DataSomeValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getDataAllValuesFrom <em>Data All Values From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getDataAllValuesFrom()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_DataAllValuesFrom();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getDataHasValue <em>Data Has Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getDataHasValue()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_DataHasValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getDataMinCardinality <em>Data Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getDataMinCardinality()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_DataMinCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getDataMaxCardinality <em>Data Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getDataMaxCardinality()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_DataMaxCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getDataExactCardinality <em>Data Exact Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getDataExactCardinality()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_DataExactCardinality();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectIntersectionOf1 <em>Object Intersection Of1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Intersection Of1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectIntersectionOf1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectIntersectionOf1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectUnionOf1 <em>Object Union Of1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Union Of1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectUnionOf1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectUnionOf1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectComplementOf1 <em>Object Complement Of1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Complement Of1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectComplementOf1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectComplementOf1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectOneOf1 <em>Object One Of1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object One Of1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectOneOf1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectOneOf1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectSomeValuesFrom1 <em>Object Some Values From1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Some Values From1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectSomeValuesFrom1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectSomeValuesFrom1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectAllValuesFrom1 <em>Object All Values From1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object All Values From1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectAllValuesFrom1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectAllValuesFrom1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectHasValue1 <em>Object Has Value1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Value1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectHasValue1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectHasValue1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectHasSelf1 <em>Object Has Self1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Has Self1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectHasSelf1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectHasSelf1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectMinCardinality1 <em>Object Min Cardinality1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Min Cardinality1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectMinCardinality1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectMinCardinality1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectMaxCardinality1 <em>Object Max Cardinality1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Max Cardinality1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectMaxCardinality1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectMaxCardinality1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getObjectExactCardinality1 <em>Object Exact Cardinality1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Exact Cardinality1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getObjectExactCardinality1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_ObjectExactCardinality1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getDataSomeValuesFrom1 <em>Data Some Values From1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Some Values From1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getDataSomeValuesFrom1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_DataSomeValuesFrom1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getDataAllValuesFrom1 <em>Data All Values From1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data All Values From1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getDataAllValuesFrom1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_DataAllValuesFrom1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getDataHasValue1 <em>Data Has Value1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Has Value1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getDataHasValue1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_DataHasValue1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getDataMinCardinality1 <em>Data Min Cardinality1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Min Cardinality1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getDataMinCardinality1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_DataMinCardinality1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getDataMaxCardinality1 <em>Data Max Cardinality1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Max Cardinality1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getDataMaxCardinality1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_DataMaxCardinality1();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubClassOf#getDataExactCardinality1 <em>Data Exact Cardinality1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Exact Cardinality1</em>'.
	 * @see org.w3._2002._07.owl.SubClassOf#getDataExactCardinality1()
	 * @see #getSubClassOf()
	 * @generated
	 */
	EReference getSubClassOf_DataExactCardinality1();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.SubDataPropertyOf <em>Sub Data Property Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Data Property Of</em>'.
	 * @see org.w3._2002._07.owl.SubDataPropertyOf
	 * @generated
	 */
	EClass getSubDataPropertyOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubDataPropertyOf#getDataProperty <em>Data Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property</em>'.
	 * @see org.w3._2002._07.owl.SubDataPropertyOf#getDataProperty()
	 * @see #getSubDataPropertyOf()
	 * @generated
	 */
	EReference getSubDataPropertyOf_DataProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubDataPropertyOf#getDataProperty1 <em>Data Property1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Property1</em>'.
	 * @see org.w3._2002._07.owl.SubDataPropertyOf#getDataProperty1()
	 * @see #getSubDataPropertyOf()
	 * @generated
	 */
	EReference getSubDataPropertyOf_DataProperty1();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.SubObjectPropertyOf <em>Sub Object Property Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Object Property Of</em>'.
	 * @see org.w3._2002._07.owl.SubObjectPropertyOf
	 * @generated
	 */
	EClass getSubObjectPropertyOf();

	/**
	 * Returns the meta object for the containment reference list '{@link org.w3._2002._07.owl.SubObjectPropertyOf#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.SubObjectPropertyOf#getObjectProperty()
	 * @see #getSubObjectPropertyOf()
	 * @generated
	 */
	EReference getSubObjectPropertyOf_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubObjectPropertyOf#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.SubObjectPropertyOf#getObjectInverseOf()
	 * @see #getSubObjectPropertyOf()
	 * @generated
	 */
	EReference getSubObjectPropertyOf_ObjectInverseOf();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubObjectPropertyOf#getObjectPropertyChain <em>Object Property Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property Chain</em>'.
	 * @see org.w3._2002._07.owl.SubObjectPropertyOf#getObjectPropertyChain()
	 * @see #getSubObjectPropertyOf()
	 * @generated
	 */
	EReference getSubObjectPropertyOf_ObjectPropertyChain();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SubObjectPropertyOf#getObjectInverseOf1 <em>Object Inverse Of1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of1</em>'.
	 * @see org.w3._2002._07.owl.SubObjectPropertyOf#getObjectInverseOf1()
	 * @see #getSubObjectPropertyOf()
	 * @generated
	 */
	EReference getSubObjectPropertyOf_ObjectInverseOf1();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.SymmetricObjectProperty <em>Symmetric Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symmetric Object Property</em>'.
	 * @see org.w3._2002._07.owl.SymmetricObjectProperty
	 * @generated
	 */
	EClass getSymmetricObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SymmetricObjectProperty#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.SymmetricObjectProperty#getObjectProperty()
	 * @see #getSymmetricObjectProperty()
	 * @generated
	 */
	EReference getSymmetricObjectProperty_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.SymmetricObjectProperty#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.SymmetricObjectProperty#getObjectInverseOf()
	 * @see #getSymmetricObjectProperty()
	 * @generated
	 */
	EReference getSymmetricObjectProperty_ObjectInverseOf();

	/**
	 * Returns the meta object for class '{@link org.w3._2002._07.owl.TransitiveObjectProperty <em>Transitive Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transitive Object Property</em>'.
	 * @see org.w3._2002._07.owl.TransitiveObjectProperty
	 * @generated
	 */
	EClass getTransitiveObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.TransitiveObjectProperty#getObjectProperty <em>Object Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Property</em>'.
	 * @see org.w3._2002._07.owl.TransitiveObjectProperty#getObjectProperty()
	 * @see #getTransitiveObjectProperty()
	 * @generated
	 */
	EReference getTransitiveObjectProperty_ObjectProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.w3._2002._07.owl.TransitiveObjectProperty#getObjectInverseOf <em>Object Inverse Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Inverse Of</em>'.
	 * @see org.w3._2002._07.owl.TransitiveObjectProperty#getObjectInverseOf()
	 * @see #getTransitiveObjectProperty()
	 * @generated
	 */
	EReference getTransitiveObjectProperty_ObjectInverseOf();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Abbreviated IRI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Abbreviated IRI</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='abbreviatedIRI' baseType='http://www.eclipse.org/emf/2003/XMLType#string'"
	 * @generated
	 */
	EDataType getAbbreviatedIRI();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Name Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Name Type</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='name_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#string'"
	 * @generated
	 */
	EDataType getNameType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OwlFactory getOwlFactory();

} //OwlPackage
