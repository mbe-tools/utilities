/**
 */
package org.w3._2002._07.owl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.w3._2002._07.owl.InverseObjectProperties;
import org.w3._2002._07.owl.ObjectInverseOf;
import org.w3._2002._07.owl.ObjectProperty;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inverse Object Properties</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.InverseObjectPropertiesImpl#getObjectPropertyExpression <em>Object Property Expression</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.InverseObjectPropertiesImpl#getObjectProperty <em>Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.InverseObjectPropertiesImpl#getObjectInverseOf <em>Object Inverse Of</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InverseObjectPropertiesImpl extends ObjectPropertyAxiomImpl implements InverseObjectProperties {
	/**
	 * The cached value of the '{@link #getObjectPropertyExpression() <em>Object Property Expression</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectPropertyExpression()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap objectPropertyExpression;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InverseObjectPropertiesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getInverseObjectProperties();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getObjectPropertyExpression() {
		if (objectPropertyExpression == null) {
			objectPropertyExpression = new BasicFeatureMap(this, OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY_EXPRESSION);
		}
		return objectPropertyExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectProperty> getObjectProperty() {
		return getObjectPropertyExpression().list(OwlPackage.eINSTANCE.getInverseObjectProperties_ObjectProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectInverseOf> getObjectInverseOf() {
		return getObjectPropertyExpression().list(OwlPackage.eINSTANCE.getInverseObjectProperties_ObjectInverseOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY_EXPRESSION:
				return ((InternalEList<?>)getObjectPropertyExpression()).basicRemove(otherEnd, msgs);
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY:
				return ((InternalEList<?>)getObjectProperty()).basicRemove(otherEnd, msgs);
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_INVERSE_OF:
				return ((InternalEList<?>)getObjectInverseOf()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY_EXPRESSION:
				if (coreType) return getObjectPropertyExpression();
				return ((FeatureMap.Internal)getObjectPropertyExpression()).getWrapper();
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY:
				return getObjectProperty();
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_INVERSE_OF:
				return getObjectInverseOf();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY_EXPRESSION:
				((FeatureMap.Internal)getObjectPropertyExpression()).set(newValue);
				return;
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY:
				getObjectProperty().clear();
				getObjectProperty().addAll((Collection<? extends ObjectProperty>)newValue);
				return;
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_INVERSE_OF:
				getObjectInverseOf().clear();
				getObjectInverseOf().addAll((Collection<? extends ObjectInverseOf>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY_EXPRESSION:
				getObjectPropertyExpression().clear();
				return;
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY:
				getObjectProperty().clear();
				return;
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_INVERSE_OF:
				getObjectInverseOf().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY_EXPRESSION:
				return objectPropertyExpression != null && !objectPropertyExpression.isEmpty();
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_PROPERTY:
				return !getObjectProperty().isEmpty();
			case OwlPackage.INVERSE_OBJECT_PROPERTIES__OBJECT_INVERSE_OF:
				return !getObjectInverseOf().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (objectPropertyExpression: ");
		result.append(objectPropertyExpression);
		result.append(')');
		return result.toString();
	}

} //InverseObjectPropertiesImpl
