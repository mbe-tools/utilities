/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Property Assertion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getObjectProperty <em>Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getObjectInverseOf <em>Object Inverse Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getNamedIndividual <em>Named Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getAnonymousIndividual <em>Anonymous Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getAnonymousIndividual1 <em>Anonymous Individual1</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getObjectPropertyAssertion()
 * @model extendedMetaData="name='ObjectPropertyAssertion' kind='elementOnly'"
 * @generated
 */
public interface ObjectPropertyAssertion extends Assertion {
	/**
	 * Returns the value of the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property</em>' containment reference.
	 * @see #setObjectProperty(ObjectProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectPropertyAssertion_ObjectProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectProperty getObjectProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getObjectProperty <em>Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Property</em>' containment reference.
	 * @see #getObjectProperty()
	 * @generated
	 */
	void setObjectProperty(ObjectProperty value);

	/**
	 * Returns the value of the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Inverse Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Inverse Of</em>' containment reference.
	 * @see #setObjectInverseOf(ObjectInverseOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectPropertyAssertion_ObjectInverseOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectInverseOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectInverseOf getObjectInverseOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getObjectInverseOf <em>Object Inverse Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Inverse Of</em>' containment reference.
	 * @see #getObjectInverseOf()
	 * @generated
	 */
	void setObjectInverseOf(ObjectInverseOf value);

	/**
	 * Returns the value of the '<em><b>Named Individual</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.NamedIndividual}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Individual</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Individual</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectPropertyAssertion_NamedIndividual()
	 * @model containment="true" lower="2" upper="2"
	 *        extendedMetaData="kind='element' name='NamedIndividual' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<NamedIndividual> getNamedIndividual();

	/**
	 * Returns the value of the '<em><b>Anonymous Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Individual</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Individual</em>' containment reference.
	 * @see #setAnonymousIndividual(AnonymousIndividual)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectPropertyAssertion_AnonymousIndividual()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AnonymousIndividual' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousIndividual getAnonymousIndividual();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getAnonymousIndividual <em>Anonymous Individual</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Individual</em>' containment reference.
	 * @see #getAnonymousIndividual()
	 * @generated
	 */
	void setAnonymousIndividual(AnonymousIndividual value);

	/**
	 * Returns the value of the '<em><b>Anonymous Individual1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Individual1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Individual1</em>' containment reference.
	 * @see #setAnonymousIndividual1(AnonymousIndividual)
	 * @see org.w3._2002._07.owl.OwlPackage#getObjectPropertyAssertion_AnonymousIndividual1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AnonymousIndividual' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousIndividual getAnonymousIndividual1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.ObjectPropertyAssertion#getAnonymousIndividual1 <em>Anonymous Individual1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Individual1</em>' containment reference.
	 * @see #getAnonymousIndividual1()
	 * @generated
	 */
	void setAnonymousIndividual1(AnonymousIndividual value);

} // ObjectPropertyAssertion
