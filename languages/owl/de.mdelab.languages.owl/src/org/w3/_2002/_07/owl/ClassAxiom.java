/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.w3._2002._07.owl.OwlPackage#getClassAxiom()
 * @model abstract="true"
 *        extendedMetaData="name='ClassAxiom' kind='elementOnly'"
 * @generated
 */
public interface ClassAxiom extends Axiom {
} // ClassAxiom
