/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Property Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.w3._2002._07.owl.OwlPackage#getDataPropertyAxiom()
 * @model abstract="true"
 *        extendedMetaData="name='DataPropertyAxiom' kind='elementOnly'"
 * @generated
 */
public interface DataPropertyAxiom extends Axiom {
} // DataPropertyAxiom
