/**
 */
package org.w3._2002._07.owl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.w3._2002._07.owl.DataProperty;
import org.w3._2002._07.owl.DisjointDataProperties;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Disjoint Data Properties</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointDataPropertiesImpl#getDataPropertyExpression <em>Data Property Expression</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointDataPropertiesImpl#getDataProperty <em>Data Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DisjointDataPropertiesImpl extends DataPropertyAxiomImpl implements DisjointDataProperties {
	/**
	 * The cached value of the '{@link #getDataPropertyExpression() <em>Data Property Expression</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataPropertyExpression()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap dataPropertyExpression;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DisjointDataPropertiesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getDisjointDataProperties();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getDataPropertyExpression() {
		if (dataPropertyExpression == null) {
			dataPropertyExpression = new BasicFeatureMap(this, OwlPackage.DISJOINT_DATA_PROPERTIES__DATA_PROPERTY_EXPRESSION);
		}
		return dataPropertyExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataProperty> getDataProperty() {
		return getDataPropertyExpression().list(OwlPackage.eINSTANCE.getDisjointDataProperties_DataProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.DISJOINT_DATA_PROPERTIES__DATA_PROPERTY_EXPRESSION:
				return ((InternalEList<?>)getDataPropertyExpression()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_DATA_PROPERTIES__DATA_PROPERTY:
				return ((InternalEList<?>)getDataProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.DISJOINT_DATA_PROPERTIES__DATA_PROPERTY_EXPRESSION:
				if (coreType) return getDataPropertyExpression();
				return ((FeatureMap.Internal)getDataPropertyExpression()).getWrapper();
			case OwlPackage.DISJOINT_DATA_PROPERTIES__DATA_PROPERTY:
				return getDataProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.DISJOINT_DATA_PROPERTIES__DATA_PROPERTY_EXPRESSION:
				((FeatureMap.Internal)getDataPropertyExpression()).set(newValue);
				return;
			case OwlPackage.DISJOINT_DATA_PROPERTIES__DATA_PROPERTY:
				getDataProperty().clear();
				getDataProperty().addAll((Collection<? extends DataProperty>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.DISJOINT_DATA_PROPERTIES__DATA_PROPERTY_EXPRESSION:
				getDataPropertyExpression().clear();
				return;
			case OwlPackage.DISJOINT_DATA_PROPERTIES__DATA_PROPERTY:
				getDataProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.DISJOINT_DATA_PROPERTIES__DATA_PROPERTY_EXPRESSION:
				return dataPropertyExpression != null && !dataPropertyExpression.isEmpty();
			case OwlPackage.DISJOINT_DATA_PROPERTIES__DATA_PROPERTY:
				return !getDataProperty().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dataPropertyExpression: ");
		result.append(dataPropertyExpression);
		result.append(')');
		return result.toString();
	}

} //DisjointDataPropertiesImpl
