/**
 */
package org.w3._2002._07.owl.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.w3._2002._07.owl.DataAllValuesFrom;
import org.w3._2002._07.owl.DataExactCardinality;
import org.w3._2002._07.owl.DataHasValue;
import org.w3._2002._07.owl.DataMaxCardinality;
import org.w3._2002._07.owl.DataMinCardinality;
import org.w3._2002._07.owl.DataSomeValuesFrom;
import org.w3._2002._07.owl.ObjectAllValuesFrom;
import org.w3._2002._07.owl.ObjectComplementOf;
import org.w3._2002._07.owl.ObjectExactCardinality;
import org.w3._2002._07.owl.ObjectHasSelf;
import org.w3._2002._07.owl.ObjectHasValue;
import org.w3._2002._07.owl.ObjectIntersectionOf;
import org.w3._2002._07.owl.ObjectMaxCardinality;
import org.w3._2002._07.owl.ObjectMinCardinality;
import org.w3._2002._07.owl.ObjectOneOf;
import org.w3._2002._07.owl.ObjectSomeValuesFrom;
import org.w3._2002._07.owl.ObjectUnionOf;
import org.w3._2002._07.owl.OwlPackage;
import org.w3._2002._07.owl.SubClassOf;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Class Of</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectIntersectionOf <em>Object Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectUnionOf <em>Object Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectComplementOf <em>Object Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectOneOf <em>Object One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectSomeValuesFrom <em>Object Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectAllValuesFrom <em>Object All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectHasValue <em>Object Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectHasSelf <em>Object Has Self</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectMinCardinality <em>Object Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectMaxCardinality <em>Object Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectExactCardinality <em>Object Exact Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getDataSomeValuesFrom <em>Data Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getDataAllValuesFrom <em>Data All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getDataHasValue <em>Data Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getDataMinCardinality <em>Data Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getDataMaxCardinality <em>Data Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getDataExactCardinality <em>Data Exact Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectIntersectionOf1 <em>Object Intersection Of1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectUnionOf1 <em>Object Union Of1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectComplementOf1 <em>Object Complement Of1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectOneOf1 <em>Object One Of1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectSomeValuesFrom1 <em>Object Some Values From1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectAllValuesFrom1 <em>Object All Values From1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectHasValue1 <em>Object Has Value1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectHasSelf1 <em>Object Has Self1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectMinCardinality1 <em>Object Min Cardinality1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectMaxCardinality1 <em>Object Max Cardinality1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getObjectExactCardinality1 <em>Object Exact Cardinality1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getDataSomeValuesFrom1 <em>Data Some Values From1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getDataAllValuesFrom1 <em>Data All Values From1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getDataHasValue1 <em>Data Has Value1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getDataMinCardinality1 <em>Data Min Cardinality1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getDataMaxCardinality1 <em>Data Max Cardinality1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubClassOfImpl#getDataExactCardinality1 <em>Data Exact Cardinality1</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubClassOfImpl extends ClassAxiomImpl implements SubClassOf {
	/**
	 * The cached value of the '{@link #getClass_() <em>Class</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClass_()
	 * @generated
	 * @ordered
	 */
	protected EList<org.w3._2002._07.owl.Class> class_;

	/**
	 * The cached value of the '{@link #getObjectIntersectionOf() <em>Object Intersection Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectIntersectionOf()
	 * @generated
	 * @ordered
	 */
	protected ObjectIntersectionOf objectIntersectionOf;

	/**
	 * The cached value of the '{@link #getObjectUnionOf() <em>Object Union Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectUnionOf()
	 * @generated
	 * @ordered
	 */
	protected ObjectUnionOf objectUnionOf;

	/**
	 * The cached value of the '{@link #getObjectComplementOf() <em>Object Complement Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectComplementOf()
	 * @generated
	 * @ordered
	 */
	protected ObjectComplementOf objectComplementOf;

	/**
	 * The cached value of the '{@link #getObjectOneOf() <em>Object One Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectOneOf()
	 * @generated
	 * @ordered
	 */
	protected ObjectOneOf objectOneOf;

	/**
	 * The cached value of the '{@link #getObjectSomeValuesFrom() <em>Object Some Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 * @ordered
	 */
	protected ObjectSomeValuesFrom objectSomeValuesFrom;

	/**
	 * The cached value of the '{@link #getObjectAllValuesFrom() <em>Object All Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 * @ordered
	 */
	protected ObjectAllValuesFrom objectAllValuesFrom;

	/**
	 * The cached value of the '{@link #getObjectHasValue() <em>Object Has Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectHasValue()
	 * @generated
	 * @ordered
	 */
	protected ObjectHasValue objectHasValue;

	/**
	 * The cached value of the '{@link #getObjectHasSelf() <em>Object Has Self</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectHasSelf()
	 * @generated
	 * @ordered
	 */
	protected ObjectHasSelf objectHasSelf;

	/**
	 * The cached value of the '{@link #getObjectMinCardinality() <em>Object Min Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectMinCardinality()
	 * @generated
	 * @ordered
	 */
	protected ObjectMinCardinality objectMinCardinality;

	/**
	 * The cached value of the '{@link #getObjectMaxCardinality() <em>Object Max Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectMaxCardinality()
	 * @generated
	 * @ordered
	 */
	protected ObjectMaxCardinality objectMaxCardinality;

	/**
	 * The cached value of the '{@link #getObjectExactCardinality() <em>Object Exact Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectExactCardinality()
	 * @generated
	 * @ordered
	 */
	protected ObjectExactCardinality objectExactCardinality;

	/**
	 * The cached value of the '{@link #getDataSomeValuesFrom() <em>Data Some Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataSomeValuesFrom()
	 * @generated
	 * @ordered
	 */
	protected DataSomeValuesFrom dataSomeValuesFrom;

	/**
	 * The cached value of the '{@link #getDataAllValuesFrom() <em>Data All Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataAllValuesFrom()
	 * @generated
	 * @ordered
	 */
	protected DataAllValuesFrom dataAllValuesFrom;

	/**
	 * The cached value of the '{@link #getDataHasValue() <em>Data Has Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataHasValue()
	 * @generated
	 * @ordered
	 */
	protected DataHasValue dataHasValue;

	/**
	 * The cached value of the '{@link #getDataMinCardinality() <em>Data Min Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataMinCardinality()
	 * @generated
	 * @ordered
	 */
	protected DataMinCardinality dataMinCardinality;

	/**
	 * The cached value of the '{@link #getDataMaxCardinality() <em>Data Max Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataMaxCardinality()
	 * @generated
	 * @ordered
	 */
	protected DataMaxCardinality dataMaxCardinality;

	/**
	 * The cached value of the '{@link #getDataExactCardinality() <em>Data Exact Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataExactCardinality()
	 * @generated
	 * @ordered
	 */
	protected DataExactCardinality dataExactCardinality;

	/**
	 * The cached value of the '{@link #getObjectIntersectionOf1() <em>Object Intersection Of1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectIntersectionOf1()
	 * @generated
	 * @ordered
	 */
	protected ObjectIntersectionOf objectIntersectionOf1;

	/**
	 * The cached value of the '{@link #getObjectUnionOf1() <em>Object Union Of1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectUnionOf1()
	 * @generated
	 * @ordered
	 */
	protected ObjectUnionOf objectUnionOf1;

	/**
	 * The cached value of the '{@link #getObjectComplementOf1() <em>Object Complement Of1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectComplementOf1()
	 * @generated
	 * @ordered
	 */
	protected ObjectComplementOf objectComplementOf1;

	/**
	 * The cached value of the '{@link #getObjectOneOf1() <em>Object One Of1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectOneOf1()
	 * @generated
	 * @ordered
	 */
	protected ObjectOneOf objectOneOf1;

	/**
	 * The cached value of the '{@link #getObjectSomeValuesFrom1() <em>Object Some Values From1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectSomeValuesFrom1()
	 * @generated
	 * @ordered
	 */
	protected ObjectSomeValuesFrom objectSomeValuesFrom1;

	/**
	 * The cached value of the '{@link #getObjectAllValuesFrom1() <em>Object All Values From1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectAllValuesFrom1()
	 * @generated
	 * @ordered
	 */
	protected ObjectAllValuesFrom objectAllValuesFrom1;

	/**
	 * The cached value of the '{@link #getObjectHasValue1() <em>Object Has Value1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectHasValue1()
	 * @generated
	 * @ordered
	 */
	protected ObjectHasValue objectHasValue1;

	/**
	 * The cached value of the '{@link #getObjectHasSelf1() <em>Object Has Self1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectHasSelf1()
	 * @generated
	 * @ordered
	 */
	protected ObjectHasSelf objectHasSelf1;

	/**
	 * The cached value of the '{@link #getObjectMinCardinality1() <em>Object Min Cardinality1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectMinCardinality1()
	 * @generated
	 * @ordered
	 */
	protected ObjectMinCardinality objectMinCardinality1;

	/**
	 * The cached value of the '{@link #getObjectMaxCardinality1() <em>Object Max Cardinality1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectMaxCardinality1()
	 * @generated
	 * @ordered
	 */
	protected ObjectMaxCardinality objectMaxCardinality1;

	/**
	 * The cached value of the '{@link #getObjectExactCardinality1() <em>Object Exact Cardinality1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectExactCardinality1()
	 * @generated
	 * @ordered
	 */
	protected ObjectExactCardinality objectExactCardinality1;

	/**
	 * The cached value of the '{@link #getDataSomeValuesFrom1() <em>Data Some Values From1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataSomeValuesFrom1()
	 * @generated
	 * @ordered
	 */
	protected DataSomeValuesFrom dataSomeValuesFrom1;

	/**
	 * The cached value of the '{@link #getDataAllValuesFrom1() <em>Data All Values From1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataAllValuesFrom1()
	 * @generated
	 * @ordered
	 */
	protected DataAllValuesFrom dataAllValuesFrom1;

	/**
	 * The cached value of the '{@link #getDataHasValue1() <em>Data Has Value1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataHasValue1()
	 * @generated
	 * @ordered
	 */
	protected DataHasValue dataHasValue1;

	/**
	 * The cached value of the '{@link #getDataMinCardinality1() <em>Data Min Cardinality1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataMinCardinality1()
	 * @generated
	 * @ordered
	 */
	protected DataMinCardinality dataMinCardinality1;

	/**
	 * The cached value of the '{@link #getDataMaxCardinality1() <em>Data Max Cardinality1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataMaxCardinality1()
	 * @generated
	 * @ordered
	 */
	protected DataMaxCardinality dataMaxCardinality1;

	/**
	 * The cached value of the '{@link #getDataExactCardinality1() <em>Data Exact Cardinality1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataExactCardinality1()
	 * @generated
	 * @ordered
	 */
	protected DataExactCardinality dataExactCardinality1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubClassOfImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getSubClassOf();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<org.w3._2002._07.owl.Class> getClass_() {
		if (class_ == null) {
			class_ = new EObjectContainmentEList<org.w3._2002._07.owl.Class>(org.w3._2002._07.owl.Class.class, this, OwlPackage.SUB_CLASS_OF__CLASS);
		}
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectIntersectionOf getObjectIntersectionOf() {
		return objectIntersectionOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectIntersectionOf(ObjectIntersectionOf newObjectIntersectionOf, NotificationChain msgs) {
		ObjectIntersectionOf oldObjectIntersectionOf = objectIntersectionOf;
		objectIntersectionOf = newObjectIntersectionOf;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF, oldObjectIntersectionOf, newObjectIntersectionOf);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectIntersectionOf(ObjectIntersectionOf newObjectIntersectionOf) {
		if (newObjectIntersectionOf != objectIntersectionOf) {
			NotificationChain msgs = null;
			if (objectIntersectionOf != null)
				msgs = ((InternalEObject)objectIntersectionOf).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF, null, msgs);
			if (newObjectIntersectionOf != null)
				msgs = ((InternalEObject)newObjectIntersectionOf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF, null, msgs);
			msgs = basicSetObjectIntersectionOf(newObjectIntersectionOf, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF, newObjectIntersectionOf, newObjectIntersectionOf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectUnionOf getObjectUnionOf() {
		return objectUnionOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectUnionOf(ObjectUnionOf newObjectUnionOf, NotificationChain msgs) {
		ObjectUnionOf oldObjectUnionOf = objectUnionOf;
		objectUnionOf = newObjectUnionOf;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF, oldObjectUnionOf, newObjectUnionOf);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectUnionOf(ObjectUnionOf newObjectUnionOf) {
		if (newObjectUnionOf != objectUnionOf) {
			NotificationChain msgs = null;
			if (objectUnionOf != null)
				msgs = ((InternalEObject)objectUnionOf).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF, null, msgs);
			if (newObjectUnionOf != null)
				msgs = ((InternalEObject)newObjectUnionOf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF, null, msgs);
			msgs = basicSetObjectUnionOf(newObjectUnionOf, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF, newObjectUnionOf, newObjectUnionOf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectComplementOf getObjectComplementOf() {
		return objectComplementOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectComplementOf(ObjectComplementOf newObjectComplementOf, NotificationChain msgs) {
		ObjectComplementOf oldObjectComplementOf = objectComplementOf;
		objectComplementOf = newObjectComplementOf;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF, oldObjectComplementOf, newObjectComplementOf);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectComplementOf(ObjectComplementOf newObjectComplementOf) {
		if (newObjectComplementOf != objectComplementOf) {
			NotificationChain msgs = null;
			if (objectComplementOf != null)
				msgs = ((InternalEObject)objectComplementOf).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF, null, msgs);
			if (newObjectComplementOf != null)
				msgs = ((InternalEObject)newObjectComplementOf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF, null, msgs);
			msgs = basicSetObjectComplementOf(newObjectComplementOf, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF, newObjectComplementOf, newObjectComplementOf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectOneOf getObjectOneOf() {
		return objectOneOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectOneOf(ObjectOneOf newObjectOneOf, NotificationChain msgs) {
		ObjectOneOf oldObjectOneOf = objectOneOf;
		objectOneOf = newObjectOneOf;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF, oldObjectOneOf, newObjectOneOf);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectOneOf(ObjectOneOf newObjectOneOf) {
		if (newObjectOneOf != objectOneOf) {
			NotificationChain msgs = null;
			if (objectOneOf != null)
				msgs = ((InternalEObject)objectOneOf).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF, null, msgs);
			if (newObjectOneOf != null)
				msgs = ((InternalEObject)newObjectOneOf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF, null, msgs);
			msgs = basicSetObjectOneOf(newObjectOneOf, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF, newObjectOneOf, newObjectOneOf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectSomeValuesFrom getObjectSomeValuesFrom() {
		return objectSomeValuesFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectSomeValuesFrom(ObjectSomeValuesFrom newObjectSomeValuesFrom, NotificationChain msgs) {
		ObjectSomeValuesFrom oldObjectSomeValuesFrom = objectSomeValuesFrom;
		objectSomeValuesFrom = newObjectSomeValuesFrom;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM, oldObjectSomeValuesFrom, newObjectSomeValuesFrom);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectSomeValuesFrom(ObjectSomeValuesFrom newObjectSomeValuesFrom) {
		if (newObjectSomeValuesFrom != objectSomeValuesFrom) {
			NotificationChain msgs = null;
			if (objectSomeValuesFrom != null)
				msgs = ((InternalEObject)objectSomeValuesFrom).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM, null, msgs);
			if (newObjectSomeValuesFrom != null)
				msgs = ((InternalEObject)newObjectSomeValuesFrom).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM, null, msgs);
			msgs = basicSetObjectSomeValuesFrom(newObjectSomeValuesFrom, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM, newObjectSomeValuesFrom, newObjectSomeValuesFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectAllValuesFrom getObjectAllValuesFrom() {
		return objectAllValuesFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectAllValuesFrom(ObjectAllValuesFrom newObjectAllValuesFrom, NotificationChain msgs) {
		ObjectAllValuesFrom oldObjectAllValuesFrom = objectAllValuesFrom;
		objectAllValuesFrom = newObjectAllValuesFrom;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM, oldObjectAllValuesFrom, newObjectAllValuesFrom);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectAllValuesFrom(ObjectAllValuesFrom newObjectAllValuesFrom) {
		if (newObjectAllValuesFrom != objectAllValuesFrom) {
			NotificationChain msgs = null;
			if (objectAllValuesFrom != null)
				msgs = ((InternalEObject)objectAllValuesFrom).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM, null, msgs);
			if (newObjectAllValuesFrom != null)
				msgs = ((InternalEObject)newObjectAllValuesFrom).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM, null, msgs);
			msgs = basicSetObjectAllValuesFrom(newObjectAllValuesFrom, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM, newObjectAllValuesFrom, newObjectAllValuesFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectHasValue getObjectHasValue() {
		return objectHasValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectHasValue(ObjectHasValue newObjectHasValue, NotificationChain msgs) {
		ObjectHasValue oldObjectHasValue = objectHasValue;
		objectHasValue = newObjectHasValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE, oldObjectHasValue, newObjectHasValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectHasValue(ObjectHasValue newObjectHasValue) {
		if (newObjectHasValue != objectHasValue) {
			NotificationChain msgs = null;
			if (objectHasValue != null)
				msgs = ((InternalEObject)objectHasValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE, null, msgs);
			if (newObjectHasValue != null)
				msgs = ((InternalEObject)newObjectHasValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE, null, msgs);
			msgs = basicSetObjectHasValue(newObjectHasValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE, newObjectHasValue, newObjectHasValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectHasSelf getObjectHasSelf() {
		return objectHasSelf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectHasSelf(ObjectHasSelf newObjectHasSelf, NotificationChain msgs) {
		ObjectHasSelf oldObjectHasSelf = objectHasSelf;
		objectHasSelf = newObjectHasSelf;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF, oldObjectHasSelf, newObjectHasSelf);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectHasSelf(ObjectHasSelf newObjectHasSelf) {
		if (newObjectHasSelf != objectHasSelf) {
			NotificationChain msgs = null;
			if (objectHasSelf != null)
				msgs = ((InternalEObject)objectHasSelf).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF, null, msgs);
			if (newObjectHasSelf != null)
				msgs = ((InternalEObject)newObjectHasSelf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF, null, msgs);
			msgs = basicSetObjectHasSelf(newObjectHasSelf, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF, newObjectHasSelf, newObjectHasSelf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectMinCardinality getObjectMinCardinality() {
		return objectMinCardinality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectMinCardinality(ObjectMinCardinality newObjectMinCardinality, NotificationChain msgs) {
		ObjectMinCardinality oldObjectMinCardinality = objectMinCardinality;
		objectMinCardinality = newObjectMinCardinality;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY, oldObjectMinCardinality, newObjectMinCardinality);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectMinCardinality(ObjectMinCardinality newObjectMinCardinality) {
		if (newObjectMinCardinality != objectMinCardinality) {
			NotificationChain msgs = null;
			if (objectMinCardinality != null)
				msgs = ((InternalEObject)objectMinCardinality).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY, null, msgs);
			if (newObjectMinCardinality != null)
				msgs = ((InternalEObject)newObjectMinCardinality).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY, null, msgs);
			msgs = basicSetObjectMinCardinality(newObjectMinCardinality, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY, newObjectMinCardinality, newObjectMinCardinality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectMaxCardinality getObjectMaxCardinality() {
		return objectMaxCardinality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectMaxCardinality(ObjectMaxCardinality newObjectMaxCardinality, NotificationChain msgs) {
		ObjectMaxCardinality oldObjectMaxCardinality = objectMaxCardinality;
		objectMaxCardinality = newObjectMaxCardinality;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY, oldObjectMaxCardinality, newObjectMaxCardinality);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectMaxCardinality(ObjectMaxCardinality newObjectMaxCardinality) {
		if (newObjectMaxCardinality != objectMaxCardinality) {
			NotificationChain msgs = null;
			if (objectMaxCardinality != null)
				msgs = ((InternalEObject)objectMaxCardinality).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY, null, msgs);
			if (newObjectMaxCardinality != null)
				msgs = ((InternalEObject)newObjectMaxCardinality).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY, null, msgs);
			msgs = basicSetObjectMaxCardinality(newObjectMaxCardinality, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY, newObjectMaxCardinality, newObjectMaxCardinality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectExactCardinality getObjectExactCardinality() {
		return objectExactCardinality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectExactCardinality(ObjectExactCardinality newObjectExactCardinality, NotificationChain msgs) {
		ObjectExactCardinality oldObjectExactCardinality = objectExactCardinality;
		objectExactCardinality = newObjectExactCardinality;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY, oldObjectExactCardinality, newObjectExactCardinality);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectExactCardinality(ObjectExactCardinality newObjectExactCardinality) {
		if (newObjectExactCardinality != objectExactCardinality) {
			NotificationChain msgs = null;
			if (objectExactCardinality != null)
				msgs = ((InternalEObject)objectExactCardinality).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY, null, msgs);
			if (newObjectExactCardinality != null)
				msgs = ((InternalEObject)newObjectExactCardinality).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY, null, msgs);
			msgs = basicSetObjectExactCardinality(newObjectExactCardinality, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY, newObjectExactCardinality, newObjectExactCardinality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSomeValuesFrom getDataSomeValuesFrom() {
		return dataSomeValuesFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataSomeValuesFrom(DataSomeValuesFrom newDataSomeValuesFrom, NotificationChain msgs) {
		DataSomeValuesFrom oldDataSomeValuesFrom = dataSomeValuesFrom;
		dataSomeValuesFrom = newDataSomeValuesFrom;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM, oldDataSomeValuesFrom, newDataSomeValuesFrom);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataSomeValuesFrom(DataSomeValuesFrom newDataSomeValuesFrom) {
		if (newDataSomeValuesFrom != dataSomeValuesFrom) {
			NotificationChain msgs = null;
			if (dataSomeValuesFrom != null)
				msgs = ((InternalEObject)dataSomeValuesFrom).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM, null, msgs);
			if (newDataSomeValuesFrom != null)
				msgs = ((InternalEObject)newDataSomeValuesFrom).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM, null, msgs);
			msgs = basicSetDataSomeValuesFrom(newDataSomeValuesFrom, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM, newDataSomeValuesFrom, newDataSomeValuesFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAllValuesFrom getDataAllValuesFrom() {
		return dataAllValuesFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataAllValuesFrom(DataAllValuesFrom newDataAllValuesFrom, NotificationChain msgs) {
		DataAllValuesFrom oldDataAllValuesFrom = dataAllValuesFrom;
		dataAllValuesFrom = newDataAllValuesFrom;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM, oldDataAllValuesFrom, newDataAllValuesFrom);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataAllValuesFrom(DataAllValuesFrom newDataAllValuesFrom) {
		if (newDataAllValuesFrom != dataAllValuesFrom) {
			NotificationChain msgs = null;
			if (dataAllValuesFrom != null)
				msgs = ((InternalEObject)dataAllValuesFrom).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM, null, msgs);
			if (newDataAllValuesFrom != null)
				msgs = ((InternalEObject)newDataAllValuesFrom).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM, null, msgs);
			msgs = basicSetDataAllValuesFrom(newDataAllValuesFrom, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM, newDataAllValuesFrom, newDataAllValuesFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataHasValue getDataHasValue() {
		return dataHasValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataHasValue(DataHasValue newDataHasValue, NotificationChain msgs) {
		DataHasValue oldDataHasValue = dataHasValue;
		dataHasValue = newDataHasValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE, oldDataHasValue, newDataHasValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataHasValue(DataHasValue newDataHasValue) {
		if (newDataHasValue != dataHasValue) {
			NotificationChain msgs = null;
			if (dataHasValue != null)
				msgs = ((InternalEObject)dataHasValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE, null, msgs);
			if (newDataHasValue != null)
				msgs = ((InternalEObject)newDataHasValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE, null, msgs);
			msgs = basicSetDataHasValue(newDataHasValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE, newDataHasValue, newDataHasValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataMinCardinality getDataMinCardinality() {
		return dataMinCardinality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataMinCardinality(DataMinCardinality newDataMinCardinality, NotificationChain msgs) {
		DataMinCardinality oldDataMinCardinality = dataMinCardinality;
		dataMinCardinality = newDataMinCardinality;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY, oldDataMinCardinality, newDataMinCardinality);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataMinCardinality(DataMinCardinality newDataMinCardinality) {
		if (newDataMinCardinality != dataMinCardinality) {
			NotificationChain msgs = null;
			if (dataMinCardinality != null)
				msgs = ((InternalEObject)dataMinCardinality).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY, null, msgs);
			if (newDataMinCardinality != null)
				msgs = ((InternalEObject)newDataMinCardinality).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY, null, msgs);
			msgs = basicSetDataMinCardinality(newDataMinCardinality, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY, newDataMinCardinality, newDataMinCardinality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataMaxCardinality getDataMaxCardinality() {
		return dataMaxCardinality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataMaxCardinality(DataMaxCardinality newDataMaxCardinality, NotificationChain msgs) {
		DataMaxCardinality oldDataMaxCardinality = dataMaxCardinality;
		dataMaxCardinality = newDataMaxCardinality;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY, oldDataMaxCardinality, newDataMaxCardinality);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataMaxCardinality(DataMaxCardinality newDataMaxCardinality) {
		if (newDataMaxCardinality != dataMaxCardinality) {
			NotificationChain msgs = null;
			if (dataMaxCardinality != null)
				msgs = ((InternalEObject)dataMaxCardinality).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY, null, msgs);
			if (newDataMaxCardinality != null)
				msgs = ((InternalEObject)newDataMaxCardinality).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY, null, msgs);
			msgs = basicSetDataMaxCardinality(newDataMaxCardinality, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY, newDataMaxCardinality, newDataMaxCardinality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataExactCardinality getDataExactCardinality() {
		return dataExactCardinality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataExactCardinality(DataExactCardinality newDataExactCardinality, NotificationChain msgs) {
		DataExactCardinality oldDataExactCardinality = dataExactCardinality;
		dataExactCardinality = newDataExactCardinality;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY, oldDataExactCardinality, newDataExactCardinality);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataExactCardinality(DataExactCardinality newDataExactCardinality) {
		if (newDataExactCardinality != dataExactCardinality) {
			NotificationChain msgs = null;
			if (dataExactCardinality != null)
				msgs = ((InternalEObject)dataExactCardinality).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY, null, msgs);
			if (newDataExactCardinality != null)
				msgs = ((InternalEObject)newDataExactCardinality).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY, null, msgs);
			msgs = basicSetDataExactCardinality(newDataExactCardinality, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY, newDataExactCardinality, newDataExactCardinality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectIntersectionOf getObjectIntersectionOf1() {
		return objectIntersectionOf1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectIntersectionOf1(ObjectIntersectionOf newObjectIntersectionOf1, NotificationChain msgs) {
		ObjectIntersectionOf oldObjectIntersectionOf1 = objectIntersectionOf1;
		objectIntersectionOf1 = newObjectIntersectionOf1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF1, oldObjectIntersectionOf1, newObjectIntersectionOf1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectIntersectionOf1(ObjectIntersectionOf newObjectIntersectionOf1) {
		if (newObjectIntersectionOf1 != objectIntersectionOf1) {
			NotificationChain msgs = null;
			if (objectIntersectionOf1 != null)
				msgs = ((InternalEObject)objectIntersectionOf1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF1, null, msgs);
			if (newObjectIntersectionOf1 != null)
				msgs = ((InternalEObject)newObjectIntersectionOf1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF1, null, msgs);
			msgs = basicSetObjectIntersectionOf1(newObjectIntersectionOf1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF1, newObjectIntersectionOf1, newObjectIntersectionOf1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectUnionOf getObjectUnionOf1() {
		return objectUnionOf1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectUnionOf1(ObjectUnionOf newObjectUnionOf1, NotificationChain msgs) {
		ObjectUnionOf oldObjectUnionOf1 = objectUnionOf1;
		objectUnionOf1 = newObjectUnionOf1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF1, oldObjectUnionOf1, newObjectUnionOf1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectUnionOf1(ObjectUnionOf newObjectUnionOf1) {
		if (newObjectUnionOf1 != objectUnionOf1) {
			NotificationChain msgs = null;
			if (objectUnionOf1 != null)
				msgs = ((InternalEObject)objectUnionOf1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF1, null, msgs);
			if (newObjectUnionOf1 != null)
				msgs = ((InternalEObject)newObjectUnionOf1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF1, null, msgs);
			msgs = basicSetObjectUnionOf1(newObjectUnionOf1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF1, newObjectUnionOf1, newObjectUnionOf1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectComplementOf getObjectComplementOf1() {
		return objectComplementOf1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectComplementOf1(ObjectComplementOf newObjectComplementOf1, NotificationChain msgs) {
		ObjectComplementOf oldObjectComplementOf1 = objectComplementOf1;
		objectComplementOf1 = newObjectComplementOf1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF1, oldObjectComplementOf1, newObjectComplementOf1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectComplementOf1(ObjectComplementOf newObjectComplementOf1) {
		if (newObjectComplementOf1 != objectComplementOf1) {
			NotificationChain msgs = null;
			if (objectComplementOf1 != null)
				msgs = ((InternalEObject)objectComplementOf1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF1, null, msgs);
			if (newObjectComplementOf1 != null)
				msgs = ((InternalEObject)newObjectComplementOf1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF1, null, msgs);
			msgs = basicSetObjectComplementOf1(newObjectComplementOf1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF1, newObjectComplementOf1, newObjectComplementOf1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectOneOf getObjectOneOf1() {
		return objectOneOf1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectOneOf1(ObjectOneOf newObjectOneOf1, NotificationChain msgs) {
		ObjectOneOf oldObjectOneOf1 = objectOneOf1;
		objectOneOf1 = newObjectOneOf1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF1, oldObjectOneOf1, newObjectOneOf1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectOneOf1(ObjectOneOf newObjectOneOf1) {
		if (newObjectOneOf1 != objectOneOf1) {
			NotificationChain msgs = null;
			if (objectOneOf1 != null)
				msgs = ((InternalEObject)objectOneOf1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF1, null, msgs);
			if (newObjectOneOf1 != null)
				msgs = ((InternalEObject)newObjectOneOf1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF1, null, msgs);
			msgs = basicSetObjectOneOf1(newObjectOneOf1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF1, newObjectOneOf1, newObjectOneOf1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectSomeValuesFrom getObjectSomeValuesFrom1() {
		return objectSomeValuesFrom1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectSomeValuesFrom1(ObjectSomeValuesFrom newObjectSomeValuesFrom1, NotificationChain msgs) {
		ObjectSomeValuesFrom oldObjectSomeValuesFrom1 = objectSomeValuesFrom1;
		objectSomeValuesFrom1 = newObjectSomeValuesFrom1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM1, oldObjectSomeValuesFrom1, newObjectSomeValuesFrom1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectSomeValuesFrom1(ObjectSomeValuesFrom newObjectSomeValuesFrom1) {
		if (newObjectSomeValuesFrom1 != objectSomeValuesFrom1) {
			NotificationChain msgs = null;
			if (objectSomeValuesFrom1 != null)
				msgs = ((InternalEObject)objectSomeValuesFrom1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM1, null, msgs);
			if (newObjectSomeValuesFrom1 != null)
				msgs = ((InternalEObject)newObjectSomeValuesFrom1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM1, null, msgs);
			msgs = basicSetObjectSomeValuesFrom1(newObjectSomeValuesFrom1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM1, newObjectSomeValuesFrom1, newObjectSomeValuesFrom1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectAllValuesFrom getObjectAllValuesFrom1() {
		return objectAllValuesFrom1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectAllValuesFrom1(ObjectAllValuesFrom newObjectAllValuesFrom1, NotificationChain msgs) {
		ObjectAllValuesFrom oldObjectAllValuesFrom1 = objectAllValuesFrom1;
		objectAllValuesFrom1 = newObjectAllValuesFrom1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM1, oldObjectAllValuesFrom1, newObjectAllValuesFrom1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectAllValuesFrom1(ObjectAllValuesFrom newObjectAllValuesFrom1) {
		if (newObjectAllValuesFrom1 != objectAllValuesFrom1) {
			NotificationChain msgs = null;
			if (objectAllValuesFrom1 != null)
				msgs = ((InternalEObject)objectAllValuesFrom1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM1, null, msgs);
			if (newObjectAllValuesFrom1 != null)
				msgs = ((InternalEObject)newObjectAllValuesFrom1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM1, null, msgs);
			msgs = basicSetObjectAllValuesFrom1(newObjectAllValuesFrom1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM1, newObjectAllValuesFrom1, newObjectAllValuesFrom1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectHasValue getObjectHasValue1() {
		return objectHasValue1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectHasValue1(ObjectHasValue newObjectHasValue1, NotificationChain msgs) {
		ObjectHasValue oldObjectHasValue1 = objectHasValue1;
		objectHasValue1 = newObjectHasValue1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE1, oldObjectHasValue1, newObjectHasValue1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectHasValue1(ObjectHasValue newObjectHasValue1) {
		if (newObjectHasValue1 != objectHasValue1) {
			NotificationChain msgs = null;
			if (objectHasValue1 != null)
				msgs = ((InternalEObject)objectHasValue1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE1, null, msgs);
			if (newObjectHasValue1 != null)
				msgs = ((InternalEObject)newObjectHasValue1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE1, null, msgs);
			msgs = basicSetObjectHasValue1(newObjectHasValue1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE1, newObjectHasValue1, newObjectHasValue1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectHasSelf getObjectHasSelf1() {
		return objectHasSelf1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectHasSelf1(ObjectHasSelf newObjectHasSelf1, NotificationChain msgs) {
		ObjectHasSelf oldObjectHasSelf1 = objectHasSelf1;
		objectHasSelf1 = newObjectHasSelf1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF1, oldObjectHasSelf1, newObjectHasSelf1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectHasSelf1(ObjectHasSelf newObjectHasSelf1) {
		if (newObjectHasSelf1 != objectHasSelf1) {
			NotificationChain msgs = null;
			if (objectHasSelf1 != null)
				msgs = ((InternalEObject)objectHasSelf1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF1, null, msgs);
			if (newObjectHasSelf1 != null)
				msgs = ((InternalEObject)newObjectHasSelf1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF1, null, msgs);
			msgs = basicSetObjectHasSelf1(newObjectHasSelf1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF1, newObjectHasSelf1, newObjectHasSelf1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectMinCardinality getObjectMinCardinality1() {
		return objectMinCardinality1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectMinCardinality1(ObjectMinCardinality newObjectMinCardinality1, NotificationChain msgs) {
		ObjectMinCardinality oldObjectMinCardinality1 = objectMinCardinality1;
		objectMinCardinality1 = newObjectMinCardinality1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY1, oldObjectMinCardinality1, newObjectMinCardinality1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectMinCardinality1(ObjectMinCardinality newObjectMinCardinality1) {
		if (newObjectMinCardinality1 != objectMinCardinality1) {
			NotificationChain msgs = null;
			if (objectMinCardinality1 != null)
				msgs = ((InternalEObject)objectMinCardinality1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY1, null, msgs);
			if (newObjectMinCardinality1 != null)
				msgs = ((InternalEObject)newObjectMinCardinality1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY1, null, msgs);
			msgs = basicSetObjectMinCardinality1(newObjectMinCardinality1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY1, newObjectMinCardinality1, newObjectMinCardinality1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectMaxCardinality getObjectMaxCardinality1() {
		return objectMaxCardinality1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectMaxCardinality1(ObjectMaxCardinality newObjectMaxCardinality1, NotificationChain msgs) {
		ObjectMaxCardinality oldObjectMaxCardinality1 = objectMaxCardinality1;
		objectMaxCardinality1 = newObjectMaxCardinality1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY1, oldObjectMaxCardinality1, newObjectMaxCardinality1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectMaxCardinality1(ObjectMaxCardinality newObjectMaxCardinality1) {
		if (newObjectMaxCardinality1 != objectMaxCardinality1) {
			NotificationChain msgs = null;
			if (objectMaxCardinality1 != null)
				msgs = ((InternalEObject)objectMaxCardinality1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY1, null, msgs);
			if (newObjectMaxCardinality1 != null)
				msgs = ((InternalEObject)newObjectMaxCardinality1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY1, null, msgs);
			msgs = basicSetObjectMaxCardinality1(newObjectMaxCardinality1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY1, newObjectMaxCardinality1, newObjectMaxCardinality1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectExactCardinality getObjectExactCardinality1() {
		return objectExactCardinality1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectExactCardinality1(ObjectExactCardinality newObjectExactCardinality1, NotificationChain msgs) {
		ObjectExactCardinality oldObjectExactCardinality1 = objectExactCardinality1;
		objectExactCardinality1 = newObjectExactCardinality1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY1, oldObjectExactCardinality1, newObjectExactCardinality1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectExactCardinality1(ObjectExactCardinality newObjectExactCardinality1) {
		if (newObjectExactCardinality1 != objectExactCardinality1) {
			NotificationChain msgs = null;
			if (objectExactCardinality1 != null)
				msgs = ((InternalEObject)objectExactCardinality1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY1, null, msgs);
			if (newObjectExactCardinality1 != null)
				msgs = ((InternalEObject)newObjectExactCardinality1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY1, null, msgs);
			msgs = basicSetObjectExactCardinality1(newObjectExactCardinality1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY1, newObjectExactCardinality1, newObjectExactCardinality1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSomeValuesFrom getDataSomeValuesFrom1() {
		return dataSomeValuesFrom1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataSomeValuesFrom1(DataSomeValuesFrom newDataSomeValuesFrom1, NotificationChain msgs) {
		DataSomeValuesFrom oldDataSomeValuesFrom1 = dataSomeValuesFrom1;
		dataSomeValuesFrom1 = newDataSomeValuesFrom1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM1, oldDataSomeValuesFrom1, newDataSomeValuesFrom1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataSomeValuesFrom1(DataSomeValuesFrom newDataSomeValuesFrom1) {
		if (newDataSomeValuesFrom1 != dataSomeValuesFrom1) {
			NotificationChain msgs = null;
			if (dataSomeValuesFrom1 != null)
				msgs = ((InternalEObject)dataSomeValuesFrom1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM1, null, msgs);
			if (newDataSomeValuesFrom1 != null)
				msgs = ((InternalEObject)newDataSomeValuesFrom1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM1, null, msgs);
			msgs = basicSetDataSomeValuesFrom1(newDataSomeValuesFrom1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM1, newDataSomeValuesFrom1, newDataSomeValuesFrom1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAllValuesFrom getDataAllValuesFrom1() {
		return dataAllValuesFrom1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataAllValuesFrom1(DataAllValuesFrom newDataAllValuesFrom1, NotificationChain msgs) {
		DataAllValuesFrom oldDataAllValuesFrom1 = dataAllValuesFrom1;
		dataAllValuesFrom1 = newDataAllValuesFrom1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM1, oldDataAllValuesFrom1, newDataAllValuesFrom1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataAllValuesFrom1(DataAllValuesFrom newDataAllValuesFrom1) {
		if (newDataAllValuesFrom1 != dataAllValuesFrom1) {
			NotificationChain msgs = null;
			if (dataAllValuesFrom1 != null)
				msgs = ((InternalEObject)dataAllValuesFrom1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM1, null, msgs);
			if (newDataAllValuesFrom1 != null)
				msgs = ((InternalEObject)newDataAllValuesFrom1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM1, null, msgs);
			msgs = basicSetDataAllValuesFrom1(newDataAllValuesFrom1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM1, newDataAllValuesFrom1, newDataAllValuesFrom1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataHasValue getDataHasValue1() {
		return dataHasValue1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataHasValue1(DataHasValue newDataHasValue1, NotificationChain msgs) {
		DataHasValue oldDataHasValue1 = dataHasValue1;
		dataHasValue1 = newDataHasValue1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE1, oldDataHasValue1, newDataHasValue1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataHasValue1(DataHasValue newDataHasValue1) {
		if (newDataHasValue1 != dataHasValue1) {
			NotificationChain msgs = null;
			if (dataHasValue1 != null)
				msgs = ((InternalEObject)dataHasValue1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE1, null, msgs);
			if (newDataHasValue1 != null)
				msgs = ((InternalEObject)newDataHasValue1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE1, null, msgs);
			msgs = basicSetDataHasValue1(newDataHasValue1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE1, newDataHasValue1, newDataHasValue1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataMinCardinality getDataMinCardinality1() {
		return dataMinCardinality1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataMinCardinality1(DataMinCardinality newDataMinCardinality1, NotificationChain msgs) {
		DataMinCardinality oldDataMinCardinality1 = dataMinCardinality1;
		dataMinCardinality1 = newDataMinCardinality1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY1, oldDataMinCardinality1, newDataMinCardinality1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataMinCardinality1(DataMinCardinality newDataMinCardinality1) {
		if (newDataMinCardinality1 != dataMinCardinality1) {
			NotificationChain msgs = null;
			if (dataMinCardinality1 != null)
				msgs = ((InternalEObject)dataMinCardinality1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY1, null, msgs);
			if (newDataMinCardinality1 != null)
				msgs = ((InternalEObject)newDataMinCardinality1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY1, null, msgs);
			msgs = basicSetDataMinCardinality1(newDataMinCardinality1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY1, newDataMinCardinality1, newDataMinCardinality1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataMaxCardinality getDataMaxCardinality1() {
		return dataMaxCardinality1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataMaxCardinality1(DataMaxCardinality newDataMaxCardinality1, NotificationChain msgs) {
		DataMaxCardinality oldDataMaxCardinality1 = dataMaxCardinality1;
		dataMaxCardinality1 = newDataMaxCardinality1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY1, oldDataMaxCardinality1, newDataMaxCardinality1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataMaxCardinality1(DataMaxCardinality newDataMaxCardinality1) {
		if (newDataMaxCardinality1 != dataMaxCardinality1) {
			NotificationChain msgs = null;
			if (dataMaxCardinality1 != null)
				msgs = ((InternalEObject)dataMaxCardinality1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY1, null, msgs);
			if (newDataMaxCardinality1 != null)
				msgs = ((InternalEObject)newDataMaxCardinality1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY1, null, msgs);
			msgs = basicSetDataMaxCardinality1(newDataMaxCardinality1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY1, newDataMaxCardinality1, newDataMaxCardinality1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataExactCardinality getDataExactCardinality1() {
		return dataExactCardinality1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataExactCardinality1(DataExactCardinality newDataExactCardinality1, NotificationChain msgs) {
		DataExactCardinality oldDataExactCardinality1 = dataExactCardinality1;
		dataExactCardinality1 = newDataExactCardinality1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY1, oldDataExactCardinality1, newDataExactCardinality1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataExactCardinality1(DataExactCardinality newDataExactCardinality1) {
		if (newDataExactCardinality1 != dataExactCardinality1) {
			NotificationChain msgs = null;
			if (dataExactCardinality1 != null)
				msgs = ((InternalEObject)dataExactCardinality1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY1, null, msgs);
			if (newDataExactCardinality1 != null)
				msgs = ((InternalEObject)newDataExactCardinality1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY1, null, msgs);
			msgs = basicSetDataExactCardinality1(newDataExactCardinality1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY1, newDataExactCardinality1, newDataExactCardinality1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.SUB_CLASS_OF__CLASS:
				return ((InternalEList<?>)getClass_()).basicRemove(otherEnd, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF:
				return basicSetObjectIntersectionOf(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF:
				return basicSetObjectUnionOf(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF:
				return basicSetObjectComplementOf(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF:
				return basicSetObjectOneOf(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM:
				return basicSetObjectSomeValuesFrom(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM:
				return basicSetObjectAllValuesFrom(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE:
				return basicSetObjectHasValue(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF:
				return basicSetObjectHasSelf(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY:
				return basicSetObjectMinCardinality(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY:
				return basicSetObjectMaxCardinality(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY:
				return basicSetObjectExactCardinality(null, msgs);
			case OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM:
				return basicSetDataSomeValuesFrom(null, msgs);
			case OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM:
				return basicSetDataAllValuesFrom(null, msgs);
			case OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE:
				return basicSetDataHasValue(null, msgs);
			case OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY:
				return basicSetDataMinCardinality(null, msgs);
			case OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY:
				return basicSetDataMaxCardinality(null, msgs);
			case OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY:
				return basicSetDataExactCardinality(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF1:
				return basicSetObjectIntersectionOf1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF1:
				return basicSetObjectUnionOf1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF1:
				return basicSetObjectComplementOf1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF1:
				return basicSetObjectOneOf1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM1:
				return basicSetObjectSomeValuesFrom1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM1:
				return basicSetObjectAllValuesFrom1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE1:
				return basicSetObjectHasValue1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF1:
				return basicSetObjectHasSelf1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY1:
				return basicSetObjectMinCardinality1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY1:
				return basicSetObjectMaxCardinality1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY1:
				return basicSetObjectExactCardinality1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM1:
				return basicSetDataSomeValuesFrom1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM1:
				return basicSetDataAllValuesFrom1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE1:
				return basicSetDataHasValue1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY1:
				return basicSetDataMinCardinality1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY1:
				return basicSetDataMaxCardinality1(null, msgs);
			case OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY1:
				return basicSetDataExactCardinality1(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.SUB_CLASS_OF__CLASS:
				return getClass_();
			case OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF:
				return getObjectIntersectionOf();
			case OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF:
				return getObjectUnionOf();
			case OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF:
				return getObjectComplementOf();
			case OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF:
				return getObjectOneOf();
			case OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM:
				return getObjectSomeValuesFrom();
			case OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM:
				return getObjectAllValuesFrom();
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE:
				return getObjectHasValue();
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF:
				return getObjectHasSelf();
			case OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY:
				return getObjectMinCardinality();
			case OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY:
				return getObjectMaxCardinality();
			case OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY:
				return getObjectExactCardinality();
			case OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM:
				return getDataSomeValuesFrom();
			case OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM:
				return getDataAllValuesFrom();
			case OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE:
				return getDataHasValue();
			case OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY:
				return getDataMinCardinality();
			case OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY:
				return getDataMaxCardinality();
			case OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY:
				return getDataExactCardinality();
			case OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF1:
				return getObjectIntersectionOf1();
			case OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF1:
				return getObjectUnionOf1();
			case OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF1:
				return getObjectComplementOf1();
			case OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF1:
				return getObjectOneOf1();
			case OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM1:
				return getObjectSomeValuesFrom1();
			case OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM1:
				return getObjectAllValuesFrom1();
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE1:
				return getObjectHasValue1();
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF1:
				return getObjectHasSelf1();
			case OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY1:
				return getObjectMinCardinality1();
			case OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY1:
				return getObjectMaxCardinality1();
			case OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY1:
				return getObjectExactCardinality1();
			case OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM1:
				return getDataSomeValuesFrom1();
			case OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM1:
				return getDataAllValuesFrom1();
			case OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE1:
				return getDataHasValue1();
			case OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY1:
				return getDataMinCardinality1();
			case OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY1:
				return getDataMaxCardinality1();
			case OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY1:
				return getDataExactCardinality1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.SUB_CLASS_OF__CLASS:
				getClass_().clear();
				getClass_().addAll((Collection<? extends org.w3._2002._07.owl.Class>)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF:
				setObjectIntersectionOf((ObjectIntersectionOf)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF:
				setObjectUnionOf((ObjectUnionOf)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF:
				setObjectComplementOf((ObjectComplementOf)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF:
				setObjectOneOf((ObjectOneOf)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM:
				setObjectSomeValuesFrom((ObjectSomeValuesFrom)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM:
				setObjectAllValuesFrom((ObjectAllValuesFrom)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE:
				setObjectHasValue((ObjectHasValue)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF:
				setObjectHasSelf((ObjectHasSelf)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY:
				setObjectMinCardinality((ObjectMinCardinality)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY:
				setObjectMaxCardinality((ObjectMaxCardinality)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY:
				setObjectExactCardinality((ObjectExactCardinality)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM:
				setDataSomeValuesFrom((DataSomeValuesFrom)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM:
				setDataAllValuesFrom((DataAllValuesFrom)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE:
				setDataHasValue((DataHasValue)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY:
				setDataMinCardinality((DataMinCardinality)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY:
				setDataMaxCardinality((DataMaxCardinality)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY:
				setDataExactCardinality((DataExactCardinality)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF1:
				setObjectIntersectionOf1((ObjectIntersectionOf)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF1:
				setObjectUnionOf1((ObjectUnionOf)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF1:
				setObjectComplementOf1((ObjectComplementOf)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF1:
				setObjectOneOf1((ObjectOneOf)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM1:
				setObjectSomeValuesFrom1((ObjectSomeValuesFrom)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM1:
				setObjectAllValuesFrom1((ObjectAllValuesFrom)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE1:
				setObjectHasValue1((ObjectHasValue)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF1:
				setObjectHasSelf1((ObjectHasSelf)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY1:
				setObjectMinCardinality1((ObjectMinCardinality)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY1:
				setObjectMaxCardinality1((ObjectMaxCardinality)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY1:
				setObjectExactCardinality1((ObjectExactCardinality)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM1:
				setDataSomeValuesFrom1((DataSomeValuesFrom)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM1:
				setDataAllValuesFrom1((DataAllValuesFrom)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE1:
				setDataHasValue1((DataHasValue)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY1:
				setDataMinCardinality1((DataMinCardinality)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY1:
				setDataMaxCardinality1((DataMaxCardinality)newValue);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY1:
				setDataExactCardinality1((DataExactCardinality)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.SUB_CLASS_OF__CLASS:
				getClass_().clear();
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF:
				setObjectIntersectionOf((ObjectIntersectionOf)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF:
				setObjectUnionOf((ObjectUnionOf)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF:
				setObjectComplementOf((ObjectComplementOf)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF:
				setObjectOneOf((ObjectOneOf)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM:
				setObjectSomeValuesFrom((ObjectSomeValuesFrom)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM:
				setObjectAllValuesFrom((ObjectAllValuesFrom)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE:
				setObjectHasValue((ObjectHasValue)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF:
				setObjectHasSelf((ObjectHasSelf)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY:
				setObjectMinCardinality((ObjectMinCardinality)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY:
				setObjectMaxCardinality((ObjectMaxCardinality)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY:
				setObjectExactCardinality((ObjectExactCardinality)null);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM:
				setDataSomeValuesFrom((DataSomeValuesFrom)null);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM:
				setDataAllValuesFrom((DataAllValuesFrom)null);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE:
				setDataHasValue((DataHasValue)null);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY:
				setDataMinCardinality((DataMinCardinality)null);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY:
				setDataMaxCardinality((DataMaxCardinality)null);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY:
				setDataExactCardinality((DataExactCardinality)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF1:
				setObjectIntersectionOf1((ObjectIntersectionOf)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF1:
				setObjectUnionOf1((ObjectUnionOf)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF1:
				setObjectComplementOf1((ObjectComplementOf)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF1:
				setObjectOneOf1((ObjectOneOf)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM1:
				setObjectSomeValuesFrom1((ObjectSomeValuesFrom)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM1:
				setObjectAllValuesFrom1((ObjectAllValuesFrom)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE1:
				setObjectHasValue1((ObjectHasValue)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF1:
				setObjectHasSelf1((ObjectHasSelf)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY1:
				setObjectMinCardinality1((ObjectMinCardinality)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY1:
				setObjectMaxCardinality1((ObjectMaxCardinality)null);
				return;
			case OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY1:
				setObjectExactCardinality1((ObjectExactCardinality)null);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM1:
				setDataSomeValuesFrom1((DataSomeValuesFrom)null);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM1:
				setDataAllValuesFrom1((DataAllValuesFrom)null);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE1:
				setDataHasValue1((DataHasValue)null);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY1:
				setDataMinCardinality1((DataMinCardinality)null);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY1:
				setDataMaxCardinality1((DataMaxCardinality)null);
				return;
			case OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY1:
				setDataExactCardinality1((DataExactCardinality)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.SUB_CLASS_OF__CLASS:
				return class_ != null && !class_.isEmpty();
			case OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF:
				return objectIntersectionOf != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF:
				return objectUnionOf != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF:
				return objectComplementOf != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF:
				return objectOneOf != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM:
				return objectSomeValuesFrom != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM:
				return objectAllValuesFrom != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE:
				return objectHasValue != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF:
				return objectHasSelf != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY:
				return objectMinCardinality != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY:
				return objectMaxCardinality != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY:
				return objectExactCardinality != null;
			case OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM:
				return dataSomeValuesFrom != null;
			case OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM:
				return dataAllValuesFrom != null;
			case OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE:
				return dataHasValue != null;
			case OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY:
				return dataMinCardinality != null;
			case OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY:
				return dataMaxCardinality != null;
			case OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY:
				return dataExactCardinality != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF1:
				return objectIntersectionOf1 != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF1:
				return objectUnionOf1 != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF1:
				return objectComplementOf1 != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF1:
				return objectOneOf1 != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM1:
				return objectSomeValuesFrom1 != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM1:
				return objectAllValuesFrom1 != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE1:
				return objectHasValue1 != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF1:
				return objectHasSelf1 != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY1:
				return objectMinCardinality1 != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY1:
				return objectMaxCardinality1 != null;
			case OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY1:
				return objectExactCardinality1 != null;
			case OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM1:
				return dataSomeValuesFrom1 != null;
			case OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM1:
				return dataAllValuesFrom1 != null;
			case OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE1:
				return dataHasValue1 != null;
			case OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY1:
				return dataMinCardinality1 != null;
			case OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY1:
				return dataMaxCardinality1 != null;
			case OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY1:
				return dataExactCardinality1 != null;
		}
		return super.eIsSet(featureID);
	}

} //SubClassOfImpl
