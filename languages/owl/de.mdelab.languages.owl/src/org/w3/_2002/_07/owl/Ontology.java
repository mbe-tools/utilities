/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

import org.eclipse.emf.ecore.xml.namespace.SpaceType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ontology</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getImport <em>Import</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getAnnotation <em>Annotation</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getAxiom <em>Axiom</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getDeclaration <em>Declaration</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getSubClassOf <em>Sub Class Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getEquivalentClasses <em>Equivalent Classes</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getDisjointClasses <em>Disjoint Classes</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getDisjointUnion <em>Disjoint Union</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getSubObjectPropertyOf <em>Sub Object Property Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getEquivalentObjectProperties <em>Equivalent Object Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getDisjointObjectProperties <em>Disjoint Object Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getInverseObjectProperties <em>Inverse Object Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getObjectPropertyDomain <em>Object Property Domain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getObjectPropertyRange <em>Object Property Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getFunctionalObjectProperty <em>Functional Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getInverseFunctionalObjectProperty <em>Inverse Functional Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getReflexiveObjectProperty <em>Reflexive Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getIrreflexiveObjectProperty <em>Irreflexive Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getSymmetricObjectProperty <em>Symmetric Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getAsymmetricObjectProperty <em>Asymmetric Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getTransitiveObjectProperty <em>Transitive Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getSubDataPropertyOf <em>Sub Data Property Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getEquivalentDataProperties <em>Equivalent Data Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getDisjointDataProperties <em>Disjoint Data Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getDataPropertyDomain <em>Data Property Domain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getDataPropertyRange <em>Data Property Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getFunctionalDataProperty <em>Functional Data Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getDatatypeDefinition <em>Datatype Definition</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getHasKey <em>Has Key</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getSameIndividual <em>Same Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getDifferentIndividuals <em>Different Individuals</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getClassAssertion <em>Class Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getObjectPropertyAssertion <em>Object Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getNegativeObjectPropertyAssertion <em>Negative Object Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getDataPropertyAssertion <em>Data Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getNegativeDataPropertyAssertion <em>Negative Data Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getAnnotationAssertion <em>Annotation Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getSubAnnotationPropertyOf <em>Sub Annotation Property Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getAnnotationPropertyDomain <em>Annotation Property Domain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getAnnotationPropertyRange <em>Annotation Property Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getBase <em>Base</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getId <em>Id</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getLang <em>Lang</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getOntologyIRI <em>Ontology IRI</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getSpace <em>Space</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Ontology#getVersionIRI <em>Version IRI</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getOntology()
 * @model extendedMetaData="name='Ontology' kind='elementOnly'"
 * @generated
 */
public interface Ontology extends EObject {
	/**
	 * Returns the value of the '<em><b>Prefix</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.Prefix}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prefix</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prefix</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_Prefix()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Prefix' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Prefix> getPrefix();

	/**
	 * Returns the value of the '<em><b>Import</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.Import}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_Import()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Import' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Import> getImport();

	/**
	 * Returns the value of the '<em><b>Annotation</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.Annotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_Annotation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Annotation' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Annotation> getAnnotation();

	/**
	 * Returns the value of the '<em><b>Axiom</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Axiom</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Axiom</em>' attribute list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_Axiom()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='Axiom:3'"
	 * @generated
	 */
	FeatureMap getAxiom();

	/**
	 * Returns the value of the '<em><b>Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.Declaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Declaration</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_Declaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Declaration' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<Declaration> getDeclaration();

	/**
	 * Returns the value of the '<em><b>Sub Class Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.SubClassOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Class Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Class Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_SubClassOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SubClassOf' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<SubClassOf> getSubClassOf();

	/**
	 * Returns the value of the '<em><b>Equivalent Classes</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.EquivalentClasses}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equivalent Classes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equivalent Classes</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_EquivalentClasses()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='EquivalentClasses' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<EquivalentClasses> getEquivalentClasses();

	/**
	 * Returns the value of the '<em><b>Disjoint Classes</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DisjointClasses}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disjoint Classes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disjoint Classes</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_DisjointClasses()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DisjointClasses' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<DisjointClasses> getDisjointClasses();

	/**
	 * Returns the value of the '<em><b>Disjoint Union</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DisjointUnion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disjoint Union</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disjoint Union</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_DisjointUnion()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DisjointUnion' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<DisjointUnion> getDisjointUnion();

	/**
	 * Returns the value of the '<em><b>Sub Object Property Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.SubObjectPropertyOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Object Property Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Object Property Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_SubObjectPropertyOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SubObjectPropertyOf' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<SubObjectPropertyOf> getSubObjectPropertyOf();

	/**
	 * Returns the value of the '<em><b>Equivalent Object Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.EquivalentObjectProperties}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equivalent Object Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equivalent Object Properties</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_EquivalentObjectProperties()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='EquivalentObjectProperties' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<EquivalentObjectProperties> getEquivalentObjectProperties();

	/**
	 * Returns the value of the '<em><b>Disjoint Object Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DisjointObjectProperties}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disjoint Object Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disjoint Object Properties</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_DisjointObjectProperties()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DisjointObjectProperties' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<DisjointObjectProperties> getDisjointObjectProperties();

	/**
	 * Returns the value of the '<em><b>Inverse Object Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.InverseObjectProperties}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inverse Object Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inverse Object Properties</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_InverseObjectProperties()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='InverseObjectProperties' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<InverseObjectProperties> getInverseObjectProperties();

	/**
	 * Returns the value of the '<em><b>Object Property Domain</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectPropertyDomain}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property Domain</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property Domain</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_ObjectPropertyDomain()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectPropertyDomain' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<ObjectPropertyDomain> getObjectPropertyDomain();

	/**
	 * Returns the value of the '<em><b>Object Property Range</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectPropertyRange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property Range</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property Range</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_ObjectPropertyRange()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectPropertyRange' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<ObjectPropertyRange> getObjectPropertyRange();

	/**
	 * Returns the value of the '<em><b>Functional Object Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.FunctionalObjectProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functional Object Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functional Object Property</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_FunctionalObjectProperty()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='FunctionalObjectProperty' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<FunctionalObjectProperty> getFunctionalObjectProperty();

	/**
	 * Returns the value of the '<em><b>Inverse Functional Object Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.InverseFunctionalObjectProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inverse Functional Object Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inverse Functional Object Property</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_InverseFunctionalObjectProperty()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='InverseFunctionalObjectProperty' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<InverseFunctionalObjectProperty> getInverseFunctionalObjectProperty();

	/**
	 * Returns the value of the '<em><b>Reflexive Object Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ReflexiveObjectProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reflexive Object Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reflexive Object Property</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_ReflexiveObjectProperty()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ReflexiveObjectProperty' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<ReflexiveObjectProperty> getReflexiveObjectProperty();

	/**
	 * Returns the value of the '<em><b>Irreflexive Object Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.IrreflexiveObjectProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Irreflexive Object Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Irreflexive Object Property</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_IrreflexiveObjectProperty()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='IrreflexiveObjectProperty' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<IrreflexiveObjectProperty> getIrreflexiveObjectProperty();

	/**
	 * Returns the value of the '<em><b>Symmetric Object Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.SymmetricObjectProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symmetric Object Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symmetric Object Property</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_SymmetricObjectProperty()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SymmetricObjectProperty' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<SymmetricObjectProperty> getSymmetricObjectProperty();

	/**
	 * Returns the value of the '<em><b>Asymmetric Object Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.AsymmetricObjectProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asymmetric Object Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asymmetric Object Property</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_AsymmetricObjectProperty()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AsymmetricObjectProperty' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<AsymmetricObjectProperty> getAsymmetricObjectProperty();

	/**
	 * Returns the value of the '<em><b>Transitive Object Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.TransitiveObjectProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitive Object Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitive Object Property</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_TransitiveObjectProperty()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='TransitiveObjectProperty' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<TransitiveObjectProperty> getTransitiveObjectProperty();

	/**
	 * Returns the value of the '<em><b>Sub Data Property Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.SubDataPropertyOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Data Property Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Data Property Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_SubDataPropertyOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SubDataPropertyOf' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<SubDataPropertyOf> getSubDataPropertyOf();

	/**
	 * Returns the value of the '<em><b>Equivalent Data Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.EquivalentDataProperties}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equivalent Data Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equivalent Data Properties</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_EquivalentDataProperties()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='EquivalentDataProperties' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<EquivalentDataProperties> getEquivalentDataProperties();

	/**
	 * Returns the value of the '<em><b>Disjoint Data Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DisjointDataProperties}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disjoint Data Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disjoint Data Properties</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_DisjointDataProperties()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DisjointDataProperties' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<DisjointDataProperties> getDisjointDataProperties();

	/**
	 * Returns the value of the '<em><b>Data Property Domain</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataPropertyDomain}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property Domain</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property Domain</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_DataPropertyDomain()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataPropertyDomain' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<DataPropertyDomain> getDataPropertyDomain();

	/**
	 * Returns the value of the '<em><b>Data Property Range</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataPropertyRange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property Range</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property Range</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_DataPropertyRange()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataPropertyRange' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<DataPropertyRange> getDataPropertyRange();

	/**
	 * Returns the value of the '<em><b>Functional Data Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.FunctionalDataProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functional Data Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functional Data Property</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_FunctionalDataProperty()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='FunctionalDataProperty' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<FunctionalDataProperty> getFunctionalDataProperty();

	/**
	 * Returns the value of the '<em><b>Datatype Definition</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DatatypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype Definition</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_DatatypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DatatypeDefinition' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<DatatypeDefinition> getDatatypeDefinition();

	/**
	 * Returns the value of the '<em><b>Has Key</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.HasKey}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Key</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Key</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_HasKey()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='HasKey' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<HasKey> getHasKey();

	/**
	 * Returns the value of the '<em><b>Same Individual</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.SameIndividual}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Same Individual</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Same Individual</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_SameIndividual()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SameIndividual' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<SameIndividual> getSameIndividual();

	/**
	 * Returns the value of the '<em><b>Different Individuals</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DifferentIndividuals}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Different Individuals</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Different Individuals</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_DifferentIndividuals()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DifferentIndividuals' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<DifferentIndividuals> getDifferentIndividuals();

	/**
	 * Returns the value of the '<em><b>Class Assertion</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ClassAssertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Assertion</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Assertion</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_ClassAssertion()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ClassAssertion' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<ClassAssertion> getClassAssertion();

	/**
	 * Returns the value of the '<em><b>Object Property Assertion</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.ObjectPropertyAssertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property Assertion</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property Assertion</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_ObjectPropertyAssertion()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectPropertyAssertion' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<ObjectPropertyAssertion> getObjectPropertyAssertion();

	/**
	 * Returns the value of the '<em><b>Negative Object Property Assertion</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.NegativeObjectPropertyAssertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Negative Object Property Assertion</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Negative Object Property Assertion</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_NegativeObjectPropertyAssertion()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='NegativeObjectPropertyAssertion' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<NegativeObjectPropertyAssertion> getNegativeObjectPropertyAssertion();

	/**
	 * Returns the value of the '<em><b>Data Property Assertion</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataPropertyAssertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property Assertion</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property Assertion</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_DataPropertyAssertion()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataPropertyAssertion' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<DataPropertyAssertion> getDataPropertyAssertion();

	/**
	 * Returns the value of the '<em><b>Negative Data Property Assertion</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.NegativeDataPropertyAssertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Negative Data Property Assertion</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Negative Data Property Assertion</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_NegativeDataPropertyAssertion()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='NegativeDataPropertyAssertion' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<NegativeDataPropertyAssertion> getNegativeDataPropertyAssertion();

	/**
	 * Returns the value of the '<em><b>Annotation Assertion</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.AnnotationAssertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Assertion</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Assertion</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_AnnotationAssertion()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AnnotationAssertion' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<AnnotationAssertion> getAnnotationAssertion();

	/**
	 * Returns the value of the '<em><b>Sub Annotation Property Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.SubAnnotationPropertyOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Annotation Property Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Annotation Property Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_SubAnnotationPropertyOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SubAnnotationPropertyOf' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<SubAnnotationPropertyOf> getSubAnnotationPropertyOf();

	/**
	 * Returns the value of the '<em><b>Annotation Property Domain</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.AnnotationPropertyDomain}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Property Domain</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Property Domain</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_AnnotationPropertyDomain()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AnnotationPropertyDomain' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<AnnotationPropertyDomain> getAnnotationPropertyDomain();

	/**
	 * Returns the value of the '<em><b>Annotation Property Range</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.AnnotationPropertyRange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Property Range</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Property Range</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_AnnotationPropertyRange()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AnnotationPropertyRange' namespace='##targetNamespace' group='#Axiom:3'"
	 * @generated
	 */
	EList<AnnotationPropertyRange> getAnnotationPropertyRange();

	/**
	 * Returns the value of the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 *     <div xmlns="http://www.w3.org/1999/xhtml">
	 *      
	 *       <h3>base (as an attribute name)</h3>
	 *       <p>
	 *        denotes an attribute whose value
	 *        provides a URI to be used as the base for interpreting any
	 *        relative URIs in the scope of the element on which it
	 *        appears; its value is inherited.  This name is reserved
	 *        by virtue of its definition in the XML Base specification.</p>
	 *      
	 *      <p>
	 *       See <a href="http://www.w3.org/TR/xmlbase/">http://www.w3.org/TR/xmlbase/</a>
	 *       for information about this attribute.
	 *      </p>
	 *     </div>
	 *    
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Base</em>' attribute.
	 * @see #setBase(String)
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_Base()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='base' namespace='http://www.w3.org/XML/1998/namespace'"
	 * @generated
	 */
	String getBase();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Ontology#getBase <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base</em>' attribute.
	 * @see #getBase()
	 * @generated
	 */
	void setBase(String value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 *     <div xmlns="http://www.w3.org/1999/xhtml">
	 *      
	 *       <h3>id (as an attribute name)</h3> 
	 *       <p>
	 *        denotes an attribute whose value
	 *        should be interpreted as if declared to be of type ID.
	 *        This name is reserved by virtue of its definition in the
	 *        xml:id specification.</p>
	 *      
	 *      <p>
	 *       See <a href="http://www.w3.org/TR/xml-id/">http://www.w3.org/TR/xml-id/</a>
	 *       for information about this attribute.
	 *      </p>
	 *     </div>
	 *    
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_Id()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID"
	 *        extendedMetaData="kind='attribute' name='id' namespace='http://www.w3.org/XML/1998/namespace'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Ontology#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 *     <div xmlns="http://www.w3.org/1999/xhtml">
	 *      
	 *       <h3>lang (as an attribute name)</h3>
	 *       <p>
	 *        denotes an attribute whose value
	 *        is a language code for the natural language of the content of
	 *        any element; its value is inherited.  This name is reserved
	 *        by virtue of its definition in the XML specification.</p>
	 *      
	 *     </div>
	 *     <div xmlns="http://www.w3.org/1999/xhtml">
	 *      <h4>Notes</h4>
	 *      <p>
	 *       Attempting to install the relevant ISO 2- and 3-letter
	 *       codes as the enumerated possible values is probably never
	 *       going to be a realistic possibility.  
	 *      </p>
	 *      <p>
	 *       See BCP 47 at <a href="http://www.rfc-editor.org/rfc/bcp/bcp47.txt">
	 *        http://www.rfc-editor.org/rfc/bcp/bcp47.txt</a>
	 *       and the IANA language subtag registry at
	 *       <a href="http://www.iana.org/assignments/language-subtag-registry">
	 *        http://www.iana.org/assignments/language-subtag-registry</a>
	 *       for further information.
	 *      </p>
	 *      <p>
	 *       The union allows for the 'un-declaration' of xml:lang with
	 *       the empty string.
	 *      </p>
	 *     </div>
	 *    
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Lang</em>' attribute.
	 * @see #setLang(String)
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_Lang()
	 * @model dataType="org.eclipse.emf.ecore.xml.namespace.LangType"
	 *        extendedMetaData="kind='attribute' name='lang' namespace='http://www.w3.org/XML/1998/namespace'"
	 * @generated
	 */
	String getLang();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Ontology#getLang <em>Lang</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lang</em>' attribute.
	 * @see #getLang()
	 * @generated
	 */
	void setLang(String value);

	/**
	 * Returns the value of the '<em><b>Ontology IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ontology IRI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ontology IRI</em>' attribute.
	 * @see #setOntologyIRI(String)
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_OntologyIRI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='ontologyIRI'"
	 * @generated
	 */
	String getOntologyIRI();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Ontology#getOntologyIRI <em>Ontology IRI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ontology IRI</em>' attribute.
	 * @see #getOntologyIRI()
	 * @generated
	 */
	void setOntologyIRI(String value);

	/**
	 * Returns the value of the '<em><b>Space</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.emf.ecore.xml.namespace.SpaceType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 *     <div xmlns="http://www.w3.org/1999/xhtml">
	 *      
	 *       <h3>space (as an attribute name)</h3>
	 *       <p>
	 *        denotes an attribute whose
	 *        value is a keyword indicating what whitespace processing
	 *        discipline is intended for the content of the element; its
	 *        value is inherited.  This name is reserved by virtue of its
	 *        definition in the XML specification.</p>
	 *      
	 *     </div>
	 *    
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Space</em>' attribute.
	 * @see org.eclipse.emf.ecore.xml.namespace.SpaceType
	 * @see #isSetSpace()
	 * @see #unsetSpace()
	 * @see #setSpace(SpaceType)
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_Space()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='space' namespace='http://www.w3.org/XML/1998/namespace'"
	 * @generated
	 */
	SpaceType getSpace();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Ontology#getSpace <em>Space</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Space</em>' attribute.
	 * @see org.eclipse.emf.ecore.xml.namespace.SpaceType
	 * @see #isSetSpace()
	 * @see #unsetSpace()
	 * @see #getSpace()
	 * @generated
	 */
	void setSpace(SpaceType value);

	/**
	 * Unsets the value of the '{@link org.w3._2002._07.owl.Ontology#getSpace <em>Space</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSpace()
	 * @see #getSpace()
	 * @see #setSpace(SpaceType)
	 * @generated
	 */
	void unsetSpace();

	/**
	 * Returns whether the value of the '{@link org.w3._2002._07.owl.Ontology#getSpace <em>Space</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Space</em>' attribute is set.
	 * @see #unsetSpace()
	 * @see #getSpace()
	 * @see #setSpace(SpaceType)
	 * @generated
	 */
	boolean isSetSpace();

	/**
	 * Returns the value of the '<em><b>Version IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version IRI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version IRI</em>' attribute.
	 * @see #setVersionIRI(String)
	 * @see org.w3._2002._07.owl.OwlPackage#getOntology_VersionIRI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='versionIRI'"
	 * @generated
	 */
	String getVersionIRI();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Ontology#getVersionIRI <em>Version IRI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version IRI</em>' attribute.
	 * @see #getVersionIRI()
	 * @generated
	 */
	void setVersionIRI(String value);

} // Ontology
