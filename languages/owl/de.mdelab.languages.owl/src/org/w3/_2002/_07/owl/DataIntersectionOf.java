/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Intersection Of</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.DataIntersectionOf#getDataRange <em>Data Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataIntersectionOf#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataIntersectionOf#getDataIntersectionOf <em>Data Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataIntersectionOf#getDataUnionOf <em>Data Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataIntersectionOf#getDataComplementOf <em>Data Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataIntersectionOf#getDataOneOf <em>Data One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DataIntersectionOf#getDatatypeRestriction <em>Datatype Restriction</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getDataIntersectionOf()
 * @model extendedMetaData="name='DataIntersectionOf' kind='elementOnly'"
 * @generated
 */
public interface DataIntersectionOf extends DataRange {
	/**
	 * Returns the value of the '<em><b>Data Range</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Range</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Range</em>' attribute list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDataIntersectionOf_DataRange()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='DataRange:4'"
	 * @generated
	 */
	FeatureMap getDataRange();

	/**
	 * Returns the value of the '<em><b>Datatype</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.Datatype}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDataIntersectionOf_Datatype()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Datatype' namespace='##targetNamespace' group='#DataRange:4'"
	 * @generated
	 */
	EList<Datatype> getDatatype();

	/**
	 * Returns the value of the '<em><b>Data Intersection Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataIntersectionOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Intersection Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Intersection Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDataIntersectionOf_DataIntersectionOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataIntersectionOf' namespace='##targetNamespace' group='#DataRange:4'"
	 * @generated
	 */
	EList<DataIntersectionOf> getDataIntersectionOf();

	/**
	 * Returns the value of the '<em><b>Data Union Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataUnionOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Union Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Union Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDataIntersectionOf_DataUnionOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataUnionOf' namespace='##targetNamespace' group='#DataRange:4'"
	 * @generated
	 */
	EList<DataUnionOf> getDataUnionOf();

	/**
	 * Returns the value of the '<em><b>Data Complement Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataComplementOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Complement Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Complement Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDataIntersectionOf_DataComplementOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataComplementOf' namespace='##targetNamespace' group='#DataRange:4'"
	 * @generated
	 */
	EList<DataComplementOf> getDataComplementOf();

	/**
	 * Returns the value of the '<em><b>Data One Of</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataOneOf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data One Of</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data One Of</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDataIntersectionOf_DataOneOf()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataOneOf' namespace='##targetNamespace' group='#DataRange:4'"
	 * @generated
	 */
	EList<DataOneOf> getDataOneOf();

	/**
	 * Returns the value of the '<em><b>Datatype Restriction</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DatatypeRestriction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype Restriction</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype Restriction</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDataIntersectionOf_DatatypeRestriction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DatatypeRestriction' namespace='##targetNamespace' group='#DataRange:4'"
	 * @generated
	 */
	EList<DatatypeRestriction> getDatatypeRestriction();

} // DataIntersectionOf
