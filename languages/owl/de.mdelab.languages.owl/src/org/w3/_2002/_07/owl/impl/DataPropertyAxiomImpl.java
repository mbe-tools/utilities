/**
 */
package org.w3._2002._07.owl.impl;

import org.eclipse.emf.ecore.EClass;

import org.w3._2002._07.owl.DataPropertyAxiom;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Property Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DataPropertyAxiomImpl extends AxiomImpl implements DataPropertyAxiom {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataPropertyAxiomImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getDataPropertyAxiom();
	}

} //DataPropertyAxiomImpl
