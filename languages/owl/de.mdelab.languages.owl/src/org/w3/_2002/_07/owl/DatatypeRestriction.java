/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Datatype Restriction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.DatatypeRestriction#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DatatypeRestriction#getFacetRestriction <em>Facet Restriction</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getDatatypeRestriction()
 * @model extendedMetaData="name='DatatypeRestriction' kind='elementOnly'"
 * @generated
 */
public interface DatatypeRestriction extends DataRange {
	/**
	 * Returns the value of the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype</em>' containment reference.
	 * @see #setDatatype(Datatype)
	 * @see org.w3._2002._07.owl.OwlPackage#getDatatypeRestriction_Datatype()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Datatype' namespace='##targetNamespace'"
	 * @generated
	 */
	Datatype getDatatype();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DatatypeRestriction#getDatatype <em>Datatype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype</em>' containment reference.
	 * @see #getDatatype()
	 * @generated
	 */
	void setDatatype(Datatype value);

	/**
	 * Returns the value of the '<em><b>Facet Restriction</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.FacetRestriction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Facet Restriction</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Facet Restriction</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDatatypeRestriction_FacetRestriction()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='FacetRestriction' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<FacetRestriction> getFacetRestriction();

} // DatatypeRestriction
