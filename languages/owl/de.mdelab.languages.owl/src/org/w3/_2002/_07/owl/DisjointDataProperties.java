/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Disjoint Data Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.DisjointDataProperties#getDataPropertyExpression <em>Data Property Expression</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DisjointDataProperties#getDataProperty <em>Data Property</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getDisjointDataProperties()
 * @model extendedMetaData="name='DisjointDataProperties' kind='elementOnly'"
 * @generated
 */
public interface DisjointDataProperties extends DataPropertyAxiom {
	/**
	 * Returns the value of the '<em><b>Data Property Expression</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property Expression</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property Expression</em>' attribute list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDisjointDataProperties_DataPropertyExpression()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='DataPropertyExpression:5'"
	 * @generated
	 */
	FeatureMap getDataPropertyExpression();

	/**
	 * Returns the value of the '<em><b>Data Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.w3._2002._07.owl.DataProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property</em>' containment reference list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDisjointDataProperties_DataProperty()
	 * @model containment="true" lower="2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataProperty' namespace='##targetNamespace' group='#DataPropertyExpression:5'"
	 * @generated
	 */
	EList<DataProperty> getDataProperty();

} // DisjointDataProperties
