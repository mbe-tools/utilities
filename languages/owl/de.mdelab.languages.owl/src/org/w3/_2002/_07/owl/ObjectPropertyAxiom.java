/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Property Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.w3._2002._07.owl.OwlPackage#getObjectPropertyAxiom()
 * @model abstract="true"
 *        extendedMetaData="name='ObjectPropertyAxiom' kind='elementOnly'"
 * @generated
 */
public interface ObjectPropertyAxiom extends Axiom {
} // ObjectPropertyAxiom
