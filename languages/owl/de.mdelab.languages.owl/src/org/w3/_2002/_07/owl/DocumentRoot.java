/**
 */
package org.w3._2002._07.owl;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getMixed <em>Mixed</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getAbbreviatedIRI <em>Abbreviated IRI</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getAnnotation <em>Annotation</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getAnnotationAssertion <em>Annotation Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getAnnotationProperty <em>Annotation Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getAnnotationPropertyDomain <em>Annotation Property Domain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getAnnotationPropertyRange <em>Annotation Property Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getAnonymousIndividual <em>Anonymous Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getAsymmetricObjectProperty <em>Asymmetric Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getClassAssertion <em>Class Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataAllValuesFrom <em>Data All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataComplementOf <em>Data Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataExactCardinality <em>Data Exact Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataHasValue <em>Data Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataIntersectionOf <em>Data Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataMaxCardinality <em>Data Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataMinCardinality <em>Data Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataOneOf <em>Data One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataProperty <em>Data Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataPropertyAssertion <em>Data Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataPropertyDomain <em>Data Property Domain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataPropertyRange <em>Data Property Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataSomeValuesFrom <em>Data Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDatatypeDefinition <em>Datatype Definition</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDatatypeRestriction <em>Datatype Restriction</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDataUnionOf <em>Data Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDeclaration <em>Declaration</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDifferentIndividuals <em>Different Individuals</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDisjointClasses <em>Disjoint Classes</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDisjointDataProperties <em>Disjoint Data Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDisjointObjectProperties <em>Disjoint Object Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getDisjointUnion <em>Disjoint Union</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getEquivalentClasses <em>Equivalent Classes</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getEquivalentDataProperties <em>Equivalent Data Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getEquivalentObjectProperties <em>Equivalent Object Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getFunctionalDataProperty <em>Functional Data Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getFunctionalObjectProperty <em>Functional Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getHasKey <em>Has Key</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getImport <em>Import</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getInverseFunctionalObjectProperty <em>Inverse Functional Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getInverseObjectProperties <em>Inverse Object Properties</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getIRI <em>IRI</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getIrreflexiveObjectProperty <em>Irreflexive Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getLiteral <em>Literal</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getNamedIndividual <em>Named Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getNegativeDataPropertyAssertion <em>Negative Data Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getNegativeObjectPropertyAssertion <em>Negative Object Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectAllValuesFrom <em>Object All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectComplementOf <em>Object Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectExactCardinality <em>Object Exact Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectHasSelf <em>Object Has Self</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectHasValue <em>Object Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectIntersectionOf <em>Object Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectInverseOf <em>Object Inverse Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectMaxCardinality <em>Object Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectMinCardinality <em>Object Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectOneOf <em>Object One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectProperty <em>Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectPropertyAssertion <em>Object Property Assertion</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectPropertyDomain <em>Object Property Domain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectPropertyRange <em>Object Property Range</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectSomeValuesFrom <em>Object Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getObjectUnionOf <em>Object Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getOntology <em>Ontology</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getReflexiveObjectProperty <em>Reflexive Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getSameIndividual <em>Same Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getSubAnnotationPropertyOf <em>Sub Annotation Property Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getSubClassOf <em>Sub Class Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getSubDataPropertyOf <em>Sub Data Property Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getSubObjectPropertyOf <em>Sub Object Property Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getSymmetricObjectProperty <em>Symmetric Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.DocumentRoot#getTransitiveObjectProperty <em>Transitive Object Property</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot()
 * @model extendedMetaData="name='' kind='mixed'"
 * @generated
 */
public interface DocumentRoot extends EObject {
	/**
	 * Returns the value of the '<em><b>Mixed</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mixed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mixed</em>' attribute list.
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_Mixed()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' name=':mixed'"
	 * @generated
	 */
	FeatureMap getMixed();

	/**
	 * Returns the value of the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XMLNS Prefix Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XMLNS Prefix Map</em>' map.
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_XMLNSPrefixMap()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;" transient="true"
	 *        extendedMetaData="kind='attribute' name='xmlns:prefix'"
	 * @generated
	 */
	EMap<String, String> getXMLNSPrefixMap();

	/**
	 * Returns the value of the '<em><b>XSI Schema Location</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XSI Schema Location</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XSI Schema Location</em>' map.
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_XSISchemaLocation()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;" transient="true"
	 *        extendedMetaData="kind='attribute' name='xsi:schemaLocation'"
	 * @generated
	 */
	EMap<String, String> getXSISchemaLocation();

	/**
	 * Returns the value of the '<em><b>Abbreviated IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abbreviated IRI</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abbreviated IRI</em>' containment reference.
	 * @see #setAbbreviatedIRI(AbbreviatedIRI1)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_AbbreviatedIRI()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AbbreviatedIRI' namespace='##targetNamespace'"
	 * @generated
	 */
	AbbreviatedIRI1 getAbbreviatedIRI();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getAbbreviatedIRI <em>Abbreviated IRI</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abbreviated IRI</em>' containment reference.
	 * @see #getAbbreviatedIRI()
	 * @generated
	 */
	void setAbbreviatedIRI(AbbreviatedIRI1 value);

	/**
	 * Returns the value of the '<em><b>Annotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation</em>' containment reference.
	 * @see #setAnnotation(Annotation)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_Annotation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Annotation' namespace='##targetNamespace'"
	 * @generated
	 */
	Annotation getAnnotation();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getAnnotation <em>Annotation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation</em>' containment reference.
	 * @see #getAnnotation()
	 * @generated
	 */
	void setAnnotation(Annotation value);

	/**
	 * Returns the value of the '<em><b>Annotation Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Assertion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Assertion</em>' containment reference.
	 * @see #setAnnotationAssertion(AnnotationAssertion)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_AnnotationAssertion()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AnnotationAssertion' namespace='##targetNamespace'"
	 * @generated
	 */
	AnnotationAssertion getAnnotationAssertion();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getAnnotationAssertion <em>Annotation Assertion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Assertion</em>' containment reference.
	 * @see #getAnnotationAssertion()
	 * @generated
	 */
	void setAnnotationAssertion(AnnotationAssertion value);

	/**
	 * Returns the value of the '<em><b>Annotation Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Property</em>' containment reference.
	 * @see #setAnnotationProperty(AnnotationProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_AnnotationProperty()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AnnotationProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	AnnotationProperty getAnnotationProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getAnnotationProperty <em>Annotation Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Property</em>' containment reference.
	 * @see #getAnnotationProperty()
	 * @generated
	 */
	void setAnnotationProperty(AnnotationProperty value);

	/**
	 * Returns the value of the '<em><b>Annotation Property Domain</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Property Domain</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Property Domain</em>' containment reference.
	 * @see #setAnnotationPropertyDomain(AnnotationPropertyDomain)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_AnnotationPropertyDomain()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AnnotationPropertyDomain' namespace='##targetNamespace'"
	 * @generated
	 */
	AnnotationPropertyDomain getAnnotationPropertyDomain();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getAnnotationPropertyDomain <em>Annotation Property Domain</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Property Domain</em>' containment reference.
	 * @see #getAnnotationPropertyDomain()
	 * @generated
	 */
	void setAnnotationPropertyDomain(AnnotationPropertyDomain value);

	/**
	 * Returns the value of the '<em><b>Annotation Property Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Property Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Property Range</em>' containment reference.
	 * @see #setAnnotationPropertyRange(AnnotationPropertyRange)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_AnnotationPropertyRange()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AnnotationPropertyRange' namespace='##targetNamespace'"
	 * @generated
	 */
	AnnotationPropertyRange getAnnotationPropertyRange();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getAnnotationPropertyRange <em>Annotation Property Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Property Range</em>' containment reference.
	 * @see #getAnnotationPropertyRange()
	 * @generated
	 */
	void setAnnotationPropertyRange(AnnotationPropertyRange value);

	/**
	 * Returns the value of the '<em><b>Anonymous Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Individual</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Individual</em>' containment reference.
	 * @see #setAnonymousIndividual(AnonymousIndividual)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_AnonymousIndividual()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AnonymousIndividual' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousIndividual getAnonymousIndividual();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getAnonymousIndividual <em>Anonymous Individual</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Individual</em>' containment reference.
	 * @see #getAnonymousIndividual()
	 * @generated
	 */
	void setAnonymousIndividual(AnonymousIndividual value);

	/**
	 * Returns the value of the '<em><b>Asymmetric Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asymmetric Object Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asymmetric Object Property</em>' containment reference.
	 * @see #setAsymmetricObjectProperty(AsymmetricObjectProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_AsymmetricObjectProperty()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AsymmetricObjectProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	AsymmetricObjectProperty getAsymmetricObjectProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getAsymmetricObjectProperty <em>Asymmetric Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asymmetric Object Property</em>' containment reference.
	 * @see #getAsymmetricObjectProperty()
	 * @generated
	 */
	void setAsymmetricObjectProperty(AsymmetricObjectProperty value);

	/**
	 * Returns the value of the '<em><b>Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' containment reference.
	 * @see #setClass(org.w3._2002._07.owl.Class)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_Class()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Class' namespace='##targetNamespace'"
	 * @generated
	 */
	org.w3._2002._07.owl.Class getClass_();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getClass_ <em>Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' containment reference.
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(org.w3._2002._07.owl.Class value);

	/**
	 * Returns the value of the '<em><b>Class Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Assertion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Assertion</em>' containment reference.
	 * @see #setClassAssertion(ClassAssertion)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ClassAssertion()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ClassAssertion' namespace='##targetNamespace'"
	 * @generated
	 */
	ClassAssertion getClassAssertion();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getClassAssertion <em>Class Assertion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Assertion</em>' containment reference.
	 * @see #getClassAssertion()
	 * @generated
	 */
	void setClassAssertion(ClassAssertion value);

	/**
	 * Returns the value of the '<em><b>Data All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data All Values From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data All Values From</em>' containment reference.
	 * @see #setDataAllValuesFrom(DataAllValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataAllValuesFrom()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataAllValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	DataAllValuesFrom getDataAllValuesFrom();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataAllValuesFrom <em>Data All Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data All Values From</em>' containment reference.
	 * @see #getDataAllValuesFrom()
	 * @generated
	 */
	void setDataAllValuesFrom(DataAllValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Data Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Complement Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Complement Of</em>' containment reference.
	 * @see #setDataComplementOf(DataComplementOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataComplementOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataComplementOf' namespace='##targetNamespace'"
	 * @generated
	 */
	DataComplementOf getDataComplementOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataComplementOf <em>Data Complement Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Complement Of</em>' containment reference.
	 * @see #getDataComplementOf()
	 * @generated
	 */
	void setDataComplementOf(DataComplementOf value);

	/**
	 * Returns the value of the '<em><b>Data Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Exact Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Exact Cardinality</em>' containment reference.
	 * @see #setDataExactCardinality(DataExactCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataExactCardinality()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataExactCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	DataExactCardinality getDataExactCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataExactCardinality <em>Data Exact Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Exact Cardinality</em>' containment reference.
	 * @see #getDataExactCardinality()
	 * @generated
	 */
	void setDataExactCardinality(DataExactCardinality value);

	/**
	 * Returns the value of the '<em><b>Data Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Has Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Has Value</em>' containment reference.
	 * @see #setDataHasValue(DataHasValue)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataHasValue()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataHasValue' namespace='##targetNamespace'"
	 * @generated
	 */
	DataHasValue getDataHasValue();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataHasValue <em>Data Has Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Has Value</em>' containment reference.
	 * @see #getDataHasValue()
	 * @generated
	 */
	void setDataHasValue(DataHasValue value);

	/**
	 * Returns the value of the '<em><b>Data Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Intersection Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Intersection Of</em>' containment reference.
	 * @see #setDataIntersectionOf(DataIntersectionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataIntersectionOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataIntersectionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	DataIntersectionOf getDataIntersectionOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataIntersectionOf <em>Data Intersection Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Intersection Of</em>' containment reference.
	 * @see #getDataIntersectionOf()
	 * @generated
	 */
	void setDataIntersectionOf(DataIntersectionOf value);

	/**
	 * Returns the value of the '<em><b>Data Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Max Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Max Cardinality</em>' containment reference.
	 * @see #setDataMaxCardinality(DataMaxCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataMaxCardinality()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataMaxCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	DataMaxCardinality getDataMaxCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataMaxCardinality <em>Data Max Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Max Cardinality</em>' containment reference.
	 * @see #getDataMaxCardinality()
	 * @generated
	 */
	void setDataMaxCardinality(DataMaxCardinality value);

	/**
	 * Returns the value of the '<em><b>Data Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Min Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Min Cardinality</em>' containment reference.
	 * @see #setDataMinCardinality(DataMinCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataMinCardinality()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataMinCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	DataMinCardinality getDataMinCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataMinCardinality <em>Data Min Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Min Cardinality</em>' containment reference.
	 * @see #getDataMinCardinality()
	 * @generated
	 */
	void setDataMinCardinality(DataMinCardinality value);

	/**
	 * Returns the value of the '<em><b>Data One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data One Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data One Of</em>' containment reference.
	 * @see #setDataOneOf(DataOneOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataOneOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataOneOf' namespace='##targetNamespace'"
	 * @generated
	 */
	DataOneOf getDataOneOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataOneOf <em>Data One Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data One Of</em>' containment reference.
	 * @see #getDataOneOf()
	 * @generated
	 */
	void setDataOneOf(DataOneOf value);

	/**
	 * Returns the value of the '<em><b>Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property</em>' containment reference.
	 * @see #setDataProperty(DataProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataProperty()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	DataProperty getDataProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataProperty <em>Data Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Property</em>' containment reference.
	 * @see #getDataProperty()
	 * @generated
	 */
	void setDataProperty(DataProperty value);

	/**
	 * Returns the value of the '<em><b>Data Property Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property Assertion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property Assertion</em>' containment reference.
	 * @see #setDataPropertyAssertion(DataPropertyAssertion)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataPropertyAssertion()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataPropertyAssertion' namespace='##targetNamespace'"
	 * @generated
	 */
	DataPropertyAssertion getDataPropertyAssertion();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataPropertyAssertion <em>Data Property Assertion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Property Assertion</em>' containment reference.
	 * @see #getDataPropertyAssertion()
	 * @generated
	 */
	void setDataPropertyAssertion(DataPropertyAssertion value);

	/**
	 * Returns the value of the '<em><b>Data Property Domain</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property Domain</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property Domain</em>' containment reference.
	 * @see #setDataPropertyDomain(DataPropertyDomain)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataPropertyDomain()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataPropertyDomain' namespace='##targetNamespace'"
	 * @generated
	 */
	DataPropertyDomain getDataPropertyDomain();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataPropertyDomain <em>Data Property Domain</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Property Domain</em>' containment reference.
	 * @see #getDataPropertyDomain()
	 * @generated
	 */
	void setDataPropertyDomain(DataPropertyDomain value);

	/**
	 * Returns the value of the '<em><b>Data Property Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Property Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Property Range</em>' containment reference.
	 * @see #setDataPropertyRange(DataPropertyRange)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataPropertyRange()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataPropertyRange' namespace='##targetNamespace'"
	 * @generated
	 */
	DataPropertyRange getDataPropertyRange();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataPropertyRange <em>Data Property Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Property Range</em>' containment reference.
	 * @see #getDataPropertyRange()
	 * @generated
	 */
	void setDataPropertyRange(DataPropertyRange value);

	/**
	 * Returns the value of the '<em><b>Data Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Some Values From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Some Values From</em>' containment reference.
	 * @see #setDataSomeValuesFrom(DataSomeValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataSomeValuesFrom()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataSomeValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	DataSomeValuesFrom getDataSomeValuesFrom();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataSomeValuesFrom <em>Data Some Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Some Values From</em>' containment reference.
	 * @see #getDataSomeValuesFrom()
	 * @generated
	 */
	void setDataSomeValuesFrom(DataSomeValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Datatype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype</em>' containment reference.
	 * @see #setDatatype(Datatype)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_Datatype()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Datatype' namespace='##targetNamespace'"
	 * @generated
	 */
	Datatype getDatatype();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDatatype <em>Datatype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype</em>' containment reference.
	 * @see #getDatatype()
	 * @generated
	 */
	void setDatatype(Datatype value);

	/**
	 * Returns the value of the '<em><b>Datatype Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype Definition</em>' containment reference.
	 * @see #setDatatypeDefinition(DatatypeDefinition)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DatatypeDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DatatypeDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	DatatypeDefinition getDatatypeDefinition();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDatatypeDefinition <em>Datatype Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype Definition</em>' containment reference.
	 * @see #getDatatypeDefinition()
	 * @generated
	 */
	void setDatatypeDefinition(DatatypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Datatype Restriction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype Restriction</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype Restriction</em>' containment reference.
	 * @see #setDatatypeRestriction(DatatypeRestriction)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DatatypeRestriction()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DatatypeRestriction' namespace='##targetNamespace'"
	 * @generated
	 */
	DatatypeRestriction getDatatypeRestriction();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDatatypeRestriction <em>Datatype Restriction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype Restriction</em>' containment reference.
	 * @see #getDatatypeRestriction()
	 * @generated
	 */
	void setDatatypeRestriction(DatatypeRestriction value);

	/**
	 * Returns the value of the '<em><b>Data Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Union Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Union Of</em>' containment reference.
	 * @see #setDataUnionOf(DataUnionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DataUnionOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DataUnionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	DataUnionOf getDataUnionOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDataUnionOf <em>Data Union Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Union Of</em>' containment reference.
	 * @see #getDataUnionOf()
	 * @generated
	 */
	void setDataUnionOf(DataUnionOf value);

	/**
	 * Returns the value of the '<em><b>Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Declaration</em>' containment reference.
	 * @see #setDeclaration(Declaration)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_Declaration()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	Declaration getDeclaration();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDeclaration <em>Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Declaration</em>' containment reference.
	 * @see #getDeclaration()
	 * @generated
	 */
	void setDeclaration(Declaration value);

	/**
	 * Returns the value of the '<em><b>Different Individuals</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Different Individuals</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Different Individuals</em>' containment reference.
	 * @see #setDifferentIndividuals(DifferentIndividuals)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DifferentIndividuals()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DifferentIndividuals' namespace='##targetNamespace'"
	 * @generated
	 */
	DifferentIndividuals getDifferentIndividuals();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDifferentIndividuals <em>Different Individuals</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Different Individuals</em>' containment reference.
	 * @see #getDifferentIndividuals()
	 * @generated
	 */
	void setDifferentIndividuals(DifferentIndividuals value);

	/**
	 * Returns the value of the '<em><b>Disjoint Classes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disjoint Classes</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disjoint Classes</em>' containment reference.
	 * @see #setDisjointClasses(DisjointClasses)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DisjointClasses()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DisjointClasses' namespace='##targetNamespace'"
	 * @generated
	 */
	DisjointClasses getDisjointClasses();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDisjointClasses <em>Disjoint Classes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disjoint Classes</em>' containment reference.
	 * @see #getDisjointClasses()
	 * @generated
	 */
	void setDisjointClasses(DisjointClasses value);

	/**
	 * Returns the value of the '<em><b>Disjoint Data Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disjoint Data Properties</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disjoint Data Properties</em>' containment reference.
	 * @see #setDisjointDataProperties(DisjointDataProperties)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DisjointDataProperties()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DisjointDataProperties' namespace='##targetNamespace'"
	 * @generated
	 */
	DisjointDataProperties getDisjointDataProperties();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDisjointDataProperties <em>Disjoint Data Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disjoint Data Properties</em>' containment reference.
	 * @see #getDisjointDataProperties()
	 * @generated
	 */
	void setDisjointDataProperties(DisjointDataProperties value);

	/**
	 * Returns the value of the '<em><b>Disjoint Object Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disjoint Object Properties</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disjoint Object Properties</em>' containment reference.
	 * @see #setDisjointObjectProperties(DisjointObjectProperties)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DisjointObjectProperties()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DisjointObjectProperties' namespace='##targetNamespace'"
	 * @generated
	 */
	DisjointObjectProperties getDisjointObjectProperties();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDisjointObjectProperties <em>Disjoint Object Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disjoint Object Properties</em>' containment reference.
	 * @see #getDisjointObjectProperties()
	 * @generated
	 */
	void setDisjointObjectProperties(DisjointObjectProperties value);

	/**
	 * Returns the value of the '<em><b>Disjoint Union</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disjoint Union</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disjoint Union</em>' containment reference.
	 * @see #setDisjointUnion(DisjointUnion)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_DisjointUnion()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DisjointUnion' namespace='##targetNamespace'"
	 * @generated
	 */
	DisjointUnion getDisjointUnion();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getDisjointUnion <em>Disjoint Union</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disjoint Union</em>' containment reference.
	 * @see #getDisjointUnion()
	 * @generated
	 */
	void setDisjointUnion(DisjointUnion value);

	/**
	 * Returns the value of the '<em><b>Equivalent Classes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equivalent Classes</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equivalent Classes</em>' containment reference.
	 * @see #setEquivalentClasses(EquivalentClasses)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_EquivalentClasses()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='EquivalentClasses' namespace='##targetNamespace'"
	 * @generated
	 */
	EquivalentClasses getEquivalentClasses();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getEquivalentClasses <em>Equivalent Classes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equivalent Classes</em>' containment reference.
	 * @see #getEquivalentClasses()
	 * @generated
	 */
	void setEquivalentClasses(EquivalentClasses value);

	/**
	 * Returns the value of the '<em><b>Equivalent Data Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equivalent Data Properties</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equivalent Data Properties</em>' containment reference.
	 * @see #setEquivalentDataProperties(EquivalentDataProperties)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_EquivalentDataProperties()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='EquivalentDataProperties' namespace='##targetNamespace'"
	 * @generated
	 */
	EquivalentDataProperties getEquivalentDataProperties();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getEquivalentDataProperties <em>Equivalent Data Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equivalent Data Properties</em>' containment reference.
	 * @see #getEquivalentDataProperties()
	 * @generated
	 */
	void setEquivalentDataProperties(EquivalentDataProperties value);

	/**
	 * Returns the value of the '<em><b>Equivalent Object Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equivalent Object Properties</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equivalent Object Properties</em>' containment reference.
	 * @see #setEquivalentObjectProperties(EquivalentObjectProperties)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_EquivalentObjectProperties()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='EquivalentObjectProperties' namespace='##targetNamespace'"
	 * @generated
	 */
	EquivalentObjectProperties getEquivalentObjectProperties();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getEquivalentObjectProperties <em>Equivalent Object Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equivalent Object Properties</em>' containment reference.
	 * @see #getEquivalentObjectProperties()
	 * @generated
	 */
	void setEquivalentObjectProperties(EquivalentObjectProperties value);

	/**
	 * Returns the value of the '<em><b>Functional Data Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functional Data Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functional Data Property</em>' containment reference.
	 * @see #setFunctionalDataProperty(FunctionalDataProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_FunctionalDataProperty()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='FunctionalDataProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionalDataProperty getFunctionalDataProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getFunctionalDataProperty <em>Functional Data Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Functional Data Property</em>' containment reference.
	 * @see #getFunctionalDataProperty()
	 * @generated
	 */
	void setFunctionalDataProperty(FunctionalDataProperty value);

	/**
	 * Returns the value of the '<em><b>Functional Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functional Object Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functional Object Property</em>' containment reference.
	 * @see #setFunctionalObjectProperty(FunctionalObjectProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_FunctionalObjectProperty()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='FunctionalObjectProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionalObjectProperty getFunctionalObjectProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getFunctionalObjectProperty <em>Functional Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Functional Object Property</em>' containment reference.
	 * @see #getFunctionalObjectProperty()
	 * @generated
	 */
	void setFunctionalObjectProperty(FunctionalObjectProperty value);

	/**
	 * Returns the value of the '<em><b>Has Key</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Key</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Key</em>' containment reference.
	 * @see #setHasKey(HasKey)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_HasKey()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='HasKey' namespace='##targetNamespace'"
	 * @generated
	 */
	HasKey getHasKey();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getHasKey <em>Has Key</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Key</em>' containment reference.
	 * @see #getHasKey()
	 * @generated
	 */
	void setHasKey(HasKey value);

	/**
	 * Returns the value of the '<em><b>Import</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import</em>' containment reference.
	 * @see #setImport(Import)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_Import()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Import' namespace='##targetNamespace'"
	 * @generated
	 */
	Import getImport();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getImport <em>Import</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Import</em>' containment reference.
	 * @see #getImport()
	 * @generated
	 */
	void setImport(Import value);

	/**
	 * Returns the value of the '<em><b>Inverse Functional Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inverse Functional Object Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inverse Functional Object Property</em>' containment reference.
	 * @see #setInverseFunctionalObjectProperty(InverseFunctionalObjectProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_InverseFunctionalObjectProperty()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='InverseFunctionalObjectProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	InverseFunctionalObjectProperty getInverseFunctionalObjectProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getInverseFunctionalObjectProperty <em>Inverse Functional Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inverse Functional Object Property</em>' containment reference.
	 * @see #getInverseFunctionalObjectProperty()
	 * @generated
	 */
	void setInverseFunctionalObjectProperty(InverseFunctionalObjectProperty value);

	/**
	 * Returns the value of the '<em><b>Inverse Object Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inverse Object Properties</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inverse Object Properties</em>' containment reference.
	 * @see #setInverseObjectProperties(InverseObjectProperties)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_InverseObjectProperties()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='InverseObjectProperties' namespace='##targetNamespace'"
	 * @generated
	 */
	InverseObjectProperties getInverseObjectProperties();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getInverseObjectProperties <em>Inverse Object Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inverse Object Properties</em>' containment reference.
	 * @see #getInverseObjectProperties()
	 * @generated
	 */
	void setInverseObjectProperties(InverseObjectProperties value);

	/**
	 * Returns the value of the '<em><b>IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>IRI</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IRI</em>' containment reference.
	 * @see #setIRI(IRI)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_IRI()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='IRI' namespace='##targetNamespace'"
	 * @generated
	 */
	IRI getIRI();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getIRI <em>IRI</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IRI</em>' containment reference.
	 * @see #getIRI()
	 * @generated
	 */
	void setIRI(IRI value);

	/**
	 * Returns the value of the '<em><b>Irreflexive Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Irreflexive Object Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Irreflexive Object Property</em>' containment reference.
	 * @see #setIrreflexiveObjectProperty(IrreflexiveObjectProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_IrreflexiveObjectProperty()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='IrreflexiveObjectProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	IrreflexiveObjectProperty getIrreflexiveObjectProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getIrreflexiveObjectProperty <em>Irreflexive Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Irreflexive Object Property</em>' containment reference.
	 * @see #getIrreflexiveObjectProperty()
	 * @generated
	 */
	void setIrreflexiveObjectProperty(IrreflexiveObjectProperty value);

	/**
	 * Returns the value of the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal</em>' containment reference.
	 * @see #setLiteral(Literal)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_Literal()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Literal' namespace='##targetNamespace'"
	 * @generated
	 */
	Literal getLiteral();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getLiteral <em>Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Literal</em>' containment reference.
	 * @see #getLiteral()
	 * @generated
	 */
	void setLiteral(Literal value);

	/**
	 * Returns the value of the '<em><b>Named Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Individual</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Individual</em>' containment reference.
	 * @see #setNamedIndividual(NamedIndividual)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_NamedIndividual()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='NamedIndividual' namespace='##targetNamespace'"
	 * @generated
	 */
	NamedIndividual getNamedIndividual();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getNamedIndividual <em>Named Individual</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Named Individual</em>' containment reference.
	 * @see #getNamedIndividual()
	 * @generated
	 */
	void setNamedIndividual(NamedIndividual value);

	/**
	 * Returns the value of the '<em><b>Negative Data Property Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Negative Data Property Assertion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Negative Data Property Assertion</em>' containment reference.
	 * @see #setNegativeDataPropertyAssertion(NegativeDataPropertyAssertion)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_NegativeDataPropertyAssertion()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='NegativeDataPropertyAssertion' namespace='##targetNamespace'"
	 * @generated
	 */
	NegativeDataPropertyAssertion getNegativeDataPropertyAssertion();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getNegativeDataPropertyAssertion <em>Negative Data Property Assertion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Negative Data Property Assertion</em>' containment reference.
	 * @see #getNegativeDataPropertyAssertion()
	 * @generated
	 */
	void setNegativeDataPropertyAssertion(NegativeDataPropertyAssertion value);

	/**
	 * Returns the value of the '<em><b>Negative Object Property Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Negative Object Property Assertion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Negative Object Property Assertion</em>' containment reference.
	 * @see #setNegativeObjectPropertyAssertion(NegativeObjectPropertyAssertion)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_NegativeObjectPropertyAssertion()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='NegativeObjectPropertyAssertion' namespace='##targetNamespace'"
	 * @generated
	 */
	NegativeObjectPropertyAssertion getNegativeObjectPropertyAssertion();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getNegativeObjectPropertyAssertion <em>Negative Object Property Assertion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Negative Object Property Assertion</em>' containment reference.
	 * @see #getNegativeObjectPropertyAssertion()
	 * @generated
	 */
	void setNegativeObjectPropertyAssertion(NegativeObjectPropertyAssertion value);

	/**
	 * Returns the value of the '<em><b>Object All Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object All Values From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object All Values From</em>' containment reference.
	 * @see #setObjectAllValuesFrom(ObjectAllValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectAllValuesFrom()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectAllValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectAllValuesFrom getObjectAllValuesFrom();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectAllValuesFrom <em>Object All Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object All Values From</em>' containment reference.
	 * @see #getObjectAllValuesFrom()
	 * @generated
	 */
	void setObjectAllValuesFrom(ObjectAllValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Object Complement Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Complement Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Complement Of</em>' containment reference.
	 * @see #setObjectComplementOf(ObjectComplementOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectComplementOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectComplementOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectComplementOf getObjectComplementOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectComplementOf <em>Object Complement Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Complement Of</em>' containment reference.
	 * @see #getObjectComplementOf()
	 * @generated
	 */
	void setObjectComplementOf(ObjectComplementOf value);

	/**
	 * Returns the value of the '<em><b>Object Exact Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Exact Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Exact Cardinality</em>' containment reference.
	 * @see #setObjectExactCardinality(ObjectExactCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectExactCardinality()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectExactCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectExactCardinality getObjectExactCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectExactCardinality <em>Object Exact Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Exact Cardinality</em>' containment reference.
	 * @see #getObjectExactCardinality()
	 * @generated
	 */
	void setObjectExactCardinality(ObjectExactCardinality value);

	/**
	 * Returns the value of the '<em><b>Object Has Self</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Has Self</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Has Self</em>' containment reference.
	 * @see #setObjectHasSelf(ObjectHasSelf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectHasSelf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectHasSelf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectHasSelf getObjectHasSelf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectHasSelf <em>Object Has Self</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Has Self</em>' containment reference.
	 * @see #getObjectHasSelf()
	 * @generated
	 */
	void setObjectHasSelf(ObjectHasSelf value);

	/**
	 * Returns the value of the '<em><b>Object Has Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Has Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Has Value</em>' containment reference.
	 * @see #setObjectHasValue(ObjectHasValue)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectHasValue()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectHasValue' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectHasValue getObjectHasValue();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectHasValue <em>Object Has Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Has Value</em>' containment reference.
	 * @see #getObjectHasValue()
	 * @generated
	 */
	void setObjectHasValue(ObjectHasValue value);

	/**
	 * Returns the value of the '<em><b>Object Intersection Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Intersection Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Intersection Of</em>' containment reference.
	 * @see #setObjectIntersectionOf(ObjectIntersectionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectIntersectionOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectIntersectionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectIntersectionOf getObjectIntersectionOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectIntersectionOf <em>Object Intersection Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Intersection Of</em>' containment reference.
	 * @see #getObjectIntersectionOf()
	 * @generated
	 */
	void setObjectIntersectionOf(ObjectIntersectionOf value);

	/**
	 * Returns the value of the '<em><b>Object Inverse Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Inverse Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Inverse Of</em>' containment reference.
	 * @see #setObjectInverseOf(ObjectInverseOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectInverseOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectInverseOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectInverseOf getObjectInverseOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectInverseOf <em>Object Inverse Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Inverse Of</em>' containment reference.
	 * @see #getObjectInverseOf()
	 * @generated
	 */
	void setObjectInverseOf(ObjectInverseOf value);

	/**
	 * Returns the value of the '<em><b>Object Max Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Max Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Max Cardinality</em>' containment reference.
	 * @see #setObjectMaxCardinality(ObjectMaxCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectMaxCardinality()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectMaxCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectMaxCardinality getObjectMaxCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectMaxCardinality <em>Object Max Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Max Cardinality</em>' containment reference.
	 * @see #getObjectMaxCardinality()
	 * @generated
	 */
	void setObjectMaxCardinality(ObjectMaxCardinality value);

	/**
	 * Returns the value of the '<em><b>Object Min Cardinality</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Min Cardinality</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Min Cardinality</em>' containment reference.
	 * @see #setObjectMinCardinality(ObjectMinCardinality)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectMinCardinality()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectMinCardinality' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectMinCardinality getObjectMinCardinality();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectMinCardinality <em>Object Min Cardinality</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Min Cardinality</em>' containment reference.
	 * @see #getObjectMinCardinality()
	 * @generated
	 */
	void setObjectMinCardinality(ObjectMinCardinality value);

	/**
	 * Returns the value of the '<em><b>Object One Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object One Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object One Of</em>' containment reference.
	 * @see #setObjectOneOf(ObjectOneOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectOneOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectOneOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectOneOf getObjectOneOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectOneOf <em>Object One Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object One Of</em>' containment reference.
	 * @see #getObjectOneOf()
	 * @generated
	 */
	void setObjectOneOf(ObjectOneOf value);

	/**
	 * Returns the value of the '<em><b>Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property</em>' containment reference.
	 * @see #setObjectProperty(ObjectProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectProperty()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectProperty getObjectProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectProperty <em>Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Property</em>' containment reference.
	 * @see #getObjectProperty()
	 * @generated
	 */
	void setObjectProperty(ObjectProperty value);

	/**
	 * Returns the value of the '<em><b>Object Property Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property Assertion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property Assertion</em>' containment reference.
	 * @see #setObjectPropertyAssertion(ObjectPropertyAssertion)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectPropertyAssertion()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectPropertyAssertion' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectPropertyAssertion getObjectPropertyAssertion();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectPropertyAssertion <em>Object Property Assertion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Property Assertion</em>' containment reference.
	 * @see #getObjectPropertyAssertion()
	 * @generated
	 */
	void setObjectPropertyAssertion(ObjectPropertyAssertion value);

	/**
	 * Returns the value of the '<em><b>Object Property Domain</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property Domain</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property Domain</em>' containment reference.
	 * @see #setObjectPropertyDomain(ObjectPropertyDomain)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectPropertyDomain()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectPropertyDomain' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectPropertyDomain getObjectPropertyDomain();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectPropertyDomain <em>Object Property Domain</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Property Domain</em>' containment reference.
	 * @see #getObjectPropertyDomain()
	 * @generated
	 */
	void setObjectPropertyDomain(ObjectPropertyDomain value);

	/**
	 * Returns the value of the '<em><b>Object Property Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Property Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Property Range</em>' containment reference.
	 * @see #setObjectPropertyRange(ObjectPropertyRange)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectPropertyRange()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectPropertyRange' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectPropertyRange getObjectPropertyRange();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectPropertyRange <em>Object Property Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Property Range</em>' containment reference.
	 * @see #getObjectPropertyRange()
	 * @generated
	 */
	void setObjectPropertyRange(ObjectPropertyRange value);

	/**
	 * Returns the value of the '<em><b>Object Some Values From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Some Values From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Some Values From</em>' containment reference.
	 * @see #setObjectSomeValuesFrom(ObjectSomeValuesFrom)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectSomeValuesFrom()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectSomeValuesFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectSomeValuesFrom getObjectSomeValuesFrom();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectSomeValuesFrom <em>Object Some Values From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Some Values From</em>' containment reference.
	 * @see #getObjectSomeValuesFrom()
	 * @generated
	 */
	void setObjectSomeValuesFrom(ObjectSomeValuesFrom value);

	/**
	 * Returns the value of the '<em><b>Object Union Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Union Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Union Of</em>' containment reference.
	 * @see #setObjectUnionOf(ObjectUnionOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ObjectUnionOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ObjectUnionOf' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectUnionOf getObjectUnionOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getObjectUnionOf <em>Object Union Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Union Of</em>' containment reference.
	 * @see #getObjectUnionOf()
	 * @generated
	 */
	void setObjectUnionOf(ObjectUnionOf value);

	/**
	 * Returns the value of the '<em><b>Ontology</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ontology</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ontology</em>' containment reference.
	 * @see #setOntology(Ontology)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_Ontology()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Ontology' namespace='##targetNamespace'"
	 * @generated
	 */
	Ontology getOntology();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getOntology <em>Ontology</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ontology</em>' containment reference.
	 * @see #getOntology()
	 * @generated
	 */
	void setOntology(Ontology value);

	/**
	 * Returns the value of the '<em><b>Prefix</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prefix</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prefix</em>' containment reference.
	 * @see #setPrefix(Prefix)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_Prefix()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Prefix' namespace='##targetNamespace'"
	 * @generated
	 */
	Prefix getPrefix();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getPrefix <em>Prefix</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prefix</em>' containment reference.
	 * @see #getPrefix()
	 * @generated
	 */
	void setPrefix(Prefix value);

	/**
	 * Returns the value of the '<em><b>Reflexive Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reflexive Object Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reflexive Object Property</em>' containment reference.
	 * @see #setReflexiveObjectProperty(ReflexiveObjectProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_ReflexiveObjectProperty()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ReflexiveObjectProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	ReflexiveObjectProperty getReflexiveObjectProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getReflexiveObjectProperty <em>Reflexive Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reflexive Object Property</em>' containment reference.
	 * @see #getReflexiveObjectProperty()
	 * @generated
	 */
	void setReflexiveObjectProperty(ReflexiveObjectProperty value);

	/**
	 * Returns the value of the '<em><b>Same Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Same Individual</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Same Individual</em>' containment reference.
	 * @see #setSameIndividual(SameIndividual)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_SameIndividual()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SameIndividual' namespace='##targetNamespace'"
	 * @generated
	 */
	SameIndividual getSameIndividual();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getSameIndividual <em>Same Individual</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Same Individual</em>' containment reference.
	 * @see #getSameIndividual()
	 * @generated
	 */
	void setSameIndividual(SameIndividual value);

	/**
	 * Returns the value of the '<em><b>Sub Annotation Property Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Annotation Property Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Annotation Property Of</em>' containment reference.
	 * @see #setSubAnnotationPropertyOf(SubAnnotationPropertyOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_SubAnnotationPropertyOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SubAnnotationPropertyOf' namespace='##targetNamespace'"
	 * @generated
	 */
	SubAnnotationPropertyOf getSubAnnotationPropertyOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getSubAnnotationPropertyOf <em>Sub Annotation Property Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub Annotation Property Of</em>' containment reference.
	 * @see #getSubAnnotationPropertyOf()
	 * @generated
	 */
	void setSubAnnotationPropertyOf(SubAnnotationPropertyOf value);

	/**
	 * Returns the value of the '<em><b>Sub Class Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Class Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Class Of</em>' containment reference.
	 * @see #setSubClassOf(SubClassOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_SubClassOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SubClassOf' namespace='##targetNamespace'"
	 * @generated
	 */
	SubClassOf getSubClassOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getSubClassOf <em>Sub Class Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub Class Of</em>' containment reference.
	 * @see #getSubClassOf()
	 * @generated
	 */
	void setSubClassOf(SubClassOf value);

	/**
	 * Returns the value of the '<em><b>Sub Data Property Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Data Property Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Data Property Of</em>' containment reference.
	 * @see #setSubDataPropertyOf(SubDataPropertyOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_SubDataPropertyOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SubDataPropertyOf' namespace='##targetNamespace'"
	 * @generated
	 */
	SubDataPropertyOf getSubDataPropertyOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getSubDataPropertyOf <em>Sub Data Property Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub Data Property Of</em>' containment reference.
	 * @see #getSubDataPropertyOf()
	 * @generated
	 */
	void setSubDataPropertyOf(SubDataPropertyOf value);

	/**
	 * Returns the value of the '<em><b>Sub Object Property Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Object Property Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Object Property Of</em>' containment reference.
	 * @see #setSubObjectPropertyOf(SubObjectPropertyOf)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_SubObjectPropertyOf()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SubObjectPropertyOf' namespace='##targetNamespace'"
	 * @generated
	 */
	SubObjectPropertyOf getSubObjectPropertyOf();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getSubObjectPropertyOf <em>Sub Object Property Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub Object Property Of</em>' containment reference.
	 * @see #getSubObjectPropertyOf()
	 * @generated
	 */
	void setSubObjectPropertyOf(SubObjectPropertyOf value);

	/**
	 * Returns the value of the '<em><b>Symmetric Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symmetric Object Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symmetric Object Property</em>' containment reference.
	 * @see #setSymmetricObjectProperty(SymmetricObjectProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_SymmetricObjectProperty()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SymmetricObjectProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	SymmetricObjectProperty getSymmetricObjectProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getSymmetricObjectProperty <em>Symmetric Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symmetric Object Property</em>' containment reference.
	 * @see #getSymmetricObjectProperty()
	 * @generated
	 */
	void setSymmetricObjectProperty(SymmetricObjectProperty value);

	/**
	 * Returns the value of the '<em><b>Transitive Object Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitive Object Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitive Object Property</em>' containment reference.
	 * @see #setTransitiveObjectProperty(TransitiveObjectProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getDocumentRoot_TransitiveObjectProperty()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='TransitiveObjectProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	TransitiveObjectProperty getTransitiveObjectProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.DocumentRoot#getTransitiveObjectProperty <em>Transitive Object Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transitive Object Property</em>' containment reference.
	 * @see #getTransitiveObjectProperty()
	 * @generated
	 */
	void setTransitiveObjectProperty(TransitiveObjectProperty value);

} // DocumentRoot
