/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotation Assertion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.AnnotationAssertion#getAnnotationProperty <em>Annotation Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.AnnotationAssertion#getIRI <em>IRI</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.AnnotationAssertion#getAbbreviatedIRI <em>Abbreviated IRI</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.AnnotationAssertion#getAnonymousIndividual <em>Anonymous Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.AnnotationAssertion#getIRI1 <em>IRI1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.AnnotationAssertion#getAbbreviatedIRI1 <em>Abbreviated IRI1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.AnnotationAssertion#getAnonymousIndividual1 <em>Anonymous Individual1</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.AnnotationAssertion#getLiteral <em>Literal</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getAnnotationAssertion()
 * @model extendedMetaData="name='AnnotationAssertion' kind='elementOnly'"
 * @generated
 */
public interface AnnotationAssertion extends AnnotationAxiom {
	/**
	 * Returns the value of the '<em><b>Annotation Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Property</em>' containment reference.
	 * @see #setAnnotationProperty(AnnotationProperty)
	 * @see org.w3._2002._07.owl.OwlPackage#getAnnotationAssertion_AnnotationProperty()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='AnnotationProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	AnnotationProperty getAnnotationProperty();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.AnnotationAssertion#getAnnotationProperty <em>Annotation Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Property</em>' containment reference.
	 * @see #getAnnotationProperty()
	 * @generated
	 */
	void setAnnotationProperty(AnnotationProperty value);

	/**
	 * Returns the value of the '<em><b>IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>IRI</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IRI</em>' containment reference.
	 * @see #setIRI(IRI)
	 * @see org.w3._2002._07.owl.OwlPackage#getAnnotationAssertion_IRI()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='IRI' namespace='##targetNamespace'"
	 * @generated
	 */
	IRI getIRI();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.AnnotationAssertion#getIRI <em>IRI</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IRI</em>' containment reference.
	 * @see #getIRI()
	 * @generated
	 */
	void setIRI(IRI value);

	/**
	 * Returns the value of the '<em><b>Abbreviated IRI</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abbreviated IRI</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abbreviated IRI</em>' containment reference.
	 * @see #setAbbreviatedIRI(AbbreviatedIRI1)
	 * @see org.w3._2002._07.owl.OwlPackage#getAnnotationAssertion_AbbreviatedIRI()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AbbreviatedIRI' namespace='##targetNamespace'"
	 * @generated
	 */
	AbbreviatedIRI1 getAbbreviatedIRI();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.AnnotationAssertion#getAbbreviatedIRI <em>Abbreviated IRI</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abbreviated IRI</em>' containment reference.
	 * @see #getAbbreviatedIRI()
	 * @generated
	 */
	void setAbbreviatedIRI(AbbreviatedIRI1 value);

	/**
	 * Returns the value of the '<em><b>Anonymous Individual</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Individual</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Individual</em>' containment reference.
	 * @see #setAnonymousIndividual(AnonymousIndividual)
	 * @see org.w3._2002._07.owl.OwlPackage#getAnnotationAssertion_AnonymousIndividual()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AnonymousIndividual' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousIndividual getAnonymousIndividual();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.AnnotationAssertion#getAnonymousIndividual <em>Anonymous Individual</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Individual</em>' containment reference.
	 * @see #getAnonymousIndividual()
	 * @generated
	 */
	void setAnonymousIndividual(AnonymousIndividual value);

	/**
	 * Returns the value of the '<em><b>IRI1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>IRI1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IRI1</em>' containment reference.
	 * @see #setIRI1(IRI)
	 * @see org.w3._2002._07.owl.OwlPackage#getAnnotationAssertion_IRI1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='IRI' namespace='##targetNamespace'"
	 * @generated
	 */
	IRI getIRI1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.AnnotationAssertion#getIRI1 <em>IRI1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IRI1</em>' containment reference.
	 * @see #getIRI1()
	 * @generated
	 */
	void setIRI1(IRI value);

	/**
	 * Returns the value of the '<em><b>Abbreviated IRI1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abbreviated IRI1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abbreviated IRI1</em>' containment reference.
	 * @see #setAbbreviatedIRI1(AbbreviatedIRI1)
	 * @see org.w3._2002._07.owl.OwlPackage#getAnnotationAssertion_AbbreviatedIRI1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AbbreviatedIRI' namespace='##targetNamespace'"
	 * @generated
	 */
	AbbreviatedIRI1 getAbbreviatedIRI1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.AnnotationAssertion#getAbbreviatedIRI1 <em>Abbreviated IRI1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abbreviated IRI1</em>' containment reference.
	 * @see #getAbbreviatedIRI1()
	 * @generated
	 */
	void setAbbreviatedIRI1(AbbreviatedIRI1 value);

	/**
	 * Returns the value of the '<em><b>Anonymous Individual1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Individual1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Individual1</em>' containment reference.
	 * @see #setAnonymousIndividual1(AnonymousIndividual)
	 * @see org.w3._2002._07.owl.OwlPackage#getAnnotationAssertion_AnonymousIndividual1()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AnonymousIndividual' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousIndividual getAnonymousIndividual1();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.AnnotationAssertion#getAnonymousIndividual1 <em>Anonymous Individual1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Individual1</em>' containment reference.
	 * @see #getAnonymousIndividual1()
	 * @generated
	 */
	void setAnonymousIndividual1(AnonymousIndividual value);

	/**
	 * Returns the value of the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal</em>' containment reference.
	 * @see #setLiteral(Literal)
	 * @see org.w3._2002._07.owl.OwlPackage#getAnnotationAssertion_Literal()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Literal' namespace='##targetNamespace'"
	 * @generated
	 */
	Literal getLiteral();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.AnnotationAssertion#getLiteral <em>Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Literal</em>' containment reference.
	 * @see #getLiteral()
	 * @generated
	 */
	void setLiteral(Literal value);

} // AnnotationAssertion
