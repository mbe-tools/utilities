/**
 */
package org.w3._2002._07.owl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.w3._2002._07.owl.DataAllValuesFrom;
import org.w3._2002._07.owl.DataExactCardinality;
import org.w3._2002._07.owl.DataHasValue;
import org.w3._2002._07.owl.DataMaxCardinality;
import org.w3._2002._07.owl.DataMinCardinality;
import org.w3._2002._07.owl.DataSomeValuesFrom;
import org.w3._2002._07.owl.DisjointClasses;
import org.w3._2002._07.owl.ObjectAllValuesFrom;
import org.w3._2002._07.owl.ObjectComplementOf;
import org.w3._2002._07.owl.ObjectExactCardinality;
import org.w3._2002._07.owl.ObjectHasSelf;
import org.w3._2002._07.owl.ObjectHasValue;
import org.w3._2002._07.owl.ObjectIntersectionOf;
import org.w3._2002._07.owl.ObjectMaxCardinality;
import org.w3._2002._07.owl.ObjectMinCardinality;
import org.w3._2002._07.owl.ObjectOneOf;
import org.w3._2002._07.owl.ObjectSomeValuesFrom;
import org.w3._2002._07.owl.ObjectUnionOf;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Disjoint Classes</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getClassExpression <em>Class Expression</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getObjectIntersectionOf <em>Object Intersection Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getObjectUnionOf <em>Object Union Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getObjectComplementOf <em>Object Complement Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getObjectOneOf <em>Object One Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getObjectSomeValuesFrom <em>Object Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getObjectAllValuesFrom <em>Object All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getObjectHasValue <em>Object Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getObjectHasSelf <em>Object Has Self</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getObjectMinCardinality <em>Object Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getObjectMaxCardinality <em>Object Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getObjectExactCardinality <em>Object Exact Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getDataSomeValuesFrom <em>Data Some Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getDataAllValuesFrom <em>Data All Values From</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getDataHasValue <em>Data Has Value</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getDataMinCardinality <em>Data Min Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getDataMaxCardinality <em>Data Max Cardinality</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.DisjointClassesImpl#getDataExactCardinality <em>Data Exact Cardinality</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DisjointClassesImpl extends ClassAxiomImpl implements DisjointClasses {
	/**
	 * The cached value of the '{@link #getClassExpression() <em>Class Expression</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassExpression()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap classExpression;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DisjointClassesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getDisjointClasses();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getClassExpression() {
		if (classExpression == null) {
			classExpression = new BasicFeatureMap(this, OwlPackage.DISJOINT_CLASSES__CLASS_EXPRESSION);
		}
		return classExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<org.w3._2002._07.owl.Class> getClass_() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_Class());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectIntersectionOf> getObjectIntersectionOf() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_ObjectIntersectionOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectUnionOf> getObjectUnionOf() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_ObjectUnionOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectComplementOf> getObjectComplementOf() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_ObjectComplementOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectOneOf> getObjectOneOf() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_ObjectOneOf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectSomeValuesFrom> getObjectSomeValuesFrom() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_ObjectSomeValuesFrom());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectAllValuesFrom> getObjectAllValuesFrom() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_ObjectAllValuesFrom());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectHasValue> getObjectHasValue() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_ObjectHasValue());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectHasSelf> getObjectHasSelf() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_ObjectHasSelf());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectMinCardinality> getObjectMinCardinality() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_ObjectMinCardinality());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectMaxCardinality> getObjectMaxCardinality() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_ObjectMaxCardinality());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectExactCardinality> getObjectExactCardinality() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_ObjectExactCardinality());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataSomeValuesFrom> getDataSomeValuesFrom() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_DataSomeValuesFrom());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataAllValuesFrom> getDataAllValuesFrom() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_DataAllValuesFrom());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataHasValue> getDataHasValue() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_DataHasValue());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataMinCardinality> getDataMinCardinality() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_DataMinCardinality());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataMaxCardinality> getDataMaxCardinality() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_DataMaxCardinality());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataExactCardinality> getDataExactCardinality() {
		return getClassExpression().list(OwlPackage.eINSTANCE.getDisjointClasses_DataExactCardinality());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.DISJOINT_CLASSES__CLASS_EXPRESSION:
				return ((InternalEList<?>)getClassExpression()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__CLASS:
				return ((InternalEList<?>)getClass_()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__OBJECT_INTERSECTION_OF:
				return ((InternalEList<?>)getObjectIntersectionOf()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__OBJECT_UNION_OF:
				return ((InternalEList<?>)getObjectUnionOf()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__OBJECT_COMPLEMENT_OF:
				return ((InternalEList<?>)getObjectComplementOf()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__OBJECT_ONE_OF:
				return ((InternalEList<?>)getObjectOneOf()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__OBJECT_SOME_VALUES_FROM:
				return ((InternalEList<?>)getObjectSomeValuesFrom()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__OBJECT_ALL_VALUES_FROM:
				return ((InternalEList<?>)getObjectAllValuesFrom()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__OBJECT_HAS_VALUE:
				return ((InternalEList<?>)getObjectHasValue()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__OBJECT_HAS_SELF:
				return ((InternalEList<?>)getObjectHasSelf()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__OBJECT_MIN_CARDINALITY:
				return ((InternalEList<?>)getObjectMinCardinality()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__OBJECT_MAX_CARDINALITY:
				return ((InternalEList<?>)getObjectMaxCardinality()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__OBJECT_EXACT_CARDINALITY:
				return ((InternalEList<?>)getObjectExactCardinality()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__DATA_SOME_VALUES_FROM:
				return ((InternalEList<?>)getDataSomeValuesFrom()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__DATA_ALL_VALUES_FROM:
				return ((InternalEList<?>)getDataAllValuesFrom()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__DATA_HAS_VALUE:
				return ((InternalEList<?>)getDataHasValue()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__DATA_MIN_CARDINALITY:
				return ((InternalEList<?>)getDataMinCardinality()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__DATA_MAX_CARDINALITY:
				return ((InternalEList<?>)getDataMaxCardinality()).basicRemove(otherEnd, msgs);
			case OwlPackage.DISJOINT_CLASSES__DATA_EXACT_CARDINALITY:
				return ((InternalEList<?>)getDataExactCardinality()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.DISJOINT_CLASSES__CLASS_EXPRESSION:
				if (coreType) return getClassExpression();
				return ((FeatureMap.Internal)getClassExpression()).getWrapper();
			case OwlPackage.DISJOINT_CLASSES__CLASS:
				return getClass_();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_INTERSECTION_OF:
				return getObjectIntersectionOf();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_UNION_OF:
				return getObjectUnionOf();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_COMPLEMENT_OF:
				return getObjectComplementOf();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_ONE_OF:
				return getObjectOneOf();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_SOME_VALUES_FROM:
				return getObjectSomeValuesFrom();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_ALL_VALUES_FROM:
				return getObjectAllValuesFrom();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_HAS_VALUE:
				return getObjectHasValue();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_HAS_SELF:
				return getObjectHasSelf();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_MIN_CARDINALITY:
				return getObjectMinCardinality();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_MAX_CARDINALITY:
				return getObjectMaxCardinality();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_EXACT_CARDINALITY:
				return getObjectExactCardinality();
			case OwlPackage.DISJOINT_CLASSES__DATA_SOME_VALUES_FROM:
				return getDataSomeValuesFrom();
			case OwlPackage.DISJOINT_CLASSES__DATA_ALL_VALUES_FROM:
				return getDataAllValuesFrom();
			case OwlPackage.DISJOINT_CLASSES__DATA_HAS_VALUE:
				return getDataHasValue();
			case OwlPackage.DISJOINT_CLASSES__DATA_MIN_CARDINALITY:
				return getDataMinCardinality();
			case OwlPackage.DISJOINT_CLASSES__DATA_MAX_CARDINALITY:
				return getDataMaxCardinality();
			case OwlPackage.DISJOINT_CLASSES__DATA_EXACT_CARDINALITY:
				return getDataExactCardinality();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.DISJOINT_CLASSES__CLASS_EXPRESSION:
				((FeatureMap.Internal)getClassExpression()).set(newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__CLASS:
				getClass_().clear();
				getClass_().addAll((Collection<? extends org.w3._2002._07.owl.Class>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_INTERSECTION_OF:
				getObjectIntersectionOf().clear();
				getObjectIntersectionOf().addAll((Collection<? extends ObjectIntersectionOf>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_UNION_OF:
				getObjectUnionOf().clear();
				getObjectUnionOf().addAll((Collection<? extends ObjectUnionOf>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_COMPLEMENT_OF:
				getObjectComplementOf().clear();
				getObjectComplementOf().addAll((Collection<? extends ObjectComplementOf>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_ONE_OF:
				getObjectOneOf().clear();
				getObjectOneOf().addAll((Collection<? extends ObjectOneOf>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_SOME_VALUES_FROM:
				getObjectSomeValuesFrom().clear();
				getObjectSomeValuesFrom().addAll((Collection<? extends ObjectSomeValuesFrom>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_ALL_VALUES_FROM:
				getObjectAllValuesFrom().clear();
				getObjectAllValuesFrom().addAll((Collection<? extends ObjectAllValuesFrom>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_HAS_VALUE:
				getObjectHasValue().clear();
				getObjectHasValue().addAll((Collection<? extends ObjectHasValue>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_HAS_SELF:
				getObjectHasSelf().clear();
				getObjectHasSelf().addAll((Collection<? extends ObjectHasSelf>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_MIN_CARDINALITY:
				getObjectMinCardinality().clear();
				getObjectMinCardinality().addAll((Collection<? extends ObjectMinCardinality>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_MAX_CARDINALITY:
				getObjectMaxCardinality().clear();
				getObjectMaxCardinality().addAll((Collection<? extends ObjectMaxCardinality>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_EXACT_CARDINALITY:
				getObjectExactCardinality().clear();
				getObjectExactCardinality().addAll((Collection<? extends ObjectExactCardinality>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__DATA_SOME_VALUES_FROM:
				getDataSomeValuesFrom().clear();
				getDataSomeValuesFrom().addAll((Collection<? extends DataSomeValuesFrom>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__DATA_ALL_VALUES_FROM:
				getDataAllValuesFrom().clear();
				getDataAllValuesFrom().addAll((Collection<? extends DataAllValuesFrom>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__DATA_HAS_VALUE:
				getDataHasValue().clear();
				getDataHasValue().addAll((Collection<? extends DataHasValue>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__DATA_MIN_CARDINALITY:
				getDataMinCardinality().clear();
				getDataMinCardinality().addAll((Collection<? extends DataMinCardinality>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__DATA_MAX_CARDINALITY:
				getDataMaxCardinality().clear();
				getDataMaxCardinality().addAll((Collection<? extends DataMaxCardinality>)newValue);
				return;
			case OwlPackage.DISJOINT_CLASSES__DATA_EXACT_CARDINALITY:
				getDataExactCardinality().clear();
				getDataExactCardinality().addAll((Collection<? extends DataExactCardinality>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.DISJOINT_CLASSES__CLASS_EXPRESSION:
				getClassExpression().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__CLASS:
				getClass_().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_INTERSECTION_OF:
				getObjectIntersectionOf().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_UNION_OF:
				getObjectUnionOf().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_COMPLEMENT_OF:
				getObjectComplementOf().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_ONE_OF:
				getObjectOneOf().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_SOME_VALUES_FROM:
				getObjectSomeValuesFrom().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_ALL_VALUES_FROM:
				getObjectAllValuesFrom().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_HAS_VALUE:
				getObjectHasValue().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_HAS_SELF:
				getObjectHasSelf().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_MIN_CARDINALITY:
				getObjectMinCardinality().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_MAX_CARDINALITY:
				getObjectMaxCardinality().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__OBJECT_EXACT_CARDINALITY:
				getObjectExactCardinality().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__DATA_SOME_VALUES_FROM:
				getDataSomeValuesFrom().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__DATA_ALL_VALUES_FROM:
				getDataAllValuesFrom().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__DATA_HAS_VALUE:
				getDataHasValue().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__DATA_MIN_CARDINALITY:
				getDataMinCardinality().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__DATA_MAX_CARDINALITY:
				getDataMaxCardinality().clear();
				return;
			case OwlPackage.DISJOINT_CLASSES__DATA_EXACT_CARDINALITY:
				getDataExactCardinality().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.DISJOINT_CLASSES__CLASS_EXPRESSION:
				return classExpression != null && !classExpression.isEmpty();
			case OwlPackage.DISJOINT_CLASSES__CLASS:
				return !getClass_().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_INTERSECTION_OF:
				return !getObjectIntersectionOf().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_UNION_OF:
				return !getObjectUnionOf().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_COMPLEMENT_OF:
				return !getObjectComplementOf().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_ONE_OF:
				return !getObjectOneOf().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_SOME_VALUES_FROM:
				return !getObjectSomeValuesFrom().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_ALL_VALUES_FROM:
				return !getObjectAllValuesFrom().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_HAS_VALUE:
				return !getObjectHasValue().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_HAS_SELF:
				return !getObjectHasSelf().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_MIN_CARDINALITY:
				return !getObjectMinCardinality().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_MAX_CARDINALITY:
				return !getObjectMaxCardinality().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__OBJECT_EXACT_CARDINALITY:
				return !getObjectExactCardinality().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__DATA_SOME_VALUES_FROM:
				return !getDataSomeValuesFrom().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__DATA_ALL_VALUES_FROM:
				return !getDataAllValuesFrom().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__DATA_HAS_VALUE:
				return !getDataHasValue().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__DATA_MIN_CARDINALITY:
				return !getDataMinCardinality().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__DATA_MAX_CARDINALITY:
				return !getDataMaxCardinality().isEmpty();
			case OwlPackage.DISJOINT_CLASSES__DATA_EXACT_CARDINALITY:
				return !getDataExactCardinality().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (classExpression: ");
		result.append(classExpression);
		result.append(')');
		return result.toString();
	}

} //DisjointClassesImpl
