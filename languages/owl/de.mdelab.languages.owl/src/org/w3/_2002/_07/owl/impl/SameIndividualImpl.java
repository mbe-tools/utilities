/**
 */
package org.w3._2002._07.owl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.w3._2002._07.owl.AnonymousIndividual;
import org.w3._2002._07.owl.NamedIndividual;
import org.w3._2002._07.owl.OwlPackage;
import org.w3._2002._07.owl.SameIndividual;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Same Individual</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.SameIndividualImpl#getIndividual <em>Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SameIndividualImpl#getNamedIndividual <em>Named Individual</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SameIndividualImpl#getAnonymousIndividual <em>Anonymous Individual</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SameIndividualImpl extends AssertionImpl implements SameIndividual {
	/**
	 * The cached value of the '{@link #getIndividual() <em>Individual</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndividual()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap individual;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SameIndividualImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getSameIndividual();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getIndividual() {
		if (individual == null) {
			individual = new BasicFeatureMap(this, OwlPackage.SAME_INDIVIDUAL__INDIVIDUAL);
		}
		return individual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NamedIndividual> getNamedIndividual() {
		return getIndividual().list(OwlPackage.eINSTANCE.getSameIndividual_NamedIndividual());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousIndividual> getAnonymousIndividual() {
		return getIndividual().list(OwlPackage.eINSTANCE.getSameIndividual_AnonymousIndividual());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.SAME_INDIVIDUAL__INDIVIDUAL:
				return ((InternalEList<?>)getIndividual()).basicRemove(otherEnd, msgs);
			case OwlPackage.SAME_INDIVIDUAL__NAMED_INDIVIDUAL:
				return ((InternalEList<?>)getNamedIndividual()).basicRemove(otherEnd, msgs);
			case OwlPackage.SAME_INDIVIDUAL__ANONYMOUS_INDIVIDUAL:
				return ((InternalEList<?>)getAnonymousIndividual()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.SAME_INDIVIDUAL__INDIVIDUAL:
				if (coreType) return getIndividual();
				return ((FeatureMap.Internal)getIndividual()).getWrapper();
			case OwlPackage.SAME_INDIVIDUAL__NAMED_INDIVIDUAL:
				return getNamedIndividual();
			case OwlPackage.SAME_INDIVIDUAL__ANONYMOUS_INDIVIDUAL:
				return getAnonymousIndividual();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.SAME_INDIVIDUAL__INDIVIDUAL:
				((FeatureMap.Internal)getIndividual()).set(newValue);
				return;
			case OwlPackage.SAME_INDIVIDUAL__NAMED_INDIVIDUAL:
				getNamedIndividual().clear();
				getNamedIndividual().addAll((Collection<? extends NamedIndividual>)newValue);
				return;
			case OwlPackage.SAME_INDIVIDUAL__ANONYMOUS_INDIVIDUAL:
				getAnonymousIndividual().clear();
				getAnonymousIndividual().addAll((Collection<? extends AnonymousIndividual>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.SAME_INDIVIDUAL__INDIVIDUAL:
				getIndividual().clear();
				return;
			case OwlPackage.SAME_INDIVIDUAL__NAMED_INDIVIDUAL:
				getNamedIndividual().clear();
				return;
			case OwlPackage.SAME_INDIVIDUAL__ANONYMOUS_INDIVIDUAL:
				getAnonymousIndividual().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.SAME_INDIVIDUAL__INDIVIDUAL:
				return individual != null && !individual.isEmpty();
			case OwlPackage.SAME_INDIVIDUAL__NAMED_INDIVIDUAL:
				return !getNamedIndividual().isEmpty();
			case OwlPackage.SAME_INDIVIDUAL__ANONYMOUS_INDIVIDUAL:
				return !getAnonymousIndividual().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (individual: ");
		result.append(individual);
		result.append(')');
		return result.toString();
	}

} //SameIndividualImpl
