/**
 */
package org.w3._2002._07.owl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.w3._2002._07.owl.AnnotationProperty;
import org.w3._2002._07.owl.OwlPackage;
import org.w3._2002._07.owl.SubAnnotationPropertyOf;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Annotation Property Of</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.SubAnnotationPropertyOfImpl#getAnnotationProperty <em>Annotation Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubAnnotationPropertyOfImpl#getAnnotationProperty1 <em>Annotation Property1</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubAnnotationPropertyOfImpl extends AnnotationAxiomImpl implements SubAnnotationPropertyOf {
	/**
	 * The cached value of the '{@link #getAnnotationProperty() <em>Annotation Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotationProperty()
	 * @generated
	 * @ordered
	 */
	protected AnnotationProperty annotationProperty;

	/**
	 * The cached value of the '{@link #getAnnotationProperty1() <em>Annotation Property1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotationProperty1()
	 * @generated
	 * @ordered
	 */
	protected AnnotationProperty annotationProperty1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubAnnotationPropertyOfImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getSubAnnotationPropertyOf();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationProperty getAnnotationProperty() {
		return annotationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotationProperty(AnnotationProperty newAnnotationProperty, NotificationChain msgs) {
		AnnotationProperty oldAnnotationProperty = annotationProperty;
		annotationProperty = newAnnotationProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY, oldAnnotationProperty, newAnnotationProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotationProperty(AnnotationProperty newAnnotationProperty) {
		if (newAnnotationProperty != annotationProperty) {
			NotificationChain msgs = null;
			if (annotationProperty != null)
				msgs = ((InternalEObject)annotationProperty).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY, null, msgs);
			if (newAnnotationProperty != null)
				msgs = ((InternalEObject)newAnnotationProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY, null, msgs);
			msgs = basicSetAnnotationProperty(newAnnotationProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY, newAnnotationProperty, newAnnotationProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationProperty getAnnotationProperty1() {
		return annotationProperty1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotationProperty1(AnnotationProperty newAnnotationProperty1, NotificationChain msgs) {
		AnnotationProperty oldAnnotationProperty1 = annotationProperty1;
		annotationProperty1 = newAnnotationProperty1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY1, oldAnnotationProperty1, newAnnotationProperty1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotationProperty1(AnnotationProperty newAnnotationProperty1) {
		if (newAnnotationProperty1 != annotationProperty1) {
			NotificationChain msgs = null;
			if (annotationProperty1 != null)
				msgs = ((InternalEObject)annotationProperty1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY1, null, msgs);
			if (newAnnotationProperty1 != null)
				msgs = ((InternalEObject)newAnnotationProperty1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY1, null, msgs);
			msgs = basicSetAnnotationProperty1(newAnnotationProperty1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY1, newAnnotationProperty1, newAnnotationProperty1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY:
				return basicSetAnnotationProperty(null, msgs);
			case OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY1:
				return basicSetAnnotationProperty1(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY:
				return getAnnotationProperty();
			case OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY1:
				return getAnnotationProperty1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY:
				setAnnotationProperty((AnnotationProperty)newValue);
				return;
			case OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY1:
				setAnnotationProperty1((AnnotationProperty)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY:
				setAnnotationProperty((AnnotationProperty)null);
				return;
			case OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY1:
				setAnnotationProperty1((AnnotationProperty)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY:
				return annotationProperty != null;
			case OwlPackage.SUB_ANNOTATION_PROPERTY_OF__ANNOTATION_PROPERTY1:
				return annotationProperty1 != null;
		}
		return super.eIsSet(featureID);
	}

} //SubAnnotationPropertyOfImpl
