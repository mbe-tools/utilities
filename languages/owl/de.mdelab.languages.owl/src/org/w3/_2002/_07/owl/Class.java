/**
 */
package org.w3._2002._07.owl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.Class#getAbbreviatedIRI <em>Abbreviated IRI</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.Class#getIRI <em>IRI</em>}</li>
 * </ul>
 *
 * @see org.w3._2002._07.owl.OwlPackage#getClass_()
 * @model extendedMetaData="name='Class' kind='empty'"
 * @generated
 */
public interface Class extends ClassExpression {
	/**
	 * Returns the value of the '<em><b>Abbreviated IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abbreviated IRI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abbreviated IRI</em>' attribute.
	 * @see #setAbbreviatedIRI(String)
	 * @see org.w3._2002._07.owl.OwlPackage#getClass_AbbreviatedIRI()
	 * @model dataType="org.w3._2002._07.owl.AbbreviatedIRI"
	 *        extendedMetaData="kind='attribute' name='abbreviatedIRI'"
	 * @generated
	 */
	String getAbbreviatedIRI();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Class#getAbbreviatedIRI <em>Abbreviated IRI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abbreviated IRI</em>' attribute.
	 * @see #getAbbreviatedIRI()
	 * @generated
	 */
	void setAbbreviatedIRI(String value);

	/**
	 * Returns the value of the '<em><b>IRI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>IRI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IRI</em>' attribute.
	 * @see #setIRI(String)
	 * @see org.w3._2002._07.owl.OwlPackage#getClass_IRI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='IRI'"
	 * @generated
	 */
	String getIRI();

	/**
	 * Sets the value of the '{@link org.w3._2002._07.owl.Class#getIRI <em>IRI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IRI</em>' attribute.
	 * @see #getIRI()
	 * @generated
	 */
	void setIRI(String value);

} // Class
