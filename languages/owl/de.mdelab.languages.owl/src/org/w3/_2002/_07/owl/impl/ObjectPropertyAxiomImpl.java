/**
 */
package org.w3._2002._07.owl.impl;

import org.eclipse.emf.ecore.EClass;

import org.w3._2002._07.owl.ObjectPropertyAxiom;
import org.w3._2002._07.owl.OwlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object Property Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ObjectPropertyAxiomImpl extends AxiomImpl implements ObjectPropertyAxiom {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObjectPropertyAxiomImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getObjectPropertyAxiom();
	}

} //ObjectPropertyAxiomImpl
