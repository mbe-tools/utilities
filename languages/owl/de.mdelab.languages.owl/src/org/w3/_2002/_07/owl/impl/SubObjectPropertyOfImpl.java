/**
 */
package org.w3._2002._07.owl.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.w3._2002._07.owl.ObjectInverseOf;
import org.w3._2002._07.owl.ObjectProperty;
import org.w3._2002._07.owl.ObjectPropertyChain;
import org.w3._2002._07.owl.OwlPackage;
import org.w3._2002._07.owl.SubObjectPropertyOf;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Object Property Of</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.w3._2002._07.owl.impl.SubObjectPropertyOfImpl#getObjectProperty <em>Object Property</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubObjectPropertyOfImpl#getObjectInverseOf <em>Object Inverse Of</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubObjectPropertyOfImpl#getObjectPropertyChain <em>Object Property Chain</em>}</li>
 *   <li>{@link org.w3._2002._07.owl.impl.SubObjectPropertyOfImpl#getObjectInverseOf1 <em>Object Inverse Of1</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubObjectPropertyOfImpl extends ObjectPropertyAxiomImpl implements SubObjectPropertyOf {
	/**
	 * The cached value of the '{@link #getObjectProperty() <em>Object Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<ObjectProperty> objectProperty;

	/**
	 * The cached value of the '{@link #getObjectInverseOf() <em>Object Inverse Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectInverseOf()
	 * @generated
	 * @ordered
	 */
	protected ObjectInverseOf objectInverseOf;

	/**
	 * The cached value of the '{@link #getObjectPropertyChain() <em>Object Property Chain</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectPropertyChain()
	 * @generated
	 * @ordered
	 */
	protected ObjectPropertyChain objectPropertyChain;

	/**
	 * The cached value of the '{@link #getObjectInverseOf1() <em>Object Inverse Of1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectInverseOf1()
	 * @generated
	 * @ordered
	 */
	protected ObjectInverseOf objectInverseOf1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubObjectPropertyOfImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OwlPackage.eINSTANCE.getSubObjectPropertyOf();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectProperty> getObjectProperty() {
		if (objectProperty == null) {
			objectProperty = new EObjectContainmentEList<ObjectProperty>(ObjectProperty.class, this, OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY);
		}
		return objectProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectInverseOf getObjectInverseOf() {
		return objectInverseOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectInverseOf(ObjectInverseOf newObjectInverseOf, NotificationChain msgs) {
		ObjectInverseOf oldObjectInverseOf = objectInverseOf;
		objectInverseOf = newObjectInverseOf;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF, oldObjectInverseOf, newObjectInverseOf);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectInverseOf(ObjectInverseOf newObjectInverseOf) {
		if (newObjectInverseOf != objectInverseOf) {
			NotificationChain msgs = null;
			if (objectInverseOf != null)
				msgs = ((InternalEObject)objectInverseOf).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF, null, msgs);
			if (newObjectInverseOf != null)
				msgs = ((InternalEObject)newObjectInverseOf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF, null, msgs);
			msgs = basicSetObjectInverseOf(newObjectInverseOf, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF, newObjectInverseOf, newObjectInverseOf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectPropertyChain getObjectPropertyChain() {
		return objectPropertyChain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectPropertyChain(ObjectPropertyChain newObjectPropertyChain, NotificationChain msgs) {
		ObjectPropertyChain oldObjectPropertyChain = objectPropertyChain;
		objectPropertyChain = newObjectPropertyChain;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY_CHAIN, oldObjectPropertyChain, newObjectPropertyChain);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectPropertyChain(ObjectPropertyChain newObjectPropertyChain) {
		if (newObjectPropertyChain != objectPropertyChain) {
			NotificationChain msgs = null;
			if (objectPropertyChain != null)
				msgs = ((InternalEObject)objectPropertyChain).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY_CHAIN, null, msgs);
			if (newObjectPropertyChain != null)
				msgs = ((InternalEObject)newObjectPropertyChain).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY_CHAIN, null, msgs);
			msgs = basicSetObjectPropertyChain(newObjectPropertyChain, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY_CHAIN, newObjectPropertyChain, newObjectPropertyChain));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectInverseOf getObjectInverseOf1() {
		return objectInverseOf1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectInverseOf1(ObjectInverseOf newObjectInverseOf1, NotificationChain msgs) {
		ObjectInverseOf oldObjectInverseOf1 = objectInverseOf1;
		objectInverseOf1 = newObjectInverseOf1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF1, oldObjectInverseOf1, newObjectInverseOf1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectInverseOf1(ObjectInverseOf newObjectInverseOf1) {
		if (newObjectInverseOf1 != objectInverseOf1) {
			NotificationChain msgs = null;
			if (objectInverseOf1 != null)
				msgs = ((InternalEObject)objectInverseOf1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF1, null, msgs);
			if (newObjectInverseOf1 != null)
				msgs = ((InternalEObject)newObjectInverseOf1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF1, null, msgs);
			msgs = basicSetObjectInverseOf1(newObjectInverseOf1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF1, newObjectInverseOf1, newObjectInverseOf1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY:
				return ((InternalEList<?>)getObjectProperty()).basicRemove(otherEnd, msgs);
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF:
				return basicSetObjectInverseOf(null, msgs);
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY_CHAIN:
				return basicSetObjectPropertyChain(null, msgs);
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF1:
				return basicSetObjectInverseOf1(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY:
				return getObjectProperty();
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF:
				return getObjectInverseOf();
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY_CHAIN:
				return getObjectPropertyChain();
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF1:
				return getObjectInverseOf1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY:
				getObjectProperty().clear();
				getObjectProperty().addAll((Collection<? extends ObjectProperty>)newValue);
				return;
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF:
				setObjectInverseOf((ObjectInverseOf)newValue);
				return;
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY_CHAIN:
				setObjectPropertyChain((ObjectPropertyChain)newValue);
				return;
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF1:
				setObjectInverseOf1((ObjectInverseOf)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY:
				getObjectProperty().clear();
				return;
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF:
				setObjectInverseOf((ObjectInverseOf)null);
				return;
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY_CHAIN:
				setObjectPropertyChain((ObjectPropertyChain)null);
				return;
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF1:
				setObjectInverseOf1((ObjectInverseOf)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY:
				return objectProperty != null && !objectProperty.isEmpty();
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF:
				return objectInverseOf != null;
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_PROPERTY_CHAIN:
				return objectPropertyChain != null;
			case OwlPackage.SUB_OBJECT_PROPERTY_OF__OBJECT_INVERSE_OF1:
				return objectInverseOf1 != null;
		}
		return super.eIsSet(featureID);
	}

} //SubObjectPropertyOfImpl
