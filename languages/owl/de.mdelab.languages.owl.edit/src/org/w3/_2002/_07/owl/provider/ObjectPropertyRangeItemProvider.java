/**
 */
package org.w3._2002._07.owl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.w3._2002._07.owl.ObjectPropertyRange;
import org.w3._2002._07.owl.OwlFactory;
import org.w3._2002._07.owl.OwlPackage;

/**
 * This is the item provider adapter for a {@link org.w3._2002._07.owl.ObjectPropertyRange} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ObjectPropertyRangeItemProvider extends ObjectPropertyAxiomItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectPropertyRangeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectInverseOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_Class());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectIntersectionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectUnionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectComplementOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectOneOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectSomeValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectAllValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectHasValue());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectHasSelf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectMinCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectMaxCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectExactCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_DataSomeValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_DataAllValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_DataHasValue());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_DataMinCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_DataMaxCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectPropertyRange_DataExactCardinality());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ObjectPropertyRange.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ObjectPropertyRange"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ObjectPropertyRange)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_ObjectPropertyRange_type") :
			getString("_UI_ObjectPropertyRange_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ObjectPropertyRange.class)) {
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_PROPERTY:
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_INVERSE_OF:
			case OwlPackage.OBJECT_PROPERTY_RANGE__CLASS:
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_INTERSECTION_OF:
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_UNION_OF:
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_COMPLEMENT_OF:
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_ONE_OF:
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_SOME_VALUES_FROM:
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_ALL_VALUES_FROM:
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_HAS_VALUE:
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_HAS_SELF:
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_MIN_CARDINALITY:
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_MAX_CARDINALITY:
			case OwlPackage.OBJECT_PROPERTY_RANGE__OBJECT_EXACT_CARDINALITY:
			case OwlPackage.OBJECT_PROPERTY_RANGE__DATA_SOME_VALUES_FROM:
			case OwlPackage.OBJECT_PROPERTY_RANGE__DATA_ALL_VALUES_FROM:
			case OwlPackage.OBJECT_PROPERTY_RANGE__DATA_HAS_VALUE:
			case OwlPackage.OBJECT_PROPERTY_RANGE__DATA_MIN_CARDINALITY:
			case OwlPackage.OBJECT_PROPERTY_RANGE__DATA_MAX_CARDINALITY:
			case OwlPackage.OBJECT_PROPERTY_RANGE__DATA_EXACT_CARDINALITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectProperty(),
				 OwlFactory.eINSTANCE.createObjectProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectInverseOf(),
				 OwlFactory.eINSTANCE.createObjectInverseOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_Class(),
				 OwlFactory.eINSTANCE.createClass()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectIntersectionOf(),
				 OwlFactory.eINSTANCE.createObjectIntersectionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectUnionOf(),
				 OwlFactory.eINSTANCE.createObjectUnionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectComplementOf(),
				 OwlFactory.eINSTANCE.createObjectComplementOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectOneOf(),
				 OwlFactory.eINSTANCE.createObjectOneOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectSomeValuesFrom(),
				 OwlFactory.eINSTANCE.createObjectSomeValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectAllValuesFrom(),
				 OwlFactory.eINSTANCE.createObjectAllValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectHasValue(),
				 OwlFactory.eINSTANCE.createObjectHasValue()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectHasSelf(),
				 OwlFactory.eINSTANCE.createObjectHasSelf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectMinCardinality(),
				 OwlFactory.eINSTANCE.createObjectMinCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectMaxCardinality(),
				 OwlFactory.eINSTANCE.createObjectMaxCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_ObjectExactCardinality(),
				 OwlFactory.eINSTANCE.createObjectExactCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_DataSomeValuesFrom(),
				 OwlFactory.eINSTANCE.createDataSomeValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_DataAllValuesFrom(),
				 OwlFactory.eINSTANCE.createDataAllValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_DataHasValue(),
				 OwlFactory.eINSTANCE.createDataHasValue()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_DataMinCardinality(),
				 OwlFactory.eINSTANCE.createDataMinCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_DataMaxCardinality(),
				 OwlFactory.eINSTANCE.createDataMaxCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectPropertyRange_DataExactCardinality(),
				 OwlFactory.eINSTANCE.createDataExactCardinality()));
	}

}
