/**
 */
package org.w3._2002._07.owl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.w3._2002._07.owl.ObjectMaxCardinality;
import org.w3._2002._07.owl.OwlFactory;
import org.w3._2002._07.owl.OwlPackage;

/**
 * This is the item provider adapter for a {@link org.w3._2002._07.owl.ObjectMaxCardinality} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ObjectMaxCardinalityItemProvider extends ClassExpressionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectMaxCardinalityItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCardinalityPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Cardinality feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCardinalityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ObjectMaxCardinality_cardinality_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ObjectMaxCardinality_cardinality_feature", "_UI_ObjectMaxCardinality_type"),
				 OwlPackage.eINSTANCE.getObjectMaxCardinality_Cardinality(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectInverseOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_Class());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectIntersectionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectUnionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectComplementOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectOneOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectSomeValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectAllValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectHasValue());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectHasSelf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectMinCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectMaxCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectExactCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_DataSomeValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_DataAllValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_DataHasValue());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_DataMinCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_DataMaxCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getObjectMaxCardinality_DataExactCardinality());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ObjectMaxCardinality.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ObjectMaxCardinality"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ObjectMaxCardinality)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_ObjectMaxCardinality_type") :
			getString("_UI_ObjectMaxCardinality_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ObjectMaxCardinality.class)) {
			case OwlPackage.OBJECT_MAX_CARDINALITY__CARDINALITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_PROPERTY:
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_INVERSE_OF:
			case OwlPackage.OBJECT_MAX_CARDINALITY__CLASS:
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_INTERSECTION_OF:
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_UNION_OF:
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_COMPLEMENT_OF:
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_ONE_OF:
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_SOME_VALUES_FROM:
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_ALL_VALUES_FROM:
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_HAS_VALUE:
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_HAS_SELF:
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_MIN_CARDINALITY:
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_MAX_CARDINALITY:
			case OwlPackage.OBJECT_MAX_CARDINALITY__OBJECT_EXACT_CARDINALITY:
			case OwlPackage.OBJECT_MAX_CARDINALITY__DATA_SOME_VALUES_FROM:
			case OwlPackage.OBJECT_MAX_CARDINALITY__DATA_ALL_VALUES_FROM:
			case OwlPackage.OBJECT_MAX_CARDINALITY__DATA_HAS_VALUE:
			case OwlPackage.OBJECT_MAX_CARDINALITY__DATA_MIN_CARDINALITY:
			case OwlPackage.OBJECT_MAX_CARDINALITY__DATA_MAX_CARDINALITY:
			case OwlPackage.OBJECT_MAX_CARDINALITY__DATA_EXACT_CARDINALITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectProperty(),
				 OwlFactory.eINSTANCE.createObjectProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectInverseOf(),
				 OwlFactory.eINSTANCE.createObjectInverseOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_Class(),
				 OwlFactory.eINSTANCE.createClass()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectIntersectionOf(),
				 OwlFactory.eINSTANCE.createObjectIntersectionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectUnionOf(),
				 OwlFactory.eINSTANCE.createObjectUnionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectComplementOf(),
				 OwlFactory.eINSTANCE.createObjectComplementOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectOneOf(),
				 OwlFactory.eINSTANCE.createObjectOneOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectSomeValuesFrom(),
				 OwlFactory.eINSTANCE.createObjectSomeValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectAllValuesFrom(),
				 OwlFactory.eINSTANCE.createObjectAllValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectHasValue(),
				 OwlFactory.eINSTANCE.createObjectHasValue()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectHasSelf(),
				 OwlFactory.eINSTANCE.createObjectHasSelf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectMinCardinality(),
				 OwlFactory.eINSTANCE.createObjectMinCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectMaxCardinality(),
				 OwlFactory.eINSTANCE.createObjectMaxCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_ObjectExactCardinality(),
				 OwlFactory.eINSTANCE.createObjectExactCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_DataSomeValuesFrom(),
				 OwlFactory.eINSTANCE.createDataSomeValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_DataAllValuesFrom(),
				 OwlFactory.eINSTANCE.createDataAllValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_DataHasValue(),
				 OwlFactory.eINSTANCE.createDataHasValue()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_DataMinCardinality(),
				 OwlFactory.eINSTANCE.createDataMinCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_DataMaxCardinality(),
				 OwlFactory.eINSTANCE.createDataMaxCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getObjectMaxCardinality_DataExactCardinality(),
				 OwlFactory.eINSTANCE.createDataExactCardinality()));
	}

}
