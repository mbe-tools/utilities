/**
 */
package org.w3._2002._07.owl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.w3._2002._07.owl.DataPropertyDomain;
import org.w3._2002._07.owl.OwlFactory;
import org.w3._2002._07.owl.OwlPackage;

/**
 * This is the item provider adapter for a {@link org.w3._2002._07.owl.DataPropertyDomain} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DataPropertyDomainItemProvider extends DataPropertyAxiomItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPropertyDomainItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_DataProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_Class());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectIntersectionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectUnionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectComplementOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectOneOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectSomeValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectAllValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectHasValue());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectHasSelf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectMinCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectMaxCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectExactCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_DataSomeValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_DataAllValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_DataHasValue());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_DataMinCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_DataMaxCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataPropertyDomain_DataExactCardinality());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns DataPropertyDomain.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DataPropertyDomain"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DataPropertyDomain)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_DataPropertyDomain_type") :
			getString("_UI_DataPropertyDomain_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DataPropertyDomain.class)) {
			case OwlPackage.DATA_PROPERTY_DOMAIN__DATA_PROPERTY:
			case OwlPackage.DATA_PROPERTY_DOMAIN__CLASS:
			case OwlPackage.DATA_PROPERTY_DOMAIN__OBJECT_INTERSECTION_OF:
			case OwlPackage.DATA_PROPERTY_DOMAIN__OBJECT_UNION_OF:
			case OwlPackage.DATA_PROPERTY_DOMAIN__OBJECT_COMPLEMENT_OF:
			case OwlPackage.DATA_PROPERTY_DOMAIN__OBJECT_ONE_OF:
			case OwlPackage.DATA_PROPERTY_DOMAIN__OBJECT_SOME_VALUES_FROM:
			case OwlPackage.DATA_PROPERTY_DOMAIN__OBJECT_ALL_VALUES_FROM:
			case OwlPackage.DATA_PROPERTY_DOMAIN__OBJECT_HAS_VALUE:
			case OwlPackage.DATA_PROPERTY_DOMAIN__OBJECT_HAS_SELF:
			case OwlPackage.DATA_PROPERTY_DOMAIN__OBJECT_MIN_CARDINALITY:
			case OwlPackage.DATA_PROPERTY_DOMAIN__OBJECT_MAX_CARDINALITY:
			case OwlPackage.DATA_PROPERTY_DOMAIN__OBJECT_EXACT_CARDINALITY:
			case OwlPackage.DATA_PROPERTY_DOMAIN__DATA_SOME_VALUES_FROM:
			case OwlPackage.DATA_PROPERTY_DOMAIN__DATA_ALL_VALUES_FROM:
			case OwlPackage.DATA_PROPERTY_DOMAIN__DATA_HAS_VALUE:
			case OwlPackage.DATA_PROPERTY_DOMAIN__DATA_MIN_CARDINALITY:
			case OwlPackage.DATA_PROPERTY_DOMAIN__DATA_MAX_CARDINALITY:
			case OwlPackage.DATA_PROPERTY_DOMAIN__DATA_EXACT_CARDINALITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_DataProperty(),
				 OwlFactory.eINSTANCE.createDataProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_Class(),
				 OwlFactory.eINSTANCE.createClass()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectIntersectionOf(),
				 OwlFactory.eINSTANCE.createObjectIntersectionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectUnionOf(),
				 OwlFactory.eINSTANCE.createObjectUnionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectComplementOf(),
				 OwlFactory.eINSTANCE.createObjectComplementOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectOneOf(),
				 OwlFactory.eINSTANCE.createObjectOneOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectSomeValuesFrom(),
				 OwlFactory.eINSTANCE.createObjectSomeValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectAllValuesFrom(),
				 OwlFactory.eINSTANCE.createObjectAllValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectHasValue(),
				 OwlFactory.eINSTANCE.createObjectHasValue()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectHasSelf(),
				 OwlFactory.eINSTANCE.createObjectHasSelf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectMinCardinality(),
				 OwlFactory.eINSTANCE.createObjectMinCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectMaxCardinality(),
				 OwlFactory.eINSTANCE.createObjectMaxCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_ObjectExactCardinality(),
				 OwlFactory.eINSTANCE.createObjectExactCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_DataSomeValuesFrom(),
				 OwlFactory.eINSTANCE.createDataSomeValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_DataAllValuesFrom(),
				 OwlFactory.eINSTANCE.createDataAllValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_DataHasValue(),
				 OwlFactory.eINSTANCE.createDataHasValue()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_DataMinCardinality(),
				 OwlFactory.eINSTANCE.createDataMinCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_DataMaxCardinality(),
				 OwlFactory.eINSTANCE.createDataMaxCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataPropertyDomain_DataExactCardinality(),
				 OwlFactory.eINSTANCE.createDataExactCardinality()));
	}

}
