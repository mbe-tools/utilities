/**
 */
package org.w3._2002._07.owl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.w3._2002._07.owl.DatatypeDefinition;
import org.w3._2002._07.owl.OwlFactory;
import org.w3._2002._07.owl.OwlPackage;

/**
 * This is the item provider adapter for a {@link org.w3._2002._07.owl.DatatypeDefinition} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DatatypeDefinitionItemProvider extends AxiomItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatatypeDefinitionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OwlPackage.eINSTANCE.getDatatypeDefinition_Datatype());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDatatypeDefinition_Datatype1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDatatypeDefinition_DataIntersectionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDatatypeDefinition_DataUnionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDatatypeDefinition_DataComplementOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDatatypeDefinition_DataOneOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDatatypeDefinition_DatatypeRestriction());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns DatatypeDefinition.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DatatypeDefinition"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DatatypeDefinition)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_DatatypeDefinition_type") :
			getString("_UI_DatatypeDefinition_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DatatypeDefinition.class)) {
			case OwlPackage.DATATYPE_DEFINITION__DATATYPE:
			case OwlPackage.DATATYPE_DEFINITION__DATATYPE1:
			case OwlPackage.DATATYPE_DEFINITION__DATA_INTERSECTION_OF:
			case OwlPackage.DATATYPE_DEFINITION__DATA_UNION_OF:
			case OwlPackage.DATATYPE_DEFINITION__DATA_COMPLEMENT_OF:
			case OwlPackage.DATATYPE_DEFINITION__DATA_ONE_OF:
			case OwlPackage.DATATYPE_DEFINITION__DATATYPE_RESTRICTION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDatatypeDefinition_Datatype(),
				 OwlFactory.eINSTANCE.createDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDatatypeDefinition_Datatype1(),
				 OwlFactory.eINSTANCE.createDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDatatypeDefinition_DataIntersectionOf(),
				 OwlFactory.eINSTANCE.createDataIntersectionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDatatypeDefinition_DataUnionOf(),
				 OwlFactory.eINSTANCE.createDataUnionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDatatypeDefinition_DataComplementOf(),
				 OwlFactory.eINSTANCE.createDataComplementOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDatatypeDefinition_DataOneOf(),
				 OwlFactory.eINSTANCE.createDataOneOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDatatypeDefinition_DatatypeRestriction(),
				 OwlFactory.eINSTANCE.createDatatypeRestriction()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == OwlPackage.eINSTANCE.getDatatypeDefinition_Datatype() ||
			childFeature == OwlPackage.eINSTANCE.getDatatypeDefinition_Datatype1();

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
