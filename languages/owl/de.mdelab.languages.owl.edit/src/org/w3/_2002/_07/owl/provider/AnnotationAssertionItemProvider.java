/**
 */
package org.w3._2002._07.owl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.w3._2002._07.owl.AnnotationAssertion;
import org.w3._2002._07.owl.OwlFactory;
import org.w3._2002._07.owl.OwlPackage;

/**
 * This is the item provider adapter for a {@link org.w3._2002._07.owl.AnnotationAssertion} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AnnotationAssertionItemProvider extends AnnotationAxiomItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationAssertionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OwlPackage.eINSTANCE.getAnnotationAssertion_AnnotationProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getAnnotationAssertion_IRI());
			childrenFeatures.add(OwlPackage.eINSTANCE.getAnnotationAssertion_AbbreviatedIRI());
			childrenFeatures.add(OwlPackage.eINSTANCE.getAnnotationAssertion_AnonymousIndividual());
			childrenFeatures.add(OwlPackage.eINSTANCE.getAnnotationAssertion_IRI1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getAnnotationAssertion_AbbreviatedIRI1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getAnnotationAssertion_AnonymousIndividual1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getAnnotationAssertion_Literal());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns AnnotationAssertion.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/AnnotationAssertion"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((AnnotationAssertion)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_AnnotationAssertion_type") :
			getString("_UI_AnnotationAssertion_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AnnotationAssertion.class)) {
			case OwlPackage.ANNOTATION_ASSERTION__ANNOTATION_PROPERTY:
			case OwlPackage.ANNOTATION_ASSERTION__IRI:
			case OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI:
			case OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL:
			case OwlPackage.ANNOTATION_ASSERTION__IRI1:
			case OwlPackage.ANNOTATION_ASSERTION__ABBREVIATED_IRI1:
			case OwlPackage.ANNOTATION_ASSERTION__ANONYMOUS_INDIVIDUAL1:
			case OwlPackage.ANNOTATION_ASSERTION__LITERAL:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getAnnotationAssertion_AnnotationProperty(),
				 OwlFactory.eINSTANCE.createAnnotationProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getAnnotationAssertion_IRI(),
				 OwlFactory.eINSTANCE.createIRI()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getAnnotationAssertion_AbbreviatedIRI(),
				 OwlFactory.eINSTANCE.createAbbreviatedIRI1()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getAnnotationAssertion_AnonymousIndividual(),
				 OwlFactory.eINSTANCE.createAnonymousIndividual()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getAnnotationAssertion_IRI1(),
				 OwlFactory.eINSTANCE.createIRI()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getAnnotationAssertion_AbbreviatedIRI1(),
				 OwlFactory.eINSTANCE.createAbbreviatedIRI1()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getAnnotationAssertion_AnonymousIndividual1(),
				 OwlFactory.eINSTANCE.createAnonymousIndividual()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getAnnotationAssertion_Literal(),
				 OwlFactory.eINSTANCE.createLiteral()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == OwlPackage.eINSTANCE.getAnnotationAssertion_IRI() ||
			childFeature == OwlPackage.eINSTANCE.getAnnotationAssertion_IRI1() ||
			childFeature == OwlPackage.eINSTANCE.getAnnotationAssertion_AbbreviatedIRI() ||
			childFeature == OwlPackage.eINSTANCE.getAnnotationAssertion_AbbreviatedIRI1() ||
			childFeature == OwlPackage.eINSTANCE.getAnnotationAssertion_AnonymousIndividual() ||
			childFeature == OwlPackage.eINSTANCE.getAnnotationAssertion_AnonymousIndividual1();

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
