/**
 */
package org.w3._2002._07.owl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.w3._2002._07.owl.OwlFactory;
import org.w3._2002._07.owl.OwlPackage;
import org.w3._2002._07.owl.SubClassOf;

/**
 * This is the item provider adapter for a {@link org.w3._2002._07.owl.SubClassOf} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SubClassOfItemProvider extends ClassAxiomItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubClassOfItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_Class());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectIntersectionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectUnionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectComplementOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectOneOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectSomeValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectAllValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectHasValue());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectHasSelf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectMinCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectMaxCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectExactCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_DataSomeValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_DataAllValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_DataHasValue());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_DataMinCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_DataMaxCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_DataExactCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectIntersectionOf1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectUnionOf1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectComplementOf1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectOneOf1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectSomeValuesFrom1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectAllValuesFrom1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectHasValue1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectHasSelf1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectMinCardinality1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectMaxCardinality1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_ObjectExactCardinality1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_DataSomeValuesFrom1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_DataAllValuesFrom1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_DataHasValue1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_DataMinCardinality1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_DataMaxCardinality1());
			childrenFeatures.add(OwlPackage.eINSTANCE.getSubClassOf_DataExactCardinality1());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns SubClassOf.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SubClassOf"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((SubClassOf)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_SubClassOf_type") :
			getString("_UI_SubClassOf_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SubClassOf.class)) {
			case OwlPackage.SUB_CLASS_OF__CLASS:
			case OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF:
			case OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF:
			case OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF:
			case OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF:
			case OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM:
			case OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM:
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE:
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF:
			case OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY:
			case OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY:
			case OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY:
			case OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM:
			case OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM:
			case OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE:
			case OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY:
			case OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY:
			case OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY:
			case OwlPackage.SUB_CLASS_OF__OBJECT_INTERSECTION_OF1:
			case OwlPackage.SUB_CLASS_OF__OBJECT_UNION_OF1:
			case OwlPackage.SUB_CLASS_OF__OBJECT_COMPLEMENT_OF1:
			case OwlPackage.SUB_CLASS_OF__OBJECT_ONE_OF1:
			case OwlPackage.SUB_CLASS_OF__OBJECT_SOME_VALUES_FROM1:
			case OwlPackage.SUB_CLASS_OF__OBJECT_ALL_VALUES_FROM1:
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_VALUE1:
			case OwlPackage.SUB_CLASS_OF__OBJECT_HAS_SELF1:
			case OwlPackage.SUB_CLASS_OF__OBJECT_MIN_CARDINALITY1:
			case OwlPackage.SUB_CLASS_OF__OBJECT_MAX_CARDINALITY1:
			case OwlPackage.SUB_CLASS_OF__OBJECT_EXACT_CARDINALITY1:
			case OwlPackage.SUB_CLASS_OF__DATA_SOME_VALUES_FROM1:
			case OwlPackage.SUB_CLASS_OF__DATA_ALL_VALUES_FROM1:
			case OwlPackage.SUB_CLASS_OF__DATA_HAS_VALUE1:
			case OwlPackage.SUB_CLASS_OF__DATA_MIN_CARDINALITY1:
			case OwlPackage.SUB_CLASS_OF__DATA_MAX_CARDINALITY1:
			case OwlPackage.SUB_CLASS_OF__DATA_EXACT_CARDINALITY1:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_Class(),
				 OwlFactory.eINSTANCE.createClass()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectIntersectionOf(),
				 OwlFactory.eINSTANCE.createObjectIntersectionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectUnionOf(),
				 OwlFactory.eINSTANCE.createObjectUnionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectComplementOf(),
				 OwlFactory.eINSTANCE.createObjectComplementOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectOneOf(),
				 OwlFactory.eINSTANCE.createObjectOneOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectSomeValuesFrom(),
				 OwlFactory.eINSTANCE.createObjectSomeValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectAllValuesFrom(),
				 OwlFactory.eINSTANCE.createObjectAllValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectHasValue(),
				 OwlFactory.eINSTANCE.createObjectHasValue()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectHasSelf(),
				 OwlFactory.eINSTANCE.createObjectHasSelf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectMinCardinality(),
				 OwlFactory.eINSTANCE.createObjectMinCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectMaxCardinality(),
				 OwlFactory.eINSTANCE.createObjectMaxCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectExactCardinality(),
				 OwlFactory.eINSTANCE.createObjectExactCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_DataSomeValuesFrom(),
				 OwlFactory.eINSTANCE.createDataSomeValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_DataAllValuesFrom(),
				 OwlFactory.eINSTANCE.createDataAllValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_DataHasValue(),
				 OwlFactory.eINSTANCE.createDataHasValue()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_DataMinCardinality(),
				 OwlFactory.eINSTANCE.createDataMinCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_DataMaxCardinality(),
				 OwlFactory.eINSTANCE.createDataMaxCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_DataExactCardinality(),
				 OwlFactory.eINSTANCE.createDataExactCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectIntersectionOf1(),
				 OwlFactory.eINSTANCE.createObjectIntersectionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectUnionOf1(),
				 OwlFactory.eINSTANCE.createObjectUnionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectComplementOf1(),
				 OwlFactory.eINSTANCE.createObjectComplementOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectOneOf1(),
				 OwlFactory.eINSTANCE.createObjectOneOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectSomeValuesFrom1(),
				 OwlFactory.eINSTANCE.createObjectSomeValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectAllValuesFrom1(),
				 OwlFactory.eINSTANCE.createObjectAllValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectHasValue1(),
				 OwlFactory.eINSTANCE.createObjectHasValue()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectHasSelf1(),
				 OwlFactory.eINSTANCE.createObjectHasSelf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectMinCardinality1(),
				 OwlFactory.eINSTANCE.createObjectMinCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectMaxCardinality1(),
				 OwlFactory.eINSTANCE.createObjectMaxCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_ObjectExactCardinality1(),
				 OwlFactory.eINSTANCE.createObjectExactCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_DataSomeValuesFrom1(),
				 OwlFactory.eINSTANCE.createDataSomeValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_DataAllValuesFrom1(),
				 OwlFactory.eINSTANCE.createDataAllValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_DataHasValue1(),
				 OwlFactory.eINSTANCE.createDataHasValue()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_DataMinCardinality1(),
				 OwlFactory.eINSTANCE.createDataMinCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_DataMaxCardinality1(),
				 OwlFactory.eINSTANCE.createDataMaxCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getSubClassOf_DataExactCardinality1(),
				 OwlFactory.eINSTANCE.createDataExactCardinality()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectIntersectionOf() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectIntersectionOf1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectUnionOf() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectUnionOf1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectComplementOf() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectComplementOf1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectOneOf() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectOneOf1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectSomeValuesFrom() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectSomeValuesFrom1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectAllValuesFrom() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectAllValuesFrom1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectHasValue() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectHasValue1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectHasSelf() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectHasSelf1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectMinCardinality() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectMinCardinality1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectMaxCardinality() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectMaxCardinality1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectExactCardinality() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_ObjectExactCardinality1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_DataSomeValuesFrom() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_DataSomeValuesFrom1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_DataAllValuesFrom() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_DataAllValuesFrom1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_DataHasValue() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_DataHasValue1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_DataMinCardinality() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_DataMinCardinality1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_DataMaxCardinality() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_DataMaxCardinality1() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_DataExactCardinality() ||
			childFeature == OwlPackage.eINSTANCE.getSubClassOf_DataExactCardinality1();

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
