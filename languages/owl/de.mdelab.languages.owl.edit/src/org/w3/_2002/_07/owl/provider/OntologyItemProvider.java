/**
 */
package org.w3._2002._07.owl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.w3._2002._07.owl.Ontology;
import org.w3._2002._07.owl.OwlFactory;
import org.w3._2002._07.owl.OwlPackage;

/**
 * This is the item provider adapter for a {@link org.w3._2002._07.owl.Ontology} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class OntologyItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OntologyItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addBasePropertyDescriptor(object);
			addIdPropertyDescriptor(object);
			addLangPropertyDescriptor(object);
			addOntologyIRIPropertyDescriptor(object);
			addSpacePropertyDescriptor(object);
			addVersionIRIPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBasePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Ontology_base_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Ontology_base_feature", "_UI_Ontology_type"),
				 OwlPackage.eINSTANCE.getOntology_Base(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Ontology_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Ontology_id_feature", "_UI_Ontology_type"),
				 OwlPackage.eINSTANCE.getOntology_Id(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Lang feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLangPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Ontology_lang_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Ontology_lang_feature", "_UI_Ontology_type"),
				 OwlPackage.eINSTANCE.getOntology_Lang(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Ontology IRI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOntologyIRIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Ontology_ontologyIRI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Ontology_ontologyIRI_feature", "_UI_Ontology_type"),
				 OwlPackage.eINSTANCE.getOntology_OntologyIRI(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Space feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSpacePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Ontology_space_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Ontology_space_feature", "_UI_Ontology_type"),
				 OwlPackage.eINSTANCE.getOntology_Space(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Version IRI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVersionIRIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Ontology_versionIRI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Ontology_versionIRI_feature", "_UI_Ontology_type"),
				 OwlPackage.eINSTANCE.getOntology_VersionIRI(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OwlPackage.eINSTANCE.getOntology_Prefix());
			childrenFeatures.add(OwlPackage.eINSTANCE.getOntology_Import());
			childrenFeatures.add(OwlPackage.eINSTANCE.getOntology_Annotation());
			childrenFeatures.add(OwlPackage.eINSTANCE.getOntology_Axiom());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Ontology.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Ontology"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Ontology)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_Ontology_type") :
			getString("_UI_Ontology_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Ontology.class)) {
			case OwlPackage.ONTOLOGY__BASE:
			case OwlPackage.ONTOLOGY__ID:
			case OwlPackage.ONTOLOGY__LANG:
			case OwlPackage.ONTOLOGY__ONTOLOGY_IRI:
			case OwlPackage.ONTOLOGY__SPACE:
			case OwlPackage.ONTOLOGY__VERSION_IRI:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case OwlPackage.ONTOLOGY__PREFIX:
			case OwlPackage.ONTOLOGY__IMPORT:
			case OwlPackage.ONTOLOGY__ANNOTATION:
			case OwlPackage.ONTOLOGY__AXIOM:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Prefix(),
				 OwlFactory.eINSTANCE.createPrefix()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Import(),
				 OwlFactory.eINSTANCE.createImport()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Annotation(),
				 OwlFactory.eINSTANCE.createAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_Declaration(),
					 OwlFactory.eINSTANCE.createDeclaration())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_SubClassOf(),
					 OwlFactory.eINSTANCE.createSubClassOf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_EquivalentClasses(),
					 OwlFactory.eINSTANCE.createEquivalentClasses())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_DisjointClasses(),
					 OwlFactory.eINSTANCE.createDisjointClasses())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_DisjointUnion(),
					 OwlFactory.eINSTANCE.createDisjointUnion())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_SubObjectPropertyOf(),
					 OwlFactory.eINSTANCE.createSubObjectPropertyOf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_EquivalentObjectProperties(),
					 OwlFactory.eINSTANCE.createEquivalentObjectProperties())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_DisjointObjectProperties(),
					 OwlFactory.eINSTANCE.createDisjointObjectProperties())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_InverseObjectProperties(),
					 OwlFactory.eINSTANCE.createInverseObjectProperties())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_ObjectPropertyDomain(),
					 OwlFactory.eINSTANCE.createObjectPropertyDomain())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_ObjectPropertyRange(),
					 OwlFactory.eINSTANCE.createObjectPropertyRange())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_FunctionalObjectProperty(),
					 OwlFactory.eINSTANCE.createFunctionalObjectProperty())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_InverseFunctionalObjectProperty(),
					 OwlFactory.eINSTANCE.createInverseFunctionalObjectProperty())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_ReflexiveObjectProperty(),
					 OwlFactory.eINSTANCE.createReflexiveObjectProperty())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_IrreflexiveObjectProperty(),
					 OwlFactory.eINSTANCE.createIrreflexiveObjectProperty())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_SymmetricObjectProperty(),
					 OwlFactory.eINSTANCE.createSymmetricObjectProperty())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_AsymmetricObjectProperty(),
					 OwlFactory.eINSTANCE.createAsymmetricObjectProperty())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_TransitiveObjectProperty(),
					 OwlFactory.eINSTANCE.createTransitiveObjectProperty())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_SubDataPropertyOf(),
					 OwlFactory.eINSTANCE.createSubDataPropertyOf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_EquivalentDataProperties(),
					 OwlFactory.eINSTANCE.createEquivalentDataProperties())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_DisjointDataProperties(),
					 OwlFactory.eINSTANCE.createDisjointDataProperties())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_DataPropertyDomain(),
					 OwlFactory.eINSTANCE.createDataPropertyDomain())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_DataPropertyRange(),
					 OwlFactory.eINSTANCE.createDataPropertyRange())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_FunctionalDataProperty(),
					 OwlFactory.eINSTANCE.createFunctionalDataProperty())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_DatatypeDefinition(),
					 OwlFactory.eINSTANCE.createDatatypeDefinition())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_HasKey(),
					 OwlFactory.eINSTANCE.createHasKey())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_SameIndividual(),
					 OwlFactory.eINSTANCE.createSameIndividual())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_DifferentIndividuals(),
					 OwlFactory.eINSTANCE.createDifferentIndividuals())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_ClassAssertion(),
					 OwlFactory.eINSTANCE.createClassAssertion())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_ObjectPropertyAssertion(),
					 OwlFactory.eINSTANCE.createObjectPropertyAssertion())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_NegativeObjectPropertyAssertion(),
					 OwlFactory.eINSTANCE.createNegativeObjectPropertyAssertion())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_DataPropertyAssertion(),
					 OwlFactory.eINSTANCE.createDataPropertyAssertion())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_NegativeDataPropertyAssertion(),
					 OwlFactory.eINSTANCE.createNegativeDataPropertyAssertion())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_AnnotationAssertion(),
					 OwlFactory.eINSTANCE.createAnnotationAssertion())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_SubAnnotationPropertyOf(),
					 OwlFactory.eINSTANCE.createSubAnnotationPropertyOf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_AnnotationPropertyDomain(),
					 OwlFactory.eINSTANCE.createAnnotationPropertyDomain())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getOntology_Axiom(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getOntology_AnnotationPropertyRange(),
					 OwlFactory.eINSTANCE.createAnnotationPropertyRange())));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return OwlEditPlugin.INSTANCE;
	}

}
