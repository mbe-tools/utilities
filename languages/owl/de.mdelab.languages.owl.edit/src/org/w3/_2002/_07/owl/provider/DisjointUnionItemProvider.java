/**
 */
package org.w3._2002._07.owl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.w3._2002._07.owl.DisjointUnion;
import org.w3._2002._07.owl.OwlFactory;
import org.w3._2002._07.owl.OwlPackage;

/**
 * This is the item provider adapter for a {@link org.w3._2002._07.owl.DisjointUnion} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DisjointUnionItemProvider extends ClassAxiomItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisjointUnionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OwlPackage.eINSTANCE.getDisjointUnion_Class());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns DisjointUnion.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DisjointUnion"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DisjointUnion)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_DisjointUnion_type") :
			getString("_UI_DisjointUnion_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DisjointUnion.class)) {
			case OwlPackage.DISJOINT_UNION__CLASS:
			case OwlPackage.DISJOINT_UNION__CLASS_EXPRESSION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_Class(),
				 OwlFactory.eINSTANCE.createClass()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_Class1(),
					 OwlFactory.eINSTANCE.createClass())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_ObjectIntersectionOf(),
					 OwlFactory.eINSTANCE.createObjectIntersectionOf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_ObjectUnionOf(),
					 OwlFactory.eINSTANCE.createObjectUnionOf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_ObjectComplementOf(),
					 OwlFactory.eINSTANCE.createObjectComplementOf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_ObjectOneOf(),
					 OwlFactory.eINSTANCE.createObjectOneOf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_ObjectSomeValuesFrom(),
					 OwlFactory.eINSTANCE.createObjectSomeValuesFrom())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_ObjectAllValuesFrom(),
					 OwlFactory.eINSTANCE.createObjectAllValuesFrom())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_ObjectHasValue(),
					 OwlFactory.eINSTANCE.createObjectHasValue())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_ObjectHasSelf(),
					 OwlFactory.eINSTANCE.createObjectHasSelf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_ObjectMinCardinality(),
					 OwlFactory.eINSTANCE.createObjectMinCardinality())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_ObjectMaxCardinality(),
					 OwlFactory.eINSTANCE.createObjectMaxCardinality())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_ObjectExactCardinality(),
					 OwlFactory.eINSTANCE.createObjectExactCardinality())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_DataSomeValuesFrom(),
					 OwlFactory.eINSTANCE.createDataSomeValuesFrom())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_DataAllValuesFrom(),
					 OwlFactory.eINSTANCE.createDataAllValuesFrom())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_DataHasValue(),
					 OwlFactory.eINSTANCE.createDataHasValue())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_DataMinCardinality(),
					 OwlFactory.eINSTANCE.createDataMinCardinality())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_DataMaxCardinality(),
					 OwlFactory.eINSTANCE.createDataMaxCardinality())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointUnion_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointUnion_DataExactCardinality(),
					 OwlFactory.eINSTANCE.createDataExactCardinality())));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		if (childFeature instanceof EStructuralFeature && FeatureMapUtil.isFeatureMap((EStructuralFeature)childFeature)) {
			FeatureMap.Entry entry = (FeatureMap.Entry)childObject;
			childFeature = entry.getEStructuralFeature();
			childObject = entry.getValue();
		}

		boolean qualify =
			childFeature == OwlPackage.eINSTANCE.getDisjointUnion_Class() ||
			childFeature == OwlPackage.eINSTANCE.getDisjointUnion_Class1();

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
