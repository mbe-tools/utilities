/**
 */
package org.w3._2002._07.owl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.w3._2002._07.owl.DocumentRoot;
import org.w3._2002._07.owl.OwlFactory;
import org.w3._2002._07.owl.OwlPackage;

/**
 * This is the item provider adapter for a {@link org.w3._2002._07.owl.DocumentRoot} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DocumentRootItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DocumentRootItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_AbbreviatedIRI());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_Annotation());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationAssertion());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationPropertyDomain());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationPropertyRange());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_AnonymousIndividual());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_AsymmetricObjectProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_Class());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ClassAssertion());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataAllValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataComplementOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataExactCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataHasValue());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataIntersectionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataMaxCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataMinCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataOneOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyAssertion());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyDomain());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyRange());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataSomeValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_Datatype());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DatatypeDefinition());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DatatypeRestriction());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DataUnionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_Declaration());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DifferentIndividuals());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DisjointClasses());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DisjointDataProperties());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DisjointObjectProperties());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_DisjointUnion());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentClasses());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentDataProperties());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentObjectProperties());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_FunctionalDataProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_FunctionalObjectProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_HasKey());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_Import());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_InverseFunctionalObjectProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_InverseObjectProperties());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_IRI());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_IrreflexiveObjectProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_Literal());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_NamedIndividual());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_NegativeDataPropertyAssertion());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_NegativeObjectPropertyAssertion());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectAllValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectComplementOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectExactCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectHasSelf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectHasValue());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectIntersectionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectInverseOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectMaxCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectMinCardinality());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectOneOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyAssertion());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyDomain());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyRange());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectSomeValuesFrom());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ObjectUnionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_Ontology());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_Prefix());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_ReflexiveObjectProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_SameIndividual());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_SubAnnotationPropertyOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_SubClassOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_SubDataPropertyOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_SubObjectPropertyOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_SymmetricObjectProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDocumentRoot_TransitiveObjectProperty());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns DocumentRoot.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DocumentRoot"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_DocumentRoot_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DocumentRoot.class)) {
			case OwlPackage.DOCUMENT_ROOT__ABBREVIATED_IRI:
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION:
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_ASSERTION:
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY:
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY_DOMAIN:
			case OwlPackage.DOCUMENT_ROOT__ANNOTATION_PROPERTY_RANGE:
			case OwlPackage.DOCUMENT_ROOT__ANONYMOUS_INDIVIDUAL:
			case OwlPackage.DOCUMENT_ROOT__ASYMMETRIC_OBJECT_PROPERTY:
			case OwlPackage.DOCUMENT_ROOT__CLASS:
			case OwlPackage.DOCUMENT_ROOT__CLASS_ASSERTION:
			case OwlPackage.DOCUMENT_ROOT__DATA_ALL_VALUES_FROM:
			case OwlPackage.DOCUMENT_ROOT__DATA_COMPLEMENT_OF:
			case OwlPackage.DOCUMENT_ROOT__DATA_EXACT_CARDINALITY:
			case OwlPackage.DOCUMENT_ROOT__DATA_HAS_VALUE:
			case OwlPackage.DOCUMENT_ROOT__DATA_INTERSECTION_OF:
			case OwlPackage.DOCUMENT_ROOT__DATA_MAX_CARDINALITY:
			case OwlPackage.DOCUMENT_ROOT__DATA_MIN_CARDINALITY:
			case OwlPackage.DOCUMENT_ROOT__DATA_ONE_OF:
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY:
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_ASSERTION:
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_DOMAIN:
			case OwlPackage.DOCUMENT_ROOT__DATA_PROPERTY_RANGE:
			case OwlPackage.DOCUMENT_ROOT__DATA_SOME_VALUES_FROM:
			case OwlPackage.DOCUMENT_ROOT__DATATYPE:
			case OwlPackage.DOCUMENT_ROOT__DATATYPE_DEFINITION:
			case OwlPackage.DOCUMENT_ROOT__DATATYPE_RESTRICTION:
			case OwlPackage.DOCUMENT_ROOT__DATA_UNION_OF:
			case OwlPackage.DOCUMENT_ROOT__DECLARATION:
			case OwlPackage.DOCUMENT_ROOT__DIFFERENT_INDIVIDUALS:
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_CLASSES:
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_DATA_PROPERTIES:
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_OBJECT_PROPERTIES:
			case OwlPackage.DOCUMENT_ROOT__DISJOINT_UNION:
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_CLASSES:
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_DATA_PROPERTIES:
			case OwlPackage.DOCUMENT_ROOT__EQUIVALENT_OBJECT_PROPERTIES:
			case OwlPackage.DOCUMENT_ROOT__FUNCTIONAL_DATA_PROPERTY:
			case OwlPackage.DOCUMENT_ROOT__FUNCTIONAL_OBJECT_PROPERTY:
			case OwlPackage.DOCUMENT_ROOT__HAS_KEY:
			case OwlPackage.DOCUMENT_ROOT__IMPORT:
			case OwlPackage.DOCUMENT_ROOT__INVERSE_FUNCTIONAL_OBJECT_PROPERTY:
			case OwlPackage.DOCUMENT_ROOT__INVERSE_OBJECT_PROPERTIES:
			case OwlPackage.DOCUMENT_ROOT__IRI:
			case OwlPackage.DOCUMENT_ROOT__IRREFLEXIVE_OBJECT_PROPERTY:
			case OwlPackage.DOCUMENT_ROOT__LITERAL:
			case OwlPackage.DOCUMENT_ROOT__NAMED_INDIVIDUAL:
			case OwlPackage.DOCUMENT_ROOT__NEGATIVE_DATA_PROPERTY_ASSERTION:
			case OwlPackage.DOCUMENT_ROOT__NEGATIVE_OBJECT_PROPERTY_ASSERTION:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_ALL_VALUES_FROM:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_COMPLEMENT_OF:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_EXACT_CARDINALITY:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_HAS_SELF:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_HAS_VALUE:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_INTERSECTION_OF:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_INVERSE_OF:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_MAX_CARDINALITY:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_MIN_CARDINALITY:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_ONE_OF:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_ASSERTION:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_DOMAIN:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_PROPERTY_RANGE:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_SOME_VALUES_FROM:
			case OwlPackage.DOCUMENT_ROOT__OBJECT_UNION_OF:
			case OwlPackage.DOCUMENT_ROOT__ONTOLOGY:
			case OwlPackage.DOCUMENT_ROOT__PREFIX:
			case OwlPackage.DOCUMENT_ROOT__REFLEXIVE_OBJECT_PROPERTY:
			case OwlPackage.DOCUMENT_ROOT__SAME_INDIVIDUAL:
			case OwlPackage.DOCUMENT_ROOT__SUB_ANNOTATION_PROPERTY_OF:
			case OwlPackage.DOCUMENT_ROOT__SUB_CLASS_OF:
			case OwlPackage.DOCUMENT_ROOT__SUB_DATA_PROPERTY_OF:
			case OwlPackage.DOCUMENT_ROOT__SUB_OBJECT_PROPERTY_OF:
			case OwlPackage.DOCUMENT_ROOT__SYMMETRIC_OBJECT_PROPERTY:
			case OwlPackage.DOCUMENT_ROOT__TRANSITIVE_OBJECT_PROPERTY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_AbbreviatedIRI(),
				 OwlFactory.eINSTANCE.createAbbreviatedIRI1()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_Annotation(),
				 OwlFactory.eINSTANCE.createAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationAssertion(),
				 OwlFactory.eINSTANCE.createAnnotationAssertion()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationProperty(),
				 OwlFactory.eINSTANCE.createAnnotationProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationPropertyDomain(),
				 OwlFactory.eINSTANCE.createAnnotationPropertyDomain()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_AnnotationPropertyRange(),
				 OwlFactory.eINSTANCE.createAnnotationPropertyRange()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_AnonymousIndividual(),
				 OwlFactory.eINSTANCE.createAnonymousIndividual()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_AsymmetricObjectProperty(),
				 OwlFactory.eINSTANCE.createAsymmetricObjectProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_Class(),
				 OwlFactory.eINSTANCE.createClass()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ClassAssertion(),
				 OwlFactory.eINSTANCE.createClassAssertion()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataAllValuesFrom(),
				 OwlFactory.eINSTANCE.createDataAllValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataComplementOf(),
				 OwlFactory.eINSTANCE.createDataComplementOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataExactCardinality(),
				 OwlFactory.eINSTANCE.createDataExactCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataHasValue(),
				 OwlFactory.eINSTANCE.createDataHasValue()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataIntersectionOf(),
				 OwlFactory.eINSTANCE.createDataIntersectionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataMaxCardinality(),
				 OwlFactory.eINSTANCE.createDataMaxCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataMinCardinality(),
				 OwlFactory.eINSTANCE.createDataMinCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataOneOf(),
				 OwlFactory.eINSTANCE.createDataOneOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataProperty(),
				 OwlFactory.eINSTANCE.createDataProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyAssertion(),
				 OwlFactory.eINSTANCE.createDataPropertyAssertion()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyDomain(),
				 OwlFactory.eINSTANCE.createDataPropertyDomain()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataPropertyRange(),
				 OwlFactory.eINSTANCE.createDataPropertyRange()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataSomeValuesFrom(),
				 OwlFactory.eINSTANCE.createDataSomeValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_Datatype(),
				 OwlFactory.eINSTANCE.createDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DatatypeDefinition(),
				 OwlFactory.eINSTANCE.createDatatypeDefinition()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DatatypeRestriction(),
				 OwlFactory.eINSTANCE.createDatatypeRestriction()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DataUnionOf(),
				 OwlFactory.eINSTANCE.createDataUnionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_Declaration(),
				 OwlFactory.eINSTANCE.createDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DifferentIndividuals(),
				 OwlFactory.eINSTANCE.createDifferentIndividuals()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DisjointClasses(),
				 OwlFactory.eINSTANCE.createDisjointClasses()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DisjointDataProperties(),
				 OwlFactory.eINSTANCE.createDisjointDataProperties()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DisjointObjectProperties(),
				 OwlFactory.eINSTANCE.createDisjointObjectProperties()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_DisjointUnion(),
				 OwlFactory.eINSTANCE.createDisjointUnion()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentClasses(),
				 OwlFactory.eINSTANCE.createEquivalentClasses()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentDataProperties(),
				 OwlFactory.eINSTANCE.createEquivalentDataProperties()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_EquivalentObjectProperties(),
				 OwlFactory.eINSTANCE.createEquivalentObjectProperties()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_FunctionalDataProperty(),
				 OwlFactory.eINSTANCE.createFunctionalDataProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_FunctionalObjectProperty(),
				 OwlFactory.eINSTANCE.createFunctionalObjectProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_HasKey(),
				 OwlFactory.eINSTANCE.createHasKey()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_Import(),
				 OwlFactory.eINSTANCE.createImport()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_InverseFunctionalObjectProperty(),
				 OwlFactory.eINSTANCE.createInverseFunctionalObjectProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_InverseObjectProperties(),
				 OwlFactory.eINSTANCE.createInverseObjectProperties()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_IRI(),
				 OwlFactory.eINSTANCE.createIRI()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_IrreflexiveObjectProperty(),
				 OwlFactory.eINSTANCE.createIrreflexiveObjectProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_Literal(),
				 OwlFactory.eINSTANCE.createLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_NamedIndividual(),
				 OwlFactory.eINSTANCE.createNamedIndividual()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_NegativeDataPropertyAssertion(),
				 OwlFactory.eINSTANCE.createNegativeDataPropertyAssertion()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_NegativeObjectPropertyAssertion(),
				 OwlFactory.eINSTANCE.createNegativeObjectPropertyAssertion()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectAllValuesFrom(),
				 OwlFactory.eINSTANCE.createObjectAllValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectComplementOf(),
				 OwlFactory.eINSTANCE.createObjectComplementOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectExactCardinality(),
				 OwlFactory.eINSTANCE.createObjectExactCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectHasSelf(),
				 OwlFactory.eINSTANCE.createObjectHasSelf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectHasValue(),
				 OwlFactory.eINSTANCE.createObjectHasValue()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectIntersectionOf(),
				 OwlFactory.eINSTANCE.createObjectIntersectionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectInverseOf(),
				 OwlFactory.eINSTANCE.createObjectInverseOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectMaxCardinality(),
				 OwlFactory.eINSTANCE.createObjectMaxCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectMinCardinality(),
				 OwlFactory.eINSTANCE.createObjectMinCardinality()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectOneOf(),
				 OwlFactory.eINSTANCE.createObjectOneOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectProperty(),
				 OwlFactory.eINSTANCE.createObjectProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyAssertion(),
				 OwlFactory.eINSTANCE.createObjectPropertyAssertion()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyDomain(),
				 OwlFactory.eINSTANCE.createObjectPropertyDomain()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectPropertyRange(),
				 OwlFactory.eINSTANCE.createObjectPropertyRange()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectSomeValuesFrom(),
				 OwlFactory.eINSTANCE.createObjectSomeValuesFrom()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ObjectUnionOf(),
				 OwlFactory.eINSTANCE.createObjectUnionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_Ontology(),
				 OwlFactory.eINSTANCE.createOntology()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_Prefix(),
				 OwlFactory.eINSTANCE.createPrefix()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_ReflexiveObjectProperty(),
				 OwlFactory.eINSTANCE.createReflexiveObjectProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_SameIndividual(),
				 OwlFactory.eINSTANCE.createSameIndividual()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_SubAnnotationPropertyOf(),
				 OwlFactory.eINSTANCE.createSubAnnotationPropertyOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_SubClassOf(),
				 OwlFactory.eINSTANCE.createSubClassOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_SubDataPropertyOf(),
				 OwlFactory.eINSTANCE.createSubDataPropertyOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_SubObjectPropertyOf(),
				 OwlFactory.eINSTANCE.createSubObjectPropertyOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_SymmetricObjectProperty(),
				 OwlFactory.eINSTANCE.createSymmetricObjectProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDocumentRoot_TransitiveObjectProperty(),
				 OwlFactory.eINSTANCE.createTransitiveObjectProperty()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return OwlEditPlugin.INSTANCE;
	}

}
