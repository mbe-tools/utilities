/**
 */
package org.w3._2002._07.owl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.w3._2002._07.owl.DisjointClasses;
import org.w3._2002._07.owl.OwlFactory;
import org.w3._2002._07.owl.OwlPackage;

/**
 * This is the item provider adapter for a {@link org.w3._2002._07.owl.DisjointClasses} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DisjointClassesItemProvider extends ClassAxiomItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisjointClassesItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns DisjointClasses.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DisjointClasses"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DisjointClasses)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_DisjointClasses_type") :
			getString("_UI_DisjointClasses_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DisjointClasses.class)) {
			case OwlPackage.DISJOINT_CLASSES__CLASS_EXPRESSION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_Class(),
					 OwlFactory.eINSTANCE.createClass())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_ObjectIntersectionOf(),
					 OwlFactory.eINSTANCE.createObjectIntersectionOf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_ObjectUnionOf(),
					 OwlFactory.eINSTANCE.createObjectUnionOf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_ObjectComplementOf(),
					 OwlFactory.eINSTANCE.createObjectComplementOf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_ObjectOneOf(),
					 OwlFactory.eINSTANCE.createObjectOneOf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_ObjectSomeValuesFrom(),
					 OwlFactory.eINSTANCE.createObjectSomeValuesFrom())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_ObjectAllValuesFrom(),
					 OwlFactory.eINSTANCE.createObjectAllValuesFrom())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_ObjectHasValue(),
					 OwlFactory.eINSTANCE.createObjectHasValue())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_ObjectHasSelf(),
					 OwlFactory.eINSTANCE.createObjectHasSelf())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_ObjectMinCardinality(),
					 OwlFactory.eINSTANCE.createObjectMinCardinality())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_ObjectMaxCardinality(),
					 OwlFactory.eINSTANCE.createObjectMaxCardinality())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_ObjectExactCardinality(),
					 OwlFactory.eINSTANCE.createObjectExactCardinality())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_DataSomeValuesFrom(),
					 OwlFactory.eINSTANCE.createDataSomeValuesFrom())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_DataAllValuesFrom(),
					 OwlFactory.eINSTANCE.createDataAllValuesFrom())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_DataHasValue(),
					 OwlFactory.eINSTANCE.createDataHasValue())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_DataMinCardinality(),
					 OwlFactory.eINSTANCE.createDataMinCardinality())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_DataMaxCardinality(),
					 OwlFactory.eINSTANCE.createDataMaxCardinality())));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDisjointClasses_ClassExpression(),
				 FeatureMapUtil.createEntry
					(OwlPackage.eINSTANCE.getDisjointClasses_DataExactCardinality(),
					 OwlFactory.eINSTANCE.createDataExactCardinality())));
	}

}
