/**
 */
package org.w3._2002._07.owl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.w3._2002._07.owl.NegativeDataPropertyAssertion;
import org.w3._2002._07.owl.OwlFactory;
import org.w3._2002._07.owl.OwlPackage;

/**
 * This is the item provider adapter for a {@link org.w3._2002._07.owl.NegativeDataPropertyAssertion} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class NegativeDataPropertyAssertionItemProvider extends AssertionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NegativeDataPropertyAssertionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OwlPackage.eINSTANCE.getNegativeDataPropertyAssertion_DataProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getNegativeDataPropertyAssertion_NamedIndividual());
			childrenFeatures.add(OwlPackage.eINSTANCE.getNegativeDataPropertyAssertion_AnonymousIndividual());
			childrenFeatures.add(OwlPackage.eINSTANCE.getNegativeDataPropertyAssertion_Literal());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns NegativeDataPropertyAssertion.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/NegativeDataPropertyAssertion"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((NegativeDataPropertyAssertion)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_NegativeDataPropertyAssertion_type") :
			getString("_UI_NegativeDataPropertyAssertion_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(NegativeDataPropertyAssertion.class)) {
			case OwlPackage.NEGATIVE_DATA_PROPERTY_ASSERTION__DATA_PROPERTY:
			case OwlPackage.NEGATIVE_DATA_PROPERTY_ASSERTION__NAMED_INDIVIDUAL:
			case OwlPackage.NEGATIVE_DATA_PROPERTY_ASSERTION__ANONYMOUS_INDIVIDUAL:
			case OwlPackage.NEGATIVE_DATA_PROPERTY_ASSERTION__LITERAL:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getNegativeDataPropertyAssertion_DataProperty(),
				 OwlFactory.eINSTANCE.createDataProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getNegativeDataPropertyAssertion_NamedIndividual(),
				 OwlFactory.eINSTANCE.createNamedIndividual()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getNegativeDataPropertyAssertion_AnonymousIndividual(),
				 OwlFactory.eINSTANCE.createAnonymousIndividual()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getNegativeDataPropertyAssertion_Literal(),
				 OwlFactory.eINSTANCE.createLiteral()));
	}

}
