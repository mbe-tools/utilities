/**
 */
package org.w3._2002._07.owl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.w3._2002._07.owl.DataExactCardinality;
import org.w3._2002._07.owl.OwlFactory;
import org.w3._2002._07.owl.OwlPackage;

/**
 * This is the item provider adapter for a {@link org.w3._2002._07.owl.DataExactCardinality} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DataExactCardinalityItemProvider extends ClassExpressionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataExactCardinalityItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCardinalityPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Cardinality feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCardinalityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DataExactCardinality_cardinality_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DataExactCardinality_cardinality_feature", "_UI_DataExactCardinality_type"),
				 OwlPackage.eINSTANCE.getDataExactCardinality_Cardinality(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataExactCardinality_DataProperty());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataExactCardinality_Datatype());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataExactCardinality_DataIntersectionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataExactCardinality_DataUnionOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataExactCardinality_DataComplementOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataExactCardinality_DataOneOf());
			childrenFeatures.add(OwlPackage.eINSTANCE.getDataExactCardinality_DatatypeRestriction());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns DataExactCardinality.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DataExactCardinality"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DataExactCardinality)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_DataExactCardinality_type") :
			getString("_UI_DataExactCardinality_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DataExactCardinality.class)) {
			case OwlPackage.DATA_EXACT_CARDINALITY__CARDINALITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case OwlPackage.DATA_EXACT_CARDINALITY__DATA_PROPERTY:
			case OwlPackage.DATA_EXACT_CARDINALITY__DATATYPE:
			case OwlPackage.DATA_EXACT_CARDINALITY__DATA_INTERSECTION_OF:
			case OwlPackage.DATA_EXACT_CARDINALITY__DATA_UNION_OF:
			case OwlPackage.DATA_EXACT_CARDINALITY__DATA_COMPLEMENT_OF:
			case OwlPackage.DATA_EXACT_CARDINALITY__DATA_ONE_OF:
			case OwlPackage.DATA_EXACT_CARDINALITY__DATATYPE_RESTRICTION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataExactCardinality_DataProperty(),
				 OwlFactory.eINSTANCE.createDataProperty()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataExactCardinality_Datatype(),
				 OwlFactory.eINSTANCE.createDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataExactCardinality_DataIntersectionOf(),
				 OwlFactory.eINSTANCE.createDataIntersectionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataExactCardinality_DataUnionOf(),
				 OwlFactory.eINSTANCE.createDataUnionOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataExactCardinality_DataComplementOf(),
				 OwlFactory.eINSTANCE.createDataComplementOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataExactCardinality_DataOneOf(),
				 OwlFactory.eINSTANCE.createDataOneOf()));

		newChildDescriptors.add
			(createChildParameter
				(OwlPackage.eINSTANCE.getDataExactCardinality_DatatypeRestriction(),
				 OwlFactory.eINSTANCE.createDatatypeRestriction()));
	}

}
