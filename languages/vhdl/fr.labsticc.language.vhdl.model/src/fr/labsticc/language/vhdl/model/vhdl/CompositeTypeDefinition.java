/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Type Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getCompositeTypeDefinition()
 * @model
 * @generated
 */
public interface CompositeTypeDefinition extends TypeDefinition {
} // CompositeTypeDefinition
