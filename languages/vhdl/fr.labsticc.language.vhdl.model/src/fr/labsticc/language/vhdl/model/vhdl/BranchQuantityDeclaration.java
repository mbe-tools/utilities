/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Branch Quantity Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getAcross <em>Across</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getThrough <em>Through</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getLeft <em>Left</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getRight <em>Right</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBranchQuantityDeclaration()
 * @model
 * @generated
 */
public interface BranchQuantityDeclaration extends QuantityDeclaration {
	/**
	 * Returns the value of the '<em><b>Across</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Across</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Across</em>' containment reference.
	 * @see #setAcross(QuantityAspect)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBranchQuantityDeclaration_Across()
	 * @model containment="true"
	 * @generated
	 */
	QuantityAspect getAcross();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getAcross <em>Across</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Across</em>' containment reference.
	 * @see #getAcross()
	 * @generated
	 */
	void setAcross(QuantityAspect value);

	/**
	 * Returns the value of the '<em><b>Through</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Through</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Through</em>' containment reference.
	 * @see #setThrough(QuantityAspect)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBranchQuantityDeclaration_Through()
	 * @model containment="true"
	 * @generated
	 */
	QuantityAspect getThrough();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getThrough <em>Through</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Through</em>' containment reference.
	 * @see #getThrough()
	 * @generated
	 */
	void setThrough(QuantityAspect value);

	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBranchQuantityDeclaration_Left()
	 * @model containment="true"
	 * @generated
	 */
	Expression getLeft();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(Expression value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBranchQuantityDeclaration_Right()
	 * @model containment="true"
	 * @generated
	 */
	Expression getRight();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(Expression value);

} // BranchQuantityDeclaration
