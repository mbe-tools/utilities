/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Nature Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.RecordNatureDefinition#getRecord <em>Record</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getRecordNatureDefinition()
 * @model
 * @generated
 */
public interface RecordNatureDefinition extends CompositeNatureDefinition {
	/**
	 * Returns the value of the '<em><b>Record</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record</em>' containment reference list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getRecordNatureDefinition_Record()
	 * @model containment="true"
	 * @generated
	 */
	EList<RecordNatureElement> getRecord();

} // RecordNatureDefinition
