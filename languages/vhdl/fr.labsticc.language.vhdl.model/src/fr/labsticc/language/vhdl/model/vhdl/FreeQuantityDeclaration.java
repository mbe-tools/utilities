/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Free Quantity Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration#getType <em>Type</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration#getQuantity <em>Quantity</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getFreeQuantityDeclaration()
 * @model
 * @generated
 */
public interface FreeQuantityDeclaration extends QuantityDeclaration {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getFreeQuantityDeclaration_Type()
	 * @model containment="true"
	 * @generated
	 */
	Expression getType();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Expression value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference.
	 * @see #setQuantity(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getFreeQuantityDeclaration_Quantity()
	 * @model containment="true"
	 * @generated
	 */
	Expression getQuantity();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration#getQuantity <em>Quantity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' containment reference.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(Expression value);

} // FreeQuantityDeclaration
