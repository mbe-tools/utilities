/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scalar Nature Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition#getName <em>Name</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition#getAcross <em>Across</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition#getThrough <em>Through</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getScalarNatureDefinition()
 * @model
 * @generated
 */
public interface ScalarNatureDefinition extends NatureDefinition {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' containment reference.
	 * @see #setName(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getScalarNatureDefinition_Name()
	 * @model containment="true"
	 * @generated
	 */
	Expression getName();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition#getName <em>Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' containment reference.
	 * @see #getName()
	 * @generated
	 */
	void setName(Expression value);

	/**
	 * Returns the value of the '<em><b>Across</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Across</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Across</em>' containment reference.
	 * @see #setAcross(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getScalarNatureDefinition_Across()
	 * @model containment="true"
	 * @generated
	 */
	Expression getAcross();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition#getAcross <em>Across</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Across</em>' containment reference.
	 * @see #getAcross()
	 * @generated
	 */
	void setAcross(Expression value);

	/**
	 * Returns the value of the '<em><b>Through</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Through</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Through</em>' containment reference.
	 * @see #setThrough(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getScalarNatureDefinition_Through()
	 * @model containment="true"
	 * @generated
	 */
	Expression getThrough();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition#getThrough <em>Through</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Through</em>' containment reference.
	 * @see #getThrough()
	 * @generated
	 */
	void setThrough(Expression value);

} // ScalarNatureDefinition
