/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Generics;
import fr.labsticc.language.vhdl.model.vhdl.Ports;
import fr.labsticc.language.vhdl.model.vhdl.Statement;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entity Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.EntityDeclarationImpl#getGeneric <em>Generic</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.EntityDeclarationImpl#getPort <em>Port</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.EntityDeclarationImpl#getStatement <em>Statement</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EntityDeclarationImpl extends LibraryUnitImpl implements EntityDeclaration {
	/**
	 * The cached value of the '{@link #getGeneric() <em>Generic</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneric()
	 * @generated
	 * @ordered
	 */
	protected Generics generic;

	/**
	 * The cached value of the '{@link #getPort() <em>Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort()
	 * @generated
	 * @ordered
	 */
	protected Ports port;

	/**
	 * The cached value of the '{@link #getStatement() <em>Statement</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatement()
	 * @generated
	 * @ordered
	 */
	protected EList<Statement> statement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntityDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VhdlPackage.eINSTANCE.getEntityDeclaration();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Generics getGeneric() {
		return generic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGeneric(Generics newGeneric, NotificationChain msgs) {
		Generics oldGeneric = generic;
		generic = newGeneric;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.ENTITY_DECLARATION__GENERIC, oldGeneric, newGeneric);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGeneric(Generics newGeneric) {
		if (newGeneric != generic) {
			NotificationChain msgs = null;
			if (generic != null)
				msgs = ((InternalEObject)generic).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.ENTITY_DECLARATION__GENERIC, null, msgs);
			if (newGeneric != null)
				msgs = ((InternalEObject)newGeneric).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.ENTITY_DECLARATION__GENERIC, null, msgs);
			msgs = basicSetGeneric(newGeneric, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.ENTITY_DECLARATION__GENERIC, newGeneric, newGeneric));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ports getPort() {
		return port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort(Ports newPort, NotificationChain msgs) {
		Ports oldPort = port;
		port = newPort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.ENTITY_DECLARATION__PORT, oldPort, newPort);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort(Ports newPort) {
		if (newPort != port) {
			NotificationChain msgs = null;
			if (port != null)
				msgs = ((InternalEObject)port).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.ENTITY_DECLARATION__PORT, null, msgs);
			if (newPort != null)
				msgs = ((InternalEObject)newPort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.ENTITY_DECLARATION__PORT, null, msgs);
			msgs = basicSetPort(newPort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.ENTITY_DECLARATION__PORT, newPort, newPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Statement> getStatement() {
		if (statement == null) {
			statement = new EObjectContainmentEList<Statement>(Statement.class, this, VhdlPackage.ENTITY_DECLARATION__STATEMENT);
		}
		return statement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VhdlPackage.ENTITY_DECLARATION__GENERIC:
				return basicSetGeneric(null, msgs);
			case VhdlPackage.ENTITY_DECLARATION__PORT:
				return basicSetPort(null, msgs);
			case VhdlPackage.ENTITY_DECLARATION__STATEMENT:
				return ((InternalEList<?>)getStatement()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VhdlPackage.ENTITY_DECLARATION__GENERIC:
				return getGeneric();
			case VhdlPackage.ENTITY_DECLARATION__PORT:
				return getPort();
			case VhdlPackage.ENTITY_DECLARATION__STATEMENT:
				return getStatement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VhdlPackage.ENTITY_DECLARATION__GENERIC:
				setGeneric((Generics)newValue);
				return;
			case VhdlPackage.ENTITY_DECLARATION__PORT:
				setPort((Ports)newValue);
				return;
			case VhdlPackage.ENTITY_DECLARATION__STATEMENT:
				getStatement().clear();
				getStatement().addAll((Collection<? extends Statement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VhdlPackage.ENTITY_DECLARATION__GENERIC:
				setGeneric((Generics)null);
				return;
			case VhdlPackage.ENTITY_DECLARATION__PORT:
				setPort((Ports)null);
				return;
			case VhdlPackage.ENTITY_DECLARATION__STATEMENT:
				getStatement().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VhdlPackage.ENTITY_DECLARATION__GENERIC:
				return generic != null;
			case VhdlPackage.ENTITY_DECLARATION__PORT:
				return port != null;
			case VhdlPackage.ENTITY_DECLARATION__STATEMENT:
				return statement != null && !statement.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EntityDeclarationImpl
