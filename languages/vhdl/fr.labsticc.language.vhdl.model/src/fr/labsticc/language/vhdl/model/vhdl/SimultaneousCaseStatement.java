/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simultaneous Case Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement#getCase <em>Case</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement#getWhen <em>When</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSimultaneousCaseStatement()
 * @model
 * @generated
 */
public interface SimultaneousCaseStatement extends Statement {
	/**
	 * Returns the value of the '<em><b>Case</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case</em>' containment reference.
	 * @see #setCase(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSimultaneousCaseStatement_Case()
	 * @model containment="true"
	 * @generated
	 */
	Expression getCase();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement#getCase <em>Case</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Case</em>' containment reference.
	 * @see #getCase()
	 * @generated
	 */
	void setCase(Expression value);

	/**
	 * Returns the value of the '<em><b>When</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>When</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>When</em>' containment reference list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSimultaneousCaseStatement_When()
	 * @model containment="true"
	 * @generated
	 */
	EList<SimultaneousAlternative> getWhen();

} // SimultaneousCaseStatement
