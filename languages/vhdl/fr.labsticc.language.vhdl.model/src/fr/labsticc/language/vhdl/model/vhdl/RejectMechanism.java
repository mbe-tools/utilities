/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reject Mechanism</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.RejectMechanism#getReject <em>Reject</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getRejectMechanism()
 * @model
 * @generated
 */
public interface RejectMechanism extends DelayMechanism {
	/**
	 * Returns the value of the '<em><b>Reject</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reject</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reject</em>' containment reference.
	 * @see #setReject(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getRejectMechanism_Reject()
	 * @model containment="true"
	 * @generated
	 */
	Expression getReject();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.RejectMechanism#getReject <em>Reject</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reject</em>' containment reference.
	 * @see #getReject()
	 * @generated
	 */
	void setReject(Expression value);

} // RejectMechanism
