/**
 */
package fr.labsticc.language.vhdl.model.vhdl.util;

import fr.labsticc.language.vhdl.model.vhdl.AccessTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.AddingExpression;
import fr.labsticc.language.vhdl.model.vhdl.Aggregate;
import fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.All;
import fr.labsticc.language.vhdl.model.vhdl.Allocator;
import fr.labsticc.language.vhdl.model.vhdl.Architecture;
import fr.labsticc.language.vhdl.model.vhdl.ArrayNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ArrayTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.AssertionStatement;
import fr.labsticc.language.vhdl.model.vhdl.Association;
import fr.labsticc.language.vhdl.model.vhdl.Attribute;
import fr.labsticc.language.vhdl.model.vhdl.AttributeDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification;
import fr.labsticc.language.vhdl.model.vhdl.BitString;
import fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration;
import fr.labsticc.language.vhdl.model.vhdl.BlockStatement;
import fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.BreakElement;
import fr.labsticc.language.vhdl.model.vhdl.BreakStatement;
import fr.labsticc.language.vhdl.model.vhdl.CaseAlternative;
import fr.labsticc.language.vhdl.model.vhdl.CaseStatement;
import fr.labsticc.language.vhdl.model.vhdl.Choice;
import fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration;
import fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.ComponentInstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.CompositeNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.CompositeTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationItem;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification;
import fr.labsticc.language.vhdl.model.vhdl.ConstantDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.ConstrainedArrayDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ConstrainedNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.Constraint;
import fr.labsticc.language.vhdl.model.vhdl.Declaration;
import fr.labsticc.language.vhdl.model.vhdl.DelayMechanism;
import fr.labsticc.language.vhdl.model.vhdl.DesignUnit;
import fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification;
import fr.labsticc.language.vhdl.model.vhdl.DiscreteRange;
import fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.EntityInstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.EnumerationTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ExitStatement;
import fr.labsticc.language.vhdl.model.vhdl.Expression;
import fr.labsticc.language.vhdl.model.vhdl.FileDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.FileTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ForGenerationScheme;
import fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme;
import fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.GenerateStatement;
import fr.labsticc.language.vhdl.model.vhdl.GenerationScheme;
import fr.labsticc.language.vhdl.model.vhdl.GenericMaps;
import fr.labsticc.language.vhdl.model.vhdl.Generics;
import fr.labsticc.language.vhdl.model.vhdl.GroupDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.GroupTemplateDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.IdentifiedElement;
import fr.labsticc.language.vhdl.model.vhdl.Identifier;
import fr.labsticc.language.vhdl.model.vhdl.IfGenerationScheme;
import fr.labsticc.language.vhdl.model.vhdl.IfStatement;
import fr.labsticc.language.vhdl.model.vhdl.IfStatementTest;
import fr.labsticc.language.vhdl.model.vhdl.Indication;
import fr.labsticc.language.vhdl.model.vhdl.InstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.IterationScheme;
import fr.labsticc.language.vhdl.model.vhdl.LibraryUnit;
import fr.labsticc.language.vhdl.model.vhdl.LimitSpecification;
import fr.labsticc.language.vhdl.model.vhdl.LogicalExpression;
import fr.labsticc.language.vhdl.model.vhdl.LoopStatement;
import fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression;
import fr.labsticc.language.vhdl.model.vhdl.Name;
import fr.labsticc.language.vhdl.model.vhdl.NameElement;
import fr.labsticc.language.vhdl.model.vhdl.NameList;
import fr.labsticc.language.vhdl.model.vhdl.NamedElement;
import fr.labsticc.language.vhdl.model.vhdl.NatureDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.NatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.NextStatement;
import fr.labsticc.language.vhdl.model.vhdl.Noise;
import fr.labsticc.language.vhdl.model.vhdl.Null;
import fr.labsticc.language.vhdl.model.vhdl.NullStatement;
import fr.labsticc.language.vhdl.model.vhdl.Open;
import fr.labsticc.language.vhdl.model.vhdl.Others;
import fr.labsticc.language.vhdl.model.vhdl.PackageBody;
import fr.labsticc.language.vhdl.model.vhdl.PackageDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary;
import fr.labsticc.language.vhdl.model.vhdl.PortMaps;
import fr.labsticc.language.vhdl.model.vhdl.Ports;
import fr.labsticc.language.vhdl.model.vhdl.PowerExpression;
import fr.labsticc.language.vhdl.model.vhdl.ProcedureCallStatement;
import fr.labsticc.language.vhdl.model.vhdl.ProcessStatement;
import fr.labsticc.language.vhdl.model.vhdl.QuantityAspect;
import fr.labsticc.language.vhdl.model.vhdl.QuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Range;
import fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.RecordNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement;
import fr.labsticc.language.vhdl.model.vhdl.RecordTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.RejectMechanism;
import fr.labsticc.language.vhdl.model.vhdl.RelationalExpression;
import fr.labsticc.language.vhdl.model.vhdl.ReportStatement;
import fr.labsticc.language.vhdl.model.vhdl.ReturnStatement;
import fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.ShiftExpression;
import fr.labsticc.language.vhdl.model.vhdl.SignExpression;
import fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Signature;
import fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousProceduralStatement;
import fr.labsticc.language.vhdl.model.vhdl.SourceAspect;
import fr.labsticc.language.vhdl.model.vhdl.SourceQuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Spectrum;
import fr.labsticc.language.vhdl.model.vhdl.Statement;
import fr.labsticc.language.vhdl.model.vhdl.SubnatureDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification;
import fr.labsticc.language.vhdl.model.vhdl.SubtypeDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.TerminalDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Transport;
import fr.labsticc.language.vhdl.model.vhdl.TypeDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.TypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.UnaryExpression;
import fr.labsticc.language.vhdl.model.vhdl.UnconstrainedArrayDefinition;
import fr.labsticc.language.vhdl.model.vhdl.UnconstrainedNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.UnitValue;
import fr.labsticc.language.vhdl.model.vhdl.UseClause;
import fr.labsticc.language.vhdl.model.vhdl.Value;
import fr.labsticc.language.vhdl.model.vhdl.VariableAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.VhdlFile;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;
import fr.labsticc.language.vhdl.model.vhdl.WaitStatement;
import fr.labsticc.language.vhdl.model.vhdl.Waveform;
import fr.labsticc.language.vhdl.model.vhdl.WhileIterationScheme;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage
 * @generated
 */
public class VhdlSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static VhdlPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VhdlSwitch() {
		if (modelPackage == null) {
			modelPackage = VhdlPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case VhdlPackage.VHDL_FILE: {
				VhdlFile vhdlFile = (VhdlFile)theEObject;
				T result = caseVhdlFile(vhdlFile);
				if (result == null) result = caseIdentifiedElement(vhdlFile);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.DESIGN_UNIT: {
				DesignUnit designUnit = (DesignUnit)theEObject;
				T result = caseDesignUnit(designUnit);
				if (result == null) result = caseIdentifiedElement(designUnit);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.USE_CLAUSE: {
				UseClause useClause = (UseClause)theEObject;
				T result = caseUseClause(useClause);
				if (result == null) result = caseDeclaration(useClause);
				if (result == null) result = caseNamedElement(useClause);
				if (result == null) result = caseIdentifiedElement(useClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.LIBRARY_UNIT: {
				LibraryUnit libraryUnit = (LibraryUnit)theEObject;
				T result = caseLibraryUnit(libraryUnit);
				if (result == null) result = caseNamedElement(libraryUnit);
				if (result == null) result = caseIdentifiedElement(libraryUnit);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ACCESS_TYPE_DEFINITION: {
				AccessTypeDefinition accessTypeDefinition = (AccessTypeDefinition)theEObject;
				T result = caseAccessTypeDefinition(accessTypeDefinition);
				if (result == null) result = caseTypeDefinition(accessTypeDefinition);
				if (result == null) result = caseIdentifiedElement(accessTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.NAME_ELEMENT: {
				NameElement nameElement = (NameElement)theEObject;
				T result = caseNameElement(nameElement);
				if (result == null) result = caseExpression(nameElement);
				if (result == null) result = caseNamedElement(nameElement);
				if (result == null) result = caseChoice(nameElement);
				if (result == null) result = caseDiscreteRange(nameElement);
				if (result == null) result = caseIdentifiedElement(nameElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.DECLARATION: {
				Declaration declaration = (Declaration)theEObject;
				T result = caseDeclaration(declaration);
				if (result == null) result = caseNamedElement(declaration);
				if (result == null) result = caseIdentifiedElement(declaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ARCHITECTURE: {
				Architecture architecture = (Architecture)theEObject;
				T result = caseArchitecture(architecture);
				if (result == null) result = caseLibraryUnit(architecture);
				if (result == null) result = caseNamedElement(architecture);
				if (result == null) result = caseIdentifiedElement(architecture);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.STATEMENT: {
				Statement statement = (Statement)theEObject;
				T result = caseStatement(statement);
				if (result == null) result = caseNamedElement(statement);
				if (result == null) result = caseIdentifiedElement(statement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ARRAY_NATURE_DEFINITION: {
				ArrayNatureDefinition arrayNatureDefinition = (ArrayNatureDefinition)theEObject;
				T result = caseArrayNatureDefinition(arrayNatureDefinition);
				if (result == null) result = caseCompositeNatureDefinition(arrayNatureDefinition);
				if (result == null) result = caseNatureDefinition(arrayNatureDefinition);
				if (result == null) result = caseIdentifiedElement(arrayNatureDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ARRAY_TYPE_DEFINITION: {
				ArrayTypeDefinition arrayTypeDefinition = (ArrayTypeDefinition)theEObject;
				T result = caseArrayTypeDefinition(arrayTypeDefinition);
				if (result == null) result = caseCompositeTypeDefinition(arrayTypeDefinition);
				if (result == null) result = caseTypeDefinition(arrayTypeDefinition);
				if (result == null) result = caseIdentifiedElement(arrayTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ASSERTION_STATEMENT: {
				AssertionStatement assertionStatement = (AssertionStatement)theEObject;
				T result = caseAssertionStatement(assertionStatement);
				if (result == null) result = caseStatement(assertionStatement);
				if (result == null) result = caseNamedElement(assertionStatement);
				if (result == null) result = caseIdentifiedElement(assertionStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.EXPRESSION: {
				Expression expression = (Expression)theEObject;
				T result = caseExpression(expression);
				if (result == null) result = caseNamedElement(expression);
				if (result == null) result = caseChoice(expression);
				if (result == null) result = caseDiscreteRange(expression);
				if (result == null) result = caseIdentifiedElement(expression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CHOICE: {
				Choice choice = (Choice)theEObject;
				T result = caseChoice(choice);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.OPEN: {
				Open open = (Open)theEObject;
				T result = caseOpen(open);
				if (result == null) result = caseExpression(open);
				if (result == null) result = caseNamedElement(open);
				if (result == null) result = caseChoice(open);
				if (result == null) result = caseDiscreteRange(open);
				if (result == null) result = caseIdentifiedElement(open);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ATTRIBUTE_DECLARATION: {
				AttributeDeclaration attributeDeclaration = (AttributeDeclaration)theEObject;
				T result = caseAttributeDeclaration(attributeDeclaration);
				if (result == null) result = caseDeclaration(attributeDeclaration);
				if (result == null) result = caseNamedElement(attributeDeclaration);
				if (result == null) result = caseIdentifiedElement(attributeDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ATTRIBUTE_SPECIFICATION: {
				AttributeSpecification attributeSpecification = (AttributeSpecification)theEObject;
				T result = caseAttributeSpecification(attributeSpecification);
				if (result == null) result = caseDeclaration(attributeSpecification);
				if (result == null) result = caseNamedElement(attributeSpecification);
				if (result == null) result = caseIdentifiedElement(attributeSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.NAME_LIST: {
				NameList nameList = (NameList)theEObject;
				T result = caseNameList(nameList);
				if (result == null) result = caseIdentifiedElement(nameList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.BLOCK_CONFIGURATION: {
				BlockConfiguration blockConfiguration = (BlockConfiguration)theEObject;
				T result = caseBlockConfiguration(blockConfiguration);
				if (result == null) result = caseConfigurationItem(blockConfiguration);
				if (result == null) result = caseIdentifiedElement(blockConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.BLOCK_STATEMENT: {
				BlockStatement blockStatement = (BlockStatement)theEObject;
				T result = caseBlockStatement(blockStatement);
				if (result == null) result = caseStatement(blockStatement);
				if (result == null) result = caseNamedElement(blockStatement);
				if (result == null) result = caseIdentifiedElement(blockStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.BREAK_ELEMENT: {
				BreakElement breakElement = (BreakElement)theEObject;
				T result = caseBreakElement(breakElement);
				if (result == null) result = caseIdentifiedElement(breakElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.BREAK_STATEMENT: {
				BreakStatement breakStatement = (BreakStatement)theEObject;
				T result = caseBreakStatement(breakStatement);
				if (result == null) result = caseStatement(breakStatement);
				if (result == null) result = caseNamedElement(breakStatement);
				if (result == null) result = caseIdentifiedElement(breakStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CASE_STATEMENT: {
				CaseStatement caseStatement = (CaseStatement)theEObject;
				T result = caseCaseStatement(caseStatement);
				if (result == null) result = caseStatement(caseStatement);
				if (result == null) result = caseNamedElement(caseStatement);
				if (result == null) result = caseIdentifiedElement(caseStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CASE_ALTERNATIVE: {
				CaseAlternative caseAlternative = (CaseAlternative)theEObject;
				T result = caseCaseAlternative(caseAlternative);
				if (result == null) result = caseIdentifiedElement(caseAlternative);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.COMPONENT_CONFIGURATION: {
				ComponentConfiguration componentConfiguration = (ComponentConfiguration)theEObject;
				T result = caseComponentConfiguration(componentConfiguration);
				if (result == null) result = caseConfigurationItem(componentConfiguration);
				if (result == null) result = caseIdentifiedElement(componentConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.COMPONENT_DECLARATION: {
				ComponentDeclaration componentDeclaration = (ComponentDeclaration)theEObject;
				T result = caseComponentDeclaration(componentDeclaration);
				if (result == null) result = caseDeclaration(componentDeclaration);
				if (result == null) result = caseNamedElement(componentDeclaration);
				if (result == null) result = caseIdentifiedElement(componentDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.INSTANTIATION_STATEMENT: {
				InstantiationStatement instantiationStatement = (InstantiationStatement)theEObject;
				T result = caseInstantiationStatement(instantiationStatement);
				if (result == null) result = caseStatement(instantiationStatement);
				if (result == null) result = caseNamedElement(instantiationStatement);
				if (result == null) result = caseIdentifiedElement(instantiationStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.COMPONENT_INSTANTIATION_STATEMENT: {
				ComponentInstantiationStatement componentInstantiationStatement = (ComponentInstantiationStatement)theEObject;
				T result = caseComponentInstantiationStatement(componentInstantiationStatement);
				if (result == null) result = caseInstantiationStatement(componentInstantiationStatement);
				if (result == null) result = caseStatement(componentInstantiationStatement);
				if (result == null) result = caseNamedElement(componentInstantiationStatement);
				if (result == null) result = caseIdentifiedElement(componentInstantiationStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ENTITY_INSTANTIATION_STATEMENT: {
				EntityInstantiationStatement entityInstantiationStatement = (EntityInstantiationStatement)theEObject;
				T result = caseEntityInstantiationStatement(entityInstantiationStatement);
				if (result == null) result = caseInstantiationStatement(entityInstantiationStatement);
				if (result == null) result = caseStatement(entityInstantiationStatement);
				if (result == null) result = caseNamedElement(entityInstantiationStatement);
				if (result == null) result = caseIdentifiedElement(entityInstantiationStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CONFIGURATION_INSTANTIATION_STATEMENT: {
				ConfigurationInstantiationStatement configurationInstantiationStatement = (ConfigurationInstantiationStatement)theEObject;
				T result = caseConfigurationInstantiationStatement(configurationInstantiationStatement);
				if (result == null) result = caseStatement(configurationInstantiationStatement);
				if (result == null) result = caseNamedElement(configurationInstantiationStatement);
				if (result == null) result = caseIdentifiedElement(configurationInstantiationStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.COMPOSITE_NATURE_DEFINITION: {
				CompositeNatureDefinition compositeNatureDefinition = (CompositeNatureDefinition)theEObject;
				T result = caseCompositeNatureDefinition(compositeNatureDefinition);
				if (result == null) result = caseNatureDefinition(compositeNatureDefinition);
				if (result == null) result = caseIdentifiedElement(compositeNatureDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.COMPOSITE_TYPE_DEFINITION: {
				CompositeTypeDefinition compositeTypeDefinition = (CompositeTypeDefinition)theEObject;
				T result = caseCompositeTypeDefinition(compositeTypeDefinition);
				if (result == null) result = caseTypeDefinition(compositeTypeDefinition);
				if (result == null) result = caseIdentifiedElement(compositeTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT: {
				SequentialSignalAssignmentStatement sequentialSignalAssignmentStatement = (SequentialSignalAssignmentStatement)theEObject;
				T result = caseSequentialSignalAssignmentStatement(sequentialSignalAssignmentStatement);
				if (result == null) result = caseStatement(sequentialSignalAssignmentStatement);
				if (result == null) result = caseNamedElement(sequentialSignalAssignmentStatement);
				if (result == null) result = caseIdentifiedElement(sequentialSignalAssignmentStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT: {
				ConditionalSignalAssignmentStatement conditionalSignalAssignmentStatement = (ConditionalSignalAssignmentStatement)theEObject;
				T result = caseConditionalSignalAssignmentStatement(conditionalSignalAssignmentStatement);
				if (result == null) result = caseStatement(conditionalSignalAssignmentStatement);
				if (result == null) result = caseNamedElement(conditionalSignalAssignmentStatement);
				if (result == null) result = caseIdentifiedElement(conditionalSignalAssignmentStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SELECTED_SIGNAL_ASSIGNMENT_STATEMENT: {
				SelectedSignalAssignmentStatement selectedSignalAssignmentStatement = (SelectedSignalAssignmentStatement)theEObject;
				T result = caseSelectedSignalAssignmentStatement(selectedSignalAssignmentStatement);
				if (result == null) result = caseStatement(selectedSignalAssignmentStatement);
				if (result == null) result = caseNamedElement(selectedSignalAssignmentStatement);
				if (result == null) result = caseIdentifiedElement(selectedSignalAssignmentStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SIMPLE_SIMULTANEOUS_STATEMENT: {
				SimpleSimultaneousStatement simpleSimultaneousStatement = (SimpleSimultaneousStatement)theEObject;
				T result = caseSimpleSimultaneousStatement(simpleSimultaneousStatement);
				if (result == null) result = caseStatement(simpleSimultaneousStatement);
				if (result == null) result = caseNamedElement(simpleSimultaneousStatement);
				if (result == null) result = caseIdentifiedElement(simpleSimultaneousStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CONDITIONAL_WAVEFORM: {
				ConditionalWaveform conditionalWaveform = (ConditionalWaveform)theEObject;
				T result = caseConditionalWaveform(conditionalWaveform);
				if (result == null) result = caseIdentifiedElement(conditionalWaveform);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CONFIGURATION_DECLARATION: {
				ConfigurationDeclaration configurationDeclaration = (ConfigurationDeclaration)theEObject;
				T result = caseConfigurationDeclaration(configurationDeclaration);
				if (result == null) result = caseLibraryUnit(configurationDeclaration);
				if (result == null) result = caseNamedElement(configurationDeclaration);
				if (result == null) result = caseIdentifiedElement(configurationDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CONFIGURATION_ITEM: {
				ConfigurationItem configurationItem = (ConfigurationItem)theEObject;
				T result = caseConfigurationItem(configurationItem);
				if (result == null) result = caseIdentifiedElement(configurationItem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CONFIGURATION_SPECIFICATION: {
				ConfigurationSpecification configurationSpecification = (ConfigurationSpecification)theEObject;
				T result = caseConfigurationSpecification(configurationSpecification);
				if (result == null) result = caseDeclaration(configurationSpecification);
				if (result == null) result = caseNamedElement(configurationSpecification);
				if (result == null) result = caseIdentifiedElement(configurationSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.DELAY_MECHANISM: {
				DelayMechanism delayMechanism = (DelayMechanism)theEObject;
				T result = caseDelayMechanism(delayMechanism);
				if (result == null) result = caseIdentifiedElement(delayMechanism);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.REJECT_MECHANISM: {
				RejectMechanism rejectMechanism = (RejectMechanism)theEObject;
				T result = caseRejectMechanism(rejectMechanism);
				if (result == null) result = caseDelayMechanism(rejectMechanism);
				if (result == null) result = caseIdentifiedElement(rejectMechanism);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.TRANSPORT: {
				Transport transport = (Transport)theEObject;
				T result = caseTransport(transport);
				if (result == null) result = caseDelayMechanism(transport);
				if (result == null) result = caseIdentifiedElement(transport);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.DISCONNECTION_SPECIFICATION: {
				DisconnectionSpecification disconnectionSpecification = (DisconnectionSpecification)theEObject;
				T result = caseDisconnectionSpecification(disconnectionSpecification);
				if (result == null) result = caseDeclaration(disconnectionSpecification);
				if (result == null) result = caseNamedElement(disconnectionSpecification);
				if (result == null) result = caseIdentifiedElement(disconnectionSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ELEMENT_DECLARATION: {
				ElementDeclaration elementDeclaration = (ElementDeclaration)theEObject;
				T result = caseElementDeclaration(elementDeclaration);
				if (result == null) result = caseIdentifiedElement(elementDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ENTITY_DECLARATION: {
				EntityDeclaration entityDeclaration = (EntityDeclaration)theEObject;
				T result = caseEntityDeclaration(entityDeclaration);
				if (result == null) result = caseLibraryUnit(entityDeclaration);
				if (result == null) result = caseNamedElement(entityDeclaration);
				if (result == null) result = caseIdentifiedElement(entityDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ENUMERATION_TYPE_DEFINITION: {
				EnumerationTypeDefinition enumerationTypeDefinition = (EnumerationTypeDefinition)theEObject;
				T result = caseEnumerationTypeDefinition(enumerationTypeDefinition);
				if (result == null) result = caseTypeDefinition(enumerationTypeDefinition);
				if (result == null) result = caseIdentifiedElement(enumerationTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.EXIT_STATEMENT: {
				ExitStatement exitStatement = (ExitStatement)theEObject;
				T result = caseExitStatement(exitStatement);
				if (result == null) result = caseStatement(exitStatement);
				if (result == null) result = caseNamedElement(exitStatement);
				if (result == null) result = caseIdentifiedElement(exitStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.FILE_DECLARATION: {
				FileDeclaration fileDeclaration = (FileDeclaration)theEObject;
				T result = caseFileDeclaration(fileDeclaration);
				if (result == null) result = caseDeclaration(fileDeclaration);
				if (result == null) result = caseNamedElement(fileDeclaration);
				if (result == null) result = caseIdentifiedElement(fileDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.FILE_TYPE_DEFINITION: {
				FileTypeDefinition fileTypeDefinition = (FileTypeDefinition)theEObject;
				T result = caseFileTypeDefinition(fileTypeDefinition);
				if (result == null) result = caseTypeDefinition(fileTypeDefinition);
				if (result == null) result = caseIdentifiedElement(fileTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.GENERATE_STATEMENT: {
				GenerateStatement generateStatement = (GenerateStatement)theEObject;
				T result = caseGenerateStatement(generateStatement);
				if (result == null) result = caseStatement(generateStatement);
				if (result == null) result = caseNamedElement(generateStatement);
				if (result == null) result = caseIdentifiedElement(generateStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.GENERATION_SCHEME: {
				GenerationScheme generationScheme = (GenerationScheme)theEObject;
				T result = caseGenerationScheme(generationScheme);
				if (result == null) result = caseIdentifiedElement(generationScheme);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.FOR_GENERATION_SCHEME: {
				ForGenerationScheme forGenerationScheme = (ForGenerationScheme)theEObject;
				T result = caseForGenerationScheme(forGenerationScheme);
				if (result == null) result = caseGenerationScheme(forGenerationScheme);
				if (result == null) result = caseIdentifiedElement(forGenerationScheme);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.IF_GENERATION_SCHEME: {
				IfGenerationScheme ifGenerationScheme = (IfGenerationScheme)theEObject;
				T result = caseIfGenerationScheme(ifGenerationScheme);
				if (result == null) result = caseGenerationScheme(ifGenerationScheme);
				if (result == null) result = caseIdentifiedElement(ifGenerationScheme);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.GENERICS: {
				Generics generics = (Generics)theEObject;
				T result = caseGenerics(generics);
				if (result == null) result = caseIdentifiedElement(generics);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.GENERIC_MAPS: {
				GenericMaps genericMaps = (GenericMaps)theEObject;
				T result = caseGenericMaps(genericMaps);
				if (result == null) result = caseIdentifiedElement(genericMaps);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.GROUP_DECLARATION: {
				GroupDeclaration groupDeclaration = (GroupDeclaration)theEObject;
				T result = caseGroupDeclaration(groupDeclaration);
				if (result == null) result = caseDeclaration(groupDeclaration);
				if (result == null) result = caseNamedElement(groupDeclaration);
				if (result == null) result = caseIdentifiedElement(groupDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.GROUP_TEMPLATE_DECLARATION: {
				GroupTemplateDeclaration groupTemplateDeclaration = (GroupTemplateDeclaration)theEObject;
				T result = caseGroupTemplateDeclaration(groupTemplateDeclaration);
				if (result == null) result = caseDeclaration(groupTemplateDeclaration);
				if (result == null) result = caseNamedElement(groupTemplateDeclaration);
				if (result == null) result = caseIdentifiedElement(groupTemplateDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.IF_STATEMENT: {
				IfStatement ifStatement = (IfStatement)theEObject;
				T result = caseIfStatement(ifStatement);
				if (result == null) result = caseStatement(ifStatement);
				if (result == null) result = caseNamedElement(ifStatement);
				if (result == null) result = caseIdentifiedElement(ifStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.IF_STATEMENT_TEST: {
				IfStatementTest ifStatementTest = (IfStatementTest)theEObject;
				T result = caseIfStatementTest(ifStatementTest);
				if (result == null) result = caseIdentifiedElement(ifStatementTest);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CONSTRAINT: {
				Constraint constraint = (Constraint)theEObject;
				T result = caseConstraint(constraint);
				if (result == null) result = caseIdentifiedElement(constraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.DISCRETE_RANGE: {
				DiscreteRange discreteRange = (DiscreteRange)theEObject;
				T result = caseDiscreteRange(discreteRange);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.LOOP_STATEMENT: {
				LoopStatement loopStatement = (LoopStatement)theEObject;
				T result = caseLoopStatement(loopStatement);
				if (result == null) result = caseStatement(loopStatement);
				if (result == null) result = caseNamedElement(loopStatement);
				if (result == null) result = caseIdentifiedElement(loopStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ITERATION_SCHEME: {
				IterationScheme iterationScheme = (IterationScheme)theEObject;
				T result = caseIterationScheme(iterationScheme);
				if (result == null) result = caseIdentifiedElement(iterationScheme);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.WHILE_ITERATION_SCHEME: {
				WhileIterationScheme whileIterationScheme = (WhileIterationScheme)theEObject;
				T result = caseWhileIterationScheme(whileIterationScheme);
				if (result == null) result = caseIterationScheme(whileIterationScheme);
				if (result == null) result = caseIdentifiedElement(whileIterationScheme);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.FOR_ITERATION_SCHEME: {
				ForIterationScheme forIterationScheme = (ForIterationScheme)theEObject;
				T result = caseForIterationScheme(forIterationScheme);
				if (result == null) result = caseIterationScheme(forIterationScheme);
				if (result == null) result = caseIdentifiedElement(forIterationScheme);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ATTRIBUTE: {
				Attribute attribute = (Attribute)theEObject;
				T result = caseAttribute(attribute);
				if (result == null) result = caseNameElement(attribute);
				if (result == null) result = caseExpression(attribute);
				if (result == null) result = caseNamedElement(attribute);
				if (result == null) result = caseChoice(attribute);
				if (result == null) result = caseDiscreteRange(attribute);
				if (result == null) result = caseIdentifiedElement(attribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.NATURE_DECLARATION: {
				NatureDeclaration natureDeclaration = (NatureDeclaration)theEObject;
				T result = caseNatureDeclaration(natureDeclaration);
				if (result == null) result = caseDeclaration(natureDeclaration);
				if (result == null) result = caseNamedElement(natureDeclaration);
				if (result == null) result = caseIdentifiedElement(natureDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.NATURE_DEFINITION: {
				NatureDefinition natureDefinition = (NatureDefinition)theEObject;
				T result = caseNatureDefinition(natureDefinition);
				if (result == null) result = caseIdentifiedElement(natureDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.NEXT_STATEMENT: {
				NextStatement nextStatement = (NextStatement)theEObject;
				T result = caseNextStatement(nextStatement);
				if (result == null) result = caseStatement(nextStatement);
				if (result == null) result = caseNamedElement(nextStatement);
				if (result == null) result = caseIdentifiedElement(nextStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.PACKAGE_BODY: {
				PackageBody packageBody = (PackageBody)theEObject;
				T result = casePackageBody(packageBody);
				if (result == null) result = caseLibraryUnit(packageBody);
				if (result == null) result = caseNamedElement(packageBody);
				if (result == null) result = caseIdentifiedElement(packageBody);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.PACKAGE_DECLARATION: {
				PackageDeclaration packageDeclaration = (PackageDeclaration)theEObject;
				T result = casePackageDeclaration(packageDeclaration);
				if (result == null) result = caseLibraryUnit(packageDeclaration);
				if (result == null) result = caseNamedElement(packageDeclaration);
				if (result == null) result = caseIdentifiedElement(packageDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION: {
				PhysicalTypeDefinition physicalTypeDefinition = (PhysicalTypeDefinition)theEObject;
				T result = casePhysicalTypeDefinition(physicalTypeDefinition);
				if (result == null) result = caseTypeDefinition(physicalTypeDefinition);
				if (result == null) result = caseIdentifiedElement(physicalTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION_SECONDARY: {
				PhysicalTypeDefinitionSecondary physicalTypeDefinitionSecondary = (PhysicalTypeDefinitionSecondary)theEObject;
				T result = casePhysicalTypeDefinitionSecondary(physicalTypeDefinitionSecondary);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.PORTS: {
				Ports ports = (Ports)theEObject;
				T result = casePorts(ports);
				if (result == null) result = caseIdentifiedElement(ports);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.PORT_MAPS: {
				PortMaps portMaps = (PortMaps)theEObject;
				T result = casePortMaps(portMaps);
				if (result == null) result = caseIdentifiedElement(portMaps);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.PROCEDURE_CALL_STATEMENT: {
				ProcedureCallStatement procedureCallStatement = (ProcedureCallStatement)theEObject;
				T result = caseProcedureCallStatement(procedureCallStatement);
				if (result == null) result = caseStatement(procedureCallStatement);
				if (result == null) result = caseNamedElement(procedureCallStatement);
				if (result == null) result = caseIdentifiedElement(procedureCallStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.PROCESS_STATEMENT: {
				ProcessStatement processStatement = (ProcessStatement)theEObject;
				T result = caseProcessStatement(processStatement);
				if (result == null) result = caseStatement(processStatement);
				if (result == null) result = caseNamedElement(processStatement);
				if (result == null) result = caseIdentifiedElement(processStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ALLOCATOR: {
				Allocator allocator = (Allocator)theEObject;
				T result = caseAllocator(allocator);
				if (result == null) result = caseExpression(allocator);
				if (result == null) result = caseNamedElement(allocator);
				if (result == null) result = caseChoice(allocator);
				if (result == null) result = caseDiscreteRange(allocator);
				if (result == null) result = caseIdentifiedElement(allocator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.RECORD_NATURE_DEFINITION: {
				RecordNatureDefinition recordNatureDefinition = (RecordNatureDefinition)theEObject;
				T result = caseRecordNatureDefinition(recordNatureDefinition);
				if (result == null) result = caseCompositeNatureDefinition(recordNatureDefinition);
				if (result == null) result = caseNatureDefinition(recordNatureDefinition);
				if (result == null) result = caseIdentifiedElement(recordNatureDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.RECORD_NATURE_ELEMENT: {
				RecordNatureElement recordNatureElement = (RecordNatureElement)theEObject;
				T result = caseRecordNatureElement(recordNatureElement);
				if (result == null) result = caseIdentifiedElement(recordNatureElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.RECORD_TYPE_DEFINITION: {
				RecordTypeDefinition recordTypeDefinition = (RecordTypeDefinition)theEObject;
				T result = caseRecordTypeDefinition(recordTypeDefinition);
				if (result == null) result = caseCompositeTypeDefinition(recordTypeDefinition);
				if (result == null) result = caseTypeDefinition(recordTypeDefinition);
				if (result == null) result = caseIdentifiedElement(recordTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.REPORT_STATEMENT: {
				ReportStatement reportStatement = (ReportStatement)theEObject;
				T result = caseReportStatement(reportStatement);
				if (result == null) result = caseStatement(reportStatement);
				if (result == null) result = caseNamedElement(reportStatement);
				if (result == null) result = caseIdentifiedElement(reportStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.RETURN_STATEMENT: {
				ReturnStatement returnStatement = (ReturnStatement)theEObject;
				T result = caseReturnStatement(returnStatement);
				if (result == null) result = caseStatement(returnStatement);
				if (result == null) result = caseNamedElement(returnStatement);
				if (result == null) result = caseIdentifiedElement(returnStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SCALAR_NATURE_DEFINITION: {
				ScalarNatureDefinition scalarNatureDefinition = (ScalarNatureDefinition)theEObject;
				T result = caseScalarNatureDefinition(scalarNatureDefinition);
				if (result == null) result = caseNatureDefinition(scalarNatureDefinition);
				if (result == null) result = caseIdentifiedElement(scalarNatureDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.NULL_STATEMENT: {
				NullStatement nullStatement = (NullStatement)theEObject;
				T result = caseNullStatement(nullStatement);
				if (result == null) result = caseStatement(nullStatement);
				if (result == null) result = caseNamedElement(nullStatement);
				if (result == null) result = caseIdentifiedElement(nullStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SIGNAL_ASSIGNMENT_STATEMENT: {
				SignalAssignmentStatement signalAssignmentStatement = (SignalAssignmentStatement)theEObject;
				T result = caseSignalAssignmentStatement(signalAssignmentStatement);
				if (result == null) result = caseStatement(signalAssignmentStatement);
				if (result == null) result = caseNamedElement(signalAssignmentStatement);
				if (result == null) result = caseIdentifiedElement(signalAssignmentStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.VARIABLE_ASSIGNMENT_STATEMENT: {
				VariableAssignmentStatement variableAssignmentStatement = (VariableAssignmentStatement)theEObject;
				T result = caseVariableAssignmentStatement(variableAssignmentStatement);
				if (result == null) result = caseStatement(variableAssignmentStatement);
				if (result == null) result = caseNamedElement(variableAssignmentStatement);
				if (result == null) result = caseIdentifiedElement(variableAssignmentStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SIGNAL_DECLARATION: {
				SignalDeclaration signalDeclaration = (SignalDeclaration)theEObject;
				T result = caseSignalDeclaration(signalDeclaration);
				if (result == null) result = caseDeclaration(signalDeclaration);
				if (result == null) result = caseNamedElement(signalDeclaration);
				if (result == null) result = caseIdentifiedElement(signalDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.VARIABLE_DECLARATION: {
				VariableDeclaration variableDeclaration = (VariableDeclaration)theEObject;
				T result = caseVariableDeclaration(variableDeclaration);
				if (result == null) result = caseDeclaration(variableDeclaration);
				if (result == null) result = caseNamedElement(variableDeclaration);
				if (result == null) result = caseIdentifiedElement(variableDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CONSTANT_DECLARATION: {
				ConstantDeclaration constantDeclaration = (ConstantDeclaration)theEObject;
				T result = caseConstantDeclaration(constantDeclaration);
				if (result == null) result = caseDeclaration(constantDeclaration);
				if (result == null) result = caseNamedElement(constantDeclaration);
				if (result == null) result = caseIdentifiedElement(constantDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SIGNATURE: {
				Signature signature = (Signature)theEObject;
				T result = caseSignature(signature);
				if (result == null) result = caseIdentifiedElement(signature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE: {
				SimultaneousAlternative simultaneousAlternative = (SimultaneousAlternative)theEObject;
				T result = caseSimultaneousAlternative(simultaneousAlternative);
				if (result == null) result = caseIdentifiedElement(simultaneousAlternative);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SIMULTANEOUS_CASE_STATEMENT: {
				SimultaneousCaseStatement simultaneousCaseStatement = (SimultaneousCaseStatement)theEObject;
				T result = caseSimultaneousCaseStatement(simultaneousCaseStatement);
				if (result == null) result = caseStatement(simultaneousCaseStatement);
				if (result == null) result = caseNamedElement(simultaneousCaseStatement);
				if (result == null) result = caseIdentifiedElement(simultaneousCaseStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT: {
				SimultaneousIfStatement simultaneousIfStatement = (SimultaneousIfStatement)theEObject;
				T result = caseSimultaneousIfStatement(simultaneousIfStatement);
				if (result == null) result = caseStatement(simultaneousIfStatement);
				if (result == null) result = caseNamedElement(simultaneousIfStatement);
				if (result == null) result = caseIdentifiedElement(simultaneousIfStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST: {
				SimultaneousIfStatementTest simultaneousIfStatementTest = (SimultaneousIfStatementTest)theEObject;
				T result = caseSimultaneousIfStatementTest(simultaneousIfStatementTest);
				if (result == null) result = caseIdentifiedElement(simultaneousIfStatementTest);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SIMULTANEOUS_PROCEDURAL_STATEMENT: {
				SimultaneousProceduralStatement simultaneousProceduralStatement = (SimultaneousProceduralStatement)theEObject;
				T result = caseSimultaneousProceduralStatement(simultaneousProceduralStatement);
				if (result == null) result = caseStatement(simultaneousProceduralStatement);
				if (result == null) result = caseNamedElement(simultaneousProceduralStatement);
				if (result == null) result = caseIdentifiedElement(simultaneousProceduralStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SOURCE_ASPECT: {
				SourceAspect sourceAspect = (SourceAspect)theEObject;
				T result = caseSourceAspect(sourceAspect);
				if (result == null) result = caseIdentifiedElement(sourceAspect);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SPECTRUM: {
				Spectrum spectrum = (Spectrum)theEObject;
				T result = caseSpectrum(spectrum);
				if (result == null) result = caseSourceAspect(spectrum);
				if (result == null) result = caseIdentifiedElement(spectrum);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.NOISE: {
				Noise noise = (Noise)theEObject;
				T result = caseNoise(noise);
				if (result == null) result = caseSourceAspect(noise);
				if (result == null) result = caseIdentifiedElement(noise);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.QUANTITY_DECLARATION: {
				QuantityDeclaration quantityDeclaration = (QuantityDeclaration)theEObject;
				T result = caseQuantityDeclaration(quantityDeclaration);
				if (result == null) result = caseDeclaration(quantityDeclaration);
				if (result == null) result = caseNamedElement(quantityDeclaration);
				if (result == null) result = caseIdentifiedElement(quantityDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.FREE_QUANTITY_DECLARATION: {
				FreeQuantityDeclaration freeQuantityDeclaration = (FreeQuantityDeclaration)theEObject;
				T result = caseFreeQuantityDeclaration(freeQuantityDeclaration);
				if (result == null) result = caseQuantityDeclaration(freeQuantityDeclaration);
				if (result == null) result = caseDeclaration(freeQuantityDeclaration);
				if (result == null) result = caseNamedElement(freeQuantityDeclaration);
				if (result == null) result = caseIdentifiedElement(freeQuantityDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.BRANCH_QUANTITY_DECLARATION: {
				BranchQuantityDeclaration branchQuantityDeclaration = (BranchQuantityDeclaration)theEObject;
				T result = caseBranchQuantityDeclaration(branchQuantityDeclaration);
				if (result == null) result = caseQuantityDeclaration(branchQuantityDeclaration);
				if (result == null) result = caseDeclaration(branchQuantityDeclaration);
				if (result == null) result = caseNamedElement(branchQuantityDeclaration);
				if (result == null) result = caseIdentifiedElement(branchQuantityDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.QUANTITY_ASPECT: {
				QuantityAspect quantityAspect = (QuantityAspect)theEObject;
				T result = caseQuantityAspect(quantityAspect);
				if (result == null) result = caseIdentifiedElement(quantityAspect);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SOURCE_QUANTITY_DECLARATION: {
				SourceQuantityDeclaration sourceQuantityDeclaration = (SourceQuantityDeclaration)theEObject;
				T result = caseSourceQuantityDeclaration(sourceQuantityDeclaration);
				if (result == null) result = caseQuantityDeclaration(sourceQuantityDeclaration);
				if (result == null) result = caseDeclaration(sourceQuantityDeclaration);
				if (result == null) result = caseNamedElement(sourceQuantityDeclaration);
				if (result == null) result = caseIdentifiedElement(sourceQuantityDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.LIMIT_SPECIFICATION: {
				LimitSpecification limitSpecification = (LimitSpecification)theEObject;
				T result = caseLimitSpecification(limitSpecification);
				if (result == null) result = caseDeclaration(limitSpecification);
				if (result == null) result = caseNamedElement(limitSpecification);
				if (result == null) result = caseIdentifiedElement(limitSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SUBNATURE_DECLARATION: {
				SubnatureDeclaration subnatureDeclaration = (SubnatureDeclaration)theEObject;
				T result = caseSubnatureDeclaration(subnatureDeclaration);
				if (result == null) result = caseDeclaration(subnatureDeclaration);
				if (result == null) result = caseNamedElement(subnatureDeclaration);
				if (result == null) result = caseIdentifiedElement(subnatureDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SUBPROGRAM_DECLARATION: {
				SubprogramDeclaration subprogramDeclaration = (SubprogramDeclaration)theEObject;
				T result = caseSubprogramDeclaration(subprogramDeclaration);
				if (result == null) result = caseDeclaration(subprogramDeclaration);
				if (result == null) result = caseNamedElement(subprogramDeclaration);
				if (result == null) result = caseIdentifiedElement(subprogramDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SUBPROGRAM_SPECIFICATION: {
				SubprogramSpecification subprogramSpecification = (SubprogramSpecification)theEObject;
				T result = caseSubprogramSpecification(subprogramSpecification);
				if (result == null) result = caseIdentifiedElement(subprogramSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SUBTYPE_DECLARATION: {
				SubtypeDeclaration subtypeDeclaration = (SubtypeDeclaration)theEObject;
				T result = caseSubtypeDeclaration(subtypeDeclaration);
				if (result == null) result = caseDeclaration(subtypeDeclaration);
				if (result == null) result = caseNamedElement(subtypeDeclaration);
				if (result == null) result = caseIdentifiedElement(subtypeDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ALIAS_DECLARATION: {
				AliasDeclaration aliasDeclaration = (AliasDeclaration)theEObject;
				T result = caseAliasDeclaration(aliasDeclaration);
				if (result == null) result = caseDeclaration(aliasDeclaration);
				if (result == null) result = caseNamedElement(aliasDeclaration);
				if (result == null) result = caseIdentifiedElement(aliasDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.TERMINAL_DECLARATION: {
				TerminalDeclaration terminalDeclaration = (TerminalDeclaration)theEObject;
				T result = caseTerminalDeclaration(terminalDeclaration);
				if (result == null) result = caseDeclaration(terminalDeclaration);
				if (result == null) result = caseNamedElement(terminalDeclaration);
				if (result == null) result = caseIdentifiedElement(terminalDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.TYPE_DECLARATION: {
				TypeDeclaration typeDeclaration = (TypeDeclaration)theEObject;
				T result = caseTypeDeclaration(typeDeclaration);
				if (result == null) result = caseDeclaration(typeDeclaration);
				if (result == null) result = caseNamedElement(typeDeclaration);
				if (result == null) result = caseIdentifiedElement(typeDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.TYPE_DEFINITION: {
				TypeDefinition typeDefinition = (TypeDefinition)theEObject;
				T result = caseTypeDefinition(typeDefinition);
				if (result == null) result = caseIdentifiedElement(typeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.RANGE_TYPE_DEFINITION: {
				RangeTypeDefinition rangeTypeDefinition = (RangeTypeDefinition)theEObject;
				T result = caseRangeTypeDefinition(rangeTypeDefinition);
				if (result == null) result = caseTypeDefinition(rangeTypeDefinition);
				if (result == null) result = caseIdentifiedElement(rangeTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.UNCONSTRAINED_ARRAY_DEFINITION: {
				UnconstrainedArrayDefinition unconstrainedArrayDefinition = (UnconstrainedArrayDefinition)theEObject;
				T result = caseUnconstrainedArrayDefinition(unconstrainedArrayDefinition);
				if (result == null) result = caseArrayTypeDefinition(unconstrainedArrayDefinition);
				if (result == null) result = caseCompositeTypeDefinition(unconstrainedArrayDefinition);
				if (result == null) result = caseTypeDefinition(unconstrainedArrayDefinition);
				if (result == null) result = caseIdentifiedElement(unconstrainedArrayDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.UNCONSTRAINED_NATURE_DEFINITION: {
				UnconstrainedNatureDefinition unconstrainedNatureDefinition = (UnconstrainedNatureDefinition)theEObject;
				T result = caseUnconstrainedNatureDefinition(unconstrainedNatureDefinition);
				if (result == null) result = caseArrayNatureDefinition(unconstrainedNatureDefinition);
				if (result == null) result = caseCompositeNatureDefinition(unconstrainedNatureDefinition);
				if (result == null) result = caseNatureDefinition(unconstrainedNatureDefinition);
				if (result == null) result = caseIdentifiedElement(unconstrainedNatureDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CONSTRAINED_ARRAY_DEFINITION: {
				ConstrainedArrayDefinition constrainedArrayDefinition = (ConstrainedArrayDefinition)theEObject;
				T result = caseConstrainedArrayDefinition(constrainedArrayDefinition);
				if (result == null) result = caseArrayTypeDefinition(constrainedArrayDefinition);
				if (result == null) result = caseCompositeTypeDefinition(constrainedArrayDefinition);
				if (result == null) result = caseTypeDefinition(constrainedArrayDefinition);
				if (result == null) result = caseIdentifiedElement(constrainedArrayDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CONSTRAINED_NATURE_DEFINITION: {
				ConstrainedNatureDefinition constrainedNatureDefinition = (ConstrainedNatureDefinition)theEObject;
				T result = caseConstrainedNatureDefinition(constrainedNatureDefinition);
				if (result == null) result = caseArrayNatureDefinition(constrainedNatureDefinition);
				if (result == null) result = caseCompositeNatureDefinition(constrainedNatureDefinition);
				if (result == null) result = caseNatureDefinition(constrainedNatureDefinition);
				if (result == null) result = caseIdentifiedElement(constrainedNatureDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.WAIT_STATEMENT: {
				WaitStatement waitStatement = (WaitStatement)theEObject;
				T result = caseWaitStatement(waitStatement);
				if (result == null) result = caseStatement(waitStatement);
				if (result == null) result = caseNamedElement(waitStatement);
				if (result == null) result = caseIdentifiedElement(waitStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.NULL: {
				Null null_ = (Null)theEObject;
				T result = caseNull(null_);
				if (result == null) result = caseExpression(null_);
				if (result == null) result = caseNamedElement(null_);
				if (result == null) result = caseChoice(null_);
				if (result == null) result = caseDiscreteRange(null_);
				if (result == null) result = caseIdentifiedElement(null_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.VALUE: {
				Value value = (Value)theEObject;
				T result = caseValue(value);
				if (result == null) result = caseExpression(value);
				if (result == null) result = caseNamedElement(value);
				if (result == null) result = caseChoice(value);
				if (result == null) result = caseDiscreteRange(value);
				if (result == null) result = caseIdentifiedElement(value);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.AGGREGATE: {
				Aggregate aggregate = (Aggregate)theEObject;
				T result = caseAggregate(aggregate);
				if (result == null) result = caseNameElement(aggregate);
				if (result == null) result = caseExpression(aggregate);
				if (result == null) result = caseNamedElement(aggregate);
				if (result == null) result = caseChoice(aggregate);
				if (result == null) result = caseDiscreteRange(aggregate);
				if (result == null) result = caseIdentifiedElement(aggregate);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ALL: {
				All all = (All)theEObject;
				T result = caseAll(all);
				if (result == null) result = caseNameElement(all);
				if (result == null) result = caseExpression(all);
				if (result == null) result = caseNamedElement(all);
				if (result == null) result = caseChoice(all);
				if (result == null) result = caseDiscreteRange(all);
				if (result == null) result = caseIdentifiedElement(all);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ASSOCIATION: {
				Association association = (Association)theEObject;
				T result = caseAssociation(association);
				if (result == null) result = caseExpression(association);
				if (result == null) result = caseNamedElement(association);
				if (result == null) result = caseChoice(association);
				if (result == null) result = caseDiscreteRange(association);
				if (result == null) result = caseIdentifiedElement(association);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.OTHERS: {
				Others others = (Others)theEObject;
				T result = caseOthers(others);
				if (result == null) result = caseNameElement(others);
				if (result == null) result = caseExpression(others);
				if (result == null) result = caseNamedElement(others);
				if (result == null) result = caseChoice(others);
				if (result == null) result = caseDiscreteRange(others);
				if (result == null) result = caseIdentifiedElement(others);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.LOGICAL_EXPRESSION: {
				LogicalExpression logicalExpression = (LogicalExpression)theEObject;
				T result = caseLogicalExpression(logicalExpression);
				if (result == null) result = caseExpression(logicalExpression);
				if (result == null) result = caseNamedElement(logicalExpression);
				if (result == null) result = caseChoice(logicalExpression);
				if (result == null) result = caseDiscreteRange(logicalExpression);
				if (result == null) result = caseIdentifiedElement(logicalExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.RELATIONAL_EXPRESSION: {
				RelationalExpression relationalExpression = (RelationalExpression)theEObject;
				T result = caseRelationalExpression(relationalExpression);
				if (result == null) result = caseExpression(relationalExpression);
				if (result == null) result = caseNamedElement(relationalExpression);
				if (result == null) result = caseChoice(relationalExpression);
				if (result == null) result = caseDiscreteRange(relationalExpression);
				if (result == null) result = caseIdentifiedElement(relationalExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SHIFT_EXPRESSION: {
				ShiftExpression shiftExpression = (ShiftExpression)theEObject;
				T result = caseShiftExpression(shiftExpression);
				if (result == null) result = caseExpression(shiftExpression);
				if (result == null) result = caseNamedElement(shiftExpression);
				if (result == null) result = caseChoice(shiftExpression);
				if (result == null) result = caseDiscreteRange(shiftExpression);
				if (result == null) result = caseIdentifiedElement(shiftExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.ADDING_EXPRESSION: {
				AddingExpression addingExpression = (AddingExpression)theEObject;
				T result = caseAddingExpression(addingExpression);
				if (result == null) result = caseExpression(addingExpression);
				if (result == null) result = caseNamedElement(addingExpression);
				if (result == null) result = caseChoice(addingExpression);
				if (result == null) result = caseDiscreteRange(addingExpression);
				if (result == null) result = caseIdentifiedElement(addingExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.MULTIPLYING_EXPRESSION: {
				MultiplyingExpression multiplyingExpression = (MultiplyingExpression)theEObject;
				T result = caseMultiplyingExpression(multiplyingExpression);
				if (result == null) result = caseExpression(multiplyingExpression);
				if (result == null) result = caseNamedElement(multiplyingExpression);
				if (result == null) result = caseChoice(multiplyingExpression);
				if (result == null) result = caseDiscreteRange(multiplyingExpression);
				if (result == null) result = caseIdentifiedElement(multiplyingExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.POWER_EXPRESSION: {
				PowerExpression powerExpression = (PowerExpression)theEObject;
				T result = casePowerExpression(powerExpression);
				if (result == null) result = caseExpression(powerExpression);
				if (result == null) result = caseNamedElement(powerExpression);
				if (result == null) result = caseChoice(powerExpression);
				if (result == null) result = caseDiscreteRange(powerExpression);
				if (result == null) result = caseIdentifiedElement(powerExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.UNARY_EXPRESSION: {
				UnaryExpression unaryExpression = (UnaryExpression)theEObject;
				T result = caseUnaryExpression(unaryExpression);
				if (result == null) result = caseExpression(unaryExpression);
				if (result == null) result = caseNamedElement(unaryExpression);
				if (result == null) result = caseChoice(unaryExpression);
				if (result == null) result = caseDiscreteRange(unaryExpression);
				if (result == null) result = caseIdentifiedElement(unaryExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.SIGN_EXPRESSION: {
				SignExpression signExpression = (SignExpression)theEObject;
				T result = caseSignExpression(signExpression);
				if (result == null) result = caseExpression(signExpression);
				if (result == null) result = caseNamedElement(signExpression);
				if (result == null) result = caseChoice(signExpression);
				if (result == null) result = caseDiscreteRange(signExpression);
				if (result == null) result = caseIdentifiedElement(signExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.RANGE: {
				Range range = (Range)theEObject;
				T result = caseRange(range);
				if (result == null) result = caseNameElement(range);
				if (result == null) result = caseExpression(range);
				if (result == null) result = caseNamedElement(range);
				if (result == null) result = caseChoice(range);
				if (result == null) result = caseDiscreteRange(range);
				if (result == null) result = caseIdentifiedElement(range);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.NAME: {
				Name name = (Name)theEObject;
				T result = caseName(name);
				if (result == null) result = caseExpression(name);
				if (result == null) result = caseNamedElement(name);
				if (result == null) result = caseChoice(name);
				if (result == null) result = caseDiscreteRange(name);
				if (result == null) result = caseIdentifiedElement(name);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.BIT_STRING: {
				BitString bitString = (BitString)theEObject;
				T result = caseBitString(bitString);
				if (result == null) result = caseNameElement(bitString);
				if (result == null) result = caseExpression(bitString);
				if (result == null) result = caseNamedElement(bitString);
				if (result == null) result = caseChoice(bitString);
				if (result == null) result = caseDiscreteRange(bitString);
				if (result == null) result = caseIdentifiedElement(bitString);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.STRING: {
				fr.labsticc.language.vhdl.model.vhdl.String string = (fr.labsticc.language.vhdl.model.vhdl.String)theEObject;
				T result = caseString(string);
				if (result == null) result = caseNameElement(string);
				if (result == null) result = caseExpression(string);
				if (result == null) result = caseNamedElement(string);
				if (result == null) result = caseChoice(string);
				if (result == null) result = caseDiscreteRange(string);
				if (result == null) result = caseIdentifiedElement(string);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.CHARACTER: {
				fr.labsticc.language.vhdl.model.vhdl.Character character = (fr.labsticc.language.vhdl.model.vhdl.Character)theEObject;
				T result = caseCharacter(character);
				if (result == null) result = caseNameElement(character);
				if (result == null) result = caseExpression(character);
				if (result == null) result = caseNamedElement(character);
				if (result == null) result = caseChoice(character);
				if (result == null) result = caseDiscreteRange(character);
				if (result == null) result = caseIdentifiedElement(character);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.IDENTIFIER: {
				Identifier identifier = (Identifier)theEObject;
				T result = caseIdentifier(identifier);
				if (result == null) result = caseNameElement(identifier);
				if (result == null) result = caseExpression(identifier);
				if (result == null) result = caseNamedElement(identifier);
				if (result == null) result = caseChoice(identifier);
				if (result == null) result = caseDiscreteRange(identifier);
				if (result == null) result = caseIdentifiedElement(identifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.INDICATION: {
				Indication indication = (Indication)theEObject;
				T result = caseIndication(indication);
				if (result == null) result = caseExpression(indication);
				if (result == null) result = caseNamedElement(indication);
				if (result == null) result = caseChoice(indication);
				if (result == null) result = caseDiscreteRange(indication);
				if (result == null) result = caseIdentifiedElement(indication);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.WAVEFORM: {
				Waveform waveform = (Waveform)theEObject;
				T result = caseWaveform(waveform);
				if (result == null) result = caseExpression(waveform);
				if (result == null) result = caseNamedElement(waveform);
				if (result == null) result = caseChoice(waveform);
				if (result == null) result = caseDiscreteRange(waveform);
				if (result == null) result = caseIdentifiedElement(waveform);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.UNIT_VALUE: {
				UnitValue unitValue = (UnitValue)theEObject;
				T result = caseUnitValue(unitValue);
				if (result == null) result = caseValue(unitValue);
				if (result == null) result = caseExpression(unitValue);
				if (result == null) result = caseNamedElement(unitValue);
				if (result == null) result = caseChoice(unitValue);
				if (result == null) result = caseDiscreteRange(unitValue);
				if (result == null) result = caseIdentifiedElement(unitValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.IDENTIFIED_ELEMENT: {
				IdentifiedElement identifiedElement = (IdentifiedElement)theEObject;
				T result = caseIdentifiedElement(identifiedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VhdlPackage.NAMED_ELEMENT: {
				NamedElement namedElement = (NamedElement)theEObject;
				T result = caseNamedElement(namedElement);
				if (result == null) result = caseIdentifiedElement(namedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>File</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>File</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVhdlFile(VhdlFile object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Design Unit</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Design Unit</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDesignUnit(DesignUnit object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Use Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Use Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUseClause(UseClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Library Unit</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Library Unit</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLibraryUnit(LibraryUnit object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessTypeDefinition(AccessTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNameElement(NameElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeclaration(Declaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Architecture</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Architecture</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArchitecture(Architecture object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStatement(Statement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Array Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Array Nature Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArrayNatureDefinition(ArrayNatureDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Array Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Array Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArrayTypeDefinition(ArrayTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assertion Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assertion Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssertionStatement(AssertionStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpression(Expression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Choice</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Choice</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChoice(Choice object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Open</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Open</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpen(Open object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttributeDeclaration(AttributeDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttributeSpecification(AttributeSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNameList(NameList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlockConfiguration(BlockConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlockStatement(BlockStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Break Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Break Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBreakElement(BreakElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Break Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Break Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBreakStatement(BreakStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Case Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Case Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaseStatement(CaseStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Case Alternative</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Case Alternative</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaseAlternative(CaseAlternative object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentConfiguration(ComponentConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentDeclaration(ComponentDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instantiation Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instantiation Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstantiationStatement(InstantiationStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Instantiation Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Instantiation Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentInstantiationStatement(ComponentInstantiationStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity Instantiation Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity Instantiation Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntityInstantiationStatement(EntityInstantiationStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Configuration Instantiation Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Configuration Instantiation Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfigurationInstantiationStatement(ConfigurationInstantiationStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Nature Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompositeNatureDefinition(CompositeNatureDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompositeTypeDefinition(CompositeTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequential Signal Assignment Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequential Signal Assignment Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequentialSignalAssignmentStatement(SequentialSignalAssignmentStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conditional Signal Assignment Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conditional Signal Assignment Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConditionalSignalAssignmentStatement(ConditionalSignalAssignmentStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Selected Signal Assignment Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Selected Signal Assignment Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSelectedSignalAssignmentStatement(SelectedSignalAssignmentStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Simultaneous Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Simultaneous Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleSimultaneousStatement(SimpleSimultaneousStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conditional Waveform</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conditional Waveform</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConditionalWaveform(ConditionalWaveform object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Configuration Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Configuration Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfigurationDeclaration(ConfigurationDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Configuration Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Configuration Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfigurationItem(ConfigurationItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Configuration Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Configuration Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfigurationSpecification(ConfigurationSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delay Mechanism</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delay Mechanism</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDelayMechanism(DelayMechanism object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reject Mechanism</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reject Mechanism</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRejectMechanism(RejectMechanism object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transport</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transport</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransport(Transport object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Disconnection Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Disconnection Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDisconnectionSpecification(DisconnectionSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementDeclaration(ElementDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntityDeclaration(EntityDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumeration Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumeration Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumerationTypeDefinition(EnumerationTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exit Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exit Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExitStatement(ExitStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>File Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>File Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFileDeclaration(FileDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>File Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>File Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFileTypeDefinition(FileTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generate Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generate Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenerateStatement(GenerateStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generation Scheme</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generation Scheme</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenerationScheme(GenerationScheme object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>For Generation Scheme</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>For Generation Scheme</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForGenerationScheme(ForGenerationScheme object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Generation Scheme</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Generation Scheme</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfGenerationScheme(IfGenerationScheme object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generics</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generics</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenerics(Generics object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Maps</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Maps</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericMaps(GenericMaps object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Group Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Group Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGroupDeclaration(GroupDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Group Template Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Group Template Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGroupTemplateDeclaration(GroupTemplateDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfStatement(IfStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Statement Test</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Statement Test</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfStatementTest(IfStatementTest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstraint(Constraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discrete Range</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discrete Range</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscreteRange(DiscreteRange object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Loop Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Loop Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoopStatement(LoopStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iteration Scheme</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iteration Scheme</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIterationScheme(IterationScheme object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>While Iteration Scheme</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>While Iteration Scheme</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWhileIterationScheme(WhileIterationScheme object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>For Iteration Scheme</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>For Iteration Scheme</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForIterationScheme(ForIterationScheme object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttribute(Attribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Nature Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Nature Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNatureDeclaration(NatureDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Nature Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNatureDefinition(NatureDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Next Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Next Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNextStatement(NextStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Package Body</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Package Body</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePackageBody(PackageBody object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Package Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Package Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePackageDeclaration(PackageDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalTypeDefinition(PhysicalTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Type Definition Secondary</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Type Definition Secondary</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalTypeDefinitionSecondary(PhysicalTypeDefinitionSecondary object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ports</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ports</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePorts(Ports object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Maps</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Maps</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortMaps(PortMaps object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Procedure Call Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Procedure Call Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcedureCallStatement(ProcedureCallStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessStatement(ProcessStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Allocator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Allocator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAllocator(Allocator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Nature Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordNatureDefinition(RecordNatureDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Nature Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Nature Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordNatureElement(RecordNatureElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordTypeDefinition(RecordTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Report Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Report Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReportStatement(ReportStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Return Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Return Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReturnStatement(ReturnStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scalar Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scalar Nature Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScalarNatureDefinition(ScalarNatureDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNullStatement(NullStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signal Assignment Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signal Assignment Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignalAssignmentStatement(SignalAssignmentStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Assignment Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Assignment Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableAssignmentStatement(VariableAssignmentStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signal Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signal Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignalDeclaration(SignalDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableDeclaration(VariableDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstantDeclaration(ConstantDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignature(Signature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simultaneous Alternative</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simultaneous Alternative</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimultaneousAlternative(SimultaneousAlternative object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simultaneous Case Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simultaneous Case Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimultaneousCaseStatement(SimultaneousCaseStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simultaneous If Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simultaneous If Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimultaneousIfStatement(SimultaneousIfStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simultaneous If Statement Test</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simultaneous If Statement Test</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimultaneousIfStatementTest(SimultaneousIfStatementTest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simultaneous Procedural Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simultaneous Procedural Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimultaneousProceduralStatement(SimultaneousProceduralStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Source Aspect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Source Aspect</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSourceAspect(SourceAspect object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Spectrum</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Spectrum</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSpectrum(Spectrum object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Noise</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Noise</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNoise(Noise object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Quantity Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Quantity Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQuantityDeclaration(QuantityDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Free Quantity Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Free Quantity Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFreeQuantityDeclaration(FreeQuantityDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Branch Quantity Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Branch Quantity Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBranchQuantityDeclaration(BranchQuantityDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Quantity Aspect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Quantity Aspect</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQuantityAspect(QuantityAspect object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Source Quantity Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Source Quantity Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSourceQuantityDeclaration(SourceQuantityDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Limit Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Limit Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLimitSpecification(LimitSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subnature Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subnature Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubnatureDeclaration(SubnatureDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subprogram Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subprogram Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubprogramDeclaration(SubprogramDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subprogram Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subprogram Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubprogramSpecification(SubprogramSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subtype Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subtype Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubtypeDeclaration(SubtypeDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alias Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alias Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAliasDeclaration(AliasDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Terminal Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Terminal Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTerminalDeclaration(TerminalDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeDeclaration(TypeDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeDefinition(TypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Range Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Range Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRangeTypeDefinition(RangeTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unconstrained Array Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unconstrained Array Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnconstrainedArrayDefinition(UnconstrainedArrayDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unconstrained Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unconstrained Nature Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnconstrainedNatureDefinition(UnconstrainedNatureDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constrained Array Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constrained Array Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstrainedArrayDefinition(ConstrainedArrayDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constrained Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constrained Nature Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstrainedNatureDefinition(ConstrainedNatureDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wait Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wait Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWaitStatement(WaitStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNull(Null object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValue(Value object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aggregate</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aggregate</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAggregate(Aggregate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Association</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Association</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssociation(Association object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>All</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>All</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAll(All object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Others</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Others</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOthers(Others object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Logical Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Logical Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLogicalExpression(LogicalExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relational Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relational Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelationalExpression(RelationalExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Shift Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Shift Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShiftExpression(ShiftExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Adding Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Adding Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAddingExpression(AddingExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multiplying Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multiplying Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiplyingExpression(MultiplyingExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Power Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Power Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePowerExpression(PowerExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnaryExpression(UnaryExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sign Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sign Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignExpression(SignExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Range</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Range</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRange(Range object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseName(Name object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseString(fr.labsticc.language.vhdl.model.vhdl.String object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Character</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Character</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCharacter(fr.labsticc.language.vhdl.model.vhdl.Character object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Identifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdentifier(Identifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Indication</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Indication</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIndication(Indication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Waveform</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Waveform</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWaveform(Waveform object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bit String</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bit String</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBitString(BitString object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unit Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unit Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnitValue(UnitValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Identified Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Identified Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdentifiedElement(IdentifiedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //VhdlSwitch
