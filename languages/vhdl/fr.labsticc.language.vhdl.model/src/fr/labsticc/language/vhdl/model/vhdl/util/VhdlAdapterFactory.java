/**
 */
package fr.labsticc.language.vhdl.model.vhdl.util;

import fr.labsticc.language.vhdl.model.vhdl.AccessTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.AddingExpression;
import fr.labsticc.language.vhdl.model.vhdl.Aggregate;
import fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.All;
import fr.labsticc.language.vhdl.model.vhdl.Allocator;
import fr.labsticc.language.vhdl.model.vhdl.Architecture;
import fr.labsticc.language.vhdl.model.vhdl.ArrayNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ArrayTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.AssertionStatement;
import fr.labsticc.language.vhdl.model.vhdl.Association;
import fr.labsticc.language.vhdl.model.vhdl.Attribute;
import fr.labsticc.language.vhdl.model.vhdl.AttributeDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification;
import fr.labsticc.language.vhdl.model.vhdl.BitString;
import fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration;
import fr.labsticc.language.vhdl.model.vhdl.BlockStatement;
import fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.BreakElement;
import fr.labsticc.language.vhdl.model.vhdl.BreakStatement;
import fr.labsticc.language.vhdl.model.vhdl.CaseAlternative;
import fr.labsticc.language.vhdl.model.vhdl.CaseStatement;
import fr.labsticc.language.vhdl.model.vhdl.Choice;
import fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration;
import fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.ComponentInstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.CompositeNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.CompositeTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationItem;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification;
import fr.labsticc.language.vhdl.model.vhdl.ConstantDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.ConstrainedArrayDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ConstrainedNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.Constraint;
import fr.labsticc.language.vhdl.model.vhdl.Declaration;
import fr.labsticc.language.vhdl.model.vhdl.DelayMechanism;
import fr.labsticc.language.vhdl.model.vhdl.DesignUnit;
import fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification;
import fr.labsticc.language.vhdl.model.vhdl.DiscreteRange;
import fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.EntityInstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.EnumerationTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ExitStatement;
import fr.labsticc.language.vhdl.model.vhdl.Expression;
import fr.labsticc.language.vhdl.model.vhdl.FileDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.FileTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ForGenerationScheme;
import fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme;
import fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.GenerateStatement;
import fr.labsticc.language.vhdl.model.vhdl.GenerationScheme;
import fr.labsticc.language.vhdl.model.vhdl.GenericMaps;
import fr.labsticc.language.vhdl.model.vhdl.Generics;
import fr.labsticc.language.vhdl.model.vhdl.GroupDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.GroupTemplateDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.IdentifiedElement;
import fr.labsticc.language.vhdl.model.vhdl.Identifier;
import fr.labsticc.language.vhdl.model.vhdl.IfGenerationScheme;
import fr.labsticc.language.vhdl.model.vhdl.IfStatement;
import fr.labsticc.language.vhdl.model.vhdl.IfStatementTest;
import fr.labsticc.language.vhdl.model.vhdl.Indication;
import fr.labsticc.language.vhdl.model.vhdl.InstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.IterationScheme;
import fr.labsticc.language.vhdl.model.vhdl.LibraryUnit;
import fr.labsticc.language.vhdl.model.vhdl.LimitSpecification;
import fr.labsticc.language.vhdl.model.vhdl.LogicalExpression;
import fr.labsticc.language.vhdl.model.vhdl.LoopStatement;
import fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression;
import fr.labsticc.language.vhdl.model.vhdl.Name;
import fr.labsticc.language.vhdl.model.vhdl.NameElement;
import fr.labsticc.language.vhdl.model.vhdl.NameList;
import fr.labsticc.language.vhdl.model.vhdl.NamedElement;
import fr.labsticc.language.vhdl.model.vhdl.NatureDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.NatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.NextStatement;
import fr.labsticc.language.vhdl.model.vhdl.Noise;
import fr.labsticc.language.vhdl.model.vhdl.Null;
import fr.labsticc.language.vhdl.model.vhdl.NullStatement;
import fr.labsticc.language.vhdl.model.vhdl.Open;
import fr.labsticc.language.vhdl.model.vhdl.Others;
import fr.labsticc.language.vhdl.model.vhdl.PackageBody;
import fr.labsticc.language.vhdl.model.vhdl.PackageDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary;
import fr.labsticc.language.vhdl.model.vhdl.PortMaps;
import fr.labsticc.language.vhdl.model.vhdl.Ports;
import fr.labsticc.language.vhdl.model.vhdl.PowerExpression;
import fr.labsticc.language.vhdl.model.vhdl.ProcedureCallStatement;
import fr.labsticc.language.vhdl.model.vhdl.ProcessStatement;
import fr.labsticc.language.vhdl.model.vhdl.QuantityAspect;
import fr.labsticc.language.vhdl.model.vhdl.QuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Range;
import fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.RecordNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement;
import fr.labsticc.language.vhdl.model.vhdl.RecordTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.RejectMechanism;
import fr.labsticc.language.vhdl.model.vhdl.RelationalExpression;
import fr.labsticc.language.vhdl.model.vhdl.ReportStatement;
import fr.labsticc.language.vhdl.model.vhdl.ReturnStatement;
import fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.ShiftExpression;
import fr.labsticc.language.vhdl.model.vhdl.SignExpression;
import fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Signature;
import fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousProceduralStatement;
import fr.labsticc.language.vhdl.model.vhdl.SourceAspect;
import fr.labsticc.language.vhdl.model.vhdl.SourceQuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Spectrum;
import fr.labsticc.language.vhdl.model.vhdl.Statement;
import fr.labsticc.language.vhdl.model.vhdl.SubnatureDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification;
import fr.labsticc.language.vhdl.model.vhdl.SubtypeDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.TerminalDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Transport;
import fr.labsticc.language.vhdl.model.vhdl.TypeDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.TypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.UnaryExpression;
import fr.labsticc.language.vhdl.model.vhdl.UnconstrainedArrayDefinition;
import fr.labsticc.language.vhdl.model.vhdl.UnconstrainedNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.UnitValue;
import fr.labsticc.language.vhdl.model.vhdl.UseClause;
import fr.labsticc.language.vhdl.model.vhdl.Value;
import fr.labsticc.language.vhdl.model.vhdl.VariableAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.VhdlFile;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;
import fr.labsticc.language.vhdl.model.vhdl.WaitStatement;
import fr.labsticc.language.vhdl.model.vhdl.Waveform;
import fr.labsticc.language.vhdl.model.vhdl.WhileIterationScheme;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage
 * @generated
 */
public class VhdlAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static VhdlPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VhdlAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = VhdlPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VhdlSwitch<Adapter> modelSwitch =
		new VhdlSwitch<Adapter>() {
			@Override
			public Adapter caseVhdlFile(VhdlFile object) {
				return createVhdlFileAdapter();
			}
			@Override
			public Adapter caseDesignUnit(DesignUnit object) {
				return createDesignUnitAdapter();
			}
			@Override
			public Adapter caseUseClause(UseClause object) {
				return createUseClauseAdapter();
			}
			@Override
			public Adapter caseLibraryUnit(LibraryUnit object) {
				return createLibraryUnitAdapter();
			}
			@Override
			public Adapter caseAccessTypeDefinition(AccessTypeDefinition object) {
				return createAccessTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseNameElement(NameElement object) {
				return createNameElementAdapter();
			}
			@Override
			public Adapter caseDeclaration(Declaration object) {
				return createDeclarationAdapter();
			}
			@Override
			public Adapter caseArchitecture(Architecture object) {
				return createArchitectureAdapter();
			}
			@Override
			public Adapter caseStatement(Statement object) {
				return createStatementAdapter();
			}
			@Override
			public Adapter caseArrayNatureDefinition(ArrayNatureDefinition object) {
				return createArrayNatureDefinitionAdapter();
			}
			@Override
			public Adapter caseArrayTypeDefinition(ArrayTypeDefinition object) {
				return createArrayTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseAssertionStatement(AssertionStatement object) {
				return createAssertionStatementAdapter();
			}
			@Override
			public Adapter caseExpression(Expression object) {
				return createExpressionAdapter();
			}
			@Override
			public Adapter caseChoice(Choice object) {
				return createChoiceAdapter();
			}
			@Override
			public Adapter caseOpen(Open object) {
				return createOpenAdapter();
			}
			@Override
			public Adapter caseAttributeDeclaration(AttributeDeclaration object) {
				return createAttributeDeclarationAdapter();
			}
			@Override
			public Adapter caseAttributeSpecification(AttributeSpecification object) {
				return createAttributeSpecificationAdapter();
			}
			@Override
			public Adapter caseNameList(NameList object) {
				return createNameListAdapter();
			}
			@Override
			public Adapter caseBlockConfiguration(BlockConfiguration object) {
				return createBlockConfigurationAdapter();
			}
			@Override
			public Adapter caseBlockStatement(BlockStatement object) {
				return createBlockStatementAdapter();
			}
			@Override
			public Adapter caseBreakElement(BreakElement object) {
				return createBreakElementAdapter();
			}
			@Override
			public Adapter caseBreakStatement(BreakStatement object) {
				return createBreakStatementAdapter();
			}
			@Override
			public Adapter caseCaseStatement(CaseStatement object) {
				return createCaseStatementAdapter();
			}
			@Override
			public Adapter caseCaseAlternative(CaseAlternative object) {
				return createCaseAlternativeAdapter();
			}
			@Override
			public Adapter caseComponentConfiguration(ComponentConfiguration object) {
				return createComponentConfigurationAdapter();
			}
			@Override
			public Adapter caseComponentDeclaration(ComponentDeclaration object) {
				return createComponentDeclarationAdapter();
			}
			@Override
			public Adapter caseInstantiationStatement(InstantiationStatement object) {
				return createInstantiationStatementAdapter();
			}
			@Override
			public Adapter caseComponentInstantiationStatement(ComponentInstantiationStatement object) {
				return createComponentInstantiationStatementAdapter();
			}
			@Override
			public Adapter caseEntityInstantiationStatement(EntityInstantiationStatement object) {
				return createEntityInstantiationStatementAdapter();
			}
			@Override
			public Adapter caseConfigurationInstantiationStatement(ConfigurationInstantiationStatement object) {
				return createConfigurationInstantiationStatementAdapter();
			}
			@Override
			public Adapter caseCompositeNatureDefinition(CompositeNatureDefinition object) {
				return createCompositeNatureDefinitionAdapter();
			}
			@Override
			public Adapter caseCompositeTypeDefinition(CompositeTypeDefinition object) {
				return createCompositeTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseSequentialSignalAssignmentStatement(SequentialSignalAssignmentStatement object) {
				return createSequentialSignalAssignmentStatementAdapter();
			}
			@Override
			public Adapter caseConditionalSignalAssignmentStatement(ConditionalSignalAssignmentStatement object) {
				return createConditionalSignalAssignmentStatementAdapter();
			}
			@Override
			public Adapter caseSelectedSignalAssignmentStatement(SelectedSignalAssignmentStatement object) {
				return createSelectedSignalAssignmentStatementAdapter();
			}
			@Override
			public Adapter caseSimpleSimultaneousStatement(SimpleSimultaneousStatement object) {
				return createSimpleSimultaneousStatementAdapter();
			}
			@Override
			public Adapter caseConditionalWaveform(ConditionalWaveform object) {
				return createConditionalWaveformAdapter();
			}
			@Override
			public Adapter caseConfigurationDeclaration(ConfigurationDeclaration object) {
				return createConfigurationDeclarationAdapter();
			}
			@Override
			public Adapter caseConfigurationItem(ConfigurationItem object) {
				return createConfigurationItemAdapter();
			}
			@Override
			public Adapter caseConfigurationSpecification(ConfigurationSpecification object) {
				return createConfigurationSpecificationAdapter();
			}
			@Override
			public Adapter caseDelayMechanism(DelayMechanism object) {
				return createDelayMechanismAdapter();
			}
			@Override
			public Adapter caseRejectMechanism(RejectMechanism object) {
				return createRejectMechanismAdapter();
			}
			@Override
			public Adapter caseTransport(Transport object) {
				return createTransportAdapter();
			}
			@Override
			public Adapter caseDisconnectionSpecification(DisconnectionSpecification object) {
				return createDisconnectionSpecificationAdapter();
			}
			@Override
			public Adapter caseElementDeclaration(ElementDeclaration object) {
				return createElementDeclarationAdapter();
			}
			@Override
			public Adapter caseEntityDeclaration(EntityDeclaration object) {
				return createEntityDeclarationAdapter();
			}
			@Override
			public Adapter caseEnumerationTypeDefinition(EnumerationTypeDefinition object) {
				return createEnumerationTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseExitStatement(ExitStatement object) {
				return createExitStatementAdapter();
			}
			@Override
			public Adapter caseFileDeclaration(FileDeclaration object) {
				return createFileDeclarationAdapter();
			}
			@Override
			public Adapter caseFileTypeDefinition(FileTypeDefinition object) {
				return createFileTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseGenerateStatement(GenerateStatement object) {
				return createGenerateStatementAdapter();
			}
			@Override
			public Adapter caseGenerationScheme(GenerationScheme object) {
				return createGenerationSchemeAdapter();
			}
			@Override
			public Adapter caseForGenerationScheme(ForGenerationScheme object) {
				return createForGenerationSchemeAdapter();
			}
			@Override
			public Adapter caseIfGenerationScheme(IfGenerationScheme object) {
				return createIfGenerationSchemeAdapter();
			}
			@Override
			public Adapter caseGenerics(Generics object) {
				return createGenericsAdapter();
			}
			@Override
			public Adapter caseGenericMaps(GenericMaps object) {
				return createGenericMapsAdapter();
			}
			@Override
			public Adapter caseGroupDeclaration(GroupDeclaration object) {
				return createGroupDeclarationAdapter();
			}
			@Override
			public Adapter caseGroupTemplateDeclaration(GroupTemplateDeclaration object) {
				return createGroupTemplateDeclarationAdapter();
			}
			@Override
			public Adapter caseIfStatement(IfStatement object) {
				return createIfStatementAdapter();
			}
			@Override
			public Adapter caseIfStatementTest(IfStatementTest object) {
				return createIfStatementTestAdapter();
			}
			@Override
			public Adapter caseConstraint(Constraint object) {
				return createConstraintAdapter();
			}
			@Override
			public Adapter caseDiscreteRange(DiscreteRange object) {
				return createDiscreteRangeAdapter();
			}
			@Override
			public Adapter caseLoopStatement(LoopStatement object) {
				return createLoopStatementAdapter();
			}
			@Override
			public Adapter caseIterationScheme(IterationScheme object) {
				return createIterationSchemeAdapter();
			}
			@Override
			public Adapter caseWhileIterationScheme(WhileIterationScheme object) {
				return createWhileIterationSchemeAdapter();
			}
			@Override
			public Adapter caseForIterationScheme(ForIterationScheme object) {
				return createForIterationSchemeAdapter();
			}
			@Override
			public Adapter caseAttribute(Attribute object) {
				return createAttributeAdapter();
			}
			@Override
			public Adapter caseNatureDeclaration(NatureDeclaration object) {
				return createNatureDeclarationAdapter();
			}
			@Override
			public Adapter caseNatureDefinition(NatureDefinition object) {
				return createNatureDefinitionAdapter();
			}
			@Override
			public Adapter caseNextStatement(NextStatement object) {
				return createNextStatementAdapter();
			}
			@Override
			public Adapter casePackageBody(PackageBody object) {
				return createPackageBodyAdapter();
			}
			@Override
			public Adapter casePackageDeclaration(PackageDeclaration object) {
				return createPackageDeclarationAdapter();
			}
			@Override
			public Adapter casePhysicalTypeDefinition(PhysicalTypeDefinition object) {
				return createPhysicalTypeDefinitionAdapter();
			}
			@Override
			public Adapter casePhysicalTypeDefinitionSecondary(PhysicalTypeDefinitionSecondary object) {
				return createPhysicalTypeDefinitionSecondaryAdapter();
			}
			@Override
			public Adapter casePorts(Ports object) {
				return createPortsAdapter();
			}
			@Override
			public Adapter casePortMaps(PortMaps object) {
				return createPortMapsAdapter();
			}
			@Override
			public Adapter caseProcedureCallStatement(ProcedureCallStatement object) {
				return createProcedureCallStatementAdapter();
			}
			@Override
			public Adapter caseProcessStatement(ProcessStatement object) {
				return createProcessStatementAdapter();
			}
			@Override
			public Adapter caseAllocator(Allocator object) {
				return createAllocatorAdapter();
			}
			@Override
			public Adapter caseRecordNatureDefinition(RecordNatureDefinition object) {
				return createRecordNatureDefinitionAdapter();
			}
			@Override
			public Adapter caseRecordNatureElement(RecordNatureElement object) {
				return createRecordNatureElementAdapter();
			}
			@Override
			public Adapter caseRecordTypeDefinition(RecordTypeDefinition object) {
				return createRecordTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseReportStatement(ReportStatement object) {
				return createReportStatementAdapter();
			}
			@Override
			public Adapter caseReturnStatement(ReturnStatement object) {
				return createReturnStatementAdapter();
			}
			@Override
			public Adapter caseScalarNatureDefinition(ScalarNatureDefinition object) {
				return createScalarNatureDefinitionAdapter();
			}
			@Override
			public Adapter caseNullStatement(NullStatement object) {
				return createNullStatementAdapter();
			}
			@Override
			public Adapter caseSignalAssignmentStatement(SignalAssignmentStatement object) {
				return createSignalAssignmentStatementAdapter();
			}
			@Override
			public Adapter caseVariableAssignmentStatement(VariableAssignmentStatement object) {
				return createVariableAssignmentStatementAdapter();
			}
			@Override
			public Adapter caseSignalDeclaration(SignalDeclaration object) {
				return createSignalDeclarationAdapter();
			}
			@Override
			public Adapter caseVariableDeclaration(VariableDeclaration object) {
				return createVariableDeclarationAdapter();
			}
			@Override
			public Adapter caseConstantDeclaration(ConstantDeclaration object) {
				return createConstantDeclarationAdapter();
			}
			@Override
			public Adapter caseSignature(Signature object) {
				return createSignatureAdapter();
			}
			@Override
			public Adapter caseSimultaneousAlternative(SimultaneousAlternative object) {
				return createSimultaneousAlternativeAdapter();
			}
			@Override
			public Adapter caseSimultaneousCaseStatement(SimultaneousCaseStatement object) {
				return createSimultaneousCaseStatementAdapter();
			}
			@Override
			public Adapter caseSimultaneousIfStatement(SimultaneousIfStatement object) {
				return createSimultaneousIfStatementAdapter();
			}
			@Override
			public Adapter caseSimultaneousIfStatementTest(SimultaneousIfStatementTest object) {
				return createSimultaneousIfStatementTestAdapter();
			}
			@Override
			public Adapter caseSimultaneousProceduralStatement(SimultaneousProceduralStatement object) {
				return createSimultaneousProceduralStatementAdapter();
			}
			@Override
			public Adapter caseSourceAspect(SourceAspect object) {
				return createSourceAspectAdapter();
			}
			@Override
			public Adapter caseSpectrum(Spectrum object) {
				return createSpectrumAdapter();
			}
			@Override
			public Adapter caseNoise(Noise object) {
				return createNoiseAdapter();
			}
			@Override
			public Adapter caseQuantityDeclaration(QuantityDeclaration object) {
				return createQuantityDeclarationAdapter();
			}
			@Override
			public Adapter caseFreeQuantityDeclaration(FreeQuantityDeclaration object) {
				return createFreeQuantityDeclarationAdapter();
			}
			@Override
			public Adapter caseBranchQuantityDeclaration(BranchQuantityDeclaration object) {
				return createBranchQuantityDeclarationAdapter();
			}
			@Override
			public Adapter caseQuantityAspect(QuantityAspect object) {
				return createQuantityAspectAdapter();
			}
			@Override
			public Adapter caseSourceQuantityDeclaration(SourceQuantityDeclaration object) {
				return createSourceQuantityDeclarationAdapter();
			}
			@Override
			public Adapter caseLimitSpecification(LimitSpecification object) {
				return createLimitSpecificationAdapter();
			}
			@Override
			public Adapter caseSubnatureDeclaration(SubnatureDeclaration object) {
				return createSubnatureDeclarationAdapter();
			}
			@Override
			public Adapter caseSubprogramDeclaration(SubprogramDeclaration object) {
				return createSubprogramDeclarationAdapter();
			}
			@Override
			public Adapter caseSubprogramSpecification(SubprogramSpecification object) {
				return createSubprogramSpecificationAdapter();
			}
			@Override
			public Adapter caseSubtypeDeclaration(SubtypeDeclaration object) {
				return createSubtypeDeclarationAdapter();
			}
			@Override
			public Adapter caseAliasDeclaration(AliasDeclaration object) {
				return createAliasDeclarationAdapter();
			}
			@Override
			public Adapter caseTerminalDeclaration(TerminalDeclaration object) {
				return createTerminalDeclarationAdapter();
			}
			@Override
			public Adapter caseTypeDeclaration(TypeDeclaration object) {
				return createTypeDeclarationAdapter();
			}
			@Override
			public Adapter caseTypeDefinition(TypeDefinition object) {
				return createTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseRangeTypeDefinition(RangeTypeDefinition object) {
				return createRangeTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseUnconstrainedArrayDefinition(UnconstrainedArrayDefinition object) {
				return createUnconstrainedArrayDefinitionAdapter();
			}
			@Override
			public Adapter caseUnconstrainedNatureDefinition(UnconstrainedNatureDefinition object) {
				return createUnconstrainedNatureDefinitionAdapter();
			}
			@Override
			public Adapter caseConstrainedArrayDefinition(ConstrainedArrayDefinition object) {
				return createConstrainedArrayDefinitionAdapter();
			}
			@Override
			public Adapter caseConstrainedNatureDefinition(ConstrainedNatureDefinition object) {
				return createConstrainedNatureDefinitionAdapter();
			}
			@Override
			public Adapter caseWaitStatement(WaitStatement object) {
				return createWaitStatementAdapter();
			}
			@Override
			public Adapter caseNull(Null object) {
				return createNullAdapter();
			}
			@Override
			public Adapter caseValue(Value object) {
				return createValueAdapter();
			}
			@Override
			public Adapter caseAggregate(Aggregate object) {
				return createAggregateAdapter();
			}
			@Override
			public Adapter caseAll(All object) {
				return createAllAdapter();
			}
			@Override
			public Adapter caseAssociation(Association object) {
				return createAssociationAdapter();
			}
			@Override
			public Adapter caseOthers(Others object) {
				return createOthersAdapter();
			}
			@Override
			public Adapter caseLogicalExpression(LogicalExpression object) {
				return createLogicalExpressionAdapter();
			}
			@Override
			public Adapter caseRelationalExpression(RelationalExpression object) {
				return createRelationalExpressionAdapter();
			}
			@Override
			public Adapter caseShiftExpression(ShiftExpression object) {
				return createShiftExpressionAdapter();
			}
			@Override
			public Adapter caseAddingExpression(AddingExpression object) {
				return createAddingExpressionAdapter();
			}
			@Override
			public Adapter caseMultiplyingExpression(MultiplyingExpression object) {
				return createMultiplyingExpressionAdapter();
			}
			@Override
			public Adapter casePowerExpression(PowerExpression object) {
				return createPowerExpressionAdapter();
			}
			@Override
			public Adapter caseUnaryExpression(UnaryExpression object) {
				return createUnaryExpressionAdapter();
			}
			@Override
			public Adapter caseSignExpression(SignExpression object) {
				return createSignExpressionAdapter();
			}
			@Override
			public Adapter caseRange(Range object) {
				return createRangeAdapter();
			}
			@Override
			public Adapter caseName(Name object) {
				return createNameAdapter();
			}
			@Override
			public Adapter caseBitString(BitString object) {
				return createBitStringAdapter();
			}
			@Override
			public Adapter caseString(fr.labsticc.language.vhdl.model.vhdl.String object) {
				return createStringAdapter();
			}
			@Override
			public Adapter caseCharacter(fr.labsticc.language.vhdl.model.vhdl.Character object) {
				return createCharacterAdapter();
			}
			@Override
			public Adapter caseIdentifier(Identifier object) {
				return createIdentifierAdapter();
			}
			@Override
			public Adapter caseIndication(Indication object) {
				return createIndicationAdapter();
			}
			@Override
			public Adapter caseWaveform(Waveform object) {
				return createWaveformAdapter();
			}
			@Override
			public Adapter caseUnitValue(UnitValue object) {
				return createUnitValueAdapter();
			}
			@Override
			public Adapter caseIdentifiedElement(IdentifiedElement object) {
				return createIdentifiedElementAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.VhdlFile <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlFile
	 * @generated
	 */
	public Adapter createVhdlFileAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.DesignUnit <em>Design Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DesignUnit
	 * @generated
	 */
	public Adapter createDesignUnitAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.UseClause <em>Use Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UseClause
	 * @generated
	 */
	public Adapter createUseClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.LibraryUnit <em>Library Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LibraryUnit
	 * @generated
	 */
	public Adapter createLibraryUnitAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.AccessTypeDefinition <em>Access Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AccessTypeDefinition
	 * @generated
	 */
	public Adapter createAccessTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.NameElement <em>Name Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NameElement
	 * @generated
	 */
	public Adapter createNameElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Declaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Declaration
	 * @generated
	 */
	public Adapter createDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Architecture <em>Architecture</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Architecture
	 * @generated
	 */
	public Adapter createArchitectureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Statement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Statement
	 * @generated
	 */
	public Adapter createStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ArrayNatureDefinition <em>Array Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ArrayNatureDefinition
	 * @generated
	 */
	public Adapter createArrayNatureDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ArrayTypeDefinition <em>Array Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ArrayTypeDefinition
	 * @generated
	 */
	public Adapter createArrayTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.AssertionStatement <em>Assertion Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AssertionStatement
	 * @generated
	 */
	public Adapter createAssertionStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Expression
	 * @generated
	 */
	public Adapter createExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Choice <em>Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Choice
	 * @generated
	 */
	public Adapter createChoiceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Open <em>Open</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Open
	 * @generated
	 */
	public Adapter createOpenAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.AttributeDeclaration <em>Attribute Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AttributeDeclaration
	 * @generated
	 */
	public Adapter createAttributeDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification <em>Attribute Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification
	 * @generated
	 */
	public Adapter createAttributeSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.NameList <em>Name List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NameList
	 * @generated
	 */
	public Adapter createNameListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration <em>Block Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration
	 * @generated
	 */
	public Adapter createBlockConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement <em>Block Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockStatement
	 * @generated
	 */
	public Adapter createBlockStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.BreakElement <em>Break Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BreakElement
	 * @generated
	 */
	public Adapter createBreakElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.BreakStatement <em>Break Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BreakStatement
	 * @generated
	 */
	public Adapter createBreakStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.CaseStatement <em>Case Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.CaseStatement
	 * @generated
	 */
	public Adapter createCaseStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.CaseAlternative <em>Case Alternative</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.CaseAlternative
	 * @generated
	 */
	public Adapter createCaseAlternativeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration <em>Component Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration
	 * @generated
	 */
	public Adapter createComponentConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration <em>Component Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration
	 * @generated
	 */
	public Adapter createComponentDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.InstantiationStatement <em>Instantiation Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.InstantiationStatement
	 * @generated
	 */
	public Adapter createInstantiationStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentInstantiationStatement <em>Component Instantiation Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentInstantiationStatement
	 * @generated
	 */
	public Adapter createComponentInstantiationStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.EntityInstantiationStatement <em>Entity Instantiation Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityInstantiationStatement
	 * @generated
	 */
	public Adapter createEntityInstantiationStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement <em>Configuration Instantiation Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement
	 * @generated
	 */
	public Adapter createConfigurationInstantiationStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.CompositeNatureDefinition <em>Composite Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.CompositeNatureDefinition
	 * @generated
	 */
	public Adapter createCompositeNatureDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.CompositeTypeDefinition <em>Composite Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.CompositeTypeDefinition
	 * @generated
	 */
	public Adapter createCompositeTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement <em>Sequential Signal Assignment Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement
	 * @generated
	 */
	public Adapter createSequentialSignalAssignmentStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement <em>Conditional Signal Assignment Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement
	 * @generated
	 */
	public Adapter createConditionalSignalAssignmentStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement <em>Selected Signal Assignment Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement
	 * @generated
	 */
	public Adapter createSelectedSignalAssignmentStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement <em>Simple Simultaneous Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement
	 * @generated
	 */
	public Adapter createSimpleSimultaneousStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform <em>Conditional Waveform</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform
	 * @generated
	 */
	public Adapter createConditionalWaveformAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration <em>Configuration Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration
	 * @generated
	 */
	public Adapter createConfigurationDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationItem <em>Configuration Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationItem
	 * @generated
	 */
	public Adapter createConfigurationItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification <em>Configuration Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification
	 * @generated
	 */
	public Adapter createConfigurationSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.DelayMechanism <em>Delay Mechanism</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DelayMechanism
	 * @generated
	 */
	public Adapter createDelayMechanismAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.RejectMechanism <em>Reject Mechanism</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RejectMechanism
	 * @generated
	 */
	public Adapter createRejectMechanismAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Transport <em>Transport</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Transport
	 * @generated
	 */
	public Adapter createTransportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification <em>Disconnection Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification
	 * @generated
	 */
	public Adapter createDisconnectionSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration <em>Element Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration
	 * @generated
	 */
	public Adapter createElementDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration <em>Entity Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration
	 * @generated
	 */
	public Adapter createEntityDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.EnumerationTypeDefinition <em>Enumeration Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EnumerationTypeDefinition
	 * @generated
	 */
	public Adapter createEnumerationTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ExitStatement <em>Exit Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ExitStatement
	 * @generated
	 */
	public Adapter createExitStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.FileDeclaration <em>File Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.FileDeclaration
	 * @generated
	 */
	public Adapter createFileDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.FileTypeDefinition <em>File Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.FileTypeDefinition
	 * @generated
	 */
	public Adapter createFileTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.GenerateStatement <em>Generate Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GenerateStatement
	 * @generated
	 */
	public Adapter createGenerateStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.GenerationScheme <em>Generation Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GenerationScheme
	 * @generated
	 */
	public Adapter createGenerationSchemeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ForGenerationScheme <em>For Generation Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ForGenerationScheme
	 * @generated
	 */
	public Adapter createForGenerationSchemeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.IfGenerationScheme <em>If Generation Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IfGenerationScheme
	 * @generated
	 */
	public Adapter createIfGenerationSchemeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Generics <em>Generics</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Generics
	 * @generated
	 */
	public Adapter createGenericsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.GenericMaps <em>Generic Maps</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GenericMaps
	 * @generated
	 */
	public Adapter createGenericMapsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.GroupDeclaration <em>Group Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GroupDeclaration
	 * @generated
	 */
	public Adapter createGroupDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.GroupTemplateDeclaration <em>Group Template Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GroupTemplateDeclaration
	 * @generated
	 */
	public Adapter createGroupTemplateDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.IfStatement <em>If Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IfStatement
	 * @generated
	 */
	public Adapter createIfStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.IfStatementTest <em>If Statement Test</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IfStatementTest
	 * @generated
	 */
	public Adapter createIfStatementTestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Constraint
	 * @generated
	 */
	public Adapter createConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.DiscreteRange <em>Discrete Range</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DiscreteRange
	 * @generated
	 */
	public Adapter createDiscreteRangeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.LoopStatement <em>Loop Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LoopStatement
	 * @generated
	 */
	public Adapter createLoopStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.IterationScheme <em>Iteration Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IterationScheme
	 * @generated
	 */
	public Adapter createIterationSchemeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.WhileIterationScheme <em>While Iteration Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.WhileIterationScheme
	 * @generated
	 */
	public Adapter createWhileIterationSchemeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme <em>For Iteration Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme
	 * @generated
	 */
	public Adapter createForIterationSchemeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Attribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Attribute
	 * @generated
	 */
	public Adapter createAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.NatureDeclaration <em>Nature Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NatureDeclaration
	 * @generated
	 */
	public Adapter createNatureDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.NatureDefinition <em>Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NatureDefinition
	 * @generated
	 */
	public Adapter createNatureDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.NextStatement <em>Next Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NextStatement
	 * @generated
	 */
	public Adapter createNextStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.PackageBody <em>Package Body</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PackageBody
	 * @generated
	 */
	public Adapter createPackageBodyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.PackageDeclaration <em>Package Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PackageDeclaration
	 * @generated
	 */
	public Adapter createPackageDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition <em>Physical Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition
	 * @generated
	 */
	public Adapter createPhysicalTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary <em>Physical Type Definition Secondary</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary
	 * @generated
	 */
	public Adapter createPhysicalTypeDefinitionSecondaryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Ports <em>Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Ports
	 * @generated
	 */
	public Adapter createPortsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.PortMaps <em>Port Maps</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PortMaps
	 * @generated
	 */
	public Adapter createPortMapsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ProcedureCallStatement <em>Procedure Call Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ProcedureCallStatement
	 * @generated
	 */
	public Adapter createProcedureCallStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ProcessStatement <em>Process Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ProcessStatement
	 * @generated
	 */
	public Adapter createProcessStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Allocator <em>Allocator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Allocator
	 * @generated
	 */
	public Adapter createAllocatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.RecordNatureDefinition <em>Record Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RecordNatureDefinition
	 * @generated
	 */
	public Adapter createRecordNatureDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement <em>Record Nature Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement
	 * @generated
	 */
	public Adapter createRecordNatureElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.RecordTypeDefinition <em>Record Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RecordTypeDefinition
	 * @generated
	 */
	public Adapter createRecordTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ReportStatement <em>Report Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ReportStatement
	 * @generated
	 */
	public Adapter createReportStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ReturnStatement <em>Return Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ReturnStatement
	 * @generated
	 */
	public Adapter createReturnStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition <em>Scalar Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition
	 * @generated
	 */
	public Adapter createScalarNatureDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.NullStatement <em>Null Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NullStatement
	 * @generated
	 */
	public Adapter createNullStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement <em>Signal Assignment Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement
	 * @generated
	 */
	public Adapter createSignalAssignmentStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.VariableAssignmentStatement <em>Variable Assignment Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VariableAssignmentStatement
	 * @generated
	 */
	public Adapter createVariableAssignmentStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration <em>Signal Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration
	 * @generated
	 */
	public Adapter createSignalDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration <em>Variable Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration
	 * @generated
	 */
	public Adapter createVariableDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ConstantDeclaration <em>Constant Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConstantDeclaration
	 * @generated
	 */
	public Adapter createConstantDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Signature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Signature
	 * @generated
	 */
	public Adapter createSignatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative <em>Simultaneous Alternative</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative
	 * @generated
	 */
	public Adapter createSimultaneousAlternativeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement <em>Simultaneous Case Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement
	 * @generated
	 */
	public Adapter createSimultaneousCaseStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement <em>Simultaneous If Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement
	 * @generated
	 */
	public Adapter createSimultaneousIfStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest <em>Simultaneous If Statement Test</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest
	 * @generated
	 */
	public Adapter createSimultaneousIfStatementTestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousProceduralStatement <em>Simultaneous Procedural Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousProceduralStatement
	 * @generated
	 */
	public Adapter createSimultaneousProceduralStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SourceAspect <em>Source Aspect</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SourceAspect
	 * @generated
	 */
	public Adapter createSourceAspectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Spectrum <em>Spectrum</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Spectrum
	 * @generated
	 */
	public Adapter createSpectrumAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Noise <em>Noise</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Noise
	 * @generated
	 */
	public Adapter createNoiseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.QuantityDeclaration <em>Quantity Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.QuantityDeclaration
	 * @generated
	 */
	public Adapter createQuantityDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration <em>Free Quantity Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration
	 * @generated
	 */
	public Adapter createFreeQuantityDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration <em>Branch Quantity Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration
	 * @generated
	 */
	public Adapter createBranchQuantityDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.QuantityAspect <em>Quantity Aspect</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.QuantityAspect
	 * @generated
	 */
	public Adapter createQuantityAspectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SourceQuantityDeclaration <em>Source Quantity Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SourceQuantityDeclaration
	 * @generated
	 */
	public Adapter createSourceQuantityDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.LimitSpecification <em>Limit Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LimitSpecification
	 * @generated
	 */
	public Adapter createLimitSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SubnatureDeclaration <em>Subnature Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubnatureDeclaration
	 * @generated
	 */
	public Adapter createSubnatureDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration <em>Subprogram Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration
	 * @generated
	 */
	public Adapter createSubprogramDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification <em>Subprogram Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification
	 * @generated
	 */
	public Adapter createSubprogramSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SubtypeDeclaration <em>Subtype Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubtypeDeclaration
	 * @generated
	 */
	public Adapter createSubtypeDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration <em>Alias Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration
	 * @generated
	 */
	public Adapter createAliasDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.TerminalDeclaration <em>Terminal Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.TerminalDeclaration
	 * @generated
	 */
	public Adapter createTerminalDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.TypeDeclaration <em>Type Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.TypeDeclaration
	 * @generated
	 */
	public Adapter createTypeDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.TypeDefinition <em>Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.TypeDefinition
	 * @generated
	 */
	public Adapter createTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition <em>Range Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition
	 * @generated
	 */
	public Adapter createRangeTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.UnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnconstrainedArrayDefinition
	 * @generated
	 */
	public Adapter createUnconstrainedArrayDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.UnconstrainedNatureDefinition <em>Unconstrained Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnconstrainedNatureDefinition
	 * @generated
	 */
	public Adapter createUnconstrainedNatureDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ConstrainedArrayDefinition <em>Constrained Array Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConstrainedArrayDefinition
	 * @generated
	 */
	public Adapter createConstrainedArrayDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ConstrainedNatureDefinition <em>Constrained Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConstrainedNatureDefinition
	 * @generated
	 */
	public Adapter createConstrainedNatureDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.WaitStatement <em>Wait Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.WaitStatement
	 * @generated
	 */
	public Adapter createWaitStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Null <em>Null</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Null
	 * @generated
	 */
	public Adapter createNullAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Value <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Value
	 * @generated
	 */
	public Adapter createValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Aggregate <em>Aggregate</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Aggregate
	 * @generated
	 */
	public Adapter createAggregateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Association <em>Association</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Association
	 * @generated
	 */
	public Adapter createAssociationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.All <em>All</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.All
	 * @generated
	 */
	public Adapter createAllAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Others <em>Others</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Others
	 * @generated
	 */
	public Adapter createOthersAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.LogicalExpression <em>Logical Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LogicalExpression
	 * @generated
	 */
	public Adapter createLogicalExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.RelationalExpression <em>Relational Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RelationalExpression
	 * @generated
	 */
	public Adapter createRelationalExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.ShiftExpression <em>Shift Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ShiftExpression
	 * @generated
	 */
	public Adapter createShiftExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.AddingExpression <em>Adding Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AddingExpression
	 * @generated
	 */
	public Adapter createAddingExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression <em>Multiplying Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression
	 * @generated
	 */
	public Adapter createMultiplyingExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.PowerExpression <em>Power Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PowerExpression
	 * @generated
	 */
	public Adapter createPowerExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.UnaryExpression <em>Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnaryExpression
	 * @generated
	 */
	public Adapter createUnaryExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.SignExpression <em>Sign Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignExpression
	 * @generated
	 */
	public Adapter createSignExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Range <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Range
	 * @generated
	 */
	public Adapter createRangeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Name <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Name
	 * @generated
	 */
	public Adapter createNameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.String
	 * @generated
	 */
	public Adapter createStringAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Character <em>Character</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Character
	 * @generated
	 */
	public Adapter createCharacterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Identifier <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Identifier
	 * @generated
	 */
	public Adapter createIdentifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Indication <em>Indication</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Indication
	 * @generated
	 */
	public Adapter createIndicationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.Waveform <em>Waveform</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Waveform
	 * @generated
	 */
	public Adapter createWaveformAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.BitString <em>Bit String</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BitString
	 * @generated
	 */
	public Adapter createBitStringAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.UnitValue <em>Unit Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnitValue
	 * @generated
	 */
	public Adapter createUnitValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.IdentifiedElement <em>Identified Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IdentifiedElement
	 * @generated
	 */
	public Adapter createIdentifiedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.language.vhdl.model.vhdl.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //VhdlAdapterFactory
