/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import java.lang.String;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Type Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition#getRange <em>Range</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition#getPrimary <em>Primary</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition#getSecondary <em>Secondary</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getPhysicalTypeDefinition()
 * @model
 * @generated
 */
public interface PhysicalTypeDefinition extends TypeDefinition {
	/**
	 * Returns the value of the '<em><b>Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range</em>' containment reference.
	 * @see #setRange(Constraint)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getPhysicalTypeDefinition_Range()
	 * @model containment="true"
	 * @generated
	 */
	Constraint getRange();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition#getRange <em>Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range</em>' containment reference.
	 * @see #getRange()
	 * @generated
	 */
	void setRange(Constraint value);

	/**
	 * Returns the value of the '<em><b>Primary</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Primary</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Primary</em>' attribute.
	 * @see #setPrimary(String)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getPhysicalTypeDefinition_Primary()
	 * @model
	 * @generated
	 */
	String getPrimary();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition#getPrimary <em>Primary</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Primary</em>' attribute.
	 * @see #getPrimary()
	 * @generated
	 */
	void setPrimary(String value);

	/**
	 * Returns the value of the '<em><b>Secondary</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Secondary</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Secondary</em>' containment reference list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getPhysicalTypeDefinition_Secondary()
	 * @model containment="true"
	 * @generated
	 */
	EList<PhysicalTypeDefinitionSecondary> getSecondary();

} // PhysicalTypeDefinition
