/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.Expression;
import fr.labsticc.language.vhdl.model.vhdl.GenericMaps;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;
import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Maps</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.GenericMapsImpl#getGeneric <em>Generic</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GenericMapsImpl extends IdentifiedElementImpl implements GenericMaps {
	/**
	 * The cached value of the '{@link #getGeneric() <em>Generic</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneric()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> generic;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GenericMapsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VhdlPackage.eINSTANCE.getGenericMaps();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getGeneric() {
		if (generic == null) {
			generic = new EObjectContainmentEList<Expression>(Expression.class, this, VhdlPackage.GENERIC_MAPS__GENERIC);
		}
		return generic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VhdlPackage.GENERIC_MAPS__GENERIC:
				return ((InternalEList<?>)getGeneric()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VhdlPackage.GENERIC_MAPS__GENERIC:
				return getGeneric();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VhdlPackage.GENERIC_MAPS__GENERIC:
				getGeneric().clear();
				getGeneric().addAll((Collection<? extends Expression>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VhdlPackage.GENERIC_MAPS__GENERIC:
				getGeneric().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VhdlPackage.GENERIC_MAPS__GENERIC:
				return generic != null && !generic.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //GenericMapsImpl
