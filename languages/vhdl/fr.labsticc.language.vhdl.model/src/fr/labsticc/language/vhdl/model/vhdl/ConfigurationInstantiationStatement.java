/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration Instantiation Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement#getGenericMap <em>Generic Map</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement#getPortMap <em>Port Map</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationInstantiationStatement()
 * @model
 * @generated
 */
public interface ConfigurationInstantiationStatement extends Statement {
	/**
	 * Returns the value of the '<em><b>Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configuration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configuration</em>' containment reference.
	 * @see #setConfiguration(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationInstantiationStatement_Configuration()
	 * @model containment="true"
	 * @generated
	 */
	Expression getConfiguration();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement#getConfiguration <em>Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configuration</em>' containment reference.
	 * @see #getConfiguration()
	 * @generated
	 */
	void setConfiguration(Expression value);

	/**
	 * Returns the value of the '<em><b>Generic Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Map</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Map</em>' containment reference.
	 * @see #setGenericMap(GenericMaps)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationInstantiationStatement_GenericMap()
	 * @model containment="true"
	 * @generated
	 */
	GenericMaps getGenericMap();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement#getGenericMap <em>Generic Map</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Map</em>' containment reference.
	 * @see #getGenericMap()
	 * @generated
	 */
	void setGenericMap(GenericMaps value);

	/**
	 * Returns the value of the '<em><b>Port Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Map</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Map</em>' containment reference.
	 * @see #setPortMap(PortMaps)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationInstantiationStatement_PortMap()
	 * @model containment="true"
	 * @generated
	 */
	PortMaps getPortMap();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement#getPortMap <em>Port Map</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Map</em>' containment reference.
	 * @see #getPortMap()
	 * @generated
	 */
	void setPortMap(PortMaps value);

} // ConfigurationInstantiationStatement
