/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Noise</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.Noise#getNoise <em>Noise</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getNoise()
 * @model
 * @generated
 */
public interface Noise extends SourceAspect {
	/**
	 * Returns the value of the '<em><b>Noise</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Noise</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Noise</em>' containment reference.
	 * @see #setNoise(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getNoise_Noise()
	 * @model containment="true"
	 * @generated
	 */
	Expression getNoise();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.Noise#getNoise <em>Noise</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Noise</em>' containment reference.
	 * @see #getNoise()
	 * @generated
	 */
	void setNoise(Expression value);

} // Noise
