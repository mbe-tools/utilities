/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.Choice;
import fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform;
import fr.labsticc.language.vhdl.model.vhdl.Expression;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;
import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conditional Waveform</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConditionalWaveformImpl#getWaveform <em>Waveform</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConditionalWaveformImpl#getChoice <em>Choice</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConditionalWaveformImpl extends IdentifiedElementImpl implements ConditionalWaveform {
	/**
	 * The cached value of the '{@link #getWaveform() <em>Waveform</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWaveform()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> waveform;

	/**
	 * The cached value of the '{@link #getChoice() <em>Choice</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoice()
	 * @generated
	 * @ordered
	 */
	protected EList<Choice> choice;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionalWaveformImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VhdlPackage.eINSTANCE.getConditionalWaveform();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getWaveform() {
		if (waveform == null) {
			waveform = new EObjectContainmentEList<Expression>(Expression.class, this, VhdlPackage.CONDITIONAL_WAVEFORM__WAVEFORM);
		}
		return waveform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Choice> getChoice() {
		if (choice == null) {
			choice = new EObjectContainmentEList<Choice>(Choice.class, this, VhdlPackage.CONDITIONAL_WAVEFORM__CHOICE);
		}
		return choice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VhdlPackage.CONDITIONAL_WAVEFORM__WAVEFORM:
				return ((InternalEList<?>)getWaveform()).basicRemove(otherEnd, msgs);
			case VhdlPackage.CONDITIONAL_WAVEFORM__CHOICE:
				return ((InternalEList<?>)getChoice()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VhdlPackage.CONDITIONAL_WAVEFORM__WAVEFORM:
				return getWaveform();
			case VhdlPackage.CONDITIONAL_WAVEFORM__CHOICE:
				return getChoice();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VhdlPackage.CONDITIONAL_WAVEFORM__WAVEFORM:
				getWaveform().clear();
				getWaveform().addAll((Collection<? extends Expression>)newValue);
				return;
			case VhdlPackage.CONDITIONAL_WAVEFORM__CHOICE:
				getChoice().clear();
				getChoice().addAll((Collection<? extends Choice>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VhdlPackage.CONDITIONAL_WAVEFORM__WAVEFORM:
				getWaveform().clear();
				return;
			case VhdlPackage.CONDITIONAL_WAVEFORM__CHOICE:
				getChoice().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VhdlPackage.CONDITIONAL_WAVEFORM__WAVEFORM:
				return waveform != null && !waveform.isEmpty();
			case VhdlPackage.CONDITIONAL_WAVEFORM__CHOICE:
				return choice != null && !choice.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ConditionalWaveformImpl
