/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration#getGeneric <em>Generic</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration#getPort <em>Port</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration#getStatement <em>Statement</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getEntityDeclaration()
 * @model
 * @generated
 */
public interface EntityDeclaration extends LibraryUnit {
	/**
	 * Returns the value of the '<em><b>Generic</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic</em>' containment reference.
	 * @see #setGeneric(Generics)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getEntityDeclaration_Generic()
	 * @model containment="true"
	 * @generated
	 */
	Generics getGeneric();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration#getGeneric <em>Generic</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic</em>' containment reference.
	 * @see #getGeneric()
	 * @generated
	 */
	void setGeneric(Generics value);

	/**
	 * Returns the value of the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' containment reference.
	 * @see #setPort(Ports)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getEntityDeclaration_Port()
	 * @model containment="true"
	 * @generated
	 */
	Ports getPort();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration#getPort <em>Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' containment reference.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(Ports value);

	/**
	 * Returns the value of the '<em><b>Statement</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.Statement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statement</em>' containment reference list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getEntityDeclaration_Statement()
	 * @model containment="true"
	 * @generated
	 */
	EList<Statement> getStatement();

} // EntityDeclaration
