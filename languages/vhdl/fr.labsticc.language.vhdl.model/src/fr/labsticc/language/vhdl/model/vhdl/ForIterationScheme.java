/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import java.lang.String;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Iteration Scheme</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme#getVariable <em>Variable</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme#getIn <em>In</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getForIterationScheme()
 * @model
 * @generated
 */
public interface ForIterationScheme extends IterationScheme {
	/**
	 * Returns the value of the '<em><b>Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable</em>' attribute.
	 * @see #setVariable(String)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getForIterationScheme_Variable()
	 * @model
	 * @generated
	 */
	String getVariable();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme#getVariable <em>Variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable</em>' attribute.
	 * @see #getVariable()
	 * @generated
	 */
	void setVariable(String value);

	/**
	 * Returns the value of the '<em><b>In</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In</em>' containment reference.
	 * @see #setIn(DiscreteRange)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getForIterationScheme_In()
	 * @model containment="true"
	 * @generated
	 */
	DiscreteRange getIn();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme#getIn <em>In</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In</em>' containment reference.
	 * @see #getIn()
	 * @generated
	 */
	void setIn(DiscreteRange value);

} // ForIterationScheme
