/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generics</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.Generics#getDeclaration <em>Declaration</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getGenerics()
 * @model
 * @generated
 */
public interface Generics extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.Declaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Declaration</em>' containment reference list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getGenerics_Declaration()
	 * @model containment="true"
	 * @generated
	 */
	EList<Declaration> getDeclaration();

} // Generics
