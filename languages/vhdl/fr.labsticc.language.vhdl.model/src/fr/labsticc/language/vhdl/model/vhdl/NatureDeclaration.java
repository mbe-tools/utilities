/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nature Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.NatureDeclaration#getIs <em>Is</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getNatureDeclaration()
 * @model
 * @generated
 */
public interface NatureDeclaration extends Declaration {
	/**
	 * Returns the value of the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is</em>' containment reference.
	 * @see #setIs(NatureDefinition)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getNatureDeclaration_Is()
	 * @model containment="true"
	 * @generated
	 */
	NatureDefinition getIs();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.NatureDeclaration#getIs <em>Is</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is</em>' containment reference.
	 * @see #getIs()
	 * @generated
	 */
	void setIs(NatureDefinition value);

} // NatureDeclaration
