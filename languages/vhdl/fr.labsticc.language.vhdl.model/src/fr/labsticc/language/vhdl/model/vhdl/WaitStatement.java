/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Wait Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.WaitStatement#getSensitivity <em>Sensitivity</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.WaitStatement#getUntil <em>Until</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.WaitStatement#getTime <em>Time</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getWaitStatement()
 * @model
 * @generated
 */
public interface WaitStatement extends Statement {
	/**
	 * Returns the value of the '<em><b>Sensitivity</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sensitivity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sensitivity</em>' containment reference list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getWaitStatement_Sensitivity()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getSensitivity();

	/**
	 * Returns the value of the '<em><b>Until</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Until</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Until</em>' containment reference.
	 * @see #setUntil(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getWaitStatement_Until()
	 * @model containment="true"
	 * @generated
	 */
	Expression getUntil();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.WaitStatement#getUntil <em>Until</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Until</em>' containment reference.
	 * @see #getUntil()
	 * @generated
	 */
	void setUntil(Expression value);

	/**
	 * Returns the value of the '<em><b>Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time</em>' containment reference.
	 * @see #setTime(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getWaitStatement_Time()
	 * @model containment="true"
	 * @generated
	 */
	Expression getTime();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.WaitStatement#getTime <em>Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time</em>' containment reference.
	 * @see #getTime()
	 * @generated
	 */
	void setTime(Expression value);

} // WaitStatement
