/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.BreakElement;
import fr.labsticc.language.vhdl.model.vhdl.BreakStatement;
import fr.labsticc.language.vhdl.model.vhdl.Expression;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Break Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.BreakStatementImpl#getBreak <em>Break</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.BreakStatementImpl#getWhen <em>When</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.BreakStatementImpl#getSensitivity <em>Sensitivity</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BreakStatementImpl extends StatementImpl implements BreakStatement {
	/**
	 * The cached value of the '{@link #getBreak() <em>Break</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBreak()
	 * @generated
	 * @ordered
	 */
	protected EList<BreakElement> break_;

	/**
	 * The cached value of the '{@link #getWhen() <em>When</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWhen()
	 * @generated
	 * @ordered
	 */
	protected Expression when;

	/**
	 * The cached value of the '{@link #getSensitivity() <em>Sensitivity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensitivity()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> sensitivity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BreakStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VhdlPackage.eINSTANCE.getBreakStatement();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BreakElement> getBreak() {
		if (break_ == null) {
			break_ = new EObjectContainmentEList<BreakElement>(BreakElement.class, this, VhdlPackage.BREAK_STATEMENT__BREAK);
		}
		return break_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getWhen() {
		return when;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWhen(Expression newWhen, NotificationChain msgs) {
		Expression oldWhen = when;
		when = newWhen;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.BREAK_STATEMENT__WHEN, oldWhen, newWhen);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWhen(Expression newWhen) {
		if (newWhen != when) {
			NotificationChain msgs = null;
			if (when != null)
				msgs = ((InternalEObject)when).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.BREAK_STATEMENT__WHEN, null, msgs);
			if (newWhen != null)
				msgs = ((InternalEObject)newWhen).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.BREAK_STATEMENT__WHEN, null, msgs);
			msgs = basicSetWhen(newWhen, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.BREAK_STATEMENT__WHEN, newWhen, newWhen));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getSensitivity() {
		if (sensitivity == null) {
			sensitivity = new EObjectContainmentEList<Expression>(Expression.class, this, VhdlPackage.BREAK_STATEMENT__SENSITIVITY);
		}
		return sensitivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VhdlPackage.BREAK_STATEMENT__BREAK:
				return ((InternalEList<?>)getBreak()).basicRemove(otherEnd, msgs);
			case VhdlPackage.BREAK_STATEMENT__WHEN:
				return basicSetWhen(null, msgs);
			case VhdlPackage.BREAK_STATEMENT__SENSITIVITY:
				return ((InternalEList<?>)getSensitivity()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VhdlPackage.BREAK_STATEMENT__BREAK:
				return getBreak();
			case VhdlPackage.BREAK_STATEMENT__WHEN:
				return getWhen();
			case VhdlPackage.BREAK_STATEMENT__SENSITIVITY:
				return getSensitivity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VhdlPackage.BREAK_STATEMENT__BREAK:
				getBreak().clear();
				getBreak().addAll((Collection<? extends BreakElement>)newValue);
				return;
			case VhdlPackage.BREAK_STATEMENT__WHEN:
				setWhen((Expression)newValue);
				return;
			case VhdlPackage.BREAK_STATEMENT__SENSITIVITY:
				getSensitivity().clear();
				getSensitivity().addAll((Collection<? extends Expression>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VhdlPackage.BREAK_STATEMENT__BREAK:
				getBreak().clear();
				return;
			case VhdlPackage.BREAK_STATEMENT__WHEN:
				setWhen((Expression)null);
				return;
			case VhdlPackage.BREAK_STATEMENT__SENSITIVITY:
				getSensitivity().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VhdlPackage.BREAK_STATEMENT__BREAK:
				return break_ != null && !break_.isEmpty();
			case VhdlPackage.BREAK_STATEMENT__WHEN:
				return when != null;
			case VhdlPackage.BREAK_STATEMENT__SENSITIVITY:
				return sensitivity != null && !sensitivity.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //BreakStatementImpl
