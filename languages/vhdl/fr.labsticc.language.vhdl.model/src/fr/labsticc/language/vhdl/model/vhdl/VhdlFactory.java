/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage
 * @generated
 */
public interface VhdlFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VhdlFactory eINSTANCE = fr.labsticc.language.vhdl.model.vhdl.impl.VhdlFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>File</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>File</em>'.
	 * @generated
	 */
	VhdlFile createVhdlFile();

	/**
	 * Returns a new object of class '<em>Design Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Design Unit</em>'.
	 * @generated
	 */
	DesignUnit createDesignUnit();

	/**
	 * Returns a new object of class '<em>Use Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Use Clause</em>'.
	 * @generated
	 */
	UseClause createUseClause();

	/**
	 * Returns a new object of class '<em>Library Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Library Unit</em>'.
	 * @generated
	 */
	LibraryUnit createLibraryUnit();

	/**
	 * Returns a new object of class '<em>Access Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access Type Definition</em>'.
	 * @generated
	 */
	AccessTypeDefinition createAccessTypeDefinition();

	/**
	 * Returns a new object of class '<em>Name Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Name Element</em>'.
	 * @generated
	 */
	NameElement createNameElement();

	/**
	 * Returns a new object of class '<em>Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Declaration</em>'.
	 * @generated
	 */
	Declaration createDeclaration();

	/**
	 * Returns a new object of class '<em>Architecture</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Architecture</em>'.
	 * @generated
	 */
	Architecture createArchitecture();

	/**
	 * Returns a new object of class '<em>Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Statement</em>'.
	 * @generated
	 */
	Statement createStatement();

	/**
	 * Returns a new object of class '<em>Array Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Array Nature Definition</em>'.
	 * @generated
	 */
	ArrayNatureDefinition createArrayNatureDefinition();

	/**
	 * Returns a new object of class '<em>Array Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Array Type Definition</em>'.
	 * @generated
	 */
	ArrayTypeDefinition createArrayTypeDefinition();

	/**
	 * Returns a new object of class '<em>Assertion Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assertion Statement</em>'.
	 * @generated
	 */
	AssertionStatement createAssertionStatement();

	/**
	 * Returns a new object of class '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expression</em>'.
	 * @generated
	 */
	Expression createExpression();

	/**
	 * Returns a new object of class '<em>Open</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Open</em>'.
	 * @generated
	 */
	Open createOpen();

	/**
	 * Returns a new object of class '<em>Attribute Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute Declaration</em>'.
	 * @generated
	 */
	AttributeDeclaration createAttributeDeclaration();

	/**
	 * Returns a new object of class '<em>Attribute Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute Specification</em>'.
	 * @generated
	 */
	AttributeSpecification createAttributeSpecification();

	/**
	 * Returns a new object of class '<em>Name List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Name List</em>'.
	 * @generated
	 */
	NameList createNameList();

	/**
	 * Returns a new object of class '<em>Block Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Block Configuration</em>'.
	 * @generated
	 */
	BlockConfiguration createBlockConfiguration();

	/**
	 * Returns a new object of class '<em>Block Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Block Statement</em>'.
	 * @generated
	 */
	BlockStatement createBlockStatement();

	/**
	 * Returns a new object of class '<em>Break Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Break Element</em>'.
	 * @generated
	 */
	BreakElement createBreakElement();

	/**
	 * Returns a new object of class '<em>Break Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Break Statement</em>'.
	 * @generated
	 */
	BreakStatement createBreakStatement();

	/**
	 * Returns a new object of class '<em>Case Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Case Statement</em>'.
	 * @generated
	 */
	CaseStatement createCaseStatement();

	/**
	 * Returns a new object of class '<em>Case Alternative</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Case Alternative</em>'.
	 * @generated
	 */
	CaseAlternative createCaseAlternative();

	/**
	 * Returns a new object of class '<em>Component Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Configuration</em>'.
	 * @generated
	 */
	ComponentConfiguration createComponentConfiguration();

	/**
	 * Returns a new object of class '<em>Component Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Declaration</em>'.
	 * @generated
	 */
	ComponentDeclaration createComponentDeclaration();

	/**
	 * Returns a new object of class '<em>Component Instantiation Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Instantiation Statement</em>'.
	 * @generated
	 */
	ComponentInstantiationStatement createComponentInstantiationStatement();

	/**
	 * Returns a new object of class '<em>Entity Instantiation Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Entity Instantiation Statement</em>'.
	 * @generated
	 */
	EntityInstantiationStatement createEntityInstantiationStatement();

	/**
	 * Returns a new object of class '<em>Configuration Instantiation Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Configuration Instantiation Statement</em>'.
	 * @generated
	 */
	ConfigurationInstantiationStatement createConfigurationInstantiationStatement();

	/**
	 * Returns a new object of class '<em>Composite Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composite Nature Definition</em>'.
	 * @generated
	 */
	CompositeNatureDefinition createCompositeNatureDefinition();

	/**
	 * Returns a new object of class '<em>Composite Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composite Type Definition</em>'.
	 * @generated
	 */
	CompositeTypeDefinition createCompositeTypeDefinition();

	/**
	 * Returns a new object of class '<em>Sequential Signal Assignment Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sequential Signal Assignment Statement</em>'.
	 * @generated
	 */
	SequentialSignalAssignmentStatement createSequentialSignalAssignmentStatement();

	/**
	 * Returns a new object of class '<em>Conditional Signal Assignment Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conditional Signal Assignment Statement</em>'.
	 * @generated
	 */
	ConditionalSignalAssignmentStatement createConditionalSignalAssignmentStatement();

	/**
	 * Returns a new object of class '<em>Selected Signal Assignment Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Selected Signal Assignment Statement</em>'.
	 * @generated
	 */
	SelectedSignalAssignmentStatement createSelectedSignalAssignmentStatement();

	/**
	 * Returns a new object of class '<em>Simple Simultaneous Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple Simultaneous Statement</em>'.
	 * @generated
	 */
	SimpleSimultaneousStatement createSimpleSimultaneousStatement();

	/**
	 * Returns a new object of class '<em>Conditional Waveform</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conditional Waveform</em>'.
	 * @generated
	 */
	ConditionalWaveform createConditionalWaveform();

	/**
	 * Returns a new object of class '<em>Configuration Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Configuration Declaration</em>'.
	 * @generated
	 */
	ConfigurationDeclaration createConfigurationDeclaration();

	/**
	 * Returns a new object of class '<em>Configuration Item</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Configuration Item</em>'.
	 * @generated
	 */
	ConfigurationItem createConfigurationItem();

	/**
	 * Returns a new object of class '<em>Configuration Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Configuration Specification</em>'.
	 * @generated
	 */
	ConfigurationSpecification createConfigurationSpecification();

	/**
	 * Returns a new object of class '<em>Delay Mechanism</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Delay Mechanism</em>'.
	 * @generated
	 */
	DelayMechanism createDelayMechanism();

	/**
	 * Returns a new object of class '<em>Reject Mechanism</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reject Mechanism</em>'.
	 * @generated
	 */
	RejectMechanism createRejectMechanism();

	/**
	 * Returns a new object of class '<em>Transport</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transport</em>'.
	 * @generated
	 */
	Transport createTransport();

	/**
	 * Returns a new object of class '<em>Disconnection Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Disconnection Specification</em>'.
	 * @generated
	 */
	DisconnectionSpecification createDisconnectionSpecification();

	/**
	 * Returns a new object of class '<em>Element Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element Declaration</em>'.
	 * @generated
	 */
	ElementDeclaration createElementDeclaration();

	/**
	 * Returns a new object of class '<em>Entity Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Entity Declaration</em>'.
	 * @generated
	 */
	EntityDeclaration createEntityDeclaration();

	/**
	 * Returns a new object of class '<em>Enumeration Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enumeration Type Definition</em>'.
	 * @generated
	 */
	EnumerationTypeDefinition createEnumerationTypeDefinition();

	/**
	 * Returns a new object of class '<em>Exit Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exit Statement</em>'.
	 * @generated
	 */
	ExitStatement createExitStatement();

	/**
	 * Returns a new object of class '<em>File Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>File Declaration</em>'.
	 * @generated
	 */
	FileDeclaration createFileDeclaration();

	/**
	 * Returns a new object of class '<em>File Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>File Type Definition</em>'.
	 * @generated
	 */
	FileTypeDefinition createFileTypeDefinition();

	/**
	 * Returns a new object of class '<em>Generate Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generate Statement</em>'.
	 * @generated
	 */
	GenerateStatement createGenerateStatement();

	/**
	 * Returns a new object of class '<em>Generation Scheme</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generation Scheme</em>'.
	 * @generated
	 */
	GenerationScheme createGenerationScheme();

	/**
	 * Returns a new object of class '<em>For Generation Scheme</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>For Generation Scheme</em>'.
	 * @generated
	 */
	ForGenerationScheme createForGenerationScheme();

	/**
	 * Returns a new object of class '<em>If Generation Scheme</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If Generation Scheme</em>'.
	 * @generated
	 */
	IfGenerationScheme createIfGenerationScheme();

	/**
	 * Returns a new object of class '<em>Generics</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generics</em>'.
	 * @generated
	 */
	Generics createGenerics();

	/**
	 * Returns a new object of class '<em>Generic Maps</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generic Maps</em>'.
	 * @generated
	 */
	GenericMaps createGenericMaps();

	/**
	 * Returns a new object of class '<em>Group Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Group Declaration</em>'.
	 * @generated
	 */
	GroupDeclaration createGroupDeclaration();

	/**
	 * Returns a new object of class '<em>Group Template Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Group Template Declaration</em>'.
	 * @generated
	 */
	GroupTemplateDeclaration createGroupTemplateDeclaration();

	/**
	 * Returns a new object of class '<em>If Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If Statement</em>'.
	 * @generated
	 */
	IfStatement createIfStatement();

	/**
	 * Returns a new object of class '<em>If Statement Test</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If Statement Test</em>'.
	 * @generated
	 */
	IfStatementTest createIfStatementTest();

	/**
	 * Returns a new object of class '<em>Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constraint</em>'.
	 * @generated
	 */
	Constraint createConstraint();

	/**
	 * Returns a new object of class '<em>Loop Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Loop Statement</em>'.
	 * @generated
	 */
	LoopStatement createLoopStatement();

	/**
	 * Returns a new object of class '<em>Iteration Scheme</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iteration Scheme</em>'.
	 * @generated
	 */
	IterationScheme createIterationScheme();

	/**
	 * Returns a new object of class '<em>While Iteration Scheme</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>While Iteration Scheme</em>'.
	 * @generated
	 */
	WhileIterationScheme createWhileIterationScheme();

	/**
	 * Returns a new object of class '<em>For Iteration Scheme</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>For Iteration Scheme</em>'.
	 * @generated
	 */
	ForIterationScheme createForIterationScheme();

	/**
	 * Returns a new object of class '<em>Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute</em>'.
	 * @generated
	 */
	Attribute createAttribute();

	/**
	 * Returns a new object of class '<em>Nature Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Nature Declaration</em>'.
	 * @generated
	 */
	NatureDeclaration createNatureDeclaration();

	/**
	 * Returns a new object of class '<em>Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Nature Definition</em>'.
	 * @generated
	 */
	NatureDefinition createNatureDefinition();

	/**
	 * Returns a new object of class '<em>Next Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Next Statement</em>'.
	 * @generated
	 */
	NextStatement createNextStatement();

	/**
	 * Returns a new object of class '<em>Package Body</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Package Body</em>'.
	 * @generated
	 */
	PackageBody createPackageBody();

	/**
	 * Returns a new object of class '<em>Package Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Package Declaration</em>'.
	 * @generated
	 */
	PackageDeclaration createPackageDeclaration();

	/**
	 * Returns a new object of class '<em>Physical Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Type Definition</em>'.
	 * @generated
	 */
	PhysicalTypeDefinition createPhysicalTypeDefinition();

	/**
	 * Returns a new object of class '<em>Physical Type Definition Secondary</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Type Definition Secondary</em>'.
	 * @generated
	 */
	PhysicalTypeDefinitionSecondary createPhysicalTypeDefinitionSecondary();

	/**
	 * Returns a new object of class '<em>Ports</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ports</em>'.
	 * @generated
	 */
	Ports createPorts();

	/**
	 * Returns a new object of class '<em>Port Maps</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Maps</em>'.
	 * @generated
	 */
	PortMaps createPortMaps();

	/**
	 * Returns a new object of class '<em>Procedure Call Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure Call Statement</em>'.
	 * @generated
	 */
	ProcedureCallStatement createProcedureCallStatement();

	/**
	 * Returns a new object of class '<em>Process Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Statement</em>'.
	 * @generated
	 */
	ProcessStatement createProcessStatement();

	/**
	 * Returns a new object of class '<em>Allocator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Allocator</em>'.
	 * @generated
	 */
	Allocator createAllocator();

	/**
	 * Returns a new object of class '<em>Record Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record Nature Definition</em>'.
	 * @generated
	 */
	RecordNatureDefinition createRecordNatureDefinition();

	/**
	 * Returns a new object of class '<em>Record Nature Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record Nature Element</em>'.
	 * @generated
	 */
	RecordNatureElement createRecordNatureElement();

	/**
	 * Returns a new object of class '<em>Record Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record Type Definition</em>'.
	 * @generated
	 */
	RecordTypeDefinition createRecordTypeDefinition();

	/**
	 * Returns a new object of class '<em>Report Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Report Statement</em>'.
	 * @generated
	 */
	ReportStatement createReportStatement();

	/**
	 * Returns a new object of class '<em>Return Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Return Statement</em>'.
	 * @generated
	 */
	ReturnStatement createReturnStatement();

	/**
	 * Returns a new object of class '<em>Scalar Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Scalar Nature Definition</em>'.
	 * @generated
	 */
	ScalarNatureDefinition createScalarNatureDefinition();

	/**
	 * Returns a new object of class '<em>Null Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Null Statement</em>'.
	 * @generated
	 */
	NullStatement createNullStatement();

	/**
	 * Returns a new object of class '<em>Signal Assignment Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Assignment Statement</em>'.
	 * @generated
	 */
	SignalAssignmentStatement createSignalAssignmentStatement();

	/**
	 * Returns a new object of class '<em>Variable Assignment Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variable Assignment Statement</em>'.
	 * @generated
	 */
	VariableAssignmentStatement createVariableAssignmentStatement();

	/**
	 * Returns a new object of class '<em>Signal Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Declaration</em>'.
	 * @generated
	 */
	SignalDeclaration createSignalDeclaration();

	/**
	 * Returns a new object of class '<em>Variable Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variable Declaration</em>'.
	 * @generated
	 */
	VariableDeclaration createVariableDeclaration();

	/**
	 * Returns a new object of class '<em>Constant Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constant Declaration</em>'.
	 * @generated
	 */
	ConstantDeclaration createConstantDeclaration();

	/**
	 * Returns a new object of class '<em>Signature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signature</em>'.
	 * @generated
	 */
	Signature createSignature();

	/**
	 * Returns a new object of class '<em>Simultaneous Alternative</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simultaneous Alternative</em>'.
	 * @generated
	 */
	SimultaneousAlternative createSimultaneousAlternative();

	/**
	 * Returns a new object of class '<em>Simultaneous Case Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simultaneous Case Statement</em>'.
	 * @generated
	 */
	SimultaneousCaseStatement createSimultaneousCaseStatement();

	/**
	 * Returns a new object of class '<em>Simultaneous If Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simultaneous If Statement</em>'.
	 * @generated
	 */
	SimultaneousIfStatement createSimultaneousIfStatement();

	/**
	 * Returns a new object of class '<em>Simultaneous If Statement Test</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simultaneous If Statement Test</em>'.
	 * @generated
	 */
	SimultaneousIfStatementTest createSimultaneousIfStatementTest();

	/**
	 * Returns a new object of class '<em>Simultaneous Procedural Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simultaneous Procedural Statement</em>'.
	 * @generated
	 */
	SimultaneousProceduralStatement createSimultaneousProceduralStatement();

	/**
	 * Returns a new object of class '<em>Source Aspect</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Source Aspect</em>'.
	 * @generated
	 */
	SourceAspect createSourceAspect();

	/**
	 * Returns a new object of class '<em>Spectrum</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Spectrum</em>'.
	 * @generated
	 */
	Spectrum createSpectrum();

	/**
	 * Returns a new object of class '<em>Noise</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Noise</em>'.
	 * @generated
	 */
	Noise createNoise();

	/**
	 * Returns a new object of class '<em>Quantity Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Quantity Declaration</em>'.
	 * @generated
	 */
	QuantityDeclaration createQuantityDeclaration();

	/**
	 * Returns a new object of class '<em>Free Quantity Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Free Quantity Declaration</em>'.
	 * @generated
	 */
	FreeQuantityDeclaration createFreeQuantityDeclaration();

	/**
	 * Returns a new object of class '<em>Branch Quantity Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Branch Quantity Declaration</em>'.
	 * @generated
	 */
	BranchQuantityDeclaration createBranchQuantityDeclaration();

	/**
	 * Returns a new object of class '<em>Quantity Aspect</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Quantity Aspect</em>'.
	 * @generated
	 */
	QuantityAspect createQuantityAspect();

	/**
	 * Returns a new object of class '<em>Source Quantity Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Source Quantity Declaration</em>'.
	 * @generated
	 */
	SourceQuantityDeclaration createSourceQuantityDeclaration();

	/**
	 * Returns a new object of class '<em>Limit Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Limit Specification</em>'.
	 * @generated
	 */
	LimitSpecification createLimitSpecification();

	/**
	 * Returns a new object of class '<em>Subnature Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subnature Declaration</em>'.
	 * @generated
	 */
	SubnatureDeclaration createSubnatureDeclaration();

	/**
	 * Returns a new object of class '<em>Subprogram Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subprogram Declaration</em>'.
	 * @generated
	 */
	SubprogramDeclaration createSubprogramDeclaration();

	/**
	 * Returns a new object of class '<em>Subprogram Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subprogram Specification</em>'.
	 * @generated
	 */
	SubprogramSpecification createSubprogramSpecification();

	/**
	 * Returns a new object of class '<em>Subtype Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subtype Declaration</em>'.
	 * @generated
	 */
	SubtypeDeclaration createSubtypeDeclaration();

	/**
	 * Returns a new object of class '<em>Alias Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alias Declaration</em>'.
	 * @generated
	 */
	AliasDeclaration createAliasDeclaration();

	/**
	 * Returns a new object of class '<em>Terminal Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Terminal Declaration</em>'.
	 * @generated
	 */
	TerminalDeclaration createTerminalDeclaration();

	/**
	 * Returns a new object of class '<em>Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Declaration</em>'.
	 * @generated
	 */
	TypeDeclaration createTypeDeclaration();

	/**
	 * Returns a new object of class '<em>Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Definition</em>'.
	 * @generated
	 */
	TypeDefinition createTypeDefinition();

	/**
	 * Returns a new object of class '<em>Range Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Range Type Definition</em>'.
	 * @generated
	 */
	RangeTypeDefinition createRangeTypeDefinition();

	/**
	 * Returns a new object of class '<em>Unconstrained Array Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unconstrained Array Definition</em>'.
	 * @generated
	 */
	UnconstrainedArrayDefinition createUnconstrainedArrayDefinition();

	/**
	 * Returns a new object of class '<em>Unconstrained Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unconstrained Nature Definition</em>'.
	 * @generated
	 */
	UnconstrainedNatureDefinition createUnconstrainedNatureDefinition();

	/**
	 * Returns a new object of class '<em>Constrained Array Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constrained Array Definition</em>'.
	 * @generated
	 */
	ConstrainedArrayDefinition createConstrainedArrayDefinition();

	/**
	 * Returns a new object of class '<em>Constrained Nature Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constrained Nature Definition</em>'.
	 * @generated
	 */
	ConstrainedNatureDefinition createConstrainedNatureDefinition();

	/**
	 * Returns a new object of class '<em>Wait Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Wait Statement</em>'.
	 * @generated
	 */
	WaitStatement createWaitStatement();

	/**
	 * Returns a new object of class '<em>Null</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Null</em>'.
	 * @generated
	 */
	Null createNull();

	/**
	 * Returns a new object of class '<em>Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value</em>'.
	 * @generated
	 */
	Value createValue();

	/**
	 * Returns a new object of class '<em>Aggregate</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aggregate</em>'.
	 * @generated
	 */
	Aggregate createAggregate();

	/**
	 * Returns a new object of class '<em>Association</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Association</em>'.
	 * @generated
	 */
	Association createAssociation();

	/**
	 * Returns a new object of class '<em>All</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>All</em>'.
	 * @generated
	 */
	All createAll();

	/**
	 * Returns a new object of class '<em>Others</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Others</em>'.
	 * @generated
	 */
	Others createOthers();

	/**
	 * Returns a new object of class '<em>Logical Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Logical Expression</em>'.
	 * @generated
	 */
	LogicalExpression createLogicalExpression();

	/**
	 * Returns a new object of class '<em>Relational Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Relational Expression</em>'.
	 * @generated
	 */
	RelationalExpression createRelationalExpression();

	/**
	 * Returns a new object of class '<em>Shift Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Shift Expression</em>'.
	 * @generated
	 */
	ShiftExpression createShiftExpression();

	/**
	 * Returns a new object of class '<em>Adding Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Adding Expression</em>'.
	 * @generated
	 */
	AddingExpression createAddingExpression();

	/**
	 * Returns a new object of class '<em>Multiplying Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multiplying Expression</em>'.
	 * @generated
	 */
	MultiplyingExpression createMultiplyingExpression();

	/**
	 * Returns a new object of class '<em>Power Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Power Expression</em>'.
	 * @generated
	 */
	PowerExpression createPowerExpression();

	/**
	 * Returns a new object of class '<em>Unary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unary Expression</em>'.
	 * @generated
	 */
	UnaryExpression createUnaryExpression();

	/**
	 * Returns a new object of class '<em>Sign Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sign Expression</em>'.
	 * @generated
	 */
	SignExpression createSignExpression();

	/**
	 * Returns a new object of class '<em>Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Range</em>'.
	 * @generated
	 */
	Range createRange();

	/**
	 * Returns a new object of class '<em>Name</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Name</em>'.
	 * @generated
	 */
	Name createName();

	/**
	 * Returns a new object of class '<em>String</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String</em>'.
	 * @generated
	 */
	String createString();

	/**
	 * Returns a new object of class '<em>Character</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Character</em>'.
	 * @generated
	 */
	Character createCharacter();

	/**
	 * Returns a new object of class '<em>Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Identifier</em>'.
	 * @generated
	 */
	Identifier createIdentifier();

	/**
	 * Returns a new object of class '<em>Indication</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Indication</em>'.
	 * @generated
	 */
	Indication createIndication();

	/**
	 * Returns a new object of class '<em>Waveform</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Waveform</em>'.
	 * @generated
	 */
	Waveform createWaveform();

	/**
	 * Returns a new object of class '<em>Bit String</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bit String</em>'.
	 * @generated
	 */
	BitString createBitString();

	/**
	 * Returns a new object of class '<em>Unit Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unit Value</em>'.
	 * @generated
	 */
	UnitValue createUnitValue();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	VhdlPackage getVhdlPackage();

} //VhdlFactory
