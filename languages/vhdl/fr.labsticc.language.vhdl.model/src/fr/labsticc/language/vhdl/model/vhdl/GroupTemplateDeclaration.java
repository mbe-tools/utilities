/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group Template Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.GroupTemplateDeclaration#getEntry <em>Entry</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getGroupTemplateDeclaration()
 * @model
 * @generated
 */
public interface GroupTemplateDeclaration extends Declaration {
	/**
	 * Returns the value of the '<em><b>Entry</b></em>' attribute list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.EntityClass}.
	 * The literals are from the enumeration {@link fr.labsticc.language.vhdl.model.vhdl.EntityClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry</em>' attribute list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityClass
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getGroupTemplateDeclaration_Entry()
	 * @model unique="false"
	 * @generated
	 */
	EList<EntityClass> getEntry();

} // GroupTemplateDeclaration
