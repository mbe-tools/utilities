/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest;
import fr.labsticc.language.vhdl.model.vhdl.Statement;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simultaneous If Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousIfStatementImpl#getTest <em>Test</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousIfStatementImpl#getStatememt <em>Statememt</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SimultaneousIfStatementImpl extends StatementImpl implements SimultaneousIfStatement {
	/**
	 * The cached value of the '{@link #getTest() <em>Test</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTest()
	 * @generated
	 * @ordered
	 */
	protected EList<SimultaneousIfStatementTest> test;

	/**
	 * The cached value of the '{@link #getStatememt() <em>Statememt</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatememt()
	 * @generated
	 * @ordered
	 */
	protected EList<Statement> statememt;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimultaneousIfStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VhdlPackage.eINSTANCE.getSimultaneousIfStatement();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SimultaneousIfStatementTest> getTest() {
		if (test == null) {
			test = new EObjectContainmentEList<SimultaneousIfStatementTest>(SimultaneousIfStatementTest.class, this, VhdlPackage.SIMULTANEOUS_IF_STATEMENT__TEST);
		}
		return test;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Statement> getStatememt() {
		if (statememt == null) {
			statememt = new EObjectContainmentEList<Statement>(Statement.class, this, VhdlPackage.SIMULTANEOUS_IF_STATEMENT__STATEMEMT);
		}
		return statememt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT__TEST:
				return ((InternalEList<?>)getTest()).basicRemove(otherEnd, msgs);
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT__STATEMEMT:
				return ((InternalEList<?>)getStatememt()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT__TEST:
				return getTest();
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT__STATEMEMT:
				return getStatememt();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT__TEST:
				getTest().clear();
				getTest().addAll((Collection<? extends SimultaneousIfStatementTest>)newValue);
				return;
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT__STATEMEMT:
				getStatememt().clear();
				getStatememt().addAll((Collection<? extends Statement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT__TEST:
				getTest().clear();
				return;
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT__STATEMEMT:
				getStatememt().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT__TEST:
				return test != null && !test.isEmpty();
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT__STATEMEMT:
				return statememt != null && !statememt.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SimultaneousIfStatementImpl
