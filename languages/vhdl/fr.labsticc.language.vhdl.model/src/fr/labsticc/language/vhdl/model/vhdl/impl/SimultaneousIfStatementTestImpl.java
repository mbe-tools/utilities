/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.Expression;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest;
import fr.labsticc.language.vhdl.model.vhdl.Statement;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simultaneous If Statement Test</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousIfStatementTestImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousIfStatementTestImpl#getStatememt <em>Statememt</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SimultaneousIfStatementTestImpl extends IdentifiedElementImpl implements SimultaneousIfStatementTest {
	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected Expression condition;

	/**
	 * The cached value of the '{@link #getStatememt() <em>Statememt</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatememt()
	 * @generated
	 * @ordered
	 */
	protected EList<Statement> statememt;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimultaneousIfStatementTestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VhdlPackage.eINSTANCE.getSimultaneousIfStatementTest();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(Expression newCondition, NotificationChain msgs) {
		Expression oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__CONDITION, oldCondition, newCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(Expression newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject)condition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__CONDITION, null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject)newCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__CONDITION, null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__CONDITION, newCondition, newCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Statement> getStatememt() {
		if (statememt == null) {
			statememt = new EObjectContainmentEList<Statement>(Statement.class, this, VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__STATEMEMT);
		}
		return statememt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__CONDITION:
				return basicSetCondition(null, msgs);
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__STATEMEMT:
				return ((InternalEList<?>)getStatememt()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__CONDITION:
				return getCondition();
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__STATEMEMT:
				return getStatememt();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__CONDITION:
				setCondition((Expression)newValue);
				return;
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__STATEMEMT:
				getStatememt().clear();
				getStatememt().addAll((Collection<? extends Statement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__CONDITION:
				setCondition((Expression)null);
				return;
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__STATEMEMT:
				getStatememt().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__CONDITION:
				return condition != null;
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST__STATEMEMT:
				return statememt != null && !statememt.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SimultaneousIfStatementTestImpl
