/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conditional Signal Assignment Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#isPostponed <em>Postponed</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#getTarget <em>Target</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#isGuarded <em>Guarded</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#getDelay <em>Delay</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#getWaveform <em>Waveform</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConditionalSignalAssignmentStatement()
 * @model
 * @generated
 */
public interface ConditionalSignalAssignmentStatement extends Statement {
	/**
	 * Returns the value of the '<em><b>Postponed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Postponed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Postponed</em>' attribute.
	 * @see #setPostponed(boolean)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConditionalSignalAssignmentStatement_Postponed()
	 * @model
	 * @generated
	 */
	boolean isPostponed();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#isPostponed <em>Postponed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Postponed</em>' attribute.
	 * @see #isPostponed()
	 * @generated
	 */
	void setPostponed(boolean value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' containment reference.
	 * @see #setTarget(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConditionalSignalAssignmentStatement_Target()
	 * @model containment="true"
	 * @generated
	 */
	Expression getTarget();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#getTarget <em>Target</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' containment reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Expression value);

	/**
	 * Returns the value of the '<em><b>Guarded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Guarded</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guarded</em>' attribute.
	 * @see #setGuarded(boolean)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConditionalSignalAssignmentStatement_Guarded()
	 * @model
	 * @generated
	 */
	boolean isGuarded();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#isGuarded <em>Guarded</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Guarded</em>' attribute.
	 * @see #isGuarded()
	 * @generated
	 */
	void setGuarded(boolean value);

	/**
	 * Returns the value of the '<em><b>Delay</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay</em>' containment reference.
	 * @see #setDelay(DelayMechanism)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConditionalSignalAssignmentStatement_Delay()
	 * @model containment="true"
	 * @generated
	 */
	DelayMechanism getDelay();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#getDelay <em>Delay</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delay</em>' containment reference.
	 * @see #getDelay()
	 * @generated
	 */
	void setDelay(DelayMechanism value);

	/**
	 * Returns the value of the '<em><b>Waveform</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Waveform</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Waveform</em>' containment reference list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConditionalSignalAssignmentStatement_Waveform()
	 * @model containment="true"
	 * @generated
	 */
	EList<ConditionalWaveform> getWaveform();

} // ConditionalSignalAssignmentStatement
