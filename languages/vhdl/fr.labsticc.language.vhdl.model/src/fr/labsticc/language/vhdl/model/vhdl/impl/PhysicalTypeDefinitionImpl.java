/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.Constraint;
import fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.lang.String;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Type Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.PhysicalTypeDefinitionImpl#getRange <em>Range</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.PhysicalTypeDefinitionImpl#getPrimary <em>Primary</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.PhysicalTypeDefinitionImpl#getSecondary <em>Secondary</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PhysicalTypeDefinitionImpl extends TypeDefinitionImpl implements PhysicalTypeDefinition {
	/**
	 * The cached value of the '{@link #getRange() <em>Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRange()
	 * @generated
	 * @ordered
	 */
	protected Constraint range;

	/**
	 * The default value of the '{@link #getPrimary() <em>Primary</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrimary()
	 * @generated
	 * @ordered
	 */
	protected static final String PRIMARY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPrimary() <em>Primary</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrimary()
	 * @generated
	 * @ordered
	 */
	protected String primary = PRIMARY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSecondary() <em>Secondary</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecondary()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalTypeDefinitionSecondary> secondary;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalTypeDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VhdlPackage.eINSTANCE.getPhysicalTypeDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint getRange() {
		return range;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRange(Constraint newRange, NotificationChain msgs) {
		Constraint oldRange = range;
		range = newRange;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.PHYSICAL_TYPE_DEFINITION__RANGE, oldRange, newRange);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRange(Constraint newRange) {
		if (newRange != range) {
			NotificationChain msgs = null;
			if (range != null)
				msgs = ((InternalEObject)range).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.PHYSICAL_TYPE_DEFINITION__RANGE, null, msgs);
			if (newRange != null)
				msgs = ((InternalEObject)newRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.PHYSICAL_TYPE_DEFINITION__RANGE, null, msgs);
			msgs = basicSetRange(newRange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.PHYSICAL_TYPE_DEFINITION__RANGE, newRange, newRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPrimary() {
		return primary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrimary(String newPrimary) {
		String oldPrimary = primary;
		primary = newPrimary;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.PHYSICAL_TYPE_DEFINITION__PRIMARY, oldPrimary, primary));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalTypeDefinitionSecondary> getSecondary() {
		if (secondary == null) {
			secondary = new EObjectContainmentEList<PhysicalTypeDefinitionSecondary>(PhysicalTypeDefinitionSecondary.class, this, VhdlPackage.PHYSICAL_TYPE_DEFINITION__SECONDARY);
		}
		return secondary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__RANGE:
				return basicSetRange(null, msgs);
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__SECONDARY:
				return ((InternalEList<?>)getSecondary()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__RANGE:
				return getRange();
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__PRIMARY:
				return getPrimary();
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__SECONDARY:
				return getSecondary();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__RANGE:
				setRange((Constraint)newValue);
				return;
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__PRIMARY:
				setPrimary((String)newValue);
				return;
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__SECONDARY:
				getSecondary().clear();
				getSecondary().addAll((Collection<? extends PhysicalTypeDefinitionSecondary>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__RANGE:
				setRange((Constraint)null);
				return;
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__PRIMARY:
				setPrimary(PRIMARY_EDEFAULT);
				return;
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__SECONDARY:
				getSecondary().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__RANGE:
				return range != null;
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__PRIMARY:
				return PRIMARY_EDEFAULT == null ? primary != null : !PRIMARY_EDEFAULT.equals(primary);
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION__SECONDARY:
				return secondary != null && !secondary.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (primary: ");
		result.append(primary);
		result.append(')');
		return result.toString();
	}

} //PhysicalTypeDefinitionImpl
