/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.AccessTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.AddingExpression;
import fr.labsticc.language.vhdl.model.vhdl.AddingOperator;
import fr.labsticc.language.vhdl.model.vhdl.Aggregate;
import fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.All;
import fr.labsticc.language.vhdl.model.vhdl.Allocator;
import fr.labsticc.language.vhdl.model.vhdl.Architecture;
import fr.labsticc.language.vhdl.model.vhdl.ArrayNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ArrayTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.AssertionStatement;
import fr.labsticc.language.vhdl.model.vhdl.Association;
import fr.labsticc.language.vhdl.model.vhdl.Attribute;
import fr.labsticc.language.vhdl.model.vhdl.AttributeDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification;
import fr.labsticc.language.vhdl.model.vhdl.BitString;
import fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration;
import fr.labsticc.language.vhdl.model.vhdl.BlockStatement;
import fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.BreakElement;
import fr.labsticc.language.vhdl.model.vhdl.BreakStatement;
import fr.labsticc.language.vhdl.model.vhdl.CaseAlternative;
import fr.labsticc.language.vhdl.model.vhdl.CaseStatement;
import fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration;
import fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.ComponentInstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.CompositeNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.CompositeTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationItem;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification;
import fr.labsticc.language.vhdl.model.vhdl.ConstantDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.ConstrainedArrayDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ConstrainedNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.Constraint;
import fr.labsticc.language.vhdl.model.vhdl.Declaration;
import fr.labsticc.language.vhdl.model.vhdl.DelayMechanism;
import fr.labsticc.language.vhdl.model.vhdl.DesignUnit;
import fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification;
import fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.EntityClass;
import fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.EntityInstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.EnumerationTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ExitStatement;
import fr.labsticc.language.vhdl.model.vhdl.Expression;
import fr.labsticc.language.vhdl.model.vhdl.FileDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.FileTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ForGenerationScheme;
import fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme;
import fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.GenerateStatement;
import fr.labsticc.language.vhdl.model.vhdl.GenerationScheme;
import fr.labsticc.language.vhdl.model.vhdl.GenericMaps;
import fr.labsticc.language.vhdl.model.vhdl.Generics;
import fr.labsticc.language.vhdl.model.vhdl.GroupDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.GroupTemplateDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Identifier;
import fr.labsticc.language.vhdl.model.vhdl.IfGenerationScheme;
import fr.labsticc.language.vhdl.model.vhdl.IfStatement;
import fr.labsticc.language.vhdl.model.vhdl.IfStatementTest;
import fr.labsticc.language.vhdl.model.vhdl.Indication;
import fr.labsticc.language.vhdl.model.vhdl.IterationScheme;
import fr.labsticc.language.vhdl.model.vhdl.LibraryUnit;
import fr.labsticc.language.vhdl.model.vhdl.LimitSpecification;
import fr.labsticc.language.vhdl.model.vhdl.LogicalExpression;
import fr.labsticc.language.vhdl.model.vhdl.LogicalOperator;
import fr.labsticc.language.vhdl.model.vhdl.LoopStatement;
import fr.labsticc.language.vhdl.model.vhdl.Mode;
import fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression;
import fr.labsticc.language.vhdl.model.vhdl.MultiplyingOperator;
import fr.labsticc.language.vhdl.model.vhdl.Name;
import fr.labsticc.language.vhdl.model.vhdl.NameElement;
import fr.labsticc.language.vhdl.model.vhdl.NameList;
import fr.labsticc.language.vhdl.model.vhdl.NatureDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.NatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.NextStatement;
import fr.labsticc.language.vhdl.model.vhdl.Noise;
import fr.labsticc.language.vhdl.model.vhdl.Null;
import fr.labsticc.language.vhdl.model.vhdl.NullStatement;
import fr.labsticc.language.vhdl.model.vhdl.Open;
import fr.labsticc.language.vhdl.model.vhdl.Others;
import fr.labsticc.language.vhdl.model.vhdl.PackageBody;
import fr.labsticc.language.vhdl.model.vhdl.PackageDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary;
import fr.labsticc.language.vhdl.model.vhdl.PortMaps;
import fr.labsticc.language.vhdl.model.vhdl.Ports;
import fr.labsticc.language.vhdl.model.vhdl.PowerExpression;
import fr.labsticc.language.vhdl.model.vhdl.ProcedureCallStatement;
import fr.labsticc.language.vhdl.model.vhdl.ProcessStatement;
import fr.labsticc.language.vhdl.model.vhdl.QuantityAspect;
import fr.labsticc.language.vhdl.model.vhdl.QuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Range;
import fr.labsticc.language.vhdl.model.vhdl.RangeDirection;
import fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.RecordNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement;
import fr.labsticc.language.vhdl.model.vhdl.RecordTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.RejectMechanism;
import fr.labsticc.language.vhdl.model.vhdl.RelationalExpression;
import fr.labsticc.language.vhdl.model.vhdl.RelationalOperator;
import fr.labsticc.language.vhdl.model.vhdl.ReportStatement;
import fr.labsticc.language.vhdl.model.vhdl.ReturnStatement;
import fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.ShiftExpression;
import fr.labsticc.language.vhdl.model.vhdl.ShiftOperator;
import fr.labsticc.language.vhdl.model.vhdl.Sign;
import fr.labsticc.language.vhdl.model.vhdl.SignExpression;
import fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.SignalKind;
import fr.labsticc.language.vhdl.model.vhdl.Signature;
import fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousProceduralStatement;
import fr.labsticc.language.vhdl.model.vhdl.SourceAspect;
import fr.labsticc.language.vhdl.model.vhdl.SourceQuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Spectrum;
import fr.labsticc.language.vhdl.model.vhdl.Statement;
import fr.labsticc.language.vhdl.model.vhdl.SubnatureDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification;
import fr.labsticc.language.vhdl.model.vhdl.SubtypeDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.TerminalDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Transport;
import fr.labsticc.language.vhdl.model.vhdl.TypeDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.TypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.UnaryExpression;
import fr.labsticc.language.vhdl.model.vhdl.UnaryOperator;
import fr.labsticc.language.vhdl.model.vhdl.UnconstrainedArrayDefinition;
import fr.labsticc.language.vhdl.model.vhdl.UnconstrainedNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.UnitValue;
import fr.labsticc.language.vhdl.model.vhdl.UseClause;
import fr.labsticc.language.vhdl.model.vhdl.Value;
import fr.labsticc.language.vhdl.model.vhdl.VariableAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.VhdlFactory;
import fr.labsticc.language.vhdl.model.vhdl.VhdlFile;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;
import fr.labsticc.language.vhdl.model.vhdl.WaitStatement;
import fr.labsticc.language.vhdl.model.vhdl.Waveform;
import fr.labsticc.language.vhdl.model.vhdl.WhileIterationScheme;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VhdlFactoryImpl extends EFactoryImpl implements VhdlFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VhdlFactory init() {
		try {
			VhdlFactory theVhdlFactory = (VhdlFactory)EPackage.Registry.INSTANCE.getEFactory(VhdlPackage.eNS_URI);
			if (theVhdlFactory != null) {
				return theVhdlFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new VhdlFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VhdlFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case VhdlPackage.VHDL_FILE: return createVhdlFile();
			case VhdlPackage.DESIGN_UNIT: return createDesignUnit();
			case VhdlPackage.USE_CLAUSE: return createUseClause();
			case VhdlPackage.LIBRARY_UNIT: return createLibraryUnit();
			case VhdlPackage.ACCESS_TYPE_DEFINITION: return createAccessTypeDefinition();
			case VhdlPackage.NAME_ELEMENT: return createNameElement();
			case VhdlPackage.DECLARATION: return createDeclaration();
			case VhdlPackage.ARCHITECTURE: return createArchitecture();
			case VhdlPackage.STATEMENT: return createStatement();
			case VhdlPackage.ARRAY_NATURE_DEFINITION: return createArrayNatureDefinition();
			case VhdlPackage.ARRAY_TYPE_DEFINITION: return createArrayTypeDefinition();
			case VhdlPackage.ASSERTION_STATEMENT: return createAssertionStatement();
			case VhdlPackage.EXPRESSION: return createExpression();
			case VhdlPackage.OPEN: return createOpen();
			case VhdlPackage.ATTRIBUTE_DECLARATION: return createAttributeDeclaration();
			case VhdlPackage.ATTRIBUTE_SPECIFICATION: return createAttributeSpecification();
			case VhdlPackage.NAME_LIST: return createNameList();
			case VhdlPackage.BLOCK_CONFIGURATION: return createBlockConfiguration();
			case VhdlPackage.BLOCK_STATEMENT: return createBlockStatement();
			case VhdlPackage.BREAK_ELEMENT: return createBreakElement();
			case VhdlPackage.BREAK_STATEMENT: return createBreakStatement();
			case VhdlPackage.CASE_STATEMENT: return createCaseStatement();
			case VhdlPackage.CASE_ALTERNATIVE: return createCaseAlternative();
			case VhdlPackage.COMPONENT_CONFIGURATION: return createComponentConfiguration();
			case VhdlPackage.COMPONENT_DECLARATION: return createComponentDeclaration();
			case VhdlPackage.COMPONENT_INSTANTIATION_STATEMENT: return createComponentInstantiationStatement();
			case VhdlPackage.ENTITY_INSTANTIATION_STATEMENT: return createEntityInstantiationStatement();
			case VhdlPackage.CONFIGURATION_INSTANTIATION_STATEMENT: return createConfigurationInstantiationStatement();
			case VhdlPackage.COMPOSITE_NATURE_DEFINITION: return createCompositeNatureDefinition();
			case VhdlPackage.COMPOSITE_TYPE_DEFINITION: return createCompositeTypeDefinition();
			case VhdlPackage.SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT: return createSequentialSignalAssignmentStatement();
			case VhdlPackage.CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT: return createConditionalSignalAssignmentStatement();
			case VhdlPackage.SELECTED_SIGNAL_ASSIGNMENT_STATEMENT: return createSelectedSignalAssignmentStatement();
			case VhdlPackage.SIMPLE_SIMULTANEOUS_STATEMENT: return createSimpleSimultaneousStatement();
			case VhdlPackage.CONDITIONAL_WAVEFORM: return createConditionalWaveform();
			case VhdlPackage.CONFIGURATION_DECLARATION: return createConfigurationDeclaration();
			case VhdlPackage.CONFIGURATION_ITEM: return createConfigurationItem();
			case VhdlPackage.CONFIGURATION_SPECIFICATION: return createConfigurationSpecification();
			case VhdlPackage.DELAY_MECHANISM: return createDelayMechanism();
			case VhdlPackage.REJECT_MECHANISM: return createRejectMechanism();
			case VhdlPackage.TRANSPORT: return createTransport();
			case VhdlPackage.DISCONNECTION_SPECIFICATION: return createDisconnectionSpecification();
			case VhdlPackage.ELEMENT_DECLARATION: return createElementDeclaration();
			case VhdlPackage.ENTITY_DECLARATION: return createEntityDeclaration();
			case VhdlPackage.ENUMERATION_TYPE_DEFINITION: return createEnumerationTypeDefinition();
			case VhdlPackage.EXIT_STATEMENT: return createExitStatement();
			case VhdlPackage.FILE_DECLARATION: return createFileDeclaration();
			case VhdlPackage.FILE_TYPE_DEFINITION: return createFileTypeDefinition();
			case VhdlPackage.GENERATE_STATEMENT: return createGenerateStatement();
			case VhdlPackage.GENERATION_SCHEME: return createGenerationScheme();
			case VhdlPackage.FOR_GENERATION_SCHEME: return createForGenerationScheme();
			case VhdlPackage.IF_GENERATION_SCHEME: return createIfGenerationScheme();
			case VhdlPackage.GENERICS: return createGenerics();
			case VhdlPackage.GENERIC_MAPS: return createGenericMaps();
			case VhdlPackage.GROUP_DECLARATION: return createGroupDeclaration();
			case VhdlPackage.GROUP_TEMPLATE_DECLARATION: return createGroupTemplateDeclaration();
			case VhdlPackage.IF_STATEMENT: return createIfStatement();
			case VhdlPackage.IF_STATEMENT_TEST: return createIfStatementTest();
			case VhdlPackage.CONSTRAINT: return createConstraint();
			case VhdlPackage.LOOP_STATEMENT: return createLoopStatement();
			case VhdlPackage.ITERATION_SCHEME: return createIterationScheme();
			case VhdlPackage.WHILE_ITERATION_SCHEME: return createWhileIterationScheme();
			case VhdlPackage.FOR_ITERATION_SCHEME: return createForIterationScheme();
			case VhdlPackage.ATTRIBUTE: return createAttribute();
			case VhdlPackage.NATURE_DECLARATION: return createNatureDeclaration();
			case VhdlPackage.NATURE_DEFINITION: return createNatureDefinition();
			case VhdlPackage.NEXT_STATEMENT: return createNextStatement();
			case VhdlPackage.PACKAGE_BODY: return createPackageBody();
			case VhdlPackage.PACKAGE_DECLARATION: return createPackageDeclaration();
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION: return createPhysicalTypeDefinition();
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION_SECONDARY: return createPhysicalTypeDefinitionSecondary();
			case VhdlPackage.PORTS: return createPorts();
			case VhdlPackage.PORT_MAPS: return createPortMaps();
			case VhdlPackage.PROCEDURE_CALL_STATEMENT: return createProcedureCallStatement();
			case VhdlPackage.PROCESS_STATEMENT: return createProcessStatement();
			case VhdlPackage.ALLOCATOR: return createAllocator();
			case VhdlPackage.RECORD_NATURE_DEFINITION: return createRecordNatureDefinition();
			case VhdlPackage.RECORD_NATURE_ELEMENT: return createRecordNatureElement();
			case VhdlPackage.RECORD_TYPE_DEFINITION: return createRecordTypeDefinition();
			case VhdlPackage.REPORT_STATEMENT: return createReportStatement();
			case VhdlPackage.RETURN_STATEMENT: return createReturnStatement();
			case VhdlPackage.SCALAR_NATURE_DEFINITION: return createScalarNatureDefinition();
			case VhdlPackage.NULL_STATEMENT: return createNullStatement();
			case VhdlPackage.SIGNAL_ASSIGNMENT_STATEMENT: return createSignalAssignmentStatement();
			case VhdlPackage.VARIABLE_ASSIGNMENT_STATEMENT: return createVariableAssignmentStatement();
			case VhdlPackage.SIGNAL_DECLARATION: return createSignalDeclaration();
			case VhdlPackage.VARIABLE_DECLARATION: return createVariableDeclaration();
			case VhdlPackage.CONSTANT_DECLARATION: return createConstantDeclaration();
			case VhdlPackage.SIGNATURE: return createSignature();
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE: return createSimultaneousAlternative();
			case VhdlPackage.SIMULTANEOUS_CASE_STATEMENT: return createSimultaneousCaseStatement();
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT: return createSimultaneousIfStatement();
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST: return createSimultaneousIfStatementTest();
			case VhdlPackage.SIMULTANEOUS_PROCEDURAL_STATEMENT: return createSimultaneousProceduralStatement();
			case VhdlPackage.SOURCE_ASPECT: return createSourceAspect();
			case VhdlPackage.SPECTRUM: return createSpectrum();
			case VhdlPackage.NOISE: return createNoise();
			case VhdlPackage.QUANTITY_DECLARATION: return createQuantityDeclaration();
			case VhdlPackage.FREE_QUANTITY_DECLARATION: return createFreeQuantityDeclaration();
			case VhdlPackage.BRANCH_QUANTITY_DECLARATION: return createBranchQuantityDeclaration();
			case VhdlPackage.QUANTITY_ASPECT: return createQuantityAspect();
			case VhdlPackage.SOURCE_QUANTITY_DECLARATION: return createSourceQuantityDeclaration();
			case VhdlPackage.LIMIT_SPECIFICATION: return createLimitSpecification();
			case VhdlPackage.SUBNATURE_DECLARATION: return createSubnatureDeclaration();
			case VhdlPackage.SUBPROGRAM_DECLARATION: return createSubprogramDeclaration();
			case VhdlPackage.SUBPROGRAM_SPECIFICATION: return createSubprogramSpecification();
			case VhdlPackage.SUBTYPE_DECLARATION: return createSubtypeDeclaration();
			case VhdlPackage.ALIAS_DECLARATION: return createAliasDeclaration();
			case VhdlPackage.TERMINAL_DECLARATION: return createTerminalDeclaration();
			case VhdlPackage.TYPE_DECLARATION: return createTypeDeclaration();
			case VhdlPackage.TYPE_DEFINITION: return createTypeDefinition();
			case VhdlPackage.RANGE_TYPE_DEFINITION: return createRangeTypeDefinition();
			case VhdlPackage.UNCONSTRAINED_ARRAY_DEFINITION: return createUnconstrainedArrayDefinition();
			case VhdlPackage.UNCONSTRAINED_NATURE_DEFINITION: return createUnconstrainedNatureDefinition();
			case VhdlPackage.CONSTRAINED_ARRAY_DEFINITION: return createConstrainedArrayDefinition();
			case VhdlPackage.CONSTRAINED_NATURE_DEFINITION: return createConstrainedNatureDefinition();
			case VhdlPackage.WAIT_STATEMENT: return createWaitStatement();
			case VhdlPackage.NULL: return createNull();
			case VhdlPackage.VALUE: return createValue();
			case VhdlPackage.AGGREGATE: return createAggregate();
			case VhdlPackage.ALL: return createAll();
			case VhdlPackage.ASSOCIATION: return createAssociation();
			case VhdlPackage.OTHERS: return createOthers();
			case VhdlPackage.LOGICAL_EXPRESSION: return createLogicalExpression();
			case VhdlPackage.RELATIONAL_EXPRESSION: return createRelationalExpression();
			case VhdlPackage.SHIFT_EXPRESSION: return createShiftExpression();
			case VhdlPackage.ADDING_EXPRESSION: return createAddingExpression();
			case VhdlPackage.MULTIPLYING_EXPRESSION: return createMultiplyingExpression();
			case VhdlPackage.POWER_EXPRESSION: return createPowerExpression();
			case VhdlPackage.UNARY_EXPRESSION: return createUnaryExpression();
			case VhdlPackage.SIGN_EXPRESSION: return createSignExpression();
			case VhdlPackage.RANGE: return createRange();
			case VhdlPackage.NAME: return createName();
			case VhdlPackage.BIT_STRING: return createBitString();
			case VhdlPackage.STRING: return createString();
			case VhdlPackage.CHARACTER: return createCharacter();
			case VhdlPackage.IDENTIFIER: return createIdentifier();
			case VhdlPackage.INDICATION: return createIndication();
			case VhdlPackage.WAVEFORM: return createWaveform();
			case VhdlPackage.UNIT_VALUE: return createUnitValue();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case VhdlPackage.SIGNAL_KIND:
				return createSignalKindFromString(eDataType, initialValue);
			case VhdlPackage.RANGE_DIRECTION:
				return createRangeDirectionFromString(eDataType, initialValue);
			case VhdlPackage.MODE:
				return createModeFromString(eDataType, initialValue);
			case VhdlPackage.UNARY_OPERATOR:
				return createUnaryOperatorFromString(eDataType, initialValue);
			case VhdlPackage.MULTIPLYING_OPERATOR:
				return createMultiplyingOperatorFromString(eDataType, initialValue);
			case VhdlPackage.SHIFT_OPERATOR:
				return createShiftOperatorFromString(eDataType, initialValue);
			case VhdlPackage.RELATIONAL_OPERATOR:
				return createRelationalOperatorFromString(eDataType, initialValue);
			case VhdlPackage.LOGICAL_OPERATOR:
				return createLogicalOperatorFromString(eDataType, initialValue);
			case VhdlPackage.ADDING_OPERATOR:
				return createAddingOperatorFromString(eDataType, initialValue);
			case VhdlPackage.SIGN:
				return createSignFromString(eDataType, initialValue);
			case VhdlPackage.ENTITY_CLASS:
				return createEntityClassFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case VhdlPackage.SIGNAL_KIND:
				return convertSignalKindToString(eDataType, instanceValue);
			case VhdlPackage.RANGE_DIRECTION:
				return convertRangeDirectionToString(eDataType, instanceValue);
			case VhdlPackage.MODE:
				return convertModeToString(eDataType, instanceValue);
			case VhdlPackage.UNARY_OPERATOR:
				return convertUnaryOperatorToString(eDataType, instanceValue);
			case VhdlPackage.MULTIPLYING_OPERATOR:
				return convertMultiplyingOperatorToString(eDataType, instanceValue);
			case VhdlPackage.SHIFT_OPERATOR:
				return convertShiftOperatorToString(eDataType, instanceValue);
			case VhdlPackage.RELATIONAL_OPERATOR:
				return convertRelationalOperatorToString(eDataType, instanceValue);
			case VhdlPackage.LOGICAL_OPERATOR:
				return convertLogicalOperatorToString(eDataType, instanceValue);
			case VhdlPackage.ADDING_OPERATOR:
				return convertAddingOperatorToString(eDataType, instanceValue);
			case VhdlPackage.SIGN:
				return convertSignToString(eDataType, instanceValue);
			case VhdlPackage.ENTITY_CLASS:
				return convertEntityClassToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VhdlFile createVhdlFile() {
		VhdlFileImpl vhdlFile = new VhdlFileImpl();
		return vhdlFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignUnit createDesignUnit() {
		DesignUnitImpl designUnit = new DesignUnitImpl();
		return designUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseClause createUseClause() {
		UseClauseImpl useClause = new UseClauseImpl();
		return useClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibraryUnit createLibraryUnit() {
		LibraryUnitImpl libraryUnit = new LibraryUnitImpl();
		return libraryUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessTypeDefinition createAccessTypeDefinition() {
		AccessTypeDefinitionImpl accessTypeDefinition = new AccessTypeDefinitionImpl();
		return accessTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameElement createNameElement() {
		NameElementImpl nameElement = new NameElementImpl();
		return nameElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Declaration createDeclaration() {
		DeclarationImpl declaration = new DeclarationImpl();
		return declaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Architecture createArchitecture() {
		ArchitectureImpl architecture = new ArchitectureImpl();
		return architecture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Statement createStatement() {
		StatementImpl statement = new StatementImpl();
		return statement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayNatureDefinition createArrayNatureDefinition() {
		ArrayNatureDefinitionImpl arrayNatureDefinition = new ArrayNatureDefinitionImpl();
		return arrayNatureDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayTypeDefinition createArrayTypeDefinition() {
		ArrayTypeDefinitionImpl arrayTypeDefinition = new ArrayTypeDefinitionImpl();
		return arrayTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertionStatement createAssertionStatement() {
		AssertionStatementImpl assertionStatement = new AssertionStatementImpl();
		return assertionStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression createExpression() {
		ExpressionImpl expression = new ExpressionImpl();
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Open createOpen() {
		OpenImpl open = new OpenImpl();
		return open;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeDeclaration createAttributeDeclaration() {
		AttributeDeclarationImpl attributeDeclaration = new AttributeDeclarationImpl();
		return attributeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeSpecification createAttributeSpecification() {
		AttributeSpecificationImpl attributeSpecification = new AttributeSpecificationImpl();
		return attributeSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameList createNameList() {
		NameListImpl nameList = new NameListImpl();
		return nameList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockConfiguration createBlockConfiguration() {
		BlockConfigurationImpl blockConfiguration = new BlockConfigurationImpl();
		return blockConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockStatement createBlockStatement() {
		BlockStatementImpl blockStatement = new BlockStatementImpl();
		return blockStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BreakElement createBreakElement() {
		BreakElementImpl breakElement = new BreakElementImpl();
		return breakElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BreakStatement createBreakStatement() {
		BreakStatementImpl breakStatement = new BreakStatementImpl();
		return breakStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseStatement createCaseStatement() {
		CaseStatementImpl caseStatement = new CaseStatementImpl();
		return caseStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseAlternative createCaseAlternative() {
		CaseAlternativeImpl caseAlternative = new CaseAlternativeImpl();
		return caseAlternative;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentConfiguration createComponentConfiguration() {
		ComponentConfigurationImpl componentConfiguration = new ComponentConfigurationImpl();
		return componentConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentDeclaration createComponentDeclaration() {
		ComponentDeclarationImpl componentDeclaration = new ComponentDeclarationImpl();
		return componentDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstantiationStatement createComponentInstantiationStatement() {
		ComponentInstantiationStatementImpl componentInstantiationStatement = new ComponentInstantiationStatementImpl();
		return componentInstantiationStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityInstantiationStatement createEntityInstantiationStatement() {
		EntityInstantiationStatementImpl entityInstantiationStatement = new EntityInstantiationStatementImpl();
		return entityInstantiationStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationInstantiationStatement createConfigurationInstantiationStatement() {
		ConfigurationInstantiationStatementImpl configurationInstantiationStatement = new ConfigurationInstantiationStatementImpl();
		return configurationInstantiationStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeNatureDefinition createCompositeNatureDefinition() {
		CompositeNatureDefinitionImpl compositeNatureDefinition = new CompositeNatureDefinitionImpl();
		return compositeNatureDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeTypeDefinition createCompositeTypeDefinition() {
		CompositeTypeDefinitionImpl compositeTypeDefinition = new CompositeTypeDefinitionImpl();
		return compositeTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SequentialSignalAssignmentStatement createSequentialSignalAssignmentStatement() {
		SequentialSignalAssignmentStatementImpl sequentialSignalAssignmentStatement = new SequentialSignalAssignmentStatementImpl();
		return sequentialSignalAssignmentStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionalSignalAssignmentStatement createConditionalSignalAssignmentStatement() {
		ConditionalSignalAssignmentStatementImpl conditionalSignalAssignmentStatement = new ConditionalSignalAssignmentStatementImpl();
		return conditionalSignalAssignmentStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectedSignalAssignmentStatement createSelectedSignalAssignmentStatement() {
		SelectedSignalAssignmentStatementImpl selectedSignalAssignmentStatement = new SelectedSignalAssignmentStatementImpl();
		return selectedSignalAssignmentStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleSimultaneousStatement createSimpleSimultaneousStatement() {
		SimpleSimultaneousStatementImpl simpleSimultaneousStatement = new SimpleSimultaneousStatementImpl();
		return simpleSimultaneousStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionalWaveform createConditionalWaveform() {
		ConditionalWaveformImpl conditionalWaveform = new ConditionalWaveformImpl();
		return conditionalWaveform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationDeclaration createConfigurationDeclaration() {
		ConfigurationDeclarationImpl configurationDeclaration = new ConfigurationDeclarationImpl();
		return configurationDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationItem createConfigurationItem() {
		ConfigurationItemImpl configurationItem = new ConfigurationItemImpl();
		return configurationItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationSpecification createConfigurationSpecification() {
		ConfigurationSpecificationImpl configurationSpecification = new ConfigurationSpecificationImpl();
		return configurationSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DelayMechanism createDelayMechanism() {
		DelayMechanismImpl delayMechanism = new DelayMechanismImpl();
		return delayMechanism;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RejectMechanism createRejectMechanism() {
		RejectMechanismImpl rejectMechanism = new RejectMechanismImpl();
		return rejectMechanism;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transport createTransport() {
		TransportImpl transport = new TransportImpl();
		return transport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisconnectionSpecification createDisconnectionSpecification() {
		DisconnectionSpecificationImpl disconnectionSpecification = new DisconnectionSpecificationImpl();
		return disconnectionSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementDeclaration createElementDeclaration() {
		ElementDeclarationImpl elementDeclaration = new ElementDeclarationImpl();
		return elementDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityDeclaration createEntityDeclaration() {
		EntityDeclarationImpl entityDeclaration = new EntityDeclarationImpl();
		return entityDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationTypeDefinition createEnumerationTypeDefinition() {
		EnumerationTypeDefinitionImpl enumerationTypeDefinition = new EnumerationTypeDefinitionImpl();
		return enumerationTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExitStatement createExitStatement() {
		ExitStatementImpl exitStatement = new ExitStatementImpl();
		return exitStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileDeclaration createFileDeclaration() {
		FileDeclarationImpl fileDeclaration = new FileDeclarationImpl();
		return fileDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileTypeDefinition createFileTypeDefinition() {
		FileTypeDefinitionImpl fileTypeDefinition = new FileTypeDefinitionImpl();
		return fileTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenerateStatement createGenerateStatement() {
		GenerateStatementImpl generateStatement = new GenerateStatementImpl();
		return generateStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenerationScheme createGenerationScheme() {
		GenerationSchemeImpl generationScheme = new GenerationSchemeImpl();
		return generationScheme;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForGenerationScheme createForGenerationScheme() {
		ForGenerationSchemeImpl forGenerationScheme = new ForGenerationSchemeImpl();
		return forGenerationScheme;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfGenerationScheme createIfGenerationScheme() {
		IfGenerationSchemeImpl ifGenerationScheme = new IfGenerationSchemeImpl();
		return ifGenerationScheme;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Generics createGenerics() {
		GenericsImpl generics = new GenericsImpl();
		return generics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericMaps createGenericMaps() {
		GenericMapsImpl genericMaps = new GenericMapsImpl();
		return genericMaps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupDeclaration createGroupDeclaration() {
		GroupDeclarationImpl groupDeclaration = new GroupDeclarationImpl();
		return groupDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupTemplateDeclaration createGroupTemplateDeclaration() {
		GroupTemplateDeclarationImpl groupTemplateDeclaration = new GroupTemplateDeclarationImpl();
		return groupTemplateDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfStatement createIfStatement() {
		IfStatementImpl ifStatement = new IfStatementImpl();
		return ifStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfStatementTest createIfStatementTest() {
		IfStatementTestImpl ifStatementTest = new IfStatementTestImpl();
		return ifStatementTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint createConstraint() {
		ConstraintImpl constraint = new ConstraintImpl();
		return constraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoopStatement createLoopStatement() {
		LoopStatementImpl loopStatement = new LoopStatementImpl();
		return loopStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IterationScheme createIterationScheme() {
		IterationSchemeImpl iterationScheme = new IterationSchemeImpl();
		return iterationScheme;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhileIterationScheme createWhileIterationScheme() {
		WhileIterationSchemeImpl whileIterationScheme = new WhileIterationSchemeImpl();
		return whileIterationScheme;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForIterationScheme createForIterationScheme() {
		ForIterationSchemeImpl forIterationScheme = new ForIterationSchemeImpl();
		return forIterationScheme;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute createAttribute() {
		AttributeImpl attribute = new AttributeImpl();
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NatureDeclaration createNatureDeclaration() {
		NatureDeclarationImpl natureDeclaration = new NatureDeclarationImpl();
		return natureDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NatureDefinition createNatureDefinition() {
		NatureDefinitionImpl natureDefinition = new NatureDefinitionImpl();
		return natureDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NextStatement createNextStatement() {
		NextStatementImpl nextStatement = new NextStatementImpl();
		return nextStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageBody createPackageBody() {
		PackageBodyImpl packageBody = new PackageBodyImpl();
		return packageBody;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageDeclaration createPackageDeclaration() {
		PackageDeclarationImpl packageDeclaration = new PackageDeclarationImpl();
		return packageDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalTypeDefinition createPhysicalTypeDefinition() {
		PhysicalTypeDefinitionImpl physicalTypeDefinition = new PhysicalTypeDefinitionImpl();
		return physicalTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalTypeDefinitionSecondary createPhysicalTypeDefinitionSecondary() {
		PhysicalTypeDefinitionSecondaryImpl physicalTypeDefinitionSecondary = new PhysicalTypeDefinitionSecondaryImpl();
		return physicalTypeDefinitionSecondary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ports createPorts() {
		PortsImpl ports = new PortsImpl();
		return ports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortMaps createPortMaps() {
		PortMapsImpl portMaps = new PortMapsImpl();
		return portMaps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureCallStatement createProcedureCallStatement() {
		ProcedureCallStatementImpl procedureCallStatement = new ProcedureCallStatementImpl();
		return procedureCallStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessStatement createProcessStatement() {
		ProcessStatementImpl processStatement = new ProcessStatementImpl();
		return processStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Allocator createAllocator() {
		AllocatorImpl allocator = new AllocatorImpl();
		return allocator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordNatureDefinition createRecordNatureDefinition() {
		RecordNatureDefinitionImpl recordNatureDefinition = new RecordNatureDefinitionImpl();
		return recordNatureDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordNatureElement createRecordNatureElement() {
		RecordNatureElementImpl recordNatureElement = new RecordNatureElementImpl();
		return recordNatureElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordTypeDefinition createRecordTypeDefinition() {
		RecordTypeDefinitionImpl recordTypeDefinition = new RecordTypeDefinitionImpl();
		return recordTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReportStatement createReportStatement() {
		ReportStatementImpl reportStatement = new ReportStatementImpl();
		return reportStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnStatement createReturnStatement() {
		ReturnStatementImpl returnStatement = new ReturnStatementImpl();
		return returnStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScalarNatureDefinition createScalarNatureDefinition() {
		ScalarNatureDefinitionImpl scalarNatureDefinition = new ScalarNatureDefinitionImpl();
		return scalarNatureDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullStatement createNullStatement() {
		NullStatementImpl nullStatement = new NullStatementImpl();
		return nullStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalAssignmentStatement createSignalAssignmentStatement() {
		SignalAssignmentStatementImpl signalAssignmentStatement = new SignalAssignmentStatementImpl();
		return signalAssignmentStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableAssignmentStatement createVariableAssignmentStatement() {
		VariableAssignmentStatementImpl variableAssignmentStatement = new VariableAssignmentStatementImpl();
		return variableAssignmentStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalDeclaration createSignalDeclaration() {
		SignalDeclarationImpl signalDeclaration = new SignalDeclarationImpl();
		return signalDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableDeclaration createVariableDeclaration() {
		VariableDeclarationImpl variableDeclaration = new VariableDeclarationImpl();
		return variableDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantDeclaration createConstantDeclaration() {
		ConstantDeclarationImpl constantDeclaration = new ConstantDeclarationImpl();
		return constantDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Signature createSignature() {
		SignatureImpl signature = new SignatureImpl();
		return signature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimultaneousAlternative createSimultaneousAlternative() {
		SimultaneousAlternativeImpl simultaneousAlternative = new SimultaneousAlternativeImpl();
		return simultaneousAlternative;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimultaneousCaseStatement createSimultaneousCaseStatement() {
		SimultaneousCaseStatementImpl simultaneousCaseStatement = new SimultaneousCaseStatementImpl();
		return simultaneousCaseStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimultaneousIfStatement createSimultaneousIfStatement() {
		SimultaneousIfStatementImpl simultaneousIfStatement = new SimultaneousIfStatementImpl();
		return simultaneousIfStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimultaneousIfStatementTest createSimultaneousIfStatementTest() {
		SimultaneousIfStatementTestImpl simultaneousIfStatementTest = new SimultaneousIfStatementTestImpl();
		return simultaneousIfStatementTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimultaneousProceduralStatement createSimultaneousProceduralStatement() {
		SimultaneousProceduralStatementImpl simultaneousProceduralStatement = new SimultaneousProceduralStatementImpl();
		return simultaneousProceduralStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceAspect createSourceAspect() {
		SourceAspectImpl sourceAspect = new SourceAspectImpl();
		return sourceAspect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Spectrum createSpectrum() {
		SpectrumImpl spectrum = new SpectrumImpl();
		return spectrum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Noise createNoise() {
		NoiseImpl noise = new NoiseImpl();
		return noise;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuantityDeclaration createQuantityDeclaration() {
		QuantityDeclarationImpl quantityDeclaration = new QuantityDeclarationImpl();
		return quantityDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FreeQuantityDeclaration createFreeQuantityDeclaration() {
		FreeQuantityDeclarationImpl freeQuantityDeclaration = new FreeQuantityDeclarationImpl();
		return freeQuantityDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BranchQuantityDeclaration createBranchQuantityDeclaration() {
		BranchQuantityDeclarationImpl branchQuantityDeclaration = new BranchQuantityDeclarationImpl();
		return branchQuantityDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuantityAspect createQuantityAspect() {
		QuantityAspectImpl quantityAspect = new QuantityAspectImpl();
		return quantityAspect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceQuantityDeclaration createSourceQuantityDeclaration() {
		SourceQuantityDeclarationImpl sourceQuantityDeclaration = new SourceQuantityDeclarationImpl();
		return sourceQuantityDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LimitSpecification createLimitSpecification() {
		LimitSpecificationImpl limitSpecification = new LimitSpecificationImpl();
		return limitSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubnatureDeclaration createSubnatureDeclaration() {
		SubnatureDeclarationImpl subnatureDeclaration = new SubnatureDeclarationImpl();
		return subnatureDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubprogramDeclaration createSubprogramDeclaration() {
		SubprogramDeclarationImpl subprogramDeclaration = new SubprogramDeclarationImpl();
		return subprogramDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubprogramSpecification createSubprogramSpecification() {
		SubprogramSpecificationImpl subprogramSpecification = new SubprogramSpecificationImpl();
		return subprogramSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubtypeDeclaration createSubtypeDeclaration() {
		SubtypeDeclarationImpl subtypeDeclaration = new SubtypeDeclarationImpl();
		return subtypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AliasDeclaration createAliasDeclaration() {
		AliasDeclarationImpl aliasDeclaration = new AliasDeclarationImpl();
		return aliasDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TerminalDeclaration createTerminalDeclaration() {
		TerminalDeclarationImpl terminalDeclaration = new TerminalDeclarationImpl();
		return terminalDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeDeclaration createTypeDeclaration() {
		TypeDeclarationImpl typeDeclaration = new TypeDeclarationImpl();
		return typeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeDefinition createTypeDefinition() {
		TypeDefinitionImpl typeDefinition = new TypeDefinitionImpl();
		return typeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeTypeDefinition createRangeTypeDefinition() {
		RangeTypeDefinitionImpl rangeTypeDefinition = new RangeTypeDefinitionImpl();
		return rangeTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnconstrainedArrayDefinition createUnconstrainedArrayDefinition() {
		UnconstrainedArrayDefinitionImpl unconstrainedArrayDefinition = new UnconstrainedArrayDefinitionImpl();
		return unconstrainedArrayDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnconstrainedNatureDefinition createUnconstrainedNatureDefinition() {
		UnconstrainedNatureDefinitionImpl unconstrainedNatureDefinition = new UnconstrainedNatureDefinitionImpl();
		return unconstrainedNatureDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstrainedArrayDefinition createConstrainedArrayDefinition() {
		ConstrainedArrayDefinitionImpl constrainedArrayDefinition = new ConstrainedArrayDefinitionImpl();
		return constrainedArrayDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstrainedNatureDefinition createConstrainedNatureDefinition() {
		ConstrainedNatureDefinitionImpl constrainedNatureDefinition = new ConstrainedNatureDefinitionImpl();
		return constrainedNatureDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WaitStatement createWaitStatement() {
		WaitStatementImpl waitStatement = new WaitStatementImpl();
		return waitStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Null createNull() {
		NullImpl null_ = new NullImpl();
		return null_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value createValue() {
		ValueImpl value = new ValueImpl();
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aggregate createAggregate() {
		AggregateImpl aggregate = new AggregateImpl();
		return aggregate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association createAssociation() {
		AssociationImpl association = new AssociationImpl();
		return association;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public All createAll() {
		AllImpl all = new AllImpl();
		return all;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Others createOthers() {
		OthersImpl others = new OthersImpl();
		return others;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalExpression createLogicalExpression() {
		LogicalExpressionImpl logicalExpression = new LogicalExpressionImpl();
		return logicalExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationalExpression createRelationalExpression() {
		RelationalExpressionImpl relationalExpression = new RelationalExpressionImpl();
		return relationalExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShiftExpression createShiftExpression() {
		ShiftExpressionImpl shiftExpression = new ShiftExpressionImpl();
		return shiftExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AddingExpression createAddingExpression() {
		AddingExpressionImpl addingExpression = new AddingExpressionImpl();
		return addingExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplyingExpression createMultiplyingExpression() {
		MultiplyingExpressionImpl multiplyingExpression = new MultiplyingExpressionImpl();
		return multiplyingExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PowerExpression createPowerExpression() {
		PowerExpressionImpl powerExpression = new PowerExpressionImpl();
		return powerExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryExpression createUnaryExpression() {
		UnaryExpressionImpl unaryExpression = new UnaryExpressionImpl();
		return unaryExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignExpression createSignExpression() {
		SignExpressionImpl signExpression = new SignExpressionImpl();
		return signExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Range createRange() {
		RangeImpl range = new RangeImpl();
		return range;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Name createName() {
		NameImpl name = new NameImpl();
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public fr.labsticc.language.vhdl.model.vhdl.String createString() {
		StringImpl string = new StringImpl();
		return string;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public fr.labsticc.language.vhdl.model.vhdl.Character createCharacter() {
		CharacterImpl character = new CharacterImpl();
		return character;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Identifier createIdentifier() {
		IdentifierImpl identifier = new IdentifierImpl();
		return identifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Indication createIndication() {
		IndicationImpl indication = new IndicationImpl();
		return indication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Waveform createWaveform() {
		WaveformImpl waveform = new WaveformImpl();
		return waveform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BitString createBitString() {
		BitStringImpl bitString = new BitStringImpl();
		return bitString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitValue createUnitValue() {
		UnitValueImpl unitValue = new UnitValueImpl();
		return unitValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalKind createSignalKindFromString(EDataType eDataType, String initialValue) {
		SignalKind result = SignalKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSignalKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeDirection createRangeDirectionFromString(EDataType eDataType, String initialValue) {
		RangeDirection result = RangeDirection.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRangeDirectionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mode createModeFromString(EDataType eDataType, String initialValue) {
		Mode result = Mode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertModeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryOperator createUnaryOperatorFromString(EDataType eDataType, String initialValue) {
		UnaryOperator result = UnaryOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUnaryOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplyingOperator createMultiplyingOperatorFromString(EDataType eDataType, String initialValue) {
		MultiplyingOperator result = MultiplyingOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMultiplyingOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShiftOperator createShiftOperatorFromString(EDataType eDataType, String initialValue) {
		ShiftOperator result = ShiftOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertShiftOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationalOperator createRelationalOperatorFromString(EDataType eDataType, String initialValue) {
		RelationalOperator result = RelationalOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRelationalOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalOperator createLogicalOperatorFromString(EDataType eDataType, String initialValue) {
		LogicalOperator result = LogicalOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLogicalOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AddingOperator createAddingOperatorFromString(EDataType eDataType, String initialValue) {
		AddingOperator result = AddingOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAddingOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sign createSignFromString(EDataType eDataType, String initialValue) {
		Sign result = Sign.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSignToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityClass createEntityClassFromString(EDataType eDataType, String initialValue) {
		EntityClass result = EntityClass.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEntityClassToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VhdlPackage getVhdlPackage() {
		return (VhdlPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static VhdlPackage getPackage() {
		return VhdlPackage.eINSTANCE;
	}

} //VhdlFactoryImpl
