/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getList <em>List</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getComponent <em>Component</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getEntity <em>Entity</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getGenericMap <em>Generic Map</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getPortMap <em>Port Map</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationSpecification()
 * @model
 * @generated
 */
public interface ConfigurationSpecification extends Declaration {
	/**
	 * Returns the value of the '<em><b>List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List</em>' containment reference.
	 * @see #setList(NameList)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationSpecification_List()
	 * @model containment="true"
	 * @generated
	 */
	NameList getList();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getList <em>List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List</em>' containment reference.
	 * @see #getList()
	 * @generated
	 */
	void setList(NameList value);

	/**
	 * Returns the value of the '<em><b>Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component</em>' containment reference.
	 * @see #setComponent(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationSpecification_Component()
	 * @model containment="true"
	 * @generated
	 */
	Expression getComponent();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getComponent <em>Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component</em>' containment reference.
	 * @see #getComponent()
	 * @generated
	 */
	void setComponent(Expression value);

	/**
	 * Returns the value of the '<em><b>Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity</em>' containment reference.
	 * @see #setEntity(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationSpecification_Entity()
	 * @model containment="true"
	 * @generated
	 */
	Expression getEntity();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getEntity <em>Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity</em>' containment reference.
	 * @see #getEntity()
	 * @generated
	 */
	void setEntity(Expression value);

	/**
	 * Returns the value of the '<em><b>Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configuration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configuration</em>' containment reference.
	 * @see #setConfiguration(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationSpecification_Configuration()
	 * @model containment="true"
	 * @generated
	 */
	Expression getConfiguration();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getConfiguration <em>Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configuration</em>' containment reference.
	 * @see #getConfiguration()
	 * @generated
	 */
	void setConfiguration(Expression value);

	/**
	 * Returns the value of the '<em><b>Generic Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Map</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Map</em>' containment reference.
	 * @see #setGenericMap(GenericMaps)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationSpecification_GenericMap()
	 * @model containment="true"
	 * @generated
	 */
	GenericMaps getGenericMap();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getGenericMap <em>Generic Map</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Map</em>' containment reference.
	 * @see #getGenericMap()
	 * @generated
	 */
	void setGenericMap(GenericMaps value);

	/**
	 * Returns the value of the '<em><b>Port Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Map</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Map</em>' containment reference.
	 * @see #setPortMap(PortMaps)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationSpecification_PortMap()
	 * @model containment="true"
	 * @generated
	 */
	PortMaps getPortMap();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getPortMap <em>Port Map</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Map</em>' containment reference.
	 * @see #getPortMap()
	 * @generated
	 */
	void setPortMap(PortMaps value);

} // ConfigurationSpecification
