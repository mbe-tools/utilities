/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Choice</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getChoice()
 * @model abstract="true"
 * @generated
 */
public interface Choice extends EObject {
} // Choice
