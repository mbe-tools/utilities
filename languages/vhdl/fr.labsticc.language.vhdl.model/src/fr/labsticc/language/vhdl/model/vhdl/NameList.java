/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Name List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.NameList#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getNameList()
 * @model
 * @generated
 */
public interface NameList extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' containment reference list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getNameList_Name()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getName();

} // NameList
