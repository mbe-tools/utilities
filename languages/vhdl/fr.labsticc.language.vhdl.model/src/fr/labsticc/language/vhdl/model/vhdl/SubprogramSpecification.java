/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import java.lang.String;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subprogram Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getName <em>Name</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getDeclaration <em>Declaration</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getPurity <em>Purity</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getReturn <em>Return</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSubprogramSpecification()
 * @model
 * @generated
 */
public interface SubprogramSpecification extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSubprogramSpecification_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.Declaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Declaration</em>' containment reference list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSubprogramSpecification_Declaration()
	 * @model containment="true"
	 * @generated
	 */
	EList<Declaration> getDeclaration();

	/**
	 * Returns the value of the '<em><b>Purity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Purity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Purity</em>' attribute.
	 * @see #setPurity(String)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSubprogramSpecification_Purity()
	 * @model
	 * @generated
	 */
	String getPurity();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getPurity <em>Purity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Purity</em>' attribute.
	 * @see #getPurity()
	 * @generated
	 */
	void setPurity(String value);

	/**
	 * Returns the value of the '<em><b>Return</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return</em>' containment reference.
	 * @see #setReturn(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSubprogramSpecification_Return()
	 * @model containment="true"
	 * @generated
	 */
	Expression getReturn();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getReturn <em>Return</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return</em>' containment reference.
	 * @see #getReturn()
	 * @generated
	 */
	void setReturn(Expression value);

} // SubprogramSpecification
