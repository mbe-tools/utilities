/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Delay Mechanism</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getDelayMechanism()
 * @model
 * @generated
 */
public interface DelayMechanism extends IdentifiedElement {
} // DelayMechanism
