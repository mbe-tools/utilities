/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Allocator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.Allocator#getAllocate <em>Allocate</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getAllocator()
 * @model
 * @generated
 */
public interface Allocator extends Expression {
	/**
	 * Returns the value of the '<em><b>Allocate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocate</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocate</em>' containment reference.
	 * @see #setAllocate(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getAllocator_Allocate()
	 * @model containment="true"
	 * @generated
	 */
	Expression getAllocate();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.Allocator#getAllocate <em>Allocate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allocate</em>' containment reference.
	 * @see #getAllocate()
	 * @generated
	 */
	void setAllocate(Expression value);

} // Allocator
