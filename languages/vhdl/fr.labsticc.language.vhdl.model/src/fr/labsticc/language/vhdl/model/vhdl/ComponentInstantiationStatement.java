/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Instantiation Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ComponentInstantiationStatement#getComponent <em>Component</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getComponentInstantiationStatement()
 * @model
 * @generated
 */
public interface ComponentInstantiationStatement extends InstantiationStatement {
	/**
	 * Returns the value of the '<em><b>Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component</em>' containment reference.
	 * @see #setComponent(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getComponentInstantiationStatement_Component()
	 * @model containment="true"
	 * @generated
	 */
	Expression getComponent();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentInstantiationStatement#getComponent <em>Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component</em>' containment reference.
	 * @see #getComponent()
	 * @generated
	 */
	void setComponent(Expression value);

} // ComponentInstantiationStatement
