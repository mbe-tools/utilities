/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import fr.labsticc.language.vhdl.model.vhdl.IdentifiedElement;
import fr.labsticc.language.vhdl.model.vhdl.NamedElement;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.lang.String;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Identified Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.IdentifiedElementImpl#getId <em>Id</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class IdentifiedElementImpl extends MinimalEObjectImpl.Container implements IdentifiedElement {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;
	
	protected static final String NAME_SEPARATOR = ".";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IdentifiedElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VhdlPackage.eINSTANCE.getIdentifiedElement();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getId() {
		//if(id == null || id.isEmpty()) {
		String name = "";
		
		if(this instanceof NamedElement) {
			NamedElement na = (NamedElement)this;
			name = na.getName();
		}
		
		if(name == null || name.isEmpty()){
//			name = this.getClass().getSimpleName().toLowerCase();
//			name = name.replaceAll("impl", "");
			name = eClass().getName().toLowerCase();
			
			EObject container = this.eContainer();
			if(container != null) {
				Object o = container.eGet(this.eContainmentFeature());
				
				if(o instanceof List<?>) {
					List<?> values = (List<?>)o;
					int index = values.indexOf(this); 
		
					if(index >= 0) {
						// add index
						name = name + index;
					}
				} // list
			} // container
		}
		
		String id;
		final EObject c = eContainer();
		
		if(c instanceof IdentifiedElement) {
			IdentifiedElement ie = (IdentifiedElement)c;
			id = ie.getId() + NAME_SEPARATOR;
			
			// DB: Take into account the containing feature to avoid id collisions.
			final EStructuralFeature containingFeat = eContainingFeature();
			
			if ( containingFeat != null ) {
				id = id + containingFeat.getName().toLowerCase();
			}
			
			id = id + NAME_SEPARATOR + name;
		}
		else {
			id = name;
		}
			
		//}
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VhdlPackage.IDENTIFIED_ELEMENT__ID:
				return getId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VhdlPackage.IDENTIFIED_ELEMENT__ID:
				return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(getId());
		result.append(')');
		return result.toString();
	}

} //IdentifiedElementImpl
