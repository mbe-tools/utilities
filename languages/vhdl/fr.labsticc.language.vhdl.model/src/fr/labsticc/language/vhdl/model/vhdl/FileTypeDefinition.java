/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File Type Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.FileTypeDefinition#getFile <em>File</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getFileTypeDefinition()
 * @model
 * @generated
 */
public interface FileTypeDefinition extends TypeDefinition {
	/**
	 * Returns the value of the '<em><b>File</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>File</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>File</em>' containment reference.
	 * @see #setFile(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getFileTypeDefinition_File()
	 * @model containment="true"
	 * @generated
	 */
	Expression getFile();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.FileTypeDefinition#getFile <em>File</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>File</em>' containment reference.
	 * @see #getFile()
	 * @generated
	 */
	void setFile(Expression value);

} // FileTypeDefinition
