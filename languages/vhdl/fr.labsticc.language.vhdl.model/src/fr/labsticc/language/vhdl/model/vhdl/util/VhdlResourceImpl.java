/**
 */
package fr.labsticc.language.vhdl.model.vhdl.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see fr.labsticc.language.vhdl.model.vhdl.util.VhdlResourceFactoryImpl
 * @generated
 */
public class VhdlResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public VhdlResourceImpl(URI uri) {
		super(uri);
	}

} //VhdlResourceImpl
