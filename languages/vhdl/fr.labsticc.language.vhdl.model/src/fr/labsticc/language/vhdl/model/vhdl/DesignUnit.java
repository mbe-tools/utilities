/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import java.lang.String;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Design Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.DesignUnit#getLibrary <em>Library</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.DesignUnit#getUse <em>Use</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.DesignUnit#getUnit <em>Unit</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getDesignUnit()
 * @model
 * @generated
 */
public interface DesignUnit extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Library</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Library</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Library</em>' attribute list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getDesignUnit_Library()
	 * @model unique="false"
	 * @generated
	 */
	EList<String> getLibrary();

	/**
	 * Returns the value of the '<em><b>Use</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use</em>' containment reference list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getDesignUnit_Use()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getUse();

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' containment reference.
	 * @see #setUnit(LibraryUnit)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getDesignUnit_Unit()
	 * @model containment="true" required="true"
	 * @generated
	 */
	LibraryUnit getUnit();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.DesignUnit#getUnit <em>Unit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' containment reference.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(LibraryUnit value);

} // DesignUnit
