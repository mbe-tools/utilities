/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access Type Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.AccessTypeDefinition#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getAccessTypeDefinition()
 * @model
 * @generated
 */
public interface AccessTypeDefinition extends TypeDefinition {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getAccessTypeDefinition_Type()
	 * @model containment="true"
	 * @generated
	 */
	Expression getType();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.AccessTypeDefinition#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Expression value);

} // AccessTypeDefinition
