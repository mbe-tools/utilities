/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Disconnection Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification#getDisconnect <em>Disconnect</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification#getType <em>Type</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification#getAfter <em>After</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getDisconnectionSpecification()
 * @model
 * @generated
 */
public interface DisconnectionSpecification extends Declaration {
	/**
	 * Returns the value of the '<em><b>Disconnect</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disconnect</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disconnect</em>' containment reference.
	 * @see #setDisconnect(NameList)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getDisconnectionSpecification_Disconnect()
	 * @model containment="true"
	 * @generated
	 */
	NameList getDisconnect();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification#getDisconnect <em>Disconnect</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disconnect</em>' containment reference.
	 * @see #getDisconnect()
	 * @generated
	 */
	void setDisconnect(NameList value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getDisconnectionSpecification_Type()
	 * @model containment="true"
	 * @generated
	 */
	Expression getType();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Expression value);

	/**
	 * Returns the value of the '<em><b>After</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>After</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>After</em>' containment reference.
	 * @see #setAfter(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getDisconnectionSpecification_After()
	 * @model containment="true"
	 * @generated
	 */
	Expression getAfter();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification#getAfter <em>After</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>After</em>' containment reference.
	 * @see #getAfter()
	 * @generated
	 */
	void setAfter(Expression value);

} // DisconnectionSpecification
