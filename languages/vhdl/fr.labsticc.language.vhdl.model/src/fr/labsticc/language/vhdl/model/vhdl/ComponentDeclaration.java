/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration#getGeneric <em>Generic</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration#getPort <em>Port</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getComponentDeclaration()
 * @model
 * @generated
 */
public interface ComponentDeclaration extends Declaration {
	/**
	 * Returns the value of the '<em><b>Generic</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic</em>' containment reference.
	 * @see #setGeneric(Generics)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getComponentDeclaration_Generic()
	 * @model containment="true"
	 * @generated
	 */
	Generics getGeneric();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration#getGeneric <em>Generic</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic</em>' containment reference.
	 * @see #getGeneric()
	 * @generated
	 */
	void setGeneric(Generics value);

	/**
	 * Returns the value of the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' containment reference.
	 * @see #setPort(Ports)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getComponentDeclaration_Port()
	 * @model containment="true"
	 * @generated
	 */
	Ports getPort();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration#getPort <em>Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' containment reference.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(Ports value);

} // ComponentDeclaration
