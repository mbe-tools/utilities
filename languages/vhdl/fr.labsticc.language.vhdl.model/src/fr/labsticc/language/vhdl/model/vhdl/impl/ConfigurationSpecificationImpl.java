/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification;
import fr.labsticc.language.vhdl.model.vhdl.Expression;
import fr.labsticc.language.vhdl.model.vhdl.GenericMaps;
import fr.labsticc.language.vhdl.model.vhdl.NameList;
import fr.labsticc.language.vhdl.model.vhdl.PortMaps;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configuration Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationSpecificationImpl#getList <em>List</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationSpecificationImpl#getComponent <em>Component</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationSpecificationImpl#getEntity <em>Entity</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationSpecificationImpl#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationSpecificationImpl#getGenericMap <em>Generic Map</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationSpecificationImpl#getPortMap <em>Port Map</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConfigurationSpecificationImpl extends DeclarationImpl implements ConfigurationSpecification {
	/**
	 * The cached value of the '{@link #getList() <em>List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getList()
	 * @generated
	 * @ordered
	 */
	protected NameList list;

	/**
	 * The cached value of the '{@link #getComponent() <em>Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponent()
	 * @generated
	 * @ordered
	 */
	protected Expression component;

	/**
	 * The cached value of the '{@link #getEntity() <em>Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntity()
	 * @generated
	 * @ordered
	 */
	protected Expression entity;

	/**
	 * The cached value of the '{@link #getConfiguration() <em>Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfiguration()
	 * @generated
	 * @ordered
	 */
	protected Expression configuration;

	/**
	 * The cached value of the '{@link #getGenericMap() <em>Generic Map</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenericMap()
	 * @generated
	 * @ordered
	 */
	protected GenericMaps genericMap;

	/**
	 * The cached value of the '{@link #getPortMap() <em>Port Map</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortMap()
	 * @generated
	 * @ordered
	 */
	protected PortMaps portMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfigurationSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VhdlPackage.eINSTANCE.getConfigurationSpecification();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameList getList() {
		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetList(NameList newList, NotificationChain msgs) {
		NameList oldList = list;
		list = newList;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.CONFIGURATION_SPECIFICATION__LIST, oldList, newList);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setList(NameList newList) {
		if (newList != list) {
			NotificationChain msgs = null;
			if (list != null)
				msgs = ((InternalEObject)list).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.CONFIGURATION_SPECIFICATION__LIST, null, msgs);
			if (newList != null)
				msgs = ((InternalEObject)newList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.CONFIGURATION_SPECIFICATION__LIST, null, msgs);
			msgs = basicSetList(newList, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.CONFIGURATION_SPECIFICATION__LIST, newList, newList));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getComponent() {
		return component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponent(Expression newComponent, NotificationChain msgs) {
		Expression oldComponent = component;
		component = newComponent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.CONFIGURATION_SPECIFICATION__COMPONENT, oldComponent, newComponent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponent(Expression newComponent) {
		if (newComponent != component) {
			NotificationChain msgs = null;
			if (component != null)
				msgs = ((InternalEObject)component).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.CONFIGURATION_SPECIFICATION__COMPONENT, null, msgs);
			if (newComponent != null)
				msgs = ((InternalEObject)newComponent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.CONFIGURATION_SPECIFICATION__COMPONENT, null, msgs);
			msgs = basicSetComponent(newComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.CONFIGURATION_SPECIFICATION__COMPONENT, newComponent, newComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getEntity() {
		return entity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntity(Expression newEntity, NotificationChain msgs) {
		Expression oldEntity = entity;
		entity = newEntity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.CONFIGURATION_SPECIFICATION__ENTITY, oldEntity, newEntity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntity(Expression newEntity) {
		if (newEntity != entity) {
			NotificationChain msgs = null;
			if (entity != null)
				msgs = ((InternalEObject)entity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.CONFIGURATION_SPECIFICATION__ENTITY, null, msgs);
			if (newEntity != null)
				msgs = ((InternalEObject)newEntity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.CONFIGURATION_SPECIFICATION__ENTITY, null, msgs);
			msgs = basicSetEntity(newEntity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.CONFIGURATION_SPECIFICATION__ENTITY, newEntity, newEntity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getConfiguration() {
		return configuration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConfiguration(Expression newConfiguration, NotificationChain msgs) {
		Expression oldConfiguration = configuration;
		configuration = newConfiguration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.CONFIGURATION_SPECIFICATION__CONFIGURATION, oldConfiguration, newConfiguration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfiguration(Expression newConfiguration) {
		if (newConfiguration != configuration) {
			NotificationChain msgs = null;
			if (configuration != null)
				msgs = ((InternalEObject)configuration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.CONFIGURATION_SPECIFICATION__CONFIGURATION, null, msgs);
			if (newConfiguration != null)
				msgs = ((InternalEObject)newConfiguration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.CONFIGURATION_SPECIFICATION__CONFIGURATION, null, msgs);
			msgs = basicSetConfiguration(newConfiguration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.CONFIGURATION_SPECIFICATION__CONFIGURATION, newConfiguration, newConfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericMaps getGenericMap() {
		return genericMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericMap(GenericMaps newGenericMap, NotificationChain msgs) {
		GenericMaps oldGenericMap = genericMap;
		genericMap = newGenericMap;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.CONFIGURATION_SPECIFICATION__GENERIC_MAP, oldGenericMap, newGenericMap);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericMap(GenericMaps newGenericMap) {
		if (newGenericMap != genericMap) {
			NotificationChain msgs = null;
			if (genericMap != null)
				msgs = ((InternalEObject)genericMap).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.CONFIGURATION_SPECIFICATION__GENERIC_MAP, null, msgs);
			if (newGenericMap != null)
				msgs = ((InternalEObject)newGenericMap).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.CONFIGURATION_SPECIFICATION__GENERIC_MAP, null, msgs);
			msgs = basicSetGenericMap(newGenericMap, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.CONFIGURATION_SPECIFICATION__GENERIC_MAP, newGenericMap, newGenericMap));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortMaps getPortMap() {
		return portMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPortMap(PortMaps newPortMap, NotificationChain msgs) {
		PortMaps oldPortMap = portMap;
		portMap = newPortMap;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.CONFIGURATION_SPECIFICATION__PORT_MAP, oldPortMap, newPortMap);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortMap(PortMaps newPortMap) {
		if (newPortMap != portMap) {
			NotificationChain msgs = null;
			if (portMap != null)
				msgs = ((InternalEObject)portMap).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.CONFIGURATION_SPECIFICATION__PORT_MAP, null, msgs);
			if (newPortMap != null)
				msgs = ((InternalEObject)newPortMap).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.CONFIGURATION_SPECIFICATION__PORT_MAP, null, msgs);
			msgs = basicSetPortMap(newPortMap, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.CONFIGURATION_SPECIFICATION__PORT_MAP, newPortMap, newPortMap));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VhdlPackage.CONFIGURATION_SPECIFICATION__LIST:
				return basicSetList(null, msgs);
			case VhdlPackage.CONFIGURATION_SPECIFICATION__COMPONENT:
				return basicSetComponent(null, msgs);
			case VhdlPackage.CONFIGURATION_SPECIFICATION__ENTITY:
				return basicSetEntity(null, msgs);
			case VhdlPackage.CONFIGURATION_SPECIFICATION__CONFIGURATION:
				return basicSetConfiguration(null, msgs);
			case VhdlPackage.CONFIGURATION_SPECIFICATION__GENERIC_MAP:
				return basicSetGenericMap(null, msgs);
			case VhdlPackage.CONFIGURATION_SPECIFICATION__PORT_MAP:
				return basicSetPortMap(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VhdlPackage.CONFIGURATION_SPECIFICATION__LIST:
				return getList();
			case VhdlPackage.CONFIGURATION_SPECIFICATION__COMPONENT:
				return getComponent();
			case VhdlPackage.CONFIGURATION_SPECIFICATION__ENTITY:
				return getEntity();
			case VhdlPackage.CONFIGURATION_SPECIFICATION__CONFIGURATION:
				return getConfiguration();
			case VhdlPackage.CONFIGURATION_SPECIFICATION__GENERIC_MAP:
				return getGenericMap();
			case VhdlPackage.CONFIGURATION_SPECIFICATION__PORT_MAP:
				return getPortMap();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VhdlPackage.CONFIGURATION_SPECIFICATION__LIST:
				setList((NameList)newValue);
				return;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__COMPONENT:
				setComponent((Expression)newValue);
				return;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__ENTITY:
				setEntity((Expression)newValue);
				return;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__CONFIGURATION:
				setConfiguration((Expression)newValue);
				return;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__GENERIC_MAP:
				setGenericMap((GenericMaps)newValue);
				return;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__PORT_MAP:
				setPortMap((PortMaps)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VhdlPackage.CONFIGURATION_SPECIFICATION__LIST:
				setList((NameList)null);
				return;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__COMPONENT:
				setComponent((Expression)null);
				return;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__ENTITY:
				setEntity((Expression)null);
				return;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__CONFIGURATION:
				setConfiguration((Expression)null);
				return;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__GENERIC_MAP:
				setGenericMap((GenericMaps)null);
				return;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__PORT_MAP:
				setPortMap((PortMaps)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VhdlPackage.CONFIGURATION_SPECIFICATION__LIST:
				return list != null;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__COMPONENT:
				return component != null;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__ENTITY:
				return entity != null;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__CONFIGURATION:
				return configuration != null;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__GENERIC_MAP:
				return genericMap != null;
			case VhdlPackage.CONFIGURATION_SPECIFICATION__PORT_MAP:
				return portMap != null;
		}
		return super.eIsSet(featureID);
	}

} //ConfigurationSpecificationImpl
