/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.QuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Quantity Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class QuantityDeclarationImpl extends DeclarationImpl implements QuantityDeclaration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QuantityDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VhdlPackage.eINSTANCE.getQuantityDeclaration();
	}

} //QuantityDeclarationImpl
