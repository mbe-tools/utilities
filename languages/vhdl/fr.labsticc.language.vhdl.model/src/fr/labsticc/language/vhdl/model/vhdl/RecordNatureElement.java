/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import java.lang.String;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Nature Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement#getName <em>Name</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement#getNature <em>Nature</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getRecordNatureElement()
 * @model
 * @generated
 */
public interface RecordNatureElement extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getRecordNatureElement_Name()
	 * @model unique="false"
	 * @generated
	 */
	EList<String> getName();

	/**
	 * Returns the value of the '<em><b>Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nature</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nature</em>' containment reference.
	 * @see #setNature(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getRecordNatureElement_Nature()
	 * @model containment="true"
	 * @generated
	 */
	Expression getNature();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement#getNature <em>Nature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nature</em>' containment reference.
	 * @see #getNature()
	 * @generated
	 */
	void setNature(Expression value);

} // RecordNatureElement
