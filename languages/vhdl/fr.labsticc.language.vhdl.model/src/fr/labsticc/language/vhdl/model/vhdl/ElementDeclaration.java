/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import java.lang.String;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getElementDeclaration()
 * @model
 * @generated
 */
public interface ElementDeclaration extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' attribute list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getElementDeclaration_Identifier()
	 * @model unique="false"
	 * @generated
	 */
	EList<String> getIdentifier();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getElementDeclaration_Type()
	 * @model containment="true"
	 * @generated
	 */
	Expression getType();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Expression value);

} // ElementDeclaration
